# [2.6.0](https://code.eyeteam.vn/etop/web/compare/2.5.0...2.6.0) (2019-10-16)


### Bug Fixes

* **affiliate:** change config price ([31ff819](https://code.eyeteam.vn/etop/web/commits/31ff8196e5fe65639a33753fe893f5bdc83647c5))
* **affiliate:** fix copy link product, environment ([93112a7](https://code.eyeteam.vn/etop/web/commits/93112a715d60f0886493c4754f665f73afca50a3))
* **affiliate:** fix create Referral, edit product row ([c2dfb42](https://code.eyeteam.vn/etop/web/commits/c2dfb42f95c6e08ac3bfa299927eafc98330fa5c))
* **affiliate:** fix link invite ([001fe80](https://code.eyeteam.vn/etop/web/commits/001fe80ef7dde577c1bbb86f3f3718586be697dd))
* **affiliate:** fix register and init referral ([bfe2d58](https://code.eyeteam.vn/etop/web/commits/bfe2d5807b14fc11e45e4db95810aa1d623d90c2))
* **affiliate:** map price product and edit icon ranking ([4b53380](https://code.eyeteam.vn/etop/web/commits/4b5338090d92604f5b35d4ed147576188b585bc7))
* **shop:** [orders]remove next-pre button at first-last page ([6d64ac8](https://code.eyeteam.vn/etop/web/commits/6d64ac814a4e9d353658693fd8f376b9f1c34aca)), closes [#13hd8](https://code.eyeteam.vn/etop/web/issues/13hd8)
* **shop:** add loading state when getting shipping service packages ([8f6817e](https://code.eyeteam.vn/etop/web/commits/8f6817e037c637d69674191fafc6578181543d38))
* **shop:** add tooltip (developing) for barcode POS ([30fe0a1](https://code.eyeteam.vn/etop/web/commits/30fe0a1b6b6fc1a09075b1118b3b9a730fe02740)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** add tooltip new customer POS ([7583e3b](https://code.eyeteam.vn/etop/web/commits/7583e3bb0b09ee8bfab2d269878e7540b3b047a1)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** bad performance in Create ticket comment ([c3f032c](https://code.eyeteam.vn/etop/web/commits/c3f032c4544262a92b77719f549b2e110ac180f3))
* **shop:** can't send request ticket- end state (orders) ([6247012](https://code.eyeteam.vn/etop/web/commits/6247012242424a382e9f79aad88281197e0350bd)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** chang label table name (orders) ([dfd56ee](https://code.eyeteam.vn/etop/web/commits/dfd56eeaebf0c81a3e4e5e63d763e5c6b856c9bb))
* **shop:** change column of service in delivery now ([7c302c6](https://code.eyeteam.vn/etop/web/commits/7c302c668e2342f5dff0a9efe4385dc5e7d7e049)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** check fullname before address (on-boarding) ([72254ba](https://code.eyeteam.vn/etop/web/commits/72254ba3c279295cde94ed494c3c79f5f83b5402)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** click item to detail in product trading ([7aab205](https://code.eyeteam.vn/etop/web/commits/7aab205c9bf88a0a45e7d0105ed902462d6aaf1e))
* **shop:** CUSTOMER - fix bug update in customer detail but not change in customer list ([0806fd6](https://code.eyeteam.vn/etop/web/commits/0806fd6311034e115dd3eaabecc7b7e4f0700e72))
* **shop:** CUSTOMER issues ([5b8305e](https://code.eyeteam.vn/etop/web/commits/5b8305ed044c49ff0e1ee880f5f3c3a17ff18730)), closes [#1fp8e8](https://code.eyeteam.vn/etop/web/issues/1fp8e8)
* **shop:** display price (order.total_amount) in orders ([b46f221](https://code.eyeteam.vn/etop/web/commits/b46f221de7294e69572db4aaec9e58313d06fc57))
* **shop:** edit tooltip prev version ([ec4c09b](https://code.eyeteam.vn/etop/web/commits/ec4c09b5a34cdef27bec2ddc80f8523490408313))
* **shop:** enable print order button (without ffm) ([5a24206](https://code.eyeteam.vn/etop/web/commits/5a24206b2e5540a724c432d4bb6856deb2f7b7b2))
* **shop:** fix bug "name of undefined" when create customer ([b8323eb](https://code.eyeteam.vn/etop/web/commits/b8323eb1a5aaaa2934e359870b45206c82fd3a5b))
* **shop:** fix bug "province of null" when creating ffm in order detail ([e2c740c](https://code.eyeteam.vn/etop/web/commits/e2c740cb08663417ac417a005dc2ae8dcfec68ec))
* **shop:** fix bug cannot create customer's address in ORDER DETAIL ([4103bee](https://code.eyeteam.vn/etop/web/commits/4103bee6edd856fc01bc116b7c33aacba65ff6d4))
* **shop:** fix bug cannot go to POS in order list ([8ab0831](https://code.eyeteam.vn/etop/web/commits/8ab0831b4904d440c56d7f30138dcebd9c7c0b0f))
* **shop:** fix bug cannot select discount_type in POS ([64f7539](https://code.eyeteam.vn/etop/web/commits/64f75395ed283dc6fbc5afb6bfb4d6cc36de843a))
* **shop:** fix bug check all always reload page after import order ([126ba44](https://code.eyeteam.vn/etop/web/commits/126ba44223b2d2a1bceabea97192823939189f67))
* **shop:** fix bug create order failed 2nd time in list orders ([75b5c09](https://code.eyeteam.vn/etop/web/commits/75b5c09b4fb751843bb963faf673f7490af3d69f)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** fix bug create-only-order failed the 2nd time doing it in POS ([8eec3ba](https://code.eyeteam.vn/etop/web/commits/8eec3ba55ec0c31faf000ba35a91d012262d1746))
* **shop:** fix bug duplicate customer after create order failed ([eb23964](https://code.eyeteam.vn/etop/web/commits/eb239648cc7e7dcbef451bc2e799a0806de6f24a))
* **shop:** fix bug missing address when dismiss address modal ([fd64949](https://code.eyeteam.vn/etop/web/commits/fd64949e80b775e6e1df40e5a50b45a3655c0ad6))
* **shop:** fix bug no locations in on-boarding page ([e941b2a](https://code.eyeteam.vn/etop/web/commits/e941b2a9a42d81099709d0ff4f19ecd3929e6a2d))
* **shop:** fix bug order lists ([096b186](https://code.eyeteam.vn/etop/web/commits/096b18619c8497a0c30accd654835481ace8f0a8))
* **shop:** fix bug white screen in ffm list ([50f43aa](https://code.eyeteam.vn/etop/web/commits/50f43aa234fe29ec37b6c9720b14fa14bfbe4cd0))
* **shop:** fix callback link vtpay and scroll form checkout trading ([1955b79](https://code.eyeteam.vn/etop/web/commits/1955b79ddf2fafb607b90374605773f97cf97709))
* **shop:** fix core button prev old ui in sidebar ([5c57d4c](https://code.eyeteam.vn/etop/web/commits/5c57d4c125868c1ecc647d44d6d3a991ffd63b9a))
* **shop:** fix duplicate customer address when create order in POS ([971e6cb](https://code.eyeteam.vn/etop/web/commits/971e6cb3263bbdb515e2f0d6c9d7871a7b99c0d4))
* **shop:** fix duplicate imports line cause by rebase to master ([6fb651c](https://code.eyeteam.vn/etop/web/commits/6fb651c007c7c92cbc61c6dd306c166f55c75eb1))
* **shop:** fix logic pre-select shipping service packages ([916ec97](https://code.eyeteam.vn/etop/web/commits/916ec97a344ed4c3ded6bc9774a9dddb0a0e3f9c))
* **shop:** fix map price, promotion eTop trading ([eca87c0](https://code.eyeteam.vn/etop/web/commits/eca87c08dcb50aa38773b00a46a8dc73b7a0b2aa))
* **shop:** fix state no customer existed in POS ([d1f5167](https://code.eyeteam.vn/etop/web/commits/d1f51678997a969d88fea97b895154e87d0a96f6))
* **shop:** fix UI shipnow service packages in order list ([611e261](https://code.eyeteam.vn/etop/web/commits/611e261f609aef32796f826ae1fe96c2008ed82c))
* **shop:** hidden overflow-x multiple order delivery fast (orders) ([b032e55](https://code.eyeteam.vn/etop/web/commits/b032e55f85ef20c8e42c782cf795d5395fed822b)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** hidden paging empty orders list ([d314432](https://code.eyeteam.vn/etop/web/commits/d31443289300f8ee47d218e21e84aa26b7251b0c))
* **shop:** hide etop trading products ([ae2dbac](https://code.eyeteam.vn/etop/web/commits/ae2dbaca76b838d680cc9abd84119a207a44b848))
* **shop:** hide scroolbar if ticket comment null ([ae7fb2c](https://code.eyeteam.vn/etop/web/commits/ae7fb2c8c223bd2ed683c8ba1f7f3256b74178af))
* **shop:** install gtag ([313ddeb](https://code.eyeteam.vn/etop/web/commits/313ddeb71fa8c5f56057526acaa04a5f27a46146))
* **shop:** large 'x' button in modal ([322fd67](https://code.eyeteam.vn/etop/web/commits/322fd679c9380d5ca896e9f723bff2ab6dbf7bef))
* **shop:** missing code, non disable print order ([e466c26](https://code.eyeteam.vn/etop/web/commits/e466c2685ba8d0527417c009e13f444cf791e80e)), closes [#1fg1](https://code.eyeteam.vn/etop/web/issues/1fg1)
* **shop:** multiple order action-print order (orders) ([1035446](https://code.eyeteam.vn/etop/web/commits/10354467ffc3b21e42eed2f23897df270df1ad99)), closes [#1fp8](https://code.eyeteam.vn/etop/web/issues/1fp8)
* **shop:** not use default gender as male any more ([fc4129d](https://code.eyeteam.vn/etop/web/commits/fc4129d10e3edf0d783fa89c82a5d69b222e0d10))
* **shop:** on-boarding create shop direct ([934ad69](https://code.eyeteam.vn/etop/web/commits/934ad69185513d68b2a21f042444e0b7351f7aef))
* **shop:** open pos in new tab ([4ce9a43](https://code.eyeteam.vn/etop/web/commits/4ce9a431e8755d961f277fde25678c66620b3be5)), closes [#1fq8c4](https://code.eyeteam.vn/etop/web/issues/1fq8c4)
* **shop:** redirect in shop setting ([ab382db](https://code.eyeteam.vn/etop/web/commits/ab382db45edf88c3ac5236d6a5d443d4e8973b34))
* **shop:** redirect to create new address in delivery fast ([fd071fb](https://code.eyeteam.vn/etop/web/commits/fd071fb73a89f443543787b43dec2e11f1ac1471)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** remove button cancel orders if orders canceled ([ab7b818](https://code.eyeteam.vn/etop/web/commits/ab7b818e431a1d2b0b9b3b44e1bff412b13e3ebb))
* **shop:** reset shipping UI after updating order ([448e238](https://code.eyeteam.vn/etop/web/commits/448e238aeda0f30bec90c3751593164b4cf53ef8)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** responsive-order-details ([1234380](https://code.eyeteam.vn/etop/web/commits/12343809fbf430db551ffdcbd2156908ac0a53d4)), closes [#1fh677](https://code.eyeteam.vn/etop/web/issues/1fh677)
* **shop:** select item behaviours in customer list ([07e575e](https://code.eyeteam.vn/etop/web/commits/07e575e255386b7d459bcbc143ea0a0f9944b4f5))
* **shop:** set placement bottom at first item in multiple delivery now ([076d07b](https://code.eyeteam.vn/etop/web/commits/076d07b4db51bb7c44e9271ed478d5a44ee11a04)), closes [#1fp6](https://code.eyeteam.vn/etop/web/issues/1fp6)
* **shop:** sync content 'Bao hiem cho hang hoa' ([28ed7ea](https://code.eyeteam.vn/etop/web/commits/28ed7ea9afddc94e6bce67a0c1e3e3085a94c7a2))
* **shop:** UI for user in charge and shop on boarding (register) ([3316070](https://code.eyeteam.vn/etop/web/commits/3316070860510b7cccda9fc4da1dbbcd85d2d953)), closes [#1fjck4](https://code.eyeteam.vn/etop/web/issues/1fjck4)
* **shop:** UI login signup button (register) ([d34705e](https://code.eyeteam.vn/etop/web/commits/d34705e2cd6117a7e9c82fbb7f58a54b08757e2c)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** UI tab customer info ([2ba14f4](https://code.eyeteam.vn/etop/web/commits/2ba14f41e17ac6f25fecaf23f9a597c9b5af9c6a)), closes [#1fm955](https://code.eyeteam.vn/etop/web/issues/1fm955)
* **shop:** wrong in notice content ([53d207b](https://code.eyeteam.vn/etop/web/commits/53d207b41206a985a96bf67f01381882fbbbb381))
* **supply:** change logic price ([c5bb4d7](https://code.eyeteam.vn/etop/web/commits/c5bb4d77fd89e07db7e51b0e335808322fd09aff))
* **supply:** change perpage ([7979873](https://code.eyeteam.vn/etop/web/commits/7979873f629c8f2ed68349514aeb61cd59ae185c))
* **supply:** fix map price product ([57bf533](https://code.eyeteam.vn/etop/web/commits/57bf5331ce6d0b601ef1facf298e6a86ba6fe06f))
* fix http service ([6436cbb](https://code.eyeteam.vn/etop/web/commits/6436cbb91b2b043c850a4989adb51fbc65618482))
* fix import environment ([4ec301a](https://code.eyeteam.vn/etop/web/commits/4ec301a5bffb80cc3e7e9ba66c4759c670af909f))
* **supply:** fix price supply promotion ([2cc143a](https://code.eyeteam.vn/etop/web/commits/2cc143af66ee84ea13f34e5396ca0df7eb0e552a))
* **supply:** fix supply commission value ([302de2b](https://code.eyeteam.vn/etop/web/commits/302de2bbad5196b700a96b396961af458146290f))
* fix tooltip css ([5a1af8f](https://code.eyeteam.vn/etop/web/commits/5a1af8ff135b0f38b51cc018edfb8a8673758e8c))


### Code Refactoring

* **core:** fix single-user-info ([49490ae](https://code.eyeteam.vn/etop/web/commits/49490ae9ffb65929cc3c7a5b84f268d14f520db7))


### Features

* **admin:** init admin client ([b0c1974](https://code.eyeteam.vn/etop/web/commits/b0c19744830265ec2b0ac058074c6c627caf2514))
* **affiliate:** add popup guide and remove gaService ([4b77e99](https://code.eyeteam.vn/etop/web/commits/4b77e994390da0612fa341e8653b3a51487ff667))
* **affiliate:** GetReferrals ([b9d8a0c](https://code.eyeteam.vn/etop/web/commits/b9d8a0c9d99c1a62f47928febc9c1620f60c72fd))
* **affiliate:** guide link in component ([7d178db](https://code.eyeteam.vn/etop/web/commits/7d178db0cac160dfc6f96292c8a72d8028c49f5a))
* **affiliate:** init affiliate client ([469dd0b](https://code.eyeteam.vn/etop/web/commits/469dd0bd716f240a6c407251f7ece9c7cac45130))
* **affiliate:** store referral for last click ([1ad8eeb](https://code.eyeteam.vn/etop/web/commits/1ad8eeb15d4ee9e8eddabfc3c9062f865aeb5fc9))
* **shop:** add customer_id to pos ([a19422a](https://code.eyeteam.vn/etop/web/commits/a19422a0fb068ffaa7972be787bb4f5cb7932e42))
* **shop:** add empty view for list views ([d27c2d7](https://code.eyeteam.vn/etop/web/commits/d27c2d738dba20d7c2155695f6c0c8f886584fbe))
* **shop:** add previous version btn ([fc28715](https://code.eyeteam.vn/etop/web/commits/fc287155ba401c71d8389826496baadb41759fb3)), closes [#1fke6](https://code.eyeteam.vn/etop/web/issues/1fke6)
* **shop:** admin login ([f233627](https://code.eyeteam.vn/etop/web/commits/f23362747314c938f04549117be048b5bb470625))
* **shop:** Chức năng mới của Nhân ([7c1a5b6](https://code.eyeteam.vn/etop/web/commits/7c1a5b66a2211804c4c2b5ba8e445147056e3225))
* **shop:** collections & category & tags in products ([ec5f18e](https://code.eyeteam.vn/etop/web/commits/ec5f18e5875004da463eedee8612d693c87797c5))
* **shop:** customer in POS ([fcf6ca2](https://code.eyeteam.vn/etop/web/commits/fcf6ca241017f578e89e545099a334ccfcc2207d))
* **shop:** customers in orders list ([574dedf](https://code.eyeteam.vn/etop/web/commits/574dedfe53af69c97058e4234f5cbb2441c65ae8))
* **shop:** disallowed input customer code (auto-generated by system) ([9162850](https://code.eyeteam.vn/etop/web/commits/9162850b1a9b4c1a3002d8a81cb9efe004995ed9)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** display orders in customer detail ([e312729](https://code.eyeteam.vn/etop/web/commits/e312729912b8719982380a532f2e478c59c36190))
* **shop:** image shop ready on-boarding ([402c0c3](https://code.eyeteam.vn/etop/web/commits/402c0c3b8d860550ece4a51079e67d4f348885f5))
* **shop:** import orders ([798b2b5](https://code.eyeteam.vn/etop/web/commits/798b2b5cd533a6374ce24e2c12997e93863dc32f))
* **shop:** important announcement popup ([e9c9811](https://code.eyeteam.vn/etop/web/commits/e9c98113de121da86da7b25b0a493e8b5ca581d9))
* **shop:** init eTop trading ([eb60972](https://code.eyeteam.vn/etop/web/commits/eb60972824777f0649d7daf83cdaf70b044b2d1a))
* **shop:** new list orders ([23edbdd](https://code.eyeteam.vn/etop/web/commits/23edbdd1a1af16e55e7c8edb55fdf577c71312b4))
* **shop:** noti old orders not applied for customers (partners) ([5f2b6f5](https://code.eyeteam.vn/etop/web/commits/5f2b6f5002bbe91b639aa38579669f8dfa2aa29b)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** notification ([fc29c17](https://code.eyeteam.vn/etop/web/commits/fc29c17281a6b26be076d9443e6db5cb0e366a33))
* **shop:** order list - shipnow ([097d49f](https://code.eyeteam.vn/etop/web/commits/097d49ff59cb271ed80386159528dd86c7b8575a))
* **shop:** partners management ([011cc94](https://code.eyeteam.vn/etop/web/commits/011cc94f199ff32c9e1279df3f23308026df29d1))
* **shop:** re-create ffm for error fulfillments (no shipping_code) ([82a9c4c](https://code.eyeteam.vn/etop/web/commits/82a9c4c60561bac090de59423a2d3cd1b30a719c)), closes [#1fk32](https://code.eyeteam.vn/etop/web/issues/1fk32)
* **shop:** select label to focus input POS ([4bb0e3f](https://code.eyeteam.vn/etop/web/commits/4bb0e3ffab84d816af83397831517b2a355c0098)), closes [#1dmqv4](https://code.eyeteam.vn/etop/web/issues/1dmqv4)
* **shop:** support_request, ffm_histories & exports in order_list ([f06e8fc](https://code.eyeteam.vn/etop/web/commits/f06e8fc862ddbc963c6b089e5e1c42f7b88df34e))
* **shop:** temporarily hide customer group column ([272415a](https://code.eyeteam.vn/etop/web/commits/272415a1383bfc11b9d28754c2d43912529b9fbc)), closes [#1](https://code.eyeteam.vn/etop/web/issues/1)
* **shop:** update list ffms ([6128d23](https://code.eyeteam.vn/etop/web/commits/6128d239a564f2158fffab3b4bdd718ec73aa5f2))
* **shop:** update list products ([c47c0c1](https://code.eyeteam.vn/etop/web/commits/c47c0c1325cecb598546e072285904fa34ef0c36))
* **shop:** view customer ([e6ebca7](https://code.eyeteam.vn/etop/web/commits/e6ebca7737a02a722fda6258d624c33529cdd06a))
* **supply:** init supply ([824369e](https://code.eyeteam.vn/etop/web/commits/824369e6c8c9ee3eeb3111919e518be9882c276d))
* **supply:** init supply client ([4acef53](https://code.eyeteam.vn/etop/web/commits/4acef534faac89eb82aaefde446a045b01133bd0))


### BREAKING CHANGES

* **core:** rename core-aff-user-info -> core-single-user-info

Change-Id: Ic08a032f62b18fbaa3883f0317924ffef546c8e5



# [2.5.0](https://code.eyeteam.vn/etop/web/compare/2.4.0...2.5.0) (2019-09-17)


### Bug Fixes

* **shop:** bug header no shown after logged in ([6a4830a](https://code.eyeteam.vn/etop/web/commits/6a4830a))
* 🐛 fix run affiliate ([3c8f31e](https://code.eyeteam.vn/etop/web/commits/3c8f31e))
* **shop:** fix issues admin/locationService was used in shop module ([dc104e7](https://code.eyeteam.vn/etop/web/commits/dc104e7))
* **shop:** fix POS order-lines (init line issues) ([91d3898](https://code.eyeteam.vn/etop/web/commits/91d3898))


### Features

* **affiliate:** Init Affiliate app ([f3b8b43](https://code.eyeteam.vn/etop/web/commits/f3b8b43))
* **shop:** new header dropdown ([7ce9899](https://code.eyeteam.vn/etop/web/commits/7ce9899))
* **shop:** new POS ([d33ac0f](https://code.eyeteam.vn/etop/web/commits/d33ac0f))
* 🎸 setup affiliate routes ([a852d30](https://code.eyeteam.vn/etop/web/commits/a852d30))
* **shop:** product issue and create product ([7b34b63](https://code.eyeteam.vn/etop/web/commits/7b34b63))



# [2.4.0](https://code.eyeteam.vn/etop/web/compare/2.3.0...2.4.0) (2019-08-27)


### Bug Fixes

* **shop:** bug sidebar in firefox and edge ([0f6a2e8](https://code.eyeteam.vn/etop/web/commits/0f6a2e8))
* **shop:** check duplicate material tag input ([25c5127](https://code.eyeteam.vn/etop/web/commits/25c5127))
* **shop:** edit styles of core buttons (padding & distance btw 2 btns in header) ([4dbff00](https://code.eyeteam.vn/etop/web/commits/4dbff00))
* **shop:** fix bug logging in always required despite already logged in ([2b86d52](https://code.eyeteam.vn/etop/web/commits/2b86d52))
* **shop:** fix bug not clear auth after logged out ([2413748](https://code.eyeteam.vn/etop/web/commits/2413748))
* **shop:** fix bug switch account in header ([42ddc86](https://code.eyeteam.vn/etop/web/commits/42ddc86))
* **shop:** fix etop table accessible event handler ([b6ea309](https://code.eyeteam.vn/etop/web/commits/b6ea309))
* **shop:** fix fb pixel and ga not working ([c0d0566](https://code.eyeteam.vn/etop/web/commits/c0d0566))
* **shop:** fix tables in prodList & ffmList. Make them a more unifying style. ([52ce494](https://code.eyeteam.vn/etop/web/commits/52ce494))
* **shop:** fix UI order-list ([ced93a7](https://code.eyeteam.vn/etop/web/commits/ced93a7))
* **shop:** fix UI POS (components using in common with list-orders) ([325a1fb](https://code.eyeteam.vn/etop/web/commits/325a1fb))
* **shop:** fix UI POS: buttons +/- quantities & select-multiline ([73fab7c](https://code.eyeteam.vn/etop/web/commits/73fab7c))
* **shop:** fix vtiger config environment ([f8ab37b](https://code.eyeteam.vn/etop/web/commits/f8ab37b))
* **shop:** hidden filter, edit product row in view products ([c7e55f5](https://code.eyeteam.vn/etop/web/commits/c7e55f5))
* **shop:** load filter "label" instead of "name" for filter selecting ([68f733f](https://code.eyeteam.vn/etop/web/commits/68f733f))
* **shop:** money transaction detail, pagination ([7329171](https://code.eyeteam.vn/etop/web/commits/7329171))
* **shop:** move code GoogleAnalyticsObject inside head ([48d8b22](https://code.eyeteam.vn/etop/web/commits/48d8b22))
* **shop:** no shop_info in order_list ([d25dae4](https://code.eyeteam.vn/etop/web/commits/d25dae4))
* **shop:** print bill and navigate after login ([b616c33](https://code.eyeteam.vn/etop/web/commits/b616c33))
* **shop:** table product issue ui ([21efb3d](https://code.eyeteam.vn/etop/web/commits/21efb3d))
* **shop:** ui login, css ([9a7dda5](https://code.eyeteam.vn/etop/web/commits/9a7dda5))
* **shop:** update deploy script (eyeteam) ([3635d1e](https://code.eyeteam.vn/etop/web/commits/3635d1e))


### Features

* **shop:** add ticket to orders ([99f4723](https://code.eyeteam.vn/etop/web/commits/99f4723))
* **shop:** beta deploy script for shop ([38de283](https://code.eyeteam.vn/etop/web/commits/38de283))
* **shop:** copy old-POS to new repo ([1bd612c](https://code.eyeteam.vn/etop/web/commits/1bd612c))
* **shop:** copy-old-list-order(bill-print & ahamove) ([fc6015a](https://code.eyeteam.vn/etop/web/commits/fc6015a))
* **shop:** create instant product ([0f65200](https://code.eyeteam.vn/etop/web/commits/0f65200))
* **shop:** create new shop ([0fe6b3d](https://code.eyeteam.vn/etop/web/commits/0fe6b3d))
* **shop:** filter in products, orders, fulfillments ([62e80ab](https://code.eyeteam.vn/etop/web/commits/62e80ab))
* **shop:** forgot password ([8658a6a](https://code.eyeteam.vn/etop/web/commits/8658a6a))
* **shop:** hide sidebar & header in POS ([4cd0661](https://code.eyeteam.vn/etop/web/commits/4cd0661))
* **shop:** list shipnow ffms ([6492ed5](https://code.eyeteam.vn/etop/web/commits/6492ed5))
* **shop:** more bug ui, add sidebar ([be97257](https://code.eyeteam.vn/etop/web/commits/be97257))
* **shop:** placeholder for etop-table ([2fee093](https://code.eyeteam.vn/etop/web/commits/2fee093))
* **shop:** redirect to list orders after logged in ([9d58903](https://code.eyeteam.vn/etop/web/commits/9d58903))
* **shop:** shared filter ([a54d60f](https://code.eyeteam.vn/etop/web/commits/a54d60f))
* **shop:** telegram to production groups if next.etop.vn ([b974810](https://code.eyeteam.vn/etop/web/commits/b974810))
* **shop:** update ga and fb tracking code ([2cb365d](https://code.eyeteam.vn/etop/web/commits/2cb365d))
* **shop:** update info setting shop ([1afb160](https://code.eyeteam.vn/etop/web/commits/1afb160))



# 2.3.0 (2019-07-17)


### Bug Fixes

* sidebar auto detect active menu item ([5cfe4ec](https://code.eyeteam.vn/etop/web/commits/5cfe4ec))
* **shop:** fix button icon style, slider header title style ([13e9dbb](https://code.eyeteam.vn/etop/web/commits/13e9dbb))
* **shop:** fix product build ([882f304](https://code.eyeteam.vn/etop/web/commits/882f304))
* **shop:** fix slider title and product select mode behavior ([a385409](https://code.eyeteam.vn/etop/web/commits/a385409))
* **shop:** onboarding page & onboarding popup ([2ec8f3a](https://code.eyeteam.vn/etop/web/commits/2ec8f3a))


### Features

* **core:** BaseClass for custom form model component ([50636c1](https://code.eyeteam.vn/etop/web/commits/50636c1))
* **shop:** copy order page from old repo ([d69e85b](https://code.eyeteam.vn/etop/web/commits/d69e85b))
* **shop:** core-etop-table allow resetting pagination ([65c48ae](https://code.eyeteam.vn/etop/web/commits/65c48ae))
* **shop:** create product variant attribute ([1d6da50](https://code.eyeteam.vn/etop/web/commits/1d6da50))
* **shop:** dialog controller & confirm dialog ([bce920c](https://code.eyeteam.vn/etop/web/commits/bce920c))
* **shop:** header controller & notification bell ([93d633d](https://code.eyeteam.vn/etop/web/commits/93d633d))
* **shop:** Product detail, edit product on drawer ([33a39be](https://code.eyeteam.vn/etop/web/commits/33a39be))
* **shop:** Product list page: import product popup ([45392b2](https://code.eyeteam.vn/etop/web/commits/45392b2))
* **shop:** shop setting page ([5b49d64](https://code.eyeteam.vn/etop/web/commits/5b49d64))


### BREAKING CHANGES

* **shop:** - deprecated shop-crm api method. use crm-api-service instead.
- deprecated environment, use ConfigSerivce instead.

Change-Id: I0d6b3e53ebac435169039d30dfcb6c5d5cf6ca15
* **shop:**  - Deprecated Variant model , use ProductSrcVariant instead.
 - make ShopProductApi._productMap, ShopProductApi._variantMap static.

Change-Id: Iac6ae4deee6b1f5cd69db57ae958aeb6611dfbb2
* **shop:** delete core/service/header.service.ts. use
core/components/header/header-controller.service.ts instead

Change-Id: Ife84d1e837d58acda547687cf9576a8bbc13c315



# 2.2.0 (2019-07-17)


### Bug Fixes

* sidebar auto detect active menu item ([5cfe4ec](https://code.eyeteam.vn/etop/web/commits/5cfe4ec))
* **shop:** fix button icon style, slider header title style ([13e9dbb](https://code.eyeteam.vn/etop/web/commits/13e9dbb))
* **shop:** fix product build ([882f304](https://code.eyeteam.vn/etop/web/commits/882f304))
* **shop:** fix slider title and product select mode behavior ([a385409](https://code.eyeteam.vn/etop/web/commits/a385409))
* **shop:** onboarding page & onboarding popup ([2ec8f3a](https://code.eyeteam.vn/etop/web/commits/2ec8f3a))


### Features

* **core:** BaseClass for custom form model component ([50636c1](https://code.eyeteam.vn/etop/web/commits/50636c1))
* **shop:** copy order page from old repo ([d69e85b](https://code.eyeteam.vn/etop/web/commits/d69e85b))
* **shop:** core-etop-table allow resetting pagination ([65c48ae](https://code.eyeteam.vn/etop/web/commits/65c48ae))
* **shop:** create product variant attribute ([1d6da50](https://code.eyeteam.vn/etop/web/commits/1d6da50))
* **shop:** dialog controller & confirm dialog ([bce920c](https://code.eyeteam.vn/etop/web/commits/bce920c))
* **shop:** header controller & notification bell ([93d633d](https://code.eyeteam.vn/etop/web/commits/93d633d))
* **shop:** Product detail, edit product on drawer ([33a39be](https://code.eyeteam.vn/etop/web/commits/33a39be))
* **shop:** Product list page: import product popup ([45392b2](https://code.eyeteam.vn/etop/web/commits/45392b2))
* **shop:** shop setting page ([5b49d64](https://code.eyeteam.vn/etop/web/commits/5b49d64))


### BREAKING CHANGES

* **shop:** - deprecated shop-crm api method. use crm-api-service instead.
- deprecated environment, use ConfigSerivce instead.

Change-Id: I0d6b3e53ebac435169039d30dfcb6c5d5cf6ca15
* **shop:**  - Deprecated Variant model , use ProductSrcVariant instead.
 - make ShopProductApi._productMap, ShopProductApi._variantMap static.

Change-Id: Iac6ae4deee6b1f5cd69db57ae958aeb6611dfbb2
* **shop:** delete core/service/header.service.ts. use
core/components/header/header-controller.service.ts instead

Change-Id: Ife84d1e837d58acda547687cf9576a8bbc13c315



