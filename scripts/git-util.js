const simpleGit = require('simple-git');
const git = simpleGit();
function escape(str) {
  return str
}

module.exports = {
  async getGitInfo(scope) {
    const author = await git.raw(['config', '--get', 'user.name']);
    const branch = await git.raw(['branch', '--show-current']);
    let gitLog = await git.log(['-n 100']);
    if(scope) {
      const re = new RegExp(`\\((${scope}|core|shared)\\)`);
      gitLog.all = gitLog.all.filter(log => re.test(log.message));
    }
    gitLog.all = gitLog.all.slice(0, 15);
    const changes = gitLog.all.map(log => log.message)
      .map(change => `- ${escape(change)}`)
      .join('\n');
    const changeList = gitLog.all.map(log => /Reviewed-on: (.*?)\n/gmi.test(log.body) && /Reviewed-on: (.*?)\n/gmi.exec(log.body)[1])
      .filter(cl => !!cl)
      .map(cl => `- ${escape(cl)}`)
      .join('\n');
    const issues = gitLog.all.map(log => log.body.match(/(https:\/\/github.com\/.+)/gmi) || [])
      .reduce((a,b) => a.concat(b))
      .filter(issue => !!issue)
      .map(issue => `- ${escape(issue)}`)
      .join('\n');

    return {author, branch, gitLog, changes, changeList, issues};
  },
  parseTemplate(template, data) {
    let message = template;
    for (const [key, value] of Object.entries(data)) {
      message = message.replace(`$$${key}`, value);
    }
    return message;
  }
}
