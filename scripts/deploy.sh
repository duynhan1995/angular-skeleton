#!/bin/bash
cd ..
export ETOPDIR=$(pwd)
export PROJECT_DIR=$(pwd)

cd $ETOPDIR/etop-config
git pull
cd $ETOPDIR

USAGE="Usage: deploy_next.sh dev|sandbox|v1prod supplier|shop|next|psx|cmx|topship-app|admin|auth|esdk-doc|internal-misc"

PROJECT=extopvn
CONTEXT=dev
ZONE=asia-southeast1-b
CLUSTER=etop-dev
EXEC_FILE=publish.sh

function switch-cluster {
  $ETOPDIR/etop-config/common/check-context.sh $CONTEXT --switch
}

export ETOPENV=$1 ; shift
case $ETOPENV in
  "dev")
    PROJECT=extopvn
    CONTEXT=dev
    ZONE=asia-southeast1-c
    CLUSTER=etop-dev
  ;;
  "sandbox")
    PROJECT=extopvn
    CONTEXT=dev
    ZONE=asia-southeast1-c
    CLUSTER=etop-dev
  ;;
  "stage")
    PROJECT=extopvn
    CONTEXT=dev
    ZONE=asia-southeast1-c
    CLUSTER=etop-dev
  ;;
  "v1prod")
    PROJECT=extopvn
    CONTEXT=v1prod
    ZONE=asia-southeast1-b
    CLUSTER=manh-prod
  ;;
  "faboprod")
    PROJECT=fabosho
    CONTEXT=faboprod
    ZONE=asia-southeast1-a
    CLUSTER=fabo-main
    EXEC_FILE=publish-fabo.sh
  ;;
esac

for SVC in "$@"
do
    CLIENT=$SVC
    DIR=$ETOPDIR/web

    case $SVC in
        admin)
            CLIENT=admin
            ;;
        etelecom)
            CLIENT=etelecom
            ;;
        etelecom-cs)
            CLIENT=etelecom-cs
            ;;
        shop)
            CLIENT=shop
            ;;
        next)
            CLIENT=shop
            ;;
        seller)
            CLIENT=affiliate
            ;;
        itopx)
            CLIENT=shop
            ;;
        whitelabel-etop)
            CLIENT=whitelabel
            ;;
        whitelabel-itopx)
            CLIENT=whitelabel
            ;;
        faboshop)
            CLIENT=faboshop
            ;;
        faboshop-next)
            CLIENT=faboshop
            ;;
        vnpost)
            CLIENT=movecrop
            ;;
        vnpost-next)
            CLIENT=movecrop
            ;;
        whitelabel-vnpost)
            CLIENT=whitelabel
            ;;
        whitelabel-vnpost-next)
            CLIENT=whitelabel
            ;;
        import)
            CLIENT=import
            ;;
        topship)
          CLIENT=topship
          ;;
        topship-next)
          CLIENT=topship
          ;;
        pos)
          CLIENT=shop
          ;;
        pos-next)
          CLIENT=shop
          ;;
        *)
            echo $USAGE
            exit 1
    esac

    cd $DIR
    node $DIR/scripts/deploy_notify.js prev $ETOPENV $SVC

    cd $DIR
    npm run $CLIENT:build:$ETOPENV
    cd $ETOPDIR

    switch-cluster
    $ETOPDIR/etop-config/frontend/$EXEC_FILE $ETOPENV $SVC

    cd $DIR
    node $DIR/scripts/deploy_notify.js done $ETOPENV $SVC
done
