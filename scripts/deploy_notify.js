const p = require('phin');
const simpleGit = require('simple-git');
const git = simpleGit();
const gitUtil = require('./git-util');

const groupIds = ['-430712504'];

function escape(str) {
  return str
}

async function preNotify() {
  let template = `
    💋💋💋️$$name chuẩn bị deploy ${process.argv[3]} ${process.argv[4]} @thangtran268 @quynguyen 💋💋💋
    Branch: $$branch
  `;
  const author = await git.raw(['config', '--get', 'user.name']);
  const branch = await git.raw(['branch', '--show-current']);

  const message = template.replace('$$name', author)
    .replace('$$branch', branch);
  await Promise.all(groupIds.map(groupID => {
    return p({
      method: 'GET',
      url: `https://api.telegram.org/bot1322183130:AAFiExtXTpPoSHHA3RtC83iUfe3LYLVVXYA/sendMessage?chat_id=${groupID}&text=${encodeURIComponent(message)}`,
      parse: 'json'
    }).then(res => res.body)
  }))
}

async function doneNotify () {
  let template = `
    💋💋💋️$$author Đã deploy ${process.argv[3]} ${process.argv[4]} @thangtran268 @quynguyen 💋💋💋
    Branch: $$branch
    Changes:
$$changes
    Changes List:
$$changeList
    Related Issue:
$$issues
Yêu @thangtran268 💋💋💋💋
  `

  let client = process.argv[4];
  switch (process.argv[4]) {
    case 'next':
      client = 'shop';
      break;
    case 'admin-next':
      client = 'admin';
      break;
  }

  const gitInfo = await gitUtil.getGitInfo(client);

  const message = gitUtil.parseTemplate(template, gitInfo);
  await Promise.all(groupIds.map(groupID => {
    return p({
      method: 'GET',
      url: `https://api.telegram.org/bot1322183130:AAFiExtXTpPoSHHA3RtC83iUfe3LYLVVXYA/sendMessage?chat_id=${groupID}&text=${encodeURIComponent(message)}`,
      parse: 'json'
    }).then(res => res.body)
  }))
}

switch (process.argv[2]) {
  case 'prev':
    preNotify().then().catch(e => {console.error(e); process.exit(1)});
    break;
  case 'done':
    doneNotify().then().catch(e => {console.error(e); process.exit(1)});
    break;
}


