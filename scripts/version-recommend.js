const conventionalRecommendedBump = require(`conventional-recommended-bump`);

conventionalRecommendedBump({
  preset: `angular`
}, (error, recommendation) => {
  if (recommendation.releaseType === 'major') {
    return console.log('minor');
  }
  console.log(recommendation.releaseType);
});
