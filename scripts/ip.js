const os = require('os');
const ifaces = os.networkInterfaces();

const ips = [];

Object.keys(ifaces).filter(ifname => /en.*/.test(ifname) || /eth.*/.test(ifname)).forEach(ifname => {
  ips.push(...ifaces[ifname].filter(iface => 'IPv4' === iface.family && iface.internal === false)
    .map(iface => iface.address));
});
console.log(ips.reduce((a, b) => a || b, false));
