#!/bin/sh
set -e

ENV=$1
CLIENT=$2
: ${ENV?"Missing env"}
: ${CLIENT?"Missing client"}

USAGE="Usage: call in the root directory of project \n
package.sh dev|sandbox|v1prod|stage supplier|shop|next|cmx|topship-app|admin..."

case $ENV in
  dev)
    ENV_PREFIX=d
    K8S_NAMESPACE=k8s-dev
    ;;
  sandbox)
    ENV_PREFIX=s
    K8S_NAMESPACE=k8s-sandbox
    ;;
  stag|stage|staging)
    ENV_PREFIX=g
    K8S_NAMESPACE=k8s-staging
    ;;
  v1prod|prod)
    ENV_PREFIX=p
    K8S_NAMESPACE=k8s-prod
    ;;
  *)
    echo $USAGE
    exit 1
esac

SVC=$CLIENT
IMG_NAME=frontend-$CLIENT

case $CLIENT in
  # etop
  admin|shop|cmx|import)
    SVC=$CLIENT
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/$CLIENT
    ;;
  shop-next)
    SVC=shop
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/shop
    ;;
  pos)
    SVC=shop
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/shop
    ;;
  pos-next)
    SVC=shop
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/shop
    ;;
  admin-next)
    SVC=admin
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/admin
    ;;
  seller)
    SVC=affiliate
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/affiliate
    ;;
  supply)
    SVC=supply
    IMG_NAME=frontend-etop-$CLIENT
    DIST=./dist/$ENV/supply
    ;;

  # topship
  topship)
    SVC=$CLIENT
    IMG_NAME=frontend-topship-shop
    DIST=./dist/$ENV/$CLIENT
    ;;
  topship-next)
    SVC=topship
    IMG_NAME=frontend-topship-shop-next
    DIST=./dist/$ENV/topship
    ;;

  # etelecom
  etelecom-cs)
    SVC=$CLIENT
    IMG_NAME=frontend-etelecom-cs
    DIST=./dist/$ENV/$CLIENT
    ;;

  # other
  import)
    SVC=$CLIENT
    IMG_NAME=frontend-other-$CLIENT
    DIST=./dist/$ENV/$CLIENT
    ;;

  core)
    # assume to deploy shop
    SVC=shop
    IMG_NAME=frontend-etop-shop
    DIST=./dist/$ENV/shop
    ;;
  *)
    echo $USAGE
    exit 1
esac

IMAGE_NAME=$ENV/$IMG_NAME
K8S_DEPLOY_NAME=${ENV_PREFIX}-$IMG_NAME

echo "Info :: " "svc - " $SVC "; IMAGE_NAME - " $IMAGE_NAME "; DIST - " $DIST "; K8S_DEPLOY_NAME - " $K8S_DEPLOY_NAME
