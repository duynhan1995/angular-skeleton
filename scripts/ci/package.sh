#!/bin/sh
set -ex

ENV=$1
: ${ENV?"Missing env"}
: ${CI_REGISTRY_IMAGE?"Missing CI_REGISTRY_IMAGE"}
: ${CI_PIPELINE_ID?"Missing CI_PIPELINE_ID"}

USAGE="Usage: call in the root directory of project \n
package.sh dev|sandbox|v1prod|stage supplier|shop|next|cmx|topship-app|admin..."

case $ENV in
  dev|sandbox|stage|v1prod)
    ;;
  *)
    echo $USAGE
    exit 1
esac

CLIENT=$2
if [ -z $CLIENT ]; then
  . ./scripts/ci/get-client.sh
elif [ -z $CLIENT ]; then
  CLIENT=shop
fi

# Get info: SVC, IMAGE_NAME, DIST
. ./scripts/ci/get-info.sh $ENV $CLIENT

# build source code
npm run $SVC:build:$ENV

# Build docker
DOCKER_IMAGE=$CI_REGISTRY_IMAGE/$IMAGE_NAME
docker build -t $DOCKER_IMAGE:$CI_PIPELINE_ID --build-arg DIST=${DIST} .
docker push $DOCKER_IMAGE:$CI_PIPELINE_ID

# make sure tag latest is the latest version
docker tag $DOCKER_IMAGE:$CI_PIPELINE_ID $DOCKER_IMAGE:latest
docker push $DOCKER_IMAGE:latest
