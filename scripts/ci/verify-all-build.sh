#!/bin/bash

set -ex

# Get scope
. ./scripts/ci/get-client.sh
echo "Check client: $CLIENT"

export NODE_OPTIONS=--max-old-space-size=8192

function build-ionic {
  # npm i
  cd apps/$1
  npm i
  cd ../..
  npm run nx build $1 -- --configuration=production
}

function build-web {
  npm run $1:build:v1prod
}

function ng-linter {
  npm run ng lint $1
}

case $CLIENT in
  "shop" | "pos")
    ng-linter shop
    build-web shop
  ;;
  "etelecom")
    ng-linter etelecom
    build-web etelecom
  ;;
  "etelecom-cs")
    ng-linter etelecom-cs
    build-web etelecom-cs
  ;;
  "topship")
    ng-linter topship
    build-web topship
  ;;
  "import")
    ng-linter import
    build-web import
  ;;
  "admin" | "admin-next")
    ng-linter admin
    build-web admin
  ;;
  "webcall")
    ng-linter webcall
    build-web webcall
  ;;
  "movecrop")
    ng-linter movecrop
    build-web movecrop
  ;;
  "ionic-etop")
    build-ionic ionic-etop
  ;;
  "supply")
    build-web supply
  ;;
  "affiliate")
    build-ionic affiliate
  ;;
  "core" | "shared")
    ng-linter shop
    build-web shop

    ng-linter etelecom-cs
    build-web etelecom-cs

    ng-linter import
    build-web import

    ng-linter admin
    build-web admin

    ng-linter topship
    build-web topship

#    build-ionic ionic-etop
#    build-ionic ionic-topship
  ;;
  "faboshop" | "ionic-faboshop")
    npm i
    ng-linter faboshop
    npm run faboshop:build:faboprod

    build-ionic ionic-faboshop
  ;;
  "ionic-topship")
    npm i

    build-ionic ionic-topship
  ;;
  *)
    echo "Scope not found: $CLIENT"
    exit 1
  ;;
esac
printf "\n✔ Done\n"
