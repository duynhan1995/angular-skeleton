#!/bin/bash

# commit message template: feat(client): ...
CLIENT=$(git log -1 | { egrep -o '\w+\((\w|-)+\).*' || true; } | { egrep -o '\(.+\)' || true; } | { egrep -o '[^\(\)]+' || true; } )
if [ -z $CLIENT ]; then
  CLIENT=shop
fi
echo "CLIENT = " $CLIENT
