#!/bin/sh
set -e

ENV=$1
: ${ENV?"Missing env"}
: ${CI_REGISTRY_IMAGE?"Missing CI_REGISTRY_IMAGE"}
: ${CI_PIPELINE_ID?"Missing CI_PIPELINE_ID"}

USAGE="Usage: publish.sh dev|sandbox|stage|v1prod supplier|shop|admin-next|cmx|topship-app|admin|whitelabel"

CLIENT=$2
if [ -z $CLIENT ]; then
  . ./scripts/ci/get-client.sh
elif [ -z $CLIENT ]; then
  CLIENT=shop
fi

. ./scripts/ci/get-info.sh $ENV $CLIENT

# Set default namespace
kubectl config set-context --current --namespace=${K8S_NAMESPACE}

# change image of initContainer in k8s deployment
DOCKER_IMAGE=$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_PIPELINE_ID
echo "Deploy to k8s $K8S_NAMESPACE using image $DOCKER_IMAGE"
kubectl set image deploy/$K8S_DEPLOY_NAME $K8S_DEPLOY_NAME=$DOCKER_IMAGE
# restart deployment
kubectl rollout status deploy/$K8S_DEPLOY_NAME

node scripts/deploy_notify.js done $ENV $CLIENT

printf "\n✓ Done\n"
