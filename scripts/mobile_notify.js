const p = require('phin');
const gitUtil = require('./git-util');

const groupIds = ['-430712504'];

async function doneNotify () {
  let template = `
$$version
    Branch: $$branch
    Changes:
$$changes
    Changes List:
$$changeList
    Related Issue:
$$issues
@thangtran268 @daonguyen176
  `
  const scope = process.argv[2];
  const version = process.argv.slice(2).join(" ");
  const gitInfo = await gitUtil.getGitInfo(scope);
  const message = gitUtil.parseTemplate(template, { ...gitInfo, version: version });

  await Promise.all(groupIds.map(groupID => {
    return p({
      method: 'GET',
      url: `https://api.telegram.org/bot1322183130:AAFiExtXTpPoSHHA3RtC83iUfe3LYLVVXYA/sendMessage?chat_id=${groupID}&text=${encodeURIComponent(message)}`,
      parse: 'json'
    }).then(res => res.body)
  }))
}

doneNotify().then()
