const fs = require('fs');
const path = require("path");

try {
  const angularConfigPath = path.join(process.cwd(), 'angular.json');
  const config = fs.readFileSync(angularConfigPath, 'UTF-8');
  if (config) {
    ngCli = JSON.parse(config);
    // update default
    ngCli.cli.defaultCollection = '@nrwl/angular';
    fs.writeFileSync(angularConfigPath, JSON.stringify(ngCli, null, 2));
  }
} catch (err) {
  console.warn("An issue was detected during installation: ", err);
}
