import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import 'bootstrap/dist/css/bootstrap.css';
import { EtopMaterialModule, MaterialInputComponent, MaterialInputAutocompleteComponent, MaterialInputFormatNumberComponent, MaterialNofilterInputAutocompleteComponent, MaterialTagInputComponent } from '@etop/shared';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
export default {
    title: 'Common Component',
    decorators: [
        moduleMetadata({
          // imports both components to allow component composition with storybook
          imports: [CommonModule, EtopMaterialModule, BrowserAnimationsModule],
        }),
      ],
}

export const MaterialInput = () => ({
    component:
        MaterialInputComponent,   
    
    template: `
    
    <div style="margin: 5px">
        <etop-material-input textLabel="Input Text" type="input">
            <span class="read-only" >Input Text</span>
        </etop-material-input>
    </div>
    <div style="margin: 5px">
        <etop-material-input textLabel="Input Custom" type="custom">
        </etop-material-input>
    </div>
    <div style="margin: 5px">
        <etop-material-input textLabel="Input Time" type="time">
            <span class="read-only" >Input Time</span>
        </etop-material-input>
    </div>
    <div style="margin: 5px">
        <etop-material-input textLabel="Input Datetime" type="datetime">
            <span class="read-only" >Input Datetime</span>
        </etop-material-input>
    </div>
    <div style="margin: 5px">
        <etop-material-input textLabel="Text Area" type="text-area">
            <span class="read-only" >Text Area</span>
        </etop-material-input>
    </div>
    
    <div style="margin: 5px">
        <etop-material-input textLabel="Select" type="select">
            <span class="read-only" >Select</span>
        </etop-material-input>
    </div>  
  `
  });

  export const AutoCompleteInput = () => ({
    component:
        MaterialInputAutocompleteComponent,   
    
    template: `
  <div style="margin: 5px">
        <etop-material-input-autocomplete
            placeholder="Autocomplete" textLabel="Autocomplete"
            [options]="provincesList$ | async" [displayMap]="displayMap" [valueMap]="valueMap"
            formControlName="selectedProvinceCode">
        </etop-material-input-autocomplete>
    </div>
    `
  });

  export const FormatNumberInput = () => ({
    component:
        MaterialInputFormatNumberComponent,   
    
    template: `
    <div style="margin: 5px">
        <etop-material-input-format-number
            [disabled]="false"
            textLabel="Format Number" placeholder="Format Number" formatNumberUnit="đ">
        </etop-material-input-format-number>
    </div>
    `
});

export const NofilterInput = () => ({
    component:
    MaterialNofilterInputAutocompleteComponent,   
    
    template: `
    <div style="margin: 5px">
        <etop-material-nofilter-input-autocomplete
            placeholder="Nofilter autocomplete" textLabel="Nofilter autocomplete"
            [options]="shops" [displayMap]="displayMap" [valueMap]="valueMap">
        </etop-material-nofilter-input-autocomplete>
    </div>
    `
});
export const TagInput = () => ({
    component:
        MaterialTagInputComponent,   
    
    template: `
    <div style="margin: 5px">
        <etop-material-tag-input
            label="Tag Input" placeholder="Tag Input">
        </etop-material-tag-input>
    </div>
    `
});
  MaterialInput.story = {
    name: `Input Type`,
  };
  