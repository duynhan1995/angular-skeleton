import { CommonLayout } from '../apps/core/src/app/CommonLayout';
import { Component } from '@angular/core';
import { moduleMetadata } from '@storybook/angular';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { AuthenticateStore } from '@etop/core';
import { CoreModule } from '../apps/core/src/core.module';
import { RouterModule } from '@angular/router';
import { MenuItem } from '../apps/core/src/components/menu-item/menu-item.interface';
import { EtopFilterModule } from '@etop/shared';

@Component({
  selector: 'app-layout',
  templateUrl: '../apps/core/src/app/common.layout.html',
})
class CommonLayoutComponent extends CommonLayout {
  sideMenus: MenuItem[] = [
    {
      name: 'Tổng quan',
      route: ['dashboard'],
      icon: 'assets/images/icon_dashboard.png',
      permissions: ['shop/dashboard:view']
    },
    {
      name: 'Sản phẩm',
      route: ['products'],
      icon: 'assets/images/icon_product.png',
      permissions: ['shop/product/basic_info:view']
    },
    {
      name: 'Hoá đơn',
      route: ['orders'],
      icon: 'assets/images/icon_order.png',
      display_submenus: false,
      submenus: [
        { name: 'Nhập hàng', route: ['purchase-orders', 'create'], hidden: true },
        { name: 'Nhập hàng', route: ['purchase-orders', 'update'], hidden: true },
      ],
      permissions: ['shop/order:view', 'shop/purchase_order:view']
    },
    {
      name: 'Phiếu thu/chi',
      route: ['receipts'],
      icon: 'assets/images/icon_receipt.png',
      permissions: ['shop/receipt:view']
    },
    {
      name: 'Đối tác',
      route: ['traders'],
      icon: 'assets/images/icon_trader.png',
      permissions: ['shop/customer:view', 'shop/supplier:view', 'shop/carrier:view']
    },
    {
      name: 'Quản lý tồn kho',
      route: ['inventory'],
      icon: 'assets/images/icon_inventory.png',
      display_submenus: false,
      submenus: [
        { name: 'Kiểm kho', route: ['balances', 'create'], hidden: true },
        { name: 'Kiểm kho', route: ['balances', 'update'], hidden: true },
        { name: 'Xuất huỷ', route: ['discards', 'create'], hidden: true },
        { name: 'Xuất huỷ', route: ['discards', 'create'], hidden: true }
      ],
      permissions: ['shop/inventory:view']
    },
    {
      name: 'Phiên chuyển tiền',
      route: ['money-transactions'],
      icon: 'assets/images/icon_transaction.png',
      permissions: ['shop/money_transaction:view']
    },
    {
      name: 'Thiết lập',
      route: ['settings'],
      icon: 'assets/images/icon_settings.png'
    },
    {
      name: 'Hỗ trợ',
      route: ['supports'],
      icon: 'assets/images/icon_support.png'
    },
    {
      name: 'Thông báo',
      route: ['notifications'],
      icon: 'assets/images/alarm_bell.png',
      hidden: true
    },
    {
      name: 'eB2B - Nguồn hàng & Dịch vụ',
      route: ['etop-trading'],
      icon: 'assets/images/icon_eb2b.png',
      hidden: false,
      icon_color: '#ff7300',
      name_custom: 'eB2B'
    },
    {
      name: 'eOrder - Nhập hàng Trung Quốc',
      route: ['eorder'],
      icon: 'assets/images/icon_eorder.png',
      icon_color: '#e74c3c',
      name_custom: 'eOrder'
    }
  ];
  constructor(private auth: AuthenticateStore) {
    super(auth);
  }
}

export default {
  title: 'Common Layout',
  decorators: [
    moduleMetadata({
      // imports both components to allow component composition with storybook
      declarations: [CommonLayoutComponent],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ],
      imports: [CommonModule, CoreModule, EtopFilterModule, RouterModule.forRoot([])],
    }),
  ],
}

export const CommonLayoutC = () => ({
  component: CommonLayoutComponent,
  template: `
    <app-layout></app-layout>
  `
});
