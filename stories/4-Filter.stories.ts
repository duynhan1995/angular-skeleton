import { FilterOptions, FilterComponent, EtopFilterModule, FilterOperator } from '@etop/shared';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
export default {
    title: 'Common Component',

    decorators: [
        moduleMetadata({
          // imports both components to allow component composition with storybook
          imports: [CommonModule, EtopFilterModule, BrowserAnimationsModule],
        }),
      ],
}
const filters : FilterOptions = [
  {
    label: 'Mã đơn hàng',
    name: 'code',
    type: 'input',
    fixed: true,
    operator: FilterOperator.eq
  },
  {
    label: 'Mã đơn giao hàng',
    name: 'shipping_code',
    type: 'input',
    fixed: true,
    operator: FilterOperator.contains
  },
  {
    label: 'Trạng thái đơn hàng',
    name: 'status',
    type: 'select',
    fixed: true,
    operator: FilterOperator.eq,
    options: [
      { name: 'Tất cả', value: '' },
      { name: 'Đặt hàng', value: 'Z' },
      { name: 'Đang xử lý', value: 'S' },
      { name: 'Trả hàng', value: 'NS' },
      { name: 'Hoàn thành', value: 'P' },
      { name: 'Hủy', value: 'N' }
    ]
  },
  {
    label: 'Tên người nhận',
    name: 'customer.name',
    type: 'input',
    operator: FilterOperator.contains
  },
  {
    label: 'Số điện thoại người nhận',
    name: 'customer.phone',
    type: 'input',
    operator: FilterOperator.eq
  },
]

export const ETopFilter = () => ({
    component: FilterComponent, 
    props: {
        filters: filters,
        
    },
    template: `
        <etop-filter [filters]="filters" ></etop-filter>
  `
  });

  ETopFilter.story = {
    name: `ETop Filter`,
  };
  