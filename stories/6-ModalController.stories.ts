import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { NotiModalComponent } from 'apps/shop/src/app/components/modals/noti-modal/noti-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from 'apps/core/src/core.module';


@Component({
  selector: 'etop-modal',
  template: `<div style="margin: 5px;" > 
  <button class="btn btn-primary" (click)="ShowModal()">Show modal</button>
</div>`,
})
class CommonModalComponent {
  constructor(private modalController: ModalController) {
  }

  ShowModal() {
    let modal = this.modalController.create({
      component: NotiModalComponent,
      showBackdrop: true,
      componentProps: {
        title: "Noti Modal",
        content: "Demo"
      }
    });
    modal.show();
  }
}

export default {
    title: 'Common Component',

    decorators: [
        moduleMetadata({
          declarations: [
            CommonModalComponent, 
            NotiModalComponent
          ],
          entryComponents: [
            CommonModalComponent, 
            NotiModalComponent
          ],
          // imports both components to allow component composition with storybook
          imports: [
            CommonModule, 
            CoreModule,
            BrowserAnimationsModule
          ],
        }),
      ],
}

export const CommonModal = () => ({
    template: `
        <etop-modal></etop-modal>
  `
  });

  
  