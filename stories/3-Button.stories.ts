export default {
    title: 'Common Component'
}

export const ButtonTheme = () => ({
    template: `
    <div style="display: flex; margin: 5px;">
        <div style="width: 200px; margin: 5px"><button class="btn btn-primary">Button primary</button></div>
        <div style="width: 200px; margin: 5px"><button class="btn btn-primary btn-outline">Button primary outline</button></div>
    </div>
    <div style="display: flex; margin: 5px;">
        <div style="width: 200px; margin: 5px"><button class="btn btn-default">Button default</button></div>
        <div style="width: 200px; margin: 5px"><button class="btn btn-default btn-outline">Button default outline</button></div>
    </div>
    <div style="display: flex; margin: 5px;">  
        <div style="width: 200px; margin: 5px"><button class="btn btn-warning">Button warning</button></div>
        <div style="width: 200px; margin: 5px"><button class="btn btn-warning btn-outline">Button warning outline</button></div>
    </div>
    `
})
