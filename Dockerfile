FROM nginx:1.21.0-alpine

ARG DIST

RUN mkdir /html
COPY ./ci/frontend-nginx.conf /etc/nginx/conf.d/frontend.conf
COPY $DIST /html

WORKDIR /html

EXPOSE 8080
