import { Component } from '@angular/core';

import { HeaderBaseComponent } from '@etop/features';

@Component({
  selector: 'etop-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent extends HeaderBaseComponent {}
