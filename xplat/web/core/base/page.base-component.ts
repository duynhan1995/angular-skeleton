import { BaseComponent } from '@etop/core';
import { Component, Directive, HostBinding } from '@angular/core';

@Component({
  selector: "page-base",
  template: '<div></div>'
})
export class PageBaseComponent extends BaseComponent{
  @HostBinding('class') clazz = 'page-container'
}
