import {BaseComponent} from '@etop/core';
import {Component, HostBinding} from '@angular/core';

@Component({
  selector: "mat-dialog-base",
  template: '<div></div>'
})
export class MatDialogBaseComponent extends BaseComponent {
  @HostBinding('class') clazz = 'matdialog-container'
}
