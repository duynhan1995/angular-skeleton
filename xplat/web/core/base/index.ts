// created from 'create-ts-index'

export * from './app.base-component';
export * from './component.base-component';
export * from './layout.base-component';
export * from './page.base-component';
export * from './mat-dialog.base-component';
