import { SystemEventService } from '../../../apps/ionic-faboshop/src/app/system-event.service';
import { ElementRef, Injectable } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { Platform } from '@ionic/angular';
import { isIOS } from '@etop/utils';
import { Plugins, KeyboardResize } from '@capacitor/core';

const { Network, Keyboard } = Plugins;

@Injectable()
export class PageBaseComponent extends BaseComponent {

  constructor(public systemEventService: SystemEventService, public el: ElementRef) {
    super();
    this.el.nativeElement.style.setProperty('transition', '0.3s');
    this.el.nativeElement.style.setProperty('position', 'fixed');
    
    if(isIOS()) {
      systemEventService.keyboardWillShow.pipe(takeUntil(this.destroy$)).subscribe((ev: any) => {
        let kb = getComputedStyle(document.documentElement).getPropertyValue('--sab');
        let ofset: any = Number(kb.replace('px', ''));
        let pb = ev.keyboardHeight;
        if (ofset > 20) {
          pb = ev.keyboardHeight - ofset;
        }
        this.el.nativeElement.style.setProperty('height', `${window.screen.height - pb}px`);
        this.el.nativeElement.style.setProperty('bottom', `${pb}px`);
      });
      
      systemEventService.keyboardWillHide.pipe(takeUntil(this.destroy$)).subscribe((ev: any) => {
        this.el.nativeElement.style.setProperty('height', `${window.screen.height}px`);
        this.el.nativeElement.style.setProperty('bottom', `0`);
      });
    }
  }

  ionViewWillEnter() {
    Keyboard.setResizeMode({ mode: KeyboardResize.Body });
  }

  ionViewWillLeave() {
    Keyboard.setResizeMode({ mode: KeyboardResize.Native });
  }
}
