import { NgModule, Optional, SkipSelf } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { throwIfAlreadyLoaded } from '@etop/utils';
import { EtopCoreModule } from '@etop/web';

@NgModule({
  imports: [EtopCoreModule, IonicModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ]
})
export class EtopIonicCoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: EtopIonicCoreModule
  ) {
    throwIfAlreadyLoaded(parentModule, 'EtopIonicCoreModule');
  }
}
