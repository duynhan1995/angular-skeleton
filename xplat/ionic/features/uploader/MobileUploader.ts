import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';

@Injectable({
  providedIn: "root"
})
export class MobileUploader {
  constructor(private http: HttpService) {
  }

  uploadBase64Image(base64Str: string, token?: string) {
    const headers = this.http.createHeader(token)
      .set('Content-Type', 'application/base64');
    let options = this.http.createDefaultOption(headers);
    return this.http.postWithOptions('upload', base64Str, options).toPromise();
  }
}
