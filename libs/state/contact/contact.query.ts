import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {Contact} from '@etop/models';
import {ContactStore, StateContact} from './contact.store';

@Injectable({
  providedIn: 'root'
})
export class ContactQuery extends QueryEntity<StateContact, Contact> {
  constructor(protected store: ContactStore) {
    super(store);
  }

  findContact(id) {
    let contacts = this.getAll();
    return contacts?.find(relationship => relationship.id === id)
  }
}
