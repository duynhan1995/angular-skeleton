import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { Contact } from '@etop/models';

export interface StateContact extends EntityState<Contact, string>, ActiveState {
}

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'contact', resettable: true })
export class ContactStore extends EntityStore<StateContact>{
  constructor() {
    super();
  }
}
