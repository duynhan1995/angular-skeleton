import {Injectable} from '@angular/core';
import {Contact} from '@etop/models';
import {ContactStore} from './contact.store';
import {ContactApi} from "@etop/api/shop/contact.api";

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  constructor(
    private contactApi: ContactApi,
    private contactStore: ContactStore,
  ) {
  }

  async getContacts(query?: any) {
    try {
      let res: any = await this.contactApi.getContacts(query);
      this.setStore(res?.contact);
    } catch (e) {
      debug.log('ERROR in getContacts');
      this.contactStore.set(null);
    }
  }

  setStore(contacts: Contact[]) {
    this.contactStore.upsertMany(contacts);
  }

}
