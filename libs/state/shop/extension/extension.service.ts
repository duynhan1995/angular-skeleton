import { Injectable } from '@angular/core';
import { ExtensionStore } from '@etop/state/shop/extension/extension.store';
import {Extension, Hotline, Relationship} from '@etop/models';
import {ExtensionAPI, ExtensionApi} from "@etop/api/shop/extension.api";
import {RelationshipQuery} from "@etop/state/relationship";
import {HotlineQuery} from "@etop/state/shop/hotline";

@Injectable({
  providedIn: 'root'
})
export class ExtensionService {
  constructor(
    private extensionApi: ExtensionApi,
    private extensionStore: ExtensionStore,
    private relationshipQuery: RelationshipQuery,
    private hotlineQuery: HotlineQuery,
  ) { }

  async getExtensions(hotlineId?: string, doMapping = false) {
    this.extensionStore.setLoading(true);
    try {
      const getExtensionFilter: ExtensionAPI.GetExtensionRequest = {
        filter: {hotline_id: hotlineId}
      };
      let extensions: Extension[] = await this.extensionApi.getExtensions(hotlineId ? getExtensionFilter : {});
      if (doMapping){
        const relationships = this.relationshipQuery.getAll();
        const hotlines = this.hotlineQuery.getAll();
        extensions = extensions.map(extension => Extension.extensionMap(extension, hotlines, relationships));
      }
      this.extensionStore.set(extensions);
    } catch(e) {
      debug.error('ERROR in getting Extensions', e);
    }
    this.extensionStore.setLoading(false);
  }

  async assignUserToExtension(extensionId, userId) {
    return await this.extensionApi.assignUserToExtension(extensionId, userId);
  }

  async removeUserOfExtension(extensionId, userId) {
    return await this.extensionApi.removeUserOfExtension(extensionId, userId);
  }

  async createExtension(extensionId, userId, extensionNumber?) {
    return await this.extensionApi.createExtension(extensionId, userId, Number(extensionNumber));
  }

  async createExtensionBySubscription(request: ExtensionAPI.CreateExtensionBySubscriptionRequest) {
    return await this.extensionApi.createExtensionBySubscription(request);
  }

  async extendExtension(request: ExtensionAPI.ExtendExtensionRequest) {
    return await this.extensionApi.extendExtension(request);
  }

  setActive(extentionId: string) {
    this.extensionStore.setActive(extentionId);
  }
}
