import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Extension } from '@etop/models';

export interface StateExtension extends EntityState<Extension, string>, ActiveState {
}

const initialState = {
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shopExtension', resettable: true })
export class ExtensionStore extends EntityStore<StateExtension> {
  constructor() {
    super(initialState);
  }
}
