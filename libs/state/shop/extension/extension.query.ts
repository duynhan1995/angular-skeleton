import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ExtensionStore, StateExtension } from '@etop/state/shop/extension/extension.store';
import { Extension } from '@etop/models';

@Injectable({
  providedIn: 'root'
})
export class ExtensionQuery extends QueryEntity<StateExtension, Extension> {
  constructor(protected store: ExtensionStore) {
    super(store);
  }

  getExtensionsByUserId(userId) {
    return  this.getAll({ filterBy:e => e.user_id === userId });

  }
  getExtensionsIdsByUserId(userId) {
    return this.getExtensionsByUserId(userId).map(extention => extention.id)
  }

  getAllUserHaveExtensions() {
    return this.getAll().map(extention => extention.user_id);
  }
}
