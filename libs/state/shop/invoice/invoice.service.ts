import {Injectable} from '@angular/core';
import {InvoiceStore} from './invoice.store';
import {InvoiceQuery} from './invoice.query';
import {InvoiceAPI, InvoiceApi} from '@etop/api/shop/invoice.api';
import {Invoice, InvoiceTransactionTab} from '@etop/models/Invoice';
import {CursorPaging, SubscriptionLine} from '@etop/models';
import { ProductSubscriptionType, SubscriptionAPI, SubscriptionApi } from '@etop/api/shop/subscription.api';
import { SubscriptionQuery, SubscriptionStore } from '../subscription';

@Injectable({
  providedIn: 'root',
})
export class InvoiceService {
  constructor(
    private invoiceApi: InvoiceApi,
    private invoiceStore: InvoiceStore,
    private invoiceQuery: InvoiceQuery,
    private subscriptionQuery: SubscriptionQuery,
    private subscriptionApi: SubscriptionApi,
    private subscriptionStore: SubscriptionStore,
  ) {
  }

  async getInvoices() {
    this.setLoading(true);
    try {
      const {filter, paging} = this.invoiceQuery.currentUi;
      await this.getSubscriptions(false);
      const subscriptionPlans = await this.getAllSubscriptionPlan();
      const res = await this.invoiceApi.getInvoices({filter, paging});
      res.invoices = res.invoices.map(invoice => {
        const subscriptionLines: Array<SubscriptionLine> = this.subscriptionQuery.getEntity(
                    invoice?.referral_ids[0])?.lines;
        return Invoice.invoiceMap(invoice, subscriptionPlans, subscriptionLines);
      })
      this.setPaging(res.paging);
      this.invoiceStore.set(res.invoices);
    } catch (e) {
      debug.error('ERROR in getting Invoices', e);
    }
    this.setLoading(false);
  }

  async getInvoice(id: string, setActive = false) {
    try {
      await this.getSubscriptions(false);
      const subscriptionPlans = await this.getAllSubscriptionPlan();
      const res = await this.invoiceApi.getInvoice(id);
      const subscriptionLines: Array<SubscriptionLine> = this.subscriptionQuery.getEntity(res?.referral_ids[0])?.lines;
      const invoice = Invoice.invoiceMap(res, subscriptionPlans, subscriptionLines);
      const invoices = this.invoiceQuery.getAll();
      if (setActive) {
        this.invoiceStore.set([invoice]);
      }
      return invoice;
    } catch (e) {
      debug.error('ERROR in getting Invoice', e);
    }
  }

  setFilter(filter: InvoiceAPI.GetInvoiceFilter) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        filter,
      },
    });
  }

  setPaging(paging: CursorPaging) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        paging,
      },
    });
  }

  setCurrentPage(currentPage: number) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        currentPage,
      },
    });
  }

  setInvoices(invoices: Invoice[]) {
    this.invoiceStore.set(invoices);
  }

  changeHeaderTab(tab: InvoiceTransactionTab) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        tab,
      },
    });
  }

  setLoading(loading: boolean) {
    this.invoiceStore.setLoading(loading);
  }

  updateInvoice(invoice: Invoice) {
    this.invoiceStore.upsert(invoice.id, invoice);
  }

  async getSubscriptions(doLoading = true){
    if(doLoading){
      this.subscriptionStore.setLoading();
    }
    try {
      const { filters } = this.subscriptionQuery.getValue().ui;
      const getSubscriptionsRequest: SubscriptionAPI.GetSubscriptionsRequest={
        filters,
      };
      let subscriptions = await this.subscriptionApi.getSubscriptions(getSubscriptionsRequest);
      subscriptions = subscriptions.filter(sub => {
        return sub.status == "P" && sub.lines.length > 0;
      })
      this.subscriptionStore.set(subscriptions);
      if ( doLoading ){
        this.subscriptionStore.setActive(null);
      }
    } catch(e) {
      debug.error('ERROR in getting Subscriptions', e);
    }
    if (doLoading) {
      this.subscriptionStore.setLoading(false);
    }
  }

  async getSubscriptionPlans(product_id: string) {
    return await this.subscriptionApi.getSubscriptionPlans(product_id);
  }

  async getSubscriptionProducts(type: ProductSubscriptionType) {
    return await this.subscriptionApi.getSubscriptionProducts(type);
  }

  async getAllSubscriptionPlan() {
    const subscriptionProducts = await this.getSubscriptionProducts(
      ProductSubscriptionType.EXTENSION);
    let subscriptionPlans = [];
    await Promise.all(subscriptionProducts.map( async subproduct => {
      const plan = await this.getSubscriptionPlans(subproduct.id);
      subscriptionPlans = subscriptionPlans.concat(plan);
    }));
    return subscriptionPlans;
  }

  setActive(id) {
    this.invoiceStore.setActive(id);
  }
}
