import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Hotline } from '@etop/models';

export interface StateHotline extends EntityState<Hotline, string>, ActiveState {
  hotlineRequested: boolean;
}

const initialState = {
  hotlineRequested: false
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shopHotline', resettable: true })
export class HotlineStore extends EntityStore<StateHotline> {
  constructor() {
    super(initialState);
  }
}
