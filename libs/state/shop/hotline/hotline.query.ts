import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Hotline } from '@etop/models';
import { HotlineStore, StateHotline } from '@etop/state/shop/hotline/hotline.store';

@Injectable({
  providedIn: 'root'
})
export class HotlineQuery extends QueryEntity<StateHotline, Hotline> {
  constructor(protected store: HotlineStore) {
    super(store);
  }
}
