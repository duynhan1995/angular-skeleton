import {Hotline, Shop, User} from '@etop/models';
import { HotlineStore } from '@etop/state/shop/hotline/hotline.store';
import { Injectable } from '@angular/core';
import {HotlineApi} from "@etop/api/shop/hotline.api";
import {TelegramService} from "@etop/features";
import {JiraService} from "@etop/state/shop/jira";
import {AuthenticateStore} from "@etop/core";

@Injectable({
  providedIn: 'root'
})
export class HotlineService {
  constructor(
    private hotlineApi: HotlineApi,
    private hotlineStore: HotlineStore,
    private telegramService: TelegramService,
    private jiraService: JiraService,
    private auth: AuthenticateStore
  ) { }

  async getHotlines() {
    this.hotlineStore.setLoading(true);
    try {
      const hotlines: Hotline[] = await this.hotlineApi.getHotlines();
      this.hotlineStore.set(hotlines);
    } catch(e) {
      debug.error('ERROR in getting Hotlines', e);
    }
    this.hotlineStore.setLoading(false);
  }

  checkHotLineRequested() {
    const oneDay = 24 * 60 * 60 * 1000;
    const lastRequest = localStorage.getItem('hotlineRequest');
    if(!lastRequest) {
      this.hotlineStore.update({hotlineRequested: false});
    } else {
      const checkRequested = parseFloat(Date.now().toString()) - parseFloat(lastRequest) < oneDay;
      this.hotlineStore.update({hotlineRequested: checkRequested});
    }
  }

  hotlineRequest() {
    this.hotlineStore.update({hotlineRequested: true});
    this.telegramService.userHotlineRequest(this.auth.snapshot.user, this.auth.snapshot.shop).then();
    this.jiraService.createJiraIssueUserHotlineRequest(this.auth.snapshot.user, this.auth.snapshot.shop).then();
    localStorage.setItem('hotlineRequest', Date.now().toString());
    toastr.success(`Hệ thống đã tiếp nhận yêu cầu mua Hotline của bạn. Chúng tôi sẽ xử lý và liên hệ với bạn trong thời gian sớm nhất.`);
  }
}
