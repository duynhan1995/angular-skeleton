import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Ticket } from '@etop/models';
import { StateTicket, TicketStore } from '@etop/state/shop/ticket/ticket.store';

@Injectable({
  providedIn: 'root'
})
export class TicketQuery extends QueryEntity<StateTicket, Ticket> {

  constructor(protected store: TicketStore) {
    super(store);
  }

  get Ui() {
    return this.getValue().ui;
  }

  get activeFulfillment() {
    return this.getValue().activeFulfillment;
  }

  getTicketById(id) {
    let tickets = this.getAll();
    return tickets?.find(ticket => ticket.id === id)
  }
}
