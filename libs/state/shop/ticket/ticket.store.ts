import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import {ActionType, Fulfillment, Ticket, TicketTab} from '@etop/models';
import { Paging } from '@etop/shared';
import { ShopTicketAPI } from '@etop/api';

export interface StateTicket extends EntityState<Ticket, string>, ActiveState {
  ui: {
    isLastPage: boolean;
    finishPrepareData: boolean,
    getTicketCommentsLoading: boolean,
    paging: Paging,
    filter: ShopTicketAPI.GetTicketsFilter,
    filterProductLabelsIds: string[];
    filterSubjectLabelsIds: string[];
    filterDetailLabelsIds: string[];
    tab: TicketTab;
  };
  actionType: ActionType,
  activeFulfillment: Fulfillment,
  ticketLabels: Array<Ticket>,
}

const initialState = {
  ui: {
    isLastPage: false,
    finishPrepareData: false,
    getTicketCommentsLoading: false,
    paging: null,

    filter: {},
    filterProductLabelsIds: [],
    filterSubjectLabelsIds: [],
    filterDetailLabelsIds: [],
    tab: TicketTab.general,
  },
  actionType: null,
  activeFulfillment: null,
  ticketLabels:[],
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shopTicket', resettable: true })
export class TicketStore extends EntityStore<StateTicket> {
  constructor() {
    super(initialState);
  }
}
