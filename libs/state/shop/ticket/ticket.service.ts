import {Injectable} from '@angular/core';
import {TicketQuery} from '@etop/state/shop/ticket/ticket.query';
import {TicketStore} from '@etop/state/shop/ticket/ticket.store';
import {AuthorizationApi, ContactAPI, ContactApi, ShopTicketAPI, ShopTicketApi} from '@etop/api';
import {trimUndefined} from '@etop/utils';
import {AddressDisplayPipe, Paging} from '@etop/shared';
import {
  ActionType, CallLogTab,
  Filters,
  Fulfillment,
  Ticket,
  TicketComment, TicketLabel,
  TicketRefType,
  TicketSource,
  TicketState, TicketTab,
  TicketType
} from '@etop/models';
import {Subject} from 'rxjs';
import {AuthenticateStore} from '@etop/core';
import {arrayUpsert} from '@datorama/akita';
import {UtilService} from 'apps/core/src/services/util.service';
import {RelationshipQuery, RelationshipStore} from '@etop/state/relationship';
import {TicketLabelQuery, TicketLabelStore} from '@etop/state/shop/ticket-labels';
import {ContactQuery} from "@etop/state/shop/contact/contact.query";
import {ContactStore} from "@etop/state/shop/contact/contact.store";

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  readonly changeActionType$ = new Subject();

  constructor(
    private ticketQuery: TicketQuery,
    private ticketStore: TicketStore,
    private shopTicketApi: ShopTicketApi,
    private addressDisplay: AddressDisplayPipe,
    private auth: AuthenticateStore,
    private util: UtilService,
    private relationshipsQuery: RelationshipQuery,
    private contactQuery: ContactQuery,
    private labelQuery: TicketLabelQuery,
    private labelStore: TicketLabelStore,
    private contactApi: ContactApi,
    private contactStore: ContactStore,
    private relationshipStore: RelationshipStore,
    private relationshipApi: AuthorizationApi
  ) {
  }

  private reMapTicket(ticket: Ticket): Ticket {
    const relationships = this.relationshipsQuery.getAll();

    let _labels: TicketLabel[] = [];
    let labelType = this.labelQuery.getValue().ui.filter.type;

    if (labelType == TicketType.system) {
      const {ticketSubjectLabels} = this.labelQuery.getValue();
      _labels = ticket.label_ids.map(id => ticketSubjectLabels.find(label => label.id == id))
        .filter(label => !!label);
      if (_labels?.length) {
        _labels = _labels.map(label => ({...label, children: []}));
      }
    }

    if (labelType == TicketType.internal) {
      const ticketLabels = this.labelQuery.getAll();
      _labels = ticketLabels.filter(label => ticket.label_ids.includes(label.id));
    }

    if (ticket.ref_type == TicketRefType.contact) {
      ticket.contact = this.contactQuery.findContactById(ticket?.ref_id);
    }
    return {
      ...ticket,
      assignedUsers: relationships.filter(rel => ticket.assigned_user_ids.includes(rel.user_id)),
      closedByUser: relationships.find(rel => rel.user_id == ticket.closed_by),
      confirmedByUser: relationships.find(rel => rel.user_id == ticket.confirmed_by),
      createdByUser: ticket.createdByUser || relationships.find(rel => rel.user_id == ticket.created_by),
      labels: [..._labels]
    };
  }

  setFilter(filter: ShopTicketAPI.GetTicketsFilter) {
    const ui = this.ticketQuery.getValue().ui;
    const _filter = {
      ...ui.filter,
      ...filter
    };
    this.ticketStore.update({
      ui: {
        ...ui,
        filter: trimUndefined(_filter)
      }
    });
  }


  setPaging(paging: Paging) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }


  setFilterProductLabels(labelIds: string[]) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        filterProductLabelsIds: labelIds
      }
    });
    this.setFilter({label_ids: labelIds});

    const subjectLabels = this.labelQuery.getValue().ticketSubjectLabels;
    this.labelStore.update({ticketSubjectLabels: subjectLabels});
  }


  setFilterSubjectLabels(labelIds: string[]) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        filterSubjectLabelsIds: labelIds
      }
    });
    if (!labelIds.length) {
      this.setFilter({label_ids: ui.filterProductLabelsIds});
    } else {
      this.setFilter({label_ids: labelIds});
    }

    const detailLabels = this.labelQuery.getValue().ticketDetailLabels;
    this.labelStore.update({ticketDetailLabels: detailLabels});
  }


  setFilterDetailLabels(labelIds: string[]) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        filterDetailLabelsIds: labelIds
      }
    });
    if (!labelIds.length) {
      return this.setFilter({label_ids: ui.filterSubjectLabelsIds});
    }
    this.setFilter({label_ids: labelIds});
  }

  setFilters(filters: Filters) {
    this.setFilter({
      code: null,
      state: null,
      label_ids: null
    });

    const filter: ShopTicketAPI.GetTicketsFilter = {};
    filters.forEach(f => {
      if (f.name == 'assigned_user_id' && f.value) {
        filter.assigned_user_id = [f.value];
      }
      if (f.name == 'code') {
        filter.code = f.value;
      }
      if (f.name == 'state') {
        filter.state = f.value;
      }
      if (f.name == 'label_ids') {
        filter.label_ids = [f.value];
      }
    });

    this.setFilter(filter);
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  async getTicketLabels(data: ShopTicketAPI.GetTicketLabels) {
    const ticket_labels = await this.shopTicketApi.getTicketLabels(data);
    this.ticketStore.update({ticketLabels: ticket_labels})
  }

  async createTicket(ticketInfo, createTicketForm?, typeTicket?: TicketType) {
    let labels_ids = []
    const actionType = this.ticketStore.getValue().actionType;
    const activeFfm = this.ticketQuery.getValue().activeFulfillment;
    const ticketLabels = this.ticketStore.getValue().ticketLabels

    if (actionType == 'ffm_update_info') {
      if (activeFfm.shipping_address.full_name != createTicketForm?.shipping_address.fullname) {
        const updatePhoneLabelId = ticketLabels.find(label => label.code == ActionType.ffm_update_phone).id;
        labels_ids.push(updatePhoneLabelId);
      }
      if (activeFfm.shipping_address.phone != createTicketForm?.shipping_address.phone) {
        const updateNameLabelId = ticketLabels.find(label => label.code == ActionType.ffm_update_name).id;
        labels_ids.push(updateNameLabelId);
      }
      if (this.addressDisplay.transform(activeFfm.shipping_address) != createTicketForm?.shipping_address.address) {
        const updateAddressLabelId = ticketLabels.find(label => label.code == ActionType.ffm_update_address)?.id;
        labels_ids.push(updateAddressLabelId);
      }
    } else {
      const ticketLabelId = ticketLabels.find(label => label.code == actionType)?.id;
      labels_ids = [ticketLabelId]
    }
    const createTicketRequest: ShopTicketAPI.CreateTicketRequest = {
      source: ticketInfo.source || TicketSource.pos_web,
      title: ticketInfo.title,
      ref_type: ticketInfo.ref_type || TicketRefType.ffm,
      ref_id: activeFfm.id,
      description: ticketInfo.description,
      label_ids: labels_ids,
      type: typeTicket
    }
    await this.shopTicketApi.createTicket(createTicketRequest);
    await this.getTickets(false);
  }

  setActiveFulfillment(activeFulfillment: Fulfillment) {
    this.ticketStore.update({activeFulfillment})
  }

  setTicketAction(actionType: ActionType) {
    this.changeActionType$.next();
    this.ticketStore.update({actionType})
  }

  validateAction(type: ActionType, ffm: Fulfillment): boolean {
    let isActiveAction = false;
    if (type == ActionType.ffm_push_pickup) {
      isActiveAction = ffm.shipping_state == 'default' || ffm.shipping_state == 'created' || ffm.shipping_state == 'picking';
    }

    if (type == ActionType.ffm_push_delivery) {
      isActiveAction = ffm.shipping_state == 'holding' || ffm.shipping_state == 'delivering';
    }

    if (type == ActionType.ffm_update_cod || type == ActionType.ffm_update_info) {
      isActiveAction = ffm.shipping_state == 'default' || ffm.shipping_state == 'created' || ffm.shipping_state == 'holding' || ffm.shipping_state == 'picking' || ffm.shipping_state == 'delivering';
    }

    if (type == ActionType.ffm_other) {
      isActiveAction = ffm.shipping_state == 'default' || ffm.shipping_state == 'created' || ffm.shipping_state == 'holding' || ffm.shipping_state == 'picking' || ffm.shipping_state == 'delivering' || ffm.shipping_state == 'delivered' || ffm.shipping_state == 'returning' || ffm.shipping_state == 'returned' || ffm.shipping_state == 'undeliverable';
    }
    return isActiveAction
  }

  changeActionType() {
    this.changeActionType$.next()
  }

  async getTicketComments(ticket: Ticket) {
    try {
      const bodyRequest: ShopTicketAPI.GetTicketCommentsRequest = {
        paging: {offset: 0, limit: 1000},
        ticket_id: ticket.id
      };
      let comments: TicketComment[] = await this.shopTicketApi.getTicketComments(bodyRequest);
      this.ticketStore.upsert(ticket.id, {
        comments: comments.reverse().map(cmt => {
          const currentUserID = this.auth.snapshot.user.id;
          const ownComment = currentUserID == cmt.created_by;

          let createdByDisplay = this.relationshipsQuery.getRelationshipNameById(cmt.created_by);
          if (!ownComment) {
            createdByDisplay = `ETELECOM`
          }
          return {
            ...cmt,
            pID: cmt?.id,
            ownComment,
            createdByDisplay
          };
        })
      });
    } catch (e) {
      debug.error('ERROR in getTicketComments', e);
    }
  }

  async createTicketComment(message: string) {
    try {
      let body: ShopTicketAPI.CreateTicketCommentRequest = {
        message: message,
        ticket_id: this.ticketQuery.getActive()?.id,
      };
      let comment: TicketComment = await this.shopTicketApi.createTicketComment(body);
      const currentUserID = this.auth.snapshot.user.id;
      const ownComment = currentUserID == comment.created_by;

      let createdByDisplay: string = this.auth.snapshot.user.full_name;
      if (!ownComment) {
        createdByDisplay = `ETELECOM`
      }

      comment = {
        ...comment,
        pID: comment?.id,
        ownComment,
        createdByDisplay
      }
      setTimeout(_ => {
        // this.updateTicketCommentStatus(comment.id, null);
        comment.pID = comment.id;
        this.mergeTicketComments([comment]);
      }, 250);
    } catch (e) {
      debug.error('ERROR in createTicketComment', e);
      throw e;
    }
  }

  async createTicketCommentHaveImg(comment: TicketComment) {
    try {
      const currentUserID = this.auth.snapshot.user.id;
      const ownComment = currentUserID == comment.created_by;
      let createdByDisplay: string = this.auth.snapshot.user.full_name;

      comment = {
        ...comment,
        pID: comment?.id,
        ownComment,
        createdByDisplay
      }
      this.mergeTicketComments([comment]);
      this.updateTicketCommentStatus(comment.id, "sending");

      let body: ShopTicketAPI.CreateTicketCommentRequest = {
        ticket_id: this.ticketQuery.getActive()?.id,
      };
      if (comment?.image_url) {
        const currentFilesHash = this.ticketQuery.getValue().ticketCommentPreUploadImagesFiles;
        const file = currentFilesHash[comment?.id];
        const _res = await this.util.uploadImages([file], 1024);
        body.image_url = _res[0]?.url;
      } else {
        body.message = comment?.message;
      }
      const res = await this.shopTicketApi.createTicketComment(body);

      setTimeout(_ => {
        this.updateTicketCommentStatus(comment.id, null);

        comment.pID = res.id;
        this.mergeTicketComments([comment]);
      }, 250);
    } catch (e) {
      debug.error('ERROR in createTicketComment', e);
      this.updateTicketCommentStatus(comment.id, "error");
      throw e;
    }
  }

  updateTicketCommentStatus(commentID: string, status: 'sending' | 'error') {
    let comment = this.ticketQuery.getActive().comments.find(cmt => cmt.id == commentID)
    comment = {
      ...comment,
      sendingStatus: status
    };
    this.mergeTicketComments([comment]);
  }

  updateTicketCommentPreUploadImagesFiles(key: string, file: File) {
    const currentFilesHash = this.ticketQuery.getValue().ticketCommentPreUploadImagesFiles;
    this.ticketStore.update({
      ticketCommentPreUploadImagesFiles: {
        ...currentFilesHash,
        [key]: file
      }
    });
  }

  mergeTicketComments(comments: TicketComment[]) {
    let _comments = JSON.parse(JSON.stringify(this.ticketQuery.getActive().comments));
    comments.forEach(cmt => _comments = arrayUpsert(_comments, cmt.id, cmt));
    this.ticketStore.updateActive({comments: _comments});
  }

  getActionIcon(actionType: ActionType) {
    let icon = '';
    switch (actionType) {
      case 'ffm_update_cod':
        icon = 'cash-outline';
        break;
      case 'ffm_other':
        icon = 'menu-outline';
        break;
      case 'ffm_push_pickup' || 'ffm_push_delivery':
        icon = 'flash-outline';
        break;
      case 'ffm_update_info':
        icon = 'person-circle-outline';
        break;
    }
    return icon;
  }

  async prepareData() {
    await this.getContacts();
    await this.getRelationships();
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        finishPrepareData: true
      }
    });
  }

  async getContacts() {
    try {
      const contacts = await this.contactApi.getContacts({});
      this.contactStore.set(contacts.contacts);
    } catch (e) {
      debug.error('ERROR in getting Contacts', e);
    }
  }

  async getContactsByPhone(phone: string) {
    try {
      const res = await this.contactApi.getContacts({filter: {phone}});
      return res.contacts;
    } catch (e) {
      debug.error('ERROR in getting Contacts By Phone', e);
      return [];
    }
  }

  async getRelationships() {
    try {
      const relationships = await this.relationshipApi.getAccountUsers({});
      this.relationshipStore.set(relationships['account_users'].map(relationship => {
        return {
          ...relationship,
          id: relationship?.user_id
        }
      }));
    } catch (e) {
      debug.error('ERROR in getting Relationships', e);
    }
  }

  async getTickets(doLoading = true) {
    this.setLastPage(false);
    if (doLoading) {
      this.ticketStore.setLoading(true);
    }
    try {
      const {filter, paging} = this.ticketQuery.getValue().ui;
      let tickets: Ticket[] = await this.shopTicketApi.getTickets({filter, paging});

      if (tickets.length == 0) {
        if (paging.offset > 0) {
          this.setLastPage(true);
          this.setPaging({
            ...paging,
            offset: paging.offset - paging.limit
          });
        } else if (paging.offset == 0 && (Object.keys(filter)?.length)) {
          this.ticketStore.set([]);
        }
      } else {
        this.ticketStore.set(tickets.map(ticket => this.reMapTicket(ticket)));
        if (doLoading) {
          this.ticketStore.setActive(null);
        }
      }
      const ui = this.ticketQuery.getValue().ui;
      this.ticketStore.update({
        ui: {
          ...ui,
          finishPrepareData: false
        }
      });
    } catch (e) {
      debug.error('ERROR in getting Tickets', e);
    }
    if (doLoading) {
      this.ticketStore.setLoading(false);
    }
  }

  async getTicket(id) {
    let ticket = await this.shopTicketApi.getTicket(id);
    return this.reMapTicket(ticket);
  }

  async selectTicket(ticket: Ticket) {
    const activeTicket = this.ticketQuery.getActive();
    if (ticket?.id == activeTicket?.id) {
      return;
    }
    if (ticket) {
      await this.getTicketComments(ticket);
    }
    this.ticketStore.setActive(ticket ? ticket.id : null);
  }

  async createEtelecomAdminTicket(ticketInfo, type?: TicketType) {
    const createTicketRequest: ShopTicketAPI.CreateTicketRequest = {
      source: ticketInfo.source || 'pos_web',
      title: ticketInfo.title,
      ref_type: ticketInfo.ref_type || null,
      ref_id: ticketInfo.ref_id || null,
      description: ticketInfo.description,
      label_ids: ticketInfo.label_ids,
      type: ticketInfo.type,
    }
    await this.shopTicketApi.createTicket(createTicketRequest);
    await this.getTickets(false);
  }

  changeHeaderTab(tab: TicketTab) {
    const ui = this.ticketQuery.Ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        tab
      }
    })
  }

  async assignTicket(assigned_user_ids: string[], ticket_id: string) {
    return this.shopTicketApi.assignTicket({assigned_user_ids, ticket_id})
  }

  async reopenTicket(note: string, ticket_id: string) {
    return this.shopTicketApi.reopenTicket({note, ticket_id})
  }

  async confirmTicket(note: string, ticket_id: string) {
    return this.shopTicketApi.confirmTicket({note, ticket_id})
  }

  async closeTicket(state: TicketState, ticket_id: string) {
    return this.shopTicketApi.closeTicket({state, ticket_id})
  }

  updateAssignTicket(ticket: Ticket) {
    const relationships = this.relationshipsQuery.getAll();
    this.ticketStore.update(ticket.id, {
      assignedUsers: relationships.filter(rel => ticket.assigned_user_ids.includes(rel.user_id))
    })
  }

  updateStateTicket(ticket: Ticket) {
    this.ticketStore.update(ticket.id, {
      state_display: Ticket.ticketStateMap(ticket.state),
      state: ticket.state
    })
  }

  updateCloseTicket(ticket: Ticket) {
    const relationships = this.relationshipsQuery.getAll();
    this.ticketStore.update(ticket.id, {
      closedByUser: relationships.find(rel => rel.user_id == ticket.closed_by)
    })
  }
}
