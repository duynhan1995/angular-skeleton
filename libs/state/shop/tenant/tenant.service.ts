import {Injectable} from '@angular/core';
import { TenantApi } from '@etop/api/shop/tenant.api';

@Injectable({
    providedIn: 'root'
})

export class TenantService {
    constructor(
      private tenantApi: TenantApi,
    ) {}

    async getTenant() {
        try {
            return  await this.tenantApi.getTenant();
        } catch (e) {
            return false;
        }
    }

    async createTenant() {
        return await this.tenantApi.createTenant();
    }
}  