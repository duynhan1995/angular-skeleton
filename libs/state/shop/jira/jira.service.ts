import {Injectable} from '@angular/core';
import {JiraQuery} from "./jira.query";
import {JiraStore} from "./jira.store";
import {HttpService} from "@etop/common";
import {JiraAPI, JiraApi} from "@etop/api/shop/jira.api";
import {ExtendedAccount, Shop, User} from "@etop/models";
import {ConfigService} from "@etop/core";

@Injectable({
  providedIn: "root"
})
export class JiraService {

  constructor(
    private http: HttpService,
    private jiraApi: JiraApi,
    private jiraStore: JiraStore,
    private jiraQuery: JiraQuery,
    private configService: ConfigService,
  ) {}

  async createJiraIssue(shopName: string, user: User) {
    let customFields = this.jiraQuery.getValue().customFields;
    customFields.forEach(custom_field => {
      switch (custom_field.key){
        case JiraCustomFieldKey.env:
          custom_field.value = this.configService.getConfig().production ? 'production' : 'dev';
          break;
        case JiraCustomFieldKey.domain:
          custom_field.value = window.location.hostname;
          break;
        case JiraCustomFieldKey.email:
          custom_field.value = user.email;
          break;
        case JiraCustomFieldKey.representator:
          custom_field.value = user.full_name;
          break;
        case JiraCustomFieldKey.app_bundle:
          custom_field.value = null;
          break;
        case JiraCustomFieldKey.product:
          custom_field.value = 'eTelecom CS';
          break;
        case JiraCustomFieldKey.sales_phone:
          custom_field.value = localStorage.getItem('e_aff');
          break;
        case JiraCustomFieldKey.customer_phone:
          custom_field.value = user.phone;
          break;
        case JiraCustomFieldKey.platform:
          custom_field.value = 'web';
          break;
      }
    })
    const data: JiraAPI.CreateJiraIssueRequest = {
      summary: shopName,
      description: null,
      custom_fields: customFields,
    }
    return this.jiraApi.createJiraIssue(data);
  }

  async createJiraIssueUserHotlineRequest(user: User, shop: ExtendedAccount) {
    let customFields = this.jiraQuery.getValue().customFields;
    customFields.forEach(custom_field => {
      switch (custom_field.key){
        case JiraCustomFieldKey.env:
          custom_field.value = this.configService.getConfig().production ? 'production' : 'dev';
          break;
        case JiraCustomFieldKey.domain:
          custom_field.value = window.location.hostname;
          break;
        case JiraCustomFieldKey.email:
          custom_field.value = null;
          break;
        case JiraCustomFieldKey.representator:
          custom_field.value = null;
          break;
        case JiraCustomFieldKey.app_bundle:
          custom_field.value = null;
          break;
        case JiraCustomFieldKey.product:
          custom_field.value = 'eTelecom CS';
          break;
        case JiraCustomFieldKey.sales_phone:
          custom_field.value = null;
          break;
        case JiraCustomFieldKey.customer_phone:
          custom_field.value = null;
          break;
        case JiraCustomFieldKey.platform:
          custom_field.value = 'web';
          break;
      }
    });
    const summary = `[Mua Hotline] - ${shop.name} - ${user.full_name} ${user.phone}`;
    const description = `
LIÊN HỆ:

User
${user.full_name} - ${user.phone}
${user.email}

Shop
${shop.code} - ${shop.name}
`
    const data: JiraAPI.CreateJiraIssueRequest = {
      summary,
      description,
      custom_fields: customFields,
    }
    return this.jiraApi.createJiraIssue(data);
  }

  async initCustomFields(){
    let customFields: JiraAPI.JiraCustomField[] = await this.jiraApi.getJiraCustomFields();
    customFields = customFields.map(custom_field => this.mapCustomFiled(custom_field));
    this.jiraStore.update({customFields: customFields});
  }

  mapCustomFiled(field) {
    return {
      key: field.key,
      value: field.name
    }
  }

}

export enum JiraCustomFieldKey{
  env = 'env',
  domain = 'domain',
  email = 'email',
  representator = 'representator',
  app_bundle = 'app_bundle',
  product = 'product',
  sales_phone = 'sales_phone',
  customer_phone = 'customer_phone',
  platform = 'platform'
}
