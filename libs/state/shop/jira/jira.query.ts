import { Injectable } from '@angular/core';
import {Query} from '@datorama/akita';
import {JiraStore, StateJira} from "./jira.store";

@Injectable({
  providedIn: 'root'
})
export class JiraQuery extends Query<StateJira> {

  constructor(protected store: JiraStore) {
    super(store);
  }

}
