import { Injectable } from '@angular/core';
import {Store, StoreConfig} from "@datorama/akita";
import {JiraAPI} from "@etop/api/shop/jira.api";

export interface StateJira {
  customFields: JiraAPI.JiraCustomField[]
}

const initialState = {
  customFields: [],
};

@Injectable({
  providedIn: "root"
})
@StoreConfig({name: 'jira'})
export class JiraStore extends Store<StateJira> {
  constructor() {
    super(initialState);
  }
}
