import { Injectable } from '@angular/core';
import { CallLog, CallLogTab, Contact, CursorPaging, Extension, Hotline, Relationship } from '@etop/models';
import { CallLogStore } from '@etop/state/shop/call-log/call-log.store';
import { CallLogQuery } from '@etop/state/shop/call-log/call-log.query';
import { trimUndefined } from '@etop/utils';
import { ExtensionQuery } from '@etop/state/shop/extension';
import { HotlineQuery, HotlineService } from '@etop/state/shop/hotline';
import {ContactQuery, ContactService} from '@etop/state/shop/contact';
import { RelationshipQuery } from '@etop/state/relationship';
import {CallLogAPI, CallLogApi} from "@etop/api/shop/call-log.api";
import {ContactAPI, ContactApi} from "@etop/api/shop/contact.api";

@Injectable({
  providedIn: 'root'
})
export class CallLogService {
  constructor(
    private callLogApi: CallLogApi,
    private callLogStore: CallLogStore,
    private callLogQuery: CallLogQuery,
    private extensionQuery: ExtensionQuery,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private hotlineQuery: HotlineQuery,
    private relationshipQuery: RelationshipQuery
  ) { }

  async getCallLogs(doMapping = false) {
    this.callLogStore.setLoading(true);
    try {
      const { filter,paging } = this.callLogQuery.Ui;

      const res = await this.callLogApi.getCallLogs({filter, paging});
      let callLogs: CallLog[] = res.call_logs;
      let contactIds = callLogs.map(calllog => calllog.contact_id);

      if(doMapping) {
        let getContactsFilter: ContactAPI.GetContactsRequest = {
          filter: {ids: contactIds}
        };
        await this.getContactsByCallLogs(getContactsFilter);
        const relationships = this.relationshipQuery.getAll();
        const extensions = this.extensionQuery.getAll();
        const contacts = this.contactQuery.getAll();
        const hotlines = this.hotlineQuery.getAll();
        callLogs = res.call_logs
          .map(callLog => this.callLogMap(callLog,relationships, extensions, hotlines, contacts));
        this.contactService.setFilter({});
      }

      this.setPaging(res.paging);
      this.callLogStore.set(callLogs)

    } catch(e) {
      debug.error('ERROR in getting CallLogs', e);
    }
    this.callLogStore.setLoading(false);
  }

  async getContactsByCallLogs(contactsFilter: ContactAPI.GetContactsRequest){
    this.contactService.setFilter(contactsFilter);
    await this.contactService.getContacts();
  }

  setFilter(filter: CallLogAPI.GetCallLogsFilter) {
    const ui = this.callLogQuery.Ui;
      this.callLogStore.update({
      ui: {
        ...ui,
        filter
      }
    });
  }

  setPaging(paging: CursorPaging) {
    const ui = this.callLogQuery.Ui;
    this.callLogStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setCurrentPage(currentPage: number) {
    const ui = this.callLogQuery.Ui;
    this.callLogStore.update({
      ui: {
        ...ui,
        currentPage: currentPage
      }
    });
  }

  // Gán relationship(Nhân viên), contact (Khách hàng), hotline (Tổng đài), From(người gọi), To (Người nghe) cho mỗi Call Log (Cuộc gọi).
  callLogsMap(callLogs: CallLog[]): CallLog[] {
    const relationships = this.relationshipQuery.getAll();
    const extensions = this.extensionQuery.getAll();
    const contacts = this.contactQuery.getAll();
    const hotlines = this.hotlineQuery.getAll();
    return callLogs.map(callLog => this.callLogMap(callLog, relationships, extensions, hotlines, contacts))
  }

  callLogMap(callLog: CallLog, relationships?: Relationship[], extensions?: Extension[], hotlines?: Hotline[], contacts?: Contact[]): CallLog {
    let mappingCallLog = {...callLog}

    //Xac dinh Hotline(tổng đài) cua callLog
    let extension = extensions.find(ext => ext.id == mappingCallLog.extension_id);
    let hotline = hotlines.find(hotl => hotl.id == callLog?.hotline_id);
    mappingCallLog.hotline = hotline;
    //Xac dinh may nhanh cua call log
    mappingCallLog.extension_number = extension?.extension_number;

    // Xác định, map relationship, contact ngừoi thực hiện cuộc gọi
    let callerExtension = extensions.find(ext => mappingCallLog.caller == ext.extension_number);
    if(callerExtension){
      let callerRelationship = relationships.find(rel => callerExtension.user_id == rel.user_id);
      mappingCallLog.callerRelationship = callerRelationship;
    }
    else{
      let contact: any = {};
      contact.phone = mappingCallLog.caller;
      let callerContact = contacts.find(cont => mappingCallLog.caller == cont.phone) || contact;
      mappingCallLog.callerContact = callerContact;
    }

    //Xác định, map relationship, contact của ngừoi nhận cuộc gọi
    let calleeExtension = extensions.find(ext => mappingCallLog.callee == ext.extension_number);
    if(calleeExtension){
      let calleeRelationship = relationships.find(rel => calleeExtension.user_id == rel.user_id);
      mappingCallLog.calleeRelationship = calleeRelationship;
    }
    else{
      let contact: any = {};
      contact.phone = mappingCallLog.callee;
      let calleeContact = contacts.find(cont => mappingCallLog.callee == cont.phone) || contact;
      mappingCallLog.calleeContact = calleeContact;
    }

    //Xác định người gọi đến của call log
    let from = mappingCallLog.callerRelationship ? mappingCallLog.callerRelationship : mappingCallLog.callerContact;
    mappingCallLog.from = from;

    //Xác định người nghe của call log
    let to = mappingCallLog.calleeRelationship ? mappingCallLog.calleeRelationship : mappingCallLog.calleeContact
    mappingCallLog.to = to;

    return mappingCallLog;
  }

  setCallLogs(callLogs: CallLog[]) {
    this.callLogStore.set(callLogs);
  }

  changeHeaderTab(tab: CallLogTab) {
    const ui = this.callLogQuery.Ui;
    this.callLogStore.update({
      ui: {
        ...ui,
        tab
      }
    })
  }

  setLoading(loading: boolean) {
    this.callLogStore.setLoading(loading);
  }
}
