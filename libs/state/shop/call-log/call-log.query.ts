import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { CallLog } from '@etop/models';
import { CallLogStore, StateCallLog } from '@etop/state/shop/call-log/call-log.store';

@Injectable({
  providedIn: 'root'
})
export class CallLogQuery extends QueryEntity<StateCallLog, CallLog> {

  constructor(protected store: CallLogStore) {
    super(store);
  }

  get Ui() {
    return this.getValue().ui;
  }
}
