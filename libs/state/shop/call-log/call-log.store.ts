import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { CallLog, CallLogTab, CursorPaging } from '@etop/models';
import {CallLogAPI} from "@etop/api/shop/call-log.api";

export interface StateCallLog extends EntityState<CallLog, string>, ActiveState {
  ui: {
    paging: CursorPaging;
    filter: CallLogAPI.GetCallLogsFilter;
    currentPage: number;
    tab: CallLogTab;
  };
}

const initialState = {
  ui: {
    paging: null,
    filter: null,
    currentPage : 1,
    tab: CallLogTab.general
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shopCallLog', resettable: true })
export class CallLogStore extends EntityStore<StateCallLog> {
  constructor() {
    super(initialState);
  }
}
