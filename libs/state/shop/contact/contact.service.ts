import {Injectable} from '@angular/core';
import {Contact, CursorPaging} from '@etop/models';
import {ContactStore} from '@etop/state/shop/contact/contact.store';
import {ContactQuery} from '@etop/state/shop/contact/contact.query';
import {ContactAPI, ContactApi} from "@etop/api/shop/contact.api";
import {ShopTicketAPI, ShopTicketApi} from "@etop/api";

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  constructor(
    private contactApi: ContactApi,
    private contactStore: ContactStore,
    private contactQuery: ContactQuery,
    private ticketApi: ShopTicketApi,
  ) {
  }

  async getContacts() {
    this.contactStore.setLoading(true);
    try {
      const { filter,paging } = this.contactQuery.Ui;
      const res = await this.contactApi.getContacts({filter, paging});
      const contacts: Contact[] = res.contacts;
      this.setPaging(res.paging);
      this.contactStore.set(contacts);
    } catch (e) {
      debug.error('ERROR in getting Contacts', e);
    }
    this.contactStore.setLoading(false);
  }

  async getTickets(request: ShopTicketAPI.GetTicketsRequest) {
    try {
      return await this.ticketApi.getTickets(request);
    } catch (e) {
      debug.error('Error in getting Tickets', e);
      return [];
    }
  }

  async createContacts(data) {
    return await this.contactApi.createContact(data);
  }

  async getContact(id) {
    return await this.contactApi.getContact(id);
  }

  setFilter(filter: ContactAPI.GetContactsRequest) {
    const ui = this.contactQuery.Ui;
    this.contactStore.update({ ui: {
      ...ui,
      filter: filter.filter,
      paging: filter.paging
    } });
  }

  setPaging(paging: CursorPaging) {
    const ui = this.contactQuery.Ui;
    this.contactStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setCurrentPage(currentPage: number) {
    const ui = this.contactQuery.Ui;
    this.contactStore.update({
      ui: {
        ...ui,
        currentPage: currentPage
      }
    });
  }

  changeHeaderTab(tab) {
    const ui = this.contactQuery.Ui;
    this.contactStore.update({ ui: {
      ...ui,
      tab,
    } });
  }

  setActive(id) {
    this.contactStore.setActive(id)
  }

  async deleteContact(id) {
    await this.contactApi.deleteContact(id);
  }

  async updateContact(contact: Contact) {
    const res = await this.contactApi.updateContact(contact);
    this.contactStore.update(res);
  }
}
