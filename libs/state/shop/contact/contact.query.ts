import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {Contact} from '@etop/models';
import {ContactStore, StateContact} from '@etop/state/shop/contact/contact.store';

@Injectable({
  providedIn: 'root'
})
export class ContactQuery extends QueryEntity<StateContact, Contact> {
  constructor(protected store: ContactStore) {
    super(store);
  }

  findContactById(id) {
    let contacts = this.getAll();
    return contacts?.find(contact => contact.id == id);
  }

  findContactByPhone(phone) {
    let contacts = this.getAll();
    return contacts?.find(contact => contact.phone == phone);
  }

  get Ui () {
    return this.getValue().ui;
  }
}
