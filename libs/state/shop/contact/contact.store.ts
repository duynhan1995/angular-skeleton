import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import {Contact, ContactTab, CursorPaging} from '@etop/models';
import {ContactAPI} from "@etop/api/shop/contact.api";

export interface StateContact extends EntityState<Contact, string>, ActiveState {
  ui: {
    paging: CursorPaging;
    filter: ContactAPI.GetContactsFilter;
    currentPage: Number;
    tab: ContactTab;
  };
}

const initialState = {
  ui: {
    paging: null,
    filter: null,
    currentPage: 1,
    tab: ContactTab.general
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shopContact', resettable: true })
export class ContactStore extends EntityStore<StateContact> {
  constructor() {
    super(initialState);
  }
}
