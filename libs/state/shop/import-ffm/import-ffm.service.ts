import { Injectable } from '@angular/core';
import {FulfillmentAPI, FulfillmentApi} from "@etop/api";
import {AuthenticateStore} from "@etop/core";
import {FilterFulfillmentImport, ImportFfmForm, ImportFfmStore} from "@etop/state/shop/import-ffm/import-ffm.store";
import {ImportFfmQuery} from "@etop/state/shop/import-ffm/import-ffm.query";
import {arrayUpsert} from "@datorama/akita";
import CreateShipmentFulfillmentFromImportRequest = FulfillmentAPI.CreateShipmentFulfillmentFromImportRequest;
import {StringHandler} from "@etop/utils";
import {Fulfillment} from "@etop/models";
import {TelegramService} from "@etop/features";

@Injectable({
  providedIn: 'root'
})
export class ImportFfmService {

  _interval;

  constructor(
    private ffmApi: FulfillmentApi,
    private query: ImportFfmQuery,
    private store: ImportFfmStore,
    private auth: AuthenticateStore,
    private telegram: TelegramService
  ) {
  }

  static isErrorFfm(ffm: ImportFfmForm): boolean {
    const {province_code, district_code, ward_code, errors} = ffm;
    return errors?.length > 0 || !(province_code && district_code && ward_code);
  }

  static validatePhone(ffm: ImportFfmForm) {
    const _ffm = JSON.parse(JSON.stringify(ffm));
    const validPhone = StringHandler.validatePhoneNumber(_ffm.customer_phone);
    const index = _ffm.errors.findIndex(err => err.field_name == 'customer_phone');
    if (validPhone) {
      if (index >= 0) {
        _ffm.errors.splice(index, 1);
      }
    } else {
      if (index < 0) {
        _ffm.errors.push({
          field_name: 'customer_phone',
          msg: 'Vui lòng nhập số điện thoại hợp lệ!'
        });
      } else {
        _ffm.errors[index] = {
          field_name: 'customer_phone',
          msg: 'Vui lòng nhập số điện thoại hợp lệ!'
        }
      }
    }
    return _ffm;
  }

  static validateCodAmount(ffm: ImportFfmForm) {
    const _ffm = JSON.parse(JSON.stringify(ffm));
    const index = _ffm.errors.findIndex(err => err.field_name == 'cod_amount');
    if (_ffm.cod_amount == 0 || _ffm.cod_amount >= 5000) {
      if (index >= 0) {
        _ffm.errors.splice(index, 1);
      }
    } else {
      if (index < 0) {
        _ffm.errors.push({
          field_name: 'cod_amount',
          msg: 'Vui lòng nhập thu hộ bằng 0đ hoặc từ 5.000đ trở lên!'
        });
      } else {
        _ffm.errors[index] = {
          field_name: 'cod_amount',
          msg: 'Vui lòng nhập thu hộ bằng 0đ hoặc từ 5.000đ trở lên!'
        }
      }
    }
    return _ffm;
  }

  static validateBasketValue(ffm: ImportFfmForm) {
    const _ffm = JSON.parse(JSON.stringify(ffm));
    const index = _ffm.errors.findIndex(err => err.field_name == 'basket_value');
    if (_ffm.basket_value == 0 || _ffm.basket_value) {
      if (index >= 0) {
        _ffm.errors.splice(index, 1);
      }
    } else {
      if (index < 0) {
        _ffm.errors.push({
          field_name: 'basket_value',
          msg: 'Vui lòng nhập khai giá từ 0đ trở lên!'
        });
      } else {
        _ffm.errors[index] = {
          field_name: 'basket_value',
          msg: 'Vui lòng nhập khai giá từ 0đ trở lên!'
        }
      }
    }
    return _ffm;
  }

  static validateTotalWeight(ffm: ImportFfmForm) {
    const _ffm = JSON.parse(JSON.stringify(ffm));
    const index = _ffm.errors.findIndex(err => err.field_name == 'total_weight');
    if (_ffm.total_weight >= 50) {
      if (index >= 0) {
        _ffm.errors.splice(index, 1);
      }
    } else {
      if (index < 0) {
        _ffm.errors.push({
          field_name: 'total_weight',
          msg: 'Vui lòng nhập khối lượng từ 50g trở lên!'
        });
      } else {
        _ffm.errors[index] = {
          field_name: 'total_weight',
          msg: 'Vui lòng nhập khối lượng từ 50g trở lên!'
        }
      }
    }
    return _ffm;
  }

  static validateFfmImport(ffm: ImportFfmForm) {
    const toBeValidatedFields = {
      ed_code: 'mã nội bộ',
      customer_name: 'tên khách hàng',
      customer_phone: 'số điện thoại khách hàng',
      shipping_address: 'địa chỉ giao hàng',
      province_code: 'tỉnh thành',
      district_code: 'quận huyện',
      ward_code: 'phường xã',
      product_description: 'thông tin sản phẩm',
      total_weight: 'khối lượng',
      cod_amount: 'thu hộ',
      basket_value: 'khai giá',
      connection_id: 'gói dịch vụ'
    }
    let _ffm = JSON.parse(JSON.stringify(ffm));
    for (let key in _ffm) {
      const index = _ffm.errors.findIndex(err => err.field_name == key);

      if (!toBeValidatedFields[key]) {
        continue;
      }
      if (key == 'ed_code') {
        if (index >= 0) {
          _ffm.errors.splice(index, 1);
        }
        continue;
      }

      if (!_ffm[key] || (typeof _ffm[key] == 'string' && !_ffm[key]?.trim())) {
        if (index < 0) {
          _ffm.errors.push({
            field_name: key,
            msg: `Vui lòng nhập ${toBeValidatedFields[key]} hợp lệ`
          });
        } else {
          _ffm.errors[index] = {
            field_name: key,
            msg: `Vui lòng nhập ${toBeValidatedFields[key]} hợp lệ`
          };
        }
      } else {
        if (index >= 0) {
          _ffm.errors.splice(index, 1);
        }
      }

      if (key == 'customer_phone') {
        _ffm = this.validatePhone(_ffm);
      }
      if (key == 'cod_amount') {
        _ffm = this.validateCodAmount(_ffm);
      }
      if (key == 'basket_value') {
        _ffm = this.validateBasketValue(_ffm);
      }
      if (key == 'total_weight') {
        _ffm = this.validateTotalWeight(_ffm);
      }
    }

    return _ffm;
  }

  setFilter(filterBy: FilterFulfillmentImport) {
    this.store.update({
      ui: {
        filter: filterBy
      }
    });
  }

  async importFfm(file) {
    this.store.setLoading(true);
    try {
      const formData = new FormData();
      formData.append('files', file);
      const res = await this.ffmApi.importFulfillments(formData);

      const fulfillments: Fulfillment[] = [];
      const importFfmForms: ImportFfmForm[] = [];

      res.fulfillments.forEach(ffm => {
        const id = Date.now() + Math.random() * 1000;
        fulfillments.push({
          ...ffm, id
        });
        importFfmForms.push({
          ...ffm,
          id, connection_id: null, try_on: ffm.try_on || this.auth.snapshot.shop.try_on,
          errors: [], selected: true
        });
      });

      const specificColumns = res.specific_columns;
      const cellErrors: any[] = res.cell_errors;

      for (let err of cellErrors) {
        const rowIndex = err.meta.row_index;
        const colIndex = err.meta.col_index;
        importFfmForms[rowIndex - 1].errors.push({
          msg: err.msg,
          field_name: specificColumns[colIndex].name
        });
      }

      this.store.set(fulfillments);
      this.store.update({importFfmForms: importFfmForms.splice(0, 1)});

      this._interval = setInterval(_ => {
        if (!importFfmForms.length) {
          clearInterval(this._interval);
          this.store.setLoading(false);
          return;
        }
        const _lastForms = this.query.getAllWithFilter();
        this.store.update({importFfmForms: _lastForms.concat(importFfmForms.splice(0, 1))});
      }, 50);

    } catch(e) {
      debug.error('ERROR in import ffm service', e);
      this.store.setLoading(false);
      throw e;
    }
  }

  updateFfmForms(formData: ImportFfmForm[]) {
    formData.forEach(_form => {
      if (!_form.errors) {
        _form.errors = []
      }
      _form = ImportFfmService.validateFfmImport(_form);

      this.store.update({
        importFfmForms: arrayUpsert(this.query.getAllWithFilter(), _form.id, {..._form})
      });
    });
  }

  async createFulfillmentsFromImport(body: CreateShipmentFulfillmentFromImportRequest[]) {
    try {
      const res = await this.ffmApi.createFulfillmentsFromImport(body);
      this.updateFfmForms(res.fulfillments);
      this.telegram.importFfmMessage(res.fulfillments).then();

      return res;
    } catch(e) {
      debug.error('ERROR in createFulfillmentFromImport()', e);
      throw e;
    }
  }

  resetStore() {
    clearInterval(this._interval);
    this.store.reset();
    this.store.set([]);
    this.store.setLoading(true);
  }

}
