import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {FilterFulfillmentImport, ImportFfmStore, StateImportFfm} from "@etop/state/shop/import-ffm/import-ffm.store";
import {combineLatest} from "rxjs";
import {distinctUntilChanged, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ImportFfmQuery extends QueryEntity<StateImportFfm> {

  constructor(protected store: ImportFfmStore) {
    super(store);
  }

  getImportFfmForm(id: string) {
    return this.getValue().importFfmForms.find(form => form.id == id);
  }

  selectImportFfmForm(id: string) {
    return this.select("importFfmForms").pipe(
      map(forms => forms.find(f => f.id == id)),
      distinctUntilChanged((a,b) => a?.id == b?.id)
    );
  }

  getAllWithFilter(onlySelectedItems = false) {
    let allFfmImportForms = this.getValue().importFfmForms;
    if (onlySelectedItems) {
      allFfmImportForms = allFfmImportForms.filter(form => form.selected);
    }
    const filter = this.getValue().ui.filter;
    switch (filter) {
      case FilterFulfillmentImport.allImport:
        return allFfmImportForms;
      case FilterFulfillmentImport.failedImport:
        return allFfmImportForms.filter(ffm => ffm.createStatus == 'failed');
      case FilterFulfillmentImport.successImport:
        return allFfmImportForms.filter(ffm => ffm.createStatus == 'success');
      default:
        return allFfmImportForms;
    }
  }

  selectAllWithFilter(onlySelectedItems = false) {
    return combineLatest([this.select("importFfmForms"), this.select(state => state.ui.filter)])
      .pipe(map(([importFfms, filter]) => {
        if (onlySelectedItems) {
          importFfms = importFfms.filter(form => form.selected);
        }
        switch (filter) {
          case FilterFulfillmentImport.allImport:
            return importFfms;
          case FilterFulfillmentImport.failedImport:
            return importFfms.filter(ffm => ffm.createStatus == 'failed');
          case FilterFulfillmentImport.successImport:
            return importFfms.filter(ffm => ffm.createStatus == 'success');
        }
        return importFfms;
      }));
  }

}
