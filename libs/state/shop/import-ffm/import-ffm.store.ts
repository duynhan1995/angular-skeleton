import { Injectable } from '@angular/core';
import {StoreConfig, EntityState, EntityStore} from '@datorama/akita';
import {Fulfillment, FulfillmentShipmentService} from "@etop/models";

export enum FilterFulfillmentImport {
  allImport = 'all_import',
  failedImport = 'failed_import',
  successImport = 'success_import'
}

export interface ImportFfmForm {
  id: string;
  selected: boolean;

  gettingShippingServices: boolean;
  shippingServicesList: FulfillmentShipmentService[];

  errors: Array<{field_name: string; msg: string;}>;

  createErrorMsg?: string;
  createStatus?: 'failed' | 'success';

  ed_code: string;
  customer_name: string;
  customer_phone: string;
  shipping_address: string;
  province_code: string;
  district_code: string;
  ward_code: string;
  product_description: string;
  total_weight: number;
  basket_value: number;
  include_insurance: boolean;
  cod_amount: number;
  shipping_service_code: string;
  shipping_service_fee: number;
  shipping_service_name: string;
  shipping_carrier_name: string;
  connection_id: string;
  try_on: string;
  shipping_note: string;
}

export interface StateImportFfm extends EntityState<Fulfillment, string> {
  ui: {
    filter: FilterFulfillmentImport
  };
  importFfmForms: ImportFfmForm[];
}

const initialState = {
  ui: {
    filter: null
  },
  importFfmForms: []
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'fulfillmentImport', resettable: true })
export class ImportFfmStore extends EntityStore<StateImportFfm> {
  constructor() {
    super(initialState);
  }
}
