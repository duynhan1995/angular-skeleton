import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Fulfillment, MoneyTransactionShop } from '@etop/models';
import { Injectable } from '@angular/core';

export interface StateMoneyTransactions extends EntityState <MoneyTransactionShop,string>, ActiveState {
  sum: {
    success_count: number,
    returned_count: number,
    cod_count: number,
    non_cod_count: number,
    total_items_value: number,
    total_cod: number,
    total_amount: number,
    total_shipping_fee_shop: number,
  };
  fulfillments: Fulfillment[];

}
const initState = {
  sum: {
    success_count: null,
    returned_count: null,
    cod_count: null,
    non_cod_count: null,
    total_items_value: null,
    total_cod: null,
    total_amount: null,
    total_shipping_fee_shop: null,
  },
  fulfillments: null,
};
@Injectable({
  providedIn: 'root'
})
@StoreConfig({name:'money_tranasction', resettable: true})
export class MoneyTransactionsStore extends EntityStore<StateMoneyTransactions>{
  constructor() {
    super(initState);
  }
}
