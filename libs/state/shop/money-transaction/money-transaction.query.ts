import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { MoneyTransactionShop } from '@etop/models';
import { MoneyTransactionsStore, StateMoneyTransactions } from '@etop/state/shop/money-transaction/money-transaction.store';

@Injectable({
  providedIn: 'root'
})
export class MoneyTransactionsQuery extends QueryEntity<StateMoneyTransactions,MoneyTransactionShop> {
  constructor(
    protected store: MoneyTransactionsStore,
  ) {
    super(store);
  }
}
