import { Injectable } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import {Filters, Fulfillment, MoneyTransactionShop} from '@etop/models';
import {FulfillmentApi, MoneyTransactionApi} from '@etop/api';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ConnectionStore } from '@etop/features';
import { MoneyTransactionsStore } from '@etop/state/shop/money-transaction/money-transaction.store';
import { MoneyTransactionsQuery } from '@etop/state/shop/money-transaction/money-transaction.query';

@Injectable({
  providedIn: 'root'
})

export class MoneyTransactionsService {
  constructor(
    private auth: AuthenticateStore,
    private moneyTransactionsStore:MoneyTransactionsStore,
    private connectionStore: ConnectionStore,
    private fulfillmentApi: FulfillmentApi,
    private transactionApi: MoneyTransactionApi,
  ) {
    this.auth.state$.pipe(map(state => state), distinctUntilChanged()).subscribe((state) => {
        this.resetData();
    });
  }

  resetData(){
    this.moneyTransactionsStore.remove();
  }

  setActiveMoneyTransaction(money_transaction: MoneyTransactionShop) {
    this.moneyTransactionsStore.setActive(money_transaction.id);
  }

  async getMoneyTransaction(id,token: string){
    return this.transactionApi.getMoneyTransaction(id,token)
  }

  upsertMoneyTransaction(id, money_transaction: MoneyTransactionShop){
    this.moneyTransactionsStore.upsert(id,money_transaction)
  }

  async selectedMoneyTransaction(start?: number, perpage?: number, filters?: Filters){
    try{
      const result:any = await this.getFulfillments(start,perpage,filters);
      const {fulfillments} = result
      this.moneyTransactionsStore.update({fulfillments:fulfillments})
      this.fulfillmentsInfo(fulfillments)
    }catch (e) {
      debug.error('ERROR in list money_transactions',e);
    }
  }

  fulfillmentsInfo(fulfillments: Fulfillment[]){
    let sum = {
      success_count: 0,
      returned_count: 0,
      cod_count: 0,
      non_cod_count: 0,
      total_items_value: 0,
      total_cod: 0,
      total_amount: 0,
      total_shipping_fee_shop: 0
    };
    fulfillments.forEach(ful => {
      switch (ful.shipping_state) {
        case 'delivered':
          break;
        case 'returning':
          sum.returned_count += 1;
          break;
        case 'returned':
          sum.returned_count += 1;
          break;
      }
      // tslint:disable-next-line:max-line-length
      if (ful.total_cod_amount > 0 && ((['returned'].indexOf(ful.shipping_state)) < 0 || (['returning'].indexOf(ful.shipping_state)) < 0)) {
        sum.cod_count += 1;
      } else {
        sum.non_cod_count += 1;
      }
      sum.total_items_value += ful.order.total_amount || 0;
      sum.total_shipping_fee_shop += ful.shipping_fee_shop || 0;
      if (ful.shipping_state == 'undeliverable' && ful.actual_compensation_amount > 0) {
        sum.total_cod += ful.actual_compensation_amount;
        sum.total_amount += ful.actual_compensation_amount - ful.shipping_fee_shop;
      } else if (ful.shipping_state.indexOf('returned') < 0 && ful.shipping_state.indexOf('returning') < 0) {
        sum.total_cod += ful.total_cod_amount;
        sum.total_amount += ful.total_cod_amount - ful.shipping_fee_shop;
      } else {
        sum.total_amount -= ful.shipping_fee_shop;
      }
    });
    this.moneyTransactionsStore.update({sum:sum});
  }

  async getMoneyTransactions(shop_id, start?: number, perpage?: number){
    const paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    const shop = this.auth.snapshot.accounts.find(a => a.id === shop_id);
    if (shop){
      const res = await this.transactionApi.getMoneyTransactions(paging,shop.token)
      this.moneyTransactionsStore.upsertMany(res.money_transactions)
    }
  }

  async getFulfillments(start?: number, perpage?: number, filters?: Filters) {
    const paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.fulfillmentApi.getFulfillments({ paging, filters })
            .then(res => {
              res.fulfillments = res.fulfillments.map(f => Fulfillment.fulfillmentMap(f, connections));
              resolve(res);
            })
            .catch(err => reject(err));
        }
      });
    });
  }

}
