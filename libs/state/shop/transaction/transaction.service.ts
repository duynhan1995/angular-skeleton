import {Injectable} from '@angular/core';
import {TransactionStore} from './transaction.store';
import {TransactionQuery} from './transaction.query';
import {Transaction} from '@etop/models/Transaction';
import {CursorPaging} from '@etop/models';
import {ShopTransactionAPI, ShopTransactionApi} from '@etop/api/shop/shop-transaction.api';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  constructor(
    private transactionApi: ShopTransactionApi,
    private transactionStore: TransactionStore,
    private transactionQuery: TransactionQuery
  ) {
  }

  async getTransactions() {
    this.setLoading(true);
    try {
      const {filter, paging} = this.transactionQuery.currentUi;
      const res = await this.transactionApi.getTransactions({filter, paging});
      this.setPaging(res.paging);
      this.transactionStore.set(res.transactions);
    } catch (e) {
      debug.error('ERROR in getting Transactions', e);
    }
    this.setLoading(false);
  }

  setFilter(filter: ShopTransactionAPI.GetTransactionFilter) {
    this.transactionStore.update({
      ui: {
        ...this.transactionQuery.currentUi,
        filter,
      },
    });
  }

  setPaging(paging: CursorPaging) {
    this.transactionStore.update({
      ui: {
        ...this.transactionQuery.currentUi,
        paging,
      },
    });
  }

  setCurrentPage(currentPage: number) {
    this.transactionStore.update({
      ui: {
        ...this.transactionQuery.currentUi,
        currentPage,
      },
    });
  }

  setTransactions(transactions: Transaction[]) {
    this.transactionStore.set(transactions);
  }

  setLoading(loading: boolean) {
    this.transactionStore.setLoading(loading);
  }
}
