import {Injectable} from '@angular/core';
import {ActiveState, EntityState, EntityStore, StoreConfig} from '@datorama/akita';
import {CursorPaging, Transaction} from '@etop/models';
import {ShopTransactionAPI} from '@etop/api/shop/shop-transaction.api';

export interface StateTransaction extends EntityState<Transaction, string>, ActiveState {
  ui: {
    paging: CursorPaging;
    filter: ShopTransactionAPI.GetTransactionFilter;
    currentPage: number;
  };
}

const initialState = {
  ui: {
    paging: null,
    filter: null,
    currentPage: 1,
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'shopTransaction', resettable: true})
export class TransactionStore extends EntityStore<StateTransaction> {
  constructor() {
    super(initialState);
  }
}
