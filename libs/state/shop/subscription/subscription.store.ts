import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Subscription } from '@etop/models';
import { Paging } from '@etop/shared';
import { SubscriptionAPI } from '@etop/api';

export interface StateSubscription extends EntityState<Subscription, string>, ActiveState {
  ui: {
    paging: Paging;
    filters: Array<SubscriptionAPI.GetSubscriptionFilter>;
    account_id: string;
  };
  trialDate: number,
  popupTrial: boolean
}

const initialState = {
  ui: {
    paging: null,
    filters: [],
    account_id: '',
  },
  trialDate: null,
  popupTrial: true
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shopSubscription', resettable: true })
export class SubscriptionStore extends EntityStore<StateSubscription> {
  constructor() {
    super(initialState);
  }
}
