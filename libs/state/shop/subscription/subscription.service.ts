import {Injectable} from '@angular/core';
import { ProductSubscriptionType, SubscriptionAPI, SubscriptionApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { Filters } from '@etop/models';
import { CmsService } from 'apps/core/src/services/cms.service';
import { SubscriptionQuery } from './subscription.query';
import { SubscriptionStore } from './subscription.store';


@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  constructor(
    private subscriptionQuery: SubscriptionQuery,
    private subscriptionStore: SubscriptionStore,
    private subscriptionApi: SubscriptionApi,
    private auth: AuthenticateStore,
    private cms: CmsService,
  ) {
  }

  setFilters(filters: Filters) {
    const ui = this.subscriptionQuery.getValue().ui;
    this.subscriptionStore.update({
      ui: {
        ...ui,
        filters
      }
    });
  }

  async getSubscriptions(doLoading = true){
    if(doLoading){
      this.subscriptionStore.setLoading();
    }
    try {
      const { filters } = this.subscriptionQuery.getValue().ui;
      const getSubscriptionsRequest: SubscriptionAPI.GetSubscriptionsRequest={
        filters,
      };
      let subscriptions = await this.subscriptionApi.getSubscriptions(getSubscriptionsRequest);
      subscriptions = subscriptions.filter(sub => {
        return sub.status == "P" && sub.lines.length > 0;
      })
      this.subscriptionStore.set(subscriptions);
      if ( doLoading ){
        this.subscriptionStore.setActive(null);
      }
    } catch(e) {
      debug.error('ERROR in getting Subscriptions', e);
    }
    if (doLoading) {
      this.subscriptionStore.setLoading(false);
    }
  }

  checkTrialSubscriptions(){
    const dataBanner = this.cms.getMessageNotSubscription();
    let user_created_at = Date.parse(this.auth.snapshot.user.created_at);
    let trial_date = user_created_at + (dataBanner.trial_date * 86400000);
    let now = Date.now();
    return Math.ceil((trial_date - now) / 86400000);
  }

  async getSubscriptionPlans(product_id: string) {
    return await this.subscriptionApi.getSubscriptionPlans(product_id);
  }

  async getSubscriptionProducts(type: ProductSubscriptionType) {
    return await this.subscriptionApi.getSubscriptionProducts(type);
  }

  async getAllSubscriptionPlan() {
    const subscriptionProducts = await this.getSubscriptionProducts(
      ProductSubscriptionType.EXTENSION);
    let subscriptionPlans = [];
    await Promise.all(subscriptionProducts.map( async subproduct => {
      const plan = await this.getSubscriptionPlans(subproduct.id);
      subscriptionPlans = subscriptionPlans.concat(plan);
    }));
    return subscriptionPlans;
  }

}
