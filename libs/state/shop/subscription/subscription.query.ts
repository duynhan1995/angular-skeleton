import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Subscription } from '@etop/models';
import { StateSubscription, SubscriptionStore } from '@etop/state/shop/subscription/subscription.store';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionQuery extends QueryEntity<StateSubscription, Subscription> {

  constructor(protected store: SubscriptionStore) {
    super(store);
  }

  getSubscriptionsById(ids: string[]) : Subscription[] {
    return this.getAll().filter(s => ids.indexOf(s.id) > -1);
  }
}
