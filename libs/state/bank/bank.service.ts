import {Injectable} from '@angular/core';
import {BankQuery} from "./bank.query";
import {BankStore} from "./bank.store";
import {Bank} from "@etop/models";
import {HttpService} from "@etop/common";
import {BankApi} from "@etop/api";

@Injectable({
  providedIn: "root"
})
export class BankService {

  constructor(
    private http: HttpService,
    private bankApi: BankApi,
    private bankStore: BankStore,
    private bankQuery: BankQuery
  ) {}

  private static sortBanks(banks) {
    const priorities = [
      { i: 0, n: '301' },
      { i: 1, n: '201' },
      { i: 2, n: '304' },
      { i: 3, n: '303' },
      { i: 4, n: '310' },
      { i: 5, n: '204' },
      { i: 6, n: '307' },
      { i: 7, n: '358' },
      { i: 8, n: '202' }
    ];
    banks.sort((a, b) => a.name > b.name);

    const priBanks = [];
    const norBanks = [];

    banks.forEach(b => {
      const prior = priorities.find(p => p.n === b.code);
      if (!prior) {
        norBanks.push(b);
        return;
      }
      priBanks[prior.i] = b;
    });

    return priBanks.concat(norBanks);
  }

  async initBanks() {
    try {
      const {banksList, bankProvincesList, bankBranchesList} = this.bankQuery.getValue();
      if (!banksList?.length) {
        await this.listBanks();
      }
      if (!bankProvincesList?.length) {
        await this.listBankProvinces();
      }
      if (!bankBranchesList?.length) {
        await this.listBankBranches();
      }

      this.mapVCBToTCB(); // NOTE: Used only in Admin

      this.setBankReady(true);
    } catch (e) {
      debug.error('ERROR in init bank service', e);

      setTimeout(_ => {
        this.initBanks();
      }, 1000);
    }
  }

  setBankReady(ready: boolean) {
    this.bankStore.update({bankReady: ready});
  }

  mapVCBToTCB() {
    this.http.get('assets/branch_mapping.json').toPromise()
      .then(data => {
        this.bankStore.update({techcombankBranches: data});
      });
  }

  async listBanks() {
    try {
      const res = await this.bankApi.listBanks();
      res.banks = BankService.sortBanks(res.banks);
      const banks = res.banks.map(b => new Bank({
        ...b,
        id: b.code
      }));
      this.bankStore.update({banksList: banks});
    } catch(e) {
      debug.log('ERROR in listBanks', e);
      throw e;
    }
  }

  async listBankProvinces() {
    try {
      const res = await this.bankApi.listBankProvinces();
      const provinces = res.provinces.map(b => new Bank({
        ...b,
        id: b.code
      }));
      this.bankStore.update({bankProvincesList: provinces});
    } catch(e) {
      debug.error('ERROR in listBankProvinces', e);
      throw e;
    }
  }

  async listBankBranches() {
    try {
      const res = await this.bankApi.listBankBranches();
      const branches = res.branches.map(b => new Bank({
        ...b,
        id: b.code
      }));
      this.bankStore.update({bankBranchesList: branches});
    } catch(e) {
      debug.error('ERROR in listBankBranches', e);
      throw e;
    }
  }

}
