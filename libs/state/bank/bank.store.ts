import { Injectable } from '@angular/core';
import {Store, StoreConfig} from "@datorama/akita";
import {Bank, BankBranch, BankProvince} from "@etop/models";

export interface StateBank {
  banksList: Bank[];
  bankProvincesList: BankProvince[];
  bankBranchesList: BankBranch[];

  bankReady: boolean;

  techcombankBranches: any;
}

const initialState = {
  banksList: [],

  bankReady: false,

  techcombankBranches: {}
};

@Injectable({
  providedIn: "root"
})
@StoreConfig({name: 'bank'})
export class BankStore extends Store<StateBank> {
  constructor() {
    super(initialState);
  }
}
