import { Injectable } from '@angular/core';
import {Query} from '@datorama/akita';
import {BankStore, StateBank} from "./bank.store";

@Injectable({
  providedIn: 'root'
})
export class BankQuery extends Query<StateBank> {

  constructor(protected store: BankStore) {
    super(store);
  }

  getBank(code: string) {
    const banks = this.getValue().banksList;
    return banks?.find(bank => bank.code == code);
  }

  getBankProvince(code: string) {
    const bankProvinces = this.getValue().bankProvincesList;
    return bankProvinces?.find(prov => prov.code == code);
  }

  getBankBranch(code: string) {
    const bankBranches = this.getValue().bankBranchesList;
    return bankBranches?.find(branch => branch.code == code);
  }

  getBankByName(name: string) {
    const banks = this.getValue().banksList;
    return banks?.find(bank => bank.name == name);
  }

  getBankProvinceByName(name: string) {
    const bankProvinces = this.getValue().bankProvincesList;
    return bankProvinces?.find(prov => prov.name == name);
  }

  getBankBranchByName(name: string, provinceCode: string) {
    const bankBranches = this.getValue().bankBranchesList;
    return bankBranches?.find(branch => branch.name == name && branch.province_code == provinceCode);
  }

}
