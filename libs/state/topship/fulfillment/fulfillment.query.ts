import { Injectable } from '@angular/core';
import { QueryEntity, QueryConfig, EntityUIQuery } from '@datorama/akita';
import { Fulfillment } from './../../../models/Fulfillment';
import { FulfillmentStore, FulfillmentUIState, StateFulfillments} from './fulfillment.store';

@Injectable({
  providedIn: 'root'
})
export class FulfillmentQuery extends QueryEntity<StateFulfillments, Fulfillment> {
  ui: EntityUIQuery<FulfillmentUIState>

  constructor(protected store: FulfillmentStore) {
    super(store);
  }
}
