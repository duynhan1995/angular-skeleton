import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentApi } from '@etop/api';
import { Injectable } from '@angular/core';
import { Filters } from '@etop/models';
import { FulfillmentStore } from './fulfillment.store';
import { FulfillmentQuery } from './fulfillment.query';
import { ConnectionService } from '@etop/features';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { distinctUntilChanged, map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FulfillmentStoreService {
  constructor(
    private fulfillmentApi: FulfillmentApi,
    private fulfillmentStore: FulfillmentStore,
    private fulfillmentQuery: FulfillmentQuery,
    private connectionStore: ConnectionStore,
    private connectionService: ConnectionService
  ) {}

  async getFulfillments(start?: number, perpage?: number, filters?: Filters) {
    this.fulfillmentStore.setLoading(true);
    try {
      let paging = {
        offset: start || 0,
        limit: perpage || 20
      };
      await this.connectionService.getValidConnections();
      const res = await this.fulfillmentApi.getFulfillments({
        paging,
        filters
      });
      res.fulfillments = res.fulfillments.map(f =>
        Fulfillment.fulfillmentMap(
          f,
          this.connectionStore.snapshot.initConnections
        )
      );
      this.fulfillmentStore.set(res.fulfillments);
    } catch (e) {
      debug.error('ERROR in getting Fulfillments', e);
    }
    this.fulfillmentStore.setLoading(false);
  }

  async getFulfillmentsLoadMore(
    start?: number,
    perpage?: number,
    filters?: Filters
  ) {
    this.fulfillmentStore.setLoading(true);
    try {
      let paging = {
        offset: start || 0,
        limit: perpage || 20
      };
      await this.connectionService.getValidConnections();
      const res = await this.fulfillmentApi.getFulfillments({
        paging,
        filters
      });
      res.fulfillments = res.fulfillments.map(f =>
        Fulfillment.fulfillmentMap(
          f,
          this.connectionStore.snapshot.initConnections
        )
      );
      this.fulfillmentStore.upsertMany(res.fulfillments);
      return res.fulfillments;
    } catch (e) {
      debug.error('ERROR in getting Fulfillments', e);
    }
    this.fulfillmentStore.setLoading(false);
  }

  async getFulfillment(id, token?): Promise<Fulfillment> {
    try{
      await this.connectionService.getValidConnections();
      let ffm = await this.fulfillmentApi.getFulfillment(id, token);
      ffm = Fulfillment.fulfillmentMap(ffm, this.connectionStore.snapshot.initConnections);
      return ffm;
    }
    catch(e){
      debug.error('ERROR in getting Fulfillment', e);
    }
  }

  upsertFfm(ffmId, ffm: Fulfillment) {
    this.fulfillmentStore.upsert(ffmId, ffm);
  }

  setActiveFfm(activeFfm: Fulfillment) {
    this.fulfillmentStore.setActive(activeFfm.id);
  }

  updateFfm(ffm: Fulfillment) {
    this.fulfillmentStore.update(ffm.id, ffm);
  }

  async cancelFfm(fulfillment, reason, token) {
    try {
      if (fulfillment?.order?.fulfillment_type == 'shipment')
        await this.fulfillmentApi.cancelShipmentFulfillment(
          fulfillment.id,
          reason
        );
      if (fulfillment?.order?.fulfillment_type == 'shipnow')
        await this.fulfillmentApi.cancelShipnowFulfillment(
          fulfillment.id,
          reason
      );
      const ffm = await this.getFulfillment(fulfillment.id, token);
      this.updateFfm(ffm);
    } catch (e) {
      throw e;
    }
  }
}
