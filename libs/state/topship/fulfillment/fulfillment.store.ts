import { Fulfillment } from './../../../models/Fulfillment';
import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState, EntityUIStore } from '@datorama/akita';

export interface FulfillmentUI {
    isOpen: boolean;
    isLoading: boolean;
}

export interface StateFulfillments extends EntityState<Fulfillment, string>, ActiveState {
}

export interface FulfillmentUIState extends EntityState<FulfillmentUI> {}

const initialState = {
};


@Injectable({
    providedIn: 'root'
  })
@StoreConfig({name:'fulfillment', resettable: true})
export class FulfillmentStore extends EntityStore<StateFulfillments>{
    constructor() {
        super(initialState);
    }
}
  