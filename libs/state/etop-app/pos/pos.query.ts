import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { StatePos, PosStore } from './pos.store';
import { OrderAPI } from '@etop/api';

@Injectable({
  providedIn: 'root'
})
export class PosQuery extends QueryEntity<StatePos, OrderAPI.CreateOrderRequest> {

  constructor(protected store: PosStore) {
    super(store);
  }
}
