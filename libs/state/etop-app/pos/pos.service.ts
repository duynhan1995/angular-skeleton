import { Injectable } from '@angular/core';
import { PosStore } from './pos.store';
import { PosQuery } from './pos.query';
import { OrderAPI, FulfillmentAPI, OrderApi, ReceiptApi } from '@etop/api';
import { Receipt } from 'libs/models/Receipt';
import { Order } from 'libs/models/Order';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import {Customer, Variant} from "@etop/models";

@Injectable({
  providedIn: 'root'
})
export class PosService {
  constructor(
    private posStore: PosStore,
    private posQuery: PosQuery,
    private orderApi: OrderApi,
    private receiptService: ReceiptService,
    private receiptApi: ReceiptApi
  ) {}

  recentlyCreateProduct(productID: string) {
    this.posStore.update({recentlyCreatedProductID: productID});
  }

  checkVariant(variant: Variant) {
    const _selectedVariants = JSON.parse(JSON.stringify(this.posStore.getValue().selectedVariants));

    const _index = _selectedVariants.findIndex(v => v.id == variant.id);

    if (_index >= 0) {
      _selectedVariants.splice(_index, 1);
    } else {
      _selectedVariants.push(variant);
    }

    this.posStore.update({selectedVariants: _selectedVariants});
  }

  updateOrder(order: Order) {
    this.posStore.update({ order });
  }

  updateCustomer(customer: Customer) {
    this.posStore.update({ customer });
  }

  updateFulfillment(
    fulfillment: FulfillmentAPI.CreateShipmentFulfillmentRequest
  ) {
    this.posStore.update({ fulfillment });
  }

  updateReceipt(receipt: Receipt) {
    this.posStore.update({ receipt });
  }

  updateConfirmOrder(shipmentConfirmOrder: Order) {
    this.posStore.update({ shipmentConfirmOrder });
  }

  async createOrder(order: OrderAPI.CreateOrderRequest) {
    try {
      const res = await this.orderApi.createOrder(order);
      await this.orderApi.confirmOrder(res.id, 'confirm');
      this.updateConfirmOrder(res);
      return res;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  async createReceipt() {
    try {
      const receipt = this.posQuery.getValue().receipt;
      const res = await this.receiptService.createReceipt(receipt);
      await this.receiptApi.confirmReceipt(res.id);
      this.updateReceipt(res);
    } catch (e) {
      throw new Error(e.message);
    }
  }

  updateLastWeight(weight: number) {
    this.posStore.update({lastWeight: weight});
  }

  resetStore() {
    const _weight = this.posQuery.getValue().lastWeight;
    this.posStore.reset();
    this.updateLastWeight(_weight);
  }

}
