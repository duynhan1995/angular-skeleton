import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import { FulfillmentAPI } from '@etop/api';
import { Receipt } from 'libs/models/Receipt';
import { Customer } from 'libs/models/Customer';
import { Order } from 'libs/models/Order';
import {Variant} from "@etop/models";

export interface StatePos extends EntityState<any, string>, ActiveState {
  order: Order;
  fulfillment: FulfillmentAPI.CreateShipmentFulfillmentRequest;
  receipt: Receipt;
  customer: Customer;
  shipmentConfirmOrder: Order,

  recentlyCreatedProductID: string;
  selectedVariants: Variant[];

  lastWeight: number;
}

const initialState = {
  order: new Order({}),
  fulfillment: null,
  receipt: null,
  customer: null,
  shipmentConfirmOrder: null,

  recentlyCreatedProductID: null,
  selectedVariants: [],

  lastWeight: null
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'pos', resettable: true })
export class PosStore extends EntityStore<StatePos> {
  constructor() {
    super(initialState);
  }
}
