import {Injectable} from '@angular/core';
import {RelationshipStore} from './relationship.store';
import {AuthorizationAPI, AuthorizationApi} from "@etop/api/shop/authorization.api";
import {ExtensionQuery} from "../shop/extension/extension.query";
import {CursorPaging, RelationshipTab} from "@etop/models";
import {RelationshipQuery} from "@etop/state/relationship/relationship.query";


@Injectable({
  providedIn: 'root'
})
export class RelationshipService {
  constructor(
    private authorizationApi: AuthorizationApi,
    private extensionQuery: ExtensionQuery,
    private relationshipStore: RelationshipStore,
    private relationshipQuery: RelationshipQuery,
  ) {
  }

  async getRelationships(doLoading = true) {
    if (doLoading) {
      this.relationshipStore.setLoading(true);
    }
    try {
      const { filter,paging } = this.relationshipQuery.currentUi;
      let res = await this.authorizationApi.getAccountUsers({filter,paging});
      this.setPaging(res.paging);
      let users = res.account_users.map(relationship => {
        return {
          ...relationship,
          id: relationship?.user_id
        }
      }).filter(relationship => !relationship.deleted);
      this.relationshipStore.set(users);
    } catch (e) {
      debug.log('ERROR in getAccountUsers');
      this.relationshipStore.set(null);
    }
    if (doLoading) {
      this.relationshipStore.setLoading(false);
    }
  }

  async createAccountUser(request : AuthorizationAPI.CreateAccountUserRequest) {
    return await this.authorizationApi.createAccountUser(request);
  }

  async getRelationshipsHaveExtension() {
    try {
      const filter = this.relationshipQuery.getValue().ui.filter;
      let res = await this.authorizationApi.getAccountUsers({filter});
      this.relationshipStore.set(res.account_users.map(relationship => {
        return {
          ...relationship,
          id: relationship?.user_id
        }
      }).filter(relationship => !relationship.deleted && this.checkRelationshipExtension(relationship.user_id)));
    } catch (e) {
      debug.log('ERROR in getRelationships');
      this.relationshipStore.set(null);
    }
  }

  checkRelationshipExtension(userId) {
    const check = this.extensionQuery.getExtensionsByUserId(userId);
    return check.length > 0;
  }

  changeHeaderTab(tab: RelationshipTab) {
    const ui = this.relationshipQuery.currentUi;
    this.relationshipStore.update({
      ui: {
        ...ui,
        tab
      }
    });
  }

  setFilter(filter: AuthorizationAPI.RelationshipFilter) {
    const ui = this.relationshipQuery.currentUi;
    this.relationshipStore.update({
      ui: {
        ...ui,
        filter
      }
    })
  }

  setPaging(paging: CursorPaging) {
    const ui = this.relationshipQuery.currentUi;
    this.relationshipStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setCurrentPage(currentPage: number) {
    const ui = this.relationshipQuery.currentUi;
    this.relationshipStore.update({
      ui: {
        ...ui,
        currentPage: currentPage
      }
    });
  }
}
