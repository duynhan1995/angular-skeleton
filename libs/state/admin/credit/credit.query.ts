import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {CreditStore, StateCredit} from 'libs/state/admin/credit/credit.store';
import {Credit} from "libs/models/admin/Credit";

@Injectable({
  providedIn: 'root'
})
export class CreditQuery extends QueryEntity<StateCredit, Credit> {

  constructor(protected store: CreditStore) {
    super(store);
  }
}
