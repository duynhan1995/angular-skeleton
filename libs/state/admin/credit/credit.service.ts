import { Injectable } from '@angular/core';
import {CreditStore} from 'libs/state/admin/credit/credit.store';
import {CreditQuery} from './credit.query';
import {Paging} from "@etop/shared";
import {CreditAPI, CreditApi} from "@etop/api/admin/credit.api";

@Injectable({
    providedIn: 'root'
})
export class CreditService {
  constructor(
    private creditApi: CreditApi,
    private creditStore: CreditStore,
    private creditQuery: CreditQuery
  ) {
  }


  setPaging(paging: Paging) {
    const ui = this.creditQuery.getValue().ui;
    this.creditStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setFilter(filter: CreditAPI.GetCreditsFilters) {
    const ui = this.creditQuery.getValue().ui;
    this.creditStore.update({
      ui: {
        ...ui,
        filter
      }
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.creditQuery.getValue().ui;
    this.creditStore.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  async getCredits(doLoading = true) {
    this.setLastPage(false);
    if (doLoading) {
      this.creditStore.setLoading(true);
    }
    try {
      const paging = this.creditQuery.getValue().ui.paging;
      const filter = this.creditQuery.getValue().ui.filter;
      const credits = await this.creditApi.getCredits({paging, filter});
      if (paging.offset > 0 && credits.length == 0) {
        this.setLastPage(true);
        this.setPaging({
          ...paging,
          offset: paging.offset - paging.limit
        });
      } else {
        this.creditStore.set(credits);
      }
    } catch(e) {
      debug.error('ERROR in getting Credits', e);
    }
    if (doLoading) {
      this.creditStore.setLoading(false);
    }
  }

}
