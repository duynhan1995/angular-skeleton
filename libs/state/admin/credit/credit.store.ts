import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import { CreditAPI } from '@etop/api';
import { Filter, Filters } from '@etop/models';
import {Paging} from "@etop/shared";
import {Credit} from "libs/models/admin/Credit";

export interface StateCredit extends EntityState<Credit, string>, ActiveState {
  ui: {
    paging: Paging;
    isLastPage: boolean;
    filter: CreditAPI.GetCreditsFilters;
  };
}

const initialState = {
  ui: {
    paging: null,
    isLastPage: false,
    filter: null,
  }
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'credit' })
export class CreditStore extends EntityStore<StateCredit> {
  constructor() {
    super(initialState);
  }
}
