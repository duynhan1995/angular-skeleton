import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {Invoice} from '@etop/models/Invoice';
import {InvoiceStore, StateInvoice} from './invoice.store';

@Injectable({
  providedIn: 'root'
})
export class InvoiceQuery extends QueryEntity<StateInvoice, Invoice> {

  constructor(protected store: InvoiceStore) {
    super(store);
  }

  get currentUi() {
    return this.getValue().ui;
  }

  getRefIds() {
    return this.getAll().map(invoice => invoice.referral_ids[0]);
  }
}
