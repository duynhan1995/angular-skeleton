import {Injectable} from '@angular/core';
import {InvoiceStore} from './invoice.store';
import {InvoiceQuery} from './invoice.query';
import {AdminInvoiceAPI, AdminInvoiceApi} from '@etop/api/admin/admin-invoice.api';
import {CursorPaging, SubscriptionLine} from '@etop/models';
import { SubscriptionService } from '../subscription/subscription.service';
import { SubscriptionQuery } from '../subscription/subscription.query';
import { Invoice, InvoiceTransactionTab } from '@etop/models/Invoice';

@Injectable({
  providedIn: 'root',
})
export class InvoiceService {
  constructor(
    private invoiceApi: AdminInvoiceApi,
    private invoiceStore: InvoiceStore,
    private invoiceQuery: InvoiceQuery,
  ) {
  }

  async getInvoices() {
    this.setLoading(true);
    try {
      const {filter, paging} = this.invoiceQuery.currentUi;
      const res = await this.invoiceApi.getInvoices({filter, paging});
      this.setPaging(res.paging);
      res.invoices = res.invoices.map(invoice => {
        return Invoice.invoiceMap(invoice, [],[]);
      });
      this.invoiceStore.set(res.invoices);
    } catch (e) {
      debug.error('ERROR in getting Invoices', e);
    }
    this.setLoading(false);
  }

  setFilter(filter: AdminInvoiceAPI.GetInvoiceFilter) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        filter,
      },
    });
  }

  setPaging(paging: CursorPaging) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        paging,
      },
    });
  }

  setCurrentPage(currentPage: number) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        currentPage,
      },
    });
  }

  setInvoices(invoices: Invoice[]) {
    this.invoiceStore.set(invoices);
  }

  changeHeaderTab(tab: InvoiceTransactionTab) {
    this.invoiceStore.update({
      ui: {
        ...this.invoiceQuery.currentUi,
        tab,
      },
    });
  }

  setLoading(loading: boolean) {
    this.invoiceStore.setLoading(loading);
  }

  setFilterAccountId(accountId: string) {
    this.setFilter({account_id: accountId});
  }
}
