import {Injectable} from '@angular/core';
import {ActiveState, EntityState, EntityStore, StoreConfig} from '@datorama/akita';
import { AdminInvoiceAPI } from '@etop/api/admin/admin-invoice.api';
import {CursorPaging} from '@etop/models';
import { Invoice, InvoiceTransactionTab } from '@etop/models/Invoice';

export interface StateInvoice extends EntityState<Invoice, string>, ActiveState {
  ui: {
    paging: CursorPaging;
    filter: AdminInvoiceAPI.GetInvoiceFilter;
    currentPage: number;
    tab: InvoiceTransactionTab;
  };
}

const initialState = {
  ui: {
    paging: null,
    filter: null,
    currentPage: 1,
    tab: InvoiceTransactionTab.invoice
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'shopInvoice', resettable: true})
export class InvoiceStore extends EntityStore<StateInvoice> {
  constructor() {
    super(initialState);
  }
}
