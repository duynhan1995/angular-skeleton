import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {
  ShipmentPriceListPromotionStore,
  StateShipmentPriceListPromotion
} from "@etop/state/admin/shipment-price-list-promotion/shipment-price-list-promotion.store";
import {ShipmentPriceListPromotion} from "libs/models/admin/ShipmentPrice";

@Injectable({
  providedIn: 'root'
})
export class ShipmentPriceListPromotionQuery extends QueryEntity<StateShipmentPriceListPromotion, ShipmentPriceListPromotion> {

  constructor(protected store: ShipmentPriceListPromotionStore) {
    super(store);
  }
}
