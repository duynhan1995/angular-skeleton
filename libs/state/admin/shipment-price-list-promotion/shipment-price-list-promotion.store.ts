import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {Paging} from "@etop/shared";
import {ShipmentPriceList, ShipmentPriceListPromotion} from "libs/models/admin/ShipmentPrice";
import {Connection} from "libs/models/Connection";

export interface ConnectionShipmentPriceList {
  id: string;
  connection: Connection;
  shipmentPriceLists: ShipmentPriceList[];
  highestPriorityPoint: number;
}

export interface StateShipmentPriceListPromotion extends EntityState<ShipmentPriceListPromotion, string>, ActiveState {
  ui: {
    paging: Paging;
    connection_id: string;
    isLastPage: boolean;
  };
  highestPriorityPoint: 0,
  connectionShipmentPriceLists: ConnectionShipmentPriceList[];
  activeConnectionShipmentPriceList: ConnectionShipmentPriceList;
}

const initialState = {
  ui: {
    paging: null,
    connection_id: null,
    isLastPage: false
  },
  connectionShipmentPriceLists: [],
  activeConnection: null
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shipmentPriceListPromotion' })
export class ShipmentPriceListPromotionStore extends EntityStore<StateShipmentPriceListPromotion> {
  constructor() {
    super(initialState);
  }
}
