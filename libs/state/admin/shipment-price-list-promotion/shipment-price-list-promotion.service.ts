import {Injectable} from '@angular/core';
import {Paging} from "@etop/shared";
import {ShipmentPriceListPromotionQuery} from "./shipment-price-list-promotion.query";
import {ConnectionShipmentPriceList, ShipmentPriceListPromotionStore} from "./shipment-price-list-promotion.store";
import {AdminConnectionApi, AdminLocationApi, ShipmentPriceAPI, ShipmentPriceApi} from "@etop/api";
import {ShipmentPriceList, ShipmentPriceListPromotion} from "libs/models/admin/ShipmentPrice";
import {ShipmentPriceListStore} from "@etop/state/admin/shipment-price-list";
import {CustomRegionStore} from "@etop/state/admin/custom-region";
import {Connection} from "libs/models/Connection";

@Injectable({
    providedIn: 'root'
})
export class ShipmentPriceListPromotionService {
  constructor(
    private query: ShipmentPriceListPromotionQuery,
    private store: ShipmentPriceListPromotionStore,
    private splStore: ShipmentPriceListStore,
    private customRegionStore: CustomRegionStore,
    private shipmentPriceApi: ShipmentPriceApi,
    private connectionApi: AdminConnectionApi,
    private customRegionApi: AdminLocationApi
  ) {
  }

  private static reMapPromotion(
    promotion: ShipmentPriceListPromotion,
    connectionsGroups: ConnectionShipmentPriceList[]): ShipmentPriceListPromotion
  {
    const group = connectionsGroups.find(gr => gr.id == promotion.connection_id);
    const shipmentPriceList = group?.shipmentPriceLists.find(spl => spl.id == promotion.shipment_price_list_id);

    promotion = {
      ...promotion,
      connection: group?.connection,
      shipment_price_list: shipmentPriceList
    };
    return promotion;
  }

  private static reMapShipmentPriceList(spl: ShipmentPriceList, connections: Connection[]): ShipmentPriceList {
    return {
      ...spl,
      connection: connections.find(conn => conn.id == spl.connection_id)
    };
  }

  setConnectionId(connection_id: string) {
    const ui = this.query.getValue().ui;
    this.store.update({
      ui: {
        ...ui,
        connection_id
      }
    });
  }

  setPaging(paging: Paging) {
    const ui = this.query.getValue().ui;
    this.store.update({
      ui: {
        ...ui,
        paging: {
          ...paging,
          sort: '-priority_point'
        }
      }
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.query.getValue().ui;
    this.store.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  async prepareData() {
    try {
      const crs = await this.customRegionApi.getCustomRegions();
      this.customRegionStore.set(crs);

      const connections = await this.connectionApi.getConnections('builtin');
      const groups: ConnectionShipmentPriceList[] = connections.map(conn => {
        return {
          id: conn.id,
          connection: conn
        }
      });
      const promises = groups.map(gr => async() => {
        try {
          gr.shipmentPriceLists = await this.shipmentPriceApi.getShipmentPriceLists({connection_id: gr.id});
        } catch(e) {
          debug.error('ERROR in getting shipmentPriceLists Promise', e);
        }
      });
      await Promise.all(promises.map(p => p()));

      this.store.update({connectionShipmentPriceLists: groups});

      const spls: ShipmentPriceList[] = [].concat(...groups.map(gr => gr.shipmentPriceLists));
      this.splStore.set(spls
        .map(spl => ShipmentPriceListPromotionService.reMapShipmentPriceList(spl, connections))
      );

    } catch(e) {
      debug.error('ERROR in groupShipmentPriceListsByConnection', e);
    }
  }

  async getShipmentPriceListPromotions(doLoading = true) {
    this.setLastPage(false);
    if (doLoading) {
      this.store.setLoading(true);
    }
    try {
      const {connection_id, paging} = this.query.getValue().ui;
      const shipment_price_list_promotions = await this.shipmentPriceApi.getShipmentPriceListPromotions({connection_id, paging});
      if (paging.offset > 0 && shipment_price_list_promotions.length == 0) {
        this.setLastPage(true);
        this.setPaging({
          ...paging,
          offset: paging.offset - paging.limit
        });
      } else {
        const groups = this.query.getValue().connectionShipmentPriceLists;
        this.store.set(shipment_price_list_promotions
          .map(promo => ShipmentPriceListPromotionService.reMapPromotion(promo, groups))
        );
        if (doLoading) {
          this.store.setActive(null);
        }
        if (paging.offset == 0) {
          this.store.update({highestPriorityPoint: shipment_price_list_promotions[0]?.priority_point});
        }
      }
    } catch(e) {
      debug.error('ERROR in getting ShipmentPriceListPromotions', e);
    }
    if (doLoading) {
      this.store.setLoading(false);
    }
  }

  selectShipmentPriceListPromotion(id: string) {
    this.store.setActive(id);
    if (id) {
      const promo = this.query.getActive();
      const group = this.query.getValue().connectionShipmentPriceLists.find(gr => gr.id == promo.connection_id);
      this.selectConnectionShipmentPriceList(group);
    }
  }

  selectConnectionShipmentPriceList(group: ConnectionShipmentPriceList) {
    this.store.update({activeConnectionShipmentPriceList: group});
  }

  async createShipmentPriceListPromotion(request: ShipmentPriceAPI.CreateShipmentPriceListPromotionRequest) {
    try {
      await this.shipmentPriceApi.createShipmentPriceListPromotion(request);
      toastr.success('Tạo bảng giá khuyến mãi thành công.');
      this.setPaging({
        ...this.query.getValue().ui.paging,
        offset: 0
      });
      this.getShipmentPriceListPromotions().then();
    } catch(e) {
      debug.error('ERROR in createShipmentPriceListPromotion', e);
      toastr.error('Tạo bảng giá khuyến mãi không thành công.', e.code && (e.message || e.msg));
    }
  }

  async updateShipmentPriceListPromotion(request: ShipmentPriceAPI.UpdateShipmentPriceListPromotionRequest) {
    try {
      await this.shipmentPriceApi.updateShipmentPriceListPromotion(request);
      toastr.success('Cập nhật bảng giá khuyến mãi thành công.');
      this.getShipmentPriceListPromotions(false).then(_ => {
        this.selectShipmentPriceListPromotion(request.id);
      });
    } catch(e) {
      debug.error('ERROR in updateShipmentPriceListPromotion', e);
      toastr.error('Cập nhật bảng giá khuyến mãi không thành công.', e.code && (e.message || e.msg));
    }
  }

  async updateShipmentPriceListPromotionStatus(id: string, status: string) {
    try {
      await this.shipmentPriceApi.updateShipmentPriceListPromotionStatus(id, status);
      toastr.success('Cập nhật trạng thái bảng giá khuyến mãi thành công.');
      this.getShipmentPriceListPromotions(false).then(_ => {
        this.selectShipmentPriceListPromotion(id);
      });
    } catch(e) {
      debug.error('ERROR in updateShipmentPriceListPromotionStatus', e);
      toastr.error('Cập nhật trạng thái bảng giá khuyến mãi không thành công.', e.code && (e.message || e.msg));
      throw e;
    }
  }

  async prioritizeShipmentPriceListPromotion(id: string) {
    try {
      await this.shipmentPriceApi.prioritizeShipmentPriceListPromotion(
        id, this.query.getValue().highestPriorityPoint + 1
      );
      toastr.success('Ưu tiên bảng giá khuyến mãi thành công.');
      this.setPaging({
        ...this.query.getValue().ui.paging,
        offset: 0
      });
      this.getShipmentPriceListPromotions().then();
    } catch(e) {
      debug.error('ERROR in prioritizeShipmentPriceListPromotion', e);
      toastr.error('Ưu tiên bảng giá khuyến mãi không thành công.', e.code && (e.message || e.msg));
      throw e;
    }
  }

}
