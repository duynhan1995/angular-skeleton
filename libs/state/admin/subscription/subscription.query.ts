import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Subscription } from '@etop/models/Subscription';
import { StateSubscription, SubscriptionStore } from '@etop/state/admin/subscription/subscription.store';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionQuery extends QueryEntity<StateSubscription, Subscription> {
  constructor(protected store: SubscriptionStore) {
    super(store);
  }
}
