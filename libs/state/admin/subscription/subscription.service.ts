import { Injectable } from '@angular/core';
import { AdminShopApi, AdminSubcriptionAPI, AdminSubscriptionApi } from '@etop/api';
import { SubscriptionStore } from '@etop/state/admin/subscription/subscription.store';
import { SubscriptionQuery } from '@etop/state/admin/subscription/subscription.query';
import { Filters, Shop } from '@etop/models';
import { Paging } from '@etop/shared';
import { Subscription, SubscriptionBill, SubscriptionPlan } from '@etop/models/Subscription';
import misc from '../../../../apps/core/src/libs/misc';
import CreateSubscriptionRequest = AdminSubcriptionAPI.CreateSubscriptionRequest;
import CreateSubscriptionBillRequest = AdminSubcriptionAPI.CreateSubscriptionBillRequest;

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  constructor(
    private subscriptionApi: AdminSubscriptionApi,
    private subscriptionQuery: SubscriptionQuery,
    private subscriptionStore: SubscriptionStore,
    private shopApi: AdminShopApi
  ) {
  }

  setFilters(filters: Filters) {
    const ui = this.subscriptionQuery.getValue().ui;
    this.subscriptionStore.update({
      ui: {
        ...ui,
        filters
      }
    });
  }

  setPaging(paging: Paging) {
    const ui = this.subscriptionQuery.getValue().ui;
    this.subscriptionStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.subscriptionQuery.getValue().ui;
    this.subscriptionStore.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  setEmptyResultFilter(isEmptyFilterResult: boolean) {
    const ui = this.subscriptionQuery.getValue().ui;
    this.subscriptionStore.update({
      ui: {
        ...ui,
        isEmptyFilterResult
      }
    });
  }

  resetFilter() {
    this.setFilters(null)
  }

  setBillFilters(filters: Filters) {
    const subscriptionBillsUi = this.subscriptionQuery.getValue().subscriptionBillsUi;
    this.subscriptionStore.update({
      subscriptionBillUi: {
        ...subscriptionBillsUi,
        filters
      }
    });
  }

  setBillPaging(paging: Paging) {
    const subscriptionBillsUi = this.subscriptionQuery.getValue().subscriptionBillUi;
    this.subscriptionStore.update({
      subscriptionBillUi: {
        ...subscriptionBillsUi,
        paging
      }
    });
  }

  setBillLastPage(isLastPage: boolean) {
    const subscriptionBillsUi = this.subscriptionQuery.getValue().subscriptionBillUi;
    this.subscriptionStore.update({
      subscriptionBillUi: {
        ...subscriptionBillsUi,
        isLastPage
      }
    });
  }

  setBillEmptyResultFilter(isEmptyFilterResult: boolean) {
    const subscriptionBillsUi = this.subscriptionQuery.getValue().subscriptionBillUi;
    this.subscriptionStore.update({
      subscriptionBillUi: {
        ...subscriptionBillsUi,
        isEmptyFilterResult
      }
    });
  }

  resetBillFilter() {
    this.setBillFilters(null)
  }

  selectSubscription(subscription: Subscription) {
    this.subscriptionStore.setActive(subscription ? subscription.id : null);
  }

  async getSubscriptions(doLoading = true) {
    if (doLoading) {
      this.subscriptionStore.setLoading(true);
    }
    try {
      const {filters, paging, accountId} = this.subscriptionQuery.getValue().ui;
      const getSubscriptionsRequest: AdminSubcriptionAPI.GetSubscriptionsRequest = {
          filters,
          paging,
          account_id: accountId
      }
      const subscriptions = await this.subscriptionApi.getSubscriptions(getSubscriptionsRequest);
      if (subscriptions.length == 0) {
        if (paging.offset > 0) {
          this.setLastPage(true);
          this.setPaging({
            ...paging,
            offset: paging.offset - paging.limit
          });
        } else if (paging.offset == 0 && (filters?.length)) {
          this.setEmptyResultFilter(true);
          this.subscriptionStore.set([]);
        }
      } else {
        this.subscriptionStore.set(subscriptions);
        if (doLoading) {
          this.subscriptionStore.setActive(null);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Subscriptions', e);
    }
    if (doLoading) {
      this.subscriptionStore.setLoading(false);
    }
  }

  async getSubscriptionBills(doLoading = true) {
    if (doLoading) {
      this.subscriptionStore.setLoading(true);
    }
    try {
      const {filters, paging, accountId} = this.subscriptionQuery.getValue().subscriptionBillUi;
      const getSubscriptionBillsRequest: AdminSubcriptionAPI.GetSubscriptionBillsRequest = {
        filters,
        paging,
        account_id: accountId
      }

      let subscriptionBills = await this.subscriptionApi.getSubscriptionBills(getSubscriptionBillsRequest);

      const accountIds = misc.uniqueArray(subscriptionBills.map(bill => bill.account_id));
      const shops = await this.shopApi.getShopsByIDs(accountIds);

      subscriptionBills = subscriptionBills.map(bill => this.mapSubscriptionBill(bill, shops));

      if (subscriptionBills.length == 0) {
        if (paging.offset > 0) {
          this.setBillLastPage(true);
          this.setBillPaging({
            ...paging,
            offset: paging.offset - paging.limit
          });
        } else if (paging.offset == 0 && (filters?.length)) {
          this.setBillEmptyResultFilter(true);
          this.subscriptionStore.update({subscriptionBills: []});
        }
      } else {
        this.subscriptionStore.update({ subscriptionBills: subscriptionBills });
      }
    } catch(e) {
      debug.error('ERROR in getting Subscription Bills', e);
    }
    if (doLoading) {
      this.subscriptionStore.setLoading(false);
    }
  }

  async getSubscriptionProducts(doLoading = true) {
    if (doLoading) {
      this.subscriptionStore.setLoading(true);
    }
    try {
      const subscriptionProducts = await this.subscriptionApi.getSubscriptionProducts();
      this.subscriptionStore.update({subscriptionProducts: subscriptionProducts});
    } catch (e) {
      debug.error('ERROR in getting Subscription Products', e);
    }
    if (doLoading) {
      this.subscriptionStore.setLoading(false);
    }
  }

  async getSubscriptionPlans(doLoading = true) {
    if (doLoading) {
      this.subscriptionStore.setLoading(true);
    }
    try {
      let subscriptionPlans = await this.subscriptionApi.getSubscriptionPlans();
      subscriptionPlans = subscriptionPlans.map(plan => this.mapProductToSubcriptionPlan(plan));
      this.subscriptionStore.update({ subscriptionPlans});
    } catch (e) {
      debug.error('ERROR in getting Subscription Plans', e);
    }
    if (doLoading) {
      this.subscriptionStore.setLoading(false);
    }
  }

  mapProductToSubcriptionPlan(subscriptionPlan: SubscriptionPlan) {
    const subscriptionProducts = this.subscriptionQuery.getValue().subscriptionProducts;
    subscriptionPlan.product = subscriptionProducts.find(prod => prod.id == subscriptionPlan.product_id)
    return subscriptionPlan;
  }

  mapSubscriptionBill(subscriptionBill: SubscriptionBill, shops?: Shop[]): SubscriptionBill {
    const subscriptions = this.subscriptionQuery.getAll();
    const subscriptionPlans = this.subscriptionQuery.getValue().subscriptionPlans;

    const billSubscription = subscriptions.find(sub => sub.id == subscriptionBill.subscription_id);
    let billPlan = subscriptionPlans.find(plan => plan.id == (billSubscription?.lines && billSubscription?.lines[0].plan_id));

    subscriptionBill.subscription = billSubscription;
    subscriptionBill.plan = billPlan;

    if (shops?.length) {
      subscriptionBill.shop = shops.find(shop => shop.id == subscriptionBill.account_id);
    }
    return subscriptionBill;
  }

  async createSubscription(createSubscriptionReq: CreateSubscriptionRequest) {
    try {
      return await this.subscriptionApi.createSubscription(createSubscriptionReq);
    } catch(e) {
      debug.error('ERROR in creating Subscription', e);
      throw e;
    }
  }

  async createSubscriptionBill(createSubscriptionBillReq: CreateSubscriptionBillRequest) {
    try {
      return await this.subscriptionApi.createSubscriptionBill(createSubscriptionBillReq);
    } catch(e) {
      debug.error('ERROR in creating Subscription', e);
      throw e;
    }
  }
}
