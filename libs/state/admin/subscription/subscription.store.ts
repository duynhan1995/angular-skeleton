import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {Paging} from "@etop/shared";
import { Filters, Query } from '@etop/models';
import { Subscription, SubscriptionBill, SubscriptionPlan, SubscriptionProduct } from '@etop/models/Subscription';


export interface StateSubscription extends EntityState<Subscription, string>, ActiveState {
  ui: {
    paging: Paging;
    filters: Filters;

    accountId: string;
    isLastPage: boolean;
    isEmptyFilterResult: boolean;
  };

  subscriptionBillUi: {
    paging: Paging;
    filters: Filters;
    accountId: string;
    isLastPage: boolean;
    isEmptyFilterResult: boolean;
  };

  subscriptionProducts: SubscriptionProduct[];
  subscriptionBills: SubscriptionBill[];
  subscriptionPlans: SubscriptionPlan[];

}

const initialState = {
  ui: {
    paging: null,
    filters: null,
    accountId: null,
    isLastPage: false,
    isEmptyFilterResult: false
  },
  subscriptionBillUi: {
    paging: null,
    filters: null,
    accountId: null,
    isLastPage: false,
    isEmptyFilterResult: false
  },
  subscriptionProducts: [],
  subscriptionBills: [],
  subscriptionPlans: [],
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'subscription' })
export class SubscriptionStore extends EntityStore<StateSubscription> {
  constructor() {
    super(initialState);
  }
}
