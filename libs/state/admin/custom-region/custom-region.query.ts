import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {CustomRegionStore, StateCustomRegion} from "./custom-region.store";
import {CustomRegion} from "libs/models/Location";

@Injectable({
  providedIn: 'root'
})
export class CustomRegionQuery extends QueryEntity<StateCustomRegion, CustomRegion> {

  constructor(protected store: CustomRegionStore) {
    super(store);
  }
}
