import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {CustomRegion} from "libs/models/Location";

export interface StateCustomRegion extends EntityState<CustomRegion, string>, ActiveState {}

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'customRegion' })
export class CustomRegionStore extends EntityStore<StateCustomRegion> {
  constructor() {
    super();
  }
}
