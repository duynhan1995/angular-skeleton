import {Injectable} from '@angular/core';
import {CustomRegionQuery} from "./custom-region.query";
import {CustomRegionStore} from "./custom-region.store";

@Injectable({
    providedIn: 'root'
})
export class CustomRegionService {
  constructor(
    private query: CustomRegionQuery,
    private store: CustomRegionStore
  ) {
  }

}
