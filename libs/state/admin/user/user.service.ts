import {Injectable} from '@angular/core';
import {Paging} from "@etop/shared";
import {UserQuery} from "./user.query";
import {UserStore} from "./user.store";
import {AdminShopApi, AdminUserAPI, AdminUserApi, EXTENSION_CHARGE_TYPE} from "@etop/api";
import {CursorPaging, Filters, Shop, User} from 'libs/models';
import {requireMethodPermissions} from "@etop/core";
import GetUserFilters = AdminUserAPI.GetUserFilters;

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private userQuery: UserQuery,
    private userStore: UserStore,
    private userApi: AdminUserApi,
    private shopApi: AdminShopApi,
  ) {
  }

  setFilters(filters: Filters) {
    const _filters: GetUserFilters = {};

    filters.forEach(f => {
      if (f.name == 'name') {
        _filters.name = f.value;
      }
      if (f.name == 'phone') {
        _filters.phone = f.value;
      }
      if (f.name == 'email') {
        _filters.email = f.value;
      }
      if (f.name == 'created_from') {
        _filters.created_at = {
          ..._filters.created_at,
          from: new Date(new Date(f.value).setHours(0,0,0,0)).toISOString()
        };
      }
      if (f.name == 'created_to') {
        _filters.created_at = {
          ..._filters.created_at,
          to: new Date(new Date(f.value).setHours(23,59,59,999)).toISOString()
        };
      }
    });

    const ui = this.userQuery.getValue().ui;
    this.userStore.update({
      ui: {
        ...ui,
        filters: _filters
      }
    });
  }

  setPaging(paging: Paging) {
    const ui = this.userQuery.getValue().ui;
    this.userStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.userQuery.getValue().ui;
    this.userStore.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  @requireMethodPermissions({
    skipError: true,
    valueOnError: [],
    permissions: ['admin/user:view']
  })
  async getUsers(doLoading = true) {
    this.setLastPage(false);
    if (doLoading) {
      this.userStore.setLoading(true);
    }
    try {
      const {filters, paging} = this.userQuery.getValue().ui;
      const users = await this.userApi.getUsers({filters, paging});
      if (paging.offset > 0 && users.length == 0) {
        this.setLastPage(true);
        this.setPaging({
          ...paging,
          offset: paging.offset - paging.limit
        });
      } else {
        this.userStore.set(users);
        if (doLoading) {
          this.userStore.setActive(null);
        }
      }
    } catch (e) {
      debug.error('ERROR in getting Users', e);
      this.userStore.set([]);
    }
    if (doLoading) {
      this.userStore.setLoading(false);
    }
  }

  async getUser(id: string){
    this.userStore.setLoading(true);

    try {
      const user = await this.userApi.getUser(id);
      this.userStore.set([user]);
      this.userStore.setActive(user.id);
    } catch (error) {
      debug.error('ERROR in getting Users', error);
      this.userStore.set([]);
    }

    this.userStore.setLoading(false);
  }

  async getUsersFilter(searchText: string) {
    let users: User[];
    let query: AdminUserAPI.GetUsersRequest = {
      paging: { offset: 0, limit: 1000 },
      filters: {
        name: searchText,
      }
    };
    users = await this.userApi.getUsers(query);
    if(!users.length) {
      query.filters = {phone: searchText};
      users = await this.userApi.getUsers(query);
    }
    return users;
  }

  async getUsersList(filters: AdminUserAPI.GetUserFilters) {
    let users: User[];
    let query: AdminUserAPI.GetUsersRequest = {
      paging:{ offset: 0, limit: 1000 },
      filters
    };
    users = await this.userApi.getUsers(query);
   
    return users;
  }

  async getUserShopList(user: User) {
    let shopList: Shop[];
    let filter = {owner_id: user.id};
    try {
      shopList = await this.shopApi.getShops({filter, paging: null});
    } catch (e) {
      shopList = [];
      debug.error('ERROR in getting Shops', e);
    }
    this.userStore.update({userShopList: shopList});
  }

  updateUserRef(request: AdminUserAPI.UpdateUserRef) {
    return this.userApi.updateUserRef(request).then(_ => {
      this.userStore.updateActive({
        ...this.userQuery.getActive(),
        ref_aff: request.ref_aff,
        ref_sale: request.ref_sale
      })
    });
  }

  selectUser(user: User) {
    this.userStore.setActive(user ? user.id : null);
  }

  updateActive(user: User) {
    this.userStore.updateActive(user);
  }

  async getUsersByIDs(ids: string[]) : Promise<User[]> {
    return await this.userApi.getUsersByIDs(ids);
  }

  async getUserSettings(request: AdminUserAPI.GetUserSettingsRequest) {
    return await this.userApi.getUserSettings(request);
  }

  async updateUserSetting(extension_charge_type: EXTENSION_CHARGE_TYPE, user_id: string) {
    return await this.userApi.updateUserSetting(extension_charge_type, user_id);
  }
}
