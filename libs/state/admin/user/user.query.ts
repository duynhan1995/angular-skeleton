import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {StateUser, UserStore} from "@etop/state/admin/user/user.store";
import {User} from "libs/models";

@Injectable({
  providedIn: 'root'
})
export class UserQuery extends QueryEntity<StateUser, User> {

  constructor(protected store: UserStore) {
    super(store);
  }

  getUserById(id: string) {
    return this.getAll().find(user => user.id === id);
  }
}
