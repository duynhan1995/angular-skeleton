import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {Shop, User} from "libs/models";
import {Paging} from "@etop/shared";
import {AdminUserAPI} from "@etop/api";
import GetUserFilters = AdminUserAPI.GetUserFilters;

export interface StateUser extends EntityState<User, string>, ActiveState {
  ui: {
    paging: Paging;
    filters: GetUserFilters;
    isLastPage: boolean;
  };
  userShopList: Shop[];
}

const initialState = {
  ui: {
    paging: null,
    filters: null,
    isLastPage: false
  },
  userShopList: [],
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'user' })
export class UserStore extends EntityStore<StateUser> {
  constructor() {
    super(initialState);
  }
}
