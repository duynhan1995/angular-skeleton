import {Injectable} from '@angular/core';
import {EntityState, StoreConfig, EntityStore} from '@datorama/akita';
import {TicketLabel} from "@etop/models";

export interface StateTicketLabel extends EntityState<TicketLabel, string> {
  ticketProductLabels: TicketLabel[];
  activeProductLabel: TicketLabel;

  ticketSubjectLabels: TicketLabel[];
  activeSubjectLabel: TicketLabel;

  ticketDetailLabels: TicketLabel[];
}

const initialState = {
  ticketProductLabels: [],
  activeProductLabel: null,

  ticketSubjectLabels: [],
  activeSubjectLabel: null,

  ticketDetailLabels: [],
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'adminTicketLabel'})
export class TicketLabelStore extends EntityStore<StateTicketLabel> {
  constructor() {
    super(initialState);
  }
}
