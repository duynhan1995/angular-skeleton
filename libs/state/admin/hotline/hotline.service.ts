import { Injectable } from '@angular/core';
import { HotlinesAPI, HotlinesApi } from '@etop/api/admin/admin-hotline.api';
import { AuthenticateStore } from '@etop/core';
import { CursorPaging } from '@etop/models/CursorPaging';
import { Hotline } from '@etop/models';
import { HotlineQuery } from '../hotline/hotline.query';
import { HotlineStore } from '../hotline/hotline.store';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root',
})
export class HotlineService {
  constructor(
    private hotlineApi: HotlinesApi,
    private hotlineStore: HotlineStore,
    private hotlineQuery: HotlineQuery,
    private auth: AuthenticateStore,
    private userService: UserService
  ) {}

  setPaging(paging: CursorPaging) {
    const ui = this.hotlineQuery.currentUi;
    this.hotlineStore.update({
      ui: {
        ...ui,
        paging,
      },
    });
  }

  setFilter(filter: HotlinesAPI.GetHotlinesFilter) {
    const ui = this.hotlineQuery.currentUi;
    this.hotlineStore.update({
      ui: {
        ...ui,
        filter,
      },
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.hotlineQuery.getValue().ui;
    this.hotlineStore.update({
      ui: {
        ...ui,
        isLastPage,
      },
    });
  }

  setActive(hotline: Hotline) {
    this.hotlineStore.setActive(hotline ? hotline?.id : null);
  }

  setHotlines(hotlines: Hotline[]) {
    this.hotlineStore.set(hotlines);
  }

  async getHotlines(doLoading = true) {
    this.setLastPage(false);
    if (doLoading) {
      this.hotlineStore.setLoading(true);
    }
    try {
      const paging = this.hotlineQuery.paging;
      const filter = this.hotlineQuery.filter;
      const request: HotlinesAPI.GetHotlinesRequest = {
        paging,
        filter,
      };
      const res = await this.hotlineApi.getHotlines(request);
      const listId: string[] = res?.hotlines.map((tenant) => tenant.owner_id);
      let hotlines = [];
      if (listId.length > 0) {
        const listUser = await this.userService.getUsersByIDs(listId);
        hotlines = res.hotlines.map((hotline) => {
          hotline.user = listUser.find((user) => user.id == hotline.owner_id);
          return hotline;
        });
      }
      this.setHotlines(hotlines);
      this.setPaging(res?.paging);
    } catch (e) {
      debug.error('ERROR in getting Hotlines', e);
    }
    if (doLoading) {
      this.hotlineStore.setLoading(false);
    }
  }

  async createHotline(hotline: Hotline): Promise<Hotline> {
    try {
      let request: HotlinesAPI.CreateHotlineRequest = {
        hotline: hotline.hotline,
        is_free_charge: hotline.is_free_charge,
        name: hotline.name,
        owner_id: hotline.owner_id,
        description: hotline.description,
        network: hotline.network,
      };

      const res = await this.hotlineApi.createHotline(request);
      return res;
    } catch (e) {
      debug.error('ERROR in createHotline', e);
      throw e;
    }
  }

  async updateHotline(hotline: Hotline) {
    try {
      let request: HotlinesAPI.UpdateHotlinelineRequest = {
        id: hotline.id,
        is_free_charge: hotline.is_free_charge,
        name: hotline.name,
        network: hotline.network,
        status: hotline.status,
        description: hotline.description,
      };
      await this.hotlineApi.updateHotline(request);
    } catch (e) {
      debug.error('ERROR in UpdateHotline', e);
      throw e;
    }
  }

  async deleteHotline(id: string) {
    try {
      await this.hotlineApi.deleteHotline(id);
    } catch (e) {
      debug.error('ERROR in deleteHotline', e);
      throw e;
    }
  }

  async addHotlineToTenant(hotline_id: string, tenant_id: string) {
    try {
      await this.hotlineApi.addHotlineToTenant(hotline_id, tenant_id);
    } catch (e) {
      debug.error('ERROR in addHotlineToTenant', e);
      throw e;
    }
  }
}
