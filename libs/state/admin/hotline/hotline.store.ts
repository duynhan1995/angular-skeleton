import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import { HotlinesAPI } from '@etop/api/admin/admin-hotline.api';
import { Hotline } from '@etop/models';
import { CursorPaging } from '@etop/models/CursorPaging';

export interface StateHotline extends EntityState<Hotline, string>, ActiveState {
  ui: {
    filter: HotlinesAPI.GetHotlinesFilter,
    paging: CursorPaging;
    isLastPage: boolean
  };
}

const initialState = {
  ui: {
    filter: null,
    paging: null,
    isLastPage: false
  }
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'hotline' })
export class HotlineStore extends EntityStore<StateHotline> {
  constructor() {
    super(initialState);
  }
}
