import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Hotline } from '@etop/models';
import { StateHotline, HotlineStore } from '../hotline/hotline.store';

@Injectable({
  providedIn: 'root'
})
export class HotlineQuery extends QueryEntity<StateHotline, Hotline> {

  constructor(protected store: HotlineStore) {
    super(store);
  }

  get filter() {
    return this.getValue().ui.filter;
  }

  get paging() {
    return this.getValue().ui.paging;
  }

  get currentUi() {
    return this.getValue().ui;
  }
}
