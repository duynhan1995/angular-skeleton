import {Injectable} from '@angular/core';
import {ShipmentPriceListQuery} from "./shipment-price-list.query";
import {ShipmentPriceListStore} from "./shipment-price-list.store";

@Injectable({
    providedIn: 'root'
})
export class ShipmentPriceListService {
  constructor(
    private query: ShipmentPriceListQuery,
    private store: ShipmentPriceListStore
  ) {
  }

  setConnectionId(connection_id: string) {
    const ui = this.query.getValue().ui;
    this.store.update({
      ui: {
        ...ui,
        connection_id
      }
    });
  }

}
