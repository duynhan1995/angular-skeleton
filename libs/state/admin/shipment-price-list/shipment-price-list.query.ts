import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {ShipmentPriceList} from "libs/models/admin/ShipmentPrice";
import {ShipmentPriceListStore, StateShipmentPriceList} from "./shipment-price-list.store";

@Injectable({
  providedIn: 'root'
})
export class ShipmentPriceListQuery extends QueryEntity<StateShipmentPriceList, ShipmentPriceList> {

  constructor(protected store: ShipmentPriceListStore) {
    super(store);
  }
}
