import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {ShipmentPriceList} from "libs/models/admin/ShipmentPrice";

export interface StateShipmentPriceList extends EntityState<ShipmentPriceList, string>, ActiveState {
  ui: {
    connection_id: string;
  };
}

const initialState = {
  ui: {
    connection_id: null
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shipmentPriceList' })
export class ShipmentPriceListStore extends EntityStore<StateShipmentPriceList> {
  constructor() {
    super(initialState);
  }
}
