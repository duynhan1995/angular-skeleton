import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {Paging} from "@etop/shared";
import {AdminUser, Fulfillment, MoneyTransactionShop, Ticket} from "@etop/models";
import {AdminTicketAPI} from "@etop/api/admin/admin-ticket.api";

export interface StateTicket extends EntityState<Ticket, string>, ActiveState {
  ui: {
    paging: Paging;

    filter: AdminTicketAPI.GetTicketsFilter;
    filterProductLabelsIds: string[];
    filterSubjectLabelsIds: string[];
    filterDetailLabelsIds: string[];

    isEmptyFilterResult: boolean;
    isLastPage: boolean;

    finishPrepareData: boolean;

    getTicketRefTypeLoading: boolean;
    getTicketCommentsLoading: boolean;
  };

  //NOTE: Tickets Properties
  adminUsers: AdminUser[];

  activeTicketFulfillment: Fulfillment;
  activeTicketMoneyTransaction: MoneyTransactionShop;

  //NOTE: Utilities
  ticketCommentPreUploadImagesFiles: {
    [key: string]: File;
  };
}

const initialState = {
  ui: {
    paging: null,

    filter: {},
    filterProductLabelsIds: [],
    filterSubjectLabelsIds: [],
    filterDetailLabelsIds: [],

    isEmptyFilterResult: false,
    isLastPage: false,

    finishPrepareData: false,

    getTicketRefTypeLoading: true,
    getTicketCommentsLoading: true,
  },

  adminUsers: [],

  activeTicketFulfillment: null,
  activeTicketMoneyTransaction: null,

  ticketCommentPreUploadImagesFiles: {}
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'adminTicket', resettable: true })
export class TicketStore extends EntityStore<StateTicket> {
  constructor() {
    super(initialState);
  }
}
