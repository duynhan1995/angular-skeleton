import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {Ticket} from "@etop/models";
import {StateTicket, TicketStore} from "@etop/state/admin/ticket/ticket.store";

@Injectable({
  providedIn: 'root'
})
export class TicketQuery extends QueryEntity<StateTicket, Ticket> {

  constructor(protected store: TicketStore) {
    super(store);
  }
}
