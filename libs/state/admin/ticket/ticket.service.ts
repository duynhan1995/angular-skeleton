import {Injectable} from '@angular/core';
import {arrayUpsert} from "@datorama/akita";
import {AuthenticateStore, ConfigService} from "@etop/core";
import {Paging} from "@etop/shared";
import {UtilService} from "apps/core/src/services/util.service";
import {TicketQuery} from "./ticket.query";
import {TicketStore} from "./ticket.store";
import {
  FeeType,
  Filters,
  Fulfillment,
  Ticket, TicketComment,
  TicketRefType,
  TicketState
} from '@etop/models';
import {
  AdminAccountApi,
  AdminFulfillmentApi,
  AdminShopApi,
  AdminTicketAPI,
  AdminTicketApi,
  AdminMoneyTransactionApi,
} from "@etop/api";
import {trimUndefined} from "@etop/utils";
import {ConnectionStore} from "@etop/features";
import {TelegramService} from "@etop/features";
import {TicketLabelQuery, TicketLabelStore} from "@etop/state/shop/ticket-labels";
import {HttpService} from "@etop/common";

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private ticketApi: AdminTicketApi,
    private adminTicketApi: AdminTicketApi,
    private adminAccountApi: AdminAccountApi,
    private adminShopApi: AdminShopApi,
    private adminFulfillmentApi: AdminFulfillmentApi,
    private adminTransactionApi: AdminMoneyTransactionApi,
    private ticketQuery: TicketQuery,
    private ticketStore: TicketStore,
    private connectionStore: ConnectionStore,
    private labelStore: TicketLabelStore,
    private labelQuery: TicketLabelQuery,
    private telegramService: TelegramService,
    private http: HttpService,
    private config: ConfigService
  ) {
  }

  private reMapTicket(ticket: Ticket): Ticket {
    const adminUsers = this.ticketQuery.getValue().adminUsers;
    const {ticketProductLabels, ticketSubjectLabels, ticketDetailLabels} = this.labelQuery.getValue();
    const _productLabels = ticket.label_ids.map(id => ticketProductLabels.find(label => label.id == id))
      .filter(label => !!label);

    let _subjectLabels = ticket.label_ids.map(id => ticketSubjectLabels.find(label => label.id == id))
      .filter(label => !!label);
    if (_subjectLabels?.length) {
      _subjectLabels = _subjectLabels.map(label => ({...label, children: []}));
    }

    const _detailLabels = ticket.label_ids.map(id => ticketDetailLabels.find(label => label.id == id))
      .filter(label => !!label);

    return {
      ...ticket,
      assignedUsers: adminUsers.filter(admin => ticket.assigned_user_ids.includes(admin.user_id)),
      closedByUser: adminUsers.find(admin => admin.user_id == ticket.closed_by),
      confirmedByUser: adminUsers.find(admin => admin.user_id == ticket.confirmed_by),
      createdByUser: ticket.createdByUser || adminUsers.find(admin => admin.user_id == ticket.created_by),
      labels: [..._productLabels, ..._subjectLabels, ..._detailLabels]
    };
  }

  setFilterProductLabels(labelIds: string[]) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        filterProductLabelsIds: labelIds
      }
    });
    this.setFilter({label_ids: labelIds});

    const subjectLabels = this.labelQuery.getValue().ticketSubjectLabels;
    this.labelStore.update({ticketSubjectLabels: subjectLabels});
  }

  setFilterSubjectLabels(labelIds: string[]) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        filterSubjectLabelsIds: labelIds
      }
    });
    if (!labelIds.length) {
      this.setFilter({label_ids: ui.filterProductLabelsIds});
    } else {
      this.setFilter({label_ids: labelIds});
    }

    const detailLabels = this.labelQuery.getValue().ticketDetailLabels;
    this.labelStore.update({ticketDetailLabels: detailLabels});
  }

  setFilterDetailLabels(labelIds: string[]) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        filterDetailLabelsIds: labelIds
      }
    });
    if (!labelIds.length) {
      return this.setFilter({label_ids: ui.filterSubjectLabelsIds});
    }
    this.setFilter({label_ids: labelIds});
  }

  setFilterAccountId(accountId: string) {
    this.setFilter({account_id: accountId});
  }

  setFilters(filters: Filters) {
    this.setFilter({
      assigned_user_id: null,
      code: null,
      state: null
    });


    const filter: AdminTicketAPI.GetTicketsFilter = {};
    filters.forEach(f => {
      if (f.name == 'assigned_user_id' && f.value) {
        filter.assigned_user_id = [f.value];
      }
      if (f.name == 'code') {
        filter.code = f.value;
      }
      if (f.name == 'state') {
        filter.state = f.value;
      }
    });

    this.setFilter(filter);
  }

  setFilter(filter: AdminTicketAPI.GetTicketsFilter) {
    const ui = this.ticketQuery.getValue().ui;
    const _filter = {
      ...ui.filter,
      ...filter
    };

    this.ticketStore.update({
      ui: {
        ...ui,
        filter: trimUndefined(_filter)
      }
    });
  }

  setPaging(paging: Paging) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  setEmptyResultFilter(isEmptyFilterResult: boolean) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        isEmptyFilterResult
      }
    });
  }

  setLoadingTicketRefType(loading: boolean) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        getTicketRefTypeLoading: loading
      }
    });
  }

  setLoadingTicketComments(loading: boolean) {
    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        getTicketCommentsLoading: loading
      }
    });
  }

  async prepareData() {
    await this.getAdminUsers();
    await this.getTicketLabels();

    const ui = this.ticketQuery.getValue().ui;
    this.ticketStore.update({
      ui: {
        ...ui,
        finishPrepareData: true
      }
    });
  }

  async getTickets(doLoading = true) {
    this.setLastPage(false);
    this.setEmptyResultFilter(false);
    if (doLoading) {
      this.ticketStore.setLoading(true);
    }
    try {
      const {filter, paging} = this.ticketQuery.getValue().ui;
      const tickets: Ticket[] = await this.adminTicketApi.getTickets({filter, paging});

      if (tickets.length == 0) {
        if (paging.offset > 0) {
          this.setLastPage(true);
          this.setPaging({
            ...paging,
            offset: paging.offset - paging.limit
          });
        } else if (paging.offset == 0 && (Object.keys(filter)?.length)) {
          this.setEmptyResultFilter(true);
          this.ticketStore.set([]);
        }
      } else {
        const shopIds = tickets.map(ticket => ticket.account_id);
        const shops = await this.adminShopApi.getShops({
          filter: {shop_ids: shopIds},
          paging: {limit: 1000, offset: 0}
        });

        this.ticketStore.set(tickets.map(ticket => {
          ticket.shop = shops.find(s => s.id == ticket.account_id);
          if (ticket.created_by == ticket.shop.owner_id) {
            ticket.createdByUser = ticket.shop.user;
          }
          return this.reMapTicket(ticket);
        }));
        if (doLoading) {
          this.ticketStore.setActive(null);
        }
      }
    } catch (e) {
      debug.error('ERROR in getting Tickets', e);
    }
    if (doLoading) {
      this.ticketStore.setLoading(false);
    }
  }

  async selectTicket(ticket: Ticket) {
    const activeTicket = this.ticketQuery.getActive();
    if (ticket?.id == activeTicket?.id) {
      return;
    }
    this.ticketStore.setActive(ticket ? ticket.id : null);

    if (ticket) {
      try {
        await this.getTicketComments(ticket);

        switch (ticket.ref_type) {
          case TicketRefType.ffm:
            await this.getFulfillmentForTicket(ticket);
            break;
          case TicketRefType.money_transaction:
            await this.getMoneyTransactionForTicket(ticket);
            break;
          default:
            break;
        }
      } catch (e) {
        debug.error('ERROR in getTicket Sub Data', e);
      }

    }
  }

  async getFulfillmentForTicket(ticket: Ticket) {
    this.setLoadingTicketRefType(true);

    try {
      const res = await this.adminFulfillmentApi.getFulfillment(ticket.ref_id);
      const ffm = Fulfillment.fulfillmentMap(res, this.connectionStore.snapshot.initAdminConnections);

      const ffmTickets: Ticket[] = await this.adminTicketApi.getTickets({
        filter: {
          ref_id: ffm.id,
          ref_type: TicketRefType.ffm
        },
        paging: {
          limit: 1000,
          offset: 0
        }
      });
      ffm.tickets = ffmTickets.filter(t => t.id != ticket.id).map(t => this.reMapTicket(t));

      this.ticketStore.update({
        activeTicketFulfillment: Fulfillment.fulfillmentMap(ffm, this.connectionStore.snapshot.initAdminConnections)
      });
    } catch (e) {
      debug.error('ERROR in getFulfillmentForTicket', e);
    }

    this.setLoadingTicketRefType(false);
  }

  async getMoneyTransactionForTicket(ticket: Ticket) {
    this.setLoadingTicketRefType(true);

    try {
      const mt = await this.adminTransactionApi.getMoneyTransactionShop(ticket.ref_id);

      const mtTickets = await this.adminTicketApi.getTickets({
        filter: {
          ref_id: mt.id,
          ref_type: TicketRefType.money_transaction
        },
        paging: {
          limit: 1000,
          offset: 0
        }
      });
      mt.tickets = mtTickets.filter(t => t.id != ticket.id).map(t => this.reMapTicket(t));

      this.ticketStore.update({
        activeTicketMoneyTransaction: mt
      });
    } catch (e) {
      debug.error('ERROR in getMoneyTransactionForTicket', e);
    }

    this.setLoadingTicketRefType(false);
  }

  async getAdminUsers() {
    try {
      const admins = await this.adminAccountApi.getAdminUsers();
      this.ticketStore.update({adminUsers: admins});
    } catch (e) {
      debug.error('ERROR in getting AdminUsers', e);
    }
  }

  findLabelByCode(code: string) {
    return this.labelQuery.getValue().ticketProductLabels.find(label => label.code == code);
  }

  async getTicketLabels() {
    try {
      const labels = await this.ticketApi.getTicketLabels(true);

      this.labelStore.update({ticketProductLabels: labels});

      const ticketProductLabels = this.labelQuery.getValue().ticketProductLabels;
      this.labelStore.update({
        ticketSubjectLabels: ticketProductLabels.reduce((a, b) => {
          return a.concat(b.children.map(child => {
            child.color = child.color || b.color;
            return child;
          }));
        }, [])
      });

      const ticketSubjectLabels = this.labelQuery.getValue().ticketSubjectLabels;
      this.labelStore.update({
        ticketDetailLabels: ticketSubjectLabels.reduce((a, b) => {
          return a.concat(b.children.map(child => {
            child.color = child.color || b.color;
            return child;
          }));
        }, [])
      });
    } catch (e) {
      debug.error('ERROR in getting TicketLabels', e);
    }
  }

  async assignUserToTicket(assignedUserIds: string[], ticketId: string) {
    try {
      await this.adminTicketApi.assignTicket(assignedUserIds, ticketId);
      await this.getTickets(false);
      const _tickets = this.ticketQuery.getAll();
      const _ticket = _tickets.find(ticket => ticket.id == ticketId);
      this.selectTicket(_ticket).then();
    } catch (e) {
      debug.error('ERROR in assignUserToTicket', e);
      throw e;
    }
  }

  async confirmTicket(ticketId: string) {
    try {
      await this.adminTicketApi.confirmTicket(ticketId);
      await this.getTickets(false);
      const _tickets = this.ticketQuery.getAll();
      const _ticket = _tickets.find(ticket => ticket.id == ticketId);
      this.selectTicket(_ticket).then();
    } catch (e) {
      debug.error('ERROR in confirmTicket', e);
      throw e;
    }
  }

  async closeTicket(state: TicketState, ticketId: string) {
    try {
      await this.adminTicketApi.closeTicket(state, ticketId);
      await this.getTickets(false);
      const _tickets = this.ticketQuery.getAll();
      const _ticket = _tickets.find(ticket => ticket.id == ticketId);
      this.selectTicket(_ticket).then();
    } catch (e) {
      debug.error('ERROR in closeTicket', e);
      throw e;
    }
  }

  async addFulfillmentShippingFee(ffm: Fulfillment, feeType: FeeType) {
    await this.adminFulfillmentApi.addShippingFee({
      id: ffm.id,
      shipping_fee_type: feeType
    });
    await this.getFulfillmentForTicket(this.ticketQuery.getActive());
    this.addShippingFeesTelegramMessage(ffm, feeType).then();
  }

  async getTicketComments(ticket: Ticket) {
    this.setLoadingTicketComments(true);

    try {
      const comments: TicketComment[] = await this.adminTicketApi.getTicketComments({
        filter: {ticket_id: ticket.id},
        paging: {offset: 0, limit: 1000}
      });

      this.ticketStore.updateActive({
        comments: comments.reverse().map(cmt => TicketComment.ticketCommentMap(
          this.auth.snapshot.user.id, cmt, this.ticketQuery.getValue().adminUsers, ticket.shop?.name
        ))
      });
    } catch (e) {
      debug.error('ERROR in getTicketComments', e);
    }

    this.setLoadingTicketComments(false);
  }

  async createTicketComment(comment: TicketComment) {
    try {
      comment = TicketComment.ticketCommentMap(
        this.auth.snapshot.user?.id, comment,
        this.ticketQuery.getValue()?.adminUsers, this.ticketQuery.getActive()?.shop?.name
      );
      this.mergeTicketComments([comment]);
      this.updateTicketCommentStatus(comment.id, "sending");

      let body: AdminTicketAPI.CreateTicketCommentRequest = {
        account_id: comment?.account_id,
        ticket_id: this.ticketQuery.getActive()?.id,
      };
      if (comment?.image_url) {
        const currentFilesHash = this.ticketQuery.getValue().ticketCommentPreUploadImagesFiles;
        const file = currentFilesHash[comment?.id];
        const _res = await this.util.uploadImages([file], 1024);
        body.image_url = _res[0]?.url;
      } else {
        body.message = comment?.message;
      }
      const res = await this.adminTicketApi.createTicketComment(body);

      setTimeout(_ => {
        this.updateTicketCommentStatus(comment.id, null);

        comment.pID = res.id;
        this.mergeTicketComments([comment]);
      }, 250);
    } catch(e) {
      debug.error('ERROR in createTicketComment', e);
      this.updateTicketCommentStatus(comment.id, "error");
      throw e;
    }
  }

  async updateTicketComment(comment: TicketComment) {
    try {
      this.updateTicketCommentStatus(comment.id, "sending");

      let body: AdminTicketAPI.UpdateTicketCommentRequest = {
        account_id: comment?.account_id,
        id: comment?.pID,
      };
      if (comment?.image_url) {
        const currentFilesHash = this.ticketQuery.getValue().ticketCommentPreUploadImagesFiles;
        const file = currentFilesHash[comment?.id];
        const _res = await this.util.uploadImages([file], 1024);
        body.image_url = _res[0]?.url;
      } else {
        body.message = comment?.message;
      }
      await this.adminTicketApi.updateTicketComment(body);

      setTimeout(_ => {
        this.updateTicketCommentStatus(comment.id, null);
        this.mergeTicketComments([comment]);
      }, 250);
    } catch(e) {
      debug.error('ERROR in updateTicketComment', e);
      this.updateTicketCommentStatus(comment.id, null);
      throw e;
    }
  }

  updateTicketCommentStatus(commentID: string, status: 'sending' | 'error') {
    let comment = this.ticketQuery.getActive().comments.find(cmt => cmt.id == commentID)
    comment = {
      ...comment,
      sendingStatus: status
    };
    this.mergeTicketComments([comment]);
  }

  mergeTicketComments(comments: TicketComment[]) {
    let _comments = JSON.parse(JSON.stringify(this.ticketQuery.getActive().comments));
    comments.forEach(cmt => _comments = arrayUpsert(_comments, cmt.id, cmt));
    this.ticketStore.updateActive({comments: _comments});
  }

  updateTicketCommentPreUploadImagesFiles(key: string, file: File) {
    const currentFilesHash = this.ticketQuery.getValue().ticketCommentPreUploadImagesFiles;
    this.ticketStore.update({
      ticketCommentPreUploadImagesFiles: {
        ...currentFilesHash,
        [key]: file
      }
    });
  }

  addShippingFeesTelegramMessage(fulfillment: Fulfillment, feeType: FeeType) {
    const connection = this.connectionStore.snapshot.initAdminConnections
      .find(conn => conn.id == fulfillment.connection_id);
    return this.telegramService.addShippingFees(fulfillment, connection, feeType);
  }

  updateShippingFeesTelegramMessage(fulfillment: Fulfillment) {
    const connection = this.connectionStore.snapshot.initAdminConnections
      .find(conn => conn.id == fulfillment.connection_id);
    return this.telegramService.updateFfmShippingFees(fulfillment, connection);
  }

  /**
   * @deprecated since 2.0
   */
  async createVtigerTicket(body: any) {
    return this.http.post(`${this.config.getConfig().crm_url}/api.crm/ticket`, body).toPromise()
      .then(res => {
        return res.data;
      });
  }

}
