import {Injectable} from '@angular/core';
import {ActiveState, EntityState, EntityStore, StoreConfig} from '@datorama/akita';
import { AdminTransactionAPI } from '@etop/api/admin/admin-transaction.api';
import {CursorPaging} from '@etop/models';
import { Transaction } from '@etop/models/Transaction';

export interface StateTransaction extends EntityState<Transaction, string>, ActiveState {
  ui: {
    paging: CursorPaging;
    filter: AdminTransactionAPI.AdminGetTransactionFilter;
    currentPage: number;
  };
}

const initialState = {
  ui: {
    paging: null,
    filter: null,
    currentPage: 1,
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'shopTransaction', resettable: true})
export class TransactionStore extends EntityStore<StateTransaction> {
  constructor() {
    super(initialState);
  }
}
