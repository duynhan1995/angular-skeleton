import {Injectable} from '@angular/core';
import {TransactionStore} from './transaction.store';
import {TransactionQuery} from './transaction.query';
import {Transaction} from '@etop/models/Transaction';
import {CursorPaging} from '@etop/models';
import { AdminTransactionAPI, AdminTransactionApi } from '@etop/api/admin/admin-transaction.api';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  constructor(
    private transactionApi: AdminTransactionApi,
    private transactionStore: TransactionStore,
    private transactionQuery: TransactionQuery
  ) {
  }

  async getTransactions() {
    try {
      this.setLoading(true);
      const {filter, paging} = this.transactionQuery.currentUi;
      const res = await this.transactionApi.getTransactions({filter, paging});
      this.setPaging(res.paging);
      this.transactionStore.set(res.transactions);
      this.setLoading(false);
    } catch (e) {
      this.setLoading(false);
      debug.error('ERROR in getting Transactions', e);
    }
  }

  setFilter(filter: AdminTransactionAPI.AdminGetTransactionFilter) {
    this.transactionStore.update({
      ui: {
        ...this.transactionQuery.currentUi,
        filter,
      },
    });
  }

  setPaging(paging: CursorPaging) {
    this.transactionStore.update({
      ui: {
        ...this.transactionQuery.currentUi,
        paging,
      },
    });
  }

  setCurrentPage(currentPage: number) {
    this.transactionStore.update({
      ui: {
        ...this.transactionQuery.currentUi,
        currentPage,
      },
    });
  }

  setTransactions(transactions: Transaction[]) {
    this.transactionStore.set(transactions);
  }

  setLoading(loading: boolean) {
    this.transactionStore.setLoading(loading);
  }

  setFilterAccountId(accountId: string) {
    this.setFilter({account_id: accountId});
  }
}
