import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {Transaction} from '@etop/models/Transaction';
import {TransactionStore, StateTransaction} from './transaction.store';

@Injectable({
  providedIn: 'root'
})
export class TransactionQuery extends QueryEntity<StateTransaction, Transaction> {

  constructor(protected store: TransactionStore) {
    super(store);
  }

  get currentUi() {
    return this.getValue().ui;
  }
}
