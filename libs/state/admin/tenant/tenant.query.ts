import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Tenant } from '@etop/models/Tenant';
import { StateTenant, TenantStore } from '../tenant/tenant.store';

@Injectable({
  providedIn: 'root'
})
export class TenantQuery extends QueryEntity<StateTenant, Tenant> {

  constructor(protected store: TenantStore) {
    super(store);
  }

  get filter() {
    return this.getValue().ui.filter;
  }

  get paging() {
    return this.getValue().ui.paging;
  }

  get currentPage() {
    return this.getValue().ui.currentPage;
  }

  get currentUi() {
    return this.getValue().ui;
  }
}
