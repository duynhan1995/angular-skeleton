import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import { TenantsAPI } from '@etop/api/admin/admin-tenant.api';
import { CursorPaging } from '@etop/models/CursorPaging';
import { Tenant } from '@etop/models/Tenant';

export interface StateTenant extends EntityState<Tenant, string>, ActiveState {
  ui: {
    filter: TenantsAPI.GetTenantsFilter,
    paging: CursorPaging;
    isLastPage: boolean;
    currentPage: number;
  };
}

const initialState = {
  ui: {
    filter: null,
    paging: null,
    currentPage: 1,
    isLastPage: false,
  }
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'tenant' })
export class TenantStore extends EntityStore<StateTenant> {
  constructor() {
    super(initialState);
  }
}
