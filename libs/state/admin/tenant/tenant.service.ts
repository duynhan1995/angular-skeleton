import { Injectable } from '@angular/core';
import { TenantsAPI, TenantsApi } from '@etop/api/admin/admin-tenant.api';
import { CursorPaging } from '@etop/models/CursorPaging';
import { Hotline } from '@etop/models/Hotline';
import { Tenant } from '@etop/models/Tenant';
import { TenantQuery } from '../tenant/tenant.query';
import { TenantStore } from '../tenant/tenant.store';
import { UserService } from '../user/user.service';
import {TelegramService} from "@etop/features";
import {AdminUserApi,AdminShopApi} from "@etop/api";
import { User } from '@etop/models/User';
import { Shop } from '@etop/models/Account';

@Injectable({
  providedIn: 'root',
})
export class TenantService {
  constructor(
    private tenantApi: TenantsApi,
    private tenantStore: TenantStore,
    private tenantQuery: TenantQuery,
    private userService: UserService,
    private telegramService: TelegramService,
    private adminUserApi: AdminUserApi,
    private adminShopApi: AdminShopApi,
  ) {}

  setPaging(paging: CursorPaging) {
    const ui = this.tenantQuery.currentUi;
    this.tenantStore.update({
      ui: {
        ...ui,
        paging,
      },
    });
  }

  setCurrentPage(currentPage: number) {
    const ui = this.tenantQuery.currentUi;
    this.tenantStore.update({
      ui: {
        ...ui,
        currentPage,
      },
    });
  }

  setFilter(filter: TenantsAPI.GetTenantsFilter) {
    const ui = this.tenantQuery.currentUi;
    this.tenantStore.update({
      ui: {
        ...ui,
        filter,
      },
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.tenantQuery.getValue().ui;
    this.tenantStore.update({
      ui: {
        ...ui,
        isLastPage,
      },
    });
  }

  setActive(tenant: Tenant) {
    this.tenantStore.setActive(tenant ? tenant.id : null);
  }

  setTenants(tenants: Tenant[]) {
    this.tenantStore.set(tenants);
  }

  async getTenantsByOwnerID(owner_id: string) {
    try {
      const request: TenantsAPI.GetTenantsRequest = {
        filter: {
          owner_id
        },
      };
      const res = await this.tenantApi.getTenants(request);
      return res?.tenants;
    } catch (e) {
      debug.error('ERROR in getting getTenantsByOwnerID', e);
      return [];
    }
  }
  async getTenants(doLoading = true) {
    this.setLastPage(false);
    if (doLoading) {
      this.tenantStore.setLoading(true);
    }
    try {
      const paging = this.tenantQuery.paging;
      const filter = this.tenantQuery.filter;
      const request: TenantsAPI.GetTenantsRequest = {
        paging,
        filter,
      };

      const res = await this.tenantApi.getTenants(request);
      if (res.tenants.length > 0) {
        const listId = res.tenants.map((tenant) => tenant.owner_id);
        const listUser = await this.userService.getUsersByIDs(listId);
        const tenants = res.tenants.map((tenant) => {
          tenant.user = listUser.find((user) => user.id == tenant.owner_id);
          return tenant;
        });
        this.setTenants(tenants);
        this.setPaging(res?.paging);
      } else {
        this.resetState();
      }
    } catch (e) {
      debug.error('ERROR in getting Tenants', e);
    }
    if (doLoading) {
      this.tenantStore.setLoading(false);
    }
  }

  async activateTenant(hotline: Hotline) {
    try{
      const tenantId = this.tenantQuery.getActive().id;
      const activateTenantsRequest: TenantsAPI.ActivateTenantRequest = {
        hotline_id: hotline.id,
        owner_id: hotline.owner_id,
        tenant_id: tenantId,
      };
      await this.tenantApi.activateTenant(activateTenantsRequest);
      const user = await this.adminUserApi.getUser(hotline.owner_id);
      const tenant = this.tenantQuery.getActive();
      this.telegramService.activateTenant( user, tenant, hotline);
    } catch (e) {
      debug.error('ERROR in active Tenant', e);
      throw e;
    }

  }

  async createTenant(user: User, shop?: Shop) {
    const request: TenantsAPI.CreateTenantRequest = {owner_id: user?.id,account_id: shop?.id };
    const tenant = await this.tenantApi.createTenant(request);
    this.telegramService.newTenant(tenant,user, shop);
    this.getTenants(tenant);
    this.setActive(tenant);
  }

  async getUsersFilter(searchText: string) {
    return await this.userService.getUsersFilter(searchText);
  }

  resetState() {
    this.setTenants([]);
    this.setPaging(null);
  }
}
