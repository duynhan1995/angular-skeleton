import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import {Shop} from "libs/models/Account";
import {ShopStore, StateShop} from "@etop/state/admin/shop/shop.store";

@Injectable({
  providedIn: 'root'
})
export class ShopQuery extends QueryEntity<StateShop, Shop> {

  constructor(protected store: ShopStore) {
    super(store);
  }
}
