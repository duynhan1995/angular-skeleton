import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState } from '@datorama/akita';
import {Paging} from "@etop/shared";
import {Shop} from "libs/models/Account";
import {ShipmentPriceList, ShopShipmentPriceList} from "libs/models/admin/ShipmentPrice";
import {Connection} from "libs/models/Connection";
import { Filters, Query } from '@etop/models';
import {AdminShopAPI} from "@etop/api";

export interface ShipmentPriceListsGroup {
  connection: Connection;
  shipmentPriceLists: ShipmentPriceList[];
}

export interface StateShop extends EntityState<Shop, string>, ActiveState {
  ui: {
    paging: Paging;
    query: Query;
    filters: Filters;
    filter: AdminShopAPI.GetShopsFilter

    isLastPage: boolean;
    isEmptyFilterResult: boolean;
  };
  shopShipmentPriceLists: ShopShipmentPriceList[];

  shipmentPriceListsGroups: ShipmentPriceListsGroup[];
}

const initialState = {
  ui: {
    paging: null,
    query: null,
    filter: null,
    filters: null,
    isLastPage: false,
    isEmptyFilterResult: false
  },
  shopShipmentPriceLists: [],

  shipmentPriceListsGroups: []
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'shop' })
export class ShopStore extends EntityStore<StateShop> {
  constructor() {
    super(initialState);
  }
}
