import { Injectable } from '@angular/core';
import {ShopQuery} from './shop.query';
import {Paging} from "@etop/shared";
import {AdminConnectionApi, AdminShopAPI, AdminShopApi, ShipmentPriceAPI, ShipmentPriceApi} from "@etop/api";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";
import {ShipmentPriceListsGroup, ShopStore} from "./shop.store";
import {Shop} from "libs/models/Account";
import {requireMethodPermissions} from "@etop/core";
import {Connection} from "libs/models/Connection";
import {ShipmentPriceList} from "libs/models/admin/ShipmentPrice";
import ConfirmShopShipmentPriceListRequest = ShipmentPriceAPI.ConfirmShopShipmentPriceListRequest;
import {TelegramService} from "@etop/features";
import { Filters, Query } from '@etop/models';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  constructor(
    private shopApi: AdminShopApi,
    private shopQuery: ShopQuery,
    private shopStore: ShopStore,
    private connectionApi: AdminConnectionApi,
    private shipmentPriceApi: ShipmentPriceApi,
    private promisesQueue: PromiseQueueService,
    private telegramService: TelegramService
  ) {
  }

  setQuery(query: Query) {
    const ui = this.shopQuery.getValue().ui;
    this.shopStore.update({
      ui: {
        ...ui,
        query
      }
    });
  }

  resetFilter() {
    this.setQuery(null)
    this.setFilters(null)
  }

  setFilter(filter: AdminShopAPI.GetShopsFilter){
    const ui = this.shopQuery.getValue().ui;
    this.shopStore.update({
      ui: {
        ...ui,
        filter
      }
    });
  }

  setFilters(filters: Filters) {
    const ui = this.shopQuery.getValue().ui;
    this.shopStore.update({
      ui: {
        ...ui,
        filters
      }
    });
  }

  setPaging(paging: Paging) {
    const ui = this.shopQuery.getValue().ui;
    this.shopStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  setLastPage(isLastPage: boolean) {
    const ui = this.shopQuery.getValue().ui;
    this.shopStore.update({
      ui: {
        ...ui,
        isLastPage
      }
    });
  }

  setEmptyResultFilter(isEmptyFilterResult: boolean) {
    const ui = this.shopQuery.getValue().ui;
    this.shopStore.update({
      ui: {
        ...ui,
        isEmptyFilterResult
      }
    });
  }

  async getShops(doLoading = true) {
    this.setLastPage(false);
    this.setEmptyResultFilter(false);
    if (doLoading) {
      this.shopStore.setLoading(true);
    }
    try {
      const {query,filter, filters, paging} = this.shopQuery.getValue().ui;
      const shops = await this.shopApi.getShops({filter, filters, paging});
      if (shops.length == 0) {
        if (paging.offset > 0) {
          this.setLastPage(true);
          this.setPaging({
            ...paging,
            offset: paging.offset - paging.limit
          });
        } else if (paging.offset == 0 && (filters?.length || !!query.name)) {
          this.setEmptyResultFilter(true);
          this.shopStore.set([]);
        }
      } else {
        this.shopStore.set(shops);
        if (doLoading) {
          this.shopStore.setActive(null);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Shops', e);
    }
    if (doLoading) {
      this.shopStore.setLoading(false);
    }
  }

  async getShopList(filters: Filters, filter: AdminShopAPI.GetShopsFilter) {
    const paging: Paging = {offset: 0, limit: 100}
    try {
      const shops = await this.shopApi.getShops({filters,filter, paging});
      return shops;
    } catch(e) {
      debug.error('ERROR in getting Shop List', e);
    }
  }

  async getShop(id: string) {
    this.shopStore.setLoading(true);
    try{
      const shop = await this.shopApi.getShop(id);
      this.shopStore.upsert(shop.id,shop);
      this.shopStore.setActive(shop.id);
    } catch (e) {
      debug.error('ERROR in getting shop', e);
      this.shopStore.set([]);
    }
    this.shopStore.setLoading(false);
  }

  selectShop(shop: Shop) {
    this.shopStore.setActive(shop ? shop.id : null);
  }

  @requireMethodPermissions({
    skipError: true,
    permissions: ['admin/shop_shipment_price_list:view']
  })
  async getShopShipmentPriceLists() {
    try {
      const shopId = this.shopQuery.getActive().id;
      const shopSpls = await this.shipmentPriceApi.getShopShipmentPriceLists({
        shop_id: shopId
      });
      this.shopStore.update({shopShipmentPriceLists: shopSpls});
    } catch(e) {
      debug.error('ERROR in getShopShipmentPriceLists', e);
    }
  }

  async confirmShopShipmentPriceLists(requests: ShipmentPriceAPI.ConfirmShopShipmentPriceListRequest[]) {
    try {
      const shop = this.shopQuery.getActive();
      const shopSpls = this.shopStore.getValue().shopShipmentPriceLists;
      const promises = requests.map(req => async() => {
        const _existed = shopSpls.some(shopSpl => shopSpl.connection_id == req.connection_id);
        if (_existed) {
          await this.shipmentPriceApi.updateShopShipmentPriceList(req);
        } else {
          await this.shipmentPriceApi.createShopShipmentPriceList(req)
        }
      });
      await this.promisesQueue.run(promises, 3);
      this.getShopShipmentPriceLists().then();
      this.selectShop(shop);

      this.sendTelegramMessage(requests).then();
    } catch(e) {
      debug.error('ERROR in confirm confirmShopShipmentPriceLists', e);
      throw e;
    }
  }

  async groupShipmentPriceListsByConnection() {
    try {
      const connections = await this.connectionApi.getConnections('builtin');
      const shipmentPriceListsGroups: ShipmentPriceListsGroup[] = [];
      await Promise.all(connections.map(async(conn) => {
        const _shipmentPriceLists = await this.getShipmentPriceListsByConnectionID(conn.id);
        const _shipmentPriceListsGroup: ShipmentPriceListsGroup = {
          connection: conn,
          shipmentPriceLists: _shipmentPriceLists
        };
        shipmentPriceListsGroups.push(_shipmentPriceListsGroup);
      }));
      this.shopStore.update({shipmentPriceListsGroups});
    } catch(e) {
      debug.error('ERROR in getting groupShipmentPriceListsByConnection', e);
    }
  }

  @requireMethodPermissions({
    skipError: true,
    valueOnError: [],
    permissions: ['admin/shipment_price_list:view']
  })
  private async getShipmentPriceListsByConnectionID(connection_id: string) {
    return await this.shipmentPriceApi.getShipmentPriceLists({connection_id})
  }

  async updateShopMoneyTransactionRrule(shop: Shop) {
    await this.shopApi.updateShopMoneyTransactionRrule(shop.id, shop.money_transaction_rrule);
    return this.telegramService.updateShopMtRruleMessage(shop);
  }

  async updateShopPriorityMoneyTransaction(shop: Shop) {
    await this.shopApi.updateShopPriorityMoneyTransaction(shop.id, !shop.is_prior_money_transaction);
    return this.telegramService.updateShopPriorityMtMessage(shop);
  }

  private sendTelegramMessage(requests: ConfirmShopShipmentPriceListRequest[]) {
    const datas: Array<{
      connection: Connection,
      shipmentPriceList: ShipmentPriceList
    }> = this.shopQuery.getValue().shipmentPriceListsGroups.map(item => {
      const req = requests.find(r => r.connection_id == item.connection.id);
      return {
        connection: item.connection,
        shipmentPriceList: item.shipmentPriceLists.find(spl => spl.id == req.shipment_price_list_id)
      };
    });
    return this.telegramService.updateShopShipmentPriceList(this.shopQuery.getActive(), datas);
  }

}
