import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore, ActiveState, EntityUIStore } from '@datorama/akita';
import { Order } from '@etop/models';

export type OrderUI = {
  isOpen: boolean;
  isLoading: boolean;
}
export interface StateOrders extends EntityState<Order, string>, ActiveState {
  ui: {
  };
}
export interface OrderUIState extends EntityState<OrderUI> { }

const initialState = {
  createOrderSuccess: {}
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'Order' })
export class OrderStore extends EntityStore<StateOrders> {
  ui: EntityUIStore<OrderUIState>;

  constructor() {
    super(initialState);
    const defaults = { isLoading: false, isOpen: true };
    this.createUIStore().setInitialEntityState(defaults);
  }
}
