import { Injectable, Provider } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { OrderApi, FulfillmentApi, AccountApi } from '@etop/api';
import { ConnectionStore, ConnectionService } from '@etop/features';
import { Connection, FilterOperator, Filters, Fulfillment, Order, ORDER_SOURCE, TRY_ON } from '@etop/models';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { OrderStore } from './order.store';
import { OrderQuery } from './order.query';
import { AuthenticateStore } from '@etop/core';
import { RelationshipQuery } from '../relationship';

@Injectable({
  providedIn: 'root'
})

export class OrdersService {

  constructor(
    private orderStore: OrderStore,
    private util: UtilService,
    private orderApi: OrderApi,
    private connectionStore: ConnectionStore,
    private connectionService: ConnectionService,
    private orderQuery: OrderQuery,
    private auth: AuthenticateStore,
    private ffmApi: FulfillmentApi,
    private accountApi: AccountApi,
    private relationshipsQuery: RelationshipQuery
  ) {
  }

  onOrderLinesChanged$ = new Subject();
  onOrderInfoChanged$ = new Subject();
  onOrderCustomerChanged$ = new Subject();
  createOrderFailed$ = new Subject();
  createOrderSuccess$ = new Subject();
  switchOffCreatingFFM$ = new Subject();

  tryOnOptions = [
    {
      code: 'try',
      value: 'try',
      name: 'Cho thử hàng'
    },
    {
      code: 'open',
      value: 'open',
      name: 'Cho xem hàng không thử'
    },
    {
      code: 'none',
      value: 'none',
      name: 'Không cho xem hàng'
    }
  ];

  setActiveOrder(activeOrder: Order) {
    this.orderStore.setActive(activeOrder.id);
  }

  appendOrders(orders: Array<any>) {
    this.orderStore.upsertMany(orders);
  }

  async getOrder(id): Promise<Order> {
    const order = await this.orderApi.getOrder(id);
    const ffm = order.fulfillments[0];
    ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
    ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
      ffm.shipping_state
    );
    ffm.try_on_display = TRY_ON[ffm.try_on];
    order.fulfillments[0] = ffm;
    order.created_by = this.relationshipsQuery.getRelationshipNameById(order.created_by);
    return order
  }

  updateOrder(body) {
    return this.orderApi.updateOrder(body);
  }

  async updateCOD(fulfillmentID, cod_amount) {
    await this.ffmApi.updateFulfillmentCOD({
      fulfillment_id: fulfillmentID,
      cod_amount: Number(cod_amount)
    });
    const token = this.auth.snapshot.token;
    const ffm = await this.ffmApi.getFulfillment(fulfillmentID, token);
    const orderId = this.orderQuery.getActive().id
    this.orderStore.update(orderId, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        total_cod_amount: cod_amount,
        cod_amount: cod_amount
      }],
      activeFulfillment: {
        ...this.orderQuery.getActive().activeFulfillment,
        shipping_fee_shop: ffm.shipping_fee_shop,
        total_cod_amount: cod_amount,
        cod_amount: cod_amount
      }
    });
  }

  async updateGrossWeight(fulfillment: Fulfillment, total_weight) {
    await this.ffmApi.updateFulfillmentInfo({
      fulfillment_id: fulfillment.id,
      gross_weight: Number(total_weight),
      shipping_address: fulfillment.shipping_address,
      pickup_address: fulfillment.pickup_address
    });
    const token = this.auth.snapshot.token;
    const ffm = await this.ffmApi.getFulfillment(fulfillment.id, token);
    this.orderStore.update(this.orderQuery.getActive().id, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        total_weight: total_weight,
        chargeable_weight: total_weight
      }],
      activeFulfillment: {
        ...this.orderQuery.getActive().activeFulfillment,
        shipping_fee_shop: ffm.shipping_fee_shop,
        total_weight: total_weight,
        chargeable_weight: total_weight
      }
    });
  }

  async updateInsurance(fulfillment: Fulfillment, insurance_value) {
    await this.ffmApi.updateFulfillmentInfo({
      fulfillment_id: fulfillment.id,
      insurance_value: Number(insurance_value),
      include_insurance: true,
      shipping_address: fulfillment.shipping_address,
      pickup_address: fulfillment.pickup_address
    });

    this.orderStore.update(this.orderQuery.getActive().id, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        insurance_value: insurance_value,
        include_insurance: true
      }],
      activeFulfillment: {
        ...this.orderQuery.getActive().activeFulfillment,
        insurance_value: insurance_value,
        include_insurance: true
      }
    });
  }

  async updatePickupInfo(fulfillment: Fulfillment, pickup_address) {
    await this.ffmApi.updateFulfillmentInfo({
      fulfillment_id: fulfillment.id,
      pickup_address: pickup_address,
      shipping_address: fulfillment.shipping_address
    });
    const token = this.auth.snapshot.token;
    const ffm = await this.ffmApi.getFulfillment(fulfillment.id, token);
    this.orderStore.update(this.orderQuery.getActive().id, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        pickup_address: pickup_address
      }],
      activeFulfillment: {
        ...this.orderQuery.getActive().activeFulfillment,
        shipping_fee_shop: ffm.shipping_fee_shop,
        pickup_address: pickup_address
      }
    });
  }

  async updateShippingInfo(fulfillment: Fulfillment, shipping_address) {
    await this.ffmApi.updateFulfillmentInfo({
      fulfillment_id: fulfillment.id,
      shipping_address: shipping_address,
      pickup_address: fulfillment.pickup_address
    });
    const token = this.auth.snapshot.token;
    const ffm = await this.ffmApi.getFulfillment(fulfillment.id, token);
    this.orderStore.update(this.orderQuery.getActive().id, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        shipping_address: shipping_address
      }],
      activeFulfillment: {
        ...this.orderQuery.getActive().activeFulfillment,
        shipping_fee_shop: ffm.shipping_fee_shop,
        shipping_address: shipping_address
      }
    });
  }

  async updateShippingNote(fulfillment: Fulfillment, note) {
    await this.ffmApi.updateFulfillmentInfo({
      fulfillment_id: fulfillment.id,
      shipping_address: fulfillment.shipping_address,
      pickup_address: fulfillment.pickup_address,
      shipping_note: note.shipping_note,
      try_on: note.try_on
    });
    this.orderStore.update(this.orderQuery.getActive().id, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        shipping_note: note.shipping_note,
        try_on: note.try_on
      }],
      activeFulfillment: {
        ...this.orderQuery.getActive().activeFulfillment,
        shipping_note: note.shipping_note,
        try_on: note.try_on
      }
    });
  }

  async updateInsuranceValue(fulfillment: Fulfillment) {
    this.orderStore.update(this.orderQuery.getActive().id, {
      fulfillments: [{
        ...this.orderQuery.getActive().fulfillments[0],
        insurance_value: fulfillment.shipment.insurance_value
      }]
    });
  }

  updatePData(order: Order, p_data) {
    order = {
      ...order,
      p_data: p_data
    };
    this.orderStore.upsertMany([order]);
  }

  async updateOrderInfo(order: Order) {
    await this.orderStore.update(order.id, order);
  }

  mapOrderInfo(order: Order, index) {
    return {
      ...order,
      index,
      search_text: this.util.makeSearchText(`${order.code}${order.customer.full_name}${order.customer.phone}`)
    };
  }

  mapSpecialFilters(filters: Filters) {
    filters.forEach(f => {
      if (f.value == '{}') {
        f.op = FilterOperator.eq;
        f.name = 'fulfillment.ids';
      }
    });
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  // TODO: Temporarily closed until dashboard works smoothly
  // @requireMethodRoles({
  //   skipError: true,
  //   valueOnError: [],
  //   roles: ['admin', 'owner', 'salesman', 'accountant']
  // })
  async getOrdersFromAPI(start?: number, perpage?: number, filters?: Array<any>): Promise<Order[]> {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };

    await this.connectionService.getValidConnections();
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a, b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.orderApi.getOrders({ paging, filters })
            .then(orders => {
              orders.map((order, index) => {
                order.activeFulfillment = this.getActiveFulfillment(order, connections);
                return this.mapOrderInfo(order, index);
              });
              resolve(orders);
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  async mapFfm(order) {
    await this.connectionService.getValidConnections();
    this.connectionStore.state$.pipe(
      map(s => s?.initConnections),
      distinctUntilChanged((a, b) => a?.length == b?.length)
    ).subscribe(connections => {
      if (connections?.length) {
        order.activeFulfillment = this.getActiveFulfillment(order, connections);
      }
    });
  }

  async getFfm(id: string, token: string) {
    this.orderStore.setLoading(true);
    const ffm = await this.ffmApi.getFulfillment(id, token);
    const order = await this.orderApi.getOrder(ffm.order.id)
    await this.mapFfm(order);
    if (order) {
      ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
      ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
        ffm.shipping_state
      );
      ffm.try_on_display = TRY_ON[ffm.try_on];
    }
    let _order = new Order({});
    _order = {
      ...order,
      p_data: {
        ..._order.p_data
      }
    };
    // order?.p_data = pData
    this.orderStore.set({ _order });
    this.orderStore.setLoading(false);
    return this.orderQuery.getAll();
  }

  getSelectFilter(filterArr: Array<any>) {
    let filterObj = {
      code: filterArr.find(f => f.name == 'code'),
      shippingCode: filterArr.find(f => f.name == 'fulfillment.shipping_code'),
      status: filterArr.find(f => f.name == 'status'),
      shippingState: filterArr.find(f => f.name == 'fulfillment.shipping_state'),
      customerName: filterArr.find(f => f.name == 'customer.name'),
      customerPhone: filterArr.find(f => f.name == 'customer.phone'),
      totalAmount: filterArr.find(f => f.name == 'total_amount')
    };
    const selectFilter = (order: Order) =>
      (!filterObj.code || order.code.indexOf(filterObj.code.value) > -1)
      && (!filterObj.shippingCode || order.fulfillments[0]?.shipping_code.indexOf(filterObj.shippingCode.value) > -1)
      && (!filterObj.status || order.status == filterObj.status.value)
      && (!filterObj.shippingState || order.fulfillments[0]?.shipping_state == filterObj.shippingState.value)
      && (!filterObj.customerName || order.customer.full_name == filterObj.customerName.value)
      && (!filterObj.customerPhone || order.customer.phone == filterObj.customerPhone.value)
      && (!filterObj.totalAmount || order.total_amount == filterObj.totalAmount.value);
    return selectFilter;
  }

  async getOrders(start?: number, perpage?: number, filters?: Array<any>, force = false) {
    if (filters.length != 0 || this.orderQuery.getCount() < start + perpage || force) {
      this.orderStore.setLoading(true);
      const orders = await this.getOrdersFromAPI(start, perpage, filters);
      orders.map(order => {
        if (order.fulfillments.length > 0) {
          const ffm = order.fulfillments[0];
          ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
          ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
            ffm.shipping_state
          );
          ffm.try_on_display = TRY_ON[ffm.try_on];
        }
        order.created_by = this.relationshipsQuery.getRelationshipNameById(order.created_by);
      });
      if (force) {
        this.orderStore.set(orders);
      } else {
        this.appendOrders(orders); // fix tạm bug check all item - update type Obj = any
      }
      this.orderStore.setLoading(false);
    }
    const selectFilter = this.getSelectFilter(filters);
    return this.orderQuery.getAll({ filterBy: selectFilter }).slice(start, start + perpage);
  }

  async addNewOrder(order: Order) {
    order = await this.getFFMStatusDisplay(order);
    this.appendOrders([order]);// fix tạm bug check all item - update type Obj = any
  }

  async getFFMStatusDisplay(order: Order) {
    await this.connectionService.getValidConnections();
    const _orders = new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a, b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.orderApi.getOrdersByIDs([order.id])
            .then(orders => {
              orders.map((order, index) => {
                order.created_by = this.relationshipsQuery.getRelationshipNameById(order.created_by)
                order.activeFulfillment = this.getActiveFulfillment(order, connections);
                return this.mapOrderInfo(order, index);
              });
              resolve(orders);
            })
            .catch(err => reject(err));
        }
      });
    });
    const orders = await _orders;
    order = orders[0];
    if (order.fulfillments.length > 0) {
      const ffm = order.fulfillments[0];
      ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
      ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
        ffm.shipping_state
      );
      ffm.try_on_display = TRY_ON[ffm.try_on];
    }
    return order;
  }

  setupDataOrder(order: Order): Order {
    order.customer.id = order.customer_id;
    order.p_data = {
      ...order.p_data,
      shipping: {
        ...order.shipping,
        cod_amount: !order.shipping && (order.basket_value + order.total_fee - order.total_discount)
          || order.shipping.cod_amount
      },
      customer: order.customer,
      selected_service: {},
      selected_service_id: '-',
      fee_lines: order.fee_lines,
      total_fee: order.total_fee,
      order_discount: order.order_discount,
      total_discount: order.total_discount,
      total_amount: order.total_amount,
      order_note: order.order_note ? order.order_note : '-',
      ed_code: order.ed_code ? order.ed_code :
        (order.external_id ? order.external_id : '-')
    };
    order.p_data.shipping.try_on_display = TRY_ON[order.p_data.shipping.try_on];
    return order;
  }

  getActiveFulfillment(order: Order, connections?: Connection[]) {
    let activeFulfillment: any;
    if (!connections) {
      connections = this.connectionStore.snapshot.validConnections;
    }
    switch (order.fulfillment_type) {
      case 'shipment':
        const shipmentFfms = order.fulfillments.map(ffm => ffm.shipment);
        const _activeShipmentFfm = shipmentFfms.find(ffm => ffm.status != 'N');
        activeFulfillment = _activeShipmentFfm || shipmentFfms[0];
        break;
      case 'shipnow':
        const shipnowFfms = order.fulfillments.map(ffm => ffm.shipnow);
        const _activeShipnowFfm = shipnowFfms.find(ffm => ffm.status != 'N');
        activeFulfillment = _activeShipnowFfm || shipnowFfms[0];
        break;
      default:
        const _activeFfm = order.fulfillments.find(ffm => ffm.status != 'N');
        activeFulfillment = _activeFfm || order.fulfillments[0];
        break;
    }
    return Fulfillment.fulfillmentMap(JSON.parse(JSON.stringify(activeFulfillment || {})), connections);
  }

  async cancelOrder(order,reason, status) {
    const res = await this.orderApi.cancelOrder(order.id, reason, status);
    res.order.created_by = this.relationshipsQuery.getRelationshipNameById(res.order.created_by);
    return res;
  }

  async orderSourceMap(source, partner_id) {
    if (partner_id != '0' && ['etop_cmx', 'api'].indexOf(source) != -1) {
      const _partner = await this.accountApi.getPublicPartnerInfo(partner_id);
      return _partner
        ? `${ORDER_SOURCE[source]} - ${_partner.name}`
        : ORDER_SOURCE[source];
    }
    return source ? ORDER_SOURCE[source] : 'Không xác định';
  }
}
