import { Injectable } from '@angular/core';
import { QueryEntity, QueryConfig, EntityUIQuery } from '@datorama/akita';
import { Order } from '@etop/models';
import { OrderStore, StateOrders, OrderUIState } from './order.store';

const sortby = (a, b) => (
  new Date(b.created_at).getTime() -
  new Date(a.created_at).getTime());

@Injectable({
  providedIn: 'root'
})
@QueryConfig({ sortBy: sortby })
export class OrderQuery extends QueryEntity<StateOrders, Order> {
  ui: EntityUIQuery<OrderUIState>;

  constructor(protected store: OrderStore) {
    super(store);
    this.createUIQuery();
  }
}
