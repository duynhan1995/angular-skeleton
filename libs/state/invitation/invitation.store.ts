import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { Filters, Invitation } from '@etop/models';
import { Paging } from '@etop/shared';


export interface StateInvitation extends EntityState<Invitation, string>, ActiveState {
  ui: {
    paging: Paging;
    filters: Filters;
  };
}

const initialState = {
  ui: {
    paging: null,
    filters: null,
  },
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'invitation', resettable: true })
export class InvitationStore extends EntityStore<StateInvitation>{
  constructor() {
    super(initialState);
  }
}
