import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Invitation } from '@etop/models';
import { InvitationStore, StateInvitation } from '@etop/state/invitation/invitation.store';

@Injectable({
  providedIn: 'root'
})
export class InvitationQuery extends QueryEntity<StateInvitation, Invitation> {
  constructor(protected store: InvitationStore) {
    super(store);
  }

}
