import {Injectable} from '@angular/core';
import {InvitationStore} from '@etop/state/invitation/invitation.store';
import {InvitationQuery} from '@etop/state/invitation/invitation.query';
import {Paging} from '@etop/shared';
import {AuthorizationApi} from "@etop/api/shop/authorization.api";
import {Filters, Invitation} from "@etop/models";


@Injectable({
  providedIn: 'root'
})
export class InvitationService {
  constructor(
    private authorizationApi: AuthorizationApi,
    private invitationStore: InvitationStore,
    private invitationQuery: InvitationQuery,
  ) {
  }

  async getShopInvitations() {
    this.invitationStore.setLoading(true);
    try {
      let {paging, filters} = this.invitationQuery.getValue().ui;
      const getRelationtipRequest = {
        paging: paging, filters: filters
      }
      let shop_invitations = await this.authorizationApi.getShopInvitations(getRelationtipRequest);
      this.invitationStore.set(shop_invitations);
    } catch (e) {
      debug.error('ERROR in getRelationships');
      this.invitationStore.set(null);
    }
    this.invitationStore.setLoading(false);
  }

  setFilters(filters: Filters) {
    const ui = this.invitationQuery.getValue().ui;
    this.invitationStore.update({
      ui: {
        ...ui,
        filters
      }
    });
  }

  setPaging(paging: Paging) {
    const ui = this.invitationQuery.getValue().ui;
    this.invitationStore.update({
      ui: {
        ...ui,
        paging
      }
    });
  }

  addInvitation(invitation: Invitation) {
    this.invitationStore.add(invitation);
  }
}
