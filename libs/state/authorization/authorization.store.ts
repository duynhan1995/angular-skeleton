import { Injectable } from '@angular/core';
import {Store, StoreConfig} from "@datorama/akita";

export interface StateAuthorization{
  invitationCancelled: boolean;
  relationshipDeleted: boolean;
  relationshipUpdated: boolean;
}

const initialState = {
  invitationCancelled: false,
  relationshipDeleted: false,
  relationshipUpdated: false
};

@Injectable({
  providedIn: "root"
})
@StoreConfig({name: 'authorization'})
export class AuthorizationStore extends Store<StateAuthorization> {
  constructor() {
    super(initialState);
  }
}
