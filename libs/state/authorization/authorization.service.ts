import {Injectable} from '@angular/core';
import {AuthorizationStore} from "@etop/state/authorization/authorization.store";
import {AuthorizationQuery} from "@etop/state/authorization/authorization.query";
import {AuthorizationAPI, AuthorizationApi} from '@etop/api';
import {ListQueryDTO} from '@etop/models/CommonQuery';
import {Invitation} from '@etop/models';


@Injectable({
  providedIn: "root"
})
export class AuthorizationService {

  constructor(
    private authorizationStore: AuthorizationStore,
    private authorizationQuery: AuthorizationQuery,
    private authorizationApi: AuthorizationApi,
  ) {
  }

  async getUserInvitations(query?: Partial<ListQueryDTO>) {
    return await this.authorizationApi.getUserInvitations(query);
  }

  async acceptInvitation(token: string) {
    await this.authorizationApi.acceptInvitation(token);
  }

  async rejectInvitation(token: string) {
    await this.authorizationApi.rejectInvitation(token);
  }

  async resendInvitation(email: string, phone: string) {
    const request: AuthorizationAPI.ResendInvitationRequest = {email, phone};
    await this.authorizationApi.resendInvitation(request);
  }

  async deleteInvitation(token: string) {
    await this.authorizationApi.deleteInvitation(token)
    this.authorizationStore.update({invitationCancelled: true});
    this.authorizationStore.update({invitationCancelled: false});
  }

  async removeUserFromAccount(userId: string) {
    await this.authorizationApi.removeUserFromAccount(userId);
  }

  removeRelationship() {
    this.authorizationStore.update({relationshipDeleted: true});
    this.authorizationStore.update({relationshipDeleted: false});
  }

  updateRelationship() {
    this.authorizationStore.update({relationshipUpdated: true});
    this.authorizationStore.update({relationshipUpdated: false});
  }

  invitationMap(invitation: Invitation) {
    return {
      ...invitation,
      roles_display: invitation.roles && invitation.roles.map
      (r => AuthorizationApi.roleMap(r)).join(', ') || ''
    };
  }
}
