import { Injectable } from '@angular/core';
import {Query} from '@datorama/akita';
import {AuthorizationStore, StateAuthorization} from "./authorization.store";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationQuery extends Query<StateAuthorization> {

  constructor(protected store: AuthorizationStore) {
    super(store);
  }

}
