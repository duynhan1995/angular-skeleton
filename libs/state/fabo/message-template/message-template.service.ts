import { Injectable } from '@angular/core';
import { MessageTemplateApi } from '@etop/api';
import { MessageTemplateQuery } from '@etop/state/fabo/message-template/message-template.query';
import { MessageTemplateStore } from '@etop/state/fabo/message-template/message-template.store';
import { FbPagesQuery } from '@etop/state/fabo/page';
import { MessageTemplate } from '@etop/models';
import { ConversationsQuery, ConversationsService } from '../conversation';
import { AuthenticateStore } from '@etop/core';

@Injectable({
  providedIn: 'root'
})
export class MessageTemplateService {
  constructor(
    private messageTemplateApi: MessageTemplateApi,
    private messageTemplateQuery: MessageTemplateQuery,
    private messageTemplateStore: MessageTemplateStore,
    private conversationsQuery: ConversationsQuery,
    private conversationsService: ConversationsService,
    private auth: AuthenticateStore,
    private FbPagesQuery: FbPagesQuery
  ) {
  }

  async getMessageTemplate() {
    const messageTemplate = await this.messageTemplateApi.messageTemplates();
    this.messageTemplateStore.set(messageTemplate);
  }

  getMessageTemplateWithShortCode(short_code) {
    let messageTemplate = this.messageTemplateQuery.getAll();
    if (short_code) {
      messageTemplate = messageTemplate.filter(template => template.short_code.toLowerCase().includes(short_code.toLowerCase()))
    }
    return messageTemplate;

  }

  async deleteMessageTemplate(id) {
    await this.messageTemplateApi.deleteMessageTemplate(id);
    this.messageTemplateStore.remove(id);
  }

  async updateMessageTemplate(messageTemplate: MessageTemplate) {
    const { id, short_code, template } = messageTemplate;
    await this.messageTemplateApi.updateMessageTemplate(id, short_code, template);
    this.messageTemplateStore.update(id, messageTemplate);
  }

  async createMessageTemplate(messageTemplate: MessageTemplate) {
    const { short_code, template } = messageTemplate;
    await this.messageTemplateApi.createMessageTemplate(short_code, template);
    await this.getMessageTemplate();
  }

  async getMessageTemplateVariables() {
    const messageTemplateVariables = await this.messageTemplateApi.messageTemplateVariables();
    this.messageTemplateStore.update({ messageTemplateVariables });
  }

  setActiveTemplate(id) {
    this.messageTemplateStore.setActive(id);
  }

  replaceVariables(template) {
    let tempalteVariables = this.messageTemplateQuery.getValue().messageTemplateVariables;
    const conversation = this.conversationsQuery.getActive();
    const page_name = this.FbPagesQuery.getFanpageById(conversation.external_page_id)?.external_name;
    tempalteVariables.forEach(variable => {
      if (template.includes(variable.label)) {
        switch(variable.label) {
          case 'Tên khách hàng': {
            template = template.replaceAll('[' + variable.label + ']', `${conversation.external_user_name}`);
          }
          case 'Tên cửa hàng': {
            template = template.replaceAll('[' + variable.label + ']', `${this.auth.snapshot.account.name}`);
          }
          case 'Tên page': {
            template = template.replaceAll('[' + variable.label + ']', `${page_name}`);
         }
          default: {
              break;
          }
        }
      }
    });
    return template;
  }

  async replaceAllTemplate(listTemplates) {
    const replacedlistTemplates = await Promise.all(listTemplates.map( async templateMessage => {
      const replacedMessage = await this.replaceVariables(templateMessage.template)
      return {
        ...templateMessage,
        template: replacedMessage
      };
    }));
    return replacedlistTemplates;
  }
}
