import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { MessageTemplate } from '@etop/models';
import { MessageTemplateStore, StateMessageTemplate } from '@etop/state/fabo/message-template/message-template.store';

@Injectable({
  providedIn: 'root'
})
export class MessageTemplateQuery extends QueryEntity<StateMessageTemplate,MessageTemplate>{
  constructor(protected store: MessageTemplateStore) {
    super(store);
  }
}
