import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { MessageTemplate, MessageTemplateVariable } from '@etop/models';


export interface StateMessageTemplate extends EntityState<MessageTemplate,string>, ActiveState{
  messageTemplateVariables: MessageTemplateVariable[];
}

const initState = {
  messageTemplateVariables: [],
}

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'message_template'})
export class MessageTemplateStore extends EntityStore<StateMessageTemplate>{
  constructor() {
    super(initState);
  }
}
