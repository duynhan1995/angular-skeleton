import { Injectable } from '@angular/core';
import { FacebookUserTagStore } from 'libs/state/fabo/facebook-user-tag/facebook-user-tag.store';
import { FacebookUserTagQuery } from 'libs/state/fabo/facebook-user-tag/facebook-user-tag.query';
import { FbUserTagApi } from '@etop/api/fabo-api/fb-user-tag.api';
import { FbUserTag } from '@etop/models';
import { CustomerFaboApi } from '@etop/api';

@Injectable({
  providedIn: 'root'
})
export class FacebookUserTagService {
  constructor(
    private fbUserTagStore: FacebookUserTagStore,
    private fbUserTagQuery: FacebookUserTagQuery,
    private fbUserTagApi: FbUserTagApi,
    private customerApi: CustomerFaboApi
  ) {}

  async createTag(tag: FbUserTag) {
    const _tag = await this.fbUserTagApi.createTag(tag.name, tag.color);
    let tags = this.fbUserTagQuery.getAll();
    this.fbUserTagStore.set(tags.concat(_tag));
    return _tag;
  }

  setNewFbUserTag(newFbUserTag: FbUserTag){
    this.fbUserTagStore.update({newFbUserTag})
  }

  async listTags() {
    const fbUserTags = await this.fbUserTagApi.listTags();
    this.fbUserTagStore.set(fbUserTags);
  }

  async deleteTag(id) {
    await this.fbUserTagApi.deleteTag(id);
    this.fbUserTagStore.remove(id);
  }

  async addTags(fb_external_user_id: string, tag_ids: string[]) {
    await this.fbUserTagApi.addTags(fb_external_user_id, tag_ids);
  }

  async updateTag(tag: FbUserTag) {
    const _tag = await this.fbUserTagApi.updateTag(tag.id, tag.name, tag.color);
    this.fbUserTagStore.update(_tag.id, _tag);
  }

  async initState(force = false) {
    let tags = this.fbUserTagQuery.getAll();
    this.fbUserTagStore.set(tags);
    if (force) {
      const listTags = await this.fbUserTagApi.listTags();
      this.fbUserTagStore.upsertMany(listTags);
    }
  }

  async updateTags(fb_external_user_id, tag_ids: Array<string>) {
    return await this.customerApi.updateTags(fb_external_user_id, tag_ids);
  }
}
