import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { FacebookUserTagStore, StateFbUserTag } from 'libs/state/fabo/facebook-user-tag/facebook-user-tag.store';
import { FbUserTag } from '@etop/models/faboshop/FbUserTag';

@Injectable({
  providedIn: 'root'
})
export class FacebookUserTagQuery extends QueryEntity<StateFbUserTag, FbUserTag> {
  constructor(protected store: FacebookUserTagStore) {
    super(store);
  }
}
