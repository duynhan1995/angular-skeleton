import {Injectable} from '@angular/core';
import {CustomersQuery} from '@etop/state/fabo/customer/customer.query';
import {CustomerStore} from '@etop/state/fabo/customer/customer.store';
import {CustomerApi} from '@etop/api';
import {OrderCustomer, Customer} from '@etop/models';
import {requireMethodRoles} from '@etop/core';
import {UtilService} from 'apps/core/src/services/util.service';
import {CustomerFaboApi} from '@etop/api/fabo-api/customer-api.service';
import {FilterOperator, Filters} from '@etop/models';
import {FacebookUserTagQuery} from "@etop/state/fabo/facebook-user-tag";

const initState = {
  selected_customer: null,
  selected_customer_address: null,
  independent_customer: new Customer({}),
  customerPaging: {
    offset: 0,
    limit: 60,
    sort: '-created_at'
  },
  ui: {
    paging: {
      page: null,
      perpage: null,
    },
  }
};

@Injectable({
  providedIn: 'root'
})

export class CustomerService {
  constructor(
    private customersQuery: CustomersQuery,
    private customerApi: CustomerApi,
    private customerFaboApi: CustomerFaboApi,
    private customerStore: CustomerStore,
    private util: UtilService,
    private fbUserTagQuery: FacebookUserTagQuery
  ) {
  }

  selected_customer: null;
  independent_customer = new Customer({});

  selectCustomer(selected_customer: Customer) {
    this.customerStore.setActive(selected_customer.id);
  }

  setSelectedListCustomer(selected_list_customer) {
    const customers = this.customersQuery.getValue().selected_list_customers;
    if (customers?.length && selected_list_customer?.length) {
      const _customers = JSON.parse(JSON.stringify(customers));
      selected_list_customer?.forEach(selected_customer => {
        const i = _customers.findIndex(_customer => _customer.id == selected_customer.id);
        if (i > -1) {
          _customers[i] = selected_customer;
        } else {
          _customers.push(selected_customer);
        }
      });
      this.customerStore.set(_customers);
      this.customerStore.update({ selected_list_customers: _customers });
    } else {
      this.customerStore.set(selected_list_customer);
      this.customerStore.update({selected_list_customers: selected_list_customer});
    }
  }

  setActiveCustomer(id) {
    this.customerStore.setActive(id);
  }

  setTagsFbUser(id, tag_ids) {
    const tags = this.mapTags(tag_ids);
    const activeCustomer = this.customersQuery.getActive();
    let fb_users = [...activeCustomer.fb_users];
    if (fb_users) {
      fb_users[0] = {
        ...fb_users[0],
        tags,
        tag_ids
      };
    }
    this.customerStore.update(id, { fb_users });
  }

  mapTags(tag_ids) {
    const _tags = this.fbUserTagQuery.getAll();
    let tags = [];
    if (tag_ids) {
      _tags.forEach(tag => {
        if (tag_ids.includes(tag.id)) {
          tags.push(tag);
        }
      });
    }
    return tags;
  }

  setLoading(loading: boolean) {
    this.customerStore.update({ loading: loading });
  }

  setLastCustomer(flag: boolean) {
    this.customerStore.update({ last_customer: flag });
  }

  async setPaging(offset, limit, filters?: Array<any>) {
    const last_page = this.customersQuery.getValue().last_customer;
    if (!last_page) {
      this.customerStore.update({paging: {offset, limit}});
      await this.fetchCustomers(filters);
    }
  }

  resetData() {
    this.customerStore.update(initState);
  }

  resetActiveCustomer() {
    this.customerStore.setActive(null);
  }

  async createCustomer(data: Customer | OrderCustomer | any) {
    const res = await this.customerApi.createCustomer(data);
    await this.listCustomersWithFbUsers();
    return res;
  }

  async createCustomerAddress(data) {
    const res = await this.customerApi.createCustomerAddress(data);
    const activeCustomer = this.customersQuery.getActive();
    if (activeCustomer) {
      await this.getCustomerAddresses(activeCustomer.id);
    }
    return res;
  }

  async getCustomerAddresses(customer_id) {
    const res = await this.customerApi.getCustomerAddresses(customer_id);
    this.setCustomerAddresses(res);
    return res;
  }

  setCustomerAddresses(customerAddresses) {
    this.customerStore.update({ customer_addresses: customerAddresses });
  }

  setCustomerIndependent(customerIndependent: Customer) {
    this.customerStore.update({ customer_independent: customerIndependent });
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: [],
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  async fetchCustomers(filters?: Array<any>) {
    this.customerStore.setLoading(true);
    const paging = this.customersQuery.getValue().paging;
    const res = await this.customerFaboApi.listCustomersWithFbUsers({
      filters,
      paging
    });
    if (res.length < paging.limit) {
      this.setLastCustomer(true);
    }
    if (res.length) {
      this.setSelectedListCustomer(res);
    }
    this.customerStore.setLoading(false);
  }

  async getCustomers(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };
    return this.customerApi
      .getCustomers({ paging, filters })
      .then(res => {
        const independent_customer = res.customers.find(
          c => c.type == 'independent' || c.type == 'anonymous'
        );
        this.setCustomerIndependent(independent_customer);
        this.customerStore.set(res.customers);
        return res.customers.map((c, index) => this.mapCustomerInfo(c, index));
      });
  }

  async getCustomer(id) {
    const res = await this.customerApi.getCustomer(id);
    this.selectCustomer(res);
  }

  async updateCustomer(data) {
    delete data.code;
    const res = await this.customerApi.updateCustomer(data);
    this.customerStore.updateActive(res);
    this.customerStore.upsert(res.id, res);
    return res;
  }

  async updateCustomerAddress(data) {
    const res = await this.customerApi.updateCustomerAddress(data);
    const activeCustomer = this.customersQuery.getActive();
    await this.getCustomerAddresses(activeCustomer?.id);
    return res;
  }

  async deleteCustomer(id) {
    await this.customerApi.deleteCustomer(id);
    this.customerStore.remove(id);
    await this.listCustomersWithFbUsers();
    this.resetActiveCustomer();
  }

  async deleteCustomerAddress(id) {
    await this.customerApi.deleteCustomerAddress(id);
    this.customerStore.remove(
      selected_customer_address => selected_customer_address.id == id
    );
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: [],
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  async checkExistedCustomer(filterObj: any): Promise<any> {
    try {
      const filters: Filters = [];
      for (let key in filterObj) {
        filters.push({
          name: key,
          op: FilterOperator.contains,
          value: filterObj[key]
        });
      }
      const res: any = await this.customerApi.getCustomers(
        { paging: { offset: 0, limit: 1000 }, filters },
        false
      );
      return res.customers;
    } catch (e) {
      debug.error('ERROR in checking Existed Customer', e);
      return [];
    }
  }

  mapCustomerInfo(customer: Customer, index) {
    return {
      ...customer,
      index,
      info: `<strong>${customer.full_name}${(customer.type != 'independent' &&
        ' - ' + customer.phone) ||
        ''}</strong>`,
      detail: `<strong>${customer.full_name}${(customer.type != 'independent' &&
        customer.phone &&
        ' - ' + customer.phone) ||
        ''}</strong>`,
      search_text: this.util.makeSearchText(
        `${customer.full_name}${customer.phone}`
      )
    };
  }

  /***
   * @deprecated use fetchCustomer instead
   */
  async listCustomersWithFbUsers(
    filters?: Array<any>,
    start?: number,
    perpage?: number
  ) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20,
      sort: ''
    };
    const res = await this.customerFaboApi.listCustomersWithFbUsers({
      filters,
      paging
    });
    if (res.length > 0) {
      this.customerStore.set(res);
      this.customerStore.update({ selected_list_customers: res });
    }
    return res;
  }
}
