import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { CustomerStore, StateCustomers } from '@etop/state/fabo/customer/customer.store';
import { Customer } from 'libs/models/Customer';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersQuery extends QueryEntity<StateCustomers, Customer> {
  constructor(
    protected store: CustomerStore,
  ) {
    super(store);
  }

  selectCustomerByPagination() {
    return combineLatest([this.selectAll(), this.select(s => s.ui.paging)])
      .pipe( map(([customers, paging]) =>
        paging && customers.slice((paging.page - 1) * paging.perpage, paging.page * paging.perpage) || []))
  }
}



