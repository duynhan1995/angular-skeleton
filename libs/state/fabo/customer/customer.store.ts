import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { Injectable } from '@angular/core';
import { Paging } from '@etop/shared';

export interface StateCustomers extends EntityState <Customer,string>, ActiveState{
  selected_customer: Customer,
  selected_customer_address: CustomerAddress,
  customer_addresses: CustomerAddress[],
  customer_independent:Customer,
  selected_list_customers: Customer[],
  last_customer: boolean,
  ui:{
    paging:{
      page: number,
      perpage: number,
    },
  }
  paging: Paging;
}

const initState = {
  selected_customer: null,
  customer_addresses: null,
  selected_customer_address: null,
  customer_independent:null,
  last_customer: null,
  ui: {
    paging:null
  },
  paging: {
    offset: 0,
    limit: 100
  }
};

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name:'customer', resettable: true})
export class CustomerStore extends EntityStore<StateCustomers>{
  constructor() {
    super(initState);
  }
}
