import { FbPagesStore } from 'libs/state/fabo/page/page.store';
import { Injectable } from '@angular/core';
import { FbPages, FbPage } from '@etop/models/faboshop/FbPage';
import { PageApi } from '@etop/api';
import { ConversationsService } from 'libs/state/fabo/conversation';
import { FilterOperator } from '@etop/models';

@Injectable({
    providedIn: 'root'
})
export class FbPagesService {

    constructor(
        private fbPagesStore: FbPagesStore,
        private pageApi: PageApi,
        private conversationsService: ConversationsService
    ) { }

    async fetchPages(selectFirstFanpage = false) {
        const pages = await this.pageApi.listPages({
            filters: [{ name: 'status', op: FilterOperator.eq, value: 'P' }]
        });
        this.setPages(pages);
        if (selectFirstFanpage) {
            this.setActivePage(pages[0]);
            this.conversationsService.setFilter({
                external_page_id: pages[0].external_id
            });
            await this.conversationsService.loadMoreConversations(true);
        }
    }

    setPages(pages: FbPages) {
        this.fbPagesStore.set(pages);
    }

    setActivePage(page: FbPage) {
        const pageId = !page ? null : page?.id;
        this.fbPagesStore.setActive(pageId);
    }
}
