import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { StateFbPages, FbPagesStore } from 'libs/state/fabo/page/page.store';
import { FbPage } from '@etop/models/faboshop/FbPage';

@Injectable({
  providedIn: 'root'
})
export class FbPagesQuery extends QueryEntity<StateFbPages, FbPage> {

  constructor(protected store: FbPagesStore) {
    super(store);
  }

  getFanpageById(id: string): FbPage {
    return this.getAll().find(p => p.external_id == id);
  }
}
