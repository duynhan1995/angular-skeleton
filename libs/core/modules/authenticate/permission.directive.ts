import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { BaseRoleDirective } from './base-role-directive.internal';
import { AuthenticateStore } from './authenticate.store.service';
import { checkMatch } from '@etop/core/modules/authenticate/common.internal';
import { AuthenticateService } from '@etop/core/modules/authenticate/authenticate.service';

@Directive({
  selector: '[ePermission]'
})
export class PermissionDirective extends BaseRoleDirective {

  permission = '';

  constructor(
    element: ElementRef,
    templateRef: TemplateRef<any>,
    viewContainer: ViewContainerRef,
    authenticateStore: AuthenticateStore,
    authenticateService: AuthenticateService
  ) {
    super(element, templateRef, viewContainer, authenticateStore, authenticateService);
  }

  @Input()
  set ePermission(permission) {
    this.permission = permission
  }

  check(): boolean {
    return this.authenticateService.checkPermissions([this.permission], 'or');
  }

}
