import { Directive, ElementRef, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { checkMatch, Operator } from 'libs/core/modules/authenticate/common.internal';
import { AuthenticateStore } from '@etop/core/modules/authenticate/authenticate.store.service';
import { BaseDirective } from '@etop/core/base/base-directive';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { BaseRoleDirective } from '@etop/core/modules/authenticate/base-role-directive.internal';
import { AuthenticateService } from '@etop/core/modules/authenticate/authenticate.service';

@Directive({
  selector: '[eRole]'
})
export class RoleDirective extends BaseRoleDirective implements OnInit{

  private role = '';

  constructor(
    element: ElementRef,
    templateRef: TemplateRef<any>,
    viewContainer: ViewContainerRef,
    authenticateStore: AuthenticateStore,
    authenticateService: AuthenticateService
  ) {
    super(element, templateRef, viewContainer, authenticateStore, authenticateService);
  }

  ngOnInit(): void {
    this.authenticateStore.authenticatedData$.pipe(
      map(data => data.permission),
      distinctUntilChanged(),
      takeUntil(this.destroy$)
    ).subscribe(() => this.updateView());
  }

  @Input()
  set eRole(val) {
    this.role = val;
    this.updateView()
  }

  protected check(): boolean {
    return this.authenticateService.checkRoles([this.role], 'or');
  }
}
