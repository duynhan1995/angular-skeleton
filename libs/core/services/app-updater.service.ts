import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { first } from 'rxjs/operators';
import { concat, interval } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppUpdater {

  constructor(private appRef: ApplicationRef, private updater:  SwUpdate ) {
  }

  get UpdateAvailable() {
    return this.updater.available;
  }

  hook() {
    const appIsStable$ = this.appRef.isStable.pipe(first(isStable => isStable === true));
    const everySixHours$ = interval(5 * 60 * 1000);
    const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

    everySixHoursOnceAppIsStable$.subscribe(() => this.updater.checkForUpdate());
  }

  forceUpdate() {
    return this.updater.activateUpdate().then(() => document.location.reload());
  }
}
