import { Inject, Injectable, Optional } from '@angular/core';

export const CONFIG_TOKEN = '$_config_token';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(@Optional() @Inject(CONFIG_TOKEN) private cfg: any) {
  }

  getConfig() {
    return this.cfg;
  }

  get(name: string) {
    return this.cfg && this.cfg[name];
  }
}
