import { BaseModel } from '../core/base/base-model';

export class Collection extends BaseModel {
  id: string;
  name: string;
  description: string;
  short_desc: string;
  desc_html: string;
  created_at: string;
  updated_at: string;
}
