import { BaseModel } from '@etop/core/base/base-model';

export class Category extends BaseModel {
  id: string;
  name: string;
  parent_id: string;
  shop_id: string;
  status: string;
}
