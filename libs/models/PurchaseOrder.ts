import { BaseModel } from '../core/base/base-model';
import { Supplier } from '@etop/models/Supplier';
import { InventoryVoucher } from '@etop/models/Inventory';
import { Attribute, Variant } from '@etop/models/Product';

export class PurchaseOrder extends BaseModel {
  id: string;
  shop_id: string;
  supplier_id: string;
  supplier: Supplier;
  basket_value: number;
  total_discount: number;
  total_amount: number;
  paid_amount: number;
  total_products: number;
  total_items: number;
  code: string;
  note: string;
  status: string;
  status_display: string;
  payment_status: string;
  payment_status_display: string;
  lines: PurchaseOrderLine[];
  created_by: string;
  cancel_reason: string;
  confirmed_at: string | Date;
  cancelled_at: string | Date;
  created_at: string | Date;
  updated_at: string | Date;
  deleted_at: string | Date;
  inventory_voucher: InventoryVoucher;
  p_data: any = {

  };
}

export class PurchaseOrderLine {
  variant_id: string;
  quantity: number | string;
  payment_price: number | string;
  attributes: Attribute[];
  image_url: string;
  code: string;
  product_id: string;
  product_name: string;
}

export class PurchaseOrderDiscount{
  amount: number;
  note: string;
}
