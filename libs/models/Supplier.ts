import { BaseModel } from '@etop/core/base/base-model';

export class Supplier extends BaseModel {
  id: string;
  code: string;
  shop_id: string;
  full_name: string;
  note: string;
  phone: string;
  email: string;
  company_name: string;
  tax_number: string;
  headquater_address: string;
  status: string;
  created_at: string | Date;
  updated_at: string | Date;

  deleted?: boolean;
  p_data: any = {

  }
}
