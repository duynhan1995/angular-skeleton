import { BaseModel, Processing } from '@etop/core/base/base-model';
import {ConnectionProvider} from "@etop/models";
import { Fulfillment } from '../Fulfillment';
import { BankAccount } from '../Bank';

export class MoneyTransactionShippingExternal extends BaseModel implements Processing {
  bank_account: BankAccount;
  code: string;
  created_at: string;
  external_paid_at: string;
  id: string;
  invoice_number: string;
  lines: MoneyTransactionShippingExternalLine[];
  note: string;
  connection_id: string;
  carrier_display: string;
  carrier: ConnectionProvider;

  provider: ConnectionProvider;
  provider_logo: string;

  status: string;
  status_display: string;
  total_cod: string;
  total_orders: string;
  updated_at: string;
  p_data: any = {
    selected: false
  }
}

export class MoneyTransactionShippingExternalLine extends BaseModel {
  created_at: string;
  etop_fulfillment_id: string;
  etop_fulfillment_id_raw: string;
  external_address: string;
  external_closed_at: string;
  external_code: string;
  external_created_at: string;
  external_customer: string;
  external_total_cod: string;
  external_total_shipping_fee: string;
  fulfillment: Fulfillment;
  id: string;
  import_error: any;
  money_transaction_shipping_external_id: string;
  note: string;
  updated_at: string;
}
