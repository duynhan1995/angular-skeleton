import { BaseModel } from '@etop/core/base/base-model';
import {FeeType} from "../Fee";
import {Connection} from "../Connection";

export enum CustomRegionType {
  noi_vung = "noi_vung",
  lien_vung = "lien_vung",
  all = "all"
}

export enum ProvinceType {
  noi_tinh = "noi_tinh",
  lien_tinh = "lien_tinh",
  all = "all"
}

export enum RegionType {
  noi_mien = "noi_mien",
  lien_mien = "lien_mien",
  can_mien = "can_mien",
  all = "all"
}

export enum UrbanType {
  noi_thanh = "noi_thanh",
  ngoai_thanh_1 = "ngoai_thanh_1",
  ngoai_thanh_2 = "ngoai_thanh_2",
  unknown = "unknown"
}

export enum ShipmentPriceLocation {
  advanced = "advanced",
  province_noi_tinh = "province_noi_tinh",
  province_lien_tinh = "province_lien_tinh",
  region_noi_mien = "region_noi_mien",
  region_lien_mien = "region_lien_mien",
  region_can_mien = "region_can_mien"
}

export enum ShipmentPriceUrban {
  all = "all",
  noi_thanh = "noi_thanh",
  ngoai_thanh_1 = "ngoai_thanh_1",
  ngoai_thanh_2 = "ngoai_thanh_2",
}

export enum ShippingLocationType {
  pick = 'pick',
  deliver = 'deliver'
}

export class ShipmentService extends BaseModel {
  connection_id: string;
  created_at: Date;
  description: string;
  ed_code: string;
  id: string;
  name: string;
  service_ids: string[];
  updated_at: Date;
  status: string;
  status_display: string;

  provider_logo: string;
  provider_name: string;

  connection: Connection;
  shipment_prices: ShipmentPrice[];
  available_locations: AvailableLocation[];
  blacklist_locations: BlacklistLocation[];

}

export interface AvailableLocation {
  custom_region_ids?: string[];
  filter_type?: 'include' | 'exclude';
  province_codes?: string[];
  regions?: ('north' | 'middle' | 'south')[];
  shipping_location_type: ShippingLocationType;
}

export interface BlacklistLocation {
  province_codes?: string[];
  district_codes?: string[];
  ward_codes?: string[];
  reason: string;
  shipping_location_type: ShippingLocationType;
}

export class ShipmentPriceList extends BaseModel {
  created_at: Date;
  description: string;
  id: string;
  is_default: boolean;
  name: string;
  connection_id: string;
  updated_at: Date;

  provider_logo: string;
  provider_name: string;

  connection: Connection;
  shipment_prices: ShipmentPrice[];
}

export interface ShopShipmentPriceList {
  connection_id: string;
  created_at: Date;
  note: string;
  shipment_price_list_id: string;
  shop_id: string;
  updated_at: Date;
  updated_by: string;
}

export class ShipmentPrice extends BaseModel {
  created_at: Date;
  details: ShipmentPriceDetail[];
  id: string;
  name: string;
  status: string;
  priority_point: number;
  custom_region_ids: string[];
  custom_region_types: CustomRegionType[];

  region_types: RegionType[];

  province_types: ProvinceType[];

  urban_types: UrbanType[];
  urban: ShipmentPriceUrban;
  urban_type_display: string;

  location: ShipmentPriceLocation;
  location_display: string;
  location_detail_display: string;
  latest_location_type: LatestLocationType;

  shipment_price_list_id: string;
  shipment_service_id: string;
  updated_at: Date;
  additional_fees: ShipmentPriceAdditionalFee[];

  error?: boolean;
}

export interface ShipmentPriceDetail {
  overweight: OverWeight[];
  price: number;
  weight: number;
}

export interface ShipmentPriceAdditionalFee {
  base_value_type: BaseValueType;
  base_value_type_display?: string;
  fee_type: FeeType;
  fee_type_display?: string;
  calculation_method: CalculationMethod;
  calculation_method_display?: string;
  rules?: Rule[];
}

export enum CalculationMethod {
  unknown = "unknown",
  cumulative = "cumulative",
  first_satisfy = "first_satisfy"
}

export enum BaseValueType {
  unknown = "unknown",
  cod_amount = "cod_amount",
  main_fee = "main_fee",
  basket_value = "basket_value"
}

export interface Rule {
  amount: number;
  min_price?: number;
  start_value?: number;

  max_value: number;
  min_value: number;
  price_modifier_type: PriceModifierType;
  price_modifier_type_display?: string;
  max_value_error?: boolean;
}

export enum PriceModifierType {
  percentage = "percentage",
  fixed_amount = "fixed_amount"
}

export interface LatestLocationType {
  custom_region_ids: string[];
  custom_region_types: CustomRegionType[];
  region_types: RegionType[];
  province_types: ProvinceType[];
}

export class OverWeight {
  max_weight: number;
  min_weight: number;
  price_step: number;
  weight_step: number;
  max_weight_error?: boolean;
}

export class ShipmentPriceListPromotion {
  applied_rules: PromotionAppliedRules;
  connection_id: string;
  created_at: Date;
  date_from: Date;
  date_to: Date;
  description: string;
  id: string;
  name: string;
  shipment_price_list_id: string;
  priority_point: number;
  status: string;
  status_display: string;
  updated_at: Date;

  connection: Connection;
  shipment_price_list: ShipmentPriceList;
}

export interface PromotionAppliedRules {
  from_custom_region_ids?: string[];
  shop_created_date?: {
    from?: string;
    to?: string;
  };
  user_created_date?: {
    from?: string;
    to?: string;
  };
  using_price_list_ids?: string[];
}
