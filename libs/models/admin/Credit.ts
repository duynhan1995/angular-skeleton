import {Shop} from "../Account";

export class Credit {
  amount: number;
  classify: string;
  created_at: Date;
  id: string;
  paid_at: Date;
  shop: Shop;
  shop_id: string;
  status: string;
  status_display: string;
  type: string;
  updated_at: Date;
}
