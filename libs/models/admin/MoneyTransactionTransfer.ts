import { BaseModel } from '@etop/core/base/base-model';
import { BankAccount } from '../Bank';
import { MoneyTransactionShop } from '../MoneyTransactionShop';

export class MoneyTransactionTransfer extends BaseModel {
  code: string;
  total_cod: number;
  total_orders: number;
  total_amount: number;
  total_fee: number;
  status: string;
  status_display: string;
  money_transactions: MoneyTransactionShop[];
  created_at: string;
  updated_at: string;
  confirmed_at: string;
  note: string;
  invoice_number: string;
  bank_account: BankAccount;
  valid_bang_ke_lines: BangKe[];
  invalid_bang_ke_lines: BangKe[];
}

export class BangKe extends BaseModel {
  id: string;
  shop_id: string;
  shop_name: string;
  ref: string;
  bank_account: BankAccount;
  bank_account_TCB?: BankAccount;
  total_amount: number;
  content: string;
  verified: boolean;
  currency_unit: string;
}
