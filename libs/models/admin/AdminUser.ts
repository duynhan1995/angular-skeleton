export enum AdminUserRole {
  admin = 'admin',
  ad_salelead = 'ad_salelead',
  ad_sale = 'ad_sale',
  ad_customerservice = 'ad_customerservice',
  ad_accountant = 'ad_accountant',
  ad_customerservice_lead = 'ad_customerservice_lead',
  ad_voip = 'ad_voip',
}

export class AdminUser {
  email: string;
  full_name: string;
  phone: string;
  roles: AdminUserRole[];
  roles_display: string;
  user_id: string;
}
