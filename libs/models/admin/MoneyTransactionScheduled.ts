import { MoneyTransactionShop } from '../MoneyTransactionShop';
import { BangKe } from './MoneyTransactionTransfer';
import { BaseModel } from '@etop/core/base/base-model';

export class MoneyTransactionScheduled extends BaseModel {
  date: Date;
  day_display: string;
  is_today: boolean;
  techcombank_transaction: MoneyTransactionScheduledBank;
  vietcombank_transaction: MoneyTransactionScheduledBank;
  other_bank_transaction: MoneyTransactionScheduledBank;
  summary_transaction: MoneyTransactionScheduledBank;
  rrules_applied: string[];
}

export class MoneyTransactionScheduledBank {
  total_amount: number;
  money_transactions: MoneyTransactionShop[];
  bang_ke_lines: BangKe[];
}
