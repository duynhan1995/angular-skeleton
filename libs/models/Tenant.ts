import { BaseModel } from '../core/base/base-model';
import { User } from './User';

export const TENANT_STATUS = {
  Z: 'Chưa kích hoạt',
  P: 'Đã kích hoạt',
  unknown: 'Không xác định',
};

export class Tenant extends BaseModel {
  id: string;
  owner_id: string;
  name: string;
  domain: string;
  created_at: string;
  updated_at: string;
  status: string;
  status_display: string;
  user: User;

  static tenantMap(tenant: Tenant): Tenant {
    tenant = new Tenant(tenant);
    tenant.status_display = TENANT_STATUS[tenant.status];
    return tenant;
  }
}
