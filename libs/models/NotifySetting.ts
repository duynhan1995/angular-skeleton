import { BaseModel } from '../core/base/base-model';

export class NotifySetting extends BaseModel {
  topic: string;
  enable: boolean;
  title?: string;
  description?: string;
}