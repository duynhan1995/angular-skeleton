import { BaseModel } from '../core/base/base-model';

export enum FeeType {
  unknown = "unknown",
  main = "main",
  return = "return",
  adjustment = "adjustment",
  insurance = "insurance",
  tax = "tax",
  other = "other",
  cods = "cods",
  address_change = "address_change",
  discount = "discount",
  redelivery = "redelivery"
}

export class FulfillmentFeeLine extends BaseModel {
  shipping_fee_type: FeeType;
  shipping_fee_type_display: string;
  cost: number;
  external_service_id: string;
  external_service_name: string;
  external_service_type: string;
  external_shipping_order_id: string;
  external_payment_channel_id: string;
}
