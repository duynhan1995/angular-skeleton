import { BaseModel } from '../core/base/base-model';

export class Noti extends BaseModel {
  id: string;
  title: string;
  message: string;
  is_read: boolean;
  entity: string;
  entity_id: string;
  account_id: string;
  seen_at: string;
  created_at: string;
  updated_at: string;
  sync_status: string;
  send_notification: boolean;
}
