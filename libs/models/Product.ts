import { BaseModel } from '../core/base/base-model';
import { Collection } from '@etop/models/Collection';
import { Category } from '@etop/models/Category';
import { PurchaseOrder } from '@etop/models/PurchaseOrder';

export type Status = 'Z' | 'P' | 'N' | 'S';
export type StatusDisplay =
  | 'new'
  | 'approving'
  | 'published'
  | 'rejected'
  | 'disabled'
  | 'stopped';


export class ProductSrc extends BaseModel {
  id: string;
  type: string;
  name: string;
  status: string;
  updated_at: string;
  created_at: string;
}

export class Product extends BaseModel {
  attributes: any;
  bg_image: string;
  category: Category;
  category_id: string;
  code: string;
  collection_ids: Array<string>;
  collections: Array<Collection>;
  cost_price?: number;
  desc_html: string;
  description: string;
  haravan_id: string;
  id: string;
  image: string;
  image_urls: Array<string>;
  list_price: number;
  meta: any;
  name: string;
  wholesale_price: number;
  s_wholesale_price: number;
  list_price_min: number;
  list_price_max: number;
  price: number;
  product_source_id: string;
  product_source_name: string;
  product_source_type: string;
  retail_price: number;
  short_desc: string;
  quantity_available: number;
  status: 'default' | 'active' | 'disabled';
  status_display: string;
  total_quantity: number;
  tags: Array<string>;
  variants: Array<Variant>;
  created_at: Date | string;
  updated_at: Date | string;
  inventory_quantity?: number;
  o_data: any;
  p_data: any = {
    selected: false,
    detailed: false,
    shift: false,
    collections: [],
    category: {},
    category_ids: [],
    haravan_error: null
  };

  product_type: string;
  gross_profit: number;
  gross_profit_percent: number;

  meta_fields: Array<MetaField>
}

export class Variant extends BaseModel {
  attributes: Array<Attribute>;
  checked?: boolean;
  code: string;
  cost_price?: number;
  created_at: Date | string;
  updated_at: Date | string;
  desc_html: string;
  description: string;
  image_urls: Array<string>;
  image: string;
  bg_image: string;
  list_price: number;
  name: string;
  price?: number;
  product_id: string;
  product_name: string;
  product_source_id: string;
  quantity_available: number;
  quantity: number;
  inventory_variant: InventoryVariant;
  retail_price?: number;
  short_desc: string;
  status: string;
  tags: Array<string>;
  unit: string;
  product: Product;
  purchase_order: PurchaseOrder;
  wholesale_price: number;w
  s_wholesale_price: number;
  list_price_min: number;
  list_price_max: number;

  p_data: any = {};
  status_display: string;
  e_status: any;
  s_status: string;
}

export class Attribute {
  name: string;
  value: string;
}

export class InventoryVariant {
  shop_id: string;
  variant_id: string;
  quantity_on_hand: number;
  quantity_picked: number;
  quantity: number;
  cost_price: number;
  created_at: Date | string;
  updated_at: Date | string;
}

export class MetaField {
  key: string;
  value: string;
}

export class ShopProduct extends Product {
  tags: Array<string>;
  updated_at: string;
}
