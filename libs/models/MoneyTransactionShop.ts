import { BaseModel, Processing } from '../core/base/base-model';
import { BankAccount } from './Bank';
import { Shop } from '@etop/models/Account';
import {Ticket} from "@etop/models/Ticket";

export class MoneyTransactionShop extends BaseModel implements Processing {
  bank_account: BankAccount;
  closed_at: string;
  code: string;
  confirmed_at: string;
  created_at: string;
  estimated_transfered_at: string;
  etop_transfered_at: string;
  id: string;
  invoice_number: string;
  money_transaction_shipping_etop_id: string;
  money_transaction_shipping_external_id: string;
  note: string;
  provider: string;
  shop: Shop;
  shop_id: string;
  status: string;
  status_display: string;
  status_mapped: string;
  tickets: Ticket[];
  total_amount: number;
  total_cod: number;
  total_orders: number;
  transfer_postponed_reason: string;
  updated_at: string;

  p_data: any = {
    selected: false
  }
}
