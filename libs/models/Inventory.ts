import { BaseModel } from '../core/base/base-model';
import { Attribute } from '@etop/models/Product';

export declare type InventoryVoucherType = 'in' | 'out';
export declare type InventoryVoucherRefType = 'order' | 'purchase_order' | 'stocktake';

export class InventoryVoucher extends BaseModel {
  total_amount: number;
  total_products: number;
  total_items: number;
  created_by: string;
  updated_by: string;
  lines: InventoryVoucherLine[];
  trader_id: string;
  note: string;
  type: InventoryVoucherType;
  type_display: string;
  id: string;
  shop_id: string;
  title: string;
  ref_id: string;
  ref_code: string;
  ref_type: InventoryVoucherRefType;
  ref_display: string;
  ref_name: string;
  created_at: string | Date;
  updated_at: string | Date;
  cancelled_at: string | Date;
  confirmed_at: string | Date;
  cancel_reason: string;
  code: string;
  trader: InventoryVoucherTrader;
  trader_display: string;
  p_data: any = {

  };
}

export class InventoryVoucherLine {
  product_id: string;
  product_name: string;
  variant_name: string;
  variant_id: string;
  code: string;
  image_url: string;
  attributes: Attribute[];
  price: number;
  quantity: number;
}

export class InventoryVoucherTrader {
  id: string;
  type: 'customer' | 'supplier';
  full_name: string;
  phone: string;
  deleted: boolean;
}

