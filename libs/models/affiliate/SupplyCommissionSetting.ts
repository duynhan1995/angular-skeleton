import { BaseModel } from '@etop/core/base/base-model';

export type DurationType = 'day' | 'month' | 'year' | string;

export class SupplyCommissionSetting extends BaseModel {
  product_id: string;
  depend_on: 'product' | 'customer' | string;
  level1_direct_commission: number;
  level1_indirect_commission: number;
  level1_limit_count: number;
  level2_direct_commission: number;
  level2_indirect_commission: number;
  m_level1_limit_duration: { duration: number, type: DurationType };
  m_lifetime_duration: { duration: number, type: DurationType };
  created_at: string;
  updated_at: string;
  group: string;
}
