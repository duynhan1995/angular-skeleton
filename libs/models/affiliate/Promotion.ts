import { BaseModel } from '@etop/core/base/base-model';

export class Promotion extends BaseModel {
  amount: number;
  id: string;
  product_id: string;
  type: 'cashback' | 'discount' | string;
  unit: 'percent' | 'vnd' | string;
}
