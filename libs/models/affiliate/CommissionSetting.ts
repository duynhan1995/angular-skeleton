import { BaseModel } from '@etop/core/base/base-model';

export class CommissionSetting extends BaseModel {
  amount: number;
  product_id: string;
  unit: 'percent' | 'vnd' | string;
  created_at: string;
  updated_at: string;
}
