import { BaseModel } from '../core/base/base-model';

export class EventMessage extends BaseModel {
  requestId?: any;
  event: string;
  data: any;
  error: any;
}
