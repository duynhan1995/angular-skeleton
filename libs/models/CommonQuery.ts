import { Filters } from '@etop/models/Filter';

export class MixedQuery {
  ids?: Array<string>;
  all?: boolean;
  all_shops?: boolean;
  all_supplier?: boolean;
}

export class ListQueryDTO {
  paging: {
    offset?: number;
    limit?: number;
    sort?: string;
  };
  filters: Filters;
  mixed: MixedQuery;
  filter: {
    name:string;
  }
}
