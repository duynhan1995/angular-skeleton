
export interface CursorPaging {
  after?: string;
  sort?: string;
  before?: string;

  limit: number;
  next?: string;
  prev?: string;

}