import { BaseModel } from '../core/base/base-model';
import { User } from '@etop/models/User';
import { BankAccount } from '@etop/models/Bank';

export declare type TraderType = 'customer' | 'carrier' | 'supplier';
export declare type LedgerType = 'cash' | 'bank';
export declare type RefType = 'order' | 'fulfillment' | 'purchase_order';
export declare type CreatedType = 'manual' | 'auto';
export declare type ReceiptType = 'receipt' | 'payment';

export class Receipt extends BaseModel {
  amount: number;
  code: string;
  created_at: string;
  created_by: string;
  created_type: CreatedType | string;
  created_type_display: string;
  description: string;
  ledger_id: string;
  ledger: Ledger;
  ledger_type: LedgerType | string;
  ledger_type_display: string;
  lines: Array<ReceiptLine>;
  paid_at: string | Date;
  trader: ReceiptTraderInfo;
  trader_type: TraderType | string;
  trader_type_display: string;
  ref_type: RefType | string;
  ref_type_display: string;
  shop_id: string;
  status: string;
  status_display: string;
  title: string;
  trader_id: string;
  type: ReceiptType | string;
  type_display: string;
  updated_at: string;
  user: User;
  user_display: string;
  cancel_reason: string;
  o_data: any;
  p_data: any = {
    selected: false,
    detailed: false
  };
  note: string;
}

export class ReceiptTraderInfo {
  full_name: string;
  phone: string;
  id: string;
  type: string;
  deleted: boolean;
}

export class ReceiptLine {
  amount: number;
  ref_id: string;
  title: string;
  created_at?: Date;
  total_amount?: number;
  received_amount?: number;
  paid_amount?: number;
  error?: boolean;
}

export class Ledger extends BaseModel {
  id: string;
  name: string;
  bank_account: BankAccount;
  note: string;
  created_by: string;
  type: string;
  created_at: string;
  updated_at: string;
  p_data: any = {};
}

