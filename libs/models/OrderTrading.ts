import { OrderAddress } from './Address';
import { BaseModel } from '@etop/core';
import { OrderCustomer, OrderFeeLine, OrderLine } from '@etop/models/Order';

export class OrderTrading extends BaseModel {
  customer: OrderCustomer;
  customer_address: OrderAddress;
  billing_address: OrderAddress;
  lines: OrderLine;
  discounts: Array<any>;
  total_items: number;
  basket_value: number;
  order_discount: number;
  total_fee: number;
  fee_lines: Array<OrderFeeLine>;
  total_discount: number;
  total_amount: number;
  order_note: string;
  referral_meta: {
    referral_code: string
  }
}
