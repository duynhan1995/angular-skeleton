export class Tenant {
    created_at: string;
    domain: string;
    id: string;
    name: string;
    owner_id: string;
    status: string;
    updated_at: string;
}