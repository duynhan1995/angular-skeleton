import { BaseModel } from '../core/base/base-model';
import { Address } from './Address';

export class ShopSettings extends BaseModel {
    payment_type_id: 'none' | 'seller' | 'buyer';
    return_address: Address;
    shipping_note: string;
    try_on: 'unknown' | 'none' | 'open' | 'try';
    weight: number;
    created_at?: string;
    updated_at?: string;
    shop_id?: string;
    return_address_id?: string;
}