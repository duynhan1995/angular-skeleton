import { BaseModel } from '../core/base/base-model';

export class Brand extends BaseModel {
  id: string;
  name: string;
  description: string;
  desc_html: string;
  policy: string;
  image_urls: Array<string>;
}
