import { BaseModel } from '@etop/core/base/base-model';

export class Carrier extends BaseModel {
  id: string;
  shop_id: string;
  full_name: string;
  phone: string;
  note: string;
  status: string;
  created_at: string;
  updated_at: string;
}
