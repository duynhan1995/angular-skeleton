import { BaseModel } from '../core/base/base-model';
import { Attribute } from '@etop/models/Product';

export declare type StocktakeType = 'balance' | 'discard';

export class Stocktake extends BaseModel {
  id: string;
  shop_id?: string;
  total_quantity: number;
  note?: string;
  code: string;
  created_by: string;
  updated_by: string;
  created_at: Date | string;
  updated_at: Date | string;
  confirmed_at: Date | string;
  cancelled_at: Date | string;
  cancel_reason: string;
  status: string;
  status_display: string;
  type: StocktakeType;
  type_display: string;
  lines: StocktakeLine[];
  total_diffs?: number;
  total_products?: number;
  increase_diff?: number;
  decrease_diff?: number;
  discard_quantity?: number;
  p_data: any = {

  };
}

export class StocktakeLine {
  product_id: string;
  product_name: string;
  variant_name: string;
  variant_id: string;
  cost_price: number;
  old_quantity: number;
  new_quantity: number;
  code: string;
  image_url: string;
  attributes: Attribute[];

  discard_quantity?: number;
}
