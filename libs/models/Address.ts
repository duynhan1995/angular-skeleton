import { Province, District, Ward } from './Location';
import {BaseModel, Processing} from '../core/base/base-model';

export class Address extends BaseModel implements Processing {
  id: string;
  province: string;
  province_code: string;
  district: string;
  district_code: string;
  ward: string;
  ward_code: string;
  address1: string;
  address2: string;
  zip: string;
  country: string;
  full_name: string;
  first_name: string;
  last_name: string;
  phone: string;
  email: string;
  position: string;
  type: string;
  notes: {
    lunch_break?: string;
    note?: string;
    open_time?: string;
    other?: string;
  };
  coordinates: Coordinates;

  info?: any;
  detail?: any;
  o_data: any;
  p_data: any;

  Province: Province;
  District: District;
  Ward: Ward;

  constructor(obj) {
    super(obj);
    if (!this.notes) {
      this.notes = {};
    }
  }

}

export class OrderAddress extends BaseModel {
  full_name: string;
  first_name?: string;
  last_name?: string;
  phone: string;
  country?: string;
  city?: string;
  province?: string;
  province_code: string;
  district?: string;
  district_code: string;
  ward?: string;
  ward_code: string;
  zip?: string;
  company?: string;
  address?: string;
  address1: string;
  address2?: string;
  coordinates?: Coordinates;

  display?: string;

  o_data?: any;

  notes?: any;

  Province?: Province;
  District?: District;
  Ward?: Ward;
}

export class Coordinates {
  latitude: number;
  longitude: number;
}

export class ShopShippingAddress {
  sh_address: OrderAddress;
  x_service_id: string;
  x_shipping_fee: number;
  x_service_name: string;
  shipping_provider: string;
}
