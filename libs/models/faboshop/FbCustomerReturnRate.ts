import { BaseModel } from '@etop/core';

export class FbCustomerReturnRate extends BaseModel {
  connection_id: string;
  connection_method: string;
  connection_name: string;
  customer_return_rate: CustomerReturnRate;
}

export class CustomerReturnRate extends BaseModel {
  level: string;
  level_code: string;
  rate: number;
  color: string;
  icon: string;
}
