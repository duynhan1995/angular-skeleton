import {BaseModel} from "@etop/core/base/base-model";

export class FbComment extends BaseModel {
  created_at: string;
  external_attachment: FbCommentExternalAttachment;
  external_comment_count: number;
  external_created_time: string;
  external_from: {
    email: string;
    id: string;
    name: string;
    external_user_picture_url: string;
  };
  external_id: string;
  external_page_id: string;
  external_post_id: string;
  external_message: string;
  external_parent: FbCommentExternalParent;
  external_parent_id: string;
  external_parent_user_id: string;
  external_user_id: string;
  fb_page_id: string;
  fb_post_id: string;
  id: string;
  updated_at: string;

  external_sticker: string;
  
  is_liked?: boolean;
  is_hidden?: boolean;
  is_private_replied?: boolean;
  error: boolean;
  loading: boolean;
  created_by?: string;
}

export class FbCommentExternalAttachment {
  media: {
    image: {
      height: number;
      src: string;
      width: number;
    }
  };
  target: {
    id: string;
    url: string;
  };
  title: string;
  type: string;
  url: string;
}

export class FbCommentExternalParent {
  created_time: Date;
  from: {
    email: string;
    id: string;
    name: string;
  };
  id: string;
  message: string;
}
