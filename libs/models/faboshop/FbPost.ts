export class FbPost {
  created_at: Date;
  external_attachments: FbPostExternalAttachment[];
  external_created_time: Date;
  external_from: {
    email: string;
    id: string;
    name: string;
  };
  external_icon: string;
  external_id: string;
  external_message: string;
  external_parent_id: string;
  external_picture: string;
  fb_page_id: string;
  id: string;
  updated_at: Date
}

export class FbPostExternalAttachment {
  media_type: string;
  sub_attachments: [
    {
      media: {
        height: number;
        src: string;
        width: number;
      };
      target: {
        id: string;
        url: string;
      };
      type: string;
      url: string;
    }
  ];
  type: string;
}

export class CreatePostResult {
  external_id: string;
  external_url: string;
}
