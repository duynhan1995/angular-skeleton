import { BaseModel } from '@etop/core/base/base-model';

export class FbPage extends BaseModel {
  connection_status: string;
  connection_status_display: string;
  created_at: Date;
  external_category: string;
  external_category_list: {
    id: string;
    name: string;
  }[];
  external_id: string;
  external_image_url: string;
  external_name: string;
  external_tasks: string[];
  fb_user_id: string;
  id: string;
  shop_id: string;
  status: string;
  updated_at: Date;
  user_id: string;

  reason: string;
}

export type FbPages = FbPage[];
