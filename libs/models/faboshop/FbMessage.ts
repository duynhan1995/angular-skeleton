import {BaseModel} from "@etop/core/base/base-model";

export class FbMessage extends BaseModel {
  created_at: string;
  external_attachments: FbMessageExternalAttachment[];
  external_conversation_id: string;
  external_created_time: string;
  external_from: {
    email: string;
    id: string;
    name: string;
  };
  external_id: string;
  external_page_id: string;
  external_message: string;
  external_to: {
    email: string;
    id: string;
    name: string;
  }[];
  fb_conversation_id: string;
  fb_page_id: string;
  id: string;
  updated_at: string;

  external_sticker: string;

  error: boolean;
  loading: boolean;
  created_by?: string;
}

export class FbMessageExternalAttachment {
  id: string;
  image_data: {
    height: number;
    image_type: number;
    max_height: number;
    max_width: number;
    preview_url: string;
    render_as_sticker: boolean;
    url: string;
    width: number;
  };
  mime_type: string;
  name: string;
  size: number;
}
