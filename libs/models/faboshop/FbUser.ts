import { BaseModel } from '@etop/core/base/base-model';
import {Customer} from '../Customer';
import { FbUserTag } from '@etop/models/faboshop/FbUserTag';
import { FbCustomerReturnRate } from '@etop/models/faboshop/FbCustomerReturnRate';

export class FbUser extends BaseModel {
  created_at: string;
  external_id: string;
  external_info: {
    first_name: string;
    image_url: string;
    last_name: string;
    name: string;
    short_name: string;
  }
  status: string;
  updated_at: string;
  customer_id: string;
  customer: Customer;
  tag_ids: string[];
  tags: FbUserTag[];
}

