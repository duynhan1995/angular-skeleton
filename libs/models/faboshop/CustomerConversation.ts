import { FbPost } from './FbPost';
import { FbUser } from './FbUser';
import { Customer } from '../Customer';
import { FbComment, FbMessage, FbUserTag } from '@etop/models';

export type CustomerConversationType = 'unknown' | 'message' | 'comment' | 'all';
export type MessageStatus = 'sending' | 'sent' | 'error';

export interface CursorPaging {
  after?: string;
  sort?: string;
  before?: string;

  limit: number;
  next?: string;
  prev?: string;

}

export interface CustomerConversation {
  id: string;

  created_at: Date;
  external_id: string;
  external_from: {
    email: string;
    id: string;
    name: string;
    external_user_picture_url: string;
  };
  external_page_id: string;
  external_user_id: string;
  external_user_name: string;
  external_user_picture_url: string;
  external_post_attachments: PostAttachment[];
  fb_page_id: string;

  is_read: boolean;
  last_message: string;
  last_message_at: Date;
  last_customer_message_at: string;
  type: CustomerConversationType;
  updated_at: string;
  image_url: string;

  external_user_tags: string[];
  tags: FbUserTag[];

  messages: Messages;
  paging: CursorPaging;
  customer: Customer;
  fbUsers: Array<FbUser>;
  fb_post?: FbPost;
  lastest_message: Message;
}

export class Message {
  id: string;

  medias: MessageMedia[];
  message: string;

  fb_page_id:string;
  fb_conversation_id: string;
  external_page_id: string;
  external_conversation_id: string;
  external_created_time: string;
  external_id: string;
  external_post_id: string;
  external_user_id: string;
  external_parent_user_id: string;

  from: {
    email: string;
    external_user_picture_url: string;
    id: string;
    name: string;
  }

  to : {
    email: string;
    external_user_picture_url: string;
    id: string;
    name: string;
  }

  created_at: string;
  updated_at: string;

  status?: MessageStatus;

  o_data: any;

  is_liked?: boolean;
  is_hidden?: boolean;
  is_private_replied?: boolean;
  created_by?: string;


  static fromFbMessage(fbMessage: FbMessage) {
    const medias = fbMessage.external_attachments.map<MessageMedia>(eAttach => {
      return {
        mime_type: eAttach.mime_type,
        height: eAttach.image_data?.height,
        width: eAttach.image_data?.width,
        max_width: eAttach.image_data?.max_width,
        max_height: eAttach.image_data?.max_height,
        preview_url: eAttach.image_data?.preview_url,
        url: eAttach.image_data?.url,
        type: 'photo'
      };
    });
    if (fbMessage.external_sticker) {
      medias.push({
        mime_type: '',
        height: null,
        width: null,
        max_width: null,
        max_height: null,
        preview_url: fbMessage.external_sticker,
        url: fbMessage.external_sticker,
        type: 'sticker'
      });
    }
    if (['video/mp4', 'audio/mpeg'].includes(fbMessage.external_attachments[0]?.mime_type)) {
      medias.splice(0);
    }
    return {
      id: fbMessage.id,
      medias: medias,
      message: fbMessage.external_message,
      fb_page_id: fbMessage.fb_page_id,
      fb_conversation_id: fbMessage.fb_conversation_id,
      external_page_id: fbMessage.external_page_id,
      external_conversation_id: fbMessage.external_conversation_id,
      external_created_time: fbMessage.external_created_time,
      external_id: fbMessage.external_id,
      external_post_id: null,
      external_user_id: fbMessage.external_from.id,
      external_parent_user_id: null,
      from: fbMessage.external_from as any,
      to: fbMessage.external_to as any,
      created_at: fbMessage.created_at,
      updated_at: fbMessage.updated_at,
      o_data: fbMessage,
      created_by: fbMessage?.created_by
    };
  }

  static fromFbComment(fbComment: FbComment): Message {
    const attachment = fbComment.external_attachment;
    const medias: MessageMedia[] = attachment && [{
      height: attachment?.media?.image?.height,
      width: attachment?.media?.image?.width,
      preview_url: attachment?.media?.image?.src,
      max_height: null,
      max_width: null,
      mime_type: '',
      type: attachment.type as any,
      url: attachment.url
    }] || [];
    return {
      id: fbComment.id,
      fb_conversation_id: '',
      fb_page_id: fbComment.external_page_id,
      external_page_id: fbComment.external_page_id,
      external_conversation_id: fbComment.external_post_id,
      external_created_time: fbComment.external_created_time,
      external_id: fbComment.external_id,
      external_post_id: fbComment.external_post_id,
      external_user_id: fbComment.external_user_id,
      external_parent_user_id: fbComment.external_parent_user_id,
      from: fbComment.external_from as any,
      to: null,
      medias,
      message: fbComment.external_message,
      created_at: fbComment.created_at,
      updated_at: fbComment.updated_at,
      o_data: fbComment,
      is_liked: fbComment.is_liked,
      is_hidden: fbComment.is_hidden,
      is_private_replied: fbComment.is_private_replied,
      created_by: fbComment?.created_by
    };
  }
}

export type Messages = Message[];

export class MessageMedia {
  height: number;
  width: number;
  max_width: number;
  max_height: number;
  mime_type: string;
  preview_url: string;
  url: string;

  type?: 'video_inline' | 'sticker' | 'photo' | '';
  pre_upload_state?: any;
}

export class PostAttachment {
  media: {
    image :{
    height: number;
    src: string;
    width: number;
  }};
  media_type: MessageMedia;
  sub_attachments: PostSubAttachment[];
  type: string;
}

export class PostSubAttachment {
  media: {
    height: number;
    src: string;
    width: number;
  };
  target: {
    id: string;
    url: string;
  };
  type: string;
  url: string;
}
