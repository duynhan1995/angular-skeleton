import { BaseModel } from '@etop/core';

export class FbUserTag extends BaseModel {
  id: string;
  name: string;
  shop_id: string;
  color: string;
  created_at: string;
  updated_at: string;
}
