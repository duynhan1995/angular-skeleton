/**
 * NativeScript helpers
 */

declare var NSObject, NSString, android, java, window;

/**
 * Determine if running on native iOS mobile app
 */
export function isIOS() {
  return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
}

/**
 * Determine if running on native Android mobile app
 */
export function isAndroid() {
  return /(android)/i.test(navigator.userAgent);
}

/**
 * Determine if running on native iOS or Android mobile app
 */
export function isNativeScript() {
  return isIOS() || isAndroid();
}

/**
 * Electron helpers
 */
export function isElectron() {
  return typeof window !== 'undefined' && window.process && window.process.type;
}
