import {DOC_ORIENTATION} from './DOC_ORIENTATION';
import {Renderer2} from '@angular/core';
import {Convert} from '@etop/utils/convert';

export class ImageCompress {

  private static CreateImage(file: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      try {
        const img = new Image();
        img.onload = function(){
          resolve(img);
        };
        img.src = file;
      }catch (e) {
        reject(e);
      }

    });
  }

  static async decodeImage(file: any): Promise<{image: any, orientation: DOC_ORIENTATION, width: number, height: number}> {
    const img = await ImageCompress.CreateImage(file);
    return new Promise((resolve, reject) => {
      ImageCompress.getOrientation(file,(doc_orientation) => {
        resolve({
          image: img,
          orientation: doc_orientation,
          width: img.naturalWidth,
          height: img.naturalHeight
        });
      })
    })
  }

  /**
   * Get the correct Orientation value from tags, in order to write correctly in our canvas
   */
  static getOrientation(file: any, callback: (result: DOC_ORIENTATION) => void) {
    if (file instanceof File) {
      const reader = new FileReader();
      try {
        reader.onload = function ($event) {
          callback(ImageCompress._getOrientation(reader.result as ArrayBuffer));
        };
        reader.readAsArrayBuffer(file);
      } catch (e) {
        return callback(0);
      }
    } else if (typeof file == 'string') {
      callback(ImageCompress._getOrientation(Convert.convertDataURIToBinary(file)));
    }
  }

  private static _getOrientation(arrayBuffer: ArrayBuffer): DOC_ORIENTATION {
    const view = new DataView(arrayBuffer);
    if (view.getUint16(0, false) !== 0xFFD8) { return DOC_ORIENTATION.NotDefined }
    const length = view.byteLength;
    let offset = 2;
    while (offset < length) {
      const marker = view.getUint16(offset, false);
      offset += 2;
      if (marker === 0xFFE1) {
        if (view.getUint32(offset += 2, false) !== 0x45786966) { return DOC_ORIENTATION.NotJpeg; }
        const little = view.getUint16(offset += 6, false) === 0x4949;
        offset += view.getUint32(offset + 4, little);
        const tags = view.getUint16(offset, little);
        offset += 2;
        for (let i = 0; i < tags; i++) {
          if (view.getUint16(offset + (i * 12), little) === 0x0112) {
            return view.getUint16(offset + (i * 12) + 8, little);
          }
        }
        // tslint:disable-next-line:no-bitwise
      } else if ((marker & 0xFF00) !== 0xFF00) { break; } else { offset += view.getUint16(offset, false); }
    }
    return DOC_ORIENTATION.NotJpeg;
  }


  /**
   * return a promise with the new image data and image orientation
   */
  static uploadFile(render: Renderer2):Promise<{image: string, orientation: DOC_ORIENTATION}> {

    const promise: Promise<{image: string, orientation: DOC_ORIENTATION}> = new Promise(function(resolve, reject) {

      const inputElement = render.createElement('input');
      render.setStyle(inputElement, 'display', 'none');
      render.setProperty(inputElement, 'type', 'file');
      render.setProperty(inputElement, 'accept', 'image/*');

      render.listen(inputElement, 'click', ($event) => {
        //console.log('MouseEvent:', $event);
        //console.log('Input:', $event.target);
        $event.target.value = null;
      });


      render.listen(inputElement, 'change', ($event) => {
        const file: File = $event.target.files[0];

        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          try {
            ImageCompress.getOrientation(file, orientation => {
              resolve({image:myReader.result as string, orientation});
            });
          } catch (e) {
            //console.log(`ngx-image-compress error ${e}`);
            reject(e);
          }
        };

        try {
          myReader.readAsDataURL(file);
        } catch (e) {
          console.warn(`ngx-image-compress - probably no file have been selected: ${e}`);
          reject("No file selected");
        }

      });
      inputElement.click();

    });

    return promise;
  }

  static compress(
    imageDataUrlSource: string,
    orientation: DOC_ORIENTATION,
    render: Renderer2,
    ratio: number = 50,
    quality: number = 50
  ): Promise<string> {
    return new Promise(async function (resolve, reject) {
      quality = quality / 100;
      ratio = ratio / 100;
      const sourceImage = new Image();
      const photon = await import('@silvia-odwyer/photon');
      sourceImage.onload = function () {
        setTimeout(() => {
          const canvas: HTMLCanvasElement = render.createElement('canvas');
          const ctx: CanvasRenderingContext2D = canvas.getContext('2d');

          let w, h;
          w = sourceImage.naturalWidth;
          h = sourceImage.naturalHeight;

          if (orientation === DOC_ORIENTATION.Right || orientation === DOC_ORIENTATION.Left) {
            const t = w;
            w = h;
            h = t;
          }

          canvas.width = w;
          canvas.height = h;

          ctx.drawImage(sourceImage, 0, 0, canvas.width, canvas.height);

          let image = photon.open_image(canvas, ctx);
          let newcanvas = photon.resize(image, canvas.width * ratio, canvas.height * ratio, 1);
          const result = newcanvas.toDataURL();
          resolve(result);
        }, 100)


      };

      sourceImage.src = imageDataUrlSource;

    });
  }


  /**
   * helper to evaluate the compression rate
   * @param s the image in base64 string format
   */
  static byteCount(s: string): number {
    return encodeURI(s).split(/%..|./).length - 1;
  }

  static addMimeIfNeeded(base64str: string) {
    if (!/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/.test(base64str)) {
      return `data:${ImageCompress.guessImageMime(base64str)};base64,` + base64str;
    }
    return base64str;
  }

  static guessImageMime(data){
    if(data.charAt(0)=='/'){
      return "image/jpeg";
    }else if(data.charAt(0)=='R'){
      return "image/gif";
    }else if(data.charAt(0)=='i'){
      return "image/png";
    }
  }

}
