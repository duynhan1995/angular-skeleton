import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { FbCustomerReturnRate } from '@etop/models/faboshop/FbCustomerReturnRate';

@Injectable({
  providedIn: 'root'
})
export class ExtraShipmentApi {
  constructor(
    private http:HttpService
  ) {
  }

  customerReturnRate( phone: string,index?: number){
    return this.http.post(
      `api/fabo.ExtraShipment/CustomerReturnRate`, { phone }
    ).toPromise().then(res => {
      res.customer_return_rates =  res.customer_return_rates.map(data => this.mapCustomerReturnRate(data))
      return {...res, index}
    });
  }

  mapCustomerReturnRate(fbCustomerReturnRate: FbCustomerReturnRate): FbCustomerReturnRate{
    const customerReturnRate = fbCustomerReturnRate?.customer_return_rate
    switch (customerReturnRate?.level_code) {
      case 'level_1':
        customerReturnRate.color = '#6FD36F';
        customerReturnRate.icon = 'check_circle_outline';
        break;
      case 'level_2':
        customerReturnRate.color = '#F7B500';
        customerReturnRate.icon = 'warning';
        break;
      case 'level_3':
        customerReturnRate.color = '#FF7300';
        customerReturnRate.icon = 'warning';
        break;
      case 'level_4':
        customerReturnRate.color = '#E74C3C';
        customerReturnRate.icon = 'not_interested';
        break;
    }
    fbCustomerReturnRate.customer_return_rate = customerReturnRate
    return fbCustomerReturnRate;
  }
}
