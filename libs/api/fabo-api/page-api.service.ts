import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { FbPages } from 'libs/models/faboshop/FbPage';

const STATUS = {
  P: 'Đã kết nối',
  N: 'Chưa kết nối'
};

@Injectable({
  providedIn: 'root'
})
export class PageApi {
  fabo_url = '';

  static get status() {
    return STATUS;
  }

  constructor(
    private http: HttpService
  ) {
  }

  static statusMap(status: string) {
    return PageApi.status[status];
  }

  connectPages(access_token: string) {
    return this.http.post(
      `api/fabo.Page/ConnectPages`, {access_token}, null
    ).toPromise();
  }

  listPages(query?: Partial<ListQueryDTO>): Promise<FbPages> {
    return this.http.post(
      `api/fabo.Page/ListPages`,
      query,
      null
    ).toPromise().then(res => res.fb_pages.map(page => {
      return {
        ...page,
        connection_status_display: PageApi.statusMap(page.connection_status)
      }
    }));
  }

  removePages(external_id: string) {
    return this.http.post(
      `api/fabo.Page/RemovePages`, {external_id}, null
    ).toPromise();
  }

}
