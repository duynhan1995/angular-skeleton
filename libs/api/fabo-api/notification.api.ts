import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { BaseModel } from '@etop/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationApi{
  constructor(
    private http: HttpService
  ) { }
  getNotifySetting(){
    return this.http.post('api/etop.User/GetNotifySetting',{}).toPromise().then()
  }
  disableNotifyTopic(topic:string){
    return this.http.post('api/etop.User/DisableNotifyTopic',{topic:topic}).toPromise().then()
  }
  enableNotifyTopic(topic:string){
    return this.http.post('api/etop.User/EnableNotifyTopic',{topic:topic}).toPromise().then()
  }
  createDevice(data) {
    return this.http.post('api/shop.Notification/CreateDevice', data).toPromise()
      .then(res => res);
  }
}

export class Notification extends BaseModel{
  topic: string
  enable: boolean
  title: string
  description: string
}
