import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { FbUserTag } from '@etop/models/faboshop/FbUserTag';

@Injectable({
  providedIn: 'root'
})
export class FbUserTagApi {
  constructor(
    private http: HttpService
  ) {
  }

  createTag(name, color: string): Promise<FbUserTag> {
    return this.http.post(
      `api/fabo.Shop/CreateTag`, { name, color }
    ).toPromise();
  }

  listTags(): Promise<FbUserTag[]> {
    return this.http.post(
      `api/fabo.Shop/GetTags`, {}
    ).toPromise().then(res => res.result);
  }

  addTags(fb_external_user_id: string, tag_ids: string[]): Promise<string[]> {
    return this.http.post(
      `api/fabo.Customer/UpdateTags`, {fb_external_user_id,tag_ids}
    ).toPromise();
  }

  deleteTag(id: string) {
    return this.http.post(
      `api/fabo.Shop/DeleteTag`, { id }
    ).toPromise();
  }

  updateTag(id, name, color: string) {
    return this.http.post(
      `api/fabo.Shop/UpdateTag`, { id, name, color }
    ).toPromise();
  }
}
