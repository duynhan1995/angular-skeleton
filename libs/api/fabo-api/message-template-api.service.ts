import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';

@Injectable({
  providedIn: 'root'
})
export class MessageTemplateApi{
  constructor(
    private http: HttpService,
  ) {
  }

  createMessageTemplate(short_code,template: string) {
    return this.http
      .post(`api/fabo.CustomerConversation/CreateMessageTemplate`, { short_code,template })
      .toPromise()
      .then();
  }

  deleteMessageTemplate(id: string) {
    return this.http
      .post(`api/fabo.CustomerConversation/DeleteMessageTemplate`, { id })
      .toPromise()
      .then();
  }

  updateMessageTemplate(id,short_code,template: string) {
    return this.http
      .post(`api/fabo.CustomerConversation/UpdateMessageTemplate`, { id,short_code,template})
      .toPromise()
      .then();
  }

  messageTemplates() {
    return this.http
      .post(`api/fabo.CustomerConversation/MessageTemplates`, {  })
      .toPromise()
      .then(res => res.templates);
  }

  messageTemplateVariables() {
    return this.http
      .post(`api/fabo.CustomerConversation/MessageTemplateVariables`, {  })
      .toPromise()
      .then(res => res.variables);
  }

}
