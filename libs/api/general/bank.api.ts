import { Injectable } from '@angular/core';
import {HttpService} from "@etop/common";

@Injectable({
  providedIn: 'root'
})
export class BankApi {

  constructor(
    private http: HttpService
  ) { }

  listBanks() {
    return this.http.post('/api/etop.Bank/GetBanks', {}).toPromise();
  }

  listBankProvinces() {
    return this.http.post('/api/etop.Bank/GetBankProvinces', {all: true}).toPromise();
  }

  listBankBranches() {
    return this.http.post('/api/etop.Bank/GetBankBranches', {all: true}).toPromise();
  }

}
