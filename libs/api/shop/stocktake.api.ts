import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { Stocktake, StocktakeType } from 'libs/models/Stocktake';

const STATUS = {
  Z: 'Chưa xác nhận',
  N: 'Huỷ',
  P: 'Đã xác nhận'
};

const TYPE = {
  balance: 'Cân bằng kho',
  discard: 'Xuất huỷ'
};

@Injectable({
  providedIn: 'root'
})
export class StocktakeApi {
  static get status() {
    return STATUS;
  }

  static get type() {
    return TYPE;
  }

  constructor(
    private http: HttpService
  ) { }

  static statusMap(status) {
    return StocktakeApi.status[status];
  }

  static typeDisplayMap(type: StocktakeType) {
    if (!type) {
      return StocktakeApi.type['balance'];
    }
    return StocktakeApi.type[type];
  }

  createStocktake(data): Promise<any> {
    data = this.parseNumberStoctake(data);
    return this.http
      .post('api/shop.Stocktake/CreateStocktake', data)
      .toPromise();
  }

  updateStocktake(data): Promise<any> {
    data = this.parseNumberStoctake(data);
    return this.http
      .post(`api/shop.Stocktake/UpdateStocktake`, data)
      .toPromise();
  }

  confirmStocktake(id: string, auto_inventory_voucher: 'create' | 'confirm') {
    return this.http
      .post('api/shop.Stocktake/ConfirmStocktake', { id, auto_inventory_voucher })
      .toPromise();
  }

  cancelStocktake(id: string, cancel_reason: string) {
    return this.http
      .post('api/shop.Stocktake/CancelStocktake', { id, cancel_reason })
      .toPromise();
  }

  getStocktake(id: string): Promise<any> {
    return this.http
      .post('api/shop.Stocktake/GetStocktake', {id})
      .toPromise()
      .then(res => this.stocktakeMap(res));
  }

  getStocktakes(query: Partial<ListQueryDTO>): Promise<any> {
    return this.http
      .post('api/shop.Stocktake/GetStocktakes', query)
      .toPromise()
      .then(res => res.stocktakes.map(s => this.stocktakeMap(s)));
  }

  stocktakeMap(stocktake: Stocktake): Stocktake {
    return {
      ...stocktake,
      status_display: StocktakeApi.statusMap(stocktake.status) || '',
      type: stocktake.type || 'balance',
      type_display: StocktakeApi.typeDisplayMap(stocktake.type),
      total_diffs: stocktake.lines.filter(l => l.new_quantity - l.old_quantity != 0).length,
      total_products: stocktake.lines.length,
      increase_diff: stocktake.lines.filter(l => l.new_quantity - l.old_quantity > 0)
        .reduce((a, b) => a + Number(b.new_quantity) - Number(b.old_quantity), 0),
      decrease_diff: stocktake.lines.filter(l => l.new_quantity - l.old_quantity < 0)
        .reduce((a, b) => a + Number(b.new_quantity) - Number(b.old_quantity), 0),
      discard_quantity: stocktake.type == 'discard' && stocktake.lines
        .reduce((a, b) => a + Number(b.old_quantity) - Number(b.new_quantity), 0),
      lines: stocktake.lines.map(l => ({
        ...l,
        discard_quantity: stocktake.type == 'discard' && (l.old_quantity - l.new_quantity)
      }))
    }
  }

  private parseNumberStoctake(stocktake: Stocktake): Stocktake {
    stocktake.total_quantity = Number(stocktake.total_quantity);
    stocktake.lines.forEach(l => {
      l.cost_price = Number(l.cost_price);
      l.old_quantity = Number(l.old_quantity);
      l.new_quantity = Number(l.new_quantity);
    });
    return stocktake;
  }

}
