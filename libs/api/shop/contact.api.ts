import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import {CursorPaging} from "@etop/models";

@Injectable({
  providedIn: 'root'
})
export class ContactApi {

  constructor(private http: HttpService) {
  }

  async getContacts(getContactsRequest?: ContactAPI.GetContactsRequest) {
    return await this.http.post('api/shop.Contact/GetContacts', getContactsRequest)
      .toPromise().then(res => {
        return {
          paging: res.paging,
          contacts: res.contacts,
        }
      });
  }

  async getContact(id) {
    return await this.http.post('api/shop.Contact/GetContact', {id}).toPromise();
  }

  async createContact(data) {
    return this.http.post('api/shop.Contact/CreateContact', data).toPromise();
  }

  async updateContact(data) {
    return this.http.post('api/shop.Contact/UpdateContact', data).toPromise();
  }

  async deleteContact(id) {
    return this.http.post('api/shop.Contact/DeleteContact', {id:id}).toPromise();
  }
}

export namespace ContactAPI {
  export class GetContactsRequest {
    filter?: GetContactsFilter;
    paging?: CursorPaging
  }
  export class GetContactsFilter {
    ids?: string[];
    phone?: string;
  }

  export class GetContactRequest {
    id: string
  }

}
