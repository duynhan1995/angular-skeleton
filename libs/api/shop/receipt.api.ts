import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { Ledger, Receipt } from 'libs/models/Receipt';

const TRADER_TYPE = {
  customer: 'Khách hàng',
  supplier: 'Nhà cung cấp',
  carrier: 'Nhà vận chuyển',
};

const RECEIPT_TYPE = {
  receipt: 'Thu',
  payment: 'Chi'
};

const RECEIPT_STATUS = {
  Z: 'Chưa xác nhận',
  N: 'Huỷ',
  P: 'Đã xác nhận'
};

const LEDGER_TYPE = {
  cash: 'Tiền mặt',
  bank: 'Chuyển khoản'
};

const REF_TYPE = {
  order: 'Đơn bán hàng',
  fulfillment: 'Đơn giao hàng',
  purchase_order: 'Đơn nhập hàng'
};

@Injectable({ providedIn: 'root' })
export class ReceiptApi {
  static get traderType() {
    return TRADER_TYPE;
  }

  static get receiptType() {
    return RECEIPT_TYPE;
  }

  static get receiptStatus() {
    return RECEIPT_STATUS;
  }

  static get ledgerType() {
    return LEDGER_TYPE;
  }

  static get refType() {
    return REF_TYPE;
  }

  constructor(private http: HttpService) { }

  static traderTypeMap(trader_type) {
    return ReceiptApi.traderType[trader_type];
  }

  static receiptTypeMap(receipt_type) {
    return ReceiptApi.receiptType[receipt_type];
  }

  static receiptStatusMap(receipt_status) {
    return ReceiptApi.receiptStatus[receipt_status];
  }

  static ledgerTypeMap(ledger_type) {
    return ReceiptApi.ledgerType[ledger_type];
  }

  static refTypeMap(ref_type) {
    return ReceiptApi.refType[ref_type];
  }

  getReceipt(id) {
    return this.http
      .post(`api/shop.Receipt/GetReceipt`, {id})
      .toPromise();
  }

  getReceipts(query: Partial<ListQueryDTO>): Promise<any> {
    return this.http
      .post('api/shop.Receipt/GetReceipts', query)
      .toPromise()
      .then(res => {
        res.receipts = res.receipts.map(r => this.receiptMap(r));
        return res;
      });
  }

  getReceiptsByLedgerType(type, query: Partial<ListQueryDTO>): Promise<any> {
    return this.http
      .post('api/shop.Receipt/GetReceiptsByLedgerType', { type, ...query })
      .toPromise()
      .then(res => {
        res.receipts = res.receipts.map(r => this.receiptMap(r));
        return res;
      });
  }

  createReceipt(data: Receipt): Promise<any> {
    data = this.parseNumberReceipt(data);
    return this.http
      .post('api/shop.Receipt/CreateReceipt', data)
      .toPromise();
  }

  deleteReceipt(id) {
    return this.http
      .post(`api/shop.Receipt/DeleteReceipt`, { id })
      .toPromise();
  }

  updateReceipt(data: Receipt): Promise<any> {
    data = this.parseNumberReceipt(data);
    return this.http
      .post(`api/shop.Receipt/UpdateReceipt`, data)
      .toPromise();
  }

  confirmReceipt(id: string): Promise<any> {
    return this.http
      .post(`api/shop.Receipt/ConfirmReceipt`, { id })
      .toPromise();
  }

  cancelReceipt(id: string, cancel_reason: string): Promise<any> {
    return this.http
      .post(`api/shop.Receipt/CancelReceipt`, { id, cancel_reason })
      .toPromise();
  }

  receiptMap(receipt: Receipt) {
    return new Receipt({
      ...receipt,
      type_display: ReceiptApi.receiptTypeMap(receipt.type) || '',
      status_display: ReceiptApi.receiptStatusMap(receipt.status) || '',
      ref_type_display: ReceiptApi.refTypeMap(receipt.ref_type) || '',
      trader_type: receipt.trader && receipt.trader.type || '',
      trader_type_display: receipt.trader
        && ReceiptApi.traderTypeMap(receipt.trader.type) || 'Không xác định',
      ledger_type: receipt.ledger && receipt.ledger.type || '',
      ledger_type_display: receipt.ledger
        && ReceiptApi.ledgerTypeMap(receipt.ledger.type) || '',
      user_display: receipt.created_type == 'auto' && 'Hệ thống'
        || (receipt.user && receipt.user.full_name || '')
    });
  }

  private parseNumberReceipt(receipt: Receipt): Receipt {
    receipt.amount = Number(receipt.amount);
    if (receipt.lines && receipt.lines.length) {
      receipt.lines.forEach(l => {
        l.amount = Number(l.amount);
      });
    }
    return receipt;
  }

  getLedgers(query: Partial<ListQueryDTO>): Promise<Array<Ledger>> {
    return this.http
      .post('api/shop.Ledger/GetLedgers', query)
      .toPromise()
      .then(res => res.ledgers);
  }

  createLedger(data): Promise<any> {
    return this.http
      .post('api/shop.Ledger/CreateLedger', data)
      .toPromise();
  }

  updateLedger(data): Promise<any> {
    return this.http
      .post('api/shop.Ledger/UpdateLedger', data)
      .toPromise();
  }

  deleteLedger(id: string): Promise<any> {
    return this.http
      .post('api/shop.Ledger/DeleteLedger', { id })
      .toPromise();
  }

}
