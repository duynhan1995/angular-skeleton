import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { CursorPaging } from '@etop/models';

export enum PaymentMethod {
  UNKNOWN = 'unknown',
  COD = 'cod',
  BANK = 'bank',
  OTHER ='other',
  VTPAY = 'vtpay',
  MOMO = 'momo',
  BANKDEPOSIT = 'bankdeposit',
  MANUAL = 'manual',
  BALANCE = 'balance',
}

@Injectable({
  providedIn: 'root'
})
export class ExtensionApi {
  constructor(
    private http: HttpService
  ) {
  }

  getHotlines() {
    return this.http.post('api/shop.Etelecom/GetHotlines', {}).toPromise().then(res => res.hotlines);
  }

  async createExtension(hotline_id: string, user_id: string, extension_number?: number) {
    return await this.http.post('api/shop.Etelecom/CreateExtension', {hotline_id, user_id, extension_number}).toPromise();
  }

  async getExtensions(GetExtensionRequest?: ExtensionAPI.GetExtensionRequest) {
    return await this.http.post('api/shop.Etelecom/GetExtensions', GetExtensionRequest).toPromise()
      .then(res => res.extensions);
  }

  assignUserToExtension(extension_id: string, user_id: string) {
    return this.http.post('api/shop.Etelecom/AssignUserToExtension', {extension_id, user_id}).toPromise();
  }

  removeUserOfExtension(extension_id: string, user_id: string) {
    return this.http.post('api/shop.Etelecom/RemoveUserOfExtension', {extension_id, user_id}).toPromise();
  }

  createExtensionBySubscription(request: ExtensionAPI.CreateExtensionBySubscriptionRequest) {
    return this.http.post('api/shop.Etelecom/CreateExtensionBySubscription', request).toPromise();
  }

  extendExtension(request: ExtensionAPI.ExtendExtensionRequest) {
    return this.http.post('api/shop.Etelecom/ExtendExtension', request).toPromise();
  }
}


export namespace ExtensionAPI {
  export interface GetExtensionFilter {
    hotline_id?: string;
  }

  export interface GetExtensionRequest {
    filter?: GetExtensionFilter;
    paging?: CursorPaging
  }

  export interface CreateExtensionBySubscriptionRequest {
    extension_number?: number;
    hotline_id: string;
    payment_method: PaymentMethod;
    subscription_id?: string;
    subscription_plan_id: string;
    user_id?: string;
  }

  export interface ExtendExtensionRequest {
    extension_id?: string;
    payment_method: PaymentMethod;
    subscription_id?: string;
    subscription_plan_id: string;
    user_id?: string;
  }
}
