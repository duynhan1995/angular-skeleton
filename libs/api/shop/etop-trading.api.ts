import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { Order } from 'libs/models/Order';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { Product } from 'libs/models/Product';
import { Promotion } from 'libs/models/affiliate/Promotion';
import { Referral } from 'libs/models/affiliate/Referral';

@Injectable({ providedIn: 'root' })
export class ETopTradingApi {
  constructor(
    private http: HttpService,
    private ubtService: UserBehaviourTrackingService
  ) {}

  async getTradingProducts(query: Partial<ListQueryDTO>, token?: string) {
    return this.http
      .post(`api/affiliate.Shop/ShopGetProducts`, query, token)
      .toPromise()
      .then(res => res.products.map(p => {
        return {
          product: new Product({
            ...p.product,
            retail_price: p.product.variants[0]?.retail_price,
            list_price: p.product.variants[0]?.list_price,
            cost_price: p.product.variants[0]?.cost_price
          }),
          promotion: p.promotion && new Promotion({
            ...p.promotion,
            amount: p.promotion.unit == 'percent' ?  p.promotion.amount / 100 : p.promotion.amount
          }),
        }
      }));
  }

  getTradingProduct(id, token?) {
    return this.http
      .post(`api/shop.Trading/TradingGetProduct`, { id })
      .toPromise()
      .then(res => res);
  }

  getTradingOrders(
    query: Partial<ListQueryDTO>,
    token?: string
  ): Promise<Array<Order>> {
    return this.http
      .post(`api/shop.Trading/TradingGetOrders`, query, token)
      .toPromise()
      .then(res => res.orders);
  }
  getTradingOrder(id, token?): Promise<Order> {
    return this.http
      .post(`api/shop.Trading/TradingGetOrder`, { id })
      .toPromise()
      .then(res => res);
  }

  createOrder(body, token?) {
    const options = this.getOptions(token);
    return this.http
      .postWithOptions('api/shop.Trading/TradingCreateOrder', body, options)
      .toPromise()
      .then(res => {
        if (res.id) {
          this.ubtService.sendUserBehaviour('CreateOrder');
        }
        return res;
      });
  }

  getOptions(token) {
    const headers = this.http.createHeader(token);
    const options = this.http.createDefaultOption(headers);
    return options;
  }

  TradingPaymentOrder(body) {
    return this.http
      .post(`api/shop.Payment/PaymentTradingOrder`, body)
      .toPromise()
      .then(res => res);
  }

  PaymentCheckReturnData(body) {
    return this.http
      .post(`api/shop.Payment/PaymentCheckReturnData`, body)
      .toPromise()
      .then(res => res);
  }

  GetProductPromotion(body) {
    return this.http
      .post(`api/affiliate.Shop/GetProductPromotion`, body)
      .toPromise()
      .then(res => {
        return {
          promotion: new Promotion({
            ...res.promotion,
            amount: res.promotion.unit == 'percent' ? res.promotion.amount / 100 : res.promotion.amount
          })
        }
      });
  }

  CheckReferralCodeValid(body) {
    return this.http
      .post(`api/affiliate.Shop/CheckReferralCodeValid`, body)
      .toPromise()
      .then(res => {
        return {
          promotion: res.promotion && new Promotion({
            ...res.promotion,
            amount: res.promotion.unit == 'percent' ? res.promotion.amount / 100 : res.promotion.amount
          }),
          referral_discount: res.referral_discount && new Referral(res.referral_discount)
        };
      });
  }

  GetTradingProductPromotions(
    query: Partial<ListQueryDTO>,
    token?: string
  ): Promise<Array<Order>> {
    return this.http
      .post(`api/affiliate.Trading/GetTradingProductPromotions`, query, token)
      .toPromise()
      .then(res => res.promotions);
  }
}
