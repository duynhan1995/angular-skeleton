import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { HttpService } from '@etop/common';
import { AuthenticateStore } from '@etop/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ReportApi {
  constructor(
    private http: HttpClient,
    private authenServer: AuthenticateStore,
    @Optional() @Inject("SERVICE_URL") private SERVICE_URL: string
  ) {
    
  }

  private getUrl(link) {
    let host = !/http(s?)/i.test(link) && (__debug_host || this.SERVICE_URL) || '';
    if (host) {
      if (!host.startsWith('http')) {
        let url = window.location.href;
        let arr = url.split('/');
        host = arr[0] + '//' + host;
      }
      if (!host.endsWith('/')) {
        host = host + '/';
      }
    }
    if (link.startsWith('/')) {
      link = link.slice(1);
    }
    return `${host}${link}`;
  }

  createHeader(token?: string) {
    const jwt = token || this.authenServer.snapshot.token;
    const header =  new Headers();
    header.append('Authorization', 'Bearer ' + jwt);
    header.append('Content-Type', 'application/json');
    return header;
  }
  createHttpHeader(token?: string) {
    const jwt = token || this.authenServer.snapshot.token;
    return new HttpHeaders()
      .set('Authorization', 'Bearer ' + jwt)
      .set('Content-Type', 'application/json');
  }

  createDefaultOption(headers?: any) {
    return {
      headers: headers || this.createHttpHeader(),
      params: null,
      observe: 'response'
    };
  }

  async getIncome(time_filter, year) {
    const header = this.createHeader();
    const link = this.getUrl(`/api/report/income-statement?time_filter=${time_filter}&year=${year}`)
    const res = await fetch(link,{
      method: 'get',
      headers: header
    }).then(function (response) {
      return response.text();
    })
    .then(function (html) {
    return html;
    });
    return res;
  }
  async getEnddayIncome(created_at) {
    const header = this.createHeader();
    const link = this.getUrl(`/api/report/orders/end-of-day?created_at=${created_at}`)
    const res = await fetch(link,{
      method: 'get',
      headers: header
    }).then(function (response) {
      return response.text();
    })
    .then(function (html) {
    return html;
    });
    return res;
  }
  async exportIncome(time_filter, year, file_type) {
    const httpOptions = {
      responseType: 'blob' as 'json',
      headers: this.createHttpHeader()
    };
    const link = this.getUrl(`/api/report/export/income-statement?time_filter=${time_filter}&year=${year}&file_type=${file_type}`)
    return this.http
      .get(link, httpOptions);
  }
  exportEnddayIncome(created_at, file_type) {
    const httpOptions = {
      responseType: 'blob' as 'json',
      headers: this.createHttpHeader()
    };
    const link = this.getUrl(`/api/report/export/orders/end-of-day?created_at=${created_at}&file_type=${file_type}`)
    return this.http
      .get(link, httpOptions);
  }
}


export namespace ReportAPI {
    
}