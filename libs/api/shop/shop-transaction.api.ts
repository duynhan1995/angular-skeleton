import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {CursorPaging} from '@etop/models';
import {Transaction} from '@etop/models/Transaction';

@Injectable({
  providedIn: 'root'
})
export class ShopTransactionApi {
  constructor(
    private http: HttpService,
  ) {
  }

  async getTransactions(request?: ShopTransactionAPI.GetTransactionRequest) {
    return this.http.post('api/shop.Transaction/GetTransactions', request)
      .toPromise().then(res => {
        res.transactions = res.transactions.map(transaction => Transaction.transactionMap(transaction))
        return res
      });
  }

}

export namespace ShopTransactionAPI {
  export interface GetTransactionFilter {
    date_from: string;
    date_to: string;
    ref_id: string;
    ref_type: string;
  }

  export interface GetTransactionRequest {
    filter?: GetTransactionFilter;
    paging?: CursorPaging
  }
}
