import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { Supplier } from 'libs/models/Supplier';

@Injectable()
export class SupplierApi {

  constructor(private http: HttpService) {}

  createSupplier(data: Supplier): Promise<any> {
    return this.http
      .post('api/shop.Supplier/CreateSupplier', data)
      .toPromise()
      .then(res => res);
  }

  deleteSupplier(id) {
    return this.http
      .post(`api/shop.Supplier/DeleteSupplier`, { id })
      .toPromise();
  }

  updateSupplier(data: Supplier): Promise<any> {
    return this.http
      .post(`api/shop.Supplier/UpdateSupplier`, data)
      .toPromise()
      .then(res => res);
  }

  getSupplier(id) {
    return this.http
      .post(`api/shop.Supplier/GetSupplier`, id)
      .toPromise()
      .then(res => res);
  }

  getSuppliers(query: Partial<ListQueryDTO>): Promise<{ paging: any; suppliers: Array<Supplier> }> {
    return this.http
      .post('api/shop.Supplier/GetSuppliers', query)
      .toPromise()
      .then(res => res);
  }

  getSuppliersByIds(ids: Array<string>) {
    return this.http
      .post('api/shop.Supplier/GetSuppliersByIDs', { ids })
      .toPromise()
      .then(res => res.suppliers);
  }

}
