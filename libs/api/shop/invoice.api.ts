import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {CursorPaging} from '@etop/models';
import {Invoice} from '@etop/models/Invoice';

@Injectable({
  providedIn: 'root'
})
export class InvoiceApi {
  constructor(
    private http: HttpService,
  ) {
  }

  async getInvoices(request?: InvoiceAPI.GetInvoiceRequest) {
    return this.http.post('api/shop.Invoice/GetInvoices', request).toPromise().then();
  }

  async getInvoice(id: String) {
    return this.http.post('api/shop.Invoice/GetInvoice', {id}).toPromise().then();
  }

}

export namespace InvoiceAPI {
  export interface GetInvoiceFilter {
    account_id: string;
    date_from: string;
    date_to: string;
    ref_id: string;
    ref_type: string;
    type: string;
  }

  export interface GetInvoiceRequest {
    filter?: GetInvoiceFilter;
    paging?: CursorPaging
  }
}
