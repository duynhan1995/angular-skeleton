import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {CursorPaging} from "@etop/models";

@Injectable({
  providedIn: 'root'
})
export class JiraApi {
  constructor(
    private http: HttpService,
  ) {
  }

  async createJiraIssue(request: JiraAPI.CreateJiraIssueRequest) {
    return await this.http.post('/api/shop.Jira/CreateJiraIssue', request).toPromise();
  }

  async getJiraCustomFields() {
    return await this.http.post('/api/shop.Jira/GetJiraCustomFields', {}).toPromise().then(
      res => res.custom_fields
    );
  }
}

export namespace JiraAPI {
  export interface JiraCustomField {
    key: string,
    value: string
  }
  export interface CreateJiraIssueRequest {
    custom_fields: JiraCustomField[],
    description: string,
    summary: string
  }
}

