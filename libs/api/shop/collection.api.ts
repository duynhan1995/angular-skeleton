import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';

export interface CollectionRequestDto {
  id?: string;
  name: string;
  description?: string;
  short_desc?: string;
  desc_html?: string;
}

@Injectable({
  providedIn: 'root'
})
export class CollectionApi {

  constructor(
    private http: HttpService
  ) {}

  createCollection(data: CollectionRequestDto) {
    return this.http.post('api/shop.Collection/CreateCollection', data)
      .toPromise()
      .then(res => {
        return res;
      });
  }

  deleteCollection(id: string) {
    return this.http.post('api/shop.Collection/DeleteCollection', { id })
      .toPromise()
      .then(res => {
        return res
      });
  }

  async getCollection(id) {
    return this.http.post('api/shop.Collection/GetCollection', { id })
      .toPromise();
  }

  async getCollections() {
    return this.http.post('api/shop.Collection/GetCollections', {})
      .toPromise()
      .then(res => res.collections);
  }

  getCollectionByIDs(ids: string[]) {
    return this.http.post('api/shop.Collection/GetCollectionsByIDs', { ids })
      .toPromise()
      .then(res => res.collections);
  }

  updateCollection(collection: CollectionRequestDto) {
    if (!collection.id) {
      throw new Error("collection.id is required")
    }
    return this.http.post('api/shop.Collection/UpdateCollection', collection).toPromise()
      .then(res => {
        return res;
      });
  }

  updateProductsCollection(collection_id: string, product_ids: string[]) {
    return this.http.post('api/shop.Collection/UpdateProductsCollection', {
      collection_id,
      product_ids
    }).toPromise();
  }

}
