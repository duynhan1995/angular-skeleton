import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {Customer, CustomerAddress} from 'libs/models/Customer'
import {ListQueryDTO} from 'libs/models/CommonQuery';
import {Order, OrderCustomer} from 'libs/models/Order';

@Injectable({
  providedIn: 'root'
})
export class CustomerApi {

  constructor(private http: HttpService) {
  }

  createCustomer(data: Customer | OrderCustomer): Promise<any> {
    return this.http
      .post('api/shop.Customer/CreateCustomer', data)
      .toPromise()
      .then(res => res);
  }

  deleteCustomer(id) {
    return this.http
      .post(`api/shop.Customer/DeleteCustomer`, {id})
      .toPromise();
  }

  updateCustomer(data: Customer): Promise<any> {
    return this.http
      .post(`api/shop.Customer/UpdateCustomer`, data)
      .toPromise()
      .then(res => res);
  }

  getCustomer(id): Promise<Customer> {
    return this.http.post(`api/shop.Customer/GetCustomer`, {id})
      .toPromise()
      .then(customer => this.customerMap(customer));
  }

  getCustomers(query: Partial<ListQueryDTO>, get_all = true): Promise<{ paging: any; customers: Array<Customer> }> {
    return this.http
      .post('api/shop.Customer/GetCustomers', {...query, get_all})
      .toPromise()
      .then(res => {
        res.customers = res.customers.map(c => this.customerMap(c));
        return res;
      });
  }

  getCustomersByIds(ids: Array<string>) {
    return this.http
      .post('api/shop.Customer/GetCustomersByIDs', {ids})
      .toPromise()
      .then(res => res.customers);
  }

  createCustomerAddress(data: Partial<CustomerAddress>): Promise<any> {
    return this.http
      .post(`api/shop.Customer/CreateCustomerAddress`, data)
      .toPromise()
      .then(res => res);
  }

  deleteCustomerAddress(id) {
    return this.http
      .post(`api/shop.Customer/DeleteCustomerAddress`, {id})
      .toPromise();
  }

  updateCustomerAddress(data: CustomerAddress): Promise<any> {
    return this.http
      .post(`api/shop.Customer/UpdateCustomerAddress`, data)
      .toPromise()
      .then(res => res);
  }

  getCustomerAddresses(customer_id: string) {
    return this.http
      .post(`api/shop.Customer/GetCustomerAddresses`, {customer_id})
      .toPromise()
      .then(res => res.addresses.map(addr => {
        addr.compare_text = addr.address1 + addr.ward_code + addr.district_code + addr.province_code;
        return addr;
      }));
  }

  customerMap(customer: Customer): Customer {
    return {
      ...customer,
      full_name: customer.type == 'independent' && 'Khách lẻ' || customer.full_name
    }
  }

}
