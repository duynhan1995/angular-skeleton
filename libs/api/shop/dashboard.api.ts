import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';

const DASHBOARD_SPEC = {
    total_count: 'count:Tổng số cuộc gọi',
    count: 'count:Số cuộc gọi',
    total_duration_call: 'sum(duration):Tổng thời lượng gọi',
    duration_call: 'sum(duration):Thời lượng gọi',
    total_duration_postage: 'sum(duration_postage):Tổng thời lượng gọi làm tròn',
    total_call_in: 'sum(duration):Tổng thời lượng nghe',
    total_fee: 'sum(postage):call_status = 1',
    total_call_completed: 'count:call_status = 1',
    total_call_failed: 'count:call_status != 1',
    duration_call_in: 'sum(duration):Thời lượng nghe'
  };

@Injectable({
  providedIn: 'root'
})
export class DashboardApi {
  constructor(
    private http: HttpService
  ) {
  }

  summaryEtelecom({ date_from, date_to }) {
    return this.http
        .post(`api/shop.Etelecom/SummaryEtelecom`, {date_from, date_to })
        .toPromise()
        .then(res => this._tableMap(res.tables))
    }

    private _tableMap(tables: any) {
        let _totalData = [];
        tables[0].data.forEach(_data => {
            _totalData.push(..._data)
        })
        return {
            total: {
                total_duration_call: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_duration_call),
                total_duration_postage: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_duration_postage),
                total_call_in: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_call_in),
                total_fee: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_fee),
                total_call_completed: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_call_completed),
                total_call_failed: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_call_failed),
                total_count: this._valueFromSpec(_totalData, DASHBOARD_SPEC.total_count),
            },
            staff: this._staffMap(tables[1])
        }
    }

    private _staffMap(data) {
        let extension_ids = data?.columns.map(column => column.spec.split(' ').pop());
        let _tables = [];
        let _convertData = []
        if (data?.data) {
            data?.data.forEach(_data => {
                _convertData.push(..._data)
            })
            extension_ids.forEach((extension_id, index) => {
                    _tables.push({
                        extension_id: extension_id,
                        total_duration: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.duration_call),
                        total_call: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.count),
                        total_fee: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.total_fee),
                        total_duration_postage: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.total_duration_postage),
                        total_call_completed: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.total_call_completed),
                        total_call_failed: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.total_call_failed),
                        duration_call_in: this._columnMap(_convertData, extension_id, DASHBOARD_SPEC.duration_call_in)
                    })

            })
        }
        return _tables;
    }

    private _columnMap(data, extension_id, spec) {
        let _data = data.find(d => d.spec.includes(spec) && d.spec.includes(extension_id))
        return _data?.value
    }

    private _valueFromSpec(data, spec) {
        let _data = data.find(d => d.spec.includes(spec))
        return _data?.value
    }
}
