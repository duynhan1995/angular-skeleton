import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { AuthenticateStore } from '@etop/core';
import {Hotline} from "@etop/models";

@Injectable({
  providedIn: 'root'
})
export class HotlineApi {
  constructor(
    private http: HttpService,
    private auth: AuthenticateStore
  ) {
  }

  async getHotlines() {
    return await this.http.post('api/shop.Etelecom/GetHotlines', {}).toPromise().then(
      (res) => {
        res.hotlines = res.hotlines.map((hotline) => Hotline.hotlineMap(hotline));
        return res.hotlines;
      });
  }

}
