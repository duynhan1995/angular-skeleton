import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';

@Injectable({
  providedIn: 'root'
})
export class TenantApi {
  constructor(
    private http: HttpService,
  ) {
  }

  async getTenant() {
    return this.http.post('api/shop.Etelecom/GetTenant', {}).toPromise();
  }

  async createTenant() {
    return this.http.post('api/shop.Etelecom/CreateTenant', {}).toPromise();
  }

}
