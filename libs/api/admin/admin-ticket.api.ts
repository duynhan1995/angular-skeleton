import {Injectable} from '@angular/core';
import {HttpService} from "libs/common/services";
import {Paging} from "@etop/shared";
import {Ticket, TicketRefType, TicketState} from "@etop/models";

@Injectable({
  providedIn: 'root'
})
export class AdminTicketApi {

  constructor(
    private http: HttpService
  ) {
  }

  createTicketLabel(request: AdminTicketAPI.CreateTicketLabelRequest) {
    return this.http.post('api/admin.Ticket/CreateTicketLabel', request).toPromise();
  }

  updateTicketLabel(request: AdminTicketAPI.UpdateTicketLabelRequest) {
    return this.http.post('api/admin.Ticket/UpdateTicketLabel', request).toPromise();
  }

  deleteTicketLabel(id: string) {
    return this.http.post('api/admin.Ticket/DeleteTicketLabel', {id}).toPromise();
  }

  getTickets(request: AdminTicketAPI.GetTicketsRequest) {
    return this.http.post('api/admin.Ticket/GetTickets', request).toPromise()
      .then(res => res.tickets.map(t => Ticket.ticketMap(t)));
  }

  assignTicket(assigned_user_ids: string[], ticket_id: string) {
    return this.http.post('api/admin.Ticket/AssignTicket', {assigned_user_ids, ticket_id})
      .toPromise();
  }

  confirmTicket(ticket_id: string, note?: string) {
    return this.http.post('api/admin.Ticket/ConfirmTicket', {ticket_id, note})
      .toPromise();
  }

  closeTicket(state: TicketState, ticket_id: string, note?: string) {
    return this.http.post('api/admin.Ticket/CloseTicket', {state, ticket_id, note})
      .toPromise();
  }

  getTicketComments(request: AdminTicketAPI.GetTicketCommentsRequest) {
    return this.http.post('api/admin.Ticket/GetTicketComments', request).toPromise()
      .then(res => res.ticket_comments);
  }

  createTicketComment(request: AdminTicketAPI.CreateTicketCommentRequest) {
    return this.http.post('api/admin.Ticket/CreateTicketComment', request).toPromise();
  }

  updateTicketComment(request: AdminTicketAPI.UpdateTicketCommentRequest) {
    return this.http.post('api/admin.Ticket/UpdateTicketComment', request).toPromise();
  }

  getTicketLabels(tree: boolean) {
    return this.http.post('api/admin.Ticket/GetTicketLabels', {tree}).toPromise()
      .then(res => res.ticket_labels);
  }

}

export namespace AdminTicketAPI {
  export interface CreateTicketLabelRequest {
    name: string;
    code: string;
    color?: string;
    parent_id?: string;
  }

  export interface UpdateTicketLabelRequest extends CreateTicketLabelRequest {
    id: string;
  }

  export interface GetTicketsRequest {
    filter?: GetTicketsFilter;
    paging: Paging;
  }

  export interface GetTicketsFilter {
    account_id?: string;
    assigned_user_id?: string[];
    closed_by?: string;
    code?: string;
    created_by?: string;
    ids?: string[];
    label_ids?: string[];
    ref_code?: string;
    ref_id?: string;
    ref_type?: TicketRefType;
    state?: TicketState;
    title?: string;
  }

  export interface GetTicketCommentsRequest {
    filter?: GetTicketCommentsFilter;
    paging: Paging;
  }

  export interface GetTicketCommentsFilter {
    account_id?: string;
    created_by?: string;
    ids?: string[];
    parent_id?: string;
    ticket_id: string;
    title?: string;
  }

  export interface CreateTicketCommentRequest {
    account_id: string;
    ticket_id: string;

    image_url?: string;
    message?: string;
    parent_id?: string;
  }

  export interface UpdateTicketCommentRequest {
    account_id: string;
    id: string;

    image_url?: string;
    message?: string;
  }
}
