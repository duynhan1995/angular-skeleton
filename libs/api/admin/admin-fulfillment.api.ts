import { Injectable } from '@angular/core';
import {ShippingState} from 'libs/models/Fulfillment';
import {FeeType} from 'libs/models/Fee';
import { HttpService } from '@etop/common';
import {Paging} from "@etop/shared";
import { Filters } from '@etop/models';

export class AdminFulfillmentsQuery {
  paging: Paging;
  filters?: Filters;
  shop_id?: string;
  order_id?: string;
  status?: string;
}

@Injectable({ providedIn: 'root' })
export class AdminFulfillmentApi {

  constructor(
    private http: HttpService
  ) { }

  getFulfillment(id: string) {
    return this.http
      .post('api/admin.Fulfillment/GetFulfillment', { id })
      .toPromise();
  }

  getFulfillments(query: AdminFulfillmentsQuery) {
    return this.http
      .post('api/admin.Fulfillment/GetFulfillments', query)
      .toPromise();
  }

  updateFulfillmentInfo(body: AdminFulfillmentAPI.UpdateFulfillmentInfoRequest) {
    return this.http.post('/api/admin.Fulfillment/UpdateFulfillmentInfo', body)
      .toPromise();
  }

  updateFulfillmentCODAmount(body: AdminFulfillmentAPI.UpdateFulfillmentCODAmountRequest) {
    return this.http.post('/api/admin.Fulfillment/UpdateFulfillmentCODAmount', body)
      .toPromise();
  }

  updateFulfillmentShippingFees(body: AdminFulfillmentAPI.UpdateFulfillmentShippingFeesRequest) {
    return this.http.post('/api/admin.Fulfillment/UpdateFulfillmentShippingFees', body)
      .toPromise();
  }

  updateFulfillmentShippingState(body: AdminFulfillmentAPI.UpdateFulfillmentShippingStateRequest) {
    return this.http.post('/api/admin.Fulfillment/UpdateFulfillmentShippingState', body)
      .toPromise();
  }

  addShippingFee(body: AdminFulfillmentAPI.AddShippingFee) {
    return this.http.post('/api/admin.Fulfillment/AddShippingFee', body)
      .toPromise();
  }

}

export namespace AdminFulfillmentAPI {

  export interface UpdateFulfillmentRequest {
    id: string;
    shipping_code?: string;
    admin_note: string;
  }

  export interface UpdateFulfillmentInfoRequest extends UpdateFulfillmentRequest {
    full_name?: string;
    phone?: string;
  }

  export interface UpdateFulfillmentCODAmountRequest extends UpdateFulfillmentRequest {
    total_cod_amount?: number;
    is_partial_delivery?: boolean;
  }

  export interface UpdateFulfillmentShippingFeesRequest extends UpdateFulfillmentRequest {
    shipping_fee_lines?: {
      cost: number;
      shipping_fee_type: FeeType;
    }[];
  }

  export interface UpdateFulfillmentShippingStateRequest extends UpdateFulfillmentRequest {
    actual_compensation_amount?: number;
    shipping_state?: ShippingState;
  }

  export interface AddShippingFee {
    id: string;
    shipping_code?: string;
    shipping_fee_type: FeeType;
  }

}
