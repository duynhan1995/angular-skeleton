import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {CursorPaging} from '@etop/models';
import {Invoice} from '@etop/models/Invoice';

@Injectable({
  providedIn: 'root'
})
export class AdminInvoiceApi {
  constructor(
    private http: HttpService,
  ) {
  }

  async getInvoices(request?: AdminInvoiceAPI.GetAdminInvoiceRequest) {
    return this.http.post('api/admin.Invoice/GetInvoices', request).toPromise();
  }

}

export namespace AdminInvoiceAPI {
  export interface GetInvoiceFilter {
    account_id?: string;
    date_from?: string;
    date_to?: string;
    ref_id?: string;
    ref_type?: string;
    type?: string;
  }

  export interface GetAdminInvoiceRequest {
    filter?: GetInvoiceFilter;
    paging?: CursorPaging
  }
}
