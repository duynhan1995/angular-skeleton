import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { CursorPaging, Hotline } from '@etop/models';

@Injectable({
  providedIn: 'root'
})
export class HotlinesApi {
  constructor(
    private http: HttpService
  ) {
  }

  async getHotlines(getHotlinesRequest?: HotlinesAPI.GetHotlinesRequest) {
    return await this.http.post('api/admin.Etelecom/GetHotlines', getHotlinesRequest).toPromise()
    .then((res) => {
      res.hotlines = res.hotlines.map((hotline) => Hotline.hotlineMap(hotline));
      return res;
    });
  }

  createHotline(request: HotlinesAPI.CreateHotlineRequest) : Promise<Hotline>{
    return this.http.post('api/admin.Etelecom/CreateHotline', request).toPromise();
  }

  updateHotline(request: HotlinesAPI.UpdateHotlinelineRequest) : Promise<Hotline>{
    return this.http.post('api/admin.Etelecom/UpdateHotline', request).toPromise();
  }

  addHotlineToTenant(hotline_id: string, tenant_id: string) : Promise<Hotline>{
    return this.http.post('api/admin.Etelecom/AddHotlineToTenant', {hotline_id, tenant_id}).toPromise();
  }

  deleteHotline(id: string) {
    return this.http.post('api/admin.Etelecom/DeleteHotline', {id}).toPromise().then();
  }

}

export namespace HotlinesAPI {
    export class GetHotlinesRequest {
      filter?: GetHotlinesFilter;
      paging?: CursorPaging
    }
    export class GetHotlinesFilter {
      tenant_id?: string;
      owner_id?: string;
    }
    export class CreateHotlineRequest {
      description?: string;
      hotline?: string;
      is_free_charge?: boolean;
      name?: string;
      network?: string;
      owner_id?:string;
    }
    export class UpdateHotlinelineRequest {
      description?: string;
      id?: string;
      is_free_charge?: boolean;
      name?: string;
      network?: string;
      status?: string;
    }
  }
