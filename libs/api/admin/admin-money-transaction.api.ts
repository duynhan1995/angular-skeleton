import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { MoneyTransactionShippingExternal } from 'libs/models/admin/MoneyTransactionShippingExternal';
import { MoneyTransactionShop } from 'libs/models/MoneyTransactionShop';
import { BankAccount } from 'libs/models/Bank';
import { MoneyTransactionTransfer } from 'libs/models/admin/MoneyTransactionTransfer';
import { Paging} from "@etop/shared";
import { Filters } from '@etop/models';

export class AdminTransactionsQuery {
  paging: Paging;
  filters?: Filters;
  ids?: string[];
  money_transaction_shipping_external_id?: string;
  shop_id?: string;
}

const PROVIDER_LOGO_SM = {
  ghn: 'provider_logos/ghn-s.png',
  ghtk: 'provider_logos/ghtk-s.png',
  vtpost: 'provider_logos/vtpost-s.png',
  default: 'placeholder_medium.png'
};

const PROVIDER_FULLNAME = {
  ghn: 'Giao hàng nhanh',
  ghtk: 'Giao hàng tiết kiệm',
  vtpost: 'Viettel Post'
};

const STATUS = {
  P: 'Đã xác nhận',
  S: 'Chờ xác nhận',
  Z: 'Chưa xác nhận',
  N: 'Huỷ'
};

const TRANSFER_FEE = {
  vietcombank: 3300,
  techcombank: 5500,
  liennganhang: 5500
};

const BANGKE_TEMPLATE = {
  not_techcombank_headers: [
    "Txn Reference",
    "Amount (VND)",
    "Benificiary Name",
    "Account Number",
    "Remarks",
    "Ben Bank",
    "Province",
    "Branch",
    "Validation"
  ],
  techcombank_headers: [
    "TÀI KHOẢN ĐÍCH",
    "SỐ TIỀN CHUYỂN",
    "DIỄN GIẢI"
  ]
};

@Injectable({
  providedIn: 'root'
})
export class AdminMoneyTransactionApi {

  static get providerLogoSm() {
    return PROVIDER_LOGO_SM;
  }

  static get providerFullname() {
    return PROVIDER_FULLNAME;
  }

  static get status() {
    return STATUS;
  }

  static get transferFee() {
    return TRANSFER_FEE;
  }

  static get bangkeTemplate() {
    return BANGKE_TEMPLATE;
  }

  constructor(
    private http: HttpService,
  ) { }

  static getProviderLogo(provider: string) {
    return `assets/images/${AdminMoneyTransactionApi.providerLogoSm[
      provider
      ] || AdminMoneyTransactionApi.providerLogoSm.default}`;
  }

  static getProviderFullname(provider: string) {
    return AdminMoneyTransactionApi.providerFullname[provider];
  }

  static statusDisplayMap(status: string) {
    return AdminMoneyTransactionApi.status[status];
  }

  // NOTE: phiên NVC
  private static mtShippingExternalMap(mt: MoneyTransactionShippingExternal): MoneyTransactionShippingExternal {
    if (!mt) { return null; }

    return {
      ...mt,
      status_display: AdminMoneyTransactionApi.statusDisplayMap(mt.status),
      carrier: mt.provider
    }
  }

  // NOTE: phiên SHOP
  private static mtShopMap(mt: MoneyTransactionShop): MoneyTransactionShop {
    if (!mt) { return null; }
    return new MoneyTransactionShop({
      ...mt,
      status_display: AdminMoneyTransactionApi.statusDisplayMap(mt.status),
      provider_logo: AdminMoneyTransactionApi.getProviderLogo(mt.provider),
      provider_fullname: AdminMoneyTransactionApi.getProviderFullname(mt.provider),
      money_transaction_shipping_etop_id: mt.money_transaction_shipping_etop_id == '0' ? null : mt.money_transaction_shipping_etop_id
    });
  }

  // NOTE: phiên CK
  private static mtTransferMap(mt: MoneyTransactionTransfer): MoneyTransactionTransfer {
    if (!mt) { return null; }
    return new MoneyTransactionTransfer({
      ...mt,
      status_display: AdminMoneyTransactionApi.statusDisplayMap(mt.status),
      money_transactions: mt.money_transactions.map(t => AdminMoneyTransactionApi.mtShopMap(t))
    })
  }

  importMoneyTransactionShippingExternal(form: FormData) {
    return this.http.postForm('api/admin.Import/MoneyTxShippingExternal', form).toPromise();
  }

  getMoneyTransactionShippingExternal(id: string) {
    return this.http
      .post('api/admin.MoneyTransaction/GetMoneyTransactionShippingExternal', {id})
      .toPromise()
      .then(mt => AdminMoneyTransactionApi.mtShippingExternalMap(mt));
  }

  getMoneyTransactionShippingExternals(query: AdminTransactionsQuery) {
    return this.http
      .post('api/admin.MoneyTransaction/GetMoneyTransactionShippingExternals', query)
      .toPromise()
      .then(res => res.money_transactions.map(mt => AdminMoneyTransactionApi.mtShippingExternalMap(mt)));
  }

  confirmMoneyTransactionShippingExternals(ids: string[]) {
    return this.http.post("api/admin.MoneyTransaction/ConfirmMoneyTransactionShippingExternals", {ids})
      .toPromise();
  }

  deleteMoneyTransactionShippingExternal(id: string) {
    return this.http.post("api/admin.MoneyTransaction/DeleteMoneyTransactionShippingExternal", {id})
      .toPromise();
  }

  splitMoneyTransactionShippingExternal(id: string) {
    return this.http.post("api/admin.MoneyTransaction/SplitMoneyTransactionShippingExternal", {
      id, is_split_by_shop_priority: true, max_money_tx_shipping_count: 2
    }).toPromise();
  }

  removeMoneyTransactionShippingExternalLines(line_ids: string[], money_transaction_shipping_external_id: string) {
    return this.http.post(`api/admin.MoneyTransaction/RemoveMoneyTransactionShippingExternalLines`, {
      line_ids, money_transaction_shipping_external_id
    }).toPromise();
  }

  importFile(form: FormData, carrier: string) {
    const api = `api/admin.Import/${carrier}/MoneyTransactions`;
    return this.http.postForm(api, form)
      .toPromise();
  }

  getMoneyTransactionShop(id: string) {
    return this.http
      .post('api/admin.MoneyTransaction/GetMoneyTransaction', {id})
      .toPromise()
      .then(mt => AdminMoneyTransactionApi.mtShopMap(mt));
  }

  getMoneyTransactionShops(query: AdminTransactionsQuery) {
    return this.http
      .post('api/admin.MoneyTransaction/GetMoneyTransactions', query)
      .toPromise()
      .then(res => res.money_transactions.map(mt => AdminMoneyTransactionApi.mtShopMap(mt)));
  }

  confirmMoneyTransactionShop(body: TransactionAPI.ConfirmMoneyTransactionShopRequest) {
    return this.http
      .post("api/admin.MoneyTransaction/ConfirmMoneyTransaction", body)
      .toPromise();
  }

  createMoneyTransactionTransfer(ids: string[]) {
    return this.http.post('api/admin.MoneyTransaction/CreateMoneyTransactionShippingEtop', { ids })
      .toPromise();
  }

  updateMoneyTransactionTransfer(body: Partial<TransactionAPI.UpdateMoneyTransactionTransferRequest>) {
    return this.http.post('api/admin.MoneyTransaction/UpdateMoneyTransactionShippingEtop', body)
      .toPromise();
  }

  getMoneyTransactionTransfers(query: AdminTransactionsQuery) {
    return this.http
      .post('api/admin.MoneyTransaction/GetMoneyTransactionShippingEtops', query)
      .toPromise()
      .then(res => res.money_transaction_shipping_etops
        .map(mt => AdminMoneyTransactionApi.mtTransferMap(mt)));
  }

  getMoneyTransactionTransfer(id: string) {
    return this.http
      .post('api/admin.MoneyTransaction/GetMoneyTransactionShippingEtop', {id})
      .toPromise()
      .then(mt => AdminMoneyTransactionApi.mtTransferMap(mt));
  }

  confirmMoneyTransactionTransfer(body: TransactionAPI.ConfirmMoneyTransactionTransferRequest) {
    return this.http
      .post("api/admin.MoneyTransaction/ConfirmMoneyTransactionShippingEtop", body)
      .toPromise();
  }

  deleteMoneyTransactionTransfer(id: string) {
    return this.http
      .post("api/admin.MoneyTransaction/DeleteMoneyTransactionShippingEtop", {id})
      .toPromise();
  }

}

export namespace TransactionAPI {
  export interface ConfirmMoneyTransactionShopRequest {
    money_transaction_id: string;
    shop_id: string;
    total_amount: number;
    total_cod: number;
    total_orders: number;
  }

  export interface ConfirmMoneyTransactionTransferRequest {
    id: string;
    total_amount: number;
    total_cod: number;
    total_orders: number;
  }

  export interface UpdateMoneyTransactionTransferRequest {
    adds: string[];
    bank_account: BankAccount,
    deletes: string[];
    id: string;
    invoice_number: string;
    note: string;
    replace_all: string[];
  }
}
