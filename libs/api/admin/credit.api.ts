import { Injectable } from '@angular/core';
import {HttpService} from "@etop/common";
import { Filter, Filters } from '@etop/models';
import {Paging} from "@etop/shared";
import {Credit} from "libs/models/admin/Credit";

const STATUS = {
  P: 'Đã xác nhận',
  Z: 'Chưa xác nhận'
};

export enum CLASSIFY {
  telecom = 'telecom',
  shipping = 'shipping'
};

@Injectable({
  providedIn: 'root'
})
export class CreditApi {

  static get status() {
    return STATUS;
  }

  constructor(
    private http: HttpService
  ) { }

  static statusDisplayMap(status) {
    return CreditApi.status[status];
  }

  static creditMap(credit: Credit) {
    return {
      ...credit,
      status_display: CreditApi.statusDisplayMap(credit.status)
    }
  }

  getCredits(request: CreditAPI.GetCreditsRequest) {
    return this.http
      .post('api/admin.Credit/GetCredits', request)
      .toPromise()
      .then(res => res.credits.map(conn => CreditApi.creditMap(conn)));
  }

  deleteCredit(id: string) {
    return this.http
      .post('api/admin.Credit/DeleteCredit', {id})
      .toPromise();
  }

  createCredit(request: CreditAPI.CreateCreditRequest) {
    return this.http
      .post('api/admin.Credit/CreateCredit', request)
      .toPromise();
  }

  confirmCredit(id: string) {
    return this.http
      .post('api/admin.Credit/ConfirmCredit', {id})
      .toPromise();
  }


}

export namespace CreditAPI {

  export interface GetCreditsRequest {
    paging: Paging;
    shop_id?: string;
    filter?: GetCreditsFilters
  }

  export interface CreateCreditRequest {
    amount: number,
    classify: string,
    paid_at: Date,
    shop_id: string,
    type: string;
  }

  export interface GetCreditsFilters {
    classify?: string;
    shop_id?: string;
  }

}
