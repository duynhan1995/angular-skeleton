import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {
  AvailableLocation,
  BaseValueType,
  CalculationMethod,
  CustomRegionType,
  PriceModifierType, PromotionAppliedRules,
  ProvinceType,
  RegionType,
  ShipmentPrice,
  ShipmentPriceAdditionalFee,
  ShipmentPriceDetail, ShipmentPriceListPromotion,
  ShipmentPriceLocation,
  ShipmentPriceUrban,
  ShipmentService,
  UrbanType
} from 'libs/models/admin/ShipmentPrice';
import {FeeType} from "libs/models/Fee";
import {Paging} from "@etop/shared";

const STATUS = {
  P: 'Đang hoạt động',
  Z: 'Tắt',
  N: 'Tắt'
};

const SHIPMENT_PRICE_PROVINCE_TYPE = {
  noi_tinh: "Nội tỉnh",
  lien_tinh: "Liên tỉnh"
};

const SHIPMENT_PRICE_REGION_TYPE = {
  noi_mien: "Nội miền",
  lien_mien: "Liên miền",
  can_mien: "Cận miền"
};

const SHIPMENT_PRICE_CUSTOM_REGION_TYPE = {
  noi_vung: "Nội vùng",
  lien_vung: "Liên vùng"
};

const SHIPMENT_PRICE_URBAN_TYPE = {
  noi_thanh: "Nội thành",
  ngoai_thanh_1: "Ngoại thành 1",
  ngoai_thanh_2: "Ngoại thành 2",
  unknown: "Không xác định",
};

const SHIPMENT_PRICE_ADDITIONAL_FEE_TYPE = {
  main: 'Chính',
  insurance: 'Bảo hiểm',
  cods: 'Thu hộ',
  redelivery: 'Kích hoạt giao lại',
  return: 'Trả hàng',
  adjustment: 'Đổi thông tin'
};

const PRICE_MODIFIER_TYPE = {
  percentage: '%',
  fixed_amount: 'đ'
}

const BASE_VALUE_TYPE = {
  cod_amount: "giá trị thu hộ",
  basket_value: "giá trị khai giá",
  main_fee: "phí chính"
}

@Injectable({
  providedIn: 'root'
})
export class ShipmentPriceApi {

  static get status() {
    return STATUS;
  }

  static get shipmentPriceProvinceType() {
    return SHIPMENT_PRICE_PROVINCE_TYPE;
  }

  static get shipmentPriceRegionType() {
    return SHIPMENT_PRICE_REGION_TYPE;
  }

  static get shipmentPriceCustomRegionType() {
    return SHIPMENT_PRICE_CUSTOM_REGION_TYPE;
  }

  static get shipmentPriceUrbanType() {
    return SHIPMENT_PRICE_URBAN_TYPE;
  }

  static get shipmentPriceAdditionalFeeType() {
    return SHIPMENT_PRICE_ADDITIONAL_FEE_TYPE;
  }

  static get priceModifierType() {
    return PRICE_MODIFIER_TYPE;
  }

  static get baseValueType() {
    return BASE_VALUE_TYPE;
  }

  constructor(
    private http: HttpService,
  ) { }

  static statusMap(status: string) {
    return ShipmentPriceApi.status[status];
  }

  static shipmentPriceProvinceTypeMap(type: string) {
    return ShipmentPriceApi.shipmentPriceProvinceType[type];
  }

  static shipmentPriceRegionTypeMap(type: string) {
    return ShipmentPriceApi.shipmentPriceRegionType[type];
  }

  static shipmentPriceCustomRegionTypeMap(type: string) {
    return ShipmentPriceApi.shipmentPriceCustomRegionType[type];
  }

  static shipmentPriceUrbanTypeMap(type: string) {
    return ShipmentPriceApi.shipmentPriceUrbanType[type];
  }

  static shipmentPriceAdditionalFeeTypeMap(type: FeeType) {
    return ShipmentPriceApi.shipmentPriceAdditionalFeeType[type];
  }

  static priceModifierTypeMap(type: PriceModifierType) {
    return ShipmentPriceApi.priceModifierType[type];
  }

  static baseValueTypeMap(type: BaseValueType) {
    return ShipmentPriceApi.baseValueType[type];
  }

  static mapBaseValueTypeByFeeType(feeType: FeeType) {

    if (feeType == FeeType.cods) {
      return BaseValueType.cod_amount;
    }
    if (feeType == FeeType.insurance) {
      return BaseValueType.basket_value;
    }

    return BaseValueType.main_fee;
  }

  private static mapShipmentService(service: ShipmentService): ShipmentService {
    return new ShipmentService({
      ...service,
      status: service.status == 'N' ? 'Z' : service.status,
      status_display: ShipmentPriceApi.statusMap(service.status)
    });
  }

  static mapShipmentPrice(shipmentPrice: ShipmentPrice): ShipmentPrice {
    const location_mapped = ShipmentPriceApi.shipmentPriceLocationMap(shipmentPrice);
    if (location_mapped) {
      shipmentPrice.location = location_mapped.location;
      shipmentPrice.location_display = location_mapped.location_display;
    }
    if (shipmentPrice.location == ShipmentPriceLocation.advanced) {
      const {region_types, custom_region_types, custom_region_ids, province_types} = shipmentPrice;
      shipmentPrice.latest_location_type = {
        region_types, custom_region_types, custom_region_ids, province_types
      };
    }

    if (shipmentPrice.additional_fees?.length) {
      shipmentPrice.additional_fees.forEach(fee => {
        if (fee.calculation_method == CalculationMethod.unknown) {
          fee.calculation_method = CalculationMethod.first_satisfy;
        }

        fee.base_value_type = ShipmentPriceApi.mapBaseValueTypeByFeeType(fee.fee_type);

        fee.fee_type_display = ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(fee.fee_type);
        fee.base_value_type_display = ShipmentPriceApi.baseValueTypeMap(fee.base_value_type);

        if (fee.rules?.length) {
          fee.rules.forEach(rule => {
            rule.price_modifier_type_display = ShipmentPriceApi.priceModifierTypeMap(rule.price_modifier_type);
          });
        }
      });
    }

    return new ShipmentPrice({
      ...shipmentPrice,
      urban_type_display: shipmentPrice.urban_types.length == 1 ?
        ShipmentPriceApi.shipmentPriceUrbanTypeMap(shipmentPrice.urban_types[0]) :
        'Tất cả',
      urban: shipmentPrice.urban_types.length == 1 ?
        shipmentPrice.urban_types[0] :
        ShipmentPriceUrban.all
    });
  }

  private static shipmentPriceLocationMap(shipment_price: ShipmentPrice) {
    let { custom_region_types, province_types, region_types } = shipment_price;
    custom_region_types = custom_region_types.filter(t => !!t);
    province_types = province_types.filter(t => !!t);
    region_types = region_types.filter(t => !!t);

    const advanced_option = custom_region_types.length ||
      (province_types.length + region_types.length) > 1;
    if (advanced_option) {
      return {
        location: ShipmentPriceLocation.advanced,
        location_display: 'Tuỳ chọn nâng cao'
      };
    }
    if (province_types.length) {
      if (province_types[0] == 'noi_tinh') {
        return {
          location: ShipmentPriceLocation.province_noi_tinh,
          location_display: 'Nội tỉnh'
        };
      }
      if (province_types[0] == 'lien_tinh') {
        return {
          location: ShipmentPriceLocation.province_lien_tinh,
          location_display: 'Liên tỉnh'
        };
      }
    }

    if (region_types.length) {
      if (region_types[0] == 'noi_mien') {
        return {
          location: ShipmentPriceLocation.region_noi_mien,
          location_display: 'Nội miền'
        };
      }
      if (region_types[0] == 'lien_mien') {
        return {
          location: ShipmentPriceLocation.region_lien_mien,
          location_display: 'Liên miền'
        };
      }
      if (region_types[0] == 'can_mien') {
        return {
          location: ShipmentPriceLocation.region_can_mien,
          location_display: 'Cận miền'
        };
      }
    }

    return null;
  }

  private static mapPromotion(promotion: ShipmentPriceListPromotion): ShipmentPriceListPromotion {
    return {
      ...promotion,
      status: promotion.status == 'N' ? 'Z' : promotion.status,
      status_display: ShipmentPriceApi.statusMap(promotion.status)
    };
  }

  // NOTE: ShipmentServices
  getShipmentServices(filter?: ShipmentPriceAPI.GetShipmentServicesFilter) {
    return this.http
      .post('api/admin.ShipmentPrice/GetShipmentServices', filter || {})
      .toPromise()
      .then(res => res.shipment_services.map(s => ShipmentPriceApi.mapShipmentService(s)));
  }

  createShipmentService(body: ShipmentPriceAPI.CreateShipmentServiceRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/CreateShipmentService', body)
      .toPromise();
  }

  updateShipmentService(body: ShipmentPriceAPI.UpdateShipmentServiceRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentService', body)
      .toPromise();
  }

  updateShipmentServiceStatus(id: string, status: string) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentService', {id, status})
      .toPromise();
  }

  updateShipmentServicesAvailableLocations(body) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentServicesAvailableLocations', body)
      .toPromise();
  }

  updateShipmentServicesBlacklistLocations(body) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentServicesBlacklistLocations', body)
      .toPromise();
  }

  deleteShipmentService(id: string) {
    return this.http
      .post('api/admin.ShipmentPrice/DeleteShipmentService', {id})
      .toPromise();
  }

  // NOTE: ShipmentPriceLists
  getShipmentPriceLists(filter?: ShipmentPriceAPI.GetShipmentPriceListsFilter) {
    return this.http
      .post('api/admin.ShipmentPrice/GetShipmentPriceLists', filter || {})
      .toPromise()
      .then(res => res.shipment_price_lists
        .sort((a, _) => !!a.is_default ? -1 : 1)
      );
  }

  createShipmentPriceList(body: ShipmentPriceAPI.CreateShipmentPriceListRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/CreateShipmentPriceList', body)
      .toPromise();
  }

  updateShipmentPriceList(body: ShipmentPriceAPI.UpdateShipmentPriceListRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentPriceList', body)
      .toPromise();
  }

  deleteShipmentPriceList(id: string) {
    return this.http
      .post('api/admin.ShipmentPrice/DeleteShipmentPriceList', {id})
      .toPromise();
  }

  setDefaultShipmentPriceList(id: string, connection_id: string) {
    return this.http
      .post('api/admin.ShipmentPrice/SetDefaultShipmentPriceList', {id, connection_id})
      .toPromise();
  }

  // NOTE: ShipmentPrices
  getShipmentPrices(body) {
    return this.http
      .post('api/admin.ShipmentPrice/GetShipmentPrices', body)
      .toPromise()
      .then(res => {
        return res.shipment_prices
          .sort((a,b) => a.priority_point > b.priority_point ? -1 : 1)
          .map(sp => ShipmentPriceApi.mapShipmentPrice(sp));
      });
  }

  createShipmentPrice(body: ShipmentPriceAPI.CreateShipmentPriceRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/CreateShipmentPrice', body)
      .toPromise()
      .then(res => ShipmentPriceApi.mapShipmentPrice(res));
  }

  updateShipmentPrice(body: ShipmentPriceAPI.UpdateShipmentPriceRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentPrice', body)
      .toPromise()
      .then(res => ShipmentPriceApi.mapShipmentPrice(res));
  }

  deleteShipmentPrice(id: string) {
    return this.http
      .post('api/admin.ShipmentPrice/DeleteShipmentPrice', {id})
      .toPromise();
  }

  // NOTE: ShopShipmentPriceLists
  getShopShipmentPriceLists(filter: ShipmentPriceAPI.GetShopShipmentPriceListsFilter) {
    return this.http
      .post('api/admin.ShipmentPrice/GetShopShipmentPriceLists', filter)
      .toPromise()
      .then(res => res.price_lists);
  }

  createShopShipmentPriceList(request: ShipmentPriceAPI.ConfirmShopShipmentPriceListRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/CreateShopShipmentPriceList', request)
      .toPromise();
  }

  updateShopShipmentPriceList(request: ShipmentPriceAPI.ConfirmShopShipmentPriceListRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShopShipmentPriceList', request)
      .toPromise();
  }

  // NOTE: ShipmentPriceListPromotions
  getShipmentPriceListPromotions(filter: ShipmentPriceAPI.GetShipmentPriceListPromotionsRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/GetShipmentPriceListPromotions', filter)
      .toPromise()
      .then(res => res.shipment_price_list_promotions.map(promo => ShipmentPriceApi.mapPromotion(promo)));
  }

  createShipmentPriceListPromotion(request: ShipmentPriceAPI.CreateShipmentPriceListPromotionRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/CreateShipmentPriceListPromotion', request)
      .toPromise();
  }

  updateShipmentPriceListPromotion(request: ShipmentPriceAPI.UpdateShipmentPriceListPromotionRequest) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentPriceListPromotion', request)
      .toPromise();
  }

  updateShipmentPriceListPromotionStatus(id: string, status: string) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentPriceListPromotion', {id, status})
      .toPromise();
  }

  prioritizeShipmentPriceListPromotion(id: string, priority_point: number) {
    return this.http
      .post('api/admin.ShipmentPrice/UpdateShipmentPriceListPromotion', {id, priority_point})
      .toPromise();
  }

  deleteShipmentPriceListPromotion(id: string) {
    return this.http
      .post('api/admin.ShipmentPrice/DeleteShipmentPriceListPromotion', {id})
      .toPromise();
  }

}

export namespace ShipmentPriceAPI {
  export interface CreateShipmentServiceRequest {
    connection_id: string;
    description: string;
    ed_code: string;
    image_url?: string;
    name: string;
    service_ids: string[];
    available_locations?: AvailableLocation[];
  }

  export interface UpdateShipmentServiceRequest extends CreateShipmentServiceRequest {
    id: string;
    status: string;
  }

  export interface GetShipmentServicesFilter {
    connection_id?: string;
  }

  export interface CreateShipmentPriceListRequest {
    name: string;
    description?: string;
    connection_id: string;
  }

  export interface UpdateShipmentPriceListRequest extends CreateShipmentPriceListRequest {
    id: string;
  }

  export interface GetShipmentPriceListsFilter {
    connection_id?: string;
    is_default?: string;
  }

  export interface CreateShipmentPriceRequest {
    additional_fees?: ShipmentPriceAdditionalFee[];
    custom_region_ids: string[];
    custom_region_types: CustomRegionType[];
    details: ShipmentPriceDetail[];
    name: string;
    priority_point: number;
    province_types: ProvinceType[];
    region_types: RegionType[];
    shipment_price_list_id?: string;
    shipment_service_id?: string;
    urban_types: UrbanType[];
  }

  export interface UpdateShipmentPriceRequest extends CreateShipmentPriceRequest {
    id: string;
    status?: string;
  }

  export interface GetShopShipmentPriceListsFilter {
    connection_id?: string;
    shipment_price_list_id?: string;
    shop_id?: string;
  }

  export interface ConfirmShopShipmentPriceListRequest {
    connection_id: string;
    note?: string;
    shipment_price_list_id: string;
    shop_id: string;
  }

  export interface GetShipmentPriceListPromotionsRequest {
    connection_id?: string;
    shipment_price_list_id?: string;
    paging: Paging;
  }

  export interface CreateShipmentPriceListPromotionRequest {
    applied_rules?: PromotionAppliedRules;
    connection_id: string;
    date_from: Date;
    date_to: Date;
    description?: string;
    name: string;
    priority_point?: number;
    shipment_price_list_id: string;
  }

  export interface UpdateShipmentPriceListPromotionRequest extends CreateShipmentPriceListPromotionRequest {
    id: string;
    status?: string;
  }

}
