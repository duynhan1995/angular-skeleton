import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {Filters, MoneyTransactionRrule, Query} from "@etop/models";
import {Shop} from "libs/models/Account";

const MONEY_TRANSACTION_RRULE = {
  'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR': 'Hàng ngày',
  'FREQ=WEEKLY;BYDAY=MO,WE,FR': 'Thứ 2-4-6 hàng tuần',
  'FREQ=WEEKLY;BYDAY=TU,TH': 'Thứ 3-5 hàng tuần',
  'FREQ=WEEKLY;BYDAY=FR': '1 tuần 1 lần vào thứ 6',
  'FREQ=MONTHLY;BYDAY=+2FR,-1FR': '1 tháng 2 lần vào thứ 6',
  'FREQ=MONTHLY;BYDAY=-1FR': '1 tháng 1 lần vào thứ 6',
  'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1': '1 tháng 1 lần vào ngày cuối cùng của tháng'
}

@Injectable({
  providedIn: 'root'
})
export class AdminShopApi {

  constructor(
    private http: HttpService
  ) {
  }

  getShop(id: string) {
    return this.http
      .post('api/admin.Shop/GetShop', {id})
      .toPromise()
      .then(res => Shop.shopMap(res));
  }

  getShops(query: AdminShopAPI.GetShopsRequest) {
    return this.http
      .post('api/admin.Shop/GetShops', query)
      .toPromise()
      .then(res => res.shops.map(s => Shop.shopMap(s)));
  }

  getShopsByIDs(ids: string[]) {
    return this.http
      .post('api/admin.Shop/GetShopsByIDs', {ids})
      .toPromise()
      .then(res => res.shops.map(s => Shop.shopMap(s)));
  }

  updateShopMoneyTransactionRrule(id: string, money_transaction_rrule: MoneyTransactionRrule) {
    return this.http
      .post('api/admin.Shop/UpdateShopInfo', {id, money_transaction_rrule})
      .toPromise();
  }

  updateShopPriorityMoneyTransaction(id: string, is_prior_money_transaction: boolean) {
    return this.http
      .post('api/admin.Shop/UpdateShopInfo', {id, is_prior_money_transaction})
      .toPromise();
  }

}

export namespace AdminShopAPI {
  export interface GetShopsRequest {
    filter?: GetShopsFilter,
    filters?: Filters;
    paging: {
      offset?: number;
      limit?: number;
      sort?: string;
    };
  }

  export interface GetShopsFilter {
    name?: string;
    owner_id?: string;
    shop_ids?: string[];
  }
}
