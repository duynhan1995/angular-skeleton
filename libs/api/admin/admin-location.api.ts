import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { CustomRegion } from 'libs/models/Location';

@Injectable({
  providedIn: 'root'
})
export class AdminLocationApi {

  constructor(
    private http: HttpService
  ) { }

  getCustomRegions() {
    return this.http
      .post('api/admin.Location/GetCustomRegions', {})
      .toPromise()
      .then(res => res.custom_regions.map(r => {
        return new CustomRegion(r);
      }));
  }

  createCustomRegion(body: AdminLocationAPI.CreateCustomRegionRequest) {
    return this.http
      .post('api/admin.Location/CreateCustomRegion', body)
      .toPromise();
  }

  updateCustomRegion(body: AdminLocationAPI.UpdateCustomRegionRequest) {
    return this.http
      .post('api/admin.Location/UpdateCustomRegion', body)
      .toPromise();
  }

  deleteCustomRegion(id: string) {
    return this.http
      .post('api/admin.Location/DeleteCustomRegion', {id})
      .toPromise();
  }

}

export namespace AdminLocationAPI {
  export interface CreateCustomRegionRequest {
    name: string;
    description?: string;
    province_codes: string;
  }

  export interface UpdateCustomRegionRequest extends CreateCustomRegionRequest {
    id: string;
  }
}
