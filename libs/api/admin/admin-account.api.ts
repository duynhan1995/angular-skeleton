import { Injectable } from '@angular/core';
import {HttpService} from "@etop/common";
import {AdminUser, AdminUserRole} from "libs/models/admin/AdminUser";
import {AuthenticateStore} from "@etop/core";

const ADMIN_USER_ROLE = {
  admin: 'Admin',
  ad_salelead: 'Lead Sale',
  ad_sale: 'Sale',
  ad_customerservice_lead: 'Lead CS',
  ad_customerservice: 'CS',
  ad_accountant: 'Kế toán',
  ad_voip: 'VOIP',
}

@Injectable({
  providedIn: 'root'
})
export class AdminAccountApi {

  static get adminUserRole() {
    return ADMIN_USER_ROLE;
  }

  static adminUserRoleMap(roles: AdminUserRole[]) {
    return roles.map(r => AdminAccountApi.adminUserRole[r]).join(', ');
  }

  static adminUserMap(admin: AdminUser): AdminUser {
    return {
      ...admin,
      roles_display: AdminAccountApi.adminUserRoleMap(admin.roles)
    };
  }

  constructor(
    private http: HttpService,
    private auth: AuthenticateStore
  ) { }

  getAdminUsers(roles?: AdminUserRole[]) {
    let filter = null;
    if (roles?.length) {
      filter = {roles: roles};
    }
    return this.http.post('api/admin.Account/GetAdminUsers', {filter}).toPromise()
      .then(res => res.admins
        .map(admin => AdminAccountApi.adminUserMap(admin))
        .sort(admin => admin.user_id == this.auth.snapshot.user.id ? -1 : 1)
      );
  }

  createAdminUser(body: AdminAccountAPI.CreateAdminUserRequest) {
    return this.http.post('api/admin.Account/CreateAdminUser', body)
      .toPromise()
      .then();
  }

  updateAdminUser(body: AdminAccountAPI.UpdateAdminUserRequest) {
    return this.http.post('api/admin.Account/UpdateAdminUser', body)
      .toPromise()
      .then();
  }

  deleteAdminUser(user_id: string) {
    return this.http.post('api/admin.Account/DeleteAdminUser', {user_id})
      .toPromise()
      .then();
  }

  adminLoginAsAccount(body: AdminAccountAPI.AdminLoginAsAccount) {
    return this.http.post('api/admin.Misc/AdminLoginAsAccount', body)
      .toPromise()
      .then();
  }

}

export namespace AdminAccountAPI {
  export interface CreateAdminUserRequest {
    email: string;
    roles: AdminUserRole[];
  }

  export interface UpdateAdminUserRequest {
    user_id: string;
    roles: AdminUserRole[];
    status?: 'Z' | 'P' | 'N';
  }

  export interface AdminLoginAsAccount {
    account_id: string;
    password: string;
    user_id: string;
  }
}
