import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { CursorPaging } from '@etop/models';
import { Tenant } from '@etop/models/Tenant';

@Injectable({
  providedIn: 'root',
})
export class TenantsApi {
  constructor(private http: HttpService) {}

  async getTenants(request?: TenantsAPI.GetTenantsRequest) {
    return await this.http
      .post('api/admin.Etelecom/GetTenants', request)
      .toPromise()
      .then((res) => {
        res.tenants = res.tenants.map((tenant) => Tenant.tenantMap(tenant));
        return res;
      });
  }

  async activateTenant(request?: TenantsAPI.ActivateTenantRequest) {
    return await this.http
      .post('api/admin.Etelecom/ActivateTenant', request)
      .toPromise();
  }

  async createTenant(request?: TenantsAPI.CreateTenantRequest) {
    return await this.http
      .post('api/admin.Etelecom/CreateTenant', request)
      .toPromise();
  }
}

export namespace TenantsAPI {
  export class GetTenantsRequest {
    filter?: GetTenantsFilter;
    paging?: CursorPaging;
  }
  export class GetTenantsFilter {
    ids?: string[];
    owner_id: string;
  }
  export class ActivateTenantRequest {
    account_id?: string;
    hotline_id: string;
    owner_id?: string;
    tenant_id?: string;
  }
  export class CreateTenantRequest {
    connection_id?: string;
    owner_id: string;
    account_id?: string;
  }
}
