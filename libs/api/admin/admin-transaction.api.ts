import {Injectable} from '@angular/core';
import {HttpService} from '@etop/common';
import {CursorPaging} from '@etop/models';
import {Transaction} from '@etop/models/Transaction';

@Injectable({
  providedIn: 'root'
})
export class AdminTransactionApi {
  constructor(
    private http: HttpService,
  ) {
  }

  async getTransactions(request?: AdminTransactionAPI.AdminGetTransactionRequest) {
    return this.http.post('api/admin.Transaction/GetTransactions', request)
      .toPromise().then(res => {
        res.transactions = res.transactions.map(transaction => Transaction.transactionMap(transaction))
        return res
      });
  }

}

export namespace AdminTransactionAPI {
  export interface AdminGetTransactionFilter {
    account_id?: string
    date_from?: string;
    date_to?: string;
    ref_id?: string;
    ref_type?: string;
  }

  export interface AdminGetTransactionRequest {
    filter?: AdminGetTransactionFilter;
    paging?: CursorPaging
  }
}
