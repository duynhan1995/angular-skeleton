import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { Connection, ConnectionMethod } from 'libs/models/Connection';

@Injectable({
  providedIn: 'root'
})
export class AdminConnectionApi {

  constructor(
    private http: HttpService
  ) { }

  getConnections(connection_method?: ConnectionMethod) {
    return this.http
      .post('api/admin.Connection/GetConnections', {connection_method})
      .toPromise()
      .then(res => res.connections.map(conn => Connection.connectionMap(conn)));
  }

  getConnectionServices(id: string) {
    return this.http
      .post('api/admin.Connection/GetConnectionServices', {id})
      .toPromise()
      .then(res => res.connection_service);
  }

  disableConnection(id: string) {
    return this.http
      .post('api/admin.Connection/DisableConnection', {id})
      .toPromise();
  }

  confirmConnection(id: string) {
    return this.http
      .post('api/admin.Connection/ConfirmConnection', {id})
      .toPromise();
  }

  updateBuiltinShopConnection(body: AdminConnectionAPI.UpdateBuiltinShopConnectionRequest) {
    return this.http
      .post('api/admin.Connection/UpdateBuiltinShopConnection', body)
      .toPromise();
  }

}

export namespace AdminConnectionAPI {
  export interface UpdateBuiltinShopConnectionRequest {
    connection_id: string;
    external_data: {
      user_id: string;
      email?: string;
    },
    token: string;
  }
}
