import { Pipe, PipeTransform } from '@angular/core';
import { Variant } from 'libs/models/Product';

@Pipe({
  name: 'variantDisplay'
})
export class VariantDisplayPipe implements PipeTransform {

  transform(value: Variant, ...args: unknown[]) {
    const product_name = value.product && value.product.name;
    if (value.name != product_name) {
      return value.name;
    }
    if (value.attributes && value.attributes.length) {
      return value.attributes.map(attr => attr.value).join(' - ');
    }
    return null;
  }

}
