export enum PrintType {
  orders = 'orders',
  fulfillments = 'fulfillments',
  o_and_f = 'o_and_f'
}
