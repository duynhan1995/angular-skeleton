import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FulfillmentAPI} from "@etop/api";
import {Address, Connection, Fulfillment} from "@etop/models";
import {CreateMultiFfmsControlPanelController} from "libs/shared/components/create-multi-ffms-control-panel/create-multi-ffms-control-panel.controller";
import {ImportFfmQuery, ImportFfmService} from "@etop/state/shop/import-ffm";
import {BaseComponent} from "@etop/core";
import {map, takeUntil} from "rxjs/operators";
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import CreateShipmentFulfillmentFromImportRequest = FulfillmentAPI.CreateShipmentFulfillmentFromImportRequest;
import {ConnectionService, ConnectionStore} from "@etop/features";
import {AddressService} from "apps/core/src/services/address.service";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";
import {FormBuilder} from "@angular/forms";
import { environment } from 'apps/shop/src/environments/environment';

export enum ImportStep {
  importFile,
  selectCarrier,
  gettingServices,
  finished
}

@Component({
  selector: 'etop-import-ffm',
  templateUrl: './import-ffm.component.html',
  styleUrls: ['./import-ffm.component.scss']
})
export class ImportFfmComponent extends BaseComponent implements OnInit {
  @ViewChild('inputImport', {static: false}) inputImport: ElementRef;
  @Input() color = 'topship';

  @Input() isBuiltin: boolean = true;
  @Input() isDirect: boolean = false;
  @Input() connectionIDs: string[] = [];

  importedFile: File;
  importedType: 'simplified' | 'normal';

  progressPercent = 0;
  importFinished = false;
  connectionId: string;
  importStep: ImportStep = 0;

  interval: any;

  loading = false;
  calculatingFee = false;

  importFulfillmentsList$ = this.importFfmQuery.selectAll();
  importFfmForms$ = this.importFfmQuery.selectAllWithFilter();
  finishFormsGettingServices$ = this.importFfmQuery.selectAllWithFilter().pipe(
    map(forms => forms.filter(f => !f.gettingShippingServices))
  );

  filter$ = this.importFfmQuery.select(state => state.ui.filter);

  connectionsList$ = this.connectionStore.state$.pipe(map(s => {
    return ConnectionService.filterConnections(s?.initConnections, this.isBuiltin, this.isDirect, this.connectionIDs);
  }));

  totalFfms$ = this.importFfmQuery.selectAllWithFilter(true).pipe(
    map(ffms => ffms.filter(ffm => ffm.createStatus != 'success'))
  );

  totalFees$ = this.importFfmQuery.selectAllWithFilter(true).pipe(
    map(ffms => ffms
      .filter(ffm => ffm.createStatus != 'success')
      .reduce((a, b) => {
        return a + (b.shipping_service_fee || 0)
      }, 0)
    )
  );

  totalCodAmounts$ = this.importFfmQuery.selectAllWithFilter(true).pipe(
    map(ffms => ffms
      .filter(ffm => ffm.createStatus != 'success')
      .reduce((a, b) => {
        return a + (b.cod_amount || 0)
      }, 0)
    )
  );

  labels = [
    {
      name: 'Tất cả',
      code: 'all_import'
    },
    {
      name: 'Thất bại',
      code: 'failed_import'
    },
    {
      name: 'Thành công',
      code: 'success_import'
    },
  ];

  checkAll = this.fb.control(false);

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${connection.name.toUpperCase()}</div>
      </div>`;
  }

  displayMap = option => option && ImportFfmComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  constructor(
    private fb: FormBuilder,
    private headerController: HeaderControllerService,
    private importFfmService: ImportFfmService,
    private addressService: AddressService,
    private importFfmQuery: ImportFfmQuery,
    private controller: CreateMultiFfmsControlPanelController,
    private dialog: DialogControllerService,
    private connectionStore: ConnectionStore,
    private promiseQueue: PromiseQueueService,
  ) {
    super();
  }

  rowDisplayed(row: Fulfillment): boolean {
    const importFfmForms = this.importFfmQuery.getAllWithFilter();
    return importFfmForms.some(ffm => ffm.id == row.id);
  }

  ngOnInit() {
    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(conns => {
        if (conns?.length == 1) {
          this.connectionId = conns[0].id;
        } else {
          this.connectionId = null;
        }
      });

    this.addressService.getFromAddresses().then(({fromAddresses, fromAddressIndex}) => {
      this.controller.updateUniData({
        ...this.controller.uniData,
        pickupAddress: {
          value: fromAddresses[fromAddressIndex],
          toggled: true
        }
      });
    });

    this.controller.updateUniData$.pipe(takeUntil(this.destroy$))
      .subscribe(emitEvent => {
        if (emitEvent) {
          this.importStep = 2;
        }
      });

    this.importFfmForms$.pipe(takeUntil(this.destroy$))
      .subscribe(ffms => {
        const totalSelecteds = ffms.filter(f => f.selected).length;
        this.checkAll.patchValue(ffms?.length == totalSelecteds, {emitEvent: false});

        const _alls = this.importFfmQuery.getAll()?.length;
        if (this.importStep != 0) {
          return;
        }
        if (ffms?.length == _alls && _alls > 0) {
          if (this.connectionId) {
            return this.selectConnection();
          }
          this.importStep = 1;
        }
      });

    this.finishFormsGettingServices$.pipe(takeUntil(this.destroy$))
      .subscribe(finisheds => {
        const _forms = this.importFfmQuery.getAllWithFilter()?.length;
        const _alls = this.importFfmQuery.getAll()?.length;
        if (this.importStep != 2) {
          return;
        }
        if ((finisheds?.length == _forms) && (finisheds?.length == _alls) && _alls > 0) {
          this.importStep = 3;
        }
      });

    this.checkAll.valueChanges.subscribe(value => {
      const _forms = this.importFfmQuery.getAllWithFilter();
      this.importFfmService.updateFfmForms(_forms.map(f => ({...f, selected: value})));
    });

    if (this.importFfmQuery.getAll()?.length) {
        this.headerController.setActions([
          {
            onClick: () => this.openImportWarningDialog(),
            title: 'Import file khác',
            cssClass: 'btn btn-outline btn-topship'
          }
        ]);
      } 
  }

  uploadFile(type: 'simplified' | 'normal') {
    this.importedType = type;
    this.inputImport.nativeElement.click();
  }

  onSelectFile(event: any) {
    try {
      if (event.target.files && event.target.files.length > 0) {
        this.importedFile = event.target.files[0];
        this.inputImport.nativeElement.value = null;
        this.importFfm().then();
      }
    } catch (e) {
      debug.error('ERROR in Upload file', e);
      toastr.error('Upload file không thành công.', e.code && (e.message || e.msg));
    }
  }

  async importFfm() {
    this.progressStartLoading();
    try {
      await this.importFfmService.importFfm(this.importedFile);
      this.headerController.setActions([
        {
          onClick: () => this.openImportWarningDialog(),
          title: 'Import file khác',
          cssClass: 'btn btn-outline btn-topship'
        }
      ]);
      this.progressEndLoading();
    } catch (e) {
      debug.error('ERROR in importFfm', e);

      if (e.code == 'cell_errors') {
        const errors = e.errors.map(err => {
          return {col: err.meta.col, row: err.meta.row, msg: err.msg};
        });
        this.showErrorPopup(errors);
      } else {
        toastr.error('Import đơn giao hàng không thành công.', e.code && (e.message || e.msg));
      }

      this.importedFile = null;
      this.progressEndLoading();
      this.progressPercent = 0;
    }
  }

  showErrorPopup(errors: Array<{ col: string, row: string, msg: string }>) {
    const body = errors.map((err, index) => {
      return `
<div class="bg-danger-light text-danger p-3 ${index > 0 && 'mt-3'}" style="border-radius: 7px;">
  <span>Lỗi ở ô ${err.row}${err.col}:</span>
  <span class="text-bold">${err.msg}</span>
</div>`;
    }).join('\n');
    const dialog = this.dialog.createConfirmDialog({
      title: 'Import đơn giao hàng',
      body,
      confirmCss: 'd-none',
      onCancel: () => {
        this.reset();
      },
    });
    dialog.show().then();
  }

  private progressStartLoading() {
    this.progressPercent = 1;
    this.increase();
    this.interval = setInterval(() => this.increase(), 500);
  }

  private increase() {
    if (this.progressPercent < 90) {
      this.progressPercent += 30;
    } else {
      clearInterval(this.interval);
    }
  }

  private progressEndLoading() {
    this.progressPercent = 100;
    clearInterval(this.interval);
  }

  async confirm() {
    this.loading = true;
    try {
      const activeFfmImports = this.importFfmQuery.getAllWithFilter(true);
      const errorFfms = activeFfmImports.filter(ffm => ImportFfmService.isErrorFfm(ffm));
      if (errorFfms.length) {
        toastr.error(`Có ${errorFfms.length} đơn bị lỗi. Vui lòng kiểm tra lại.`);
        this.loading = false;
        return;
      }

      const _importForms = activeFfmImports.filter(ffm => ffm.createStatus != 'success');

      const body: CreateShipmentFulfillmentFromImportRequest[] = _importForms.map((ffm) => {
        const {pickupAddress, weight, insurance, shippingNote, tryOn} = this.controller.uniData;

        const {
          id, basket_value, cod_amount, connection_id, ed_code,
          include_insurance, product_description, shipping_note,
          shipping_service_code, shipping_service_fee, shipping_service_name,
          total_weight, try_on, customer_name, customer_phone,
          province_code, district_code, ward_code, shipping_address, shipping_carrier_name
        } = ffm;

        return {
          fulfillment: {
            basket_value, cod_amount, connection_id, ed_code,
            product_description,
            include_insurance: insurance.toggled ? insurance.value : include_insurance,
            shipping_note: shippingNote.toggled ? shippingNote.value : shipping_note,
            total_weight: weight.toggled ? weight.value : total_weight,
            try_on: tryOn.toggled ? tryOn.value : try_on,
            pickup_address: pickupAddress.value,
            shipping_address: {
              ...new Address({}),
              full_name: customer_name, phone: customer_phone,
              province_code, district_code, ward_code,
              address1: shipping_address
            },
            shipping_service_code, shipping_service_fee, shipping_service_name
          },
          id, shipping_carrier_name
        };
      });

      const res = await this.importFfmService.createFulfillmentsFromImport(body);

      const success = res.errors.filter(err => err.code == 'ok').length;
      if (success == 0) {
        toastr.error('Tạo đơn giao hàng không thành công.');
      } else if (success < _importForms.length) {
        toastr.warning(`Tạo thành công ${success}/${_importForms.length} đơn giao hàng.`);
      } else {
        toastr.success('Tạo đơn giao hàng thành công');
      }

      this.onImportFinished();

    } catch (e) {
      debug.error('ERROR in confirm()', e);
      toastr.error('Tạo đơn giao hàng không thành công', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  trackFfmImports(index: number, ffm: any) {
    return ffm.id;
  }

  filterByLabel(code) {
    this.importFfmService.setFilter(code);
  }

  onImportFinished() {
    const _ffms = this.importFfmQuery.getAllWithFilter();
    const failed = _ffms.filter(f => f.createStatus == 'failed').length;
    const success = _ffms.filter(f => f.createStatus == 'success').length;
    this.labels[1].name = `Thất bại (${failed})`;
    this.labels[2].name = `Thành công (${success})`;
    this.filterByLabel(failed ? this.labels[1].code : this.labels[0].code);
    this.importFinished = true;
  }

  openImportWarningDialog() {
    const importFfms = this.importFfmQuery.getValue().importFfmForms.filter(ffm => ffm.createStatus != 'success');
    if (!importFfms.length) {
      return this.reset();
    }
    const dialog = this.dialog.createConfirmDialog({
      title: 'Import đơn giao hàng',
      body: `
<div>Bạn đang thực hiện import <b class="upper-medium-font">${importFfms?.length}</b> đơn giao hàng.</div>
<div>Thoát khỏi màn hình này đồng nghĩa với việc các thao tác import bạn đang thực hiện sẽ không được lưu lại.</div>
<div>Bạn có chắc muốn đóng popup này và huỷ thao tác import đơn giao hàng?</div>`,
      confirmTitle: 'Xác nhận',
      confirmCss: `btn-${this.color}`,
      onConfirm: () => {
        this.reset();
        dialog.close().then();
      },
    });
    dialog.show().then();
  }

  private reset() {
    this.importFinished = false;
    this.progressPercent = 0;
    this.importStep = 0;

    const connections = ConnectionService.filterConnections(
      this.connectionStore.snapshot.initConnections, this.isBuiltin, this.isDirect, this.connectionIDs
    );
    if (connections.length == 1) {
      this.connectionId = connections[0].id;
    } else {
      this.connectionId = null;
    }

    this.importFfmService.resetStore();
    this.headerController.clearActions();
  }

  getSampleFile(simple: boolean) {
    if (simple) {
      return location.href = environment.base_url + '/dl/imports/shop_fulfillments.v1.simplified.xlsx';
    }
    return location.href = environment.base_url + '/dl/imports/shop_fulfillments.v1.xlsx';
  }

  selectConnection() {
    if (!this.connectionId) {
      return toastr.error('Vui lòng chọn nhà vận chuyển!');
    }
    this.importStep = 2;
    this.controller.updateUniData({
      ...this.controller.uniData,
      connectionID: {
        value: this.connectionId,
        toggled: true
      }
    }, true);
  }

  cancelGettingServices() {
    this.importStep = 1;
    this.promiseQueue.destroyQ();
  }

}
