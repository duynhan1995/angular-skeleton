import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {EtopMaterialModule, MaterialModule} from "@etop/shared";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CreateMultiFfmsControlPanelModule} from "libs/shared/components/create-multi-ffms-control-panel/create-multi-ffms-control-panel.module";
import {ImportFfmRowInputComponent} from "@etop/shared/components/import-ffm/import-ffm-row/import-ffm-row-input/import-ffm-row-input.component";
import {ImportFfmRowComponent} from "@etop/shared/components/import-ffm/import-ffm-row/import-ffm-row.component";
import {ImportFfmComponent} from "@etop/shared/components/import-ffm/import-ffm.component";

@NgModule({
  exports: [
    ImportFfmComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    EtopMaterialModule,
    CreateMultiFfmsControlPanelModule,
    NgbModule
  ],
  declarations: [
    ImportFfmComponent,
    ImportFfmRowComponent,
    ImportFfmRowInputComponent
  ]
})

export class ImportFfmModule {
}
