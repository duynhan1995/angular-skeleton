import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component, ComponentFactory, ComponentFactoryResolver, ComponentRef,
  EventEmitter,
  Input,
  OnInit,
  Output, ViewChild, ViewContainerRef,
} from '@angular/core';
import {FulfillmentShipmentService, TRY_ON} from "@etop/models";
import {ConnectionStore} from "@etop/features";
import {FormBuilder} from "@angular/forms";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {CreateMultiFfmsControlPanelController} from "libs/shared/components/create-multi-ffms-control-panel/create-multi-ffms-control-panel.controller";
import {map, takeUntil} from "rxjs/operators";
import {FulfillmentAPI, FulfillmentApi} from "@etop/api";
import {DecimalPipe} from "@angular/common";
import {ImportFfmForm, ImportFfmQuery, ImportFfmService} from "@etop/state/shop/import-ffm";
import {ImportFfmRowInputComponent} from "@etop/shared/components/import-ffm/import-ffm-row/import-ffm-row-input/import-ffm-row-input.component";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: '[etop-import-ffm-row]',
  templateUrl: './import-ffm-row.component.html',
  styleUrls: ['./import-ffm-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportFfmRowComponent extends BaseComponent implements OnInit {
  @ViewChild("inputContainer", { read: ViewContainerRef }) inputContainer;

  @Input() color = 'topship';
  @Input() ffm: ImportFfmForm;
  @Input() index: number;
  @Input() disabled = false;

  connectionsList$ = this.connectionStore.state$.pipe(map(s => s?.validConnections));

  getServicesIndex = 0;

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  serviceDisplayMap = (option: FulfillmentShipmentService) => option && this.shipmentServiceMap(option) || null;
  serviceValueMap = option => option && option.code || null;

  constructor(
    private resolver: ComponentFactoryResolver,
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private auth: AuthenticateStore,
    private controller: CreateMultiFfmsControlPanelController,
    private connectionStore: ConnectionStore,
    private ffmApi: FulfillmentApi,
    private importFfmService: ImportFfmService,
    private importFfmQuery: ImportFfmQuery,
    private numberFormat: DecimalPipe,
    private promiseQueue: PromiseQueueService,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  get provinceDisplay() {
    return this.locationQuery.getProvince(this.ffm?.province_code)?.name;
  }

  get districtDisplay() {
    return this.locationQuery.getDistrict(this.ffm?.district_code)?.name;
  }

  get wardDisplay() {
    return this.locationQuery.getWard(this.ffm?.ward_code)?.name;
  }

  get tryOnDisplay() {
    return TRY_ON[this.ffm.try_on];
  }

  get shippingServiceDisplayOfForm() {
    const {shipping_carrier_name, shipping_service_name, shipping_service_fee} = this.ffm;
    return `${shipping_carrier_name} - ${shipping_service_name} (${this.numberFormat.transform(shipping_service_fee)}đ)`;
  }

  get createErrorMessageOfForm() {
    return this.ffm.createErrorMsg;
  }

  get errorFfm() {
    const {province_code, district_code, ward_code, errors, gettingShippingServices} = this.ffm;
    return (errors?.length > 0 && !gettingShippingServices) || !(province_code && district_code && ward_code);
  }

  get uniData() {
    return this.controller.uniData;
  }

  private shipmentServiceMap(service: FulfillmentShipmentService) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${service.provider_logo}" alt="">
    ${service?.from_topship ? `<img src="assets/images/r-topship_256.png" class="topship-logo" alt="">` : ''}
  </div>
  <div class="pl-2" style="line-height: 1.4; white-space: normal;">
    ${service?.connection_info?.name?.toUpperCase()} - ${service?.name} (${this.numberFormat.transform(service.fee)}đ)
  </div>
</div>`;
  }

  errorField(fieldName: string) {
    return this.ffm?.errors.find(err => err.field_name == fieldName);
  }

  ngOnInit() {
    if (this.controller.uniData.connectionID.value) {
      this.getServices().then();
    }

    this.controller.updateUniData$.pipe(takeUntil(this.destroy$))
      .subscribe(emitEvent => {
        this.cdr.detectChanges();
        if (emitEvent) {
          this.getServices().then();
        }
      });

    this.controller.updateShipmentServiceOrders$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.selectSuitableService();
      });
  }

  createInputComponent(inputName) {
    this.inputContainer.clear();
    const factory: ComponentFactory<ImportFfmRowInputComponent> = this.resolver.resolveComponentFactory(ImportFfmRowInputComponent);
    let componentRef: ComponentRef<ImportFfmRowInputComponent> = this.inputContainer.createComponent(factory);
    componentRef.instance.form = this.importFfmQuery.getImportFfmForm(this.ffm.id);
    componentRef.instance.index = this.index;
    componentRef.instance.color = this.color;
    componentRef.instance.onBlur.subscribe(_ => {
      this.cdr.detectChanges();
      componentRef.destroy();
    });
    componentRef.instance.focus(inputName);
  }

  updateForm() {
    this.importFfmService.updateFfmForms([this.ffm]);
  }

  async getServices() {
    this.ffm.gettingShippingServices = true;
    this.importFfmService.updateFfmForms([this.ffm]);

    if (this.ffm.createStatus == 'success') {
      this.ffm.gettingShippingServices = false;
      return this.importFfmService.updateFfmForms([this.ffm]);
    }

    this.ffm.shipping_service_code = null;
    this.ffm.shippingServicesList = [];
    this.importFfmService.updateFfmForms([this.ffm]);

    const {pickupAddress, weight, insurance, connectionID} = this.controller.uniData;
    if (!this.ffm.ward_code || !pickupAddress.value?.ward_code || !connectionID.value) {
      this.ffm.gettingShippingServices = false;
      return this.importFfmService.updateFfmForms([this.ffm]);
    }

    this.getServicesIndex++;

    try {
      const body: FulfillmentAPI.GetShipmentServicesRequest = {
        from_province_code: pickupAddress.value?.province_code,
        from_district_code: pickupAddress.value?.district_code,
        from_ward_code: pickupAddress.value?.ward_code,

        to_province_code: this.ffm.province_code,
        to_district_code: this.ffm.district_code,
        to_ward_code: this.ffm.ward_code,

        chargeable_weight: (weight.toggled ? weight.value : this.ffm.total_weight) || 500,
        total_cod_amount: this.ffm.cod_amount || 0,
        basket_value:  this.ffm.basket_value || 0,
        include_insurance: insurance.toggled ? insurance.value : this.ffm.include_insurance,

        connection_ids: [connectionID.value],
        index: this.getServicesIndex + this.index
      };

      await this.promiseQueue.enQ(async() => {
        const res = await this.ffmApi.getShipmentServices(body);
        if (res.index == this.getServicesIndex + this.index) {

          this.ffm.shippingServicesList = res.services.filter(s => s.is_available);
          this.ffm.gettingShippingServices = false;

          this.importFfmService.updateFfmForms([this.ffm]);

          this.selectSuitableService();
        }
      });

    } catch(e) {
      debug.error('ERROR in Getting Shipment Services', e);
      this.ffm.gettingShippingServices = false;
      this.importFfmService.updateFfmForms([this.ffm]);
    }
  }

  private selectSuitableService() {
    if (this.ffm.createStatus == 'success') {
      return;
    }
    const selectedService = CreateMultiFfmsControlPanelController.selectSuitableService(
      this.controller.sortShipmentServiceOrders,
      this.ffm.shippingServicesList
    );

    this.importFfmService.updateFfmForms([{
      ...this.ffm,
      shipping_service_code: selectedService?.code,
      shipping_service_fee: selectedService?.fee >= 0 ? selectedService?.fee : null,
      shipping_service_name: selectedService?.name || null,
      shipping_carrier_name: selectedService?.connection_info?.name?.toUpperCase() || null,
      connection_id: selectedService?.connection_info?.id || null,
    }]);
  }

}
