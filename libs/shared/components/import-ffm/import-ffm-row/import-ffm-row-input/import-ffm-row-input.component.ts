import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import {FormBuilder, FormControl} from "@angular/forms";
import {ImportFfmForm, ImportFfmQuery, ImportFfmService} from "@etop/state/shop/import-ffm";
import {MaterialInput} from "@etop/shared/components/etop-material/material-input.interface";
import {FulfillmentShipmentService, TRY_ON_OPTIONS} from "@etop/models";
import {ConnectionStore} from "@etop/features";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {CreateMultiFfmsControlPanelController} from "libs/shared/components/create-multi-ffms-control-panel/create-multi-ffms-control-panel.controller";
import {FulfillmentAPI, FulfillmentApi} from "@etop/api";
import {DecimalPipe} from "@angular/common";
import {map, takeUntil} from "rxjs/operators";
import {combineLatest, Subject} from "rxjs";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'etop-import-ffm-row-input',
  templateUrl: './import-ffm-row-input.component.html',
  styleUrls: ['./import-ffm-row-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportFfmRowInputComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('edCode', {static: false}) edCode: MaterialInput;
  @ViewChild('customerName', {static: false}) customerName: MaterialInput;
  @ViewChild('customerPhone', {static: false}) customerPhone: MaterialInput;
  @ViewChild('shippingAddress', {static: false}) shippingAddress: MaterialInput;
  @ViewChild('provinceCode', {static: false}) provinceCode: MaterialInput;
  @ViewChild('districtCode', {static: false}) districtCode: MaterialInput;
  @ViewChild('wardCode', {static: false}) wardCode: MaterialInput;
  @ViewChild('productDescription', {static: false}) productDescription: MaterialInput;
  @ViewChild('totalWeight', {static: false}) totalWeight: MaterialInput;
  @ViewChild('basketValue', {static: false}) basketValue: MaterialInput;
  @ViewChild('codAmount', {static: false}) codAmount: MaterialInput;
  @ViewChild('shippingServiceCode', {static: false}) shippingServiceCode: MaterialInput;
  @ViewChild('tryOn', {static: false}) tryOn: MaterialInput;
  @ViewChild('shippingNote', {static: false}) shippingNote: MaterialInput;

  @Input() form: ImportFfmForm;
  @Input() index: number;
  @Input() color = 'topship';

  clickedInside = true;
  initializing = true;

  getServicesIndex = 0;
  // getShipmentServices = new Subject<boolean>();

  fulfillmentForm = this.fb.group({
    id: '',
    selected: false,
    errors: [],

    createErrorMsg: '',
    createStatus: null,

    ed_code: '',
    customer_name: '',
    customer_phone: '',
    shipping_address: '',
    province_code: '',
    district_code: '',
    ward_code: '',
    product_description: '',
    total_weight: null,
    basket_value: null,
    include_insurance: false,
    cod_amount: null,
    shipping_service_code: '',
    shipping_service_fee: null,
    shipping_service_name: '',
    shipping_carrier_name: '',
    connection_id: '',
    try_on: '',
    shipping_note: '',
    gettingShippingServices: false,
    shippingServicesList: []
  });

  onBlur = new Subject();

  connectionsList$ = this.connectionStore.state$.pipe(map(s => s?.validConnections));

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.fulfillmentForm.controls['province_code'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.fulfillmentForm.controls['district_code'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  @HostListener('click', ['$event'])
  public clickInsideComponent() {
    this.clickedInside = true;
  }

  @HostListener('document:click', ['$event'])
  public clickOutsideComponent(event) {
    if (event?.target?.parentNode?.className == 'mat-option-text') {
      return;
    }
    if (this.fulfillmentForm.controls['gettingShippingServices'].value) {
      return;
    }
    if (!this.clickedInside) {
      this.onBlur.next();
    }
    this.clickedInside = false;
  }

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  serviceDisplayMap = (option: FulfillmentShipmentService) => option && this.shipmentServiceMap(option) || null;
  serviceValueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private auth: AuthenticateStore,
    private controller: CreateMultiFfmsControlPanelController,
    private connectionStore: ConnectionStore,
    private ffmApi: FulfillmentApi,
    private importFfmService: ImportFfmService,
    private importFfmQuery: ImportFfmQuery,
    private numberFormat: DecimalPipe,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  private shipmentServiceMap(service: FulfillmentShipmentService) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${service.provider_logo}" alt="">
    ${service?.from_topship ? `<img src="assets/images/r-topship_256.png" class="topship-logo" alt="">` : ''}
  </div>
  <div class="pl-2" style="line-height: 1.4; white-space: normal;">
    ${service?.connection_info?.name?.toUpperCase()} - ${service?.name} (${this.numberFormat.transform(service.fee)}đ)
  </div>
</div>`;
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  get errorFfm() {
    const {province_code, district_code, ward_code, errors, gettingShippingServices} = this.fulfillmentForm.getRawValue();
    return (errors?.length > 0 && !gettingShippingServices) || !(province_code && district_code && ward_code);
  }

  get wardCodeForm() {
    return this.fulfillmentForm.controls['ward_code'] as FormControl;
  }

  get includeInsuranceForm() {
    return this.fulfillmentForm.controls['include_insurance'] as FormControl;
  }

  get codAmountForm() {
    return this.fulfillmentForm.controls['cod_amount'] as FormControl;
  }

  get uniData() {
    return this.controller.uniData;
  }

  get shippingServiceDisplayOfForm() {
    const shipping_carrier_name = this.fulfillmentForm.controls['shipping_carrier_name'].value;
    const shipping_service_name = this.fulfillmentForm.controls['shipping_service_name'].value;
    const shipping_service_fee = this.fulfillmentForm.controls['shipping_service_fee'].value;
    return `${shipping_carrier_name} - ${shipping_service_name} (${this.numberFormat.transform(shipping_service_fee)}đ)`;
  }

  get displayShipmentServiceSelector() {
    const {shippingServicesList, gettingShippingServices} = this.fulfillmentForm.getRawValue();
    return shippingServicesList?.length || gettingShippingServices;
  }

  ngOnInit() {
    this.fulfillmentForm.controls['province_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.initializing) {
          this.fulfillmentForm.patchValue({
            district_code: '',
            ward_code: '',
          });
        }
      });

    this.fulfillmentForm.controls['district_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.initializing) {
          this.fulfillmentForm.patchValue({
            ward_code: '',
          });
        }
      });

    this.fulfillmentForm.controls['shipping_service_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        const _service = this.fulfillmentForm.controls['shippingServicesList'].value?.find(s => s.code == value);
        this.fulfillmentForm.patchValue({
          shipping_service_fee: _service?.fee || null,
          shipping_service_name: _service?.name || null,
          shipping_carrier_name: _service?.connection_info?.name?.toUpperCase() || null,
          connection_id: _service?.connection_info?.id || null,
        });
      });

    this.fulfillmentForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(form => {
        form = ImportFfmService.validateFfmImport(form);
        this.fulfillmentForm.patchValue({
          errors: form.errors
        }, {emitEvent: false});

        this.importFfmService.updateFfmForms([form]);
      });

    this.getServicesHandlers();

    this.prepareFormData();
    this.afterCreateFulfillmentsStatusChecker();
  }

  ngAfterViewInit() {
    this.initializing = true;

    this.fulfillmentForm.patchValue({
      province_code: this.form.province_code,
      district_code: this.form.district_code
    });

    this.initializing = false;
  }

  prepareFormData() {
    this.fulfillmentForm.patchValue({
      ...this.form,
      try_on: this.form.try_on || this.auth.snapshot.shop.try_on
    }, {emitEvent: false});
  }

  afterCreateFulfillmentsStatusChecker() {
    const createStatus = this.fulfillmentForm.getRawValue().createStatus;
    const createErrorMsg = this.fulfillmentForm.getRawValue().createErrorMsg;

    if (!(this.form && this.form.createStatus && this.form.createErrorMsg)) {
      return;
    }

    if (createStatus != this.form.createStatus || createErrorMsg != this.form.createErrorMsg) {
      this.fulfillmentForm.patchValue({
        ...this.form
      }, {emitEvent: false});
    }
  }

  errorField(fieldName: string) {
    return this.fulfillmentForm.controls['errors'].value?.find(err => err.field_name == fieldName);
  }

  focus(inputName) {
    setTimeout(_ => {
      const input = this[inputName] as MaterialInput;
      input.focus();
    });
  }

  onFormBlur() {
    this.getServices().then();
  }

  getServicesHandlers() {
    this.wardCodeForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getServices().then();
      });

    this.includeInsuranceForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getServices().then();
      });
  }

  async getServices() {
    this.fulfillmentForm.patchValue({
      shipping_service_code: null,
      shippingServicesList: []
    });

    const rawData = this.fulfillmentForm.getRawValue();
    const {pickupAddress, weight, insurance, connectionID} = this.controller.uniData;
    if (!rawData.ward_code || !pickupAddress.value?.ward_code || !connectionID.value) {
      return;
    }

    this.fulfillmentForm.patchValue({
      gettingShippingServices: true
    });

    this.getServicesIndex++;

    try {
      const body: FulfillmentAPI.GetShipmentServicesRequest = {
        from_province_code: pickupAddress.value?.province_code,
        from_district_code: pickupAddress.value?.district_code,
        from_ward_code: pickupAddress.value?.ward_code,

        to_province_code: rawData.province_code,
        to_district_code: rawData.district_code,
        to_ward_code: rawData.ward_code,

        chargeable_weight: (weight.toggled ? weight.value : rawData.total_weight) || 500,
        total_cod_amount: rawData.cod_amount || 0,
        basket_value:  rawData.basket_value || 0,
        include_insurance: insurance.toggled ? insurance.value : rawData.include_insurance,

        connection_ids: [connectionID.value],
        index: this.getServicesIndex + this.index
      };

      const res = await this.ffmApi.getShipmentServices(body);
      if (res.index == this.getServicesIndex + this.index) {
        this.fulfillmentForm.patchValue({
          shippingServicesList: res.services.filter(s => s.is_available)
        });

        this.selectSuitableService();

        this.fulfillmentForm.patchValue({
          gettingShippingServices: false
        });
      }
    } catch(e) {
      debug.error('ERROR in Getting Shipment Services', e);
      this.fulfillmentForm.patchValue({
        gettingShippingServices: false
      });
    }
    this.cdr.detectChanges();

  }

  private selectSuitableService() {
    const selectedService = CreateMultiFfmsControlPanelController.selectSuitableService(
      this.controller.sortShipmentServiceOrders,
      this.fulfillmentForm.controls['shippingServicesList'].value
    );

    this.fulfillmentForm.patchValue({
      shipping_service_code: selectedService?.code,
    });
  }

}
