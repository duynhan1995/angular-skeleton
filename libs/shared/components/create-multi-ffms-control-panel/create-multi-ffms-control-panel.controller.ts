import { Injectable } from '@angular/core';
import {Address, FulfillmentShipmentService} from "@etop/models";
import {AuthenticateStore} from "@etop/core";
import {Subject} from "rxjs";
import {FulfillmentService} from "apps/shop/src/services/fulfillment.service";

export interface UniData<T> {
  value: T,
  toggled: boolean;
}

export interface CreateMultiFfmsUnidata {
  pickupAddress: UniData<Address>;
  weight: UniData<number>;
  tryOn: UniData<string>
  shippingNote: UniData<string>
  insurance: UniData<boolean>;
  connectionID: UniData<string>;
}

export enum SortShipmentServiceType {
  minFee = 'minFee',
  minDeliveryTime = 'minDeliveryTime'
}

export interface SortShipmentServiceOrder {
  sortType: SortShipmentServiceType;
}

export type SortShipmentServiceOrders = SortShipmentServiceOrder[];

@Injectable()
export class CreateMultiFfmsControlPanelController {

  uniData: CreateMultiFfmsUnidata = {
    pickupAddress: {
      value: null,
      toggled: true
    },
    weight: {
      value: 500,
      toggled: false
    },
    tryOn: {
      value: this.auth.snapshot.shop.try_on,
      toggled: false
    },
    shippingNote: {
      value: '',
      toggled: false
    },
    insurance: {
      value: false,
      toggled: false
    },
    connectionID: {
      value: '',
      toggled: true
    }
  };

  updateUniData$ = new Subject<boolean>();

  sortShipmentServiceOrders: SortShipmentServiceOrders = [{
    sortType: SortShipmentServiceType.minFee
  }];

  updateShipmentServiceOrders$ = new Subject<SortShipmentServiceOrders>();

  constructor(
    private auth: AuthenticateStore
  ) { }

  static selectSuitableService(sortShipmentServiceOrders: SortShipmentServiceOrders, services: FulfillmentShipmentService[]) {
    try {
      if (!services || !services.length) {
        return;
      }

      let _services: FulfillmentShipmentService[] = [];

      switch (sortShipmentServiceOrders[0].sortType) {
        case SortShipmentServiceType.minFee:
          _services = FulfillmentService.autoSelectShipmentServiceByMinFee(services);
          return _services[0];

        case SortShipmentServiceType.minDeliveryTime:
          _services = FulfillmentService.autoSelectShipmentServiceByMinDeliveryTime(services);
          return _services[0];

        default:
          return services[0];
      }
    } catch(e) {
      debug.error('ERROR in selecting suitable service', e);
      throw e;
    }
  }

  reset() {
    this.uniData = {
      pickupAddress: {
        value: null,
        toggled: true
      },
      weight: {
        value: 500,
        toggled: false
      },
      tryOn: {
        value: this.auth.snapshot.shop.try_on,
        toggled: false
      },
      shippingNote: {
        value: '',
        toggled: false
      },
      insurance: {
        value: false,
        toggled: false
      },
      connectionID: {
        value: '',
        toggled: true
      }
    }
  }

  updateUniData(data: CreateMultiFfmsUnidata, emitEvent = false) {
    this.uniData = {
      ...this.uniData,
      ...data
    };
    this.updateUniData$.next(emitEvent);
  }

  updateSortShipmentServiceOrders(orders: SortShipmentServiceOrders) {
    this.sortShipmentServiceOrders = orders;
    this.updateShipmentServiceOrders$.next(this.sortShipmentServiceOrders);
  }

}
