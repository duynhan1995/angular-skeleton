import {Component, OnInit} from '@angular/core';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {Connection} from "@etop/models/Connection";
import {FormBuilder} from "@angular/forms";
import {BaseComponent} from "@etop/core";
import {
  CreateMultiFfmsControlPanelController,
  SortShipmentServiceOrders,
  SortShipmentServiceType
} from "@etop/shared/components/create-multi-ffms-control-panel/create-multi-ffms-control-panel.controller";
import {MatTabChangeEvent} from "@angular/material/tabs/tab-group";

@Component({
  selector: 'shop-auto-select-shipping-service',
  templateUrl: './auto-select-shipping-service.component.html',
  styleUrls: ['./auto-select-shipping-service.component.scss']
})
export class AutoSelectShippingServiceComponent extends BaseComponent implements OnInit {

  sortShipmentServiceOrders: SortShipmentServiceOrders = [];

  connectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  autoSelectShipmentServiceForm = this.fb.group({
    connectionId: '',
    serviceSortType: 'fee'
  });

  selectedTabIndex = 0;

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }

  displayMap = option => option && AutoSelectShippingServiceComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  constructor(
    private fb: FormBuilder,
    private connectionStore: ConnectionStore,
    private controller: CreateMultiFfmsControlPanelController
  ) {
    super();
  }

  ngOnInit() {
    this.autoSelectShipmentServiceForm.valueChanges.subscribe(form => {
    });

    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.autoSelectShipmentServiceForm.setValue({
          connectionId: connections?.length ? connections[0].id : null,
          serviceSortType: 'fee'
        });
      });

  }

  selectTab(tab: MatTabChangeEvent) {
    this.selectedTabIndex = tab.index;
    this.autoSelectShipmentServiceForm.setValue({
      connectionId: this.connectionStore.snapshot.validConnections[0].id,
      serviceSortType: 'fee'
    });
  }


}
