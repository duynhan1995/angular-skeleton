import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Address, Connection, ConnectionMethod, TRY_ON_OPTIONS} from "@etop/models";
import {AddressService} from "apps/core/src/services/address.service";
import {
  CreateMultiFfmsControlPanelController,
  CreateMultiFfmsUnidata,
  SortShipmentServiceType
} from "@etop/shared/components/create-multi-ffms-control-panel/create-multi-ffms-control-panel.controller";
import {FromAddressModalComponent} from "../from-address-modal/from-address-modal.component";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {ShopService} from "@etop/features";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {ConnectionStore} from "@etop/features";

@Component({
  selector: 'etop-create-multi-ffms-control-panel',
  templateUrl: './create-multi-ffms-control-panel.component.html',
  styleUrls: ['./create-multi-ffms-control-panel.component.scss']
})
export class CreateMultiFfmsControlPanelComponent extends BaseComponent implements OnInit {
  @Input() color = 'topship';

  @Input() totalFfms = 0;
  @Input() totalCodAmount = 0;
  @Input() totalFee = 0;
  @Input() loading = false;
  @Input() calculatingFee = false;

  @Input() connectionsList: Connection[] = [];

  @Output() confirm = new EventEmitter();

  fromAddresses: Address[] = [];
  fromAddressIndex: number;

  uniData: CreateMultiFfmsUnidata;
  sortShipmentServiceType: SortShipmentServiceType = SortShipmentServiceType.minFee;

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }

  displayMap = option => option && CreateMultiFfmsControlPanelComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  constructor(
    private addressService: AddressService,
    private controller: CreateMultiFfmsControlPanelController,
    private modalController: ModalController,
    private shopService: ShopService,
    private auth: AuthenticateStore,
    private connectionStore: ConnectionStore,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  get cannotCreateFfm() {
    const {loading, calculatingFee, totalFfms, fromAddresses} = this;
    return loading || calculatingFee || !totalFfms || !fromAddresses?.length
  }

  ngOnInit() {
    this.uniData = this.controller.uniData;

    if (this.addressService.addressesList) {
      this.prepareAddresses(this.addressService.addressesList);
    }
    this.addressService.addressesList$.pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.prepareAddresses(data);
      });
  }

  private prepareAddresses(data: {fromAddresses: Address[], fromAddressIndex: number}) {
    const {fromAddresses, fromAddressIndex} = data;
    this.fromAddresses = fromAddresses.map(addr => {
      return {
        ...addr,
        info: addr.info.replace(/-topship/g, `-${this.color}`),
        detail: addr.info.replace(/-topship/g, `-${this.color}`),
      };
    });
    const _index = fromAddresses.findIndex(a => a.id == this.uniData.pickupAddress?.value?.id);
    this.fromAddressIndex = _index != -1 ? _index : fromAddressIndex;
  }

  newPickupAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({address}) => {
      if (address) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
        this.uniData.pickupAddress.value = address;
        this.updateUniData(true);
        this.addressService.getFromAddresses().then();
      }
    });
  }

  setPickupAddress() {
    this.uniData.pickupAddress.value = this.fromAddresses[this.fromAddressIndex];
    this.updateUniData(true);
  }

  updateUniData(emitEvent = false) {
    this.uniData.insurance.value = this.uniData.insurance.toggled;
    this.controller.updateUniData(this.uniData, emitEvent);
    this.cdr.detectChanges();
  }

  updateSortShipmentServiceOrders() {
    this.controller.updateSortShipmentServiceOrders([{sortType: this.sortShipmentServiceType}]);
    this.cdr.detectChanges();
  }

  onConfirm() {
    if (this.cannotCreateFfm) {
      return;
    }
    this.confirm.emit();
  }

}
