import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CreateMultiFfmsControlPanelComponent} from "./create-multi-ffms-control-panel.component";
import {AutoSelectShippingServiceComponent} from "./auto-select-shipping-service/auto-select-shipping-service.component";
import {CreateMultiFfmsControlPanelController} from "./create-multi-ffms-control-panel.controller";
import {EtopMaterialModule, MaterialModule} from "@etop/shared";
import {SharedModule} from "apps/shared/src/shared.module";

@NgModule({
  declarations: [
    CreateMultiFfmsControlPanelComponent,
    AutoSelectShippingServiceComponent
  ],
  exports: [
    CreateMultiFfmsControlPanelComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    EtopMaterialModule,
    SharedModule,
    NgbModule
  ],
  providers: [
    CreateMultiFfmsControlPanelController
  ]
})
export class CreateMultiFfmsControlPanelModule {
}
