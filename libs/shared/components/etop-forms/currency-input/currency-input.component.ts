
import {
  Component,
  OnInit,
  forwardRef,
  Injector,
  EventEmitter,
  Output,
  Input,
  ViewChild,
  ChangeDetectorRef,
  ElementRef,
  OnDestroy, HostListener
} from '@angular/core';
import { NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';
import { Observable, Subscription } from 'rxjs';
import List from 'identical-list';

interface FilterOption {
  id: string;
  value: string | number;
}
@Component({
  selector: 'etop-currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CurrencyInputComponent),
      multi: true
    }
  ]
})
export class CurrencyInputComponent implements OnInit, OnDestroy {
  @ViewChild('input', { static: false }) input: ElementRef;

  @Input() isDiscount;
  @Input() fontWeight;
  @Input() align = 'right';
  @Input() posfixText;
  @Input() min = 0;
  @Input() max = 999999999;
  @Input() placeholder = '';
  @Input() classInner = '';
  @Input() inputClass = '';
  @Input() errorCOD = false;
  @Input() selectInput: Observable<void>;
  @Input() isCOD = false;
  @Input() options = new List<FilterOption>([]);
  @Input() rulesFree = false;
  @Input() error = false;

  @Output() focus = new EventEmitter();
  @Output() keydown = new EventEmitter();
  @Output() blur = new EventEmitter();

  show_input = false;
  invalid_input = false;
  isShowMenu = false;
  _value;
  _prev_value;
  private _inputSelectSubscription = new Subscription();

  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    event.stopPropagation();
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    this.onBlur();
  }

  constructor(
    private injector: Injector,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (this.selectInput) {
      this._inputSelectSubscription = this.selectInput.subscribe(() =>
        this.showEdit()
      );
    }
    this.show_input = !this._value;
  }

  ngOnDestroy() {
    this._inputSelectSubscription.unsubscribe();
  }

  showEdit(e?) {
    this.show_input = true;
    this.changeDetector.detectChanges();
    this.focus.emit();
    if (this.input) {
      setTimeout(() => this.input.nativeElement.select());
    }
    this.isShowMenu = true;
  }

  selectOption(option) {
    if (option.id != this._value) {
      this._value = option.id;
      this.onBlur();
    }
  }

  onUpdate(v) {
    let value;
    if (v === 0 || v) {
      if (v > this.max) {
        value = this.max;
      } else if (v < this.min) {
        value = this.min;
      } else {
        value = v;
      }
    } else {
      value = null;
    }
    const model = this.injector.get(NgModel);
    model.viewToModelUpdate(value);
  }

  onBlur() {
    if (this._value || this._value == 0) {
      this.show_input = false;
    }
    this.changeDetector.detectChanges();
    this.isShowMenu = false;
    if (this._value == this._prev_value) {
      return;
    }
    this._prev_value = this._value;
    if (this._value > 0 && this.posfixText == 'đ' && this._value < 5000 && !this.rulesFree) {
      if (this.isCOD) {
        this.invalid_input = true;
        return toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
      }
      if (this._value < 1000 && !this.isCOD) {
        this.invalid_input = true;
        return toastr.error('Vui lòng nhập số tiền lớn hơn 1.000đ!');
      }
    } else if (this._value > 100 && this.posfixText == '%') {
      toastr.error('Giá trị giảm giá không hợp lệ!');
      this.invalid_input = true;
      return;
    } else {
      this.errorCOD = false;
      this.invalid_input = false;
      this.onUpdate(this._value);
    }
  }

  writeValue(value) {
    this._value = value;
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event, this.min < 0);
  }

  registerOnChange() {}
  registerOnTouched() {}
}
