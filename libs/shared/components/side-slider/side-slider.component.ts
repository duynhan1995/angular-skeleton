import {
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef
} from '@angular/core';

@Component({
  selector: 'etop-side-slider',
  templateUrl: './side-slider.component.html',
  styleUrls: ['./side-slider.component.scss']
})
export class SideSliderComponent implements OnInit, OnChanges {
  @ContentChild('customHeader', { static: true })
  customHeader: TemplateRef<any>;

  @Input() title: string;
  @Input() scroll_id: string;
  @Input() width = '50%';

  @Output() slideClosed = new EventEmitter();

  constructor(private hostElement: ElementRef) { }

  liteMode = false;
  private _nextLiteMode = false;

  ngOnInit() {
    this.hostElement.nativeElement.style.width = this.width;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.liteMode && changes.width) {
      this.hostElement.nativeElement.style.width = changes.width.currentValue;
      this.hostElement.nativeElement.style.left = 100 - parseInt(changes.width.currentValue) + '%';
    }
  }

  toggleLiteMode(value: boolean) {
    const nextValue = value === undefined ? !this.liteMode : value;
    if (nextValue === true) {
      this.hostElement.nativeElement.style.width = this.width;
      this.hostElement.nativeElement.style.left = 100 - parseInt(this.width) + '%';
    } else {
      this.hostElement.nativeElement.style.left = '110%';
    }
    this._nextLiteMode = nextValue;
    this.liteMode = this._nextLiteMode;
  }

  close() {
    this.toggleLiteMode(false);
    this.slideClosed.next();
  }
}
