import {
  Component,
  OnInit,
  forwardRef,
  Input,
  Injector,
  ElementRef,
  ViewChild,
  HostListener,
  OnChanges
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  NgModel
} from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-filter-select',
  templateUrl: './filter-select.component.html',
  styleUrls: ['./filter-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FilterSelectComponent),
      multi: true
    }
  ]
})
export class FilterSelectComponent
  implements OnInit, ControlValueAccessor, OnChanges {
  constructor(private util: UtilService, private injector: Injector) {}

  @Input() options;
  @Input() placeHolder;
  @Input() name;
  @Input() classOuter;
  @Input() classInner;
  @Input() useBootstrapFromControl = true;
  @Input() disabled;
  @Input() showAngle = false;
  @Input() border_radius = '0px';

  @ViewChild('inputSearch', { static: true }) inputSearch: ElementRef;

  _value;
  _keyword;
  _emittedValue;
  filterResult;
  clickedInside = false;
  isShowMenu = false;
  isShowAll = false;

  private selectedOption;

  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    // event.stopPropagation();
    this.clickedInside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.hideDropdown();
    }

    this.clickedInside = false;
  }

  ngOnInit() {
    this.initValue();
  }

  ngOnChanges() {
    this.initValue();
  }

  get isPSX() {
    return false;
  }

  filter() {
    this.filterResult = this.options
      ? this.options.filter(option =>
          this.util.searchCompare(option.name, this._keyword)
        )
      : [];
  }

  initValue() {
    if (!this.options) {
      return;
    }

    let activeOpt = this.options.find(option => option.code == this._value);
    this.selectedOption = activeOpt;
    this._keyword = (activeOpt && activeOpt.name) || '';
    this.filter();
  }

  selectOption(option) {
    this.selectedOption = option;
    this._keyword = option.name;
    let model = this.injector.get(NgModel);
    model.viewToModelUpdate(option.code);
    this.writeValue(option.code);
    this._emittedValue = option.code;
    this.hideDropdown();
  }

  toggleDropdown() {
    this.isShowMenu = !this.isShowMenu;
    if (this.isShowMenu) {
      this.inputSearch.nativeElement.select();
    } else {
      this.inputSearch.nativeElement.blur();
      this.checkValidation();
    }
  }

  toggleDropdownAll() {
    this.filterResult = this.options;
    this.toggleDropdown();
  }

  showDropdown() {
    this.filterResult = this.options;
    this.isShowMenu = true;
    this.inputSearch.nativeElement.select();
  }

  hideDropdown() {
    this.isShowMenu = false;
    this.checkValidation();
  }

  writeValue(value) {
    this._value = value;
    this.initValue();
  }

  registerOnChange() {}

  registerOnTouched() {}

  private checkValidation() {
    if (!this.selectedOption || this.selectedOption.name != this._keyword) {
      this._keyword = '';
      this._value = '';
      this.selectedOption = null;
      // check before emit multiple same value
      if (this._emittedValue != this._value) {
        let model = this.injector.get(NgModel);
        model.viewToModelUpdate('');
        this.writeValue('');
        this._emittedValue = this._value;
      }
    }
  }
}
