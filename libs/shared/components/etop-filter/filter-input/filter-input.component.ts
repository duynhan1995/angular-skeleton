import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {UtilService} from 'apps/core/src/services/util.service';
import List from 'identical-list';

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'etop-filter-input',
  templateUrl: './filter-input.component.html',
  styleUrls: ['./filter-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FilterInputComponent),
      multi: true
    }
  ]
})
export class FilterInputComponent implements OnInit, OnChanges {
  constructor(
    private util: UtilService,
    private injector: Injector
  ) {}

  @ViewChild('inputSearch', {static: false}) inputSearch: ElementRef;

  @Input() options = new List<FilterOption>([]);
  @Input() type = 'number';
  @Input() unit = 'g';
  @Input() defaultValue = 0;
  @Input() align = 'right';
  @Input() color = 'black';
  @Input() hasBorder = true;
  @Input() validateFn: (s: string) => boolean;
  @Input() placeholder;
  @Input() name;
  @Input() classOuter;
  @Input() classInner = '';
  @Input() inputClass = '';
  @Input() disabled;
  @Input() showAngle = false;
  @Input() addOptionOnBlur = true;
  @Input() textarea = false;
  @Input() prefixes = [];
  @Input() prefix;

  @Output() addOption = new EventEmitter<FilterOption>();
  @Output() prefixSelected = new EventEmitter();

  _value;
  _keyword;
  selectedOption;
  filterResult = [];

  showPrefixDropdown = false;

  showInput = false;
  isShowMenu = false;
  clickedInside = false;
  state = 0;
  // 0 no keyword,
  // 1 has keyword and invalid,
  // 2 has keyword and valid not found,
  // 3 has keyword and valid found not exactly
  // 4 has keyword and valid and found exactly


  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    this.clickedInside = true;
    event.stopPropagation();
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    this.clickedInside = false;
    this.componentBlur();
  }

  ngOnInit() {
    this.initValue();
  }

  ngOnChanges(changes) {
    this.initValue();
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  filter(textarea = false) {
    const { _keyword } = this;
    if (_keyword === null || _keyword === undefined) {
      this.state = 0;
    } else {
      this.filterResult = this.options
        ? this.options.filter(option => this.util.searchCompare(option.value, _keyword))
        : [];
      const foundExactly = this.options.get(_keyword);
      const isValid = this.validateInputValue();
      if (!isValid) {
        this.state = 1;
      } else if (foundExactly) {
        this.state = 4;
      } else {
        this.state = this.filterResult.length ? 3 : 2;
      }
    }
    if (textarea && this.state > 0) {
      this.filterResult = this.options
        ? this.options.filter(option => this.util.searchCompare(option.value, _keyword))
        : [];
    }
    this.filterResult = this.filterResult.filter(res => ["0", ""].indexOf(res.value) < 0);
  }

  initValue() {
    if (!this.options) {
      return;
    }
    this.selectedOption = this.options.get(this._value);
    this.filter();
  }

  selectOption(option) {
    let _option = option;
    if (option.id != this._value) {
      this.selectedOption = _option;
      this._keyword = _option.value;
      let model = this.injector.get(NgModel);
      model.viewToModelUpdate(_option.id);
      this.writeValue(_option.id);
    }
    this.hideDropdown();
  }

  onAddOption(keyword: string) {
    const option = {
      value: keyword || this.defaultValue.toString(),
      id: keyword || this.defaultValue.toString(),
    };

    if (!this.options.get(option.id)) {
      this.options.add(option);
      this.addOption.emit(option);
    }
    this.selectOption(option);
    return option;
  }

  toggleDropdown() {
    if (this.isShowMenu) {
      this.hideDropdown();
    } else {
      this.showEdit();
    }
  }

  showEdit() {
    this.filterResult = this.options.asArray();
    this.showInput = true;
    this.showDropdown();
  }

  showDropdown() {
    this.isShowMenu = true;
    this.hidePrefix();
    setTimeout(() => {
      $(this.inputSearch.nativeElement).select();
    }, 50);
  }

  onInputClick() {
    this.showEdit();
  }

  hideDropdown() {
    this.showInput = false;
    this.isShowMenu = false;
    if ((this.state === 3 || this.state === 2) && !this._value) {
      this._keyword = "";
      this.state = 0;
    }
    if (this.selectedOption) {
      this._keyword = this.selectedOption.value;
    }
  }

  writeValue(value) {
    this._value = value || this.defaultValue;
    this._keyword = value || this.defaultValue;
    this.initValue();
  }

  registerOnChange() { }

  registerOnTouched() { }

  private validateInputValue() {
    if (this.validateFn) {
      return this.validateFn(this._keyword);
    }

    if (this.type == 'number' && this._keyword) {
      const num = Number(this._keyword);
      if (Number.isNaN(num) || num < 0) {
        return false;
      }
    }

    return true;
  }

  onKeyTab(event) {
    if (this.addOptionOnBlur) {
      this.componentBlur();
    }
  }

  componentBlur(textarea = false) {
    try {
      this.showInput = false;
      this.isShowMenu = false;
      if (!textarea && this._keyword < 1000 && this._keyword > 0 && this.unit == "đ") {
        this.state = 1;
        return toastr.error('Vui lòng nhập số tiền lớn hơn 1000đ!');
      } else {
        this.state = 4;
      }

      if (!this.addOptionOnBlur && !this.clickedInside && (this.state == 2 || this.state == 4 || this.state == 0)) {
        this.filter();
        this.hideDropdown();
      }

      if (((this.clickedInside || this.addOptionOnBlur) && [2, 3, 4].indexOf(this.state) > -1)
        || textarea) {
        this.onAddOption(this._keyword);
        this.hideDropdown();
      }
    } catch (e) {
    }
  }

  showPrefix() {
    this.showPrefixDropdown = true;
    this.hideDropdown();
  }

  hidePrefix() {
    this.showPrefixDropdown = false;
  }

  selectPrefix(option) {
    this.prefix = option;
    this.hidePrefix();
    this.prefixSelected.emit(option);
  }
}
