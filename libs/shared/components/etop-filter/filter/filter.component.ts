import { Component, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import {FilterOptions, Filters, Query, QueryOption} from '@etop/models';

@Component({
  selector: 'etop-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  @Output() filterChanged = new Subject<Filters>();
  @Output() queryChanged = new Subject<Query>();

  toppings = new FormControl();
  @Input() filters: FilterOptions;
  @Input() query: QueryOption;

  @Input() topshipFilter = false;

  constructor() {
  }

  ngOnInit() {
    if (this.filters) {
      this.toppings.setValue(this.filters.filter(filter => filter.show));
    }
  }

  get availableFilters() {
    return this.filters && this.filters.filter(filter => !filter.fixed);
  }

  changeSelect(options: FilterOptions) {
    this.filters.forEach(filter => filter.show = false);
    options.forEach(filter => filter.show = true);
    this.filters.forEach(filter => {
      if (!filter.show) { filter.value = ''; }
    });
    this.onSubmitFilter();
  }

  public onSubmitFilter() {
    const filters: Filters = this.filters.filter(f => !!f.value).map(f => ({
      name: f.name,
      op: f.operator,
      value: f.value
    }));
    this.filterChanged.next(filters);
  }

  public onSubmitQuery() {
    const query: Query = {
      name: this.query.value
    };
    this.queryChanged.next(query);
  }

}
