import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotSubscriptionComponent } from 'libs/shared/components/not-subscription/not-subscription.component';
import { EtopPipesModule } from '@etop/shared/pipes/etop-pipes.module';

@NgModule({
  declarations: [
    NotSubscriptionComponent
  ],
  exports: [
    NotSubscriptionComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule
  ]
})
export class NotSubscriptionModule { }
