import { Component, OnInit } from '@angular/core';
import { CmsService } from 'apps/core/src/services/cms.service';

@Component({
  selector: 'etop-not-subscription',
  templateUrl: './not-subscription.component.html',
  styleUrls: ['./not-subscription.component.scss']
})
export class NotSubscriptionComponent implements OnInit {
  banner = '';
  constructor(
    private cms: CmsService,
  ) { }

  ngOnInit(): void {
    this.getSubscriptionBanner();
  }

  async getSubscriptionBanner(){
    await this.cms.initBanners();
    const result = this.cms.getNotSubscription();
    if(result){
      this.banner = result;
    }
  }

}
