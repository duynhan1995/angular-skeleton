import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IonicModule} from "@ionic/angular";
import {IonShipmentServicesOptionsComponent} from "libs/shared/components/etop-ionic/ion-shipment-services-options/ion-shipment-services-options.component";
import {EtopPipesModule} from "@etop/shared/pipes";
import {FulfillmentService} from "@etop/features/fabo/fulfillment/fulfillment.service";

@NgModule({
  declarations: [IonShipmentServicesOptionsComponent],
  imports: [
    CommonModule,
    IonicModule,
    EtopPipesModule
  ],
  exports: [
    IonShipmentServicesOptionsComponent
  ],
  providers: [
    FulfillmentService
  ]
})
export class IonShipmentServicesOptionsModule { }
