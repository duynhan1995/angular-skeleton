import { Component, OnInit } from '@angular/core';
import { FulfillmentApi } from '@etop/api';
import { FulfillmentService } from '@etop/features/fabo/fulfillment/fulfillment.service';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { BaseComponent } from '@etop/core';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { FulfillmentShipmentService, ShipmentCarrier } from 'libs/models/Fulfillment';

@Component({
  selector: 'etop-ion-shipment-services-options',
  templateUrl: './ion-shipment-services-options.component.html',
  styleUrls: ['./ion-shipment-services-options.component.scss']
})
export class IonShipmentServicesOptionsComponent extends BaseComponent
  implements OnInit {
  carriers: ShipmentCarrier[] = [];

  getServicesIndex = 0;
  gettingServices = false;

  errorMessage = '';

  shipmentServicesRequest$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.shipmentServicesRequest),
    distinctUntilChanged(
      (prev, next) => JSON.stringify(prev) == JSON.stringify(next)
    )
  );

  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private fulfillmentService: FulfillmentService,
    private connectionStore: ConnectionStore,
    private connectionService: ConnectionService,
    private fulfillmentApi: FulfillmentApi,
  ) {
    super();
  }

  get headerTitle() {
    const shipmentServicesRequest = this.confirmOrderStore.snapshot
      .shipmentServicesRequest;
    return (
      (shipmentServicesRequest?.to_ward_code && 'Chọn gói vận chuyển') ||
      'Vui lòng điền đầy đủ thông tin để lấy gói vận chuyển'
    );
  }

  get isEnoughData() {
    return !!this.confirmOrderStore.snapshot.shipmentServicesRequest
      .to_ward_code;
  }

  isSelectedService(service: FulfillmentShipmentService) {
    const selectedService = this.confirmOrderStore.snapshot
      .activeShipmentService;
    return selectedService?.unique_id == service?.unique_id;
  }

  ngOnInit() {
    this.shipmentServicesRequest$
      .pipe(takeUntil(this.destroy$))
      .subscribe(request => {
        this.getServices(request).then();
      });
  }

  selectService(service: FulfillmentShipmentService) {
    this.confirmOrderService.selectShipmentService(service);
  }

  async getServices(data) {
    if (!data || !data.to_ward_code) {
      return;
    }

    this.selectService(null);

    this.gettingServices = true;
    try {
      this.getServicesIndex++;
      const body = {
        ...data,
        index: this.getServicesIndex
      };
      const loggedInConnections: any = this.connectionStore.snapshot.loggedInConnections;
      const shopConnectionIds = loggedInConnections && loggedInConnections.map(con => con.connection_id);

      const res = await this.fulfillmentApi.getShipmentServices(body);

      if (res.index == this.getServicesIndex) {
        this.carriers = this.fulfillmentService.groupShipmentServicesByConnection(
          res.services,
          shopConnectionIds
        );
        this.gettingServices = false;
      }
    } catch (e) {
      debug.error('ERROR in Getting Services', e);
      this.errorMessage = e.message;
      this.gettingServices = false;
    }
  }
}
