import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors} from '@angular/forms';
import {ValueAccessorBase} from 'apps/core/src/interfaces/ValueAccessorBase';
import {IonInput} from '@ionic/angular';

@Component({
  selector: 'etop-ion-input-format-number',
  templateUrl: './ion-input-format-number.component.html',
  styleUrls: ['./ion-input-format-number.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: IonInputFormatNumberComponent, multi: true},
    {provide: NG_VALIDATORS, useExisting: IonInputFormatNumberComponent, multi: true}
  ]
})
export class IonInputFormatNumberComponent extends ValueAccessorBase<number> implements OnInit {
  @Output() blur = new EventEmitter();
  @Output() focus = new EventEmitter();

  @ViewChild('ionInput', {static: true}) ionInput: IonInput;

  @Input() cssClass: string;
  @Input() placeholder: string;
  @Input() textLabel: string;
  @Input() error = false;
  @Input() errorMessage = false;
  @Input() disabled: boolean;
  @Input() topshipInput = false;
  @Input() formatNumberUnit: string;
  @Input() lines: string;

  @Input() debug = false;

  format_number = false;

  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  writeValue(value: number) {
    super.writeValue(value);
    debug.log('value write', value, typeof value);
    this.format_number = typeof value == 'number' && value >= 0;
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
  }

  changeValue(d) {
    this.error = !Number(d) && (!!d && d != 0);
  }

  onBlur() {
    const _value = this.value?.toString();

    if (!_value || !_value.length || Number(_value) < 0) {
      this.format_number = false;
      this.changeDetector.detectChanges();
      return;
    }

    this.format_number = !this.format_number;
    this.changeDetector.detectChanges();
    if (!this.format_number && this.ionInput) {
      this.ionInput.setFocus();
    }
  }

  textFocus() {
    this.format_number = false;
    this.ionInput.setFocus();
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return !Number(control.value) && {
      invalid: true
    };
  }
}
