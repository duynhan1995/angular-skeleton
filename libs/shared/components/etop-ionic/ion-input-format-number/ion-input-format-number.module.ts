import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonInputFormatNumberComponent } from './ion-input-format-number.component';
import {IonicModule} from "@ionic/angular";
import {FormsModule} from "@angular/forms";
import {EtopPipesModule} from "@etop/shared/pipes";



@NgModule({
  declarations: [IonInputFormatNumberComponent],
  exports: [
    IonInputFormatNumberComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    EtopPipesModule
  ]
})
export class IonInputFormatNumberModule { }
