import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';

export enum NavigationDirection {
  FORWARD = 1,
  BACKWARD = -1
}

export class PaginationOpt {
  nextDisabled: boolean;
  previousDisabled: boolean;
  hidePerpage: boolean;
}

@Component({
  selector: 'etop-pagination',
  templateUrl: './etop-pagination.component.html',
  styleUrls: ['./etop-pagination.component.scss']
})
export class EtopPaginationComponent implements OnInit, OnChanges {
  setting: PaginationOpt = {
    nextDisabled: false,
    previousDisabled: false,
    hidePerpage: false
  };
  @Input() perpage: Number = 20;
  @Input() config: PaginationOpt;
  @Input() perpage_array = [20, 50, 100, 200, 500];
  @Input() topship = false;

  @Output() navigate = new EventEmitter<NavigationDirection>();
  @Output() perpageChange = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {
    this.perpageChange.emit(this.perpage_array[0]);
  }

  ngOnChanges(changes: SimpleChanges): void {
    let config = changes.config && changes.config.currentValue;
    if (config) {
      this.setting = {
        ...this.setting,
        ...config
      };
    }
  }

  nav($e, direction) {
    $e.preventDefault();
    if (
      ($e.target.name == 'prev' && this.setting.previousDisabled) ||
      ($e.target.name == 'next' && this.setting.nextDisabled)
    )
      return;
    this.navigate.emit(direction);
  }

  perpageChanged(value) {
    this.perpageChange.emit(parseInt(value, 10));
  }
}
