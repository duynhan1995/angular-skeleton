export interface Paging {
  offset: number;
  limit: number;
  sort?: string;
}
