import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'etop-table-placeholder',
  templateUrl: './etop-table-placeholder.component.html',
  styleUrls: ['./etop-table-placeholder.component.scss']
})
export class EtopTablePlaceholderComponent implements OnInit {
  @Input() isHeader = false;

  constructor() { }

  ngOnInit() {
  }

}
