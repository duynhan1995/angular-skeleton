import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef, EventEmitter,
  HostListener,
  Input, OnChanges,
  OnInit, Output, SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { NavigationDirection, PaginationOpt } from '@etop/shared/components/etop-common/etop-pagination/etop-pagination.component';

@Component({
  selector: 'etop-table',
  templateUrl: './etop-table.component.html',
  styleUrls: ['./etop-table.component.scss']
})
export class EtopTableComponent implements OnInit, OnChanges {

  @ContentChild('item', { static: true })
  itemTemplate: TemplateRef<any>;

  @ContentChild('emptyItem', { static: true })
  emptyItemTemplate: TemplateRef<any>;

  @ContentChild('header', { static: true })
  headerTemplate: TemplateRef<any>;

  @Input() items: Array<any> = new Array<any>();
  @Input() perpage = 20;
  @Input() perpage_array = [20, 50, 100, 200, 500];

  @Input() topship = false;
  @Input() scroll_id: string;
  @Input() show_paging = true;
  @Input() width = '50%';
  @Input() loading = false;
  @Input() externalCurrentPage = 0;
  @ViewChild('rowContainer', { static: true }) rowContainer: ElementRef;

  @Output() requestNewPage = new EventEmitter<{ page: number, perpage: number }>();

  liteMode = false;


  currentPage = 1;

  paginationConfig: PaginationOpt = {
    hidePerpage: false,
    nextDisabled: false,
    previousDisabled: false
  };

  constructor(
    private hostElement: ElementRef,
    private ref: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.externalCurrentPage) {
      this.currentPage = changes.externalCurrentPage.currentValue;
    }
    if (changes.items && changes.items.currentValue) {
      if (this.rowContainer) {
        this.rowContainer.nativeElement.scrollTop = 0;
      }

      this.paginationConfig = Object.assign({}, this.paginationConfig, {
        previousDisabled: this.currentPage <= 1,
        nextDisabled: changes.items.currentValue.length < this.perpage
      });
    }
    if (this.liteMode && changes.width) {
      this.hostElement.nativeElement.style.width = changes.width.currentValue;
    }
  }

  toggleNextDisabled(disabled: boolean) {
    this.paginationConfig = {
      ...this.paginationConfig,
      nextDisabled: disabled
    };
  }

  decreaseCurrentPage(number: number) {
    this.currentPage -= number;
  }

  toggleLiteMode(value?: boolean) {
    const nextValue = value === undefined ? !this.liteMode : value;
    if (nextValue === true) {
      this.liteMode = true;
      this.hostElement.nativeElement.style.width = this.width;
    } else {
      this.hostElement.nativeElement.style.width = '100%';
    }
    this.liteMode = nextValue;

    this.paginationConfig = Object.assign({}, this.paginationConfig, {
      hidePerpage: nextValue
    });
  }

  navigate(direction) {
    switch (direction) {
      case NavigationDirection.BACKWARD:
        this.currentPage--;
        break;
      case NavigationDirection.FORWARD:
        this.currentPage++;
        break;
    }
    this.currentPage = this.currentPage < 0 ? 0 : this.currentPage;
    this._requestNewPage();
  }

  perpageChanged(value) {
    this.currentPage = 1;
    this.perpage = value;
    this._requestNewPage();
  }

  resetPagination() {
    this.currentPage = 1;
    this.paginationConfig = Object.assign({}, this.paginationConfig, {
      previousDisabled: true,
    });
    this._requestNewPage();
  }

  private _requestNewPage() {
    this.requestNewPage.emit({
      page: this.currentPage,
      perpage: this.perpage
    });
  }

  get total_rows() {
    let total_rows = [];
    for (let i = 0; i < this.perpage; i++) {
      total_rows.push(i);
    }
    return total_rows;
  }

}
