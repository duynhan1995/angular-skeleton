import { Component, Input, OnInit } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { ValueAccessorBase } from 'apps/core/src/interfaces/ValueAccessorBase';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'etop-material-tag-input',
  templateUrl: './material-tag-input.component.html',
  styleUrls: ['./material-tag-input.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: MaterialTagInputComponent, multi: true}
  ]
})
export class MaterialTagInputComponent extends ValueAccessorBase<string[]> implements OnInit {

  @Input() error = false;
  @Input() label;
  @Input() placeholder = '';
  @Input() topshipInput = false;

  removable = true;
  addOnBlur = true;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnInit() {
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (!this.value) {
      this.value = [];
    }

    if ((value || '').trim()) {
      if (this.value.indexOf(value.trim().toLowerCase()) < 0) {
        this.value.push(value.trim().toLowerCase());
      }
      this.value = this.value.slice(0);
    }

    if (input) {
      input.value = '';
    }
  }

  remove(index) {
    this.value.splice(index, 1);
    this.value = this.value.slice(0);
  }

}
