import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
  NgForm,
} from '@angular/forms';
import { ValueAccessorBase } from 'apps/core/src/interfaces/ValueAccessorBase';
import { MatInput } from '@angular/material/input';
import {
  DateAdapter,
  ErrorStateMatcher,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MaterialInput } from '@etop/shared/components/etop-material/material-input.interface';
import { MatSelect } from '@angular/material/select';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';
import moment from 'moment';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';

class CustomerStateMatcher extends ErrorStateMatcher {
  constructor(public ctx: MaterialYearPickerComponent) {
    super();
  }

  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return this.ctx.error;
  }
}
export const YEAR_MODE_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'etop-material-year-picker',
  templateUrl: './material-year-picker.component.html',
  styleUrls: ['./material-year-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MaterialYearPickerComponent,
      multi: true,
    },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: YEAR_MODE_FORMATS },
  ],
})
export class MaterialYearPickerComponent
  extends ValueAccessorBase<any>
  implements OnInit, MaterialInput {
  @ViewChild('matInput', { static: false }) matInput: MatInput;

  @Input() appearance: any;
  @Input() textLabel: string;
  @Input() keyboardType = 'text';
  @Input() error = false;
  @Input() errorMessage: string;
  @Input() placeholder: string;
  @Input() disabled: boolean;
  @Input() displayHTML = false;
  @Input() topshipInput = false;
  @Input() displayMap: (any) => string;
  @Input() valueMap: (any) => string;

  @Output() blur = new EventEmitter();
  @Output() select = new EventEmitter();
  @Output() enter = new EventEmitter();

  date = new FormControl(moment());
  // NOTE: *** use for CUSTOM-TYPE material input where value will never change ***
  immutableValue = '-';

  errorStateMatcher = new CustomerStateMatcher(this);

  ngOnInit() {}

  focusInput() {
    setTimeout((_) => this.matInput.focus());
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  focus() {
    if (this.matInput) {
      this.matInput.focus();
    }
  }

  onInputYearChange() {
    const _inputValue = this.matInput.value;
    if (!_inputValue) {
      this.value = null;
    }
    this.value = this.matInput.value;
  }

  chosenYearHandler(chosenDate: Moment, datepicker?: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.year(chosenDate.year());
    this.date.setValue(ctrlValue);
    if (datepicker) {
      datepicker.close();
    }
  }

  _openDatepickerOnClick(datepicker: MatDatepicker<Moment>) {
    if (!datepicker.opened) {
      datepicker.open();
    }
  }
}
