import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ValueAccessorBase } from 'apps/core/src/interfaces/ValueAccessorBase';
import {MatInput} from "@angular/material/input";
import {MaterialInput} from "@etop/shared/components/etop-material/material-input.interface";

@Component({
  selector: 'etop-material-input-format-number',
  templateUrl: './material-input-format-number.component.html',
  styleUrls: ['./material-input-format-number.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: MaterialInputFormatNumberComponent, multi: true}
  ]
})
export class MaterialInputFormatNumberComponent extends ValueAccessorBase<number> implements OnInit, MaterialInput {
  @Output() blur = new EventEmitter();

  @ViewChild('input', { static: true }) input: MatInput;

  @Input() placeholder: string;
  @Input() appearance: any;
  @Input() textLabel: string;
  @Input() error = false;
  @Input() errorMessage: string;
  @Input() disabled: boolean;
  @Input() topshipInput = false;
  @Input() formatNumberUnit: string;
  @Input() min = 0;

  format_number = false;

  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {}

  writeValue(value: number) {
    super.writeValue(value);
    this.format_number = typeof value == 'number' && value >= 0;
    this.changeDetector.detectChanges();
  }

  changeValue() {
    if (!this.value) {
      this.format_number = false;
      this.changeDetector.detectChanges();
    }
  }

  formatNumber() {
    const _value = this.value?.toString();

    if (!_value || !_value.length || Number(_value) < 0) {
      this.format_number = false;
      this.changeDetector.detectChanges();
      return;
    }

    this.format_number = !this.format_number;
    this.changeDetector.detectChanges();
    if (!this.format_number && this.input) {
      this.input.focus();
    }
  }

  focus() {
    this.format_number = false;
    this.changeDetector.detectChanges();
    this.input.focus();
  }

}
