import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {UtilService} from 'apps/core/src/services/util.service';
import {ValueAccessorBase} from "apps/core/src/interfaces/ValueAccessorBase";

@Component({
  selector: 'etop-material-chips-autocomplete',
  templateUrl: './material-chips-autocomplete.component.html',
  styleUrls: ['./material-chips-autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, useExisting: MaterialChipsAutocompleteComponent, multi: true
    }
  ]
})
export class MaterialChipsAutocompleteComponent extends ValueAccessorBase<string[]> implements OnInit, OnChanges {
  @ViewChild('input', { static: true }) input: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: true }) matAutocomplete: MatAutocomplete;

  @Input() textLabel: string;
  @Input() placeholder: string;
  @Input() options: any[] = [];
  @Input() topshipInput = false;
  @Input() displayMap: (any) => string;
  @Input() valueMap: (any) => string;

  @Input() multiple = true;

  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  filteredOptions: any[] = [];
  remainedOptions: any[] = [];
  selectedOptions: any[] = [];


  searchValue: string;

  constructor(
    private util: UtilService
  ) {
    super();
  }

  ngOnInit() {
    this.remainedOptions = this.options;
    this.onSearch();
  }

  ngOnChanges(changes: SimpleChanges) {
    const options = changes.options;
    if (options && options.currentValue) {
      this.remainedOptions = options.currentValue;
      this.writeValue(this.value);
    }
  }

  onSearch() {
    this.filteredOptions = this.searchValue ? this._filter(this.searchValue) : this.util.deepClone(this.remainedOptions);
  }

  updateOptions() {
    const selectedOptionsValues = this.selectedOptions.map(opt => this.valueMap ? this.valueMap(opt) : opt);
    this.remainedOptions = this.options?.filter(option => {
      return !selectedOptionsValues.includes(this.valueMap ? this.valueMap(option) : option);
    });
  }

  remove(index): void {
    if (index >= 0) {
      this.selectedOptions.splice(index, 1);
      this.value = this.selectedOptions.map(opt => this.valueMap ? this.valueMap(opt) : opt);
    }
    this.updateOptions();
    this.input.nativeElement.value = null;
    this.searchValue = null;
  }

  onSelectOption(event: MatAutocompleteSelectedEvent) {
    this.selectedOptions.push(event.option.value);
    this.value = this.selectedOptions.map(opt => this.valueMap ? this.valueMap(opt) : opt);

    this.updateOptions();
    this.input.nativeElement.value = null;
    this.searchValue = null;
  }

  private _filter(value: any): string[] {
    let filterValue = value;
    const displayMap = this.displayMap;
    if (typeof value == 'string') {
      filterValue = this.util.removeDiacritic(filterValue.toLowerCase());
    }
    if (typeof value == 'object' && displayMap) {
      filterValue = displayMap(filterValue).toLowerCase();
    }
    return this.remainedOptions.filter(
      opt => this.util.removeDiacritic((displayMap ? displayMap(opt) : opt))?.toLowerCase().includes(filterValue)
    );
  }

  writeValue(value: string[]) {
    super.writeValue(value);
    this.remainedOptions = this.options;
    if (value) {
      this.selectedOptions = this.remainedOptions?.filter(option => {
        const _option = this.valueMap ? this.valueMap(option) : option;
        return this.multiple ? value.includes(_option) : value == _option;
      });
    } else {
      this.selectedOptions = [];
    }
    this.updateOptions();
    this.onSearch();
    this.input.nativeElement.value = null;
    this.searchValue = null;
  }

  resetSearchValue() {
    this.input.nativeElement.value = null;
  }
}
