import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component({
  selector: 'etop-material-select-date-range',
  templateUrl: './material-select-date-range.component.html',
  styleUrls: ['./material-select-date-range.component.scss']
})
export class MaterialSelectDateRangeComponent  implements OnInit {
  @Output() rangeSelect = new EventEmitter();
  @Input() displayIcon = false;
  @Input() labelBold = false;
  @Input() cssClass = '';
  @Input() valueIndex = 0;
  @Input() hasAllRange = false;

  datetime: { startDate: Moment; endDate: Moment };

  rangeLabel = '';
  range = this.fb.group({
    start: '',
    end: ''
  });

  ranges = [
    {
      label: 'Trong tuần',
      value: [moment().startOf('isoWeek'), moment()]
    },
    {
      label: 'Hôm nay',
      value: [moment(), moment()]
    },
    {
      label: 'Trong tháng',
      value: [moment().startOf('month'), moment()]
    },
    {
      label: 'Tháng trước',
      value: [
        moment().subtract(1, 'month').startOf('month'),
        moment().subtract(1, 'month').endOf('month')
      ]
    },
    {
      label: '7 ngày gần đây',
      value: [moment().subtract(6, 'days'), moment()]
    },
    {
      label: '30 ngày gần đây',
      value: [moment().subtract(29, 'days'), moment()]
    },
  ];
  maxDate: Date;

  constructor(
    private fb: FormBuilder
  ) {
    this.maxDate = new Date();
  }

  ngOnInit() {
    if (this.hasAllRange) {
      this.ranges.push({
        label: 'Tất cả ngày',
        value: [null]
      })
    }
    this.selectedRange(this.ranges[this.valueIndex || 0]);
    let endDate = '';
    this.range.get('end').valueChanges.subscribe((res) => {
      if (res && endDate != res) {
        endDate = res;
        this.datetime = {
          startDate: moment(this.range.controls['start'].value),
          endDate: moment(res)
        }
        this.rangeLabel = moment(this.range.controls['start'].value).format('DD/MM/yyy') + ' - ' + moment(res).format('DD/MM/yyy');
        this.rangeSelect.next(this.datetime);
      }
    })
  }

  selectedRange(range) {
    this.rangeLabel = range?.label;
    this.datetime = {
      startDate: range?.value[0],
      endDate: range?.value[1]
    }
    this.rangeSelect.next(this.datetime);
  }

}
