import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import {MatInput} from "@angular/material/input";
import {ValueAccessorBase} from "apps/core/src/interfaces/ValueAccessorBase";
import {Subject} from "rxjs";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'etop-material-nofilter-input-autocomplete',
  templateUrl: './material-nofilter-input-autocomplete.component.html',
  styleUrls: ['./material-nofilter-input-autocomplete.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: MaterialNofilterInputAutocompleteComponent,
    multi: true
  }]
})
export class MaterialNofilterInputAutocompleteComponent extends ValueAccessorBase<string> implements OnInit {
  @ViewChild('input', { static: true }) input: MatInput;

  @Input() appearance: any;
  @Input() textLabel: string;
  @Input() placeholder: string;
  @Input() options: any[] = [];
  @Input() displayMap: (any) => string;
  @Input() valueMap: (any) => string;
  @Input() inputMap: (any) => string;
  @Input() keepValueAsSearchText = false;

  @Input() topshipInput = false;

  @Output() searching = new EventEmitter<string>();

  searching$ = new Subject<string>();
  searchText = "";

  constructor(
      private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.searching$.pipe(debounceTime(500))
      .subscribe(searchText => {
        if (typeof searchText == 'string') {
          if (this.keepValueAsSearchText) {
            this.value = searchText;
          }
          this.searching.emit(searchText);
        }
      });
  }

  selectOption(event: MatAutocompleteSelectedEvent) {
    this.value = this.valueMap ? this.valueMap(event.option.value) : event.option.value;
    this.input.value = this.inputMap ? this.inputMap(event.option.value) :
      (this.displayMap ? this.displayMap(event.option.value) : event.option.value);
  }

  search() {
    if (!this.searchText) {
      this.value = "";
    }
    this.searching$.next(this.searchText);
  }

  writeValue(value: any) {
    super.writeValue(value);

    const _option = this.options.find(opt => {
      const optValue = this.valueMap ? this.valueMap(opt) : opt;
      return optValue == value;
    });

    if (_option && !this.keepValueAsSearchText) {
      this.input.value = this.inputMap ? this.inputMap(_option) :
        (this.displayMap ? this.displayMap(_option) : _option);

      this.searchText = this.inputMap ? this.inputMap(_option) :
        (this.displayMap ? this.displayMap(_option) : _option);
    } else {
      this.input.value = this.keepValueAsSearchText ? value : "";
      this.searchText = this.keepValueAsSearchText ? value : "";
    }

    this.cdr.detectChanges();
  }

}
