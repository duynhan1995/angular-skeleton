import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {Ticket, TicketComment} from "@etop/models";
import {UtilService} from "apps/core/src/services/util.service";
import {Observable} from "rxjs";
import {takeUntil} from "rxjs/operators";
import { TicketQuery, TicketService } from '@etop/state/shop/ticket';
import { RelationshipQuery } from '@etop/state/relationship';
import {MatDialogController} from "@etop/shared";

const ACCEPT_IMAGE_FILE_EXTENSION = 'image/png,image/jpg,image/jpeg,image/gif';

interface ImageSource {
  src: string;
  id: string;
}

@Component({
  selector: 'shared-shop-ticket-comments',
  templateUrl: './shop-ticket-comments.component.html',
  styleUrls: ['./shop-ticket-comments.component.scss']
})
export class ShopTicketCommentsComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('imageMessageInput', {static: false}) imageMessageInput: ElementRef;

  activeTicket$: Observable<Ticket> = this.ticketQuery.selectActive();
  activeTicketComments$ = this.ticketQuery.selectActive(ticket => ticket.comments?.map(cmt => {
    return {
      ...cmt,
      createdByDisplay: this.relationshipQuery.getRelationshipNameById(cmt.created_by)
    }
  }));

  textMessage = '';

  imageSources: ImageSource[] = [];

  initializing = true;
  showBubbleMenu: number;

  @HostListener('document:click', ['$event'])
  public documentClick(event) {
    if (event.target.className.includes('bubble-menu-icon')) {
      return;
    }
    this.showBubbleMenu = null;
  }

  constructor(
    private auth: AuthenticateStore,
    private ticketService: TicketService,
    private ticketQuery: TicketQuery,
    private util: UtilService,
    private relationshipQuery: RelationshipQuery,
    private matDialogController: MatDialogController,
  ) {
    super();
  }

  ngOnInit() {
    this.chatBoxEnterKeyHandler();
  }

  ngAfterViewInit() {
    this.activeTicketComments$.pipe(takeUntil(this.destroy$))
      .subscribe(cmts => {
        if (cmts && this.initializing) {
          const latestCmtID = cmts[cmts.length - 1]?.id;
          setTimeout(_ => {
            this.initializing = false;
            setTimeout(__ => {
              const scrollToElement = document.getElementById(latestCmtID);
              scrollToElement?.scrollIntoView({behavior: "smooth"});
            });
          }, 1000);
        }
      });
  }

  chatBoxEnterKeyHandler() {
    setTimeout(_ => {
      const chatBox = document.getElementById('chat-box');
      const toolbar = document.getElementById('message-toolbar');
      chatBox?.addEventListener('keydown', (event: any) => {
        if (event.key == 'Enter' && !event.shiftKey) {
          event.preventDefault();
          return;
        }

        setTimeout(__ => {
          if (event.key == 'Backspace' || event.key == 'Delete') {
            chatBox.style.height = 'auto';
          }
          chatBox.style.height = chatBox.scrollHeight + 'px';

          toolbar.style.alignItems = chatBox.scrollHeight > 50 ? 'flex-end' : 'center';

          const start = event.target.selectionStart;
          if (chatBox.scrollHeight > 50 && start >= this.textMessage.length) {
            chatBox.scrollTo({top: chatBox.scrollHeight});
          }
        });
      });
    });
  }

  onTextMessageChanged() {
    setTimeout(_ => {
      const chatBox = document.getElementById('chat-box');
      const toolbar = document.getElementById('message-toolbar');
      if (!this.textMessage) {
        toolbar.style.alignItems = 'center';
        chatBox.style.height = 'auto';
      }
    });
  }

  uploadImage() {
    this.imageMessageInput.nativeElement.click();
  }

  async onImageSelected(event) {
    try {
      if (Object.values(event.target.files).some((file: File) => !ACCEPT_IMAGE_FILE_EXTENSION.includes(file.type))) {
        toastr.error('Định dạng file không hợp lệ. Chỉ chấp nhận các định dạng: .png, .jpg, .jpeg');
        return;
      }

      const files: File[] = Object.values(event.target.files);

      const promises = files.map((file, index) => async () => {
        const image: any = await this.util.fileToImage(file);
        const id = (new Date().getTime() + index).toString();
        this.imageSources.push({
          src: image.src,
          id
        });
        this.ticketService.updateTicketCommentPreUploadImagesFiles(id, file);
      });

      await Promise.all(promises.map(p => p()));

      setTimeout(_ => {
        const chatBox = document.getElementById('chat-box');
        chatBox.focus();
      });

    } catch (e) {
      debug.error('ERROR in onImageSelected', e);
      toastr.error('Có lỗi xảy ra khi chọn hình ảnh.', e.code && (e.message || e.msg));
    }
  }

  onImageRemoved(index: number) {
    this.imageSources.splice(index, 1);
  }

  createTicketComment() {
    const newTicketComments: TicketComment[] = this.imageSources.map((imgSrc) => {
      return {
        ...new TicketComment(),
        id: imgSrc?.id,
        image_url: imgSrc?.src,
        account_id: this.ticketQuery.getActive()?.shop?.id,
        created_by: this.auth.snapshot.user?.id,
        created_at: new Date()
      };
    });
    this.imageSources = [];

    if (this.textMessage) {
      const newTicketComment: TicketComment = {
        ...new TicketComment(),
        id: new Date().getTime().toString(),
        message: this.textMessage,
        account_id: this.ticketQuery.getActive()?.shop?.id,
        created_by: this.auth.snapshot.user?.id,
        created_at: new Date()
      };
      this.textMessage = '';
      this.onTextMessageChanged();
      newTicketComments.push(newTicketComment);
    }

    newTicketComments.forEach(cmt => {
      this.ticketService.createTicketCommentHaveImg(cmt).catch(e => {
        toastr.error('Gửi phản hồi không thành công.', e.code && (e.message || e.msg));
      });
    });
    const scrollToID = newTicketComments[newTicketComments?.length - 1]?.id;

    setTimeout(_ => {
      const scrollToElement = document.getElementById(scrollToID);
      scrollToElement?.scrollIntoView({behavior: "smooth"});
    });
  }

  retryCreateTicketComment(comment) {
    this.ticketService.createTicketCommentHaveImg(comment).catch(e => {
      toastr.error('Gửi phản hồi không thành công.', e.code && (e.message || e.msg));
    });
  }

  focusMenu(index: number) {
    this.showBubbleMenu = index;
  }

  openImage(imageUrl: string) {
    const dialog = this.matDialogController.create({
      template: {
        title: ``,
        content: `
              <img style="width: 100%" alt="" src="${imageUrl}">
            `,
      },
      displayFooter: false,
      afterClosedCb: () => {
      },
    });
    dialog.open();
  }
}
