import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShopTicketCommentsComponent} from "./shop-ticket-comments.component";
import {EmptyPageModule, MaterialModule} from "@etop/shared";
import {AuthenticateModule} from "@etop/core";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ShopTicketCommentsComponent
  ],
  exports: [
    ShopTicketCommentsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AuthenticateModule,
    FormsModule,
    EmptyPageModule
  ]
})
export class ShopTicketCommentsModule {
}
