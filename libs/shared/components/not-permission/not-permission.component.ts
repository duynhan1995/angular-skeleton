import {Component, OnInit} from '@angular/core';
import {EmptyType} from "../empty-page";

@Component({
  selector: 'etop-not-permission',
  templateUrl: './not-permission.component.html',
  styleUrls: ['./not-permission.component.scss']
})
export class NotPermissionComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  get type() {
    return EmptyType.block;
  }

}
