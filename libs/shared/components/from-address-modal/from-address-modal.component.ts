import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {AddressAPI} from "@etop/api";
import { Address } from '@etop/models/Address';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { AddressService } from 'apps/core/src/services/address.service';
import { BaseComponent } from '@etop/core';
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

const START_OPEN_TIME = '08:30';
const END_OPEN_TIME = '17:30';
const START_LUNCH_BREAK = '12:00';
const END_LUNCH_BREAK = '13:30';

@Component({
  selector: 'etop-from-address-modal',
  templateUrl: './from-address-modal.component.html',
  styleUrls: ['./from-address-modal.component.scss']
})
export class FromAddressModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() address: Address;
  @Input() action: string;
  @Input() title: string;
  @Input() type: string;

  loading = false;

  start_open_time: string;
  end_open_time: string;
  start_lunch_break: string;
  end_lunch_break: string;
  notLunchBreak = false;

  callOption = 'no-call';
  actionType;

  locationForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
    wardCode: ''
  });
  formInitializing = true;

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.locationForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.locationForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private addressService: AddressService,
    private modalDismiss: ModalAction,
    private utilService: UtilService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  ngOnInit() {
    if (this.address.district_code) {
      this.actionType = 'update'
    }
    else {
      this.actionType = 'save'
    }

    this.prepareTimeData();
    this.callOption = (this.address && this.address.notes && this.address.notes.other) || 'no-call';

    this.locationForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.locationForm.patchValue({
            districtCode: '',
            wardCode: '',
          });
        }
        this.address.province_code = value;
      });

    this.locationForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.locationForm.patchValue({
            wardCode: '',
          });
        }
        this.address.district_code = value;
      });

    this.locationForm.controls['wardCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.address.ward_code = value;
      });
  }

  ngAfterViewInit() {
    const {province_code, district_code, ward_code} = this.address;

    this.formInitializing = true;

    this.locationForm.patchValue({
      provinceCode: province_code,
      districtCode: district_code,
      wardCode: ward_code
    });

    this.formInitializing = false;
  }

  closeModal() {
    this.modalDismiss.close(false);
  }

  private prepareTimeData() {
    if (!this.address || !this.address.notes) {
      this.start_open_time = START_OPEN_TIME;
      this.end_open_time = END_OPEN_TIME;
      this.start_lunch_break = START_LUNCH_BREAK;
      this.end_lunch_break = END_LUNCH_BREAK;
    }

    if (this.address.notes.open_time) {
      const open_time = this.address.notes.open_time.split('-');
      this.start_open_time = (open_time && open_time.length == 2) ? open_time[0].trim() : START_OPEN_TIME;
      this.end_open_time = (open_time && open_time.length == 2) ? open_time[1].trim() : END_OPEN_TIME;
    } else {
      this.start_open_time = START_OPEN_TIME;
      this.end_open_time = END_OPEN_TIME;
    }

    if (this.address.notes.lunch_break) {
      const lunch_break = this.address.notes.lunch_break.split('-');
      this.start_lunch_break = (lunch_break && lunch_break.length == 2) ? lunch_break[0].trim() : START_LUNCH_BREAK;
      this.end_lunch_break = (lunch_break && lunch_break.length == 2) ? lunch_break[1].trim() : END_LUNCH_BREAK;
    } else {
      this.start_lunch_break = START_LUNCH_BREAK;
      this.end_lunch_break = END_LUNCH_BREAK;
      this.notLunchBreak = true;
    }
  }

  validate(data: AddressAPI.UpdateAddressRequest): boolean {
    if (!data.province_code) {
      toastr.error('Vui lòng chọn tỉnh thành!');
      return false;
    }

    if (!data.district_code) {
      toastr.error('Vui lòng chọn quận huyện!');
      return false;
    }

    if (!data.ward_code) {
      toastr.error('Vui lòng chọn phường xã!');
      return false;
    }

    if (!data.address1) {
      toastr.error('Vui lòng nhập địa chỉ cụ thể!');
      return false;
    }

    if (!data.full_name) {
      toastr.error('Vui lòng nhập tên người phụ trách!');
      return false;
    }

    if (!this.start_open_time || !this.end_open_time) {
      toastr.error('Vui lòng nhập khung giờ lấy hàng');
      return false;
    }

    if (!this.notLunchBreak && (!this.start_lunch_break || !this.end_lunch_break)) {
      toastr.error('Vui lòng nhập giờ nghỉ trưa');
      return false;
    }

    return true;
  }

  async save() {
    this.loading = true;
    try {
      this.address = this.utilService.trimFields(this.address, ['address1', 'full_name', 'phone']);

      const body: AddressAPI.UpdateAddressRequest = {
        ...this.address,
        type: this.type,
        notes: {
          other: this.callOption,
          open_time: this.start_open_time + ' - ' + this.end_open_time,
          lunch_break: this.notLunchBreak ? '' : this.start_lunch_break + ' - ' + this.end_lunch_break
        }
      };
      const valid_address = this.validate(body);
      if (!valid_address) {
        return this.loading = false;
      }

      let res;
      if (this.actionType == 'save') {
        res = await this.addressService.createAddress(body);
      } else {
        res = await this.addressService.updateAddress(body);
      }
      toastr.success(`${this.actionType == 'update' ? 'Cập nhật' : 'Tạo'} địa chỉ thành công!`);
      this.modalDismiss.dismiss({
        address: res
      });
    } catch (e) {
      debug.log('ERROR in Saving Address', e);
      toastr.error(
        `${this.actionType == 'update' ? 'Cập nhật' : 'Tạo'} địa chỉ không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }
}
