import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MaterialModule} from "@etop/shared";
import {SharedModule} from "apps/shared/src/shared.module";
import {FromAddressModalComponent} from "./from-address-modal.component";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
  ],
  declarations: [
    FromAddressModalComponent
  ]
})

export class FromAddressModalModule {}
