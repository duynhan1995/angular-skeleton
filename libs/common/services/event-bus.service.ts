import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventBus {

  private _obs: {[key: string]: {subscriber:any; observable: Observable<any>}} = {};

  private createObservable(name) {
    if(!this._obs[name]) {
      this._obs[name] = {
        observable: new Observable(subscriber => {
          this._obs[name].subscriber = subscriber;
        }),
        subscriber: null
      };
    }
  }

  message<T>(name: string): Subscriber<T> {
    this.createObservable(name);
    return this._obs[name].subscriber;
  }

  on<T>(name: string): Observable<T> {
    this.createObservable(name);
    return this._obs[name].observable;
  }

}
