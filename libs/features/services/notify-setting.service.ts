import { Injectable } from '@angular/core';
import { NotifySettingApi } from '@etop/api';

@Injectable({
  providedIn: 'root'
})
export class NotifySettingService {

  constructor(private notiApi: NotifySettingApi) { }

  getNotifySetting() {
    return this.notiApi.getNotifySetting();
  }

  enableNotifyTopic(topic) {
    return this.notiApi.enableNotifyTopic(topic);
  }

  disableNotifyTopic(topic) {
    return this.notiApi.disableNotifyTopic(topic);
  }
}