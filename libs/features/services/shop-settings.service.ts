import { Injectable } from '@angular/core';
import { ShopSettingsApi } from '@etop/api/shop/shop-settings.api';

@Injectable()
export class ShopSettingsService {

    constructor(private shopSettingsServiceApi: ShopSettingsApi) {}

    async getSetting() {
        return await this.shopSettingsServiceApi.getSetting();
    }

    async updateSetting(data) {
        return await this.shopSettingsServiceApi.updateSetting(data);
    }
}
