import { Injectable } from '@angular/core';
import {ConfirmOrderStore} from "@etop/features/fabo/confirm-order/confirm-order.store";
import {Customer} from "libs/models/Customer";
import { Order } from 'libs/models/Order';
import { Address } from 'libs/models/Address';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';

@Injectable()
export class ConfirmOrderController {
  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderSerivce: ConfirmOrderService
  ) {}

  setCustomerForOrder(customer: Customer) {
    this.confirmOrderStore.setCustomer(customer);
    this.confirmOrderSerivce.updateOrder({
      ...this.confirmOrderStore.snapshot.order,
      customer
    })
    this.confirmOrderStore.forceUpdateForm();
  }

  setOrder(order: Order) {
    this.confirmOrderStore.setOrder(order);
    this.confirmOrderStore.forceUpdateForm();
  }

  setToAddress(address: Address) {
    this.confirmOrderStore.setToAddresses([address]);
    this.confirmOrderStore.forceUpdateForm();
  }

  resetForm() {
    this.confirmOrderSerivce.resetConfirmOrderForm();
  }
}
