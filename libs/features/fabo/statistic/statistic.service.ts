import { Injectable } from '@angular/core';
import { StatisticApi } from '@etop/api';
// import { UtilService } from 'apps/core/src/services/util.service';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  constructor(
    // private util: UtilService,
    private statisticApi: StatisticApi,
  ) {}

  async getSummaryShop({ date_from, date_to }) {
    return  await this.statisticApi.summaryShop({ date_from, date_to });
    // tables = this.util.matchKeyArray()
  }
}