import { Injectable } from '@angular/core';
import { ConversationApi } from '@etop/api';

@Injectable()
export class PostService {
  constructor(
    private conversationApi:ConversationApi
  ) {
  }
  async createPost(external_page_id, content){
    const request = {
      external_page_id: external_page_id,
      message: content
    }
    return await this.conversationApi.createPost(request)
  }
}
