import { Injectable } from '@angular/core';
import {Customer, CustomerAddress} from 'libs/models/Customer';
import { CustomerFaboApi } from '@etop/api/fabo-api/customer-api.service';
import {CustomerService} from "../../services/customer.service";
import { Filters } from '@etop/models';

@Injectable()
export class FaboCustomerService {
  constructor(
    private customerService: CustomerService,
    private customerApi: CustomerFaboApi
  ) {
  }

  async createAndUpdate(customer: Customer, address?: CustomerAddress) {
    const { province_code, district_code, ward_code, address1 } = address;
    let _customer: any;
    let _address: any;
    if (customer?.id) {
      _customer = await this.customerService.updateCustomer(customer);
    } else {
      _customer = await this.customerService.createCustomer(customer);
    }
    address.customer_id = _customer.id;
    address.full_name = _customer.full_name;
    address.email = _customer.email;
    address.phone = _customer.phone;
    if (province_code && district_code && ward_code && address1) {
      if (address?.id) {
        _address = await this.customerService.updateCustomerAddress(address);
      } else {
        _address = await this.customerService.createCustomerAddress(address);
      }
    }
    return { _customer, _address };
  }

  listFbUsers(customer_id) {
    return this.customerApi.listFbUsers(customer_id);
  }

  listCustomersWithFbUsers(filters?: Filters, paging?) {
    paging = {
      offset: 0,
      limit: 1000,
      sort: ''
    };
    return this.customerApi.listCustomersWithFbUsers({filters, paging});
  }

}
