import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageLoaderService {
  _cache = {};
  constructor() {
  }

  getCache(src: string) {
    return this._cache[src];
  }

  setCache(src, data: any) {
    this._cache[src] = data;
    return data;
  }
}
