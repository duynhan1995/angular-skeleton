import { Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ImageLoaderService } from '@etop/features/image-loader/image-loader.service';

@Directive({
  selector: '[imageLoader]'
})
export class ImageLoaderDirective implements OnChanges, OnInit {
  @Input('imageLoader') imageLoader: string;
  src = null;

  _originSrc

  constructor(
    private imageLoaderService: ImageLoaderService,
    private el: ElementRef) {
  }

  ngOnInit() {
  }

  async loadImage() {
    const cache = this.imageLoaderService.getCache(this.src);
    if (cache) {
      this.el.nativeElement.src = cache;
      return;
    }
    const img = new Image();
    img.crossOrigin = 'anonymous';
    img.onload = () => {
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      // Set width and height
      canvas.width = img.width;
      canvas.height = img.height;
      // Draw the image
      ctx.drawImage(img, 0, 0);
      const base64 =  canvas.toDataURL('image/jpeg');
      this.imageLoaderService.setCache(this.src, base64);
      setTimeout(() => this.el.nativeElement.src = base64, 0);
    }
    img.onerror = () => (this.el.nativeElement.src = this._originSrc);
    img.src = this.src;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { currentValue, previousValue, firstChange } = changes.imageLoader;
    if (firstChange) {
      this._originSrc = this.el.nativeElement.src;
    }
    if (currentValue == previousValue) {
      return;
    }
    if (!currentValue) {
      this.el.nativeElement.src = this._originSrc;
      return;
    }
    this.src = currentValue;
    this.loadImage();
  }
}
