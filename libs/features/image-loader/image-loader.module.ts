import { NgModule } from '@angular/core';
import { ImageLoaderDirective } from '@etop/features/image-loader/image-loader.directive';

@NgModule({
  declarations: [ImageLoaderDirective],
  exports: [ImageLoaderDirective]
})
export class ImageLoaderModule {}
