import { Directive, ElementRef, HostListener } from '@angular/core';
import { AInfiniteScroll } from '@etop/features/infinite-scroll/infinite-scroll.class';

@Directive({
  selector: '[infiniteScroll]'
})
export class InfiniteScrollDirective extends AInfiniteScroll {

  constructor(private host: ElementRef) {
    super();
  }

  @HostListener('scroll', ['$event'])
  onScroll(event) {
    super.onScroll(event);
  }

}
