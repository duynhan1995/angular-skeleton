import { Directive, EventEmitter, Input, Output } from '@angular/core';

export interface InfiniteScrollEvent {
  target: any;
  threshold: number;
  scrollTop: number;
  scrollHeight: number;
  elementHeight: number;
  untilReachTop: number;
  untilReachBottom: number;
  detail?: any;
}


@Directive()
export class AInfiniteScroll {
  @Output() bottom = new EventEmitter<InfiniteScrollEvent>();
  @Output() top = new EventEmitter<InfiniteScrollEvent>();

  @Input() threshold = 100;

  protected invoked = false;


  onScroll(event) {
    const element = event.target;

    const bound: InfiniteScrollEvent = {
      target: this,
      threshold: this.threshold,
      scrollTop: element.scrollTop,
      scrollHeight: element.scrollHeight,
      elementHeight: element.clientHeight,
      untilReachTop: element.scrollTop,
      untilReachBottom: element.scrollHeight - (element.clientHeight + element.scrollTop)
    }

    if(bound.untilReachTop - this.threshold <= 0 && !this.invoked) {
      this.invoked = true;
      this.top.emit(bound);
    }

    if (bound.untilReachBottom <= this.threshold && !this.invoked) {
      this.invoked = true;
      this.bottom.next(bound);
    }

    if (bound.untilReachTop > this.threshold && bound.untilReachBottom > this.threshold) {
      this.invoked = false;
    }

  }

}
