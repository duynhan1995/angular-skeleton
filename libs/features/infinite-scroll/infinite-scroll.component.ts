import { ChangeDetectorRef, Component, ElementRef, HostBinding, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { AInfiniteScroll } from '@etop/features/infinite-scroll/infinite-scroll.class';

@Component({
  selector: 'infinite-scroll',
  template: `
    <div class="spinner-border spinner-border-sm text-primary m-auto text-center" role="status">
    </div>
  `,
  styles: [
    `
      :host {
        display: none;
      }
      :host.active {
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin: 10px 0;
      }
    `
  ]
})
export class InfiniteScrollComponent extends AInfiniteScroll implements OnInit {

  parentEl: HTMLElement;

  constructor(private selfEl: ElementRef, private ref: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    const parentEl = this.selfEl.nativeElement.parentElement;
    this.parentEl = parentEl;
    fromEvent(parentEl, 'scroll').subscribe((event) => {
      super.onScroll(event);
    });

    this.bottom.subscribe(() => {
      if (this.bottom.observers.length > 1) {
        this.active();
      }
    });
    this.top.subscribe(() => {
      if (this.top.observers.length > 1) {
        this.active();
      }
    });
  }

  public active() {
    this.selfEl.nativeElement.classList = 'active';
  }

  public complete() {
    this.selfEl.nativeElement.classList = '';
  }

}
