
const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

function getConfig(env) {
  const configs = {
    'production': {
      name: 'v1prod',
      mode: 'production',
      sourceMap: false,
      itopxAuthUrl: '//pos.itopx.vn/welcome',
      redirectAfterLogout: '//id.imgroup.vn',
      itopxRedirectUrl: '//id.imgroup.vn?p=itopx',
      itopxInvitationURL: 'https://itopx.vn'
    },
    'development': {
      name: 'dev',
      mode: 'development',
      sourceMap: 'inline-source-map',
      itopxAuthUrl: '//itopx.d.etop.vn/welcome',
      redirectAfterLogout: 'http://id.webitop.com/itopx/login',
      itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
      itopxInvitationURL: 'http://id.webitop.com/itopx/register'
    },
    'sandbox': {
      name: 'sandbox',
      mode: 'production',
      sourceMap: 'inline-source-map',
      itopxAuthUrl: '//itopx.sandbox.etop.vn/welcome',
      redirectAfterLogout: 'http://id.webitop.com/itopx/login',
      itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
      itopxInvitationURL: 'http://id.webitop.com/itopx/register'
    },
    'stage': {
      name: 'stage',
      mode: 'development',
      sourceMap: 'inline-source-map',
      itopxAuthUrl: '//itopx.g.etop.vn/welcome',
      redirectAfterLogout: 'http://id.webitop.com/itopx/login',
      itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
      itopxInvitationURL: 'http://id.webitop.com/itopx/register'
    }
  };
  const config = {
    name: 'dev',
    mode: 'development',
    sourceMap: 'inline-source-map',
    itopxAuthUrl: '//itopx.d.etop.vn/welcome',
    redirectAfterLogout: 'http://id.webitop.com/itopx/login',
    itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
    itopxInvitationURL: 'http://id.webitop.com/itopx/register'
  };
  return {
    ...config,
    ...configs[env]
  };
}

module.exports = function(env, argv) {
  const config = getConfig(argv.configuration);
  return {
    mode: config.mode,
    devtool: config.sourceMap,
    entry: {
      'etop.vn/assets/cfg': './src/etop.vn/cfg.ts',
      'itopx.vn/assets/cfg': './src/itopx.vn/cfg.ts'
    },
    output: {
      path: path.join(__dirname, '../../dist', config.name, 'whitelabel'),
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js', '.json']
    },
    plugins: [
      new CopyPlugin([
        { from: '**/images/*', context: 'src' },
        { from: '**/favicon.*', context: 'src' }
      ]),
      new webpack.EnvironmentPlugin({
        ...config
      })
    ],
    module: {
      rules: [
        // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
        { test: /\.tsx?$/, use: ['ts-loader'], exclude: /node_modules/ }
      ]
    }
  };
};

