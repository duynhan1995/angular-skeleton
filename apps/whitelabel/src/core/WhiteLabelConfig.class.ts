export class AppConfig {
  appId: string;
  appName: string;
  disableUser?: boolean;
  redirectUserLink?: string;
  redirectAfterLogout?: string;
}

export class ThemeConfig {
  [key: string]: string;
}

export class HTMLConfig {
  html_topship_balance_warning: string;
  html_topship_balance_note: string;
  html_main_slogan: string;
}

export class TextConfig {
  text_trading_promotion: string;
}

export class CMSBannerConfig {
  cms_announcement_content: string;
  cms_transaction_note: string;
  cms_shipment_policy: string;
}

export class WhiteLabelConfig {
  app: AppConfig;
  theme: ThemeConfig;
  htmls: HTMLConfig;
  texts: TextConfig;
  cms_banners: CMSBannerConfig;
  links?: {
    trading_guide?: string;
    invitation?: string;
  };
}
