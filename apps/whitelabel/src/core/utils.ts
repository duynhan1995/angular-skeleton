function decodeBase64(str) {
  return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

export function fireEvent(name, data) {
  document.dispatchEvent(new CustomEvent(name, data));
}

export function checkTokenAndRedirect(redirectURL?: string) {
  let storeData = localStorage.getItem('buG7rWEgY29uIGfDoA==');

  if (!storeData) {
    return redirect(redirectURL);
  }

  const data = process.env.mode == 'production'
    ? JSON.parse(decodeBase64(storeData))
    : JSON.parse(storeData);

  if (!data || !data['auth-data'] || !data['auth-data']['token']) {
    return redirect(redirectURL);
  }
}

export function redirect(url) {
  location.href = url;
}
