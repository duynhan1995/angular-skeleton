import { WhiteLabelConfig } from 'src/core/WhiteLabelConfig.class';
import { checkTokenAndRedirect, fireEvent } from '../core/utils';

declare const window: any;

let cfg: WhiteLabelConfig = {
  app: {
    appId: 'itopx.vn',
    appName: 'iTopX',
    disableUser: true,
    redirectUserLink: process.env.itopxAuthUrl,
    redirectAfterLogout: process.env.redirectAfterLogout,
  },
  theme: {
    'primary-color': '#410258',
    'primary-dark-color': '#0C85A1',
    'primary-light-color': '#CCF5FE',
    'secondary-color': '#33B6D4',
    'default-color': '#999999',
    'light-color': '#888888',
    'dark-color': '#444444',
    'default-boder-color': '#E5E5E5',
    'light-boder-color': '#F5F5F5',
    'dark-boder-color': '#C5C5C5',
    'default-bg-color': '#F5F5F5',
    'dark-bg-color': '#F0F0F0',
    'light-bg-color': '#FEFEFE',
    'danger-color': '#E74C3C',
    'danger-dark-color': '#C92A1A',
    'danger-light-color': '#FDBDB6',
    'warning-color': '#F7941E',
    'warning-dark-color': '#E78005',
    'warning-light-color': '#FFD39D',
    'success-color': '#2ECC71',
    'success-dark-color': '#11AF54',
    'success-light-color': '#ADFFD0',
    color_init: '',
    color_init_dark: '',
    color_init_light: ''
  },
  htmls: {
    html_topship_balance_warning: `
    <span>
      Vui lòng liên hệ TOPSHIP thông qua
      <a href="https://www.facebook.com/topship.asia/" target="_blank" class="text-primary font-weight-bold"
        style="text-decoration: underline;">
        Facebook
      </a>
      hoặc
      <a href="https://topship.vn/" target="_blank" class="text-primary font-weight-bold"
        style="text-decoration: underline;">
        Website
      </a>
      để thanh toán.
    </span>
    `,
    html_topship_balance_note: `
    <span>
      Vui lòng liên hệ TOPSHIP thông qua
      <a href="https://www.facebook.com/topship.asia/" target="_blank" class="text-primary font-weight-bold"
        style="text-decoration: underline;">
        Facebook
      </a>
      hoặc
      <a href="https://topship.vn/" target="_blank" class="text-primary font-weight-bold"
        style="text-decoration: underline;">
        Website
      </a>
      để giải đáp thắc mắc.
    </span>
    `,
    html_main_slogan: `
    <div class="text-bold pb-2" style="font-size: 2.4rem">Phần mềm quản lý bán hàng</div>
    <h4>Miễn phí vĩnh viễn, không giới hạn tính năng!</h4>
    `
  },
  texts: {
    text_trading_promotion: "Bạn được hoàn lại"
  },
  cms_banners: {
    cms_announcement_content: null,
    cms_transaction_note: "<p><strong><img src='https://www.etop.vn/uploads/1547287636_danger-sign.png'  width='15' height='14' />&nbsp;<span style='font-size: 14pt;''>THÔNG BÁO</span></strong></p><div><strong><em>Thời gian đối soát và Phí chuyển tiền</em></strong></div><div>&nbsp;</div><div>Phí chuyển khoản cùng ngân hàng:<strong>3.300đ</strong>.<br />Phí chuyển khoản khác ngân hàng:<strong> 5.500đ</strong>.<br /><em>* Đã bao gồm VAT phí chuyển khoản.</em></div><div><strong><em>Khuyến khích dùng cùng ngân hàng Vietcombank để nhanh hơn và tiết kiệm phí chuyển khoản cho Shop</em></strong></div><div>(Khác ngân hàng, chiều đối soát sẽ nhận được vào sáng hôm sau.)</div><div>&nbsp;</div><div>Tham khảo thêm về biểu phí dịch vụ của Vietcombank</div><div><a href='https://www.vietcombank.com.vn/Corp/Documents/Ngan%20hang%20dien%20tu3.pdf' target='_blank' rel='noopener'>https://www.vietcombank.com.vn/Corp/Documents/Ngan%20hang%20dien%20tu3.pdf</a></div>",
    cms_shipment_policy: "https://topship.vn/chinh-sach-top-ship"
  },
  links: {
    invitation: process.env.itopxInvitationURL
  }
};

window.cfg = cfg;
if (!location.href.includes(cfg.app.redirectUserLink) && cfg.app.disableUser && !location.href.includes(`invitation`)) {
  checkTokenAndRedirect(process.env.itopxRedirectUrl || cfg.app.redirectUserLink);
}

fireEvent('onConfigLoaded', cfg);
