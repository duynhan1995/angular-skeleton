 import { BaseModel } from '@etop/core/base/base-model';

export class Stat {
  name: string;
  data: Array<any>;
  className?: string;
  renderFormat?: (number) => any;
  chartOpt?: any;
}

export class StatLabel extends BaseModel {
  label: string;
  label2: string;
  spec: string;
  cells?: Array<StatValue>;
  className: string;
  render?: (any) => any;
}

export class StatValue extends BaseModel {
  spec: string;
  label: string;
  value: number;
  className: string;
  render?: (any) => any;
}

export class StatTable extends BaseModel {
  name: string;
  label: string;
  subLabels: Array<string>;
  columns: Array<StatLabel>;
  rows: Array<StatLabel>;
}
