import { Injectable } from '@angular/core';
import {Fulfillment} from 'libs/models/Fulfillment';
import {FulfillmentApi} from '@etop/api';
import {Filters} from "@etop/models";

@Injectable()
export class FulfillmentService {

  constructor(
    private fulfillmentApi: FulfillmentApi
  ) {
  }

  trackingLink(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return null;
    }
    return fulfillment.shipping_shared_link;
  }

  async getShipnowFulfillments(start?: number, perpage?: number, filters?: Filters) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };

    return this.fulfillmentApi.getShipnowFulfillments({paging, filters})
      .then(res => {
        res.shipnow_fulfillments = res.shipnow_fulfillments.map(f => Fulfillment.fulfillmentMap(f, []));
        return res;
      });
  }

}
