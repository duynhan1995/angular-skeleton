import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {TicketListComponent} from "apps/movecrop/src/app/pages/tickets/ticket-list/ticket-list.component";
import {TicketsComponent} from "apps/movecrop/src/app/pages/tickets/tickets.component";
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from "@etop/shared";
import { TicketRowComponent } from './components/ticket-row/ticket-row.component';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';
import {AuthenticateModule} from "@etop/core";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { TicketService } from 'apps/movecrop/src/services/ticket.service';

const routes: Routes = [
  {
    path: '',
    component: TicketsComponent
  }
];

@NgModule({
  declarations: [
    TicketsComponent,
    TicketListComponent,
    TicketRowComponent,
    TicketDetailComponent,
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MaterialModule,
        EtopMaterialModule,
        EtopFilterModule,
        EtopCommonModule,
        EtopPipesModule,
        SideSliderModule,
        AuthenticateModule,
        NotPermissionModule,
        NgbModule
    ],
    providers:[TicketService]
})
export class TicketsModule { }
