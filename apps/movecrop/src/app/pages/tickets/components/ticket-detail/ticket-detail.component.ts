import {Component, Input, OnInit} from '@angular/core';
import {AdminUser, Ticket} from "@etop/models";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";

@Component({
  selector: 'movecrop-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent extends BaseComponent implements OnInit {

  loading = false;
  @Input() ticket: Ticket;

  constructor(
    private auth: AuthenticateStore,
    private dialogController: DialogControllerService,
  ) {
    super();
  }

  ngOnInit(){
  };
}
