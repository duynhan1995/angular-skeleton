import {Component, Input, OnInit} from '@angular/core';
import {Ticket} from "@etop/models";

@Component({
  selector: '[movecrop-ticket-row]',
  templateUrl: './ticket-row.component.html',
  styleUrls: ['./ticket-row.component.scss']
})
export class TicketRowComponent implements OnInit {
  @Input() ticket: Ticket;
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
