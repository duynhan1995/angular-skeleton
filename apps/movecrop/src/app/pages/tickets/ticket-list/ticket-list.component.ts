import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {EtopTableComponent, SideSliderComponent} from "@etop/shared";
import {AuthenticateStore} from "@etop/core";
import { FilterOperator, FilterOptions, Filters, Relationship, Shop, Ticket } from '@etop/models';
import {takeUntil} from "rxjs/operators";
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { ContactQuery, ContactService } from '@etop/state/contact';


@Component({
  selector: 'movecrop-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('ticketTable', { static: true }) ticketTable: EtopTableComponent;
  @ViewChild('ticketSlider', { static: true }) ticketSlider: SideSliderComponent;

  ticketsList: Ticket[] = [];
  selectedTicket: Ticket;

  detailLabelId: string = null;

  emptyTitle = 'Chưa có ticket nào';

  shopId: string;
  shops: Shop[] = [];
  shopDisplayMap = (shop: Shop) => shop && `${shop.code} - ${shop.name}` || null;
  shopValueMap = (shop: Shop) => shop && shop.id || null;

  relationships: Relationship[] = [];

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private relationshipsService: RelationshipService,
    private relationshipsQuery: RelationshipQuery,
    private contactService: ContactService,
    private contactsQuery: ContactQuery,
  ) {
    super();
  }

  get showPaging() {
    return !this.ticketTable.liteMode && !this.ticketTable.loading;
  }

  get sliderTitle() {
    return 'Chi tiết ticket';
  }

  ngOnInit() {
  }

  detail(ticket){
    this.selectedTicket = ticket;
    this.ticketTable.toggleLiteMode(!!this.selectedTicket);
    this.ticketSlider.toggleLiteMode(!!this.selectedTicket);
  }

  onSliderClosed(){
    this.selectedTicket = null;
    this.ticketTable.toggleLiteMode();
    this.ticketSlider.toggleLiteMode(false);
  }

  async loadPage({page, perpage}) {
    this.ticketTable.loading = true;
    // this.ticketsList = await this.ticketService.getTickets((page - 1) * perpage, perpage);
    await this.mapRelationShips();
    this.ticketTable.loading = false;
  }

  async mapRelationShips() {
    this.relationships = this.relationshipsQuery.getAll();
    if (!this.relationships?.length) {
      await this.relationshipsService.getRelationships();
    }
    let ref_ids = this.ticketsList.filter(ticket => ticket.ref_type == "contact")?.map(ticket => ticket.ref_id);
    await this.contactService.getContacts({
      filter: {
        ids: ref_ids
      }
    })
    this.ticketsList = this.ticketsList.map(ticket => {
      return {
        ...ticket,
        update_by_name: this.relationshipsQuery.getRelationshipNameById(ticket.created_by),
        confirmed_by_name: this.relationshipsQuery.getRelationshipNameById(ticket.created_by),
        closed_by_name: this.relationshipsQuery.getRelationshipNameById(ticket.created_by),
        contact: this.contactsQuery.findContact(ticket.ref_id),
        from: {
          ...ticket.from,
          name: this.relationshipsQuery.getRelationshipNameById(ticket.created_by)
        }
      }
    })
  }
}
