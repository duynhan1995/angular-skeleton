import {Component, OnInit} from '@angular/core';
import {AuthenticateStore} from "@etop/core";
import { TicketService } from 'apps/movecrop/src/services/ticket.service';


@Component({
  selector: 'movecrop-tickets',
  template: `<movecrop-ticket-list></movecrop-ticket-list>`
})
export class TicketsComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private ticketService: TicketService
  ) {}

  ngOnInit() {
  }

}
