import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CallLogQuery, CallLogService, CallLogStore } from '@etop/state/shop/call-log';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from '../../../../../core/src/components/modal-controller/modal-controller.service';
import { HotlineService } from '@etop/state/shop/hotline';
import { ContactService } from '@etop/state/shop/contact';
import { RelationshipService } from '@etop/state/relationship';
import { ExtensionService } from '@etop/state/shop/extension';
@Component({
  selector: 'etelecom-call-log',
  template: `
    <etelecom-desktop-call-log></etelecom-desktop-call-log>`,
  styleUrls: ['./call-log.component.scss']
})
export class CallLogComponent implements OnInit {
  constructor(
    private callLogService: CallLogService,
    private ref: ChangeDetectorRef,
    private authorizationApi: AuthorizationApi,
    private modalController: ModalController,
    private hotlineService: HotlineService,
    private contactService: ContactService,
    private relationshipService: RelationshipService,
    private extensionService: ExtensionService,
    private callLogQuery: CallLogQuery,
    private callLogStore: CallLogStore
  ) { }

  async ngOnInit() {
    this.callLogStore.setLoading(true);
    await this._prepareData();
    this.callLogStore.setLoading(false);
  }


  async _prepareData() {
    await this.relationshipService.getRelationships();
    await this.hotlineService.getHotlines();
    await this.contactService.getContacts();
    await this.extensionService.getExtensions();
    this.mappingCallLogs();
  }

  mappingCallLogs() {
    let callLogs = this.callLogQuery.getAll();
    callLogs = this.callLogService.callLogsMap(callLogs);
    this.callLogService.setCallLogs(callLogs);

  }
}
