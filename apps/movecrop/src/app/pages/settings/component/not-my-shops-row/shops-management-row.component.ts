import { Component, Input, OnInit } from '@angular/core';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { AuthorizationApi } from '@etop/api';
import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';

@Component({
  selector: '[movecrop-shops-management-row]',
  templateUrl: './shops-management-row.component.html',
  styleUrls: ['./shops-management-row.component.scss']
})
export class ShopsManagementRowComponent implements OnInit {
  @Input() account: Account;
  @Input() currentShop: ExtendedAccount;

  dropdownActions = [];

  constructor(
    private gaService: GoogleAnalyticsService,
    private settingStore: SettingStore
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa',
        cssClass: this.account.id != this.currentShop.id && 'cursor-not-allowed',
        disabled: this.account.id != this.currentShop.id,
        onClick: () => this.edit()
      }
    ];
  }

  rolesDisplay(roles: string[]) {
    if (roles && roles.length) {
      let rolesMap =  roles.map(r => AuthorizationApi.roleMap(r)).join(", ");
      if(rolesMap == "Chủ shop")
        rolesMap = "Quản lí bưu cục";
      return rolesMap;
    }
    return 'Quản lí bưu cục';
  }

  switchAccount(index) {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_SHOP_LIST_SETTING,
      USER_BEHAVIOR.LABEL_ACCESS_SHOP
    );
    window.open(`/s/${index}/settings/shop`, '_blank');
  }

  edit() {
    this.settingStore.changeMenu(SettingMenu.shop);
  }

}
