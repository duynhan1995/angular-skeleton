import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from 'apps/core/src/services/user.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ShopAccountAPI } from '@etop/api/shop/shop-account.api';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';

@Component({
  selector: 'movecrop-onboarding-modal',
  templateUrl: './onboarding-modal.component.html',
  styleUrls: ['./onboarding-modal.component.scss']
})
export class OnboardingModalComponent extends BaseComponent implements OnInit {
  loading = false;
  shippingPolicyAccepted = false;
  shopName = '';

  constructor(
    private auth: AuthenticateStore,
    private modalAction: ModalAction,
    private userService: UserService,
    private commonUsecase: CommonUsecase,
  ) {
    super();
  }

  get shop() {
    return this.auth.snapshot.shop;
  }

  ngOnInit() {}

  async createShop() {
    this.loading = true;
    try{
      if(!this.shopName){
        toastr.error('Vui lòng điền tên bưu cục');
        this.loading = false;
        return;
      }
      const registerShopRequest: ShopAccountAPI.RegisterShopRequest = {
        name: this.shopName,
        phone: this.auth.snapshot.user.phone
      }
      await this.userService.registerShop(registerShopRequest);
      await this.commonUsecase.updateSessionInfo(true);
      toastr.success('Tạo cửa hàng thành công.');
      this.dismissModal();
    }catch(e){
      debug.error('ERROR in Creating Shop', e);
      toastr.error('Tạo cửa hàng không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  closeModal() {
    this.modalAction.close(false);
  }

}
