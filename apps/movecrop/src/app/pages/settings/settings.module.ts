import { ShopsManagementRowComponent } from './component/not-my-shops-row/shops-management-row.component';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from 'apps/shared/src/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {SettingsComponent} from 'apps/movecrop/src/app/pages/settings/settings.component';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {AuthenticateModule} from '@etop/core';
import {UserInfoComponent} from 'apps/movecrop/src/app/pages/settings/user-info/user-info.component';
import {AccountsInfoComponent} from 'apps/movecrop/src/app/pages/settings/accounts-info/accounts-info.component';
import {ShopInfoComponent} from 'apps/movecrop/src/app/pages/settings/shop-info/shop-info.component';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MovecropCommonUsecase} from 'apps/movecrop/src/app/usecases/movecrop-common.usecase.service';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {ChangePasswordModalComponent} from 'apps/movecrop/src/app/pages/settings/component/change-password-modal/change-password-modal.component';
import { MyShopsManagementComponent } from './component/my-shops-management/my-shops-management.component';
import { NotMyShopsManagamentComponent } from './component/not-my-shops-managament/not-my-shops-managament.component';
import { AccountInvitationRowComponent } from './component/account-invitation-row/account-invitation-row.component';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { OnboardingModalComponent } from './component/onboarding-modal/onboarding-modal.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'user',
        component: UserInfoComponent
      },
      {
        path: 'accounts',
        component: AccountsInfoComponent
      },
      {
        path: 'shop',
        component: ShopInfoComponent
      },
      {
        path: '**', redirectTo: 'user'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ModalControllerModule,
    AuthenticateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    NgbModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
  ],
  exports: [],
  declarations: [
    SettingsComponent,
    UserInfoComponent,
    ChangePasswordModalComponent,
    AccountsInfoComponent,
    ShopInfoComponent,
    MyShopsManagementComponent,
    NotMyShopsManagamentComponent,
    AccountInvitationRowComponent,
    ShopsManagementRowComponent,
    OnboardingModalComponent
  ],
  providers: [
    MovecropCommonUsecase,
    GoogleAnalyticsService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {
}
