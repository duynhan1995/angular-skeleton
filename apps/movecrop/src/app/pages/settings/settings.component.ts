import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {SettingMenu, SettingStore} from 'apps/core/src/stores/setting.store';
import {BankAccount, CompanyInfo, ExtendedAccount, User} from 'libs/models';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'movecrop-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SettingsComponent extends BaseComponent implements OnInit {
  user: User;

  current_menu = SettingMenu.user;
  menus = [
    {
      title: 'Thông tin bưu cục',
      slug: 'shop',
    },
  ];

  accountMenus = [
    {
      title: 'Thông tin tài khoản',
      slug: 'user'
    },
    {
      title: 'Danh sách bưu cục',
      slug: 'accounts'
    }
  ];

  currentShop = new ExtendedAccount({
    bank_account: {
      account_name: '',
      account_number: ''
    },
    address: {
      address1: '',
      province_code: '',
      district_code: '',
      ward_code: ''
    },
    company_info: {
      name: '',
      tax_code: '',
      address: '',
      website: '',
      legal_representative: {
        name: '',
        position: '',
        phone: '',
        email: ''
      }
    }
  });

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private settingStore: SettingStore,
  ) {
    super();
  }

  async ngOnInit() {
    this.auth.authenticatedData$.subscribe(info => {
      this.loadAccount(info);
    });

    this.settingStore.menuChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe((menu: SettingMenu) => {
        if (menu != this.current_menu) {
          this.current_menu = menu;
          this.changeMenu(menu);
        }
      });
  }

  async changeMenu(menu) {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    await this.router.navigateByUrl(`/s/${slug}/settings/${menu}`);
  }

  loadAccount(data) {
    this.currentShop = data.shop || new ExtendedAccount({});
    this.user = data.user;
    // this.accounts = this.auth.snapshot.accounts;

    this.checkNullShop();
  }
  checkNullShop() {
    if (!this.currentShop.bank_account) {
      this.currentShop.bank_account = new BankAccount();
    }
    if (!this.currentShop.company_info) {
      this.currentShop.company_info = new CompanyInfo({
        legal_representative: {
          name: '',
          position: '',
          phone: '',
          email: ''
        }
      });
    }
  }

}
