import {Component, OnInit} from '@angular/core';
import {AuthenticateStore} from '@etop/core';
import {User} from "@etop/models";
import {AppService} from '@etop/web/core/app.service';
import {ActivatedRoute} from '@angular/router';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ChangePasswordModalComponent } from '../component/change-password-modal/change-password-modal.component';

@Component({
  selector: 'movecrop-account-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private appService: AppService,
    private modalController: ModalController,
  ) {
  }

  get user(): User {
    return this.auth.snapshot.user;
  }

  get appName() {
    return this.appService.appID != 'etop.vn' ? 'Movecrop' : '';
  }
  
  onChangePassword(){
    this.openChangePasswordModal();
  }

  openChangePasswordModal() {
    this.modalController
      .create({
        component: ChangePasswordModalComponent
      })
      .show().then();
  }

  ngOnInit() {}

}
