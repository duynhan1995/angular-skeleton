import { Component, Input, OnInit } from '@angular/core';
import { Relationship } from 'libs/models/Authorization';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { AuthorizationApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-update-staff-modal',
  templateUrl: './update-staff-modal.component.html',
  styleUrls: ['./update-staff-modal.component.scss']
})
export class UpdateStaffModalComponent implements OnInit {
  @Input() staff = new Relationship();
  loading = false;

  constructor(
    private modalAction: ModalAction,
    private authorizationAPI: AuthorizationApi,
    private auth: AuthenticateStore
  ) { }

  get infoUpdatable() {
    return this.auth.snapshot.permission.permissions.includes('relationship/relationship:update');
  }

  get permissionUpdatable() {
    return this.auth.snapshot.permission.permissions.includes('relationship/permission:update');
  }

  ngOnInit() {}

  async updateStaff() {
    this.loading = true;
    try {
      const {full_name, user_id} = this.staff.p_data;
      if (!full_name) {
        this.loading = false;
        return toastr.error('Chưa nhập tên nhân viên!');
      }
      await this.authorizationAPI.updateRelationship(full_name, user_id);
      toastr.success('Cập nhật thông tin nhân viên thành công');
      this.modalAction.dismiss(true);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in creating Invitation', e);
    }
    this.loading = false;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }

}
