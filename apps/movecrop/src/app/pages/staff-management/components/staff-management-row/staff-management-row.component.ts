import { Component, Input, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { Relationship } from '@etop/models';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { CreateExtensionModalComponent } from '../create-hotline-modal/create-hotline-modal.component';
import { UpdateStaffModalComponent } from '../update-staff-modal/update-staff-modal.component';
import { HotlineQuery } from '@etop/state/shop/hotline';
import { ExtensionQuery, ExtensionStore } from '@etop/state/shop/extension';
import {ExtensionApi} from "@etop/api/shop/extension-api.service";
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: '[etelecom-staff-management-row]',
  templateUrl: './staff-management-row.component.html',
  styleUrls: ['./staff-management-row.component.scss']
})
export class StaffManagementRowComponent implements OnInit {
  @Input() staff = new Relationship();

  hotlines;
  extensions = [];
  hotlinesId;
  currentHotline;
  currentExtension;

  loading = true;

  removing = false;

  dropdownActions = [];

  private modal: any;

  constructor(
    private authorizationApi: AuthorizationApi,
    private authorizationService: AuthorizationService,
    private authStore: AuthenticateStore,
    private dialog: DialogControllerService,
    private modalController: ModalController,
    private extensionApi: ExtensionApi,
    private hotlineQuery: HotlineQuery,
    private extensionQuery: ExtensionQuery,
    private extensionStore: ExtensionStore,
  ) { }

  get isOwner() {
    return this.staff.roles.indexOf('owner') != -1;
  }

  async ngOnInit() {
    this.loading = true;
    this.dropdownActions = [
      {
        title: 'Cập nhật',
        cssClass: this.isCurrentUser && 'cursor-not-allowed',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:update','relationship/permission:update'],
        onClick: () => this.updateStaff()
      },
      {
        title: 'Xoá khỏi shop',
        cssClass: this.isCurrentUser && 'cursor-not-allowed' || 'text-danger',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:remove'],
        onClick: () => this.confirmRemoveStaff()
      }
    ];
    this.loading = false;
  }

  get isCurrentUser() {
    return this.staff.user_id == this.authStore.snapshot.user.id;
  }

  updateStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: UpdateStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        staff: {
          ...this.staff,
          p_data: {
            ...this.staff,
          }
        }
      }
    });
    this.modal.show();
    this.modal.onDismiss().then(updated => {
      this.modal = null;
      if (updated) {
        this.authorizationService.updateRelationship();
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async removeStaff() {
    this.removing = true;
    try {
      await this.authorizationApi.removeUserFromAccount(this.staff.user_id);
      this.authorizationService.removeRelationship();
      toastr.success('Xoá nhân viên thành công.')
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Staff', e);
    }
    this.removing = false;
  }

  confirmRemoveStaff() {
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá nhân viên`,
      body: `
        <div>Bạn thực sự muốn xóa nhân viên "<strong>${this.staff?.full_name}</strong>"?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmCss: 'btn-danger text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removeStaff();
        modal.close().then();
      }
    });
    modal.show().then();
  }

  async getHotlines(){
    return await this.extensionApi.getHotlines();
  }

  //Kiểm tra xem Nhân viên ấy có hotline, extension chưa.
  hasUserHotline(): boolean {
    this.loading = true;
    const extensions = this.extensionQuery.getAll();
    const hotlines = this.hotlineQuery.getAll();
    if(extensions && extensions.length){
      this.currentExtension = extensions.find(ex => ex.user_id == this.staff.user_id);
      this.currentHotline = hotlines.find(hotline => hotline.id == this.currentExtension?.hotline_id);
      this.loading = false;
      return this.currentExtension ;
    }
    this.loading = false;
    return false;
  }

  getExtensions(hotline_id){
    return this.extensionApi.getExtensions(hotline_id);
  }

  async createExtension(){
    const hotlines = this.hotlineQuery.getAll();
    this.modal = this.modalController.create({
      component: CreateExtensionModalComponent,
      componentProps: {
        hotlines: hotlines,
        staff: this.staff
      },
      showBackdrop: true,
      cssClass: 'modal-md'
    })
    this.modal.show();
    this.modal.onDismiss().then(extension => {
      this.extensionStore.add(extension);
      this.currentExtension = extension;
    })
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

}
