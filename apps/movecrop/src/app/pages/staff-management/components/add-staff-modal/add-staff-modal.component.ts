import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { AuthorizationApi } from '@etop/api';
import {ExtensionApi} from "@etop/api/shop/extension-api.service";

@Component({
  selector: 'movecrop-add-staff-modal',
  templateUrl: './add-staff-modal.component.html',
  styleUrls: ['./add-staff-modal.component.scss']
})
export class AddStaffModalComponent implements OnInit {
  newStaff = {
    full_name: '',
    hotline_id: '',
    password: '',
    phone: ''
  };

  loading = false;
  hotline_id;
  hotlines;
  hotlinesOption = [];

  constructor(
    private modalAction: ModalAction,
    private authorizationAPI: AuthorizationApi,
    private extensionApi: ExtensionApi
  ) { }

  async ngOnInit() {
    await this.getHotlinesOption();
  }

  async createInvitation() {
    this.loading = true;
    try {
      const { full_name, hotline_id, password, phone} = this.newStaff;
      if (!full_name) {
        this.loading = false;
        return toastr.error('Chưa nhập tên nhân viên!');
      }
      if (!hotline_id) {
        this.loading = false;
        return toastr.error('Chưa chọn số tổng đài!');
      }
      if (!password) {
        this.loading = false;
        return toastr.error('Chưa nhập mật khẩu!');
      }
      if (!phone) {
        this.loading = false;
        return toastr.error('Chưa nhập số điện thoại!');
      }
      const body  = {
        full_name, hotline_id, password, phone
      };
      const res = await this.authorizationAPI.createStaff(body);
      toastr.success('Thêm nhân viên thành công');
      this.modalAction.dismiss(res);
    } catch(e) {
      toastr.error(e.message, 'Thêm nhân viên không thành công. Vui lòng bấm F5 để load lại trang và thử lại!');
      debug.error('ERROR in creating Invitation', e);
    }
    this.loading = false;
  }

  async getHotlines(){
    return await this.extensionApi.getHotlines();
  }

  async getHotlinesOption(){
    this.hotlines = await this.getHotlines();
    this.hotlines.forEach(hotline => {
      if(hotline.connection_method == 'builtin'){
        let option = {
          name: 'Mặc định',
          value: hotline.id
        }
        this.hotlinesOption.push(option);
      }else {
        let option = {
          name: hotline.hotline,
          value: hotline.id
        }
        this.hotlinesOption.push(option);
      }
    })
  }

  showPassword() {
    let elementPass = <HTMLInputElement>document.querySelector('#mat-input-2');
    elementPass.type = 'text';
  }

  hidePassword() {
    let elementPass = <HTMLInputElement>document.querySelector('#mat-input-2');
    elementPass.type = 'password';
  }

  copyInputMessage() {
    const el = document.createElement('input');
    el.value = this.newStaff.password;
    document.body.append(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    toastr.success("Sao chép thành công")
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  closeModal() {
    this.modalAction.close(false);
  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }

}
