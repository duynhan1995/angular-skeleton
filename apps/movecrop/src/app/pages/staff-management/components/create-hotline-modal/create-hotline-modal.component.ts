import { Component, OnInit, Input } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import {ExtensionApi} from "@etop/api/shop/extension-api.service";

@Component({
  selector: 'etelecom-create-hotline-modal',
  templateUrl: './create-hotline-modal.component.html',
  styleUrls: ['./create-hotline-modal.component.scss']
})
export class CreateExtensionModalComponent implements OnInit {

  @Input() hotlines;
  @Input() staff;

  hotline_id;
  hotlinesOption = [];

  constructor(
    private modalAction: ModalAction,
    private auth: AuthenticateStore,
    private extensionApi: ExtensionApi
  ) { }

  closeModal() {
    this.modalAction.close(false);
  }

  async confirm(){
    let hotline_id;
    if(this.hotlines.length == 1){
      hotline_id = this.hotlines[0].id
    }else{
      hotline_id = this.hotline_id
    }
    try{
      const hotline = await this.extensionApi.createExtension(hotline_id, this.staff.user_id);
      toastr.success('Khởi tạo máy nhánh thành công');
      this.modalAction.dismiss(hotline);
    } catch(e){
      toastr.error(e.code && (e.message || e.msg), 'Khởi tạo máy nhánh không thành công.');
      this.modalAction.dismiss(hotline_id);
    }
  }

  getHotlinesOption(){
    this.hotlines.forEach(hotline => {
      if(hotline.connection_method == 'builtin'){
        let option = {
          name: 'Hotline mặc định',
          value: hotline.id
        }
        this.hotlinesOption.push(option);
      }else {
        let option = {
          name: hotline.hotline,
          value: hotline.id
        }
        this.hotlinesOption.push(option);
      }
    })
  }

  ngOnInit(): void {
    this.getHotlinesOption();
  }

}
