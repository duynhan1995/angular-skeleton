import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { EtopPipesModule, MaterialModule } from '@etop/shared';
import { StaffManagementComponent } from './staff-management.component'
import { AuthenticateModule } from '@etop/core';
import { DesktopStaffManagementComponent } from './desktop-staff-management/desktop-staff-management.component';
import { AddStaffModalComponent } from './components/add-staff-modal/add-staff-modal.component';
import { StaffManagementRowComponent } from './components/staff-management-row/staff-management-row.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { CreateExtensionModalComponent } from './components/create-hotline-modal/create-hotline-modal.component';
import { UpdateStaffModalComponent } from './components/update-staff-modal/update-staff-modal.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: '',
    component: StaffManagementComponent
  }
];

@NgModule({
  imports: [
    AuthenticateModule,
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    MaterialModule,
    NgbTooltipModule,
    DropdownActionsModule,
  ],
  exports: [],
  declarations: [
    UpdateStaffModalComponent,
    AddStaffModalComponent,
    StaffManagementComponent,
    StaffManagementRowComponent,
    DesktopStaffManagementComponent,
    CreateExtensionModalComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class StaffManagementModule {}
