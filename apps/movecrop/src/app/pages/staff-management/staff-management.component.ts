import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AuthorizationApi } from '@etop/api';
import { BaseComponent } from '@etop/core';
import { AppService } from '@etop/web/core/app.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { Invitation, Relationship } from 'libs/models/Authorization';
import { FilterOperator, Filters } from '@etop/models';
import { takeUntil } from 'rxjs/operators';
import { AddStaffModalComponent } from './components/add-staff-modal/add-staff-modal.component';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { RelationshipService } from '@etop/state/relationship';
import { HotlineService } from '@etop/state/shop/hotline';
import { ExtensionService } from '@etop/state/shop/extension';

@Component({
  selector: 'etelecom-staff-management',
  template: `
    <etelecom-desktop-staff-management class='hide-768'></etelecom-desktop-staff-management>
  `,
  styleUrls: ['./staff-management.component.scss']
})
export class StaffManagementComponent extends BaseComponent implements OnInit {
  shop_invitations: Invitation[] = [];
  relationships: Relationship[] = [];

  private modal: any;

  constructor(
    private invitationService: InvitationService,
    private relationshipService: RelationshipService,
    private hotlineService: HotlineService,
    private extensionService: ExtensionService,
    private cdr: ChangeDetectorRef,
  ) {
    super();
  }

  async ngOnInit() {
    await this.relationshipService.getRelationships();
    await this.hotlineService.getHotlines();
    await this.extensionService.getExtensions();
    await this.getShopInvitations();
    this.cdr.detectChanges();
  }

  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const filters : Filters = [
        {
          name: "status",
          op: FilterOperator.eq,
          value: "Z"
        },
        {
          name: "expires_at",
          op: FilterOperator.gt,
          value: dateString
        }
      ]
      this.invitationService.setFilters(filters);
      this.invitationService.getShopInvitations();
    } catch(e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }
}
