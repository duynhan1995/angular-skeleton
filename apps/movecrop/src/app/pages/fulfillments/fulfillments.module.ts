import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FulfillmentShipnowRoutesModule} from "apps/movecrop/src/app/components/fulfillment-shipnow-routes/fulfillment-shipnow-routes.module";
import {DetailInfoComponent} from "apps/movecrop/src/app/pages/fulfillments/components/single-ffm-detail/detail-info/detail-info.component";
import {SingleFfmDetailComponent} from "apps/movecrop/src/app/pages/fulfillments/components/single-ffm-detail/single-ffm-detail.component";
import {FulfillmentsComponent} from 'apps/movecrop/src/app/pages/fulfillments/fulfillments.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FulfillmentListComponent} from './fulfillment-list/fulfillment-list.component';
import {FulfillmentRowComponent} from './components/fulfillment-row/fulfillment-row.component';
import {FormsModule} from '@angular/forms';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from '@etop/core';
import {EtopCommonModule, EtopFilterModule, EtopPipesModule, MaterialModule, SideSliderModule} from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: FulfillmentsComponent,
  }
];

@NgModule({
  declarations: [
    FulfillmentsComponent,
    FulfillmentListComponent,
    FulfillmentRowComponent,
    SingleFfmDetailComponent,
    DetailInfoComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgbTooltipModule,
    AuthenticateModule,
    EtopPipesModule,
    SideSliderModule,
    EtopFilterModule,
    EtopCommonModule,
    MaterialModule,
    FulfillmentShipnowRoutesModule
  ]
})
export class FulfillmentsModule {
}
