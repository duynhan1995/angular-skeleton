import {
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {FulfillmentService} from "apps/movecrop/src/services/fulfillment.service";
import {Fulfillment} from 'libs/models/Fulfillment';
import {
  Filters
} from '@etop/models';
import {EtopTableComponent} from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import {UtilService} from 'apps/core/src/services/util.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SideSliderComponent} from 'libs/shared/components/side-slider/side-slider.component';
import {AuthenticateStore} from '@etop/core';
import {PageBaseComponent} from "@etop/web";
import {OrderApi} from "@etop/api";

@Component({
  selector: 'movecrop-fulfillment-list',
  templateUrl: './fulfillment-list.component.html',
  styleUrls: ['./fulfillment-list.component.scss']
})
export class FulfillmentListComponent extends PageBaseComponent implements OnInit {
  @ViewChild('fulfillmentTable', {static: true}) fulfillmentTable: EtopTableComponent;
  @ViewChild('slider', {static: true}) slider: SideSliderComponent;

  filters: Filters = [];

  tabs = [
    {name: 'Thông tin', value: 'detail_info'},
    {name: 'Lộ trình', value: 'shipnow_route'}
  ];
  activeTab: 'detail_info' | 'shipnow_route' = 'detail_info';

  fulfillmentList: Fulfillment[] = [];
  selectedFfms: Fulfillment[] = [];

  page: number;
  perpage: number;

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private route: ActivatedRoute,
    private util: UtilService,
    private fulfillmentService: FulfillmentService,
    private orderApi: OrderApi,
    private changeDetector: ChangeDetectorRef,
  ) {
    super();
  }

  get sliderTitle() {
    return 'Chi tiết đơn hàng';
  }

  get showPaging() {
    return !this.fulfillmentTable.liteMode && !this.fulfillmentTable.loading;
  }

  get emptyResultFilter() {
    return (
      this.page == 1 &&
      this.fulfillmentList.length == 0 &&
      this.filters.length > 0
    );
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy đơn hàng phù hợp';
    }
    return 'Cửa hàng của bạn chưa có đơn hàng';
  }

  ngOnInit() {}

  resetState() {
    this.slider.toggleLiteMode(false);
    this.fulfillmentTable.toggleLiteMode(false);
  }

  filter($event: Filters) {
    this.selectedFfms = [];
    this.filters = $event;
    this.fulfillmentTable.resetPagination();
  }

  async getShipnowFfms(page, perpage) {
    try {
      this.resetState();
      this.fulfillmentTable.toggleNextDisabled(false);

      let res = await this.fulfillmentService.getShipnowFulfillments(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
      if (page > 1 && res.shipnow_fulfillments.length == 0) {
        this.fulfillmentTable.toggleNextDisabled(true);
        this.fulfillmentTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả đơn hàng.');
        return;
      }

      this.fulfillmentList = res.shipnow_fulfillments.map(ffm => ({...ffm, p_data: {}}));
      await this.mapOrders(this.fulfillmentList)

      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list shipnow ffms', e);
    }
  }

  loadFulfillments(page, perpage) {
    return this.getShipnowFfms(page, perpage).then();
  }

  async loadPage({page, perpage}) {
    this.fulfillmentTable.loading = true;
    this.changeDetector.detectChanges();
    await this.loadFulfillments(page, perpage);
    this.fulfillmentTable.loading = false;
    this.changeDetector.detectChanges();
  }

  detail(event, ffm: Fulfillment) {
    this.changeTab('detail_info');
    this.fulfillmentList.forEach(f => {
      f.p_data.detailed = false;
    });
    ffm.p_data.detailed = true;
    this.selectedFfms = [ffm];
    this.slider.toggleLiteMode(true);
    this.fulfillmentTable.toggleLiteMode(true);
  }

  onSliderClosed() {
    this.fulfillmentList.forEach(f => {
      f.p_data.detailed = false;
    });
    this.selectedFfms = this.fulfillmentList.filter(f => f.p_data.detailed);
    this.slider.toggleLiteMode(false);
    this.fulfillmentTable.toggleLiteMode(false);
  }

  changeTab(tab_value) {
    this.activeTab = tab_value;
  }

  async mapOrders(ffms: Fulfillment[]) {
    let orderIds = []
    ffms.forEach(ffm => orderIds = orderIds.concat(ffm.order_ids))
    const orders = await this.orderApi.getOrdersByIDs(orderIds, true);
    ffms.forEach(ffm => ffm.orders  = orders?.filter(order => {
      if  (ffm.order_ids?.includes(order.id)) {
        return order
      }
    }))
  }

}
