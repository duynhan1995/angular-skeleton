import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FilterOperator, FilterOptions} from '@etop/models';
import {FulfillmentService} from 'apps/movecrop/src/services/fulfillment.service';
import {PageBaseComponent} from '@etop/web/core/base/page.base-component';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {Connection} from "libs/models/Connection";
import {LocationQuery} from "@etop/state/location";
import {FulfillmentListComponent} from "apps/movecrop/src/app/pages/fulfillments/fulfillment-list/fulfillment-list.component";
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {combineLatest} from "rxjs";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";

@Component({
  selector: 'movecrop-fulfillments',
  template: `
    <etop-filter
      class="no-print"
      [filters]="filters" (filterChanged)="fulfillmentList.filter($event)">
    </etop-filter>
    <movecrop-fulfillment-list #fulfillmentList></movecrop-fulfillment-list>`
})

export class FulfillmentsComponent extends PageBaseComponent implements OnInit {
  @ViewChild('fulfillmentList', {static: false}) fulfillmentList: FulfillmentListComponent;

  filters: FilterOptions;

  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  connectionReady$ = this.connectionStore.state$.pipe(
    map(s => s?.initConnections),
    distinctUntilChanged((a, b) => a?.length == b?.length)
  );

  constructor(
    private headerController: HeaderControllerService,
    private modal: ModalController,
    private ffmService: FulfillmentService,
    private locationQuery: LocationQuery,
    private connectionStore: ConnectionStore,
  ) {
    super();
  }

  private static specialConnectionNameDisplay(connection: Connection) {
    if (!connection.id) {
      return connection.name;
    }
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
    ${connection.connection_method == 'builtin' ? '<img class="topship-logo" alt="" src="assets/images/r-topship_256.png">' : ''}
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }

  ngOnInit() {
    this.initFilters();
    combineLatest([this.locationReady$, this.connectionReady$])
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.initFilters();
      });
  }

  initFilters() {
    this.filters = [
      {
        label: 'Mã đơn giao hàng',
        name: 'shipping_code',
        type: 'input',
        fixed: true,
        operator: FilterOperator.eq
      },
      {
        label: "Trạng thái toàn trình",
        name: "shipping_state",
        type: 'select',
        fixed: true,
        operator: FilterOperator.in,
        options: [
          {name: 'Tất cả', value: null},
          {name: "Đã tạo", value: 'created, default'},
          {name: 'Đang nhận giao', value: 'assigning'},
          {name: 'Đang lấy hàng', value: 'picking'},
          {name: 'Đang giao hàng', value: 'delivering'},
          {name: 'Hoàn thành', value: 'delivered'},
          {name: 'Đã hủy', value: 'cancelled'},
        ]
      }
    ];
  }

}
