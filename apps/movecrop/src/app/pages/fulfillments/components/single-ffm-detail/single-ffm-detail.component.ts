import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'movecrop-single-ffm-detail',
  templateUrl: './single-ffm-detail.component.html',
  styleUrls: ['./single-ffm-detail.component.scss']
})
export class SingleFfmDetailComponent extends BaseComponent implements OnInit {
  @Input() ffm = new Fulfillment({});
  @Input() activeTab: 'detail_info' | 'shipnow_route' = 'detail_info';

  @Output() viewRoutes = new EventEmitter();


  constructor() {
    super();
  }

  ngOnInit() {}

  onViewRoutes() {
    this.viewRoutes.emit();
  }

}
