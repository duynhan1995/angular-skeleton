import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges,} from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentService } from 'apps/movecrop/src/services/fulfillment.service';

@Component({
  selector: 'movecrop-detail-info',
  templateUrl: './detail-info.component.html',
  styleUrls: ['./detail-info.component.scss'],
})
export class DetailInfoComponent implements OnInit, OnChanges {
  @Input() ffm = new Fulfillment({});
  @Output() viewRoutes = new EventEmitter();

  constructor(
    private ffmService: FulfillmentService
  ) { }

  ngOnInit() {
    this.initBarcode();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initBarcode();
    if (changes.ffm?.currentValue?.delivery_points?.length == 1) {
      this.ffm.shipping_address = changes.ffm?.currentValue?.delivery_points[0].shipping_address;
    }
  }

  initBarcode() {
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.ffm.shipping_code),
      0
    );
  }

  trackingLink() {
    const link = this.ffmService.trackingLink(this.ffm);
    if (!link) { return; }
    window.open(link);
  }

  onViewRoutes() {
    this.viewRoutes.emit();
  }

}
