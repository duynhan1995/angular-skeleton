import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from '@etop/models';

@Component({
  selector: '[movecrop-fulfillment-row]',
  templateUrl: './fulfillment-row.component.html',
  styleUrls: ['./fulfillment-row.component.scss']
})
export class FulfillmentRowComponent implements OnInit {
  @Input() fulfillment = new Fulfillment({});
  @Input() liteMode = false;

  constructor() {}

  get showCancelReason() {
    return this.fulfillment.shipping_state == 'cancelled' && this.fulfillment.cancel_reason;
  }

  async ngOnInit() {
  }

}
