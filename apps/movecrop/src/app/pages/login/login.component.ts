import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {Invitation} from 'libs/models/Authorization';

@Component({
  selector: 'movecrop-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('passwordInput', {static: false}) passwordInput: ElementRef;

  login: string;
  password: string;
  phone: string;
  verify_code: string;
  email: string;
  fullname: string;

  loading = false;

  invitation_token = '';
  invitation = new Invitation({});

  loadingView = true;

  constructor(
    private commonUsecase: CommonUsecase,
  ) {
  }

  async ngOnInit() {
    this.commonUsecase.redirectIfAuthenticated().then(() => (this.loadingView = false));
  }

  async onEnterLoginInput() {
    if (!this.password) {
      if (this.passwordInput) {
        setTimeout(_ => {
          this.passwordInput.nativeElement.focus();
        });
      }
    } else {
      this.onLogin().then();
    }
  }

  showPassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'text';
      });
    }
  }

  hidePassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'password';
      });
    }
  }

  async onLogin() {
    if (!this.login || !this.password) {
      toastr.error('Vui lòng nhập đầy đủ thông tin đăng nhập để tiếp tục.');
      return;
    }

    this.loading = true;
    try {
      await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
    this.loading = false;
  }

}
