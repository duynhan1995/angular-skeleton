import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'etelecom-mobile-dashboard',
  templateUrl: './mobile-dashboard.component.html',
  styleUrls: ['./mobile-dashboard.component.scss']
})
export class MobileDashboardComponent implements OnInit {
  @Input() dashboard: any;
  @Input() loading = true;
  @Output() filter = new EventEmitter();
  selected;
  
  constructor() { }

  ngOnInit(): void {
    this.selected = {
      startDate: moment().startOf('month'),
      endDate: moment()
    };
    this.filter.emit(this.selected)
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
      this.filter.emit(event)
    }
  }

}
