import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RelationshipQuery } from '@etop/state/relationship/relationship.query';
import { RelationshipService } from '@etop/state/relationship/relationship.service';
import { DashboardService } from 'apps/e-telecom/src/services/dashboard.service';
import {ExtensionApi} from "@etop/api/shop/extension-api.service";

@Component({
  selector: 'etelecom-dashboard-pos',
  template: `
    <etelecom-desktop-dashboard class='hide-768' [dashboard]='dashboard' [loading]='loading' (filter)="getDashboard($event)">
    </etelecom-desktop-dashboard>
  `,
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  dashboard: any;
  loading = true;
  extensions: any;
  constructor(
    private dashboardService: DashboardService,
    private extensionApi: ExtensionApi,
    private relationshopService: RelationshipService,
    private relationshopQuery: RelationshipQuery,
    private changeDef: ChangeDetectorRef
  ) {}

  async ngOnInit() {
    this.loading = true;
    let relationships: any = this.relationshopQuery.getAll();
    if (!relationships?.length) {
      relationships = await this.relationshopService.getRelationships();
    }
    this.extensions = await this.getExtensions();
    this.loading = false;
  }

  async getExtensions() {
    try {
      let extensions = await this.extensionApi.getExtensions();
      extensions = extensions.map(extension => {
        return {
          ...extension,
          user_name: this.relationshopQuery.getRelationshipNameById(extension.user_id)
        }
      })
      return extensions
    } catch(e) {
      debug.log('ERROR in getExtensions', e)
    }
  }

  async getDashboard(ranges) {
    try {
      this.dashboard = await this.dashboardService.summaryEtelecom({
        date_from: ranges.startDate.format('yy-MM-DD'),
        date_to: ranges.endDate.add(1, 'days').format('yy-MM-DD')
      })
      this.dashboard.staff = this.dashboard.staff.map(d => {
        return {
          ...d,
          full_name: this.extensions?.find(e => e.id == d.extension_id).user_name || 'Không xác định'
        }
      })
      this.changeDef.detectChanges();
    } catch(e) {
      debug.log('ERROR in getDashboard', e)
    }
  }
}
