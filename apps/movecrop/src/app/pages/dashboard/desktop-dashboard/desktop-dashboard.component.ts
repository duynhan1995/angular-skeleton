import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { HeaderControllerService } from '../../../../../../core/src/components/header/header-controller.service';
import { UtilService } from '../../../../../../core/src/services/util.service';
import { CmsService } from '../../../../../../core/src/services/cms.service';
import { PaycardTurtorialModalComponent } from '../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service"

@Component({
  selector: 'etelecom-desktop-dashboard',
  templateUrl: './desktop-dashboard.component.html',
  styleUrls: ['./desktop-dashboard.component.scss']
})
export class DesktopDashboardComponent implements OnInit, OnDestroy {
  @Input() dashboard: any;
  @Input() loading = true;
  @Output() filter = new EventEmitter();
  private modal: any;
  guideline = false;
  selected;
  banner = '';

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private cms: CmsService,
    private modalController: ModalController
  ) { }

  ngOnInit(): void {
    this.selected = {
      startDate: moment().startOf('month'),
      endDate: moment()
    };
    this.filter.emit(this.selected);
  }

  get isMobileVersion() {
    return this.util.isMobile;
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
      this.filter.emit(event)
    }
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

}
