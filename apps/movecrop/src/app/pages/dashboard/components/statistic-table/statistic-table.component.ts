import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import { StatTable } from 'apps/shop/src/models/stat';

declare var eyeDebug: any;

@Component({
  selector: 'shop-statistic-table',
  templateUrl: './statistic-table.component.html',
  styleUrls: ['./statistic-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StatisticTableComponent implements OnInit {

  @Input() statTable: StatTable;

  constructor() { }

  ngOnInit() {
  }

  get isDebug() {
    return eyeDebug == "eyeteam.vn";
  }

}
