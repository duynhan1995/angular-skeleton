import { Component, OnInit } from '@angular/core';
import { AppService } from '@etop/web/core/app.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import {LocationService} from "@etop/state/location";

@Component({
  selector: 'movecrop-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loadConfig = true;
  delayPassed = false;
  completedView = false;

  constructor(
    private appService: AppService,
    private commonUsecase: CommonUsecase,
    private locationService: LocationService
  ) {}


  ngOnInit() {
    this.locationService.initLocations().then();

    setTimeout(() => this.delayPassed = true, 1000);

    this.appService.bootstrap()
      .then(() => this.commonUsecase.checkAuthorization(true))
      .then(() => this.loadConfig = false)
      .catch(err => debug.log('Bootstrap failed', err));
  }

}
