import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule} from '@angular/core';

import {CoreModule} from 'apps/core/src/core.module';
import {MovecropCommonUsecase} from "apps/movecrop/src/app/usecases/movecrop-common.usecase.service";
import {SharedModule} from 'apps/shared/src/shared.module';

import {AppComponent} from './app.component';
import {CommonModule, DatePipe, DecimalPipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {UtilService} from 'apps/core/src/services/util.service';
import {FulfillmentService} from '../services/fulfillment.service';
import {MaterialModule} from '@etop/shared/components/etop-material/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginModule} from 'apps/shared/src/pages/login/login.module';
import {LoginComponent} from './pages/login/login.component';
import {AppGuard} from 'apps/movecrop/src/app/app.guard';
import {IsUserActiveGuard} from './is-user-active.guard';
import {CONFIG_TOKEN} from '@etop/core/services/config.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../environments/environment';
import {EtopPipesModule} from 'libs/shared/pipes/etop-pipes.module';
import {EtopFilterModule} from '@etop/shared/components/etop-filter/etop-filter.module';
import {EtopFormsModule, EtopMaterialModule} from '@etop/shared';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';

import {registerLocaleData} from '@angular/common';
import localeVi from '@angular/common/locales/vi';

registerLocaleData(localeVi);

const services = [
  FulfillmentService,
  UtilService
];

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsUserActiveGuard]
  },
  {
    path: 's',
    loadChildren: () =>
      import('apps/movecrop/src/app/shop/shop.module').then(m => m.ShopModule)
  },
  {
    path: '**',
    redirectTo: 's/-1/settings'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    CoreModule.forRoot(),
    SharedModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgbModule,
    LoginModule,
    EtopFilterModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopFormsModule,
    environment.production ? [] : AkitaNgDevtools.forRoot()
  ],
  providers: [
    {provide: CommonUsecase, useClass: MovecropCommonUsecase},
    {provide: CONFIG_TOKEN, useValue: environment},
    {provide: LOCALE_ID, useValue: 'vi'},
    ...services,
    AppGuard,
    IsUserActiveGuard,
    DecimalPipe,
    DatePipe
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
