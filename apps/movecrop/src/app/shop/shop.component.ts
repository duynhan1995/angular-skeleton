import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CommonLayout} from 'apps/core/src/app/CommonLayout';
import {AuthenticateStore} from '@etop/core';
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {NavigationData} from "apps/core/src/components/header/header.interface";
import {MenuItem} from 'apps/core/src/components/menu-item/menu-item.interface';
import {ConnectionService} from "@etop/features/connection/connection.service";
import {BankService} from "@etop/state/bank";

@Component({
  selector: 'movecrop-shop',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent extends CommonLayout implements OnInit {
  sideMenus: MenuItem[] = [
    {
      name: 'Thống kê',
      route: ['dashboard'],
      matIcon: 'dashboard',
      matIconOutlined: true,
    },
    {
      name: 'Đơn hàng',
      route: ['fulfillments'],
      matIcon: 'shopping_cart',
      matIconOutlined: true,
    },
    {
      name: 'Nhân viên',
      route: ['staffs'],
      matIcon: 'account_circle',
      matIconOutlined: true,
    },
    {
      name: 'Lịch sử cuộc gọi',
      route: ['call-log'],
      matIcon: 'list',
      matIconOutlined: true
    },
    {
      name: 'Ticket',
      route: ['tickets'],
      matIcon: 'support',
      matIconOutlined: true,
    },
    {
      name: 'Thiết lập',
      route: ['settings'],
      matIcon: 'settings',
      matIconOutlined: true,
    }
  ];

  hideSidebar = false;
  hideHeader = false;

  showNotificationIcon = false;
  showVerifyWarning = false;
  showForgotPassword = false;
  isSingle = true;

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private headerController: HeaderControllerService,
    private connectionService: ConnectionService,
    private bankService: BankService
  ) {
    super(auth);
  }

  ngOnInit() {
    this.bankService.initBanks().then();
    this.connectionService.getValidConnections(true).then();

    this.headerController.onNavigate.subscribe(({ target, data }) => {
      this.onNavigate(target, data);
    });
  }

  onNavigate(target, data: NavigationData) {
    let slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    if (data.payload) {
      let accIndex = this.auth.snapshot.accounts.findIndex(a => a.id == data.payload.id);
      accIndex = accIndex < 0 ? 0 : accIndex;
      const acc = this.auth.snapshot.accounts[accIndex];
      slug = acc && acc.url_slug || accIndex;
    }

    switch (data.target) {
      case 'account':
        window.open(`/s/${slug}/settings/user`, '_blank');
        break;
      case 'setting':
        this.router.navigateByUrl(`/s/${slug}/settings/user`).then();
        break;
      default:
        return;
    }
  }

}
