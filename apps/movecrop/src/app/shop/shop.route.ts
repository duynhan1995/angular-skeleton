import { Routes } from '@angular/router';

export const shopRoutes: Routes = [
  {
    path: 'fulfillments',
    loadChildren: () => import('../pages/fulfillments/fulfillments.module').then(m => m.FulfillmentsModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'tickets',
    loadChildren: () => import('../pages/tickets/tickets.module').then(m => m.TicketsModule)
  },
  {
    path: 'call-log',
    loadChildren: () => import('../pages/call-log/call-log.module').then(m => m.CallLogModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'staffs',
    loadChildren: () => import('../pages/staff-management/staff-management.module').then(m => m.StaffManagementModule)
  },
  {
    path: '**',
    redirectTo: 'settings'
  }
];
