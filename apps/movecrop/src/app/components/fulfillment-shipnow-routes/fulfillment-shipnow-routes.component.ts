import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from '@etop/models';

@Component({
  selector: 'movecrop-fulfillment-shipnow-routes',
  templateUrl: './fulfillment-shipnow-routes.component.html',
  styleUrls: ['./fulfillment-shipnow-routes.component.scss']
})
export class FulfillmentShipnowRoutesComponent implements OnInit {
  @Input() ffm = new Fulfillment({});

  loading = true;

  constructor() { }

  ngOnInit() {
    setTimeout(_ => {
      this.loading = false;
    }, 5000);
  }

  trackingLink() {
    if (!this.ffm.shipping_shared_link) { return; }
    window.open(this.ffm.shipping_shared_link);
  }

}
