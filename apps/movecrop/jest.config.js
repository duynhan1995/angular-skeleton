module.exports = {
  name: 'movecrop',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/movecrop/',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
