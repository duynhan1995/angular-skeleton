export const environment = {
  production: true,
  hmr: false,

  base_url: "https://etop.vn",
  crm_url: "https://crm-service.etop.vn",
};
