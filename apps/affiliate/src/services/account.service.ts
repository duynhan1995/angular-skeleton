import { Injectable } from '@angular/core';
import { AccountApi } from '../api/account-api.service';

@Injectable()
export class AccountService {
  constructor(private accountApi: AccountApi) {
  }

  registerAffiliate(data) {
    return this.accountApi.registerAffiliate(data);
  }
}
