import { Injectable } from '@angular/core';
import { AffiliateApi } from 'apps/affiliate/src/api/affiliate-api.service';



@Injectable()
export class AffService {
  constructor(private affApi: AffiliateApi) {}
  accessUser = [
    'phuocquy1392@gmail.com',
    'giang@etop.vn',
    'giang2224@gmail.com',
    'demoaccount@etop.vn'
  ];

  getAffiliate() {
    return this.affApi.getAffiliates();
  }

  async loadProduct(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.affApi.AffiliateGetProducts({ paging });
  }

  async GetReferralCodes(
    start?: number,
    perpage?: number,
    filters?: Array<any>
  ) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.affApi.GetReferralCodes({ paging });
  }

  async GetReferrals(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.affApi.GetReferrals({ paging });
  }

  UpdateAffiliateBankAccount(body) {
    return this.affApi.UpdateAffiliateBankAccount(body);
  }

  CreateOrUpdateAffiliateCommissionSetting(body) {
    return this.affApi.CreateOrUpdateAffiliateCommissionSetting(body);
  }

  GetAffiliateTransaction() {
    return this.affApi.GetTransactions();
  }

  GetOrders() {
    return this.affApi.GetOrders();
  }

  CreateReferralCode(code) {
    return this.affApi.CreateReferralCode(code);
  }

  UpdateReferral(body) {
    return this.affApi.UpdateReferral(body);
  }

  async GetCommissions(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.affApi.GetCommissions({ paging });
  }
}
