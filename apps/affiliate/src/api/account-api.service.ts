import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ServiceCacheBase } from 'apps/core/src/interfaces/ServiceCacheBase';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';

@Injectable({ providedIn: 'root' })
export class AccountApi extends ServiceCacheBase {
  constructor(
    private http: HttpService,
    private ubtService: UserBehaviourTrackingService
  ) {
    super();
  }

  async registerAffiliate(data) {
    return this.http
      .post('api/affiliate.Account/RegisterAffiliate', data)
      .toPromise()
      .then(res => {
        if (res.shop && res.shop.id) {
          this.ubtService.sendUserBehaviour('CreateAff');
        }
        return res;
      });
  }
}
