import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ServiceCacheBase } from 'apps/core/src/interfaces/ServiceCacheBase';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { Product } from 'libs/models/Product';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { AuthenticateStore } from '@etop/core';
import { Promotion } from 'libs/models/affiliate/Promotion';
import { CommissionSetting } from 'libs/models/affiliate/CommissionSetting'

@Injectable({ providedIn: 'root' })
export class AffiliateApi extends ServiceCacheBase {
  transaction = [
    {
      code: 'AS.XB23.3S558',
      created_at: '23/09/2019 12:04',
      description: 'Bạn rút tiền về tài khoản ngân hàng',
      related_order: '',
      debit: -1000000,
      credit: null,
      balance: 5176871,
      status: 'Z'
    },
    {
      code: 'AH.XB28.9S598',
      created_at: '22/09/2019 11:44',
      description: 'Chiết khấu gián tiếp cho đơn hàng',
      related_order: 'BH.KB14.2S515',
      debit: null,
      credit: 240000,
      balance: 6176871,
      status: 'P'
    },
    {
      code: 'AH.XB71.5S247',
      created_at: '15/09/2019 11:45',
      description: 'Chiết khấu gián tiếp cho đơn hàng',
      related_order: 'BS.XB50.1S678',
      debit: null,
      credit: 26000,
      balance: 5936871,
      status: 'P'
    },
    {
      code: 'BS.KB74.5S603',
      created_at: '12/09/2019 11:46',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'BS.XB92.6S340',
      debit: null,
      credit: 1680000,
      balance: 5910871,
      status: 'P'
    },
    {
      code: 'AH.KB90.7S357',
      created_at: '12/09/2019 11:47',
      description: 'Chiết khấu gián tiếp cho đơn hàng',
      related_order: 'AS.KB55.3S416',
      debit: null,
      credit: 216000,
      balance: 4230871,
      status: 'P'
    },
    {
      code: 'BH.XB23.3S445',
      created_at: '12/09/2019 9:47',
      description: 'Bạn rút tiền về tài khoản ngân hàng',
      related_order: '',
      debit: -500000,
      credit: null,
      balance: 4014871,
      status: 'P'
    },
    {
      code: 'BS.XB17.4S782',
      created_at: '11/09/2019 11:48',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'BH.KB59.8S523',
      debit: null,
      credit: 340879,
      balance: 4514871,
      status: 'P'
    },
    {
      code: 'BH.XB21.6S595',
      created_at: '10/09/2019 11:49',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'AS.XB91.6S425',
      debit: null,
      credit: 53823,
      balance: 4173992,
      status: 'P'
    },
    {
      code: 'AH.XB86.8S260',
      created_at: '08/09/2019 11:50',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'BS.XB71.6S316',
      debit: null,
      credit: 1680000,
      balance: 4120169,
      status: 'P'
    },
    {
      code: 'AS.KB19.9S793',
      created_at: '07/09/2019 11:51',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'AS.XB73.4S767',
      debit: null,
      credit: 340879,
      balance: 2440169,
      status: 'P'
    },
    {
      code: 'AH.KB89.5S892',
      created_at: '06/09/2019 11:52',
      description: 'Chiết khấu gián tiếp cho đơn hàng',
      related_order: 'AS.XB25.9S866',
      debit: null,
      credit: 94000,
      balance: 2099290,
      status: 'P'
    },
    {
      code: 'BS.KB80.8S877',
      created_at: '05/09/2019 11:53',
      description: 'Chiết khấu gián tiếp cho đơn hàng',
      related_order: 'AS.KB95.8S151',
      debit: null,
      credit: 30000,
      balance: 2005290,
      status: 'P'
    },
    {
      code: 'AH.KB48.4S251',
      created_at: '05/09/2019 10:53',
      description: 'Bạn rút tiền về tài khoản ngân hàng',
      related_order: '',
      debit: -1000000,
      credit: null,
      balance: 1975290,
      status: 'P'
    },
    {
      code: 'AH.KB17.3S715',
      created_at: '05/09/2019 8:54',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'BH.XB61.9S169',
      debit: null,
      credit: 30290,
      balance: 2975290,
      status: 'P'
    },
    {
      code: 'AH.XB95.8S488',
      created_at: '04/09/2019 11:55',
      description: 'Chiết khấu gián tiếp cho đơn hàng',
      related_order: 'AS.XB70.4S445',
      debit: null,
      credit: 4000,
      balance: 2945000,
      status: 'P'
    },
    {
      code: 'AS.XB17.7S111',
      created_at: '03/09/2019 11:56',
      description: 'Chiết khấu trực tiếp cho đơn hàng',
      related_order: 'BS.KB13.8S894',
      debit: null,
      credit: 120000,
      balance: 2941000,
      status: 'P'
    }
  ];

  orders = [
    {
      code: 'AH.KB82.5S519',
      created_at: '23/09/2019 11:42',
      customer: 'Hoang Thi Thu Phuong',
      shop_name: 'Mỹ phẩm tóc',
      sku: 'OMNIPOWER',
      product_name: 'Omni Power 12 Month',
      product_image: '',
      value: 12000000,
      commission_type: 'Trực tiếp',
      commission_percent: '22%',
      aff_phone: '-',
      aff_name: '',
      status: 'N',
      commission_vnd: 2640000
    },
    {
      code: 'AH.KB69.4S773',
      created_at: '22/09/2019 10:43',
      customer: 'Trương Giang Yến Nhi',
      shop_name: 'Nhi Shop',
      sku: 'IMGGH',
      product_name: 'Google nâng cao',
      product_image: '',
      value: 4900000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0123618979',
      aff_name: 'Trịnh Kiều Nhi',
      status: 'N',
      commission_vnd: 98000
    },
    {
      code: 'BH.KB14.2S515',
      created_at: '22/09/2019 11:44',
      customer: 'Pham ngoc trang',
      shop_name: 'Phố hàng da',
      sku: 'IMAMZ',
      product_name: 'Amazon Camp',
      product_image: '',
      value: 12000000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0121692592',
      aff_name: 'Tô Thị Minh Hiền',
      status: 'P',
      commission_vnd: 240000
    },
    {
      code: 'BS.XB50.1S678',
      created_at: '15/09/2019 11:45',
      customer: 'Phạm Thị Ái Diễm',
      shop_name: 'Hàng chất Việt Nam',
      sku: 'IMFB',
      product_name: 'Facebook cơ bản',
      product_image: '',
      value: 1300000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0121758530',
      aff_name: 'Trương Mẫn Ngọc',
      status: 'P',
      commission_vnd: 26000
    },
    {
      code: 'BS.XB92.6S340',
      created_at: '12/09/2019 11:46',
      customer: 'Võ minh thiện',
      shop_name: 'POLI CASE',
      sku: 'IMPLAN',
      product_name: 'IMPlan',
      product_image: '',
      value: 12000000,
      commission_type: 'Trực tiếp',
      commission_percent: '14%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 1680000
    },
    {
      code: 'AS.KB55.3S416',
      created_at: '12/09/2019 11:47',
      customer: 'Trần thị đào',
      shop_name: 'Cherry BOT',
      sku: 'WEBECOADV12',
      product_name: 'Web Ecommerce Advance 12 Month',
      product_image: '',
      value: 10800000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0121692592',
      aff_name: 'Tô Thị Minh Hiền',
      status: 'P',
      commission_vnd: 216000
    },
    {
      code: 'BH.KB59.8S523',
      created_at: '11/09/2019 11:48',
      customer: 'Lê Minh gương',
      shop_name: 'Săn hàng mỹ',
      sku: 'HARAFN25K',
      product_name: 'HARAFUNNEL 25000 subscribes',
      product_image: '',
      value: 1549450,
      commission_type: 'Trực tiếp',
      commission_percent: '22%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 340879
    },
    {
      code: 'AS.XB91.6S425',
      created_at: '10/09/2019 11:49',
      customer: 'Trần Thanh Thảo',
      shop_name: 'Thảo Nhuộm',
      sku: 'HARAFN2K',
      product_name: 'HARAFUNNEL 2000 subscribes',
      product_image: '',
      value: 244650,
      commission_type: 'Trực tiếp',
      commission_percent: '22%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 53823
    },
    {
      code: 'BS.XB71.6S316',
      created_at: '08/09/2019 11:50',
      customer: 'Trần thị đào',
      shop_name: 'Cherry BOT',
      sku: 'IMPLAN',
      product_name: 'IMPlan',
      product_image: '',
      value: 12000000,
      commission_type: 'Trực tiếp',
      commission_percent: '14%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 1680000
    },
    {
      code: 'AS.XB73.4S767',
      created_at: '07/09/2019 11:51',
      customer: 'Bùi Quốc Anh',
      shop_name: 'Chặn cửa thép',
      sku: 'HARAFN25K',
      product_name: 'HARAFUNNEL 25000 subscribes',
      product_image: '',
      value: 1549450,
      commission_type: 'Trực tiếp',
      commission_percent: '22%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 340879
    },
    {
      code: 'AS.XB25.9S866',
      created_at: '06/09/2019 11:52',
      customer: 'Hai yen',
      shop_name: 'Hải Yến Store',
      sku: 'IMSS',
      product_name: 'Sale System',
      product_image: '',
      value: 4700000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0932372098',
      aff_name: 'Minh Huệ',
      status: 'P',
      commission_vnd: 94000
    },
    {
      code: 'AS.KB95.8S151',
      created_at: '05/09/2019 11:53',
      customer: 'Võ minh thiện',
      shop_name: 'POLI CASE',
      sku: 'IMMKTPLAN',
      product_name: 'Xây dựng một chiến dịch marketing đa kênh hiệu quả',
      product_image: '',
      value: 1500000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0122035691',
      aff_name: 'Lý Thị Thuỳ Trang',
      status: 'P',
      commission_vnd: 30000
    },
    {
      code: 'BH.XB61.9S169',
      created_at: '05/09/2019 11:54',
      customer: 'Phạm Thị Ái Diễm',
      shop_name: 'Hàng chất Việt Nam',
      sku: 'HARAFN1K',
      product_name: 'HARAFUNNEL 1000 subscribes',
      product_image: '',
      value: 151450,
      commission_type: 'Trực tiếp',
      commission_percent: '20%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 30290
    },
    {
      code: 'AS.XB70.4S445',
      created_at: '04/09/2019 11:55',
      customer: 'Phạm Thị Ái Diễm',
      shop_name: 'Hàng chất Việt Nam',
      sku: 'HARARETAIL',
      product_name: 'Chuỗi cửa hàng có hơn 2 Store',
      product_image: '',
      value: 200000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0932372098',
      aff_name: 'Minh Huệ',
      status: 'P',
      commission_vnd: 4000
    },
    {
      code: 'BS.KB13.8S894',
      created_at: '03/09/2019 11:56',
      customer: 'Nguyễn Thị Hà',
      shop_name: 'Mỹ phẩm OASIS',
      sku: 'WEBECPRO1',
      product_name: 'Web Ecommerce Pro 1 Month',
      product_image: '',
      value: 600000,
      commission_type: 'Trực tiếp',
      commission_percent: '20%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 120000
    },
    {
      code: 'BS.KB22.3S717',
      created_at: '22/08/2019 11:57',
      customer: 'Lan Le',
      shop_name: 'LANLAN',
      sku: 'IMTELESALES',
      product_name: 'Chốt đơn online đỉnh cao',
      product_image: '',
      value: 1200000,
      commission_type: 'Trực tiếp',
      commission_percent: '11%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 132000
    },
    {
      code: 'AS.XB39.2S843',
      created_at: '24/07/2019 11:58',
      customer: 'Nguyễn thị thu hà',
      shop_name: 'Hà House',
      sku: 'OMNIPOWER',
      product_name: 'Omni Power 12 Month',
      product_image: '',
      value: 12000000,
      commission_type: 'Trực tiếp',
      commission_percent: '22%',
      aff_phone: '-',
      aff_name: '',
      status: 'P',
      commission_vnd: 2640000
    },
    {
      code: 'AH.KB30.5S386',
      created_at: '23/06/2019 11:59',
      customer: 'Bùi Quốc Anh',
      shop_name: 'Chặn cửa thép',
      sku: 'IMCRE_VIDEO',
      product_name: 'Sản xuất video bằng smartphone',
      product_image: '',
      value: 950000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0121758530',
      aff_name: 'Trương Mẫn Ngọc',
      status: 'P',
      commission_vnd: 19000
    },
    {
      code: 'AS.KB96.4S652',
      created_at: '15/06/2019 11:60',
      customer: 'Nguyễn Quỳnh Anh',
      shop_name: 'ANI HOUSE',
      sku: 'IMMKTPLAN',
      product_name: 'Xây dựng một chiến dịch marketing đa kênh hiệu quả',
      product_image: '',
      value: 1500000,
      commission_type: 'Gián tiếp',
      commission_percent: '2%',
      aff_phone: '0122108515',
      aff_name: 'Bùi thi tu lan',
      status: 'P',
      commission_vnd: 30000
    }
  ];

  affiliates = [
    {
      phone: 934354945,
      name: 'Nguyễn Hoàng Ngọc Trâm',
      email: 'nguyenhoangngoctram@yahoo.com',
      created_at: '',
      total_orders: 0,
      total_revenue: 0,
      received: 0
    },
    {
      phone: 123618979,
      name: 'Trịnh Kiều Nhi',
      email: 'trinhkieunhi2794@gmail.com',
      created_at: '',
      total_orders: 1,
      total_revenue: 4900000,
      received: 240000
    },
    {
      phone: 932372098,
      name: 'Minh Huệ',
      email: 'minhhue@yahoo.com',
      created_at: '',
      total_orders: 2,
      total_revenue: 4900000,
      received: 162290
    },
    {
      phone: 121692592,
      name: 'Tô Thị Minh Hiền',
      email: 'tothiminhhien6380@yahoo.com',
      created_at: '',
      total_orders: 2,
      total_revenue: 10800000,
      received: 1896000
    },
    {
      phone: 122035691,
      name: 'Lý Thị Thuỳ Trang',
      email: 'lythithuytrang@yahoo.com',
      created_at: '',
      total_orders: 1,
      total_revenue: 1500000,
      received: 132000
    },
    {
      phone: 935081530,
      name: 'Pham ngoc kieu uyen',
      email: 'phamngockieuuyen@yahoo.com',
      created_at: '',
      total_orders: 0,
      total_revenue: 0,
      received: 0
    },
    {
      phone: 127364915,
      name: 'Đặng Hồng Vân',
      email: 'danghongvan@gmail.com',
      created_at: '',
      total_orders: 0,
      total_revenue: 0,
      received: 0
    },
    {
      phone: 121758530,
      name: 'Trương Mẫn Ngọc',
      email: 'truongmanngoc3059@yahoo.com',
      created_at: '',
      total_orders: 2,
      total_revenue: 950000,
      received: 94000
    },
    {
      phone: 122108515,
      name: 'Bùi thi tu lan',
      email: 'buithitulan@gmail.com',
      created_at: '',
      total_orders: 1,
      total_revenue: 1500000,
      received: 0
    },
    {
      phone: 934916385,
      name: 'Ha My Linh',
      email: 'hamylinh4590@gmail.com',
      created_at: '',
      total_orders: 0,
      total_revenue: 0,
      received: 0
    }
  ];

  mapImg = [
    {
      sku: 'HARAFN500',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN1K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN2K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN5K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN10K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN15K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN20K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'HARAFN25K',
      image_url: 'https://www.etop.vn/uploads/1569228876_harafunnel.png'
    },
    {
      sku: 'WEBBS12',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'WEBECPRO1',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'WEBECPRO12',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'WEBECOADV1',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'WEBECOADV12',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'CBSC_WEB1',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'CBSC_WEB12',
      image_url: 'https://www.etop.vn/uploads/1569229084_haraweb.png'
    },
    {
      sku: 'OMNIPOWER',
      image_url: 'https://www.etop.vn/uploads/1569229273_omnipower.png'
    },
    {
      sku: 'HARASOCIAL',
      image_url: 'https://www.etop.vn/uploads/1569229435_harasocial.png'
    },
    {
      sku: 'HARARETAIL',
      image_url: 'https://www.etop.vn/uploads/1569229971_hararetail.png'
    },
    {
      sku: 'HARALOYAL',
      image_url: 'https://www.etop.vn/uploads/1569230096_haraloyal.png'
    },
    {
      sku: 'IMSEOC',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMSEOMC',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMGG',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMCHATBOT',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMFB',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMZALO',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMGGH',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMSS',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMFBH',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMZALOH',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMCONTENT',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMLIVESTR',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMPLAN',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMCRE_VIDEO',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMAMZ',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMTELESALES',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMMKTPLAN',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMMKTOPT1',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMMKTOPT2',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    },
    {
      sku: 'IMAUTOBUSINESS',
      image_url: 'https://www.etop.vn/uploads/1569230881_imgroup.png'
    }
  ];

  promotions: any = {
    amount: 0
  };
  constructor(
    private http: HttpService,
    private ubtService: UserBehaviourTrackingService,
    private auth: AuthenticateStore
  ) {
    super();
  }

  async getAffiliates() {
    return this.affiliates;
  }

  async AffiliateGetProducts(query: Partial<ListQueryDTO>, token?: string) {
    return this.http
      .post('api/affiliate.Affiliate/AffiliateGetProducts', query, token)
      .toPromise()
      .then(res => res.products.map(p => {
        return {
          product: new Product({...p.product,
            retail_price: p.product.variants[0].retail_price,
            list_price: p.product.variants[0].list_price,
            cost_price: p.product.variants[0].cost_price
          }),
          promotion: p.promotion && new Promotion({
            ...p.promotion,
            amount: p.promotion.amount / 100
          }),
          affiliate_commission_setting: p.affiliate_commission_setting && new CommissionSetting(p.affiliate_commission_setting),
          shop_commission_setting: p.shop_commission_setting && new CommissionSetting({
            ...p.shop_commission_setting,
            amount: p.shop_commission_setting.amount / 100
          })
        };
      }));
  }

  async CreateOrUpdateAffiliateCommissionSetting(body, token?) {
    const options = this.getOptions(token);
    return this.http
      .postWithOptions(
        `api/affiliate.Affiliate/CreateOrUpdateAffiliateCommissionSetting`,
        body,
        options
      )
      .toPromise()
      .then(res => res);
  }

  async CreateOrUpdateTradingCommissionSetting(body, token?) {
    const options = this.getOptions(token);
    return this.http
      .postWithOptions(
        `api/affiliate.Affiliate/CreateOrUpdateTradingCommissionSetting`,
        body,
        options
      )
      .toPromise()
      .then(res => res);
  }

  async CreateProductPromotion() {}

  async GetCommissions(query: Partial<ListQueryDTO>, token?: string) {
    return this.http
      .post('api/affiliate.Affiliate/GetCommissions', query, token)
      .toPromise()
      .then(res => res.commissions);
  }

  async GetProductPromotionByProductID(id) {}

  async UpdateProductPromotion() {}

  getOptions(token) {
    const headers = this.http.createHeader(token);
    const options = this.http.createDefaultOption(headers);
    return options;
  }

  async UpdateAffiliateBankAccount(bank_account, token?: string) {
    return this.http
      .post(
        'api/affiliate.Account/UpdateAffiliateBankAccount',
        { bank_account },
        token
      )
      .toPromise()
      .then(res => res);
  }

  async GetTransactions() {
    return this.transaction;
  }

  async GetReferralCodes(query: Partial<ListQueryDTO>, token?: string) {
    return this.http
      .post('api/affiliate.Affiliate/GetReferralCodes', query, token)
      .toPromise()
      .then(res => res.referral_codes);
  }

  async CreateReferralCode(code) {
    return this.http
      .post(`api/affiliate.Affiliate/CreateReferralCode`, { code })
      .toPromise()
      .then(res => res);
  }

  async GetOrders() {
    this.orders.map(order => {
      order.product_image = this.MapImage(order.sku);
    });
    return this.orders;
  }

  MapImage(sku) {
    let item = this.mapImg.find(v => v.sku == sku);
    return item ? item.image_url : '';
  }

  UpdateReferral(body) {
    return this.http
      .post(`api/affiliate.User/UpdateReferral`, body)
      .toPromise()
      .then(res => res);
  }

  async GetReferrals(query: Partial<ListQueryDTO>, token?: string) {
    return this.http
      .post('api/affiliate.Affiliate/GetReferrals', query, token)
      .toPromise()
      .then(res => res.referrals);
  }
}

