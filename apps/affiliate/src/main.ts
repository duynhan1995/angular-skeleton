import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './environments/environment';
import { hmrBootstrap } from 'apps/core/src/hmr';
import * as debug from '@etop/utils/debug';
import { AffiliateRootModule } from './app/affiliate-root.module';

(window as any).__debug_host = '';
debug.setupDebug(environment);

if (environment.production) {
  enableProdMode();
}

(window as any).__debug_host = '';

const bootstrap = () => platformBrowserDynamic().bootstrapModule(AffiliateRootModule);

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  bootstrap().catch(err => console.log(err));
}
