import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AffiliateModule } from './affiliate/affiliate.module';
import { LoginModule } from '../../../shared/src/pages/login/login.module';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AffiliateCommonUsecase } from 'apps/affiliate/src/app/usecases/affiliate-common.usecase.service';
import { SharedModule } from 'apps/shared/src/shared.module';
import { CommonModule } from '@angular/common';
import { CONFIG_TOKEN } from '@etop/core/services/config.service';
import { environment } from '../environments/environment';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AffiliateModule,
    LoginModule,
    CommonModule,
    SharedModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    {provide: CONFIG_TOKEN, useValue: environment},
    { provide: CommonUsecase, useClass: AffiliateCommonUsecase }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AffiliateRootModule {
}
