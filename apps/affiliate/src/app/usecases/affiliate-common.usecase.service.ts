import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { UserService } from 'apps/core/src/services/user.service';
import { AuthenticateStore } from '@etop/core';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { AccountService } from 'apps/affiliate/src/services/account.service';
import { TelegramService } from '@etop/features';
import { CrmApi } from 'apps/core/src/apis/crm-api.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserApi } from '@etop/api';
import { StorageService } from 'apps/core/src/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AffiliateCommonUsecase extends CommonUsecase {
  constructor(private userService: UserService,
              private userApi: UserApi,
              private auth: AuthenticateStore,
              private affService: AffService,
              private accountService: AccountService,
              private telegram: TelegramService,
              private crmService: CrmApi,
              private router: Router,
              private storage: StorageService,
  ) {
    super();
  }

  async checkAuthorization(fetchAccounts?: boolean): Promise<any> {
    try {
      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(fetchAccounts);
    } catch (e) {
      debug.error('ERROR in checking authorization', e);
      this.auth.clear();
      this.auth.setRef('/login');
      this.router.navigateByUrl('/login');
    }
  }

  async login(data: { login: string; password: string }): Promise<any> {
    let res = await this.userApi.login({
      login: data.login,
      password: data.password,
      account_type: 'affiliate'
    });
    this.auth.updateToken(res.access_token);
  }

  async register(data: any, source: string): Promise<any> {
    return await this.userService.signUp({ ...data, source });
  }

  async updateSessionInfo(fetchAccounts?: boolean): Promise<any> {
    let res = await this.userService.checkToken();
    let referralCode = new URL(window.location.href).searchParams.get('e_aff') || this.storage.get('e_aff');

    let affAccounts = res.available_accounts.filter(
      account => account.type == 'affiliate'
    );
    if (!affAccounts || affAccounts.length == 0) {
      let body = {
        name: res.user.full_name,
        phone: res.user.phone,
        email: res.user.email
      };
      let newAffiliate = await this.accountService.registerAffiliate(body);
      res = await this.userService.switchAccount(newAffiliate.id);
      this.auth.updateToken(res.access_token);
      res = await this.userService.checkToken();
      affAccounts = res.available_accounts.filter(
        account => account.type == 'affiliate'
      );
      if (referralCode) {
        await this.affService.UpdateReferral({
          referral_code: referralCode
        });
      }
      this.telegram.newSellerMessage(res.user).then();
    }

    this.vTigerCreate(res.user);

    this.auth.updateInfo({
      token: res.access_token,
      account: res.account,
      session: res,
      user: res.user,
      accounts: affAccounts,
      isAuthenticated: true,
    });
    let referralCodes = await this.affService.GetReferralCodes();
    if (!referralCodes.find(r => (r.code = res.user.phone))) {
      await this.affService.CreateReferralCode(res.user.phone);
    }
  }

  async vTigerCreate(user) {
    let isTest = user.email.split(/-[0-9a-zA-Z]+-test$/).length > 1;
    let body = {
      firstname: isTest ? 'TEST' : '',
      lastname: user.full_name,
      phone: user.phone,
      email: user.email,
      leadsource: user.source,
      etop_id: user.id,
      account_type: 'affiliate'
    };

    this.crmService.updateLead(body);
    this.crmService.updateContact(body);
  }

  async redirectIfAuthenticated(): Promise<any> {
    return undefined
  }
}
