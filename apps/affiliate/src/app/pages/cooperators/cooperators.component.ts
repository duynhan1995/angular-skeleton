import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { InviteAffiliateModalComponent } from '../../components/invite-affiliate-modal/invite-affiliate-modal.component';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';

@Component({
  selector: 'aff-cooperators',
  templateUrl: './cooperators.component.html',
  styleUrls: ['./cooperators.component.scss']
})
export class CooperatorsComponent implements OnInit, OnDestroy {
  affList;
  handle = 'cooperators';
  constructor(
    private affService: AffService,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private headerController: HeaderControllerService
  ) {}

  ngOnInit() {
    this.getAffList();
    this.headerController.setActions([
      {
        title: 'Mời cộng tác viên',
        cssClass: 'btn btn-primary',
        onClick: () => this.showModal()
      }
    ]);
  }

  ngOnDestroy(): void {
    this.headerController.clearActions();
  }

  showModal() {
    let modal = this.modalController.create({
      component: InviteAffiliateModalComponent,
      showBackdrop: true,
      cssClass: 'modal-lg',
      componentProps: {}
    });
    modal.show().then();
    modal.onDismiss().then(async () => {});
  }

  async getAffList() {
    this.affList = await this.affService.GetReferrals();
  }
}
