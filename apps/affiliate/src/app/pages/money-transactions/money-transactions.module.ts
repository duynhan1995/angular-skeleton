import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { MoneyTransactionsComponent } from './money-transactions.component';
import { GuideLinkModule } from '../../components/guide-link/guide-link.module';

const routes: Routes = [
  {
    path: '',
    component: MoneyTransactionsComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [MoneyTransactionsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    GuideLinkModule
  ],
  exports: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MoneyTransactionsModule {}
