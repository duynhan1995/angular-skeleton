import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { WithdrawModalComponent } from '../../components/withdraw-modal/withdraw-modal.component';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { VND } from 'libs/shared/pipes/etop.pipe';

@Component({
  selector: 'aff-money-transactions',
  templateUrl: './money-transactions.component.html',
  styleUrls: ['./money-transactions.component.scss']
})
export class MoneyTransactionsComponent implements OnInit, OnDestroy {
  transactions: any;

  credit = {
    first: 2821000,
    last: 6176871
  };
  handle = 'transactions';
  private transformMoney = new VND();

  constructor(
    private affService: AffService,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private headerController: HeaderControllerService
  ) {}

  ngOnInit() {
    let accessUser = this.affService.accessUser;
    if (accessUser.indexOf(this.auth.snapshot.user.email) >= 0) {
      this.getTransactions();
    } else {
      this.credit = {
        first: 0,
        last: 0
      };
    }
    this.headerController.setActions([
      {
        title: 'Tạo lệnh rút tiền',
        cssClass: 'btn btn-primary',
        onClick: () => this.showModal()
      }
    ]);

    this.headerController.setContent(`
      <div class="text-right">
        <span>Số dư</span>
        <h5 class="font-weight-bold">${this.transformMoney.transform(this.credit.last)}đ</h5>
      </div>
    `);
  }



  ngOnDestroy(): void {
    this.headerController.clearActions();
    this.headerController.clearContent();
  }

  showModal() {
    let modal = this.modalController.create({
      component: WithdrawModalComponent,
      showBackdrop: true,
      cssClass: 'modal-lg',
      componentProps: {}
    });
    modal.show().then();
    modal.onDismiss().then(async () => { });
  }

  async getTransactions() {
    this.transactions = await this.affService.GetAffiliateTransaction();
  }

  statusDisplay(status) {
    const statusMap = {
      Z: 'Chưa xác nhận',
      P: 'Đã xác nhận'
    };
    return statusMap[status];
  }
}
