import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import * as validatecontraints from 'apps/core/src/services/validation-contraints.service';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { StorageService } from '../../../../../core/src/services/storage.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';

const accountSource = [
  'unknown',
  'psx',
  'etop',
  'topship',
  'ts_app_android',
  'ts_app_ios',
  'ts_app_web',
  'partner'
];

@Component({
  selector: 'aff-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  constructor(
    private router: Router,
    private util: UtilService,
    private commonUsecase: CommonUsecase,
    private storage: StorageService,
    private affService: AffService,
  ) {}

  signupData: any = {
    agree_email_info: true,
    agree_tos: false
  };

  error = false;
  errorMessage = '';
  loading = false;

  serviceTermsLink = '';

  async ngOnInit() {
    this.serviceTermsLink = await this.util.getEtopContent(0);
  }

  async signUp() {
    const signupData = this.signupData;
    this.error = false;
    let phone = signupData.phone;
    let email = signupData.email;

    let source = localStorage.getItem('REF');
    if (!source || accountSource.indexOf(source) === -1) {
      source = 'etop';
    }

    try {
      if (!signupData.full_name) {
        throw new Error('Vui lòng nhập Tên đầy đủ.');
      }

      phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      phone = (phone && phone.split('-test')[0]) || '';

      if (!phone || !phone.match(/^[0-9]{9,10}$/)) {
        throw new Error('Số điện thoại không đúng');
      }

      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = email.split('-test')[0];

      if (!validatecontraints.EmailValidates[0].check(email)) {
        throw new Error('Địa chỉ email không hợp lệ.');
      }
      if (signupData.password !== signupData.confirm) {
        throw new Error('Mật khẩu nhập lại không chính xác.');
      }
      await this.commonUsecase.register(signupData, source).then((signupRes) => this.signupData = Object.assign({}, signupData, signupRes.user));

      await this.commonUsecase.login({
        login: signupData.email,
        password: signupData.password
      }).then(() => this.commonUsecase.updateSessionInfo(true))
        .then(() => this.router.navigateByUrl('/dashboard'));
    } catch (e) {
      this.error = true;
      this.errorMessage = e.message;
      toastr.error(e.message, 'Đăng ký thất bại.');
      return;
    }
  }

  get isMobile() {
    return this.util.isMobile;
  }
}
