import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';



@Component({
  selector: 'aff-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products;
  handle = 'products';
  page: number;
  perpage: number;

  @ViewChild('productTable', { static: true }) productTable: EtopTableComponent;

  constructor(
    private affService: AffService,
    private auth: AuthenticateStore
  ) {}

  async ngOnInit() {
  }

  async getProducts(page, perpage) {
    this.products = await this.affService.loadProduct(
      (page - 1) * perpage,
      perpage
    );
  }

  onPagination({ page, perpage }) {
    this.page = page;
    setTimeout(async () => {
      this.productTable.loading = true;
      this.perpage = perpage;
      await this.getProducts(page, perpage);
      this.productTable.loading = false;
    }, 0);
  }

  async reloadProduct(e) {
    const { perpage } = this;
    await this.getProducts(this.page, perpage);
  }
}
