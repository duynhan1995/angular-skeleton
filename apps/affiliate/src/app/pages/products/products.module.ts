import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ProductsComponent } from './products.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyInputModule } from '../../components/currency-input/currency-input.module';
import { FormsModule } from '@angular/forms';
import { ProductRowComponent } from './components/product-row/product-row.component';
import { ProductDetailModalComponent } from './components/product-detail-modal/product-detail-modal.component';
import { GuideLinkModule } from '../../components/guide-link/guide-link.module';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [
    ProductsComponent,
    ProductRowComponent,
    ProductDetailModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    FormsModule,
    CurrencyInputModule,
    RouterModule.forChild(routes),
    GuideLinkModule
  ],
  exports: [],
  providers: [],
  entryComponents: [ProductDetailModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductsModule {}
