import { Component, OnInit, Input } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'aff-product-detail-modal',
  templateUrl: './product-detail-modal.component.html',
  styleUrls: ['./product-detail-modal.component.scss']
})
export class ProductDetailModalComponent implements OnInit {
  @Input() product;
  desc: any = [];

  constructor(
    private util: UtilService,
    private sanitizer: DomSanitizer,
    private modalDismiss: ModalAction
  ) {}

  ngOnInit() {
    let text = this.product.description.split('&&&&')[0].split('###');
    text.map(d => {
      this.desc.push({
        title: d.split('##')[0],
        content: this.sanitizer.bypassSecurityTrustResourceUrl(
          d.split('##')[1]
        ),
        id: this.util.createHandle(d.split('##')[0])
      });
    });
  }
  close() {
    this.modalDismiss.dismiss(null);
  }
}
