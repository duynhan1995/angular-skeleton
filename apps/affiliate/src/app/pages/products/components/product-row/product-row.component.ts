import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductDetailModalComponent } from '../product-detail-modal/product-detail-modal.component';
import { ConfigService } from '@etop/core';


@Component({
  selector: '[aff-product-row]',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss']
})
export class ProductRowComponent implements OnInit {
  @Input() item;
  @Output() updateProduct = new EventEmitter();
  editting = false;
  aff_commission_vnd;
  aff_commission_percent;

  constructor(
    private affService: AffService,
    private modalController: ModalController,
    private modalDismiss: ModalAction,
    private config: ConfigService
  ) {
  }

  ngOnInit() {
    this.item = this.productMap(this.item);
  }

  copyLinkProduct(id) {
    let url = this.config.getConfig().base_url + '/s/aff/etop-trading/' + id;
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    toastr.success('Sao chép thành công!');
  }

  onEdit() {
    this.aff_commission_vnd = this.item.affiliate_commission_setting.amount;
    this.aff_commission_percent =
      Math.round((this.aff_commission_vnd / this.item.etop_revenue) * 1000) /
      10;
    this.editting = true;
  }

  async updateCommisstion() {
    try {
      if (
        this.aff_commission_vnd > this.item.shop_commission_setting.exchange
      ) {
        toastr.error('Số tiền bạn chiết khấu đang lớn hơn eTop chiết khấu!');
        return;
      }
      let body = {
        product_id: this.item.product.id,
        amount: this.aff_commission_vnd,
        unit: 'vnd'
      };
      await this.affService.CreateOrUpdateAffiliateCommissionSetting(body);
      this.updateProduct.emit();
      toastr.success('Lưu chiết khấu thành công');
    } catch (e) {
      debug.log('Error update commission', e);
    }
  }

  back() {
    this.editting = false;
  }

  onChangeCommission() {
    if (this.aff_commission_vnd > this.item.shop_commission_setting.exchange) {
      toastr.error('Số tiền bạn chiết khấu đang lớn hơn eTop chiết khấu!');
      return;
    }
    this.aff_commission_percent =
      Math.round((this.aff_commission_vnd / this.item.etop_revenue) * 1000) /
      10;
  }

  detailProduct() {
    let modal = this.modalController.create({
      component: ProductDetailModalComponent,
      showBackdrop: true,
      cssClass: 'modal-xl',
      componentProps: {
        product: this.item.product
      }
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  productMap(item) {
    if (!item.promotion) {
      item.promotion = {
        amount: 0,
        exchange: 0
      };
    }
    if (!item.affiliate_commission_setting) {
      item.affiliate_commission_setting = {
        amount: 0,
        exchange: 0
      };
    }
    if (!item.shop_commission_setting) {
      item.shop_commission_setting = {
        amount: 0,
        exchange: 0
      };
    }
    let etop_revenue =
      item.product.retail_price * (1 - item.promotion.amount / 100);

    return Object.assign(item, {
      etop_revenue: etop_revenue,
      promotion: {
        exchange: (item.promotion.amount / 100) * item.product.retail_price,
        amount: item.promotion.amount
      },
      affiliate_commission_setting: {
        amount: item.affiliate_commission_setting.amount,
        exchange: Number((item.affiliate_commission_setting.amount / etop_revenue) * 100).toFixed(2)
      },
      shop_commission_setting: {
        amount: item.shop_commission_setting.amount,
        exchange: (item.shop_commission_setting.amount * etop_revenue) / 100
      },
      aff_receive_exchange:
        (item.shop_commission_setting.amount * etop_revenue) / 100 -
        item.affiliate_commission_setting.amount,
      aff_receive: Number(item.shop_commission_setting.amount - (item.affiliate_commission_setting.amount / etop_revenue) * 100).toFixed(2)
    });
  }
}
