import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { StorageService } from 'apps/core/src/services/storage.service';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';

@Component({
  selector: 'aff-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login: string;
  password: string;

  error = false;
  errorMessage = '';

  loading = false;

  constructor(
    private commonUsecase: CommonUsecase,
    private router: Router,
    private storage: StorageService,
    private affService: AffService
  ) {}

  ngOnInit() {}

  async submit() {
    this.loading = true;
    try {
      await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
      await this.commonUsecase.updateSessionInfo(true);
      this.router.navigateByUrl('/dashboard');
    } catch (e) {
      console.error(e);
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
    this.loading = false;
  }

  forgotPassword() {}
}
