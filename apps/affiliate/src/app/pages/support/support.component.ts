import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'aff-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  ngOnInit() {}

  handle = 'handle';

  copyLinkToClipboard(link) {
    if (typeof link === 'string') {
      let selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = link;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
      toastr.success('Sao chép thành công!');
    }
  }

  openLinkInNewTab(href) {
    if (typeof href === 'string') {
      Object.assign(document.createElement('a'), {
        target: '_blank',
        href
      }).click();
    }
  }
}
