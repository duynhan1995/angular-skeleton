import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SupportComponent } from './support.component';
import { FormsModule } from '@angular/forms';
import { GuideLinkModule } from '../../components/guide-link/guide-link.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: '',
    component: SupportComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [SupportComponent],
  imports: [
    CommonModule,
    NgbModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild(routes),
    GuideLinkModule
  ],
  exports: [],
  providers: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupportModule {}
