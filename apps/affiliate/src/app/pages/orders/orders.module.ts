import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { GuideLinkModule } from '../../components/guide-link/guide-link.module';
import { OrderRowComponent } from './components/order-row/order-row.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    children: [{ path: '', component: OrdersComponent}]
  }
];

@NgModule({
  declarations: [OrdersComponent, OrderRowComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    GuideLinkModule
  ],
  exports: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule {}
