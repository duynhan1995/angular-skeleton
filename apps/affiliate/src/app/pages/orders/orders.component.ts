import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { MappingService } from 'apps/core/src/services/mapping.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';

@Component({
  selector: 'aff-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  @ViewChild('productTable', { static: true }) productTable: EtopTableComponent;

  orders: any;
  handle = 'orders';
  page: number;
  perpage: number;
  loadDone = false;

  constructor(
    private affService: AffService,
    private auth: AuthenticateStore,
    private mapingService: MappingService
  ) {}

  async ngOnInit() {}

  async getOrders(page, perpage) {
    this.orders = await this.affService.GetCommissions(
      (page - 1) * perpage,
      perpage
    );
    this.loadDone = true;

    debug.log('orders', this.orders);
  }

  onPagination({ page, perpage }) {
    this.page = page;
    setTimeout(async () => {
      this.productTable.loading = true;
      this.perpage = perpage;
      await this.getOrders(page, perpage);
      this.productTable.loading = false;
    }, 0);
  }


}
