import { Component, OnInit, Input } from '@angular/core';
import { MappingService } from 'apps/core/src/services/mapping.service';

@Component({
  selector: '[aff-order-row]',
  templateUrl: './order-row.component.html',
  styleUrls: ['./order-row.component.scss']
})
export class OrderRowComponent implements OnInit {
  @Input() item;

  constructor(private mapingService: MappingService) {}

  ngOnInit() {}

  statusDisplay(status) {
    if (status != 'P') {
      return 'Chưa thanh toán';
    }
    return this.mapingService.mapPaymentStatus(status);
  }

  typeDisplay(type) {
    if (type == 'direct') {
      return 'Trực tiếp'
    } else {
      return 'Gián tiếp'
    }
  }
}
