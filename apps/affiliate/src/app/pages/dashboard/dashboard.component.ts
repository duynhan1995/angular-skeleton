import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { StatTable } from 'apps/shop/src/models/stat';
import { UtilService } from 'apps/core/src/services/util.service';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { AuthenticateStore } from '@etop/core';

declare var eyeDebug: any;

class ChartDataOpts {
  includeColDefault?: boolean;
  includeCellDefault?: boolean;
  includeRowDefault?: boolean;
  includedCol?: Array<string>;
  includedCell?: Array<string>;
  includedRow?: Array<string>;
  excludedCol?: Array<string>;
  excludedCell?: Array<string>;
  excludedRow?: Array<string>;
  colorMap?: any;
  typeMap?: any;
}

@Component({
  selector: 'aff-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  topSale = [
    {
      image: '',
      name: 'HARAWEB Web Ecommerce Pro 1 Month',
      quantity: 25
    },
    {
      image: '',
      name: 'LOYALTY Membership & Automation Marketing',
      quantity: 22
    },
    {
      image: '',
      name: 'HARAFUNNEL 20000 subscribers',
      quantity: 18
    },
    {
      image: '',
      name: 'HARAWEB Web Ecommerce Advance 12 Month',
      quantity: 16
    },
    {
      image: '',
      name: 'HARAFUNNEL 5000 subscribers',
      quantity: 15
    },
    {
      image: '',
      name: 'HARAFUNNEL 25000 subscribers',
      quantity: 15
    },
    {
      image: '',
      name: 'OMNI POWER 12 Month',
      quantity: 10
    },
    {
      image: '',
      name: 'HARAFUNNEL 10000 subscribers',
      quantity: 9
    },
    {
      image: '',
      name: 'HARAWEB Web Ecommerce Advance 1 Month',
      quantity: 7
    },
    {
      image: '',
      name: 'HARAFUNNEL 1000 subscribers',
      quantity: 3
    }
  ];

  loading = true;
  shop_name = '';
  guideline = false;

  unpaid_ffm: StatTable;
  order_status: StatTable;
  ffm_expected_delivery: StatTable;
  created_order: StatTable;
  shipping_provider: StatTable;
  created_order_after: StatTable;
  average_create_order_after: StatTable;
  chartData: any;

  before_cross_check = true;

  mainMaxHeight = 'calc(100vh - 50px)';
  private _tabs = [];
  handle = 'dashboard';

  constructor(
    private utilService: UtilService,
    public sanitizer: DomSanitizer,
    private gaService: GoogleAnalyticsService,
    private auth: AuthenticateStore,
    private cdref: ChangeDetectorRef
  ) {}

  async ngOnInit() {}

  get isDebug() {
    return eyeDebug == 'eyeteam.vn';
  }
}
