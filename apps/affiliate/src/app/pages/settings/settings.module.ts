import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SettingsComponent } from './settings.component';
import { FormsModule } from '@angular/forms';
import { GuideLinkModule } from '../../components/guide-link/guide-link.module';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild(routes),
    GuideLinkModule
  ],
  exports: [],
  providers: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {}
