import {AfterViewInit, Component, OnInit} from '@angular/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ChangePasswordModalComponent } from 'apps/shared/src/components/change-password-modal/change-password-modal.component';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import { AffService } from 'apps/affiliate/src/services/affiliate.service';
import { UserService } from 'apps/core/src/services/user.service';
import {BankQuery} from "@etop/state/bank";
import {FormBuilder} from "@angular/forms";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {BankAccount} from "@etop/models";

@Component({
  selector: 'aff-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent extends BaseComponent implements OnInit, AfterViewInit {
  bankAccount: BankAccount;
  user: any;

  loading_bank = true;
  url;
  handle = 'settings';

  bankForm = this.fb.group({
    bankCode: '',
    bankProvinceCode: '',
    bankBranchCode: ''
  });

  formInitializing = true;

  banksList$ = this.bankQuery.select("banksList");
  bankProvincesList$ = combineLatest([
    this.bankQuery.select("bankProvincesList"),
    this.bankForm.controls['bankCode'].valueChanges]).pipe(
    map(([bankProvinces, bankCode]) => {
      if (!bankCode) { return []; }
      return bankProvinces?.filter(prov => prov.bank_code == bankCode);
    })
  );
  bankBranchesList$ = combineLatest([
    this.bankQuery.select("bankBranchesList"),
    this.bankForm.controls['bankCode'].valueChanges,
    this.bankForm.controls['bankProvinceCode'].valueChanges]).pipe(
    map(([bankBranches, bankCode, bankProvinceCode]) => {
      if (!bankProvinceCode) { return []; }
      return bankBranches?.filter(branch => branch.bank_code == bankCode && branch.province_code == bankProvinceCode);
    })
  );

  bankValueMap = bank => bank && bank.code || null;

  constructor(
    private fb: FormBuilder,
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private affService: AffService,
    private userService: UserService,
    private bankQuery: BankQuery
  ) {
    super();
  }

  ngOnInit() {
    this.user = this.auth.snapshot.user;
    this.url = window.location.origin;

    this.bankForm.controls['bankCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.bankForm.patchValue({
            bankProvinceCode: '',
            bankBranchCode: '',
          });
          this.bankAccount.name = this.bankQuery.getBank(value)?.name;
        }
      });

    this.bankForm.controls['bankProvinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.bankForm.patchValue({
            bankBranchCode: '',
          });
          this.bankAccount.province = this.bankQuery.getBankProvince(value)?.name;
        }
      });

    this.bankForm.controls['bankBranchCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.bankAccount.branch = this.bankQuery.getBankBranch(value)?.name;
        }
      });
  }

  async ngAfterViewInit() {
    let session = await this.userService.checkToken();

    this.bankAccount = session.affiliate.bank_account;

    if (!this.bankAccount) { return; }

    this.formInitializing = true;

    const bankCode = this.bankQuery.getBankByName(this.bankAccount.name)?.code;
    const bankProvinceCode = this.bankQuery.getBankProvinceByName(this.bankAccount.province)?.code;
    const bankBranchCode = this.bankQuery.getBankBranchByName(this.bankAccount.branch, bankProvinceCode)?.code;

    this.bankForm.patchValue({
      bankCode,
      bankProvinceCode,
      bankBranchCode,
    });

    this.formInitializing = false;
  }

  onDataEdited(e) {
    debug.log('e', e);
  }

  copyText(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    toastr.success('Sao chép thành công!');
  }

  openChangePasswordModal() {
    this.modalController
      .create({
        component: ChangePasswordModalComponent
      })
      .show()
      .then();
  }

  async UpdateBankAccount() {
    try {
      await this.affService.UpdateAffiliateBankAccount(this.bankAccount);
      await this.userService.checkToken();
      toastr.success('Cập nhật tài khoản ngân hàng thành công');
    } catch (e) {
      toastr.error('Cập nhật thất bại! ', e);
    }
  }
}
