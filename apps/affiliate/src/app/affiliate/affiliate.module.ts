import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AffiliateComponent } from './affiliate.component';
import { AffiliateRouteModule } from './affiliate-route.module';
import { CoreModule } from '../../../../core/src/core.module';
import { SharedModule } from '../../../../shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { LoginModule } from '../pages/login/login.module';
import { AffiliateApi } from '../../api/affiliate-api.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChangePasswordModalComponent } from 'apps/shared/src/components/change-password-modal/change-password-modal.component';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { ShopCrmService } from 'apps/shop/src/services/shop-crm.service';
import { AccountApi } from '../../api/account-api.service';
import { AccountService } from '../../services/account.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { InviteAffiliateModalComponent } from '../components/invite-affiliate-modal/invite-affiliate-modal.component';
import { WithdrawModalComponent } from '../components/withdraw-modal/withdraw-modal.component';
import { RegisterModule } from '../pages/register/register.module';
import { AffService } from '../../services/affiliate.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GuideModalComponent } from '../components/guide-modal/guide-modal.component';
import { TelegramService } from '@etop/features';
import { WelcomeModalComponent } from 'apps/affiliate/src/app/components/welcome-modal/welcome-modal.component';

const components = [
  InviteAffiliateModalComponent,
  WithdrawModalComponent,
  GuideModalComponent,
  WelcomeModalComponent
];
const apis = [
  AffiliateApi,
  AccountApi
];
const services = [
  AffService,
  GoogleAnalyticsService,
  ShopCrmService,
  AccountService,
  UtilService,
  TelegramService
];

@NgModule({
  declarations: [AffiliateComponent, ...components],
  imports: [
    CoreModule.forRoot(),
    CommonModule,
    FormsModule,
    LoginModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AffiliateRouteModule,
    RegisterModule,
    NgbModule
  ],
  entryComponents: [
    ChangePasswordModalComponent,
    InviteAffiliateModalComponent,
    WithdrawModalComponent,
    GuideModalComponent,
    WelcomeModalComponent
  ],
  providers: [...apis, ...services],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AffiliateModule {}
