import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AffiliateComponent } from './affiliate.component';
import { affiliateRoutes } from './affiliate-route';

const slugRoute = [
  {
    path: '',
    component: AffiliateComponent,
    children: affiliateRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(slugRoute)],
  exports: [RouterModule]
})
export class AffiliateRouteModule {

}
