import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateStore, ConfigService } from '@etop/core';
import {StringHandler} from "@etop/utils";
import { UserService } from 'apps/core/src/services/user.service';
import { CommonLayout } from 'apps/core/src/app/CommonLayout';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { GuideModalComponent } from '../components/guide-modal/guide-modal.component';
import { CmsService } from 'apps/core/src/services/cms.service';
import { StorageService } from 'apps/core/src/services/storage.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { WelcomeModalComponent } from 'apps/affiliate/src/app/components/welcome-modal/welcome-modal.component';
import {BankService} from "@etop/state/bank";

const SPECIAL_ROUTES = {
  login: {
    display_on_mobile: true,
    require_login: false
  },
  register: {
    display_on_mobile: true,
    require_login: false
  },
  'top-ship': {
    display_on_mobile: true,
    require_login: false
  },
  tracking: {
    display_on_mobile: true,
    require_login: false
  },
  'reset-password': {
    display_on_mobile: true,
    require_login: false
  },
  'on-boarding': {
    display_on_mobile: true,
    require_login: true
  },
  survey: {
    display_on_mobile: true,
    require_login: true
  },
  'admin-login': {
    display_on_mobile: true,
    require_login: false
  },
  'verify-email': {
    display_on_mobile: true,
    require_login: true
  }
};

@Component({
  selector: 'aff-affiliate',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./affiliate.component.scss']
})
export class AffiliateComponent extends CommonLayout implements OnInit {
  sideMenus = [
    {
      name: 'Thống kê',
      route: ['/dashboard'],
      icon: 'assets/images/icon_dashboard.png'
    },
    {
      name: 'Sản phẩm',
      route: ['/products'],
      icon: 'assets/images/icon_product.png'
    },
    {
      name: 'Đơn hàng',
      route: ['/orders'],
      icon: 'assets/images/icon_order.png'
    },
    {
      name: 'Giao dịch',
      route: ['/transactions'],
      icon: 'assets/images/icon_transaction.png'
    },
    {
      name: 'Cộng tác viên',
      route: ['/cooperators'],
      icon: 'assets/images/icon_trader.png'
    },
    {
      name: 'Thiết lập',
      route: ['/settings'],
      icon: 'assets/images/icon_settings.png'
    },
    {
      name: 'Hỗ trợ',
      route: ['/support'],
      icon: 'assets/images/icon_support.png'
    },
    {
      name: 'Bảng xếp hạng',
      route: ['/ranking'],
      icon: 'assets/images/icon_ranking.png'
    }
  ];

  customSidebar;
  isSingle = true;
  banners: any;
  private announcement_modal: any;

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private userService: UserService,
    private modalController: ModalController,
    private cms: CmsService,
    private storage: StorageService,
    private util: UtilService,
    private configService: ConfigService,
    private commonUsecase: CommonUsecase,
    private bankService: BankService
  ) {
    super(auth);
  }

  async ngOnInit() {
    await this.commonUsecase.checkAuthorization().then(() => this.navigateAfterAuthorized());
    this.auth.isAuthenticated$.subscribe((isAuthenticated) => {
      if (isAuthenticated) {
        this.bankService.initBanks().then();
        this.getWelcomeContents();
      }
    });
  }

  async navigateAfterAuthorized() {
    const ref = this.auth.getRef(true);
    const defaultRef = 'dashboard';
    await this.router.navigateByUrl(ref || `/${defaultRef}`);
  }

  getWelcomeContents() {
    if (this.cms.bannersLoaded) {
      let result;
      if (this.configService.getConfig().production) {
        result = this.cms.getWelcomeContentsProd();
      } else {
        result = this.cms.getWelcomeContentsDev();
      }
      this.handleAndOpenAnnounceContent(result);
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        let result;
        if (this.configService.getConfig().production) {
          result = this.cms.getWelcomeContentsProd();
        } else {
          result = this.cms.getWelcomeContentsDev();
        }
        this.handleAndOpenAnnounceContent(result);
      });
    }
  }

  handleAndOpenAnnounceContent(result) {
    const auth = this.auth.snapshot;
    if (!result) {
      this.openGuideModal();
      return;
    }
    if (!(auth && auth.user && auth.user.id)) {
      return;
    }
    if (this.announcement_modal) {
      return;
    }
    const content = this.util.keepHtmlCssStyle(
      StringHandler.trimNewLineCharacters(result.description.split(/content##<\/.>/)[1])
    );
    const content_structure = StringHandler.trimNewLineCharacters(StringHandler.trimHtml(result.description));

    const announcement_content: any = {};
    for (let item of content_structure.split('####')) {
      const key = item.split('##')[0];
      const value = item.split('##')[1];
      if (key == 'content') {
        announcement_content[key] = content;
      } else {
        announcement_content[key] = value;
      }
    }
    const { title, handle, loop, day_from, day_to, template, link } = announcement_content;
    let displayed_count_saved = Number(localStorage.getItem(handle));
    const from = new Date(day_from).getTime();
    const to = new Date(day_to).getTime();
    const now = new Date().getTime();
    if (!(title && content && handle)) {
      this.openGuideModal();
      return;
    }
    if (now < from || now > to) {
      this.openGuideModal();
      return;
    }
    if (displayed_count_saved && displayed_count_saved >= Number(loop)) {
      this.openGuideModal();
      return;
    }

    this.announcement_modal = this.modalController.create({
      component: WelcomeModalComponent,
      showBackdrop: true,
      cssClass: 'modal-lg',
      componentProps: {
        announcement_content,
        result
      }
    });
    this.announcement_modal.show().then();
    this.announcement_modal.onDismiss().then(_ => {
      this.announcement_modal = null;
      if (displayed_count_saved) displayed_count_saved += 1;
      else displayed_count_saved = 1;
      localStorage.setItem(handle, displayed_count_saved.toString());
      debug.log(localStorage.getItem(handle));
    });
  }

  async openGuideModal() {
    let banner = await this.cms.banners[33];
    let count = 4;
    if (this.storage.get('GuideCount')) {
      count = this.storage.get('GuideCount');
    }
    if (count == 1) {
      return;
    }
    this.storage.set('GuideCount', count);
    if (banner && banner.is_active == '1') {
      let modal = this.modalController.create({
        component: GuideModalComponent,
        showBackdrop: true,
        cssClass: 'modal-lg',
        componentProps: {
          banner: banner
        }
      });
      modal.show().then();
      modal.onDismiss().then(async () => {
      });
    }
  }
}
