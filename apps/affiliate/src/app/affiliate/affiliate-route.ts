import { Routes } from '@angular/router';

export const affiliateRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'products',
    loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'transactions',
    loadChildren: () => import('../pages/money-transactions/money-transactions.module').then(m => m.MoneyTransactionsModule)
  },
  {
    path: 'cooperators',
    loadChildren: () => import('../pages/cooperators/cooperators.module').then(m => m.CooperatorsModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'support',
    loadChildren: () => import('../pages/support/support.module').then(m => m.SupportModule)
  },
  {
    path: 'ranking',
    loadChildren: () => import('../pages/ranking/ranking.module').then(m => m.RankingModule)
  }
];
