import { Component, OnInit } from '@angular/core';
import { StorageService } from 'apps/core/src/services/storage.service';

@Component({
  selector: 'aff-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit{
  constructor(
    private storage: StorageService
  ) {}

  ngOnInit(): void {
    let eAff = new URL(window.location.href).searchParams.get('e_aff');
    this.storage.set('e_aff', eAff || this.storage.get('e_aff'));
  }

}
