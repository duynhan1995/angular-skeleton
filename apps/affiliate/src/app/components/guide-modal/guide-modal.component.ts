import { Component, OnInit, Input } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { CmsService } from 'apps/core/src/services/cms.service';
import { StorageService } from 'apps/core/src/services/storage.service';

@Component({
  selector: 'aff-guide-modal',
  templateUrl: './guide-modal.component.html',
  styleUrls: ['./guide-modal.component.scss']
})
export class GuideModalComponent implements OnInit {
  @Input() banner;
  constructor(
    private modalDismiss: ModalAction,
    private storage: StorageService
  ) {}

  ngOnInit() {}

  close() {
    this.modalDismiss.dismiss(null);
    this.storage.set('GuideCount', this.storage.get('GuideCount') - 1);
  }

  gotoGuide(link) {
    window.open(link, '_blank');
  }
}
