import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'aff-guide-link',
  templateUrl: './guide-link.component.html',
  styleUrls: ['./guide-link.component.scss']
})
export class GuideLinkComponent implements OnInit {
  @Input() handle;
  banner;
  linksGuide = [];
  link: any;
  loading = false;
  constructor(private sanitizer: DomSanitizer, private util: UtilService) {}

  async ngOnInit() {
    await this.getBanner();
    let text = this.banner.description.split('###');
    text.map(d => {
      this.linksGuide.push({
        handle: this.removeNewLineCharacters(this.stripHTML(d.split('##')[0])),
        link: this.stripHTML(d.split('##')[2]),
        title: this.removeNewLineCharacters(this.stripHTML(d.split('##')[1]))
      });
    });
    if (this.handle) {
      this.link = this.linksGuide.find(l => l.handle === this.handle);
      if (this.link) {
        this.loading = true;
      }
    }

  }

  async getBanner() {
    try {
      const res = await fetch('https://cms-setting.etop.vn/api/get-banner').then(r =>
        r.json()
      );
      this.banner = res.data[32];
      return this.banner;
    } catch (e) {
      debug.log(e);
    }
  }

  private stripHTML(html: string) {
    const _tempDIV = document.createElement('div');
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || '';
  }

  private removeNewLineCharacters(str) {
    return str.replace(/\r?\n|\r/g, '');
  }

  gotoGuide(link) {
    window.open(link, '_blank');
  }
}
