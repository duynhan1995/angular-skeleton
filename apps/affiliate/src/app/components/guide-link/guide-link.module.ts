import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/shared/src/shared.module';
import { GuideLinkComponent } from './guide-link.component';


@NgModule({
  declarations: [GuideLinkComponent],
  imports: [CommonModule, SharedModule],
  exports: [GuideLinkComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuideLinkModule {}