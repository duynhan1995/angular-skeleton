import { Component, OnInit, Input } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'aff-Welcome-modal',
  templateUrl: './welcome-modal.component.html',
  styleUrls: ['./welcome-modal.component.scss']
})
export class WelcomeModalComponent implements OnInit {
  @Input() announcement_content: any;
  @Input() result;

  constructor(private modalDismiss: ModalAction) {}

  ngOnInit() {
  }

  dismissModal() {
    this.modalDismiss.dismiss(null);
  }

  gotoGuide(link) {
    window.open(link, '_blank');
  }
}
