import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { Route, Router } from '@angular/router';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'aff-invite-affiliate-modal',
  templateUrl: './invite-affiliate-modal.component.html',
  styleUrls: ['./invite-affiliate-modal.component.scss']
})
export class InviteAffiliateModalComponent implements OnInit {
  user: any;
  url;
  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private modalDismiss: ModalAction
  ) {}

  ngOnInit() {
    this.user = this.auth.snapshot.user;
    this.url = window.location.origin;
  }

  copyText(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    toastr.success('Sao chép thành công!');
  }

  sendMail(mail) {
    let email = '';
    let subject = 'Tham gia chương trình eTop Affiliate - Lời mời cộng tác viên';

    mail = document.createElement('a');
    mail.href = `mailto:${email}?subject=${subject}`;
    mail.click();
  }

  close() {
    this.modalDismiss.dismiss(null);
  }
}
