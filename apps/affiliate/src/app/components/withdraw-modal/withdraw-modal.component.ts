import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'aff-withdraw-modal',
  templateUrl: './withdraw-modal.component.html',
  styleUrls: ['./withdraw-modal.component.scss']
})
export class WithdrawModalComponent implements OnInit {

  amount: any;
  bankAccount: any;
  constructor(
    private auth: AuthenticateStore,
    private modalDismiss: ModalAction
  ) {}

  ngOnInit() {
    let session = this.auth.snapshot.session;
    this.bankAccount = session.affiliate.bank_account;
  }

  close() {
    this.modalDismiss.dismiss(null);
  }
}
