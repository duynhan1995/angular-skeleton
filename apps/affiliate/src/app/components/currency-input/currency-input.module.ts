import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyInputComponent } from './currency-input.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';

@NgModule({
  declarations: [CurrencyInputComponent],
  imports: [CommonModule, FormsModule, SharedModule],
  exports: [CurrencyInputComponent],
  providers: []
})
export class CurrencyInputModule {}
