import { Component, OnInit, forwardRef, ViewChild, Input, ElementRef, EventEmitter, Output, Injector, ChangeDetectorRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';
@Component({
  selector: 'aff-currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CurrencyInputComponent),
      multi: true
    }
  ]
})
export class CurrencyInputComponent implements OnInit {
  @ViewChild("input", { static: false }) input: ElementRef;

  @Input() disabledinput;
  @Input() color;
  @Input() isDiscount;
  @Input() fontWeight;
  @Input() align = "right";
  @Input() posfixText;
  @Input() min = 0;
  @Input() max = 999999999;
  @Input() placeholder = '';
  @Input() classInner = '';
  @Input() inputClass = '';
  @Input() errorCOD = false;

  @Output() focus = new EventEmitter();
  @Output() keydown = new EventEmitter();

  show_input = false;
  invalid_input = false;
  _value;

  constructor(
    private injector: Injector,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() { }

  showEdit(e) {
    this.show_input = true;
    this.focus.emit();
    this.changeDetector.detectChanges();
    if (this.input) {
      setTimeout(() => this.input.nativeElement.select());
    }
  }

  onUpdate(v) {
    let value;
    if (v === 0 || v) {
      if (v > this.max) {
        value = this.max;
      } else if (v < this.min) {
        value = this.min;
      } else {
        value = v;
      }
    } else {
      value = null;
    }
    const model = this.injector.get(NgModel);
    model.viewToModelUpdate(value);
    // this.writeValue(value);
  }

  onBlur() {
    this.show_input = false;
    if (this._value > 100 && this.posfixText == '%') {
      toastr.error('Giá trị giảm giá không hợp lệ!');
      this.invalid_input = true;
      return
    } else {
      this.errorCOD = false;
      this.invalid_input = false;
      this.onUpdate(this._value);
    }
  }

  writeValue(value) {
    this._value = value;
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  registerOnChange() { }
  registerOnTouched() { }
}
