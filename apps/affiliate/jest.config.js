module.exports = {
  name: 'affiliate',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/affiliate',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
