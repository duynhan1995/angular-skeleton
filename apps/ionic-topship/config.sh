#!/bin/bash

DIR=config.store
ENV=dev
TARGET=ios/App/App
case $1 in
  prod)
    ENV=prod
    ;;
  *)
    ;;
esac

case $2 in
  ios)
    TARGET=ios/App/App
    ;;
  android)
    TARGET=android/App
    ;;
  *)
    echo "Not supported: $2";
    exit 1
    ;;
esac
echo "sync $DIR/$ENV/$TARGET/ $TARGET/"
rsync -av $DIR/$ENV/$TARGET/ $TARGET/
