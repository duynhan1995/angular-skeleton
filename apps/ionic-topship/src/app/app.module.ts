import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { CoreModule } from './core/core.module';
import { SharedModule } from './features/shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopshipAppCommonUsecase } from 'apps/ionic-topship/src/app/usecases/topship-app-commom.usecase.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { IonicModule } from '@ionic/angular';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { NetworkStatusModule } from './components/network-status/network-status.module';
import { CONFIG_TOKEN } from '@etop/core/services/config.service';
import { AuthenticateModule } from '@etop/core';
import { FilterModalComponent } from './components/filter-modal/filter-modal.component';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { Badge } from '@ionic-native/badge/ngx';
import { TopshipAppModule } from './topship-app/topship-app.module';
import { SearchSuggestComponent } from './components/search-modal/search-suggest/search-suggest.component';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { LoadingViewModule } from './components/loading-view/loading-view.module';

const apis = [];

const services = [];

const providers = [
  { provide: CONFIG_TOKEN, useValue: environment },
  { provide: CommonUsecase, useClass: TopshipAppCommonUsecase },
  { provide: 'SERVICE_URL', useValue: environment.api_url },
  ...apis,
  ...services,
  Badge,
  OneSignal
];

// if (environment.production) {
//   providers.unshift({ provide: 'SERVICE_URL', useValue: 'https://etop.vn' });
// }

@NgModule({
  imports: [
    CoreModule,
    SharedModule,
    EtopPipesModule,
    AppRoutingModule,
    TopshipAppModule,
    FormsModule,
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios',
      scrollAssist: true,
    }),
    NetworkStatusModule,
    AuthenticateModule,
    ReactiveFormsModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    LoadingViewModule.forRoot()
  ],
  declarations: [
    AppComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    FilterModalComponent,
    SearchSuggestComponent,
  ],
  entryComponents: [
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    SearchSuggestComponent
  ],
  providers,
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
