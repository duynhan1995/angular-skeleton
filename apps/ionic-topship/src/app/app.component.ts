import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar as NgxStatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticateService, ConfigService } from '@etop/core';
import {LocationService} from '@etop/state/location';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { LoadingViewService } from './components/loading-view/loading-view.service';

const { StatusBar, Keyboard } = Plugins;

@Component({
  selector: 'etop-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: NgxStatusBar,
    private authenticateService: AuthenticateService,
    private locationService: LocationService,
    private commonUsecase: CommonUsecase,
    private loadingView: LoadingViewService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is('capacitor')) {
        if (this.platform.is('ios')) {
          StatusBar.setStyle({
            style: StatusBarStyle.Light
          });
        } else {
          StatusBar.setStyle({
            style: StatusBarStyle.Dark
          });
        }
      } else {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }

      this.authenticateService.hookDecorator();
      this.locationService.initLocations().then();
    });
    this.commonUsecase.checkAuthorization(true)
    .then(() => {
      setTimeout(() => this.loadingView.hideLoadingView(), 2000)
    });
  }

}
