import { Injectable } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ETopTradingApi } from '@etop/api';

@Injectable({
  providedIn: 'root'
})
export class ETopTradingService {
  constructor(private eTopTradingApi: ETopTradingApi, private util: UtilService) {}

  async loadTradingProducts(
    start?: number,
    perpage?: number,
    filters?: Array<any>
  ) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.eTopTradingApi.getTradingProducts({
      paging,
      filters
    });
  }

  async loadTradingOrders(
    start?: number,
    perpage?: number,
    filters?: Array<any>
  ) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.eTopTradingApi.getTradingOrders({
      paging,
      filters
    });
  }

  async getTradingProduct(id) {
    return await this.eTopTradingApi.getTradingProduct(id);
  }

  async getTradingOrder(id) {
    return await this.eTopTradingApi.getTradingOrder(id);
  }

  async createTradingOrder(body) {
    return await this.eTopTradingApi.createOrder(body);
  }

  async TradingPaymentOrder(order) {
    let payment = await this.eTopTradingApi.TradingPaymentOrder({
      order_id: order.id,
      desc: 'Thanh toán cho đơn hàng ' + order.code,
      return_url: window.location.origin + '/s/' + this.util.getSlug() + '/etop-trading/' + order.lines[0].product_id,
      amount: order.total_amount,
      payment_provider: 'vtpay'
    });
    location.href = payment.url;
  }

  async PaymentCheckReturnData(body) {
    return await this.eTopTradingApi.PaymentCheckReturnData(body);
  }

  async GetProductPromotion(body) {
    return await this.eTopTradingApi.GetProductPromotion(body);
  }

  async CheckReferralCodeValid(body) {
    return await this.eTopTradingApi.CheckReferralCodeValid(body);
  }

  async GetTradingProductPromotions(
    start?: number,
    perpage?: number,
    filters?: Array<any>
  ) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };

    return this.eTopTradingApi.GetTradingProductPromotions({
      paging,
      filters
    });
  }
}
