import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(private toastController: ToastController) {}

  async error(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'danger',
      mode: 'md',
      position: 'top'
    });
    toast.present();
  }

  async success(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'success',
      mode: 'md',
      position: 'top'
    });
    toast.present();
  }

  async warning(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'warning',
      mode: 'md',
      position: 'top'
    });
    toast.present();
  }

  async primary(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'primary',
      mode: 'md',
      position: 'top'
    });
    toast.present();
  }

  async netWorkDiscontect() {
    const toast = await this.toastController.create({
      message: 'Không có kết nối mạng! Vui lòng kiểm tra đường truyền.',
      color: 'danger',
      mode: 'md',
      position: 'bottom',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    toast.present();
  }
}
