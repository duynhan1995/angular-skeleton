
import { Injectable } from '@angular/core';
import { FulfillmentApi } from '@etop/api/shop/fulfillment.api';
import { Fulfillment } from 'libs/models/Fulfillment';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  constructor(private ffmApi: FulfillmentApi) {}

  async getShippingServices(body) {
    return this.ffmApi.getShipmentServices(body);
  }

  parseFulfillmentData(fulfillment) {
    return {
      basket_value: fulfillment.basket_value,
      chargeable_weight: fulfillment.chargeable_weight,
      include_insurance: fulfillment.include_insurance,
      insurance_value: fulfillment.insurance_value,
      from_province_code: fulfillment.pickup_address.province_code,
      from_district_code: fulfillment.pickup_address.district_code,
      from_ward_code: fulfillment.pickup_address.ward_code,
      to_province_code: fulfillment.shipping_address.province_code,
      to_district_code: fulfillment.shipping_address.district_code,
      to_ward_code: fulfillment.shipping_address.ward_code,
      total_cod_amount: fulfillment.total_cod_amount,
      connection_ids: [fulfillment.shipment.connection_id]
    };
  }

  shippingServiceMap(services: any, fulfillment) {
    const { shipping_service_name } = fulfillment;
    return services.filter(
      service =>
        service.name == shipping_service_name
    );
  }
}
