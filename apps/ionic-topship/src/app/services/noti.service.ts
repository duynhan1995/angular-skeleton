import { Injectable } from '@angular/core';
import { HttpService } from 'libs/common/services';
import { Noti } from 'libs/models/Noti';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

@Injectable()
export class NotiService {
  external_device_id: string = "";
  device_id: string = "";
  notifications: Array<Noti> = [];
  notiChange = new Subject();
  readAll = new Subject();

  constructor(
    private http: HttpService,
    private router: Router,
    private util: UtilService,
  ) {
    this._init();
  }

  private _init() {
  }

  createDevice() {
    const data = JSON.parse(localStorage.getItem("device_data"));
    return this.http.post('api/shop.Notification/CreateDevice', data).toPromise()
      .then(res => {
        debug.log("CREATED DEVICE", res);
        this.external_device_id = res.external_device_id;
        this.device_id = res.device_id;
      });
  }

  deleteDevice() {
    const { external_device_id, device_id } = this;
    return this.http.post('api/shop.Notification/DeleteDevice', { external_device_id, device_id }).toPromise();
  }

  getNotifications(token: string, start?: number, perpage?: number) {
    try {
      const paging = {
        offset: start || 0,
        limit: perpage || 10,
      };
      const header = this.http.createHeader(token);
      const options = this.http.createDefaultOption(header);
      return this.http.postWithOptions('api/shop.Notification/GetNotifications', { paging }, options).toPromise()
        .then(res => {
          if (start) {
            this.notifications = this.notifications.concat(res.notifications);
          } else {
            this.notifications = res.notifications;
          }
          this.notiChange.next();
          return  res.notifications
          ;
        });
    } catch(e) {
      debug.error(e);
    }
  }

  updateNotifications(ids: Array<string>, is_read: boolean) {
    if (!ids || !ids.length) {
      return;
    }
    return this.http.post('api/shop.Notification/UpdateNotifications', { ids, is_read }).toPromise()
      .then(res => res);
  }

  readNoti(noti_data) {
    if (noti_data && !this.util.isEmptyObject(noti_data)) {
      this.updateNotifications([noti_data['NotiID']], true);
    }
    const shop_id = noti_data['ShopID'];
    switch (noti_data.Entity) {
      case "fulfillment":
        this.router.navigateByUrl(`fulfillments/detail/${noti_data.EntityID}${shop_id ? `?shop_id=${shop_id}` : ''}`);
        break;
      case "money_transaction_shipping":
        this.router.navigateByUrl(`transactions/detail/${noti_data.EntityID}${shop_id ? `?shop_id=${shop_id}` : ''}`);
        break;
      case "popup":
        this.router.navigateByUrl('notifications');
        break;
      default:
        break;
    }
  }

}
