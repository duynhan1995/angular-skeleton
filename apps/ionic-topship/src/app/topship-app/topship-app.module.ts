import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TopshipAppRoutingModule } from 'apps/ionic-topship/src/app/topship-app/topship-app-routing.module';
import { TopshipAppComponent } from 'apps/ionic-topship/src/app/topship-app/topship-app.component';
import { TopshipAppGuard } from 'apps/ionic-topship/src/app/topship-app/topship-app.guard';
import { IonicModule } from '@ionic/angular';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { AnnouncementPopupComponent } from '../components/popovers/announcement-popup/announcement-popup.component';

const pages = [];

@NgModule({
  declarations: [TopshipAppComponent,AnnouncementPopupComponent, ...pages],
  providers: [TopshipAppGuard, Deeplinks],
  imports: [
    CommonModule,
    IonicModule,
    EtopPipesModule,
    NgbModule,
    TopshipAppRoutingModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TopshipAppModule {}
