import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TopshipAppComponent } from 'apps/ionic-topship/src/app/topship-app/topship-app.component';
import { TopshipAppGuard } from './topship-app.guard';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabNavModule } from 'apps/ionic-topship/src/app/components/tab-nav/tab-nav.module';
import { TopshipAppRoutes } from './topship-app.route';

const routes: Routes = [
  {
    path: 's/:shop_index',
    canActivate: [TopshipAppGuard],
    resolve: {
      account: TopshipAppGuard
    },
    component: TopshipAppComponent,
    children: TopshipAppRoutes
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabNavModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule]
})
export class TopshipAppRoutingModule {}
