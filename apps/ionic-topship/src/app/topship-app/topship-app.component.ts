import { Component, OnDestroy, OnInit } from '@angular/core';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { IonRouterOutlet, NavController, PopoverController } from '@ionic/angular';
import {Router} from "@angular/router";
import { CmsService } from 'apps/core/src/services/cms.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { AnnouncementPopupComponent } from '../components/popovers/announcement-popup/announcement-popup.component';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'etop-etop-app',
  templateUrl: './topship-app.component.html',
  styleUrls: ['./topship-app.component.scss']
})
export class TopshipAppComponent extends BaseComponent implements OnInit {
  withoutFooterTabsUrls = [
    'confirm-order'
  ];
  constructor(
    private router: Router,
    private deeplinks: Deeplinks,
    private navCtrl: NavController,
    private cms: CmsService,
    private util: UtilService,
    private routerOutlet: IonRouterOutlet,
    private popoverController: PopoverController
  ) {
    super()
  }

  get hideFooterTabs() {
    const path = this.router.url.split('/').pop();
    return this.withoutFooterTabsUrls.includes(path);
  }

  async ngOnInit() {
    this.getAnnouncementContent()
    this.deeplinks.route({
      '/invitation': ''
    }).subscribe(match => {
      // match.$route - the route we matched, which is the matched entry from the arguments to route()
      // match.$args - the args passed in the link
      // match.$link - the full link data
      if (match.$link.path === '/invitation') {
        this.navCtrl.navigateForward(`invitation?${match.$link.queryString}`, {
          animated: false
        });
      }
    }, nomatch => {
      // nomatch.$link - the full link data
      debug.error('Got a deeplink that didn\'t match', nomatch);
    });
  }


  getAnnouncementContent() {
      this.cms.onBannersLoaded.pipe(takeUntil(this.destroy$))
        .subscribe(_ => {
        const result = this.cms.getAnnouncementIonicTopship();
        if (result) {
          this.handleAndOpenAnnounceContent(result)
        }
      });
  }

  async handleAndOpenAnnounceContent(result) {
    if(localStorage.getItem('isOpeningAnnouncementModal')=="true") {return;}
    localStorage.setItem("isOpeningAnnouncementModal", "true");
    const content = this.util.keepHtmlCssStyle(
      this.util.removeNewLineCharacters(result.split(/content##<\/.>/)[1])
    );
    const content_structure = this.util.removeNewLineCharacters(this.util.stripHTML(result));
    const announcement_content: any = {};
    for (let item of content_structure.split('####')) {
      const key = item.split('##')[0];
      const value = item.split('##')[1];
      if (key == 'content') {
        announcement_content[key] = content;
      } else {
        announcement_content[key] = value;
      }
    }

    const { title, handle, loop, day_from, day_to, image_only } = announcement_content;
    let displayed_count_saved = Number(localStorage.getItem(handle));
    const from = new Date(day_from).getTime();
    const to = new Date(day_to).getTime();
    const now = new Date().getTime();
    if((!(title && content && handle))  || (displayed_count_saved && displayed_count_saved >= Number(loop)) || (now < from || now > to)) {
        return localStorage.setItem("isOpeningAnnouncementModal", "false");
    }
    const modal = await this.popoverController.create({
      component: AnnouncementPopupComponent,
      componentProps: {
        announcement_content
      },
      translucent: true,
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    modal.present().then();
    modal.onDidDismiss().then(() => {
      if (displayed_count_saved) displayed_count_saved += 1;
      else displayed_count_saved = 1;
      localStorage.setItem(handle, displayed_count_saved.toString());
      localStorage.setItem("isOpeningAnnouncementModal", "false");
    });
  }
}
