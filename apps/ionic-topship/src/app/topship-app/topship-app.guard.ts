import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticateStore } from '@etop/core';
@Injectable()
export class TopshipAppGuard implements CanActivate, Resolve<Promise<any>> {
  constructor(private auth: AuthenticateStore, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (!this.auth.snapshot.account) {
      this.router.navigate(['/login']);
      return false;
    }
    return !!this.auth.snapshot.account;
  }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | Observable<Promise<any>> | Promise<Promise<any>> {
    return new Promise((res, rej) => {
      const params = route.params;
      const index = params['shop_index'];
      const result = this.auth.selectAccount(index);
      if (!result) {
        const frags = state.url.split('/');
        frags.splice(
          2,
          1,
          this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex.toString()
        );
        const nextUrl = frags.join('/');
        this.router.navigateByUrl(nextUrl);
        return res(false);
      }
      return res(result);
    });
  }
}
