import { Routes } from '@angular/router';
import { tabNavRoutes } from 'apps/ionic-topship/src/app/components/tab-nav/tab-nav.module';

export const TopshipAppRoutes: Routes = [
  ...tabNavRoutes,
  {
    path: 'products',
    loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'tickets',
    loadChildren: () => import('../pages/tickets/tickets.module').then(m => m.TicketsModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('../pages/customers/customers.module').then(m => m.CustomersModule)
  },
  {
    path: 'account',
    loadChildren: () => import('../pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'developing',
    loadChildren: () => import('../pages/developing-view/developing-view.module').then(m => m.DevelopingViewModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('../pages/notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: '**',
    redirectTo: 'tab-nav'
  }
];
