import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonSkeletonComponent } from './ion-skeleton.component';



@NgModule({
  declarations: [IonSkeletonComponent],
  providers: [],
  imports: [CommonModule, IonicModule],
  exports: [IonSkeletonComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class IonSkeletonModule {}
