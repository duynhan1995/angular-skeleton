import { Component, OnInit, Input } from '@angular/core';
import { IonDatetime, ModalController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { FilterOptions } from '@etop/models';

@Component({
  selector: 'ionfabo-filter-modal',
  templateUrl: './filter-modal.component.html',
  styleUrls: ['./filter-modal.component.scss']
})
export class FilterModalComponent implements OnInit {
  @Input() filters: FilterOptions;
  filterForm = this.fb.array([])
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }

  ionViewWillEnter() {
    this.filterForm.clear();
    this.filters.forEach(filter => {
      filter.options = [filter.options] as any;
      this.filterForm.push(this.fb.group(filter));
    })
  }

  back() {
    this.modalCtrl.dismiss();
  }

  submit() {
    this.modalCtrl.dismiss({ reset: false, filters: this.filterForm.value});
  }

  removeFilter() {
    this.modalCtrl.dismiss({ reset: true, filters: this.filterForm.value });
  }

}
