import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Plugins, NetworkStatus } from '@capacitor/core';

const { Network } = Plugins;

@Component({
  selector: 'etop-network-status',
  templateUrl: './network-status.component.html',
  styleUrls: ['./network-status.component.scss']
})
export class NetworkStatusComponent implements OnInit {
  networkStatus = true;
  status: NetworkStatus;
  constructor(private zone: NgZone) {
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        this.networkStatus = false;
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          setTimeout(() => {
            this.networkStatus = true;
          }, 1000);
        });
      } else {
        this.zone.run(async () => {
          this.networkStatus = false;
        });
      }
    });
  }

  ngOnInit() {}

  async getStatus() {
    return await Network.getStatus();
  }
}
