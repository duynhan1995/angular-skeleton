import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs, NavController } from '@ionic/angular';
import { UtilService } from '../../../../../core/src/services/util.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'ionfabo-tab-nav',
  templateUrl: './tab-nav.component.html',
  styleUrls: ['./tab-nav.component.scss']
})
export class TabNavComponent implements OnInit {
  @ViewChild(IonTabs) tabs: IonTabs;
  tabsList = [
    {
      link: `orders`,
      icon: 'cube',
      label: 'Đơn hàng',
      permissions: ['shop/fulfillment:view']
    },
    {
      link: `transaction`,
      icon: 'cash',
      label: 'Đối soát',
      permissions: ['shop/money_transaction:view']
    },
    {
      link: `pos`,
      icon: 'add-circle',
      label: 'Tạo đơn',
      permissions: ['shop/fulfillment:create']
    },
    {
      link: `notifications`,
      icon: 'notifications',
      label: 'Thông báo'
    },
    {
      link: `account`,
      icon: 'apps',
      label: 'Khác'
    }
  ];

  currentTab = '';
  constructor(
    private changeDetector: ChangeDetectorRef,
    private navCtrl: NavController,
    private util: UtilService,
    private router: Router) {}

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentTab = event.url.split('/')[4];
      }
    });
  }

  setTab(tab) {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/tab-nav/${tab.link}`)
    this.currentTab = this.tabs.getSelected();
    this.changeDetector.detectChanges();
  }
}
