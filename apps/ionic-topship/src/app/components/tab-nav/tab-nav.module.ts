import { NotificationsComponent } from './../../pages/notifications/notifications.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabNavComponent } from './tab-nav.component';
import { Routes } from '@angular/router';
import { OrderListModule } from 'apps/ionic-topship/src/app/pages/orders/order-list/order-list.module';
import { OrderListComponent } from 'apps/ionic-topship/src/app/pages/orders/order-list/order-list.component';
import { ListCustomerModule } from 'apps/ionic-topship/src/app/pages/customers/list-customer/list-customer.module';
import { ProductListModule } from 'apps/ionic-topship/src/app/pages/products/product-list/product-list.module';
import { AccountMenuComponent } from 'apps/ionic-topship/src/app/pages/account/account-menu/account-menu.component';
import { AccountMenuModule } from 'apps/ionic-topship/src/app/pages/account/account-menu/account-menu.module';
import { LoadingViewModule } from '../loading-view/loading-view.module';
import { DevelopingViewModule } from '../../pages/developing-view/developing-view.module';
import { NotificationsModule } from '../../pages/notifications/notifications.module';
import { PosModule } from '../../pages/pos/pos.module';
import { AuthenticateModule } from '@etop/core';

export const tabNavRoutes: Routes = [
  {
    path: 'tab-nav',
    component: TabNavComponent,
    children: [
      {
        path: '',
        redirectTo: 'orders',
        pathMatch: 'full'
      },
      {
        path: 'transaction',
        loadChildren: () => import('../../pages/transaction/transaction.module').then(m => m.TransactionModule)
      },
      {
        path: 'orders',
        component: OrderListComponent,
      },
      {
        path: 'notifications',
        component: NotificationsComponent
      },
      {
        path: 'account',
        component: AccountMenuComponent
      },
      {
        path: 'pos',
        loadChildren: () => import('../../pages/pos/pos.module').then(m => m.PosModule)
      },
    ]
  }
];

@NgModule({
  declarations: [TabNavComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderListModule,
    ListCustomerModule,
    ProductListModule,
    AccountMenuModule,
    LoadingViewModule,
    NotificationsModule,
    DevelopingViewModule,
    PosModule,
    AuthenticateModule
  ],
  exports: [TabNavComponent, OrderListModule, ListCustomerModule, ProductListModule, AccountMenuModule, NotificationsModule, PosModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TabNavModule {
}
