import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateProductComponent } from 'apps/ionic-topship/src/app/components/create-product/create-product.component';
import { FormsModule } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { IonicModule } from '@ionic/angular';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';

@NgModule({
  declarations: [CreateProductComponent],
  entryComponents: [CreateProductComponent],
  imports: [CommonModule, FormsModule, IonicModule, EtopPipesModule],
  providers: [BarcodeScanner]
})
export class CreateProductModule {}
