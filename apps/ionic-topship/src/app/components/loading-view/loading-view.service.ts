import { Injectable } from '@angular/core';
import { LoadingViewComponent } from './loading-view.component';

@Injectable()
export class LoadingViewService {
  private loadingView: LoadingViewComponent;

  constructor() {
  }

  setLoadingView(view: LoadingViewComponent) {
    this.loadingView = view;
  }

  showLoadingView() {
    if (this.loadingView) {
      this.loadingView.show = true;
    }
  }

  hideLoadingView() {
    if (this.loadingView) {
      this.loadingView.show = false;
    }
  }
}
