import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { UserApi } from '@etop/api';
import { ToastService } from '../../services/toast.service';
import { FormBuilder, Validators } from '@angular/forms';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'etop-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordData: any = {};
  errorMessage = '';
  passwordForm = this.fb.group({
    current_password: ['', Validators.required],
    new_password: ['', Validators.required],
    confirm_password: ['', Validators.required]
  });
  constructor(
    private modalCtrl: ModalController,
    private auth: AuthenticateStore,
    private userService: UserApi,
    private toast: ToastService,
    private fb: FormBuilder,
    private loadingService: LoadingService
  ) {}

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async submit() {
    const data = this.passwordForm.value;
    const { current_password, new_password, confirm_password } = data;
    if (this.passwordForm.invalid) {
      return this.toast.error('Vui lòng nhập đầy đủ thông tin');
    }
    if (new_password !== confirm_password) {
      return this.toast.error('Mật khẩu nhập lại không chính xác.');
    }
    try {
      await this.loadingService.start('Đang cập nhật');
      const body = {
        login: this.auth.snapshot.user.email || this.auth.snapshot.user.phone,
        current_password,
        new_password,
        confirm_password
      };
      await this.userService.changePassword(body);
      setTimeout(async() => {
        await this.loadingService.end();
        this.toast.success('Thay đổi mật khẩu thành công.');
        this.dismiss();
      }, 1000)
    } catch (e) {
      this.loadingService.end();
      this.toast.error(e.message);
    }
  }
}
