import {NgModule} from "@angular/core";
import {EmptyComponent} from "apps/ionic-topship/src/app/components/empty/empty.component";
import {CommonModule} from "@angular/common";
import {IonicModule} from "@ionic/angular";

@NgModule({
  declarations: [
    EmptyComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    EmptyComponent
  ]
})

export class EmptyModule {}
