import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'etop-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.scss']
})
export class EmptyComponent implements OnInit {
  @Input() empty: {
    title: string;
    subtitle: string;
    action?: string;
    buttonClick: () => void;
  };

  constructor() { }

  ngOnInit() {
  }

  handleClick() {
    if (this.empty.buttonClick) {
      return this.empty.buttonClick();
    }
    return;
  }

}
