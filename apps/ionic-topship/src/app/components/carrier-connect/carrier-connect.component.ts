import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Connection} from 'libs/models/Connection';
import {ConnectionAPI} from '@etop/api';
import {ToastService} from 'apps/ionic-topship/src/app/services/toast.service';
import {ConnectionService} from '@etop/features/connection/connection.service';

enum Step {
  carriersList = 'carriersList',
  carrierLogin = 'carrierLogin',
  carrierOTP = 'carrierOTP'
}

@Component({
  selector: 'etop-carrier-connect',
  templateUrl: './carrier-connect.component.html',
  styleUrls: ['./carrier-connect.component.scss']
})
export class CarrierConnectComponent implements OnInit {
  @Input() step: Step = Step.carriersList;
  @Input() selectedCarrierConnection: Connection;
  @Input() loginInfo = new ConnectionAPI.LoginShopConnectionRequest();
  // tslint:disable-next-line: new-parens
  loginOTP = new ConnectionAPI.LoginShopConnectionWithOTPRequest();

  constructor(
    private toast: ToastService,
    private connectionService: ConnectionService,
    private modalCtrl: ModalController
  ) {}

  get popoverTitle() {
    return this.step == Step.carriersList ? 'Chọn nhà vận chuyển' : 'Kết nối';
  }

  get firstStep() {
    return this.step == Step.carriersList;
  }

  get secondStep() {
    return this.step == Step.carrierLogin;
  }

  get thirdStep() {
    return this.step == Step.carrierOTP;
  }

  ngOnInit() {}

  dismiss() {
    if (this.secondStep) {
      this.step = Step.carriersList;
    } else {
      this.modalCtrl.dismiss().then();
    }
  }

  back() {
    this.step = Step.carrierLogin;
  }

  carrierLogin(selectedCarrierConnection: Connection) {
    this.step = Step.carrierLogin;
    this.selectedCarrierConnection = selectedCarrierConnection;
  }

  loginInfoChanged(loginInfo: ConnectionAPI.LoginShopConnectionRequest) {
    this.loginInfo = loginInfo;
  }

  loginOTPChanged(loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest) {
    this.loginOTP = loginOTP;
  }

  async continue() {
    try {
      if (!this.loginInfo.identifier) {
        return this.toast.error('Vui lòng nhập số điện thoại.');
      }
      this.loginInfo.connection_id = this.selectedCarrierConnection.id;
      await this.connectionService.loginConnection(this.loginInfo);
      this.step = Step.carrierOTP;
    } catch (e) {
      this.toast.error(e.message);
    }
  }

  async retryOTP(e) {
    try {
      this.loginInfo.connection_id = this.selectedCarrierConnection.id;
      await this.connectionService.loginConnection(this.loginInfo);
    } catch (e) {
      this.toast.error(e.message);
    }
  }

  confirm() {
    if (!this.loginOTP.otp) {
      return this.toast.error('Vui lòng nhập mã xác thực.');
    }
    this.modalCtrl
      .dismiss({
        loginOTP: {
          ...this.loginOTP,
          connection_id: this.selectedCarrierConnection.id
        },
        step: this.step,
        selectedCarrierConnection: this.selectedCarrierConnection
      })
      .then();
  }
}
