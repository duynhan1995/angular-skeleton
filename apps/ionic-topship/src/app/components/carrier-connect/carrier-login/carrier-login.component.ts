import {Component, Input, OnInit, Output} from '@angular/core';
import {Connection} from "libs/models/Connection";
import {ConnectionAPI} from "@etop/api";
import {Subject} from "rxjs";

@Component({
  selector: 'etop-carrier-login',
  templateUrl: './carrier-login.component.html',
  styleUrls: ['./carrier-login.component.scss']
})
export class CarrierLoginComponent implements OnInit {
  @Output() onLoginInfoChanged = new Subject<ConnectionAPI.LoginShopConnectionRequest>()
  @Input() carrierConnection: Connection;

  loginInfo = new ConnectionAPI.LoginShopConnectionRequest();

  constructor() { }

  ngOnInit() {
  }

  loginInfoChanged() {
    this.onLoginInfoChanged.next(this.loginInfo);
  }

}
