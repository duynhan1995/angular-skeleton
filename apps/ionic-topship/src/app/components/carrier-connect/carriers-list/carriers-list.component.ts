import {Component, OnInit, Output} from '@angular/core';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {map} from "rxjs/operators";
import {Connection} from "libs/models/Connection";
import {Subject} from "rxjs";

@Component({
  selector: 'etop-carriers-list',
  templateUrl: './carriers-list.component.html',
  styleUrls: ['./carriers-list.component.scss']
})
export class CarriersListComponent implements OnInit {
  @Output() onConnectCarrier = new Subject<Connection>();
  availableConnections$ = this.connectionStore.state$.pipe(map(s => s.availableConnections));

  inDevelopmentConnections = [
    {
      provider_logo: 'assets/images/provider_logos/ghtk-s.png',
      name_display: 'Giao Hàng Tiết Kiệm'
    },
    {
      provider_logo: 'assets/images/provider_logos/vtpost-s.png',
      name_display: 'Viettel Post'
    },
    {
      provider_logo: 'assets/images/provider_logos/vnpost-s.png',
      name_display: 'Vietnam Post'
    },
    {
      provider_logo: 'assets/images/provider_logos/topship-s.png',
      name_display: 'Topship'
    }
  ]

  constructor(
    private connectionStore: ConnectionStore
  ) { }

  ngOnInit() {
  }

  async connectCarrier(carrierConnection: Connection) {
    this.onConnectCarrier.next(carrierConnection);
  }

}
