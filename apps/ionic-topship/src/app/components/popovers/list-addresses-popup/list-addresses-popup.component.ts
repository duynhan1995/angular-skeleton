import {Component, Input, OnInit} from '@angular/core';
import {Address} from "libs/models/Address";
import {PopoverController} from "@ionic/angular";
import {ConfirmOrderStore} from "@etop/features/fabo/confirm-order/confirm-order.store";

@Component({
  selector: 'etop-list-addresses-popup',
  templateUrl: './list-addresses-popup.component.html',
  styleUrls: ['./list-addresses-popup.component.scss']
})
export class ListAddressesPopupComponent implements OnInit {
  @Input() type: 'from' | 'to' = 'from';
  @Input() addresses: Address[];

  constructor(
    private popoverController: PopoverController,
    private confirmOrderStore: ConfirmOrderStore
  ) { }

  get typeDisplay() {
    return this.type == 'from' ? 'lấy' : 'giao';
  }

  get popoverTitle() {
    return `Chọn địa chỉ ${this.typeDisplay} hàng`;
  }

  get emptyTitle() {
    return `Chưa có địa chỉ ${this.typeDisplay} hàng`;
  }

  isSelectedAddress(address: Address) {
    let selectedAddress = this.type == 'from' ?
      this.confirmOrderStore.snapshot.activeFromAddress :
      this.confirmOrderStore.snapshot.activeToAddress;

    return selectedAddress.id == address.id;
  }

  ngOnInit() {}

  dismiss() {
    this.popoverController.dismiss({closed: true}).then();
  }

  selectAddress(address: Address) {
    this.popoverController.dismiss({address}).then();
  }

  newAddress() {
    this.popoverController.dismiss({createAddress: true}).then();
  }

}
