import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'topship-announcement-popup',
  templateUrl: './announcement-popup.component.html',
  styleUrls: ['./announcement-popup.component.scss']
})
export class AnnouncementPopupComponent implements OnInit {
  @Input() announcement_content: any;
  constructor(
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit(): void {
  }

  dismiss() {
    this.popoverCtrl.dismiss(null);
  }
}
