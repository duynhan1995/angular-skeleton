import {Component, Input, OnInit} from '@angular/core';
import {Address} from 'libs/models/Address';
import {BaseComponent} from '@etop/core';
import {District, Province, Ward} from 'libs/models/Location';
import {ToastService} from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ModalController } from '@ionic/angular';
import {LocationQuery, LocationService} from '@etop/state/location';

@Component({
  selector: 'etop-create-update-address-popup',
  templateUrl: './create-update-address-popup.component.html',
  styleUrls: ['./create-update-address-popup.component.scss']
})
export class CreateUpdateAddressPopupComponent extends BaseComponent implements OnInit {
  @Input() type: 'shipfrom' | 'to' = 'shipfrom';
  @Input() address = new Address({});

  provincesList: Province[] = [];
  districtsList: District[] = [{ id: null, code: null, name: 'Chưa chọn tỉnh thành' }];
  wardsList: Ward[] = [{ id: null, code: null, name: 'Chưa chọn quận huyện' }];

  constructor(
    private toast: ToastService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private modalCtrl: ModalController
  ) {
    super();
  }

  get typeDisplay() {
    return this.type == 'shipfrom' ? 'lấy' : 'giao';
  }

  get popoverTitle() {
    return this.address?.id && `Cập nhật địa chỉ ${this.typeDisplay} hàng` || `Tạo địa chỉ ${this.typeDisplay} hàng`;
  }

  get confirmBtnTitle() {
    return this.address?.id && `Cập nhật địa chỉ` || `Tạo địa chỉ`;
  }

  ngOnInit() {
    this.provincesList = this.locationQuery.getValue().provincesList;
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
  }

  dismiss() {
    this.modalCtrl.dismiss({closed: true}).then();
  }

  onProvinceSelected() {
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.address.province = this.locationQuery.getProvince(this.address.province_code)?.name;
    this.address.district_code = '';
    this.address.ward_code = '';
  }

  onDistrictSelected() {
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.address.district = this.locationQuery.getDistrict(this.address.district_code)?.name;
    this.address.ward_code = '';
  }

  onWardSelected() {
    this.address.ward = this.locationQuery.getWard(this.address.ward_code)?.name;
  }

  confirm() {
    const {full_name, phone, province_code, district_code, ward_code, address1} = this.address;
    if (!full_name) {
      return this.toast.error('Chưa nhập tên');
    }
    if (!phone) {
      return this.toast.error('Chưa nhập số điện thoại');
    }
    if (!province_code) {
      return this.toast.error('Chưa nhập tỉnh thành');
    }
    if (!district_code) {
      return this.toast.error('Chưa nhập quận huyện');
    }
    if (!ward_code) {
      return this.toast.error('Chưa nhập phường xã');
    }
    if (!address1) {
      return this.toast.error('Chưa nhập địa chỉ cụ thể');
    }

    this.modalCtrl.dismiss({address: this.address}).then();
  }

}
