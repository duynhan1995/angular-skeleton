import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkDisconnectComponent } from './network-disconnect.component';



@NgModule({
  declarations: [NetworkDisconnectComponent],
  entryComponents: [],
  imports: [CommonModule, FormsModule, IonicModule],
  exports: [NetworkDisconnectComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NetworkDisconnectModule {}
