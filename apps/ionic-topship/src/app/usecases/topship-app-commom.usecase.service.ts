import { Injectable } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AuthenticateStore } from '@etop/core';
import { Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { Account } from 'libs/models/Account';
import { UserApi } from '@etop/api';
import { ToastService } from '../services/toast.service';

@Injectable()
export class TopshipAppCommonUsecase extends CommonUsecase {
  app = 'ionic-topship';
  full_name;
  password;
  email;
  confirm;
  error = false;

  signupData: any = {};

  loading = false;

  provinces;

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore,
    private toastService: ToastService
  ) {
    super();
  }

  async checkAuthorization(fetchAccount = false) {
    debug.log('checkAuthorization')
    const ref = this.router.url.split('?')[0];
    try {
      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(fetchAccount);
      const pathname = this.router.url.split('?')[0];
      if (pathname.startsWith('/s/')) {
        const index = pathname.split('/')[2];
        this.auth.selectAccount(index);
      } else {
        this.auth.selectAccount(0);
      }
      await this.navigateAfterAuthorized();
    } catch (e) {
      this.auth.clear();
      this.auth.setRef(ref);
    }
  }

  async navigateAfterAuthorized() {
    await this.router.navigateByUrl(
      `/s/${this.auth.snapshot.account.url_slug || 0}/tab-nav`
    );
  }

  async login(data: { login: string; password: string } ) {
    try {
      const res = await this.userApi.login({
        login: data.login,
        password: data.password,
        account_type: 'shop'
      });
      this.auth.updateToken(res.access_token);
      this.auth.updateUser(res.user);
      if (res.shop) {
        await this.setupAndRedirect();
      } else {
        await this.userService.checkToken(this.auth.snapshot.token);
      }
    } catch (e) {
      return this.toastService.error(e.message)
    }
  }

  async setupAndRedirect() {
    await this.updateSessionInfo(true);
    this.auth.selectAccount(0);
    const ref = this.auth.getRef(true);
    debug.log('ref', ref);
    await this.router.navigateByUrl(
      `/s/${this.auth.snapshot.account.url_slug || 0}/${ref || 'tab-nav'}`
    );
  }

  async updateSessionInfo(fetchAccounts = false) {
    const res = await this.userService.checkToken(this.auth.snapshot.token);
    let { access_token, account, shop, user, available_accounts } = res;
    const shop_accounts = available_accounts
      .filter(a => a.type === 'shop')
      .sort((a, b) => a.id > b.id);

    let no_init_shop = true;

    if (shop) {
      no_init_shop = false;
    }
    if (!shop && available_accounts && available_accounts.length) {
      shop = shop_accounts[0];
      account = shop_accounts[0];
    }
    const accounts: Account[] = fetchAccounts
      ? await Promise.all(
          shop_accounts.map(async (a, index) => {
            const accRes = await this.userService.switchAccount(a.id);
            a.token = accRes.access_token;
            a.shop = accRes.shop;
            a.id = accRes.shop && accRes.shop.id;
            a.image_url = a.shop.image_url;
            a.display_name = `${a.shop.code} - ${a.shop.name}`;
            a.permission = accRes.account.user_account.permission;
            return new Account(a);
          })
        )
      : this.auth.snapshot.accounts;

    if (accounts.length > 0) {
      this.auth.updateInfo({
        token: (no_init_shop && accounts[0].token) || access_token,
        account: {
          ...account,
          ...shop,
          display_name: `${shop.code} - ${shop.name}`
        },
        accounts,
        shop,
        user,
        permission: account.user_account.permission,
        isAuthenticated: true
      });
    }
  }

  async register(data: any, source) {
    const res = await this.userService.signUpUsingToken({ ...data, source });
    this.signupData = Object.assign({}, data, res.user);
    await this.login(
      {
        login: this.signupData.email,
        password: this.signupData.password
      }
    );
  }

  async redirectIfAuthenticated(): Promise<any> {
    return this.checkAuthorization();
  }
}
