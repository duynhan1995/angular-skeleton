import { AStore } from 'apps/core/src/interfaces/AStore';
import { Injectable } from '@angular/core';

export interface CustomerData {
  customers: any;
  filters: any;
}

@Injectable()
export class CustomerStore extends AStore<CustomerData> {
  initState: CustomerData = {
    customers: null,
    filters: null
  };
  constructor() {
    super();
  }

  setCustomers(customers) {
    this.setState({ customers });
  }

  setFilters(filters) {
    this.setState({ filters });
  }
}