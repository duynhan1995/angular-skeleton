import { NgModule } from '@angular/core';
import { ListCustomerComponent } from 'apps/ionic-topship/src/app/pages/customers/list-customer/list-customer.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/ionic-topship/src/app/features/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkStatusModule } from 'apps/ionic-topship/src/app/components/network-status/network-status.module';
import { NetworkDisconnectModule } from 'apps/ionic-topship/src/app/components/network-disconnect/network-disconnect.module';
import { AuthenticateModule } from '@etop/core';
import { EtopPipesModule } from '@etop/shared';
import { CustomerService, FaboCustomerService, ImageLoaderModule } from '@etop/features';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    NetworkStatusModule,
    NetworkDisconnectModule,
    AuthenticateModule,
    EtopPipesModule,
    ReactiveFormsModule,
    ImageLoaderModule
  ],
  declarations: [ListCustomerComponent],
  exports: [ListCustomerComponent],
  providers: [FaboCustomerService, CustomerService]
})
export class ListCustomerModule {}
