import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { NetworkStatus, Plugins } from '@capacitor/core';
import { IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { Customer } from 'libs/models/Customer';
import { UtilService } from 'apps/core/src/services/util.service';
import { FilterModalComponent } from '../../../components/filter-modal/filter-modal.component';
import { CustomersQuery } from '@etop/state/fabo/customer/customer.query';
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { FilterOperator, FilterOptions, Filters } from '@etop/models';

const { Network } = Plugins;

@Component({
  selector: 'etop-list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.scss']
})
export class ListCustomerComponent implements OnInit {
  customers: Customer[] = [];
  customersPaging$ = this.customersQuery.selectCustomerByPagination()
  customer$ = this.customersQuery.select(s => s.selected_list_customers)
  loading = true;
  status: NetworkStatus;
  viewDisconnect = false;
  filters: Filters = [];
  page = 1;
  perpage = 20;
  offset = 0;
  filterOptions: FilterOptions = [
    {
      name: 'full_name',
      operator: FilterOperator.contains,
      value: '',
      type: 'input',
      label: 'Tên',
      placeholder: 'Nhập tên khách hàng'
    },
    {
      name: 'phone',
      operator: FilterOperator.contains,
      value: '',
      type: 'input',
      label: 'Số điện thoại',
      placeholder: 'Nhập số điện thoại'
    }
  ];
  constructor(
    private customerService: CustomerService,
    private customersQuery: CustomersQuery,
    private zone: NgZone,
    private navCtrl: NavController,
    private util: UtilService,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private changeDetector: ChangeDetectorRef
  ) {
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        this.viewDisconnect = true;
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getCustomers();
        });
      } else {
        this.zone.run(async () => {
          if (!this.customers) {
            this.viewDisconnect = true;
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.customersPaging$.subscribe(customers => {
      if (customers.length){
        this.customerService.setSelectedListCustomer(customers)
      }
    })
    await this.customerService.setPaging(0, this.perpage);
    if (!this.customersQuery.getValue().selected_list_customers) {
      this.customerService.fetchCustomers();
    }
  }

  async getStatus() {
    return await Network.getStatus();
  }

  async doRefresh(event) {
    this.customerService.setLastCustomer(false);
    await this.customerService.setPaging(0, this.perpage);
    event.target.complete();
  }

  async resetFilters() {
    this.filterOptions = this.filterOptions.map(filterOption => {
      filterOption.value = '';
      return filterOption;
    });
    this.offset = 0;
    await this.customerService.setLastCustomer(false);
    await this.customerService.setPaging(0, this.perpage, []);
  }
  async loadMore(event) {
    const last_page = this.customersQuery.getValue().last_customer;
    if (!last_page){
      this.offset += this.perpage;
      await this.customerService.setPaging(this.offset, this.perpage, this.filters);
    }
    event.target.complete();
  }
  async getCustomers(filterOptions?) {
    try {
      const filters = filterOptions
          ?.filter(fo => !!fo.value)
          .map(filterOption => {
            return [
              {
                name: filterOption.name,
                op: filterOption.operator,
                value: filterOption.value
              }
            ];
          })
          .reduce((a, b) => a.concat(b), []) || [];
      this.filters = filters;
      this.customerService.setLastCustomer(false);
      await this.customerService.setSelectedListCustomer([]);
      await this.customerService.setPaging(0, this.perpage, filters);
      this.loading = false;
      this.changeDetector.detectChanges();
    } catch (e) {
      debug.error('ERROR in getting customers', e);
    }
  }

  async customerDetail(customer) {
    await this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/customers/detail/${customer.id}`
    );
  }

  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        filters: this.filterOptions
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        if (data.data.reset) {
          await this.resetFilters();
        } else {
          const filters = data.data.filters;
          this.filterOptions = filters;
        }
        this.getCustomers(this.filterOptions);
      }else {
        await this.customerService.setPaging(0, this.perpage);
      }
    });
    return await modal.present();
  }

  async createCustomer() {
    await this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/customers/create`
    );
  }

  trackCustomer(index, customer: Customer) {
    return customer.id;
  }
}
