import { Component, OnInit, Input, ChangeDetectorRef, NgZone } from '@angular/core';
import { Customer, CustomerAddress, CustomerEvent } from 'libs/models/Customer';
import { ModalController, ActionSheetController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { BaseComponent } from '@etop/core';
import { ToastService } from '../../../services/toast.service';
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { CustomersQuery } from '@etop/state/fabo/customer/customer.query';
import {LocationQuery, LocationService} from '@etop/state/location';
import {Router} from '@angular/router';
import {UtilService} from "apps/core/src/services/util.service";

@Component({
  selector: 'etop-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent extends BaseComponent implements OnInit {
  @Input() customerData = new Customer({});
  customer = new Customer({});
  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  provinces = [];
  districts = [];
  wards = [];
  locationLoaded = false;

  genders = [
    {
      title: 'Nam',
      value: 'male'
    },
    {
      title: 'Nữ',
      value: 'female'
    },
    {
      title: 'Khác',
      value: 'other'
    }
  ];
  show = true;
  customerAddresses: Array<CustomerAddress>;
  initializing = true;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private util: UtilService,
    private customerService: CustomerService,
    private customersQuery: CustomersQuery,
    private modalCtrl: ModalController,
    private actionSheetController: ActionSheetController,
    private toast: ToastService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private changeDetector: ChangeDetectorRef,
    private zone: NgZone
  ) {
    super();
  }
  ngOnInit() {
    const navigation = this.router.getCurrentNavigation();
    const { customerData } = navigation.extras.state;
    this.customerData = customerData;
  }

  async ionViewWillEnter() {
    this.initializing = true;
    this.customer = { ...this.customerData };
    const res = this.customersQuery.getValue().customer_addresses;
    if (res?.length) {
      this.address = res[0];
    }
    this.customerAddresses = res;
    await this.loadLocation();
    await this._prepareLocationData();
    this.initializing = false;
  }

  async updateCustomer() {
    try {
      this.customer.birthday =
        (this.customer.birthday &&
          moment(this.customer.birthday).format('YYYY-MM-DD')) ||
        '';
      if (!this.customer.full_name) {
        return this.toast.error('Vui lòng nhập tên!');
      }
      if (!this.customer.phone) {
        return this.toast.error('Vui lòng nhập số điện thoại!');
      }
      const res = await this.customerService.updateCustomer(this.customer);

      const customerAddress: any = {
        province_code: this.address.province_code,
        district_code: this.address.district_code,
        ward_code: this.address.ward_code,
        address1: this.address.address1,
        email: res.email,
        phone: res.phone,
        full_name: res.full_name
      };
      const {
        province_code,
        district_code,
        ward_code,
        address1
      } = customerAddress;
      if (province_code && district_code && ward_code && address1) {
        if (this.customerAddresses.length > 0) {
          await this.customerService.updateCustomerAddress({
            ...customerAddress,
            id: this.customerAddresses[0].id
          });
        } else {
          await this.customerService.createCustomerAddress({
            ...customerAddress,
            customer_id: this.customer.id
          });
        }
      }
      this.navCtrl.back();
      this.toast.success('Cập nhật khách hàng thành công');
      // this.modalCtrl.dismiss({customer: this.customer, role: 'update'});
    } catch (e) {
      this.toast.error(
        `Cập nhật thông tin khách hàng không thành công!\n${e.message}`
      );
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  async loadLocation() {
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
    this.locationLoaded = true;
  }

  onProvinceSelected() {
    this.address.district_code = '';
    this.address.ward_code = '';
    this.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
  }

  onDistrictSelected() {
    this.address.ward_code = '';
    this.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  onWardSelected() {
    this.address.address1 = '';
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Xóa khách hàng',
          role: 'destructive',
          handler: () => {
            this.deleteCustomer();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async deleteCustomer() {
    try {
      await this.customerService.deleteCustomer(this.customer.id);
      this.changeDetector.detectChanges();
      this.zone.run(async() => {
        await this.navCtrl.navigateBack(
          `/s/${this.util.getSlug()}/tab-nav/customers`
        );
      })
      this.toast.success('Xóa khách hàng thành công');
    } catch (e) {
      this.toast.error(e.message);
    }
  }
}
