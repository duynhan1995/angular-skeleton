import { Component, OnInit, Provider, NgZone } from '@angular/core';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { ModalController, NavController, IonRouterOutlet } from '@ionic/angular';
import { BaseComponent } from '@etop/core';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';
import { ActivatedRoute } from '@angular/router';
import { Order } from 'libs/models/Order';
import { OrderService } from '@etop/features';
import { UtilService } from 'apps/core/src/services/util.service';
import {FbUser} from "libs/models/faboshop/FbUser";
import {FaboCustomerService} from "@etop/features/fabo/customer/fabo-customer.service";
import {ConfirmOrderController} from "@etop/features/fabo/confirm-order/confirm-order.controller";
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { CustomersQuery } from '@etop/state/fabo/customer/customer.query';
import { distinctUntilChanged } from 'rxjs/operators';
import { FilterOperator, Filters } from '@etop/models';
import { FacebookUserTagService, FacebookUserTagQuery } from '@etop/state/fabo/facebook-user-tag';
import { ConversationsService } from '@etop/state/fabo/conversation';


@Component({
  selector: 'etop-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent extends BaseComponent implements OnInit {
  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  show = true;
  order_show = true;
  address_display = '';
  customerEdited = false;
  fbUser = new FbUser({});
  orders: Order[] = [];
  customer$ = this.customersQuery
    .selectActive<Customer>()
    .pipe(distinctUntilChanged());

  constructor(
    private customerService: CustomerService,
    private customersQuery: CustomersQuery,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private orderService: OrderService,
    private util: UtilService,
    private confirmOrderController: ConfirmOrderController,
    private routerOutlet: IonRouterOutlet,
    private zone: NgZone,
    private fbUserTagQuery: FacebookUserTagQuery,
    private conversationService: ConversationsService
  ) {
    super();
  }

  async ngOnInit() {}

  async ionViewWillEnter() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.customerService.setActiveCustomer(id);
    const customer = this.customersQuery.getActive();
    if (customer.fb_users.length > 0) {
      this.fbUser = await this.conversationService.getFbUser(customer.fb_users[0].external_id);
      this.customerService.setTagsFbUser(id, this.fbUser.tag_ids);
    }
    await this.customerService.getCustomerAddresses(id);
    let res: CustomerAddress[];
    this.customersQuery
      .select(s => s.customer_addresses)
      .pipe()
      .subscribe(customeAddresses => {
        if (customeAddresses.length) {
          res = customeAddresses;
          if (res.length) {
            this.address = res[0];
            this.address_display = this.mapAddress(this.address);
          }
        }
      });
    const filters: Filters = [
      {
        name: 'customer.id',
        op: FilterOperator.eq,
        value: id
      }
    ];
    this.orders = await this.orderService.getOrders(0, 100, filters);
    this.orders.map(order => {
      if (order.fulfillments.length > 0) {
        const ffm = order.fulfillments[0];
        ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
        ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
          ffm.shipping_state
        );
      }
    });
  }

  mapTags(tag_ids) {
    const _tags = this.fbUserTagQuery.getAll();
    let tags = [];
    if (tag_ids) {
      _tags.forEach(tag => {
        if (tag_ids.includes(tag.id)) {
          tags.push(tag);
        }
      });
    }
    return tags;
  }

  mapAddress(address) {
    return (
      address.address1 +
      ', ' +
      address.ward +
      ', ' +
      address.district +
      ', ' +
      address.province
    );
  }

  genderMap(gender) {
    switch (gender) {
      case 'male': {
        return 'Nam';
      }
      case 'female': {
        return 'Nữ';
      }
      case 'other': {
        return 'Khác';
      }
      default: {
        return '-';
      }
    }
  }

  async dismiss() {
    this.navCtrl.back();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  toggleOrder() {
    this.order_show = !this.order_show;
  }

  async customerEdit() {
    const customer = this.customersQuery.getActive();
    this.navCtrl.navigateForward(['.', 'edit'], {
      relativeTo: this.activatedRoute,
      state: {
        customerData: customer
      }
    });
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  async orderDetail(order) {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/orders/${order.id}`
    );
  }

  confirmOrder() {
    const customer = this.customersQuery.getActive();
    this.confirmOrderController.setCustomerForOrder(customer);
    this.navCtrl
      .navigateForward(`/s/${this.util.getSlug()}/confirm-order`)
      .then();
  }

}
