import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { TimerService } from '../../../../../core/src/services/timer.service';
import { Router } from '@angular/router';
import { CmsService } from '../../../../../core/src/services/cms.service';
import { SurveyInfo } from '@etop/models';
import { NavController } from '@ionic/angular';
import { UtilService } from '../../../../../core/src/services/util.service';
import { ShopService } from '@etop/features';
import { ToastService } from '../../services/toast.service';
import { CommonUsecase } from '../../../../../shared/src/usecases/common.usecase.service';

interface SurveyQuestion {
  key: string;
  image_url: string;
  question: string;
  answersList: { text: string; value: string | number }[];
  answer: string | number;
}

@Component({
  selector: 'topship-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  providerQuestion: SurveyQuestion = null;
  ordersPerDayQuestion: SurveyQuestion = null;
  defaultWeightQuestion: SurveyQuestion = null;

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private cms: CmsService,
    private navCtrl: NavController,
    private ref: ChangeDetectorRef,
    private zone: NgZone,
    private util: UtilService,
    private shopService: ShopService,
    private toastr: ToastService,
    private commonUsecase: CommonUsecase
  ) { }

  async ngOnInit() {
    if (this.cms.bannersLoaded) {
      const _survey = await this.cms.getTopshipSurvey();
      this.providerQuestion = _survey['provider'];
      this.ordersPerDayQuestion = _survey['orders_per_day'];
      this.defaultWeightQuestion = _survey['default_weight'];
    } else {
      this.cms.onBannersLoaded.subscribe(async _ => {
        const _survey = await this.cms.getTopshipSurvey();
        this.providerQuestion = _survey['provider'];
        this.ordersPerDayQuestion = _survey['orders_per_day'];
        this.defaultWeightQuestion = _survey['default_weight'];
      });
    }
  }

  get user() {
    return this.auth.snapshot.user;
  }

  async saveSurvey() {
    if (!(this.providerQuestion.answer && this.ordersPerDayQuestion.answer && this.defaultWeightQuestion.answer)) {
      return this.toastr.error('Bạn chưa trả lời hết tất cả các câu hỏi.');
    }

    const survey: SurveyInfo[] = [
      {
        key: this.providerQuestion.key,
        question: this.providerQuestion.question,
        answer: this.providerQuestion.answer.toString()
      },
      {
        key: this.ordersPerDayQuestion.key,
        question: this.ordersPerDayQuestion.question,
        answer: this.ordersPerDayQuestion.answer.toString()
      },
      {
        key: this.defaultWeightQuestion.key,
        question: this.defaultWeightQuestion.question,
        answer: this.defaultWeightQuestion.answer.toString()
      }
    ];

    await this.shopService.updateShopField({survey_info: survey});
    await this.commonUsecase.updateSessionInfo(true);
    this.zone.run(async () => {
      await this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/tab-nav/orders`,
        {
          animated: false
        }
      );
    });
  }
}
