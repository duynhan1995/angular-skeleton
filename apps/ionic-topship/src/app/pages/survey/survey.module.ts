import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes } from '@angular/router';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { LoginComponent } from '../login/login.component';
import { SurveyComponent } from './survey.component';

const routes: Routes = [
  {
    path: '',
    component: SurveyComponent
  }
];

@NgModule({
  declarations: [
    SurveyComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NetworkDisconnectModule
  ]
})
export class SurveyModule { }
