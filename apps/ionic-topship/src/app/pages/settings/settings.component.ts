import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShopService } from '@etop/features';
import { ToastController, NavController } from '@ionic/angular';
import { ShopInfoModel } from 'apps/ionic-topship/src/models/ShopInfo';
import { Plugins, CameraResultType } from '@capacitor/core';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import List from 'identical-list';
import { ToastService } from '../../services/toast.service';
import {LocationQuery, LocationService} from '@etop/state/location';
import { FormBuilder, Validators } from '@angular/forms';

const { Camera } = Plugins;

@Component({
  selector: 'etop-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent extends BaseComponent implements OnInit {
  currentShop: any = {
    address: {
      address1: '',
      province_code: '',
      district_code: '',
      ward_code: ''
    }
  };
  provinces = [];
  districts = [];
  wards = [];
  currentShopOriginal: any = new ShopInfoModel();
  currentAccount: any = {};
  uploadLoading = false;
  user: any = {};
  loadData = false;

  accounts = new List<any>();

  shopInfo = this.fb.group({
    name: ['', Validators.required],
    phone: ['', Validators.required],
    website_url: '',
    email: ['', Validators.required],
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  })

  constructor(
    private authStore: AuthenticateStore,
    private util: UtilService,
    private shopService: ShopService,
    private toastController: ToastController,
    private uploader: MobileUploader,
    private toastService: ToastService,
    private auth: AuthenticateStore,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private navCtrl: NavController,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {

  }
  async ionViewWillEnter() {
    const auth = this.authStore.snapshot;
    this.loadAccount(auth);
    await this.loadLocation();
    const { name, phone, website_url, email } = this.currentShop;
    const { address1, province_code, district_code, ward_code } = this.currentShop.address;
    this.shopInfo.patchValue({
      name,
      phone,
      website_url,
      email,
      address1,
      province_code,
      district_code,
      ward_code
    })
    this.loadData = true;
  }

  dismiss() {
    this.navCtrl.back();
  }

  disableView(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }

  loadAccount(data) {
    this.loadData = false;
    this.currentShop = data.shop || new ShopInfoModel();
    this.currentShopOriginal = this.currentShop;
    this.user = data.user;
    this.currentAccount = data.account || {};
    this.accounts = new List(this.authStore.snapshot.accounts);
  }

  loadLocation() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.districts = this.locationService.filterDistrictsByProvince(this.currentShop.address.province_code);
    this.wards = this.locationService.filterWardsByDistrict(this.currentShop.address.district_code);
  }

  onProvinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(this.shopInfo.get('province_code').value);
    this.shopInfo.patchValue({
        district_code: '',
        ward_code: ''
    })
  }

  onDistrictSelected() {
    this.wards = this.locationService.filterWardsByDistrict(this.shopInfo.get('district_code').value);
    this.shopInfo.patchValue({
      ward_code: ''
    }, {
      emitEvent: false
    })
  }

  validatePhoneNumber(phone) {
    if (phone && phone.match(/^0[0-9]{9,10}$/)) {
      if (phone.length === 11 && phone[1] === '1') {
        this.toastService.error('Vui lòng nhập số điện thoại di động 10 số');
      }
      return true;
    }
    this.toastService.error('Vui lòng nhập số điện thoại hợp lệ');
  }

  async updateContactInfo() {
    const _data: any = this.shopInfo.value;
    try {
      if (!_data.name) {
        throw new Error('Vui lòng nhập tên cửa hàng');
      }

      const phone = _data.phone;
      this.validatePhoneNumber(phone);
      if (
        _data.website_url &&
        !_data.website_url.match(
          /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        )
      ) {
        throw new Error(`Vui lòng điền địa chỉ website hợp lệ`);
      }

      let email = _data.email;
      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = (email && email.split('-test')[0]) || '';
      if (
        email &&
        !email.match(
          /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
      ) {
        throw new Error('Vui lòng nhập email hợp lệ!');
      }
      if (!_data.address1) {
        throw new Error('Vui lòng nhập địa chỉ');
      }

      const proObj = this.provinces.find(
        p => p.code == _data.province_code
      );
      const disObj = this.districts.find(
        d => d.code == _data.district_code
      );
      const warObj = this.wards.find(
        w => w.code == _data.ward_code
      );

      if (!_data.province_code || !proObj) {
        throw new Error('Vui lòng chọn tỉnh thành');
      }
      if (!_data.district_code || !disObj) {
        throw new Error('Vui lòng chọn quận huyện');
      }
      if (!_data.ward_code || !warObj) {
        throw new Error('Vui lòng chọn phường xã');
      }

      const address = {
        address1: _data.address1,
        province: proObj.name,
        province_code: proObj.code,
        district: disObj.name,
        district_code: disObj.code,
        ward: warObj.name,
        ward_code: warObj.code
      };

      const data = {
        name: _data.name,
        image_url: this.currentShop.image_url,
        phone: _data.phone,
        website_url: _data.website_url,
        email: _data.email,
        address
      };

      await this.shopService.updateShopField(data);
      this.currentShop = Object.assign(this.currentShop, data);
      this.currentShopOriginal = this.currentShop;
      this.authStore.updateAccount(this.currentAccount);
      this.authStore.updateShop(this.currentShop);
      this.toastService.success('Cập nhật thành công')
    } catch (e) {
      this.toastService.error(e.message);
      debug.log('ERROR in update shop', e)
    }
  }

  async onFileSelected($e) {
    try {
      this.uploadLoading = true;
      const { files } = $e.target;
      const res = await this.util.uploadImages([files[0]], 250);
      this.currentShop.image_url = res[0].url;

      const data = {
        image_url: this.currentShop.image_url
      };
      await this.shopService.updateShopField(data);
      this.authStore.updateShop(this.currentShop);
      this.toastService.success('Cập nhật logo thành công')
      this.uploadLoading = false;
    } catch (e) {
      this.uploadLoading = false;
      debug.error(e);
      this.toastService.error(`Chọn logo thất bại: ${e.message}`)
    }
  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      width: 640
    });
    const imageElement = await this.uploader.uploadBase64Image(
      image.base64String
    );
    if (imageElement) {
      this.currentShop.image_url = imageElement.result[0].url;
    }
  }
}
