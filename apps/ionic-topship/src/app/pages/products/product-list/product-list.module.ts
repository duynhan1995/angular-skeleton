import { NgModule } from '@angular/core';
import { ProductListComponent } from './product-list.component';
import { SharedModule } from 'apps/ionic-topship/src/app/features/shared/shared.module';
import { EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkDisconnectModule } from 'apps/ionic-topship/src/app/components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from 'apps/ionic-topship/src/app/components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';

@NgModule({
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    IonInputFormatNumberModule,
    ReactiveFormsModule
  ],
  declarations: [ProductListComponent],
  exports: [ProductListComponent]
})
export class ProductListModule{}
