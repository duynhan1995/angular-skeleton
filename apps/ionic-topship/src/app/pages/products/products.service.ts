import { Injectable } from '@angular/core';
import { ProductApi } from '@etop/api';
import { ProductStore } from './products.store';
import { UtilService } from 'apps/core/src/services/util.service';
import { Variant } from 'libs/models/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private productApi: ProductApi,
    private productsStore: ProductStore,
    private util: UtilService
  ) {}

  async fetchProducts(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return await this.productApi.getProducts({ paging, filters }).then(res => {
      // update product list store
      this.productsStore.setProducts(res.products);
      return res;
    });
  }

  async fetchProduct(id, reset?: boolean) {
    // update product store by id
    let products = this.productsStore.snapshot.products;
    let res: any;
    if (!products || reset) {
      res = await this.productApi.getProduct(id);
      const index = products.map(product => product.id).indexOf(id);
      Object.assign(products[index], res);
      this.productsStore.setProducts(products);
    } else {
      res = products.find(p => p.id == id);
    }
    this.productsStore.setActiveProduct(res);
    return res;
  }

  async deleteProduct(ids: Array<string>) {
    let products = this.productsStore.snapshot.products;
    const index = products.findIndex(product => ids.includes(product.id));
    products.splice(index, 1);
    this.productsStore.setProducts(products);
    return this.productApi.removeProducts(ids);
  }

  async removeVariants(ids: Array<string>) {
    return this.productApi.removeVariants(ids);
  }

  async updateVariant(data) {
    return this.productApi.updateVariant(data);
  }

  async updateProduct(data) {
    return this.productApi.updateProduct(data);
  }

  async updateProductImages(data) {
    return this.productApi.updateProductImages(data);
  }

  async createProduct(data) {
    return this.productApi.createProduct(data);
  }

  async createVariant(data) {
    return this.productApi.createVariant(data);
  }
  insertProductToStore(product : any){
    this.productsStore.insertProduct(product)
  }
  allPossibleCases(attrArr) {
    if (attrArr.length == 1) {
      return attrArr[0].values.map(a => {
        return [
          {
            name: attrArr[0].name,
            value: a
          }
        ];
      });
    } else {
      let result = [];
      let allCasesOfRest = this.allPossibleCases(attrArr.slice(1));
      for (let i = 0; i < allCasesOfRest.length; i++) {
        for (let j = 0; j < attrArr[0].values.length; j++) {
          let a = JSON.parse(JSON.stringify(allCasesOfRest[i]));
          a.push({
            name: attrArr[0].name,
            value: attrArr[0].values[j]
          });
          result.push(a);
        }
      }
      return result;
    }
  }

  getCombineVariantsFromAttrs(attrs) {
    let allAttrsArray = [];
    attrs.forEach(a => {
      allAttrsArray.push(a.values);
    });
    let variants = [];
    let allAttrsCombine = this.allPossibleCases(attrs);
    allAttrsCombine.forEach(a => {
      let v = {
        code: '',
        retail_price: '',
        attributes: []
      };
      v.attributes = a;
      variants.push(v);
    });
    return variants;
  }
}
