import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, ActionSheetController } from '@ionic/angular';
import { Product } from 'libs/models/Product';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CameraResultType, Plugins } from '@capacitor/core';
import { ImageCompress } from '@etop/utils/image-compressor/image-compressor';
import { ImageCompressor } from '@etop/utils/image-compressor/image-compressor.service';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { UtilService } from 'apps/core/src/services/util.service';
import { ProductService } from '../../products.service';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';

const { Camera } = Plugins;

@Component({
  selector: 'etop-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {
  product = new Product({});

  productForm = this.fb.group({
    name: '',
    code: '',
    description: ''
  });

  productImgEdited = false;
  productImg = '';
  uploading = false;

  constructor(
    private navCtrl: NavController,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private actionSheetController: ActionSheetController,
    private imageCompressor: ImageCompressor,
    private uploader: MobileUploader,
    private toast: ToastService,
    private util: UtilService,
    private zone: NgZone
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    const id = this.activatedRoute.snapshot.params.id;
    this.product = await this.productService.fetchProduct(id);
    const { name, code, description } = this.product;
    this.productForm.setValue({ name, code, description });
  }

  back() {
    this.navCtrl.back()
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Chụp ảnh',
          handler: () => {}
        },
        {
          text: 'Lấy từ bộ sưu tập',
          handler: () => {}
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async takePicture() {
    try {
      const image = await Camera.getPhoto({
        quality: 100,
        allowEditing: false,
        resultType: CameraResultType.DataUrl
      });
      const limitSize = 1024 * 1024;
      const decode = await ImageCompress.decodeImage(image.dataUrl);
      let scaleRatio = (limitSize * 100) / (decode.height * decode.width);
      scaleRatio = scaleRatio > 100 ? 100 : scaleRatio;
      const compressedDataUrl = await this.imageCompressor.compressFile(
        image.dataUrl,
        decode.orientation,
        scaleRatio,
        100
      );
      this.uploading = true;
      const result = await this.uploader.uploadBase64Image(
        compressedDataUrl.split(',')[1]
      );
      this.productImg = result.result[0].url;
      if (this.productImg) {
        this.productImgEdited = true;
      }
      this.product.image = this.productImg;
      this.uploading = false;
    } catch (e) {
      this.uploading = false;
      debug.log('Error in takePicture', e);
    }
  }

  async updateProduct() {
    try {
      const { name, code, description } = this.productForm.getRawValue();
      if (!name) {
        return this.toast.error('Tên sản phẩm không được để trống!')
      }

      const data = {
        code,
        id: this.product.id,
        name,
        description
      };
      await this.productService.updateProduct(data);
      if (this.productImgEdited) {
        await this.updateProductImage({
          id: this.product.id,
          replace_all: [this.product.image]
        });
      }
      await this.productService.fetchProduct(this.product.id, true);
      this.toast.success('Cập nhật sản phẩm thành công!');
      this.back();
    } catch (e) {
      this.toast.error('Lỗi cập nhật sản phẩm: ' + e.message);
    }
  }

  async updateProductImage(data) {
    try {
      await this.productService.updateProductImages(data);
    } catch (e) {
      debug.error('ERROR in updateProductImages', e);
      this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
    }
  }

  toEditVariant() {
    this.zone.run(() => {
      this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/products/variant/list/${
        this.product.id
        }`
      );
    });
  }
}
