import { Component, OnInit } from '@angular/core';
import { ProductStore } from '../../products.store';
import { FormBuilder, FormArray } from '@angular/forms';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'etop-add-variant-modal',
  templateUrl: './add-variant-modal.component.html',
  styleUrls: ['./add-variant-modal.component.scss']
})
export class AddVariantModalComponent implements OnInit {
  product: any;
  variantForm = this.fb.group({
    code: '',
    retail_price: '',
    attributes: this.fb.array([])
  });
  constructor(
    private productStore: ProductStore,
    private fb: FormBuilder,
    private modalCtrl: ModalController
  ) {}

  get attributes() {
    return this.variantForm.controls['attributes'] as FormArray;
  }

  ngOnInit(): void {}

  ionViewWillEnter() {
    this.product = this.productStore.snapshot.productCreate;

    if (this.product.variants) {
      let attributes =
        (this.product.variants &&
          this.product.variants[0] &&
          this.product.variants[0].attributes) ||
        [];
      attributes.map(a => {
        return {
          name: a.name,
          value: null
        };
      });
      attributes.forEach(attr => {
        this.attributes.push(
          this.fb.group({
            name: attr.name,
            value: ''
          })
        );
      });
    }
  }

  async create() {
    debug.log('variantForm', this.variantForm.value);

    const { code, retail_price, attributes } = this.variantForm.value;
    let _attributes = [];
    attributes.forEach(attr => {
      if (attr.value) {
        _attributes.push(attr);
      }
    });
    this.product.variants.push({
      code,
      retail_price,
      attributes: _attributes
    })
    this.productStore.setProductCreate(this.product);
    this.modalCtrl.dismiss(true)
  }

  back() {
    this.modalCtrl.dismiss();
  }
}
