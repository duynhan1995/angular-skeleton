import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { FormBuilder, FormArray, FormControl, FormGroupDirective } from '@angular/forms';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { ProductStore } from '../../products.store';
import { AddAttributeComponent } from '../../add-attribute/add-attribute.component';
import { VariantsGenerateComponent } from '../../components/variants-generate/variants-generate.component';

@Component({
  selector: 'etop-create-attribute',
  templateUrl: './create-attribute.component.html',
  styleUrls: ['./create-attribute.component.scss']
})
export class CreateAttributeComponent implements OnInit {
  attributes = this.fb.array([
    this.fb.group({
      name: '',
      values: []
    })
  ]);

  myForm = this.fb.group({
    attributes: this.attributes
  });

  product: any;

  constructor(
    private navCtrl: NavController,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private fb: FormBuilder,
    private toast: ToastService,
    private productStore: ProductStore
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.product = this.productStore.snapshot.productCreate;
  }

  back() {
    this.navCtrl.back();
  }

  addLineAttributes() {
    this.attributes.push(
      this.fb.group({
        name: '',
        values: []
      })
    );
  }

  removeLineAttribute(index) {
    this.attributes.removeAt(index);
  }

  removeValueAttribute(index, i) {
    let _values = this.attributes.controls[index].get('values').value;
    _values.splice(i, 1);
    this.attributes.controls[index].get('values').setValue(_values);
  }

  async addAttribute(attribute, index) {
    if (!attribute.name) {
      return this.toast.error('Vui lòng nhập tên thuộc tính trước khi tạo giá trị mới');
    }
    debug.log('attribute', attribute);
    const modal = await this.modalController.create({
      component: AddAttributeComponent,
      cssClass: 'my-custom-class',
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        attribute
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.attributes.at(index).setValue(data.data);
      }
    });
    return await modal.present();
  }

  async generateVariants() {
    let openModal = true;
    this.attributes.value.forEach(attr => {
      if (!attr.name.trim()) {
        openModal = false;
        return this.toast.error('Vui lòng nhập tên thuộc tính');
      }
      if (!attr.values?.length) {
        openModal = false;
        return this.toast.error(`Vui lòng thêm giá trị cho thuộc tính "${attr.name}"`);
      }
    });
    if (openModal) {
      const modal = await this.modalController.create({
        component: VariantsGenerateComponent,
        cssClass: 'my-custom-class',
        swipeToClose: false,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          attributes: this.attributes.value,
          product: this.product
        }
      });
      modal.onWillDismiss().then(async data => {
        if (data.data.submit) {
          this.back();
        }
      });
      return await modal.present();
    }
  }
}
