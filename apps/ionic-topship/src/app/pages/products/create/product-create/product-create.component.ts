import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { CameraResultType, Plugins } from '@capacitor/core';
import { ImageCompress } from '@etop/utils/image-compressor/image-compressor';
import { ImageCompressor } from '@etop/utils/image-compressor/image-compressor.service';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { UtilService } from 'apps/core/src/services/util.service';
import { Product, ShopProduct, Variant } from 'libs/models/Product';
import { map } from 'rxjs/operators';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { ProductService } from '../../products.service';
import { LoadingService } from 'apps/ionic-topship/src/app/services/loading.service';
import { ProductStore } from '../../products.store';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';

const { Camera } = Plugins;

@Component({
  selector: 'etop-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {
  uploading = false;
  productImg = '';
  productImgEdited = false;

  productForm = this.fb.group({
    name: '',
    code: '',
    retail_price: '',
    description: '',
    variants: []
  });

  price = false;
  product: Product;
  success = 0;

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private imageCompressor: ImageCompressor,
    private uploader: MobileUploader,
    private toast: ToastService,
    private productService: ProductService,
    private loadingService: LoadingService,
    private util: UtilService,
    private productStore: ProductStore,
    private promiseQueue: PromiseQueueService
  ) {}

  ngOnInit(): void {}

  async back() {
    await this.navCtrl.back()
  }

  async ionViewWillEnter() {
    this.product = this.productStore.snapshot.productCreate;
    if (this.product?.variants) {
      this.price = true;
    }
  }

  priceCompare() {
    const product = this.productStore.snapshot.productCreate;
    return this.util.compareProductPrice(product);
  }
  async takePicture() {
    try {
      const image = await Camera.getPhoto({
        quality: 100,
        allowEditing: false,
        resultType: CameraResultType.DataUrl
      });
      const limitSize = 1024 * 1024;
      const decode = await ImageCompress.decodeImage(image.dataUrl);
      let scaleRatio = (limitSize * 100) / (decode.height * decode.width);
      scaleRatio = scaleRatio > 100 ? 100 : scaleRatio;
      const compressedDataUrl = await this.imageCompressor.compressFile(
        image.dataUrl,
        decode.orientation,
        scaleRatio,
        100
      );
      this.uploading = true;
      const result = await this.uploader.uploadBase64Image(
        compressedDataUrl.split(',')[1]
      );
      this.productImg = result.result[0].url;
      if (this.productImg) {
        this.productImgEdited = true;
      }
      this.uploading = false;
    } catch (e) {
      this.uploading = false;
      debug.log('Error in takePicture', e);
    }
  }

  async create() {
    const {
      name,
      retail_price,
      code,
      description
    } = this.productForm.value;
    if (!name) {
      return this.toast.error('Tên sản phẩm phải có ít nhất 2 kí tự không tính dấu cách!');
    }
    if (!retail_price && !this.product?.variants) {
      return this.toast.error('Chưa nhập giá bán sản phẩm!');
    }
    try {
      const body = new Product({
        name,
        code,
        description
      });
      await this.loadingService.start('Đang tạo sản phẩm');
      const product = await this.productService.createProduct(body);
      if (this.productImg) {
        await this.updateProductImage({
          id: product.id,
          replace_all: [this.productImg]
        });
      }
      await this.createVariants(product);
      this.loadingService.end();
      this.productService.fetchProducts();
      this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/tab-nav/products`
      );
    } catch (e) {
      this.loadingService.end();
      debug.error('ERROR in creating product', e);
      const msg =
        e.message ||
        'Tạo sản phẩm thất bại';
      this.toast.error(msg);
    }
  }

  async updateProductImage(data) {
    try {
      await this.productService.updateProductImages(data);
    } catch (e) {
      debug.error('ERROR in updateProductImages', e);
    }
  }

  async createVariants(product: Product | ShopProduct) {
    try {
      const { retail_price } = this.productForm.value;
      const body: any = {
        product_id: product.id,
        name: product.name,
        retail_price,
        list_price: retail_price,
        image_urls: product.image_urls,
        is_available: true,
        code: product.code
      };
      if (this.product && this.product.variants?.length) {
        const promises = this.product.variants.map(v => async() => {
          v.product_id = product.id;
          v.retail_price = Number(v.retail_price);
          try {
            await this.productService.createVariant(v);
            this.success += 1;
          } catch (e) {
            debug.log('ERROR in Create variant', e);
          }
        });
        await this.promiseQueue.run(promises, 1);
        this.toast.success(
          `Tạo ${this.success}/${this.product.variants.length} mẫu mã thành công.`
        );
      } else {
        body.retail_price = this.productForm.controls['retail_price'].value;
        await this.productService.createVariant(body);
      }
    } catch (e) {
      debug.error('ERROR in creating variants', e);
      // throw e;
    }
  }

  initializationAttribute() {
    this.productStore.setProductCreate(this.productForm.value);
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/products/attribute/create`
    );
  }

  editVariants() {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/products/variants`);
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    let _attrs = []
    attrs.forEach(a => {
      if (a.value) {
        _attrs.push(a.value)
      }
    });
    return _attrs.join(' - ');
  }
}
