import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';

@Component({
  selector: 'etop-add-attribute-name',
  templateUrl: './add-attribute-name.component.html',
  styleUrls: ['./add-attribute-name.component.scss']
})
export class AddAttributeNameComponent implements OnInit {
  name = '';

  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  back() {
    this.modalCtrl.dismiss();
  }

  submit() {
    debug.log('name', this.name);
    if (!this.name) {
      return this.toast.error('Tên thuộc tính không được rỗng!');
    }
    this.modalCtrl.dismiss(this.name);
  }
}
