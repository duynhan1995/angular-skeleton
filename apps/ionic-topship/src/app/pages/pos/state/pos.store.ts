import { Injectable } from '@angular/core';
import { StoreConfig, EntityStore } from '@datorama/akita';
import { FulfillmentAPI } from '@etop/api';
import { Customer, Fulfillment, Order, OrderAddress, Shipping } from '@etop/models';

const initialState = {
    order: new Order({
        lines: [],
        customer: new Customer({}),
        shipping : new Shipping(),
    }),
    resetData: false,
    fulfillment: new Fulfillment({
        pickup_address: new OrderAddress({}),
        shipping_address: new OrderAddress({}),
        chargeable_weight: null,
    }),
    shipmentServicesRequest: new FulfillmentAPI.GetShipmentServicesRequest(),
    pickupAddresses: [],
    shippingAddresses: null,
    defaultPickupAddress: null,
    products: null,
};

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'Pos' })
export class PosStore extends EntityStore<any> {
  constructor() {
    super(initialState);
  }
}
