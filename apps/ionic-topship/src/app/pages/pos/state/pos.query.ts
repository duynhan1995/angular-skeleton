import { Injectable } from '@angular/core';
import { QueryEntity, QueryConfig } from '@datorama/akita';
import { PosStore } from './pos.store';

@Injectable({
  providedIn: 'root'
})
@QueryConfig({})
export class PosQuery extends QueryEntity<any, any> {
  constructor(protected store: PosStore) {
    super(store);
  }
}
