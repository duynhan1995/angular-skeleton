import { Injectable } from '@angular/core';
import { AddressApi, ProductApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { Address, Customer, Fulfillment, Order, OrderAddress, OrderLine, Shipping } from '@etop/models';
import { Subject } from 'rxjs';
import { PosQuery } from './pos.query';
import { PosStore } from './pos.store';
import { CmsService } from '../../../../../../core/src/services/cms.service';

@Injectable({
    providedIn: 'root'
})

export class PosService {
    readonly forceUpdateForm$ = new Subject();
    readonly forceUpdateFulfillment$ = new Subject();
    readonly forceGetShipmentService$ = new Subject();
    readonly forceResetCOD$ = new Subject();
    getServicesIndex = 0;
    constructor(
        private posStore: PosStore,
        private posQuery: PosQuery,
        private addressApi: AddressApi,
        private productApi: ProductApi,
        private auth: AuthenticateStore,
        private cms: CmsService,
    ) {}

    updateFulfillment(fulfillment: Fulfillment) {
      this.posStore.update({ fulfillment });
    }

    updateOrder(order: Order) {
        this.posStore.update({ order })
    }

    updatePickupAddresses(pickupAddresses) {
        this.posStore.update({ pickupAddresses })
    }

    forceUpdateForm() {
        this.forceUpdateForm$.next();
    }

    forceUpdateFulfillment() {
        this.forceUpdateFulfillment$.next();
    }

    forceGetShipmentService() {
        this.forceGetShipmentService$.next();
    }

    forceUpdateLines() {
      const _order = this.posQuery.getValue().order;
        const ffm = this.posQuery.getValue().fulfillment;
        let {lines, fee_lines, order_discount} = _order;
        lines?.forEach(line => {
            if (line.payment_price != null) {
                line.payment_price = Number(line.payment_price);
            }
            if (line.quantity != null) {
                line.quantity = Number(line.quantity);
            }
            line.list_price = line.payment_price;
            line.retail_price = line.payment_price;
        });
        const basketValue = (lines && lines.reduce((a, b) => a + b.payment_price * b.quantity, 0)) || 0;
        const totalItems = (lines && lines.reduce((a, b) => a + b.quantity, 0)) || 0;
        const totalDiscount = Number(order_discount) || 0;
        const totalFee = (fee_lines && fee_lines.reduce((a, b) => a + Number(b.amount), 0)) || 0;
        const totalAmount = basketValue - totalDiscount + totalFee;
        this.posStore.update({order : {
            ..._order,
            basket_value: basketValue,
            total_items: totalItems,
            total_discount: totalDiscount,
            total_fee: totalFee,
            total_amount: totalAmount
        }})
        this.posStore.update({fulfillment : {
            ...ffm,
            basket_value: basketValue,
        }})
        this.forceUpdateForm$.next();
        this.forceGetShipmentService$.next();
        this.forceResetCOD$.next();
    }

    async addOrderLine(data: OrderLine) {
      const _order = this.posQuery.getValue().order;
        let { lines } = _order;
        const existVariant = lines?.find(line => line.variant_id === data.variant_id);
        if (existVariant) {
            existVariant.quantity += 1;
        } else {
            lines = Object.assign([], lines);
            lines.push(data);
        }
        this.posStore.update({order: {
            ..._order,
            lines
        }})
      this.forceUpdateLines();
    }

    async removeOrderLine(orderLine) {
        const _order = this.posQuery.getValue().order;
        let lines = _order.lines;
        let index = lines.findIndex(line => line.variant_id === orderLine.variant_id);
        lines.splice(index, 1);
        this.posStore.update({order : {..._order, lines: lines}})
        this.forceUpdateLines();
    }

    async updateOrderLine(orderLine) {
        const _order = this.posQuery.getValue().order;
        let lines = _order.lines;
        let index = lines.findIndex(line => line.variant_id === orderLine.variant_id);
        lines[index].quantity = orderLine.quantity;
        this.posStore.update({order : {..._order, lines: lines}})
        this.forceUpdateLines();
    }

    async getPickupAddress() {
        const addresses = await this.addressApi.getAddresses();
        const fromAddresses = addresses.filter(a => a.type === 'shipfrom');
        this.posStore.update({ pickupAddresses: fromAddresses });
    }

    selectDefaultPickup(address: Address) {
        this.posStore.update({ defaultPickupAddress: address })
        this.updatePickupAddress(address)
    }

    updatePickupAddress(address: Address) {
        const fulfillment = this.posQuery.getValue().fulfillment;
        this.posStore.update({fulfillment: {
            ...fulfillment,
            pickup_address: address
        }})
    }

    updateShippingAddress(address: Address) {
        const fulfillment = this.posQuery.getValue().fulfillment;
        this.posStore.update({fulfillment: {
            ...fulfillment,
            shipping_address: address
        }})
        this.forceGetShipmentService$.next();
    }

    async getCustomerAddress(query: any) {
        return await this.addressApi.getCustomerAddress(query);
    }

    async fetchProducts(start?: number, perpage?: number, filters?: Array<any>) {
        let paging = {
          offset: start || 0,
          limit: perpage || 1000
        };

        return await this.productApi.getProducts({ paging, filters }).then(res => {
          // update product list store
          this.posStore.update({products: res.products})
          return res;
        });
    }

    async searchProducts(query) {
      let paging = {
          offset: 0,
          limit: 10
        };
        const res = await this.productApi.getProducts({
          paging, filter: {
            name: query
          }
        });
        this.posStore.update({products: res.products});
    }

    getServicesData(ffm: Fulfillment) {
        const data = {
            basket_value: ffm.basket_value,
            chargeable_weight: ffm.chargeable_weight,
            include_insurance: ffm.include_insurance,
            from_province_code: ffm.pickup_address?.province_code,
            from_district_code: ffm.pickup_address?.district_code,
            from_ward_code: ffm.pickup_address?.ward_code,
            to_province_code: ffm.shipping_address?.province_code,
            to_district_code: ffm.shipping_address?.district_code,
            to_ward_code: ffm.shipping_address?.ward_code,
            total_cod_amount: ffm.cod_amount,
        }
        return data;
      }

    updateShippingService(service) {
        const fulfillment = this.posQuery.getValue().fulfillment;
        if (!service) {
            this.posStore.update({
                fulfillment: {
                    ...fulfillment,
                selected_service_unique_id: null,
                shipping_service_fee: null,
                shipping_service_name: null,
                shipping_service_code: null,
                connection_id: null,
                  carrier: null,
                }
            })
        } else {
            this.posStore.update({
                fulfillment: {
                    ...fulfillment,
                  selected_service_unique_id: service.unique_id,
                  shipping_service_fee: service.service_fee,
                  shipping_service_name: service?.name,
                  shipping_service_code: service?.code,
                  connection_id: service?.connection_info?.id,
                  carrier: service?.carrier
                }
            })
        }

    }

    resetStore() {
        this.posStore.update({
            order: new Order({
                lines: [],
                customer: new Customer({}),
                shipping : new Shipping(),
            }),
            resetData: false,
            fulfillment: new Fulfillment({
                pickup_address: new OrderAddress({}),
                shipping_address: new OrderAddress({}),
                chargeable_weight: this.defaultWeight(),
            }),
        })
    }

    async fastCreateProduct({ product_name, price }) {
        const product = await this.productApi.createProduct({
          name: product_name,
          retail_price: price
        });

        await this.productApi.createVariant({
          product_id: product.id,
          name: product.name,
          product_name: product.name,
          retail_price: product.retail_price,
          list_price: product.retail_price,
          code: product.code
        });
        return this.productApi.getProduct(product.id);
      }


       defaultWeight(): Number {
        let defaultWeight = 250;
        let defaultWeightFromLatestOrder = this.getDefaultWeightLatestOrder();
        let defaultWeightFromSurveyShop = this.getDefaultWeightFromSurveyShop();
        let defaultWeightFromCms = this.getDefaultWeightFromCms();

        return defaultWeightFromLatestOrder || defaultWeightFromSurveyShop || defaultWeightFromCms || defaultWeight;
      }

      getDefaultWeightLatestOrder(): Number {
        let defaultWeight = localStorage.getItem('lastWeight');
        return Number(defaultWeight);
      }

      getDefaultWeightFromSurveyShop(): Number {
        let defaultWeightInfo: any = this.auth.snapshot?.shop?.survey_info?.find(survey => survey.key == 'default_weight');
        return Number(defaultWeightInfo?.answer)
      }

      getDefaultWeightFromCms(): Number {
        let defaultWeight = this.cms.getDefaultWeight();
        return Number(defaultWeight)
      }

      defaultTryOn() {
        let defaultTryOn: any = this.auth.snapshot.shop?.try_on;
        return defaultTryOn;
      }
}
