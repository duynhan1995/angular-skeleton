import { ShipmentServiceOptionsComponent } from './components/shipment-service-options/shipment-service-options.component';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { PosQuery, PosService } from './state';
import { ToastService } from '../../services/toast.service';
import { CustomerApi, FulfillmentAPI, FulfillmentApi, OrderApi } from '@etop/api';
import { NavController } from '@ionic/angular';
import { LoadingService } from '../../services/loading.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { takeUntil } from 'rxjs/operators';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { OrderInfoComponent } from './components/order-info/order-info.component';
import { TelegramService } from '@etop/features';
import { Fulfillment } from '@etop/models';
import { ConnectionStore } from '@etop/features';

@Component({
  selector: 'topship-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosComponent extends BaseComponent implements OnInit {
  @ViewChild(OrderInfoComponent) orderInfoComponent: OrderInfoComponent;
  @ViewChild(ShipmentServiceOptionsComponent) shipmentServiceOptionsComponent: ShipmentServiceOptionsComponent
  order_id;
  constructor(
    private posQuery: PosQuery,
    private posService: PosService,
    private toast: ToastService,
    private fulfillmentApi: FulfillmentApi,
    private navCtrl: NavController,
    private customerApi: CustomerApi,
    private orderApi: OrderApi,
    private loadingService: LoadingService,
    private util: UtilService,
    private zone: NgZone,
    private auth: AuthenticateStore,
    private telegram: TelegramService,
    private connectionStore: ConnectionStore
  ) {
    super();
  }

  ngOnInit() {
    this.posService.resetStore();
    this.posService.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.order_id = '';
      });
  }

  async ionViewWillEnter() {
    this.posService.resetStore();
    await this.posService.getPickupAddress();
    const pickupAddresses = this.posQuery.getValue().pickupAddresses;
    const shopDefaultAddressID = this.auth.snapshot.shop.ship_from_address_id;
    let pickup_address;
    if (shopDefaultAddressID && shopDefaultAddressID != '0') {
      pickup_address = pickupAddresses.find(address => address.id === shopDefaultAddressID);
    } else {
      pickup_address = pickupAddresses[0];
    }
    this.posService.selectDefaultPickup(pickup_address);
    this.posService.forceUpdateForm();
  }

  validateInfo() {
    const order = this.posQuery.getValue().order;
    const { lines  } = order;
    const ffm = this.posQuery.getValue().fulfillment;
    const { pickup_address, shipping_address, chargeable_weight, selected_service_unique_id, cod_amount  } = ffm;
    if (!pickup_address?.ward_code || !pickup_address?.address1) {
      this.toast.error('Vui lòng điền đầy đủ địa chỉ lấy hàng.');
      return false;
    }

    if (!shipping_address?.ward_code || !shipping_address?.address1) {
      this.toast.error('Vui lòng điền đầy đủ địa chỉ giao hàng.');
      return false;
    }

    if (lines?.length < 1) {
      this.toast.error('Vui lòng chọn sản phẩm.');
      return false;
    }

    if(lines.some(line => line.quantity == 0)) {
      this.toast.error('Số lượng của sản phẩm không hợp lệ. Vui lòng kiểm tra lại.');
      return false;
    }

    if(cod_amount.length == 0) {
      this.toast.error('Vui lòng điền giá trị thu hộ (COD).');
      return false;
    }

    if(Number(cod_amount) != 0 && cod_amount < 5000){
      this.toast.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5000đ');
      return false;
    }

    if (!chargeable_weight) {
      this.toast.error('Vui lòng điền khối lượng.');
      return false;
    }

    if (!selected_service_unique_id) {
      this.toast.error('Vui lòng chọn gói dịch vụ.');
      return false;
    }
    return true;
  }

  async confirm() {
    if (!this.validateInfo()) {
      return;
    }
    try {
      this.loadingService.start('Đang tạo đơn hàng');
      let order = await this.createOrder();

      let ffm = this.posQuery.getValue().fulfillment;

      const {
        pickup_address,
        shipping_address,
        chargeable_weight,
        cod_amount,
        insurance_value,
        shipping_service_code,
        shipping_service_fee,
        shipping_service_name,
        connection_id,
        try_on,
        shipping_note,
        include_insurance,
      } = ffm;
      const ffmBody: FulfillmentAPI.CreateShipmentFulfillmentRequest = {
        pickup_address,
        shipping_address,
        insurance_value,
        chargeable_weight,
        cod_amount,
        shipping_service_code,
        shipping_service_fee,
        shipping_service_name,
        connection_id,
        try_on,
        shipping_note,
        include_insurance,
        order_id: order.id,
        shipping_type: 'shipment',
        shipping_payment_type : 'seller'
      };
      const res = await this.fulfillmentApi.createShipmentFulfillment(ffmBody);
      res.fulfillment = res.map(f =>
        Fulfillment.fulfillmentMap(
          f,
          this.connectionStore.snapshot.initConnections
        )
      );
      localStorage.setItem('lastWeight', chargeable_weight.toString());
      this.telegram.sendOrderTelegramMessage(order, 'confirm',null, res.fulfillment[0]).then();
      this.loadingService.end();
      this.toast.success('Tạo đơn hàng thành công');
      this.orderInfoComponent.dropDown = false;
      this.shipmentServiceOptionsComponent.reset();
      this.zone.run(_ => {
        this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/tab-nav/pos/successfully?order_id=${order.id}`, { animated: false });
      })
    } catch (e) {
      this.loadingService.end();
      debug.log('ERROR in create Fulfillment', e);
      this.toast.error(e.message || e.msg);
    }

  }

  async createOrder() {
    const ffm = this.posQuery.getValue().fulfillment;
    let order = this.posQuery.getValue().order;
    try {
      const { shipping_address } = ffm;
      const { customer } = order;
      order.shipping = shipping_address;
      if (customer?.customer_id) {
        order.customer_id = customer.customer_id;
        delete order.customer;
        if (customer?.phone != shipping_address?.phone) {
          //CreateCustomerAddress
          await this.customerApi.createCustomerAddress({
            customer_id: customer.customer_id,
            ...shipping_address
          })
        } else {
          if (shipping_address?.address1 != customer?.address1
            || shipping_address?.full_name != customer?.full_name
            ||shipping_address?.ward_code != customer?.ward_code
          ) {
            await this.customerApi.updateCustomerAddress({
              customer_id: customer.customer_id,
              ...shipping_address
            })
            //UpdateCustomerAddress
          }
        }
      } else {
        //CreateCustomer
        try {
          const newCustomer = await this.customerApi.createCustomer(shipping_address);
          order.customer_id = newCustomer?.id;
          delete order.customer;
        }
        catch(error){
          debug.log(error);
        }
        if(order.customer_id){
          try {
          //CreateCustomerAddress
            await this.customerApi.createCustomerAddress({
              customer_id: order.customer_id,
              ...shipping_address
            })
          }
          catch(error){
            debug.log(error);
          }
        }
      }
      //CreateOrder
      let res;
      if (!this.order_id) {
        order = {
          ...order,
          payment_method: 'cod',
          source: 'ts_app'
        }
        res = await this.orderApi.createOrder(order);
        this.order_id = res.id;
      } else {
        res = await this.orderApi.updateOrder({
          ...order,
          id: this.order_id
        });
      }
      await this.orderApi.confirmOrder(res.id);
      return res;
    } catch (e) {
      this.toast.error(e.msg || e.message);
      debug.log('ERROR in createOrder', e);
    }
  }

  async ionViewDidLeave() {
    this.posService.resetStore();
  }
}
