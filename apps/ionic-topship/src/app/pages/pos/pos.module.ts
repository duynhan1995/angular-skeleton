import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PickupAddressComponent } from './components/pickup-address/pickup-address.component';
import { ShippingInfoComponent } from './components/shipping-info/shipping-info.component';
import { ProductSelectorComponent } from './components/product-selector/product-selector.component';
import { FulfillmentInfoComponent } from './components/fulfillment-info/fulfillment-info.component';
import { ShipmentServiceOptionsComponent } from './components/shipment-service-options/shipment-service-options.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PosComponent } from './pos.component';
import { SharedModule } from '../../features/shared/shared.module';
import { ListPickupAddressComponent } from './components/list-pickup-address/list-pickup-address.component';
import { EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { OrderLinesComponent } from './components/order-lines/order-lines.component';
import { AddressApi } from '@etop/api';
import { VariantSelectorComponent } from './components/variant-selector/variant-selector.component';
import { OrderInfoComponent } from './components/order-info/order-info.component';
import { ConnectionService, FulfillmentService } from '@etop/features';
import { PosSuccessfullyComponent } from './components/pos-successfully/pos-successfully.component';
import { CreateProductLiteComponent } from './components/create-product-lite/create-product-lite.component';
import { AuthenticateModule } from '@etop/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'successfully', component: PosSuccessfullyComponent },
      { path: '', component: PosComponent }
    ]
  }
]

@NgModule({
  declarations: [
    PosComponent,
    PickupAddressComponent,
    ShippingInfoComponent,
    ProductSelectorComponent,
    FulfillmentInfoComponent,
    ShipmentServiceOptionsComponent,
    ListPickupAddressComponent,
    OrderLinesComponent,
    VariantSelectorComponent,
    OrderInfoComponent,
    PosSuccessfullyComponent,
    CreateProductLiteComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    AuthenticateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    EtopPipesModule,
    SelectSuggestModule,
    IonInputFormatNumberModule

  ],
  entryComponents: [ListPickupAddressComponent],
  exports: [
    PosComponent,
    PickupAddressComponent,
    ShippingInfoComponent,
    ProductSelectorComponent,
    FulfillmentInfoComponent,
    ShipmentServiceOptionsComponent,
    OrderLinesComponent,
    OrderInfoComponent,
    PosSuccessfullyComponent
  ],
  providers: [AddressApi, FulfillmentService, ConnectionService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class PosModule {}
