import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { TRY_ON_OPTIONS } from '@etop/models/Fulfillment';
import { UserService } from 'apps/core/src/services/user.service';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { takeUntil } from 'rxjs/operators';
import { PosQuery, PosService } from '../../state';

@Component({
  selector: 'topship-fulfillment-info',
  templateUrl: './fulfillment-info.component.html',
  styleUrls: ['./fulfillment-info.component.scss']
})
export class FulfillmentInfoComponent extends BaseComponent implements OnInit {
  fulfillmentForm = this.fb.group({
    chargeable_weight: this.defaultWeight(),
    cod_amount: null,
    try_on: this.defaultTryOn(),
    shipping_note: '',
    shipping_payment_type : 'cod',
    include_insurance: false,
    shipment: 'shipment'
  });
  suggestions = false;
  total_amount = 0;
  constructor(
    private userService: UserService,
    private auth: AuthenticateStore,
    private fb: FormBuilder,
    private posService: PosService,
    private posQuery: PosQuery,
    private toastr: ToastService, 
  ) {
    super()
  }

  async ngOnInit() {
    this.posService.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getFormDataFromStore();
      });

    this.posService.forceResetCOD$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.fulfillmentForm.patchValue({
          cod_amount : null
        })
      });

      this.fulfillmentForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(ffm => {
        const _ffm = this.posQuery.getValue().fulfillment;
        this.posService.updateFulfillment({..._ffm, ...ffm});
        if (
          ffm.cod_amount != _ffm.cod_amount
          || ffm.include_insurance != _ffm.include_insurance
          || ffm.chargeable_weight != _ffm.chargeable_weight
        ) {
          this.posService.forceGetShipmentService();
        }
      });
  }

  defaultWeight() {
    return this.posService.defaultWeight();
  }

  defaultTryOn() {
    return this.posService.defaultTryOn();
  }

  getFormDataFromStore() {
    const _ffm = this.posQuery.getValue().fulfillment;
    this.fulfillmentForm.patchValue({
      chargeable_weight: _ffm.chargeable_weight || this.defaultWeight(),
      cod_amount: _ffm.cod_amount,
      try_on: _ffm.try_on || this.defaultTryOn(),
      shipping_note: _ffm.shipping_note,
      shipping_payment_type : _ffm.shipping_payment_type,
      include_insurance: _ffm.include_insurance,
      shipment: _ffm.shipment
    })
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  codSelected(amount) {
    this.fulfillmentForm.patchValue({
      cod_amount: Number(amount)
    });
    this.suggestions = false;
  }

  focusSuggest() {
    const order = this.posQuery.getValue().order;
    const { total_amount } = order;
    this.total_amount = total_amount || 0;
  }

  hideSuggest() {
    setTimeout(() => {
      this.suggestions = false;
      if(this.isInvalidCodAmount) {
        this.toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5000đ');
      }
    }, 200);
  }

  get isInvalidCodAmount() {
    const cod_amount = this.fulfillmentForm.value.cod_amount;
    if (Number(cod_amount) == 0) return false;
    return String(cod_amount).length > 0 && cod_amount < 5000;
  }
}
