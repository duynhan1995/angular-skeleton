import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { OrderApi } from '@etop/api';
import { BaseComponent } from '@etop/core';
import { OrderService } from '@etop/features';
import { Fulfillment, Order } from '@etop/models';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { LoadingService } from 'apps/ionic-topship/src/app/services/loading.service';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { PosQuery, PosService } from '../../state';
import { AddressDisplayPipe } from '@etop/shared';

@Component({
  selector: 'topship-pos-successfully',
  templateUrl: './pos-successfully.component.html',
  styleUrls: ['./pos-successfully.component.scss']
})
export class PosSuccessfullyComponent extends BaseComponent implements OnInit {

  order_id: string;
  order = new Order({});
  ffm = new Fulfillment({});
  loading = true;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private orderApi: OrderApi,
    private toast: ToastService,
    private loadingService: LoadingService,
    private util: UtilService,
    private posQuery: PosQuery,
    private posService: PosService,
    private activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private addressDisplay: AddressDisplayPipe,
  ) {
    super();
  }

  get confirmStatusImg() {
    if (this.order.confirm_status == 'P') {
      return 'assets/imgs/checked.png';
    }
    return 'assets/imgs/warning.png';
  }

  get hasError() {
    return this.order.confirm_status != 'P';
  }

  ngOnInit() {

  }

  async ionViewWillEnter() {
    const orderID = this.activatedRoute.snapshot.queryParamMap.get('order_id');
    const order = this.posQuery.getValue().shipmentConfirmOrder;
    this.order_id = orderID || order?.id;
    await this.getOrderData();
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.ffm?.shipping_code),
      0
    );
  }

  async getOrderData() {
    this.loading = true;
    try {
      this.order = await this.orderService.getOrder(this.order_id)
    } catch (e) {
      debug.error('ERROR in getting order data', e);
    }
    this.ffm = this.order.activeFulfillment;
    this.loading = false;
  }

  async retryConfirm() {
    this.loading = true;
    await this.loadingService.start('');
    try {
      await this.orderApi.confirmOrder(this.order_id, 'confirm');
      this.order = await this.orderService.getOrder(this.order_id)
    } catch (e) {
      this.loadingService.end();
      debug.error('ERROR in getting order data', e);
      this.toast.error('Có lỗi xảy ra. Xin vui lòng thử lại!').then();
    }
    this.loadingService.end();
    this.loading = false;
  }

  async newOrder() {
    let navigationExtras: NavigationExtras = {
      state: {
        fromPosSuccessComponent: true
      }
    };
    this.posService.resetStore();
    this.posService.forceUpdateForm();
    await this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/pos`, navigationExtras);
  }

  viewOrders() {
    this.posService.resetStore();
    this.posService.forceUpdateForm()
    this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/orders`).then();
  }

  get customerAddress() {
    return this.addressDisplay.transform(this.ffm.shipping_address);
  }
}
