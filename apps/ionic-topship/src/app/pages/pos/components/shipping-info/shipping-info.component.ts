import { BaseComponent } from '@etop/core';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Address, CustomerAddress, District, Province, Ward } from '@etop/models';
import { LocationQuery, LocationService } from '@etop/state/location';
import { debounceTime, map, pairwise, takeUntil } from 'rxjs/operators';
import { PosQuery, PosService } from '../../state';


@Component({
  selector: 'topship-shipping-info',
  templateUrl: './shipping-info.component.html',
  styleUrls: ['./shipping-info.component.scss']
})
export class ShippingInfoComponent extends BaseComponent implements OnInit {
  submitted = false;
  customerForm = this.fb.group({
    full_name: ['', Validators.required],
    phone: ['', Validators.required],
    province_code: ['', Validators.required],
    address1: ['', Validators.required],
    district_code: ['', Validators.required],
    ward_code: ['', Validators.required]
  })
  customerAddresses: any;
  provincesList: Province[] = [];
  districtsList: District[] = [
    { id: null, code: null, name: 'Chưa chọn tỉnh thành' }
  ];
  wardsList: Ward[] = [{ id: null, code: null, name: 'Chưa chọn quận huyện' }];
  selectedCustomer = false;
  _checkFocus = true;
  loadingSelect = false;
  constructor(
    private fb: FormBuilder,
    private posService: PosService,
    private ref: ChangeDetectorRef,
    private posQuery: PosQuery,
    private locationService: LocationService,
    private locationQuery: LocationQuery
  ) {
    super()
  }

  async ngOnInit() {
    await this.locationService.initLocations();
    this.provincesList = this.locationQuery.getValue().provincesList;
    this.posService.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getFormDataFromStore();
      });

    this.customerForm.controls.phone.valueChanges
        .pipe(
          takeUntil(this.destroy$),
          debounceTime(200),
          map(value => value)
        )
        .subscribe(async phone => {
          if (this._checkFocus && phone) {
            this.selectedCustomer = !!phone;
            this.customerAddresses = await this.posService.getCustomerAddress(
              { filter: { phone } }
            );
          }
        });
    this.customerForm.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        pairwise(),
        debounceTime(200),
        map(value => value))
      .subscribe(([prev, next]: [any, any]) => {
          if (!next.ward_code || (prev.province_code && (prev.province_code != next.province_code))) {
            this.posService.updateShippingAddress(null);
          } else {
            const ffm = this.posQuery.getValue().fulfillment;
            this.posService.updateShippingAddress({...ffm.shipping_address, ...next});
          }
          if (next.ward_code != prev.ward_code) {
            this.posService.forceGetShipmentService$.next();
          }
          this.ref.detectChanges();

      });

  }

  getFormDataFromStore() {
    const customer = this.posQuery.getValue().fulfillment?.shipping_address;
    this.customerForm.patchValue({
      full_name: customer?.full_name,
      phone: customer?.phone,
      province_code: customer?.province_code,
      address1: customer?.address1,
      district_code : customer?.district_code,
      ward_code: customer?.ward_code,
    })
  }

  get f() {
    return this.customerForm.controls;
  }

  checkFocus() {
    this._checkFocus = true;
  }

  outFocus() {
    this._checkFocus = false;
    setTimeout(() => {
      this.selectedCustomer = false;
    }, 200);
  }

  customerSelected(customerAddress) {
    this.selectedCustomer = false;
    const { phone, full_name, province_code, address1, district_code, ward_code } = customerAddress
    const order = this.posQuery.getValue().order;
    this.posService.updateShippingAddress(customerAddress);
    this.customerForm.patchValue({
      phone,
      full_name,
      province_code,
      address1,
      district_code,
      ward_code
    });
    this.posService.updateOrder({...order, customer: {...customerAddress, id: customerAddress.customer_id}});
    this.ref.detectChanges();
  }

  onProvinceSelected() {
    debug.log('onProvinceSelected')
    this.customerForm.patchValue(
      {
        district_code: '',
        ward_code: ''
      }
    );
    this.districtsList = this.locationService.filterDistrictsByProvince(
      this.customerForm.controls.province_code.value
    );
  }

  onDistrictSelected() {
    this.customerForm.patchValue(
      {
        ward_code: ''
      }
    );
    this.wardsList = this.locationService.filterWardsByDistrict(
      this.customerForm.controls.district_code.value
    );
  }
}
