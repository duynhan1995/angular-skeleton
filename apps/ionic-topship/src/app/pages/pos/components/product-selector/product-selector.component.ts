import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseComponent } from '@etop/core';
import { Product, Variant } from '@etop/models';
import { IonRouterOutlet, ModalController, PopoverController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { PosQuery, PosService } from '../../state';
import { CreateProductLiteComponent } from '../create-product-lite/create-product-lite.component';
import { VariantSelectorComponent } from '../variant-selector/variant-selector.component';

@Component({
  selector: 'topship-product-selector',
  templateUrl: './product-selector.component.html',
  styleUrls: ['./product-selector.component.scss']
})
export class ProductSelectorComponent extends BaseComponent implements OnInit {
  productQuery = new FormControl('');
  showProductList = false;
  products: any;
  product = '';

  constructor(
    private posService: PosService,
    private ref: ElementRef,
    private util: UtilService,
    private posQuery: PosQuery,
    private routerOutlet: IonRouterOutlet,
    private modalCtrl: ModalController,
    ) {
      super()
    }

  async ngOnInit() {
    // this.products = await this.posService.fetchProducts();
    this.productQuery.valueChanges.pipe(takeUntil(this.destroy$), debounceTime(300)).subscribe(async value => {

      this.showProductList = !!value;
      if (value) {
        await this.posService.searchProducts(value);
        this.products = this.posQuery.getValue().products;
      }
    });
  }

  @HostListener('window:click', ['$event'])
  detectOutsideClick(event) {
    const checkELement = this.ref.nativeElement;
    let targetElement = event.target;

    do {
      if (targetElement == checkELement) {
        return;
      }
      targetElement = targetElement.parentNode;
    } while (targetElement);
    this.showProductList = false;
  }

  priceCompare(product) {
    return this.util.compareProductPrice(product);
  }

  resetInput(){
    this.product = '';
  }

  async productSelected(product: Product) {
    if (product.variants.length == 1) {
      return this.variantSelected(product.variants[0]);
    }
    const modal = await this.modalCtrl.create({
      component: VariantSelectorComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        product
      }
    });
    modal.onWillDismiss().then(data => data.data && this.variantSelected(data.data));
    return await modal.present();
  }

  variantSelected(variant: Variant) {
    this.showProductList = false;
    this.productQuery.setValue('', { emitEvent: false });
    this.posService.addOrderLine({
      product_name: variant.product_name,
      code: variant.code,
      variant_id: variant.id,
      payment_price: variant.retail_price,
      attributes: variant.attributes,
      quantity: 1
    });
  }

  async createProduct() {
    const modal = await this.modalCtrl.create({
      component: CreateProductLiteComponent,
      backdropDismiss: false,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { productName: this.productQuery.value }
    });
    await modal.present();
    modal
      .onWillDismiss()
      .then(data => data.data && this.variantSelected(data.data.variants[0]));
  }

}
