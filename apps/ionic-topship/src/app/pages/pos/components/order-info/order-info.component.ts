import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { BaseComponent } from '@etop/core';
import { debounceTime, map, pairwise, takeUntil } from 'rxjs/operators';
import { PosQuery, PosService } from '../../state';

@Component({
  selector: 'topship-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent extends BaseComponent implements OnInit {

  dropDown = false;
  orderForm = this.fb.group({
    order_discount: null,
    fee_lines: this.fb.array([
      this.fb.group({
        type: 'other',
        name: 'Phụ thu',
        amount: null
      })
    ]),
    basket_value: 0,
    total_items: 0,
    total_discount: 0,
    total_fee: 0,
    total_amount: 0
  });
  loadData = false;

  constructor(
    private fb: FormBuilder,
    private posQuery: PosQuery,
    private posService: PosService
  ) {
    super();
  }

  get feeLines() {
    return this.orderForm?.controls.fee_lines as FormArray;
  }

  ngOnInit() {
    this.posService.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.updateInfo();
      });
    this.loadData = false;
  }

  onBlur(){
    const orderForm = this.orderForm.value;
    const _order = this.posQuery.getValue().order;
    this.posService.updateOrder({
      ..._order,
      ...orderForm
    });
    this.updateInfo();
    this.posService.forceUpdateLines();
    this.posService.forceUpdateFulfillment();
  }

  updateInfo() {
      const _order = this.posQuery.getValue().order;
      const { basket_value, total_items, total_discount, total_fee, total_amount, fee_lines, order_discount} = _order;

      if(fee_lines && fee_lines.length) {
        fee_lines.forEach(line => line.amount = line.amount && Number(line.amount))
        this.feeLines.patchValue(fee_lines, {
          emitEvent: false
        })
      } else {
        this.feeLines.patchValue([{
          type: 'other',
          name: 'Phụ thu',
          amount: null
        }], {
          emitEvent: false
        })
      }
      this.orderForm.patchValue(
        {  
          basket_value,
          total_items,
          total_discount,
          total_fee,
          total_amount,
          order_discount: order_discount && Number(order_discount)
        },
        {
          emitEvent: false
        }
      );
  }

  showDropDown() {
    this.dropDown = !this.dropDown;
  }
}
