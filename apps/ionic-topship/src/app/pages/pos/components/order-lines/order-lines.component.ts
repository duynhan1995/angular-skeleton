import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseComponent } from '@etop/core';
import { filter, takeUntil } from 'rxjs/operators';
import { PosQuery, PosService } from '../../state';

@Component({
  selector: 'topship-order-lines',
  templateUrl: './order-lines.component.html',
  styleUrls: ['./order-lines.component.scss']
})
export class OrderLinesComponent extends BaseComponent implements OnInit {

  orderLines = this.fb.array([]);

  private dataLoading = false;

  constructor(
    private fb: FormBuilder,
    private posQuery: PosQuery,
    private posService: PosService
  ) {
    super();
  }

  ngOnInit() {
    this.posService.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getFormDataFromStore();
      });
  }

  forceUpdate(event, line) {
    this.posService.updateOrderLine(line);
    this.posService.forceUpdateFulfillment();
  }

  getFormDataFromStore() {
    this.dataLoading = true;
    this.orderLines.clear();
    const order = this.posQuery.getValue().order;
    order.lines.forEach(line => {
      const group = this.fb.group(line);
      group.patchValue({
        attributes: line.attributes
      });
      this.orderLines.push(group);
    });
    this.dataLoading = false;
  }

  removeVariant(line) {
    this.posService.removeOrderLine(line);
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs
      .map(a => {
        return a.value;
      })
      .join(' - ');
  }

}
