import { Component, Input, OnInit } from '@angular/core';
import { Product } from '@etop/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'topship-variant-selector',
  templateUrl: './variant-selector.component.html',
  styleUrls: ['./variant-selector.component.scss']
})
export class VariantSelectorComponent implements OnInit {

  @Input() product: Product;

  constructor(private modalController: ModalController) {}

  ngOnInit(): void {}

  dismiss() {
    this.modalController.dismiss();
  }

  variantSelected(variant) {
    this.modalController.dismiss(variant);
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs
      .map(a => {
        return a.value;
      })
      .join(' - ');
  }

}
