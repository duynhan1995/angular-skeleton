import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validator, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { LoadingService } from 'apps/ionic-topship/src/app/services/loading.service';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { PosService } from '../../state';

@Component({
  selector: 'topship-create-product-lite',
  templateUrl: './create-product-lite.component.html',
  styleUrls: ['./create-product-lite.component.scss']
})
export class CreateProductLiteComponent implements OnInit {

  @Input() productName: string;

  productForm = this.fb.group({
    product_name: ['', Validators.required],
    price: ['', Validators.required]
  });

  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService,
    private fb: FormBuilder,
    private posService: PosService,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    this.productForm.patchValue({
      product_name: this.productName
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async submit() {
    const data = this.productForm.value;
    data.price = Number(data.price);
    if (this.productForm.controls.product_name.invalid) {
      return this.toast.error('Tên sản phẩm không được để trống');
    }
    if (this.productForm.controls['price'].value == '') {
      return this.toast.error('Vui lòng điền giá sản phẩm');
    }

    try {
      await this.loadingService.start('Đang cập nhật');
      const product = await this.posService.fastCreateProduct(data);
      setTimeout(async() => {
        await this.loadingService.end();
        this.toast.success('Tạo sản phẩm thành công.');
        this.modalCtrl.dismiss(product);
      }, 1000)
    } catch (e) {
      debug.error(e);
      this.toast.error(e.message);
    }
  }

}
