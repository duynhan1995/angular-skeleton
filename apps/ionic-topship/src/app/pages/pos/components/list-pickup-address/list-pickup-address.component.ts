import { Component, Input, OnInit } from '@angular/core';
import { Address } from '@etop/models';
import { ModalController } from '@ionic/angular';
import { PosQuery, PosService } from '../../state';

@Component({
  selector: 'topship-list-pickup-address',
  templateUrl: './list-pickup-address.component.html',
  styleUrls: ['./list-pickup-address.component.scss']
})
export class ListPickupAddressComponent implements OnInit {
  @Input() addresses: Address[]
  constructor(
    private modalCtrl: ModalController,
    private posQuery: PosQuery,
    private posService: PosService,
  ) { }

  ngOnInit(): void {
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  isSelectedAddress(address) {
    const ffm = this.posQuery.getValue().fulfillment;
    return address?.id == ffm?.pickup_address?.id;
  }

  selectAddress(address) {
    this.posService.updatePickupAddress(address);
    this.modalCtrl.dismiss();
  }

  async addAddress() {
    this.modalCtrl.dismiss({create: true});
  }

  get emptyTitle() {
    return `Chưa có địa chỉ lấy hàng`;
  }
}
