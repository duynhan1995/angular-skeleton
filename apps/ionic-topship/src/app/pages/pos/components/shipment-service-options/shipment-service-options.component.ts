import { Component, OnInit } from '@angular/core';
import { FulfillmentAPI } from '@etop/api';
import { BaseComponent } from '@etop/core';
import { ConnectionService, ConnectionStore } from '@etop/features';
import { FulfillmentShipmentService, ShipmentCarrier } from '@etop/models';
import { FulfillmentService } from 'apps/ionic-topship/src/app/services/fulfillment.service';
import { map, takeUntil } from 'rxjs/operators';
import { PosQuery, PosService } from '../../state';

@Component({
  selector: 'topship-shipment-service-options',
  templateUrl: './shipment-service-options.component.html',
  styleUrls: ['./shipment-service-options.component.scss']
})
export class ShipmentServiceOptionsComponent extends BaseComponent implements OnInit {
  carriers: ShipmentCarrier[] = [];

  getServicesIndex = 0;

  errorMessage = '';
  connectionsList$ = this.connectionStore.state$.pipe(map(s => s?.validConnections));

  fromWard = '';
  toWard = '';

  selectedShipmentService: FulfillmentShipmentService;

  constructor(
    private connectionStore: ConnectionStore,
    private ffmService: FulfillmentService,
    private posService: PosService,
    private posQuery: PosQuery,
    private connectionService: ConnectionService
  ) {
    super();
  }

  isSelectedService(service: FulfillmentShipmentService) {
    return this.selectedShipmentService?.unique_id == service?.unique_id;
  }

  async ngOnInit() {
    const connections = this.connectionStore.snapshot.validConnections;
    if (!connections) {
      await this.connectionService.getConnections();
    }
    this.posService.forceGetShipmentService$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        const ffm = this.posQuery.getValue().fulfillment;
        const order = this.posQuery.getValue().order;
        if (ffm?.shipping_address && ffm?.cod_amount !== null && order?.lines?.length) {
          this.posService.updateShippingService(null);
          this.getServices(this.posService.getServicesData(ffm));
        } else {
          this.reset();
          this.errorMessage = 'Vui lòng điền đầy đủ thông tin: địa chỉ nhận hàng, sản phẩm và giá trị thu hộ (COD)';
          this.carriers = [];
          this.posService.updateShippingService(null);
        }
      });
    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.carriers = connections?.map(conn => {
          return {
            name: conn.name,
            id: conn.id,
            logo: conn.provider_logo,
            services: [],
            from_topship: conn.connection_method == 'builtin'
          };
        });
      });
  }

  reset() {
    this.fromWard = '';
    this.toWard = '';
    this.errorMessage = '';
    this.selectedShipmentService = null;

    this.carriers = this.connectionStore.snapshot.validConnections
      .filter(conn => conn.connection_subtype == 'shipment')
      .map(conn => {
        return {
          name: conn.name,
          id: conn.id,
          logo: conn.provider_logo,
          services: [],
          from_topship: conn.connection_method == 'builtin'
        };
      });
  }

  selectService(service: FulfillmentShipmentService) {
    this.selectedShipmentService = service;
    this.posService.updateShippingService(service);
  }

  async getServices(data: FulfillmentAPI.GetShipmentServicesRequest) {
    this.reset();
    this.fromWard = data.from_ward_code;
    this.toWard = data.to_ward_code;

    if (!this.fromWard ||!this.toWard || !data.chargeable_weight) {
      return;
    }

    try {
      this.getServicesIndex++;

      const promises = this.carriers.map(carrier => async() => {

        carrier.loading = true;

        try {
          const body: FulfillmentAPI.GetShipmentServicesRequest = {
            ...data,
            connection_ids: [carrier.id],
            index: this.getServicesIndex
          };

          const res = await this.ffmService.getShipmentServices(body);
          if (res.index == this.getServicesIndex) {
            carrier.services = res.services;
            carrier = this.ffmService.sortAndFilterShipmentServices([carrier])[0];
            carrier.loading = false;
          }
        } catch(e) {
          debug.error('ERROR in Getting Shipment Services', e);
          carrier.loading = false;
        }

      });

      await Promise.all(promises.map(p => p()));
    } catch (e) {
      debug.error(e);
      this.errorMessage = e.message;
    }
  }

  get codAmount() {
    return this.posQuery.getValue().fulfillment?.cod_amount;
  }

  get isDisplayNote() {
    return parseInt(this.posQuery.getValue().fulfillment?.cod_amount) >= 0;
  }
}
