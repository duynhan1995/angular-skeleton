import { Component, OnInit } from '@angular/core';
import { AddressApi } from '@etop/api';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { AddressDisplayPipe } from '@etop/shared';
import { IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { CreateUpdateAddressPopupComponent } from 'apps/ionic-topship/src/app/components/popovers/create-update-address-popup/create-update-address-popup.component';
import { PosQuery, PosService } from '../../state';
import { ListPickupAddressComponent } from '../list-pickup-address/list-pickup-address.component';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../../../../../../core/src/services/util.service';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';

@Component({
  selector: 'topship-pickup-address',
  templateUrl: './pickup-address.component.html',
  styleUrls: ['./pickup-address.component.scss']
})
export class PickupAddressComponent extends BaseComponent implements OnInit {
  pickupAddresses$ = this.posQuery.select("pickupAddresses")

  fromPosSuccessComponent: boolean = false;
  constructor(
    private toast: ToastService,
    private navCtrl: NavController,
    private posQuery: PosQuery,
    private addressDisplay: AddressDisplayPipe,
    private posService: PosService,
    private auth: AuthenticateStore,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private addressApi: AddressApi,
    private activatedRoute: ActivatedRoute,
    private util: UtilService,
    private router: Router
  ) {
    super()
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.fromPosSuccessComponent = this.router.getCurrentNavigation().extras.state.fromPosSuccessComponent;
      }
    });
  }

  get shippingAdressViewPermission(){
    return this.auth.snapshot.permission.permissions.includes('shop/settings/shipping_setting:view');
  }

  async ngOnInit() {
    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.fromPosSuccessComponent = this.router.getCurrentNavigation().extras.state.fromPosSuccessComponent;
      }
    });
  }

  get pickupAddressDisplay() {
    const ffm = this.posQuery.getValue().fulfillment;
    return this.addressDisplay.transform(ffm.pickup_address);
  }

  async changeAddress() {
    const pickupAddresses = this.posQuery.getValue().pickupAddresses
    const modal = await this.modalCtrl.create({
      component: ListPickupAddressComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        addresses: pickupAddresses
      }
    });
    modal.onDidDismiss().then(async data => {
      if (data?.data?.create) {
        await modal.dismiss();
        this.addAddress();
      }
    });
    return await modal.present();
  }

  async addAddress() {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateAddressPopupComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        type: 'shipfrom'
      }
    });
    modal.onDidDismiss().then(async data => {
      if (data?.data?.address) {
        try{
          const address = await this.addressApi.createAddress({...data?.data?.address, type: 'shipfrom'});
          const ffm = this.posQuery.getValue().fulfillment;
          await this.posService.getPickupAddress();
          if(!ffm.pickup_address?.id){
            this.posService.updatePickupAddress(address);
          }
          this.changeAddress();
        } catch(e){
          this.toast.error(e);
        }
      }
    });
    return await modal.present();
  }

  back() {
    if(this.fromPosSuccessComponent) {
      this.fromPosSuccessComponent = false;
      this.router.navigateByUrl(`/s/${this.util.getSlug()}/tab-nav/orders`).then();
    } else {
      this.navCtrl.back();
    }
    this.posService.resetStore();
    this.posService.forceUpdateForm();
  }
}
