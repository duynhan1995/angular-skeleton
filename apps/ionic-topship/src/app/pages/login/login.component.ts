import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import {AuthenticateService, AuthenticateStore} from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { IonContent, IonFooter, ModalController, NavController, Platform } from '@ionic/angular';
import * as validatecontraints from 'apps/core/src/services/validation-contraints.service';
import { ForgotPasswordComponent } from '../../components/forgot-password/forgot-password.component';
import { NetworkStatus, Plugins } from '@capacitor/core';
import { ConfigService } from '@etop/core/services/config.service';
import { ExtendedAccount } from 'libs/models/Account';
import {AccountApi} from '@etop/api';
import { LoadingService } from '../../services/loading.service';
import { ToastService } from '../../services/toast.service';

const { Network, Keyboard } = Plugins;

enum View {
  PHONE_SUBMIT = 'phonesubmit',
  PHONE_VERIFY = 'phoneverify',
  REGISTER = 'register',
  LOGIN = 'login'
}

@Component({
  selector: 'etop-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  @ViewChild(IonFooter) footer;
  view = View;
  // tslint:disable-next-line: variable-name
  _currentView: View = View.PHONE_SUBMIT;
  phone;

  password;
  session: any;
  ionLoading = false;

  signupData: any = {
    agree_email_info: false,
    agree_tos: true
  };

  serviceTermsLink = '';
  countdown = 60;
  verify_code;
  loadingView = true;

  status: NetworkStatus;
  shop_name = '';

  constructor(
    private util: UtilService,
    private commonUsecase: CommonUsecase,
    private userService: UserService,
    private auth: AuthenticateStore,
    private config: ConfigService,
    private modalCtrl: ModalController,
    private platform: Platform,
    private zone: NgZone,
    private accountApi: AccountApi,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private toast: ToastService,
    public el: ElementRef
  ) {
    this.zone.run(async () => {
      this.status = await this.getStatus();
    });
    Network.addListener('networkStatusChange', status => {
      this.zone.run(async () => {
        this.status = status;
      });
    });
    this._currentView = View.PHONE_SUBMIT;
    this.platform.backButton.observers.pop();
  }

  async ngOnInit() {
    this._currentView = View.PHONE_SUBMIT;

    this.serviceTermsLink = await this.util.getEtopContent(0);
  }

  async getStatus() {
    return await Network.getStatus();
  }

  ionViewDidEnter() {
    this._currentView = View.PHONE_SUBMIT;
  }

  currentView(view: View) {
    return this._currentView === view;
  }

  toView(view: View) {
    this._currentView = view;
    if (view === View.PHONE_SUBMIT) {
      this.ionLoading = false;
    }
  }

  async forgotPassword() {
    // return this.toastService.warning('Tính năng đang phát triển!');
    const modal = await this.modalCtrl.create({
      component: ForgotPasswordComponent,
      componentProps: {
        phone: this.phone
      },
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
        debug.log('openResetPassword login', data);
        this.phone = data.data.phone;
      }
    });
    return await modal.present();
  }

  async checkUserRegistration(phone) {
    try {
      if (!phone) {
        throw new Error('Vui lòng nhập số điện thoại để tiếp tục!');
      }
      const recaptcha_token = await AuthenticateService.getReCaptcha(this.config.get('recaptcha_key'));
      const data = {
        phone,
        recaptcha_token
      };
      const check = await this.userService.checkUserRegistration(data);
      if (check.exists) {
        this.password = null;
        this._currentView = View.LOGIN;
        return;
      }
      this.session = await this.userService.initSession();
      this.auth.updateToken(this.session.access_token);
      await this.userService.sendPhoneVerification(phone);
      this._currentView = View.PHONE_VERIFY;
    } catch (e) {
      debug.error('ERROR in Login checkUserRegistration', e);
      throw e;
    }
  }

  async onPhoneVerify() {
    try {
      if (!this.verify_code) {
        return this.toast.error('Vui lòng nhập mã xác nhận!');
      }
      const res = await this.userService.verifyPhoneUsingToken(this.verify_code);
      if (res.code === 'fail') {
        this.toast.error(res.msg);
      } else {
        this._currentView = View.REGISTER;
      }
    } catch (e) {
      this.toast.error(e.message);
    }
  }

  async onPhoneSubmit() {
    this.ionLoading = true;
    try {
      if (!this.phone) {
        this.ionLoading = false;
        return this.toast.error('Vui lòng nhập số điện thoại!');
      }
      let isTest = this.phone.split(/-[0-9a-zA-Z]+-test$/).length > 1;
      if (isTest || this.validatePhoneNumber(this.phone)) {
        await this.checkUserRegistration(this.phone);

        if (this._currentView === View.PHONE_VERIFY) {
          this.verify_code = '';
          this.countTimeVerify();
        }
      }
    } catch (e) {
      debug.error('ERROR in Login onPhoneSubmit', e);
      this.toast.error(e.message);
    }
    this.ionLoading = false;
  }

  validatePhoneNumber(phone) {
    if (phone && phone.match(/^0[0-9]{9,10}$/)) {
      if (phone.length < 10) {
        this.toast.error('Vui lòng nhập số điện thoại di động 10 số');
      }
      return true;
    }
    this.toast.error('Vui lòng nhập số điện thoại hợp lệ');
  }

  async reSendverifyphone() {
    await this.userService.sendPhoneVerification(this.phone);
    this.countTimeVerify();
  }

  countTimeVerify() {
    this.countdown = 60;
    const interval = setInterval(() => {
      if (this.countdown < 1) {
        clearInterval(interval);
        debug.log('countdown', this.countdown);
      } else {
        this.countdown -= 1;
      }
    }, 1000);
  }

  async onLogin() {
    this.ionLoading = true;
    try {
      if (!this.password) {
        this.ionLoading = false;
        return this.toast.error('Vui lòng nhập mật khẩu!');
      }
      await this.commonUsecase.login({
        login: this.phone,
        password: this.password
      });
    } catch (e) {
      debug.log('ERROR in Login', e);
      this.ionLoading = false;
    }
    this.ionLoading = false;
  }

  async signUp() {
    this.signupData.phone = this.phone;
    const signupData = this.signupData;
    let phone = signupData.phone;
    let email = signupData.email;
    let source;
    if (this.platform.is('ios')) {
      localStorage.setItem('REF', 'ts_app_ios');
      source = 'ts_app_ios';
    } else {
      source = localStorage.setItem('REF', 'ts_app_android');
      source = 'ts_app_android';
    }

    try {
      if (!signupData.full_name) {
        return this.toast.error('Vui lòng nhập Tên đầy đủ.');
      }

      phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      phone = (phone && phone.split('-test')[0]) || '';

      if (!phone || !phone.match(/^[0-9]{9,10}$/)) {
        return this.toast.error('Số điện thoại không đúng');
      }

      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = email.split('-test')[0];

      if (!validatecontraints.EmailValidates[0].check(email)) {
        return this.toast.error('Địa chỉ email không hợp lệ.');
      }

      if (!this.shop_name) {
        return this.toast.error('Vui lòng nhập tên cửa hàng.');
      }

      if (!signupData.password) {
        return this.toast.error('Mật khẩu không được để trống.');
      }

      if (signupData.password !== signupData.confirm) {
        return this.toast.error('Mật khẩu nhập lại không chính xác.');
      }

      if (!signupData.agree_tos) {
        return this.toast.error('Vui lòng đồng ý với điều khoản sử dụng dịch vụ.');
      }

      this.loadingService.start('Đang tạo tài khoản');
      await this.commonUsecase.register(signupData, source);
      await this.createShop();
      this.loadingService.end();
      await this.commonUsecase.updateSessionInfo(true);
      this.zone.run(async () => {
        await this.navCtrl.navigateForward(
          `/survey`,
          {
            animated: false
          }
        );
      });
    } catch (e) {
      this.loadingService.end();
      return this.toast.error('Đăng ký thất bại! ' + e.message);
    }
  }

  checkPhone() {
    this._currentView = this.view.PHONE_VERIFY;
  }

  async createShop() {
    try {
      const shopData: any = {};
      shopData.money_transaction_rrule = 'FREQ=WEEKLY;BYDAY=FR';
      shopData.phone = this.phone;
      shopData.email = this.signupData.email;
      shopData.name = this.shop_name;
      const res = await this.userService.registerShop(shopData);
      const shop = new ExtendedAccount(res.shop);
      const url_slug =
        this.util.createHandle(shop.name) + '-' + shop.code.toLowerCase();
      await this.accountApi.updateURLSlug({
        account_id: shop.id,
        url_slug
      });
    } catch (e) {
      debug.log('ERROR in createShop', e);
    }
  }
}
