import { NotificationsComponent } from './notifications.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MatIconModule } from '@angular/material/icon';
import { AuthenticateModule } from '@etop/core';

const routes: Routes = [
  {
    path: '',
    component: NotificationsComponent
  }
];

@NgModule({
  declarations: [NotificationsComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule,
    AuthenticateModule,
  ],
  exports: [NotificationsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NotificationsModule {}
