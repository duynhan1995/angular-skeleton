import { Component, OnInit, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { NotiService } from '../../services/noti.service';
import { Noti } from 'libs/models/Noti';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'topship-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  content = null;
  loading = true;
  loadingMore = false;
  notifications: Array<Noti> = [];
  $destroy = new Subject();
  segment = 'all';
  accounts: any[] = [];
  accountIdNoti;
  currentAccount;
  accountNotiSlug;
  offset = 0;
  perpage = 10;
  token = this.auth.snapshot.token;
  shop_id = '';
  customOptions:any  = {
    cssClass:  'popover-select'
  }

  constructor(
    private noti: NotiService,
    private auth: AuthenticateStore,
    private router: Router,
    private util: UtilService,
    private ref: ChangeDetectorRef,
    private toast: ToastService
  ) { }

  async ngOnInit() {
    await this.loadNotifications(this.token);
    this.getListStore();
    this.currentAccount = this.auth.snapshot.account;
    this.accountIdNoti = this.currentAccount.id;
  }
  async loadNotifications(token) {
    this.loading = true;
    try {
      const notification = await this.noti.getNotifications(token, this.offset, this.perpage);
      if(this.segment !== 'all'){
        this.notifications = notification.filter(noti => noti.entity == this.segment);
      }
      else{
        this.notifications = notification;
      }
    } catch(e) {
      toastr.error('Có lỗi trong việc load thông báo. Vui lòng thử lại.');
    }
    this.loading = false;
  }

  async doRefresh(event) {
    setTimeout(async () => {
      if (this.infiniteScroll) {
        this.infiniteScroll.disabled = false;
      }
      this.offset = 0;
      await this.loadNotifications(this.token);
      event.target.complete();
    },500);
  }

  async loadMore(event) {
    this.offset += this.perpage;
    let notification = await this.noti.getNotifications(this.token, this.offset, this.perpage);
    if(this.segment !== 'all'){
      notification = notification.filter(noti => noti.entity === this.segment);
    }
    this.notifications = this.notifications.concat(notification);
    if(notification.length < this.perpage) {
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
    }
    event.target.complete();
  }

  async readNoti(noti: Noti, index: number) {
    try {
      await this.noti.updateNotifications([noti.id], true);
      this.notifications[index].is_read = true;
      switch (noti.entity) {
        case 'fulfillment':
          this.router.navigateByUrl(`/s/${this.accountNotiSlug}/orders/${noti.entity_id}`);
          break;
        case 'money_transaction_shipping':
          this.router.navigateByUrl(`/s/${this.accountNotiSlug}/tab-nav/transaction/detail/${noti.entity_id}`);
          break;
        default:
          break;
      }
    } catch(e) {
      debug.error(e);
    }
  }

  async readAllNoti() {
    try {
      const ids = this.notifications.map(noti => noti.id);
      await this.noti.updateNotifications(ids, true);
      this.notifications.forEach(noti => noti.is_read = true);
      this.noti.readAll.next();
      this.toast.success('Đánh dấu đọc tất cả tin nhắn thành công');
    } catch(e) {
      debug.error(e);
    }
  }

  async segmentChanged(event) {
    if (this.infiniteScroll) {
      this.infiniteScroll.disabled = true;
    }
    this.infiniteScroll?.complete();
    this.segment = event;
    this.offset = 0;
    await this.loadNotifications(this.token);
    this.ref.detectChanges();
  }

  async getListStore(){
    this.accounts = this.auth.snapshot.accounts;
  }

  async onSelectChange(selectedValue: any) {
    this.offset = 0;
    this.notifications = null;
    this.accountIdNoti = selectedValue;
    this.token = this.accounts.filter(account => account.id == selectedValue)[0].token;
    this.accountNotiSlug = this.accounts.filter(account => account.id == selectedValue)[0].url_slug;
    await this.loadNotifications(this.token);
    this.ref.detectChanges();
  }

}
