import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { UserApi } from '@etop/api';
import { ModalController, PopoverController } from '@ionic/angular';
import { ToastService } from '../../../../../services/toast.service';

@Component({
  selector: 'etop-require-stoken',
  templateUrl: './require-stoken.component.html',
  styleUrls: ['./require-stoken.component.scss']
})
export class RequireStokenComponent implements OnInit {

  loading = false;
  email_verified = '';
  sToken = '';

  constructor(
    private popoverController: PopoverController,
    private auth: AuthenticateStore,
    private toastr: ToastService,
    private userApi: UserApi,
    private modalCtrl: ModalController
  ) { }

  ngOnInit(): void {
  }

  async ionViewWillEnter() {
    this.email_verified = this.auth.snapshot.user.email;
  }

    dismiss() {
    this.modalCtrl.dismiss();
  }

  async submitSToken() {
    if(!this.sToken) {
      return this.toastr.error('Vui lòng nhập mã xác nhận');
    }
    this.loading = true;
    try {
      let res = await this.userApi.upgradeAccessToken(this.sToken);
      this.auth.updateSToken(res.access_token);
      this.modalCtrl.dismiss(res.access_token);
    } catch (e) {
      debug.error('ERROR in submitting sToken', e);
      this.toastr.error(e.code ? (e.message || e.msg) : '' || 'Có lỗi xảy ra');
    }
    this.loading = false;
  }

}
