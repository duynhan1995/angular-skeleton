import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { MoneyTransactionsQuery } from '@etop/state/shop/money-transaction/money-transaction.query';
import { Ticket } from '@etop/models';
import { AuthenticateStore } from '@etop/core';
import * as moment from 'moment';
import { VTigerService } from 'apps/core/src/services/vtiger.service';
import { CmsService } from 'apps/core/src/services/cms.service';
import { FulfillmentService } from '../../../../../../services/fulfillment.service';

enum TypeTicketModal {
  weeken = 'weeken',
  holiday = 'holiday',
  support = 'support',
}
@Component({
  selector: 'etop-transaction-detail-shipment',
  templateUrl: './transaction-detail-shipment.component.html',
  styleUrls: ['./transaction-detail-shipment.component.scss']
})
export class TransactionDetailShipmentComponent implements OnInit {
  // @ViewChild(IonSlides, { static: false }) slider: IonSlides;
  segment = 'info';
  loading = true;

  ffm: any;
  ffmHistories = [];

  holiday = false;
  reason_holiday = '';

  ticketNote = '';
  tickets: Array<Ticket> = [];

  transaction$ = this.moneyTransactionsQuery.selectActive();

  constructor(
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private moneyTransactionsQuery: MoneyTransactionsQuery,
    private ffmService: FulfillmentService,
    private auth: AuthenticateStore,
    private vtigerService: VTigerService,
    private cms: CmsService,
    private modalController: ModalController,
  ) {
  }

  async ngOnInit() {
    this.ticketNote = await this.cms.getTicketNote();
  }

  async ionViewWillEnter() {
    this.segment = 'info';
    await this.prepareData();
  }

  async prepareData() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    const fulfillments = this.moneyTransactionsQuery.getValue().fulfillments;
    this.ffm = fulfillments?.find(ffm => ffm.id = id);
    if (this.ffm) {
      this.ffmHistories = await this.ffmService.getFulfillmentHistory(this.ffm);
    }
    this.getTickets();
    this.getHoliday();
  }

  async getTickets() {
    this.loading = true;
    try {
      let user = this.auth.snapshot.user;
      this.tickets = await this.vtigerService.getTickets({
        etop_id: user.id,
        order_id: this.ffm.order_id
      });
    } catch (e) {
      debug.error('TICKETS ERROR', e);
    }
    this.loading = false;
  }

  async openReqSupModal() {
    const now = new Date();
    const day = now.getDay();
    const hours = now.getHours();
    let type = TypeTicketModal.support

    if (this.holiday) {
      type = TypeTicketModal.holiday
    }

    if (day == 0 || (day == 6 && hours >= 12)) {
      type = TypeTicketModal.weeken
    }

    // TODO: call ticket modal
    // const modal = await this.modalController.create({
    //   component: TicketModalComponent,
    //   componentProps: {
    //     ffm: JSON.parse(JSON.stringify(this.ffm)),
    //     type: type,
    //     note: {
    //       reason_holiday :this.reason_holiday
    //     }
    //   },
    // });
    // modal.onDidDismiss().then(() => this.getTickets());
    // await modal.present();

  }

  async ticketDetail(ticket: Ticket) {
    // TODO: call ticket detail modal
    // const modal = await this.modalController.create({
    //   component: TicketDetailComponent,
    //   componentProps: {
    //     ticket: ticket,
    //   },
    // });
    // modal.onDidDismiss().then(() => this.getTickets());
    // await modal.present();
  }

  async getHoliday() {
    try {
      const res = await fetch('https://cms-setting.etop.vn/api/get-banner').then(r => r.json());
      const now = moment(new Date()).format('DD/MM/YYYY');
      if (res.data) {
        let banner = res.data[24];
        let description = this.removeNewLineCharacters(this.stripHTML(banner.description)).split('###');
        description.forEach((item) => {
          item = item.split(',');
          for (let i = 0; i < item.length; i++) {
            if (item[i] == now) {
              this.holiday = true;
              this.reason_holiday = item[item.length - 1];
            }
          }
        })
      }
      else {
        return '';
      }
    } catch (e) {
      debug.log(e);
    }
  }

  private stripHTML(html: string) {
    const _tempDIV = document.createElement("div");
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || "";
  }

  private removeNewLineCharacters(str) {
    return str.trim().replace(/\r?\n|\r/g, '');
  }

  canForcePicking(fulfillment) {
    return this.ffmService.canForcePicking(fulfillment);
  }

  canForceDelivering(fulfillment) {
    return this.ffmService.canForceDelivering(fulfillment);
  }

  async segmentChanged(event) {
    this.segment = event;
  }

  back() {
    this.navCtrl.back();
  }


}
