import { Component, OnInit } from '@angular/core';
import { FilterOperator, Fulfillment, MoneyTransactionShop } from '@etop/models';
import { NavController } from '@ionic/angular';
import { MoneyTransactionsQuery } from '@etop/state/shop/money-transaction/money-transaction.query';
import { MoneyTransactionsService } from '@etop/state/shop/money-transaction/money-transaction.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute } from '@angular/router';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'etop-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss']
})
export class TransactionDetailComponent implements OnInit {
  activeTransaction$ = this.moneyTransactionsQuery.selectActive();
  fulfillments: Fulfillment[];
  sum;


  constructor(
    private navCtrl: NavController,
    private util: UtilService,
    private moneyTransactionsService: MoneyTransactionsService,
    private moneyTransactionsQuery: MoneyTransactionsQuery,
    private activatedRoute: ActivatedRoute,
    private auth: AuthenticateStore,
  ) {
  }

  ngOnInit(): void {
  }

  async ionViewWillEnter() {
    await this.prepareData();
    await this.moneyTransactionsService.selectedMoneyTransaction(0, 20, [
      {
        name: 'money_transaction.id',
        op: FilterOperator.eq,
        value: this.transaction?.id
      }
    ])
    this.fulfillments = this.moneyTransactionsQuery.getValue().fulfillments
    this.sum = this.moneyTransactionsQuery.getValue().sum
  }

  async prepareData() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    const hasFfm = this.moneyTransactionsQuery.hasEntity(id);
    if(!hasFfm) {
      const token = this.auth.snapshot.token;
      const moneyTransaction = await this.moneyTransactionsService.getMoneyTransaction(id, token);
      this.moneyTransactionsService.upsertMoneyTransaction(id, moneyTransaction)
      this.moneyTransactionsService.setActiveMoneyTransaction(moneyTransaction);
    }
  }

  get transaction() {
    return this.moneyTransactionsQuery.getActive();
  }

  back() {
    this.navCtrl.back();
  }

  detailShipment(ffmId) {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/tab-nav/transaction/detail/shipment/${ffmId}`
    );
  }
}
