import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { IonInfiniteScroll, NavController } from '@ionic/angular';
import { NetworkStatus, Plugins } from '@capacitor/core';
import { MoneyTransactionsService } from '@etop/state/shop/money-transaction/money-transaction.service';
import { MoneyTransactionsQuery } from '@etop/state/shop/money-transaction/money-transaction.query';
import { UtilService } from '../../../../../../../core/src/services/util.service';
const { Network } = Plugins;


@Component({
  selector: 'etop-list-transaction',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  moneyTransactions$ = this.moneyTransactionsQuery.selectAll();
  loading$ = this.moneyTransactionsQuery.selectLoading();
  offset = 0;
  shopID = '';
  perpage = 20;
  status: NetworkStatus;
  viewDisconnect = false;

  constructor(
    private authenticateStore : AuthenticateStore,
    private navCtrl: NavController,
    private util: UtilService,
    private moneyTransactionsQuery: MoneyTransactionsQuery,
    private moneyTransactionsService: MoneyTransactionsService,

  ) {}

  ngOnInit() {
  }
  async getStatus() {
    return await Network.getStatus();
  }

  async ionViewWillEnter(){
    this.offset = 0;
    this.shopID = this.authenticateStore.snapshot.shop.id
  }

  async getMoneyTransactions(){
    try {
      await this.moneyTransactionsService.getMoneyTransactions(this.shopID,this.offset,this.perpage)
    }catch (e) {
      debug.error('ERROR in list money_transactions',e);
    }
  }

  async doRefresh(event) {
    await this.getMoneyTransactions()
    event.target.complete();
  }

  async loadMore(event) {
    this.offset += this.perpage;
    await this.moneyTransactionsService.getMoneyTransactions(this.shopID,this.offset,this.perpage);
    event.target.complete();
  }

  back() {
    this.navCtrl.back();
  }

  transactionDetail(transaction){
    this.moneyTransactionsService.setActiveMoneyTransaction(transaction)
    this.navCtrl.navigateForward(
    `/s/${this.util.getSlug()}/tab-nav/transaction/detail/${transaction.id}`
  );
  }

}
