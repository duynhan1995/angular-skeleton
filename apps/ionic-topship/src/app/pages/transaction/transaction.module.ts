import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { EtopMaterialModule, EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { MoneyTransactionApi } from '@etop/api';
import { VTigerService } from 'apps/core/src/services/vtiger.service';
import {EmptyModule} from "apps/ionic-topship/src/app/components/empty/empty.module";
import { TransactionComponent } from './transaction.component';
import { MoneyTransactionsService } from '@etop/state/shop/money-transaction/money-transaction.service';
import { ShopService } from '@etop/features';
import { UpdateBankAccountComponent } from './component/update-bank-account/update-bank-account.component';
import { TransactionNotePopupComponent } from './component/transaction-note/transaction-note.component';
import { UpdateTransferScheduleComponent } from './component/update-transfer-schedule/update-transfer-schedule.component';
import { RouterModule } from '@angular/router';
import { TransactionDetailComponent } from './component/transaction-detail/transaction-detail.component';
import { TransactionDetailShipmentComponent } from './component/transaction-detail/component/transaction-detail-shipment/transaction-detail-shipment.component';
import { TransactionListComponent } from './component/transaction-list/transaction-list.component';
import { TelegramService } from '@etop/features';
import { RequireStokenComponent } from './component/update-bank-account/require-stoken/require-stoken.component';

const routes = [
  { path: 'detail/shipment/:id', component: TransactionDetailShipmentComponent },
  { path: 'detail/:id', component: TransactionDetailComponent },
  { path: 'list-transactions', component: TransactionListComponent },
  { path: '', component: TransactionComponent },

]

@NgModule({
  declarations: [
    TransactionComponent,
    UpdateBankAccountComponent,
    TransactionNotePopupComponent,
    UpdateTransferScheduleComponent,
    TransactionDetailComponent,
    TransactionDetailShipmentComponent,
    TransactionListComponent,
    RequireStokenComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    EtopMaterialModule,
    EtopPipesModule,
    IonInputFormatNumberModule,
    ReactiveFormsModule,
    EmptyModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    MobileUploader,
    OpenNativeSettings,
    VTigerService,
    MoneyTransactionsService,
    MoneyTransactionApi,
    ShopService,
    TelegramService
  ],
  exports: [
    TransactionComponent,
    UpdateBankAccountComponent,
    TransactionNotePopupComponent,
    UpdateTransferScheduleComponent,
    TransactionDetailComponent,
    TransactionDetailShipmentComponent,
    TransactionListComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransactionModule {}
