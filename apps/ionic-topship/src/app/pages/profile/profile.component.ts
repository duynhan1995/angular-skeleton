import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, NavController, AlertController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { ChangePasswordComponent } from '../../components/change-password/change-password.component';
import { resetStores } from '@datorama/akita';
import { NotificationApi } from '@etop/api';
import { Plugins } from '@capacitor/core';
import { StorageService } from 'apps/core/src/services/storage.service';
import { UserService } from '../../../../../core/src/services/user.service';
import { ToastService } from '../../services/toast.service';
const { Device } = Plugins;

@Component({
  selector: 'etop-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  userRefAff = '';
  constructor(
    private modalCtrl: ModalController,
    private alertController: AlertController,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private toast : ToastService,
    private zone: NgZone,
    private userService: UserService,
    private notificationApi: NotificationApi,
    private storage: StorageService
  ) {}

  ngOnInit() {
    this.user = this.auth.snapshot.user;
  }

  dismiss() {
    this.navCtrl.back();
  }
  async logout() {
    const alert = await this.alertController.create({
      header: 'Đăng xuất!',
      message: 'Bạn thực sự muốn đăng xuất!',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {}
        },
        {
          text: 'Đăng xuất',
          cssClass: 'text-danger font-12',
          handler: async () => {
            const oneSignal = this.auth.snapshot.oneSignal;
            if (oneSignal) {
              debug.log('deleteDevice');
              this.notificationApi.deleteDevice(oneSignal).then();
            }
            this.auth.clear();
            this.zone.run(async () => {
              resetStores();
              await this.navCtrl.navigateForward('/login', {
                animated: false
              });
            });
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async changePassword() {
    const modal = await this.modalCtrl.create({
      component: ChangePasswordComponent,
      componentProps: {
        user: this.user
      }
    });
    return await modal.present();
  }

  async updateRefAff() {
    try{
      if(!this.userRefAff){
        return this.toast.error('Vui lòng nhập mã giới thiệu')
      }
      await this.userService.changeRefAff(this.userRefAff)
      this.toast.success('Cập nhật mã giới thiệu thành công')
      this.user.ref_aff = this.userRefAff
    }catch (e) {
      this.toast.error(e.msg || e.message);
    }
  }
}
