import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonRouterOutlet,
  ModalController,
  NavController,
} from '@ionic/angular';
import {Order, ORDER_STATUS} from 'libs/models/Order';
import { ToastService } from '../../../services/toast.service';
import { OrderApi } from '@etop/api';
import { Fulfillment } from 'libs/models/Fulfillment';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { OrderQuery, OrdersService } from '@etop/state/order';
import { FulfillmentService } from '../../../services/fulfillment.service';
import { ActionType } from '@etop/models';
import { TicketService } from '@etop/state/shop/ticket';
import { OtherReasonCancelComponent } from '../components/modals/other-reason-cancel/other-reason-cancel.component';
import { FulfillmentStoreService } from '../../../../../../../libs/state/topship/fulfillment/fulfillment.service';
import { FulfillmentQuery } from './../../../../../../../libs/state/topship/fulfillment/fulfillment.query';
import { AuthenticateStore } from '@etop/core';
import { ConnectionService } from '@etop/features';
@Component({
  selector: 'etop-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
  order = new Order({});
  showList = true;
  paymentDisplay = true;
  fulfillment: Fulfillment;
  segment : 'support_request' | 'info' = 'info';
  constructor(
    private fulfillmentQuery: FulfillmentQuery,
    private fulfillmentStoreService: FulfillmentStoreService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private toast: ToastService,
    private auth: AuthenticateStore,
    private orderApi: OrderApi,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private util: UtilService,
    private zone: NgZone,
    private orderQuery: OrderQuery,
    private orderService: OrdersService,
    private changeDetector: ChangeDetectorRef,
    private ffmService: FulfillmentService,
    private connecttionService: ConnectionService,
    private ticketService: TicketService

  ) {}
  token = this.auth.snapshot.token;

  ngOnInit() {
  }

  async ionViewWillEnter() {
    await this.resetData();
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.fulfillment?.shipping_code),
      0
    );
  }

  async resetData() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.fulfillment = this.fulfillmentQuery.getEntity(id);
    if (!this.fulfillment) {
      await this.connecttionService.initConnections();
      this.fulfillment = await this.ffmService.getFulfillment(id);
    }
  }

  async action() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Tạo yêu cầu hỗ trợ',
      cssClass: 'left-align-buttons',
      buttons: [
        {
          text: `Giục lấy hàng`,
          icon:'flash-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_push_pickup, this.fulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.viewToCreateTicketPage(this.fulfillment,ActionType.ffm_push_pickup)
          }
        },
        {
          text: 'Giục giao hàng',
          icon:'flash-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_push_delivery, this.fulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.viewToCreateTicketPage(this.fulfillment,ActionType.ffm_push_delivery)
          }
        },
        {
          text: 'Đổi tiền thu hộ (COD)',
          icon:'cash-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_update_cod, this.fulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.viewToCreateTicketPage(this.fulfillment,ActionType.ffm_update_cod)
          }
        },
        {
          text: 'Đổi thông tin người nhận',
          icon:'person-circle-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_update_info, this.fulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.viewToCreateTicketPage(this.fulfillment,ActionType.ffm_update_info)
          }
        },
        {
          text: 'Yêu cầu khác',
          icon:'menu-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_other, this.fulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.viewToCreateTicketPage(this.fulfillment,ActionType.ffm_other)
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  dismiss() {
    this.navCtrl.back();
  }

  segmentChanged(event) {
    this.zone.run(() => {
      this.segment = event;
      // this.loading = true;
    });
  }

  toggleList() {
    this.showList = !this.showList;
  }

  togglePayment() {
    this.paymentDisplay = !this.paymentDisplay;
  }

  viewToCreateTicketPage(ffm: Fulfillment, actionType: ActionType) {
    this.ticketService.setActiveFulfillment(ffm);
    this.ticketService.setTicketAction(actionType);
    this.zone.run(() => {
      this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/tickets/create-ticket`
      );
    });
  }

  get isPaymentDone() {
    // TODO: temporarily doing this!!! wait for VALID order.payment_status
    return this.order.received_amount >= this.order.total_amount;
  }

  get isCancelledFfm() {
    return this.fulfillment?.shipping_status == 'P' || this.fulfillment?.shipping_status == 'N';
  }


  statusMap(status) {
    return ORDER_STATUS[status] || 'Không xác định';
  }
  async editOrder(order) {
    if (!this.enableEdit(order.fulfillments[0])) {
      return this.toast.error(
        `Không thể chỉnh sửa đơn hàng. Đơn đang ở trạng thái ${order.fulfillments[0].shipping_state_display}`
      );
    }
    this.orderService.setActiveOrder(order);
    const actionSheet = await this.actionSheetController.create({
      header: 'Thay đổi thông tin đơn hàng',
      cssClass: 'action-sheet-left',
      buttons: [
        {
          text: 'Thông tin người gửi',
          icon: 'person-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/pickup-info`
              );
            });
          }
        },
        {
          text: 'Thông tin người nhận',
          icon: 'person-outline',
          handler: async () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/shipping-info`
              );
            });
          }
        },
        {
          text: 'Tiền thu hộ (COD)',
          icon: 'wallet-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/cod`
              );
            });
          }
        },
        {
          text: 'Khai báo giá trị hàng hóa',
          icon: 'wallet-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/insurance`
              );
            });
          }
        },
        {
          text: 'Khối lượng',
          icon: 'cube-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/gross-weight`
              );
            });
          }
        },
        {
          text: 'Ghi chú',
          icon: 'chatbox-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/shipping-note`
              );
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel Clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Đơn giao hàng',
      buttons: [
        {
          text: 'Tạo yêu cầu hỗ trợ',
          role: !this.getPermissionCreateTicket() ? 'hidden' : '',
          handler: () => {
            this.action();
          }
        },
        {
          text: 'Hủy đơn giao hàng',
          role: 'destructive',
          handler: () => {
            this.ressonCancelSheet();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel Clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async ressonCancelSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Vui lòng chọn lý do hủy',
      cssClass: 'action-sheet-left',
      buttons: [
        {
          text: 'Sai thông tin',
          handler: () => {
            this.cancelOrder('Sai thông tin');
          }
        },
        {
          text: 'Khách yêu cầu hủy',
          handler: () => {
            this.cancelOrder('Khách yêu cầu hủy');
          }
        },
        {
          text: 'Tư vấn sai sản phẩm',
          handler: () => {
            this.cancelOrder('Tư vấn sai sản phẩm');
          }
        },
        {
          text: 'Hết hàng',
          handler: () => {
            this.cancelOrder('Hết hàng');
          }
        },
        {
          text: 'Lý do khác',
          handler: () => {
            this.otherReason();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async otherReason() {
    const modal = await this.modalCtrl.create({
      component: OtherReasonCancelComponent,
      backdropDismiss: false,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
      }
    });
    await modal.present();
    modal.onWillDismiss().then(data => {
      if(data && data.data){
        return this.cancelOrder(data.data);
      }
      return this.ressonCancelSheet();
    });
  }

  async cancelOrder(reason) {
    try {
      await this.fulfillmentStoreService.cancelFfm(this.fulfillment,reason, this.token);
      this.toast.success('Hủy đơn giao hàng thành công!');
      this.zone.run(_ => {
        this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/orders`).then();
      })
    } catch (e) {
      debug.log('Error when cancel fulfillment', e);
      this.toast.error('Có lỗi khi hủy đơn giao hàng. ' + e.message || e.msg);
    }
  }

  enableEdit(fulfillment: Fulfillment) {
    if (!fulfillment?.shipping_code) {
      return false;
    }
    if (fulfillment) {
      return ['Z', 'S'].includes(fulfillment.status);
    } else {
      return false;
    }
  }
  
  getPermissionCreateTicket(){
    return this.auth.snapshot.permission.permissions.includes('shop/shop_ticket:create');
  }

  back() {
    this.navCtrl.back();
  }
}
