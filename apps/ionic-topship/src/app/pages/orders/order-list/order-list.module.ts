import { NgModule } from '@angular/core';
import { OrderListComponent } from './order-list.component';
import { SharedModule } from 'apps/ionic-topship/src/app/features/shared/shared.module';
import { EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkStatusModule } from 'apps/ionic-topship/src/app/components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { FulfillmentService } from '../../../services/fulfillment.service';

@NgModule({
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    NetworkStatusModule,
    AuthenticateModule,
    ReactiveFormsModule,
    IonInputFormatNumberModule,
  ],
  declarations: [OrderListComponent],
  exports: [OrderListComponent],
  providers:[FulfillmentService]
})
export class OrderListModule{

}
