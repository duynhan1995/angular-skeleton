import { FulfillmentQuery } from './../../../../../../../libs/state/topship/fulfillment/fulfillment.query';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { FulfillmentStoreService } from '../../../../../../../libs/state/topship/fulfillment/fulfillment.service';
import { Order } from 'libs/models/Order';
import { NetworkStatus, Plugins } from '@capacitor/core';
import {
  IonRouterOutlet,
  ModalController,
  NavController,
  IonInfiniteScroll, IonContent
} from '@ionic/angular';
import { AuthenticateStore, ConfigService } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { FilterOperator, FilterOptions, Fulfillment } from '@etop/models';
import { OrderQuery, OrdersService } from '@etop/state/order';
import { ConversationsService } from '@etop/state/fabo/conversation';
import { FulfillmentService } from '../../../services/fulfillment.service';
import { FilterModalComponent } from '../../../components/filter-modal/filter-modal.component';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { NotificationApi } from '@etop/api';

const { Network, Device } = Plugins;

@Component({
  selector: 'etop-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('content') content: IonContent;
  orders: Order[] = [];
  fulfillments$ = this.fulfillmentQuery.selectAll();
  loading$ = this.fulfillmentQuery.selectLoading();
  status: NetworkStatus;
  viewDisconnect = false;
  networkStatus = true;
  orders$ = this.orderQuery.selectAll();

  offset = 0;
  perpage = 20;

  shippingState = [
    { name: 'Tất cả', value: '' },
    { name: 'Đã tạo', value: 'created' },
    { name: 'Đã xác nhận', value: 'confirmed' },
    { name: 'Đang xử lý', value: 'processing' },
    { name: 'Đang lấy hàng', value: 'picking' },
    { name: 'Chờ giao', value: 'holding' },
    { name: 'Đang giao hàng', value: 'delivering' },
    { name: 'Đang trả hàng', value: 'returning' },
    { name: 'Đã giao hàng', value: 'delivered' },
    { name: 'Đã trả hàng', value: 'returned' },
    { name: 'Không giao được', value: 'undeliverable' },
    { name: 'Hủy', value: 'cancelled' },
    { name: 'Không xác định', value: 'unknown' }
  ];

  filterOptions: FilterOptions = [
    {
      name: 'shipping_code',
      operator: FilterOperator.eq,
      value: '',
      type: 'input',
      label: 'Mã đơn giao hàng',
      placeholder: 'Nhập mã đơn giao hàng'
    },
    {
      name: 'address_to.full_name',
      operator: FilterOperator.contains,
      value: '',
      type: 'input',
      label: 'Tên người nhận',
      placeholder: 'Nhập tên người nhận'
    },
    {
      name: 'address_to.phone',
      operator: FilterOperator.contains,
      value: '',
      type: 'input',
      label: 'Số điện thoại người nhận',
      placeholder: 'Nhập số điện thoại'
    },
    {
      name: 'created_at',
      operator: FilterOperator.gte,
      value: '',
      type: 'datetime',
      label: 'Ngày tạo từ',
      placeholder: 'Nhập ngày tạo từ',
    },
    {
      name: 'created_at',
      operator: FilterOperator.lte,
      value: '',
      type: 'datetime',
      label: 'Ngày tạo đến',
      placeholder: 'Nhập ngày tạo đến'
    },
    {
      name: 'shipping_state',
      operator: FilterOperator.in,
      value: '',
      type: 'select',
      label: 'Trạng thái đơn giao hàng',
      placeholder: 'Chọn trạng thái đơn',
      options: this.shippingState
    }
    // {
    //   name: 'chargeable_weight',
    //   operator: FilterOperator.gt,
    //   value: {
    //     lower: 0,
    //     upper: 50000
    //   },
    //   type: 'ion-range',
    //   meta: {
    //     unit: 'g',
    //     min: 0,
    //     max: 50000,
    //     step: 100
    //   },
    //   label: 'Khối lượng'
    // }
  ];

  filters = [];
  constructor(
    private fulfillmentQuery: FulfillmentQuery,
    private fulfillmentStoreService: FulfillmentStoreService,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private zone: NgZone,
    private util: UtilService,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private orderQuery: OrderQuery,
    private oneSignal: OneSignal,
    private config: ConfigService,
    private notificationApi: NotificationApi,
  ) {
    this.viewDisconnect = false;
    this.zone.run(async () => {
      this.status = await this.getStatus();
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getFulfillmentByStore();
        });
      }
    });
  }

  ngOnInit() {}

  async getStatus() {
    return await Network.getStatus();
  }

  ionViewDidEnter() {
    this.content.scrollToTop(0);
  }

  async ionViewWillEnter() {
    await this.setupOneSignal();
    this.offset = 0;
    this.infiniteScroll.disabled = false;
    await this.getFulfillmentByStore();
  }

  async setupOneSignal() {
    this.oneSignal.startInit(
      this.config.getConfig().onesignal_app_id,
      this.config.getConfig().google_project_number
    );
    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.Notification
    );
    this.oneSignal.handleNotificationReceived().subscribe(data => {
      // this.badge.increase(1);
    });

    this.oneSignal.handleNotificationOpened().subscribe(data => {
      const _data: any = data.notification.payload.additionalData;
      // this.badge.decrease(1);
      if (_data.entity) {
        this.readNoti(_data);
      }
    });
    const oneSignal = this.auth.snapshot.oneSignal;
    if (!oneSignal) {
      this.oneSignal.getIds().then(async data => {
        if (data) {
          const device = await Device.getInfo();
          this.notificationApi
            .createDevice({
              device_id: device.uuid,
              device_name: device.model,
              external_device_id: data.userId
            })
            .then(_ => {
              this.auth.updateOneSignal({
                device_id: device.uuid,
                external_device_id: data.userId
              });
            });
        }
      });
    }
    this.oneSignal.endInit();
  }

  async readNoti(noti_data) {
    switch (noti_data.entity) {
      case 'fulfillment':
        this.zone.run(() => {
          this.navCtrl.navigateForward(
            `/s/${this.util.getSlug()}/orders/${noti_data.entity_id}`
          );
        });
        break;
      case 'money_transaction_shipping':
        this.zone.run(() => {
          this.navCtrl.navigateForward(
            `/s/${this.util.getSlug()}/tab-nav/transaction/detail/${noti_data.entity_id}`
          );
        });
        break;
      default:
        break;
    }
  }

  async doRefresh(event) {
    // this.infiniteScroll.disabled = false;
    // this.offset = 0;
    this.filters = [];
    this.resetFilters();
    await this.fulfillmentStoreService.getFulfillments(
      this.offset,
      this.perpage,
    );
    event.target.complete();
  }

  goToPos(){
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/tab-nav/pos`
    );
  }

  goToFfmDetail(ffm) {
    if (
      !this.auth.snapshot.permission.permissions.includes(
        'shop/fulfillment:view'
      )
    ) {
      return;
    }
    if (!this.status.connected) {
      return;
    }
    this.fulfillmentStoreService.setActiveFfm(ffm);
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/orders/${ffm.id}`);
  }

  async getFulfillmentByStore(filterOptions?) {
    try {
      const filters =
        filterOptions
          ?.filter(fo => !!fo.value)
          .map(filterOption => {
            if (filterOption.type == 'ion-range') {
              const ranges = [];
              const value = filterOption.value;
              const meta = filterOption.meta;
              if (value.lower > meta.min) {
                ranges.push({
                  name: filterOption.name,
                  op: FilterOperator.gte,
                  value: value.lower.toString()
                });
              }
              if (value.upper < meta.max) {
                ranges.push({
                  name: filterOption.name,
                  op: FilterOperator.lte,
                  value: value.upper.toString()
                });
              }
              return ranges;
            } else {
              // if(filterOptions.name === 'created_at' && filterOptions.operator == 'FilterOperator')
              return [
                {
                  name: filterOption.name,
                  op: filterOption.operator,
                  value: filterOption.value
                }
              ];
            }
          })
          .reduce((a, b) => a.concat(b), []) || [];
      this.filters = filters;
      await this.fulfillmentStoreService.getFulfillments(
        0,
        this.perpage,
        this.filters
      );
    } catch (e) {
      debug.error('ERROR in getting fulfillments', e.message);
    }
  }

  resetFilters() {
    this.infiniteScroll.disabled = false;
    this.offset = 0;
    this.filterOptions = this.filterOptions.map(filterOption => {
      if (filterOption.type == 'ion-range') {
        filterOption.value = {
          upper: filterOption.meta?.max,
          lower: filterOption.meta?.min
        };
      } else {
        filterOption.value = '';
      }
      return filterOption;
    });
  }

  async loadMore(event) {
    debug.log('loadMore');
    this.offset += this.perpage;
    const ffms: any = await this.fulfillmentStoreService.getFulfillmentsLoadMore(
      this.offset,
      this.perpage,
      this.filters
    );
    this.fulfillments$ = this.fulfillmentQuery.selectAll();
    if (ffms.length < this.perpage) {
      this.infiniteScroll.disabled = true;
    }
    event.target.complete();
  }

  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        filters: JSON.parse(JSON.stringify(this.filterOptions))
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        if (data.data.reset) {
          this.resetFilters();
        } else {
          const filters = data.data.filters;
          this.filterOptions = filters;
        }
        let created_at_to, created_at_from;
        if(this.filterOptions[3].value){
          created_at_from = this.filterOptions[3].value.split('T')[0] + 'T00:00:00+07:00';
          this.filterOptions[3].value = created_at_from;
        }
        if(this.filterOptions[4].value){
          created_at_to = this.filterOptions[4].value.split('T')[0] + 'T23:59:59+07:00';
          this.filterOptions[4].value = created_at_to;
        }
        this.getFulfillmentByStore(this.filterOptions);
      }
    });
    return await modal.present();
  }

  trackOrder(index, order: Order) {
    return order.id;
  }
}
