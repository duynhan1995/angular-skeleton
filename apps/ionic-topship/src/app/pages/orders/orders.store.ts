import { AStore } from 'apps/core/src/interfaces/AStore';
import { Injectable } from '@angular/core';

export interface OrderData {
  orders: any;
  filters: any;
  activeOrder: any;
}

@Injectable({
  providedIn: 'root'
})
export class OrderStore extends AStore<OrderData> {
  initState: OrderData = {
    orders: null,
    filters: null,
    activeOrder: null
  };
  constructor() {
    super();
  }

  setOrders(orders) {
    this.setState({ orders });
  }

  setFilters(filters) {
    this.setState({ filters });
  }

  setActiveOrder(activeOrder) {
    this.setState({ activeOrder: activeOrder });
  }
}
