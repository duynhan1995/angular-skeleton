import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ShippingService } from 'apps/ionic-topship/src/app/services/shipping.service';
import { FulfillmentApi } from '@etop/api/shop';
import { LoadingService } from 'apps/ionic-topship/src/app/services/loading.service';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { OrdersService } from '@etop/state/order';
import {TRY_ON} from "@etop/models";

@Component({
  selector: 'ionfabo-confirm-shipping-note',
  templateUrl: './confirm-shipping-note.component.html',
  styleUrls: ['./confirm-shipping-note.component.scss']
})
export class ConfirmShippingNoteComponent implements OnInit {
  @Input() fulfillment;
  @Input() note;
  shipping_fee;
  loading = true;

  constructor(
    private modalCtrl: ModalController,
    private shippingService: ShippingService,
    private orderService: OrdersService,
    private loadingService: LoadingService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async ionViewWillEnter() {
    let data: any = this.shippingService.parseFulfillmentData(this.fulfillment);
    const { shipping_note, try_on } = this.note;
    data = Object.assign({}, data, {
      shipping_note,
      try_on
    });
    this.loading = true;
    const services = await this.shippingService.getShippingServices(data);
    const service = this.shippingService.shippingServiceMap(
      services.services,
      this.fulfillment
    );
    this.shipping_fee = service.length ? service[0].fee : null;
    this.loading = false;
  }

  async confirm() {
    try {
      this.loadingService.start('Đang xử lý');
      await this.orderService.updateShippingNote(this.fulfillment, this.note);
      const order = await this.orderService.getOrder(this.fulfillment.order_id);
      this.orderService.updateOrderInfo(order);
      this.toast.success('Thay đổi thông tin thành công.');
      this.loadingService.end();
      this.modalCtrl.dismiss(true);
    } catch (e) {
      this.loadingService.end();
      this.toast.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }

  getTryOn(try_on) {
    return TRY_ON[try_on];
  }
}
