import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';

@Component({
  selector: 'topship-other-reason-cancel',
  templateUrl: './other-reason-cancel.component.html',
  styleUrls: ['./other-reason-cancel.component.scss']
})
export class OtherReasonCancelComponent implements OnInit {

  otherReason;
  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  submit(){
    if(!this.otherReason){
      return this.toast.error('Vui lòng nhập lý do');
    }
    this.modalCtrl.dismiss(this.otherReason);
  }

}
