import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ShippingService } from 'apps/ionic-topship/src/app/services/shipping.service';
import { FulfillmentApi } from '@etop/api/shop';
import { LoadingService } from 'apps/ionic-topship/src/app/services/loading.service';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { OrdersService } from '@etop/state/order';

@Component({
  selector: 'ionfabo-confirm-pickup-info',
  templateUrl: './confirm-pickup-info.component.html',
  styleUrls: ['./confirm-pickup-info.component.scss']
})
export class ConfirmPickupInfoComponent implements OnInit {
  @Input() fulfillment;
  @Input() pickup_address;
  shipping_fee;
  loading = true;

  constructor(
    private modalCtrl: ModalController,
    private shippingService: ShippingService,
    private orderService: OrdersService,
    private loadingService: LoadingService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    let data: any = this.shippingService.parseFulfillmentData(this.fulfillment);
    const { province_code, district_code, ward_code } = this.pickup_address;
    data = Object.assign({}, data, {
      from_province_code: province_code,
      from_district_code: district_code,
      from_ward_code: ward_code
    });
    this.loading = true;
    const services = await this.shippingService.getShippingServices(data);
    const service = this.shippingService.shippingServiceMap(
      services.services,
      this.fulfillment
    );
    this.shipping_fee = service.length ? service[0].fee : null;
    this.loading = false;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async confirm() {
    try {
      await this.loadingService.start('Đang xử lý');
      await this.orderService.updatePickupInfo(this.fulfillment, this.pickup_address);
      const order = await this.orderService.getOrder(this.fulfillment.order_id);
      this.orderService.updateOrderInfo(order);
      this.toast.success('Thay đổi thông tin thành công.');
      this.loadingService.end();
      this.modalCtrl.dismiss(true);
    } catch (e) {
      this.loadingService.end();
      this.toast.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }
}
