import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from '@etop/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'topship-fulfillment-journey',
  templateUrl: './fulfillment-journey.component.html',
  styleUrls: ['./fulfillment-journey.component.scss']
})
export class FulfillmentJourneyComponent implements OnInit {
  @Input() fulfillment: Fulfillment
  constructor(
    private modalCtrl: ModalController,
  ) { }


  ngOnInit(): void {
  }

  back() {
    this.modalCtrl.dismiss()
  }

  loadEvent(e) {
    // setTimeout(() => {
    //   this.loading = false;
    // }, 500);
  }

}
