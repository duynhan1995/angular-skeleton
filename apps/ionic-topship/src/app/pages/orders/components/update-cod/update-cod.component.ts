import { Component, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { NavController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfirmCodComponent } from '../modals/confirm-cod/confirm-cod.component';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { OrderQuery } from '@etop/state/order';

@Component({
  selector: 'ionfabo-update-cod',
  templateUrl: './update-cod.component.html',
  styleUrls: ['./update-cod.component.scss']
})
export class UpdateCodComponent implements OnInit {
  order: Order;
  formCod = this.fb.group({
    cod_amount: ['', Validators.required]
  });
  constructor(
    private navCtrl: NavController,
    private orderQuery: OrderQuery,
    private fb: FormBuilder,
    private routerOutlet: IonRouterOutlet,
    private modalController: ModalController,
    private toast: ToastService
  ) { }

  ngOnInit(): void {
  }

  async ionViewWillEnter() {
    this.order = this.orderQuery.getActive();
    // tslint:disable-next-line: max-line-length
    this.formCod.patchValue({
      cod_amount: this.order.fulfillments[0].cod_amount
    });
  }

  back() {
    this.navCtrl.back();
  }

  async submit() {
    const cod = this.formCod.controls['cod_amount'].value;
    if (Number(cod) < 5000 && (Number(cod) > 0)) {
      return this.toast.error('Giá trị thu hộ phải lớn hơn 5.000đ hoặc bằng 0');
    }
    if (!this.formCod.controls['cod_amount'] || Number(cod) < 0) {
      return this.toast.error('Giá trị thu hộ không hợp lệ');
    }
    if (this.order.fulfillments[0].cod_amount != cod) {
      const modal = await this.modalController.create({
        component: ConfirmCodComponent,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          fulfillment: this.order.fulfillments[0],
          cod_amount: this.formCod.controls['cod_amount'].value
        }
      });
      modal.onWillDismiss().then(async data => {
        if (data?.data) {
          this.navCtrl.back();
        }
      });
      return await modal.present();
    } else {
      this.toast.error('Vui lòng nhập thay đổi thông tin');
    }
  }
}
