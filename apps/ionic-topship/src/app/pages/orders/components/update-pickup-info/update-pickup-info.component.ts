import { Component, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { NavController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfirmPickupInfoComponent } from '../modals/confirm-pickup-info/confirm-pickup-info.component';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { OrderQuery } from '@etop/state/order';
import {LocationQuery, LocationService} from '@etop/state/location';

@Component({
  selector: 'ionfabo-update-pickup-info',
  templateUrl: './update-pickup-info.component.html',
  styleUrls: ['./update-pickup-info.component.scss']
})
export class UpdatePickupInfoComponent implements OnInit {
  provinces;
  districts;
  wards;
  order: Order;
  pickup_address = this.fb.group({
    full_name: ['', Validators.required],
    phone: ['', Validators.required],
    address1: ['', Validators.required],
    province_code: ['', Validators.required],
    province: '',
    district_code: ['', Validators.required],
    district: '',
    ward_code: ['', Validators.required],
    ward: ''
  });
  loading = true;
  constructor(
    private navCtrl: NavController,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private orderQuery: OrderQuery,
    private fb: FormBuilder,
    private routerOutlet: IonRouterOutlet,
    private modalController: ModalController,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  dismiss() {
    this.navCtrl.back();
  }

  async ionViewWillEnter() {
    this.loading = true;
    this.order = this.orderQuery.getActive();
    // tslint:disable-next-line: max-line-length
    const {
      full_name,
      phone,
      address1,
      province_code,
      province,
      district_code,
      district,
      ward_code,
      ward
    } = this.order.fulfillments[0].pickup_address;
    this.pickup_address.setValue({
      full_name,
      phone,
      address1,
      province_code,
      province,
      district_code,
      district,
      ward_code,
      ward
    });
    this._prepareLocationData();
    this.loading = false;
  }

  _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.districts = this.locationService.filterDistrictsByProvince(
      this.pickup_address.controls.province_code.value
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.pickup_address.controls.district_code.value
    );
  }

  onProvinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(
      this.pickup_address.controls.province_code.value
    );
    this.pickup_address.patchValue({
      district_code: '',
      ward_code: ''
    });
  }

  onDistrictSelected() {
    this.wards = this.locationService.filterWardsByDistrict(
      this.pickup_address.controls.district_code.value
    );
    debug.log('wards', this.wards);
    this.pickup_address.patchValue({
      ward_code: ''
    });
  }

  async submit() {
    if (this.pickup_address.invalid) {
      return this.toast.error('Thông tin không được để trống');
    }
    const province = this.provinces.find(
      p => p.code == this.pickup_address.controls.province_code.value
    ).name;
    const district = this.districts.find(
      d => d.code == this.pickup_address.controls.district_code.value
    ).name;
    const ward = this.wards.find(
      w => w.code == this.pickup_address.controls.ward_code.value
    ).name;
    this.pickup_address.patchValue({
      province,
      district,
      ward
    });

    const _checkDifferent = this.checkDifferent(
      this.order.fulfillments[0].pickup_address,
      this.pickup_address.value
    );
    if (_checkDifferent) {
      const modal = await this.modalController.create({
        component: ConfirmPickupInfoComponent,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          fulfillment: this.order.fulfillments[0],
          pickup_address: this.pickup_address.value
        }
      });
      modal.onWillDismiss().then(async data => {
        if (data?.data) {
          this.navCtrl.back();
        }
      });
      return await modal.present();
    } else {
      return this.toast.error('Vui lòng thay đổi thông tin');
    }
  }

  checkDifferent(oldData, newData) {
    return (
      oldData.full_name != newData.full_name ||
      oldData.phone != newData.phone ||
      oldData.address1 != newData.address1 ||
      oldData.ward_code != newData.ward_code
    );
  }
}
