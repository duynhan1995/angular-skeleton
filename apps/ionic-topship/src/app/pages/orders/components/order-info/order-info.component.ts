import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from '@etop/models';
import { FulfillmentJourneyComponent } from '../fulfillment-journey/fulfillment-journey.component';
import { IonRouterOutlet, ModalController } from '@ionic/angular';

@Component({
  selector: 'topship-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent implements OnInit {
  @Input() fulfillment: Fulfillment
  constructor(
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet
  ) { }

  ngOnInit(): void {
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.fulfillment?.shipping_code),
      0
    );
  }

  async openFfmJourneyModal() {
    const modal = await this.modalCtrl.create({
      component: FulfillmentJourneyComponent,
      componentProps: {
        fulfillment:this.fulfillment
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    modal.onDidDismiss().then();
    return await modal.present();
  }


}
