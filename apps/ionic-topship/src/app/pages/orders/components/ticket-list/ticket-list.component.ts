import { Component, Input, NgZone, OnInit } from '@angular/core';
import { TicketQuery, TicketService, TicketStore } from '@etop/state/shop/ticket';
import {Paging} from "@etop/shared";
import { Fulfillment, TicketRefType } from '@etop/models';
import { ShopTicketAPI } from '@etop/api';
import { NavController } from '@ionic/angular';
import { UtilService } from '../../../../../../../core/src/services/util.service';

@Component({
  selector: 'topship-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {
  @Input() fulfillment: Fulfillment

  ticket_list$ = this.ticketQuery.selectAll()
  constructor(
    private ticketService: TicketService,
    private ticketQuery: TicketQuery,
    private ticketStore: TicketStore,
    private zone: NgZone,
    private navCtrl: NavController,
    private util: UtilService
  ) { }

  async ngOnInit() {
    await this.loadTickets()
  }

  async loadTickets() {
    const paging: Paging = {
      offset: 0,
      limit: 1000,
    }
    this.ticketService.setPaging(paging)

    const filter: ShopTicketAPI.GetTicketsFilter= {
      ref_id: this.fulfillment.id,
      ref_type: TicketRefType.ffm
    }
    this.ticketService.setFilter(filter)

    await this.ticketService.getTickets()
  }

  async doRefresh(event) {
    const paging: Paging = {
      offset: 0,
      limit: 1000,
    }
    const filter: ShopTicketAPI.GetTicketsFilter= {
      ref_id: this.fulfillment.id,
      ref_type: TicketRefType.ffm
    }
    this.ticketService.setPaging(paging)
    this.ticketService.setFilter(filter)
    await this.ticketService.getTickets()
    event.target.complete();
  }

  get isDirectOrder() {
    return !this.fulfillment.from_topship;
  }

  viewToTicketDetail(ticket) {
    this.ticketStore.setActive(ticket.id);
    this.zone.run(() => {
      this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/tickets/ticket-detail`
      );
    });
  }
}
