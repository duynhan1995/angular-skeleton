import { Component, OnInit, NgZone, Input } from '@angular/core';
import {resetStores} from "@datorama/akita";
import { AlertController, NavController, ActionSheetController, ModalController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { AuthorizationApi } from '@etop/api';
import { Invitation } from 'libs/models/Authorization';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { ToastService } from '../../../services/toast.service';
import { NewShopComponent } from '../new-shop/new-shop.component';

@Component({
  selector: 'etop-list-store',
  templateUrl: './list-store.component.html',
  styleUrls: ['./list-store.component.scss']
})
export class ListStoreComponent implements OnInit {
  @Input() tab;
  accounts: any[] = [];
  currentShop: any = {};
  shopId;
  segment = 'owner';
  listInvite: any;
  userInvitations: Invitation[] = [];
  myStore: any = [];
  loading = true;

  constructor(
    private auth: AuthenticateStore,
    private alertController: AlertController,
    private zone: NgZone,
    private navCtrl: NavController,
    private userService: UserService,
    private authorizationApi: AuthorizationApi,
    private util: UtilService,
    private toast: ToastService,
    private actionSheetController: ActionSheetController,
    private router: Router,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.accounts = this.auth.snapshot.accounts;
    this.currentShop = this.auth.snapshot.account;
    this.shopId = this.currentShop.id;
    debug.log('accounts', this.accounts)
  }

  async ionViewDidEnter() {
    this.loading = true;
    if (this.tab) {
      this.segment = this.tab;
    }
    this.currentShop = this.auth.snapshot.account;
    this.shopId = this.currentShop.id;
    await this.prepareSession();
  }
  async prepareSession() {
    this.accounts = this.auth.snapshot.accounts;
    this.loading = false;
  }
  dismiss() {
    this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/account`);
  }

  isOwner(account) {
    if (account.permission) {
      return account.permission.roles.indexOf('owner') != -1;
    }
  }

  segmentChanged(event) {
    this.segment = event;
  }

  rolesDisplay(roles: string[]) {
    if (roles && roles.length) {
      return roles.map(r => AuthorizationApi.roleMap(r)).join(', ');
    }
    return 'Chủ shop';
  }

  async switchShop(event, account) {
    const alert = await this.alertController.create({
      header: 'Chuyển đổi cửa hàng.',
      message: `Bạn có chắc muốn chuyển sang cửa hàng <strong>"${account.shop.name}"</strong>`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            event.target.checked = false;
          }
        },
        {
          text: 'Truy cập',
          cssClass: 'text-primary font-12',
          handler: async () => {
            const index = this.auth.findAccountIndex(account.id)
            this.auth.selectAccount(index);
            let res =  await this.userService.switchAccount(account.id);
            this.auth.updateToken(res.access_token);
            this.auth.updatePermissions(res.account.user_account.permission);
            resetStores();
            this.zone.run(async () => {
              await this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug || index}/tab-nav/orders`)
            });
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  get hasAnyShop() {
    const roles = this.auth.snapshot.accounts.map(acc => acc?.user_account?.permission?.roles);
    return roles.some(roleArr => roleArr.includes('owner'));
  }

  async newShop() {
    const modal = await this.modalCtrl.create({
      component: NewShopComponent,
      componentProps: {},
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.prepareSession();
        this.segment = 'owner';
      }
    });
    return await modal.present();
  }
}
