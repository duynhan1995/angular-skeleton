import { Component, OnInit } from '@angular/core';
import { ToastService } from '../../../services/toast.service';
import { NotifySetting } from 'libs/models/NotifySetting';
import { NotifySettingService } from '@etop/features';
import { Plugins, PermissionType } from '@capacitor/core';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { NavController } from '@ionic/angular';

const { Permissions } = Plugins;

const topic_topship = [
  {
    topic: 'fulfillment',
    title: 'Đơn hàng',
    description:
      'Nhận thông báo khi cập nhật mới về trạng thái, thông tin đơn hàng'
  },
  {
    topic: 'money_transaction',
    title: 'Phiên chuyển tiền',
    description:
      'Nhận thông báo khi cập nhật mới về trạng thái, thông tin phiên chuyển tiền'
  },
  {
    topic: 'system',
    title: 'Hệ thống',
    description:
      'Nhận thông báo chung từ Topship về chính sách, tính năng, chương trình khuyến mãi, ...'
  }
];

@Component({
  selector: 'ionfabo-notification-setting',
  templateUrl: './notification-setting.component.html',
  styleUrls: ['./notification-setting.component.scss']
})
export class NotificationSettingComponent implements OnInit {
  topics: NotifySetting[];
  permissionNoti;

  constructor(
    private navCtrl: NavController,
    private toast: ToastService,
    private notiService: NotifySettingService,
    private openNativeSettings: OpenNativeSettings
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.getSettings();
    this.permissionNoti = await this.getPermisstion();
  }

  async getPermisstion() {
    const permission = await Permissions.query({
      name: PermissionType.Notifications
    });
    return permission.state;
  }

  async goSettingApp() {
    await this.openNativeSettings.open('application_details');
  }

  back() {
    this.navCtrl.back();
  }

  toggleSetting(topic) {
    if (topic.enable) {
      this.disableTopic(topic);
    } else {
      this.enableTopic(topic);
    }
  }

  mapData(topics: NotifySetting[]) {
    return topics.map(topic => this.mapTopic(topic));
  }

  mapTopic(topic: NotifySetting) {
    return {
      ...topic,
      title: this.getTopic(topic).title,
      description: this.getTopic(topic).description
    };
  }

  getTopic(topic): any {
    return topic_topship.find(t => t.topic == topic.topic) || '';
  }

  async enableTopic(topic) {
    try {
      const res = await this.notiService.enableNotifyTopic(topic.topic);
      this.topics.find(t => t.topic == topic.topic).enable = true;
      debug.log('res', res);
      this.toast.success(`Bật thông báo ${topic.title} thành công`);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in enableTopic', e);
    }
  }

  async disableTopic(topic) {
    try {
      const res = await this.notiService.disableNotifyTopic(topic.topic);
      debug.log('res', res);
      this.topics.find(t => t.topic == topic.topic).enable = false;
      this.toast.success(`Tắt thông báo ${topic.title} thành công`);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in disableTopic', e);
    }
  }

  async getSettings() {
    try {
      const _topics = await this.notiService.getNotifySetting();
      this.topics = this.mapData(_topics);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in getNotifySetting', e);
    }
  }
}
