import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { NavController, ActionSheetController, PopoverController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { map } from 'rxjs/operators';
import { Address } from 'libs/models/Address';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { ToastService } from '../../../services/toast.service';
import { CreateUpdateAddressPopupComponent } from '../../../components/popovers/create-update-address-popup/create-update-address-popup.component';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-delivery-address',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss']
})
export class DeliveryAddressComponent implements OnInit {
  currentShop: any;

  addressDeleting = false;

  fromAddress$ = this.confirmOrderStore.state$.pipe(map(s => s?.fromAddresses));
  popover: HTMLIonPopoverElement;

  empty = {
    title: 'Chưa có địa chỉ lấy hàng',
    subtitle: 'Vui lòng thêm địa chỉ lấy hàng để sử dụng dịch vụ',
    action: 'Tạo địa chỉ',
    buttonClick: this.createAddress.bind(this)
  };

  constructor(
    private navCtrl: NavController,
    private actionSheetController: ActionSheetController,
    private confirmOrderService: ConfirmOrderService,
    private auth: AuthenticateStore,
    private confirmOrderStore: ConfirmOrderStore,
    private shopService: ShopService,
    private toast: ToastService,
    private zone: NgZone,
    private popoverController: PopoverController,
    private util: UtilService,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private changeDetector: ChangeDetectorRef
  ) {}

  async ngOnInit() {
    await this.prepareAddress();
  }

  back() {
    this.navCtrl.back();
  }

  async prepareAddress() {
    await this.confirmOrderService.getFromAddresses();
    this.currentShop = this.auth.snapshot.shop;
  }

  async action(address) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Địa chỉ lấy hàng',
      buttons: [
        {
          text: 'Chỉnh sửa',
          handler: () => {
            this.popover = null;
            setTimeout(() => this.editAddress(address));
          }
        },
        {
          text: 'Xóa địa chỉ',
          role: 'destructive',
          handler: () => setTimeout(() => this.removeAddress(address))
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async setDefaultAddress(address: Address, type) {
    try {
      await this.shopService.setDefaultAddress(address.id, type);
      await this.auth.setDefaultAddress(address.id, type, this.currentShop.id);
      this.currentShop = this.auth.snapshot.shop;
      this.toast.success('Cập nhật địa chỉ mặc định thành công!');
    } catch (e) {
      this.toast.error('Chọn địa chỉ thất bại: ' + e.message);
    }
  }

  async removeAddress(address: Address) {
    if (this.addressDeleting) {
      return;
    }
    this.addressDeleting = true;
    try {
      if (this.currentShop.ship_from_address_id === address.id) {
        return this.toast.error('Không được xóa địa chỉ mặc định');
      }
      await this.confirmOrderService.removeAddress(address.id);
      this.zone.run(() => {
        this.prepareAddress();
      });
      this.toast.success('Xóa địa chỉ thành công!');
    } catch (e) {
      this.toast.error('Xóa địa chỉ thất bại: ' + e.message);
    }
    this.addressDeleting = false;
  }

  async createOrUpdateAddress(type: 'shipfrom' | 'to', address: Address) {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async data => {
      if (data.data?.address) {
        const _address = data.data?.address;
        await this.createOrUpdateShopAddress(_address);
        this.zone.run(async () => {
          await this.prepareAddress();
          this.changeDetector.detectChanges();
        });
      }
    });
    await modal.present().then();
  }

  createAddress() {
    this.createOrUpdateAddress('shipfrom', new Address({}));
  }

  async editAddress(address) {
    let _address = this.util.deepClone(address);
    await this.createOrUpdateAddress('shipfrom', _address);
  }

  async createOrUpdateShopAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateFromAddress(address);
    } else {
      await this.confirmOrderService.createFromAddress(address);
    }
  }
}
