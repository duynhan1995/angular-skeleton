import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, AlertController, PopoverController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { CarrierConnectComponent } from '../../../components/carrier-connect/carrier-connect.component';
import { map } from 'rxjs/operators';
import { ConnectionAPI } from '@etop/api';
import { LoadingService } from '../../../services/loading.service';
import { ToastService } from '../../../services/toast.service';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { Connection, ShopConnection } from 'libs/models/Connection';
import { LocationQuery, LocationService } from '@etop/state/location';
import LoginShopConnectionWithOTPRequest = ConnectionAPI.LoginShopConnectionWithOTPRequest;
import LoginShopConnectionRequest = ConnectionAPI.LoginShopConnectionRequest;
import LoginShopConnectionByTokenRequest = ConnectionAPI.LoginShopConnectionByTokenRequest;
import RegisterShopConnectionRequest = ConnectionAPI.RegisterShopConnectionRequest;
import { CarrierConnectionWithOtpModalComponent } from './components/carrier-connection-with-otp-modal/carrier-connection-with-otp-modal.component';
import { CarrierConnectionModalComponent } from './components/carrier-connection-modal/carrier-connection-modal.component';
import { DeleteConnectionModalComponent } from './components/delete-connection-modal/delete-connection-modal.component';


enum LoginByOTPStep {
  phone= 'phone',
  otp = 'otp'
}

@Component({
  selector: 'etop-carrier-connection',
  templateUrl: './carrier-connection.component.html',
  styleUrls: ['./carrier-connection.component.scss']
})
export class CarrierConnectionComponent implements OnInit {
  @ViewChild("connectionModal") connectionModal: ElementRef;
  @ViewChild("connectionWithOTPModal") connectionWithOTPModal: ElementRef;
  @ViewChild("deleteConnectionModal") deleteConnectionModal: ElementRef;

  connections: Connection[] = [];
  selectedConnection: Connection;

  tab: 'login' | 'register' = 'login';

  countDown = 600;
  timer: string;
  loginByOTPStep: LoginByOTPStep = LoginByOTPStep.phone;

  loginByOtp = new LoginShopConnectionWithOTPRequest();
  loginInfo = new LoginShopConnectionRequest();
  loginByToken = new LoginShopConnectionByTokenRequest();
  registerInfo = new RegisterShopConnectionRequest();

  loggedInConnections$ = this.connectionStore.state$.pipe(
    map(s => s?.loggedInConnections)
  )

  availableConnections$ = this.connectionStore.state$.pipe(
    map(s => s?.availableConnections)
  )
  constructor(
    private navCtrl: NavController,
    private popoverController: PopoverController,
    private connectionStore: ConnectionStore,
    private loading: LoadingService,
    private connectionService: ConnectionService,
    private toast: ToastService,
    private alertController: AlertController,
    private changeDetector: ChangeDetectorRef,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private locationService: LocationService,
    private locationQuery: LocationQuery,
  ) {}


  async ngOnInit() {
    await this.getConnections();
  }

  back() {
    this.navCtrl.back();
  }

  async getConnections() {
    try {
      await this.loadProvinces();
      await this.connectionService.getConnections();
    } catch(e) {
      debug.error('ERROR in Getting Connections', e);
    }
  }

  dismiss() {
    this.navCtrl.back();
  }

  async loadProvinces() {
    try {
      await this.locationService.listProvinces()
    } catch (e) {
      debug.error(e);
    }
  }

  async openDeleteConnectionModal(connection) {
    const popover = await this.popoverController.create({
      component: DeleteConnectionModalComponent,
      componentProps: {
        connection
      },
      translucent: true,
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    popover.onDidDismiss().then(() => {
    });
    await popover.present().then();
  }

  async showConnectionModal(connection: Connection) {
    if(connection.connection_provider == 'ghn') {
      const popover = await this.popoverController.create({
        component: CarrierConnectionWithOtpModalComponent,
        componentProps: {
          connection
        },
        cssClass: 'center-popover',
        animated: true,
        showBackdrop: true,
        backdropDismiss: false
      });
      popover.onDidDismiss().then(() => {
      });
      await popover.present().then();
    } else {
      const popover = await this.popoverController.create({
        component: CarrierConnectionModalComponent,
        componentProps: {
          connection
        },
        cssClass: 'center-popover',
        animated: true,
        showBackdrop: true,
        backdropDismiss: false
      });
      popover.onDidDismiss().then(() => {
      });
      await popover.present().then();
    }
  }

  editPhone() {
    this.loginByOTPStep = LoginByOTPStep.phone;
    this.loginByOtp.otp = '';
  }

  get provinces() {
    return this.locationQuery.getValue().provincesList
  }

  isConnectedConnection(connection) {
    let logInConnectionIds = this.connectionStore.snapshot.loggedInConnections.map(conn => conn.connection_id)
    return logInConnectionIds.indexOf(connection.id) != -1
  }
}
