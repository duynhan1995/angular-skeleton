import { Component, Input, OnInit } from '@angular/core';
import { Connection } from '@etop/models';
import { ConnectionAPI } from '@etop/api';
import LoginShopConnectionWithOTPRequest = ConnectionAPI.LoginShopConnectionWithOTPRequest;
import { ConnectionService } from '@etop/features';
import { ModalController, PopoverController } from '@ionic/angular';
import { ToastService } from '../../../../../services/toast.service';

enum LoginByOTPStep {
  phone= 'phone',
  otp = 'otp'
}

@Component({
  selector: 'app-carrier-connection-with-otp-modal',
  templateUrl: './carrier-connection-with-otp-modal.component.html',
  styleUrls: ['./carrier-connection-with-otp-modal.component.scss']
})
export class CarrierConnectionWithOtpModalComponent implements OnInit {
  @Input() connection: Connection

  loginByOTPStep: LoginByOTPStep = LoginByOTPStep.phone;

  loginByOtp = new LoginShopConnectionWithOTPRequest();
  countDown = 600;
  timer: string;
  loading = false;
  constructor(
    private connectionService: ConnectionService,
    private popoverController: PopoverController,
    private toast: ToastService
  ) { }

  ngOnInit() {
  }

  private validateLoginByOTPInfo(data: LoginShopConnectionWithOTPRequest, step: LoginByOTPStep) {
    if (!data.identifier) {
      this.toast.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    if (!data.otp && step == LoginByOTPStep.otp) {
      this.toast.error('Vui lòng nhập OTP!');
      return false;
    }
    return true;
  }

  editPhone() {
    this.loginByOTPStep = LoginByOTPStep.phone;
    this.loginByOtp.otp = '';
  }

  async retrySendOtp() {
    const { identifier } = this.loginByOtp;
    await this.connectionService.loginConnection({
      connection_id: this.connection.id,
      identifier
    });

    this.countDown = 600;
    this.startTimer();
  }

  startTimer() {
    const interval = setInterval(() => {
      if (this.countDown < 1) {
        clearInterval(interval);
      } else {
        this.timer = this.displayCount(this.countDown);
        this.countDown -= 1;
      }
    }, 1000);
  }

  displayCount(timer) {
    const minutes = Math.floor(timer / 60);
    const seconds = Math.floor(timer % 60);
    let _seconds: any = seconds;
    if (seconds < 10) {
      _seconds = '0' + seconds;
    }
    return minutes + ':' + _seconds + 's';
  }

  closeConnectionModal() {
    this.popoverController.dismiss()
  }

  async loginUsingOTP() {
    this.loading = true;
    try {
      if (!this.validateLoginByOTPInfo(this.loginByOtp, this.loginByOTPStep)) {
        return this.loading = false;
      }

      const { identifier, otp } = this.loginByOtp;

      if (this.loginByOTPStep == LoginByOTPStep.phone) {
        await this.connectionService.loginConnection({
          connection_id: this.connection.id,
          identifier
        });
        this.loginByOTPStep = LoginByOTPStep.otp;
        this.countDown = 600;
        this.startTimer();
        return this.loading = false;
      } else {
        await this.connectionService.loginShopConnectionWithOTP({
          connection_id: this.connection.id,
          identifier, otp
        });
      }

      this.toast.success(`Kết nối với ${this.connection.name} thành công.`);
      this.closeConnectionModal();
    } catch(e) {
      debug.error('ERROR in loginUsingOTP', e);
      this.loading = false;
      this.toast.error(
        `Kết nối với ${this.connection.name} không thành công.`+
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }
}
