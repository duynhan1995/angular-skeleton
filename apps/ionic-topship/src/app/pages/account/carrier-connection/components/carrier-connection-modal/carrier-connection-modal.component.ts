import { Component, OnInit, Input } from '@angular/core';
import { Connection } from '@etop/models';
import { ConnectionAPI } from '@etop/api';
import LoginShopConnectionByTokenRequest = ConnectionAPI.LoginShopConnectionByTokenRequest;
import { ModalController, PopoverController } from '@ionic/angular';
import LoginShopConnectionRequest = ConnectionAPI.LoginShopConnectionRequest;
import RegisterShopConnectionRequest = ConnectionAPI.RegisterShopConnectionRequest;
import { ToastService } from '../../../../../services/toast.service';
import { ConnectionService } from '@etop/features';
import { LocationQuery, LocationService } from '@etop/state/location';

@Component({
  selector: 'app-carrier-connection-modal',
  templateUrl: './carrier-connection-modal.component.html',
  styleUrls: ['./carrier-connection-modal.component.scss']
})
export class CarrierConnectionModalComponent implements OnInit {
  @Input() connection: Connection

  loading = false;
  tab: 'login' | 'register' = 'login';

  loginByToken = new LoginShopConnectionByTokenRequest();
  loginInfo = new LoginShopConnectionRequest();
  registerInfo = new RegisterShopConnectionRequest();

  selectedProvinceCode: string = null;
  selectedDistrictCode: string = null;
  selectedWardCode: string = null;

  provinces = []
  districts = []
  wards = []
  constructor(
    private popoverController: PopoverController,
    private toast: ToastService,
    private connectionService: ConnectionService,
    private locationQuery: LocationQuery,
    private locationService: LocationService
  ) { }

  ngOnInit() {
    this.provinces = this.locationQuery.getValue().provincesList
  }



  private validateLoginByTokenInfo(data) {
    if (!data.user_id) {
      this.toast.error('Vui lòng nhập User ID!');
      return false;
    }
    if (!data.token) {
      this.toast.error('Vui lòng nhập Token!');
      return false;
    }
    return true;
  }

  private validateLoginInfo(data) {
    if (!data.email) {
      this.toast.error('Vui lòng nhập email!');
      return false;
    }
    if (!data.password) {
      this.toast.error('Vui lòng nhập password!');
      return false;
    }
    return true;
  }

  private validateRegisterInfo(data, provider: string) {
    if (!data.name) {
      this.toast.error('Vui lòng nhập họ tên!');
      return false;
    }
    if (!data.phone) {
      this.toast.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    if (!data.email) {
      this.toast.error('Vui lòng nhập số email!');
      return false;
    }
    if (!data.password && provider == 'ghn') {
      this.toast.error('Vui lòng nhập mật khẩu!');
      return false;
    }
    if (!data.province) {
      this.toast.error('Vui lòng nhập tỉnh thành!');
      return false;
    }
    if (!data.district) {
      this.toast.error('Vui lòng nhập quận huyện!');
      return false;
    }
    if (!data.address) {
      this.toast.error('Vui lòng nhập địa chỉ!');
      return false;
    }
    return true;
  }

  closeConnectionModal() {
    this.popoverController.dismiss()
  }

  switchTab(tab) {
    this.tab = tab;
  }

  async doConnect() {
    this.loading = true;
    if (this.connection.connection_provider == 'partner') {
      await this.loginUsingToken();
    } else {
      switch (this.tab) {
        case 'login':
          await this.loginShopConnection();
          break;
        case 'register':
          await this.registerShopConnection();
          break;
        default:
          break;
      }
    }
    this.loading = false;
  }

  async registerShopConnection() {
    try {
      const valid = this.validateRegisterInfo(
        this.registerInfo,
        this.connection.connection_provider
      );
      if (!valid) {
        return this.loading = false;
      }
      const body: RegisterShopConnectionRequest = {
        ...this.registerInfo,
        connection_id: this.connection.id
      };
      await this.connectionService.registerShopConnection(body);
      this.toast.success(`Kết nối với ${this.connection.name_display} thành công.`);
      this.closeConnectionModal();
    } catch(e) {
      debug.error('ERROR in registerShopConnection', e);
      this.toast.error(
        `Kết nối với ${this.connection.name_display} không thành công.`+
        e.code ? (e.message || e.msg) : ''
      );
    }
  }

  async loginShopConnection() {
    try {
      const valid = this.validateLoginInfo(this.loginInfo);
      if (!valid) {
        return this.loading = false;
      }
      const body: LoginShopConnectionRequest = {
        ...this.loginInfo,
        connection_id: this.connection.id
      };
      await this.connectionService.loginConnection(body);
      this.toast.success(`Kết nối với ${this.connection.name_display} thành công.`);
      this.closeConnectionModal();
    } catch(e) {
      debug.error('ERROR in loginShopConnection', e);
      this.toast.error(
        `Kết nối với ${this.connection.name_display} không thành công.`+
        e.code ? (e.message || e.msg) : ''
      );
    }
  }

  async loginUsingToken() {
    try {
      const valid = this.validateLoginByTokenInfo(this.loginByToken);
      if (!valid) {
        return this.loading = false;
      }

      const { user_id, token } = this.loginByToken;

      const body: LoginShopConnectionByTokenRequest = {
        external_data: { user_id },
        token,
        connection_id: this.connection.id
      };
      await this.connectionService.updateShopConnection(body);
      this.toast.success(`Kết nối với ${this.connection.name_display} thành công.`);
      this.closeConnectionModal();
    } catch(e) {
      debug.error('ERROR in loginUsingToken', e);
      this.toast.error(
        `Kết nối với ${this.connection.name_display} không thành công.`+
        e.code ? (e.message || e.msg) : ''
      );
    }
  }

  onProvinceSelected(province_code) {
    if (!province_code) {
      return;
    }
    const province = this.locationQuery.getValue().provincesList.find(province => province.code == province_code)
    if (province && (province.name != this.registerInfo.province)) {
      this.registerInfo.province = province.name;
      this.selectedDistrictCode = null;
    }

    this.districts = this.locationService.filterDistrictsByProvince(
      this.selectedProvinceCode
    );
  }

  onDistrictSelected(district_code) {
    if (!this.selectedDistrictCode) {
      return;
    }
    const district = this.locationQuery.getValue().districtsList.find(district => district.code == district_code)

    if (district && (district.name != this.registerInfo.district)) {
      this.registerInfo.district = district.name;
    }
  }
}
