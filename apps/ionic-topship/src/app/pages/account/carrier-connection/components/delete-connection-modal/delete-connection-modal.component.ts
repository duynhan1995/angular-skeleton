import { Component, Input, OnInit } from '@angular/core';
import { Connection } from '@etop/models';
import { PopoverController } from '@ionic/angular';
import { ConnectionService } from '@etop/features';
import { ToastService } from '../../../../../services/toast.service';

@Component({
  selector: 'app-delete-connection-modal',
  templateUrl: './delete-connection-modal.component.html',
  styleUrls: ['./delete-connection-modal.component.scss']
})
export class DeleteConnectionModalComponent implements OnInit {
  loading = false
  @Input() connection: Connection
  constructor(
    private connectionService: ConnectionService,
    private toast: ToastService,
    private popoverController: PopoverController
  ) { }

  ngOnInit() {
  }

  close() {
    this.popoverController.dismiss({closed: true}).then();
  }

  async deleteShopConnection() {
    this.loading = true;
    try {
      await this.connectionService.deleteShopConnection(this.connection);
      this.toast.success(`Gỡ kết nối với nhà vận chuyển ${this.connection.name_display} thành công.`);
      this.close()
    } catch (e) {
      debug.error('ERROR in confirming Receipt', e);
      this.toast.error(`Gỡ kết nối với nhà vận chuyển ${this.connection.name_display} không thành công.`+ e.message || e.msg);
      this.loading = false;
    }
    this.loading = false;
  }
}
