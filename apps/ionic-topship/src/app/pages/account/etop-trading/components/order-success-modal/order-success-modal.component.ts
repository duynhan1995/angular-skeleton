import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'topship-order-success-modal',
  templateUrl: './order-success-modal.component.html',
  styleUrls: ['./order-success-modal.component.scss']
})
export class OrderSuccessModalComponent implements OnInit {
  @Input() product: any;
  @Input() phone: any;
  constructor(
    private modalCtrl: ModalController,
  ) { }

  back() {
    this.modalCtrl.dismiss(false)
  }

  ngOnInit(): void {
  }
 
}
