import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AccountComponent } from './account.component';
import { SettingsComponent } from '../settings/settings.component';
import { ListStoreComponent } from './list-store/list-store.component';
import { ProfileComponent } from '../profile/profile.component';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { NewShopComponent } from './new-shop/new-shop.component';
import { AuthenticateModule } from '@etop/core';

import { CarrierConnectionItemComponent } from './carrier-connection/components/carrier-connection-item/carrier-connection-item.component';
import { CarrierConnectModule } from '../../components/carrier-connect/carrier-connect.module';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { EmptyModule } from '../../components/empty/empty.module';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { PopoversModule } from '../../components/popovers/popovers.module';
import { PostService } from '@etop/features/fabo/post/post.service';
import { NotificationSettingComponent } from './notification-setting/notification-setting.component';
import { NotifySettingService } from '@etop/features';
import { NotifySettingApi } from '@etop/api';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { ShopService } from '../../../../../faboshop/src/services/shop.service';
import { CarrierConnectionModalComponent } from './carrier-connection/components/carrier-connection-modal/carrier-connection-modal.component';
import { CarrierConnectionWithOtpModalComponent } from './carrier-connection/components/carrier-connection-with-otp-modal/carrier-connection-with-otp-modal.component';
import { DeleteConnectionModalComponent } from './carrier-connection/components/delete-connection-modal/delete-connection-modal.component';
import { CarrierConnectionComponent } from './carrier-connection/carrier-connection.component';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { EtopTradingComponent } from './etop-trading/etop-trading.component';
import { EtopPipesModule } from '@etop/shared';
import { CheckoutModalComponent } from './etop-trading/components/checkout-modal/checkout-modal.component';
import { OrderSuccessModalComponent } from './etop-trading/components/order-success-modal/order-success-modal.component';
import { TopshipAppCommonUsecase } from '../../usecases/topship-app-commom.usecase.service';
import { IonSkeletonModule } from '../../components/ion-skeleton/ion-skeleton.module'
import { StaffModule } from './staff/staff.module';
import { StaffComponent } from './staff/staff.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'list-store', component: ListStoreComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'carrier', component: CarrierConnectionComponent },
      { path: 'address', component: DeliveryAddressComponent },
      { path: 'notification', component: NotificationSettingComponent },
      { path: 'etop-trading', component: EtopTradingComponent },
      { path: 'staff', component: StaffComponent },
      { path: 'setting', component: SettingsComponent },
    ]
  }
];

@NgModule({
  declarations: [
    AccountComponent,
    SettingsComponent,
    ListStoreComponent,
    ProfileComponent,
    NewShopComponent,
    CarrierConnectionComponent,
    CarrierConnectionModalComponent,
    CarrierConnectionWithOtpModalComponent,
    DeleteConnectionModalComponent,
    CarrierConnectionItemComponent,
    DeliveryAddressComponent,
    NotificationSettingComponent,
    EtopTradingComponent,
    CheckoutModalComponent,
    OrderSuccessModalComponent,
  ],
  entryComponents: [
    SettingsComponent,
    ListStoreComponent,
    ProfileComponent,
    NewShopComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    CarrierConnectModule,
    EmptyModule,
    EtopPipesModule,
    PopoversModule,
    RouterModule.forChild(routes),
    SelectSuggestModule,
    IonSkeletonModule,
    StaffModule
  ],
  providers: [
    MobileUploader,
    PostService,
    ConnectionService,
    ConnectionStore,
    ConfirmOrderStore,
    ConfirmOrderService,
    NotifySettingService,
    NotifySettingApi,
    OpenNativeSettings,
    ShopService,
    TopshipAppCommonUsecase
  ],
  exports: [AccountComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccountModule {}
