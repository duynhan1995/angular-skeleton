import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffComponent } from './staff.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UserInvitationRowComponent } from './user-invitation-row/user-invitation-row.component';
import { StaffManagementRowComponent } from './staff-management-row/staff-management-row.component';
import { StaffDetailComponent } from './staff-detail/staff-detail.component';
import { AddStaffModalComponent } from './add-staff-modal/add-staff-modal.component';


const routes: Routes = [
  {
    path: '',
    component: StaffComponent
  }
];
@NgModule({
  declarations: [
    StaffComponent,
    UserInvitationRowComponent,
    StaffManagementRowComponent,
    StaffDetailComponent,
    AddStaffModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),

  ],
  exports: [StaffComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StaffModule { }
