import { NgModule } from '@angular/core';
import { AccountMenuComponent } from './account-menu.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkDisconnectModule } from 'apps/ionic-topship/src/app/components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from 'apps/ionic-topship/src/app/components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { CarrierConnectModule } from 'apps/ionic-topship/src/app/components/carrier-connect/carrier-connect.module';
import { EmptyModule } from 'apps/ionic-topship/src/app/components/empty/empty.module';
import { PopoversModule } from 'apps/ionic-topship/src/app/components/popovers/popovers.module';
import { IframeService } from '../../../../../../core/src/services/iframe.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    CarrierConnectModule,
    EmptyModule,
    PopoversModule,
  ],
  declarations: [AccountMenuComponent],
  exports: [AccountMenuComponent],
  providers: [IframeService]
})
export class AccountMenuModule{

}
