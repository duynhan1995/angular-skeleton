import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketsComponent } from './tickets.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { MatIconModule } from '@angular/material/icon';
import { CreateTicketComponent } from './components/create-ticket/create-ticket.component';
import { ConfirmTicketModalComponent } from './components/confirm-ticket-modal/confirm-ticket-modal.component';
import { UpdateShippingAddressModalComponent } from './components/update-shipping-address-modal/update-shipping-address-modal.component';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'create-ticket', component: CreateTicketComponent },
      { path: 'ticket-detail', component: TicketDetailComponent },
    ]
  }
];
@NgModule({
  declarations: [
    TicketsComponent,
    CreateTicketComponent,
    ConfirmTicketModalComponent,
    UpdateShippingAddressModalComponent,
    TicketDetailComponent
  ],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    NetworkStatusModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    IonInputFormatNumberModule,
    MatIconModule,
    SelectSuggestModule,
    NgbModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TicketsModule { }
