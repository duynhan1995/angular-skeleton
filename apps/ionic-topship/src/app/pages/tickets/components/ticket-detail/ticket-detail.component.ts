import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { TicketQuery, TicketService, TicketStore } from '@etop/state/shop/ticket';
import { UtilService } from '../../../../../../../core/src/services/util.service';
import { IonContent, IonTextarea, NavController } from '@ionic/angular';
import { FulfillmentQuery } from '@etop/state/topship/fulfillment';
import { Ticket,  } from '@etop/models';
import { map, takeUntil } from 'rxjs/operators';
import { SystemEventService } from '../../../../../../../ionic-faboshop/src/app/system-event.service';
import { PageBaseComponent } from '@etop/ionic';
import { ToastService } from '../../../../services/toast.service';

@Component({
  selector: 'topship-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent extends PageBaseComponent implements OnInit {
  @ViewChild('content') content: IonContent;
  @ViewChild('messageInput', {static: false}) messageInput: IonTextarea;
  activeTicket$ = this.ticketQuery.selectActive();
  activeFulfillment$ = this.ticketQuery.select('activeFulfillment');
  lastestTicketComment$ = this.ticketQuery.selectActive().pipe(map(ticket => ticket?.comments[this.ticketQuery.getActive()?.comments?.length - 1]));
  segment : 'response' | 'info' = 'info';
  textMessage = '';
  sendingText = false;

  constructor(
    private ticketQuery: TicketQuery,
    private ticketStore: TicketStore,
    private ticketService: TicketService,
    private util: UtilService,
    private zone: NgZone,
    private navCtrl: NavController,
    private fulfillmentQuery: FulfillmentQuery,
    public systemEvent: SystemEventService,
    public el: ElementRef,
    private toast: ToastService
  ) {
    super(systemEvent, el);
  }

  async ngOnInit() {
    this.systemEvent.keyboardDidShow.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.scrollToBottom(200);
    })
  }

  async ionViewWillEnter() {
    await this._prepareData();
  }

  async _prepareData() {
    const ticket: Ticket = this.ticketQuery.getActive();
    await this.ticketService.getTicketComments(ticket);
  }

  back() {
    this.navCtrl.back();
  }

  onTextMessageChanged() {
    setTimeout(_ => {
      const chatBox = document.getElementById('chat-box');
      const toolbar = document.getElementById('message-toolbar');
      if (!this.textMessage) {
        toolbar.style.alignItems = 'center';
        chatBox.style.height = 'auto';
      }
    });
    this.content.scrollToBottom(200)
  }

  viewToResponse() {
    this.zone.run(() => {
      this.segment = 'response';
    });
    setTimeout(_ => {
      this.messageInput.setFocus();
    }, 1000);

  }

  async scrollToBottom(duration = 0) {
    await this.content.scrollToBottom(duration);
  }

  segmentChanged(event) {
    this.zone.run(() => {
      this.segment = event;
    });

    if(event == 'response') {
      setTimeout(() => this.scrollToBottom(1000));
    }
  }

  get isCancelledFfm() {
    return this.fulfillmentQuery.getActive()?.shipping_status == 'N' || this.fulfillmentQuery.getActive()?.shipping_status == 'P';
  }

  async createTicketComment() {
    if(this.isCancelledFfm) {
        return this.toast.error('Không thể phản hồi trên đơn hàng đã hủy');
    }
    if (!this.textMessage || this.sendingText) {
      return;
    }
    this.sendingText = true;
    try {
      this.onTextMessageChanged();
      await this.sendMessageWithText();
      this.textMessage = '';
      setTimeout(() => this.scrollToBottom(200), 300);
    } catch (e) {
      debug.error('ERROR in create ticket comment', JSON.stringify(e));
    }
    this.sendingText = false;
  }

  async sendMessageWithText() {
    await this.ticketService.createTicketComment(this.textMessage).catch(e => {
      this.toast.error('Gửi phản hồi không thành công.');
    });
  }
  // retryCreateTicketComment(comment: TicketComment) {
  //   this.ticketService.createTicketComment(comment).catch(e => {
  //     toastr.error('Gửi phản hồi không thành công.', e.code && (e.message || e.msg));
  //   });
  // }

}
