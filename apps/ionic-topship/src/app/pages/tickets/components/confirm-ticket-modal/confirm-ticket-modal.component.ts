import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActionType, Fulfillment } from '@etop/models';
import { TicketQuery, TicketService } from '@etop/state/shop/ticket';
import { AddressDisplayPipe } from '@etop/shared';

@Component({
  selector: 'topship-confirm-ticket-modal',
  templateUrl: './confirm-ticket-modal.component.html',
  styleUrls: ['./confirm-ticket-modal.component.scss']
})
export class ConfirmTicketModalComponent implements OnInit {
  @Input() ticketInfo;
  title = '';
  constructor(
    private modalCtrl: ModalController,
    private ticketQuery: TicketQuery,
    private ticketService: TicketService,
    private addressDisplay: AddressDisplayPipe
  ) { }

  ngOnInit(): void {
    this.mapTitle();
  }

  mapTitle() {
    const activeFfm = this.ticketQuery.getValue().activeFulfillment;
    const actionType: ActionType = this.ticketQuery.getValue().actionType;
    switch (actionType) {
      case ActionType.ffm_other:
        this.title = `${this.ticketInfo.title} cho đơn #${activeFfm.shipping_code}`
        // code block
        break;
      case ActionType.ffm_update_cod:
        this.title = `Đổi giá trị COD đơn #${activeFfm.shipping_code}`
        // code block
        break;
      case ActionType.ffm_update_info:
        this.title = `Đổi thông tin người nhận đơn #${activeFfm.shipping_code}`
        // code block
        break;
      case ActionType.ffm_push_pickup:
        this.title = `Giục lấy hàng đơn #${activeFfm.shipping_code}`
        // code block
        break;
      case ActionType.ffm_push_delivery:
        this.title = `Giục giao hàng đơn #${activeFfm.shipping_code}`;
        // code block
        break;
      default:
      // code block
    }
  }

  get isChanges() {
    return this.actionType == ActionType.ffm_update_cod || this.actionType == ActionType.ffm_update_info || this.actionType == ActionType.ffm_other;
  }

  get actionType(): ActionType {
    return this.ticketQuery.getValue().actionType;
  }

  confirm() {
    this.modalCtrl.dismiss(true);
  }

  back() {
    this.modalCtrl.dismiss(null);
  }

  get newShippingAddressDisplay() {
    return this.addressDisplay.transform(this.ticketInfo.newAddress)
  }

  get activeFulfillment() {
    return this.ticketQuery.getValue().activeFulfillment;
  }

  get oldShippingAddressDisplay() {
    return this.addressDisplay.transform(this.activeFulfillment.shipping_address);
  }

  get isPhoneChanged() {
    return this.ticketInfo.newPhone != this.activeFulfillment.shipping_address.phone
  }

  get isFullnameChanged() {
    return this.ticketInfo.newFullname != this.activeFulfillment.shipping_address.full_name
  }

  get isAddressChanged() {
    return this.ticketInfo.newAddress != this.addressDisplay.transform(this.activeFulfillment.shipping_address)
  }
}
