import { Component, OnInit } from '@angular/core';
import { ActionSheetController, IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { TicketQuery, TicketService, TicketStore } from '@etop/state/shop/ticket';
import { ActionType, Address, Fulfillment } from '@etop/models';
import { FormBuilder } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { ToastService } from 'apps/ionic-topship/src/app/services/toast.service';
import { ConfirmTicketModalComponent } from '../confirm-ticket-modal/confirm-ticket-modal.component';
import { ShopTicketAPI } from '@etop/api';
import { UpdateShippingAddressModalComponent } from '../update-shipping-address-modal/update-shipping-address-modal.component';
import { AddressDisplayPipe } from '@etop/shared';

@Component({
  selector: 'topship-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.scss']
})
export class CreateTicketComponent extends BaseComponent implements OnInit {
  fulfillment$ = this.ticketQuery.select('activeFulfillment');
  actionType$ = this.ticketQuery.select('actionType');
  actionIcon: string = '';
  reason:'';
  _checkFocus = true;
  modal : any;
   ACTION_TYPE_DISPLAY = {
    ffm_update_cod : 'Thay đổi tiền thu hộ đơn',
    ffm_other : 'Yêu cầu khác cho đơn',
    ffm_push_pickup : 'Giục lấy hàng đơn',
    ffm_push_delivery : 'Giục giao hàng đơn',
    ffm_push_re_delivery : 'Giục giao lại đơn',
    ffm_update_info : 'Đổi thông tin người nhận đơn',
  };

  createTicketForm = this.fb.group({
    reason : '',
    cod_amount: '',
    title:'',
    content:'',
    shipping_address: this.fb.group({
      address: '',
      fullname: '',
      phone: '',
    })
  })
  // fulfillment$ = this.ticketQuery.select('activeFulfillment');
  constructor(
    private navCtrl: NavController,
    private ticketStore: TicketStore,
    private ticketService: TicketService,
    private ticketQuery: TicketQuery,
    private actionSheetController: ActionSheetController,
    private fb: FormBuilder,
    private toastr: ToastService,
    private modalController: ModalController,
    private routerOutlet:  IonRouterOutlet,
    private addressDisplay: AddressDisplayPipe,
  ) { super() }

  ngOnInit() {
    const activeFulfillment = JSON.parse(JSON.stringify(this.ticketQuery.getValue().activeFulfillment));
    // this.createTicketForm.reset();
    this.createTicketForm.patchValue({
      cod_amount: activeFulfillment.total_cod_amount,
      shipping_address: {
        address: this.addressDisplay.transform(activeFulfillment.shipping_address),
        phone: activeFulfillment.shipping_address.phone,
        fullname: activeFulfillment.shipping_address.full_name,
      }
    })
    this.ticketService.changeActionType$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
      const activeFulfillment = this.ticketQuery.getValue().activeFulfillment;
      this.createTicketForm.reset();
      this.createTicketForm.patchValue({
        cod_amount: activeFulfillment.total_cod_amount,
        shipping_address: {
          address: activeFulfillment.shipping_address.address1,
          phone: activeFulfillment.shipping_address.phone,
          fullname: activeFulfillment.shipping_address.full_name,
        }
      })
    })
  }

  async ionViewWillEnter() {
    await this.ticketService.getTicketLabels({tree: false});
    const actionType = this.ticketQuery.getValue().actionType;
    this.actionIcon = this.ticketService.getActionIcon(actionType);
  }

  validate() {
    const activeFfm = this.ticketQuery.getValue().activeFulfillment;
    const actionType = this.ticketQuery.getValue().actionType;
    const formValue = this.createTicketForm.getRawValue();
    let isValid = true;
    if(actionType == ActionType.ffm_other) {
      if(!formValue.title) {
        isValid = false;
        this.toastr.error('Vui lòng nhập tiêu đề')
        return isValid;
      }
      if(!formValue.content) {
        isValid = false;
        this.toastr.error('Vui lòng nhập nội dung')
      }
    }
    if(actionType == ActionType.ffm_push_delivery) {
      if(!formValue.reason) {
        isValid = false;
        this.toastr.error('Vui lòng nhập lý do')
      }
    }
    if(actionType == ActionType.ffm_push_pickup) {
      if(!formValue.reason) {
        isValid = false;
        this.toastr.error('Vui lòng nhập lý do')
      }
    }
    if(actionType == ActionType.ffm_update_info) {
      if(formValue.shipping_address.fullname == activeFfm.shipping_address.full_name
        && formValue.shipping_address.address == this.addressDisplay.transform(activeFfm.shipping_address)
        && formValue.shipping_address.phone == activeFfm.shipping_address.phone
      ) {
        isValid = false;
        this.toastr.error('Nội dung bạn cần thay đổi đang trùng với thông tin hiện tại');
        return isValid;
      }
      if(!formValue.reason) {
        isValid = false;
        this.toastr.error('Vui lòng nhập lý do');
      }
    }
    if(actionType == ActionType.ffm_update_cod) {
      if(!formValue.reason) {
        isValid = false;
        this.toastr.error('Vui lòng nhập lý do')
        return isValid;
      }
      if(activeFfm.total_cod_amount == formValue.cod_amount) {
        isValid = false;
        this.toastr.error('Nội dung bạn cần thay đổi đang trùng với thông tin hiện tại')
      }
      if(formValue.cod_amount != 0 && formValue.cod_amount < 5000){
        isValid = false;
        this.toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5000đ')
      }
    }
    return isValid;
  }

  get actionTypeDisplay() {
    return this.ACTION_TYPE_DISPLAY[this.ticketQuery.getValue().actionType]
  }

  async openConfirmTicketModal() {
    if(!this.validate()) {
      return;
    }
    let createTicketForm = this.createTicketForm.getRawValue()
    let actionType = this.ticketQuery.getValue().actionType;
    let ticketInfo : any = {
      description: '',
    };
    if(actionType == ActionType.ffm_push_pickup || actionType == ActionType.ffm_push_delivery) {
      ticketInfo.description = createTicketForm.reason;
    }
    if(actionType == ActionType.ffm_update_cod) {
      ticketInfo.newCodAmount = createTicketForm.cod_amount;
      ticketInfo.description = createTicketForm.reason;
    }
    if(actionType == ActionType.ffm_update_info) {
      ticketInfo.newFullname = createTicketForm.shipping_address.fullname;
      ticketInfo.newPhone = createTicketForm.shipping_address.phone;
      ticketInfo.description = createTicketForm.reason;
      ticketInfo.newAddress = createTicketForm.shipping_address.address;
    }
    if(actionType == ActionType.ffm_other) {
      ticketInfo.title = createTicketForm.title;
      ticketInfo.content = createTicketForm.content;
    }
    const modal = await this.modalController.create({
      component: ConfirmTicketModalComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        ticketInfo
      }
    });
    modal.onDidDismiss().then( async res => {
      if(res.data) {
        try {
          await this.createTicket().then(_ => this.back());
        } catch(e) {
          this.toastr.error('Tạo phiếu yêu cầu hỗ trợ không thành công')
        }
      }
    });
    await modal.present().then();
  }

  back() {
    this.navCtrl.back();
  }

  checkFocus() {
    this._checkFocus = true;
  }

  outFocus() {
    this._checkFocus = false;
  }

  async openActionSheet() {
    const activeFulfillment = this.ticketQuery.getValue().activeFulfillment;
    const actionSheet = await this.actionSheetController.create({
      header: 'Tạo yêu cầu hỗ trợ',
      cssClass: 'left-align-buttons',
      buttons: [
        {
          text: `Giục lấy hàng`,
          icon:'flash-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_push_pickup, activeFulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.changeActionType(activeFulfillment,ActionType.ffm_push_pickup)
          }
        },
        {
          text: 'Giục giao hàng',
          icon:'flash-outline',
          cssClass: 'myActionSheetBtnStyle',
          role: !this.ticketService.validateAction(ActionType.ffm_push_delivery, activeFulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.changeActionType(activeFulfillment,ActionType.ffm_push_delivery)
          }
        },
        {
          text: 'Đổi tiền thu hộ (COD)',
          icon:'cash-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_update_cod, activeFulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.changeActionType(activeFulfillment,ActionType.ffm_update_cod)
          }
        },
        {
          text: 'Đổi thông tin người nhận',
          icon:'person-circle-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_update_info, activeFulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.changeActionType(activeFulfillment,ActionType.ffm_update_info)
          }
        },
        {
          text: 'Yêu cầu khác',
          icon:'menu-outline',
          role: !this.ticketService.validateAction(ActionType.ffm_other, activeFulfillment) ? 'disabled-left' : 'left',
          handler: () => {
            this.changeActionType(activeFulfillment,ActionType.ffm_other)
          }
        },
        // {
        //   text: 'Đóng',
        //   role: 'cancel',
        //   handler: () => {
        //     debug.log('Cancel clicked');
        //   }
        // }
      ]
    });
    await actionSheet.present();
  }

  changeActionType(ffm: Fulfillment, actionType: ActionType) {
    this.ticketService.setTicketAction(actionType);
    this.actionIcon = this.ticketService.getActionIcon(actionType);
  }

  async createTicket() {
    try {
      let ticketInfo = {
        title: '',
        description: '',
      }
      const activeFfm = this.ticketQuery.getValue().activeFulfillment;
      const createTicketForm = this.createTicketForm.getRawValue();
      const actionType = this.ticketQuery.getValue().actionType;
      if(actionType == ActionType.ffm_other) {
        ticketInfo.title = `${createTicketForm.title} #${activeFfm.shipping_code}`;
        ticketInfo.description = `NỘI DUNG:
      ${createTicketForm.content}
`;
      }
      if(actionType == ActionType.ffm_update_info) {
        ticketInfo.title = `${this.ACTION_TYPE_DISPLAY.ffm_update_info} #${activeFfm.shipping_code}
`;
        ticketInfo.description += `NỘI DUNG THAY ĐỔI:
`
        if(activeFfm.shipping_address.full_name != createTicketForm?.shipping_address.fullname) {
          ticketInfo.description += `- Tên người nhận: ${activeFfm.shipping_address.full_name} → ${createTicketForm?.shipping_address.fullname}.
`
        }
        if(activeFfm.shipping_address.phone != createTicketForm?.shipping_address.phone) {
          ticketInfo.description += `- SĐT người nhận: ${activeFfm.shipping_address.phone} → ${createTicketForm?.shipping_address.phone}.
`
        }
        if(this.addressDisplay.transform(activeFfm.shipping_address)  != createTicketForm?.shipping_address.address) {
          ticketInfo.description += `- Địa chỉ nhận hàng: ${this.addressDisplay.transform(activeFfm.shipping_address)} → ${createTicketForm?.shipping_address.address}.
`
        }
        ticketInfo.description += `
LÝ DO:
${createTicketForm.reason}`;
      }
      if(actionType == ActionType.ffm_update_cod) {
        ticketInfo.title = `${this.ACTION_TYPE_DISPLAY.ffm_update_cod} #${activeFfm.shipping_code}
`;

        ticketInfo.description = `NỘI DUNG THAY ĐỔI:
Thu hộ thay đổi từ ${activeFfm.total_cod_amount}đ thành ${createTicketForm.cod_amount}đ.

LÝ DO:
${createTicketForm.reason}
`;
      }
      if(actionType == ActionType.ffm_push_delivery) {
        ticketInfo.title = `${this.ACTION_TYPE_DISPLAY.ffm_push_delivery} #${activeFfm.shipping_code}
`;
        ticketInfo.description = `LÝ DO:
${createTicketForm.reason}.
`;
      }
      if(actionType == ActionType.ffm_push_pickup) {
        ticketInfo.title = `${this.ACTION_TYPE_DISPLAY.ffm_push_pickup} #${activeFfm.shipping_code}
`;
        ticketInfo.description = `LÝ DO:
${createTicketForm.reason}
`;
      }
      await this.ticketService.createTicket(ticketInfo, createTicketForm)
      this.toastr.success('Gửi yêu cầu hỗ trợ thành công');
    } catch(e) {
      return this.toastr.error(e.msg || e.message);
    }

  }

  async openUpdateAddressModal() {
    if(this.modal) return;
    const activeFfm = this.ticketQuery.getValue().activeFulfillment;
    this.modal = await this.modalController.create({
      component: UpdateShippingAddressModalComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        address: {...activeFfm.shipping_address }
      }
    });
    this.modal.onDidDismiss().then( async res => {
      let address = res.data;
      if(address) {
        this.createTicketForm.controls['shipping_address'].patchValue({
          address: this.addressDisplay.transform(address)
        })
      }
      this.modal = null;
    });
    await this.modal.present().then();
  }


}
