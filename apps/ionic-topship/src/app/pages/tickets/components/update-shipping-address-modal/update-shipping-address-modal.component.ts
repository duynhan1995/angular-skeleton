import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Address, District, Province, Ward } from '@etop/models';
import { ToastService } from '../../../../../../../ionic-faboshop/src/app/services/toast.service';
import { LocationQuery, LocationService } from '@etop/state/location';

@Component({
  selector: 'topship-update-shipping-address-modal',
  templateUrl: './update-shipping-address-modal.component.html',
  styleUrls: ['./update-shipping-address-modal.component.scss']
})
export class UpdateShippingAddressModalComponent implements OnInit {
  @Input() address : Address;
  constructor(
    private toast: ToastService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private modalCtrl: ModalController
  ) { }

  provincesList: Province[] = [];
  districtsList: District[] = [{ id: null, code: null, name: 'Chưa chọn tỉnh thành' }];
  wardsList: Ward[] = [{ id: null, code: null, name: 'Chưa chọn quận huyện' }];

  ngOnInit(): void {
    this.provincesList = this.locationQuery.getValue().provincesList;
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
  }

  dismiss() {
    this.modalCtrl.dismiss().then();
  }

  onProvinceSelected() {
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.address.province = this.locationQuery.getProvince(this.address.province_code)?.name;
    this.address.district_code = '';
    this.address.ward_code = '';
  }

  onDistrictSelected() {
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.address.district = this.locationQuery.getDistrict(this.address.district_code)?.name;
    this.address.ward_code = '';
  }

  onWardSelected() {
    this.address.ward = this.locationQuery.getWard(this.address.ward_code)?.name;
  }

  confirm() {
    const {province_code, district_code, ward_code, address1} = this.address;
    if (!province_code) {
      return this.toast.error('Chưa nhập tỉnh thành');
    }
    if (!district_code) {
      return this.toast.error('Chưa nhập quận huyện');
    }
    if (!ward_code) {
      return this.toast.error('Chưa nhập phường xã');
    }
    if (!address1) {
      return this.toast.error('Chưa nhập địa chỉ cụ thể');
    }
    this.modalCtrl.dismiss(this.address).then()
  }
}
