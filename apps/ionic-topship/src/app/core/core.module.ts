import { NgModule } from '@angular/core';

// libs
import { EtopIonicCoreModule } from '@etop/ionic';
import { UtilService } from 'apps/core/src/services/util.service';
import { UserApi, ShopAccountApi, CategoryApi } from '@etop/api';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { OrderService } from 'apps/shop/src/services/order.service';
import { ShopService } from '@etop/features';
import { UserService } from 'apps/core/src/services/user.service';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { TelegramService } from '@etop/features';
import { LocationApi } from '@etop/api/general/location.api';
import { LoadingViewService } from '../components/loading-view/loading-view.service';
import { FulfillmentService } from 'apps/ionic-topship/src/app/services/fulfillment.service'
import { NotiService } from 'apps/ionic-topship/src/app/services/noti.service';

const services = [
  UserApi,
  UtilService,
  GoogleAnalyticsService,
  TelegramService,
  FulfillmentService,
  OrderService,
  ShopAccountApi,
  ShopService,
  UserService,
  UserBehaviourTrackingService,
  CategoryApi,
  PromiseQueueService,
  CustomerService,
  LocationApi,
  LoadingViewService,
  NotiService,
];
const api = [];

@NgModule({
  imports: [EtopIonicCoreModule],
  providers: [...services, ...api]
})
export class CoreModule {}
