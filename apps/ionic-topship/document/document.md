**LIVE-RELOAD CAUTION**

- Khi chạy live-reload thì file `capacitor.config.json` sẽ bị thay đổi, thêm 1 field là `server: <ip>:<port>`.
Điều này dẫn đến khi build ra thiết bị thật để demo thì sẽ bị `Could not connect to server`.
- => sau khi chạy live-reload, nếu muốn build ra thiết bị thật để demo cho ng khác xem thì nhớ vào file `capacitor.config.json` xoá đoạn đc add thêm vô.

