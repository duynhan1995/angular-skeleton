import { Component } from '@angular/core';

@Component({
  selector: 'etop-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'shared';
}
