
export abstract class CommonUsecase {
  abstract login(data: {login: string; password: string}): Promise<any>;
  abstract register(data: any, source: string): Promise<any>;
  abstract checkAuthorization(fetchAccounts?: boolean): Promise<any>;
  abstract updateSessionInfo(fetchAccounts?: boolean): Promise<any>;
  abstract redirectIfAuthenticated(): Promise<any>;
}
