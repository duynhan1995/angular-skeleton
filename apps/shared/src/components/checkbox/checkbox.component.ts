import { Component, OnInit, Input, Injector, forwardRef } from '@angular/core';
import { NgModel, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'shared-etop-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent implements OnInit {
  @Input() checked = false;
  @Input() disabled = false;
  constructor(private injector: Injector) {}

  ngOnInit() {
    if (!this.checked && this.checked !== false) {
      this.checked = false;
    }
  }

  registerOnChange() {}

  registerOnTouched() {}

  public writeValue(value) {
    this.checked = value;
  }

  onChanged() {
    try {
      let model = this.injector.get(NgModel);
      model.viewToModelUpdate(this.checked);
    } catch (e) {}
  }

  public validate() {
    return this.checked ? null : { checkboxUncheck: { valid: false } };
  }
}
