export interface DropdownActionOpt {
  title: string;
  onClick?: () => void;
  cssClass?: string;
  icon?: string;
  tooltips?: string;
  disabled?: boolean;
  hidden?: boolean;
  permissions?: string[];
  roles?: string[];
}
