import { ChangeDetectorRef, Component, HostListener, Input, OnInit } from '@angular/core';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';

@Component({
  selector: 'shared-dropdown-actions',
  templateUrl: './dropdown-actions.component.html',
  styleUrls: ['./dropdown-actions.component.scss']
})
export class DropdownActionsComponent implements OnInit {
  @Input() dropdownActions: DropdownActionOpt[] = [];
  @Input() rightSide = false;
  @Input() useOnlyIcon = true;

  showDropdown = false;
  clickedInside = false;

  @HostListener('click')
  public clickInsideComponent() {
    this.clickedInside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.showDropdown = false;
    }
    this.clickedInside = false;
  }

  constructor(
    private ref: ChangeDetectorRef,
    private dropdownController: DropdownActionsControllerService
  ) { }

  get allHidden() {
    if (!this.dropdownActions || !this.dropdownActions.length) {
      return false;
    }
    return !this.dropdownActions.some(a => !a.hidden);
  }

  get rolesOfActionsArray() {
    let roles = [];
    this.dropdownActions.forEach(a => {
      if(a.roles && a.roles.length){
        roles = roles.concat(a.roles);
      }
    })
    return roles
  }

  get permissionsOfActionsArray() {
    let permissions = [];
    this.dropdownActions.forEach(a => {
      if (a.permissions && a.permissions.length) {
        permissions = permissions.concat(a.permissions);
      }
    });
    return permissions;
  }

  ngOnInit() {
    this.dropdownController.registerInstance(this);
  }

  toggleDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  setActions(actions: DropdownActionOpt[]) {
    this.dropdownActions = actions;
    this.ref.markForCheck();
  }

  clearActions() {
    this.dropdownActions = [];
    this.ref.markForCheck();
  }

  handleClick(event, action: DropdownActionOpt) {
    if (action && action.disabled) {
      event.preventDefault();
      return;
    }
    if (action && action.onClick) {
      action.onClick();
    }
  }

}
