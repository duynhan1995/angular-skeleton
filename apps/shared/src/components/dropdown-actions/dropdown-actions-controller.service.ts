import { Injectable } from '@angular/core';
import { DropdownActionsComponent } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.component';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';

@Injectable({
  providedIn: 'root'
})
export class DropdownActionsControllerService {
  private dropdownActions: DropdownActionsComponent[] = [];

  constructor() { }

  registerInstance(instance: DropdownActionsComponent) {
    this.dropdownActions.push(instance);
  }

  setActions(actions: DropdownActionOpt[]) {
    this.dropdownActions.forEach(item => item.setActions(actions));
  }

  clearActions() {
    this.dropdownActions.forEach(item => item.clearActions());
  }

}
