import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "@etop/shared";
import {DropdownActionsComponent} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.component';
import {DropdownActionsControllerService} from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from '@etop/core';


@NgModule({
  declarations: [
    DropdownActionsComponent
  ],
  exports: [
    DropdownActionsComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    AuthenticateModule,
    MaterialModule
  ],
  providers: [
    DropdownActionsControllerService
  ]
})
export class DropdownActionsModule {
}
