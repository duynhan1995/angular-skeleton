import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragdropListComponent } from './dragdrop-list.component';
import { MaterialModule } from '@etop/shared/components/etop-material/material';

@NgModule({
  declarations: [DragdropListComponent],
  exports: [
    DragdropListComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class DragdropListModule { }
