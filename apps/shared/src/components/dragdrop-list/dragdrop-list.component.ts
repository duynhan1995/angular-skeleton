import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'shared-dragdrop-sorting',
  templateUrl: './dragdrop-list.component.html',
  styleUrls: ['./dragdrop-list.component.scss']
})
export class DragdropListComponent implements OnInit {
  @Input() drag_items: Array<any> = [];
  @Output() dropped = new EventEmitter<any>();
  @ContentChild('item', { static: true })
  itemTemplate: TemplateRef<any>;

  constructor() { }

  ngOnInit() {
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.drag_items, event.previousIndex, event.currentIndex);
    this.dropped.emit(this.drag_items);
  }

}
