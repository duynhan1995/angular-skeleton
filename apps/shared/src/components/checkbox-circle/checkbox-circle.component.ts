import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'shared-checkbox-circle',
  templateUrl: './checkbox-circle.component.html',
  styleUrls: ['./checkbox-circle.component.scss']
})
export class CheckboxCircleComponent implements OnInit {
  @Input() active = false;
  @Input() colorClass: string;

  constructor() { }

  ngOnInit() {
  }

}
