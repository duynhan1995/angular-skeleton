import {
  Component,
  OnInit,
  forwardRef,
  Injector,
  HostListener,
  EventEmitter,
  Output,
  Input,
  ViewChild,
  ElementRef,
  OnChanges, SimpleChanges, ChangeDetectorRef
} from '@angular/core';
import { NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';

export interface SelectMultilineOption {
  index: number;
  info: string;
  detail: string;
  search_text: string;
}

@Component({
  selector: 'shared-select-multiline',
  templateUrl: './select-multiline.component.html',
  styleUrls: ['./select-multiline.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectMultilineComponent),
      multi: true
    }
  ]
})
export class SelectMultilineComponent implements OnInit, OnChanges {
  @ViewChild('inputSearch', { static: false }) inputSearch: ElementRef;

  @Output('searchChange') inputOut = new EventEmitter();
  @Output() additionOptionClick = new EventEmitter();
  @Output() focus = new EventEmitter();

  @Input() textLabel: string;
  @Input() options: SelectMultilineOption[];
  @Input() loading = false;
  @Input() width = 'unset';
  @Input() min_width = '400px';
  @Input() additionOption: string;
  @Input() placeholder = '';
  @Input() search_input = '';
  @Input() inputClass = '';
  @Input() showPlaceholderFirst = false;
  @Input() showAddition = true;
  @Input() error = false;

  selectedIndex: number;
  displayOptions = false;
  clickedInside = false;
  // inputShown = false;
  valueShown = true;
  search_options: Array<SelectMultilineOption> = [];

  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    this.clickedInside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.hideOptions();
    }
    this.clickedInside = false;
  }

  constructor(
    private injector: Injector,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
    if (this.options.length) {
      this.writeValue(0);
      this.search_options = this.options;
    }
    this.search_input = '';
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.options &&
      changes.options.currentValue &&
      changes.options.currentValue.length
    ) {
      this.search_options = changes.options.currentValue;
    }
  }

  writeValue(index) {
    this.selectedIndex = index;
  }

  selectOption(index) {
    if (index !== this.selectedIndex) {
      const model = this.injector.get(NgModel);
      model.viewToModelUpdate(index);
      this.writeValue(index);
    }
    this.hideOptions();
    this.valueShown = true;
  }

  registerOnChange() { }

  registerOnTouched() { }

  toggleOptions() {
    this.displayOptions = !this.displayOptions;
    this.search_options = this.options;
  }
  showOptions() {
    this.displayOptions = true;
    this.search_options = this.options;
    // this.inputShown = true;
  }
  hideOptions() {
    this.displayOptions = false;
    this.search_options = this.options;
    this.hideInput();
  }

  showInput() {
    // this.inputShown = true;
    this.valueShown = false;
    this.focusInput();
  }

  hideInput() {
    // this.inputShown = false;
    this.valueShown = true;
  }

  focusInput() {
    this.changeDetector.detectChanges();
    if (this.inputSearch) {
      setTimeout(() => this.inputSearch.nativeElement.select());
    }
    this.focus.emit();
  }

  search(event) {
    if (this.search_input.length) {
      let _input = this.util.makeSearchText(this.search_input);
      this.search_options = this.options.filter(
        o => o.search_text.indexOf(_input) > -1
      );
    } else {
      this.search_options = this.options;
    }
    this.inputOut.emit(this.search_input);
  }

  get selectedValue() {
    return this.options[this.selectedIndex] && this.options[this.selectedIndex].detail;
  }

  get firstValue(){
    return this.options[0] && this.options[0].detail;
  }

  get inputShown(): boolean {
    return !this.valueShown || !this.options.length ||
      (this.showPlaceholderFirst && !this.selectedIndex && this.selectedIndex != 0);
  }

}
