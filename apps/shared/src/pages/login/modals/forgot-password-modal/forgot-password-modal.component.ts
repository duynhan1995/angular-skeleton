import { Component, OnInit } from '@angular/core';
import { UserService } from 'apps/core/src/services/user.service';
import * as validatecontraints from 'apps/core/src/services/validation-contraints.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ConfigService } from '@etop/core/services/config.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import {AuthenticateService, AuthenticateStore} from '@etop/core';
import { VerifyPhoneModalComponent } from '../verify-phone-modal/verify-phone-modal.component';

@Component({
  selector: 'shared-forgot-password-modal',
  templateUrl: './forgot-password-modal.component.html',
  styleUrls: ['./forgot-password-modal.component.scss']
})
export class ForgotPasswordModalComponent implements OnInit {
  formForgot: any = {};
  loading = false;
  usingEmail = true;

  constructor(
    private userService: UserService,
    private modalDismiss: ModalAction,
    private config: ConfigService,
    private modalController:ModalController,
    private auth: AuthenticateStore
  ) {
  }

  ngOnInit() {
  }

  async doForgotPassword() {
    if(this.usingEmail && !this.formForgot.email) {
      return toastr.error('Vui lòng nhập email!');
    }
    if(!this.usingEmail && !this.formForgot.phone) {
      return toastr.error('Vui lòng nhập số điện thoại!');
    }
    this.loading = true;
    try {
      let captcha = await AuthenticateService.getReCaptcha(this.config.get('recaptcha_key'));
      let email = this.formForgot.email;
      let phone = this.formForgot.phone;
      this.usingEmail ? phone = '' : email = '';
      if (email && !validatecontraints.EmailValidates[0].check(email) && !/(?:-[0-9a-zA-Z])?-test/.test(email)) {
        throw new Error('Địa chỉ email không hợp lệ.');
      }

      const data = {
        email,
        phone,
        recaptcha_token: captcha
      };

      const res = await this.userService.requestResetPassword(data);
      this.auth.updateToken(res.access_token);

      if(!this.usingEmail){
        let modal = this.modalController.create({
          component: VerifyPhoneModalComponent,
          componentProps: {
            phone: data.phone
          }
        });
        modal.show().then();
        modal.onDismiss().then(() => {
        });
      }
      if(this.usingEmail) {
        toastr.success(
          'Một email với các chỉ dẫn đặt lại mật khẩu đã được gửi tới địa chỉ email của bạn.');
      } else {
        toastr.success(
          'Một mã xác thực đã được gửi tới số điện thoại của bạn.');
      }
      this.modalDismiss.dismiss(this);
    } catch (e) {
      toastr.error(e.message, 'Yêu cầu mật khẩu mới thất bại!');
    }
    this.loading = false;
  }

  numberOnly(event) {
    const num = Number(event.key);
    if (Number.isNaN(num)) {
      event.preventDefault();
      return false;
    }
  }

  onChangeType() {
    this.usingEmail = !this.usingEmail
  }
}
