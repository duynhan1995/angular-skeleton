import { Component, OnInit } from '@angular/core';
import { CommonUsecase } from '../../usecases/common.usecase.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ForgotPasswordModalComponent } from './modals/forgot-password-modal/forgot-password-modal.component';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationApi } from '@etop/api';
import { Invitation } from 'libs/models/Authorization';

@Component({
  selector: 'admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  invitation_token = '';
  invitation = new Invitation({});

  login: string;
  password: string;

  error = false;
  errorMessage = '';

  loading = true;

  constructor(
    private commonUsecase: CommonUsecase,
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute,
    private authorizationApi: AuthorizationApi,
  ) {
  }

  get registerHref() {
    if (this.invitation_token) {
      return `/register?invitation_token=${this.invitation_token}`;
    }
    return '/register';
  }

  async ngOnInit() {
    this.invitation_token = this.activatedRoute.snapshot.queryParamMap.get('invitation_token');
    if (this.invitation_token) {
      this.invitation = await this.authorizationApi.getInvitationByToken(this.invitation_token);
      this.login = this.invitation && this.invitation.email || '';
    }

    this.commonUsecase.redirectIfAuthenticated().then(() => this.loading = false);
  }

  async submit() {
    this.loading = true;
    try {
      await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
    } catch (e) {
      console.error(e);
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
    this.loading = false;
  }

  forgotPassword() {
    const modal = this.modalController.create({
      component: ForgotPasswordModalComponent,
    });
    modal.show().then();
    modal.onDismiss().then(() => {});
  }
}
