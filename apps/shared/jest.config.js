module.exports = {
  name: 'shared',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/shared/',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
