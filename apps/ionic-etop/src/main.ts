import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as debug from '@etop/utils/debug';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { defineCustomElements } from '@ionic/pwa-elements/loader';

(window as any).__debug_host = '';
debug.setupDebug(environment, true);

if (environment.production) {
  enableProdMode();
}

(window as any).__debug_host = '';

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.log(err));
defineCustomElements(window);
