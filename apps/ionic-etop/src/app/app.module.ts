import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {CoreModule} from './core/core.module';
import {SharedModule} from './features/shared/shared.module';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {EtopAppCommonUsecase} from './usecases/etop-app-commom.usecase.service';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {IonicModule} from '@ionic/angular';
import {EtopAppModule} from 'apps/ionic-etop/src/app/etop-app/etop-app.module';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {FormsModule} from '@angular/forms';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {EtopPipesModule} from 'libs/shared/pipes/etop-pipes.module';
import {BrowserModule} from '@angular/platform-browser';
import {MenuModule} from 'apps/ionic-etop/src/app/components/menu/menu.module';
import {environment} from '../environments/environment';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {NetworkStatusModule} from './components/network-status/network-status.module';
import {CONFIG_TOKEN} from '@etop/core/services/config.service';
import {AuthenticateModule} from '@etop/core';
import {VerifyEmailModalComponent} from './components/verify-email-modal/verify-email-modal.component';
import {CountDownModule} from './components/count-down/count-down.module';
import {FilterModalComponent} from './components/filter-modal/filter-modal.component';
import {LoadingViewModule} from './components/loading-view/loading-view.module';
import {DatePipe, DecimalPipe} from '@angular/common';
import {AppGuard} from 'apps/ionic-etop/src/app/app.guard';

import {registerLocaleData} from '@angular/common';
import localeVi from '@angular/common/locales/vi';
import { SearchSuggestComponent } from './components/search-modal/search-suggest/search-suggest.component';

registerLocaleData(localeVi);

const apis = [];

const services = [];

const providers = [
  {provide: CONFIG_TOKEN, useValue: environment},
  {provide: CommonUsecase, useClass: EtopAppCommonUsecase},
  {provide: 'SERVICE_URL', useValue: environment.api_url},
  {provide: LOCALE_ID, useValue: 'vi'},
  ...apis,
  ...services,
  AppGuard,
  DecimalPipe,
  DatePipe
];

@NgModule({
  imports: [
    CoreModule,
    SharedModule,
    EtopPipesModule,
    AppRoutingModule,
    EtopAppModule,
    FormsModule,
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios',
      scrollPadding: true,
      scrollAssist: true
    }),
    MenuModule,
    NetworkStatusModule,
    AuthenticateModule,
    CountDownModule,
    LoadingViewModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    FilterModalComponent,
    ResetPasswordComponent,
    VerifyEmailModalComponent,
    SearchSuggestComponent
  ],
  entryComponents: [
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    VerifyEmailModalComponent
  ],
  providers,
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
}
