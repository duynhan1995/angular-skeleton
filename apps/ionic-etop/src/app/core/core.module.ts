import { NgModule } from '@angular/core';

// libs
import { EtopIonicCoreModule } from '@etop/ionic';
import { UtilService } from 'apps/core/src/services/util.service';
import { UserApi, ShopAccountApi, CategoryApi } from '@etop/api';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { OrderService } from 'apps/shop/src/services/order.service';
import { ShopService } from '@etop/features';
import { UserService } from 'apps/core/src/services/user.service';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { CustomerService } from 'apps/ionic-etop/src/services/customer.service';
import { TelegramService } from '@etop/features';
import { GaService } from '../../services/ga.service';
import { EtopMaterialModule } from '@etop/shared';
import {FulfillmentService} from 'apps/ionic-etop/src/services/fulfillment.service';
import {StatisticService} from 'apps/ionic-etop/src/services/statistic.service';

const services = [
  UserApi,
  UtilService,
  GoogleAnalyticsService,
  TelegramService,
  FulfillmentService,
  OrderService,
  ShopAccountApi,
  ShopService,
  UserService,
  UserBehaviourTrackingService,
  CategoryApi,
  PromiseQueueService,
  CustomerService,
  StatisticService,
  GaService
];
const api = [];

@NgModule({
  imports: [EtopIonicCoreModule, EtopMaterialModule],
  providers: [...services, ...api]
})
export class CoreModule {}
