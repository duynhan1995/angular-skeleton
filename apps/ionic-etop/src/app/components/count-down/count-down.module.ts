import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CountDownComponent } from './count-down.component';



@NgModule({
  declarations: [CountDownComponent],
  providers: [],
  imports: [CommonModule, FormsModule, IonicModule],
  exports: [CountDownComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CountDownModule {}
