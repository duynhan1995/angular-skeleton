import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'etop-count-down',
  templateUrl: './count-down.component.html',
  styleUrls: ['./count-down.component.scss']
})
export class CountDownComponent implements OnInit, OnChanges {
  @Input() countdown = 60;
  @Output() countDone = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    debug.log('changes', changes);
    if (changes.countdown.currentValue == 60) {
      this.countTimeVerify();
    }
  }
  countTimeVerify() {
    this.countdown = 60;
    let interval = setInterval(() => {
      if (this.countdown < 1) {
        clearInterval(interval);
        debug.log('countdown', this.countdown);
        this.countDone.emit();
      } else {
        this.countdown -= 1;
      }
    }, 1000);
  }
}
