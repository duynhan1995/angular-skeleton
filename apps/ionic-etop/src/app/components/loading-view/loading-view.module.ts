import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingViewComponent } from 'apps/ionic-etop/src/app/components/loading-view/loading-view.component';
import { LoadingViewService } from './loading-view.service';

@NgModule({
  declarations: [LoadingViewComponent],
  exports: [LoadingViewComponent],
  imports: [CommonModule]
})
export class LoadingViewModule {
  static forRoot(): ModuleWithProviders<LoadingViewModule> {
    return {
      ngModule: LoadingViewModule,
      providers: [LoadingViewService]
    };
  }
}
