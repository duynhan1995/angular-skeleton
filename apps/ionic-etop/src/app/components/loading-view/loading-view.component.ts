import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { LoadingViewService } from './loading-view.service';

@Component({
  selector: 'etop-loading-view',
  templateUrl: './loading-view.component.html',
  styleUrls: ['./loading-view.component.scss']
})
export class LoadingViewComponent implements OnInit {
  @Input() set show(value) {
    this.el.nativeElement.style.visibility = value ? 'visible' : 'hidden';
  }

  constructor(private service: LoadingViewService, private el: ElementRef) {}
  ngOnInit() {
    this.service.setLoadingView(this);
  }
}
