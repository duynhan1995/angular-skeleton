import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserService } from 'apps/core/src/services/user.service';
import { ToastService } from '../../../services/toast.service';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';
import {AuthenticateService, AuthenticateStore} from '@etop/core';
import { ConfigService } from '@etop/core/services/config.service';

@Component({
  selector: 'etop-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  phone;
  ionLoading = false;

  constructor(
    private modalCtrl: ModalController,
    private userService: UserService,
    private toast: ToastService,
    private config: ConfigService,
    private auth: AuthenticateStore,
  ) {}

  ngOnInit() {}

  async dismiss() {
    debug.log('dismiss modal');
    await this.modalCtrl.dismiss();
  }

  async sendResetPassword() {
    try {
      let captcha = await AuthenticateService.getReCaptcha(this.config.get('recaptcha_key'));
      let res = await this.userService.requestResetPasswordByPhone({
        phone: this.phone,
        recaptcha_token: captcha
      });
      this.auth.updateToken(res.access_token);
      await this.openResetPassword();
    } catch (e) {
      debug.error('ERROR in sendResetPassword', e);
      this.toast.error(e.message);
    }
  }

  async openResetPassword() {
    this.modalCtrl.dismiss({ phone: this.phone });
    const modal = await this.modalCtrl.create({
      component: ResetPasswordComponent,
      componentProps: {
        phone: this.phone
      },
      animated: false
    });
    modal.onDidDismiss().then(data => {
      debug.log('openResetPassword', data);
    });
    return await modal.present();
  }

  async checkUserRegistration() {
    try {
      if (!this.phone) {
        throw new Error('Vui lòng nhập số điện thoại để tiếp tục!');
      }
      this.ionLoading = true;
      let captcha = await AuthenticateService.getReCaptcha(this.config.get('recaptcha_key'));
      let data = {
        phone: this.phone,
        recaptcha_token: captcha
      };
      let check = await this.userService.checkUserRegistration(data);
      if (check.exists) {
        this.ionLoading = false;
        await this.sendResetPassword();
      } else {
        this.ionLoading = false;
        return this.toast.error(
          `Số điện thoại ${this.phone} chưa đăng ký tài khoản eTop. Vui lòng kiểm tra lại!`
        );
      }
    } catch (e) {
      this.ionLoading = false;
      debug.error('ERROR in Login checkUserRegistration', e);
      throw e;
    }
  }
}
