import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonInputFormatNumberModule } from '@etop/shared/components/etop-ionic';
import { EtopPipesModule } from '@etop/shared/pipes';
import { IonicModule } from '@ionic/angular';
import { TicketModalComponent } from '../ticket-modal/ticket-modal.component';
import { TicketDetailComponent } from './component/ticket-detail/ticket-detail.component';
import { TicketComponent } from './ticket.component';


@NgModule({
    declarations: [
        TicketComponent,
        TicketDetailComponent,
        TicketModalComponent
    ],
    providers: [],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        EtopPipesModule,
        IonInputFormatNumberModule,
    ],
    exports: [
        TicketComponent,
        TicketDetailComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA,
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class TicketModule { }