import { Component, OnInit } from '@angular/core';
import { Ticket } from '@etop/models';
import { ModalController } from '@ionic/angular';

@Component({
  selector: "etop-ticket-detail",
  templateUrl: "./ticket-detail.component.html",
  styleUrls: ["./ticket-detail.component.scss"]
})
export class TicketDetailComponent implements OnInit {
  // TODO: refactor to new data type of Ticket...
  ticket: any;

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  dismiss() {
    this.modalController.dismiss().then();
  }

  getStatusDisplay(status) {
    switch (status) {
      case "Open":
        return "Mới";
      case "Confirmed":
        return "Đã tiếp nhận";
      case "In Progress":
        return "Đang xử lý";
      case "Wait For Response":
        return "Chờ Shop phản hồi";
      case "Closed":
        return "Đóng";
    }
    return status;
  }

  subStatusMap(substatus) {
    return substatus
      ? {
        reject: "Từ chối xử lý",
        failed: "Xử lý thất bại",
        success: "Xử lý thành công"
      }[substatus]
      : "";
  }

  getStatusSnake(status) {
    return status ? status.replace(" ", "-").toLowerCase() : "";
  }
}
