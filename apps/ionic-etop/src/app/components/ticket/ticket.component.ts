import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Ticket } from '@etop/models';

@Component({
  selector: 'etop-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  @Output() onTicketTouched = new EventEmitter();

  // TODO: refactor to new data type of Ticket...
  @Input() ticket: any;

  constructor() { }

  ngOnInit() {
  }

  getStatusDisplay(status) {
    switch (status) {
      case "Open":
        return "Mới";
      case "Confirmed":
        return "Đã tiếp nhận";
      case "In Progress":
        return "Đang xử lý";
      case "Wait For Response":
        return "Chờ Shop phản hồi";
      case "Closed":
        return "Đóng";
    }
    return status;
  }

  getStatusSnake(status) {
    return status.replace(" ", "-").toLowerCase();
  }

  ticketDetail() {
    this.onTicketTouched.emit(this.ticket);
  }
  subStatusMap(substatus) {
    return substatus
      ? {
        reject: "Từ chối xử lý",
        failed: "Xử lý thất bại",
        success: "Xử lý thành công"
      }[substatus]
      : "";
  }

}
