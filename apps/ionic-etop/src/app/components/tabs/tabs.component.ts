import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'etop-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  routeLink;
  linkSlug = `/s/${this.auth.snapshot.account.url_slug}`;
  tabsList = [
    {
      link: `${this.linkSlug}/orders`,
      icon: 'cart',
      label: 'Đơn hàng'
    },
    {
      link: `${this.linkSlug}/products`,
      icon: 'pricetag',
      label: 'Sản phẩm'
    },
    {
      link: `${this.linkSlug}/pos`,
      icon: 'add',
      label: 'Tạo đơn'
    },
    {
      link: `${this.linkSlug}/customers`,
      icon: 'people',
      label: 'Khách hàng'
    },
    {
      link: `${this.linkSlug}/receipts`,
      icon: 'card',
      label: 'Thu chi'
    }
  ];
  constructor(
    private router: Router,
    private navCtrl: NavController,
    private auth: AuthenticateStore,
    private platform: Platform,
  ) {
    this.changeRoute();
    this.platform.backButton.subscribe(() => {
      debug.log('backButton', this.router.url);
      if (this.router.url === `${this.linkSlug}/orders`) {
        // tslint:disable-next-line: no-string-literal
        navigator['app'].exitApp();
      }
    });
  }

  ngOnInit() {
    this.getColorActive('orders');
  }

  changeRoute() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeLink = event.url;
      }
    });
  }

  getColorActive(link) {
    return link === this.routeLink ? 'primary' : '';
  }

  gotoLink(link) {
    this.navCtrl.navigateForward(link, { animated: false });
  }
}
