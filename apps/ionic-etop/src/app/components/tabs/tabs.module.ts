import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabsComponent } from './tabs.component';



@NgModule({
  declarations: [TabsComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  exports: [TabsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TabsModule {}
