import {Component, OnInit, Output} from '@angular/core';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {map} from "rxjs/operators";
import {Connection} from "libs/models/Connection";
import {Subject} from "rxjs";

@Component({
  selector: 'etop-carriers-list',
  templateUrl: './carriers-list.component.html',
  styleUrls: ['./carriers-list.component.scss']
})
export class CarriersListComponent implements OnInit {
  @Output() onConnectCarrier = new Subject<Connection>();
  availableConnections$ = this.connectionStore.state$.pipe(map(s => s?.availableConnections));
  loggedInConnections$ = this.connectionStore.state$.pipe(map(s => s?.loggedInConnections));

  constructor(
    private connectionStore: ConnectionStore
  ) { }

  ngOnInit() {
  }

  isActive(connection){
    const loggedInConnections = this.connectionStore.snapshot.loggedInConnections;
    return loggedInConnections.map(a=>a.connection_id).includes(connection.id)
  }
  async connectCarrier(carrierConnection: Connection) {
    this.onConnectCarrier.next(carrierConnection);
  }

}
