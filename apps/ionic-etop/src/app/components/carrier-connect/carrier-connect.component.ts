import {Component, Input, OnInit} from '@angular/core';
import {ModalController, PopoverController} from "@ionic/angular";
import {Connection} from "libs/models/Connection";
import {ConnectionAPI} from "@etop/api";
import {ToastService} from "apps/ionic-faboshop/src/app/services/toast.service";
import { ConnectionService } from '@etop/features';

enum Step {
  carriersList = 'carriersList',
  carrierLogin = 'carrierLogin',
  carrierOTP = 'carrierOTP',
  carrierPhone = 'carrierPhone',
  carrierToken = 'carrierToken',
  carrierSignUp = 'carrierSignUp'
}

@Component({
  selector: 'etop-carrier-connect',
  templateUrl: './carrier-connect.component.html',
  styleUrls: ['./carrier-connect.component.scss']
})
export class CarrierConnectComponent implements OnInit {
  @Input() step: Step = Step.carriersList;
  @Input() selectedCarrierConnection: Connection;
  @Input() loginInfo = new ConnectionAPI.LoginShopConnectionRequest();
  @Input() loginOTP = new ConnectionAPI.LoginShopConnectionWithOTPRequest();
  @Input() loginToken = new ConnectionAPI.LoginShopConnectionByTokenRequest();

  signUpInfo = new ConnectionAPI.RegisterShopConnectionRequest();


  constructor(
    private modalCtrl: ModalController,
    private connectionService: ConnectionService,
    private toast: ToastService
  ) { }

  get modalTitle() {
    return this.step == Step.carriersList ? 'Chọn nhà vận chuyển' : 'Kết nối';
  }

  get firstStep() {
    return this.step == Step.carriersList;
  }

  get loginStep() {
    return this.step == Step.carrierLogin;
  }

  get phoneStep() {
    return this.step == Step.carrierPhone;
  }

  get OTPStep() {
    return this.step == Step.carrierOTP;
  }

  get tokenStep() {
    return this.step == Step.carrierToken;
  }

  get signUpStep() {
    return this.step == Step.carrierSignUp;
  }
  ngOnInit() {
  }

  dismiss() {
    if (!this.firstStep) {
      this.step = Step.carriersList;
    } else {
      this.modalCtrl.dismiss().then();
    }
  }

  carrierLogin(selectedCarrierConnection: Connection) {
    this.step = Step.carrierLogin;
    if (selectedCarrierConnection.connection_provider == 'partner') {
      this.step = Step.carrierToken;
    }
    if (selectedCarrierConnection.connection_provider == 'ghn') {
      this.step = Step.carrierPhone;
    }
    this.selectedCarrierConnection = selectedCarrierConnection;
  }

  loginInfoChanged(loginInfo: ConnectionAPI.LoginShopConnectionRequest) {
    this.loginInfo = loginInfo;
  }
  
  loginOTPChanged(loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest) {
    this.loginOTP = loginOTP;
  }

  loginTokenChanged(loginToken: ConnectionAPI.LoginShopConnectionByTokenRequest) {
    this.loginToken = loginToken;
  }

  signUpInfoChanged(signUpInfo: ConnectionAPI.RegisterShopConnectionRequest) {
    this.step = Step.carrierSignUp
    this.signUpInfo = signUpInfo;
  }
  
  confirm() {
    if (!this.loginInfo.email) {
      return this.toast.error('Chưa nhập email.');
    }
    if (!this.loginInfo.password) {
      return this.toast.error('Chưa nhập mật khẩu.');
    }
    this.modalCtrl.dismiss({
      loginInfo: {
        ...this.loginInfo,
        connection_id: this.selectedCarrierConnection.id,
      },
      step: this.step,
      selectedCarrierConnection: this.selectedCarrierConnection
    }).then();
  }

  confirmToken() {
    if (!this.loginToken.user_id) {
      return this.toast.error('Chưa nhập user ID.');
    }
    if (!this.loginToken.token) {
      return this.toast.error('Chưa nhập Token.');
    }
    this.modalCtrl.dismiss({
      loginInfo: {
        ...this.loginToken,
        connection_id: this.selectedCarrierConnection.id,
      },
      step: this.step,
      selectedCarrierConnection: this.selectedCarrierConnection
    }).then();
  }

  confirmOTP() {
    if (!this.loginOTP.otp) {
      return this.toast.error('Vui lòng nhập mã xác thực.');
    }
    this.modalCtrl.dismiss({
      loginInfo: {
        ...this.loginOTP,
        connection_id: this.selectedCarrierConnection.id,
      },
      step: this.step,
      selectedCarrierConnection: this.selectedCarrierConnection
    }).then();
  }

  async continue() {
    try {
      if (!this.loginInfo.identifier) {
        return this.toast.error('Vui lòng nhập số điện thoại.');
      }
      this.loginInfo.connection_id = this.selectedCarrierConnection.id;
      await this.connectionService.loginConnection(this.loginInfo);
      this.step = Step.carrierOTP;
    } catch (e) {
      this.toast.error(e.message);
    }
  }

  validateRegisterInfo(data, provider: string) {
    if (!data.name) {
      this.toast.error('Vui lòng nhập họ tên!');
      return false;
    }
    if (!data.phone) {
      this.toast.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    if (!data.email) {
      this.toast.error('Vui lòng nhập số email!');
      return false;
    }
    if (!data.password && provider == 'ghn') {
      this.toast.error('Vui lòng nhập mật khẩu!');
      return false;
    }
    if (!data.province) {
      this.toast.error('Vui lòng nhập tỉnh thành!');
      return false;
    }
    if (!data.district) {
      this.toast.error('Vui lòng nhập quận huyện!');
      return false;
    }
    if (!data.address) {
      this.toast.error('Vui lòng nhập địa chỉ!');
      return false;
    }
    return true;
  }
  registConnection() {
    if (!this.validateRegisterInfo(this.signUpInfo, this.selectedCarrierConnection.connection_provider)) {
      return 
    }
    this.modalCtrl.dismiss({
      signUpInfo: {
        ...this.signUpInfo,
        connection_id: this.selectedCarrierConnection.id,
      },
      step: this.step,
      selectedCarrierConnection: this.selectedCarrierConnection
    }).then();
  }
  async retryOTP(e) {
    try {
      this.loginInfo.connection_id = this.selectedCarrierConnection.id;
      await this.connectionService.loginConnection(this.loginInfo);
    } catch (e) {
      this.toast.error(e.message);
    }
  }
}
