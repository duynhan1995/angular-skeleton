import {Component, Input, OnInit, Output} from '@angular/core';
import {Connection} from "libs/models/Connection";
import {ConnectionAPI} from "@etop/api";
import {combineLatest, Subject} from "rxjs";
import { LocationQuery } from '@etop/state/location';
import { map, takeUntil } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'etop-carrier-login',
  templateUrl: './carrier-login.component.html',
  styleUrls: ['./carrier-login.component.scss']
})
export class CarrierLoginComponent extends BaseComponent implements OnInit {
  @Output() onLoginInfoChanged = new Subject<ConnectionAPI.LoginShopConnectionRequest>()
  @Output() onSignUpInfoChanged = new Subject<ConnectionAPI.RegisterShopConnectionRequest>()
  @Input() carrierConnection: Connection;
  segment = 'signin';
  loginInfo = new ConnectionAPI.LoginShopConnectionRequest();
  signUpInfo = new ConnectionAPI.RegisterShopConnectionRequest();

  locationForm = this.fb.group({
    provinceCode: '',
    districtCode: ''
  });
  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.locationForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  constructor(
    private locationQuery: LocationQuery,
    private fb: FormBuilder,
  ) { super(); }

  ngOnInit() {
    this.locationForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
    .subscribe(()=> {
      this.signUpInfo.province = this.locationQuery.getProvince(
        this.locationForm.controls['provinceCode'].value)?.name;
      this.signUpInfo.district = this.locationQuery.getDistrict(
        this.locationForm.controls['districtCode'].value)?.name;
      
      this.signUpInfoChanged();
    }
     
    )
  }

  loginInfoChanged() {
    this.onLoginInfoChanged.next(this.loginInfo);
  }

  signUpInfoChanged() {
    this.onSignUpInfoChanged.next(this.signUpInfo);
  }

  async segmentChanged(event) {
    this.segment = event;
    this.signUpInfoChanged()
  }
}
