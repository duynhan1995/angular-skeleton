import { Component, OnInit, Output, Input, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Connection } from 'libs/models/Connection';
import { ConnectionAPI } from '@etop/api';
import { Subject } from 'rxjs';

@Component({
  selector: 'etop-carrier-otp',
  templateUrl: './carrier-otp.component.html',
  styleUrls: ['./carrier-otp.component.scss']
})
export class CarrierOtpComponent implements OnInit, OnChanges {
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onLoginInfoChanged = new Subject<ConnectionAPI.LoginShopConnectionWithOTPRequest>()
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onRetry = new EventEmitter();
  @Input() carrierConnection: Connection;
  @Input() identifier;
  countDown = 600;
  timer;

  loginInfo = new ConnectionAPI.LoginShopConnectionWithOTPRequest();

  constructor() {
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.startTimer();
    this.loginInfo = {
      ...this.loginInfo,
      connection_id: this.carrierConnection.id,
      identifier: this.identifier
    };
  }

  ionViewWillEnter() {
  }

  loginInfoChanged() {
    this.onLoginInfoChanged.next(this.loginInfo);
  }

  retrySendOtp() {
    this.countDown = 600;
    this.onRetry.emit()
  }

  startTimer() {
    const interval = setInterval(() => {
      if (this.countDown < 1) {
        clearInterval(interval);
      } else {
        this.timer = this.displayCount(this.countDown);
        this.countDown -= 1;
      }
    }, 1000);
  }

  displayCount(timer) {
    const minutes = Math.floor(timer / 60);
    const seconds = Math.floor(timer % 60);
    let _seconds: any = seconds;
    if (seconds < 10) {
      _seconds = '0' + seconds;
    }
    return minutes + ':' + _seconds + 's';
  }

}
