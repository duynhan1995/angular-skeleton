import { Component, OnInit, Output, Input, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Connection } from 'libs/models/Connection';
import { ConnectionAPI, ConnectionApi } from '@etop/api';
import { Subject } from 'rxjs';

@Component({
  selector: 'etop-carrier-token',
  templateUrl: './carrier-token.component.html',
  styleUrls: ['./carrier-token.component.scss']
})
export class CarrierTokenComponent implements OnInit, OnChanges {
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onLoginTokenChanged = new Subject<ConnectionAPI.LoginShopConnectionByTokenRequest>()
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onRetry = new EventEmitter();
  @Input() carrierConnection: Connection;
  @Input() identifier;
  countDown = 600;
  timer;
  loading = false;
  loginToken = new ConnectionAPI.LoginShopConnectionByTokenRequest();

  constructor(
  private connectionApi: ConnectionApi,
  ) {
  }

  ngOnInit() {}

  ngOnChanges(): void {
  }

  ionViewWillEnter() {
  }

  loginTokenChanged() {
    this.onLoginTokenChanged.next(this.loginToken);
  }

}
