import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from "@ionic/angular";
import { CarrierLoginComponent } from './carrier-login/carrier-login.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CarriersListComponent } from './carriers-list/carriers-list.component';
import { CarrierConnectComponent } from './carrier-connect.component';
import { CarrierOtpComponent } from './carrier-otp/carrier-otp.component';
import { CarrierTokenComponent } from './carrier-token/carrier-token.component';
import { EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    CarriersListComponent,
    CarrierLoginComponent,
    CarrierConnectComponent,
    CarrierOtpComponent,
    CarrierTokenComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    EtopPipesModule,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CarrierConnectModule { }
