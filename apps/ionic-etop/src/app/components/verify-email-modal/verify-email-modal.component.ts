import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonInput, ModalController } from '@ionic/angular';
import { UserApi } from '@etop/api';
import { ToastService } from '../../../services/toast.service';
import { LoadingService } from '../../../services/loading.service';

@Component({
  selector: 'etop-verify-email-modal',
  templateUrl: './verify-email-modal.component.html',
  styleUrls: ['./verify-email-modal.component.scss']
})
export class VerifyEmailModalComponent implements OnInit {
  @ViewChild('codeVerify1', { static: false }) codeVerify1: IonInput;
  @ViewChild('codeVerify2', { static: false }) codeVerify2: IonInput;
  @ViewChild('codeVerify3', { static: false }) codeVerify3: IonInput;
  @ViewChild('codeVerify4', { static: false }) codeVerify4: IonInput;
  @ViewChild('codeVerify5', { static: false }) codeVerify5: IonInput;
  @ViewChild('codeVerify6', { static: false }) codeVerify6: IonInput;

  @Input() email;

  codeVerify = ['', '', '', '', '', ''];
  sendCode = '';
  countdown = 60;

  constructor(
    private modalCtrl: ModalController,
    private userApi: UserApi,
    private toast: ToastService,
    private userService: UserApi,
    private loadingService: LoadingService
  ) {}

  ngOnInit() {
    setTimeout(() => {
      this.codeVerify1.setFocus();
    }, 150);
    this.countdown = 60;
  }

  ionViewLoaded() {
    setTimeout(() => {
      this.codeVerify1.setFocus();
    }, 150);
  }

  focusInput(index) {
    const input: IonInput = this[`codeVerify${index + 1}`];
    if (index < 6 && index > 0) {
      input.setFocus();
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  onInputChange(event, index?) {
    if (event) {
      this.focusInput(index);
    } else {
      this.focusInput(index - 2);
    }
  }

  onChange(event, index) {
    if (this.codeVerify[index]) {
      this.focusInput(index + 1);
    }
  }

  async verify() {
    for (let index = 0; index < this.codeVerify.length; index++) {
      this.sendCode = this.codeVerify.join('');
      if (this.codeVerify[index] === '') {
        return this.toast.error('Vui lòng nhập đủ mã xác nhận!');
      }
    }
    try {
      this.loadingService.start('Đang xác thực email');
      await this.userApi.verifyEmailUsingOTP(this.sendCode);
      this.loadingService.end();
      this.modalCtrl.dismiss({verify: true});
    } catch (e) {
      this.loadingService.end();
      debug.log('ERROR in verify email by OTP', e);
      this.toast.error(e.message || e.msg);
    }
  }

  async retry() {
    this.codeVerify = ['', '', '', '', '', ''];
    try {
      await this.userService.sendEmailVerificationUsingOTP(this.email);
      this.countdown = 60;
    } catch (e) {
      this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      debug.log('ERROR retry send requestResetPasswordByPhone', e);
    }
  }
}
