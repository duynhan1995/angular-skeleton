import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { Fulfillment, Order } from '@etop/models';
import { ModalController } from '@ionic/angular';
import { environment } from 'apps/core/src/environments/environment';
import { FulfillmentService } from 'apps/ionic-etop/src/services/fulfillment.service';
import { ShopCrmService } from 'apps/shop/src/services/shop-crm.service';
import moment from 'moment';
import { TelegramService } from '@etop/features';
import { ToastService } from '../../../services/toast.service';

enum Type {
  weeken = 'weeken',
  holiday = 'holiday',
  support = 'support',
}
enum TypeRequest {
  picking = 'force-picking',
  delivering = 'force-delivering',
  cod = 'change-shop-cod',
  phone = 'change-phone',
  others = 'others'
}

@Component({
  selector: 'etop-ticket-modal',
  templateUrl: './ticket-modal.component.html',
  styleUrls: ['./ticket-modal.component.scss']
})
export class TicketModalComponent implements OnInit {
  @Input() type: Type = Type.support;
  @Input() ffm: any;
  @Input() note: any;
  @ViewChild('codInput') codInput: ElementRef;

  customAlertOptions: any = {
    cssClass: 'topship'
  }
  loading = false;
  supportRequest = {
    code: '',
    name: '',
    value: '',
    old_value: null,
    reason: ''
  };
  supportRequests = [
    {
      code: TypeRequest.picking,
      name: 'Giục lấy hàng',
      placeholder: null,
    },
    {
      code: TypeRequest.delivering,
      name: 'Giục giao hàng',
      placeholder: null,
    },
    {
      code: TypeRequest.cod,
      name: 'Thay đổi tiền thu hộ',
      placeholder: 'Nhập số tiền thu hộ mới',
    },
    {
      code: TypeRequest.phone,
      name: 'Thay đổi số điện thoại',
      placeholder: 'Nhập số điện thoại mới',
    },
    {
      code: TypeRequest.others,
      name: 'Yêu cầu khác',
      placeholder: null,
    }
  ];

  inputShow = false;
  inputShowPlaceholder = '';

  requestName = 'Gửi yêu cầu hỗ trợ';
  reasons = [
    'Sai thông tin',
    'Khách yêu cầu hủy',
    'Tư vấn sai sản phẩm',
    'Hết hàng',
    'Nhà vận chuyển không lấy hàng',
    'Lý do khác'
  ];
  reasonOption = '';
  reasonInput = '';

  invalid_input = false;
  disabled = true;
  force_warning = false;
  force_title = '';
  force_expected = '';
  money_formatted = false;

  constructor(
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private telegramService: TelegramService,
    private vtigerService: ShopCrmService,
    private fulfillmentService: FulfillmentService,
    private toast: ToastService
  ) { }

  get popoverTitle() {
    return 'Gửi yêu cầu hỗ trợ';
  }

  get isWeeken() {
    return this.type == Type.weeken;
  }

  get isHoliday() {
    return this.type == Type.holiday;
  }

  get isSupport() {
    return this.type == Type.support;
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    if (!this.canForcePicking(this.ffm)) {
      this.supportRequests = this.supportRequests.filter(req => req.code != 'force-picking');
    }
    if (!this.canForceDelivering(this.ffm)) {
      this.supportRequests = this.supportRequests.filter(req => req.code != 'force-delivering');
    }
  }

  async selectRequest(selected_req_code) {
    this.supportRequest.value = '';
    this.supportRequest.old_value = '';
    this.force_warning = false;

    const request = this.supportRequests.find(req => req.code == selected_req_code);
    if (selected_req_code == TypeRequest.phone || selected_req_code == TypeRequest.cod) {
      this.inputShowPlaceholder = request.placeholder;
    }
    this.supportRequest.name = request?.name;
    if (request.code != 'others') {
      this.requestName = this.supportRequest?.name;
    } else {
      this.requestName = 'Gửi yêu cầu hỗ trợ';
    }

    let diff: number;
    switch (selected_req_code) {
      case 'force-delivering':
        this.force_title = 'GIAO';
        this.force_expected = moment(this.ffm.expected_delivery_at).format('HH:mm DD/MM');
        diff = moment(moment.now()).diff(moment(this.ffm.expected_delivery_at), 'hours');
        if (diff <= 0) {
          this.force_warning = true;
        }
        break;
      case 'force-picking':
        this.force_title = 'LẤY';
        this.force_expected = `trước ${moment(this.ffm.expected_pick_at).format('HH:mm DD/MM')}`;
        diff = moment(moment.now()).diff(moment(this.ffm.expected_pick_at), 'hours');
        if (diff <= 0) {
          this.force_warning = true;
        }
        break;
      case 'change-shop-cod':
        this.supportRequest.old_value = `${this.ffm.total_cod_amount}`;
        break;
      case 'change-phone':
        this.supportRequest.old_value = `${this.ffm.shipping_address.phone}`;
        break;
      default:
        break;
    }
  }

  async sendSupReq() {
    try {
      this.loading = true;
      let shop = this.auth.snapshot.shop;
      if (!shop) {
        shop = this.auth.snapshot.accounts.find(a => a.id === this.ffm.order.shop_id).shop;
      }
      this.supportRequest.reason.trim();
      if (!this.supportRequest.reason) {
        this.toast.error('Vui lòng nhập lý do');
        this.loading = false;
        return;
      }

      let old_value = '';
      let telegram_request = { ...this.supportRequest, title: this.supportRequest.name };
      if (this.supportRequest.value) {
        switch (this.supportRequest.code) {
          case 'change-phone':
            try {
              this.validatePhoneNumber(`${this.supportRequest.value}`);
            } catch (e) {
              this.loading = false;
              this.toast.error(e.message);
              return false;
            }
            old_value = this.ffm.shipping_address.phone;
            break;
          case 'change-shop-cod':
            const _value = Number(this.supportRequest.value);
            if (_value > 0 && _value < 5000) {
              this.loading = false;
              this.invalid_input = true;
              return this.toast.error('Vui lòng nhập tiền thu hộ bằng 0đ hoặc lớn hơn 5.000đ');
            }
            old_value = await this.telegramService.moneyFormat(this.ffm.total_cod_amount);
            this.supportRequest.value = await this.telegramService.moneyFormat(this.supportRequest.value);
            break;
          default:
            break;
        }
        telegram_request = { ...this.supportRequest, old_value, title: this.supportRequest.name };
      }

      this.createVtigerTicket(shop, telegram_request, this.ffm.order, this.ffm);
      this.telegramService.forceMessage(telegram_request, this.ffm);
      this.toast.success(`Đã gửi yêu cầu ${this.supportRequest.name}`);
      this.dismiss()
    } catch (e) {
      this.toast.error(e.message || 'Tạo yêu cầu thất bại');
      debug.error(e.message);
    }
    this.loading = false;
  }

  async createVtigerTicket(shop, request, order: Order, ffm: Fulfillment) {
    try {
      const {
        shipping_service_code, shipping_provider
      } = ffm;
      let account = 'unknown';
      let login = '';
      switch (shipping_provider) {
        case 'ghn':
          if (shipping_service_code.charAt(0) === 'D') {
            account = 'TK 300 gr';
            login = '0938222489';
          } else if (shipping_service_code.charAt(0) === 'E') {
            account = 'TK 500 gr';
            login = '0976358769';
          }
          break;
        case 'ghtk':
          if (shipping_service_code.charAt(1) === 'D') {
            account = 'TK Default';
            login = 'giang2224@gmail.com';
          } else if (shipping_service_code.charAt(2) === 'S') {
            account = 'TK Đồng giá';
            login = 'giang@etop.vn';
          }
          break;
        default:
          account = 'vtpost';
      }
      let body = Object.assign({}, request, {
        etop_id: shop.owner_id,
        environment: environment.ticket_environment,
        order_id: order?.id,
        order_code: order.code,
        ffm_code: ffm.shipping_code,
        ffm_url: `https://admin.etop.vn/admin/ffms/${ffm.id}`,
        company: shop.name,
        account: this.auth.snapshot.user,
        provider: shipping_provider.toUpperCase(),
        note: `
    ${shipping_provider.toUpperCase()} ${account}
    ${login ? `Login ${login}` : ''}
    `
      });
      await this.vtigerService.createTicket(body);
    } catch (e) {
      throw e;
    }
  }

  validateData() {
    this.supportRequest.reason.trim();
    if (!this.supportRequest.reason || !this.supportRequest.code) {
      this.disabled = true;
    } else if (this.supportRequest.old_value 
      && !this.supportRequest.value 
      && this.supportRequest.code != 'others') {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
  }

  canForcePicking(fulfillment) {
    return this.fulfillmentService.canForcePicking(fulfillment);
  }

  canForceDelivering(fulfillment) {
    return this.fulfillmentService.canForceDelivering(fulfillment);
  }

  validatePhoneNumber(phone) {
    if (phone && phone.match(/^0[0-9]{9,10}$/)) {
      if (phone.length < 10) {
        this.toast.error('Vui lòng nhập số điện thoại di động 10 số');
      }
      return true;
    }
    this.toast.error('Vui lòng nhập số điện thoại hợp lệ');
  }
  dismiss() {
    this.modalController.dismiss().then();
  }

}
