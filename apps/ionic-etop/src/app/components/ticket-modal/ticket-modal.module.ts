import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonInputFormatNumberModule, EtopPipesModule } from '@etop/shared';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    IonInputFormatNumberModule,
    EtopPipesModule
  ]
})
export class TicketModalModule {}
