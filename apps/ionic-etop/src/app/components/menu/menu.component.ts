import { SubscriptionQuery } from './../../../../../../libs/state/shop/subscription/subscription.query';
import { SubscriptionService } from '@etop/features';
import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { NavController, MenuController, Platform } from '@ionic/angular';
import { Market } from '@ionic-native/market/ngx';
import { Plugins, NetworkStatus } from '@capacitor/core';
import { NavigationEnd, Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { GaService } from 'apps/ionic-etop/src/services/ga.service';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';
import { NetworkService } from 'apps/ionic-etop/src/services/network.service';
import { takeUntil } from 'rxjs/operators';
const { Device, App } = Plugins;

enum Menu {
  dashboard = 'dashboard',
  products = 'products',
  lines = 'lines',
  orders = 'orders',
  customers = 'customers',
  receipts = 'receipts',
  inventory = 'inventory',
  staff = 'staff',
  account = 'account',
  eorder = 'eorder'
}
@Component({
  selector: 'etop-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent extends BaseComponent implements OnInit {
  subscriptions$ = this.subscriptionQuery.selectAll();
  trialDate$ = this.subscriptionQuery.select("trialDate");
  menus = [
    {
      icon: 'icon_dashboard.png',
      title: 'Tổng quan',
      link: `dashboard`,
      permissions: ['shop/dashboard:view']
    },
    {
      icon: 'icon_product.png',
      title: 'Sản phẩm',
      link: `products`,
      permissions: ['shop/product/basic_info:view']
    },
    {
      icon: 'icon_pos.png',
      title: 'Bán hàng',
      link: `pos`,
      permissions: ['shop/order:create']
    },
    // {
    //   icon: 'icon_pos_dathang.png',
    //   title: 'Đặt hàng',
    //   link: `dat-hang`,
    //   css: 'text-dark'
    // },
    {
      icon: 'icon_pos_invoice.png',
      title: 'Quản lý hóa đơn',
      link: `orders`,
      permissions: ['shop/order:view', 'shop/purchase_order:view']
    },
    // {
    //   icon: 'icon_pos_QL_dathang.png',
    //   title: 'Quản lý đặt hàng',
    //   link: `dat-hang`,
    //   css: 'text-dark'
    // },
    {
      icon: 'icon_customer.png',
      title: 'Khách hàng',
      link: `customers`,
      permissions: ['shop/customer:view']
    },
    {
      icon: 'icon_receipt.png',
      title: 'Thu chi',
      link: `receipts`,
      permissions: ['shop/receipt:view']
    },
    {
      icon: 'icon_inventory.png',
      title: 'Tồn kho',
      link: `inventory`,
      permissions: ['shop/inventory:view']
    },
    {
      icon: 'icon_partner.png',
      title: 'Nhân viên',
      link: `staff`,
      permissions: ['relationship/relationship:view']
    },
    {
      icon: 'icon_settings.png',
      title: 'Thiết lập',
      link: `account`
    }
  ];

  menusTwo = [
    {
       icon: 'icon_store.png',
       title: 'Nguồn hàng',
       link: `etop-trading`
    },
    {
      icon: 'icon_support.png',
      title: 'Hỗ trợ',
      link: `supports`
    }
    // {
    //   icon: 'icon_support.png',
    //   title: 'Hỗ trợ',
    //   link: `supports`
    // },
    // {
    //   icon: 'icon_tut.png',
    //   title: 'Hướng dẫn sử dụng',
    //   link: `guide`
    // }
  ];

  menuSubscription = {
    icon: 'icon_pos.png',
    title: 'Quản lý bán hàng',
    link: `subscription`
  }

  current_menu: Menu;
  version;

  status: NetworkStatus;
  listener: any;

  swipeGestureEnable = [
    'dashboard',
    'products',
    'lines',
    'orders',
    'customers',
    'receipts',
    'inventory',
    'staff',
    'account',
    'eorder'
  ];

  constructor(
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private menu: MenuController,
    private market: Market,
    private platform: Platform,
    private router: Router,
    private ga: GaService,
    private network: NetworkService,
    private util: UtilService,
    private subscriptionQuery: SubscriptionQuery
  ) {
    super();
    this.platform.backButton.subscribe(() => {
      if (this.router.url === `${this.linkSlug}/orders`) {
        // tslint:disable-next-line: no-string-literal
        navigator['app'].exitApp();
      }
    });
    this.status = this.network.status;
  }

  get swipeGesture() {
    const router = location.pathname.split('/').pop();
    return this.swipeGestureEnable.indexOf(router) > -1;
  }
  get permissionsOfActionsArray() {
    let permissions = [];
    this.menus.forEach(m => {
      if (m.permissions && m.permissions.length) {
        permissions = permissions.concat(m.permissions);
      }
    });
    return permissions;
  }

  get account() {
    return this.auth.snapshot.account;
  }

  get user() {
    return this.auth.snapshot.user;
  }

  get linkSlug() {
    if (this.auth.snapshot.account) {
      this.auth.updatePermissions(
        this.auth.snapshot.account.user_account.permission
      );
    }
    return this.auth.snapshot.account
      ? `/s/${this.auth.snapshot.account.url_slug || this.util.getSlug()}`
      : '';
  }

  async ngOnInit() {
    const info = await Device.getInfo();
    this.version = info.appVersion;
    if (this.auth.snapshot.account) {
      this.auth.updatePermissions(
        this.auth.snapshot.account.user_account.permission
      );
    }
    
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
          const menu: any = event.url.split('/')[3];
          if (menu) {
            this.changeMenu(menu);
          } else {
            this.changeMenu(Menu.dashboard);
          }
      }
    });
  }

  changeMenu(link) {
    this.current_menu = link
  }

  gotoLink(link) {
    this.menu.close().then();
    this.changeMenu(link);
    this.navCtrl.navigateForward(`${this.linkSlug}/${link}`, {
      // skipLocationChange: true,
      replaceUrl: true,
      animated: false
    }).then();
  }

  gotoProfile() {
    this.menu.close();
    this.navCtrl.navigateForward(`${this.linkSlug}/account`);
  }

  async openAppStore() {
    if (this.platform.is('ios')) {
      const res = await App.canOpenUrl({
        url: 'https://apps.apple.com/us/app/topship-topship-vn/id1453126665'
      });
      if (res.value) {
        await App.openUrl({
          url: 'https://apps.apple.com/us/app/topship-topship-vn/id1453126665'
        });
      } else {
        this.market.open('id1453126665');
      }
    } else {
      const res = await App.canOpenUrl({ url: 'vn.topship.app' });
      if (res.value) {
        await App.openUrl({ url: 'vn.topship.app' });
      } else {
        this.market.open('vn.topship.app');
      }
    }
    this.ga.logEvent('app_promotion', { app_name: 'Topship' });
  }
}
