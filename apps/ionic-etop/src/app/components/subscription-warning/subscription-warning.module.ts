import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionWarningComponent } from './subscription-warning.component'
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    SubscriptionWarningComponent
  ],
  exports: [
    SubscriptionWarningComponent
  ],
  imports: [
    CommonModule, IonicModule
  ]
})
export class SubscriptionWarningModule { }
