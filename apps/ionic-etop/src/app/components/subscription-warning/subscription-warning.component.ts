import { SubscriptionService } from '@etop/features';
import { Component, OnInit } from '@angular/core';
import { CmsService } from 'apps/core/src/services/cms.service';
import { SubscriptionQuery } from '@etop/state/shop/subscription/subscription.query';
import { SubscriptionStore } from '@etop/state/shop/subscription/subscription.store';
@Component({
  selector: 'shop-subscription-warning',
  templateUrl: './subscription-warning.component.html',
  styleUrls: ['./subscription-warning.component.scss']
})
export class SubscriptionWarningComponent implements OnInit {
  subscriptions$ = this.subscriptionQuery.selectAll();
  trialDate$ = this.subscriptionQuery.select("trialDate");
  popupTrial = this.subscriptionQuery.getValue().popupTrial;
  banner = '';
  checkClose = false;
  trialSubscription;
  subscription
  constructor(
    private cms: CmsService,
    private subscriptionQuery: SubscriptionQuery,
    private subscriptionStore: SubscriptionStore,
  ) { }

  async ngOnInit() {
    this.getTrialSubscriptionBanner();
  }

  async getTrialSubscriptionBanner(){
    await this.cms.initBanners();
    const result = this.cms.getMessageNotSubscription();
    if(result){
      this.banner = result.message_app;
      this.banner = this.banner.replace('{{trial_remain}}', (this.subscriptionQuery.getValue().trialDate).toString());
    }
  }

  closeNoti(){
    this.popupTrial = false;
    this.subscriptionStore.update({popupTrial: this.popupTrial});
  }

}
