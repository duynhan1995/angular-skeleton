import {NgModule} from "@angular/core";
import {EmptyComponent} from "./empty.component";
import {CommonModule} from "@angular/common";
import {IonicModule} from "@ionic/angular";

@NgModule({
  declarations: [
    EmptyComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    EmptyComponent
  ]
})

export class EmptyModule {}
