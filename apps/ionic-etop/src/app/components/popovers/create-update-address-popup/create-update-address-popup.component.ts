import {Component, Input, OnInit} from '@angular/core';
import {Address} from 'libs/models/Address';
import {ModalController} from '@ionic/angular';
import {BaseComponent} from '@etop/core';
import {ToastService} from 'apps/ionic-faboshop/src/app/services/toast.service';
import {LocationQuery} from '@etop/state/location/location.query';
import { District, Province, Ward } from '@etop/models';
import { LocationService } from '@etop/state/location';
import { CustomerService } from 'apps/ionic-etop/src/services/customer.service';
import { AddressService } from 'apps/core/src/services/address.service';
import { PosQuery, PosService } from '@etop/state/etop-app/pos';

@Component({
  selector: 'etop-create-update-address-popup',
  templateUrl: './create-update-address-popup.component.html',
  styleUrls: ['./create-update-address-popup.component.scss']
})
export class CreateUpdateAddressPopupComponent extends BaseComponent implements OnInit {
  @Input() type: 'shipfrom' | 'shipto' = 'shipfrom';
  @Input() address = new Address({});
  provincesList: Province[] = [];
  districtsList: District[] = [{ id: null, code: null, name: 'Chưa chọn tỉnh thành' }];
  wardsList: Ward[] = [{ id: null, code: null, name: 'Chưa chọn quận huyện' }];

  customAlertOptions: any = {
    cssClass: 'topship'
  }

  initializing = true;

  constructor(
    private toast: ToastService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private modalCtrl: ModalController,
    private customerService: CustomerService,
    private addressService: AddressService,
    private posQuery: PosQuery,
    private posService: PosService,
  ) {
    super();
  }

  get typeDisplay() {
    return this.type == 'shipfrom' ? 'lấy' : 'giao';
  }

  get popoverTitle() {
    return this.address?.id && `Cập nhật địa chỉ ${this.typeDisplay} hàng` || `Tạo địa chỉ ${this.typeDisplay} hàng`;
  }

  get confirmBtnTitle() {
    return this.address?.id && `Cập nhật địa chỉ` || `Tạo địa chỉ`;
  }

  ngOnInit() {
    this.provincesList = this.locationQuery.getValue().provincesList;
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
  }

  dismiss() {
    this.modalCtrl.dismiss({closed: true}).then();
  }

  async confirm() {
    const {full_name, phone, province_code, district_code, ward_code, address1, id} = this.address;

    if (!full_name) {
      return this.toast.error('Chưa nhập tên');
    }
    if (!phone) {
      return this.toast.error('Chưa nhập số điện thoại');
    }
    if (!province_code) {
      return this.toast.error('Chưa nhập tỉnh thành');
    }
    if (!district_code) {
      return this.toast.error('Chưa nhập quận huyện');
    }
    if (!ward_code) {
      return this.toast.error('Chưa nhập phường xã');
    }
    if (!address1) {
      return this.toast.error('Chưa nhập địa chỉ cụ thể');
    }

    if (this.type == 'shipfrom') {
      try {
        const _address = await this.createOrUpdateShopAddress({ ...this.address, type: this.type}).then();
        const mess = this.address?.id && `Cập nhật địa chỉ lấy hàng thành công.` || `Tạo địa chỉ lấy hàng thành công.`;
        this.address = _address;
        this.toast.success(mess);
        this.modalCtrl.dismiss({
          address: this.address
        }).then();
      } catch (e) {
        debug.error('ERROR in createOrUpdateShopAddress', e);
        const mess = this.address?.id && `Cập nhật địa chỉ lấy hàng thất bại.` || `Tạo địa chỉ lấy hàng thất bại.`;
        this.toast.error(`${mess} ${e.code && (e.message || e.msg)}`).then();
      }
    } else {
      try {
        await this.createOrUpdateCustomerAddress({ ...this.address, type: this.type}).then();
        const mess = this.address?.id && `Cập nhật địa chỉ giao hàng thành công.` || `Tạo địa chỉ giao hàng thành công.`;
        this.toast.success(mess);
        this.modalCtrl.dismiss({
          address: this.address
        }).then();
      } catch (e) {
        debug.error('ERROR in createOrUpdateCustomerAddress', e);
        const mess = this.address?.id && `Cập nhật địa chỉ giao hàng thất bại.` || `Tạo địa chỉ giao hàng thất bại.`;
        this.toast.error(`${mess} ${e.code && (e.message || e.msg)}`).then();
      }
    }
  }

  async createOrUpdateShopAddress(address: Address) {
    let res : any;
    if (address.id) {
      res = await this.addressService.updateAddress(address);
    } else {
      res = await this.addressService.createAddress(address);
    }
    return res;
  }

  async createOrUpdateCustomerAddress(address: Address) {
    const {customer_id, id, customer } = this.posQuery.getValue().order;
    if ( !customer_id && id || customer?.deleted) { return; }
    if ( !customer_id ) {
      try {
        const newCustomer = await this.customerService.createCustomer({
          full_name: address.full_name,
          phone: address.phone
        });
        const order = this.posQuery.getValue().order
        this.posService.updateOrder({...order,
          customer_id: newCustomer.id ? newCustomer.id  : null,
          customer: newCustomer.id ? null : order.customer,
        })
        try {
          const res =  await this.customerService.createCustomerAddress({
            ...address,
            customer_id: newCustomer.id
          });
          return res;
        } catch (e) {
          debug.error('ERROR in createCustomerAddress', e);
          throw (e);
        }
      } catch(e) {
        debug.error('ERROR in createCustomer', e);
        throw (e);
      }
    }

    if (address.id) {
      await this.customerService.updateCustomerAddress(address);
    } else {
      await this.customerService.createCustomerAddress({...address, customer_id: customer_id});
    }
  }

  onProvinceSelected() {
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.address.province = this.locationQuery.getProvince(this.address.province_code)?.name;
    this.address.district_code = '';
    this.address.ward_code = '';
  }

  onDistrictSelected() {
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.address.district = this.locationQuery.getDistrict(this.address.district_code)?.name;
    this.address.ward_code = '';
  }

  onWardSelected() {
    this.address.ward = this.locationQuery.getWard(this.address.ward_code)?.name;
  }
}
