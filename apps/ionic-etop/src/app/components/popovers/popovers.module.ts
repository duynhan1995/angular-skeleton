import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUpdateAddressPopupComponent } from './create-update-address-popup/create-update-address-popup.component';
import {IonicModule} from "@ionic/angular";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ListAddressesPopupComponent } from './list-addresses-popup/list-addresses-popup.component';
import {EtopPipesModule} from "@etop/shared";
import { CreateCustomerPopupComponent } from './create-customer-popup/create-customer-popup.component';
import { SelectSuggestModule } from '../select-suggest/select-suggest.module';

@NgModule({
  declarations: [CreateUpdateAddressPopupComponent, ListAddressesPopupComponent, CreateCustomerPopupComponent],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        EtopPipesModule,
        ReactiveFormsModule,
        SelectSuggestModule
    ]
})
export class PopoversModule { }
