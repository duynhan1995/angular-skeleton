import {Component, Input, OnInit} from '@angular/core';
import {Address} from "libs/models/Address";
import {PopoverController} from "@ionic/angular";

@Component({
  selector: 'etop-list-addresses-popup',
  templateUrl: './list-addresses-popup.component.html',
  styleUrls: ['./list-addresses-popup.component.scss']
})
export class ListAddressesPopupComponent implements OnInit {
  @Input() type: 'from' | 'to' = 'from';
  @Input() selectedAddressID: string;
  @Input() addresses: Address[];

  constructor(
    private popoverController: PopoverController,
  ) { }

  get typeDisplay() {
    return this.type == 'from' ? 'lấy' : 'giao';
  }

  get popoverTitle() {
    return `Chọn địa chỉ ${this.typeDisplay} hàng`;
  }

  get emptyTitle() {
    return `Chưa có địa chỉ ${this.typeDisplay} hàng`;
  }

  isSelectedAddress(address: Address) {
    return this.selectedAddressID == address.id;
  }

  ngOnInit() {}

  dismiss() {
    this.popoverController.dismiss({closed: true}).then();
  }

  selectAddress(address: Address) {
    this.popoverController.dismiss({address}).then();
  }

  newAddress() {
    this.popoverController.dismiss({createAddress: true}).then();
  }

}
