import {Component, Input, OnInit} from '@angular/core';
import {Customer} from "libs/models/Customer";
import {PopoverController} from "@ionic/angular";
import {ToastService} from "apps/ionic-faboshop/src/app/services/toast.service";

@Component({
  selector: 'etop-create-customer-popup',
  templateUrl: './create-customer-popup.component.html',
  styleUrls: ['./create-customer-popup.component.scss']
})
export class CreateCustomerPopupComponent implements OnInit {
  @Input() customer: Customer;

  constructor(
    private popoverController: PopoverController,
    private toast: ToastService,
  ) {}

  ngOnInit() {}

  dismiss() {
    this.popoverController.dismiss({closed: true}).then();
  }

  create() {
    if (!this.customer.full_name) {
      return this.toast.error('Chưa nhập tên.');
    }
    if (!this.customer.phone) {
      return this.toast.error('Chưa nhập số điện thoại.')
    }
    this.popoverController.dismiss({customer: this.customer}).then();
  }

}
