import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { UserApi } from '@etop/api';
import { Validators, FormBuilder } from '@angular/forms';
import { ToastService } from '../../../services/toast.service';
import { LoadingService } from '../../../services/loading.service';
@Component({
  selector: 'etop-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  passwordForm = this.fb.group({
    current_password: ['', Validators.required],
    new_password: ['', Validators.required],
    confirm_password: ['', Validators.required]
  });
  constructor(
    private modalCtrl: ModalController,
    private auth: AuthenticateStore,
    private userService: UserApi,
    private toast: ToastService,
    private fb: FormBuilder,
    private loadingService: LoadingService
  ) { }

  ngOnInit() { }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  validate() {
    const data = this.passwordForm.value;
    const { current_password, new_password, confirm_password } = data;
    if (!current_password) {
      throw new Error('Vui lòng nhập mật khẩu hiện tại.');
    }
    if (!new_password) {
      throw new Error('Vui lòng nhập mật khẩu mới.');
    }
    if (new_password !== confirm_password) {
      throw new Error('Mật khẩu nhập lại không chính xác.');
    }
  }

  async submit() {
    const data = this.passwordForm.value;
    const { current_password, new_password, confirm_password } = data;

    try {
      this.validate();
    }catch (e) {
      this.toast.error(e.message);
      return;
    }

    try {
      await this.loadingService.start('Đang cập nhật');
      const body = {
        login: this.auth.snapshot.user.email || this.auth.snapshot.user.phone,
        current_password,
        new_password,
        confirm_password
      };
      await this.userService.changePassword(body);
      setTimeout(async () => {
        await this.loadingService.end();
        this.toast.success('Thay đổi mật khẩu thành công.');
        this.dismiss();
      }, 1000)
    } catch (e) {
      this.loadingService.end();
      this.toast.error(e.message);
    }
  }
}
