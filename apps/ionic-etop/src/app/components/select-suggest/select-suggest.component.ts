import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter, NgZone, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { ValueAccessorBase } from 'apps/core/src/interfaces/ValueAccessorBase';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { IonInput, ModalController } from '@ionic/angular';
import { SearchSuggestComponent } from '../search-modal/search-suggest/search-suggest.component';

@Component({
  selector: 'ionetop-select-suggest',
  templateUrl: './select-suggest.component.html',
  styleUrls: ['./select-suggest.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: SelectSuggestComponent, multi: true },
  ]
})
export class SelectSuggestComponent extends ValueAccessorBase<any> implements OnInit, OnChanges {
  @ViewChild(IonInput) myInput: IonInput;
  @Input() placeholder;
  @Input() label;
  @Input() options;
  @Input() cssItemClass;
  @Input() cssInputClass;
  @Input() position;
  @Input() hasLabel: boolean = true;
  @Input() lines = '';
  _value: any;
  _options: any;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private modalCtrl: ModalController,
    private zone: NgZone,
  ) {
      super();
    }

  ngOnInit(): void {}
  
  ngOnChanges(changes: SimpleChanges) {
    this.writeValue(this.value);
    this.changeDetector.detectChanges();
  }

  writeValue(value: any) {
    super.writeValue(value);
    if (value) {
      this._value = this.options.find(option => option.code == value)?.name;
    } else {
      this._value = '';
      this.value = '';
    }
    this.changeDetector.detectChanges();
  }

  async modalSuggest(event) {
    const modal = await this.modalCtrl.create({
      component: SearchSuggestComponent,
      componentProps: { options: this.options, title: this.label || this.placeholder },
      animated: false,
    });
    modal.onDidDismiss().then(async (data: any) => {
      if (data?.data) {
        this.value = data.data.code;
        this._value = data.data.name;
      };
    });
    return await modal.present();
  }
}