import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EtopAppRoutingModule } from 'apps/ionic-etop/src/app/etop-app/etop-app-routing.module';
import { EtopAppComponent } from './etop-app.component';
import { EtopAppGuard } from './etop-app.guard';
import { IonicModule } from '@ionic/angular';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';

const pages = [];

@NgModule({
  declarations: [EtopAppComponent, ...pages],
  providers: [EtopAppGuard, Deeplinks],
  imports: [
    CommonModule,
    IonicModule,
    EtopPipesModule,
    NgbModule,
    EtopAppRoutingModule
  ]
})
export class EtopAppModule {}
