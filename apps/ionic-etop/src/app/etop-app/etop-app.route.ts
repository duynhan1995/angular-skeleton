import { Routes } from '@angular/router';

export const eTopAppRoutes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('../pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'products',
    loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'shipping',
    loadChildren: () => import('../pages/shipping/shipping.module').then(m => m.ShippingModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('../pages/customers/customers.module').then(m => m.CustomersModule)
  },
  {
    path: 'pos',
    loadChildren: () => import('../pages/pos/pos.module').then(m => m.PosModule)
  },
  {
    path: 'receipts',
    loadChildren: () => import('../pages/receipts/receipts.module').then(m => m.ReceiptsModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'staff',
    loadChildren: () => import('../pages/staff/staff.module').then(m => m.StaffModule)
  },
  {
    path: 'etop-trading',
    loadChildren: () => import('../pages/etop-trading/etop-trading.module').then(m => m.EtopTradingModule)
  },
  {
    path: 'supports',
    loadChildren: () => import('../pages/supports/supports.module').then(m => m.SupportsModule)
  },
  {
    path: 'guide',
    loadChildren: () => import('../pages/guide/guide.module').then(m => m.GuideModule)
  },
  {
    path: 'inventory',
    loadChildren: () => import('../pages/inventory/inventory.module').then(m => m.InventoryModule)
  },
  {
    path: 'account',
    loadChildren: () => import('../pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'dat-hang',
    loadChildren: () => import('../pages/ecomify-order/ecomify-order.module').then(m => m.OrdersModule)
  },
  {
    path: 'etop-trading',
    loadChildren: () => import('../pages/etop-trading/etop-trading.module').then(m => m.EtopTradingModule)
  },
  {
    path: 'subscription',
    loadChildren: () => import('../pages/not-subscription/not-subscription.module').then(m => m.NotSubscriptionModule)
  },
  {
    path: '**',
    redirectTo: 'subscription'
  }
];
