import { SubscriptionStore } from '@etop/state/shop/subscription/subscription.store';
import { AuthenticateStore } from '@etop/core';
import { SubscriptionService } from './../../../../../libs/state/shop/subscription/subscription.service';
import {Component, OnInit} from '@angular/core';
import {ConnectionService} from '@etop/features';
import {BankService} from "@etop/state/bank";
import { SubscriptionQuery } from '@etop/state/shop/subscription/subscription.query';

@Component({
  selector: 'etop-etop-app',
  templateUrl: './etop-app.component.html',
  styleUrls: ['./etop-app.component.scss']
})
export class EtopAppComponent implements OnInit {
  constructor(
    private connectionService: ConnectionService,
    private bankService: BankService,
    private subscriptionService: SubscriptionService,
    private auth: AuthenticateStore,
    private subscriptionStore: SubscriptionStore,
  ) {
  }

  async ngOnInit() {
    this.connectionService.getValidConnections().then();
    this.bankService.initBanks().then();
    let filters = [];
        filters.push({
            name: "account_id",
            op: "=",
            value: this.auth.snapshot.account.id
        });
    this.subscriptionService.setFilters(filters);
    this.subscriptionService.getSubscriptions();
    const trialDate = this.subscriptionService.checkTrialSubscriptions();
    this.subscriptionStore.update({trialDate});
  }
}
