import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { eTopAppRoutes } from './etop-app.route';
import { EtopAppComponent } from './etop-app.component';
import { EtopAppGuard } from './etop-app.guard';

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [EtopAppGuard],
    resolve: {
      account: EtopAppGuard
    },
    component: EtopAppComponent,
    children: eTopAppRoutes
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class EtopAppRoutingModule {}
