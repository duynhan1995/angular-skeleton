import {Injectable} from '@angular/core';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {AuthenticateStore} from '@etop/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from 'apps/core/src/services/user.service';
import {GoogleAnalyticsService} from 'apps/core/src/services/google-analytics.service';
import {Account} from 'libs/models/Account';
import {UserApi} from '@etop/api';
import {ToastController, NavController, LoadingController} from '@ionic/angular';

import {Plugins} from '@capacitor/core';
import { TelegramService } from '@etop/features';
import {GaService} from '../../services/ga.service';

const {Device} = Plugins;

@Injectable()
export class EtopAppCommonUsecase extends CommonUsecase {
  app = 'dashboard';
  password;
  email;
  confirm;
  error = false;

  signupData: any = {};

  loading = false;

  provinces;
  openAppLinks = false;

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore,
    private gaService: GoogleAnalyticsService,
    private activatedRoute: ActivatedRoute,
    private toastController: ToastController,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private telegramService: TelegramService,
    private ga: GaService,
  ) {
    super();
  }

  private checkInvitationToken() {
    return this.activatedRoute.snapshot.queryParamMap.get('t');
  }

  async checkAuthorization(fetchAccount = false) {
    const ref = this.router.url.split('?')[0];
    try {
      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(true);
      if (!this.auth.snapshot.shop) {
        this.router.navigateByUrl('/survey');
      }

      const pathname = this.router.url.split('?')[0];
      if (pathname.startsWith('/s/')) {
        const index = pathname.split('/')[2];
        this.auth.selectAccount(index);
      } else {
        this.auth.selectAccount(null);
      }
      await this.navigateAfterAuthorized();
    } catch (e) {
      this.auth.clear();
      this.auth.setRef(ref);
      if (ref.indexOf('login') == -1) {
        await this.router.navigate(['/login'], {
          queryParams: {invitation_token: this.checkInvitationToken()}
        });
      }
    }
  }

  async navigateAfterAuthorized() {
    const user = this.auth.snapshot.user;
    this.ga.setUserId(this.auth.snapshot.account.id);
    this.ga.setUserProperty(
      'is_email_verified',
      !!user.email_verified_at
    );
    this.ga.setUserProperty(
      'is_phone_verified',
      !!user.phone_verified_at
    );
    if (!this.openAppLinks) {
      const ref = this.auth.getRef(true).replace('/', '');
      const defaultRef = 'subscription';
      if (ref == 'login' || ref == 'register') {
        await this.router.navigate([`./${defaultRef}`]);
      } else {
        debug.log('GET REF', this.auth.currentAccountIndex());
        await this.router.navigateByUrl(ref || `/s/${this.auth.currentAccountIndex()}/subscription`);
      }
    }
  }

  async login(
    data: { login: string; password: string },
    after_register?: boolean
  ) {
    try {
      const res = await this.userApi.login({
        login: data.login,
        password: data.password,
        account_type: 'shop'
      });
      this.gaService.login(this.email);
      this.auth.updateToken(res.access_token);
      this.auth.updateUser(res.user);
      // tslint:disable-next-line: variable-name
      const invitation_token = this.activatedRoute.snapshot.queryParamMap.get('invitation_token');
      // tslint:disable-next-line: variable-name
      const invitation_phone = this.activatedRoute.snapshot.queryParamMap.get('invitation_phone');
      if (!res.account && !invitation_token && !invitation_phone) {
        this.loading = false;
        return await this.navCtrl.navigateForward('/survey', {
          animated: false
        });
      }
      await this.setupAndRedirect(
        invitation_token,
        after_register,
        invitation_phone
      );
    } catch (e) {
      const toast = await this.toastController.create({
        message: e.message,
        duration: 2000,
        color: 'danger',
        mode: 'md',
        position: 'top',
        cssClass: 'font-12'
      });
      toast.present();
      return;
    }
  }

  // tslint:disable-next-line: variable-name
  async setupAndRedirect(invitation_token?: string, after_register?: boolean, invitation_phone?: string) {
    if (invitation_token) {
      await this.router.navigate(['/invitation'], {
        queryParams: {t: invitation_token}
      });
    } else if (invitation_phone) {
      await this.router.navigateByUrl(`/i/p${invitation_phone}`);
    } else {
      await this.updateSessionInfo(true);
      this.auth.selectAccount(0);
      const ref = this.auth.getRef(true);
      await this.router.navigateByUrl(
        `/s/${this.auth.snapshot.account.url_slug || 0}/${ref || 'dashboard'}`
      );
    }
  }

  async updateSessionInfo(fetchAccounts = true) {
    const res = await this.userService.checkToken(this.auth.snapshot.token);

    let {access_token, account, shop, user, available_accounts} = res;
    const shop_accounts = available_accounts
      .filter(a => a.type === 'shop')
      .sort((a, b) => a.id > b.id);

    let no_init_shop = true;

    if (shop) {
      no_init_shop = false;
    }
    if (!shop && available_accounts && available_accounts.length) {
      shop = shop_accounts[0];
      account = shop_accounts[0];
    }
    const accounts: Account[] = fetchAccounts
      ? await Promise.all(
        shop_accounts.map(async (a, index) => {
          const accRes = await this.userService.switchAccount(a.id);
          a.token = accRes.access_token;
          a.shop = accRes.shop;
          a.id = accRes.shop && accRes.shop.id;
          a.image_url = a.shop.image_url;
          a.display_name = `${a.shop.code} - ${a.shop.name}`;
          a.permission = accRes.account.user_account.permission;
          return new Account(a);
        })
      )
      : this.auth.snapshot.accounts;

    if (accounts.length > 0) {
      this.auth.updateInfo({
        token: (no_init_shop && accounts[0].token) || access_token,
        account: {
          ...account,
          ...shop,
          display_name: `${shop.code} - ${shop.name}`
        },
        accounts,
        shop,
        user,
        permission: account.user_account.permission,
        isAuthenticated: true,
        uptodate: true
      });
    }
  }

  async register(data: any, source) {
    const loading = await this.loadingController.create({
      duration: 2000,
      message: 'Đang đăng ký...',
      translucent: true,
      cssClass: 'custom-class custom-loading font-12'
    });
    await loading.present();
    const res = await this.userService.signUpUsingToken({...data, source});
    this.signupData = Object.assign({}, data, res.user);
    const info = await Device.getInfo();
    this.telegramService.newMerchantMessage(this.signupData, info);
    this.ga.logEvent('account_create', {
      name: this.signupData.full_name,
      phone: this.signupData.phone,
      email: this.signupData.email
    });
    this.login(
      {
        login: this.signupData.email,
        password: this.signupData.password
      },
      true
    );
    this.gaService.registerSuccessfully(data.email, data.phone, data.full_name);
  }

  async redirectIfAuthenticated(): Promise<any> {
    return this.checkAuthorization();
  }
}
