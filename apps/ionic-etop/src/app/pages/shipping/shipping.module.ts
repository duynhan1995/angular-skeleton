import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EtopPipesModule } from '@etop/shared/pipes/etop-pipes.module';
import { AuthenticateModule } from '@etop/core';
import { AddressApi, FulfillmentApi } from '@etop/api';
import { FulfillmentService } from '@etop/features/fabo/fulfillment/fulfillment.service';
import {CustomerService} from "apps/ionic-etop/src/services/customer.service";
import { OrderService } from 'apps/ionic-etop/src/services/order.service';
import { ShippingComponent } from './shipping.component';
import { IonShipmentServiceOptionsComponent } from 'apps/ionic-etop/src/app/pages/shipping/components/ion-shipment-service-options/ion-shipment-service-options.component';
import { SharedModule } from '../../features/shared/shared.module';
import { PopoversModule } from '../../components/popovers/popovers.module';
import { RouterModule, Routes } from '@angular/router';
import { IonInputFormatNumberModule } from '@etop/shared/components/etop-ionic/ion-input-format-number/ion-input-format-number.module';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { IonShipmentServiceOptionsPlaceholderComponent } from './components/ion-shipment-service-options-placeholder/ion-shipment-service-options-placeholder.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ShippingComponent },
    ]
  }
];

const components = [
  ShippingComponent,
  IonShipmentServiceOptionsComponent,
];

@NgModule({
  declarations: [...components, IonShipmentServiceOptionsPlaceholderComponent],
  entryComponents: [],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    PopoversModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    IonInputFormatNumberModule
  ],
  providers: [
    FulfillmentApi,
    FulfillmentService,
    OrderService,
    PosStoreService,
    ReceiptService,
    AddressApi,
    CustomerService
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShippingModule {}
