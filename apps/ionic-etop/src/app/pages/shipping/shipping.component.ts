import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {NavController, PopoverController, ModalController, IonRouterOutlet} from '@ionic/angular';
import {FulfillmentAPI, FulfillmentApi} from '@etop/api';
import {Address} from 'libs/models/Address';
import {FulfillmentStore} from "apps/core/src/stores/fulfillment.store";
import {Fulfillment, FulfillmentShipmentService, TRY_ON_OPTIONS} from 'libs/models/Fulfillment';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ListAddressesPopupComponent} from '../../components/popovers/list-addresses-popup/list-addresses-popup.component';
import {CreateUpdateAddressPopupComponent} from '../../components/popovers/create-update-address-popup/create-update-address-popup.component';
import {ToastService} from '../../../services/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {ActivatedRoute} from '@angular/router';
import {PosQuery, PosService} from "@etop/state/etop-app/pos";
import {AddressService} from "apps/core/src/services/address.service";
import {IonShipmentServiceOptionsComponent} from "apps/ionic-etop/src/app/pages/shipping/components/ion-shipment-service-options/ion-shipment-service-options.component";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {ConnectionStore} from "@etop/features";
import {CustomerService} from "apps/ionic-etop/src/services/customer.service";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'etop-order-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent extends BaseComponent implements OnInit {
  @ViewChild('shipmentServicesOptions', {static: false}) shipmentServicesOptions: IonShipmentServiceOptionsComponent
  popover: HTMLIonPopoverElement;

  fromAddresses: Address[] = [];
  toAddresses: Address[] = [];

  fulfillmentForm = this.fb.group({
    chargeable_weight: null,
    cod_amount: null,
    try_on: null,
    shipping_note: '',
    include_insurance: false,
    shipping_service_name: '',
    shipping_service_code: '',
    shipping_service_fee: '',
    connection_id: '',

    pickup_address: this.fb.group({
      province_code: '',
      province: '',
      district_code: '',
      district: '',
      ward_code: '',
      ward: '',
      address1: '',
      full_name: '',
      phone: '',
      id: ''
    }),
    shipping_address: this.fb.group({
      province_code: '',
      province: '',
      district_code: '',
      district: '',
      ward_code: '',
      ward: '',
      address1: '',
      full_name: '',
      phone: '',
      id: ''
    })
  });

  suggestions = false;
  total_amout: number;

  customAlertOptions: any = {
    cssClass: 'topship'
  };

  connectionStoreReady$ = this.connectionStore.state$.pipe(map(s => s?.validConnections?.length));

  defaultWeight$ = this.ffmStore.state$.pipe(
    map(s => s?.defaultWeight), distinctUntilChanged()
  );

  constructor(
    private auth: AuthenticateStore,
    private fb: FormBuilder,
    private popoverController: PopoverController,
    private ffmApi: FulfillmentApi,
    private toast: ToastService,
    private navCtrl: NavController,
    private loading: LoadingService,
    private customerService: CustomerService,
    private addressService: AddressService,
    private posQuery: PosQuery,
    private posService: PosService,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private activatedRoute: ActivatedRoute,
    private connectionStore: ConnectionStore,
    private locationQuery: LocationQuery,
    private changeDetector: ChangeDetectorRef,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  get pickupAddressForm() {
    return this.fulfillmentForm.controls.pickup_address as FormGroup;
  }

  get shippingAddressForm() {
    return this.fulfillmentForm.controls.shipping_address as FormGroup;
  }

  get includeInsuranceForm() {
    return this.fulfillmentForm.controls.include_insurance as FormControl;
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  dismiss() {
    this.navCtrl.back();
  }

  ngOnInit() {
    this.ffmStore.initDefaultWeight();
    this.getServicesHandlers();
  }

  async ionViewDidEnter() {
    const order = this.posQuery.getValue().order;
    const { total_amount } = order;
    this.total_amout = total_amount;

    await this.prepareFromAddress();
    await this.prepareToAddress();

    this.prepareFormData();
  }

  onFormBlur() {
    this.getServices().then();
  }

  getServicesHandlers() {
    this.pickupAddressForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getServices().then();
      });

    this.shippingAddressForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getServices().then();
      });

    this.includeInsuranceForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getServices().then();
      });
  }

  async getServices() {
    const from_address = this.pickupAddressForm.getRawValue();
    const to_address = this.shippingAddressForm.getRawValue();

    const rawData = this.fulfillmentForm.getRawValue();
    const {chargeable_weight, cod_amount, include_insurance} = rawData;

    const order = this.posQuery.getValue().order;
    const {basket_value} = order;

    const data: FulfillmentAPI.GetShipmentServicesRequest = {
      from_province_code: from_address.province_code,
      from_district_code: from_address.district_code,
      from_ward_code: from_address.ward_code,

      to_province_code: to_address.province_code,
      to_district_code: to_address.district_code,
      to_ward_code: to_address.ward_code,

      chargeable_weight,
      total_cod_amount: cod_amount || 0,
      basket_value,
      include_insurance
    };

    if (this.connectionStore.snapshot.validConnections?.length) {
      await this.shipmentServicesOptions.getServices(data);
    } else {
      this.connectionStoreReady$.pipe(takeUntil(this.destroy$))
        .subscribe(async() => {
          await this.shipmentServicesOptions.getServices(data);
        });
    }
  }

  async prepareFromAddress(addressId = null) {
    try {
      const res = await this.addressService.getAddresses();
      const shopAddresses = res.filter(a => a.type == 'shipfrom');
      this.fromAddresses = shopAddresses;
      const selectedAddressID = addressId ? addressId : this.auth.snapshot.shop?.ship_from_address_id
      const pickupAddress = shopAddresses.find(addr => addr.id == selectedAddressID) || shopAddresses[0];

      const {province_code, district_code, ward_code, address1, full_name, phone, id} = pickupAddress;

      this.pickupAddressForm.patchValue({
        province_code, district_code, ward_code, address1, full_name, phone, id,
        province: this.locationQuery.getProvince(province_code)?.name,
        district: this.locationQuery.getDistrict(district_code)?.name,
        ward: this.locationQuery.getWard(ward_code)?.name
      });
    } catch(e) {
      debug.error('ERROR in prepareFromAddress', e);
    }
  }

  async prepareToAddress() {
    const customerID = this.posQuery.getValue().order.customer_id;
    if (!customerID) { return; }
    try {
      const customerAddresses = await this.customerService.getCustomerAddresses(customerID);
      this.toAddresses = customerAddresses;

      const shippingAddress = customerAddresses[0];

      if (!shippingAddress) { return; }

      const {
        province_code, district_code, ward_code, address1,
        full_name, phone, id
      } = shippingAddress;

      this.shippingAddressForm.patchValue({
        province_code, district_code, ward_code, address1,
        full_name, phone, id,
        province: this.locationQuery.getProvince(province_code)?.name,
        district: this.locationQuery.getDistrict(district_code)?.name,
        ward: this.locationQuery.getWard(ward_code)?.name
      });
    } catch(e) {
      debug.error('ERROR in prepareToAddress', e);
    }
  }

  prepareFormData() {
    this.fulfillmentForm.patchValue({
      try_on: this.auth.snapshot.shop.try_on,
      shipping_note: '',
      include_insurance: false
    }, {emitEvent: false});

    const orderWeight = this.posQuery.getValue().order?.shipping?.chargeable_weight // NOTE: Import Orders from Web
    const lastWeight = this.posQuery.getValue().lastWeight;
    const shopDefaultWeight = this.ffmStore.snapshot.defaultWeight;

    if (shopDefaultWeight) {
      const chargeableWeight = orderWeight || lastWeight || shopDefaultWeight;
      this.fulfillmentForm.patchValue({
        chargeable_weight: chargeableWeight
      });
      this.getServices().then();
    } else {
      this.defaultWeight$.pipe(takeUntil(this.destroy$))
        .subscribe(value => {
          const chargeableWeight = orderWeight || lastWeight || value;
          this.fulfillmentForm.patchValue({
            chargeable_weight: chargeableWeight
          });
          this.getServices().then();
        });
    }
  }

  // TODO: Output of ShipmentServiceOptionsComponent
  onSelectShipmentService(service: FulfillmentShipmentService) {
    this.fulfillmentForm.patchValue({
      shipping_service_name: service?.name,
      shipping_service_code: service?.code,
      shipping_service_fee: service?.fee,
      connection_id: service?.connection_info?.id
    }, {emitEvent: false});
  }

  private validateFulfillmentInfo(ffm: Fulfillment): boolean {
    const {
      pickup_address,
      shipping_address,
      chargeable_weight,
      cod_amount,
      shipping_service_code
    } = ffm;
    if (!pickup_address?.district_code) {
      this.toast.error('Chưa chọn địa chỉ lấy hàng!').then();
      return false;
    }

    if (!shipping_address?.district_code) {
      this.toast.error('Chưa chọn địa chỉ giao hàng!').then();
      return false;
    }

    if (!chargeable_weight) {
      this.toast.error('Chưa nhập khối lượng!').then();
      return false;
    }

    if (cod_amount == null) {
      this.toast.error('Chưa nhập giá trị thu hộ!').then();
      return false;
    }

    if (cod_amount > 0 && cod_amount < 5000 ) {
      this.toast.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc lớn hơn 5.000đ!').then();
      return false;
    }

    if (!shipping_service_code) {
      this.toast.error('Chưa chọn gói vận chuyển!').then();
      return false;
    }

    return true;
  }

  async createShipment() {
    const ffmData = this.fulfillmentForm.getRawValue();
    if (!this.validateFulfillmentInfo(ffmData)) {
      return;
    }

    this.loading.start('Đang xử lý...').then();

    try {
      let order_id: string;
      const order = this.posQuery.getValue().order;
      if (!order.id) {
        try {
          const _order: any = await this.posService.createOrder(order);
          order_id = _order.id;
        } catch (e) {
          debug.error('ERROR in create order', e);
          throw(e);
        }
      } else {
        order_id = order.id;
      }

      const {
        pickup_address, shipping_address,
        chargeable_weight, cod_amount,
        shipping_service_code, shipping_service_fee, shipping_service_name, connection_id,
        try_on, shipping_note, include_insurance
      } = ffmData;

      const ffmBody: FulfillmentAPI.CreateShipmentFulfillmentRequest = {
        pickup_address, shipping_address,
        chargeable_weight: Number(chargeable_weight),
        cod_amount: Number(cod_amount),
        shipping_service_fee: Number(shipping_service_fee),
        shipping_service_code, shipping_service_name, connection_id,
        try_on, shipping_note, include_insurance,
        order_id,
        shipping_type: 'shipment'
      };

      try {
        await this.ffmApi.createShipmentFulfillment(ffmBody);
        this.posService.updateLastWeight(ffmBody.chargeable_weight);
      } catch (e) {
        debug.error('ERROR in create Shipment Fulfillment', e);
        throw(e);
      }
      this.posService.resetStore();

      const ref = this.activatedRoute.snapshot.queryParamMap.get('ref');
      if (ref == "pos") {
        const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
        await this.navCtrl.navigateForward(`/s/${slug}/pos/success?order_id=${order_id}`);
      } else {
        this.navCtrl.back();
      }

    } catch (e) {
      debug.error('ERROR in create shipment', e);
      this.toast.error(`Tạo đơn giao hàng không thành công. ${e.code && (e.message || e.msg) || ''}`).then();
    }
    this.loading.end();
  }

  codSelected(amount) {
    this.fulfillmentForm.patchValue({
      cod_amount: Number(amount)
    });
    this.getServices().then();
    this.suggestions = false;
  }

  focusSuggest() {
    const scrollContent: any = document.getElementById('codInput');
    scrollContent.scrollIntoView();
  }

  hideSuggest() {
    setTimeout(() => {
      this.suggestions = false;
    }, 200);
  }

  toggleInsurance() {
    const _current = this.fulfillmentForm.controls.include_insurance.value;
    this.fulfillmentForm.patchValue({
      include_insurance: !_current
    });
  }

  async changeAddress(type: 'shipfrom' | 'shipto') {
    if (this.popover) { return; }
    const fromAddresses = this.fromAddresses;
    const toAddresses = this.toAddresses;

    this.popover = await this.popoverController.create({
      component: ListAddressesPopupComponent,
      translucent: true,
      componentProps: {
        type,
        selectedAddressID: type == 'shipfrom' ? this.pickupAddressForm.getRawValue().id : this.shippingAddressForm.getRawValue().id,
        addresses: type == 'shipfrom' ? fromAddresses : toAddresses
      },
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });

    this.popover.onDidDismiss().then(async (data) => {
      this.popover = null;
      if (data.data?.closed) { return; }
      if (data.data?.createAddress) {
        const customer = this.posQuery.getValue().order.customer || this.posQuery.getValue().customer;
        this.createOrUpdateAddress(type, new Address({
          full_name: type == 'shipto' ? customer?.full_name : null,
          phone: type == 'shipto' ? customer?.phone : null
        })).then();
      }
      if (data.data?.updateAddress) {
        this.createOrUpdateAddress(type, data.data?.updateAddress).then();
      }
      if (data.data?.address) {
        const _address = data.data?.address;
        const {
          province_code, district_code, ward_code, address1,
          full_name, phone, id
        } = _address;
        if (type == 'shipfrom') {
          this.pickupAddressForm.patchValue({
            province_code, district_code, ward_code, address1, full_name, phone, id,
            province: this.locationQuery.getProvince(province_code)?.name,
            district: this.locationQuery.getDistrict(district_code)?.name,
            ward: this.locationQuery.getWard(ward_code)?.name
          });
        } else {
          this.shippingAddressForm.patchValue({
            province_code, district_code, ward_code, address1,
            full_name, phone, id,
            province: this.locationQuery.getProvince(province_code)?.name,
            district: this.locationQuery.getDistrict(district_code)?.name,
            ward: this.locationQuery.getWard(ward_code)?.name
          });
        }
      }

    });
    this.popover.present().then();
  }

  async createOrUpdateAddress(type: 'shipfrom' | 'shipto', address: Address) {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async (data) => {
      if (data.data?.closed) {
        return this.changeAddress(type);
      }
      if (data.data?.address) {
        const _address = data.data?.address;
        const {
          province_code, district_code, ward_code, address1,
          full_name, phone, id
        } = _address;
        if (type == 'shipfrom') {
          this.prepareFromAddress(data.data?.address.id).then();
        } else {
          this.shippingAddressForm.patchValue({
            province_code, district_code, ward_code, address1,
            full_name, phone, id,
            province: this.locationQuery.getProvince(province_code)?.name,
            district: this.locationQuery.getDistrict(district_code)?.name,
            ward: this.locationQuery.getWard(ward_code)?.name
          });
          this.changeDetector.detectChanges();
          this.prepareToAddress().then();
        }
      }
    });
    await modal.present().then();
  }
}
