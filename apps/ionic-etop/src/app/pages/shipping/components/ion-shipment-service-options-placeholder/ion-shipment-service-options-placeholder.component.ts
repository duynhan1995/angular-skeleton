import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'etop-ion-shipment-service-options-placeholder',
  templateUrl: './ion-shipment-service-options-placeholder.component.html',
  styleUrls: ['./ion-shipment-service-options-placeholder.component.scss']
})
export class IonShipmentServiceOptionsPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
