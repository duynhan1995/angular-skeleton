import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FulfillmentAPI} from "@etop/api";
import {map, takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";
import {ShipmentCarrier, FulfillmentShipmentService} from 'libs/models/Fulfillment';
import {FulfillmentService} from 'apps/ionic-etop/src/services/fulfillment.service';
import {ConnectionService, ConnectionStore} from '@etop/features/connection';

@Component({
  selector: 'etop-ion-shipment-service-options',
  templateUrl: './ion-shipment-service-options.component.html',
  styleUrls: ['./ion-shipment-service-options.component.scss']
})
export class IonShipmentServiceOptionsComponent extends BaseComponent implements OnInit {
  @Output() serviceSelect = new EventEmitter<FulfillmentShipmentService>();
  carriers: ShipmentCarrier[] = [];

  getServicesIndex = 0;

  errorMessage = '';
  connectionsList$ = this.connectionStore.state$.pipe(map(s => s?.validConnections));

  fromWard = '';
  toWard = '';

  selectedShipmentService: FulfillmentShipmentService;

  constructor(
    private connectionStore: ConnectionStore,
    private connectionService: ConnectionService,
    private ffmService: FulfillmentService,
  ) {
    super();
  }

  isSelectedService(service: FulfillmentShipmentService) {
    return this.selectedShipmentService?.unique_id == service?.unique_id;
  }

  ngOnInit() {
    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.carriers = connections?.map(conn => {
          return {
            name: conn.name,
            id: conn.id,
            logo: conn.provider_logo,
            services: [],
            from_topship: conn.connection_method == 'builtin'
          };
        });
      });
  }

  reset() {
    this.fromWard = '';
    this.toWard = '';
    this.errorMessage = '';
    this.selectedShipmentService = null;
    this.serviceSelect.emit(this.selectedShipmentService);

    this.carriers = this.connectionStore.snapshot.validConnections
      .filter(conn => conn.connection_subtype == 'shipment')
      .map(conn => {
        return {
          name: conn.name,
          id: conn.id,
          logo: conn.provider_logo,
          services: [],
          from_topship: conn.connection_method == 'builtin'
        };
      });
  }

  selectService(service: FulfillmentShipmentService) {
    this.selectedShipmentService = service;
    this.serviceSelect.emit(this.selectedShipmentService);
  }

  async getServices(data: FulfillmentAPI.GetShipmentServicesRequest) {
    this.reset();
    this.fromWard = data.from_ward_code;
    this.toWard = data.to_ward_code;

    if (!this.fromWard ||!this.toWard || !data.chargeable_weight) {
      return;
    }

    try {
      this.getServicesIndex++;

      const promises = this.carriers.map(carrier => async() => {

        carrier.loading = true;

        try {
          const body: FulfillmentAPI.GetShipmentServicesRequest = {
            ...data,
            connection_ids: [carrier.id],
            index: this.getServicesIndex
          };

          const res = await this.ffmService.getShipmentServices(body);
          if (res.index == this.getServicesIndex) {
            carrier.services = res.services;
            carrier = this.ffmService.sortAndFilterShipmentServices([carrier])[0];
            carrier.loading = false;
          }
        } catch(e) {
          debug.error('ERROR in Getting Shipment Services', e);
          carrier.loading = false;
        }

      });

      await Promise.all(promises.map(p => p()));

    } catch (e) {
      debug.error(e);
      this.errorMessage = e.message;
    }
  }

}
