import { Component, OnInit, Input } from '@angular/core';
import { Relationship } from 'libs/models/Authorization';
import { ModalController } from '@ionic/angular';
import { StaffDetailComponent } from '../staff-detail/staff-detail.component';
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: 'etop-staff-management-row',
  templateUrl: './staff-management-row.component.html',
  styleUrls: ['./staff-management-row.component.scss']
})
export class StaffManagementRowComponent implements OnInit {
  @Input() staff = new Relationship();

  constructor(
    private authorizationService: AuthorizationService,
    private modalController: ModalController
  ) {}

  get isOwner() {
    return this.staff.roles.indexOf('owner') != -1;
  }

  ngOnInit() {}

  async staffDetail(staff) {
    const modal = await this.modalController.create({
      component: StaffDetailComponent,
      componentProps: {
        staff
      },
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.authorizationService.updateRelationship();
      }
    });
    return await modal.present();
  }
}
