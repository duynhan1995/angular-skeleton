import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Invitation } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';
import { ToastService } from '../../../../services/toast.service';
import { LoadingService } from '../../../../services/loading.service';
import { AlertController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import {AuthorizationService} from "@etop/state/authorization";
const { Share, Clipboard } = Plugins;

@Component({
  selector: 'etop-user-invitation-row',
  templateUrl: './user-invitation-row.component.html',
  styleUrls: ['./user-invitation-row.component.scss']
})
export class UserInvitationRowComponent implements OnInit {
  @Input() invitation = new Invitation({});
  @Output() cancelInvitation = new EventEmitter();

  constructor(
    private authorizationService: AuthorizationService,
    private toast: ToastService,
    private loadingService: LoadingService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  async del() {
    const alert = await this.alertController.create({
      header: 'Xóa lời mời',
      message: `Bạn có chắc xóa "<strong>${this.invitation.full_name}</strong>" trong danh sách lời mời.`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger',
          handler: () => {
            this.deleteInvitation();
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  async deleteInvitation() {
    try {
      this.loadingService.start('Đang hủy lời mời');
      await this.authorizationService.deleteInvitation(this.invitation.token);
      this.cancelInvitation.emit();
      setTimeout(() => {
        this.loadingService.end();
      }, 2000)
    } catch (e) {
      this.loadingService.end();
      this.toast.error('Huỷ lời mời không thành công. Vui lòng thử lại!');
      debug.error('ERROR in deleting Invitation', e);
    }
  }

  async shareLink(link) {
    await Share.share({text: link});
  }

  copyLink(link) {
    Clipboard.write({ string: link });
    this.toast.success('Sao chép thành công')
  }

  async resendInvitation() {
    try {
      await this.authorizationService.resendInvitation(this.invitation.email, this.invitation.phone);
      this.toast.success('Gửi lại lời mời thành công');
    } catch (e) {
      this.toast.error(e.message)
    }
  }
}
