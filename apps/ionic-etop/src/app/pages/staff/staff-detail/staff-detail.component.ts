import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { AuthorizationApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { ToastService } from '../../../../services/toast.service';
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: 'etop-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.scss']
})
export class StaffDetailComponent implements OnInit {
  @Input() staff;
  full_name = '';
  roles: any = [];

  roleList = [
    { name: 'Kế toán', value: 'accountant', checked: false },
    { name: 'Quản lý kho', value: 'inventory_management', checked: false },
    { name: 'Thu mua', value: 'purchasing_management', checked: false },
    { name: 'Bán hàng', value: 'salesman', checked: false },
    { name: 'Quản lý nhân viên', value: 'staff_management', checked: false },
    {
      name: 'Phân tích (Đang phát triển)',
      value: 'analysis',
      disabled: true,
      checked: false
    }
  ];
  constructor(
    private modalCtrl: ModalController,
    private authorizationAPI: AuthorizationApi,
    private auth: AuthenticateStore,
    private toast: ToastService,
    private authorizationService: AuthorizationService,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.full_name = this.staff.full_name;
    this.roleList.map(role => {
      if (this.staff.roles.includes(role.value)) {
        role.checked = true;
      }
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async updateStaff() {
    this.roles = [];
    this.roleList.forEach(role => {
      if (role.checked) {
        this.roles.push(role.value);
      }
    });
    try {
      await this.authorizationAPI.updateRelationship(
        this.full_name,
        this.staff.user_id
      );
      await this.authorizationAPI.updatePermission(
        this.roles,
        this.staff.user_id
      );
      this.toast.success('Cập nhật nhân viên thành công.');
      this.modalCtrl.dismiss(this);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('error updateStaff', e);
    }
  }

  async del() {
    const alert = await this.alertController.create({
      header: 'Xóa lời mời',
      message: `Bạn có chắc xóa nhân viên "<strong>${this.staff.full_name}</strong>".`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger',
          handler: () => {
            this.removeStaff();
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async removeStaff() {
    try {
      await this.authorizationAPI.removeUserFromAccount(this.staff.user_id);
      this.authorizationService.removeRelationship();
      this.toast.success('Xoá nhân viên thành công.');
      this.modalCtrl.dismiss(this);
    } catch (e) {
      this.toast.error(e.message);
      debug.error('ERROR in removing Staff', e);
    }
  }
}
