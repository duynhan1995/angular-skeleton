import { Component, OnInit } from '@angular/core';
import { Relationship } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../../../services/toast.service';

@Component({
  selector: 'etop-add-staff-modal',
  templateUrl: './add-staff-modal.component.html',
  styleUrls: ['./add-staff-modal.component.scss']
})
export class AddStaffModalComponent implements OnInit {
  invitation = new Relationship();
  roles = [];
  roleList = [
    { name: 'Kế toán', value: 'accountant', checked: false },
    { name: 'Quản lý kho', value: 'inventory_management', checked: false },
    { name: 'Thu mua', value: 'purchasing_management', checked: false },
    { name: 'Bán hàng', value: 'salesman', checked: false },
    { name: 'Quản lý nhân viên', value: 'staff_management', checked: false },
    {
      name: 'Phân tích (Đang phát triển)',
      value: 'analysis',
      disabled: true,
      checked: false
    }
  ];
  constructor(private authorizationAPI: AuthorizationApi, private modalCtrl: ModalController, private toast: ToastService) {}

  ngOnInit(): void {}

  dismiss() {
    this.modalCtrl.dismiss();
  }
  async createInvitation() {
    try {
      const { email, phone, full_name } = this.invitation.p_data;
      if (!full_name) {
        return this.toast.error('Chưa nhập tên nhân viên!');
      }
      if (!phone) {
        return this.toast.error('Chưa nhập số điện thoại!');
      }
      this.roles = [];
      this.roleList.forEach(role => {
        if (role.checked) {
          this.roles.push(role.value);
        }
      });
      if (!this.roles || !this.roles.length) {
        return this.toast.error('Chưa chọn vai trò!');
      }
      const body = {
        phone,
        email,
        full_name,
        roles: this.roles
      };
      const res = await this.authorizationAPI.createInvitation(body);
      this.toast.success('Thêm nhân viên thành công');
      this.modalCtrl.dismiss(res);
    } catch (e) {
      this.toast.error(
        'Thêm nhân viên không thành công. Vui lòng thử lại!' +
          e.message
      );
      debug.error('ERROR in creating Invitation', e);
    }
  }
}
