import { Component, OnInit, NgZone } from '@angular/core';
import { Invitation, Relationship } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from '@ionic/angular';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent, AuthenticateStore } from '@etop/core';
import { AddStaffModalComponent } from './add-staff-modal/add-staff-modal.component';
import { FilterOperator } from '@etop/models';
import {AuthorizationQuery} from "@etop/state/authorization";

@Component({
  selector: 'etop-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent extends BaseComponent implements OnInit {
  shopInvitations: Invitation[] = [];
  relationships: Relationship[] = [];
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');
  segment = 'staff';
  private modal: any;

  constructor(
    private authorizationApi: AuthorizationApi,
    private authorizationQuery: AuthorizationQuery,
    private modalController: ModalController,
    private ngZone: NgZone,
    private auth: AuthenticateStore
  ) {
    super();
  }

  async ngOnInit() {
    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(cancelled => {
        if (cancelled) {
          this.getShopInvitations();
        }
      });

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe(deleted => {
        if (deleted) {
          this.getRelationships();
        }
      });
    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(updated => {
        if (updated) {
          this.getRelationships();
        }
      });
  }

  async ionViewWillEnter() {
    this.segment = 'staff';
    this.ngZone.run(async () => {
      await this.getRelationships();
      await this.getShopInvitations();
    });
  }

  disableView(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }
  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      this.shopInvitations = await this.authorizationApi.getShopInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
    } catch (e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }

  async getRelationships() {
    try {
      this.relationships = await this.authorizationApi.getRelationships({});
      this.relationships = this.relationships.filter(r => !r.deleted);
    } catch (e) {
      debug.error('ERROR in getting Relationships of Shop', e);
    }
  }

  async addStaff() {
    const modal = await this.modalController.create({
      component: AddStaffModalComponent,
      componentProps: {},
      animated: false
    });
    modal.onDidDismiss().then(invitation => {
      if (invitation.data) {
        this.ngZone.run(async () => {
          await this.getShopInvitations();
        });
      }
    });
    return await modal.present();
  }

  segmentChanged(event) {
    debug.log('event', event);
    this.ngZone.run(() => {
      this.segment = event;
    });
  }
  cancelInvitation() {
    this.ngZone.run(async () => {
      await this.getShopInvitations();
    });
  }
}
