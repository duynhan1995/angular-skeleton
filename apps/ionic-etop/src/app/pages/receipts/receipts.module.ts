import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { TabsModule } from '../../components/tabs/tabs.module';
import { MenuModule } from '../../components/menu/menu.module';
import { ReceiptsComponent } from './receipts.component';
import { ReceiptDetailComponent } from './receipt-detail/receipt-detail.component';
import { ReceiptService } from '../../../../../shop/src/services/receipt.service';
import { PurchaseOrderApi } from '@etop/api';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { ReceiptListComponent } from './receipt-list/receipt-list.component';
import { IonSkeletonModule } from '../../components/ion-skeleton/ion-skeleton.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ReceiptListComponent },
      { path: ':id', component: ReceiptDetailComponent }
    ]
  }
];

@NgModule({
  declarations: [ReceiptsComponent, ReceiptDetailComponent, ReceiptListComponent],
  entryComponents: [ReceiptDetailComponent],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    IonSkeletonModule,
    SubscriptionWarningModule
  ],
  providers: [ReceiptService, PurchaseOrderApi],
  exports: [ReceiptsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptsModule {}
