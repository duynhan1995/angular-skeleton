import { Component, Input, OnInit, NgZone } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { OrderApi, PurchaseOrderApi, ReceiptApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute } from '@angular/router';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { Receipt } from '@etop/models';

@Component({
  selector: 'etop-receipt-detail',
  templateUrl: './receipt-detail.component.html',
  styleUrls: ['./receipt-detail.component.scss']
})
export class ReceiptDetailComponent implements OnInit {
  receipt = new Receipt({});
  showList = true;
  receipt_lines = [];

  constructor(
    private orderApi: OrderApi,
    private purchaseOrderApi: PurchaseOrderApi,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private util: UtilService,
    private activatedRoute: ActivatedRoute,
    private receiptService: ReceiptService,
    private zone: NgZone
  ) {}

  async ngOnInit() {}

  async ionViewWillEnter() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.receipt = await this.receiptService.getReceipt(id);
    await this.prepareReceiptLines();
  }

  dismiss() {
    this.navCtrl.back();
  }

  toggleList() {
    this.showList = !this.showList;
  }

  async prepareReceiptLines() {
    try {
      const line_ids = this.receipt.lines
        .filter(l => l.ref_id && l.ref_id != '0')
        .map(l => l.ref_id);
      let refs: any[];
      switch (this.receipt.ref_type) {
        case 'order':
          refs = await this.orderApi.getOrdersByIDs(line_ids);
          break;
        case 'purchase_order':
          refs = await this.purchaseOrderApi.getPurchaseOrdersByIDs(line_ids);
          break;
        default:
          refs = [];
          break;
      }
      this.receipt_lines = refs;
    } catch (e) {
      debug.error('ERROR in preparing Receipt Lines', e);
      this.receipt_lines = [];
    }
  }

  async orderDetail(order) {
    if (this.auth.snapshot.permission.permissions.includes('shop/order:view')) {
      this.zone.run(async () => {
        await this.navCtrl.navigateForward(
          `/s/${this.util.getSlug()}/orders/${order.id}`
        );
      });
    }
  }

  getReceiptStatus(status) {
    return ReceiptApi.receiptStatusMap(status);
  }

  getLedgerDisplay(type) {
    return ReceiptApi.ledgerTypeMap(type);
  }

  getRefType(type) {
    return ReceiptApi.refTypeMap(type);
  }

  getReceiptTypeMap(type) {
    return ReceiptApi.receiptTypeMap(type);
  }
}
