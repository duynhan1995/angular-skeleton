import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { Receipt } from 'libs/models/Receipt';
import { Plugins, NetworkStatus } from '@capacitor/core';
import { UtilService } from 'apps/core/src/services/util.service';

const { Network } = Plugins;


@Component({
  selector: 'etop-receipt-list',
  templateUrl: './receipt-list.component.html',
  styleUrls: ['./receipt-list.component.scss']
})
export class ReceiptListComponent implements OnInit {
  receipts: Receipt[] = [];
  loading = true;
  status: NetworkStatus;
  viewDisconnect = false;
  networkStatus = true;

  constructor(
    private receiptService: ReceiptService,
    private zone: NgZone,
    private navCtrl: NavController,
    private util: UtilService
  ) {
    this.viewDisconnect = false;
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        if (this.receipts.length == 0) {
          this.viewDisconnect = true;
        }
        this.networkStatus = false;
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getReceipts();
          setTimeout(() => {
            this.networkStatus = true;
          }, 1000);
        });
      } else {
        this.zone.run(async () => {
          this.networkStatus = false;
          if (!this.receipts) {
            this.viewDisconnect = true;
          }
        });
      }
    });
  }

  ngOnInit() {
    this.getReceipts();
  }

  async getStatus() {
    return await Network.getStatus();
  }

  doRefresh(event) {
    this.getReceipts();
    event.target.complete();
  }

  async getReceipts() {
    try {
      this.loading = true;
      const res = await this.receiptService.getReceipts();
      this.receipts = res.receipts;
      this.loading = false;
    } catch (e) {
      this.loading = false;
      debug.log('ERROR in getting receipts', e);
    }
  }

  async receiptDetail(receipt) {
    if (!this.status.connected) {
      return;
    }
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/receipts/${receipt.id}`);
  }
}
