import { distinctUntilChanged, take } from 'rxjs/operators';
import { SubscriptionQuery } from '@etop/state/shop/subscription/subscription.query';
import { Component, OnInit } from '@angular/core';
import { CmsService } from 'apps/core/src/services/cms.service';
import { NavController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { SubscriptionService } from '@etop/state/shop/subscription/subscription.service';

@Component({
  selector: 'etop-not-subscription',
  templateUrl: './not-subscription.component.html',
  styleUrls: ['./not-subscription.component.scss']
})
export class NotSubscriptionComponent implements OnInit {
  banner = '';
  subscription$ = this.subscriptionQuery.selectAll();
  trialDate = this.subscriptionQuery.getValue().trialDate;
  subscription;
  loading
  constructor(
    private cms: CmsService,
    private subscriptionQuery: SubscriptionQuery,
    private navCtrl: NavController,
    private auth: AuthenticateStore,
    private util: UtilService,
  ) { }

  async ngOnInit() {
    this.loading = true;
     this.subscription = this.subscription$.pipe(distinctUntilChanged()).subscribe(subscription => {
      if(subscription.length || this.trialDate > 0){
        this.navCtrl.navigateForward(`${this.linkSlug}/orders`, {
          replaceUrl: true,
          animated: false
        }).then();
      }
    }
    );
    this.getSubscriptionBanner();
    this.loading = false;
  }

  get linkSlug() {
    if (this.auth.snapshot.account) {
      this.auth.updatePermissions(
        this.auth.snapshot.account.user_account.permission
      );
    }
    return this.auth.snapshot.account
      ? `/s/${this.auth.snapshot.account.url_slug || this.util.getSlug()}`
      : '';
  }

  async getSubscriptionBanner(){
    await this.cms.initBanners();
    const result = this.cms.getNotSubscriptionApp();
    if(result){
      this.banner = result;
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
