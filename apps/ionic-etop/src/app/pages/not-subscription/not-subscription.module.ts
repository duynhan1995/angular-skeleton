import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EtopPipesModule } from '@etop/shared/pipes/etop-pipes.module';
import { NotSubscriptionComponent } from './not-subscription.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: NotSubscriptionComponent
  }
];

@NgModule({
  declarations: [
    NotSubscriptionComponent
  ],
  exports: [
    NotSubscriptionComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule,
    RouterModule.forChild(routes)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NotSubscriptionModule { }
