import { Component, NgZone, OnInit } from '@angular/core';
import {AuthenticateService, AuthenticateStore, BaseComponent} from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalController, Platform } from '@ionic/angular';
import * as validatecontraints from 'apps/core/src/services/validation-contraints.service';
import { ForgotPasswordComponent } from '../../components/forgot-password/forgot-password.component';
import { ToastService } from '../../../services/toast.service';
import { NetworkStatus, Plugins } from '@capacitor/core';
import { ConfigService } from '@etop/core/services/config.service';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationApi } from '@etop/api';

const { App, Network } = Plugins;


enum View {
  PHONE_SUBMIT = 'phonesubmit',
  PHONE_VERIFY = 'phoneverify',
  REGISTER = 'register',
  LOGIN = 'login'
}


@Component({
  selector: 'etop-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {
  view = View;
  // tslint:disable-next-line: variable-name
  _currentView: View = View.PHONE_SUBMIT;
  phone;

  password;
  session: any;
  ionLoading = false;

  signupData: any = {
    agree_email_info: true,
    agree_tos: false
  };

  serviceTermsLink = '';
  countdown = 60;
  verify_code;
  loadingView = true;

  status: NetworkStatus;

  constructor(
    private util: UtilService,
    private commonUsecase: CommonUsecase,
    private userService: UserService,
    private auth: AuthenticateStore,
    private config: ConfigService,
    private modalCtrl: ModalController,
    private toastService: ToastService,
    private platform: Platform,
    private zone: NgZone,
    private authorizationApi: AuthorizationApi
  ) {
    super();
    this.zone.run(async () => {
      this.status = await this.getStatus();
    });
    Network.addListener('networkStatusChange', status => {
      this.zone.run(async () => {
        this.status = status;
      });
    });
    this._currentView = View.PHONE_SUBMIT;
  }

  async ngOnInit() {}

  async getStatus() {
    return await Network.getStatus();
  }

  async ionViewWillEnter() {
    await this.commonUsecase.redirectIfAuthenticated().then(() => (this.loadingView = false));
    this._currentView = View.PHONE_SUBMIT;
    this.serviceTermsLink = await this.util.getEtopContent(0);
    const invitation_token = this.platform.getQueryParam('invitation_token');
    const invitation_phone = this.platform.getQueryParam('invitation_phone');
    if (invitation_token) {
      const invitation = await this.authorizationApi.getInvitationByToken(
        invitation_token
      );
      this.signupData.email = invitation.email;
    }
    if (invitation_phone) {
      this.phone = invitation_phone;
    }
  }

  currentView(view: View) {
    return this._currentView === view;
  }

  toView(view: View) {
    this._currentView = view;
    if (view === View.PHONE_SUBMIT) {
      this.ionLoading = false;
    }
  }

  async forgotPassword() {
    // return this.toastService.warning('Tính năng đang phát triển!');
    const modal = await this.modalCtrl.create({
      component: ForgotPasswordComponent,
      componentProps: {
        phone: this.phone
      },
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.phone = data.data.phone;
      }
    });
    return await modal.present();
  }

  async checkUserRegistration(phone) {
    try {
      if (!phone) {
        throw new Error('Vui lòng nhập số điện thoại để tiếp tục!');
      }
      const recaptcha_token = await AuthenticateService.getReCaptcha(this.config.get('recaptcha_key'));
      const data = {
        phone,
        recaptcha_token: recaptcha_token
      };
      const check = await this.userService.checkUserRegistration(data);
      if (check.exists) {
        this.password = null;
        this._currentView = View.LOGIN;
        return;
      }
      this.session = await this.userService.initSession();
      this.auth.updateToken(this.session.access_token);
      await this.userService.sendPhoneVerification(phone);
      this._currentView = View.PHONE_VERIFY;
    } catch (e) {
      debug.error('ERROR in Login checkUserRegistration', e);
      throw e;
    }
  }

  async onPhoneVerify() {
    try {
      if (!this.verify_code) {
        return this.toastService.error('Vui lòng nhập mã xác nhận!');
      }
      const res = await this.userService.verifyPhoneUsingToken(this.verify_code);
      if (res.code === 'fail') {
        this.toastService.error(res.msg);
      } else {
        this._currentView = View.REGISTER;
      }
    } catch (e) {
      this.toastService.error(e.message);
    }
  }

  async onPhoneSubmit() {
    this.ionLoading = true;
    try {
      if (!this.phone) {
        this.ionLoading = false;
        return this.toastService.error('Vui lòng nhập số điện thoại!');
      }
      const isTest = this.phone.split(/-[0-9a-zA-Z]+-test$/).length > 1;
      if (isTest || this.validatePhoneNumber(this.phone)) {
        await this.checkUserRegistration(this.phone);

        if (this._currentView === View.PHONE_VERIFY) {
          this.verify_code = '';
          this.countdown = 60;
        }
      }
    } catch (e) {
      debug.error('ERROR in Login onPhoneSubmit', e);
      this.toastService.error(e.message);
    }
    this.ionLoading = false;
  }

  validatePhoneNumber(phone) {
    if (phone && phone.match(/^0[0-9]{9,10}$/)) {
      if (phone.length < 10) {
        this.toastService.error('Vui lòng nhập số điện thoại di động 10 số');
      }
      return true;
    }
    this.toastService.error('Vui lòng nhập số điện thoại hợp lệ');
  }

  async reSendVerifyPhone() {
    await this.userService.sendPhoneVerification(this.phone);
    this.countdown = 60;
  }

  async onLogin() {
    if (!this.password) {
      return this.toastService.error('Vui lòng nhập mật khẩu!');
    }
    await this.commonUsecase.login({
      login: this.phone,
      password: this.password
    });
  }

  async signUp() {
    this.signupData.phone = this.phone;
    const signupData = this.signupData;
    let phone = signupData.phone;
    let email = signupData.email;
    let source;
    if (this.platform.is('ios')) {
      source = localStorage.setItem('REF', 'etop_app_ios');
    } else {
      source = localStorage.setItem('REF', 'etop_app_android');
    }

    try {
      if (!signupData.full_name) {
        throw new Error('Vui lòng nhập Tên đầy đủ.');
      }

      phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      phone = (phone && phone.split('-test')[0]) || '';

      if (!phone || !phone.match(/^[0-9]{9,10}$/)) {
        throw new Error('Số điện thoại không đúng');
      }

      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = email.split('-test')[0];

      if (!validatecontraints.EmailValidates[0].check(email)) {
        throw new Error('Địa chỉ email không hợp lệ.');
      }
      if (signupData.password !== signupData.confirm) {
        throw new Error('Mật khẩu nhập lại không chính xác.');
      }
      await this.commonUsecase.register(signupData, source);
    } catch (e) {
      return this.toastService.error('Đăng ký thất bại! ' + e.message);
    }
  }

  checkPhone() {
    this._currentView = this.view.PHONE_VERIFY;
  }
}
