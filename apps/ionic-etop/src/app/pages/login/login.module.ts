import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LoadingViewModule } from 'apps/ionic-etop/src/app/components/loading-view/loading-view.module';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { CountDownModule } from '../../components/count-down/count-down.module';
import { EtopMaterialModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [LoginComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LoadingViewModule,
    NetworkDisconnectModule,
    CountDownModule,
    EtopMaterialModule
  ],
  providers: [FirebaseAnalytics],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LoginModule {}
