import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CreateShopComponent } from './create-shop.component';
import { IonicModule } from '@ionic/angular';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { EtopAppCommonUsecase } from '../../usecases/etop-app-commom.usecase.service';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';

const routes: Routes = [
  {
    path: '',
    component: CreateShopComponent
  }
];

@NgModule({
  declarations: [CreateShopComponent],
  entryComponents: [],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SelectSuggestModule
    ],
  exports: [CreateShopComponent],
  providers: [FirebaseAnalytics, EtopAppCommonUsecase],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateShopModule {}
