import { Component, OnInit, NgZone } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ToastController, NavController, ActionSheetController } from '@ionic/angular';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import {ExtendedAccount, Account, DEFAULT_RRULE} from 'libs/models/Account';
import { UserService } from 'apps/core/src/services/user.service';
import { ShopInfoModel } from 'apps/ionic-etop/src/models/ShopInfo';
import { Plugins } from '@capacitor/core';
import { TelegramService } from '@etop/features';
import { LoadingService } from '../../../services/loading.service';
import {AccountApi} from '@etop/api';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { GaService } from '../../../services/ga.service';
import {LocationQuery, LocationService} from '@etop/state/location';
import {combineLatest} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {FormBuilder} from '@angular/forms';
const { Device } = Plugins;

@Component({
  selector: 'etop-create-shop',
  templateUrl: './create-shop.component.html',
  styleUrls: ['./create-shop.component.scss']
})
export class CreateShopComponent extends BaseComponent implements OnInit {
  shop = new ShopInfoModel();
  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  provinces = [];
  districts = [];
  wards = [];

  vtiger_obj = {
    user: {},
    shop: {},
    address: {}
  };

  constructor(
    private fb: FormBuilder,
    private util: UtilService,
    private toastController: ToastController,
    private auth: AuthenticateStore,
    private telegramService: TelegramService,
    private userService: UserService,
    private navCtrl: NavController,
    private ga: GaService,
    private loadingService: LoadingService,
    private accountApi: AccountApi,
    private commonUsercase: CommonUsecase,
    private actionSheetController: ActionSheetController,
    private zone: NgZone,
    private locationService: LocationService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  async ngOnInit() {
    const user = this.auth.snapshot.user;
    this.shop.name = user.full_name;
    this.shop.email = user.email;
    this.shop.phone = user.phone;

    await this.loadLocation();
    await this._prepareLocationData();
  }

  async createShop() {
    try {
      await this.loadingService.start('Đang xử lý');
      this.shop.address = this.address;
      this.shop.phone = this.util.cleanNumberString(this.shop.phone);
      if (!this.shop.name) {
        throw new Error(`Vui lòng điền tên cửa hàng`);
      }

      let phone = this.shop.phone;
      phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      phone = (phone && phone.split('-test')[0]) || '';
      if (
        this.shop.website_url &&
        !this.shop.website_url.match(
          /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        )
      ) {
        throw new Error(`Vui lòng điền địa chỉ website hợp lệ`);
      }

      let email = this.shop.email;
      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = (email && email.split('-test')[0]) || '';
      if (
        email &&
        !email.match(
          /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
      ) {
        throw new Error('Vui lòng nhập email hợp lệ!');
      }

      if (!this.shop.address.province_code) {
        throw new Error(`Vui lòng chọn tỉnh thành`);
      }

      if (!this.shop.address.district_code) {
        throw new Error(`Vui lòng chọn quận huyện`);
      }

      if (!this.shop.address.ward_code) {
        throw new Error(`Vui lòng chọn phường xã`);
      }

      if (!this.shop.address.address1) {
        throw new Error(`Vui lòng nhập địa chỉ cửa hàng`);
      }

      const shopData: any = {};

      shopData.name = this.shop.name;
      shopData.phone = this.shop.phone;
      shopData.website_url = this.shop.website_url;
      shopData.email = this.shop.email;
      shopData.address = this.shop.address;
      shopData.image_url = this.shop.image_url;
      shopData.url_slug = this.shop.url_slug;
      shopData.auto_create_ffm = true;
      const shopAccount = await this._createShopAccount(shopData);
      await this.accountApi.updateURLSlug({
        account_id: shopAccount.shop.id,
        url_slug:
          this.util.createHandle(shopAccount.shop.name) +
          '-' +
          shopAccount.shop.code.toLowerCase()
      });
      shopAccount.shop.url_slug =
        this.util.createHandle(shopAccount.shop.name) +
        '-' +
        shopAccount.shop.code.toLowerCase();
      this.vTigerUpdate(this.vtiger_obj);
      this.auth.addAccount(shopAccount);
      this.updateInfo(shopAccount);
      this.auth.updateShop(shopAccount.shop, true);
      await this.userService.checkToken(this.auth.snapshot.token);
      this.loadingService.end();
      this.commonUsercase.updateSessionInfo(true);
      return await this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/dashboard`,
        { animated: false }
      );
    } catch (e) {
      this.loadingService.end();
      this.sendToast(e.message, 'danger');
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Đăng xuất',
          role: 'destructive',
          handler: () => {
            this.auth.clear();
            this.zone.run(async () => {
              await this.navCtrl.navigateForward('/login', {
                animated: false
              });
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  updateInfo(shopAccount) {
    this.auth.updateInfo({
      token: shopAccount.token,
      account: shopAccount.account,
      session: shopAccount.session,
      isAuthenticated: true
    });
  }

  async _createShopAccount(shopData) {
    const survey_info = [
      {
        key: 'orders_per_day',
        question: 'Số lượng đơn hàng mỗi ngày',
        answer: JSON.parse(localStorage.getItem('survey'))
          ? JSON.parse(localStorage.getItem('survey')).orders_per_day_text
          : null
      },
      {
        key: 'business',
        question: 'Ngành hàng kinh doanh',
        answer: JSON.parse(localStorage.getItem('survey'))
          ? JSON.parse(localStorage.getItem('survey')).business_lines
          : null
      }
    ];
    shopData.survey_info = survey_info;
    shopData.money_transaction_rrule = DEFAULT_RRULE;
    const res = await this.userService.registerShop(shopData);
    const shop = new ExtendedAccount(res.shop);
    const accountRes = await this.userService.switchAccount(
      shop.id,
      'ON BOARDING COMPONENT'
    );
    const { access_token, account } = accountRes;
    this.vtiger_obj = {
      user: accountRes.user,
      shop,
      address: {}
    };
    shop.token = access_token;
    const newAccount = new Account(account);
    newAccount.token = access_token;
    newAccount.shop = shop;
    const surveyData = JSON.parse(localStorage.getItem('survey'));
    const shopAddress = this.telegramService.formatAddress(shopData.address);
    const info = await Device.getInfo();
    this.telegramService.newShopMessage(
      accountRes.user,
      shop,
      shopAddress,
      surveyData,
      info
    );
    return newAccount;
  }

  async vTigerUpdate(obj: any) {
    const { user, shop, address } = obj;
    const isTest = user.email.split(/-[0-9a-zA-Z]+-test$/).length > 1;
    const survey = JSON.parse(localStorage.getItem('survey'));

    const body = {
      company: shop.name,
      website: shop.website_url,
      secondaryemail: shop.email,
      mobile: shop.phone,
      lane: `${address.address1} - ${address.ward}`,
      city: address.district,
      state: address.province,
      country: 'Vietnam',
      email: user.email,
      firstname: isTest ? 'TEST' : '',
      lastname: user.full_name,
      phone: user.phone,
      description: survey ? `${survey.orders_per_day_text} \n` : '',
      leadsource: user.source,
      etop_id: user.id,
      assistant_name: address.full_name,
      personal_merchant: survey ? survey.reAnswer : '',
      orders_per_day: survey ? survey.orders_per_day_text : ''
    };

    // this.shopCrm.rawUpdateLeadAndContact(body);
  }

  async sendToast(message, color) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color,
      mode: 'md',
      position: 'top',
      cssClass: 'font-12'
    });
    toast.present();
  }

  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  async loadLocation() {
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  onProvinceSelected() {
    this.address.district_code = '';
    this.address.ward_code = '';
    this.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
  }

  onDistrictSelected() {
    this.address.ward_code = '';
    this.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  onWardSelected() {
    this.address.address1 = '';
  }
}
