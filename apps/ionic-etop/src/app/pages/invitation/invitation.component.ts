import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { AuthorizationApi, UserApi } from '@etop/api';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AppService } from '@etop/web/core/app.service';
import { Invitation } from 'libs/models/Authorization';
import { ToastService } from '../../../services/toast.service';
import { Plugins, AppState } from '@capacitor/core';
import { ModalController, NavController, NavParams, Platform } from '@ionic/angular';
import { VerifyEmailModalComponent } from '../../components/verify-email-modal/verify-email-modal.component';
import { LoadingService } from '../../../services/loading.service';
import { UserService } from 'apps/core/src/services/user.service';

const { App } = Plugins;

@Component({
  selector: 'etop-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {
  token = '';
  invitation = new Invitation({});

  unauthenticated = false;
  not_found = false;
  invalid_email = false;

  loading = true;
  refusing = false;
  accepting = false;

  sendEmailMsg = '';
  sendEmailSuccess: boolean;
  sendingEmail = false;

  constructor(
    private auth: AuthenticateStore,
    private authorizationApi: AuthorizationApi,
    private userApi: UserApi,
    private util: UtilService,
    private router: Router,
    private commonUseCase: CommonUsecase,
    private appService: AppService,
    private ngZone: NgZone,
    private toast: ToastService,
    private modalCtrl: ModalController,
    private loadingService: LoadingService,
    private userService: UserService,
    private navCtrl: NavController,
    private platform: Platform
  ) {}

  get user() {
    return this.auth.snapshot.user;
  }

  get emailVerified() {
    if (!this.user) {
      return false;
    }
    return this.auth.snapshot.user && this.auth.snapshot.user.email_verified_at;
  }

  get actionAvailable() {
    return (
      !this.invitation.declined_at &&
      !this.invitation.accepted_at &&
      !this.not_found
    );
  }

  roleMap(role) {
    return AuthorizationApi.roleMap(role);
  }

  ngOnInit(): void {}

  async ionViewWillEnter() {
    await this.commonUseCase.updateSessionInfo(true);
    await this.checkAndSetupInvitation();
  }
  async checkAndSetupInvitation() {
    this.loading = true;
    this.unauthenticated = false;
    this.invalid_email = false;
    try {
      this.token = this.platform.getQueryParam('t');
      this.invitation = await this.authorizationApi.getInvitationByToken(
        this.token
      );
      const _snapshot = this.auth.snapshot;
      if (!_snapshot.user) {
        this.unauthenticated = true;
      } else if (_snapshot.user.email != this.invitation.email) {
        this.unauthenticated = true;
        this.invalid_email = true;
      }
      this.loading = false;
    } catch (e) {
      this.not_found = e.code == 'not_found';
      this.loading = false;
      debug.error('ERROR in checkAndSetupInvitation', e);
    }
  }

  async checkToken() {
    try {
      let res = await this.userApi.sessionInfo();
      this.auth.updateUser(res.user);
    } catch (e) {
      debug.error('ERROR in checking Token', e);
      if (e.code == 'unauthenticated') {
        this.unauthenticated = true;
      }
    }
  }

  async refuse() {
    this.refusing = true;
    try {
      await this.authorizationApi.rejectInvitation(this.token);
      this.invitation = await this.authorizationApi.getInvitationByToken(
        this.token
      );
      this.ngZone.run(async () => {
        await this.router.navigateByUrl(`/login`);
      });
    } catch (e) {
      this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      debug.error('ERROR in refusing Invitation', e);
    }
    this.refusing = false;
  }

  async accept() {
    this.accepting = true;
    try {
      this.loadingService.start('Đang đồng bộ dữ liệu');
      await this.authorizationApi.acceptInvitation(this.token);
      await this.commonUseCase.updateSessionInfo(true);
      this.auth.selectAccount(this.invitation.shop_id);
      this.loadingService.end();
      this.ngZone.run(async () => {
        await this.router.navigateByUrl(`/s/${this.util.getSlug()}/dashboard`);
      });
    } catch (e) {
      this.loadingService.end();
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in accepting Invitation', e);
    }
    this.accepting = false;
  }

  login() {
    this.auth.clear();
    this.ngZone.run(async () => {
      this.router.navigate(['/login'], {
        queryParams: { invitation_token: this.token }
      });
    });
  }

  async sendVerifyEmail() {
    try {
      this.loadingService.start('Đang gửi mã xác thực');
      await this.userService.sendEmailVerificationUsingOTP(
        this.auth.snapshot.user.email
      );
      this.loadingService.end();
      const modal = await this.modalCtrl.create({
        component: VerifyEmailModalComponent,
        componentProps: {
          email: this.auth.snapshot.user.email
        },
        animated: false
      });
      modal.onDidDismiss().then(async data => {
        if (data) {
          this.commonUseCase.updateSessionInfo(true);
          this.checkToken();
          await this.checkAndSetupInvitation();
        }
      });
      return await modal.present();
    } catch (e) {
      this.loadingService.end();
      debug.log('ERROR in sendVerifyEmail', e.message);
      this.toast.error(e.message);
    }
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }

  dismiss() {
    this.ngZone.run(async () => {
      if (this.unauthenticated) {
        this.navCtrl.navigateForward(`/login`, {
          animated: false
        });
      } else {
        if (this.auth.snapshot.account) {
          this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/dashboard`, {
            animated: false
          });
        } else {
          this.navCtrl.navigateForward(`/survey`, {
            animated: false
          });
        }
      }
    });
  }
}
