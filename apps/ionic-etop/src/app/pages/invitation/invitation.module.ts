import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { InvitationComponent } from './invitation.component';

const routes: Routes = [
  {
    path: '',
    component: InvitationComponent
  }
];

@NgModule({
  declarations: [InvitationComponent],
  imports: [
    CommonModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [InvitationComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvitationModule {}
