import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { GuideComponent } from './guide.component';

const routes: Routes = [
  {
    path: '',
    component: GuideComponent
  }
];

@NgModule({
  declarations: [GuideComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [GuideComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuideModule {}
