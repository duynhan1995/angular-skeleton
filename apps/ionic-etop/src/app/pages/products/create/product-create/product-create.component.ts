import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { CameraResultType, Plugins, PermissionType } from '@capacitor/core';
import { ImageCompress } from '@etop/utils/image-compressor/image-compressor';
import { ImageCompressor } from '@etop/utils/image-compressor/image-compressor.service';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { UtilService } from 'apps/core/src/services/util.service';
import { Product, ShopProduct } from '@etop/models/Product';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';
import { ProductService } from '../../products.service';
import { LoadingService } from 'apps/ionic-etop/src/services/loading.service';
import { ProductStore } from '../../products.store';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { StocktakeApi, InventoryApi } from '@etop/api';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { PermisionService } from 'apps/ionic-etop/src/services/permission.service';
import {ActivatedRoute} from '@angular/router';
import {PosService} from '@etop/state/etop-app/pos';

const { Camera, Permissions } = Plugins;

@Component({
  selector: 'etop-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {
  uploading = false;
  productImg = '';
  productImgEdited = false;

  productForm = this.fb.group({
    name: '',
    code: '',
    retail_price: '',
    cost_price: '',
    description: '',
    total_quantity: '',
    variants: []
  });

  price = false;
  product: Product;
  success = 0;

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private imageCompressor: ImageCompressor,
    private uploader: MobileUploader,
    private toast: ToastService,
    private productService: ProductService,
    private loadingService: LoadingService,
    private util: UtilService,
    private productStore: ProductStore,
    private promiseQueue: PromiseQueueService,
    private stocktakeApi: StocktakeApi,
    private inventoryApi: InventoryApi,
    private barcodeScanner: BarcodeScanner,
    private alertController: AlertController,
    private openNativeSettings: OpenNativeSettings,
    private permisionService: PermisionService,
    private activatedRoute: ActivatedRoute,
    private posService: PosService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      if (queryParams?.barcode) {
        this.productForm.patchValue({
          code: queryParams?.barcode
        });
      }
    });
  }

  back() {
    this.navCtrl.back();
  }

  async ionViewWillEnter() {
    this.product = this.productStore.snapshot.productCreate;
    if (this.product?.variants) {
      this.price = true;
    }
  }

  async scanBarcode() {
    const permission = await this.checkPermissionCamera();
    if (permission === 'denied') {
      this.permisionService.open({
        message: '<strong>eTop POS</strong> cần quyền truy cập Camera để quét mã vạch',
        setting: 'application_details'
      });
    } else {
      this.barcodeScanner
        .scan()
        .then(barcodeData => {
          if (barcodeData.text) {
            this.productForm.patchValue({
              code: barcodeData.text
            });
          }
        })
        .catch(err => {
          debug.error('ERROR in scanning barcode', err);
        });
    }
  }

  async checkPermissionCamera() {
    const permission = await Permissions.query({ name: PermissionType.Camera });
    return permission.state;
  }

  async checkPermissionPhoto() {
    const permission = await Permissions.query({ name: PermissionType.Photos });
    return permission.state;
  }

  priceCompare() {
    const product = this.productStore.snapshot.productCreate;
    return this.util.compareProductPrice(product);
  }
  async takePicture() {
    const permission = await this.checkPermissionCamera();
    const accessPhotos = await this.checkPermissionPhoto();
    if (permission === 'denied') {
      this.permisionService.open({
        message: '<strong>eTop POS</strong> cần quyền truy cập Camera để chụp ảnh làm ảnh sản phẩm',
        setting: 'application_details'
      });
    } else if (accessPhotos === 'denied') {
      this.permisionService.open({
        message: '<strong>eTop POS</strong> cần quyền truy cập Thư viện ảnh làm ảnh sản phẩm',
        setting: 'application_details'
      });
    } else {
      try {
        const image = await Camera.getPhoto({
          quality: 100,
          allowEditing: false,
          resultType: CameraResultType.DataUrl
        });
        const limitSize = 1024 * 1024;
        const decode = await ImageCompress.decodeImage(image.dataUrl);
        let scaleRatio = (limitSize * 100) / (decode.height * decode.width);
        scaleRatio = scaleRatio > 100 ? 100 : scaleRatio;
        const compressedDataUrl = await this.imageCompressor.compressFile(
          image.dataUrl,
          decode.orientation,
          scaleRatio,
          100
        );
        this.uploading = true;
        const result = await this.uploader.uploadBase64Image(
          compressedDataUrl.split(',')[1]
        );
        this.productImg = result.result[0].url;
        if (this.productImg) {
          this.productImgEdited = true;
        }
        this.uploading = false;
      } catch (e) {
        this.uploading = false;
        debug.log('Error in takePicture', e);
      }
    }
  }

  async create() {
    const { name, retail_price, code, description } = this.productForm.value;
    if (!name) {
      return this.toast.error('Chưa nhập tên sản phẩm!');
    }
    if (!retail_price && !this.product?.variants) {
      return this.toast.error('Chưa nhập giá bán sản phẩm!');
    }
    try {
      const body = new Product({
        name,
        code,
        description
      });

      this.loadingService.start('Đang tạo sản phẩm');

      const product = await this.productService.createProduct(body);
      if (this.productImg) {
        await this.updateProductImage({
          id: product.id,
          replace_all: [this.productImg]
        });
      }
      await this.createVariants(product);

      this.loadingService.end();
      this.activatedRoute.queryParams.subscribe(queryParams => {
        if (queryParams?.ref == 'pos') {
          this.posService.recentlyCreateProduct(product.id);
        }
      });

      this.navCtrl.back();
    } catch (e) {
      this.loadingService.end();
      debug.error('ERROR in creating product', e);
      const msg = e.message || 'Tạo sản phẩm thất bại';
      this.toast.error(msg);
    }
  }

  async updateProductImage(data) {
    try {
      await this.productService.updateProductImages(data);
    } catch (e) {
      debug.error('ERROR in updateProductImages', e);
    }
  }

  async createVariants(product: Product | ShopProduct) {
    try {
      const { retail_price } = this.productForm.value;
      const body: any = {
        product_id: product.id,
        name: product.name,
        retail_price,
        list_price: retail_price,
        image_urls: product.image_urls,
        is_available: true,
        code: product.code
      };
      if (this.product && this.product.variants?.length) {
        const promises = this.product.variants.map(v => async () => {
          v.product_id = product.id;
          v.product_name = product.name;
          v.retail_price = Number(v.retail_price);
          v.cost_price = Number(v.cost_price);
          v.list_price = Number(v.retail_price);
          try {
            const res = await this.productService.createVariant(v);
            this.success += 1;
            await this.updateInventoryVariantCostPrice(res.id, v.cost_price);
            await this.createStocktake(res, v.quantity, v.cost_price);
          } catch (e) {
            debug.log('ERROR in Create variant', e);
          }
        });
        await this.promiseQueue.run(promises, 1);
        this.toast.success(
          `Tạo ${this.success}/${this.product.variants.length} mẫu mã thành công.`
        );
      } else {
        // tslint:disable-next-line: no-shadowed-variable
        const product = this.productForm.value;
        const {total_quantity, cost_price} = product
        body.retail_price = this.productForm.controls.retail_price.value;
        const res = await this.productService.createVariant(body);
        await this.updateInventoryVariantCostPrice(res.id, cost_price);
        await this.createStocktake(res, total_quantity, cost_price);
      }
    } catch (e) {
      debug.error('ERROR in creating variants', e);
      // throw e;
    }
  }

  initializationAttribute() {
    this.productStore.setProductCreate(this.productForm.value);
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/products/attribute/create`
    );
  }

  editVariants() {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/products/variants`);
  }

  async updateInventoryVariantCostPrice(variant_id, cost_price) {
    try {
      await this.inventoryApi.updateInventoryVariantCostPrice(
        variant_id,
        cost_price
      );
    } catch (e) {
      debug.log('Tạo giá vồn thất bại', e.message);
    }
  }

  async createStocktake(variant, quantity, cost_price) {
    try {
      const lines = [
        {
          product_id: variant.product_id,
          product_name: variant.product_name,
          variant_name: variant.name,
          variant_id: variant.id,
          old_quantity: 0,
          cost_price,
          new_quantity: Number(quantity),
          code: variant.code,
          image_url: variant.image,
          attributes: variant.attributes
        }
      ];
      const data = {
        total_quantity: Number(quantity),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(data);
      // auto comfirm stocktake
      await this.stocktakeApi.confirmStocktake(stocktake.id, 'confirm');
    } catch (e) {
      debug.log('Cập nhật tồn kho thất bại', e.message);
    }
    return;
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    const _attrs = [];
    attrs.forEach(a => {
      if (a.value) {
        _attrs.push(a.value);
      }
    });
    return _attrs.join(' - ');
  }
}
