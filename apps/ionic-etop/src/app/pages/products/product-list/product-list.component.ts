import { Component, OnInit, NgZone } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { ProductStore } from '../products.store';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { ProductService } from '../products.service';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'etop-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  loading = true;
  products$ = this.productStore.state$.pipe(map(s => s?.products), distinctUntilChanged());

  constructor(
    private navCtrl: NavController,
    private util: UtilService,
    private productStore: ProductStore,
    private productService: ProductService,
    private zone: NgZone,
    private auth: AuthenticateStore,
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.loading = true;
    await this.productService.fetchProducts();
    this.loading = false;
  }

  async doRefresh(event) {
    await this.productService.fetchProducts();
    event.target.complete();
  }

  priceCompare(product) {
    return this.util.compareProductPrice(product);
  }

  async productDetail(product) {
    if (this.isPermissions('shop/product/basic_info:view')) {
      this.productService.fetchProduct(product.id);
      this.zone.run(async () => {
        this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/products/detail/${product.id}`);
      })
    }
  }

  addProduct() {
    this.productStore.setProductCreate(null);
    this.zone.run(async () => {
      this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/products/create`);
    })
  }

  isPermissions(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }
}
