import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FormBuilder, FormArray } from '@angular/forms';
import { ProductStore } from '../../products.store';
import { ProductService } from '../../products.service';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Plugins, PermissionType } from '@capacitor/core';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { PermisionService } from 'apps/ionic-etop/src/services/permission.service';

const { Permissions } = Plugins;

@Component({
  selector: 'etop-variant-create',
  templateUrl: './variant-create.component.html',
  styleUrls: ['./variant-create.component.scss']
})
export class VariantCreateComponent implements OnInit {
  product: any;
  variantForm = this.fb.group({
    code: '',
    retail_price: '',
    attributes: this.fb.array([])
  });

  constructor(
    private navCtrl: NavController,
    private productStore: ProductStore,
    private fb: FormBuilder,
    private productService: ProductService,
    private toast: ToastService,
    private barcodeScanner: BarcodeScanner,
    private permissionService: PermisionService
  ) {}

  ngOnInit(): void {}

  back() {
    this.navCtrl.back();
  }

  get attributes() {
    return this.variantForm.controls['attributes'] as FormArray;
  }

  ionViewWillEnter() {
    this.product = this.productStore.snapshot.activeProduct;

    if (this.product.variants) {
      let attributes =
        (this.product.variants &&
          this.product.variants[0] &&
          this.product.variants[0].attributes) ||
        [];
      attributes.map(a => {
        return {
          name: a.name,
          value: null
        };
      });
      attributes.forEach(attr => {
        this.attributes.push(
          this.fb.group({
            name: attr.name,
            value: ''
          })
        );
      });
    }
  }

  async scanBarcode() {
    const permission = await this.checkPermissionCamera();
    if (permission === 'denied') {
      this.permissionService.open({
        message: '<strong>eTop POS</strong> cần quyền truy cập Camera để quét mã vạch',
        setting: 'application_details'
      })
    } else {
      this.barcodeScanner
        .scan()
        .then(barcodeData => {
          if (barcodeData.text) {
            this.variantForm.patchValue({
              code: barcodeData.text
            });
          }
        })
        .catch(err => {
          debug.error('ERROR in scanning barcode', err);
        });
    }
  }

  async checkPermissionCamera() {
    const permission = await Permissions.query({ name: PermissionType.Camera });
    return permission.state;
  }

  async create() {
    const { code, retail_price, attributes } = this.variantForm.value;
    if (!retail_price) {
      return this.toast.error('Vui lòng nhập giá cho mẫu mã');
    }
    try {
      const data = {
        product_id: this.product.id,
        code,
        retail_price,
        attributes
      };
      await this.productService.createVariant(data);
      await this.productService.fetchProduct(this.product.id, true);
      this.toast.success('Tạo mẫu mã thành công.');
      this.navCtrl.back();
    } catch (e) {
      this.toast.error('Tạo mẫu mã thất bại' + e.message);
      debug.log('ERROR in create variant', e);
    }
  }
}
