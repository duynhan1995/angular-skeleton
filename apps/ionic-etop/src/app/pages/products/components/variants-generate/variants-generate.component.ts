import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormArray } from '@angular/forms';
import { ProductService } from '../../products.service';
import { ProductStore } from '../../products.store';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { PermisionService } from 'apps/ionic-etop/src/services/permission.service';

@Component({
  selector: 'etop-variants-generate',
  templateUrl: './variants-generate.component.html',
  styleUrls: ['./variants-generate.component.scss']
})
export class VariantsGenerateComponent implements OnInit {
  @Input() attributes;
  @Input() product;
  variants = this.fb.array([]);

  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private productService: ProductService,
    private productStore: ProductStore,
    private toast: ToastService,
    private barcodeScanner: BarcodeScanner,
    private permisionService: PermisionService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    let variants = this.productService.getCombineVariantsFromAttrs(
      this.attributes
    );
    variants.forEach(variant => {
      variant.retail_price = this.product?.retail_price || null;
      variant.cost_price = this.product?.cost_price || null;
      variant.quantity = this.product?.total_quantity || null;
      variant.attributes = this.fb.array(
        variant.attributes.map(attr => this.fb.group(attr))
      );
      this.variants.push(this.fb.group(variant));
    });
  }

  scanBarcode(index) {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        if (barcodeData.text) {
          this.variants.at(index).patchValue({
            code: barcodeData.text
          });
        }
      })
      .catch(err => {
        this.permisionService.open({
          message: '<strong>eTop POS</strong> cần quyền truy cập Camera để quét mã vạch',
          setting: 'application_details'
        });
        debug.error('ERROR in scanning barcode', err);
      });
  }

  removeVariant(index) {
    this.variants.removeAt(index);
  }

  back() {
    this.modalCtrl.dismiss({ submit: false });
  }

  submit() {
    this.variants.value.forEach((variant, index) => {
      if (!variant?.retail_price) {
        throw this.toast.error(`Vui lòng nhập giá cho mẫu mã ${index + 1}`);
      }
    });
    this.product.variants = this.variants.value;
    this.productStore.setProductCreate(this.product);
    this.modalCtrl.dismiss({ submit: true });
  }
}
