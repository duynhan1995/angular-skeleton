import { AStore } from 'apps/core/src/interfaces/AStore';
import { Injectable } from '@angular/core';
import { Product } from 'libs/models/Product';

export interface ProductsData {
  products: Product[];
  activeProduct: Product;
  attributes: any;
  variantsCreate: any,
  productCreate: any
}

@Injectable()
export class ProductStore extends AStore<ProductsData> {
  initState: ProductsData = {
    products: null,
    activeProduct: null,
    attributes: null,
    variantsCreate: null,
    productCreate: null
  };
  constructor() {
    super();
  }

  setProducts(products) {
    this.setState({ products });
  }

  setActiveProduct(product) {
    this.setState({ activeProduct: product });
  }

  setAttributes(attributes) {
    this.setState({ attributes });
  }

  setProductCreate(productCreate) {
    this.setState({ productCreate });
  }
}
