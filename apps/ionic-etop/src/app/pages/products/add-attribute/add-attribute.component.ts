import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../../../services/toast.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.scss']
})
export class AddAttributeComponent implements OnInit {
  @Input() attribute;
  values = [];
  newValue;
  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService,
    private util: UtilService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.values = this.attribute.values || [];
  }

  back() {
    this.modalCtrl.dismiss();
  }

  addValue() {
    if (!this.newValue) {
      return this.toast.error('Vui lòng nhập giá trị thuộc tính');
    }
    let _values = [];
    this.values.map(v => {
      _values.push(this.util.createHandle(v));
    });
    if (_values.includes(this.util.createHandle(this.newValue))) {
      return this.toast.error('Giá trị thuộc tính đã tồn tại');
    }
    this.values.push(this.newValue);
    this.newValue = null;
  }

  removeValue(index) {
    this.values.splice(index, 1);
  }

  confirm() {
    this.attribute.values = this.values;
    this.modalCtrl.dismiss(this.attribute);
  }
}
