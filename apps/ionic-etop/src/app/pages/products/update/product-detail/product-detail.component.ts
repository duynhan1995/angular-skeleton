import { Component, OnInit, NgZone } from '@angular/core';
import { ActionSheetController, AlertController, NavController } from '@ionic/angular';
import { Product } from 'libs/models/Product';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ProductService } from '../../products.service';
import { ProductStore } from '../../products.store';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'etop-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  product = new Product({});
  toggle = true;
  togglev = true;
  loading = true;

  constructor(
    private toastr: ToastService,
    private actionSheetController: ActionSheetController,
    private zone: NgZone,
    private alertController: AlertController,
    private navCtrl: NavController,
    private util: UtilService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private productStore: ProductStore,
    private auth: AuthenticateStore,
  ) { }

  ngOnInit() { }

  async ionViewWillEnter() {
    this.loading = true;
    this.product = this.productStore.snapshot.activeProduct;
    if (!this.product) {
      const id = this.activatedRoute.snapshot.params.id;
      this.product = await this.productService.fetchProduct(id, true);
    }
    setTimeout(() => {
      this.loading = false;
    });
  }

  back() {
    this.navCtrl.back();
  }

  priceCompare(product) {
    return this.util.compareProductPrice(product);
  }

  toggleDetail() {
    this.toggle = !this.toggle;
  }

  toggleVariant() {
    this.togglev = !this.togglev;
  }

  async deleteProducts() {
    try {
      await this.productService.deleteProduct([this.product.id]);
      this.toastr.success('Xóa sản phẩm thành công.');
    } catch (e) {
      debug.error('ERROR in deleting products', e);
      this.toastr.error('Xóa sản phẩm không thành công' + e.message);
    }
  }

  async presentActionSheet() {
    let buttons = [];
      if (this.isPermissions('shop/product/basic_info:update') && this.product.variants.length > 0) {
        buttons.push({
          text: 'Chỉnh sửa thông tin',
          role: '',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/edit/${this.product.id}`
              );
            });
          }
        },
        {
          text: 'Chỉnh sửa mẫu mã',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/variant/list/${
                this.product.id
                }`
              );
            });
          }
        })
      }
      if (this.isPermissions('shop/stocktake:update') && this.product.variants.length > 0) {
        buttons.push({
          text: 'Chỉnh sửa tồn kho',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/inventory`
              );
            });
          }
        });
      }

      if (this.isPermissions('shop/product:delete')) {
        buttons.push({
          text: 'Xóa sản phẩm',
          role: 'destructive',
          handler: () => {
            this.requestDelete();
          }
        });
      }

      buttons.push({
        text: 'Đóng',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
      });
    const actionSheet = await this.actionSheetController.create({
      buttons
    });
    await actionSheet.present();
  }

  async requestDelete() {
    const alert = await this.alertController.create({
      header: 'Xóa sản phẩm!',
      message: 'Bạn thực sự muốn xóa sản phẩm này',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => { }
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: () => {
            this.zone.run(async () => {
              await this.deleteProducts();
              this.navCtrl.back();
            });
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs
      .map(a => {
        return a.value;
      })
      .join(' - ');
  }

  isPermissions(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }
}
