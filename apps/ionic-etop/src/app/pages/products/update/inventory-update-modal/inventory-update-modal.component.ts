import { Component, OnInit, Input, Attribute } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Variant } from 'libs/models/Product';
import { UtilService } from 'apps/core/src/services/util.service';
import { StocktakeApi } from '@etop/api';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ProductService } from '../../products.service';

@Component({
  selector: 'etop-inventory-update-modal',
  templateUrl: './inventory-update-modal.component.html',
  styleUrls: ['./inventory-update-modal.component.scss']
})
export class InventoryUpdateModalComponent implements OnInit {
  @Input() variant: Variant = new Variant({});
  quantity = 0;
  actualQuantity = 0;
  lines: Array<Attribute> = [];

  constructor(
    private modalCtrl: ModalController,
    private util: UtilService,
    private stocktakeApi: StocktakeApi,
    private toast: ToastService,
    private productService: ProductService
  ) {}

  ngOnInit(): void {}

  ionViewWillEnter() {
    this.quantity = (this.variant.inventory_variant && this.variant.inventory_variant.quantity) || 0;
    this.actualQuantity = this.quantity;
  }

  get differenceQuantity(): number {
    return this.actualQuantity - this.quantity || 0;
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  back() {
    this.modalCtrl.dismiss();
  }

  async confirm() {
    if (this.actualQuantity === this.quantity) {
      this.toast.success('Cập nhật tồn kho thành công!');
      this.back();
      return;
    }
    if (!this.actualQuantity) {
      this.toast.error('Vui lòng nhập thông tin số lượng thực tế trước khi xác nhận.');
      return;
    }

    try {
      const lines = [
        {
          product_id: this.variant.product && this.variant.product.id,
          product_name: this.variant.product && this.variant.product.name,
          variant_name: this.variant.name,
          variant_id: this.variant.id,
          old_quantity: Number(this.quantity),
          new_quantity: Number(this.actualQuantity),
          code: this.variant.code,
          image_url: this.variant.image,
          attributes: this.variant.attributes
        }
      ];
      const data = {
        total_quantity: Number(this.actualQuantity),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(data);

      // auto comfirm stocktake
      await this.stocktakeApi.confirmStocktake(stocktake.id, 'confirm');

      this.toast.success('Cập nhật tồn kho thành công!');
      await this.productService.fetchProduct(this.variant.product_id, true);
      this.modalCtrl.dismiss(true);
    } catch (e) {
      this.toast.error('Cập nhật tồn kho thất bại.' + e.message);
    }
    return;
  }
}
