import { Component, OnInit } from '@angular/core';
import { Product } from 'libs/models/Product';
import { NavController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { ProductStore } from '../../products.store';
import { InventoryUpdateModalComponent } from '../inventory-update-modal/inventory-update-modal.component';
import { InventoryHistoryModalComponent } from '../inventory-history-modal/inventory-history-modal.component';

@Component({
  selector: 'etop-inventory-edit',
  templateUrl: './inventory-edit.component.html',
  styleUrls: ['./inventory-edit.component.scss']
})
export class InventoryEditComponent implements OnInit {
  product = new Product({});
  constructor(
    private navCtrl: NavController,
    private productStore: ProductStore,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.product = this.productStore.snapshot.activeProduct;
  }

  async back() {
    this.navCtrl.back();
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '-';
    }
    let _attrs = [];
    attrs.forEach(a => {
      if (a.value) {
        _attrs.push(a.value);
      }
    });
    return _attrs.join(' - ');
  }

  async editInventory(variant) {
    const modal = await this.modalCtrl.create({
      component: InventoryUpdateModalComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        variant
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.product = this.productStore.snapshot.activeProduct;
      }
    });
    return await modal.present();
  }

  async historyInventory(variant) {
    const modal = await this.modalCtrl.create({
      component: InventoryHistoryModalComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        variant
      }
    });
    return await modal.present();
  }
}
