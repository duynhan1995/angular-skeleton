import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ProductStore } from '../../products.store';
import { FormBuilder } from '@angular/forms';
import { LoadingService } from 'apps/ionic-faboshop/src/app/services/loading.service';
import { ProductService } from '../../products.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';

@Component({
  selector: 'etop-create-and-update-attribute',
  templateUrl: './create-and-update-attribute.component.html',
  styleUrls: ['./create-and-update-attribute.component.scss']
})
export class CreateAndUpdateAttributeComponent implements OnInit {
  product: any;
  attributes = this.fb.array([]);
  constructor(
    private navCtrl: NavController,
    private alertController: AlertController,
    private productStore: ProductStore,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private productService: ProductService,
    private promiseQueue: PromiseQueueService
  ) {}

  ngOnInit(): void {}

  ionViewWillEnter() {
    this.product = this.productStore.snapshot.activeProduct;

    if (this.product.variants) {
      let attributes =
        (this.product.variants &&
          this.product.variants[0] &&
          this.product.variants[0].attributes) ||
        [];
      attributes.map(a => {
        this.attributes.push(
          this.fb.group({
            oldName: a.name,
            name: a.name,
            values: this.getAttrValues(a.name)
          })
        );
      });
    }
  }

  getAttrValues(name) {
    let values = [];
    this.product.variants.forEach(v => {
      let attribute = v.attributes.find(attr => attr.name == name);
      if (attribute && values.indexOf(attribute.value) < 0) {
        values.push(attribute.value);
      }
    });
    return values.join(', ');
  }

  back() {
    this.navCtrl.back();
  }

  async delAttribute(attribute, index) {
    const alert = await this.alertController.create({
      header: 'Xóa thuộc tính!',
      message: `Bạn thực sự muốn xóa thuộc tính <strong>${attribute.name}</strong>!!!`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            console.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: async () => {
            let variants = this.findVariantAttribute(
              this.product.variants,
              attribute
            );
            const promises = variants.map(v => async () => {
              await this.productService.updateVariant(v);
            });
            await this.promiseQueue.run(promises, 1);
            this.attributes.removeAt(index);
            this.productService.fetchProduct(this.product.id, true);
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  findVariantAttribute(variants, attribute) {
    let _variants: any = [];
    variants.forEach(v => {
      v.attributes.forEach(attr => {
        if (attr.name == attribute.oldName) {
          _variants.push({
            id: v.id,
            attributes: this.mapAttr(v.attributes, attribute.oldName)
          });
        }
      });
    });
    return _variants;
  }

  mapAttr(attributes, name) {
    let _attrs: any = [];
    attributes.forEach(attr => {
      if (attr.name != name) {
        _attrs.push({
          name: attr.name,
          value: attr.value
        });
      }
    });
    return _attrs;
  }

  async updateAttributes() {
    this.loadingService.start('Đang cập nhật thuộc tính');
    let _variants: any = [];
    this.product.variants.forEach(v => {
      v.attributes.map(attr => {
        this.attributes.value.forEach(a => {
          if (a.oldName != a.name && a.oldName == attr.name) {
            attr.name = a.name;
            _variants.push({
              id: v.id,
              attributes: v.attributes
            });
          }
        });
      });
    });
    const promises = _variants.map(v => async () => {
      await this.productService.updateVariant(v);
    });
    await this.promiseQueue.run(promises, 1);
    this.productService.fetchProduct(this.product.id, true);
    this.loadingService.end();
  }
}
