import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonRouterOutlet,
  ModalController,
  NavController,
  PopoverController
} from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'libs/models/Product';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';
import { ProductService } from '../../products.service';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ProductStore } from '../../products.store';
import { AddAttributeNameComponent } from '../../components/add-attribute-name/add-attribute-name.component';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { FilterOperator } from '@etop/models';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { InventoryApi } from '@etop/api';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'etop-variant-list',
  templateUrl: './variant-list.component.html',
  styleUrls: ['./variant-list.component.scss']
})
export class VariantListComponent implements OnInit {
  product = new Product({});
  variants = this.fb.array([]);
  changeData = false;

  constructor(
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private actionSheetController: ActionSheetController,
    private toast: ToastService,
    private fb: FormBuilder,
    private alertController: AlertController,
    private zone: NgZone,
    private util: UtilService,
    private productStore: ProductStore,
    private changeDetector: ChangeDetectorRef,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private promiseQueue: PromiseQueueService,
    private purchaseOrderService: PurchaseOrderService,
    private inventoryApi: InventoryApi,
    public popoverController: PopoverController,
    private barcodeScanner: BarcodeScanner
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.variants = this.fb.array([]);
    this.prepareData();
    // tslint:disable-next-line: no-unused-expression
    customElements.get('popover-example-page') ||
      customElements.define(
        'popover-example-page',
        class ModalContent extends HTMLElement {
          connectedCallback() {
            this.innerHTML = `
              <div class="ion-padding bg-white font-10 ion-text-justify">Mẫu mã này đã có đơn nhập hàng, giá vốn được tính theo bình quân gia quyền giữa các lần nhập hàng</div>
            `;
          }
        }
      );
  }

  async back() {
    this.navCtrl.back();
  }

  async prepareData() {
    const id = this.activatedRoute.snapshot.params.id;
    this.product = this.productStore.snapshot.activeProduct;
    if (this.product?.id != id) {
      this.product = await this.productService.fetchProduct(id, true);
    }
    await this.getVariantsPO();
    const variants: any = this.product.variants;
    this.resetData(variants);
  }

  async getVariantsPO() {
    try {
      const promises = this.product.variants.map(v => async () => {
        try {
          const res = await this.purchaseOrderService.getPurchaseOrders(0, 1, [
            {
              name: 'variant_ids',
              op: FilterOperator.contains,
              value: v.id
            },
            {
              name: 'status',
              op: FilterOperator.eq,
              value: 'P'
            }
          ]);
          v.purchase_order = res.purchase_orders[0];
        } catch (e) {
          debug.error('ERROR in getting Variants PO in promise.all', e);
        }
      });
      await this.promiseQueue.run(promises, 5);
    } catch (e) {
      debug.error('ERROR in getting Variants PO', e);
    }
  }

  scanBarcode(index) {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        if (barcodeData.text) {
          this.variants.at(index).patchValue({
            code: barcodeData.text
          });
        }
      })
      .catch(err => {
        debug.error('ERROR in scanning barcode', err);
      });
  }

  resetData(variants) {
    variants.forEach(variant => {
      const {
        id,
        code,
        retail_price,
        cost_price,
        attributes,
        purchase_order
      } = variant;
      const _variant = {
        id,
        code,
        retail_price,
        cost_price,
        new_cost_price: cost_price,
        attributes,
        purchase_order
      };
      _variant.attributes = this.fb.array(
        variant.attributes.map(attr => this.fb.group(attr))
      );

      this.variants.push(this.fb.group(_variant));
    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Chỉnh sửa thuộc tính',
          handler: () => {
            debug.log('ád', this.variants.value[0].attributes);
            if (
              this.variants.value.length == 1 &&
              this.variants.value[0].attributes?.length == 0
            ) {
              return this.toast.error(
                'Sản phẩm chưa có thuộc tính! Vui lòng thêm thuộc tính trước.'
              );
            }
            this.zone.run(async () => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/attribute`
              );
            });
          }
        },
        {
          text: 'Thêm thuộc tính',
          handler: () => {
            this.zone.run(async () => {
              this.addAttribute();
            });
          }
        },
        {
          text: 'Thêm mẫu mã',
          handler: () => {
            if (
              this.variants.value.length == 1 &&
              this.variants.value[0].attributes?.length == 0
            ) {
              return this.toast.error(
                'Vui lòng thêm thuộc tính trước khi thêm mẫu mã.'
              );
            }
            this.zone.run(async () => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/variant/create`
              );
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async addAttribute() {
    const modal = await this.modalController.create({
      component: AddAttributeNameComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.mapVariants(data.data);
      }
    });
    return await modal.present();
  }

  mapVariants(attribute) {
    this.variants.value.forEach(v => {
      v.attributes.push({
        name: attribute,
        value: ''
      });
    });
    const _variants = this.variants.value;
    this.variants = this.fb.array([]);
    this.resetData(_variants);
  }
  async updateVariant() {
    try {
      const promises = this.variants.value.map(v => async () => {
        v.retail_price = Number(v.retail_price);
        await this.productService.updateVariant(v);
        if (v.cost_price != v.new_cost_price) {
          await this.inventoryApi.updateInventoryVariantCostPrice(
            v.id,
            v.cost_price || 0
          );
        }
      });
      await this.promiseQueue.run(promises, 1);
      this.productService.fetchProduct(this.product.id, true);
      this.toast.success('Cập nhật thành công');
    } catch (e) {
      debug.log('ERROR in updateVariant', e);
      this.toast.error('Cập nhật thất bại' + e.message);
    }
  }

  async delVariant(variant, index) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Xóa mẫu mã',
      message: `Bạn thực sự muốn xóa mẫu mã <strong>${variant.code}</strong>!!!`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            console.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: () => {
            this.removeVariant(variant.id, index);
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  async removeVariant(id, index) {
    try {
      await this.productService.removeVariants([id]);
      this.product = await this.productService.fetchProduct(
        this.product.id,
        true
      );
      // this.prepareData();
      this.variants.removeAt(index);
      this.changeDetector.detectChanges();
      this.toast.success('Đã xóa 1 mẫu mã');
    } catch (e) {
      debug.log('ERROR in remove variant', e);
      this.toast.error(e.message);
    }
  }

  async presentPopover(ev: any) {
    const popover = Object.assign(document.createElement('ion-popover'), {
      component: 'popover-example-page',
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    document.body.appendChild(popover);

    return popover.present();
  }
}
