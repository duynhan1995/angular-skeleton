import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Variant } from 'libs/models/Product';
import { InventoryVoucher } from 'libs/models/Inventory';
import { InventoryService } from 'apps/faboshop/src/services/inventory.service';
import { Filters, FilterOperator } from '@etop/models';

@Component({
  selector: 'etop-inventory-history-modal',
  templateUrl: './inventory-history-modal.component.html',
  styleUrls: ['./inventory-history-modal.component.scss']
})
export class InventoryHistoryModalComponent implements OnInit {
  @Input() variant: Variant = new Variant({});
  inventory_vouchers: Array<InventoryVoucher> = [];
  filters: Filters
  constructor(
    private modalCtrl: ModalController,
    private inventoryService: InventoryService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    debug.log('variant', this.variant);
    this.filters = [
      {
        name: 'variant_ids',
        op: FilterOperator.contains,
        value: this.variant.id
      }
    ];
    this.inventory_vouchers = await this.inventoryService.getInventoryVouchers(0, 1000, this.filters);
  }

  back() {
    this.modalCtrl.dismiss();
  }

  getIndexByVariantId(lines, id) {
    return lines && lines.findIndex(l => l.variant_id == id);
  }
}
