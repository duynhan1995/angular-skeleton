import { Component, OnInit, NgZone } from '@angular/core';
import { ToastController, NavController, ActionSheetController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { GaService } from '../../../services/ga.service';

enum View {
  STEP1 = 'step1',
  STEP2 = 'step2',
}

@Component({
  selector: 'etop-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {
  // tslint:disable-next-line: variable-name
  orders_per_day_mapping = [
    {
      value: 1,
      text: 'Dưới 10 đơn/ngày'
    },
    {
      value: 2,
      text: '10 - 30 đơn/ngày'
    },
    {
      value: 3,
      text: '30 - 50 đơn/ngày'
    },
    {
      value: 4,
      text: '50 - 100 đơn/ngày'
    },
    {
      value: 5,
      text: '100 - 500 đơn/ngày'
    },
    {
      value: 6,
      text: 'Trên 500 đơn/ngày'
    }
  ];

  business = [
    {
      value: 'Thời trang',
      text: 'Thời trang'
    },
    {
      value: 'Giày dép, túi xách',
      text: 'Giày dép, túi xách'
    },
    {
      value: 'Đồng hồ, mắt kính, phụ kiện thời trang',
      text: 'Đồng hồ, mắt kính, phụ kiện thời trang'
    },
    {
      value: 'Mẹ và bé',
      text: 'Mẹ và bé'
    },
    {
      value: 'Hoa, quà tặng',
      text: 'Hoa, quà tặng'
    },
    {
      value: 'Nội thất, gia dụng',
      text: 'Nội thất, gia dụng'
    },
    {
      value: 'Công nghệ, phụ kiện công nghệ',
      text: 'Công nghệ, phụ kiện công nghệ'
    },
    {
      value: 'Mỹ phẩm',
      text: 'Mỹ phẩm'
    },
    {
      value: 'Sách, văn phòng phẩm',
      text: 'Sách, văn phòng phẩm'
    },
    {
      value: 'Nguyên vật liệu - Phụ liệu',
      text: 'Nguyên vật liệu - Phụ liệu'
    },
    {
      value: 'Nông sản, thực phẩm',
      text: 'Nông sản, thực phẩm'
    },
    {
      value: 'Thủ công mỹ nghệ',
      text: 'Thủ công mỹ nghệ'
    },
    {
      value: 'Ô tô, xe máy, linh kiện điện tử',
      text: 'Ô tô, xe máy, linh kiện điện tử'
    },
    {
      value: 'Nhà thuốc',
      text: 'Nhà thuốc'
    },
    {
      value: 'others',
      text: 'Khác'
    }
  ];

  view = View;
  // tslint:disable-next-line: variable-name
  _currentView: View = View.STEP1;

  nextstep = false;

  business_line;

  other_line;

  order_per_line;

  user: any;

  constructor(
    private toastController: ToastController,
    private navCtrl: NavController,
    private auth: AuthenticateStore,
    private ga: GaService,
    private actionSheetController: ActionSheetController,
    private zone: NgZone
  ) {}

  ngOnInit() {
    this.ga.setCurrentScreen('survey');
    this.user = this.auth.snapshot.user;
  }

  currentView(view: View) {
    return this._currentView === view;
  }

  toView(view: View) {
    this._currentView = view;
  }

  radioChecked(value) {
    this.business_line = value;
    if (this.business_line !== 'others') {
      this.other_line = '';
    }
    debug.log('value', value);
  }

  radioCheck(value) {
    this.order_per_line = value;
  }

  nextStep() {
    if (!this.business_line) {
      return this.sendToast('Vui lòng chọn ngành hàng kinh doanh', 'danger');
    } else {
      if (this.business_line === 'others' && !this.other_line) {
        return this.sendToast('Vui lòng nhập ngành hàng kinh doanh', 'danger');
      } else {
        this._currentView = View.STEP2;
      }
    }
  }

  backStep() {
    this.other_line = '';
    this.business_line = '';
    this._currentView = View.STEP1;
  }

  async saveSurvey() {
    if (!this.order_per_line) {
      return this.sendToast('Vui lòng số lượng đơn', 'danger');
    } else {
      let survey = {
        orders_per_day_text: this.orders_per_day_mapping.find(
          e => e.value === this.order_per_line
        ).text,
        orders_per_day: this.order_per_line,
        business_lines:
          this.business_line === 'others' ? this.other_line : this.business_line
      };
      localStorage.setItem('survey', JSON.stringify(survey));
      debug.log('survey', survey);
      return await this.navCtrl.navigateForward('/create-shop', {
        animated: false
      });
    }
  }

  async sendToast(message, color) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color,
      mode: 'md',
      position: 'top',
      cssClass: 'font-12'
    });
    toast.present();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Đăng xuất',
          role: 'destructive',
          handler: () => {
            this.auth.clear();
            this.zone.run(async () => {
              await this.navCtrl.navigateForward('/login', {
                animated: false
              });
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }
}
