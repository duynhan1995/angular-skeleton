import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SurveyComponent } from './survey.component';
import { IonicModule } from '@ionic/angular';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

const routes: Routes = [
  {
    path: '',
    component: SurveyComponent
  }
];

@NgModule({
  declarations: [SurveyComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [SurveyComponent],
  providers: [FirebaseAnalytics],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SurveyModule {}
