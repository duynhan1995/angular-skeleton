import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SupportsComponent } from './supports.component';
import { IonicModule } from '@ionic/angular';

const routes: Routes = [
  {
    path: '',
    component: SupportsComponent
  }
];

@NgModule({
  declarations: [SupportsComponent],
  entryComponents: [],
  imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes)],
  exports: [SupportsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupportsModule {}
