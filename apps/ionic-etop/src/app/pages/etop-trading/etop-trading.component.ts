import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { ETopTradingService } from 'apps/ionic-etop/src/services/etop-trading.service';
import { CheckoutModalComponent } from './components/checkout-modal/checkout-modal.component';
import { OrderSuccessModalComponent } from './components/order-success-modal/order-success-modal.component';

@Component({
  selector: 'etop-etop-trading',
  templateUrl: './etop-trading.component.html',
  styleUrls: ['./etop-trading.component.scss']
})
export class EtopTradingComponent implements OnInit {
  banners: any;
  products: any;
  orProducts: any = [];
  sortList: any = [];
  goodsProduct: any;
  servicesProduct: any;
  phone = "0975066378";
  constructor(
    private navCtrl: NavController,
    private tradingService: ETopTradingService,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
  ) { }

  async ngOnInit() {
    await this.getBanners();
    await this.getProducts()
  }

  back() {
    this.navCtrl.back();
  }

  async getProducts() {
    let filters = [];
    let paging = {
      offset: 0,
      limit: 50
    };
    try {
      // this.layoutService.setLoading(true);
      this.products = await this.tradingService.loadTradingProducts(1,1000);
      this.sortProducts();
      // this.layoutService.setLoading(false);
    } catch (e) {
      // this.layoutService.setLoading(false);
    }
  }

  sortProducts() {
    const desc = this.stripHTML(this.banners[36].description);
    const json_desc = JSON.parse(desc);
    const _products = [...this.products];
    for (const p of json_desc) {
      let _product = _products.find(item => item.product.id == p.product_id);
      if (_product) {
        this.sortList.push(_product);
      }
    }
    if (this.sortList && this.sortList.length > 0) {
      const idx = this.sortList.map(x => x.product.id);
      for (const p of this.products) {
        if (!idx.includes(p.product.id)) {
          this.orProducts.push(p);
        }
      }
      this.products = this.sortList.concat(this.orProducts);
      this.goodsProduct = this.products.filter(
        p =>
          p.product.product_type == "goods" &&
          p.product.variants[0].quantity > 0
      );
    }
  }

  async getBanners() {
    try {
      const res = await fetch("https://cms-setting.etop.vn/api/get-banner").then(r =>
        r.json()
      );
      this.banners = res.data;
      return this.banners;
    } catch (e) {
      debug.log(e);
    }
  }

  stripHTML(html: string) {
    const _tempDIV = document.createElement("div");
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || "";
  }

  async openCheckoutModal(product) {
    const modal = await this.modalCtrl.create({
      component: CheckoutModalComponent,
      componentProps: {
        // phone: this.phone
        product:product
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    modal.onDidDismiss().then(async res => {
      if(res.data) {
        const success_modal = await this.modalCtrl.create({
          component: OrderSuccessModalComponent,
          componentProps: {
            phone: this.phone,
            product:product
          },
          swipeToClose: true,
          presentingElement: this.routerOutlet.nativeEl,
          animated: true,
          showBackdrop: true,
          backdropDismiss: false
        });
        await success_modal.present();
        modal.onDidDismiss().then(async res => {})
      }
    });
    return await modal.present();
  }
}
