import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EtopTradingComponent } from './etop-trading.component';
import { EtopPipesModule } from '@etop/shared/pipes/etop-pipes.module';
import { CheckoutModalComponent } from './components/checkout-modal/checkout-modal.component';
import { OrderSuccessModalComponent } from './components/order-success-modal/order-success-modal.component';


const routes: Routes = [
  {
    path: '',
    component: EtopTradingComponent
  }
];

@NgModule({
  declarations: [
    EtopTradingComponent,
    CheckoutModalComponent,
    OrderSuccessModalComponent
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EtopPipesModule,
    RouterModule.forChild(routes)
  ],
  exports: [EtopTradingComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EtopTradingModule {}
