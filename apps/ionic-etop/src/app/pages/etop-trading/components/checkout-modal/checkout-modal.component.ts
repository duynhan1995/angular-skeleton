import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TelegramService } from '@etop/features';
import {  AuthenticateStore } from '@etop/core';
import { ToastService } from '../../../../../services/toast.service';

@Component({
  selector: 'topship-checkout-modal',
  templateUrl: './checkout-modal.component.html',
  styleUrls: ['./checkout-modal.component.scss']
})
export class CheckoutModalComponent implements OnInit {
  @Input() product;
  loading = false;
  min_quantity=0;
  quantity=0;
  banners: any;
  shop: any;
  phone = "0975066378";
  user: any;
  desc: any;
  isInvalid = false;
  constructor(
    private modalCtrl: ModalController,
    private telegram: TelegramService,
    private auth: AuthenticateStore,
    private toast: ToastService
              ) { }

  back() {
    this.modalCtrl.dismiss(false)
  }

  async ngOnInit() {
    this.shop = this.auth.snapshot.shop;
    this.user = this.auth.snapshot.user;
    await this.getBanners();
    this.phone = this.stripHTML(this.banners[40].description);

    let text = this.product.description.split("&&&&");
    if (text && text.length > 1) {
      text[1].split("###").map(d => {
        if (d.split("##")[0] == "min_quantity") {
          this.min_quantity = Number(d.split("##")[1]);
        }
      });
    }
    this.quantity = this.min_quantity ? this.min_quantity : 1;

  }

  get totalAmount() {
    return this.quantity || this.quantity > 0
      ? this.quantity * this.product.variants[0].retail_price
      : 0;
  }

  validateQuantity() {
    this.isInvalid = false;
  }
  checkout() {
    if (!this.quantity || this.quantity == 0) {
      this.isInvalid = true;
      return this.toast.error("Vui lòng nhập số lượng sản phẩm lớn hơn 0");
    }
    if (this.min_quantity && this.quantity < this.min_quantity) {
      this.isInvalid = true;
      return this.toast.error(
        "Vui lòng đặt hàng với số lượng tối thiểu là " + this.min_quantity
      );
    }
    this.modalCtrl.dismiss(true);
    // NOTE: wth is this?
    this.telegram.recommendTrading(this.shop, this.user, this.product, this.quantity);
  }

  async getBanners() {
    try {
      const res = await fetch("https://cms-setting.etop.vn/api/get-banner").then(r =>
        r.json()
      );
      this.banners = res.data;
      return this.banners;
    } catch (e) {
      debug.log(e);
    }
  }

  stripHTML(html: string) {
    const _tempDIV = document.createElement("div");
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || "";
  }

}
