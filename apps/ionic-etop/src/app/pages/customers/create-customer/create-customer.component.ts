import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'apps/ionic-etop/src/services/customer.service';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { Customer } from 'libs/models/Customer';
import { BaseComponent } from '@etop/core';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';
import {LocationQuery, LocationService} from '@etop/state/location';

@Component({
  selector: 'etop-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent extends BaseComponent implements OnInit {
  customer: Customer = new Customer({});
  address: any = {};
  provinces = [];
  districts = [];
  wards = [];
  genders = [
    {
      title: 'Nam',
      value: 'male'
    },
    {
      title: 'Nữ',
      value: 'female'
    },
    {
      title: 'Khác',
      value: 'other'
    }
  ];
  show = true;

  constructor(
    private navCtrl: NavController,
    private customerService: CustomerService,
    private toastService: ToastService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {
    super();
  }

  async ngOnInit() {
    await this._prepareLocationData();
  }

  dismiss() {
    this.navCtrl.back();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  async createCustomer() {
    try {
      this.customer.birthday = this.customer.birthday &&
        moment(this.customer.birthday).format('YYYY-MM-DD') || '';
      if (!this.customer.full_name) {
        return this.toastService.error('Vui lòng nhập tên!');
      }
      if (!this.customer.phone) {
        return this.toastService.error('Vui lòng nhập số điện thoại!');
      }
      const {
        province_code,
        district_code,
        ward_code,
        address1
      } = this.address;

      if (province_code || district_code || ward_code || address1) {
        if (!province_code) {
          return this.toastService.error('Vui lòng chọn tỉnh thành.');
        }
        if (!district_code) {
          return this.toastService.error('Vui lòng chọn quận huyện.');
        }
        if (!ward_code) {
          return this.toastService.error('Vui lòng chọn phường xã.');
        }
        if (!address1) {
          return this.toastService.error('Vui lòng nhập địa chỉ.');
        }
      }

      const res = await this.customerService.createCustomer(this.customer);

      const customerAddress: any = {
        customer_id: res.id,
        province_code: this.address.province_code,
        district_code: this.address.district_code,
        ward_code: this.address.ward_code,
        address1: this.address.address1,
        email: res.email,
        phone: res.phone,
        full_name: res.full_name
      };
      
      if (province_code && district_code && ward_code && address1) {
        await this.customerService.createCustomerAddress(customerAddress);
      }
      this.toastService.success('Tạo khách hàng mới thành công');
      this.customer = new Customer({});
      this.address = {};
      this.navCtrl.back();
    } catch (e) {
      this.toastService.error(`Tạo khách hàng thất bại!\n${e.message}`);
    }
  }

  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  onProvinceSelected() {
    this.address.district_code = '';
    this.address.ward_code = '';
    this.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(this.address.province_code);
  }

  onDistrictSelected() {
    this.address.ward_code = '';
    this.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(this.address.district_code);
  }

  onWardSelected() {
    this.address.address1 = '';
  }
}
