import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CustomersComponent } from './customers.component';
import { MenuModule } from '../../components/menu/menu.module';
import { TabsModule } from '../../components/tabs/tabs.module';
import { SharedModule } from '../../features/shared/shared.module';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { CreateCustomerAddressComponent } from './create-customer-address/create-customer-address.component';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { AuthenticateModule } from '@etop/core';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerEditFormComponent } from 'apps/ionic-etop/src/app/pages/customers/customer-edit-form/customer-edit-form.component';
import { CustomerService } from '@etop/features';
import { EtopPipesModule } from '@etop/shared';
import { IonSkeletonModule } from '../../components/ion-skeleton/ion-skeleton.module';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: CustomerListComponent },
      { path: 'detail/:id', component: CustomerDetailComponent },
      { path: 'create', component: CreateCustomerComponent },
      { path: 'edit/:id', component: CustomerEditFormComponent }
    ]
  }
];

@NgModule({
  declarations: [
    CustomersComponent,
    CustomerDetailComponent,
    CreateCustomerComponent,
    CreateCustomerAddressComponent,
    CustomerListComponent,
    CustomerEditFormComponent
  ],
  entryComponents: [
    CustomerDetailComponent,
    CreateCustomerAddressComponent,
    CreateCustomerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkStatusModule,
    NetworkDisconnectModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    EtopPipesModule,
    IonSkeletonModule,
    SelectSuggestModule,
    SubscriptionWarningModule
  ],
  providers: [FirebaseAnalytics, CustomerService],
  exports: [CustomersComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomersModule {}
