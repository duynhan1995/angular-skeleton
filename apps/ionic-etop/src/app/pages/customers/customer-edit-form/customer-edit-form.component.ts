import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { ActionSheetController, NavController } from '@ionic/angular';
import { CustomerService } from 'apps/ionic-etop/src/services/customer.service';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import { GaService } from '../../../../services/ga.service';
import { ToastService } from '../../../../services/toast.service';
import { LoadingService } from '../../../../services/loading.service';
import {LocationQuery, LocationService} from '@etop/state/location';
import {ActivatedRoute} from '@angular/router';
import {combineLatest} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'etop-customer-edit',
  templateUrl: './customer-edit-form.component.html',
  styleUrls: ['./customer-edit-form.component.scss']
})
export class CustomerEditFormComponent extends BaseComponent implements OnInit {
  customer = new Customer({});
  customerHasAddress = false;
  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  provinces = [];
  districts = [];
  wards = [];
  locationLoaded = false;
  customerAddresses: Array<CustomerAddress>;
  genders = [
    {
      title: 'Nam',
      value: 'male'
    },
    {
      title: 'Nữ',
      value: 'female'
    },
    {
      title: 'Khác',
      value: 'other'
    }
  ];
  show = true;
  formInitializing = true;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private ga: GaService,
    private auth: AuthenticateStore,
    private toastService: ToastService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private loadingService: LoadingService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.ga.setCurrentScreen('customer_view');
    const { params } = this.activatedRoute.snapshot;
    const customerId = params.id;

    this.customer = await this.customerService.getCustomer(customerId);
    const res = await this.customerService.getCustomerAddresses(
      this.customer.id
    );

    this.customerHasAddress = res.length > 0;
    if ( res?.length ) {
      this.address = res[0]
    }
    this.customerAddresses = res;
    await this.loadLocation();
    await this._prepareLocationData();
  }

  disableView(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }

  async updateCustomer() {
    try {
      if (!this.customer.full_name) {
        return this.toastService.error('Vui lòng nhập tên');
      }
      if (!this.customer.phone) {
        return this.toastService.error('Vui lòng nhập số điện thoại');
      }
      const {
        province_code,
        district_code,
        ward_code,
        address1
      } = this.address;

      if (province_code || district_code || ward_code || address1) {
        if (!province_code) {
          return this.toastService.error('Vui lòng chọn tỉnh thành.');
        }
        if (!district_code) {
          return this.toastService.error('Vui lòng chọn quận huyện.');
        }
        if (!ward_code) {
          return this.toastService.error('Vui lòng chọn phường xã.');
        }
        if (!address1) {
          return this.toastService.error('Vui lòng nhập địa chỉ.');
        }
      }
      
      this.loadingService.start('Đang cập nhật').then();

      const res = await this.customerService.updateCustomer(this.customer);
      const customerAddress: any = {
        province_code: this.address.province_code,
        district_code: this.address.district_code,
        ward_code: this.address.ward_code,
        address1: this.address.address1,
        email: res.email,
        phone: res.phone,
        full_name: res.full_name
      };
      
      if (province_code && district_code && ward_code && address1) {
        if (this.customerHasAddress) {
          const body = {
            ...customerAddress,
            id: this.customerAddresses[0].id
          }
          await this.customerService.updateCustomerAddress(body);
        } else {
          await this.customerService.createCustomerAddress({
            ...customerAddress,
            customer_id: this.customer.id
          });
        }
      }
      this.loadingService.end();
      this.toastService.success('Cập nhật khách hàng thành công').then();
      this.navCtrl.back();
    } catch (e) {
      this.loadingService.end();
      this.toastService.error(`Cập nhật thông tin khách hàng không thành công!\n${e.message}`).then();
    }
  }

  dismiss() {
    this.navCtrl.back();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  isAnonymousCustomer() {
    return this.customer && this.customer.id == '1';
  }

  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  async loadLocation() {
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
    this.locationLoaded = true;
  }

  onProvinceSelected() {
    this.address.district_code = '';
    this.address.ward_code = '';
    this.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
  }

  onDistrictSelected() {
    this.address.ward_code = '';
    this.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  onWardSelected() {
    this.address.address1 = '';
  }
}
