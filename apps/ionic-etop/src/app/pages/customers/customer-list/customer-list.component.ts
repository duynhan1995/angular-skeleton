import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Customer } from '@etop/models/Customer';
import { ModalController, NavController, IonInfiniteScroll } from '@ionic/angular';
import { GaService } from '../../../../services/ga.service';
import { CustomerService } from '@etop/features';
import { Plugins, NetworkStatus } from '@capacitor/core';
import { UtilService } from 'apps/core/src/services/util.service';
import {AuthenticateStore} from "@etop/core";

const { Network } = Plugins;

@Component({
  selector: 'etop-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  customers: Customer[] = [];
  loading = true;
  status: NetworkStatus;
  viewDisconnect = false;
  offset = 0;
  perpage = 20;
  constructor(
    private modalCtrl: ModalController,
    private customerService: CustomerService,
    private ga: GaService,
    private zone: NgZone,
    private navCtrl: NavController,
    private util: UtilService,
    private auth: AuthenticateStore
  ) {
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        this.viewDisconnect = true;
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getCustomers();
        });
      } else {
        this.zone.run(async () => {
          if (!this.customers) {
            this.viewDisconnect = true;
          }
        });
      }
    });
  }

  ngOnInit() {}

  async ionViewWillEnter() {
    this.offset = 0;
    this.ga.setCurrentScreen('customer_list');
    this.zone.run(async() => {
      await this.getCustomers();
    })
  }

  async getStatus() {
    return await Network.getStatus();
  }

  async doRefresh(event) {
    this.offset = 0;
    this.infiniteScroll.disabled = false;
    await this.getCustomers();
    event.target.complete();
  }

  async loadMore(event) {
    this.offset += this.perpage;
    let _customers = await this.customerService.getCustomers(
      this.offset,
      this.perpage
    );
    if (_customers.length == 0) {
      this.infiniteScroll.disabled = true;
    }
    this.customers = this.customers.concat(_customers);
    event.target.complete();
  }

  async getCustomers() {
    this.loading = true;
    try {
      this.customers = await this.customerService.getCustomers(
        this.offset,
        this.perpage
      );
    } catch (e) {
      debug.log('ERROR in getting customers', e);
      this.ga.logEvent('customer_list', { error_msg: e.message || e.msg });
    }
    this.loading = false;
  }

  async customerDetail(customer) {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    await this.navCtrl.navigateForward(`/s/${slug}/customers/detail/${customer.id}`);
  }

  async addCustomer() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    await this.navCtrl.navigateForward(`/s/${slug}/customers/create`);
  }
}
