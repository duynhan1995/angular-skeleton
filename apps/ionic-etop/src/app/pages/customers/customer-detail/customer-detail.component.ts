import { Component, OnInit, Provider } from '@angular/core';
import { Customer } from 'libs/models/Customer';
import { ActionSheetController, ModalController, NavController } from '@ionic/angular';
import { CustomerService } from 'apps/ionic-etop/src/services/customer.service';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {Filters, FilterOperator, PROVIDER_LOGO, FULFILLMENT_STATE} from '@etop/models';
import { ActivatedRoute } from '@angular/router';
import { Order } from '@etop/models';
import { OrderService } from '@etop/features';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent extends BaseComponent implements OnInit {
  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  show = true;
  order_show = true;
  address_display = '';
  customerEdited = false;
  orders: Order[] = [];
  customer: Customer;
  historyLoading = false;
  customerLoading = false;

  constructor(
    private customerService: CustomerService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private orderService: OrderService,
    private auth: AuthenticateStore,
    private toastService: ToastService,
    private actionSheetController: ActionSheetController,
    private util: UtilService,
    ) {
    super();
  }

  async ngOnInit() {
  }

  async ionViewWillEnter() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    await this.prepareData();
    const filters: Filters = [
      {
        name: 'customer.id',
        op: FilterOperator.eq,
        value: id
      }
    ];
    this.historyLoading = true;
    this.orders = await this.orderService.getOrders(0, 100, filters);
    this.orders.map(order => {
      if (order.fulfillments.length > 0) {
        const ffm = order.fulfillments[0];
        ffm.shipping_provider_logo = PROVIDER_LOGO[ffm.carrier];
        ffm.shipping_state_display = FULFILLMENT_STATE[ffm.shipping_state];
      }
    });
    this.historyLoading = false;
  }

  async prepareData() {
    this.customerLoading = true;
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.customer = await this.customerService.getCustomer(id);
    this.address = await this.customerService.getCustomerAddresses(id);
    if (this.address[0]) {
      this.address_display = this.mapAddress(this.address[0]);
    } else {
      this.address_display = '';
    }
    this.customerLoading = false;
  }

  mapAddress(address) {
    if (!address) {
      return '';
    }
    return (
      address?.address1 +
      ', ' +
      address?.ward +
      ', ' +
      address?.district +
      ', ' +
      address?.province
    );
  }

  genderMap(gender) {
    switch (gender) {
      case 'male': {
        return 'Nam';
      }
      case 'female': {
        return 'Nữ';
      }
      case 'other': {
        return 'Khác';
      }
      default: {
        return '-';
      }
    }
  }

  async dismiss() {
    this.navCtrl.back();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  toggleOrder() {
    this.order_show = !this.order_show;
  }

  async customerEdit() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    this.navCtrl.navigateForward(`/s/${slug}/customers/edit/${this.customer.id}`).then();
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  async orderDetail(order) {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    this.navCtrl.navigateForward(`/s/${slug}/orders/${order.id}`).then();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Chỉnh sửa',
          handler: () => {
            this.customerEdit();
          }
        },
        {
          text: 'Xóa khách hàng',
          role: 'destructive',
          handler: () => {
            this.deleteCustomer();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async deleteCustomer() {
    try {
      await this.customerService.deleteCustomer(this.customer.id);
      this.toastService.success('Xóa khách hàng thành công').then();

      const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
      this.navCtrl.navigateBack(`/s/${slug}/customers`).then();
    } catch (e) {
      this.toastService.error(e.message).then();
    }
  }

  addOrder () {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/pos/lines`
    );
  }
}
