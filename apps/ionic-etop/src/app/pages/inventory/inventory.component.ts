import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { InventoryService } from 'apps/shop/src/services/inventory.service';
import { InventoryVoucher } from 'libs/models/Inventory';
import { FilterOperator, Filters } from '@etop/models';
import { InventoryDetailComponent } from './inventory-detail/inventory-detail.component';

@Component({
  selector: 'etop-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  @ViewChild(IonSlides, { static: false }) slider: IonSlides;
  segment = 'all';
  loading = false;

  vouchers: Array<InventoryVoucher> = [];
  vouchersIn: Array<InventoryVoucher> = [];
  vouchersOut: Array<InventoryVoucher> = [];

  constructor(
    private inventoryService: InventoryService,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.segment = 'all';
    await this.getInventoryVouchers();
  }

  async segmentChanged(event) {
    this.segment = event;
  }

  async doRefresh(event) {
    await this.getInventoryVouchers();
    event.target.complete();
  }

  async getInventoryVouchers(page?, perpage?) {
    try {
      this.loading = true;
      this.vouchers = await this.inventoryService.getInventoryVouchers();
      const filtersIn: Filters = [
        {
          name: 'type',
          op: FilterOperator.eq,
          value: 'in'
        }
      ];
      const filtersOut: Filters = [
        {
          name: 'type',
          op: FilterOperator.eq,
          value: 'out'
        }
      ];
      this.vouchersIn = await this.inventoryService.getInventoryVouchers(
        0,
        1000,
        filtersIn
      );
      this.vouchersOut = await this.inventoryService.getInventoryVouchers(
        0,
        1000,
        filtersOut
      );
      this.loading = false;
    } catch (e) {
      this.loading = false;
      debug.error('ERROR in getting list vouchers', e);
    }
  }

  async inventoryDetail(voucher) {
    const modal = await this.modalCtrl.create({
      component: InventoryDetailComponent,
      componentProps: {
        voucher
      },
      animated: false
    });
    return await modal.present();
  }
}
