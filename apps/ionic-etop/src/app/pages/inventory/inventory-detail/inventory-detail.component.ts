import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'etop-inventory-detail',
  templateUrl: './inventory-detail.component.html',
  styleUrls: ['./inventory-detail.component.scss']
})
export class InventoryDetailComponent implements OnInit {
  @Input() voucher;
  toggle = true;
  toggleProduct = true;
  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss();
  }

  toggleDetail() {
    this.toggle = !this.toggle;
  }

  toggleProd() {
    this.toggleProduct = !this.toggleProduct;
  }
}
