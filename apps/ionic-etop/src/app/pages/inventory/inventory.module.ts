import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { InventoryComponent } from './inventory.component';
import { SharedModule } from '../../features/shared/shared.module';
import { InventoryDetailComponent } from './inventory-detail/inventory-detail.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { IonSkeletonModule } from '../../components/ion-skeleton/ion-skeleton.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

const routes: Routes = [
  {
    path: '',
    component: InventoryComponent
  }
];

@NgModule({
  declarations: [InventoryComponent, InventoryDetailComponent],
  entryComponents: [InventoryDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IonSkeletonModule,
    SubscriptionWarningModule
  ],
  exports: [InventoryComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InventoryModule {}
