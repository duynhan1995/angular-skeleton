import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { ShopInfoModel } from 'apps/ionic-etop/src/models/ShopInfo';
import { BaseComponent, AuthenticateStore } from '@etop/core';
import { TelegramService } from '@etop/features';
import { ToastService } from '../../../../services/toast.service';
import { UtilService } from 'apps/core/src/services/util.service';
import {ExtendedAccount, Account, DEFAULT_RRULE} from 'libs/models/Account';
import { LoadingService } from '../../../../services/loading.service';
import { UserService } from 'apps/core/src/services/user.service';
import { Plugins } from '@capacitor/core';
import { GaService } from '../../../../services/ga.service';
import { AccountApi } from '@etop/api';
import {LocationQuery, LocationService} from '@etop/state/location';
import {combineLatest} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {FormBuilder} from '@angular/forms';

const { Device } = Plugins;

@Component({
  selector: 'etop-new-shop',
  templateUrl: './new-shop.component.html',
  styleUrls: ['./new-shop.component.scss']
})
export class NewShopComponent extends BaseComponent implements OnInit {
  business = [
    {
      value: 'Thời trang',
      text: 'Thời trang'
    },
    {
      value: 'Giày dép, túi xách',
      text: 'Giày dép, túi xách'
    },
    {
      value: 'Đồng hồ, mắt kính, phụ kiện thời trang',
      text: 'Đồng hồ, mắt kính, phụ kiện thời trang'
    },
    {
      value: 'Mẹ và bé',
      text: 'Mẹ và bé'
    },
    {
      value: 'Hoa, quà tặng',
      text: 'Hoa, quà tặng'
    },
    {
      value: 'Nội thất, gia dụng',
      text: 'Nội thất, gia dụng'
    },
    {
      value: 'Công nghệ, phụ kiện công nghệ',
      text: 'Công nghệ, phụ kiện công nghệ'
    },
    {
      value: 'Mỹ phẩm',
      text: 'Mỹ phẩm'
    },
    {
      value: 'Sách, văn phòng phẩm',
      text: 'Sách, văn phòng phẩm'
    },
    {
      value: 'Nguyên vật liệu - Phụ liệu',
      text: 'Nguyên vật liệu - Phụ liệu'
    },
    {
      value: 'Nông sản, thực phẩm',
      text: 'Nông sản, thực phẩm'
    },
    {
      value: 'Thủ công mỹ nghệ',
      text: 'Thủ công mỹ nghệ'
    },
    {
      value: 'Ô tô, xe máy, linh kiện điện tử',
      text: 'Ô tô, xe máy, linh kiện điện tử'
    },
    {
      value: 'Nhà thuốc',
      text: 'Nhà thuốc'
    },
    {
      value: 'others',
      text: 'Khác'
    }
  ];

  shop = new ShopInfoModel();

  vtiger_obj = {
    user: {},
    shop: {},
    address: {}
  };

  business_lines;
  other_lines = '';
  source;

  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  provinces = [];
  districts = [];
  wards = [];

  constructor(
    private modalCtrl: ModalController,
    private telegramService: TelegramService,
    private toastService: ToastService,
    private util: UtilService,
    private auth: AuthenticateStore,
    private userService: UserService,
    private loadingService: LoadingService,
    private ga: GaService,
    private zone: NgZone,
    private platform: Platform,
    private accountApi: AccountApi,
    private locationService: LocationService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  async ngOnInit() {

    if (this.platform.is('ios')) {
      this.source = localStorage.setItem('REF', 'etop_app_ios');
    } else {
      this.source = localStorage.setItem('REF', 'etop_app_android');
    }
    await this.loadLocation();
    await this._prepareLocationData();
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  onChangeSurvey() {
    debug.log('business_lines', this.business_lines);
  }

  async createShop() {
    try {
      this.shop.address = this.address;
      if (!this.shop.name) {
        return this.toastService.error(`Vui lòng điền tên cửa hàng`);
      }
      let phone = this.shop.phone;
      phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      phone = (phone && phone.split('-test')[0]) || '';
      // tslint:disable-next-line: max-line-length
      if (
        this.shop.website_url &&
        !this.shop.website_url.match(
          /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        )
      ) {
        return this.toastService.error(`Vui lòng điền địa chỉ website hợp lệ`);
      }
      this.shop.phone = this.util.cleanNumberString(this.shop.phone);
      let email = this.shop.email;
      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = (email && email.split('-test')[0]) || '';
      if (
        email &&
        !email.match(
          // tslint:disable-next-line: max-line-length
          /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
      ) {
        return this.toastService.error('Vui lòng nhập email hợp lệ!');
      }

      if (
        !this.business_lines ||
        (this.other_lines == '' && this.business_lines == 'others')
      ) {
        return this.toastService.error(
          `Ngành hàng kinh doanh không được trống!`
        );
      }

      if (!this.shop.address.address1) {
        return this.toastService.error(`Vui lòng nhập địa chỉ cửa hàng`);
      }

      const shopData: any = {
        name: this.shop.name,
        phone: this.shop.phone,
        website_url: this.shop.website_url,
        email: this.shop.email,
        address: this.shop.address,
        image_url: this.shop.image_url,
        url_slug: this.shop.url_slug,
        auto_create_ffm: true
      };
      await this.loadingService.start('Đang xử lý');
      const shopAccount = await this._createShopAccount(shopData);
      await this.accountApi.updateURLSlug({
        account_id: shopAccount.shop.id,
        url_slug: this.util.createHandle(shopAccount.shop.name) + '-' + shopAccount.shop.code.toLowerCase()
      });
      shopAccount.shop.url_slug = this.util.createHandle(shopAccount.shop.name) + '-' + shopAccount.shop.code.toLowerCase();
      this.auth.addAccount(shopAccount);
      this.loadingService.end();
      this.zone.run(async () => {
        this.modalCtrl.dismiss(shopAccount);
      });
    } catch (e) {
      await this.toastService.error(e.message);
      this.loadingService.end();
    }
  }

  updateInfo(shopAccount) {
    this.auth.updateInfo({
      token: shopAccount.token,
      account: shopAccount.account,
      session: shopAccount.session,
      isAuthenticated: true
    });
  }

  private async _createShopAccount(shopData) {
    const survey_info = [
      {
        key: 'business',
        question: 'Ngành hàng kinh doanh',
        answer:
          this.business_lines == 'others'
            ? this.other_lines
            : this.business_lines
      }
    ];
    shopData.survey_info = survey_info;
    shopData.money_transaction_rrule = DEFAULT_RRULE;
    const res = await this.userService.registerShop(shopData);
    const shop = new ExtendedAccount(res.shop);
    const accountRes = await this.userService.switchAccount(
      shop.id,
      'ON BOARDING COMPONENT'
    );
    const { access_token, account } = accountRes;
    this.vtiger_obj = {
      user: accountRes.user,
      shop,
      address: {}
    };
    this.auth.updatePermissions(account.user_account.permission);
    shop.token = access_token;
    const newAccount = new Account(account);
    newAccount.token = access_token;
    newAccount.shop = shop;
    const shopAddress = this.telegramService.formatAddress(shopData.address);
    const info = await Device.getInfo();
    const survey = {
      business_lines: this.business_lines = survey_info[0].answer
    };
    localStorage.setItem('survey', JSON.stringify(survey));
    this.telegramService.newShopMessage(
      accountRes.user,
      shop,
      shopAddress,
      survey_info,
      info
    );
    this.ga.logEvent('shop_create', { id: res.shop.id, name: res.shop.name });
    return newAccount;
  }

  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  async loadLocation() {
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  onProvinceSelected() {
    this.address.district_code = '';
    this.address.ward_code = '';
    this.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
  }

  onDistrictSelected() {
    this.address.ward_code = '';
    this.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  onWardSelected() {
    this.address.address1 = '';
  }
}
