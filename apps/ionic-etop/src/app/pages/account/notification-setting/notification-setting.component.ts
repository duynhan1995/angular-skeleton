import { Component, OnInit } from '@angular/core';
import { NotifySetting } from 'libs/models/NotifySetting';
import { NotifySettingService } from '@etop/features';
import { Plugins, PermissionType } from '@capacitor/core';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';

const { Permissions } = Plugins;

const topic_etop = [
  {
    topic: 'fulfillment',
    title: 'Đơn hàng',
    description:
      'Nhận thông báo khi cập nhật mới về trạng thái, thông tin đơn hàng'
  },
  {
    topic: 'system',
    title: 'Hệ thống',
    description:
      'Nhận thông báo chung từ Faboshop về chính sách, tính năng, chương trình khuyến mãi, ...'
  }
];

@Component({
  selector: 'ionfabo-notification-setting',
  templateUrl: './notification-setting.component.html',
  styleUrls: ['./notification-setting.component.scss']
})
export class NotificationSettingComponent implements OnInit {
  topics: NotifySetting[];
  permissionNoti;

  constructor(
    private toast: ToastService,
    private notiService: NotifySettingService,
    private openNativeSettings: OpenNativeSettings
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.getSettings();
    this.permissionNoti = await this.getPermisstion();
  }

  async getPermisstion() {
    const permission = await Permissions.query({
      name: PermissionType.Notifications
    });
    return permission.state;
  }

  async goSettingApp() {
    await this.openNativeSettings.open('application_details');
  }

  toggleSetting(topic) {
    if (topic.enable) {
      this.disableTopic(topic);
    } else {
      this.enableTopic(topic);
    }
  }

  mapData(topics: NotifySetting[]) {
    return topics.map(topic => this.mapTopic(topic));
  }

  mapTopic(topic: NotifySetting) {
    const gotTopic = this.getTopic(topic)
    if (!gotTopic) {
      return null
    }
    return {
      ...topic,
      title: gotTopic.title,
      description: gotTopic.description
    };
  }

  getTopic(topic): any {
    return topic_etop.find(t => t.topic == topic.topic) || '';
  }

  async enableTopic(topic) {
    if (!topic) {
      return
    }
    try {
      await this.notiService.enableNotifyTopic(topic.topic);
      this.topics.find(t => t.topic == topic.topic).enable = true;
      this.toast.success(`Bật thông báo ${topic.title} thành công`);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in enableTopic', e);
    }
  }

  async disableTopic(topic) {
    if (!topic) {
      return
    }
    try {
      await this.notiService.disableNotifyTopic(topic.topic);
      this.topics.find(t => t.topic == topic.topic).enable = false;
      this.toast.success(`Tắt thông báo ${topic.title} thành công`);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in disableTopic', e);
    }
  }

  async getSettings() {
    try {
      const _topics = await this.notiService.getNotifySetting();
      this.topics = this.mapData(_topics).filter( (el) => {
        return el != null;
      });
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in getNotifySetting', e);
    }
  }
}
