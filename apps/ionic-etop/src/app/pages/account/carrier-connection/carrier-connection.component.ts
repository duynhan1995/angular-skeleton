import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConnectionAPI } from '@etop/api';
import { ShopConnection } from 'libs/models/Connection';
import { BaseComponent } from '@etop/core';
import { AlertController, PopoverController, NavController, ModalController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { LoadingService } from '../../../../../../ionic-faboshop/src/app/services/loading.service';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { ToastService } from '../../../../../../ionic-faboshop/src/app/services/toast.service';
import { CarrierConnectComponent } from '../../../components/carrier-connect/carrier-connect.component';

enum requestType {
  carrierLogin = 'carrierLogin',
  carrierOTP = 'carrierOTP',
  carrierToken = 'carrierToken'
}
@Component({
  selector: 'shop-carrier-connect',
  templateUrl: './carrier-connection.component.html',
  styleUrls: ['./carrier-connection.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarrierConnectionComponent extends BaseComponent implements OnInit {
  loggedInConnections$ = this.connectionStore.state$.pipe(map(s => s?.loggedInConnections));
  availableConnections$ = this.connectionStore.state$.pipe(map(s => s?.availableConnections));
  empty = {
    title: 'Chưa kết nối nhà vận chuyển',
    subtitle: 'Vui lòng kết nối Nhà vận chuyển để giao hàng',
    buttonClick: this.showCarriersList.bind(this)
  };
  shopConnections: ShopConnection[];

  constructor(
    private modalCtrl: ModalController,
    private connectionStore: ConnectionStore,
    private loading: LoadingService,
    private connectionService: ConnectionService,
    private toast: ToastService,
    private alertController: AlertController,
    private changeDetector: ChangeDetectorRef,
    private navCtrl: NavController
  ) {
    super();
  }

  async ngOnInit() {
    this.connectionService.getConnections();
  }
  isActive(connection) {
    const loggedInConnections = this.connectionStore.snapshot.loggedInConnections;
    return loggedInConnections.map(a=>a.connection_id).includes(connection.id)
  }

  dismiss() {
    this.navCtrl.back();
  }

  async showCarriersList(lastState?: any) {
    const modal = await this.modalCtrl.create({
      component: CarrierConnectComponent,
      componentProps: {
        ...lastState
      },
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    modal.onDidDismiss().then(async data => {
      if (data && data.data) {
        await this.loginConnection(data.data);
      }
    });
    modal.present().then();
  }

  async loginConnection(data: any) {
    try {
      await this.loading.start('Đang xử lý...');
      if (data.signUpInfo) {
        this.registerConnection(data.signUpInfo, data.selectedCarrierConnection);
        // this.showCarriersList(data).then();
      } else {
        if (data.step == requestType.carrierLogin) {
          const loginInfo: ConnectionAPI.LoginShopConnectionRequest = {
            ...data.loginInfo
          };
          await this.connectionService.loginConnection(loginInfo);
        }
        if (data.step == requestType.carrierOTP) {
          const loginInfo: ConnectionAPI.LoginShopConnectionWithOTPRequest = {
            ...data.loginInfo
          };
          await this.connectionService.loginShopConnectionWithOTP(loginInfo);
        }
        if (data.step == requestType.carrierToken) {
          const user_id = data.loginInfo.user_id
          const token = data.loginInfo.token
          const body: ConnectionAPI.LoginShopConnectionByTokenRequest = {
            external_data: { user_id },
            token,
            connection_id: data.loginInfo.connection_id
          };
          await this.connectionService.loginConnectionWithToken(body);
        }
        this.toast.success('Kết nối thành công').then();
      }
    } catch (e) {
      console.error('ERROR in loginConnection', e);
      this.toast.error('Kết nối không thành công').then();
      this.showCarriersList(data).then();
    }
    this.loading.end();
  }

  async deleteShopConnection(connection) {
    const alert = await this.alertController.create({
      header: 'Gỡ kết nối!',
      message: `Bạn có chắc muốn gỡ kết nối với nhà vận chuyển <strong>${connection.name_display}</strong>`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {}
        },
        {
          text: 'Gỡ kết nối',
          cssClass: 'text-danger font-12',
          handler: async () => {
            this.connectionService.deleteShopConnection(connection);
            this.changeDetector.detectChanges();
            await this.connectionService.getConnections();
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  

  async registerConnection (signUpInfo, connection) {
    try {
      const body: ConnectionAPI.RegisterShopConnectionRequest = {
        ...signUpInfo,
        connection_id: connection.id
      };
      await this.connectionService.registerShopConnection(body);
      this.toast.success(`Kết nối với ${connection.name_display} thành công.`);
    } catch(e) {
      debug.error('ERROR in registerShopConnection', e);
      this.toast.error(  e.code ? (e.message || e.msg) : ''
         ||`Kết nối với ${connection.name_display} không thành công.`
      );
    }
  }
}
