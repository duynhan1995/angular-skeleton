import { SubscriptionQuery } from '@etop/state/shop/subscription/subscription.query';
import { Component, OnInit, NgZone, Input } from '@angular/core';
import {resetStores} from "@datorama/akita";
import { ModalController, AlertController, NavController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { NewShopComponent } from '../new-shop/new-shop.component';
import { UserService } from 'apps/core/src/services/user.service';
import { AuthorizationApi } from '@etop/api';
import { Invitation } from 'libs/models/Authorization';
import { ToastService } from '../../../../services/toast.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { EtopAppCommonUsecase } from '../../../usecases/etop-app-commom.usecase.service';
import { FilterOperator } from '@etop/models';

@Component({
  selector: 'etop-list-store',
  templateUrl: './list-store.component.html',
  styleUrls: ['./list-store.component.scss']
})
export class ListStoreComponent implements OnInit {
  @Input() tab;
  accounts: any[] = [];
  currentShop: any = {};
  shopId;
  segment = 'owner';
  listInvite: any;
  userInvitations: Invitation[] = [];
  myStore: any = [];
  loading = true;

  constructor(
    private modalCtrl: ModalController,
    private auth: AuthenticateStore,
    private alertController: AlertController,
    private zone: NgZone,
    private navCtrl: NavController,
    private userService: UserService,
    private authorizationApi: AuthorizationApi,
    private toast: ToastService,
    private util: UtilService,
    private eTopCommon: EtopAppCommonUsecase,
  ) {}

  async ngOnInit() {}

  async ionViewDidEnter() {
    this.loading = true;
    if (this.tab) {
      this.segment = this.tab;
    }
    this.currentShop = this.auth.snapshot.account;
    this.shopId = this.currentShop.id;
    await this.prepareSession();
    await this.getUserInvitations();
  }
  async prepareSession() {
    await this.eTopCommon.updateSessionInfo(true);
    this.accounts = this.auth.snapshot.accounts;
    this.listInvite = this.accounts.filter(
      account => account.permission.roles.indexOf('owner') === -1
    );
    this.myStore = this.accounts.filter(
      account => account.permission.roles.indexOf('owner') != -1
    );
    this.loading = false;
  }
  dismiss() {
    this.navCtrl.back();
  }

  isOwner(account) {
    if (account.permission) {
      return account.permission.roles.indexOf('owner') != -1;
    }
  }

  async getUserInvitations() {
    try {
      this.userInvitations = [];
      const date = new Date();
      const dateString = date.toISOString();
      let userInvitations = await this.authorizationApi.getUserInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
      this.userInvitations = userInvitations.filter(user => user.phone || user.email);
      debug.log('userInvitations', this.userInvitations);
    } catch (e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }

  segmentChanged(event) {
    this.segment = event;
  }

  rolesDisplay(roles: string[]) {
    if (roles && roles.length) {
      return roles.map(r => AuthorizationApi.roleMap(r)).join(', ');
    }
    return 'Chủ shop';
  }
  async switchShop(event, account, index) {
    const alert = await this.alertController.create({
      header: 'Chuyển đổi cửa hàng.',
      message: `Bạn có chắc muốn chuyển sang cửa hàng <strong>"${account.shop.name}"</strong>`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            event.target.checked = false;
          }
        },
        {
          text: 'Truy cập',
          cssClass: 'text-primary font-12',
          handler: async () => {
            this.auth.selectAccount(index);
            this.auth.updatePermissions(account.user_account.permission);
            await this.userService.switchAccount(account.id);
            resetStores();
            this.zone.run(async () => {
              await this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/subscription`,
                {
                  skipLocationChange: true,
                  replaceUrl: true,
                  animated: false
                }
              );
            });
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async newShop() {
    const modal = await this.modalCtrl.create({
      component: NewShopComponent,
      componentProps: {},
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.prepareSession();
        this.segment = 'owner';
      }
    });
    return await modal.present();
  }

  async acceptInvitation(invitation) {
    try {
      await this.authorizationApi.acceptInvitation(invitation.token);
      this.zone.run(async () => {
        this.prepareSession();
        await this.getUserInvitations();
      });
    } catch (e) {
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Accepting Invitation', e);
    }
  }

  async reject(invitation) {
    const alert = await this.alertController.create({
      header: 'Từ chối tham gia quản trị',
      message: `Bạn có chắc từ chối lời mời từ cửa hàng "<strong>${invitation.shop.name}</strong>".`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Từ chối',
          cssClass: 'text-danger',
          handler: () => {
            this.rejectInvitation(invitation);
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  async rejectInvitation(invitation) {
    try {
      await this.authorizationApi.rejectInvitation(invitation.token);
      this.zone.run(async () => {
        await this.getUserInvitations();
      });
    } catch (e) {
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Rejecting Invitation', e);
    }
  }
}
