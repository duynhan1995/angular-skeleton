import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AccountComponent } from './account.component';
import { ListStoreComponent } from './list-store/list-store.component';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { NewShopComponent } from './new-shop/new-shop.component';
import { AuthenticateModule } from '@etop/core';
import { EtopAppCommonUsecase } from '../../usecases/etop-app-commom.usecase.service';
import { EtopMaterialModule, EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { CarrierConnectionComponent } from './carrier-connection/carrier-connection.component';
import { CarrierConnectModule } from '../../components/carrier-connect/carrier-connect.module';
import { AccountMenuComponent } from './account-menu/account-menu.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { TransactionComponent } from './transaction/transaction.component';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { PermisionService } from '../../../services/permission.service';
import { IonSkeletonModule } from '../../components/ion-skeleton/ion-skeleton.module';
import { TransactionListComponent } from './transaction/component/transaction-list/transaction-list.component';
import { TransactionDetailComponent } from './transaction/component/transaction-detail/transaction-detail.component';
import { UpdateBankAccountComponent } from './transaction/component/update-bank-account/update-bank-account.component';
import { RequireStokenComponent } from './transaction/component/update-bank-account/require-stoken/require-stoken.component';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { TransactionDetailShipmentComponent } from './transaction/component/transaction-detail/component/transaction-detail-shipment/transaction-detail-shipment.component';
import { TransactionNotePopupComponent } from './transaction/component/transaction-note/transaction-note.component';
import { VTigerService } from 'apps/core/src/services/vtiger.service';
import { TicketModule } from '../../components/ticket/ticket.module';
import { UpdateTransferScheduleComponent } from './transaction/component/update-transfer-schedule/update-transfer-schedule.component';
import {EmptyModule} from "apps/ionic-etop/src/app/components/empty/empty.module";
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { NotificationSettingComponent } from './notification-setting/notification-setting.component';
import { NotifySettingService } from '@etop/features';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: AccountMenuComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'carrier', component: CarrierConnectionComponent },
      { path: 'list-store', component: ListStoreComponent },
      { path: 'setting', component: SettingsComponent },
      { path: 'address', component: DeliveryAddressComponent },
      { path: 'transaction', component: TransactionComponent },
      { path: 'transaction/detail/:id', component: TransactionDetailComponent },
      { path: 'transaction/list-transactions', component: TransactionListComponent },
      { path: 'transaction/detail/shipment/:id', component: TransactionDetailShipmentComponent },
      // { path: 'notification', component: NotificationSettingComponent },
    ]
  }
];

@NgModule({
  declarations: [
    AccountComponent,
    SettingsComponent,
    ListStoreComponent,
    TransactionComponent,
    CarrierConnectionComponent,
    ProfileComponent,
    NewShopComponent,
    AccountMenuComponent,
    TransactionListComponent,
    TransactionDetailComponent,
    UpdateBankAccountComponent,
    RequireStokenComponent,
    DeliveryAddressComponent,
    TransactionDetailShipmentComponent,
    TransactionNotePopupComponent,
    UpdateTransferScheduleComponent,
    NotificationSettingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    CarrierConnectModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    EtopMaterialModule,
    IonSkeletonModule,
    EtopPipesModule,
    IonInputFormatNumberModule,
    TicketModule,
    ReactiveFormsModule,
    EmptyModule,
    SelectSuggestModule,
    SubscriptionWarningModule
  ],
  providers: [
    MobileUploader,
    EtopAppCommonUsecase,
    OpenNativeSettings,
    PermisionService,
    VTigerService,
    NotifySettingService,
  ],
  exports: [AccountComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccountModule {}
