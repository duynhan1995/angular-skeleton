import { Component, OnInit, NgZone } from '@angular/core';
import {resetStores} from "@datorama/akita";
import { ModalController, NavController, AlertController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { ChangePasswordComponent } from '../../../components/change-password/change-password.component';
import { UserService } from 'apps/core/src/services/user.service';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';

@Component({
  selector: 'etop-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  userRefAff:any;
  constructor(
    private modalCtrl: ModalController,
    private alertController: AlertController,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private userService: UserService,
    private toast : ToastService,
    private zone: NgZone
  ) {}

  ngOnInit() {
    this.user = this.auth.snapshot.user;
  }

  dismiss() {
    this.navCtrl.back();
  }
  async logout() {
    const alert = await this.alertController.create({
      header: 'Đăng xuất!',
      message: 'Bạn thực sự muốn đăng xuất!',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {}
        },
        {
          text: 'Đăng xuất',
          cssClass: 'text-danger font-12',
          handler: () => {
            this.auth.clear();
            resetStores();
            this.zone.run(async () => {
              await this.navCtrl.navigateForward('/login', {
                animated: false
              });
            });
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async changePassword() {
    const modal = await this.modalCtrl.create({
      component: ChangePasswordComponent,
      componentProps: {
        user: this.user
      }
    });
    return await modal.present();
  }

  async updateRefAff() {
    try{
      if(!this.userRefAff){
        return this.toast.error('Vui lòng nhập mã giới thiệu')
      }
      await this.userService.changeRefAff(this.userRefAff)
      this.toast.success('Cập nhật mã giới thiệu thành công')
      this.user.ref_aff = this.userRefAff
    }catch (e) {
      this.toast.error(e.msg || e.message);
    }
  }
}
