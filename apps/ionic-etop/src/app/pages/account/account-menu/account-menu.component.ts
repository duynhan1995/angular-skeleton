import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.scss']
})
export class AccountMenuComponent implements OnInit {
  constructor(
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private util: UtilService
  ) {}

  listItems = [
    {
      icon: 'person-circle-outline',
      title: 'Thông tin tài khoản',
      link: 'profile'
    },
    {
      icon: 'cog-outline',
      title: 'Thiết lập cửa hàng',
      link: 'setting'
    },
    {
      icon: 'list-outline',
      title: 'Danh sách cửa hàng',
      link: 'list-store'
    },
    {
      icon: 'cube-outline',
      title: 'Kết nối nhà vận chuyển',
      link: 'carrier'
    },
    {
      icon: 'location-outline',
      title: 'Địa chỉ lấy hàng',
      link: 'address'
    },
    {
      icon: 'wallet-outline',
      title: 'Đối soát TOPSHIP',
      link: 'transaction'
    },
    // {
    //   icon: 'notifications-outline',
    //   title: 'Thiết lập thông báo',
    //   link: 'notification'
    // },
  ];
  get account() {
    return this.auth.snapshot.account;
  }

  get user() {
    return this.auth.snapshot.user;
  }

  ngOnInit(): void {}

  page(link) {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/account/${link}`);
  }
}
