import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { NavController, ActionSheetController, PopoverController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { Address } from 'libs/models/Address';
import { ToastService } from '../../../../services/toast.service';
import { CreateUpdateAddressPopupComponent } from '../../../components/popovers/create-update-address-popup/create-update-address-popup.component';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShopService } from '@etop/features';
import { AddressService } from "apps/core/src/services/address.service";
import { LoadingService } from "apps/ionic-etop/src/services/loading.service";

@Component({
  selector: 'etop-delivery-address',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss']
})
export class DeliveryAddressComponent implements OnInit {
  popover: HTMLIonPopoverElement;

  empty = {
    title: 'Chưa có địa chỉ lấy hàng',
    subtitle: 'Vui lòng thêm địa chỉ lấy hàng để sử dụng dịch vụ',
    action: 'Tạo địa chỉ',
    buttonClick: this.createAddress.bind(this)
  };

  addresses: Address[] = [];

  constructor(
    private navCtrl: NavController,
    private actionSheetController: ActionSheetController,
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private toast: ToastService,
    private zone: NgZone,
    private util: UtilService,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private changeDetector: ChangeDetectorRef,
    private addressService: AddressService,
    private loading: LoadingService
  ) { }

  isDefaultAddress(address: Address) {
    return this.auth.snapshot.shop.ship_from_address_id == address.id;
  }

  ngOnInit() {
    this.prepareAddress();
  }

  back() {
    this.navCtrl.back();
  }

  prepareAddress() {
    this.addressService.getAddresses().then(res => {
      this.addresses = res.filter(addr => addr.type == 'shipfrom');
    });
  }

  async action(address) {
    const buttons = [
      {
        text: 'Chỉnh sửa',
        role: '',
        handler: () => {
          this.popover = null;
          setTimeout(() => this.editAddress(address));
        }
      }]
    if (this.auth.snapshot.shop.ship_from_address_id != address.id) {
      buttons.push({
        text: 'Xóa địa chỉ',
        role: 'destructive',
        handler: () => setTimeout(() => this.removeAddress(address))
      })
    }
    buttons.push({
      text: 'Đóng',
      role: 'cancel',
      handler: () => {
        debug.log('Cancel clicked');
      }
    })
    const actionSheet = await this.actionSheetController.create({
      header: 'Địa chỉ lấy hàng',
      buttons
    });
    await actionSheet.present();
  }

  async setDefaultAddress(address: Address, type) {
    try {
      await this.shopService.setDefaultAddress(address.id, type);
      await this.auth.setDefaultAddress(address.id, type, this.auth.snapshot.shop.id);

      this.toast.success('Cập nhật địa chỉ mặc định thành công!').then();
    } catch (e) {
      debug.error('ERROR in setDefaultAddress', e);
      this.toast.error(`Cập nhật địa chỉ mặc định thất bại. ${e.code && (e.message || e.msg)}`).then();
    }
  }

  async removeAddress(address: Address) {
    this.loading.start('Đang xử lý...').then();
    try {
      await this.addressService.removeAddress(address.id);
      this.zone.run(() => {
        this.prepareAddress();
      });
      this.toast.success('Xóa địa chỉ thành công!').then();
    } catch (e) {
      this.toast.error(`Xóa địa chỉ thất bại. ${e.code && (e.message || e.msg)}`).then();
    }
    this.loading.end();
  }

  async createOrUpdateAddress(type: 'shipfrom' | 'shipto', address: Address) {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async data => {
      if (data.data?.address) {
        this.zone.run(async () => {
          await this.prepareAddress();
          this.changeDetector.detectChanges();
        });
      }
    });
    await modal.present().then();
  }

  createAddress() {
    this.createOrUpdateAddress('shipfrom', new Address({})).then();
  }

  async editAddress(address) {
    const _address = this.util.deepClone(address);
    await this.createOrUpdateAddress('shipfrom', _address);
  }
}
