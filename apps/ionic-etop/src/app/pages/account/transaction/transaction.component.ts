import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, PopoverController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import {MoneyTransactionShop, Shop, Balance, ExtendedAccount} from '@etop/models';
import { UtilService } from 'apps/core/src/services/util.service';
import { ConnectionService } from '@etop/features';
import { UpdateBankAccountComponent } from './component/update-bank-account/update-bank-account.component';
import { MoneyTransactionsQuery } from '@etop/state/shop/money-transaction/money-transaction.query';
import { MoneyTransactionsService } from '@etop/state/shop/money-transaction/money-transaction.service';
import { TransactionNotePopupComponent } from './component/transaction-note/transaction-note.component';
import { takeUntil } from 'rxjs/operators';
import { ShopService } from '@etop/features';
import { UpdateTransferScheduleComponent } from './component/update-transfer-schedule/update-transfer-schedule.component';

const { App } = Plugins;
const STATUS_WAITING = "Z"
@Component({
  selector: 'etop-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent extends BaseComponent implements OnInit {
  shop: ExtendedAccount;
  offset = 0;
  shopID = '';
  perpage = 20;
  loading = true;
  estimatedTransfered = new MoneyTransactionShop(null);
  segment = 'transaction';
  balance: Balance;
  moneyTransactions$ = this.moneyTransactionsQuery.selectCount();

  constructor(
    private navCtrl: NavController,
    private auth: AuthenticateStore,
    private popoverController: PopoverController,
    private modalCtrl: ModalController,
    private connectionService: ConnectionService,
    private util: UtilService,
    private moneyTransactionsQuery: MoneyTransactionsQuery,
    private moneyTransactionsService: MoneyTransactionsService,
    private shopService: ShopService
  ) { super(); }

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.loading = true;
    this.connectionService.initConnections().then();
    this.auth.state$.pipe(takeUntil(this.destroy$)).
    subscribe((data)=> {
      this.shop = ExtendedAccount.shopMap(data.shop);
    })

    this.offset = 0;
    this.shopID = this.auth.snapshot.shop.id
    await this.getMoneyTransactions()
    await this.getEstimated();
    this.balance = await this.shopService.calcShopBalance('shipping');
    this.loading = false;
  }

  getEstimated() {
    const estimatedTrans = this.moneyTransactionsQuery.getAll({filterBy: entity => entity.status == STATUS_WAITING})
    .sort((a, b) =>
      new Date(b.estimated_transfered_at).getTime() -
      new Date(a.estimated_transfered_at).getTime());
    if (!estimatedTrans || estimatedTrans.length == 0) {
      this.estimatedTransfered = null;
      return;
    }
    const lastEstimatedTime = estimatedTrans[0].estimated_transfered_at;
    let sumLastedAmount = 0;
    estimatedTrans.forEach(trans => {
      if (trans.estimated_transfered_at != lastEstimatedTime) {
        return;
      }
      sumLastedAmount = sumLastedAmount + trans.total_amount;
    });
    this.estimatedTransfered.total_amount = sumLastedAmount;
    this.estimatedTransfered.estimated_transfered_at = lastEstimatedTime;
  }

  async getMoneyTransactions() {
    try {
      await this.moneyTransactionsService.getMoneyTransactions(
        this.shopID,
        this.offset,
        this.perpage
      );
    } catch (e) {
      debug.error('ERROR in list money_transactions', e);
    }
  }

  back() {
    this.navCtrl.back();
  }

  async listMoneyTransactions() {
    await this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/account/transaction/list-transactions`
    );
  }

  async changeBankAccount() {
    const modal = await this.modalCtrl.create({
      component: UpdateBankAccountComponent
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
      }
    });
    return await modal.present();
  }

  async transactionNote() {
    const popover = await await this.popoverController.create({
      component: TransactionNotePopupComponent,
      componentProps: {},
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: true
    });
    await popover.present();
  }

  async segmentChanged(event) {
    this.segment = event;
  }

  async updateMoneyTransactionCalendar() {
    const modal = await this.modalCtrl.create({
      component: UpdateTransferScheduleComponent,
      componentProps: {
        money_transaction_rrule: this.shop.money_transaction_rrule
      }
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
      }
    });
    return await modal.present();
  }
}
