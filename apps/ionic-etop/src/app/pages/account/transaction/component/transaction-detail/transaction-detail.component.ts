import { Component, OnInit } from '@angular/core';
import { FilterOperator, Fulfillment, MoneyTransactionShop } from '@etop/models';
import { NavController } from '@ionic/angular';
import { MoneyTransactionsQuery } from '@etop/state/shop/money-transaction/money-transaction.query';
import { MoneyTransactionsService } from '@etop/state/shop/money-transaction/money-transaction.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss']
})
export class TransactionDetailComponent implements OnInit {
  transaction: MoneyTransactionShop;
  fulfillments: Fulfillment[];
  sum;


  constructor(
    private navCtrl: NavController,
    private util: UtilService,
    private moneyTransactionsService: MoneyTransactionsService,
    private moneyTransactionsQuery: MoneyTransactionsQuery,
  ) {
  }

  ngOnInit(): void {
  }

  async ionViewWillEnter() {
    this.transaction = this.moneyTransactionsQuery.getActive()
    await this.moneyTransactionsService.selectedMoneyTransaction(0, 20, [
      {
        name: 'money_transaction.id',
        op: FilterOperator.eq,
        value: this.transaction?.id
      }
    ])
    this.fulfillments = this.moneyTransactionsQuery.getValue().fulfillments
    this.sum = this.moneyTransactionsQuery.getValue().sum
  }

  back() {
    this.navCtrl.back();
  }

  detailShipment(ffmId) {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/account/transaction/detail/shipment/${ffmId}`
    );
  }
}
