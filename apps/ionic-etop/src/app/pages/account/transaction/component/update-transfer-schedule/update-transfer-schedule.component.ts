import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TelegramService } from '@etop/features';
import { AuthenticateStore } from '@etop/core';
import { ExtendedAccount } from '@etop/models';
import { ShopService } from '@etop/features';
import { LoadingService } from 'apps/ionic-etop/src/services/loading.service';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';

@Component({
  selector: 'etop-update-transfer-schedule',
  templateUrl: './update-transfer-schedule.component.html',
  styleUrls: ['./update-transfer-schedule.component.scss']
})
export class UpdateTransferScheduleComponent implements OnInit {
  @Input() money_transaction_rrule;
  money_transaction_rrule_options = [
    {
      name: 'Hàng ngày (thứ 2-3-4-5-6) (Liên hệ Sales)',
      value: 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR',
      disabled: true
    },
    {
      name: 'Thứ 2-4-6 hàng tuần (Liên hệ Sales)',
      value: 'FREQ=WEEKLY;BYDAY=MO,WE,FR',
      disabled: true
    },
    {
      name: 'Thứ 3-5 hàng tuần (Liên hệ Sales)',
      value: 'FREQ=WEEKLY;BYDAY=TU,TH',
      disabled: true
    },
    {
      name: '1 tuần 1 lần vào thứ 6',
      value: 'FREQ=WEEKLY;BYDAY=FR'
    },
    {
      name: '1 tháng 2 lần vào thứ 6 (tuần thứ 2 và tuần cuối cùng của tháng)',
      value: 'FREQ=MONTHLY;BYDAY=+2FR,-1FR'
    },
    {
      name: '1 tháng 1 lần vào thứ 6 (cuối cùng của tháng)',
      value: 'FREQ=MONTHLY;BYDAY=-1FR'
    },
    {
      name: '1 tháng 1 lần vào ngày cuối cùng của tháng',
      value: 'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1'
    }
  ];
  currentShop: ExtendedAccount;
  customAlertOptions: any = {
    cssClass: 'topship'
  }

  constructor(
    private modalCtrl: ModalController,
    private telegramService: TelegramService,
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private loadingService: LoadingService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.auth.authenticatedData$.subscribe(info => {
      this.currentShop = { ...info.shop } || new ExtendedAccount({});
      this.money_transaction_rrule = this.currentShop.money_transaction_rrule;
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async updateMoneyTransactionCalendar() {
    try {
      if (!this.money_transaction_rrule) {
        throw new Error('Vui lòng chọn lịch chuyển khoản');
      }
      this.loadingService.start('Đang cập nhật')
      let body = {
        money_transaction_rrule: this.money_transaction_rrule
      };
      await this.shopService.updateMoneyTransactionCalendar(body);
      this.currentShop.money_transaction_rrule = this.money_transaction_rrule;
      this.auth.updateShop(this.currentShop);
      this.telegramService.updateMTCalendar(
        this.currentShop,
        this.money_transaction_rrule_options.find(
          o => o.value == this.money_transaction_rrule
        ).name
      );
      this.loadingService.end();
      this.toast.success(
        'Cập nhật lịch chuyển khoản thành công, thay đổi sẽ có tác dụng trong vòng 24h.'
      );
      this.modalCtrl.dismiss();
    } catch (e) {
      this.loadingService.end();
      this.toast.error('Cập nhật lịch chuyển khoản không thành công.');
    }
  }
}
