import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { CmsService } from 'apps/core/src/services/cms.service';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';

@Component({
    selector: 'etop-transaction-note-popup',
    templateUrl: './transaction-note.component.html',
    styleUrls: ['./transaction-note.component.scss']
  })
  export class TransactionNotePopupComponent extends BaseComponent implements OnInit {
  note : any;
    constructor(
      private popoverController: PopoverController,
      private cms: CmsService
    ) {
        super();
    }

    ngOnInit() {
        this.note = this.cms.getTransactionNote();
        this.cms.onBannersLoaded.pipe(takeUntil(this.destroy$))
          .subscribe(_ => {
            this.note = this.cms.getTransactionNote();
          });
    }

    dismiss() {
      this.popoverController.dismiss({closed: true}).then();
    }
  }
