import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ShopService } from '@etop/features';
import { ExtendedAccount } from '@etop/models';
import { UtilService } from 'apps/core/src/services/util.service';
import {
  GoogleAnalyticsService,
} from 'apps/core/src/services/google-analytics.service';
import { map, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { RequireStokenComponent } from './require-stoken/require-stoken.component';
import { FormBuilder } from '@angular/forms';
import { combineLatest } from 'rxjs';
import { BankQuery, BankService } from '@etop/state/bank';
import { LoadingService } from 'apps/ionic-etop/src/services/loading.service';
import { TelegramService } from '@etop/features';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';

@Component({
  selector: 'etop-update-bank-account',
  templateUrl: './update-bank-account.component.html',
  styleUrls: ['./update-bank-account.component.scss']
})
export class UpdateBankAccountComponent extends BaseComponent
  implements OnInit {
  current_shop: ExtendedAccount;

  bankForm = this.fb.group({
    bankCode: null,
    bankProvinceCode: null,
    bankBranchCode: null,
    accountName: null,
    accountNumber: null,
  });

  formInitializing = true;

  banksList$ = this.bankQuery.select('banksList');
  bankProvincesList: any = [];
  bankBranchesList: any = []

  customAlertOptions: any = {
    cssClass: 'topship'
  }

  bankValueMap = bank => (bank && bank.code) || null;
  bankDisplayMap = bank => (bank && bank.name) || null;

  constructor(
    private modalController: ModalController,
    private shopService: ShopService,
    private loadingService: LoadingService,
    private gaService: GoogleAnalyticsService,
    private telegramService: TelegramService,
    private popoverController: PopoverController,
    private bankService: BankService,
    private bankQuery: BankQuery,
    private util: UtilService,
    private auth: AuthenticateStore,
    private toastService: ToastService,
    private fb: FormBuilder,
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  async ngOnInit() { }

  ionViewWillEnter() {
    this.bankForm.controls.bankCode.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(bankCode => {
        if (!this.formInitializing) {
          this.bankForm.patchValue({
            bankProvinceCode: '',
            bankBranchCode: ''
          });
          this.bankProvincesList = this.bankQuery.getValue().bankProvincesList
            .filter(p => p.bank_code == bankCode);
        }
      });

    this.bankForm.controls.bankProvinceCode.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(bankProvinceCode => {
        if (!this.formInitializing) {
          this.bankForm.patchValue({
            bankBranchCode: ''
          });
          this.bankBranchesList = this.bankQuery.getValue()
            .bankBranchesList.filter(b =>
              b.bank_code == this.bankForm.controls.bankCode.value &&
              b.province_code == bankProvinceCode
            );
        }
      });
    this.auth.authenticatedData$.subscribe(info => {
      this.current_shop = { ...info.shop } || new ExtendedAccount({});
      if (this.formInitializing) {
        this.zone.run(async _ => {
          await this.prepareBankForm();
        });
      }
    });
  }

  async prepareBankForm() {
    await this.bankService.initBanks();
    if (!this.current_shop?.bank_account) {
      this.formInitializing = false;
      return;
    }

    this.formInitializing = true;

    const bankCode = this.bankQuery.getBankByName(this.current_shop.bank_account.name)?.code;
    const bankProvinceCode = this.bankQuery.getBankProvinceByName(this.current_shop.bank_account.province)?.code;
    const bankBranchCode = this.bankQuery.getBankBranchByName(this.current_shop.bank_account.branch, bankProvinceCode)?.code;
    this.bankForm.patchValue({
      bankCode,
      bankProvinceCode,
      bankBranchCode,
      accountName: this.current_shop.bank_account.account_name,
      accountNumber: this.current_shop.bank_account.account_number
    });
    this.bankProvincesList = this.bankQuery.getValue().bankProvincesList.filter(p => p.bank_code == bankCode);
    this.bankBranchesList = this.bankQuery.getValue().bankBranchesList.filter(
      b => (b.bank_code == bankCode && b.province_code == bankProvinceCode)
    );
    this.changeDetector.detectChanges();
    this.formInitializing = false;
  }

  formatName() {
    const accountName = this.bankForm.getRawValue().accountName;
    this.bankForm.patchValue(
      {
        accountName: this.util.formatName(accountName)
      },
      { emitEvent: false }
    );
  }

  validateBankInfo() {
    const {
      bankCode,
      bankProvinceCode,
      bankBranchCode,
      accountName,
      accountNumber
    } = this.bankForm.getRawValue();
    if (!bankCode) {
      this.toastService.error('Vui lòng chọn ngân hàng');
      return false;
    }
    if (!bankProvinceCode) {
      this.toastService.error('Vui lòng chọn tỉnh của ngân hàng!');
      return false;
    }
    if (!bankBranchCode) {
      this.toastService.error('Vui lòng chọn chi nhánh ngân hàng!');
      return false;
    }
    if (!accountName) {
      this.toastService.error('Vui lòng nhập tên chủ tài khoản!');
      return false;
    }
    if (!accountNumber) {
      this.toastService.error('Vui lòng nhập số tài khoản!');
      return false;
    } else {
      const _accountNumber = this.util.cleanNumberString(accountNumber);
      if (!_accountNumber.match(/^[0-9]{5,30}$/)) {
        this.toastService.error('Vui lòng nhập số tài khoản hợp lệ!');
        return false;
      }
    }

    return true;
  }

  async updateBankAccount(stoken?) {
    await this.loadingService.start('Đang xử lí');
    try {
      if (!this.validateBankInfo()) {
        return this.loadingService.end();
      }

      const bankData = this.bankForm.getRawValue();

      const bank_account = {
        name: this.bankQuery.getBank(bankData.bankCode)?.name,
        province: this.bankQuery.getBankProvince(bankData.bankProvinceCode)?.name,
        branch: this.bankQuery.getBankBranch(bankData.bankBranchCode)?.name,
        account_name: bankData.accountName,
        account_number: bankData.accountNumber
      };
      const token = stoken || this.auth.snapshot.shop.token;

      const res = await this.shopService.updateBankAccount(bank_account, token);
      const shop = { ...res.shop, bank_account };
      this.auth.updateShop(shop, true);

      this.toastService.success('Cập nhật thành công!').then();
      this.telegramService
        .updateBankAccount(this.current_shop, bank_account)
        .then();

      this.dismiss();
    } catch (e) {
      debug.error('ERROR in Updating Bank Account', e);
      this.loadingService.end();
      if (e && e.meta && e.meta.xcode == 'stoken_required') {
        await this.requireStoken();
      } else {
        await this.toastService.error('Cập nhật không thành công!');
      }
    }
    this.loadingService.end();
  }

  async requireStoken() {
    const popover = await this.popoverController.create({
      component: RequireStokenComponent,
      translucent: true,
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    popover.onDidDismiss().then(async data => {
      if (data && data.data) {
        await this.updateBankAccount(data.data);
      }
    });
    popover.present().then();
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
