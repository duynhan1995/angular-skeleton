import {Component, ChangeDetectorRef, OnInit, NgZone, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { AuthenticateStore } from '@etop/core';
import { GaService } from '../../../services/ga.service';
import { Plugins, NetworkStatus } from '@capacitor/core';
import {DecimalPipe} from '@angular/common';
import {StatisticService} from 'apps/ionic-etop/src/services/statistic.service';

const { Network } = Plugins;

@Component({
  selector: 'page-home',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('chart', { static: false }) chartElement: ElementRef;

  loading = true;
  shop_name = '';
  guideline = false;
  rangeLabelOnInput = '';

  chartData: any;
  selected: { startDate: Moment; endDate: Moment };
  alwaysShowCalendars: boolean;

  bestSeller: any = [];

  status: NetworkStatus;
  viewDisconnect = false;
  dashboardData: any;

  summarizePOSData: any[] = [];
  chartDateData: any = [];
  chartCod: any;
  chartCodData: any = [];
  dateArr = [];
  chartDate: any;
  generalStatistics: any = [];
  generalSummarize: any = [];
  orderToday: any;
  returnOrderToday: any;
  revenueStaff: any = [];
  summarizeRevenue: any = [];
  compareYesterday: any = {
    increase: true,
    value: 0
  };
  compareMonth: any = {
    increase: true,
    value: 0
  };

  constructor(
    private statisticService: StatisticService,
    public sanitizer: DomSanitizer,
    private gaService: GoogleAnalyticsService,
    private auth: AuthenticateStore,
    private cdref: ChangeDetectorRef,
    private ga: GaService,
    private zone: NgZone,
    private numberFormat: DecimalPipe,
    private cdr: ChangeDetectorRef
  ) {
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        this.viewDisconnect = true;
      }
    });
    Network.addListener('networkStatusChange', status => {
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          this.getArrdays();
          await this.loadStat();
          await this.drawChartDate();
        });
      } else {
        this.viewDisconnect = true;
      }
    });
  }

  ngOnInit() {}

  async ionViewWillEnter() {
    this.loading = true;

    this.selected = {
      startDate: moment().startOf('month'),
      endDate: moment()
    };
    this.getArrdays();
    await this.loadStat();
    await this.drawChartDate();

    this.loading = false;
  }

  ngAfterViewInit() {
    this.initChart();
  }

  initChart(yAxisHeight?: number) {
    const ctx = this.chartElement.nativeElement.getContext('2d');
    const config = {
      type: 'bar',
      options: {
        legend: {
          display: false
        },
        title: {
          display: true,
          text: 'Doanh thu bán hàng'
        },
        tooltips: {
          callbacks: {
            label(tooltipItem, _) {
              return (
                tooltipItem.yLabel
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, '.') + 'đ'
              );
            }
          }
        },
        scales: {
          xAxes: [
            {
              display: false
            }
          ],
          yAxes: [
            {
              ticks: {
                callback: (value, _, __) => {
                  return value < 1000000
                    ? value == 0
                      ? 0
                      : this.numberFormat.transform(value / 1000) + 'K'
                    : this.numberFormat.transform(value / 1000000) + ' triệu';
                },
                beginAtZero: true,
                stepSize: (yAxisHeight || 1000000) / 5,
                max: yAxisHeight || 1000000
              }
            }
          ]
        }
      }
    }

    this.chartDate = new Chart(ctx, config);

  }

  async getStatus() {
    return await Network.getStatus();
  }

  async loadStat() {
    try {
      await this.summarizePOS(this.selected.startDate, this.selected.endDate);
    } catch (e) {
      debug.error('DASHBOARD:LOADSTAT', e);
    }
  }

  async summarizePOS(date_from, date_to) {
    try {
      date_to = moment(date_to).add(1, 'days');
      this.summarizePOSData = await this.statisticService.summarizePOS({
        date_from: date_from.format('YYYY-MM-DD'),
        date_to: date_to.format('YYYY-MM-DD')
      });
      const bestSeller = this.summarizePOSData[3];
      this.bestSeller = bestSeller.data.map(p => {
        return {
          image_urls: p[2].image_urls[0],
          name: p[3].label,
          quantity: p[4].value
        };
      });

      this.revenueStaff = this.summarizePOSData[4].data.map(p => {
        return {
          name: p[1].label,
          total_count: p[2].value,
          total_amount: p[3].value
        };
      });

      this.summarizeRevenue = this.summarizePOSData[2].data.map(p => {
        return {
          total_amount: p[0].value
        };
      });
      this.generalStatistics = this.summarizePOSData[0].data;
      this.orderToday = this.generalStatistics[1];
      this.returnOrderToday = this.generalStatistics[0];
      if (this.generalStatistics[2][1].value == 0) {
        if (this.orderToday[1].value > 0) {
          this.compareYesterday.value = 100;
        } else {
          this.compareYesterday.value = 0;
        }
      } else {
        this.compareYesterday.value = Math.round(
          (100 *
            (this.orderToday[1].value - this.generalStatistics[2][1].value)) /
            this.generalStatistics[2][1].value
        );
      }
      if (this.compareYesterday.value >= 0) {
        this.compareYesterday.increase = true;
      } else {
        this.compareYesterday.increase = false;
      }

      if (this.generalStatistics[3][1].value == 0) {
        if (this.orderToday[1].value > 0) {
          this.compareMonth.value = 100;
        } else {
          this.compareMonth.value = 0;
        }
      } else {
        this.compareMonth.value = Math.round(
          (100 *
            (this.orderToday[1].value - this.generalStatistics[3][1].value)) /
            this.generalStatistics[3][1].value
        );
      }
      if (this.compareMonth.value >= 0) {
        this.compareMonth.increase = true;
      } else {
        this.compareMonth.increase = false;
      }

      const generalSummarize = this.summarizePOSData[1].data;
      this.generalSummarize = {
        order: generalSummarize[0],
        return_order: generalSummarize[1]
      };
    } catch (e) {
      debug.error('ERROR in summarize POS', e);
    }
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
    }
    this.getArrdays();
    await this.loadStat();
    await this.drawChartDate();
  }

  randomScalingFactor() {
    return Math.floor(Math.random() * 100);
  }

  getArrdays() {
    this.dateArr = [];
    let start = new Date(Number(this.selected.startDate));
    const end = new Date(Number(this.selected.endDate));
    while (start < end) {
      this.dateArr.push(moment(start).format('DD-MM'));
      const newDate = start.setDate(start.getDate() + 1);
      start = new Date(newDate);
    }
  }

  async drawChartDate() {
    this.chartDateData = [];
    for (let index = 0; index < this.dateArr.length; index++) {
      const value = this.summarizeRevenue[index].total_amount;
      this.chartDateData.push(value);
    }

    const max_value = Math.max(...this.chartDateData);
    let yAxisHeight =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      yAxisHeight = 1000000;
    }

    this.initChart(yAxisHeight);

    this.chartDate.data = {
      labels: this.dateArr,
      datasets: [
        {
          label: 'Doanh thu',
          backgroundColor: '#26B5DA',
          data: this.chartDateData
        }
      ]
    };

    this.chartDate.update();

    this.cdr.detectChanges();

  }
}
