import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import {SharedModule} from '../../features/shared/shared.module';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {IonicModule} from '@ionic/angular';
import {EtopPipesModule} from 'libs/shared/pipes/etop-pipes.module';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';
import {NetworkDisconnectModule} from '../../components/network-disconnect/network-disconnect.module';
import {NetworkStatusModule} from '../../components/network-status/network-status.module';
import {FulfillmentService} from 'apps/ionic-etop/src/services/fulfillment.service';
import {StatisticService} from 'apps/ionic-etop/src/services/statistic.service';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    SharedModule,
    HomeRoutingModule,
    IonicModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    CommonModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    SubscriptionWarningModule
  ],
  providers: [FirebaseAnalytics, StatisticService, FulfillmentService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule {
}
