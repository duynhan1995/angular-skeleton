import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { Order } from 'libs/models/Order';
import { ToastService } from '../../../../services/toast.service';
import { OrderApi } from '@etop/api';
import { GaService } from '../../../../services/ga.service';

@Component({
  selector: 'etop-ecomify-order-detail',
  templateUrl: './ecomify-order-detail.component.html',
  styleUrls: ['./ecomify-order-detail.component.scss']
})
export class EcomifyOrderDetailComponent implements OnInit {

  @Input() order = new Order({});
  showList = false;
  paymentDisplay = false;

  constructor(
    private modalCtrl: ModalController,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private toast: ToastService,
    private orderApi: OrderApi,
    private ga: GaService
  ) { }

  ngOnInit() {
    debug.log('order', this.order);
    this.ga.setCurrentScreen('order_view');
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  toggleList() {
    this.showList = !this.showList;
  }

  togglePayment() {
    this.paymentDisplay = !this.paymentDisplay;
  }

  get isPaymentDone() {
    // TODO: temporarily doing this!!! wait for VALID order.payment_status
    return this.order.received_amount >= this.order.total_amount;
  }

  statusMap(status) {
    const statusMap = {
      Z: 'Nháp',
      S: 'Đã xác nhận',
      P: 'Đã xác nhận',
      N: 'Huỷ',
      NS: 'Trả hàng'
    };
    return statusMap[status] || 'Không xác định';
  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Đơn hàng',
      buttons: [
        {
          text: 'Hủy',
          role: 'destructive',
          handler: () => {
            if (this.order.confirm_status == 'N') {
              return this.toast.warning('Đơn hàng đã ở trạng thái hủy. Vui lòng kiểm tra lại.');
            }
            this.ressonCancelSheet();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async ressonCancelSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Vui lòng chọn lý do hủy',
      buttons: [
        {
          text: 'Sai thông tin',
          handler: () => {
            this.cancelOrder('Sai thông tin');
          }
        },
        {
          text: 'Khách yêu cầu hủy',
          handler: () => {
            this.cancelOrder('Khách yêu cầu hủy');
          }
        },
        {
          text: 'Tư vấn sai sản phẩm',
          handler: () => {
            this.cancelOrder('Tư vấn sai sản phẩm');
          }
        },
        {
          text: 'Hết hàng',
          handler: () => {
            this.cancelOrder('Hết hàng');
          }
        },
        {
          text: 'Lý do khác',
          handler: () => {
            this.otherReason();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async otherReason() {
    const alert = await this.alertController.create({
      message: 'Nhập lý do bạn muốn hủy đơn.',
      inputs: [
        {
          name: 'other_reason',
          type: 'text',
          placeholder: 'Lý do hủy đơn'
        }
      ],
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => { }
        },
        {
          text: 'Xác nhận',
          cssClass: 'text-primary font-12',
          handler: async alertData => {
            this.cancelOrder(alertData.other_reason);
          }
        }
      ]
    });

    await alert.present();
  }

  async cancelOrder(reason) {
    try {
      const res = await this.orderApi.cancelOrder(this.order.id, reason, 'confirm');
      if (
        res.fulfillment_errors &&
        res.fulfillment_errors[0] &&
        res.fulfillment_errors[0].msg
      ) {
        throw new Error(res.fulfillment_errors[0].msg);
      }
      this.toast.success('Hủy đơn hàng thành công!');
      this.modalCtrl.dismiss(this.order);
      this.ga.logEvent('order_cancel', { id: res.id, code: res.code });
    } catch (e) {
      debug.log('Error when cancel order', e);
      this.ga.logEvent('order_cancel', { error_msg: e.message });
    }
  }

}
