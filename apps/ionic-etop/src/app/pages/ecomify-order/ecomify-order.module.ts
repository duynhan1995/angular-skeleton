import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsModule } from '../../components/tabs/tabs.module';
import { MenuModule } from '../../components/menu/menu.module';
import { SharedModule } from '../../features/shared/shared.module';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { EcomifyOrderComponent } from './ecomify-order.component';
import { EcomifyOrderDetailComponent } from './ecomify-order-detail/ecomify-order-detail.component';
import { EtopPipesModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: EcomifyOrderComponent
  }
];

@NgModule({
  declarations: [EcomifyOrderComponent, EcomifyOrderDetailComponent],
  entryComponents: [],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkStatusModule,
    AuthenticateModule,
    RouterModule.forChild(routes)
  ],
  providers: [FirebaseAnalytics],
  exports: [EcomifyOrderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule {}
