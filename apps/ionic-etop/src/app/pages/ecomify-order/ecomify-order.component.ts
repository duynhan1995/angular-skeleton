import { Component, OnInit, NgZone } from '@angular/core';
import { OrderService } from 'apps/shop/src/services/order.service';
import { ModalController, NavController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { GaService } from '../../../services/ga.service';
import { Plugins, NetworkStatus } from '@capacitor/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { EcomifyOrderDetailComponent } from './ecomify-order-detail/ecomify-order-detail.component';
import { FilterOperator } from '@etop/models';

const { Network } = Plugins;

@Component({
  selector: 'etop-ecomify-order',
  templateUrl: './ecomify-order.component.html',
  styleUrls: ['./ecomify-order.component.scss']
})
export class EcomifyOrderComponent implements OnInit {

  orders: any = [];
  loadding = true;
  status: NetworkStatus;
  viewDisconnect = false;
  networkStatus = true;
  searchbar = '';
  items: any = [];

  constructor(
    private orderService: OrderService,
    private modalCtrl: ModalController,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private ga: GaService,
    private zone: NgZone,
    private util: UtilService
  ) {
    this.viewDisconnect = false;
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        if (this.orders.length == 0) {
          this.viewDisconnect = true;
        }
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getOrders();
        });
      } else {
        this.zone.run(async () => {
          if (!this.orders) {
            this.viewDisconnect = true;
          }
        });
      }
    });
  }

  ngOnInit() { }

  async getStatus() {
    return await Network.getStatus();
  }

  async ionViewWillEnter() {
    debug.log('ionViewWillEnter');
    this.ga.setCurrentScreen('order_list');
    await this.getOrders();
    this.items = [...this.orders];
  }

  getItems(ev: any) {
    const val = ev.target.value;
    this.searchbar = val;
    const items = this.items;
    if (val && val.trim() !== '') {
      this.orders = items.filter(item => {
        return (
          this.util
            .removeDiacritic(item.customer.full_name)
            .toLowerCase()
            .indexOf(this.util.removeDiacritic(val).toLowerCase()) > -1 ||
          item.code.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.customer.phone.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
      debug.log('orders', this.orders);
    } else {
      this.orders = this.items;
    }
  }

  async doRefresh(event) {
    this.searchbar = '';
    await this.getOrders();
    event.target.complete();
  }
  async getOrders() {
    try {
      const orders = await this.orderService.getOrders(0, 1000, [{
        "name": "source",
        "value": "ecomify",
        "op": FilterOperator.eq
      }]);
      if (orders) {
        this.orders = orders;
      }
      this.loadding = false;
    } catch (e) {
      debug.log('ERROR in getting orders', e.message);
      this.ga.logEvent('order_list', { error_msg: e.message });
    }
  }
  async orderDetail(order) {
    if (
      !this.auth.snapshot.permission.permissions.includes('shop/order:view')
    ) {
      return;
    }
    if (!this.status.connected) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: EcomifyOrderDetailComponent,
      componentProps: {
        order
      },
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data) {
        this.getOrders();
      }
      this.searchbar = '';
    });
    return await modal.present();
  }

  statusMap(status) {
    const statusMap = {
      Z: 'Nháp',
      S: 'Đã xác nhận',
      P: 'Đã xác nhận',
      N: 'Huỷ',
      NS: 'Trả hàng'
    };
    return statusMap[status] || 'Không xác định';
  }

  createOrder() {
    this.navCtrl.navigateForward(
      `/s/${this.auth.snapshot.account.url_slug}/pos`,
      { animated: false }
    );
  }
}
