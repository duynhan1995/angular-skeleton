import {Component, OnInit, Provider, ChangeDetectorRef} from '@angular/core';
import {ActionSheetController, AlertController, ModalController, NavController} from '@ionic/angular';
import {OrderService} from "apps/ionic-etop/src/services/order.service";
import {ToastService} from '../../../../services/toast.service';
import {OrderApi} from '@etop/api';
import {ActivatedRoute} from '@angular/router';
import {UtilService} from 'apps/core/src/services/util.service';
import {AuthenticateStore} from '@etop/core';
import {Fulfillment, Order, ORDER_STATUS, Ticket} from '@etop/models';
import {CmsService} from 'apps/core/src/services/cms.service';
import {TicketDetailComponent} from '../../../components/ticket/component/ticket-detail/ticket-detail.component';
import moment from 'moment';
import {TicketModalComponent} from '../../../components/ticket-modal/ticket-modal.component';
import {ShopCrmService} from 'apps/shop/src/services/shop-crm.service';
import {FulfillmentService} from 'apps/ionic-etop/src/services/fulfillment.service';
import {PosService} from "@etop/state/etop-app/pos";

enum TypeTicketModal {
  weekend = 'weekend',
  holiday = 'holiday',
  support = 'support',
}

@Component({
  selector: 'etop-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  constructor(
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private toast: ToastService,
    private orderService: OrderService,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private fulfillmentService: FulfillmentService,
    private auth: AuthenticateStore,
    private posService: PosService,
    private vtigerService: ShopCrmService,
    private cms: CmsService,
    private modalController: ModalController
  ) {
  }

  get isPaymentDone() {
    // TODO: temporarily doing this!!! wait for VALID order.payment_status
    return this.order.received_amount >= this.order.total_amount;
  }
  order = new Order({});
  showList = true;
  paymentDisplay = true;
  fulfillment: Fulfillment;
  loading = false;
  segment = 'info';

  holiday = false;
  reason_holiday = '';

  ticketNote = '';
  tickets: Array<Ticket> = [];

  showActionSheet = false;

  private static stripHTML(html: string) {
    const _tempDIV = document.createElement('div');
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || '';
  }

  private static removeNewLineCharacters(str) {
    return str.trim().replace(/\r?\n|\r/g, '');
  }

  async ngOnInit() {
  }

  async ionViewWillEnter() {
    await this.resetData();
    this.showActionSheet =  !['N', 'NS'].includes(this.order.status)
      && this.isPermissions('shop/order:cancel');
    this.ticketNote = await this.cms.getTicketNote();
    await this.getTickets();
    await this.getHoliday();
  }

  async resetData() {
    this.loading = true;
    this.ref.detectChanges();
    try {
      const {params} = this.activatedRoute.snapshot;
      const id = params.id;
      this.order = await this.orderService.getOrder(id);
      this.fulfillment = this.order.activeFulfillment;
    } catch(e) {
      debug.error('ERROR in resetData', e);
    }
    this.loading = false;
    this.ref.detectChanges();
  }

  dismiss() {
    this.navCtrl.back();
  }

  toggleList() {
    this.showList = !this.showList;
  }

  togglePayment() {
    this.paymentDisplay = !this.paymentDisplay;
  }

  statusMap(status) {
    return ORDER_STATUS[status] || 'Không xác định';
  }

  async presentActionSheet() {
    const buttons = [];
    if (
      !['N', 'NS', 'P'].includes(this.order.status) &&
      this.isPermissions('shop/order:confirm')
    ) {
      buttons.push({
        text: 'Hoàn thành đơn hàng',
        cssClass: 'text-success',
        handler: () => {
          this.completeOrderSheet();
        }
      });
    }
    if (
      !['N', 'NS'].includes(this.order.status) &&
      this.isPermissions('shop/order:cancel')
    ) {
      buttons.push({
        text: 'Hủy đơn hàng',
        role: 'destructive',
        handler: () => {
          this.reasonCancelSheet();
        }
      });
    }
    if (buttons.length > 0) {
      buttons.push(
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: () => {}
        }
      );
    }
    const actionSheet = await this.actionSheetController.create({
      header: 'Đơn hàng',
      buttons
    });
    await actionSheet.present();
  }

  isPermissions(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  async reasonCancelSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Vui lòng chọn lý do hủy',
      buttons: [
        {
          text: 'Sai thông tin',
          handler: () => {
            this.cancelOrder('Sai thông tin');
          }
        },
        {
          text: 'Khách yêu cầu hủy',
          handler: () => {
            this.cancelOrder('Khách yêu cầu hủy');
          }
        },
        {
          text: 'Tư vấn sai sản phẩm',
          handler: () => {
            this.cancelOrder('Tư vấn sai sản phẩm');
          }
        },
        {
          text: 'Hết hàng',
          handler: () => {
            this.cancelOrder('Hết hàng');
          }
        },
        {
          text: 'Lý do khác',
          handler: () => {
            this.otherReason();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async otherReason() {
    const alert = await this.alertController.create({
      message: `Nhập lý do bạn muốn hủy đơn hàng.`,
      inputs: [
        {
          name: 'other_reason',
          type: 'text',
          placeholder: 'Lý do hủy đơn'
        }
      ],
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12'
        },
        {
          text: 'Xác nhận',
          cssClass: 'text-primary font-12',
          handler: async alertData => {
            this.cancelOrder(alertData.other_reason).then();
          }
        }
      ]
    });

    await alert.present();
  }

  orderShipping() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    this.posService.updateOrder(this.order);
    this.navCtrl.navigateForward(`/s/${slug}/shipping`).then();
  }

  isCancelledOrder(): boolean {
    return this.order.status == 'N' || this.order.status == 'NS';
  }

  hasActiveFulfillment(): boolean {
    if (this.order.activeFulfillment) {
      const {status, shipping_code} = this.order.activeFulfillment;
      return status != 'N' && !!shipping_code;
    }
    return false;
  }

  hasFulfillment(): boolean {
    if (this.order.activeFulfillment) {
      const {shipping_code} = this.order.activeFulfillment;
      return !!shipping_code;
    }
    return false;
  }

  async cancelOrder(reason) {
    try {
      if (
        this.order.confirm_status == 'N' &&
        this.fulfillment.shipping_state == 'cancelled'
      ) {
        return this.toast.error(
          'Đơn đã ở trạng thái hủy. Vui lòng kiểm tra lại!'
        );
      }

      if (this.order.confirm_status != 'N') {
        await this.orderService.cancelOrder(this.order, reason);
      }
      this.resetData().then();
      this.toast.success('Hủy đơn hàng thành công!').then();
    } catch (e) {
      debug.error('ERROR when cancel order', e);
      this.toast.error('Hủy đơn hàng không thành công. ' + (e.code && (e.message || e.msg))).then();
    }
  }

  trackingLink() {
    return this.fulfillmentService.trackingLink(this.order.activeFulfillment);
  }

  async completeOrderSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: `Bạn có chắc muốn hoàn thành đơn hàng \n ${this.order.code}?`,
      buttons: [
        {
          text: 'Xác nhận',
          handler: () => {
            this.completeOrder();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: () => {
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async completeOrder() {
    try {
      await this.orderService.completeOrder(this.order);
      this.toast.success('Hoàn thành đơn hàng thành công.').then();
      this.resetData().then();
    } catch (e) {
      debug.error('ERROR in confirming Order', e);
      this.toast.error(`Hoàn thành đơn hàng không thành công. ${e.code && (e.message || e.msg)}`).then();
    }
  }

  async confirmOrder() {
    try {
      await this.orderService.confirmOrder(this.order.id);
      this.toast.success('Xác nhận đơn hàng thành công.').then();
      this.resetData().then();
    } catch (e) {
      debug.error('ERROR in confirming Order', e);
      this.toast.error(`Xác nhận đơn hàng không thành công. ${e.code && (e.message || e.msg)}`).then();
    }
  }

  async getTickets() {
    try {
      const shop = this.auth.snapshot.shop;
      this.tickets = await this.vtigerService.getTickets({
        etop_id: shop.user.id,
        order_id: this.order?.id
      });
    } catch (e) {
      debug.error('TICKETS ERROR', e);
    }
  }

  async openRequestTicketModal() {
    const now = new Date();
    const day = now.getDay();
    const hours = now.getHours();
    let type = TypeTicketModal.support;

    if (this.holiday) {
      type = TypeTicketModal.holiday;
    }

    if (day == 0 || (day == 6 && hours >= 12)) {
      type = TypeTicketModal.weekend;
    }

    this.fulfillment.order = {
      ...this.fulfillment.order,
      id: this.order.id,
      code: this.order.code
    };
    const modal = await this.modalController.create({
      component: TicketModalComponent,
      componentProps: {
        ffm: JSON.parse(JSON.stringify(this.fulfillment)),
        type
      }
    });
    modal.onDidDismiss().then(() => this.getTickets());
    await modal.present();
  }

  async ticketDetail(ticket: Ticket) {
    const modal = await this.modalController.create({
      component: TicketDetailComponent,
      componentProps: {
        ticket
      }
    });
    modal.onDidDismiss().then(() => this.getTickets());
    await modal.present();
  }

  async getHoliday() {
    try {
      const res = await fetch(
        'https://cms-setting.etop.vn/api/get-banner'
      ).then(r => r.json());
      const now = moment(new Date()).format('DD/MM/YYYY');
      if (res.data) {
        const banner = res.data[24];
        const description = OrderDetailComponent.removeNewLineCharacters(
          OrderDetailComponent.stripHTML(banner.description)
        ).split('###');
        description.forEach(item => {
          item = item.split(',');
          for (let i = 0; i < item.length; i++) {
            if (item[i] == now) {
              this.holiday = true;
              this.reason_holiday = item[item.length - 1];
            }
          }
        });
      } else {
        return '';
      }
    } catch (e) {
      debug.log(e);
    }
  }

  canForcePicking(fulfillment) {
    return this.fulfillmentService.canForcePicking(fulfillment);
  }

  canForceDelivering(fulfillment) {
    return this.fulfillmentService.canForceDelivering(fulfillment);
  }

  async segmentChanged(event) {
    this.segment = event;
  }

  async reloadTicket(event) {
    await this.getTickets();
    event.target.complete();
  }
}
