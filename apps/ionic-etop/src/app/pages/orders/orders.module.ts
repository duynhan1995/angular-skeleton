import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import {OrderService} from "apps/ionic-etop/src/services/order.service";
import { OrdersComponent } from './orders.component';
import { TabsModule } from '../../components/tabs/tabs.module';
import { MenuModule } from '../../components/menu/menu.module';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OrderListComponent } from './order-list/order-list.component';
import { FilterModalComponent } from '../../components/filter-modal/filter-modal.component';
import {  AddressApi, FulfillmentApi } from '@etop/api';
import { PopoversModule } from '../../components/popovers/popovers.module';
import { VTigerService } from 'apps/core/src/services/vtiger.service';
import { TelegramService } from '@etop/features';
import { TicketModule } from '../../components/ticket/ticket.module';
import { ShopCrmService } from 'apps/shop/src/services/shop-crm.service';
import { CrmApi } from 'apps/core/src/apis/crm-api.service';
import { IonSkeletonModule } from '../../components/ion-skeleton/ion-skeleton.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: OrderListComponent },
      { path: ':id', component: OrderDetailComponent },
    ]
  }
];

const components = [
  OrdersComponent,
  OrderDetailComponent,
  OrderListComponent,
];

@NgModule({
  declarations: [...components],
  entryComponents: [],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkStatusModule,
    PopoversModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    TicketModule,
    IonSkeletonModule,
    SubscriptionWarningModule
  ],
  providers: [
    FilterModalComponent,
    FulfillmentApi,
    AddressApi,
    VTigerService,
    TelegramService,
    ShopCrmService,
    CrmApi,
    OrderService
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule {}
