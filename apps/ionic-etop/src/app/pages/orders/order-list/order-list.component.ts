import {Component, NgZone, OnInit, Provider, ViewChild} from '@angular/core';
import {Order, ORDER_STATUS} from 'libs/models/Order';
import {NetworkStatus, Plugins} from '@capacitor/core';
import {OrderService} from '@etop/features';
import {IonInfiniteScroll, IonRouterOutlet, ModalController, NavController} from '@ionic/angular';
import {AuthenticateStore} from '@etop/core';
import {UtilService} from 'apps/core/src/services/util.service';
import {OrderApi} from '@etop/api';
import {FilterOperator, FilterOptions} from '@etop/models';
import {FilterModalComponent} from '../../../components/filter-modal/filter-modal.component';

const {Network} = Plugins;

@Component({
  selector: 'etop-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  orders: Order[] = [];
  loading = true;
  status: NetworkStatus;
  viewDisconnect = false;
  networkStatus = true;
  offset = 0;
  perpage = 20;

  shippingState = [
    {name: 'Tất cả', value: ''},
    {name: 'Đã tạo', value: 'created'},
    {name: 'Đã xác nhận', value: 'confirmed'},
    {name: 'Đang xử lý', value: 'processing'},
    {name: 'Đang lấy hàng', value: 'picking'},
    {name: 'Chờ giao', value: 'holding'},
    {name: 'Đang giao hàng', value: 'delivering'},
    {name: 'Đang trả hàng', value: 'returning'},
    {name: 'Đã giao hàng', value: 'delivered'},
    {name: 'Đã trả hàng', value: 'returned'},
    {name: 'Không giao được', value: 'undeliverable'},
    {name: 'Hủy', value: 'cancelled'},
    {name: 'Không xác định', value: 'unknown'}
  ];

  filterOptions: FilterOptions = [
    {
      name: 'fulfillment.shipping_code',
      operator: FilterOperator.n,
      value: '',
      type: 'input',
      label: 'Mã đơn giao hàng',
      placeholder: 'Nhập mã đơn giao hàng'
    },
    {
      name: 'customer.name',
      operator: FilterOperator.contains,
      value: '',
      type: 'input',
      label: 'Tên người nhận',
      placeholder: 'Nhập tên người nhận'
    },
    {
      name: 'customer.phone',
      operator: FilterOperator.eq,
      value: '',
      type: 'input',
      label: 'Số điện thoại người nhận',
      placeholder: 'Nhập số điện thoại'
    },
    {
      name: 'fulfillment.shipping_state',
      operator: FilterOperator.n,
      value: '',
      type: 'select',
      label: 'Trạng thái đơn giao hàng',
      placeholder: 'Chọn trạng thái đơn',
      options: this.shippingState
    }
    // {
    //   name: 'chargeable_weight',
    //   operator: FilterOperator.gt,
    //   value: {
    //     lower: 0,
    //     upper: 50000
    //   },
    //   type: 'ion-range',
    //   meta: {
    //     unit: 'g',
    //     min: 0,
    //     max: 50000,
    //     step: 100
    //   },
    //   label: 'Khối lượng'
    // }
  ];

  filters = [];

  constructor(
    private orderService: OrderService,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private zone: NgZone,
    private util: UtilService,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
  ) {
    this.viewDisconnect = false;
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        if (this.orders.length == 0) {
          this.viewDisconnect = true;
        }
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getOrders();
        });
      } else {
        this.zone.run(async () => {
          if (!this.orders) {
            this.viewDisconnect = true;
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  async getStatus() {
    return await Network.getStatus();
  }

  async ionViewWillEnter() {
    await this.getOrders();
  }

  async doRefresh(event) {
    await this.getOrders();
    event.target.complete();
  }

  resetFilters() {
    this.filterOptions = this.filterOptions.map(filterOption => {
      if (filterOption.type == 'ion-range') {
        filterOption.value = {
          upper: filterOption.meta?.max,
          lower: filterOption.meta?.min
        };
      } else {
        filterOption.value = '';
      }
      return filterOption;
    });
  }

  async getOrders(filterOptions?) {
    this.infiniteScroll.disabled = false;
    this.offset = 0;
    try {
      // TODO: Map filter
      const filters =
        filterOptions
          ?.filter(fo => !!fo.value)
          .map(filterOption => {
            if (filterOption.type == 'ion-range') {
              const ranges = [];
              const value = filterOption.value;
              const meta = filterOption.meta;
              if (value.lower > meta.min) {
                ranges.push({
                  name: filterOption.name,
                  op: FilterOperator.gte,
                  value: value.lower.toString()
                });
              }
              if (value.upper < meta.max) {
                ranges.push({
                  name: filterOption.name,
                  op: FilterOperator.lte,
                  value: value.upper.toString()
                });
              }
              return ranges;
            } else {
              return [
                {
                  name: filterOption.name,
                  op: filterOption.operator,
                  value: filterOption.value
                }
              ];
            }
          })
          .reduce((a, b) => a.concat(b), []) || [];
      this.filters = filters;
      const _orders = await this.orderService.getOrders(
        0,
        this.perpage,
        filters
      );
      const orders = _orders.map(o => this.orderService.setupDataOrder(o));
      orders.map(order => {
        if (order.fulfillments.length > 0) {
          const ffm = order.fulfillments[0];
          ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
            ffm.shipping_state
          );
        }
      });
      if (orders) {
        this.orders = orders;
      }
      this.loading = false;
    } catch (e) {
      this.loading = false;
      debug.error('ERROR in getting orders', e.message);
    }
  }

  async loadMore(event) {
    this.offset += this.perpage;
    const _orders = await this.orderService.getOrders(
      this.offset,
      this.perpage,
      this.filters
    );
    if (_orders.length == 0) {
      this.infiniteScroll.disabled = true;
    }
    this.orders = this.orders.concat(_orders);
    event.target.complete();
  }

  async orderDetail(order) {
    if (
      !this.auth.snapshot.permission.permissions.includes('shop/order:view')
    ) {
      return;
    }
    if (!this.status.connected) {
      return;
    }
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/orders/${order.id}`
    );
  }

  statusMap(status) {
    return ORDER_STATUS[status] || 'Không xác định';
  }

  createOrder() {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/pos`, {
      animated: false
    });
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        filters: JSON.parse(JSON.stringify(this.filterOptions))
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.loading = true;
        if (data.data.reset) {
          this.resetFilters();
        } else {
          this.filterOptions = data.data.filters;
        }
        this.getOrders(this.filterOptions);
      }
    });
    return await modal.present();
  }

  trackOrder(index, order: Order) {
    return order.id;
  }

  addOrder () {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/pos/lines`
    );
  }
}
