import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register.component';
import { Routes, RouterModule } from '@angular/router';
import { LoadingViewModule } from 'apps/ionic-etop/src/app/components/loading-view/loading-view.module';

const routes: Routes = [
  {
    path: '',
    component: RegisterComponent
  }
];

@NgModule({
  declarations: [RegisterComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    LoadingViewModule
  ],
  exports: [RegisterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegisterModule {}
