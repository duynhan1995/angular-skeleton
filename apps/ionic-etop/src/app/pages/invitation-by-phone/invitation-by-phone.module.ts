import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../features/shared/shared.module';
import { InvitationByPhoneComponent } from './invitation-by-phone.component';
import { EtopPipesModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: InvitationByPhoneComponent
  }
];

@NgModule({
  declarations: [InvitationByPhoneComponent],
  imports: [
    CommonModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [InvitationByPhoneComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvitationByPhoneModule {}
