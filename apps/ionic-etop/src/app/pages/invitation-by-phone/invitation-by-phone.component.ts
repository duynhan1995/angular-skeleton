import { Component, OnInit, NgZone } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController, Platform } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { Invitation } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';
import { ToastService } from '../../../services/toast.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { LoadingService } from '../../../services/loading.service';
import { FilterOperator } from '@etop/models';

@Component({
  selector: 'etop-invitation-by-phone',
  templateUrl: './invitation-by-phone.component.html',
  styleUrls: ['./invitation-by-phone.component.scss']
})
export class InvitationByPhoneComponent implements OnInit {
  phone;
  userInvitations: Invitation[] = [];

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private util: UtilService,
    private ngZone: NgZone,
    private router: Router,
    private authorizationApi: AuthorizationApi,
    private toast: ToastService,
    private alertController: AlertController,
    private zone: NgZone,
    private commonUsecase: CommonUsecase,
    private loadingService: LoadingService,
    private platform: Platform
  ) {
    const sub = this.platform.backButton.subscribe(() => {
      sub.unsubscribe();
    });
  }

  async ngOnInit() {}

  async ionViewDidEnter() {
    this.phone = this.route.snapshot.params.phone.substring(1);
    if (!this.differentUser && this.user) {
      await this.getUserInvitations();
    }
  }

  dismiss() {
    this.ngZone.run(async () => {
      this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/dashboard}`, {
        animated: false
      });
    });
  }
  get user() {
    return this.auth.snapshot.user;
  }

  get account() {
    return this.auth.snapshot.account;
  }

  get differentUser() {
    if (!this.user) {
      return false;
    }
    return this.auth.snapshot.user.phone != this.phone;
  }

  login() {
    this.auth.clear();
    this.router.navigate(['/login'], {
      queryParams: { invitation_phone: this.phone }
    });
  }

  async getUserInvitations() {
    try {
      this.userInvitations = [];
      const date = new Date();
      const dateString = date.toISOString();
      const userInvitations = await this.authorizationApi.getUserInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
      this.userInvitations = userInvitations.filter(
        user => user.phone == this.phone
      );
    } catch (e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }

  async acceptInvitation(invitation) {
    try {
      this.loadingService.start('Đang đồng bộ dữ liệu');
      await this.authorizationApi.acceptInvitation(invitation.token);
      await this.commonUsecase.updateSessionInfo(true);
      const shop_index = this.auth.findAccountIndex(invitation.shop_id);
      this.auth.selectAccount(shop_index);
      this.loadingService.end();
      this.zone.run(async () => {
        this.navCtrl.navigateForward(
          `/s/${this.util.getSlug()}/dashboard}`,
          {
            animated: false
          }
        );
      });
    } catch (e) {
      this.loadingService.end();
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Accepting Invitation', e);
    }
  }

  async reject(invitation) {
    const alert = await this.alertController.create({
      header: 'Từ chối tham gia quản trị',
      message: `Bạn có chắc từ chối lời mời từ cửa hàng "<strong>${invitation.shop.name}</strong>".`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Từ chối',
          cssClass: 'text-danger',
          handler: () => {
            this.rejectInvitation(invitation);
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  async rejectInvitation(invitation) {
    try {
      await this.authorizationApi.rejectInvitation(invitation.token);
      this.zone.run(async () => {
        await this.getUserInvitations();
      });
    } catch (e) {
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Rejecting Invitation', e);
    }
  }

  backToShop() {
    this.zone.run(async () => {
      this.navCtrl.navigateForward(
        `/s/${this.auth.snapshot.account.url_slug}/dashboard}`,
        {
          animated: false
        }
      );
    });
  }

  createShop() {
    this.ngZone.run(async () => {
      this.navCtrl.navigateForward(`/survey`, {
        animated: false
      });
    });
  }
}
