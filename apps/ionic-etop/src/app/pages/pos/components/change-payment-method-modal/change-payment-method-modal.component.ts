import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Ledger } from 'libs/models/Receipt';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';

@Component({
  selector: 'etop-change-payment-method-modal',
  templateUrl: './change-payment-method-modal.component.html',
  styleUrls: ['./change-payment-method-modal.component.scss']
})
export class ChangePaymentMethodModalComponent implements OnInit {
  selected_ledger = new Ledger({});
  ledgers: Ledger[] = [];

  constructor(
    private receiptStore: ReceiptStoreService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    const _snapshot = this.receiptStore.snapshot;
    this.selected_ledger = _snapshot.selected_ledger;
    this.ledgers = _snapshot.ledgers;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  selectLedger(ledger: Ledger) {
    this.receiptStore.updateSelectedLedger(ledger);
    this.dismiss();
  }

}
