import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { Product } from 'libs/models/Product';
import {PosQuery} from '@etop/state/etop-app/pos';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'etop-pos-product-item',
  templateUrl: './pos-product-item.component.html',
  styleUrls: ['./pos-product-item.component.scss']
})
export class PosProductItemComponent implements OnInit, OnChanges {
  @Input() product = new Product({});

  isSelected$ = new Observable();

  constructor(
    private posQuery: PosQuery
  ) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.product?.currentValue?.id) {
      const product: Product = changes.product?.currentValue;
      this.isSelected$ = this.posQuery.select('selectedVariants').pipe(
        map(variants => variants.some(variant => product.variants.map(v => v.id).includes(variant.id)))
      )
    }
  }

}
