import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderLine } from 'libs/models/Order';
import { BaseComponent } from '@etop/core';
import { IonInput, NavController } from '@ionic/angular';
import { ToastService } from '../../../../services/toast.service';
import { FormBuilder, FormArray } from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';
import { takeUntil } from 'rxjs/operators';
import {PosQuery, PosService} from '@etop/state/etop-app/pos';

@Component({
  selector: 'etop-pos-basket',
  templateUrl: './pos-basket.component.html',
  styleUrls: ['./pos-basket.component.scss']
})
export class PosBasketComponent extends BaseComponent implements OnInit {
  @ViewChild('discountInput', { static: false }) discountInput: IonInput;
  @ViewChild('feeInput', { static: false }) feeInput: IonInput;
  order_lines: OrderLine[] = [];

  discount: number;
  fee: number;
  total_amount: number;

  order$ = this.posQuery.select('order');
  orderForm = this.fb.group({
    basket_value: 0,
    order_discount: 0,
    lines: this.fb.array([]),
    total_fee: 0,
    total_discount: 0,
    total_amount: 0,
    fee_lines: this.fb.array([])
  });

  constructor(
    private navCtrl: NavController,
    private toastService: ToastService,
    private posQuery: PosQuery,
    private posService: PosService,
    private fb: FormBuilder,
    private util: UtilService
  ) {
    super();
  }

  get basketValue() {
    const lines = this.orderForm.controls.lines.value;
    return lines.reduce((a, b) => a + Number(b.quantity) * Number(b.retail_price), 0);
  }

  get totalItems() {
    const lines = this.orderForm.controls.lines.value;
    return lines.reduce((a, b) => a + Number(b.quantity), 0);
  }

  get totalFee() {
    const fee_lines = this.orderForm.controls.fee_lines.value;
    return fee_lines.reduce((a, b) => a + Number(b.amount), 0);
  }

  get lines() {
    return this.orderForm.controls.lines as FormArray;
  }

  get feeLines() {
    return this.orderForm.controls.fee_lines as FormArray;
  }

  ngOnInit() {}

  valueChanges() {
    this.lines.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(lines => {
      if (lines.length > 0) {
        const basket_value = lines.reduce(
          (a, b) => a + Number(b.quantity) * Number(b.retail_price),
          0
        );
        const total_items = lines.reduce((a, b) => a + Number(b.quantity), 0);
        this.orderForm.patchValue({
          basket_value,
          total_items
        });
      } else {
        this.orderForm.patchValue({
          basket_value: 0,
          total_items: 0
        });
      }
      this.totalAmount();
    });
    this.orderForm.controls.order_discount.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.totalAmount();
      });
    this.feeLines.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.totalAmount();
      });
  }

  ionViewWillEnter() {
    this.resetFormData();
    this.valueChanges();
    const order = this.posQuery.getValue().order;
    const { lines, basket_value, fee_lines, order_discount } = order;
    if (lines?.length) {
      lines.forEach(line => {
        this.lines.push(
          this.fb.group({
            ...line,
            attributes: [line.attributes]
          })
        );
      });
    }

    if (fee_lines?.length) {
      fee_lines.forEach(fee => {
        this.feeLines.push(this.fb.group(fee));
      });
      this.orderForm.patchValue({
        fee_lines
      });
    } else {
      this.feeLines.push(
        this.fb.group({
          type: 'other',
          name: 'Phụ phí',
          desc: 'Phụ thu',
          amount: 0
        })
      );
    }
    this.orderForm.patchValue({
      basket_value,
      order_discount: order_discount || 0
    });
    this.totalAmount();
  }

  ionViewDidLeave() {
    this.saveDataStore();
    this.resetFormData();
  }

  back() {
    this.navCtrl.back();
  }

  resetFormData() {
    this.orderForm.patchValue({
      basket_value: 0,
      order_discount: 0,
      total_fee: 0,
      total_discount: 0,
      total_amount: 0,
    });
    this.lines.clear();
    this.feeLines.clear();
  }

  totalAmount() {
    const discount = this.orderForm.controls.order_discount.value;
    this.total_amount =
      Number(this.basketValue) +
      Number(this.totalFee || 0) -
      Number(discount || 0);
    this.orderForm.patchValue({
      total_amount: this.total_amount
    });
  }

  checkBasket(line: OrderLine, index) {
    line.quantity = Number(line.quantity);
    if (line.quantity == 0) {
      this.lines.at(index).patchValue({
        quantity: 1
      });
    } else {
      this.lines.at(index).patchValue({
        quantity: Number(line.quantity)
      });
    }
    this.totalAmount();
  }

  saveDataStore() {
    const order = this.orderForm.value;
    const { lines, total_amount, fee_lines } = order;

    const order_discount = this.orderForm.getRawValue().order_discount;
    const _lines = this.lines.value;
    _lines.map(line => {
      return {
        ...line,
        attributes: Array(line.attributes)
      };
    });
    const total_items = lines.reduce((a, b) => a + Number(b.quantity), 0);
    const basket_value = lines.reduce(
      (a, b) => a + Number(b.quantity) * Number(b.retail_price),
      0
    );
    const preOrder = this.posQuery.getValue().order;
    this.posService.updateOrder({
      ...preOrder,
      lines: _lines,
      basket_value,
      total_fee: this.totalFee,
      order_discount: Number(order_discount) || 0,
      total_discount: Number(order_discount) || 0,
      total_amount,
      fee_lines,
      total_items
    });
  }

  async toPayment() {
    if (this.total_amount < 0) {
      return this.toastService.error('Số tiền khách phải trả không hợp lệ!');
    }
    await this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/pos/payment`,
      { animated: true }
    );
  }

  removeLine(index: number) {
    this.lines.removeAt(index);
    this.totalAmount();
  }

  lineDisplayAttributes(line: OrderLine) {
    let str = '';
    for (let i = 0; i < line.attributes.length; i++) {
      if (i == 0) {
        str += line.attributes[i].value;
      } else {
        str += ' - ' + line.attributes[i].value;
      }
    }
    return str;
  }
}
