import {Component, OnInit, Provider} from '@angular/core';
import {OrderApi} from '@etop/api';
import {NavController} from "@ionic/angular";
import {Order} from 'libs/models/Order';
import {ToastService} from 'apps/ionic-etop/src/services/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseComponent} from '@etop/core';
import {LoadingService} from 'apps/ionic-etop/src/services/loading.service';
import {ReceiptStoreService} from 'apps/core/src/stores/receipt.store.service';
import {UtilService} from 'apps/core/src/services/util.service';
import {PosQuery, PosService} from '@etop/state/etop-app/pos';
import { Fulfillment, PROVIDER_LOGO } from '@etop/models';
import { OrderService } from 'apps/ionic-etop/src/services/order.service';

@Component({
  selector: 'etop-pos-success',
  templateUrl: './pos-success.component.html',
  styleUrls: ['./pos-success.component.scss']
})
export class PosSuccessComponent extends BaseComponent implements OnInit {
  order_id: string;
  order = new Order({});
  ffm = new Fulfillment({});
  loading = true;

  constructor(
    private navCtrl: NavController,
    private orderApi: OrderApi,
    private receiptStore: ReceiptStoreService,
    private toast: ToastService,
    private loadingService: LoadingService,
    private util: UtilService,
    private posQuery: PosQuery,
    private posService: PosService,
    private activatedRoute: ActivatedRoute,
    private orderService: OrderService,
  ) {
    super();
  }

  get confirmStatusImg() {
    if (this.order.confirm_status == 'P') {
      return 'assets/imgs/checked.png';
    }
    return 'assets/imgs/warning.png';
  }

  get hasError() {
    return this.order.confirm_status != 'P';
  }

  get paymentMethodDisplay() {
    const selected_ledger = this.receiptStore.snapshot.selected_ledger;
    if (selected_ledger.type == 'bank') {
      return `<div>Chuyển khoản qua TK thanh toán <b>${selected_ledger.name}</b></div>`;
    }
    return '<span>Tiền mặt</span>';
  }

  ngOnInit() {}

  ionViewWillEnter() {
    const orderID = this.activatedRoute.snapshot.queryParamMap.get('order_id');
    const order = this.posQuery.getValue().shipmentConfirmOrder;
    this.order_id = orderID || order?.id;
    this.getOrderData().then();
  }

  async getOrderData() {
    this.loading = true;
    await this.loadingService.start('');
    try {
      this.order = await this.orderService.getOrder(this.order_id)
    } catch (e) {
      debug.error('ERROR in getting order data', e);
    }
    this.ffm = this.order.activeFulfillment;
    this.loadingService.end();
    this.loading = false;
  }

  async retryConfirm() {
    this.loading = true;
    await this.loadingService.start('');
    try {
      await this.orderApi.confirmOrder(this.order_id, 'confirm');
      this.order = await this.orderService.getOrder(this.order_id)
    } catch (e) {
      debug.error('ERROR in getting order data', e);
      this.toast.error('Có lỗi xảy ra. Xin vui lòng thử lại!').then();
    }
    this.loadingService.end();
    this.loading = false;
  }

  newOrder() {
    this.posService.resetStore();
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/pos`).then();
  }

  viewOrders() {
    this.posService.resetStore();
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/orders`).then();
  }
}
