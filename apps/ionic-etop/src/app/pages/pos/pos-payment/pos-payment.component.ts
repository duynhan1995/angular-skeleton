import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Customer } from 'libs/models/Customer';
import { Order, OrderLine } from 'libs/models/Order';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { CustomerApi, OrderApi, ReceiptApi } from '@etop/api';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { ToastService } from 'apps/ionic-etop/src/services/toast.service';
import { LoadingService } from 'apps/ionic-etop/src/services/loading.service';
import { CustomerService } from 'apps/ionic-etop/src/services/customer.service';
import { IonInput, NavController } from '@ionic/angular';
import { Ledger } from 'libs/models/Receipt';
import { UtilService } from 'apps/core/src/services/util.service';
import { GaService } from '../../../../services/ga.service';
import { Plugins } from '@capacitor/core';
import { OrderService } from '@etop/features';
import {PosQuery, PosService} from "@etop/state/etop-app/pos";

const { Keyboard } = Plugins;

@Component({
  selector: 'etop-pos-payment',
  templateUrl: './pos-payment.component.html',
  styleUrls: ['./pos-payment.component.scss']
})
export class PosPaymentComponent implements OnInit {
  @ViewChild('customerNameInput', { static: false })
  customerNameInput: IonInput;
  @ViewChild('customerPhoneInput', { static: false })
  customerPhoneInput: IonInput;

  customerList: Customer[] = [];
  customerSearchResult: Customer[] = [];
  activeShipment = false;

  order_id: string;
  order_lines: OrderLine[] = [];
  customer = new Customer({});
  customer_id = null;
  selected_ledger = new Ledger({ type: 'cash' });

  received_amount: number;
  format_money = false;

  paymentMethodDisplay = 'Khách trả tiền mặt';

  suggestDisplay = false;
  isShipping = false;
  loading = true;

  constructor(
    private receiptApi: ReceiptApi,
    private orderApi: OrderApi,
    private customerApi: CustomerApi,
    private orderService: OrderService,
    private receiptService: ReceiptService,
    private customerService: CustomerService,
    private receiptStore: ReceiptStoreService,
    private auth: AuthenticateStore,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private util: UtilService,
    private ga: GaService,
    private zone: NgZone,
    private navCtrl: NavController,
    private posQuery: PosQuery,
    private posService: PosService,
  ) {
    Keyboard.addListener('keyboardDidHide', () => {
      this.zone.run(async () => {
        this.hideSearchResult();
      });
    });
  }

  get basketValue() {
    return this.order_lines.reduce(
      (a, b) => a + Number(b.quantity) * Number(b.retail_price),
      0
    );
  }

  get totalAmount() {
    const order = this.posQuery.getValue().order;
    return order.total_amount;
  }

  async ngOnInit() {
    this.ga.setCurrentScreen('pos_payment');
  }

  async ionViewWillEnter() {
    this.loading = true;
    const order = this.posQuery.getValue().order;
    this.order_lines = order.lines;
    this.received_amount = order?.total_amount || order?.basket_value;
    this.format_money = this.received_amount >= 0;
    await this.prepareData();
    this.loading = false;
  }

  back() {
    this.navCtrl.back();
  }

  async prepareData() {
    this.customerList = await this.customerService.getCustomers(0, 1000, [], false);
    let idx = -2
    while (idx != -1 && this.customerList) {
      idx = this.customerList.findIndex(c => c.full_name == 'Khách lẻ');
      if (idx > -1) {
        this.customerList.splice(idx, 1);
      }
    }

    const _selected_ledger = this.receiptStore.snapshot.selected_ledger;
    if (!_selected_ledger) {
      const _ledgers = await this.receiptService.getLedgers();
      const _ledger = _ledgers.find(l => l.type == 'cash');
      this.selected_ledger = _ledger;
      this.receiptStore.updateLedgers(_ledgers);
      this.receiptStore.updateSelectedLedger(_ledger);
    } else {
      this.selected_ledger = _selected_ledger;
    }
    await this.saveDataStore();
  }

  resetData() {
    this.received_amount = null;
  }

  async saveDataStore() {
    try {
      const preOrder = this.posQuery.getValue().order;
      const customer_id = await this.checkAndCreateCustomer();
      this.posService.updateOrder({
        ...preOrder,
        source: 'etop_pos',
        payment_method: 'cod',
        customer: customer_id ? null : this.customer,
        customer_id: customer_id || null
      });
      this.posService.updateCustomer(this.customer);
      const receipt = this.posQuery.getValue().receipt;
      this.posService.updateReceipt({
        ...receipt,
        ledger_id: this.selected_ledger.id,
        type: 'receipt',
        paid_at: new Date(),
        amount:
          (this.received_amount > this.totalAmount && this.totalAmount) ||
          this.received_amount,
        title: 'Thanh toán đơn hàng',
        ref_type: 'order',
        lines: [
          {
            title: 'Thanh toán đơn hàng',
            amount:
              (this.received_amount > this.totalAmount && this.totalAmount) ||
              this.received_amount,
            ref_id: null
          }
        ]
      })
    } catch (e) {
      debug.log('ERROR in saveDataStore', e);
      throw e;
    }
  }

  async checkAndCreateCustomer() {
    try {
      // NOTE: khách lẻ
      if (!this.customer.full_name && !this.customer.phone) {
        return null;
      }
      const existedCustomers: Customer[] = await this.customerService.checkExistedCustomer(
        {
          phone: this.customer.phone
        }
      );
      const found: Customer = existedCustomers.find(
        c => c.phone == this.customer.phone
      );
      if (found) {
        if (this.customer.full_name != found.full_name) {
          const _body: any = {
            id: found.id,
            full_name: this.customer.full_name
          };
          await this.customerApi.updateCustomer(_body);
        }
        return found.id;
      }

      const body: any = {
        full_name: this.customer.full_name,
        phone: this.customer.phone
      };
      const res = await this.customerApi.createCustomer(body);
      return res.id;
    } catch (e) {
      debug.error('ERROR in checkAndCreateCustomer', e);
      return null;
    }
  }

  private checkReceipt() {
    if (
      this.selected_ledger.type == 'bank' &&
      this.received_amount > this.totalAmount
    ) {
      this.toastService.error(
        'Với phương thức Chuyển khoản, số tiền nhận từ khách không được lớn hơn số tiền khách phải trả!'
      ).then();
      return false;
    }
    return true;
  }

  private checkCustomer() {
    if (!this.customer.full_name && this.customer.phone) {
      this.toastService.error('Vui lòng nhập tên khách hàng!').then();
      return false;
    }
    if (!this.customer.phone && this.customer.full_name) {
      this.toastService.error('Vui lòng nhập số điện thoại khách hàng!').then();
      return false;
    }
    if (this.customer.phone && this.customer.phone.length < 8) {
      this.toastService.error('Số điện thoại không hợp lệ!').then();
      return false;
    }
    return true;
  }

  async create(createAndConfirm: boolean, createShipment: boolean) {
    await this.loadingService.start('Đang xử lý');
    try {
      await this.saveDataStore();
      if (!this.checkReceipt()) {
        this.loadingService.end();
        return;
      }
      if (!this.checkCustomer()) {
        this.loadingService.end();
        return;
      }
      const body: any = this.posQuery.getValue().order;

      const res = await this.orderService.createOrder(body);
      this.posService.updateConfirmOrder(res);
      if (createAndConfirm) {
        try {
          await this.orderApi.confirmOrder(res.id, 'confirm');
        } catch (e) {
          debug.error('ERROR in confirming Order', e);
        }
      }
      if (this.received_amount && !createShipment) {
        await this.createReceipt(res);
      }
      if (!createShipment) {
        const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
        await this.navCtrl.navigateForward(`/s/${slug}/pos/success`);
      }
      this.resetData();
    } catch (e) {
      debug.error('ERROR in creating Order', e);
      this.toastService.error(e.code && (e.msg || e.message) || 'Tạo đơn hàng không thành công. Vui lòng thử lại').then();
    }
    this.loadingService.end();
  }

  async createReceipt(order: Order) {
    try {
      const body: any = {
        ledger_id: this.selected_ledger.id,
        type: 'receipt',
        paid_at: new Date(),
        trader_id: order.customer_id,
        amount:
          (this.received_amount > this.totalAmount && this.totalAmount) ||
          this.received_amount,
        title: 'Thanh toán đơn hàng',
        ref_type: 'order',
        lines: [
          {
            title: 'Thanh toán đơn hàng',
            amount:
              (this.received_amount > this.totalAmount && this.totalAmount) ||
              this.received_amount,
            ref_id: order.id
          }
        ]
      };
      const receipt = await this.receiptService.createReceipt(body);
      await this.receiptApi.confirmReceipt(receipt.id);
      this.toastService.success('Tạo phiếu thu thành công!').then();
    } catch (e) {
      debug.error('ERROR in creating receipt', e);
      this.toastService.error('Tạo phiếu thu không thành công!').then();
    }
  }

  async changePaymentMethod() {
    return this.toastService.warning('Tính năng đang phát triển');
  }

  searchCustomer(search_by: string) {
    this.suggestDisplay = true;
    let query: string;
    switch (search_by) {
      case 'name':
        query = this.customer.full_name;
        break;
      case 'phone':
        query = this.customer.phone;
        break;
      default:
        query = this.customer.full_name;
        break;
    }
    setTimeout(_ => {
      if (!query) {
        this.customerSearchResult = [];
        this.suggestDisplay = false;
      } else {
        query = this.util.makeSearchText(query);
        this.customerSearchResult = this.customerList.filter(
          c => c.search_text.indexOf(query.toLowerCase()) > -1
        );

      }
    }, 250);
  }

  selectCustomer(customer: Customer) {
    this.customer.full_name = customer.full_name;
    this.customer.phone = customer.phone;
  }

  hideSearchResult() {
    setTimeout(_ => {
      this.customerSearchResult = [];
    }, 200);
    this.suggestDisplay = false;
  }

  goToNextInputField(fieldName: IonInput) {
    fieldName.setFocus();
  }

  async orderShipping() {
    try {
      this.isShipping = true;
      await this.saveDataStore();
      if (!this.checkCustomer()) { return; }
      await this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/shipping?ref=pos`);
    } catch (e) {
      debug.log('ERROR in orderShipping', e)
    }
  }
}
