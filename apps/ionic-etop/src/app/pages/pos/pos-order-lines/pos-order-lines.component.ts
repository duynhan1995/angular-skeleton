import {Component, OnInit, NgZone, ChangeDetectorRef} from '@angular/core';
import {ProductService} from 'apps/shop/src/services/product.service';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {Product, Variant} from 'libs/models/Product';
import {AlertController, ModalController, NavController, PopoverController} from '@ionic/angular';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {ToastService} from 'apps/ionic-etop/src/services/toast.service';
import {LoadingService} from 'apps/ionic-etop/src/services/loading.service';
import {
  SelectVariantModalComponent
} from 'apps/ionic-etop/src/app/pages/pos/components/select-variant-modal/select-variant-modal.component';
import {GaService} from '../../../../services/ga.service';

import {Plugins, NetworkStatus} from '@capacitor/core';
import {ProductStore} from 'apps/core/src/stores/product.store';
import {FormBuilder, FormArray} from '@angular/forms';
import {UtilService} from 'apps/core/src/services/util.service';
import {PermisionService} from '../../../../services/permission.service';
import {OrderLine} from '@etop/models/Order';
import {PosQuery, PosService} from '@etop/state/etop-app/pos';
import {combineLatest} from 'rxjs';

const {Network} = Plugins;

const ANIMATION_DURATION = 500;

// NOTE: Remeber to change duration in SCSS file when you want to change duration time

@Component({
  selector: 'etop-pos',
  templateUrl: './pos-order-lines.component.html',
  styleUrls: ['./pos-order-lines.component.scss']
})
export class PosOrderLinesComponent extends BaseComponent implements OnInit {
  products$ = this.productStore.state$.pipe(
    map(p => p?.productList),
    distinctUntilChanged(
      (prev, next) => JSON.stringify(prev) == JSON.stringify(next)
    )
  );

  showQuickBasket = false;

  animating_up = false;
  animating_down = false;
  old_quantity = 0;

  status: NetworkStatus;
  networkStatus = true;
  loading = true;

  orderForm = this.fb.group({
    total_items: 0,
    basket_value: 0,
    lines: this.fb.array([])
  });

  recentlyCreatedProductID$ = this.posQuery.select('recentlyCreatedProductID');

  constructor(
    private productService: ProductService,
    private auth: AuthenticateStore,
    private modalCtrl: ModalController,
    private barcodeScanner: BarcodeScanner,
    private alertController: AlertController,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private popoverController: PopoverController,
    private ga: GaService,
    private zone: NgZone,
    private ref: ChangeDetectorRef,
    private productStore: ProductStore,
    private posQuery: PosQuery,
    private posService: PosService,
    private fb: FormBuilder,
    private util: UtilService,
    private permisionService: PermisionService
  ) {
    super();
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        this.networkStatus = false;
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          setTimeout(() => {
            this.networkStatus = true;
          }, 1000);
        });
      } else {
        this.zone.run(async () => {
          this.networkStatus = false;
        });
      }
    });
  }

  get lines() {
    return this.orderForm.controls.lines as FormArray;
  }

  ngOnInit() {
    this.ga.setCurrentScreen('pos');

    combineLatest([this.recentlyCreatedProductID$, this.products$]).pipe(
      map(([productID, productsList]) => {
        return {productID, productsList};
      }),
      takeUntil(this.destroy$)
    ).subscribe(({productID, productsList}) => {
      if (productID) {
        const product = productsList.find(p => p.id == productID);
        if (product) {
          this.selectVariant(product).then();
          this.posService.recentlyCreateProduct(null);
        }
      }
    });
  }

  async ionViewWillEnter() {
    this.posService.updateConfirmOrder(null);

    await this.productService.getProducts(0, 1000);
    this.loading = false;

    this.resetFormData();

    const order = this.posQuery.getValue().order;
    if (order.id) {
      this.posService.resetStore();
    }
    if (order) {
      const {basket_value, total_items, lines} = order;
      
      if (lines?.length) {
        lines.forEach(line => {
          this.lines.push(this.fb.group({
            ...line,
            attributes: [line.attributes]
          }));
        });
      }
      this.orderForm.patchValue({
        basket_value: basket_value || 0,
        total_items: total_items || 0
      });
    }

    this.lines.valueChanges.subscribe(lines => {
      if (lines.length > 0) {
        const basket_value = lines.reduce(
          (a, b) => a + Number(b.quantity) * Number(b.retail_price),
          0
        );
        const total_items = lines.reduce((a, b) => a + Number(b.quantity), 0);
        this.orderForm.patchValue({
          basket_value,
          total_items
        });
      } else {
        this.orderForm.patchValue({
          basket_value: 0,
          total_items: 0
        });
      }

    });
    this.ref.detectChanges();
  }

  resetFormData() {
    this.orderForm.patchValue({
      total_items: 0,
      basket_value: 0
    });
    this.lines.clear();
  }

  async getStatus() {
    return await Network.getStatus();
  }

  async showPopover() {
    this.showQuickBasket = !this.showQuickBasket;
  }

  private countLines() {
    const total_quantity = this.lines.value.reduce(
      (a, b) => a + Number(b.quantity),
      0
    );
    if (total_quantity > this.old_quantity) {
      this.animating_up = true;
    }
    if (total_quantity < this.old_quantity) {
      this.animating_down = true;
    }
    this.old_quantity = total_quantity;
    setTimeout(_ => {
      this.animating_up = false;
      this.animating_down = false;
    }, ANIMATION_DURATION);
  }

  removeLine(index: number) {
    this.onRemoveLine(this.lines.at(index).value);
    this.lines.removeAt(index);
    this.countLines();
  }

  checkBasket(line: OrderLine, index) {
    line.quantity = Number(line.quantity);
    if (line.quantity == 0) {
      this.lines.at(index).patchValue({
        quantity: 1
      });
    } else {
      this.lines.at(index).patchValue({
        quantity: Number(line.quantity)
      });
    }
  }

  async scanBarcode() {
    this.barcodeScanner
      .scan()
      .then(async barcodeData => {
        this.ga.logEvent('scan_barcode', {view: 'pos'});
        if (!barcodeData.text) {
          return;
        }

        const variants = this.productStore.snapshot.productList.reduce((a, b) => a.concat(b.variants), []);

        const variant = variants.find(v => v.code == barcodeData.text);
        if (variant) {
          this.addToOrderLines(variant);
        } else {
          await this.noSkuFound(barcodeData.text);
        }
      })
      .catch(err => {
        this.permisionService.open({
          message: '<strong>eTop POS</strong> cần quyền truy cập Camera để quét mã vạch',
          setting: 'application_details'
        });
        debug.error('ERROR in scanning barcode', err);
        this.ga.logEvent('scan_barcode', {error_msg: err});
      });
  }

  async noSkuFound(barcode) {
    const alert = await this.alertController.create({
      header: 'Không tìm thấy!',
      message: `Mã sản phẩm không tồn tại. Bạn có muốn tạo sản phẩm mới với mã ${barcode} không?`,
      buttons: [
        {
          text: 'Không',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: () => {
          }
        },
        {
          text: 'Tạo',
          cssClass: 'text-primary font-12',
          handler: () => {
            this.createProduct(barcode);
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  addToOrderLines(variant: Variant) {
    const index = this.lines.value.findIndex(l => l.variant_id == variant.id);
    if (index >= 0) {
      const quantity = this.lines.at(index).value.quantity;
      this.lines.at(index).patchValue({
        quantity: quantity + 1
      });
    } else {
      this.lines.push(
        this.fb.group({
          variant_id: variant.id,
          product_id: variant.product_id,
          retail_price: variant.retail_price,
          payment_price: variant.retail_price,
          quantity: 1,
          product_name: this.productService.makeProductNameForOrderLines(
            variant
          ),
          image_url: variant.image,
          attributes: [variant.attributes],
        })
      );
    }

    this.updateOrderLinesStore();

    this.countLines();
  }

  async toBasket() {
    if (!this.status.connected) {
      return;
    }
    if (!this.lines.length) {
      return this.toastService.error('Vui lòng chọn sản phẩm!');
    }

    this.updateOrderLinesStore();

    await this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/pos/basket`);
  }

  updateOrderLinesStore() {
    const _order = this.orderForm.value;
    const {basket_value, total_items} = _order;
    const _lines = this.lines.value;
    _lines.map(line => {
      return {
        ...line,
        attributes: [line.attributes]
      };
    });
    this.posService.updateOrder({
      ...this.posQuery.getValue().order,
      basket_value,
      total_items,
      lines: _lines
    });
  }

  async createProduct(barcode?: string) {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    const queryParams: any = {ref: 'pos'};
    if (barcode) {
      queryParams.barcode = barcode;
    }
    await this.navCtrl.navigateForward(
      `/s/${slug}/products/create`,
      {queryParams}
    );
  }

  async selectVariant(product: Product) {
    if (product.variants && product.variants.length == 1) {
      this.posService.checkVariant(product.variants[0]);
      this.singleVariantHandler(product.variants[0]);
    } else {
      const popover = await this.popoverController.create({
        component: SelectVariantModalComponent,
        translucent: true,
        componentProps: {
          product
        },
        cssClass: 'select-variant',
        animated: true,
        showBackdrop: true,
        backdropDismiss: false
      });
      popover.onDidDismiss().then(data => {
        if (data?.data?.variant) {
          const variant = data.data.variant;
          this.singleVariantHandler(variant);
        }
      });
      return await popover.present();
    }
  }

  private singleVariantHandler(variant: Variant) {
    const _selectedVariants = this.posQuery.getValue().selectedVariants;
    const _selected = _selectedVariants.some(v => v.id == variant.id);

    if (_selected) {
      this.addToOrderLines(variant);
    } else {
      const idx = this.lines.value.findIndex(l => l.variant_id == variant.id);
      if (idx >= 0) {
        this.lines.removeAt(idx);
        this.countLines();
      }
    }
  }

  onRemoveLine(line: OrderLine) {
    const variant = this.posQuery.getValue().selectedVariants.find(v => v.id == line.variant_id);
    this.posService.checkVariant(variant);
  }
}

