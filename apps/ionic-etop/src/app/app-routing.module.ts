import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./pages/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: 'survey',
    loadChildren: () =>
      import('./pages/survey/survey.module').then(m => m.SurveyModule)
  },
  {
    path: 'create-shop',
    loadChildren: () =>
      import('./pages/create-shop/create-shop.module').then(m => m.CreateShopModule)
  },
  {
    path: 'invitation',
    loadChildren: () =>
      import('./pages/invitation/invitation.module').then(m => m.InvitationModule)
  },
  {
    path: 'i/:phone',
    loadChildren: () =>
      import('./pages/invitation-by-phone/invitation-by-phone.module').then(m => m.InvitationByPhoneModule)
  },
  {
    path: 's',
    loadChildren: () =>
      import('apps/ionic-etop/src/app/etop-app/etop-app.module').then(m => m.EtopAppModule)
  },
  {
    path: '**',
    redirectTo: 's/-1/orders'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
