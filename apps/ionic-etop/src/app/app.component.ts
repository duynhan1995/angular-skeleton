import {Component, NgZone, ElementRef, ViewChild, OnInit} from '@angular/core';
import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar as NgxStatusBar } from '@ionic-native/status-bar/ngx';
import {LocationService} from '@etop/state/location';
import { AuthenticateService } from '@etop/core';
import { Router } from '@angular/router';
import {CmsService} from "apps/core/src/services/cms.service";
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { LoadingViewService } from './components/loading-view/loading-view.service';
import { ConnectionService } from '@etop/features';

const { StatusBar, App } = Plugins;

@Component({
  selector: 'etop-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: NgxStatusBar,
    private authenticateService: AuthenticateService,
    private router: Router,
    private zone: NgZone,
    private locationService: LocationService,
    private cms: CmsService,
    private commonUsecase: CommonUsecase,
    private loadingView: LoadingViewService
  ) {
  }

  ngOnInit() {
    this.initializeApp();
  }

  initializeApp() {
    this.loadingView.showLoadingView();
    this.locationService.initLocations().then();
    this.cms.initBanners().then();
    this.platform.ready().then(() => {
      if (this.platform.is('capacitor')) {
        if (this.platform.is('ios')) {
          StatusBar.setStyle({
            style: StatusBarStyle.Light
          });
        } else {
          StatusBar.setStyle({
            style: StatusBarStyle.Dark
          });
        }
      } else {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }
      this.locationService.initLocations().then();
    });
    App.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        const slug = data.url.split('etop.vn').pop();
        this.router.navigateByUrl(slug);
      });
    });
    this.commonUsecase.checkAuthorization(true)
      .then(() => {
        setTimeout(() => this.loadingView.hideLoadingView(), 2000)
      });
  }
}
