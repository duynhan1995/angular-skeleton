import { Injectable } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { Subject } from 'rxjs';
import { CustomerApi } from '@etop/api';
import { requireMethodRoles } from '@etop/core';
import {Customer, FilterOperator, Filters, OrderCustomer} from '@etop/models';

@Injectable()
export class CustomerService {
  customerAddressChanged$ = new Subject();
  independent_customer = new Customer({});

  constructor(
    private customerApi: CustomerApi,
    private util: UtilService
  ) {}

  async createCustomer(data: Customer | OrderCustomer | any) {
    return await this.customerApi.createCustomer(data);
  }

  async createCustomerAddress(data) {
    return await this.customerApi.createCustomerAddress(data);
  }

  async getCustomerAddresses(customer_id) {
    return await this.customerApi.getCustomerAddresses(customer_id);
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: [],
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  async getCustomers(start?: number, perpage?: number, filters?: Array<any>, getAll = true) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };
    return this.customerApi.getCustomers({ paging, filters }, getAll)
      .then(res => {
        this.independent_customer = res.customers.find(
          c => c.type == 'independent' || c.type == 'anonymous'
        );
        return res.customers.map((c, index) => this.mapCustomerInfo(c, index))
      });
  }

  async getCustomer(id) {
    return await this.customerApi.getCustomer(id);
  }

  async updateCustomer(data) {
    delete data.code;
    return await this.customerApi.updateCustomer(data);
  }

  async updateCustomerAddress(data) {
    return await this.customerApi.updateCustomerAddress(data);
  }

  async deleteCustomer(id) {
    return await this.customerApi.deleteCustomer(id);
  }

  async deleteCustomerAddress(id) {
    return await this.customerApi.deleteCustomerAddress(id);
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: [],
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  async checkExistedCustomer(filterObj: any): Promise<any> {
    try {
      const filters: Filters = [];
      for (let key in filterObj) {
        filters.push({
          name: key,
          op: FilterOperator.contains,
          value: filterObj[key]
        });
      }
      const res: any = await this.customerApi.getCustomers(
        { paging: {offset: 0, limit: 1000}, filters },
        false
      );
      return res.customers;
    } catch(e) {
      debug.error('ERROR in checking Existed Customer', e);
      return [];
    }
  }

  mapCustomerInfo(customer: Customer, index) {
    return {
      ...customer,
      index,
      info: `<strong>${customer.full_name}${customer.type != 'independent' && ' - ' + customer.phone || ''}</strong>`,
      detail: `<strong>${customer.full_name}${(customer.type != 'independent' && customer.phone) && ' - ' + customer.phone || ''}</strong>`,
      search_text: this.util.makeSearchText(`${customer.full_name}${customer.phone}`)
    }
  }

}
