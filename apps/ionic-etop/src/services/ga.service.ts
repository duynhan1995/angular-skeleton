import { Injectable } from '@angular/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

@Injectable({
  providedIn: 'root'
})
export class GaService {
  constructor(
    private firebaseAnalytics: FirebaseAnalytics,
  ) {}

  setUserId(id) {
    this.firebaseAnalytics.setUserId(id);
  }

  logEvent(event, parameter) {
    this.firebaseAnalytics
      .logEvent(event, parameter)
      .then((res: any) => debug.log('firebaseAnalytics', res))
      .catch((error: any) => debug.error('firebaseAnalytics Error', error));
  }

  setUserProperty(name, value) {
    this.firebaseAnalytics.setUserProperty(name, value);
  }

  setCurrentScreen(view) {
    this.firebaseAnalytics.setCurrentScreen(view);
  }

}
