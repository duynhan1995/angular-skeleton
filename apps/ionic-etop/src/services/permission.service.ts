import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@Injectable({
  providedIn: 'root'
})
export class PermisionService {
  constructor(
    private openNativeSettings: OpenNativeSettings,
    private alertController: AlertController
  ) {}

  async open({message, setting}) {
    const alert = await this.alertController.create({
      header: 'Yêu cầu quyền truy cập',
      message,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium',
          handler: blah => {
            console.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Cấp quyền',
          handler: () => {
            this.openNativeSettings.open(setting);
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

}
