module.exports = {
  name: 'e-telecom',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/e-telecom/',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
