import {Injectable} from '@angular/core';
import {DashboardApi} from "@etop/api/shop/dashboard.api";

@Injectable({
    providedIn: 'root'
  })
export class DashboardService {

  constructor(
      private dashboardApi: DashboardApi
  ) {}

  async summaryEtelecom({ date_from, date_to }) {
    return await this.dashboardApi.summaryEtelecom({ date_from, date_to });
  }
}
