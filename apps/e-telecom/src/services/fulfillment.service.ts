import { Injectable } from '@angular/core';
import {Fulfillment} from 'libs/models/Fulfillment';
import {FulfillmentApi} from '@etop/api';
import {Filters} from "@etop/models";
import { Subject } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ConnectionStore } from '@etop/features/connection';
import moment from 'moment';
import List from 'identical-list';


interface ForceItem {
  id: string;
  type: string;
  expireTime: number;
}

@Injectable()
export class FulfillmentService {

  constructor(
    private fulfillmentApi: FulfillmentApi,
    private connectionStore: ConnectionStore,
  ) {
  }
  
  forceList: List<ForceItem>;
  onViewReturningFfms$ = new Subject();

  private __secretCode = 'fulfillment-data';


  trackingLink(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return null;
    }
    return fulfillment.shipping_shared_link;
  }

  async getShipnowFulfillments(start?: number, perpage?: number, filters?: Filters) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };

    return this.fulfillmentApi.getShipnowFulfillments({paging, filters})
      .then(res => {
        res.shipnow_fulfillments = res.shipnow_fulfillments.map(f => Fulfillment.fulfillmentMap(f, []));
        return res;
      });
  }

  getShipnowFulfillment(id, token?) {
    return this.fulfillmentApi.getShipnowFulfillment(id, token)
      .then(res => Fulfillment.fulfillmentMap(res, []));
  }
  
  async getFulfillments(start?: number, perpage?: number, filters?: Filters) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.fulfillmentApi.getFulfillments({ paging, filters })
            .then(res => {
              res.fulfillments = res.fulfillments.map(f => Fulfillment.fulfillmentMap(f, connections));
              resolve(res);
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  async cancelShipnowFulfillment(ffmId, reason) {
    await this.fulfillmentApi.cancelShipnowFulfillment(ffmId, reason);
  }

  async cancelShipmentFulfillment(ffmId, reason) {
    await this.fulfillmentApi.cancelShipmentFulfillment(ffmId, reason);
  }

  async getFulfillmentHistory(ffm) {
    return this.fulfillmentApi.getFulfillmentHistory(ffm);
  }

  getFulfillment(id, token?): Promise<Fulfillment> {
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.fulfillmentApi.getFulfillment(id, token)
            .then(ffm => {
              resolve(Fulfillment.fulfillmentMap(ffm, connections));
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  canSendSupportRequest(fulfillment: Fulfillment) {
    if (!fulfillment || !fulfillment.from_topship) return false;
    const { shipping_state } = fulfillment;
    return (
      shipping_state === 'delivering' ||
      shipping_state === 'holding' ||
      shipping_state === 'picking' ||
      shipping_state === 'created' ||
      shipping_state === 'confirmed' ||
      shipping_state === 'undeliverable' ||
      shipping_state === 'returning' ||
      shipping_state === 'processing'
    );
  }

  checkStatusForceItem(shipping_code: string) {
    const item = this.forceList.get(shipping_code);
    if (!item) {
      return false;
    }
    const now = moment.now();
    return item.expireTime > now;
  }

  addForceItem(shipping_code, type) {
    const expireTime = moment(moment.now())
      .add(6, 'hours')
      .valueOf();
    this.forceList.add({
      id: shipping_code,
      type,
      expireTime
    });
    this.updateLocalForceList();
  }

  updateLocalForceList() {
    const data = { forceList: this.forceList.asArray() };
    localStorage.setItem(this.__secretCode, JSON.stringify(data));
  }
}
