import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ETelecomComponent} from './e-telecom.component';
import {ETelecomGuard} from './e-telecom.guard';
import {eTelecomRoutes} from './e-telecom.route';

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [ETelecomGuard],
    resolve: {
      account: ETelecomGuard
    },
    component: ETelecomComponent,
    children: eTelecomRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ETelecomRoutingModule {
}
