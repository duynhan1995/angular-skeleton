import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ETelecomRoutingModule} from './e-telecom-routing.module';
import {ETelecomComponent} from './e-telecom.component';
import {CoreModule} from 'apps/core/src/core.module';
import {ETelecomGuard} from './e-telecom.guard';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FulfillmentService} from "../../services/fulfillment.service";
import {ExtensionService} from '@etop/state/shop/extension';
import {FromAddressModalModule} from "@etop/shared/components/from-address-modal/from-address-modal.module";
import {CustomerService} from '../../services/customer.service';


const pages = [];

@NgModule({
  declarations: [ETelecomComponent, ...pages],
  providers: [
    ETelecomGuard,
    FulfillmentService,
    ExtensionService,
    CustomerService,
  ],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    ETelecomRoutingModule,
    FromAddressModalModule
  ]
})
export class ETelecomModule {
}
