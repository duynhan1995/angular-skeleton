import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { FulfillmentShipnowRoutesComponent } from './fulfillment-shipnow-routes.component';

@NgModule({
  declarations: [
    FulfillmentShipnowRoutesComponent
  ],
  exports: [
    FulfillmentShipnowRoutesComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule
  ]
})
export class FulfillmentShipnowRoutesModule { }
