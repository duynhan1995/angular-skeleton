import { Component, OnInit, ViewChild } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { CustomerListComponent } from './customer-list/customer-list.component';

@Component({
  selector: 'etelecom-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent extends PageBaseComponent implements OnInit {
  @ViewChild('customerList', {static: true}) customerList: CustomerListComponent;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
