import { Component, OnInit, Input } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { CustomerAddress } from '@etop/models';

@Component({
  selector: 'shop-delete-customer-modal',
  templateUrl: './delete-customer-modal.component.html',
  styleUrls: ['./delete-customer-modal.component.scss']
})
export class DeleteCustomerModalComponent implements OnInit {
  @Input() address;
  constructor(
    private modalDismiss: ModalAction,
    private locationCompact: LocationCompactPipe
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalDismiss.close(false);
  }

  addressDisplay(address: CustomerAddress) {
    if (!address) {
      return '';
    }
    const address1 = this.locationCompact.transform(address.address1);
    const ward = this.locationCompact.transform(address.ward);
    const district = this.locationCompact.transform(address.district);
    const province = this.locationCompact.transform(address.province);

    return `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
  }

  confirmDelete() {
    this.modalDismiss.dismiss(true)
  }
}
