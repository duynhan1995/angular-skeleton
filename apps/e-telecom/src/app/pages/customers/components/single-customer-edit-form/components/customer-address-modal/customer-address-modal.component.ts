import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { CustomerAddress } from '@etop/models';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { LocationQuery } from "@etop/state/location";
import { combineLatest } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { FormBuilder } from "@angular/forms";
import { BaseComponent } from "@etop/core";
import { CustomerService } from 'apps/e-telecom/src/services/customer.service';

@Component({
  selector: 'etelecom-customer-address-modal',
  templateUrl: './customer-address-modal.component.html',
  styleUrls: ['./customer-address-modal.component.scss']
})
export class CustomerAddressModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() address = new CustomerAddress({});
  @Input() title: string;
  @Input() customer_id: string;
  @Input() confirmBtnClass = 'btn-primary';

  loading = false;
  formInitializing = true;

  customerAddressForm = this.fb.group({
    full_name: '',
    phone: '',
    province_code: '',
    district_code: '',
    ward_code: '',
    address1: ''
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.customerAddressForm.controls['province_code'].valueChanges]).pipe(
      map(([districts, provinceCode]) => {
        if (!provinceCode) { return []; }
        return districts?.filter(dist => dist.province_code == provinceCode);
      })
    );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.customerAddressForm.controls['district_code'].valueChanges]).pipe(
      map(([wards, districtCode]) => {
        if (!districtCode) { return []; }
        return wards?.filter(ward => ward.district_code == districtCode);
      })
    );

  private static validateAddress(data) {
    const { full_name, phone, province_code, district_code, ward_code, address1 } = data;
    if (!full_name) {
      toastr.error('Chưa nhập tên!');
      return false;
    }
    if (!phone) {
      toastr.error('Chưa nhập số điện thoại!');
      return false;
    }
    if (!province_code) {
      toastr.error('Chưa chọn tỉnh thành!');
      return false;
    }
    if (!district_code) {
      toastr.error('Chưa chọn quận huyện!');
      return false;
    }
    if (!ward_code) {
      toastr.error('Chưa chọn phường xã!');
      return false;
    }
    if (!address1) {
      toastr.error('Chưa nhập địa chỉ cụ thể!');
      return false;
    }
    return true;
  }

  constructor(
    private fb: FormBuilder,
    private modalDismiss: ModalAction,
    private customerService: CustomerService,
    private util: UtilService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  ngOnInit() {
    this.customerAddressForm.controls['province_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.formInitializing) {
          this.customerAddressForm.patchValue({
            districtCode: '',
            wardCode: ''
          });
        }
      });

    this.customerAddressForm.controls['district_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.formInitializing) {
          this.customerAddressForm.patchValue({
            wardCode: ''
          });
        }
      });

    if (!this.address) {
      return this.formInitializing = false;
    }

  }

  ngAfterViewInit() {
    this.formInitializing = true;

    const { full_name, phone, province_code, district_code, ward_code, address1 } = this.address;
    this.customerAddressForm.patchValue({
      full_name,
      phone,
      province_code,
      district_code,
      ward_code,
      address1
    });

    this.formInitializing = false;
  }

  getTitle() {
    if (this.title == 'create') {
      return 'Thêm địa chỉ mới';
    } else {
      return 'Chỉnh sửa địa chỉ';
    }
  }

  async createAddress() {
    const data = this.customerAddressForm.getRawValue();
    try {
      const customer = await this.customerService.getCustomer(this.customer_id);
      if (customer) {
        const res = await this.customerService.createCustomerAddress({ ...data, customer_id: this.customer_id });
        toastr.success('Tạo địa chỉ mới thành công!');
        return res;
      }
    } catch (e) {
      if (e.code == 'not_found') {
          return data;
      } else {
        debug.error('ERROR in Creating New Address', e);
        throw e;
      }
    }
  }

  async updateAddress() {
    try {
      const data = this.customerAddressForm.getRawValue();
      const res = await this.customerService.updateCustomerAddress({ ...data, id: this.address.id });
      toastr.success('Cập nhật địa chỉ thành công!');
      return res;
    } catch (e) {
      debug.error('ERROR in Updating New Address', e);
      throw e;
    }
  }

  async confirm() {
    this.loading = true;
    try {
      let data = this.customerAddressForm.getRawValue();
      data = this.util.trimFields(data, ['full_name', 'phone', 'address1']);
      const validAddress = CustomerAddressModalComponent.validateAddress(data);
      if (!validAddress) {
        return this.loading = false;
      }
      if (!this.customer_id && !this.address.id) {
        this.modalDismiss.dismiss(data);
        return this.loading = false;
      }

      let res: any;
      if (this.title == 'create') {
        res = await this.createAddress();
      } else {
        res = await this.updateAddress();
      }
      this.modalDismiss.dismiss(res);
    } catch (e) {
      toastr.error(
        `${this.title == 'create' ? 'Tạo' : 'Cập nhật'} địa chỉ không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }

  numberOnly(event) {
    const num = Number(event.key);
    if (Number.isNaN(num)) {
      event.preventDefault();
      return false;
    }
  }

  closeModal() {
    this.modalDismiss.close(false);
  }
}
