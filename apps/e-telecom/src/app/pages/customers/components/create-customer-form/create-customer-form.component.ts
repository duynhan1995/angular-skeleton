import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Customer } from '@etop/models';
import * as moment from 'moment';
import { BaseComponent } from '@etop/core';
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import { CustomerService } from 'apps/e-telecom/src/services/customer.service';

@Component({
  selector: 'etelecom-create-customer-form',
  templateUrl: './create-customer-form.component.html',
  styleUrls: ['./create-customer-form.component.scss']
})
export class CreateCustomerFormComponent extends BaseComponent implements OnInit {
  customer = new Customer({});
  @Output() createCustomer = new EventEmitter();

  genders = [
    { name: 'Nam', value: 'male' }, 
    { name: 'Nữ', value: 'female' },
    { name: 'Khác', value: 'other' }
  ];

  customerAddressForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
    wardCode: '',
    address1: ''
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.customerAddressForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.customerAddressForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  loading = false;

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private locationQuery: LocationQuery
  ) {
    super();
  }

  ngOnInit() {
    this.customerAddressForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.customerAddressForm.patchValue({
          districtCode: '',
          wardCode: ''
        });
      });

    this.customerAddressForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.customerAddressForm.patchValue({
          wardCode: ''
        });
      });
  }

  async create() {
    this.loading = true;
    try {
      if (this.customer.birthday) {
        this.customer.birthday = moment(this.customer.birthday)
          .format('YYYY-MM-DD');
      }
      if (!this.customer.full_name) {
        this.loading = false;
        return toastr.error('Vui lòng nhập tên!');
      }
      if (!this.customer.phone) {
        this.loading = false;
        return toastr.error('Vui lòng nhập số điện thoại!');
      }
      const res = await this.customerService.createCustomer(this.customer);

      const {provinceCode, districtCode, wardCode} = this.customerAddressForm.getRawValue();

      const customerAddress: any = {
        customer_id: res.id,
        province_code: provinceCode,
        district_code: districtCode,
        ward_code: wardCode,
        address1: this.customerAddressForm.getRawValue().address1,
        email: res.email,
        phone: res.phone,
        full_name: res.full_name
      };
      const {province_code, district_code, ward_code, address1} = customerAddress;
      if (province_code && district_code && ward_code && address1) {
        this.customerService.createCustomerAddress(customerAddress).then();
      }
      toastr.success('Tạo khách hàng mới thành công');
      this.createCustomer.emit();
      this.customer = new Customer({});
    }
    catch (e) {
      toastr.error('Tạo khách hàng thất bại!', e.message);
    }
    this.loading = false;
  }

}
