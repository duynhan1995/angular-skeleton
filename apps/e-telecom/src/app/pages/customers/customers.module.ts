import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'apps/shared/src/shared.module';
import {AuthenticateModule} from '@etop/core';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {SideSliderModule} from '@etop/shared/components/side-slider/side-slider.module';
import {EtopCommonModule, EtopPipesModule, MaterialModule, EmptyPageModule} from '@etop/shared';
import {RouterModule, Routes} from '@angular/router';
import {CustomersComponent} from './customers.component';
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomerRowComponent} from './components/customer-row/customer-row.component';
import {SingleCustomerEditFormModule} from './components/single-customer-edit-form/single-customer-edit.module';
import {CreateCustomerFormModule} from './components/create-customer-form/create-customer-form.module';
import {MultipleCustomerEditFormModule} from './components/multiple-customer-edit-form/multiple-customer-edit-form.module';
import {TradersControllerService} from './traders-controller.service';
import {OrderService} from 'apps/shop/src/services/order.service';

const routes: Routes = [
  {
    path: '',
    component: CustomersComponent
  }
]

@NgModule({
  declarations: [
    CustomersComponent,
    CustomerListComponent,
    CustomerRowComponent
  ],
  imports: [
    SingleCustomerEditFormModule,
    CreateCustomerFormModule,
    MultipleCustomerEditFormModule,
    CommonModule,
    SharedModule,
    AuthenticateModule,
    DropdownActionsModule,
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    MaterialModule,
    RouterModule.forChild(routes),
    EmptyPageModule
  ],
  exports: [
    CustomersComponent
  ],
  providers: [TradersControllerService, OrderService]
})
export class CustomersModule {
}
