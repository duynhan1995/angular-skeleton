import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';
import { CreateContactFormComponent } from './create-contact-form.component';


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule],
  exports: [CreateContactFormComponent],
  entryComponents: [],
  declarations: [CreateContactFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateContactFormModule {}
