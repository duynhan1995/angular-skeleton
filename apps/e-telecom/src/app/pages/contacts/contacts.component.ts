import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactService } from '@etop/state/shop/contact';
import { ContactsListComponent } from './component/contacts-list/contacts-list.component';

@Component({
  selector: 'etelecom-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  @ViewChild('contactsList', {static: true}) contactsList: ContactsListComponent;
  constructor(
    private contactService: ContactService,
  ) { }

  async ngOnInit(): Promise<void> {
    await this.contactService.getContacts();
  }

}
