import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../../../../../../core/src/services/cms.service';
import { ModalAction } from '../../../../../../../core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'etelecom-paycard-turtorial-modal',
  templateUrl: './paycard-turtorial-modal.component.html',
  styleUrls: ['./paycard-turtorial-modal.component.scss']
})
export class PaycardTurtorialModalComponent implements OnInit {
  banner ='';

  constructor(
    private cms: CmsService,
    private modalAction: ModalAction,
  ) { }

  ngOnInit(): void {
    this.getPayCardTurtorial();
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async getPayCardTurtorial() {
    await this.cms.initBanners();
    const result = this.cms.getPayCardTutorial();
    if (result) {
      this.banner = result;
    }
  }
}
