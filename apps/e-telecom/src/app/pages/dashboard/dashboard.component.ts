import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RelationshipQuery } from '@etop/state/relationship/relationship.query';
import { RelationshipService } from '@etop/state/relationship/relationship.service';
import { DashboardService } from 'apps/e-telecom/src/services/dashboard.service';
import { ShopService } from '@etop/features';
import { PaycardTurtorialModalComponent } from './components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import { ModalController } from '../../../../../core/src/components/modal-controller/modal-controller.service';
import * as moment from 'moment';
import {ExtensionApi} from "@etop/api/shop/extension.api";

@Component({
  selector: 'etelecom-dashboard-pos',
  template: `
    <etelecom-desktop-dashboard class="hide-768" [dashboard]="dashboard" [loading]="loading" (filter)="getDashboard($event)" >
    </etelecom-desktop-dashboard>
    <etelecom-mobile-dashboard class="show-768" [dashboard]="dashboard" [loading]="loading" (filter)="getDashboard($event)" >
    </etelecom-mobile-dashboard>`,
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  private modal: any;
  dashboard: any;
  loading = true;
  extensions: any = [];
  constructor(
    private dashboardService: DashboardService,
    private extensionApi: ExtensionApi,
    private relationshopService: RelationshipService,
    private relationshopQuery: RelationshipQuery,
    private changeDef: ChangeDetectorRef,
    private shopService: ShopService,
    private modalController: ModalController
  ) {}

  async ngOnInit() {
    this.loading = true;
    let relationships: any = this.relationshopQuery.getAll();
    if (!relationships?.length) {
      relationships = await this.relationshopService.getRelationships();
    }
    this.loading = false;
    const ranges = {
      startDate: moment().startOf('isoWeek'),
      endDate: moment()
    }
    this.getDashboard(ranges);
  }

  async getExtensions() {
    try {
      let extensions = await this.extensionApi.getExtensions();
      extensions = extensions.map(extension => {
        return {
          ...extension,
          user_name: this.relationshopQuery.getRelationshipNameById(extension.user_id)
        }
      })
      return extensions
    } catch(e) {
      debug.log('ERROR in getExtensions', e)
    }
  }

  async getDashboard(ranges) {
    try {

      this.dashboard = await this.dashboardService.summaryEtelecom({
        date_from: ranges.startDate.format('yy-MM-DD'),
        date_to: ranges.endDate.add(1, 'days').format('yy-MM-DD')
      })
      ranges.endDate.subtract(1, 'days').format('yy-MM-DD')
      if (this.extensions) {
        this.extensions = await this.getExtensions();
      }
      this.dashboard.staff = this.dashboard.staff.map(d => {
        return {
          ...d,
          full_name: this.extensions?.find(e => e.id == d.extension_id).user_name || 'Không xác định'
        }
      })
      this.changeDef.detectChanges();
    } catch(e) {
      debug.log('ERROR in getDashboard', e)
    }
  }

  openPayCardTurtorialModal() {
    if(this.modal) {
      return;
    }

    this.modal = this.modalController.create({
      component: PaycardTurtorialModalComponent,
    });

    this.modal.show().then();
    this.modal.onDismiss().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

}
