import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { HeaderControllerService } from '../../../../../../core/src/components/header/header-controller.service';
import { UtilService } from '../../../../../../core/src/services/util.service';
import { CmsService } from '../../../../../../core/src/services/cms.service';
import { PaycardTurtorialModalComponent } from '../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service"
import { Moment } from 'moment';

@Component({
  selector: 'etelecom-desktop-dashboard',
  templateUrl: './desktop-dashboard.component.html',
  styleUrls: ['./desktop-dashboard.component.scss']
})
export class DesktopDashboardComponent implements OnInit, OnDestroy {
  @Input() dashboard: any;
  @Input() loading = true;
  @Input() telecomBalance: number;
  @Output() filter = new EventEmitter();
  private modal: any;
  guideline = false;
  selected: { startDate: Moment; endDate: Moment };
  banner = '';
  alwaysShowCalendars: boolean;

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private cms: CmsService,
    private modalController: ModalController
  ) {
    this.alwaysShowCalendars = true;
  }

  ngOnInit(): void {
    this.headerController.setActions([
      {
        title: 'Đăng ký đầu số Hotline',
        cssClass: 'btn btn-outline btn-primary',
        mobileHidden: true,
        onClick: () => this.onNavigateRegisterHotline(),
      },
      {
        title: 'Nạp tiền',
        cssClass: 'btn btn-primary',
        onClick: () => this.openPayCardTurtorialModal(),
      }
    ]);
  }

  get isMobileVersion() {
    return this.util.isMobile;
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
      this.filter.emit(event)
    }
  }

  onNavigateRegisterHotline(){
    window.open("https://etelecom.vn/dang-ky-dau-so-hotline/", "_blank");
  }

  onNavigateRecharge(){
    window.open("https://etelecom.vn/nap-tien/", "_blank");
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  openPayCardTurtorialModal() {
    if(this.modal) {
      return;
    }
    const cssClass = this.isMobileVersion &&
      'modal-xxl' || 'modal-md';

    this.modal = this.modalController.create({
      component: PaycardTurtorialModalComponent,
      cssClass,
    });
    this.modal.show().then();
    this.modal.onDismiss().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

}
