import { Component, Input, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { Relationship } from '@etop/models';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { CreateExtensionModalComponent } from '../create-hotline-modal/create-hotline-modal.component';
import { ExtensionService } from '@etop/state/shop/extension';
import { UpdateStaffModalComponent } from '../update-staff-modal/update-staff-modal.component';
import { ExtensionQuery, ExtensionStore } from '@etop/state/shop/extension';
import { HotlineQuery, HotlineStore } from '@etop/state/shop/hotline';
import { AuthorizationService } from "@etop/state/authorization";


@Component({
  selector: '[etelecom-staff-management-row]',
  templateUrl: './staff-management-row.component.html',
  styleUrls: ['./staff-management-row.component.scss']
})
export class StaffManagementRowComponent implements OnInit {
  @Input() staff = new Relationship();

  currentHotline;
  currentExtension;

  loading = true;
  removing = false;

  dropdownActions = [];

  private modal: any;

  constructor(
    private authorizationService: AuthorizationService,
    private authStore: AuthenticateStore,
    private dialog: DialogControllerService,
    private modalController: ModalController,
    private extensionQuery: ExtensionQuery,
    private hotlineQuery: HotlineQuery,
    private extensionStore: ExtensionStore,
  ) { }

  get isOwner() {
    return this.staff.roles.indexOf('owner') != -1;
  }

  async ngOnInit() {
    this.loading = true;
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa',
        cssClass: this.isCurrentUser && 'cursor-not-allowed',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:update', 'relationship/permission:update'],
        onClick: () => this.updateStaff()
      },
      {
        title: 'Xoá khỏi shop',
        cssClass: this.isCurrentUser && 'cursor-not-allowed' || 'text-danger',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:remove'],
        onClick: () => this.confirmRemoveStaff()
      }
    ];
    this.loading = false;
  }

  get isCurrentUser() {
    return this.staff.user_id == this.authStore.snapshot.user.id;
  }

  updateStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: UpdateStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        staff: {
          ...this.staff,
          p_data: {
            ...this.staff,
          }
        }
      }
    });
    this.modal.show();
    this.modal.onDismiss().then(updated => {
      this.modal = null;
      if (updated) {
        this.authorizationService.updateRelationship();
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async removeStaff() {
    this.removing = true;
    try {
      await this.authorizationService.removeUserFromAccount(this.staff.user_id);
      this.authorizationService.removeRelationship();
      toastr.success('Xoá nhân viên thành công.')
    } catch (e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Staff', e);
    }
    this.removing = false;
  }

  confirmRemoveStaff() {
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá nhân viên`,
      body: `
        <div>Bạn thực sự muốn xóa nhân viên "<strong>${this.staff?.full_name}</strong>"?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmCss: 'btn-danger text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removeStaff();
        modal.close().then();
      }
    });
    modal.show().then();
  }

  //Check employee have extension or not.
  hasUserExtension(): boolean {
    const extensions = this.extensionQuery.getAll();
    const hotlines = this.hotlineQuery.getAll();
    if (extensions?.length) {
      this.currentExtension = extensions.find(ex => ex.user_id == this.staff.user_id);
      this.currentHotline = hotlines.find(hotline => hotline.id == this.currentExtension?.hotline_id);
    }
    return !!this.currentExtension?.extension_number;
  }

  async createExtension() {
    const hotlines = this.hotlineQuery.getAll();
    this.modal = this.modalController.create({
      component: CreateExtensionModalComponent,
      componentProps: {
        hotlines: hotlines,
        staff: this.staff
      },
      showBackdrop: true,
      cssClass: 'modal-md'
    })
    this.modal.show();
    this.modal.onDismiss().then(extension => {
      this.extensionStore.add(extension);
      this.currentExtension = extension;
    })
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  get roleDisplay() {
    if (this.staff.roles.includes('owner'))
      return 'Chủ shop';
    if (this.staff.roles.includes('telecom_customerservice') && this.staff.roles.includes('salesman')) {
      if (this.staff.roles.includes('staff_management') && this.staff.roles.length == 3)
        return 'Quản lý';
      if (this.staff.roles.length == 2)
        return 'Bán hàng';
    }
    return 'Khác'
  }

}
