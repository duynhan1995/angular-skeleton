import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { FilterOperator, Filters } from '@etop/models';
import { RelationshipService } from '@etop/state/relationship';
import { HotlineService } from '@etop/state/shop/hotline';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { ExtensionService } from '@etop/state/shop/extension';
import { AddStaffModalComponent } from './components/add-staff-modal/add-staff-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AuthorizationApi } from '@etop/api';
import { InvitationStore } from '@etop/state/invitation/invitation.store';
import { AuthorizationService } from '@etop/state/authorization/authorization.service';

@Component({
  selector: 'etelecom-staff-management',
  template: `
      <etelecom-desktop-staff-management class='hide-768'></etelecom-desktop-staff-management>
      <etelecom-mobile-staff-management class='show-768'></etelecom-mobile-staff-management>`,
  styleUrls: ['./staff-management.component.scss']
})
export class StaffManagementComponent extends BaseComponent implements OnInit {
  private modal: any;
  constructor(
    private invitationService: InvitationService,
    private relationshipService: RelationshipService,
    private hotlineService: HotlineService,
    private extensionService: ExtensionService,
    private cdr: ChangeDetectorRef,
    private modalController: ModalController,
    private authorizationService: AuthorizationService,
    private invitationStore: InvitationStore,
  ) {
    super();
  }

  async ngOnInit() {
    await this.relationshipService.getRelationships();
    await this.hotlineService.getHotlines();
    await this.extensionService.getExtensions();
    await this.getShopInvitations();
    this.cdr.detectChanges();
  }

  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const filters : Filters = [
        {
          name: "status",
          op: FilterOperator.eq,
          value: "Z"
        },
        {
          name: "expires_at",
          op: FilterOperator.gt,
          value: dateString
        }
      ]
      this.invitationService.setFilters(filters);
      this.invitationService.getShopInvitations();
    } catch(e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }

  addStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show();
    this.modal.onDismiss().then(invitation => {
      this.modal = null;
      if (invitation) {
        invitation = this.authorizationService.invitationMap(invitation);
        this.invitationStore.add(invitation);
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }
}
