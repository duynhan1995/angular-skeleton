import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { FilterOperator, Invitation } from '@etop/models';
import { AuthorizationService } from '@etop/state/authorization/authorization.service';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';

@Component({
  selector: 'etelecom-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {
  phone;
  userInvitation: Invitation;
  refusing = false;
  accepting = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authorizationService: AuthorizationService,
    private auth: AuthenticateStore,
    private zone: NgZone,
    private navCtrl: NavController,
    private commonUsecase: CommonUsecase,
    private util: UtilService,
  ) { }

  async ngOnInit() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.phone = id.substring(1);
    if (this.compareAccount()) {
      await this.getUserInvitations()
    }

  }

  get user() {
    return this.auth.snapshot.user;
  }

  compareAccount() {
    return this.auth.snapshot.user?.phone == this.phone;
  }

  roleMap(role) {
    return AuthorizationApi.roleMap(role);
  }

  async getUserInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const userInvitations = await this.authorizationService.getUserInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
      this.userInvitation = userInvitations.find(
        user => user.phone == this.phone
      );
    } catch (e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }

  async acceptInvitation(invitation) {
    this.accepting = true;
    try {
      await this.authorizationService.acceptInvitation(invitation.token);
      await this.commonUsecase.updateSessionInfo(true);this.zone.run(async () => {
        await this.navCtrl.navigateForward(`s/${this.util.getSlug()}/settings/accounts`, {
          animated: false
        });
      });
    } catch (e) {
      debug.error('ERROR in Accepting Invitation', e);
    }
    this.accepting = false;
  }

  async rejectInvitation(invitation) {
    this.refusing = true;
    try {
      await this.authorizationService.rejectInvitation(invitation.token);
      await this.goShop();
    } catch (e) {
      debug.error('ERROR in Rejecting Invitation', e);
    }
    this.refusing = false;
  }

  async loginAnotherAccount() {
      this.auth.clear();
      this.zone.run(async () => {
        await this.navCtrl.navigateForward(`/register?invitation=${this.phone}&type=phone`, {
          animated: false
        });
      });
  }

  async goShop() {
    this.zone.run(async () => {
      await this.navCtrl.navigateForward(`s/${this.util.getSlug()}/settings/shop`, {
        animated: false
      });
    });
  }

  async login() {
    this.zone.run(async () => {
      await this.navCtrl.navigateForward(`/register?invitation=${this.phone}&type=phone`, {
        animated: false
      });
    });
  }

}
