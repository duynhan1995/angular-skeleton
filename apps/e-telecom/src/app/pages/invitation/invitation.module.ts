import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvitationComponent } from './invitation.component';
import { RouterModule, Routes } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopPipesModule } from '@etop/shared';

const routes: Routes = [
  {
    path: ':id',
    component: InvitationComponent
  }
];

@NgModule({
  declarations: [InvitationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    SharedModule,
    EtopPipesModule,
  ]
})
export class InvitationModule { }
