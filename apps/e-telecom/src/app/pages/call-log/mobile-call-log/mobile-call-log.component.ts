import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {CursorPaging} from '@etop/models';
import {ModalController} from '../../../../../../core/src/components/modal-controller/modal-controller.service';
import {AudioModalComponent} from '../components/audio-modal/audio-modal.component';
import {EtopTableComponent} from '@etop/shared';
import {CallLogQuery, CallLogService} from '@etop/state/shop/call-log';

@Component({
  selector: 'etelecom-mobile-call-log',
  templateUrl: './mobile-call-log.component.html',
  styleUrls: ['./mobile-call-log.component.scss']
})
export class MobileCallLogComponent implements OnInit {
  isFirstRequestNewPage = true;
  @ViewChild('callLogTable', {static: true}) callLogTable: EtopTableComponent;
  callLogs$ = this.callLogQuery.selectAll();
  ui$ = this.callLogQuery.select('ui')

  constructor(
    private callLogService: CallLogService,
    private ref: ChangeDetectorRef,
    private modalController: ModalController,
    private callLogQuery: CallLogQuery
  ) {
  }

  async loadPage({perpage, page}) {
    if (this.isFirstRequestNewPage) {
      return this.isFirstRequestNewPage = false;
    }

    let paging: CursorPaging;
    let currentPaging = this.callLogQuery.getValue().ui.paging;
    let currentPage = this.callLogQuery.getValue().ui.currentPage;

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.'
      };
    }

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next
      }
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev
      }
    }

    this.callLogService.setPaging(paging) // paging dùng để gửi request lên server

    this.ref.detectChanges();
    await this.callLogService.getCallLogs(true);
    this.callLogService.setCurrentPage(page); // set số trang hiện tại
    this.ref.detectChanges();
  }

  async ngOnInit() {
    this.callLogQuery.selectLoading().subscribe(loading => this.callLogTable.loading = loading);
  }

  openAudioModal(callLog) {
    let modal = this.modalController.create({
      component: AudioModalComponent,
      showBackdrop: 'static',
      componentProps: {
        callLog
      },
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy lịch sử cuộc gọi phù hợp';
    }
    return 'Cửa hàng của bạn chưa có lịch sử cuộc gọi';
  }

  get emptyResultFilter() {
    const page = this.callLogQuery.getValue().ui.currentPage;
    const callLogList = this.callLogQuery.getAll();
    const filter = this.callLogQuery.getValue().ui.filter;
    return page == 1 && callLogList.length == 0 && filter;
  }
}
