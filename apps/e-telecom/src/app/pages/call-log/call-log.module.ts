import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { EtopCommonModule, EtopPipesModule, MaterialModule, EmptyPageModule } from '@etop/shared';
import { CallLogComponent } from './call-log.component'
import { MobileCallLogComponent } from './mobile-call-log/mobile-call-log.component';
import { DesktopCallLogComponent } from './desktop-call-log/desktop-call-log.component';
import { AudioModalComponent } from './components/audio-modal/audio-modal.component';
import { MatTableModule } from '@angular/material/table';
import { CallLogRowComponent } from './components/call-log-row/call-log-row.component';
import { MobileCallLogRowComponent } from './components/mobile-call-log-row/mobile-call-log-row.component';
import { CallLogService } from '@etop/state/shop/call-log';

const routes: Routes = [
  {
    path: '',
    component: CallLogComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    MaterialModule,
    MatTableModule,
    EtopCommonModule,
    EmptyPageModule
  ],
  exports: [
    MobileCallLogComponent
  ],
  declarations: [
    CallLogComponent,
    MobileCallLogComponent,
    DesktopCallLogComponent,
    AudioModalComponent,
    CallLogRowComponent,
    MobileCallLogRowComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CallLogModule {}
