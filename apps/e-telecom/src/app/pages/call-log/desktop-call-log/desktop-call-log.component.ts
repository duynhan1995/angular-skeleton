import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { CursorPaging } from '@etop/models';
import { CallLogQuery, CallLogService, CallLogStore } from '@etop/state/shop/call-log';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AudioModalComponent } from '../components/audio-modal/audio-modal.component';
import { EtopTableComponent } from '@etop/shared';

@Component({
  selector: 'etelecom-desktop-call-log',
  templateUrl: './desktop-call-log.component.html',
  styleUrls: ['./desktop-call-log.component.scss']
})
export class DesktopCallLogComponent implements OnInit {
  @ViewChild('callLogTable', { static: true }) callLogTable: EtopTableComponent;
  isFirstRequestNewPage = true;

  callLogs$ = this.callLogQuery.selectAll();
  loading$ = this.callLogQuery.selectLoading();
  ui$ = this.callLogQuery.select('ui')
  constructor(
    private callLogService: CallLogService,
    private ref: ChangeDetectorRef,
    private modalController: ModalController,
    private callLogQuery: CallLogQuery,
    private callLogStore: CallLogStore,
  ) { }

  async loadPage({ perpage, page }) {
    if(this.isFirstRequestNewPage) {
      return this.isFirstRequestNewPage = false;
    }

    this.callLogStore.setLoading(true);
    let paging: CursorPaging;
    let currentPaging = this.callLogQuery.getValue().ui.paging;
    let currentPage = this.callLogQuery.getValue().ui.currentPage;

    // perpage change or init page
    if(page == 1) {
      paging = {
        limit: perpage,
        after: '.'
      };
    }

    // navigate next page
    if(page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next
      }
    }

    // navigate previous page
    if(page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev
      }
    }

    this.callLogService.setPaging(paging) // paging dùng để gửi request lên server
    this.ref.detectChanges();
    await this.callLogService.getCallLogs(true);
    this.callLogService.setCurrentPage(page); // set số trang hiện tại
    this.ref.detectChanges();
    this.callLogStore.setLoading(false);
  }

  async ngOnInit() {
  }

  openAudioModal(callLog) {
    let modal = this.modalController.create({
      component: AudioModalComponent,
      showBackdrop: 'static',
      componentProps: {
        callLog
      },
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy lịch sử cuộc gọi phù hợp';
    }
    return 'Cửa hàng của bạn chưa có lịch sử cuộc gọi';
  }

  get emptyResultFilter() {
    const page = this.callLogQuery.getValue().ui.currentPage;
    const callLogList = this.callLogQuery.getAll();
    const filter = this.callLogQuery.getValue().ui.filter;
    return page == 1 && callLogList.length == 0 && filter;
  }
}
