import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CallLogQuery, CallLogService, CallLogStore} from '@etop/state/shop/call-log';
import {HotlineService} from '@etop/state/shop/hotline';
import {ContactService} from '@etop/state/shop/contact';
import {RelationshipService} from '@etop/state/relationship';
import {ExtensionService} from '@etop/state/shop/extension';

@Component({
  selector: 'etelecom-call-log',
  template: `
    <etelecom-desktop-call-log class='hide-768' style="height: 100%"></etelecom-desktop-call-log>
    <etelecom-mobile-call-log class='show-768'></etelecom-mobile-call-log>`,
  styleUrls: ['./call-log.component.scss']
})
export class CallLogComponent implements OnInit {

  constructor(
    private callLogService: CallLogService,
    private hotlineService: HotlineService,
    private contactService: ContactService,
    private relationshipService: RelationshipService,
    private extensionService: ExtensionService,
    private callLogQuery: CallLogQuery,
    private callLogStore: CallLogStore,
    private cdr: ChangeDetectorRef,
  ) {
  }

  async ngOnInit() {
    this.callLogStore.setLoading(true);
    await this._prepareData();
    this.callLogStore.setLoading(false);
  }

  async _prepareData() {
    const paging = {
      limit: 20,
      after: '.'
    };
    this.callLogService.setPaging(paging);
    await this.relationshipService.getRelationships();
    await this.hotlineService.getHotlines();
    await this.contactService.getContacts();
    await this.extensionService.getExtensions();
    await this.callLogService.getCallLogs();
    this.mappingCallLogs();
    this.cdr.detectChanges();
  }

  mappingCallLogs() {
    let callLogs = this.callLogQuery.getAll();
    callLogs = this.callLogService.callLogsMap(callLogs);
    this.callLogService.setCallLogs(callLogs);
  }
}
