import { Component, Input, OnInit } from '@angular/core';
import { CallLog } from '@etop/models';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'etelecom-audio-modal',
  templateUrl: './audio-modal.component.html',
  styleUrls: ['./audio-modal.component.scss']
})
export class AudioModalComponent implements OnInit {
  @Input() callLog: CallLog;
  constructor(
    private modalAction: ModalAction
  ) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.modalAction.dismiss(true);
  }
}
