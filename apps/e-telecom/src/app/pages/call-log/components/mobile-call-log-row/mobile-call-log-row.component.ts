import { Component, Input, OnInit } from '@angular/core';
import { CallLog } from '@etop/models';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AudioModalComponent } from '../audio-modal/audio-modal.component';

@Component({
  selector: '[etelecom-mobile-call-log-row]',
  templateUrl: './mobile-call-log-row.component.html',
  styleUrls: ['./mobile-call-log-row.component.scss']
})
export class MobileCallLogRowComponent implements OnInit {
  @Input() callLog = new CallLog({});

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit(): void {
  }

  openAudioModal() {
    let modal = this.modalController.create({
      component: AudioModalComponent,
      showBackdrop: 'static',
      componentProps: {
        callLog: this.callLog
      },
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  isOutDirection(callLog: CallLog) {
    return callLog.direction == "out";
  }

}
