import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Fulfillment, ShipnowDeliveryPoint } from 'libs/models/Fulfillment';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { FulfillmentsController } from '../../fulfillments.controller';
import { OrdersService } from '@etop/state/order';

@Component({
  selector: 'etelecom-single-ffm-detail',
  templateUrl: './single-ffm-detail.component.html',
  styleUrls: ['./single-ffm-detail.component.scss']
})
export class SingleFfmDetailComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() ffm = new Fulfillment({});
  active_tab: 'detail_info' | 'support_request' | 'history' | 'shipnow_route' = 'detail_info';

  loading_shipnow_orders = false;

  constructor(
    private ffmsController: FulfillmentsController,
    private orderService: OrdersService
  ) {
    super();
  }

  ngOnInit() {
    this.ffmsController.onChangeTab$
      .pipe(takeUntil(this.destroy$))
      .subscribe((tab_value: any) => {
        this.active_tab = tab_value;
      });
    this.ffm = this.reMapFFM(this.ffm);
  }

  async ngOnChanges(changes: SimpleChanges) {
    this.ffm = this.reMapFFM(this.ffm);
    if (this.ffmsController.fulfillment_type == 'shipnow') {
      this.getShipnowOrders();
    }
  }

  async getShipnowOrders() {
    this.loading_shipnow_orders = true;
    const delivery_points: ShipnowDeliveryPoint[]= [];
    try {
      for (let dp of this.ffm.delivery_points) {
        const order = await this.orderService.getOrder(dp.order_id);
        dp = {
          ...dp,
          order_code: order.code,
          order_status: order.status,
          order_status_display: order.status_display,
          order_total_amount: order.total_amount
        };
        delivery_points.push(dp);
      }
      this.ffm.delivery_points = delivery_points;
    } catch(e) {
      debug.error('ERROR in getShipnowOrders', e);
    }
    this.loading_shipnow_orders = false;
  }

  reMapFFM(ffm: Fulfillment): Fulfillment {
    ffm.p_data = {
      ...ffm.p_data,
      shipping_code: ffm.shipping_code || '-',
      shipping_note: ffm.shipping_note || '-',
      shipping_state: ffm.shipping_state || '-',
      estimated_pickup_at: ffm.estimated_pickup_at || '-',
      estimated_delivery_at: ffm.estimated_delivery_at || '-',
      shipping_service_name: ffm.shipping_service_name || '-',
      try_on: ffm.try_on || '-',
      carrier: ffm.carrier || 'unknown',
      pickup_address: ffm.pickup_address,
      shipping_address: ffm.shipping_address,
      total_cod_amount: ffm.total_cod_amount,
      total_weight: ffm.total_weight,
      shipping_service_fee:
        this.ffmsController.fulfillment_type != 'shipnow' && ffm.shipping_fee_shop || ffm.shipping_service_fee,
    };
    return ffm;
  }

}
