import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { FulfillmentDetailComponent } from './fulfillment-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: FulfillmentDetailComponent }
    ]
  }
];

@NgModule({
  declarations: [
    FulfillmentDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FulfillmentDetailModule { }
