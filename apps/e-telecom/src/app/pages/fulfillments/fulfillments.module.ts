import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImportFfmModule} from "libs/shared/components/import-ffm/import-ffm.module";
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FulfillmentListComponent} from './fulfillment-list/fulfillment-list.component';
import {FulfillmentRowComponent} from './components/fulfillment-row/fulfillment-row.component';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {FormsModule} from '@angular/forms';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from '@etop/core';
import {CancelObjectModule} from '../../components/cancel-object/cancel-object.module';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopPipesModule,
  MaterialModule,
  SideSliderModule,
  EmptyPageModule
} from '@etop/shared';
import {FulfillmentsComponent} from './fulfillments.component';
import {SingleFfmDetailModule} from './components/single-ffm-detail/single-ffm-detail.module';
import {FulfillmentsController} from './fulfillments.controller';
import {ReturningFulfillmentsWarningModule} from '../../components/returning-fulfillments-warning/returning-fulfillments-warning.module';
import {FulfillmentService} from 'apps/e-telecom/src/services/fulfillment.service';
import {TelegramService} from "@etop/features";

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: FulfillmentsComponent},
      {
        path: ':id',
        loadChildren: () => import('./components/fulfillment-detail/fulfillment-detail.module').then(m => m.FulfillmentDetailModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    FulfillmentsComponent,
    FulfillmentListComponent,
    FulfillmentRowComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    ModalControllerModule,
    ReturningFulfillmentsWarningModule,
    DropdownActionsModule,
    CancelObjectModule,
    RouterModule.forChild(routes),
    NgbTooltipModule,
    SingleFfmDetailModule,
    AuthenticateModule,
    // BillPrintingModule,
    EtopPipesModule,
    SideSliderModule,
    EtopFilterModule,
    EtopCommonModule,
    MaterialModule,
    ImportFfmModule,
    EmptyPageModule
  ],
  providers: [
    FulfillmentsController,
    FulfillmentService,
    TelegramService
  ]
})
export class FulfillmentsModule {
}
