import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum PrintType {
  fulfillments = 'fulfillments',
}

@Injectable()
export class FulfillmentsController {
  fulfillment_type = 'shipment';

  onChangeTab$ = new Subject();

  constructor() { }

}

