import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { BankAccount, CompanyInfo, ExtendedAccount, User } from '@etop/models';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';
import { Router } from '@angular/router';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { ModalController } from '../../../../../core/src/components/modal-controller/modal-controller.service';
import { CreateShopModalComponent } from './components/create-shop-modal/create-shop-modal.component';
@Component({
  selector: 'etelecom-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SettingsComponent extends BaseComponent implements OnInit {
  private modal: any;
  accounts: Account[] = [];
  user: User;
  canGoBack = false;
  current_menu = SettingMenu.user;

  menus = [
    {
      mobileTitle: 'Cửa hàng',
      title: 'Thông tin cửa hàng',
      slug: 'shop',
    },
    {
      mobileTitle: 'Giao hàng',
      title: 'Thiết lập giao hàng',
      slug: 'shipping_info',
    }
  ];

  accountMenus = [
    {
      mobileTitle: 'Tài khoản',
      title: 'Thông tin tài khoản eTelecom',
      slug: 'user',
    },
    {
      mobileTitle: 'Danh sách cửa hàng',
      title: 'Danh sách cửa hàng',
      slug: 'accounts',
    },
  ];

  currentShop = new ExtendedAccount({
    bank_account: {
      account_name: '',
      account_number: ''
    },
    address: {
      address1: '',
      province_code: '',
      district_code: '',
      ward_code: ''
    },
    company_info: {
      name: '',
      tax_code: '',
      address: '',
      website: '',
      legal_representative: {
        name: '',
        position: '',
        phone: '',
        email: ''
      }
    }
  });
  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private settingStore: SettingStore,
    private util: UtilService,
    private headerController: HeaderControllerService,
    private modalController: ModalController,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  async ngOnInit() {
    this.auth.authenticatedData$.subscribe(info => {
      this.loadAccount(info);
    });

    this.settingStore.menuChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe((menu: SettingMenu) => {
        if (menu == "accounts") {
          this.headerController.setActions([
            {
              title: 'Tạo cửa hàng',
              cssClass: 'btn btn-primary',
              onClick: () => this.openCreateShopModal(),
            }
          ]);
        } else {
          this.headerController.setActions([]);
        }

        if (menu != this.current_menu) {
          this.current_menu = menu;
          this.changeMenu(menu);
        }
        this.cdr.detectChanges();
      });


  }

  async openCreateShopModal() {
    this.modal = this.modalController.create({
      component: CreateShopModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async () => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async changeMenu(menu) {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    await this.router.navigateByUrl(`/s/${slug}/settings/${menu}`);
  }

  async goBack() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    await this.router.navigateByUrl(`/s/${slug}/settings/user`);
  }

  loadAccount(data) {
    this.currentShop = data.shop || new ExtendedAccount({});
    this.user = data.user;
    // this.accounts = this.auth.snapshot.accounts;

    this.checkNullShop();
  }

  checkNullShop() {
    if (!this.currentShop.bank_account) {
      this.currentShop.bank_account = new BankAccount();
    }
    if (!this.currentShop.company_info) {
      this.currentShop.company_info = new CompanyInfo({
        legal_representative: {
          name: '',
          position: '',
          phone: '',
          email: ''
        }
      });
    }
  }

}
