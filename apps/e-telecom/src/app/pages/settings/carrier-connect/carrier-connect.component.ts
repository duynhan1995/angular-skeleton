import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ConnectionStore} from "@etop/features";
import { BaseComponent } from '@etop/core';
import {ConnectionService} from "@etop/features/connection/connection.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'etelecom-carrier-connect',
  templateUrl: './carrier-connect.component.html',
  styleUrls: ['./carrier-connect.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarrierConnectComponent extends BaseComponent implements OnInit {
  connectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.initConnections?.filter(c => c.connection_method == 'direct' && c.status == 'P'))
  );

  constructor(
    private connectionStore: ConnectionStore,
    private connectionService: ConnectionService
  ) {
    super();
  }

  ngOnInit() {}

  getConnections() {
    this.connectionService.getValidConnections(true).then();
  }

}
