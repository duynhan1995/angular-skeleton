import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges,
} from '@angular/core';
import { Address } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import { ShopService } from '@etop/features';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import { FromAddressModalComponent } from 'libs/shared/components/from-address-modal/from-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {map, takeUntil} from "rxjs/operators";

@Component({
  selector: 'etelecom-from-address',
  templateUrl: './from-address.component.html',
  styleUrls: ['./from-address.component.scss']
})
export class FromAddressComponent extends BaseComponent implements OnInit, OnChanges {
  @Output() addressRemoved = new EventEmitter();

  @Input() address: Address;

  dropdownActions = [];
  shipFromAddressId$ = this.auth.state$.pipe(map(s => s.shop?.ship_from_address_id));

  isDefault = false;

  constructor(
    private addressService: AddressService,
    private gaService: GoogleAnalyticsService,
    private shopService: ShopService,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private ref: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.shipFromAddressId$.pipe(takeUntil(this.destroy$))
      .subscribe(id => {
        this.isDefault = id == this.address?.id;
        this.dropdownActions[1].hidden = this.isDefault;
      });

    this.ref.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    const address = changes.address.currentValue;
    this.isDefault = this.auth.snapshot.shop?.ship_from_address_id == address?.id;

    this.dropdownActions = [
      {
        onClick: () => this.editFromAddress(this.address),
        title: 'Chỉnh sửa',
        permissions: ['shop/settings/shipping_setting:update'],
      },
      {
        onClick: () => this.onRemoveAddress(this.address),
        title: 'Xóa địa chỉ',
        cssClass: 'text-danger',
        hidden: this.isDefault
      }
    ];
  }

  formatAddress(address) {
    return this.addressService.joinAddress(address);
  }

  async setDefaultAddress(address: Address, type) {
    try {
      this.gaService.sendUserBehavior(
        USER_BEHAVIOR.ACTION_DELIVER_SETTING,
        USER_BEHAVIOR.LABEL_UPDATE_DEFAULT_FROM_ADDRESS
      );

      await this.shopService.setDefaultAddress(address.id, type);
      await this.auth.setDefaultAddress(address.id, type);

      toastr.success('Cập nhật địa chỉ mặc định thành công.');
    } catch (e) {
      debug.error('ERROR in setDefaultAddress', e);
      toastr.error('Cập nhật địa chỉ mặc định không thành công.', e.code && (e.message || e.msg));
    }
  }

  editFromAddress(address: Address) {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_DELIVER_SETTING,
      USER_BEHAVIOR.LABEL_UPDATE_FROM_ADDRESS
    );

    let modal = this.modalController.create({
      component: FromAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: address,
        type: 'shipfrom',
        title: 'Cập nhật địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({address: addr}) => {
      this.address = addr
    });
  }

  onRemoveAddress(address: Address) {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_DELIVER_SETTING,
      USER_BEHAVIOR.LABEL_REMOVE_FROM_ADDRESS
    );

    const dialog = this.dialogController.createConfirmDialog({
        title: 'Xóa địa chỉ',
        body: `
        <p>Bạn có chắc muốn xóa địa chỉ này?</p>
        <strong>${this.formatAddress(address)}</strong>
        `,
        closeAfterAction: false,
        onConfirm: async () => {
          await this.removeAddress(address);
          dialog.close().then();
        }
      });

    dialog.show().then();
  }

  async removeAddress(address: Address) {
    try {
      await this.addressService.removeAddress(address.id);
      this.addressRemoved.emit(address.id);
      toastr.success('Xóa địa chỉ thành công!');
    } catch (e) {
      debug.error('ERROR in removeAddress', e);
      toastr.error('Xóa địa chỉ thất bại: ' + e.message);
    }
  }
}
