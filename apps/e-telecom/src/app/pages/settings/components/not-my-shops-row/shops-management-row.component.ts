import { Component, Input, OnInit } from '@angular/core';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { AuthorizationApi } from '@etop/api';
import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';

@Component({
  selector: '[etelecom-shops-management-row]',
  templateUrl: './shops-management-row.component.html',
  styleUrls: ['./shops-management-row.component.scss']
})
export class ShopsManagementRowComponent implements OnInit {
  @Input() account: Account;
  @Input() currentShop: ExtendedAccount;

  dropdownActions = [];

  constructor(
    private gaService: GoogleAnalyticsService,
    private settingStore: SettingStore
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa',
        cssClass: this.account.id != this.currentShop.id && 'cursor-not-allowed',
        disabled: this.account.id != this.currentShop.id,
        onClick: () => this.edit()
      }
    ];
  }

  roleDisplay(roles) {
    if (roles.includes('owner'))
      return 'Chủ shop';
    if (roles.includes('telecom_customerservice') && roles.includes('salesman')) {
      if (roles.includes('staff_management') && roles.length == 3)
        return 'Quản lý';
      if (roles.length == 2)
        return 'Bán hàng';
    }
    return 'Khác'
  }

  switchAccount(index) {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_SHOP_LIST_SETTING,
      USER_BEHAVIOR.LABEL_ACCESS_SHOP
    );
    window.open(`/s/${index}/settings/shop`, '_blank');
  }

  edit() {
    this.settingStore.changeMenu(SettingMenu.shop);
  }

}
