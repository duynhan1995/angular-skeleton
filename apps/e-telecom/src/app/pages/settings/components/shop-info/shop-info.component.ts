import {AfterViewInit, Component, OnInit} from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShopService } from '@etop/features';
// import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import {AccountApi} from '@etop/api';
import { Router } from '@angular/router';
import { AddressService } from 'apps/core/src/services/address.service';
import { Account, CompanyInfo, ExtendedAccount } from '@etop/models/Account';
import { BankAccount } from '@etop/models/Bank';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';
import {map, takeUntil} from 'rxjs/operators';
import {LocationQuery} from "@etop/state/location";
import {FormBuilder} from "@angular/forms";
import {combineLatest} from "rxjs";

@Component({
  selector: 'etelecom-shop-info',
  templateUrl: './shop-info.component.html',
  styleUrls: ['./shop-info.component.scss']
})
export class ShopInfoComponent extends BaseComponent implements OnInit, AfterViewInit {
  accounts: Account[] = [];
  shop: ExtendedAccount;

  uploading = false;
  preparing_data = true;

  updating = {
    shop_info: false,
    company_info: false
  };

  currentAccount: Account;

  locationForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
    wardCode: ''
  });
  formInitializing = true;

  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.locationForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.locationForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private auth: AuthenticateStore,
    public util: UtilService,
    private shopService: ShopService,
    // private gaService: GoogleAnalyticsService,
    private accountApi: AccountApi,
    private router: Router,
    private addressesService: AddressService,
    private settingStore: SettingStore,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  ngOnInit() {
    this.auth.authenticatedData$.subscribe(info => {
      this.shop = {...info.shop} || new ExtendedAccount({});
      this.checkNullShop();
      this.currentAccount = info.account || new Account({});
      this.accounts = info.accounts;
    });

    this.locationReady$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.preparing_data = false;
      });

    this.locationForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.locationForm.patchValue({
            districtCode: '',
            wardCode: '',
          });
        }
        this.shop.address.province_code = value;
      });

    this.locationForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.locationForm.patchValue({
            wardCode: '',
          });
        }
        this.shop.address.district_code = value;
      });

    this.locationForm.controls['wardCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.shop.address.ward_code = value;
      });
  }

  ngAfterViewInit() {
    this.prepareFormData();
  }

  prepareFormData() {
    if (!this.shop.address) {
      return;
    }

    const {province_code, district_code, ward_code} = this.shop.address;

    this.formInitializing = true;

    this.locationForm.patchValue({
      provinceCode: province_code,
      districtCode: district_code,
      wardCode: ward_code
    });

    this.formInitializing = false;
  }

  async onFileSelected(event) {
    this.uploading = true;
    try {
      const files = Object.values(event.target.files);
      const resImages = await this.util.uploadImages([files[0]], 1024);
      this.shop.image_url = resImages[0].url;

      const data = {
        image_url: this.shop.image_url
      };
      const res = await this.shopService.updateShopField(data);
      this.auth.updateShop(res.shop);
      toastr.success('Cập nhật logo thành công!');
    } catch (e) {
      debug.error(e);
      toastr.error('Cập nhật logo không thành công.', e.code ? (e.message || e.msg) : '');
    }
    this.uploading = false;
  }

  validateShopInfo(data) {
    const { name, phone, website_url, email, address } = data;
    if (!name) {
      toastr.error('Vui lòng nhập tên cửa hàng');
      return false;
    }

    let _phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    _phone = (_phone && _phone.split('-test')[0]) || '';
    const validPhone = this.util.validatePhoneNumber(_phone);
    if (!validPhone) {
      return false;
    }

    if (website_url &&
      !website_url.match(
        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
      )
    ) {
      toastr.error(`Vui lòng điền địa chỉ website hợp lệ`);
      return false;
    }

    let _email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    _email = (_email && _email.split('-test')[0]) || '';
    if (_email &&
      !_email.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      toastr.error('Vui lòng nhập email hợp lệ!');
      return false;
    }

    if (!address.province_code) {
      toastr.error('Vui lòng chọn tỉnh thành');
      return false;
    }
    if (!address.district_code) {
      toastr.error('Vui lòng chọn quận huyện');
      return false;
    }
    if (!address.ward_code) {
      toastr.error('Vui lòng chọn phường xã');
      return false;
    }
    if (!address.address1) {
      toastr.error('Vui lòng nhập địa chỉ');
      return false;
    }
    return true;
  }

  async updateShopInfo() {
    this.updating.shop_info = true;
    // this.gaService.sendUserBehavior(
    //   USER_BEHAVIOR.ACTION_SHOP_SETTING,
    //   USER_BEHAVIOR.LABEL_UPDATE_CONTACT_INFO
    // );

    try {
      const { name, phone, website_url, email, address } = this.shop;

      const _address = {
        address1: address.address1,
        province_code: address.province_code,
        district_code: address.district_code,
        ward_code: address.ward_code
      };

      const body = {
        name, phone, website_url, email,
        address: _address
      };

      const validInfo = this.validateShopInfo(body);
      if (!validInfo) {
        return this.updating.shop_info = false;
      }

      const res = await this.shopService.updateShopField(body);

      if (this.currentAccount.url_slug) {
        let urlSlug = this.currentAccount.url_slug;
        urlSlug = this.util.createHandle(urlSlug);

        let slugExisted = this.accounts
          .filter(account => account.id != this.currentAccount.id)
          .reduce((x, y) => x || y.url_slug == urlSlug, false);

        if (slugExisted) {
          let index: any = this.accounts
            .findIndex(account => account.id == this.currentAccount.id);
          index = index.toString();

          urlSlug += index ? '-' + index : '-0';
        }

        await this.accountApi.updateURLSlug({
          account_id: this.currentAccount.id,
          url_slug: urlSlug
        });
        this.currentAccount.url_slug = urlSlug;
        this.auth.updateAccount(this.currentAccount);
      }

      this.auth.updateShop(res.shop);
      this.checkNullShop();
      this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings/shop`);
      toastr.success('Cập nhật thông tin cửa hàng thành công!');
    } catch (e) {
      debug.error('ERROR in Updating Shop Info', e);
      toastr.error('Cập nhật thông tin cửa hàng không thành công.', e.code ? (e.message || e) : '');
    }
    this.updating.shop_info = false;
  }

  validateCompanyInfo(data) {
    data = this.util.trimFields(data, ['name', 'tax_code', 'address']);
    data.legal_representative = this.util.trimFields(
      data.legal_representative,
      ['name', 'position', 'phone', 'email']
    );

    if (!data.name) {
      toastr.error('Chưa nhập tên doanh nghiệp.');
      return false;
    }
    if (!data.tax_code) {
      toastr.error('Chưa nhập mã số thuế.');
      return false;
    }
    if (!data.address) {
      toastr.error('Chưa nhập địa chỉ trụ sở.');
      return false;
    }
    if (!data.legal_representative.name) {
      toastr.error('Chưa nhập tên người đại diện pháp luật.');
      return false;
    }
    if (!data.legal_representative.position) {
      toastr.error('Chưa nhập vị trí người đại diện pháp luật.');
      return false;
    }
    if (!data.legal_representative.phone) {
      toastr.error('Chưa nhập SĐT người đại diện pháp luật.');
      return false;
    }
    const validPhone = this.util.validatePhoneNumber(
      data.legal_representative.phone
    );
    if (!validPhone) {
      return false;
    }
    if (!data.legal_representative.email) {
      toastr.error('Chưa nhập email người đại diện pháp luật.');
      return false;
    }
    return true;
  }

  async updateCompanyInfo() {
    this.updating.company_info = true;
    try {
      const body = {
        name: this.shop.company_info.name,
        tax_code: this.shop.company_info.tax_code,
        address: this.shop.company_info.address,
        legal_representative: {
          name: this.shop.company_info.legal_representative.name,
          position: this.shop.company_info.legal_representative.position,
          phone: this.shop.company_info.legal_representative.phone,
          email: this.shop.company_info.legal_representative.email
        }
      };
      const validInfo = this.validateCompanyInfo(body);
      if (!validInfo) {
        return this.updating.company_info = false;
      }

      const res = await this.shopService.updateCompanyInfo(body);

      this.auth.updateShop(res.shop);
      this.checkNullShop();

    } catch (e) {
      debug.error('ERROR in Updating Company Info', e);
      toastr.error('Cập nhật thông tin doanh nghiệp không thành công.', e.code ? (e.message || e.msg) : '');
    }
    this.updating.company_info = false;
  }

  onChangeShopName() {
    this.currentAccount.url_slug = this.util.createHandle(
      this.shop.name
    );
  }

  checkNullShop() {
    if (!this.shop.bank_account) {
      this.shop.bank_account = new BankAccount();
    }
    if (!this.shop.company_info) {
      this.shop.company_info = new CompanyInfo({
        legal_representative: {}
      });
    }
  }

  formatAddress(address) {
    return this.addressesService.joinAddress(address);
  }

  updateFromAddress() {
    this.settingStore.changeMenu(SettingMenu.shipping_info);
  }
}
