import { TicketService } from '@etop/state/shop/ticket';
import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticateStore} from "@etop/core";
import { TicketsSystemListComponent } from './tickets-system-list/tickets-system-list.component';

@Component({
  selector: 'etelecom-admin-tickets',
  template: `
      <etop-not-permission *ngIf="noPermission"></etop-not-permission>
      <etelecom-admin-ticket-list
        *ePermissions="['shop/shop_ticket_comment:view']" #ticketList>
      </etelecom-admin-ticket-list>`
})
export class TicketsSystemComponent implements OnInit {
  @ViewChild('ticketList', {static: true}) ticketList: TicketsSystemListComponent;

  constructor(
    private auth: AuthenticateStore,
    private ticketService: TicketService
  ) {}

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('shop/shop_ticket_comment:view');
  }

  ngOnInit() {
    this.ticketService.prepareData().then();
  }

}
