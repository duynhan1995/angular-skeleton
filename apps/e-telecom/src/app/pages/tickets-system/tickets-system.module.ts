import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from "@etop/shared";
import {AuthenticateModule} from "@etop/core";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {TicketsSystemComponent} from './tickets-system.component';
import {TicketsSystemListComponent} from './tickets-system-list/tickets-system-list.component';
import {TicketsSystemRowComponent} from './components/tickets-system-row/tickets-system-row.component';
import {TicketsSystemDetailComponent} from './components/tickets-system-detail/tickets-system-detail.component';
import {CreateTicketAdminComponent} from './components/tickets-system-create/tickets-system-create.component';
import {ShopTicketCommentsModule} from "@etop/shared/components/shop-ticket-comments/shop-ticket-comments.module";

const routes: Routes = [
  {
    path: '',
    component: TicketsSystemComponent
  }
];

@NgModule({
  declarations: [
    TicketsSystemComponent,
    TicketsSystemListComponent,
    TicketsSystemRowComponent,
    TicketsSystemDetailComponent,
    CreateTicketAdminComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    EtopMaterialModule,
    EtopFilterModule,
    EtopCommonModule,
    EtopPipesModule,
    SideSliderModule,
    AuthenticateModule,
    NotPermissionModule,
    NgbModule,
    EmptyPageModule,
    ShopTicketCommentsModule
  ]
})
export class TicketsSystemModule {
}
