import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { FaboshopAppModule } from 'apps/faboshop/src/app/faboshop-app.module';
import { environment } from './environments/environment';
import { hmrBootstrap } from 'apps/core/src/hmr';
import * as debug from '@etop/utils/debug';
import * as querystring from "querystring";


(window as any).__debug_host = '';
debug.setupDebug(environment);

if (environment.production) {
  enableProdMode();
}

let debugHost = querystring.parse(location.search.slice(1))?.debug_host;
__debug_host = debugHost as string;

const bootstrap = () => platformBrowserDynamic().bootstrapModule(FaboshopAppModule);

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    // tslint:disable-next-line:no-console
    console.error('HMR is not enabled for webpack-dev-server!');
    // tslint:disable-next-line:no-console
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  // tslint:disable-next-line:no-console
  bootstrap().catch(err => console.log(err));
}
