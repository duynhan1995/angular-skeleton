import { Injectable } from '@angular/core';
import { InventoryApi } from '@etop/api';
import { Filters } from '@etop/models';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(
    private inventoryApi: InventoryApi
  ) { }

  async getInventoryVouchers(start?: number, perpage?: number, filters?: Filters) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return await this.inventoryApi.getInventoryVouchers({ paging, filters });
  }

}
