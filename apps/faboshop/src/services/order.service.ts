import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { AuthenticateStore, requireMethodRoles } from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { UtilService } from 'apps/core/src/services/util.service';
import List from 'identical-list';
import { Subject } from 'rxjs';
import {OrderApi, AccountApi} from '@etop/api';
import { Order } from 'libs/models/Order';
import { FilterOperator, Filters } from '@etop/models';
import {Fulfillment} from "@etop/models";

const orderSource = {
  ts_app: 'TOPSHIP App',
  unknown: 'Không xác định',
  default: 'Mặc định',
  etop_pos: 'eTop POS',
  haravan: 'Haravan',
  etop_pxs: 'eTop POS Extension',
  self: 'Nhập hàng',
  import: 'Import',
  etop_cmx: 'CMX',
  api: 'API'
};

@Injectable()
export class OrderService {
  onOrderLinesChanged$ = new Subject();
  onOrderInfoChanged$ = new Subject();
  onOrderCustomerChanged$ = new Subject();
  createOrderFailed$ = new Subject();
  createOrderSuccess$ = new Subject();
  switchOffCreatingFFM$ = new Subject();

  shopAccounts = [];
  orderList = new List();
  tryOnOptions = [
    {
      code: 'try',
      value: 'try',
      name: 'Cho thử hàng'
    },
    {
      code: 'open',
      value: 'open',
      name: 'Cho xem hàng không thử'
    },
    {
      code: 'none',
      value: 'none',
      name: 'Không cho xem hàng'
    }
  ];

  constructor(
    private http: HttpService,
    private auth: AuthenticateStore,
    private userService: UserService,
    private accountApi: AccountApi,
    private util: UtilService,
    private orderApi: OrderApi,
  ) {
    this.shopAccounts = this.auth.snapshot.accounts.map(acc => this.util.deepClone(acc));
    this.auth.authenticatedData$.subscribe(info => {
      this.shopAccounts =
        (info.accounts && info.accounts.map(acc => this.util.deepClone(acc))) ||
        [];
    });
  }

  async orderSourceMap(source, partner_id) {
    if (partner_id != '0' && ['etop_cmx', 'api'].indexOf(source) != -1) {
      const _partner = await this.accountApi.getPublicPartnerInfo(partner_id);
      return _partner
        ? `${orderSource[source]} - ${_partner.name}`
        : orderSource[source];
    }
    return source ? orderSource[source] : 'Không xác định';
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: true,
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  async checkEmptyOrders(shop_id) {
    return await this.orderApi.checkEmptyOrders(shop_id);
  }

  async getOrder(id): Promise<Order> {
    return new Promise((resolve, reject) => {
      this.orderApi.getOrder(id)
        .then(order => {
          order.activeFulfillment = this.getActiveFulfillment(order);
          resolve(order);
        })
        .catch(err => reject(err));
    });
  }

  // TODO: Temporarily closed until dashboard works smoothly
  // @requireMethodRoles({
  //   skipError: true,
  //   valueOnError: [],
  //   roles: ['admin', 'owner', 'salesman', 'accountant']
  // })
  async getOrders(start?: number, perpage?: number, filters?: Array<any>): Promise<Order[]> {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return new Promise((resolve, reject) => {
      this.orderApi.getOrders({ paging, filters })
        .then(orders => {
          orders.map((order, index) => {
            order.activeFulfillment = this.getActiveFulfillment(order);
            return this.mapOrderInfo(order, index);
          });
          resolve(orders);
        })
        .catch(err => reject(err));
    });
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: null,
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  getShopOrders(token, {paging : {limit}}) {
    return this.orderApi.getShopOrders(token, { paging: { limit} });
  }

  createOrder(body) {
    const data = {...body};
    delete data.o_data;
    delete data.p_data;
    for (let key in data) {
      if (!data[key]) { continue; }
      if (data[key].o_data) { delete data[key].o_data; }
      if (data[key].p_data) { delete data[key].p_data; }
    }
    return this.orderApi.createOrder(data);
  }

  updateOrder(body) {
    return this.orderApi.updateOrder(body);
  }

  async importOrder(formData) {
    return await this.orderApi.importOrder(formData);
  }

  getOrdersByIds(ids) {
    let subset = this.orderList.asArray();
    return subset.filter(order => ids.indexOf(order.id) > -1);
  }

  mapOrderInfo(order: Order, index) {
    return {
      ...order,
      index,
      search_text: this.util.makeSearchText(`${order.code}${order.customer.full_name}${order.customer.phone}`)
    }
  }

  mapSpecialFilters(filters: Filters) {
    filters.forEach(f => {
      if (f.value == '{}') {
        f.op = FilterOperator.eq;
        f.name = 'fulfillment.ids'
      }
    });
  }

  getActiveFulfillment(order: Order) {
    let activeFulfillment: any;
    switch (order.fulfillment_type) {
      case 'shipment':
        activeFulfillment = order.fulfillments[0].shipment;
        break;
      case 'shipnow':
        activeFulfillment = order.fulfillments[0].shipnow;
        break;
      default:
        activeFulfillment = order.fulfillments[0];
        break;
    }
    activeFulfillment = Fulfillment.fulfillmentMap(activeFulfillment, []);
    return activeFulfillment;
  }

}
