import { Injectable } from '@angular/core';
import { CollectionApi } from '@etop/api';

@Injectable()
export class CollectionService {

  constructor(
    private collectionApi: CollectionApi
  ) { }

  getCollections() {
    return this.collectionApi.getCollections();
  }

  createCollection(data) {
    return this.collectionApi.createCollection(data);
  }

}
