import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { Subject } from 'rxjs';
import { Ticket } from 'libs/models/Ticket';
import { CrmApi } from 'apps/core/src/apis/crm-api.service';
import { ConfigService } from '@etop/core/services/config.service';

let url = "";

@Injectable()
export class ShopCrmService {

  onTicketCreated = new Subject<Ticket>();

  constructor(
    private crmApi: CrmApi,
    private http: HttpService,
    private configService: ConfigService
  ) {
    url = configService.getConfig().crm_url;
  }

  /**
   * @deprecated since 2.0
   */
  crmCustomFieldMap(key: string) {
    return key;
  }

  async updateLeadAndContact(user, shop, address) {
    let isTest = user.email.split(/-[0-9a-zA-Z]+-test$/).length > 1;
    let body = {
      company: shop.name,
      website: shop.website_url,
      secondaryemail: shop.email,
      mobile: shop.phone,
      lane: `${address.address1} - ${address.ward}`,
      city: address.district,
      state: address.province,
      country: 'Vietnam',
      email: user.email,
      firstname: isTest ? 'TEST' : '',
      lastname: user.full_name,
      phone: user.phone,
      leadsource: user.source,
      etop_id: user.id
    };
    this.crmApi.updateLead(body);
    this.crmApi.updateContact(body);
  }

  async rawUpdateLeadAndContact(body) {
    this.crmApi.updateLead(body);
    this.crmApi.updateContact(body);
  }

  /**
   * @deprecated since 2.0
   */
  async createLead(body: {}) {
    this.http.post(`${url}/api.crm/lead`, body).toPromise()
      .then(res => {
        if (res.data) {
          debug.log('SUCCESS -->>>', res);
        } else {
          debug.log('ERROR -->>>', res);
        }
      }).catch(err => {
      debug.log('ERROR -->>>', err);
      });
  }

  /**
   * @deprecated since 2.0
   */
  async updateLead(body: {}) {
    this.http.post(`${url}/api.crm/lead/update`, body).toPromise()
      .then(res => {
        if (res.data) {
          debug.log('SUCCESS -->>>', res);
        } else {
          debug.log('ERROR -->>>', res);
        }
      }).catch(err => {
      debug.log('ERROR -->>>', err);
      });
  }

  /**
   * @deprecated since 2.0
   */
  async createContact(body: {}) {
    this.http.post(`${url}/api.crm/contact`, body).toPromise()
      .then(res => {
        if (res.data) {
          debug.log('SUCCESS -->>>', res);
        } else {
          debug.log('ERROR -->>>', res);
        }
      }).catch(err => {
      debug.log('ERROR -->>>', err);
      });
  }

  /**
   * @deprecated since 2.0
   */
  async updateContact(body: {}) {
    this.http.post(`${url}/api.crm/contact/update`, body).toPromise()
      .then(res => {
        if (res.data) {
          debug.log('updateContact SUCCESS -->>>', res);
        } else {
          debug.log('updateContact ERROR -->>>', res);
        }
      }).catch(err => {
      debug.log('updateContact ERROR -->>>', err);
      });
  }

  /**
   * @deprecated since 2.0
   */
  async createTicket(body: {}) {
    return this.http.post(`${url}/api.crm/ticket`, body).toPromise()
      .then(res => {
        this.onTicketCreated.next(res.data);
        return res.data;
      });
  }

  /**
   * @deprecated since 2.0
   */
  async getTickets(query: {}) {
    return this.http.get(`${url}/api.crm/ticket`, query).toPromise()
      .then(res => res.data);
  }

  /**
   * @deprecated since 2.0
   */
  async createComment(body: {}) {
    return this.http
      .post(`${url}/api.crm/ticket/comment`, body)
      .toPromise()
      .then(res => res.data);
  }

  /**
   * @deprecated since 2.0
   */
  async getTicketComment(ticket_id) {
    return this.http.get(`${url}/api.crm/ticket/${ticket_id}`)
      .toPromise()
      .then(res => res.data);
  }

}
