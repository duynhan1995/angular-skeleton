import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { Receipt } from 'libs/models/Receipt';
import { ReceiptApi } from '@etop/api';

@Injectable()
export class ReceiptService {
  constructor(
    private http: HttpService,
    private receiptApi: ReceiptApi
  ) { }

  async getReceipt(id) {
    return await this.receiptApi.getReceipt(id);
  }

  async getReceipts(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return await this.receiptApi.getReceipts({ paging, filters })
      .then(res => {
        res.receipts = res.receipts.map((r, index) => this.mapReceiptInfo(r, index));
        return res;
      });
  }

  async getReceiptsByLedgerType(type, start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return await this.receiptApi.getReceiptsByLedgerType(type, { paging, filters })
      .then(res => {
        res.receipts = res.receipts.map((r, index) => this.mapReceiptInfo(r, index));
        return res;
      });
  }

  async createReceipt(data: Receipt) {
    return await this.receiptApi.createReceipt(data);
  }

  async updateReceipt(data) {
    return await this.receiptApi.updateReceipt(data);
  }

  async deleteReceipt(id) {
    return await this.receiptApi.deleteReceipt(id);
  }

  mapReceiptInfo(receipt: Receipt, index) {
    return {
      ...receipt,
      index
    }
  }

  async getLedgers(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return await this.receiptApi.getLedgers({ paging, filters });
  }

  async deleteLedger(id: string) {
    return await this.receiptApi.deleteLedger(id);
  }

}
