import { Injectable } from '@angular/core';
// @ts-ignore
import { } from "googlemaps";
declare var google: any;

// NOTE 01 - Hanoi; 79 - Saigon

const BOUND = {
  "01": {
    south_west: {
      lat: 20.5623231,
      lng: 105.28546589999996
    },
    north_east: {
      lat: 21.3850271,
      lng: 106.01988589999996
    }
  },
  "79": {
    south_west: {
      lat: 10.3493704,
      lng: 106.36387839999998
    },
    north_east: {
      lat: 11.1602136,
      lng: 107.02657690000001
    }
  }
};

const ALLOWED_PROVINCE = ['01', '79'];
/*
** 01 = HA NOI
** 79 = SAI GON
 */

@Injectable({
  providedIn: 'root'
})
export class GoogleMapService {
  // @ts-ignore
  map: google.maps.Map;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer({
    suppressMarkers: true,
    polylineOptions: {
      strokeColor: '#33B7D5',
      strokeOpacity: 1.0,
      strokeWeight: 4
    }
  });
  // @ts-ignore
  markers: Array<google.maps.Marker> = [];
  map_points = []; // map_points is an Array containing {lat, lng} of every marker in the map (including starting point a.k.a pickup address)

  count_request = {
    Map_API: 0,
    Geocoder_API: 0,
    Direction_API: 0,
    Places_API: 0
  };

  bounds: any;

  constructor() { }

  static get allowedProvinces() {
    return ALLOWED_PROVINCE;
  }

  initMap(gmapElement: any, lat: number, lng: number) {
    this.map_points = [];
    let mapProp = {
      center: { lat, lng },
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false,
      mapTypeControl: false,
      streetViewControl: false
    };
    this.map = new google.maps.Map(gmapElement.nativeElement, mapProp);
    // SECTION: to count
    this.count_request.Map_API += 1;
  }

  initDefaultMap(gmapElement: any) {
    let mapProp = {
      center: { lat: 14.0583, lng: 108.2772 }, // lat & lng of Vietnam
      zoom: 1,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false,
      mapTypeControl: false,
      streetViewControl: false
    };
    this.map = new google.maps.Map(gmapElement.nativeElement, mapProp);
    // SECTION: to count
    this.count_request.Map_API += 1;
  }

  reCenterMap() {
    try {
      const bounds = new google.maps.LatLngBounds();
      // the first item of map_points is pickup_address
      bounds.extend(new google.maps.LatLng(this.map_points[0].lat, this.map_points[0].lng));
      this.markers.forEach((m: any) => {
        m.setMap(this.map);
        // SECTION: to count
        this.count_request.Map_API += 1;
        const loc = new google.maps.LatLng(m.position.lat(), m.position.lng());
        bounds.extend(loc);
      });
      google.maps.event.addListener(this.map, 'zoom_changed', () => {
        let zoomChangeBoundsListener =
          google.maps.event.addListener(this.map, 'bounds_changed', (event) => {
            if (this.map.getZoom() > 18) {
              this.map.setZoom(18);
              // SECTION: to count
              this.count_request.Map_API += 2;
            }
            google.maps.event.removeListener(zoomChangeBoundsListener);
          });
      });
      this.map.fitBounds(bounds);
      this.map.panToBounds(bounds);
      // SECTION: to count
      this.count_request.Map_API += 2;
    } catch (e) {
      debug.error('ERROR in reCenterMap', e);
    }
  }

  getLatLngFromAddress(address: string) {
    return new Promise((resolve, reject) => {
      try {
        let geocoder = new google.maps.Geocoder();
        geocoder.geocode({ address }, (results, status) => {
          // SECTION: to count
          this.count_request.Geocoder_API += 1;
          if (status.toString() == 'OK') {
            let invalid = false;
            let lat = results[0].geometry.location.lat();
            let lng = results[0].geometry.location.lng();
            if (!(lat && lng)) invalid = true;
            resolve({ lat: lat, lng: lng, invalid });
          } else resolve({
            lat: null,
            lng: null,
            invalid: true
          });
        });
      } catch (e) {
        debug.error("ERROR in getting Lat Lng", e);
        reject(e);
      }
    });
  }

  getMarkerFromLatLng(lat, lng, marker_url: string) {
    try {
      let mapProp = {
        center: { lat, lng }
      };
      const marker: any = new google.maps.Marker({
        position: mapProp.center,
        icon: marker_url,
        map: this.map
      });
      return marker;
    } catch (e) {
      debug.error("ERROR in getting Marker", e);
    }
  }

  calculateAndDisplayRoute(start, end, waypoints) {
    this.directionsDisplay.setMap(null);
    // SECTION: to count
    this.count_request.Map_API += 1;
    return new Promise((resolve, reject) => {
      try {
        let travelMode: any = 'DRIVING';
        this.directionsService.route({
          origin: start,
          destination: end,
          waypoints,
          travelMode
        }, (response, status: any) => {
          // SECTION: to count
          this.count_request.Direction_API += 1;
          if (status == 'OK') {
            this.directionsDisplay.setMap(this.map);
            // SECTION: to count
            this.count_request.Map_API += 1;
            this.directionsDisplay.setDirections(response);
            // SECTION: to count
            this.count_request.Direction_API += 1;
          }
          resolve();
        });
      } catch (e) {
        reject(e);
        debug.error("ERROR in calculateAndDisplayRoute", e);
      }
    });
  }

  getBoundCornersOfCity(province_code: string) {
    if (ALLOWED_PROVINCE.indexOf(province_code) != -1) {
      const southWest = new google.maps.LatLng(BOUND[province_code].south_west.lat, BOUND[province_code].south_west.lng);
      const northEast = new google.maps.LatLng(BOUND[province_code].north_east.lat, BOUND[province_code].north_east.lng);
      this.bounds = new google.maps.LatLngBounds(southWest, northEast);
    }
  }

  resetMap() {
    this.directionsDisplay.setMap(null);
    this.markers.forEach(m => m.setMap(null));
    this.markers = [];
    this.reCenterMap();
  }

}
