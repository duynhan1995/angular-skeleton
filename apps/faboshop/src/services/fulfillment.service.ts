import { Injectable } from '@angular/core';
// TODO: check this import
import moment from 'moment';
import List from 'identical-list';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Subject } from 'rxjs';
import { CmsService } from 'apps/faboshop/src/services/cms.service';
import { AppService } from '@etop/web/core/app.service';
import { FulfillmentApi } from '@etop/api';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';

interface ForceItem {
  id: string;
  type: string;
  expireTime: number;
}

const SHIPPING_TYPE = {
  fast: 'Nhanh',
  standard: 'Chuẩn'
};

const CARRIER_FULL_NAME = {
  ghn: 'Giao hàng nhanh',
  ghtk: 'Giao hàng tiết kiệm',
  vtpost: 'Viettel Post'
};

@Injectable()
export class FulfillmentService {
  shipping_type = 'shipment';

  weight_calc_type = 1;
  /* 1: Khoi luong (chargeable_weight)
  ** 2: The tich (length x width x height)
   */

  forceList: List<ForceItem>;

  onResetData$ = new Subject();
  onViewReturningFfms$ = new Subject();

  private __secretCode = 'fulfillment-data';

  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private fulfillmentApi: FulfillmentApi,
    private appService: AppService,
    private cms: CmsService,
    private promiseQueueService: PromiseQueueService,
  ) {
    this.getLocalForceList();
  }

  resetData() {
    this.shipping_type = 'shipment';
    this.weight_calc_type = 1;
    this.onResetData$.next();
  }

  async getFulfillments(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return await this.fulfillmentApi.getFulfillments({ paging, filters }).then(res => {
       res.fulfillments = res?.fulfillments.map(ffm => Fulfillment.fulfillmentMap(ffm, []));
       return res;
    });
  }

  async getFulfillment(id, token?) {
    return this.fulfillmentApi.getFulfillment(id, token);
  }

  async cancelSingleFfm(ffm: Fulfillment, reason) {
    await this.fulfillmentApi.cancelShipmentFulfillment(ffm.id, reason);
  }

  async cancelFfms(availableFfms: Fulfillment[],reason: string, notification: any) {
    const promises = availableFfms.map(f => async () => {
      try {
        await this.cancelSingleFfm(f, reason);
        notification.successCount += 1;
      } catch (e) {
        notification.errorMessage = e.message;
        notification.failedCount += 1;
      }
    });
    await this.promiseQueueService.run(promises, 10);
  }

  async getShipnowServices(body) {
    return this.fulfillmentApi.getShipnowServices(body);
  }

  async createShipnowFulfillment(body) {
    return this.fulfillmentApi.createShipnowFulfillment(body);
  }

  async confirmShipnowFulfillment(shipnow_id) {
    return this.fulfillmentApi.confirmShipnowFulfillment(shipnow_id);
  }

  async cancelShipnowFulfillment(shipnow_id, cancel_reason) {
    return this.fulfillmentApi.cancelShipnowFulfillment(shipnow_id, cancel_reason);
  }

  checkStatusForceItem(shipping_code: string) {
    const item = this.forceList.get(shipping_code);
    if (!item) {
      return false;
    }
    const now = moment.now();
    return item.expireTime > now;
  }

  getLocalForceList() {
    const data: any = localStorage.getItem(this.__secretCode);
    const list = data ? JSON.parse(data).forceList : [];
    const now = moment.now();
    this.forceList = new List(list.filter(i => i.expireTime > now));
  }

  addForceItem(shipping_code, type) {
    const expireTime = moment(moment.now())
      .add(6, 'hours')
      .valueOf();
    this.forceList.add({
      id: shipping_code,
      type,
      expireTime
    });
    this.updateLocalForceList();
  }

  updateLocalForceList() {
    const data = { forceList: this.forceList.asArray() };
    localStorage.setItem(this.__secretCode, JSON.stringify(data));
  }

  trackingLink(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return null;
    }
    switch (fulfillment.carrier) {
      case 'ghn':
        return `https://track.ghn.vn/order/tracking?code=${
          fulfillment.shipping_code
        }`;
      case 'vtpost':
        return `https://old.viettelpost.com.vn/Tracking?KEY=${
          fulfillment.shipping_code
        }`;
      case 'ahamove':
        return fulfillment.shipping_shared_link;
      default:
        return null;
    }
  }

  public exportFulfillment(export_filters) {
    return this.fulfillmentApi.requestExportFulfillmentExcel(export_filters);
  }

  canForcePicking(fulfillment: Fulfillment) {
    if (!fulfillment) { return false; }
    const { shipping_state } = fulfillment;
    return shipping_state === 'picking' || shipping_state === 'created' || shipping_state === 'confirmed';
  }

  canForceDelivering(fulfillment: Fulfillment) {
    if (!fulfillment) { return false; }
    const { shipping_state } = fulfillment;
    return shipping_state === 'delivering' || shipping_state === 'holding';
  }

  canSendSupportRequest(fulfillment: Fulfillment) {
    if (!fulfillment) return false;
    const { shipping_state } = fulfillment;
    return (
      shipping_state === 'delivering' ||
      shipping_state === 'holding' ||
      shipping_state === 'picking' ||
      shipping_state === 'created' ||
      shipping_state === 'confirmed' ||
      shipping_state === 'undeliverable' ||
      shipping_state === 'returning' ||
      shipping_state === 'processing'
    );
  }

  async getFulfillmentHistory(ffm) {
    return this.fulfillmentApi.getFulfillmentHistory(ffm);
  }

  groupShipmentServicesByConnection(shipment_services: any[]) {
    const hash = {};
    for (let service of shipment_services) {
      if (hash[service.connection_info.id]) {
        hash[service.connection_info.id].services.push(service);
      } else {
        hash[service.connection_info.id] = {
          carrier_name: service.carrier,
          name: service.connection_info.name,
          from_topship: service.connection_info.name.toLowerCase().indexOf('topship') != -1,
          logo: `assets/images/provider_logos/${service.carrier}-s.png`,
          services: [service]
        };
      }
    }
    const carriers = [
      'topship-ghn', 'topship-ghtk', 'topship-vtp', 'ghn', 'ghtk', 'vtp'
    ];
    const results = [];
    // NOTE: Group
    for (let c of carriers) {
      for (let key in hash) {
        const name = hash[key].name;
        if (this.util.createHandle(name) == c) {
          results.push(hash[key]);
        }
      }
    }
    // NOTE: Sort
    for (let carrier of results) {
      carrier.services = carrier.services.sort((s1, s2) => {
        const fee1 = s1.fee;
        const fee2 = s2.fee;
        return fee1 - fee2;
      });
      // NOTE: do_filter if needed
      const do_filter = this.appService && this.appService.appID == 'etop.vn';
      if (do_filter) {
        carrier.services = this.getMinPacks(carrier.services, carrier.carrier_name);
      }
    }
    return results;
  }

  filterShippingService(shippingServices: any[], excludeProvider?: string) {
    const group = {
      ghn: [],
      ghtk: [],
      vtpost: []
    };

    const providerGroup = shippingServices.filter(s => s.carrier !== excludeProvider).reduce((providerHash, s) => {
      providerHash[s.carrier].push(s);
      return providerHash;
    }, group);

    return Object.keys(providerGroup).map(key => {
      let services = providerGroup[key] as Array<any>;
      services = this.getMinPacks(services, key);
      return {
        name: key,
        imageSrc: `/assets/images/provider_logos/${key}-s.png`,
        services: services,
        full_name: CARRIER_FULL_NAME[key]
      };
    });
  }

  getMinPacks(services, carrier_name) {
    try {
      let minStandardPack = this.findMinPack(services, SHIPPING_TYPE.standard);
      let minFastPack = this.findMinPack(services, SHIPPING_TYPE.fast);
      let minPack = this.findMinPack(services, null);

      const standardDeliveryTime = minStandardPack.estimated_delivery_at ? new Date(minStandardPack.estimated_delivery_at).getTime() : 0;
      const fastDeliveryTime = minFastPack.estimated_delivery_at ? new Date(minFastPack.estimated_delivery_at).getTime() : 0;

      // NOTE GHN has only Package which is got by CMS
      if (carrier_name == 'ghn') {
        const package_name = this.cms.getGHNPriorityPackage();
        if (!package_name) return [minPack];
        if (package_name == 'Chuẩn') return [minStandardPack];
        return [minFastPack];
      }
      if (minFastPack.is_available && minStandardPack.is_available) {
        // standard pack with estimated time <= fast
        if (standardDeliveryTime <= fastDeliveryTime && minStandardPack.fee <= minFastPack.fee) {
          return [{ name: SHIPPING_TYPE.fast, is_available: false, fee: -1 }, minStandardPack];
        }
        // fast pack with fee <= standard
        if (minFastPack.fee <= minStandardPack.fee && fastDeliveryTime <= standardDeliveryTime) {
          return [minFastPack, { name: SHIPPING_TYPE.standard, is_available: false, fee: -1 }];
        }
      }
      return [minFastPack, minStandardPack];
    } catch(e) {
      debug.error("ERROR in finding min Packs", e);
    }
  }

  private findMinPack(services, type: string) {
    let minPack = services
      .filter(s => !type || s.name === type)
      .reduce((min, n) => {
        if (!min) {
          return n;
        }
        return min.service_fee > n.service_fee ? n : min;
      }, undefined);

    minPack = minPack && { ...minPack, is_available: true } || { is_available: false };
    return minPack;
  }

  getMinPacksByBestPrice(services: any[], carrier: string, service_name: string) {
    const min_price = Math.min(...services.map(s => s.fee));
    const min_price_arr = services.filter(s => s.fee == min_price);
    if (min_price_arr.length == 1) {
      return min_price_arr[0];
    }
    return this.sortPacksByCustomPriority(min_price_arr, carrier, service_name);
  }

  sortPacksByCustomPriority(services: any[], carrier: string, service_name: string) {
    // deepClone, prevent pass by reference
    let _services: any[] = JSON.parse(JSON.stringify(services));
    _services = _services.sort((a,b) => {
      if (a.carrier == b.carrier) {
        return a.name == SHIPPING_TYPE[service_name] ? -1 : 1;
      }
      return a.carrier == carrier ? -1 : 1;
    });
    return _services[0];
  }

}
