import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { PurchaseOrderApi } from '@etop/api';
import { PurchaseOrder } from 'libs/models/PurchaseOrder';
import { AuthenticateStore, requireMethodRoles } from '@etop/core';
import { Filters } from '@etop/models';

@Injectable()
export class PurchaseOrderService {
  constructor(
    private http: HttpService,
    private purchaseOrderApi: PurchaseOrderApi,
    private auth: AuthenticateStore
  ) { }

  @requireMethodRoles({
    skipError: true,
    valueOnError: {purchase_orders: []},
    roles: ['admin', 'owner', 'purchasing_management', 'accountant']
  })
  async getPurchaseOrders(start?: number, perpage?: number, filters?: Filters, variant_ids?: string[]) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };
    const res = await this.purchaseOrderApi.getPurchaseOrders({ paging, filters });

    return {
      ...res,
      variant_ids
    };
  }

  mapPurchaseOrderInfo(purchase_order: PurchaseOrder, index) {
    return {
      ...purchase_order,
      index
    }
  }

}
