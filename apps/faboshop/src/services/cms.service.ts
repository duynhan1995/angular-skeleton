import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AppService } from '@etop/web/core/app.service';

@Injectable()
export class CmsService {
  cms_banners: any = {};

  banners = [];
  banners_management = {
    'shipping_policy': 1,
    'transaction_note': 6,
    'pos_note': 9,
    'no_transaction_days': 11,
    'announcement_content': 19,
    'ghn_priority_package': 25,
    'ghtk_unavailable_text': 26,
    'carrier_description': 27,
    'header_announcement': 28,
    'user_manuals': 34,
    'welcome_contents_prod': 38,
    'welcome_contents_dev': 39
  };

  onBannersLoaded = new Subject();
  banner_loaded = false;

  constructor(
    private router: Router,
    private appService: AppService
  ) {
    this.getAppConfig();
    this.handlingRouterChange();
    this.getBanners();
  }

  getAppConfig() {
    this.cms_banners = this.appService.cms_banners;
  }

  handlingRouterChange() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.getBanners();
      }
    })
  }

  async getBanners() {
    try {
      for (let banner in this.cms_banners) {
        if (!this.cms_banners.hasOwnProperty(banner)) {
          continue;
        }
        if (this.cms_banners[banner] != 'cms_flexbox_settings') {
          this.banner_loaded = true;
          this.onBannersLoaded.next();
          return;
        }
      }
      const res = await fetch('https://www.etop.vn/api/get-banner').then(r =>
        r.json()
      );
      this.banners = res.data;
      this.banner_loaded = true;
      this.onBannersLoaded.next();
      return this.banners;
    } catch (e) {
      debug.error('ERROR in getting banners', e);
    }
  }

  getBannerInfo(banner_name) {
    try {
      const cms_banners = this.cms_banners;
      if (cms_banners[`cms_${banner_name}`] && cms_banners[`cms_${banner_name}`] != 'cms_flexbox_settings') {
        return {
          not_flexbox: true,
          content: cms_banners[`cms_${banner_name}`]
        };
      }
      if (!this.banners.length) {
        this.getBanners();
      }
      return this.banners[this.banners_management[banner_name]];
    } catch (e) {
      debug.log(`ERROR in getting banner info ${banner_name.toUpperCase()}`, e);
      return null;
    }
  }

  // SECTION TOPSHIP references
  getGHNPriorityPackage() {
    const banner = this.getBannerInfo('ghn_priority_package');
    return banner && banner.is_active === '1' && this.removeNewLineCharacters(this.stripHTML(banner.description)) || '';
  }

  getPOSNote() {
    const banner = this.getBannerInfo('pos_note');
    return banner && banner.is_active === '1' && banner.description || '';
  }

  getCarrierDescription() {
    const banner = this.getBannerInfo('carrier_description');
    return banner && banner.is_active === '1' && banner.description || '';
  }

  getGHTKUnavailableText() {
    const banner = this.getBannerInfo('ghtk_unavailable_text');
    return banner && banner.is_active === '1' && this.removeNewLineCharacters(this.stripHTML(banner.description)) || '';
  }
  // END Section TOPSHIP references

  // SECTION SELLER AFFILIATE references
  getWelcomeContentsProd() {
    const banner = this.getBannerInfo('welcome_contents_prod');
    return banner && banner.is_active === '1' && banner || '';
  }

  getWelcomeContentsDev() {
    const banner = this.getBannerInfo('welcome_contents_dev');
    return banner && banner.is_active === '1' && banner || '';
  }
  // END Section SELLER AFFILIATE references

  getAnnouncementContents() {
    const banner = this.getBannerInfo('announcement_content');
    if (banner && banner.not_flexbox) {
      return banner.content;
    }
    return banner && banner.is_active === '1' && banner.description || '';
  }

  getShippingPolicy() {
    const banner = this.getBannerInfo('shipping_policy');
    if (banner && banner.not_flexbox) {
      return banner.content;
    }
    return banner && banner.is_active === '1' && banner.url || '';
  }

  getTransactionNote() {
    const banner = this.getBannerInfo('transaction_note');
    if (banner && banner.not_flexbox) {
      return banner.content;
    }
    return banner && banner.is_active === '1' && banner.description || '';
  }

  getNoTransactionDays() {
    const banner = this.getBannerInfo('no_transaction_days');
    return banner && banner.is_active === '1' && this.removeNewLineCharacters(this.stripHTML(banner.description)) || '';
  }

  getUserManuals() {
    const banner = this.getBannerInfo('user_manuals');
    return banner && banner.is_active === '1' && banner || null;
  }

  stripHTML(html: string) {
    const _tempDIV = document.createElement("div");
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || "";
  }

  removeNewLineCharacters(str) {
    return str && str.replace(/\r?\n|\r/g, '') || '';
  }
}
