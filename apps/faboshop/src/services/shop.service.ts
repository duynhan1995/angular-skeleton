import { Injectable } from '@angular/core';
import List from "identical-list";
import { Subject } from 'rxjs';
import { HttpService } from '@etop/common';
import { AuthenticateStore } from '@etop/core';
import { ShopProduct, ProductSrc } from 'libs/models/Product';
import { Partner } from 'libs/models/Account';

@Injectable()
export class ShopService {
  onProductSrcUpdate = new Subject();

  productSrcList = new List<ProductSrc>();
  productIDList = new List<any>();
  productList = new List<ShopProduct>();
  total = 0;
  authorized_partners: Array<Partner> = [];

  onProductIDListUpdated$ = new Subject<List<any>>();
  onProductListUpdated$ = new Subject<List<ShopProduct>>();

  private _isLoading = true;
  loadingCategories = true;
  onLoadingCategoriesChange = new Subject<any>();
  onLoadingProductChange = new Subject<boolean>();

  get isLoading() {
    return this._isLoading;
  }

  set isLoading(loading) {
    this._isLoading = loading;
  }

  constructor(
    private http: HttpService,
    private auth: AuthenticateStore
  ) {
    this.isLoading = true;
  }

  loadProduct(query?: any, token?): Promise<Array<ShopProduct>> {
    let headers = this.http.createHeader(token);
    let options = this.http.createDefaultOption(headers);
    return this.http
      .postWithOptions(`api/shop.Product/GetProducts`, query || {}, options)
      .toPromise()
      .then(res => {
        res.products.sort(function(a, b) {
          let x: any = new Date(b.updated_at);
          let y: any = new Date(a.updated_at);
          return x - y;
        });
        this.total = res.paging.total;
        return res.products;
      })
      .catch(e => {
        debug.error('[ShopService:loadProduct]', e);
        return [];
      });
  }

  setDefaultAddress(address_id, type, token?) {
    if (token) {
      let headers = this.http.createHeader(token);
      let options = this.http.createDefaultOption(headers);
      return this.http
        .postWithOptions(
          `api/shop.Account/SetDefaultAddress`,
          {
            id: address_id,
            type: type
          },
          options
        )
        .toPromise();
    }

    return this.http
      .post(`api/shop.Account/SetDefaultAddress`, {
        id: address_id,
        type: type
      })
      .toPromise();
  }

  deleteShop(id: string) {
    return this.http
      .post(`api/shop.Account/DeleteShop`, { id: id })
      .toPromise();
  }

  updateMoneyTransactionCalendar(data) {
    return this.http.post('api/shop.Account/UpdateShop', data).toPromise();
  }

  updateBankAccount(bank_account, token) {
    return this.http
      .post('api/shop.Account/UpdateShop', { bank_account }, token)
      .toPromise();
  }

  updateShopField(data) {
    return this.http.post('api/shop.Account/UpdateShop', data).toPromise();
  }

  updateCompanyInfo(body: any) {
    return this.http
      .post('api/shop.Account/UpdateShop', { company_info: body })
      .toPromise();
  }

  addNewProductSource(shopName?: string, token?: string) {
    const productSrc = new ProductSrc({
      type: 'custom',
      name: shopName || this.auth.snapshot.shop.name
    });
    return this.createProductSource(productSrc, token);
  }

  createProductSource(data: ProductSrc, token?: string): Promise<ProductSrc> {
    if (!data.name) {
      throw new Error("Product source name can't be null!");
    }
    if (token) {
      const options = this.http.createCustomDefaultOption(token);
      return this.http
        .postWithOptions(
          'api/shop.ProductSource/CreateProductSource',
          data,
          options
        )
        .toPromise();
    }
    return this.http
      .post('api/shop.ProductSource/CreateProductSource', data)
      .toPromise();
  }

  calcShopBalance() {
    return this.http
      .post(`/api/shop.Summary/CalcBalanceShop`, {})
      .toPromise()
      .then(res => {
        return res.balance;
      });
  }

  getAuthorizedPartners() {
    return this.http
      .post('api/shop.Authorize/GetAuthorizedPartners', {})
      .toPromise()
      .then(res => {
        this.authorized_partners = res.partners.map(p => p.partner);
        return res;
      });
  }

  pullProducts() {
    this._loadProduct(10000);
  }

  private async _loadProduct(limit) {
    try {
      let products: any = await this.loadProduct({ paging: { limit: limit } });
      this.productList = new List(products);
      this.productIDList = new List(products.map(p => p.id));
      this._invokeUpdate();
    } catch (e) {}
    this.isLoading = false;
    this.onLoadingProductChange.next(this.isLoading);
  }

  private _invokeUpdate() {
    this.onProductListUpdated$.next(this.productList);
    this.onProductIDListUpdated$.next(this.productIDList);
  }
}
