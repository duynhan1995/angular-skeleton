import {Injectable} from '@angular/core';
import {OrderService} from "./order.service";
import {FulfillmentService} from "./fulfillment.service";
import * as moment from "moment";
import { HttpService } from '@etop/common';
import { UtilService } from 'apps/core/src/services/util.service';
import { StatLabel, StatTable, StatValue } from 'apps/faboshop/src/models/stat';

@Injectable()
export class StatisticService {

  ready = false;

  constructor(private orderService: OrderService,
              private fulfillmentService: FulfillmentService,
              private utilService: UtilService,
              private http: HttpService
  ) {
  }
  summarizePOS({ date_from, date_to }) {
    date_from = date_from || moment().format("YYYY-MM-DD");
    date_to = date_to || moment().format("YYYY-MM-DD");
    return this.http
      .post(`api/shop.Summary/SummarizePOS`, {
        date_from,
        date_to
      })
      .toPromise()
      .then(res => res.tables)
  }

  summarizeTopship({ date_from, date_to }) {
    date_from = date_from || moment().format("YYYY-MM-DD");
    date_to = date_to || moment().format("YYYY-MM-DD");
    return this.http
      .post(`api/shop.Summary/SummarizeTopShip`, {
        date_from,
        date_to
      })
      .toPromise()
      .then(res => res.tables);
  }

  loadStat({date_from, date_to}) {
    date_from = date_from || moment().startOf("month").format("YYYY-MM-DD");
    date_to = date_to || moment().endOf("month").format("YYYY-MM-DD");
    return this.http.post(`api/shop.Summary/SummarizeFulfillments`, {
      date_from,
      date_to
    }).toPromise()
      .then(res => res.tables)
      .then(tables => tables.map(table => this._tableMap(table)));
  }

  private _tableMap(table) {
    const nameMap = {
      "unpaid_ffm": "overview_before_cross_check",
      "order_status": "shipping_status_before_cross_check",
      "ffm_expected_delivery": "expected_delivery_at_before_cross_check",
      "created_order": "created_at_before_cross_check",
      "shipping_provider": "shipping_provider_after_cross_check",
      "created_order_after": "created_at_after_cross_check",
      "average_create_order_after": "average_created_at_after_cross_check"
    };
    let statTable = new StatTable({});
    let labelTokens = table.label.replace(/[\n\r]/, ":::").match(/(.+)\[(.*)]/);
    if (labelTokens) {
      statTable.label = labelTokens[1];
      statTable.subLabels = labelTokens[2].split(":::");
    } else {
      statTable.label = table.label;
    }

    for (let name of Object.keys(nameMap)) {
      let tag = nameMap[name];
      if (table.tags.some(t => t == tag)) {
        statTable.name = name;
      }
    }

    statTable.columns = table.columns.map(c => {
      let tokens = c.label.match(/(.+)\[(.*)]/);
      if (tokens) {
        c.label = tokens[1];
        c.label2 = tokens[2];
      }
      return new StatLabel(c);
    });
    statTable.rows = table.rows.map(r => {
      let tokens = r.label.match(/(.+)\[(.*)]/);
      if (tokens) {
        r.label = tokens[1];
        r.label2 = tokens[2];
      }
      return new StatLabel(Object.assign(r, {
        className: this._classMap(r.spec),
        render: this._renderMap(r.spec)
      }));
    });

    table.data.forEach((d, index) => {
      let row = statTable.rows[index];
      row.cells = d.map(c => new StatValue(c));
    });

    statTable = this._additionStatCalc(statTable);

    return statTable;
  }

  private _additionStatCalc(statTable: StatTable): StatTable {

    switch (statTable.name) {
      case "unpaid_ffm": {
        if (!statTable.rows.length) {
          break;
        }
        let summaryRow = new StatLabel({
          label: "Còn lại",
          spec: "summary",
          className: "summary-row",
          render: this._renderMap("summary"),
          cells: []
        });
        let rows = statTable.rows;
        for (let i = 0; i < rows[0].cells.length; i++) {
          let sum = 0;
          rows.forEach(row => {
            let cell = row.cells[i];
            if (cell.spec.indexOf("sum") < 0) {
              return;
            }
            sum += cell.spec.indexOf("fee") > -1 ? -cell.value : cell.value;
          });
          summaryRow.cells[i] = new StatValue({
            value: sum
          });
        }

        statTable.rows.push(summaryRow);
      }
    }

    return statTable;
  }

  private _classMap(spec: string) {
    const classMap = {
      "sub-row": ["count:cod", "count:!cod", "count:shipping_status=NS", "count:shipping_status=!NS"],
      "summary": ["summary"]
    };
    let className = this.utilService.matchKeyArray(classMap, spec);
    return className || "";
  }

  private _renderMap(spec: string) {
    const labelMap = {
      "normal": [
        "count:", "count:cod", "count:!cod", "count:shipping_status=NS", "count:cod,shipping_status=P|S",
        "count:!cod,shipping_status=P|S", "count:shipping_state=returning|returned", "count:shipping_status!=returning|returned",
        "count:shipping_status=!Z", "count:shipping_status=!Z,cod", "count:shipping_status=!Z,!cod",
        "count:shipping_status=!NS", "count:shipping_status=NS",
        "count:shipping_status=P",
      ]
    };
    const renderMap = {
      "currency": (value) => this.utilService.formatCurrency(value) + " đ"
    };

    for (let label of Object.keys(labelMap)) {
      if (labelMap.hasOwnProperty(label)) {
        let specs = labelMap[label];
        let check = specs.some(s => s == spec);
        if (check) {
          return renderMap[label];
        }
      }
    }

    return (value) => this.utilService.formatCurrency(value) + " đ";
  }

}
