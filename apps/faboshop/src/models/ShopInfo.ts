export class ShopAddressModel {
  id?: string;
  province?: string;
  province_code: string;
  district?: string;
  district_code: string;
  ward?: string;
  ward_code: string;
  address1: string;
}

export class ShopInfoModel {
  id: string;
  name: string;
  address: ShopAddressModel = new ShopAddressModel();
  phone?: string;
  bank_account: ShopBankModel =  new ShopBankModel();
  website_url?: string;
  image_url?: string;
  email?: string;
  url_slug?: string;
  try_on: string;
  token: string;
  money_transaction_rrule?: string;

  onSubmit: (ShopInfoModel) => void;

  constructor(shop?: ShopInfoModel) {
    if (shop) {
      this.name = shop.name;
      this.address = shop.address;
      this.phone = shop.phone;
      this.bank_account = shop.bank_account;
      this.website_url = shop.website_url;
      this.image_url = shop.image_url;
      this.email = shop.email;
    }
  }
}

export class ShopBankModel {
  name?: string;
  province?: string;
  branch?: string;
  account_number?: string;
  account_name?: string;
}
