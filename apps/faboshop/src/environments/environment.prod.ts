export const environment = {
  production: true,
  hmr: false,

  base_url: "https://etop.vn",
  app: "dashboard",
  ticket_environment: "production",
  admin_url: "https://admin.etop.vn",
  crm_url: "https://crm-service.etop.vn",
  prev_url: "https://prev.etop.vn",
  recaptcha_key: '6LdKXAEVAAAAAFGWxbyZKcVeZcrpBfMVjRhj0hX9',
  onesignal_app_id: "8a018f7c-fa32-4d33-ae19-dd97dc1c4364",
  firebase_sender_id: "13267590075",
  safari_web_id: "665e4130-cb92-443f-ad4d-8156961c0995",
};
