export const environment = {
  production: true,
  hmr: false,

  base_url: "https://shop.sandbox.etop.vn",
  app: "dashboard",
  ticket_environment: "sandbox",
  admin_url: "https://admin.d.etop.vn",
  crm_url: "https://crm-service.d.etop.vn",
  prev_url: "https://prev.d.etop.vn",
  recaptcha_key: '6LcVOnkUAAAAAGd1izSWEZduQuBcExcbVfQMS-7Y',
  onesignal_app_id: "5186e97c-b919-4eb0-8740-ac5c12f954ae",
  firebase_sender_id: "13267590075",
  safari_web_id: '2e21fe47-8329-4413-bae9-ecef4da3342d',
};
