import { Injectable } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AuthenticateStore } from '@etop/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { ShopCrmService } from '../../services/shop-crm.service';
import { NotificationService } from '../../services/notification.service';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { NotificationApi, UserApi } from '@etop/api';
import { AppService } from '@etop/web/core/app.service';
import { UtilService } from '../../../../core/src/services/util.service';

const SPECIAL_ROUTES = {
  'top-ship': {
    display_on_mobile: true,
    require_login: false
  },
  tracking: {
    display_on_mobile: true,
    require_login: false
  },
  'reset-password': {
    display_on_mobile: true,
    require_login: false
  },
  'on-boarding': {
    display_on_mobile: true,
    require_login: true
  },
  'admin-login': {
    display_on_mobile: true,
    require_login: false
  },
  'verify-email': {
    display_on_mobile: true,
    require_login: true
  },
  'invitation': {
    display_on_mobile: true,
    require_login: false
  }
};

@Injectable()
export class ShopCommonUsecase extends CommonUsecase {
  app = 'dashboard';
  full_name;
  password;
  email;
  confirm;
  error = false;

  signupData: any = {};

  loading = false;

  provinces;

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore,
    private notificationApi: NotificationApi,
    private util: UtilService,
    private crmService: ShopCrmService,
    private notification: NotificationService,
    private activatedRoute: ActivatedRoute,
    private appService: AppService
  ) {
    super();
  }

  private checkInvitationToken() {
    return this.activatedRoute.snapshot.queryParamMap.get('invitation');
  }

  private checkInvitationTokenUserPhone() {
    return this.activatedRoute.snapshot.queryParamMap.get('type');
  }

  async checkAuthorization(fetchAccount = false) {
    let appConfig = await this.appService.getAppConfig();
    const ref = this.router.url.split('?')[0];
    try {
      const url = window.location.pathname.replace('/', '');
      const special = SPECIAL_ROUTES[url];
      if (special && !special.require_login) {
        this.auth.setRef(this.router.url);
        return;
      }

      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(fetchAccount);
      if (!this.auth.snapshot.shop) {
        this.router.navigateByUrl('/survey');
      }

      const pathname = this.router.url.split('?')[0];
      if (pathname.startsWith('/s/')) {
        const index = pathname.split('/')[2];
        this.auth.selectAccount(index);
      } else {
        this.auth.selectAccount(0);
      }

      await this.navigateAfterAuthorized();
    } catch (e) {
      debug.error('ERROR in checkAuthorization', e);
      this.auth.clear();
      this.auth.setRef(ref);

      if (appConfig.disableUser) {
        location.href = appConfig.redirectUserLink || '/';
        return;
      }

      if (ref.indexOf('register') == -1) {
        if (ref.indexOf('login') != -1) {
          await this.router.navigate(
            ['/login'],
            {
              queryParams: { invitation: this.checkInvitationToken() ,type: this.checkInvitationTokenUserPhone()}
            });
        } else {
          await this.router.navigate(
            ['/register'],
            {
              queryParams: { invitation: this.checkInvitationToken(),type: this.checkInvitationTokenUserPhone() }
            });
        }
      }
    }
  }

  async navigateAfterAuthorized() {
    let ref = this.auth.getRef(true).replace('/', '');
    const defaultRef = 'messages';
    if (ref == 'login' || ref == 'register') {
      await this.router.navigate([`./${defaultRef}`]);
    } else {
      await this.router.navigateByUrl(ref);
    }
    await this.registerDeviceForNoti();
  }

  async login(data: { login: string; password: string }, after_register?: boolean) {
    try {
      const res = await this.userApi.login({
        login: data.login,
        password: data.password,
        account_type: 'shop'
      });

      this.auth.updateToken(res.access_token);
      this.auth.updateUser(res.user);
      const invitation_token = this.activatedRoute.snapshot.queryParamMap.get('invitation');
      const invitation_type = this.activatedRoute.snapshot.queryParamMap.get('type');
      await this.registerDeviceForNoti();
      await this.setupAndRedirect(invitation_token, after_register,invitation_type);
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
  }

  async setupAndRedirect(invitation_token?: string, after_register?: boolean,invitation_type?: string) {
    await this.updateSessionInfo(true);
    this.auth.selectAccount(0);
    if (invitation_token) {
      if (invitation_type == 'phone') {
        await this.router.navigateByUrl(`/i/p${invitation_token}`)
      } else {
        await this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug || 0}/messages`);
      }
    } else {
      const ordersCount = 0;
      const ref = this.auth.getRef(true);
      await this.router.navigateByUrl(
        `/s/${this.auth.snapshot.account.url_slug || 0}/${ref || (ordersCount >= 0 ? 'messages' : 'guide')}`
      );
    }
  }

  async updateSessionInfo(fetchAccounts = false) {
    const res = await this.userService.checkToken(this.auth.snapshot.token);
    let { access_token, account, shop, user, available_accounts } = res;
    const shop_accounts = available_accounts.filter(a => a.type === 'shop').sort((a, b) => a.id > b.id);

    let no_init_shop = true;

    if (shop) {
      no_init_shop = false;
      this.crmService.updateLeadAndContact(user, shop, shop.address);
    }
    if (!shop && available_accounts && available_accounts.length) {
      shop = shop_accounts[0];
      account = shop_accounts[0];
    }
    const accounts: Account[] = fetchAccounts ? await Promise.all(shop_accounts.map(async (a, index) => {
      const accRes = await this.userService.switchAccount(a.id);
      a.token = accRes.access_token;
      a.shop = accRes.shop;
      a.id = accRes.shop && accRes.shop.id;
      a.image_url = a.shop.image_url;
      a.display_name = `${a.shop.code} - ${a.shop.name}`;
      a.permission = accRes.account.user_account.permission;
      return new Account(a);
    })) : this.auth.snapshot.accounts;

    if (accounts.length > 0) {
      this.auth.updateInfo({
        token: no_init_shop && accounts[0].token || access_token,
        account: {
          ...account,
          ...shop,
          display_name: `${shop.code} - ${shop.name}`
        },
        accounts,
        shop,
        user,
        permission: account.user_account.permission,
        isAuthenticated: true,
        uptodate: true
      });
    }
  }

  async register(data: any, source = '') {
    const res = await this.userService.signUpUsingToken({ ...data, source });
    this.signupData = Object.assign({}, data, res.user);
    toastr.success('Đăng ký thành công!');

    const loginRes = await this.userApi.login({
      login: data.phone,
      password: data.password,
      account_type: 'shop'
    });

    this.auth.updateToken(loginRes.access_token);
    this.auth.updateUser(res.user);

    const shopRes = await this.userService.registerShop(new ExtendedAccount({
      phone: data.phone,
      name: data.shop_name
    }));
    const account = await this.userService.switchAccount(shopRes.shop.id);
    this.auth.updateToken(account.access_token);
    const invitation_token = this.activatedRoute.snapshot.queryParamMap.get('invitation');
    const invitation_type = this.activatedRoute.snapshot.queryParamMap.get('type');
    await this.setupAndRedirect(invitation_token, true,invitation_type);
  }

  async registerDeviceForNoti() {
    try {
      const device = this.auth.snapshot.oneSignal;
      if (this.notification.oneSignalInitialized) {
        OneSignal.getUserId().then(async (external_device_id) => {
          if (!device) {
            await this.notificationApi.createDevice({ device_id: external_device_id, external_device_id });
            this.auth.updateOneSignal({ device_id: external_device_id, external_device_id })
          }
        });
      } else {
        this.notification.onOneSignalInitialized.subscribe(async _ => {
          OneSignal.getUserId().then(async (external_device_id) => {
            if (!device) {
              await this.notificationApi.createDevice({ device_id: external_device_id, external_device_id });
              this.auth.updateOneSignal({ device_id: external_device_id, external_device_id })
            }
          });
        });
      }
    } catch (e) {
      debug.log('ERROR in registerDeviceForNoti', e);
    }

  }


  async redirectIfAuthenticated(): Promise<any> {
    return this.checkAuthorization(true);
  }

}

