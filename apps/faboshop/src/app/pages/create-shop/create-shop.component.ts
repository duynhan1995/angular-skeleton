import { Component, OnInit } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Address } from 'libs/models/Address';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { ExtendedAccount } from 'libs/models/Account';
import { UserService } from 'apps/core/src/services/user.service';
import { AddressService } from 'apps/core/src/services/address.service';
import { ShopCrmService } from 'apps/faboshop/src/services/shop-crm.service';
import { Router } from '@angular/router';
import { MoneyTransactionService } from 'apps/faboshop/src/services/money-transaction.service';
import { CmsService } from 'apps/faboshop/src/services/cms.service';
import {LocationQuery} from "@etop/state/location/location.query";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {CommonUsecase} from "apps/shared/src/usecases/common.usecase.service";
import {AccountApi} from "@etop/api";

@Component({
  selector: 'shop-create-shop',
  templateUrl: './create-shop.component.html',
  styleUrls: ['./create-shop.component.scss']
})
export class CreateShopComponent extends BaseComponent implements OnInit {
  uploading = false;
  loading = false;

  shop = new ExtendedAccount({
    address: new Address({})
  });
  address = new Address({});

  start_open_time = '';
  end_open_time = '';
  start_lunch_break = '12:00';
  end_lunch_break = '13:30';
  notLunchBreak = true;
  error = false;
  callOption = 'no-call';

  morning = false;
  afternoon = false;
  evening = false;
  enter_time = false;

  shipping_policy_accept = false;
  getShippingPolicyLink = '';

  user: any;

  locationForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
    wardCode: ''
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.locationForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.locationForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private util: UtilService,
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private userService: UserService,
    private addressesService: AddressService,
    private shopCrm: ShopCrmService,
    private cms: CmsService,
    private router: Router,
    private commonUsecase: CommonUsecase,
    private accountApi: AccountApi,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  async ngOnInit() {
    this.user = this.auth.snapshot.user;
    this.shop.name = this.user.full_name;
    this.shop.email = this.user.email;
    this.shop.phone = this.user.phone;
    this.onChangeShopName();
    this.getShippingPolicy();

    this.locationForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.locationForm.patchValue({
          districtCode: '',
          wardCode: '',
        });
        this.shop.address.province_code = value;
      });

    this.locationForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.locationForm.patchValue({
          wardCode: '',
        });
        this.shop.address.district_code = value;
      });

    this.locationForm.controls['wardCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.shop.address.ward_code = value;
      });
  }

  getShippingPolicy() {
    if (this.cms.banner_loaded) {
      this.getShippingPolicyLink = this.cms.getShippingPolicy();
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        this.getShippingPolicyLink = this.cms.getShippingPolicy();
      });
    }
  }

  signout() {
    this.auth.clear();
    this.router.navigateByUrl('/login');
  }

  async onFileSelected($e) {
    this.uploading = true;
    try {
      const { files } = $e.target;
      const res = await this.util.uploadImages([files[0]], 250);
      this.shop.image_url = res[0].url;
    } catch (e) {
      toastr.error(e.message, 'Chọn logo thất bại');
    }
    this.uploading = false;
  }

  toggleEnterTime() {
    if (this.enter_time) {
      this.afternoon = false;
      this.evening = false;
      this.morning = false;
    }
  }

  toggleSession() {
    this.enter_time = false;
    if (this.morning) {
      this.start_open_time = '08:00';
      this.end_open_time = '12:00';
    }

    if (this.afternoon) {
      this.end_open_time = '18:00';
      if (!this.morning) {
        this.start_open_time = '12:00';
      }
    }
    if (this.evening) {
      this.end_open_time = '21:00';
      if (!this.morning) {
        this.start_open_time = '12:00';
      }
      if (!this.afternoon) {
        this.start_open_time = '18:00';
      }
    }
  }

  private validateShop(shop: ExtendedAccount) {
    shop = this.util.trimFields(shop, [
      'name',
      'email',
      'address1',
      'website_url'
    ]);
    if (!shop.name) {
      toastr.error(`Vui lòng điền tên cửa hàng`);
      return false;
    }

    if (shop.website_url &&
      !shop.website_url.match(
        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
      )
    ) {
      toastr.error(`Vui lòng điền địa chỉ website hợp lệ`);
      return false;
    }

    let phone = (shop.phone && shop.phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    phone = (phone && phone.split('-test')[0]) || '';
    const valid_phone = this.util.validatePhoneNumber(phone);
    if (!valid_phone) {
      return false;
    }

    let email = (shop.email && shop.email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    email = (email && email.split('-test')[0]) || '';
    if (
      email &&
      !email.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      toastr.error('Vui lòng nhập email hợp lệ!');
      return false;
    }

    return true;
  }

  private validateAddress(address: Address) {
    if (!address.full_name) {
      toastr.error('Vui lòng nhập tên người phụ trách');
      return false;
    }
    if (!address.address1) {
      toastr.error(`Vui lòng nhập địa chỉ cửa hàng`);
      return false;
    }
    if (!address.province_code) {
      toastr.error(`Vui lòng chọn tỉnh thành`);
      return false;
    }
    if (!address.district_code) {
      toastr.error(`Vui lòng chọn quận huyện`);
      return false;
    }
    if (!address.ward_code) {
      toastr.error(`Vui lòng chọn phường xã`);
      return false;
    }
    if (!this.start_open_time || !this.end_open_time) {
      toastr.error('Vui lòng nhập khung giờ lấy hàng!');
      return false;
    }
    if (!this.notLunchBreak && (!this.start_lunch_break || !this.end_lunch_break)) {
      toastr.error('Vui lòng nhập giờ nghỉ trưa');
      return false;
    }
    return true;
  }

  async confirm() {
    this.loading = true;
    try {
      const shop_data = {
        ...this.shop,
        auto_create_ffm: true,
        address: {
          ...this.shop.address,
          phone: this.shop.phone,
          type: 'shipfrom',
          notes: {
            other: this.callOption,
            open_time: this.start_open_time + ' - ' + this.end_open_time,
            lunch_break: this.notLunchBreak ? '' : (this.start_lunch_break + ' - ' + this.end_lunch_break)
          }
        }
      };
      const valid_shop = this.validateShop(shop_data);
      if (!valid_shop) {
        return this.loading = false;
      }

      const valid_address = this.validateAddress(shop_data.address);
      if (!valid_address) {
        return this.loading = false;
      }

      shop_data.survey_info = [
        {
          key: 'orders_per_day',
          question: 'Số lượng đơn hàng mỗi ngày',
          answer: JSON.parse(localStorage.getItem('survey'))
            ? JSON.parse(localStorage.getItem('survey')).orders_per_day_text
            : null
        },
        {
          key: 'business',
          question: 'Ngành hàng kinh doanh',
          answer: JSON.parse(localStorage.getItem('survey'))
            ? JSON.parse(localStorage.getItem('survey')).business_lines
            : null
        }
      ];
      shop_data.money_transaction_rrule = MoneyTransactionService.defaultRrule;

      const res = await this.userService.registerShop(shop_data);
      const account = await this.userService.switchAccount(res.shop.id);
      this.auth.updateToken(account.access_token);

      const url_slug = this.util.createHandle(res.shop.name) + '-' + res.shop.code.toLowerCase();
      await this.accountApi.updateURLSlug({
        account_id: res.shop.id,
        url_slug: url_slug
      });
      const address = await this.createAddress(shop_data.address);

      await this.commonUsecase.updateSessionInfo(true);

      const slug = this.util.getSlug();
      this.router.navigateByUrl(`/s/${slug}/dashboard`);
      toastr.success('Tạo cửa hàng thành công.');
    } catch (e) {
      toastr.error('Tạo cửa hàng không thành công.');
      debug.log('ERROR in Creating Shop', e);
    }
    this.loading = false;
  }

  async createAddress(address_data) {
    try {
      const address = await this.addressesService.createAddress(address_data);
      if (address) {
        await this.shopService.setDefaultAddress(
          address.id,
          'shipfrom',
          this.auth.snapshot.token
        );
      }
      return address;
    } catch(e) {
      debug.log('ERROR in Creating Address', e);
      return null;
    }
  }

  onChangeShopName() {
    this.shop.url_slug = this.util.createHandle(this.shop.name);
  }

}
