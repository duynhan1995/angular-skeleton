import {AfterContentChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import { VND } from '@etop/shared/pipes/etop.pipe';
import moment from 'moment';
import { Moment } from 'moment';
import {StatisticService} from '@etop/features/fabo/statistic/';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterContentChecked {
  loading = false;

  chartDate: any;
  chartCod: any;
  chartSales: any;
  chartReturned: any;

  dateArr = [];

  chartData: any;
  selected: { startDate: Moment; endDate: Moment, type?:string};

 
  rangeDate: string;
  rangeLabel = 'Trong tuần này';

  detailStatsTop = [
    {
      name: 'Doanh thu theo sản phẩm',
      field: 'sales'
    },
    {
      name: 'Doanh thu theo thu hộ (COD)',
      field: 'salesCod'
    },
    {
      name: 'Khách hàng mới',
      field: 'newCustomer'
    },
    {
      name: 'Đã chốt đơn',
      field: 'confirmed'
    }
  ];

  detailStatsBot = [
    {
      name: 'Bàn giao NVC',
      field: 'delivered'
    },
    {
      name: 'Đã giao thành công',
      field: 'successed'
    },
    {
      name: 'Trả hàng về',
      field: 'returned'
    },
    {
      name: 'Huỷ',
      field: 'deleted'
    },
  ];

  todayStat = {
    sales: 0,
    salesCod: 0,
    newCustomer: 0,
    confirmed: 0,
    delivered: 0,
    successed: 0,
    returned: 0,
    deleted:0
  }

  filterStat = {
    sales: 0,
    salesCod: 0,
    newCustomer: 0,
    confirmed: 0,
    delivered: 0,
    successed: 0,
    returned: 0,
    deleted:0
  }

  currentChartStat: any;
  preChartStat: any;

  displayedColumns =
      ['user', 'new_customer', 'distinct_customer', 'message', 'comment', 'order', 'basket_value', 'total_cod_amount'];
  dataStaff = [];
  private transformMoney = new VND();

  constructor(
    private auth: AuthenticateStore,
    private cdref: ChangeDetectorRef,
    private statisticService: StatisticService,
    private relationshipsQuery: RelationshipQuery,
    private relationshipsService: RelationshipService
  ) {
    this.selected = {
      startDate: moment().startOf('isoWeek'),
      endDate: moment(),
      type: 'week',
    };
    this.rangeDate = '(' + moment(this.selected.startDate).format('DD/MM/YY')
    + ' - ' + moment(this.selected.endDate).format('DD/MM/YY') + ')';
  }

  async ngOnInit() {
    this.getArrdays();
    await this.loadStat();
    await this.drawChart();
  }

  async loadStat() {
    await this.relationshipsService.getRelationships();
    this.preChartStat = {
      basket_value: [],
      total_cod_amount: [],
      delivered: [],
      returned: []
    };
    const formatDate = {
      date_from: moment(this.selected.startDate).format('YYYY-MM-DD'),
      date_to: moment(this.selected.endDate).add(1, 'days').format('YYYY-MM-DD')
    }
    const stat = await this.statisticService.getSummaryShop(formatDate);

    this.todayStat = {
      sales: stat.today.basket_value,
      salesCod: stat.today.total_cod_amount,
      newCustomer: stat.today.new_customer,
      confirmed: stat.today.shop_confirm,
      delivered: stat.today.carrier,
      successed: stat.today.delivered,
      returned: stat.today.returned,
      deleted:stat.today.cancelled
    }
    this.filterStat = {
      sales: stat.total.basket_value,
      salesCod: stat.total.total_cod_amount,
      newCustomer: stat.total.new_customer,
      confirmed: stat.total.shop_confirm,
      delivered: stat.total.carrier,
      successed: stat.total.delivered,
      returned: stat.total.returned,
      deleted:stat.total.cancelled
    }

    this.currentChartStat = stat.charts;

    if (this.selected.type == 'week') {
      const formatlastDate = {
        date_from: moment().subtract(1, 'week').startOf('week').add(1, 'days').format('YYYY-MM-DD'),
        date_to: moment().subtract(1, 'week').endOf('week').add(2, 'days').format('YYYY-MM-DD')
      }
      const  lastStat= await this.statisticService.getSummaryShop(formatlastDate);
      this.preChartStat = lastStat.charts;
    }
    if (this.selected.type == 'month') {
      const formatlastDate = {
        date_from: moment().subtract(1, 'months').startOf('months').format('YYYY-MM-DD'),
        date_to: moment().subtract(1, 'months').endOf('months').add(1, 'days').format('YYYY-MM-DD')
      }
      const  lastStat= await this.statisticService.getSummaryShop(formatlastDate);
      this.preChartStat = lastStat.charts;
    }
    stat.staff.map(staff => {
      staff.user = this.relationshipsQuery.getRelationshipNameById(staff.user);
    });
    this.dataStaff = stat.staff;
  }

  async drawChart() {
    await this.drawRevenueChart();
    await this.drawRevenueCODChart();
    await this.drawSaledChart();
    await this.drawReturnedChart();
    this.rangeLabel = '';
  }
  async onChangeFilter(event) {
    if(event.startDate && moment(event.endDate).subtract(1, 'years')
    > moment(event.startDate)) {
      this.selected = {
        startDate: moment().startOf('isoWeek'),
        endDate: moment(),
        type: 'week',
      };
      this.rangeDate = '(' + moment(this.selected.startDate).format('DD/MM/YY')
      + ' - ' + moment(this.selected.endDate).format('DD/MM/YY') + ')';
      toastr.error('Không thể chọn thời gian quá 1 năm.')
      return;
    }
    if (event.startDate) {
      this.selected = {...this.selected,
        startDate: event.startDate,
        endDate: event.endDate
      };
    }
    this.dateArr = [];
    this.getArrdays();
    await this.loadStat();
    await this.drawChart();
  }

  rangeClicked(e) {
    this.rangeLabel = e.label;
  }

  getArrdays() {
    let start = new Date(Number(this.selected.startDate));
    let end = new Date(Number(this.selected.endDate));

    switch(this.rangeLabel?.toUpperCase()) {
      case 'TRONG TUẦN NÀY':
        this.selected.type = 'week';
        this.dateArr=['T2','T3','T4','T5','T6','T7','CN'];
        this.rangeDate = '(' + moment(this.selected.startDate).format('DD/MM/YY')
          + ' - ' + moment(this.selected.endDate).format('DD/MM/YY') + ')';
        break;
      case 'TRONG THÁNG NÀY':
        this.selected.type = 'month';
        const max = Math.max(moment().subtract(1, "month").daysInMonth(), Math.max(moment().daysInMonth()));
        for (let index = 1; index < max; index++) {
          this.dateArr.push(index);
        }
        this.rangeDate = '(' + moment(this.selected.startDate).format('DD/MM/YY')
          + ' - ' + moment(this.selected.endDate).format('DD/MM/YY') + ')';
          break;
      default:
        this.rangeDate = '';
        while (start < end) {
          this.dateArr.push(moment(start).format('D/M'));
          let newDate = start.setDate(start.getDate() + 1);
          start = new Date(newDate);
        }
    }

  }

  async drawRevenueChart() {
    const orderChart = document.getElementById('orders_revenue');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');

    if (this.chartDate) {
      this.chartDate.destroy();
    }
    const config = this.configChart(this.currentChartStat.basket_value, this.preChartStat.basket_value, 'Doanh thu');
    this.chartDate = new Chart(ctx, config);
  }

  async drawRevenueCODChart() {

    const orderChart = document.getElementById('orders_revenue_cod');
    if (!orderChart) {
      return;
    }
    if (this.chartCod) {
      this.chartCod.destroy();
    }
    let ctx = (orderChart as any).getContext('2d');
    const config = this.configChart(this.currentChartStat.total_cod_amount, this.preChartStat.total_cod_amount, 'Doanh thu');

    this.chartCod = new Chart(ctx, config);
  }

  async drawSaledChart() {

    const orderChart = document.getElementById('orders_saled');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    if (this.chartSales) {
      this.chartSales.destroy();
    }
    const config = this.configChart(this.currentChartStat.delivered, this.preChartStat.delivered, 'Đơn hàng');
    this.chartSales = new Chart(ctx, config);
  }

  async drawReturnedChart() {

    const orderChart = document.getElementById('orders_returned');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    if (this.chartReturned) {
      this.chartReturned.destroy();
    }
    const config = this.configChart(this.currentChartStat.returned, this.preChartStat.returned, 'Đơn hàng trả về');
    this.chartReturned = new Chart(ctx, config);
  }

  mapLabel(type) {
    const date = this.selected;
    const from = date.startDate;
    const to = date.endDate;
    switch(type) {
      case 'week': {
        return {
          toLabel: `Tuần này (${from.format('D/M')}-${to.format('D/M')})`,
          fromLabel: `Tuần trước (${moment().subtract(1, 'week').startOf('week').add(1, 'days').format('D/M')}-${moment().subtract(1, 'week').endOf('week').add(1, 'days').format('D/M')})`,
        }
      }
      case 'month': {
        return {
          toLabel: `Tháng này`,
          fromLabel: `Tháng trước`,
        }
      }
      default:{
        return {
          toLabel: `Tùy chọn (${from.format('D/M')}-${to.format('D/M')})`,
          fromLabel: `Tùy chọn (${from.format('D/M')}-${to.format('D/M')})`,
        }
      }
    }
  }

  configChart(currentData, preData, title) {
    const label = this.mapLabel(this.selected.type);
    let _data = preData.map(data => data.value);
    let _data_2 = currentData.map(data => data.value);
    const max_value = Math.max(..._data, ..._data_2);
    let heightyAxes =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      heightyAxes = 1000000;
    }

    let dataset = [{
      label: label.toLabel,
      borderColor: '#2851a3',
      borderWidth: 1,
      lineTension: 0,
      pointBackgroundColor:'#2851a3',
      fill: false,
      data: _data_2
    }]
    if (preData) {
      dataset.push({
        label: label.fromLabel,
        borderColor: '#ffdd30',
        borderWidth: 1,
        lineTension: 0,
        pointBackgroundColor:'#ffdd30',
        fill: false,
        data: _data
      })
    }
    let config = {
      type: 'line',
      data: {
        labels: this.dateArr,
        datasets: dataset
      },
      options: {
        elements: {
          point:{
              radius: 0
          }
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle:true,
            boxWidth: 5,
          },
        },
        title: {
          display: true,
          position: 'left',
          text: title
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              return (
                tooltipItem.yLabel
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, '.') + 'đ'
              );
            }
          }
        },
        scales: {
          xAxes: [
            {
              gridLines:{
                display: false
              },
              display: true,
              ticks: {
                maxTicksLimit: 15,
                beginAtZero: false,
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                fontSize: 10,
                maxTicksLimit: 15,
                callback: (value) => this.transUint(value),
                beginAtZero: false,
                stepSize: Math.ceil(heightyAxes / 5),
                max: heightyAxes
              }
            }
          ]
        },
      }
    };
    return config;
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  hasViewPermission() {
    return this.auth.snapshot.permission?.permissions.includes('shop/dashboard:view');
  }

  transUint(value) {
    return value < 1000000
      ? value < 1000
        ? value
        : this.transformMoney.transform(value / 1000) + 'K'
      : this.transformMoney.transform(value / 1000000) +
          'triệu';
  }
}
