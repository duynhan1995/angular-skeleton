import { Component, OnInit } from '@angular/core';
import { ConversationAPI, PageApi } from '@etop/api';
import { CustomerConversation } from '@etop/models';
import { ChatConnectionModalComponent } from '../../components/modals/chat-connection-modal/chat-connection-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { takeUntil } from 'rxjs/operators';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { FbPagesQuery, FbPagesService } from '@etop/state/fabo/page';
import { ActivatedRoute } from '@angular/router';
import { RelationshipService } from '@etop/state/relationship';

@Component({
  selector: 'faboshop-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent extends BaseComponent implements OnInit {

  pages$ = this.fbPagesQuery.selectAll();
  pageCount$ = this.fbPagesQuery.selectCount();
  currentPage$ = this.fbPagesQuery.selectActive();
  activeConversation$ = this.conversationsQuery.selectActive();
  idActiveConversation$ = this.conversationsQuery.selectActiveId();
  conversationID = '';

  conversations: CustomerConversation[] = [];
  loading = false;
  initializing = true;
  showOrder = false;

  paging = {
    after: '.',
    limit: 10,
    sort: '-last_message_at'
  };
  constructor(
    private api: PageApi,
    private auth: AuthenticateStore,
    private activatedRoute: ActivatedRoute,
    private confirmOrderService:ConfirmOrderService,
    private modalController: ModalController,
    private conversationsService: ConversationsService,
    private conversationsQuery: ConversationsQuery,
    private fbPagesService: FbPagesService,
    private fbPagesQuery: FbPagesQuery,
    private relationshipsService: RelationshipService
  ) {
    super();
  }

  checkPermission()  {
    return this.auth.snapshot.permission.permissions.includes('facebook/message:view' || 'facebook/message:comment');
  }

  toggleConfirmOrder(show: boolean) {
    this.showOrder = show;
  }

  async ngOnInit() {
    await this.relationshipsService.getRelationships();
    this.idActiveConversation$.pipe(takeUntil(this.destroy$))
    .subscribe(async(s) => {
      this.toggleConfirmOrder(false);
      const activeConversation = this.conversationsQuery.getActive()
      this.confirmOrderService.setActiveCustomer(activeConversation?.customer)
    });

    this.activeConversation$.pipe(takeUntil(this.destroy$))
      .subscribe(async(s) => {
        if (s?.customer?.id){
          const addresses = s.customer?.addresses;
          if (addresses){
            this.confirmOrderService.activeCustomerAddresses(addresses)
            this.confirmOrderService.activeCustomerAddress(addresses[0])
          } else{
            this.confirmOrderService.activeCustomerAddress(null)
          }
        }else {
          this.confirmOrderService.activeCustomerAddress(null)
        }
      });
    const initFilter =  new ConversationAPI.GetCustomerConversationsFilter();
    this.conversationsService.setFilter(initFilter);
    this.listPages()
    .then(() => this.conversationsService.loadMoreConversations(true).then(() => this.showConversationWithNotification()))
    .then(()=> this.conversationsService.preloadConversationsMessages());

  }

  async showConversationWithNotification() {
    this.activatedRoute.url.subscribe(url => {
      this.conversationID = url[0]?.path;
    });
    if(this.conversationID) {
      await this.conversationsService.getCustomerConversationByNotification(this.conversationID)
    }
  }

  async listPages() {
    this.initializing = true;
    try {
      await this.fbPagesService.fetchPages();
    } catch (e) {
      debug.error('ERROR in listPages', JSON.stringify(e));
    }
    this.initializing = false;
  }

  async connectFB() {
    try {
      let res = await this.loginFB();
      this.loading = true;
      const connectRes = await this.api.connectPages(res.accessToken);
      this.showResult({
        errors: connectRes.fb_error_pages,
        successes: connectRes.fb_pages
      });
    } catch (e) {
      debug.error('ERROR in list FanPages', e);
    }
  }

  loginFB(): any {
    return new Promise((res, rej) => {
      FB.login(
        function(response) {
          if (response.authResponse) {
            res(response.authResponse);
          } else {
            rej('User cancelled login or did not fully authorize.');
          }
        },
        {
          scope:
            'pages_show_list,pages_messaging,pages_manage_metadata,pages_read_engagement,pages_read_user_content,pages_manage_engagement,pages_manage_posts'
        }
      );
    });
  }

  showResult(data) {
    const modal = this.modalController.create({
      component: ChatConnectionModalComponent,
      cssClass: 'modal-radius',
      componentProps: data
    });
    modal.onDismiss().then(async () => {
      this.listPages().then(() => this.conversationsService.loadMoreConversations(true));
    });
    modal.onClosed().then(async () => {
      this.listPages().then(() => this.conversationsService.loadMoreConversations(true));
    });
    modal.show().then();
  }

  filter(event) {
    const { page, isRead, messageType } = event;
    this.fbPagesService.setActivePage(page);
    this.conversationsService.setFilter({
      ...this.conversationsQuery.getValue().ui.filter,
      is_read: isRead,
      type: messageType,
      external_page_id: page ? page.external_id : null
    });
    this.conversationsService.loadMoreConversations(true);
  }

  resetData() {
    this.paging = {
      after: '.',
      limit: 10,
      sort: '-last_message_at'
    };
    this.conversations = [];
  }
}
