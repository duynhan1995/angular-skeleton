import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/shared/src/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessagesComponent } from './messages.component';
import { MessageChatboxComponent } from './components/message-chatbox/message-chatbox.component';
import { InfiniteScrollModule } from '@etop/features/infinite-scroll/infinite-scroll.module';
import { CustomerInfoComponent } from './components/customer-info/customer-info.component';
import { CollapseComponent } from './components/customer-info/collapse/collapse.component';
import { UserInfoComponent } from './components/customer-info/user-info/user-info.component';
import { ListConversationComponent } from './components/list-conversation/list-conversation.component';
import { PageSelectorComponent } from './components/page-selector/page-selector.component';
import {
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule,
  NotPermissionModule
} from '@etop/shared';
import { CreateAndUpdateCustomerComponent } from './components/customer-info/create-and-update-customer/create-and-update-customer.component';
import { ModalControllerModule } from 'apps/core/src/components/modal-controller/modal-controller.module';
import { ConfirmOrderComponent } from './components/confirm-order/confirm-order.component';
import { CustomerInfoOrderComponent } from './components/confirm-order/components/customer-info-order/customer-info-order.component';
import { ProductsOrderComponent } from './components/confirm-order/components/products-order/products-order.component';
import { DeliveryInfoComponent } from './components/confirm-order/components/delivery-info/delivery-info.component';
import { DeliveryServiceComponent } from './components/confirm-order/components/delivery-info/components/delivery-service/delivery-service.component';
import { FaboCustomerService } from '@etop/features/fabo/customer/fabo-customer.service';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { AddressApi } from '@etop/api';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { DeliveryModule } from 'apps/faboshop/src/app/components/delivery/delivery.module';
import { OrderInfoComponent } from './components/customer-info/order-info/order-info.component';
import { CarrierConnectPopupModule } from 'apps/faboshop/src/app/components/modals/carrier-connect-popup/carrier-connect-popup.module';
import { ProductComponent } from './components/confirm-order/components/products-order/product/product.component';
import { CreateProductComponent } from './components/confirm-order/components/products-order/product/create-product/create-product.component';
import { VariantComponent } from './components/confirm-order/components/products-order/product/variant/variant.component';
import { WarningChatboxComponent } from './components/message-chatbox/warning-chatbox/warning-chatbox.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomerService, ImageLoaderModule } from '@etop/features';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateAndUpdateTagModule } from '../../components/create-and-update-tag/create-and-update-tag.module';
import { ListTagsModule } from './components/message-chatbox/list-tags/list-tags.module';
import { RiskWarningCustomerModule } from './components/confirm-order/components/customer-info-order/risk-warning-customer/risk-warning-customer.module';
import { SearchMessageComponent } from './components/page-selector/search-message/search-message.component';
import { SearchingConversationComponent } from './components/list-conversation/searching-conversation/searching-conversation.component';
import { CreateUpdateTemplateModule } from '../../components/create-update-template/create-update-template.module';
import { SenPrivateReplyModule } from '../../components/send-private-reply/send-private-reply.module';
import { AuthenticateModule } from '@etop/core';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: MessagesComponent },
      { path: ':id', component: MessagesComponent },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    InfiniteScrollModule,
    EtopMaterialModule,
    ModalControllerModule,
    EtopFilterModule,
    EtopPipesModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MaterialModule,
    CreateAndUpdateTagModule,
    ReactiveFormsModule,
    DeliveryModule,
    NotPermissionModule,
    CarrierConnectPopupModule,
    NgbModule,
    ListTagsModule,
    RiskWarningCustomerModule,
    ImageLoaderModule,
    CreateUpdateTemplateModule,
    SenPrivateReplyModule,
    AuthenticateModule,
  ],
  exports: [],
  declarations: [
    MessagesComponent,
    MessageChatboxComponent,
    CustomerInfoComponent,
    ListConversationComponent,
    PageSelectorComponent,
    CustomerInfoComponent,
    CollapseComponent,
    UserInfoComponent,
    CreateAndUpdateCustomerComponent,
    ConfirmOrderComponent,
    CustomerInfoOrderComponent,
    ProductsOrderComponent,
    ProductComponent,
    CreateProductComponent,
    WarningChatboxComponent,
    VariantComponent,
    DeliveryInfoComponent,
    DeliveryServiceComponent,
    OrderInfoComponent,
    SearchMessageComponent,
    SearchingConversationComponent,
  ],
  providers: [
    FaboCustomerService,
    ConfirmOrderService,
    ConfirmOrderStore,
    ConfirmOrderController,
    AddressApi,
    ConnectionService,
    ConnectionStore,
    CustomerService,
    ShopSettingsService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessagesModule {}
