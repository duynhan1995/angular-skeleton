import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { UserInfoComponent } from './user-info/user-info.component';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { Customer } from 'libs/models/Customer';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { ListTagsComponent } from '../message-chatbox/list-tags/list-tags.component';
import { MatDialog } from '@angular/material/dialog';
import { CustomerReturnRate } from '@etop/models/faboshop/FbCustomerReturnRate';
import { ConfirmOrderService, ConfirmOrderStore, ConnectionStore } from '@etop/features';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { RiskWarningCustomerComponent } from '../confirm-order/components/customer-info-order/risk-warning-customer/risk-warning-customer.component';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'faboshop-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild(UserInfoComponent) userInfo: UserInfoComponent;
  @Output() showConfirmOrder = new EventEmitter();
  confirmOrder$ = this.confirmOrderStore.state$.pipe(map(s => s?.confirmOrder));
  loggedInConnections$ = this.connectionStore.state$.pipe(map(s => s?.loggedInConnections));

  activeConversation$ = this.conversationsQuery.selectActive();
  activeFbUser$ = this.conversationsQuery.selectActive(({ fbUsers }) => fbUsers && fbUsers[0]);
  activeFbUserChanged$ = this.conversationsQuery.selectActive(({ fbUsers }) => fbUsers && fbUsers[0])
    .pipe(takeUntil(this.destroy$),distinctUntilChanged((prev, next) => prev?.customer?.phone == next?.customer?.phone));
  customerReturnRating: CustomerReturnRate;
  customerReturnError = false;
  errorMessage = '';
  indexCustomerReturnRate = 0;


  constructor(
    private conversationsQuery: ConversationsQuery,
    private changeDetector: ChangeDetectorRef,
    private connectionStore: ConnectionStore,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderStore: ConfirmOrderStore,
    public dialog: MatDialog,
    private confirmOrderController: ConfirmOrderController,
    private conversationsService: ConversationsService
  ) {
    super();
  }

  async ngOnInit() {
    this.activeFbUserChanged$.subscribe( async fbUser => {
      this.indexCustomerReturnRate++;
      this.customerReturnRating = null;
      this.changeDetector.detectChanges()
      if(fbUser?.customer?.phone){
        this.customerReturnRating = await this.getCustomerReturnRate();
      }
    })
    this.confirmOrder$.subscribe(confirmOder => {
      if (confirmOder) {
        this.confirmOrderService.activeConfirmOrder(false);
      }
    });
  }

 ngOnDestroy() {
   this.conversationsService.resetActiveFbUser();
 }

  onShowConfirmOrder() {
    const fbUser = this.conversationsQuery.getActive().fbUsers[0];
    if (fbUser?.customer) {
      this.confirmOrderController.setCustomerForOrder(fbUser?.customer);
    } else {
      this.confirmOrderController.setCustomerForOrder(new Customer({
        full_name: fbUser?.external_info?.name,
        phone: null,
        external_id: fbUser?.external_id,
        external_info: fbUser?.external_info
      }));
    }

    this.showConfirmOrder.emit();
  }

  call() {
    let phone = this.conversationsQuery.getActive().fbUsers[0].customer.phone;

    window.location.href = 'tel://' + phone;
  }

  addPhone() {
    this.userInfo.showCustomerModal('create');
  }

  async getCustomerReturnRate() {
    const customer = this.conversationsQuery.getActive()?.customer
    if (customer?.phone?.length > 9) {
      let customerReturnRate;
      this.customerReturnError = false;
      try {
        customerReturnRate = await this.confirmOrderService.customerReturnRate(customer.phone,this.indexCustomerReturnRate);
      }catch (e) {
        this.customerReturnError = true;
        this.errorMessage = e.message
        return null
      }
      if (customerReturnRate?.customer_return_rates){
        customerReturnRate.customer_return_rates.sort(function(a,b) {
          return b.customer_return_rate.rate - a.customer_return_rate.rate;
        })
        if (customerReturnRate.index == this.indexCustomerReturnRate){
          return this.customerReturnRating = customerReturnRate.customer_return_rates[0]?.customer_return_rate;
        }else {
          return null
        }
      }
    }
  }

  hideWaring() {
    const fbUser = this.conversationsQuery.getActive().fbUsers[0];
    this.dialog.open(RiskWarningCustomerComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: { customerReturnRate: this.customerReturnRating, phone: fbUser?.customer.phone,
        customerReturnError: this.customerReturnError, errorMessage: this.errorMessage }
    });
  }

  listTagsSelected() {
     this.dialog.open(ListTagsComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: { conversation: this.conversationsQuery.getActive() }
    });
  }
}
