import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'faboshop-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss']
})
export class CollapseComponent implements OnInit {
  @Input() show = false;
  @Input() title;
  constructor() { }

  ngOnInit(): void {
  }

  handleToggleShow() {
    this.show = !this.show
  }

}
