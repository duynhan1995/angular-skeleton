import { Component, OnInit, Input } from '@angular/core';
import {Customer, CustomerAddress} from 'libs/models/Customer';
import { BaseComponent } from '@etop/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { FaboCustomerService } from '@etop/features/fabo/customer/fabo-customer.service';
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";

@Component({
  selector: 'faboshop-create-and-update-customer',
  templateUrl: './create-and-update-customer.component.html',
  styleUrls: ['./create-and-update-customer.component.scss']
})
export class CreateAndUpdateCustomerComponent extends BaseComponent
  implements OnInit {
  @Input() title;
  @Input() customer;

  address = new CustomerAddress({});
  provinces = [];
  districts = [];
  wards = [];
  genders = [
    { name: 'Nam', value: 'male' },
    { name: 'Nữ', value: 'female' },
    { name: 'Khác', value: 'other' }
  ];

  loading = false;

  valueMap = option => option && option.code || null;
  displayMap = option => option && option.name || null;


  constructor(
    private modalAction: ModalAction,
    private faboCustomer: FaboCustomerService,
    private locationQuery: LocationQuery,
    private locationService: LocationService
  ) {
    super();
  }

  async ngOnInit() {
    if (this.customer) {
      this.address = this.customer.address;
    } else {
      this.customer = new Customer({});
    }
    if (!this.customer.address) {
      this.address = new CustomerAddress({});
    }
    this.getProvinces();
  }

  close() {
    this.modalAction.close(false);
  }

  getProvinces() {
    this._prepareLocation();
  }

  _prepareLocation() {
    const _provinces = this.locationQuery.getValue().provincesList;
    const _districts = this.locationQuery.getValue().districtsList;
    const _wards = this.locationQuery.getValue().wardsList;

    if (!_provinces || !_provinces.length) {
      return;
    }
    if (!_districts || !_districts.length) {
      return;
    }
    if (!_wards || !_wards.length) {
      return;
    }

    this.provinces = _provinces;
    this.districts = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.wards = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.provinces.forEach(province => {
      province.value = province.code;
    });
    this.districts.forEach(district => {
      district.value = district.code;
    });
    this.wards.forEach(ward => {
      ward.value = ward.code;
    });
  }

  provinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.districts.forEach(district => {
      district.value = district.code;
    });
    this.address.district_code= ''
  }
  districtSelected() {
    this.wards = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.wards.forEach(ward => {
      ward.value = ward.code;
    });
    this.address.ward_code= ''
  }

  async action(type) {
    if (this.customer.full_name === "") {
      return toastr.error("Tên khách hàng không thể để trống");
    }
    if (!this.customer.phone) {
      return toastr.error("Số điện thoại không thể để trống");
    }
    const {address1,province_code,district_code } = this.address;
    const fullAddress = address1 + province_code + district_code;

    if (fullAddress) {
      if (!this.address.address1) {
        return toastr.error("Vui lòng nhập địa chỉ");
      }
      if (!this.address.province_code) {
        return toastr.error("Vui lòng chọn tỉnh thành");
      }
      if (!this.address.district_code) {
        return toastr.error("Vui lòng chọn quận huyện");
      }
      if (!this.address.ward_code) {
        return toastr.error("Vui lòng chọn phường xã");
      }
    }
    this.loading = true;
    try {
      let data = await this.faboCustomer.createAndUpdate(this.customer, this.address);
      const { _customer, _address } = data;
      this.modalAction.close(true, {
        customer: _customer,
        type: type,
        address: _address
      });
      toastr.success('Cập nhật khách hàng thành công.');
    } catch (e) {
      debug.error('ERROR when update customer', e);
      toastr.error('Cập nhật khách hàng không thành công.',e.message);
    }
    this.loading = false;
  }

}
