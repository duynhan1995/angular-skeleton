import { Component, OnInit } from '@angular/core';
import { takeUntil } from "rxjs/operators";
import { BaseComponent } from "@etop/core";
import { Order } from "libs/models/Order";
import { OrderService } from "@etop/features";
import { FulfillmentApi } from "@etop/api";
import { ConfirmOrderStore } from "@etop/features/fabo/confirm-order/confirm-order.store";
import { ConversationsQuery } from '@etop/state/fabo/conversation';
import {FULFILLMENT_STATE, PROVIDER_LOGO} from "@etop/models";
import { FilterOperator, Filters } from '@etop/models';

@Component({
  selector: 'faboshop-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent extends BaseComponent implements OnInit {

  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0]);

  orders: Order[] = [];

  constructor(
    private conversationsQuery: ConversationsQuery,
    private confirmOrderStore: ConfirmOrderStore,
    private orderService: OrderService
  ) {
    super();
  }

  ngOnInit() {
    this.confirmOrderStore.forceUpdateForm$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        const customer_id = this.conversationsQuery.getActive().fbUsers[0]?.customer_id;
        if (customer_id) {
          this.getOrdersByCustomer(customer_id).then();
        }
      });

    this.activeFbUser$.pipe(takeUntil(this.destroy$))
      .subscribe(user => {
        if (user?.customer_id) {
          this.getOrdersByCustomer(user?.customer_id).then();
        }
      });
  }

  async getOrdersByCustomer(customer_id: string) {
    const filters: Filters = [
      {
        name: 'customer.id',
        op: FilterOperator.eq,
        value: customer_id
      }
    ];
    this.orders = await this.orderService.getOrders(0, 100, filters);
    this.orders.map(order => {
      if (order.fulfillments.length > 0) {
        const ffm = order.fulfillments[0];
        ffm.shipping_provider_logo = PROVIDER_LOGO[ffm.carrier];
        ffm.shipping_state_display = ffm.shipping_code ? FULFILLMENT_STATE[ffm.shipping_state] : FULFILLMENT_STATE.error;
      }
    });
  }

  orderDetail(order: Order) {
    this.orderService.navigate(order.code);
  }

}
