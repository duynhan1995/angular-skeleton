import { Component, OnInit } from '@angular/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { CreateAndUpdateCustomerComponent } from '../create-and-update-customer/create-and-update-customer.component';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { Customer } from '@etop/models/Customer';
import { ConfirmOrderService } from '@etop/features';

@Component({
  selector: 'faboshop-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  external_id;
  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0]);
  customerAddress$ = this.conversationsQuery.selectActive(({customer}) => customer?.address);

  constructor(
    private modalController: ModalController,
    private conversationsQuery: ConversationsQuery,
    private conversationsService: ConversationsService,
    private confirmOrderService:ConfirmOrderService,
  ) {}

  ngOnInit(): void {}

  showCustomerModal(type) {
    const activeConversation = this.conversationsQuery.getActive();
    const { fbUsers, customer } = activeConversation;
    const activeFbUser = JSON.parse(JSON.stringify(fbUsers))[0];
    const customerAddress = JSON.parse(JSON.stringify(customer?.address || {}));
    const newCustomer: Customer = {
      full_name: activeFbUser?.external_info.name,
      ...activeFbUser.customer,
      address: (activeFbUser.customer && customerAddress) || null
    }

    this.external_id = this.conversationsQuery.getActive().external_user_id;
    let modal = this.modalController.create({
      component: CreateAndUpdateCustomerComponent,
      componentProps: {
        title: type,
        customer: newCustomer
      },
      showBackdrop: 'static'
    });
    modal.show().then();
    const conversationId = this.conversationsQuery.getActive().id
    modal.onDismiss().then(async data => {
      if (data.customer) {
        if (data.type == 'create') {
          let res = await this.conversationsService.createFbUserCustomer(
            data.customer.id,
            this.external_id
          );
          this.conversationsService.setActiveFbUser(conversationId,res);
        } else {
          activeFbUser.customer = data.customer;
          this.conversationsService.setConversationCustomer(conversationId,data.customer);
          this.conversationsService.setActiveFbUser(conversationId,activeFbUser);
        }
        this.conversationsService.setCustomerAddress(conversationId,data.address);
        if (data.address){
          this.confirmOrderService.activeCustomerAddress(data.address)
        }
      }
    });
  }

  mapAddress(address) {
    if (!address) {
      return '-';
    }
    return (
      address.address1 +
      ', ' +
      address.ward +
      ', ' +
      address.district +
      ', ' +
      address.province
    );
  }

  genderMap(gender) {
    switch (gender) {
      case 'male': {
        return 'Nam';
      }
      case 'female': {
        return 'Nữ';
      }
      case 'other': {
        return 'Khác';
      }
      default: {
        return '-';
      }
    }
  }
}
