import { Component, OnInit, Output } from '@angular/core';
import { FbPage } from 'libs/models/faboshop/FbPage';
import { BaseComponent } from '@etop/core';
import { CustomerConversation } from 'libs/models/faboshop/CustomerConversation';
import { Customer } from 'libs/models/Customer';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { combineLatest, Subject } from 'rxjs';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { FbPagesQuery } from '@etop/state/fabo/page';
import { map, takeUntil } from 'rxjs/operators';
import { ListTagsComponent } from '../message-chatbox/list-tags/list-tags.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'faboshop-list-conversation',
  templateUrl: './list-conversation.component.html',
  styleUrls: ['./list-conversation.component.scss']
})
export class ListConversationComponent extends BaseComponent implements OnInit {
  @Output() confirmOrder = new Subject();

  loading$ = this.conversationsQuery.selectLoading();
  filter$ = this.conversationsQuery.select(({ ui }) => ui.filter);
  activeConversation$ = this.conversationsQuery.selectActive();
  activePage$ = this.fbPagesQuery.selectActive();
  hasConversations$ = this.conversationsQuery.selectCount();
  next_conversations_empty = false;
  conversationScroll: any;
  searchingConversation$ = this.conversationsQuery.select(s => s.searchingConverastions)
  conversations$ = this.initConversationWithSearch(null);

  constructor(
    private conversationsService: ConversationsService,
    public dialog: MatDialog,
    private conversationsQuery: ConversationsQuery,
    private fbPagesQuery: FbPagesQuery,
    private confirmOrderController: ConfirmOrderController
  ) {
    super();
  }

  initConversationWithSearch(filter){
    return combineLatest([this.conversationsQuery.select(s => s.searchingConverastions),this.conversationsQuery.selectAll({filterBy:filter})])
      .pipe(map(([searchingConversatations,listConversations]) => {
        if (!searchingConversatations?.length) {
          return listConversations
        }
        const listConv = listConversations.filter(conversation => !searchingConversatations.includes(conversation.external_id + conversation.id));
        return listConv;
      }))
  }

  async ngOnInit() {
    this.filter$.pipe(takeUntil(this.destroy$)).subscribe((filter) => {
      if (!filter) {
        this.conversations$ = this.initConversationWithSearch(null);
        return;
      }
      const { external_page_id, is_read, type } = filter;
      const selectFilter = (conversation) => (is_read == null || false || conversation.is_read == is_read)
        && (!external_page_id || conversation.external_page_id == external_page_id)
        && (!type || type == 'all' || conversation.type == type);
      this.conversations$ = this.initConversationWithSearch(selectFilter);
      
      this.hasConversations$ = this.conversations$.pipe(map(c => c.length));
    });
  }

  async selectConversation(conversation) {
    await this.conversationsService.selectConversation(conversation);
  }

  getFanpageById(id: string): FbPage {
    return this.fbPagesQuery.getAll().find(p => p.external_id == id);
  }

  async getConversations(reset = false) {
    const paging = await this.conversationsService.loadMoreConversations(reset);
    this.next_conversations_empty = !paging.next;
  }

  loadmore(event) {
    this.next_conversations_empty = false;
    setTimeout(async () => {
      await this.getConversations();
      event?.target?.complete();
    }, 2000);
  }

  async onConfirmOrder(conversation: CustomerConversation) {
    await this.selectConversation(conversation);
    const fbUser = this.conversationsQuery.getActive().fbUsers[0];
    if (fbUser?.customer) {
      this.confirmOrderController.setCustomerForOrder(fbUser?.customer);
    } else {
      this.confirmOrderController.setCustomerForOrder(new Customer({
        id: null,
        full_name: fbUser?.external_info?.name,
        phone: null,
        external_id: fbUser?.external_id,
        external_info: fbUser?.external_info
      }));
    }
    this.confirmOrder.next();
  }

  confirmOrderSearch() {
    this.confirmOrder.next();
  }

  async call(conversation: CustomerConversation) {
    const fbUser = await this.conversationsService.getFbUser(conversation.external_user_id);
    if (fbUser?.customer?.phone) {
      window.location.href = 'tel://' + fbUser?.customer?.phone;
    } else {
      return toastr.error('Khách hàng chưa có số điện thoại');
    }
  }

  trackByFn(item) {
    return item.id;
  }

  closeSearchConversation(){
    this.conversationsService.searchConversation(null,true)
  }

  listTagsSelected(conversation) {
    this.dialog.open(ListTagsComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: { conversation: conversation }
    });
  }
}
