import { Component, OnInit, Output } from '@angular/core';
import { ListTagsComponent } from '../../message-chatbox/list-tags/list-tags.component';
import { MatDialog } from '@angular/material/dialog';
import { combineLatest, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { Customer, CustomerConversation, FbPage } from '@etop/models';
import { ConfirmOrderController } from '@etop/features';
import { FbPagesQuery } from '@etop/state/fabo/page';

@Component({
  selector: 'faboshop-searching-conversation',
  templateUrl: './searching-conversation.component.html',
  styleUrls: ['./searching-conversation.component.scss']
})
export class SearchingConversationComponent implements OnInit {
  @Output() confirmOrder = new Subject();
  activePage$ = this.fbPagesQuery.selectActive();
  conversations$ = this.conversationsQuery.selectAll();
  activeConversation$ = this.conversationsQuery.selectActive();
  searchingConersation = false;
  searchingConversation$ = combineLatest([this.conversationsQuery.select(s => s.searchingConverastions),this.conversations$])
    .pipe(map(([searchingConversatations,listConversations]) => {
      if (!searchingConversatations) {
        this.searchingConersation = false;
        return;
      }
      this.searchingConersation = true;
      return listConversations.filter(conversation => {
        if (searchingConversatations.includes(conversation.external_id + conversation.id)) {
          return conversation
        }
      })
    }))

  constructor(
    public dialog: MatDialog,
    private conversationsQuery: ConversationsQuery,
    private fbPagesQuery: FbPagesQuery,
    private confirmOrderController: ConfirmOrderController,
  private conversationsService: ConversationsService,

  ) { }

  ngOnInit() {
  }


  trackByFn(item) {
    return item.id;
  }

  getFanpageById(id: string): FbPage {
    return this.fbPagesQuery.getAll().find(p => p.external_id == id);
  }


  async selectConversation(conversation) {
    await this.conversationsService.selectConversation(conversation);
  }

  listTagsSelected(conversation) {
    this.dialog.open(ListTagsComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: { conversation: conversation }
    });
  }

  async call(conversation: CustomerConversation) {
    const fbUser = await this.conversationsService.getFbUser(conversation.external_user_id);
    if (fbUser?.customer?.phone) {
      window.location.href = 'tel://' + fbUser?.customer?.phone;
    } else {
      return toastr.error('Khách hàng chưa có số điện thoại');
    }
  }

  async onConfirmOrder(conversation: CustomerConversation) {
    await this.selectConversation(conversation);
    const fbUser = this.conversationsQuery.getActive().fbUsers[0];
    if (fbUser?.customer) {
      this.confirmOrderController.setCustomerForOrder(fbUser?.customer);
    } else {
      this.confirmOrderController.setCustomerForOrder(new Customer({
        id: null,
        full_name: fbUser?.external_info?.name,
        phone: null,
        external_id: fbUser?.external_id,
        external_info: fbUser?.external_info
      }));
    }
    this.confirmOrder.next();
  }
}
