import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FbPage, FbPages } from '../../../../../../../../libs/models/faboshop/FbPage';
import { CustomerConversationType } from '../../../../../../../../libs/models/faboshop/CustomerConversation';
import { MatDialog } from '@angular/material/dialog';
import { SearchMessageComponent } from './search-message/search-message.component';

@Component({
  selector: 'faboshop-page-selector',
  templateUrl: './page-selector.component.html',
  styleUrls: ['./page-selector.component.scss']
})
export class PageSelectorComponent implements OnInit {

  @Input() pages: FbPages = [];
  @Input() currentPage: FbPage;

  @Output() changed = new EventEmitter<{
    page: FbPage;
    isRead: string;
    messageType: string;
  }>()


  is_read_arr = [
    {
      name: 'Tất cả trạng thái',
      value: 'all'
    },
    {
      name: 'Tin chưa đọc',
      value: false
    },
    {
      name: 'Tin đã đọc',
      value: true
    }
  ];
  selected_is_read = 'all';

  type_arr = [
    {
      name: 'Tất cả nguồn',
      value: 'all'
    },
    {
      name: 'Inbox',
      value: 'message'
    },
    {
      name: 'Comment',
      value: 'comment'
    }
  ];
  selected_type: CustomerConversationType = 'all';

  selectedPage: FbPage;

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.selectedPage = this.currentPage;
  }

  selectPage(page) {
    this.selectedPage = page;
    this.invokeChange();
  }

  invokeChange() {
    this.changed.next({
      page: this.selectedPage,
      isRead: this.selected_is_read == 'all' ? null : JSON.parse(this.selected_is_read),
      messageType: this.selected_type
    });
  }

  searchMessage() {
    this.dialog.open(SearchMessageComponent, {
      hasBackdrop: true,
      minWidth: '50vw',
      maxWidth: '50vw',
      position: {
        top: '8%'
      },
    });
  }

}
