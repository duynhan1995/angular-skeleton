import { Component, OnInit } from '@angular/core';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { CustomerConversation, FbPage } from '@etop/models';
import { FbPagesQuery } from '@etop/state/fabo/page';

@Component({
  selector: 'faboshop-search-message',
  templateUrl: './search-message.component.html',
  styleUrls: ['./search-message.component.scss']
})
export class SearchMessageComponent extends BaseComponent implements OnInit {

  searchText = this.fb.control('');

  conversations: CustomerConversation[] = [];
  activePage$ = this.fbPagesQuery.selectActive();
  loading = false;

  search = false;


  constructor(
    private conversationsQuery: ConversationsQuery,
    private fb: FormBuilder,
    private fbPagesQuery: FbPagesQuery,
    private conversationsService: ConversationsService,
    public dialogRef: MatDialogRef<SearchMessageComponent>
  ) {
    super();
  }

  ngOnInit() {
    this.searchText.valueChanges.pipe(debounceTime(500), takeUntil(this.destroy$)).subscribe(async text => {
      this.loading = true;
      this.conversations = await this.conversationsService.searchCustomerConversation(text);
      this.loading = false;
      this.search = text != '';
    });
  }

  selectedConversation(conversation: CustomerConversation) {
    this.conversationsService.appendConversations([conversation]);
    this.conversationsService.searchConversation(conversation,false)
    this.conversationsService.selectConversation(conversation)
    this.dialogRef.close();
  }


  getFanpageById(id: string): FbPage {
    return this.fbPagesQuery.getAll().find(p => p.external_id == id);
  }
}
