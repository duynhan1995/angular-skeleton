import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateAndUpdateTagComponent } from '../../../../../components/create-and-update-tag/create-and-update-tag.component';
import { FacebookUserTagQuery, FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { ConversationsService } from '@etop/state/fabo/conversation';


export interface DialogTagData {
  conversation: any;
}

@Component({
  selector: 'faboshop-list-tags',
  templateUrl: './list-tags.component.html',
  styleUrls: ['./list-tags.component.scss']
})
export class ListTagsComponent implements OnInit {
  isChecked: '';
  listTags : any;

  constructor(
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogTagData,
    public dialogRef: MatDialogRef<CreateAndUpdateTagComponent>,
    private facebookUserTagQuery: FacebookUserTagQuery,
    private facebookUserTagService: FacebookUserTagService,
    private conversationsService: ConversationsService,
  ) { }

  ngOnInit() {
    this.facebookUserTagService.setNewFbUserTag(null);
    let tags = this.facebookUserTagQuery.getAll();
    this.listTags = [...tags]
    const currentTags = this.data.conversation.external_user_tags || [];
    this.listTags = this.listTags.map(tag => {
      return {
        ...tag,
        isChecked: currentTags.includes(tag.id)
      };
    });
  }

  async createTag() {
    const dialogRef = this.dialog.open(CreateAndUpdateTagComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: {fbUser: this.data.conversation.fbUsers[0]}
    });
    const componentInstance = dialogRef.componentInstance.submitClicked;
    if (componentInstance) {
      componentInstance.subscribe(async result => {
        let tag:any = {
          ...this.facebookUserTagQuery.getValue().newFbUserTag
        }
        if(tag){
          tag.isChecked = true;
          this.listTags.splice(this.listTags.length,0,tag)
        }
      });
    }
  }

  async confirm(){
    const tagChecked = this.listTags.filter(tag => tag.isChecked == true)
    let tagIDs:string[] = [];
    tagChecked.forEach(tag => {
      if(tag){
        tagIDs.push(tag.id)
      }
    })
    await this.facebookUserTagService.addTags(this.data.conversation.external_user_id, tagIDs)
    if (this.data.conversation) {
      this.conversationsService.updateTagsInConversation(this.data.conversation, tagIDs);
    }
    this.dialogRef.close();
  }
}
