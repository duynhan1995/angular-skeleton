import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Message } from 'libs/models/faboshop/CustomerConversation';
import { BaseComponent } from '@etop/core';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { FbPagesQuery } from '@etop/state/fabo/page';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ListTagsComponent } from './list-tags/list-tags.component';
import { MessageTemplateService } from '@etop/state/fabo/message-template';
import { UtilService } from 'apps/core/src/services/util.service';
import { SenPrivateReplyComponent } from 'apps/faboshop/src/app/components/send-private-reply/send-private-reply.component';

const ACCEPT_IMAGE_FILE_EXTENSION = 'image/png,image/jpg,image/jpeg';

@Component({
  selector: 'faboshop-message-chatbox',
  templateUrl: './message-chatbox.component.html',
  styleUrls: ['./message-chatbox.component.scss']
})
export class MessageChatboxComponent extends BaseComponent implements OnInit {
  @ViewChild('textArea', { static: true }) textArea: HTMLTextAreaElement;
  @ViewChild('messageList', { static: true }) messageList: ElementRef;
  @ViewChild('imageMessageInput', { static: false }) imageMessageInput: ElementRef;
  @ViewChild('chatBox', { static: true }) chatBox: ElementRef;

  messages$ = this.conversationsQuery.selectActive(({ messages }) => messages);
  activePage$ = this.fbPagesQuery.selectActive();
  activeConversation$ = this.conversationsQuery.selectActive().pipe(takeUntil(this.destroy$),distinctUntilChanged());
  activeConversationPost$ = this.conversationsQuery.selectActive(({ fb_post }) => fb_post);
  needScroll$ = this.conversationsQuery.select().pipe(map(s => s.needMessageScroll), filter(need => need));

  textMessage = '';
  imageFile: File;
  checkWarningChatBox = false;

  previousMessageListScrollHeight = 0;

  parentDisplay: any = [];
  displayParentById = [];

  listTemplates: any;
  isQuickMessageModeOn = false;
  settingTemplateURL = '';

  constructor(
    public dialog: MatDialog,
    private conversationsService: ConversationsService,
    private conversationsQuery: ConversationsQuery,
    private fbPagesQuery: FbPagesQuery,
    private messageTemplateService: MessageTemplateService,
    private changeDetector: ChangeDetectorRef,
    private util: UtilService,
  ) {
    super();
  }

  get currentPage() {
    return this.fbPagesQuery.getAll().find(
      p => p.external_id == this.conversationsQuery.getActive().external_page_id
    );
  }

  isEmptyContent(message: Message) {
    return !message?.message && !message?.medias?.length;
  }

  displayParentComment(message: Message) {
    return !this.isMessageOfPage(message) && message?.o_data.external_parent && message.from.id != message.o_data.external_parent.external_from.id;
  }

  mergeDisplayParentComment(message: Message) {
    const display = this.displayParentComment(message);
    if (this.displayParentById.includes(message.id)) {
      return true;
    }
    if (display && !this.parentDisplay.includes(message?.o_data.external_parent.id)) {
      this.parentDisplay.push(message.o_data.external_parent.id);
      this.displayParentById.push(message.id);
      return true;
    } else {
      return false;
    }
  }

  isMessageOfPage(message: Message) {
    return !message.from || (message.external_page_id == message.from.id);
  }

  isVeryNearConversation(item: Message) {
    const messages = this.conversationsQuery.getActive().messages;
    const index = messages.findIndex(m => m.id == item.id);
    const next_item = messages[index + 1];

    if (!item || item.status == 'error' || !next_item) {
      return false;
    }

    if (this.isEmptyContent(item) || this.isEmptyContent(next_item)) {
      return false;
    }

    if (this.isMessageOfPage(item) != this.isMessageOfPage(next_item)) {
      return false;
    }

    const diffTimeInMinute =
      (new Date(next_item.external_created_time).getTime() -
        new Date(item.external_created_time).getTime()) /
      60000;
    // NOTE: 1min = 60s = 60000ms
    return diffTimeInMinute <= 1;
  }

  async ngOnInit() {
    this.needScroll$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.scrollToBottom();
      this.conversationsService.setNeedMessageScroll(false);
    });

    this.activeConversation$.subscribe(conversation => {
      if (!conversation) {
        return;
      }
      this.checkWarningChatBox = false;
      const now = Date.now();
      const last = new Date(conversation.last_customer_message_at).getTime();
      if (now - last > 86400000 && conversation.type == 'message') {
        this.checkWarningChatBox = true;
      }
      this.parentDisplay = [];
      this.displayParentById = [];
      this.isQuickMessageModeOn = false;
    });

    const chatBox = document.getElementById('chat-box');
    chatBox.addEventListener('keydown', (event: any) => {
      if (event.key == 'Enter' && !event.shiftKey) {
        event.preventDefault();
        return;
      }
      setTimeout(_ => {
        chatBox.style.height = 'auto';
        chatBox.style.height = chatBox.scrollHeight + 'px';
        const start = event.target.selectionStart;
        if (chatBox.scrollHeight > 50 && start >= this.textMessage.length) {
          chatBox.scrollTo({ top: chatBox.scrollHeight });
        }
      }, 0);
    });
  }

  async doLoadMore(event) {
    this.previousMessageListScrollHeight = this.messageList.nativeElement.scrollHeight;
    setTimeout(() => this.conversationsService.loadMoreMessages(this.conversationsQuery.getActive())
        .then(() => setTimeout(() => this.messageList.nativeElement.scrollTo(0, this.messageList.nativeElement.scrollHeight - this.previousMessageListScrollHeight, false)))
        .then(() => event?.target?.complete())
      , 2000);
  }

  onTextMessageChanged() {
    const chatBox = document.getElementById('chat-box');
    
    if (!this.textMessage) {
      chatBox.style.height = 'auto';
      chatBox.style.height = chatBox.scrollHeight + 'px';
    }
    if(this.textMessage.indexOf('/') == 0) {
      this.searchQuickMessageTemplate(this.textMessage.slice(1));
    } else {
      this.closeQuickTemplate();
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      const height = this.messageList.nativeElement.scrollHeight + 1000;
      this.messageList.nativeElement.scrollTo(
        0, height
      );
    }, 50);
  }

  sendMessageWithText() {
    if (!this.textMessage?.trim()) {
      return;
    }
    this.conversationsService.sendMessageWithText(this.textMessage).catch(e => toastr.error(e.message));
    this.textMessage = '';
  }

  sendMessageWithImage() {
    this.conversationsService.sendMessageWithImageWeb(this.imageFile);
  }

  retrySendMessage(message: Message) {
    if (message?.medias[0]?.preview_url) {
      return this.conversationsService.sendMessageWithImageWeb(this.imageFile, message.id);
    }
    this.conversationsService.sendMessage(message, this.conversationsQuery.getActive()).then();
  }

  uploadImage() {
    this.imageMessageInput.nativeElement.click();
  }

  async onImageSelected(event) {
    try {
      if (Object.values(event.target.files).some((file: File) => !ACCEPT_IMAGE_FILE_EXTENSION.includes(file.type))) {
        toastr.error('Định dạng file không hợp lệ. Chỉ chấp nhận các định dạng: .png, .jpg, .jpeg');
        return;
      }

      const files: File[] = Object.values(event.target.files);
      if (files && files.length) {
        files.forEach(file => {
          this.imageFile = file;
          this.sendMessageWithImage();
        });
      }
    } catch (e) {
      debug.error('Chọn hình ảnh thất bại: ', e);
    }

  }

  async call() {
    const conversation = this.conversationsQuery.getActive();
    const fbUser = await this.conversationsService.getFbUser(conversation.external_user_id);
    if (fbUser?.customer?.phone) {
      window.location.href = 'tel://' + fbUser?.customer?.phone;
    } else {
      return toastr.error('Khách hàng chưa có số điện thoại');
    }
  }

  chatboxFocus() {
    const currentConversation = this.conversationsQuery.getActive();
    const isRead$ = this.conversationsQuery.selectActive().pipe(map(s => s?.is_read));
    if (!isRead$) {
      this.conversationsService.updateReadStatus(currentConversation).then();
    }
  }

  trackMessage(index, message: Message) {
    return message.id;
  }

  createTag() {
    this.dialog.open(ListTagsComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: {conversation: this.conversationsQuery.getActive()}
    });
  }

  async initQuickTemplate(shortCode?) {
    const slug = this.util.getSlug();
    this.settingTemplateURL = `/s/${slug}/settings/message-template`;
    await this.messageTemplateService.getMessageTemplateVariables()
    await this.messageTemplateService.getMessageTemplate();
    this.listTemplates = await this.messageTemplateService.getMessageTemplateWithShortCode(shortCode);
    this.listTemplates = await this.messageTemplateService.replaceAllTemplate(this.listTemplates);
    this.changeDetector.detectChanges();
  }

  async searchQuickMessageTemplate(shortCode) {
    await this.initQuickTemplate(shortCode);
    this.isQuickMessageModeOn = true;
  }

  async toggleModeQuickMessageTemplate() {
    if (this.isQuickMessageModeOn) {
      this.closeQuickTemplate();
      return;
    }
    await this.initQuickTemplate();
    this.changeDetector.detectChanges();
    this.isQuickMessageModeOn = true;
  }

  closeQuickTemplate() {
    this.isQuickMessageModeOn = false;
  }

  async addMessageToChat(templateQuickMessage) {
    this.textMessage = templateQuickMessage.template
    this.closeQuickTemplate();
    this.chatBox.nativeElement.focus();
    this.changeDetector.detectChanges();
    const chatBox = document.getElementById('chat-box');
    chatBox.scrollTo(0, chatBox.scrollHeight);
  }

  toggleLikeComment(message: Message) {
    const consversation = this.conversationsQuery.getActive();
    message = {...message,
      is_liked: !message.is_liked
    }
    const likeStatusText = message.is_liked? 'thích' : 'bỏ thích';
    try {
      this.conversationsService.mergeMessages([message],consversation.id);
      this.conversationsService.updateLikeCommentAction(message);
      toastr.success(`Thực hiện ${likeStatusText} bình luận thành công.`);
    } catch(e) {
      toastr.error(e.msg || e.message);
    }
    
  }

  toggleHideComment(message: Message) {
    const consversation = this.conversationsQuery.getActive();
    message = {...message,
      is_hidden: !message.is_hidden
    }
    const hiddenStatusText = !message.is_hidden? 'hiện' : 'ẩn';
    try {
      this.conversationsService.mergeMessages([message],consversation.id);
      this.conversationsService.updateHideCommentAction(message);
      toastr.success(`Thực hiện ${hiddenStatusText} bình luận thành công.`);
    } catch(e) {
      toastr.error(e.msg || e.message);
    }
  }

  sendPrivateReply(message) {
    this.dialog.open(SenPrivateReplyComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: message
    });
  }
  
  readMessage(message) {
    this.conversationsService.selectConversationWithFbUser(message.external_user_id);
  }
}
