import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { ModalAction } from '../../../../../../../../core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'fabo-warning-chatbox',
  templateUrl: './warning-chatbox.component.html',
  styleUrls: ['./warning-chatbox.component.scss']
})

export class WarningChatboxComponent extends BaseComponent implements OnInit{
  constructor(
    private modalDismiss: ModalAction,
  ) {
    super();
  }
  async ngOnInit() {
  }
  dismissModal() {
    this.modalDismiss.dismiss(null);
  }
}
