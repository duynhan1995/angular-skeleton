import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTagsComponent } from './list-tags.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MaterialModule } from '@etop/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ListTagsComponent
  ],
  exports: [
    ListTagsComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MaterialModule,
    NgbModule,
    FormsModule,
  ]
})
export class ListTagsModule { }
