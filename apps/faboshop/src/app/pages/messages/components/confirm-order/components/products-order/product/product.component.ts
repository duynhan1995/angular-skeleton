
import {
  Component, EventEmitter,
  Input,
  OnInit, Output
} from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Product, Variant } from 'libs/models/Product';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import {CreateProductComponent} from 'apps/faboshop/src/app/pages/messages/components/confirm-order/components/products-order/product/create-product/create-product.component'
import {VariantComponent} from 'apps/faboshop/src/app/pages/messages/components/confirm-order/components/products-order/product/variant/variant.component'
import { ModalController } from '../../../../../../../../../../core/src/components/modal-controller/modal-controller.service';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, map, takeUntil } from 'rxjs/operators';


@Component({
  selector: 'faboshop-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent extends BaseComponent implements OnInit {
  @Output() variantSelected = new EventEmitter<Variant>()

  products$ = this.confirmOrderStore.state$.pipe(map(s => s.products), distinctUntilChanged());
  loading$ = this.confirmOrderStore.state$.pipe(map(s => s.productLoading), distinctUntilChanged());
  product_name = new FormControl('');

  dropDown = false;
  showCreateProduct = false;

  constructor(
    private fb: FormBuilder,
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private authStore: AuthenticateStore,
    private modalController: ModalController,
  ) {
    super();
  }

  async ngOnInit() {
    this.showCreateProduct = this.authStore.snapshot.permission.permissions.includes('shop/product:create');
    this.product_name.valueChanges.pipe(debounceTime(300)).subscribe((value) => {
      if (value == ''){
        this.hideDropDown();
      }else {
        this.showDropDown();
      }
      this.confirmOrderService.searchProducts(value);
    })
    this.confirmOrderStore.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.product_name.patchValue('');
      });
  }

  showDropDown() {
    this.dropDown = true;
  }

  hideDropDown() {
    this.dropDown = false;
  }

  itemSelected(variant: Variant) {
    this.product_name.patchValue(variant.product_name,{emitEvent:false})
    this.variantSelected.emit(variant);
    this.hideDropDown();
  }

  showCreateProductModal() {
    let modal = this.modalController.create({
      component: CreateProductComponent,
      componentProps: {
        p_product_name : this.product_name.value
      },
      showBackdrop: 'static'
    });
    modal.show().then();
    modal.onDismiss().then(variant => {
      if (variant != null) {
        this.itemSelected(variant);
      }else {
        this.hideDropDown();
      }
    });
  }

  showVariant(product: Product) {
    let modal = this.modalController.create({
      component: VariantComponent,
      componentProps: {
        product: product
      },
      showBackdrop: 'static'
    });
    if (product.variants.length >1){
      modal.show().then();
      modal.onDismiss().then(variant => {
        if (variant != null) {
          this.itemSelected(variant);
        }else {
          this.hideDropDown();
        }
      });
    }else {
      this.itemSelected(product.variants[0]);
    }
  }
}
