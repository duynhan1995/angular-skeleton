import { Component, Inject, OnInit } from '@angular/core';
import { CustomerReturnRate, FbCustomerReturnRate } from '@etop/models/faboshop/FbCustomerReturnRate';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Customer } from '@etop/models';

export interface DialogTagData {
  customerReturnRate: CustomerReturnRate;
  phone: string;
  customerReturnError: boolean;
  errorMessage: string;
}

@Component({
  selector: 'faboshop-risk-warning-customer',
  templateUrl: './risk-warning-customer.component.html',
  styleUrls: ['./risk-warning-customer.component.scss']
})
export class RiskWarningCustomerComponent implements OnInit {
  phone = '';
  loading = false;
  customerReturnRate: CustomerReturnRate;
  hideText = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogTagData
  ) {
  }

  ngOnInit() {
    if (this.data.customerReturnRate?.level_code == 'level_1') {
      this.hideText = true;
    }
    this.phone = this.data.phone;
    this.customerReturnRate = this.data.customerReturnRate;
  }

}
