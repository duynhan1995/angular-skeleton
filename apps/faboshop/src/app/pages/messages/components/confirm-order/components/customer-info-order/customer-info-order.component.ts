import {
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { debounceTime, distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { BaseComponent } from '@etop/core';
import { AddressAPI } from '@etop/api';
import { Customer, CustomerAddress } from '../../../../../../../../../../libs/models/Customer';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { LocationQuery } from '@etop/state/location/location.query';
import { LocationService } from '@etop/state/location';
import { FbUser } from '@etop/models/faboshop';
import { Subscription } from 'rxjs';
import { RiskWarningCustomerComponent } from './risk-warning-customer/risk-warning-customer.component';
import { MatDialog } from '@angular/material/dialog';
import { ConnectionStore } from '@etop/features';
import { CustomerReturnRate } from '@etop/models/faboshop/FbCustomerReturnRate';
import GetCustomerAddressesRequest = AddressAPI.GetCustomerAddressesRequest;

@Component({
  selector: 'faboshop-customer-info-order',
  templateUrl: './customer-info-order.component.html',
  styleUrls: ['./customer-info-order.component.scss']
})
export class CustomerInfoOrderComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() hideConfirmOrder: boolean;
  loggedInConnections$ = this.connectionStore.state$.pipe(map(s => s?.loggedInConnections));
  dropDown = false;
  provinces;
  districts;
  wards;
  address1;
  customerForm = this.fb.group({
    full_name: '',
    phone: '',
    province_code: '',
    address1: '',
    district_code: '',
    ward_code: ''
  });
  customerInitializing = true;
  subcribe: Subscription;

  activeFbUserChanged$ = this.conversationsQuery.selectActive(({ fbUsers }) => fbUsers && fbUsers[0])
    .pipe(takeUntil(this.destroy$),distinctUntilChanged((prev, next) => prev?.customer?.phone == next?.customer?.phone));
  activeCustomerAddress$ = this.confirmOrderStore.state$.pipe(takeUntil(this.destroy$),map(s => s?.activeCustomerAddress), distinctUntilChanged());
  activeConversation$ = this.conversationsQuery.selectActive();
  customerAddresses: CustomerAddress[] = [];
  customerAddresses$ = this.confirmOrderStore.state$.pipe(takeUntil(this.destroy$),
    map(s => s.customerAddress), distinctUntilChanged());
  activeFbUser$ = this.conversationsQuery.selectActive(({ fbUsers }) => fbUsers && fbUsers[0]);
  idActiveConversation$ = this.conversationsQuery.selectActive(({ id }) => id);
  customerReturnRating: CustomerReturnRate;
  customerReturnError = false;
  errorMessage = '';
  indexCustomerReturnRate = 0;

  @HostListener('document:click', ['$event'])
  handleKeyDown(event) {
    if (event.target.id != 'dropdowm-address') {
      this.hideDropDown();
    }
  }
  valueMap = option => option && option.code || null;
  displayMap = option => option && option.name || null;

  constructor(
    private fb: FormBuilder,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderStore: ConfirmOrderStore,
    private conversationsQuery: ConversationsQuery,
    private conversationService: ConversationsService,
    private detectChange: ChangeDetectorRef,
    private locationQuery: LocationQuery,
    private connectionStore: ConnectionStore,
    private locationService: LocationService,
    public dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    this.customerForm.controls['province_code'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      this.provinceSelected();
      this.districtSelected();
    });
    this.customerForm.controls['district_code'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      this.districtSelected();
    });
    this.customerForm.controls['ward_code'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      this.wardSelected();
    });
    this.customerForm.controls['address1'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(address1 => {
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      const { shipping_address } = ffm;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: {
          ...shipping_address,
          address1: address1
        }
      });
    });
    this.customerForm.controls['phone'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(phone => {
      this.customerReturnRating = null;
      this.customerReturnError = false;
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      const { shipping_address } = ffm;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: {
          ...shipping_address,
          phone: phone
        }
      });
    });
    this.customerForm.controls['full_name'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(full_name => {
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      const { shipping_address } = ffm;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: {
          ...shipping_address,
          full_name: full_name
        }
      });
    });
    this.activeCustomerAddress$.pipe(takeUntil(this.destroy$)).subscribe(customerAddress => {
      if (this.customerAddresses) {
        this.getCustomerAddress(customerAddress);
      }
    });
    this.customerInitializing = false;
    this.getProvinces();
    this.confirmOrderStore.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.hideDropDown();
      });

    this.activeConversation$.pipe(takeUntil(this.destroy$)).subscribe(conversaion => {
      if (!conversaion) {
        return;
      }
      this.checkCustomer(conversaion.customer).then();
    });
    
    this.idActiveConversation$.pipe(takeUntil(this.destroy$)).subscribe(id => {
      if (!id) {
        return;
      }
      this.resetData();
      this.customerReturnRating = null;
    });

    this.confirmOrderStore.forceUpdateForm$.pipe(takeUntil(this.destroy$))
    .subscribe(async _ => {
      if(!this.customerForm.controls['phone'].value){
        const phone = await this.conversationService.suggestPhoneFromMessage(100)
        if(phone) {
          this.customerForm.patchValue({
            phone: phone
          });
          this.getCustomerReturnRate();
        }
      }
    });

    this.activeFbUserChanged$.subscribe(fbUser => {
      this.detectChange.detectChanges();
      this.indexCustomerReturnRate++;
      this.customerReturnRating = null;
      if(fbUser?.customer?.phone){
        this.getCustomerReturnRate();
      }
    });

    this.customerForm.valueChanges.subscribe(customer => {
      const order = this.confirmOrderStore.snapshot.order;
      this.confirmOrderService.updateOrder({
        ...order,
        customer
      });
    });
  }


  ngOnChanges() {
    this.customerReturnRating = null
    const customer = this.conversationsQuery.getActive().customer;
    const customerAddress = this.confirmOrderStore.snapshot.activeCustomerAddress;
    let phone = this.customerForm.controls['phone'].value;
    if (customer?.phone != phone) {
      phone = customerAddress ? customerAddress.phone : customer ? customer.phone :  phone;
      this.customerForm.patchValue({
        phone: phone
      },{emitEvent: false});
    }
    this.getCustomerReturnRate();
  }

  getProvinces() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.provinces.forEach(province => {
      province.value = province.code;
    });
  }

  activeCustomerAddress(customerAddress: CustomerAddress) {
    this.customerForm.patchValue({
      full_name: customerAddress.full_name,
      phone: customerAddress.phone
    }, { emitEvent: false });
    if (!this.customerReturnRating) {
      this.getCustomerReturnRate();
    }
    this.getCustomerAddress(customerAddress);
    this.confirmOrderService.activeCustomerAddress(customerAddress);
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      shipping_address: {
        ...ffm.shipping_address,
        ...customerAddress
      }
    });
    this.hideDropDown();
  }

  showDropDown() {
    this.dropDown = true;
  }

  hideDropDown() {
    this.dropDown = false;
  }

  getCustomerAddress(address, emitEvent = true) {
    this.customerForm.patchValue({
      address1: address?.address1,
      province_code: address?.province_code
    }, { emitEvent });
    this.provinceSelected();
    this.customerForm.patchValue({
      district_code: address?.district_code
    }, { emitEvent });
    this.districtSelected();
    this.customerForm.patchValue({
      ward_code: address?.ward_code
    }, { emitEvent });
  }

  async checkCustomer(customer) {
    this.subcribe?.unsubscribe();
    this.subcribe = this.customerForm.get('phone').valueChanges.pipe(debounceTime(500), takeUntil(this.destroy$))
      .subscribe(async (value: string) => {
        this.hideDropDown();
        this.customerAddresses = null;
        const filter = {
          phone: value
        };
        let query: GetCustomerAddressesRequest = {
          filter: filter
        };
        if (!customer?.phone) {
          this.customerAddresses = await this.confirmOrderService.getCustomerAddress(query);
        }
        if (!this.customerAddresses?.length) {
          this.hideDropDown();
        } else {
          this.showDropDown();
        }
      });
    if (customer?.phone) {
      this.getCustomerFromFbUser(customer);
    }
  }


  getCustomerFromFbUser(customer) {
    const { activeCustomerAddress } = this.confirmOrderStore.snapshot;
    this.customerReturnRating = null;
    let fullName = customer?.full_name;
    let phone = customer?.phone;
    if (activeCustomerAddress && customer.id == activeCustomerAddress.customer_id) {
      fullName = activeCustomerAddress ? activeCustomerAddress.full_name : fullName;
    }
    if (activeCustomerAddress && customer.id == activeCustomerAddress.customer_id) {
      phone = activeCustomerAddress? activeCustomerAddress.phone : phone;
    }
    this.customerForm.patchValue({
      full_name: fullName || '',
      phone: phone || ''
    }, { emitEvent: false });
  }

  resetData() {
    const _order = this.confirmOrderStore.snapshot.order;
    this.confirmOrderService.updateOrder({
      ..._order,
      id: null
    }, true);
    this.confirmOrderService.updateFulfillment(this.confirmOrderStore.initState.fulfillment);

    let customer = new Customer({});
    let activeFbUser = new FbUser({});
    let customerConversation = new Customer({});
    this.activeFbUser$.pipe(takeUntil(this.destroy$)).subscribe(fbUser => {
      customer = new Customer({});
      activeFbUser = this.conversationsQuery.getActive()?.fbUsers[0];
      customerConversation = this.conversationsQuery.getActive()?.customer;
      if (activeFbUser?.customer) {
        customer = JSON.parse(JSON.stringify(activeFbUser?.customer || {}));
        customer.address = JSON.parse(JSON.stringify(customerConversation?.address || {}));
      } else {
        customer.full_name = activeFbUser?.external_info.name;
      }
      this.customerReturnRating = null;
      this.customerReturnError = false;
      const phone = customer?.address?.phone || customer?.phone || fbUser?.customer?.phone;
      this.customerForm.patchValue({
        phone: phone,
        full_name:customer?.address?.full_name || customer?.full_name || fbUser?.external_info.name,
        address1: customer?.address?.address1 || '',
        ward_code: customer?.address?.ward_code || '',
        province_code: customer?.address?.province_code || '',
        district_code: customer?.address?.district_code || ''
      }, { emitEvent: false });
    });
  }

  provinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(this.customerForm.get('province_code').value);
    this.districts.forEach(district => {
      district.value = district.code;
    });
    this.customerForm.patchValue({
      district_code: ''
    });
  }

  districtSelected() {
    this.wards = this.locationService.filterWardsByDistrict(this.customerForm.get('district_code').value);
    this.wards.forEach(ward => {
      ward.value = ward.code;
    });
    this.customerForm.patchValue({
      ward_code: ''
    });
  }

  wardSelected() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    const { shipping_address } = ffm;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      shipping_address: {
        ...shipping_address,
        address1: this.customerForm.controls['address1'].value,
        province_code: this.customerForm.controls['province_code'].value,
        district_code: this.customerForm.controls['district_code'].value,
        ward_code: this.customerForm.controls['ward_code'].value
      }
    });
  }

  async getCustomerReturnRate() {
    this.indexCustomerReturnRate++ ;
    const phone = this.customerForm.controls['phone'].value;
    if (phone?.length > 9) {
      let customerReturnRate;
      this.customerReturnError = false;
      try {
         customerReturnRate = await this.confirmOrderService.customerReturnRate(phone,this.indexCustomerReturnRate);
      }catch (e) {
        this.customerReturnError = true;
        this.errorMessage = e.message
      }
      if (customerReturnRate?.customer_return_rates){
        customerReturnRate.customer_return_rates.sort(function(a,b) {
          return b.customer_return_rate.rate - a.customer_return_rate.rate;
        })
        if (customerReturnRate.index == this.indexCustomerReturnRate){
          return this.customerReturnRating = customerReturnRate.customer_return_rates[0]?.customer_return_rate;
        }else {
          return this.customerReturnRating = null
        }
      }
    }else {
      return this.customerReturnRating = null;
    }
  }

  hideWaring() {
    const phone = this.customerForm.controls['phone'].value;
    this.dialog.open(RiskWarningCustomerComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: { customerReturnRate: this.customerReturnRating, phone: phone,
        customerReturnError: this.customerReturnError, errorMessage: this.errorMessage }
    });
  }

}
