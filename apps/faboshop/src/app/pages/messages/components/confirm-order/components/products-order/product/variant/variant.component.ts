import { Component, Input, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Product, Variant } from 'libs/models/Product';
import { OrderLine } from 'libs/models/Order';
import { Attribute } from 'libs/models/Product';

@Component({
  selector: 'fabo-confirm-order-variant',
  templateUrl: './variant.component.html',
  styleUrls: ['./variant.component.scss']
})

export class VariantComponent extends BaseComponent implements OnInit {
  @Input() product = new Product({});

  line = new OrderLine();
  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private authStore: AuthenticateStore,
    private modalDismiss: ModalAction,
  ) {
    super();
  }
  async ngOnInit() {
  }
  dismissModal() {
    this.modalDismiss.dismiss(null);
  }
  itemSelected(variant: Variant) {
    this.modalDismiss.dismiss(variant);
  }

  joinAttrValues(attributes: Array<Attribute>): string {
    let attrValueArr = [];
    attributes.forEach(a => attrValueArr.push(a.value));
    return attrValueArr.join('-');
  }
}

