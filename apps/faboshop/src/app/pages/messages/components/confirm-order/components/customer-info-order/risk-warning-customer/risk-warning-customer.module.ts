import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MaterialModule } from '@etop/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { RiskWarningCustomerComponent } from './risk-warning-customer.component';



@NgModule({
  declarations: [
    RiskWarningCustomerComponent
  ],
  exports: [
    RiskWarningCustomerComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MaterialModule,
    NgbModule,
    FormsModule,
  ]
})
export class RiskWarningCustomerModule { }
