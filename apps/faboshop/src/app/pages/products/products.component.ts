import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { FilterOperator, FilterOptions } from '@etop/models';

@Component({
  selector: 'shop-products',
  template: `
    <shared-filter [filters]="filters" (filterChanged)="productList.filter($event)"></shared-filter>
    <div class="page-content pb-0">
      <shop-product-list #productList></shop-product-list>
    </div>
    <!-- <div class="page-content h-100 text-center">
      <img src="assets/images/undercons.png" class="h-100 w-100" style="object-fit: cover; max-width: 600px">
    </div> -->
  `
})
export class ProductsComponent extends PageBaseComponent implements OnInit {
  filters: FilterOptions = [
    {
      label: 'Tên sản phẩm',
      name: 'name',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
    {
      label: 'Mã sản phẩm',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
    {
      label: 'Giá bán lẻ',
      name: 'price',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
