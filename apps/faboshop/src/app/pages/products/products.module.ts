import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRowComponent } from 'apps/faboshop/src/app/pages/products/components/product-row/product-row.component';
import { ProductsComponent } from 'apps/faboshop/src/app/pages/products/products.component';
import { ProductListComponent } from 'apps/faboshop/src/app/pages/products/product-list/product-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ImportProductModalComponent } from './components/import-product-modal/import-product-modal.component';
import { ModalControllerModule } from 'apps/core/src/components/modal-controller/modal-controller.module';
import { MultipleProductEditFormComponent } from './components/multiple-product-edit-form/multiple-product-edit-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SingleProductEditFormModule } from './components/single-product-edit-form/single-product-edit-form.module';
import { AddProductCardComponent } from './components/add-product-card/add-product-card.component';
import { CreateProductFormModule } from './components/create-product-form/create-product-form.module';
import { ProductsControllerService } from './products-controller.service';
import { CategoryService } from 'apps/faboshop/src/services/category.service';
import { AddCategoryModalComponent } from './components/add-category-modal/add-category-modal.component';
import { AddCollectionsModalComponent } from './components/add-collections-modal/add-collections-modal.component';
import { CollectionService } from 'apps/faboshop/src/services/collection.service';
import { StocktakeService } from 'apps/faboshop/src/services/stocktake.service';
import { StocktakeApi } from '@etop/api';
import { AuthenticateModule } from '@etop/core';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { PrintBarcodeComponent } from './components/print-barcode/print-barcode.component';
import { OptionsBarcodeComponent } from './components/options-barcode/options-barcode.component';
import { ProductStore } from 'apps/core/src/stores/product.store';
import { TrimTooLongPipe } from '../../pipes/trim-too-long.pipe';
import { EtopPipesModule, EtopCommonModule,SideSliderModule, EtopMaterialModule } from '@etop/shared';

const components = [
  ProductsComponent,
  ProductRowComponent,
  ImportProductModalComponent,
  MultipleProductEditFormComponent,
  AddProductCardComponent,
];

const services = [
  CategoryService,
  CollectionService,
];

const pages = [
  ProductListComponent,
];

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    children: [
      { path: '', component: ProductListComponent },
    ]
  }
];

@NgModule({
  declarations: [
    ...components,
    ...pages,
    AddCategoryModalComponent,
    AddCollectionsModalComponent,
    PrintBarcodeComponent,
    OptionsBarcodeComponent,
    TrimTooLongPipe
  ],
  entryComponents: [
    ImportProductModalComponent,
    AddCategoryModalComponent,
    AddCollectionsModalComponent
  ],
  imports: [
    CommonModule,
    ModalControllerModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild(routes),
    SingleProductEditFormModule,
    CreateProductFormModule,
    AuthenticateModule,
    DropdownActionsModule,
    EtopPipesModule,
    EtopCommonModule,
    SideSliderModule,
    ReactiveFormsModule
    // FaboshopAppModule
  ],
  exports: [
    AddProductCardComponent
  ],
  providers: [
    ...services,
    ProductsControllerService,
    StocktakeService,
    StocktakeApi,
    ProductStore,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductsModule { }
