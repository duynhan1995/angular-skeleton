import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductsControllerService } from 'apps/faboshop/src/app/pages/products/products-controller.service';
import { Collection } from 'libs/models/Collection';

@Component({
  selector: 'shop-add-collections-modal',
  templateUrl: './add-collections-modal.component.html',
  styleUrls: ['./add-collections-modal.component.scss']
})
export class AddCollectionsModalComponent implements OnInit {
  new_collections: Array<Collection> = [];
  constructor(
    private modalDismiss: ModalAction,
    private productsController: ProductsControllerService
  ) { }

  ngOnInit() {}

  dismissModal() {
    this.modalDismiss.dismiss(this);
  }

  createCollections() {
    this.productsController.new_collections = this.new_collections.map(coll => {
      return new Collection({
        name: coll
      });
    });
    this.modalDismiss.dismiss(this);
  }

}
