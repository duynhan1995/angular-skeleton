import { ChangeDetectorRef, Component, Input, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { Variant } from 'libs/models/Product';
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {ValueAccessorBase} from "apps/core/src/interfaces/ValueAccessorBase";

@Component({
  selector: '[shop-add-attribute-row]',
  templateUrl: './add-attribute-row.component.html',
  styleUrls: ['./add-attribute-row.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: AddAttributeRowComponent, multi: true}
  ]
})
export class AddAttributeRowComponent extends ValueAccessorBase<Variant> implements OnInit {
  @ViewChild('retailInput', { static: false }) retail_price_input: MaterialInputComponent;
  @ViewChild('costInput', { static: false }) cost_price_input: MaterialInputComponent;
  @Input() variant = new Variant({});
  @Input() isVariantEdit = false;
  @Input() variantIndex = 0;
  @Output() itemChecked = new EventEmitter<boolean>();
  @Output() removeVariant = new EventEmitter();
  format_money = {
    retail_price: true,
    cost_price: true,
  };
  price_error = {
    retail_price: false,
    cost_price: false,
  };

  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.format_money = {
      retail_price: this.variant.retail_price >= 0,
      cost_price: this.variant.cost_price >= 0,
    };
  }

  checkCode() {
    this.variant.p_data.requireCode = this.variant.p_data.checked ? !!this.variant.code : false;
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs.map(a => {
      return a.value;
    }).join(' - ');
  }

  formatMoney(type) {
    if (!this.variant[type] && this.variant[type] != 0) {
      this.format_money[type] = false;
      return;
    }
    this.format_money[type] = !this.format_money[type];
    this.changeDetector.detectChanges();
    if (!this.format_money[type] && this[`${type}_input`]) {
      this[`${type}_input`].focusInput();
    }
  }

  changePrice(type) {
    if (!Number(this.variant[type]) && type == 'retail_price') {
      this.price_error[type] = true;
      toastr.error('Giá bán không được bằng 0!');
    } else {
      this.price_error[type] = false;
    }
  }

  varItemChecked() {
    this.variant.p_data.checked = !this.variant.p_data.checked;
    this.itemChecked.emit(this.variant.p_data.checked);
  }

  onRemoveVariant() {
    this.removeVariant.emit()
  }
}
