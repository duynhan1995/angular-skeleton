import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { FormBuilder } from '@angular/forms';
import { ProductService } from 'apps/faboshop/src/app/pages/products/product-store/product.service';

@Component({
  selector: 'shop-add-attributes-modal',
  templateUrl: './add-attributes-modal.component.html',
  styleUrls: ['./add-attributes-modal.component.scss']
})
export class AddAttributesModalComponent implements OnInit {
  addAttrForm = this.fb.array([])

  addingAttr = false;
  format_money = {
    retail_price: true,
    cost_price: true,
  };
  price_error = {
    retail_price: false,
    cost_price: false,
  };

  attribute_error = [{
    name: false,
    value: false,
  }]
  constructor(
    private dialog: DialogControllerService,
    private modalDismiss: ModalAction,
    private util: UtilService,
    private modalAction: ModalAction,
    private changeDetector: ChangeDetectorRef,
    private productService: ProductService,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit() {
  }

  addAttribute() {
    this.addAttrForm.push(
      this.fb.group({
        name: '',
        values: [],
      })
    )
    this.attribute_error.push({
      name: false,
      value: false,
    })
  };

  get attributeLength() {
    return this.addAttrForm.controls.length;
  }

  removeAttribute(attrIndex) {
    this.addAttrForm.removeAt(attrIndex);
    this.attribute_error.splice(attrIndex, 1);
  }

  submit() {
    const attrs = this.addAttrForm.value;
    const attrNames = attrs.map(attr => attr.name);
    if(this.util.isDuplicateExists(attrNames)) {
      toastr.error("Tên thuộc tính không được trùng nhau. Vui lòng kiểm tra lại.");
      return;
    }
    if (!attrs.length) {
      toastr.error('Vui lòng thêm thuộc tính trước khi tiếp tục');
      this.addingAttr = false;
      return;
    } else {
      for (const [i, attr] of attrs.entries()) {
        if (!attr.name) {
          toastr.error(
            `Vui lòng nhập tên cho thuộc tính thứ ${i + 1}.`
          );
          this.attribute_error[i].name = true;
          this.addingAttr = false;
          return;
        } else {
          this.attribute_error[i].name = false;
        }
        if (!attr?.values?.length) {
          this.attribute_error[i].value = true;
          toastr.error(
            `Vui lòng nhập tên và giá trị cho thuộc tính thứ ${i + 1}.`
          );
          this.addingAttr = false;
          return;
        } else {
          this.attribute_error[i].value = false;
        }
      }
    }
    const variants = this.productService.combineVariantFromAttrs(attrs);
    this.modalAction.dismiss(variants);
  }

  closeModal() {
    this.modalAction.close(false);
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  cancel() {
    this.modalDismiss.dismiss(null);
  }

  clearErr(err,type) {
    err[type] = false;
  }
}
