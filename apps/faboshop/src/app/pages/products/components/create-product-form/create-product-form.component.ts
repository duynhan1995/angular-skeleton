import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { InventoryVariant, Product, ShopProduct, Variant } from 'libs/models/Product';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AddAttributesModalComponent } from './components/add-attributes-modal/add-attributes-modal.component';
import { BaseComponent } from '@etop/core';
import { ProductsControllerService } from '../../products-controller.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ProductService } from 'apps/faboshop/src/services/product.service';
import { InventoryApi, ProductTagsUpdateDto } from '@etop/api';
import { FormBuilder } from '@angular/forms';
import { ProductStore } from 'apps/faboshop/src/app/pages/products/product-store/product.store.service';
import { takeUntil } from 'rxjs/operators';
import { ProductService as ProductStoreService } from 'apps/faboshop/src/app/pages/products/product-store/product.service';
import { UpdateAttributeModalComponent } from './components/update-attribute-modal/update-attribute-modal.component';
import { ConfirmAttributesModalComponent } from './components/confirm-attributes-modal/confirm-attributes-modal.component';
import { AddVariantModalComponent } from '../single-product-edit-form/components/add-variant-modal/add-variant-modal.component';

@Component({
  selector: 'shop-create-product-form',
  templateUrl: './create-product-form.component.html',
  styleUrls: ['./create-product-form.component.scss']
})
export class CreateProductFormComponent extends BaseComponent implements OnInit {
  @ViewChild('retailInput', { static: false }) retail_price_input: MaterialInputComponent;
  @ViewChild('costInput', { static: false }) cost_price_input: MaterialInputComponent;
  @ViewChild('moneyInput', { static: false }) moneyInput: MaterialInputComponent;



  createProductForm = this.fb.group({
    productName: [''],
    productCode:  [''],
    retail_price:  [''],
    image: '',
    tags : [],
    description:  [''],
    variants: [[]],
})

  // product = new Product({});
  creatingProduct = false;
  product_id: string;

  format_money = {
    retail_price: true,
    cost_price: true,
  };
  price_error = {
    retail_price: false,
    cost_price: false,
  };
  inventory_quantity: number;

  constructor(
    private util: UtilService,
    private modalController: ModalController,
    private productsController: ProductsControllerService,
    private promiseQueue: PromiseQueueService,
    private productService: ProductService,
    private changeDetector: ChangeDetectorRef,
    private inventoryApi: InventoryApi,
    private fb: FormBuilder,
    private productStore: ProductStore,
    private productStoreService: ProductStoreService,
  ) {
    super();
  }

  ngOnInit() {
    this.productStoreService.clearCreateProductStore();
    this.createProductForm.valueChanges.subscribe(value => {
      this.productStoreService.setActiveProduct(new Product({
        image_urls: [value?.image],
        name:value?.productName,
        description:value?.description,
        variants: value?.variants,
        tags: value?.tags,
        code : value?.productCode,
        retail_price : value?.retail_price,
      }));
    });

    this.format_money = {
      retail_price: this.product.p_data.retail_price >= 0,
      cost_price: this.product.p_data.cost_price >= 0,
    };

    this.productStore.forceUpdateCreateProductForm$
    .pipe(takeUntil(this.destroy$))
    .subscribe(_ => {
      this.getFormDataFromStore();
    });
  }

  get productVariantAttributes() {
    return  this.product.variants[0]
      && this.product.variants[0].attributes
      && this.product.variants[0].attributes.map(a => a.name) || [];
  }
  get product() {
    return this.productStore.snapshot.product;
  }


  get product_price() {
    return this.createProductForm.get('retail_price').value;
  }

  getFormDataFromStore() {
    const product = this.productStore.snapshot.product;
    this.createProductForm.patchValue({
      productName: product.name,
      productCode: product.code,
      retail_price: product.price,
      image: product.image,
      tags : product.tags,
      description: product.description,
      variants: product.variants,
    }, {emitEvent: false});
  }


  async create() {
    if (!this.createProductForm.controls.productName.value) {
      toastr.remove();
      toastr.error('Chưa nhập tên sản phẩm!');
      return;
    }

    if (!this.createProductForm.controls.retail_price.value && !this.createProductForm.controls.variants.value ) {
      toastr.remove();
      toastr.error('Chưa nhập giá bán sản phẩm!');
      return;
    }

    if (!this.product.variants.every(vl => vl.code) && !this.product.variants.every(vl => !vl.code)) {
      this.product.variants.forEach((v, i) => {
        if(!v.code) {
          toastr.error(`Vui lòng nhập mã mẫu mã cho mẫu mã thứ ${i+1}.`);
        }
      })
      return;
    }

    if(!this.product.variants.every(v => v.retail_price)) {
      this.product.variants.forEach((vl, i) => {
        if(!vl.retail_price) {
          toastr.error(`Vui lòng nhập giá bán cho mẫu mã thứ ${i+1}.`);
        }
      })
      return;
    }
    this.creatingProduct = true;
    try {
      const productData = new Product({
        code:this.createProductForm.get('productCode').value,
        image_urls:[this.createProductForm.get('image').value],
        name:this.createProductForm.get('productName').value,
        description: this.createProductForm.get('description').value,
        variants: this.createProductForm.get('variants').value,
        tags: this.createProductForm.get('tags').value,
        retail_price:Number(this.createProductForm.get('retail_price').value),
      });
      let product = await this.productStoreService.createProduct(productData);
      product.variants = this.createProductForm.get('variants').value;
      let variantRes = await this.createVariants(product);
      await this.checkAndCreateTags({ ...this.product, id: product.id});
      this.productStoreService.setActiveProduct({...product});
      toastr.success('Tạo sản phẩm thành công!');
      this.productsController.onCreateProductSuccessful$.next();
      this.resetData();
    } catch (e) {
      debug.error('ERROR in creating product', e);
      toastr.error('Tạo sản phẩm thất bại', e.msg || e.message);
    }
    this.creatingProduct = false;
  }

  async checkAndCreateTags(product: Product) {
    try {
      const tags = product.tags;
      let tagsData: ProductTagsUpdateDto = {
        ids: [product.id]
      };
      if (tags && tags.length) {
        tagsData.replace_all = tags;
      } else {
        tagsData.delete_all = true;
      }
      await this.productService.updateProductTags(tagsData);
    } catch (e) {
      debug.error('ERROR in checking and creating tags', e);
    }
  }

  resetData() {
    this.productStoreService.clearCreateProductStore();
    this.inventory_quantity = null;
    this.format_money = {
      retail_price: false,
      cost_price: false,
    };
    this.createProductForm.patchValue({
      productName: '',
      productCode:  '',
      retail_price: '',
      image: '',
      tags : [],
      description: '',
      variants: [],
    })
  }


  async createVariants(product: Product | ShopProduct) {
    try {
      let variants: Variant[] = [];
      let success = 0;
      if (!this.product.variants || !this.product.variants.length) {
        const body = new Variant({
          product_id: product.id,
          retail_price: this.product.retail_price,
          list_price: this.product.retail_price,
          image_urls: product.image_urls,

        });
        const res = await this.productService.createVariant(body);
        variants.push({
          ...res,
          inventory_variant: {
            ...new InventoryVariant(),
            quantity: this.inventory_quantity,
            cost_price: this.product.cost_price
          }
        });
      } else {
        const totalVariants = product.variants.length;
        const promises = this.product.variants.map(variant => async() => {
          try {
            const body = new Variant({
              product_id: product.id,
              retail_price: variant.retail_price,
              list_price: variant.list_price,
              image_urls: product.image_urls,
              attributes: variant.attributes,
              code: variant.code
            });
            const res = await this.productService.createVariant(body);
            variants.push({
              ...res,
              inventory_variant: {
                ...new InventoryVariant(),
                quantity: variant.quantity,
                cost_price: variant.cost_price
              }
            });
            success += 1;
          }
          catch (e) {
            debug.error(`ERROR in Creating Variant`, e);
          }
        })
        await this.promiseQueue.run(promises, 1);
        if (success == totalVariants) {
          toastr.success(`Tạo thành công ${totalVariants} mẫu mã.`);
        } else if (success > 0 && success < totalVariants) {
          toastr.warning(`Tạo thành công ${success}/${totalVariants} mẫu mã`);
        } else {
          toastr.error(`Tạo thất bại ${totalVariants} mẫu mã.`);
        }
      }
      if (variants.length) {
        await this.updateInventoryVariantCostPrice(variants);
      }
    } catch (e) {
      debug.error('ERROR in creating variants', e);
      throw e;
    }
  }

  async updateInventoryVariantCostPrice(variants: Variant[]) {
    try {
      const promises = variants.filter(v => v.inventory_variant.cost_price >= 0)
        .map(v => async() => {
          try {
            await this.inventoryApi.updateInventoryVariantCostPrice(v.id, v.inventory_variant.cost_price);
          } catch(e) {
            debug.error('ERROR in updating InventoryVariantCostPrice inside Promise.all', e);
          }
        });
      await this.promiseQueue.run(promises, 5);
    } catch(e) {
      debug.error('ERROR in updating InventoryVariantCostPrice', e);
    }
  }

  async onChangeLogo(event) {
    const $parent = $(event.target).parent();
    $parent.addClass('loading');
    const { files } = event.target;
    let res = await this.util.uploadImages([files[0]], 1024);
    this.createProductForm.patchValue({
      image: res[0].url,
    })
    $parent.removeClass('loading');
  }

  formatMoney(type) {
    if (!this.createProductForm.get('retail_price').value && this.createProductForm.get('retail_price').value != 0) {
      this.format_money[type] = false;
      return;
    }
    this.format_money[type] = !this.format_money[type];
    this.changeDetector.detectChanges();
    if (!this.format_money[type] && this[`${type}_input`]) {
      this[`${type}_input`].focusInput();
    }
  }

  changePrice(type) {
    if (!Number(this.product.p_data[type]) && type == 'retail_price') {
      this.price_error[type] = true;
      toastr.error('Giá bán không được bằng 0!');
    } else {
      this.price_error[type] = false;
      this.product[type] = this.product.p_data[type];
    }
  }

  openAddAttributesPopup(e, isDeclareAttrsAgain = false) {
    e.preventDefault();
    if((!this.product.variants.length) || (this.product.variants.length && isDeclareAttrsAgain==true) ) {
      const modal = this.modalController.create({
        component: AddAttributesModalComponent,
        cssClass: 'modal-lg'
      });
      modal.show().then();
      modal.onDismiss().then(variants => {
        if (variants) {
          this.productStoreService.setVariantListForm(variants);
          this.openEditVariantModal(e);
        }
      });
    } else {
      const modal = this.modalController.create({
        component: AddVariantModalComponent,
        componentProps: {
          attributeNames : this.productVariantAttributes
        },
        cssClass: 'modal-lg'
      });
      modal.onDismiss().then(data => {
        if (data?.variant) {
          this.product.variants.push(data.variant);
          this.productStoreService.setVariantListForm(this.product.variants);
        }
      });
      modal.show().then();
    }

  }

  openEditVariantModal($e) {
    const variants = this.productStore.snapshot.variantListForm;
    const modal = this.modalController.create({
      cssClass: 'modal-lg',
      component: ConfirmAttributesModalComponent,
      componentProps: {
        variants: variants
      }
    });
    modal.show();
    modal.onDismiss().then(data => {
      if(!data) {
        return;
      }
      if (data.action == 'edit-attr') {
        return this.openEditAttributeModal($e);
      }
      if (data.action == 'submit') {
        this.createProductForm.patchValue({ variants: data.data})
      }
    })
  }

  openEditAttributeModal($e, fromConfirmAttrForm = true) {
    const attrs = this.productStore.snapshot.variantListForm[0]?.attributes.map(attr => new Object({
      name: attr.name,
      oldName: attr.name
    })) || [];
    const modal = this.modalController.create({
      cssClass: 'modal-md',
      component: UpdateAttributeModalComponent,
      componentProps: {
        attributes: attrs,
        fromConfirmAttrForm: fromConfirmAttrForm
      }
    });
    modal.show();
    modal.onDismiss().then(res => {
      if (res && res.changes) {
        const variants = [...this.productStore.snapshot.variantListForm];
        res.changes.forEach(change => {
          variants.forEach(variant => {
            switch (change.action) {
              case 'delete':
                if(change.oldName){
                  variant.attributes.splice(variant.attributes.findIndex(attr => attr.name == change.oldName), 1);
                }
                break;
              case 'update':
                variant.attributes.find(attr => attr.name == change.oldName).name = change.name;
                break;
              case 'add':
                variant.attributes.push({ name: change.name, value: '' });
                break;
            }
          })
        })
        this.productStoreService.setVariantListForm(variants)
      }
      if(res?.openAddAttributeModal) {
        this.openAddAttributesPopup($e, true);
      }
      if(fromConfirmAttrForm) {
        this.openEditVariantModal($e);
      }
    })
  }

  onDataEdited(variant) {
    variant.p_data.edited = true;
  }

}
