import { ChangeDetectorRef, Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ValueAccessorBase } from 'apps/core/src/interfaces/ValueAccessorBase';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Variant } from 'libs/models/Product';
import { ProductStore } from 'apps/faboshop/src/app/pages/products/product-store/product.store.service';
import { ProductService } from 'apps/faboshop/src/services/product.service';

export class VariantLine extends Variant {
  isChecked: boolean;
  skuError: any;
  retail_price_error: boolean;
}

@Component({
  selector: 'shop-variant-lines',
  templateUrl: './variant-lines.component.html',
  styleUrls: ['./variant-lines.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: VariantLinesComponent, multi: true}
  ]
})
export class VariantLinesComponent extends ValueAccessorBase<VariantLine[]>  implements OnInit {
  @Output() checkVariantLine  :EventEmitter<any> = new EventEmitter();
  format_money = {
    retail_price: true,
    cost_price: true,
  };

  price_error = {
    retail_price: false,
    cost_price: false,
  };


  constructor(private productStore: ProductStore, private productService:ProductService, private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs.map(a => {
      return a.value;
    }).join(' - ');
  }

  formatMoney(variant,type) {
    if (!variant[type] && variant[type] != 0) {
      this.format_money[type] = false;
      return;
    }
    this.format_money[type] = !this.format_money[type];
    this.changeDetector.detectChanges();
    if (!this.format_money[type] && this[`${type}_input`]) {
      this[`${type}_input`].focusInput();
    }
  }

  changePrice(variant,type) {
    if (!Number(variant[type]) && type == 'retail_price') {
      this.price_error[type] = true;
      toastr.error('Giá bán không được bằng 0!');
    } else {
      this.price_error[type] = false;
    }
  }

  checkCode(variant) {
    variant.requireCode = variant?.isChecked ? !! variant?.code : false;
  }

  invokeChange() {
    this.value = this.value.slice(0);
  }

  onCheckVariant() {
    this.checkVariantLine.emit();
  }

  onRemoveVariant(index) {
    this.value.splice(index, 1);
  }

  clearErr(variant) {
    variant.skuError = false;
  }

  clearRetailPriceError(variant) {
    variant.retail_price_error = false;
  }

}
