import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'libs/models/Product';
import { ProductsControllerService } from '../../products-controller.service';

@Component({
  selector: 'shop-multiple-product-edit-form',
  templateUrl: './multiple-product-edit-form.component.html',
  styleUrls: ['./multiple-product-edit-form.component.scss']
})
export class MultipleProductEditFormComponent implements OnInit {
  @Input() products: Array<Product>;
  categories = [];
  tags = [];
  collections = [];

  constructor(
    private  productsControllerService: ProductsControllerService
  ) {}

  ngOnInit() {}

  onDataEdited(value, field) {
    //TODO
  }

  update() {
    //TODO
  }

  printBarcode() {
    this.productsControllerService.showPrintBarcode = true;
  }

}
