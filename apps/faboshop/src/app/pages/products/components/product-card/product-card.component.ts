import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'libs/models/Product';

@Component({
  selector: 'shop-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  variantQuantity : number;

  constructor() {}

  ngOnInit() {
    this.sumVariantQuantity();
  }

  sumVariantQuantity() {
    let variantQuantity = 0;
    this.product.variants.forEach(variant => {
      variantQuantity += variant.quantity
    });
    this.variantQuantity = variantQuantity
  }

}
