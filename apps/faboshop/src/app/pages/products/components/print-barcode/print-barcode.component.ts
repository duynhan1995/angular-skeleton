import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ProductStore } from 'apps/core/src/stores/product.store';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-print-barcode',
  templateUrl: './print-barcode.component.html',
  styleUrls: ['./print-barcode.component.scss']
})
export class PrintBarcodeComponent extends BaseComponent implements OnInit {
  barcodeInfo = [];
  showOptions = false;
  showShopname = false;
  showPrice = true;

  standard = 'CODE128';
  standards = [
    {
      title: 'CODE128',
      desc: 'Gồm số và chữ, tối đa 95 ký tự',
      image_url: 'assets/images/code128.jpg',
      value: 'CODE128'
    },
    {
      title: 'EAN13',
      desc: 'Chỉ gồm số, tối đa 13 ký tự',
      image_url: 'assets/images/ean.png',
      value: 'EAN13'
    },
    {
      title: 'UPC',
      desc: 'Chỉ gồm số, tối đa 13 ký tự',
      image_url: 'assets/images/upc.jpg',
      value: 'UPC'
    }
  ];

  size = 'three104x22';
  sizes = [
    {
      title: '104x22mm/4.2x0.9 Inch',
      desc: 'Mẫu giấy cuộn 3 nhãn',
      image_url: 'assets/images/three104x22.png',
      value: 'three104x22'
    },
    {
      title: '72x22mm',
      desc: 'Mẫu giấy cuộn 2 nhãn (72x22mm)',
      image_url: 'assets/images/two74x22.png',
      value: 'two72x22'
    },
    {
      title: '74x22mm',
      desc: 'Mẫu giấy cuộn 2 nhãn (74x22mm)',
      image_url: 'assets/images/two74x22.png',
      value: 'two74x22'
    },
    {
      title: 'Tomy 103 202x162mm',
      desc: 'Mẫu giấy 12 nhãn',
      image_url: 'assets/images/BarCodeTomy103.jpg',
      value: 'twelve202x162'
    },
    {
      title: 'A4 - Tomy 145',
      desc: 'Mẫu giấy 65 nhãn',
      image_url: 'assets/images/BarCodeTomy145.jpg',
      value: 'sixtyfivea4'
    },
    {
      title: '40mm x 30mm',
      desc: 'Mẫu giấy 1 nhãn',
      image_url: 'assets/images/one40x30.jpg',
      value: 'one40x30'
    },
    {
      title: '75mm x 10mm',
      desc: 'Mẫu tem hàng trang sức',
      image_url: 'assets/images/one75x10.jpg',
      value: 'one75x10'
    }
  ];

  constructor(
    private productStore: ProductStore,
    private dropdownController: DropdownActionsControllerService,
    private auth: AuthenticateStore
  ) {
    super();
  }

  ngOnInit() {
    this.productStore.selectedProductsChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(prods => {
        const variants = prods.reduce((a,b) => a.concat(b.variants), []);
        this.barcodeInfo = variants.map(variant => {
          return {
            name: variant.name,
            code: variant.code,
            price: variant.price,
            print_quantity: variant.quantity > 0 && variant.quantity || 0
          }
        });
      });
    this.dropdownController.clearActions();
  }

  removeVariant(i) {
    this.barcodeInfo.splice(i, 1);
  }

  toggleDisplayPrice() {
    this.showPrice = !this.showPrice
  }

  toggleDisplayShopname() {
    this.showShopname = !this.showShopname
  }

  chooseOptions() {
    if (this.barcodeInfo.length > 50) {
      toastr.error('Chỉ có thể in được cùng lúc tối đa 50 mẫu mã sản phẩm.');
      return;
    }
    this.barcodeInfo = this.barcodeInfo.filter(b => b.code);
    this.showOptions = true;
  }

  get unavailableBarcodeInfo() {
    return this.barcodeInfo.every(b => b.code == '' || null);
  }

  changeStandard(standard) {
    this.standard = standard;
  }

  changePaperSize(size) {
    this.size = size;
  }

  viewBarcode() {
    if (!this.validateStandardWithSKU()) {
      return;
    }
    let barcodeInfo = this.barcodeInfo.map(b => {
      return [ b.name, b.code, b.price, b.print_quantity ];
    });
    let printInfo = {
      barcodeInfo: barcodeInfo,
      standard: this.standard,
      size: this.size,
      shopName: this.auth.snapshot.shop.name,
      showShopname: this.showShopname,
      showPrice: this.showPrice
    };
    window.open('/printer/barcode?data='+JSON.stringify(printInfo), '_blank');
  }

  private validateStandardWithSKU() {
    const hasNaNSKU = this.barcodeInfo.some(b => isNaN(Number(b.code)));
    if (['EAN13', 'UPC'].indexOf(this.standard) != -1 && hasNaNSKU) {
      toastr.error(`Chuẩn mã vạch ${this.standard} chỉ hỗ trợ định dạng "Chỉ gồm số, tối đa 13 ký tự"`);
      return false;
    }
    return true;
  }

}
