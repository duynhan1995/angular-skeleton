import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { InventoryVoucher } from 'libs/models/Inventory';
import { InventoryService } from 'apps/faboshop/src/services/inventory.service';
import { Variant } from 'libs/models/Product';
import {
  NavigationDirection,
  PaginationOpt
} from '@etop/shared/components/etop-common/etop-pagination/etop-pagination.component';
import { Filter, FilterOperator } from '@etop/models';

@Component({
  selector: 'shop-history-inventory-modal',
  templateUrl: './history-inventory-modal.component.html',
  styleUrls: ['./history-inventory-modal.component.scss']
})
export class HistoryInventoryModalComponent implements OnInit {
  @Input() variant: Variant = new Variant({});
  inventory_vouchers: Array<InventoryVoucher> = [];
  filters: Array<Filter> = [];
  paginationConfig: PaginationOpt = {
    hidePerpage: true,
    nextDisabled: false,
    previousDisabled: true
  };
  numPages = 0;
  currentPage = 1;
  perpage = 10;


  constructor(
    private modalAction: ModalAction,
    private inventoryService: InventoryService
  ) { }

  async ngOnInit() {
     this.filters = [{
      name: "variant_ids",
      op: FilterOperator.contains,
      value: this.variant.id
    }];
    this.numPages = await this.getNumPages();
    this._requestNewPage(1);
  }

  getIndexByVariantId(lines, id) {
    return lines && lines.findIndex(l => l.variant_id == id);
  }

  async getNumPages() {
    const res = await this.inventoryService.getInventoryVouchers(0, 1000, this.filters);
    return Math.ceil(res.length / this.perpage)
  }

  navigate(direction) {
    switch (direction) {
      case NavigationDirection.BACKWARD:
        this.currentPage = this.currentPage == 1 ? 1 : this.currentPage - 1;
        break;
      case NavigationDirection.FORWARD:
        this.currentPage = this.currentPage == this.numPages ? this.numPages : this.currentPage + 1;
        break;
    }
    if(this.currentPage > 0 && this.currentPage < this.numPages + 1){
      this._requestNewPage(this.currentPage);
    }
  }

  async _requestNewPage(currentPage) {
    const { perpage } = this;
    try {
      if (currentPage == 1 && this.numPages == 1 ){
        this.paginationConfig = {...this.paginationConfig,previousDisabled:true, nextDisabled:true}
      }
      else if(currentPage == this.numPages) {
        this.paginationConfig = {...this.paginationConfig,nextDisabled:true,previousDisabled: false}
      }
      else if(currentPage == 1 ) {
        this.paginationConfig = {...this.paginationConfig,previousDisabled:true, nextDisabled:false}
      }
      const res = await this.inventoryService.getInventoryVouchers((currentPage * perpage) - perpage, perpage, this.filters)
      const  inventory_vouchers  = res;
      if (inventory_vouchers.length) {
        this.inventory_vouchers = inventory_vouchers;
      }
    } catch (e) {
      debug.log(e);
    }
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

}
