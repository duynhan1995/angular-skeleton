import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VariantEditRowComponent } from './components/variant-edit-row/variant-edit-row.component';
import { SingleProductEditFormComponent } from './single-product-edit-form.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { RemoveVariantModalComponent } from './components/remove-variant-modal/remove-variant-modal.component';
import { HistoryInventoryModalComponent } from './components/history-inventory-modal/history-inventory-modal.component';
import { EditInventoryModalComponent } from './components/edit-inventory-modal/edit-inventory-modal.component';
import { PurchaseOrderService } from 'apps/faboshop/src/services/purchase-order.service';
import { PurchaseOrderApi } from '@etop/api';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticateModule } from '@etop/core';
import { EtopPipesModule, EtopMaterialModule } from '@etop/shared';
import { EditAttributeModalComponent } from './components/edit-attribute-modal/edit-attribute-modal.component';
import { AddVariantModalComponent } from './components/add-variant-modal/add-variant-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    NgbModule,
    AuthenticateModule,
    EtopPipesModule,
    EtopMaterialModule,
  ],
  exports: [
    SingleProductEditFormComponent
  ],
  entryComponents: [
    RemoveVariantModalComponent,
    HistoryInventoryModalComponent,
    EditInventoryModalComponent,
    EditAttributeModalComponent,
    AddVariantModalComponent
  ],
  declarations: [
    SingleProductEditFormComponent,
    VariantEditRowComponent,
    RemoveVariantModalComponent,
    HistoryInventoryModalComponent,
    EditInventoryModalComponent,
    EditAttributeModalComponent,
    AddVariantModalComponent
  ],
  providers: [
    PurchaseOrderService,
    PurchaseOrderApi
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleProductEditFormModule {
}
