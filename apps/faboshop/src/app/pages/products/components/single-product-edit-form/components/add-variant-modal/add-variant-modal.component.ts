import { Component, OnInit, Input } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { FormBuilder } from '@angular/forms';
import { VariantLine } from '../../../create-product-form/components/variant-lines/variant-lines.component';
import { Attribute, Variant, InventoryVariant } from 'libs/models/Product';
import { ProductService } from 'apps/faboshop/src/services/product.service';

@Component({
  selector: 'shop-add-variant-modal',
  templateUrl: './add-variant-modal.component.html',
  styleUrls: ['./add-variant-modal.component.scss']
})
export class AddVariantModalComponent implements OnInit {
  @Input() attributeNames: [];
  @Input() product;
  loading =false;
  variant = new Variant({});
  constructor(private modalAction: ModalAction, private fb: FormBuilder, private productService: ProductService) { }

  ngOnInit() {
    this.variant.attributes =  this.attributeNames.map(attrName => {
      let attr = new Attribute();
      attr.name = attrName;
      attr.value = '';
      return attr;
    })
  }

  cancel() {
    this.modalAction.dismiss(null);
  }

  async submit() {
    this.loading = true;
    try {
      if(this.variant.attributes.every(attr => !attr.value)){
        toastr.error('Vui lòng điền ít nhất một giá trị thuộc tính');
        return  this.loading = false;
      }
      if(!this.variant.retail_price){
        toastr.error('Vui lòng nhập giá bán mẫu mã');
        return  this.loading = false;
      }
  
      const variant = this.variant;
      const body = new Variant({
        product_id: this.product.id,
        retail_price: variant.retail_price,
        attributes: variant.attributes,
        code: variant.code,
      });
      const res = await this.productService.createVariant(body);
      this.product.variants.push({
        ...res,
        inventory_variant: {
          ...new InventoryVariant(),
          quantity: variant.quantity,
          cost_price: variant.cost_price
        }
      });
      this.product.p_data.variants.push({
        ...res,
        inventory_variant: {
          ...new InventoryVariant(),
          quantity: variant.quantity,
          cost_price: variant.cost_price
        }
      });
      this.modalAction.dismiss(null)
    } catch(e) {
      this.loading = false;
      return toastr.error("Thêm mẫu mã thất bại", e.message || e.msg);
    }
    this.loading = false;
  }
}
