import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProductStore } from 'apps/faboshop/src/app/pages/products/product-store/product.store.service';
import { ProductService } from 'apps/faboshop/src/app/pages/products/product-store/product.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Product } from 'libs/models/Product';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'faboshop-update-attribute-modal',
  templateUrl: './edit-attribute-modal.component.html',
  styleUrls: ['./edit-attribute-modal.component.scss']
})
export class EditAttributeModalComponent implements OnInit {
  @Input() attributes: any[];
  productVariantAttributes = [];
  deletedAttrs = [];
  updateAttrForm = this.fb.array([]);
  edittingAttrs = [];
  constructor(private fb: FormBuilder,
              private modalAction: ModalAction,
              private util: UtilService
              ) { }

  ngOnInit() {
    this.attributes.forEach(attr => {
      this.updateAttrForm.push(this.fb.group({
        oldName: attr.name,
        name: attr.name
      }))
    })
  }

  addAttribute() {
    this.updateAttrForm.push(this.fb.group({
      oldName: '',
      name: ''
    }))
  }

  removeAttribute(attr, index) {
    this.deletedAttrs.push(attr);
    this.updateAttrForm.removeAt(index);
  }

  get attributeLength() {
    return this.updateAttrForm.value.length;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  cancel() {
    this.modalAction.dismiss(null);
  }

  submit() {
    const attrNames = this.updateAttrForm.value.map(attr => attr.name)
    if(this.util.isDuplicateExists(attrNames)) {
      toastr.error("Tên thuộc tính không được trùng nhau. Vui lòng kiểm tra lại.");
      return;
    }
    const changes = this.updateAttrForm.value.filter(attr => !!attr.name);
    this.modalAction.dismiss([]
      .concat(this.deletedAttrs.map(change => Object.assign(change, { action: 'delete' })))
      .concat(changes.filter(attr => !!attr.oldName && attr.oldName != attr.name).map(change => Object.assign(change, { action: 'update' })))
      .concat(changes.filter(attr => !attr.oldName).map(change => Object.assign(change, { action: 'add' }))));
  }
}

