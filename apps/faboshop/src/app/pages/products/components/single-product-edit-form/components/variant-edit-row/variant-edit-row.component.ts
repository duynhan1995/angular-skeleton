import { ChangeDetectorRef, Component, Input, OnInit, ViewChild, Output } from '@angular/core';
import { Variant } from 'libs/models/Product';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ProductService } from 'apps/faboshop/src/services/product.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { RemoveVariantModalComponent } from '../remove-variant-modal/remove-variant-modal.component';
import { ProductsControllerService } from 'apps/faboshop/src/app/pages/products/products-controller.service';
import { HistoryInventoryModalComponent } from '../history-inventory-modal/history-inventory-modal.component';
import { EditInventoryModalComponent } from '../edit-inventory-modal/edit-inventory-modal.component';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: '[shop-variant-edit-row]',
  templateUrl: './variant-edit-row.component.html',
  styleUrls: ['./variant-edit-row.component.scss']
})
export class VariantEditRowComponent implements OnInit {
  @ViewChild('retailInput', { static: false }) retail_price_input: MaterialInputComponent;
  @ViewChild('costInput', { static: false }) cost_price_input: MaterialInputComponent;
  @Input() variant: Variant;
  @Input() removable = false;

  allowCostPriceInput = true;
  allowRetailPriceInput = true;
  allowBasicInfoInput = true;

  format_money = {
    retail_price: true,
    cost_price: true,
  };
  price_error = {
    retail_price: false,
    cost_price: false,
  };

  variantHasPOTooltip = 'Mẫu mã này đã có đơn nhập hàng, giá vốn được tính theo bình quân gia quyền giữa các lần nhập hàng';

  private modal: any;

  constructor(
    private modalController: ModalController,
    private productsController: ProductsControllerService,
    private productService: ProductService,
    private changeDetector: ChangeDetectorRef,
    private authStore: AuthenticateStore
  ) { }

  ngOnInit() {
    this.format_money = {
      retail_price: this.variant.retail_price >= 0,
      cost_price: this.variant.cost_price >= 0,
    };
    this.allowCostPriceInput = this.authStore.snapshot.permission.permissions.includes('shop/product/cost_price:update');
    this.allowRetailPriceInput = this.authStore.snapshot.permission.permissions.includes('shop/product/retail_price:update');
    this.allowBasicInfoInput = this.authStore.snapshot.permission.permissions.includes('shop/product/basic_info:update');
  }

  onDataEdited() {
    this.variant.p_data.edited = true;
  }

  formatMoney(type) {
    if (type == 'retail_price' && !this.allowRetailPriceInput) {
      return;
    }
    if (type == 'cost_price' && !this.allowCostPriceInput) {
      return;
    }
    if (!this.variant.p_data[type] && this.variant.p_data[type] != 0) {
      this.format_money[type] = false;
      return;
    }
    this.format_money[type] = !this.format_money[type];
    this.changeDetector.detectChanges();
    if (!this.format_money[type] && this[`${type}_input`]) {
      this[`${type}_input`].focusInput();
    }
  }

  changePrice(type) {
    if (!Number(this.variant.p_data[type]) && type == 'retail_price') {
      this.price_error[type] = true;
      toastr.error(`Giá bán không được bằng 0!`);
    } else {
      this.price_error[type] = false;
    }
  }

  onRemoveVariant() {
    if (this.modal) {
      return;
    }
    this.modal = this.modalController.create({
      component: RemoveVariantModalComponent,
      cssClass: 'modal-lg',
      componentProps: {
        variant: this.variant
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(removed => {
      this.modal = null;
      if (removed) {
        this.productsController.reloadProductList();
      }
    });
  }

  openHistoryInventoryModal() {
    if (this.modal) return;
    this.modal = this.modalController.create({
      component: HistoryInventoryModalComponent,
      cssClass: 'modal-lg',
      showBackdrop: 'static',
      componentProps: {
        variant: this.variant
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(_ => {
      this.modal = null;
    });
  }

  openEditInventoryModal() {
    if (this.modal) return;
    this.modal = this.modalController.create({
      component: EditInventoryModalComponent,
      cssClass: 'modal-lg',
      showBackdrop: 'static',
      componentProps: {
        variant: this.variant
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(reload => {
      if (reload) {
        this.productsController.reloadProductList();
      }
      this.modal = null;
    });
  }

}
