import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'libs/models/Product';
import { CollectionService } from 'apps/faboshop/src/services/collection.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { ProductsControllerService } from 'apps/faboshop/src/app/pages/products/products-controller.service';

@Component({
  selector: '[shop-product-row]',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss']
})
export class ProductRowComponent implements OnInit {
  @Input() product: Product;
  @Input() liteMode = false;
  @Output() clickChangeCategoryHandler = new EventEmitter();

  isExternal = false;
  constructor(
    private collectionService: CollectionService,
    private shopService: ShopService,
    private util: UtilService,
    private productsController: ProductsControllerService
  ) {
  }

  ngOnInit() {
    this.isExternal = !!this.product.product_source_id &&
      !!this.product.product_source_name &&
      !!this.product.product_source_type;
  }

  formatAttr() {
    let attrs = [];
    if (this.product.variants && this.product.variants[0]) {
      let attrsName = this.getAttrName(this.product.variants[0].attributes);

      this.product.variants.map(v => {
        attrs = attrs.concat(v.attributes);
      });

      return (attrsName.map(n => {
        return attrs.filter(a => {
          return a.name == n;
        });
      }).map(a => {
        return {
          name: a[0].name,
          value: this.util.uniqueArray(a.map(item => {
            return item.value;
          })).join(', ')
        };
      }));
    }
    return []

  }

  getAttrName(attrs) {
    let attrsName = [];
    if (attrs) {
      attrs.map(a => {
        if (attrsName.indexOf(a.name) == -1) {
          attrsName.push(a.name);
        }
      });
    }

    return attrsName;
  }

  getCollectionName(collection_id) {
    const found_collection = this.productsController.collections.find(c => c.id == collection_id);
    return found_collection && found_collection.name;
  }

  getCategoryName(category_id) {
    const found_category = this.productsController.categories.find(c => c.id == category_id);
    return found_category && found_category.name;
  }

  get displayCollections() {
    const { collection_ids } = this.product;
    return collection_ids.map(c_id => this.getCollectionName(c_id));
  }

  get hasCategory() {
    return this.product.category_id && this.product.category_id != "0";
  }

  get hasCollections() {
    return this.product.collection_ids && this.product.collection_ids.length > 0;
  }

  get variantAmount() {
    return this.product.variants.length;
  }

  get priceList() {
    let priceList = [];
    this.product.variants.forEach(v => priceList.push(v.retail_price));
    return priceList;
  }

  get minPrice() {
    return Math.min(...this.priceList);
  }

  get maxPrice() {
    return Math.max(...this.priceList);
  }
}
