import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductsControllerService } from 'apps/faboshop/src/app/pages/products/products-controller.service';
import { Category } from 'libs/models/Category';

@Component({
  selector: 'shop-add-category-modal',
  templateUrl: './add-category-modal.component.html',
  styleUrls: ['./add-category-modal.component.scss']
})
export class AddCategoryModalComponent implements OnInit {
  new_category = new Category({});
  constructor(
    private modalDismiss: ModalAction,
    private productsController: ProductsControllerService
  ) { }

  ngOnInit() {}

  dismissModal() {
    this.modalDismiss.dismiss(this);
  }

  createCategory() {
    this.productsController.new_category = this.new_category;
    this.modalDismiss.dismiss(this);
  }

}
