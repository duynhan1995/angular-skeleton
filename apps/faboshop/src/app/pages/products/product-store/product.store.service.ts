import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { Attribute, Product , Variant} from 'libs/models/Product';
import { Subject } from 'rxjs';

export interface ProductData {
    product: Product;
    variantListForm: Variant[],
}
@Injectable({
    providedIn: 'root'
  })
export class ProductStore extends AStore<ProductData> {
    initState: ProductData = {
        product: new Product({
            name: '',
            code:'',
            image: '',
            tags: [],
            price: 0,
            description: '',
            variants: [],
        }),
        variantListForm: [],
    }

    readonly forceUpdateForm$ = new Subject();
    readonly forceUpdateCreateProductForm$ = new Subject();
    constructor(){
        super();
    }

    setActiveProduct(activeProduct: Product) {
        this.setState({product: activeProduct})
    }

    clearProduct() {
        this.setState({product: this.initState.product});
    }

    setVariantListForm(variantList: Variant[]) {
        this.setState({variantListForm: variantList})
    }
}
