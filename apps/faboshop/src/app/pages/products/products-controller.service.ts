import { Injectable } from '@angular/core';
import { Product, ShopProduct, Variant } from 'libs/models/Product';
import { Subject } from 'rxjs';
import { Collection } from 'libs/models/Collection';
import { ProductListComponent } from 'apps/faboshop/src/app/pages/products/product-list/product-list.component';
import { CategoryService } from 'apps/faboshop/src/services/category.service';
import { Category } from 'libs/models/Category';
import { CollectionService } from 'apps/faboshop/src/services/collection.service';

@Injectable()
export class ProductsControllerService {
  onCreateProductSuccessful$ = new Subject();
  onRemovedVariant$ = new Subject();

  new_category = new Category({});
  new_collections: Array<Collection> = [];

  categories: Array<Category> = [];
  collections: Array<Collection> = [];
  showPrintBarcode = false;
  variantsBarcodeNumber = 1;

  private productListComponent: ProductListComponent;

  constructor(
    private categoryService: CategoryService,
    private collectionService: CollectionService
  ) { }

  async reloadProductList() {
    await this.productListComponent.reloadProducts();
  }

  async loadCategories() {
    const res = await this.categoryService.getCategories();
    this.categories = res.categories;
  }

  async loadCollections() {
    this.collections = await this.collectionService.getCollections();
  }

  registerProductList(instance: ProductListComponent) {
    this.productListComponent = instance;
  }

  setupData(product: Product | ShopProduct): Product | ShopProduct {
    product.p_data = {
      ...product.p_data,
      total_quantity: product.variants.reduce((acc, cur) => {
        return acc + (cur.inventory_variant ? Number(cur.inventory_variant.quantity) : 0);
      }, 0),
      code: product.code,
      id: product.id,
      name: product.name,
      tags: product.tags,
      variants: product.variants.map(v => this.setupDataVariant(v)),
      description: product.description,
      desc_html: product.desc_html,
      edited: false,
      editedField: {}
    };
    return product;
  }

  setupDataVariant(variant: Variant): Variant {
    variant.p_data = {
      ...variant.p_data,
      id: variant.id,
      name: variant.name,
      code: variant.code,
      list_price: variant.list_price,
      retail_price: variant.retail_price,
      cost_price: variant.cost_price,
      edited: false,
      attributes: variant.attributes.slice(0)
    };
    return variant;
  }

}
