import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../../../../core/src/services/util.service';
import { FulfillmentApi, NotificationApi } from '@etop/api';
import { NotificationService } from '../../../services/notification.service';
import { AuthenticateStore } from '@etop/core';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';

@Component({
  selector: 'faboshop-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss']
})
export class NotifyComponent implements OnInit {

  notification: any;
  notification_id: string;
  shop_id: string;
  availableNotiShop: any;
  selectedNotiShopToken = '';
  availableNotiShops = [];


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private conversationsQuery: ConversationsQuery,
    private conversationsService: ConversationsService,
    private notificationApi: NotificationApi,
    private fulfillmentApi: FulfillmentApi,
    private notificationService: NotificationService,
    private util: UtilService,
    private auth: AuthenticateStore

  ) {
  }

  async ngOnInit() {
    await this.parseUrl();
  }

  async parseUrl() {
    this.availableNotiShops = this.auth.snapshot.accounts.map(a => {
      return { name: a.name, code: a.token, id: a.id, url_slug: a.url_slug };
    });
    this.shop_id = this.activatedRoute.snapshot.queryParamMap.get('shop_id');
    this.activatedRoute.url.subscribe(url => {
      this.notification_id = url[0].path;
    });

    this.notification_id = this.activatedRoute.snapshot.url[0].path;

    this.availableNotiShop = this.availableNotiShops.find(a => a.id == this.shop_id);
    if (!this.availableNotiShop) {
      return await this.router.navigateByUrl(`s/0/messages`);
    }
    this.selectedNotiShopToken = this.availableNotiShop ? this.availableNotiShop.code : null;
    try {
      this.notification = await this.notificationService.getNotification(this.notification_id, this.selectedNotiShopToken);
    }catch (e) {
      return await this.router.navigateByUrl(`s/0/messages`);
    }
    if (!this.availableNotiShop?.url_slug){
      this.availableNotiShop.url_slug = this.util.getSlug()
    }
    if (!this.notification.entity) {
      return await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/messages`);
    }
    await this.navControllerOnesignal(this.notification);
  }

  async navControllerOnesignal(data) {
    switch (data.entity) {
      case 'fb_external_message':
        try {
          const conversationMessage = await this.conversationsService.getCustomerConversationByID(data.meta_data?.conversation_id)
          await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/messages/${conversationMessage?.id || ''}`);
        }catch (e) {
          await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/messages`);
        }
        break;
      case 'fb_external_comment':
        try {
          const conversationComment = await this.conversationsService.getCustomerConversationByID(data.meta_data?.conversation_id).then()
          await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/messages/${conversationComment?.id || ''}`);
        }catch (e) {
          await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/messages`);
        }
        break;
      case 'fulfillment':
        try {
          const ffm = await this.fulfillmentApi.getFulfillment(data.entity_id,this.selectedNotiShopToken).then();
          if (ffm) {
            return await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/orders?code=${ffm?.order?.code}&&type=so`);
          }else {
            await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/orders`);
          }
        }catch (e) {
          await this.router.navigateByUrl(`s/${this.availableNotiShop.url_slug}/orders`);
        }
        break;
      case 'system':
        break;
      default:
         await this.router.navigateByUrl(`s/0/messages`);
    }
  }

}
