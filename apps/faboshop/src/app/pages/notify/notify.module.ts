import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyComponent } from './notify.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: NotifyComponent },
      { path: ':id', component: NotifyComponent },
    ]
  }
];

@NgModule({
  declarations: [NotifyComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class NotifyModule { }
