import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ReturningFulfillmentsWarningModule } from 'apps/faboshop/src/app/components/returning-fulfillments-warning/returning-fulfillments-warning.module';
import { SellOrderListComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-order-list/sell-order-list.component';
import { SellOrderRowComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/sell-order-row/sell-order-row.component';
import { MultipleOrderActionsComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-actions/multiple-order-actions.component';
import { PrintComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/print/print.component';
import { ImportOrderComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/import-order/import-order.component';
import { SelectAndUpdateCustomerAddressComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/select-and-update-customer-address/select-and-update-customer-address.component';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { MultipleOrderDeliveryFastModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/multiple-order-delivery-fast.module';
import { MultipleOrderDeliveryNowModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/multiple-order-delivery-now.module';
import { SingleOrderDetailModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-detail/single-order-detail.module';
import { SingleOrderEditFormModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form.module';
import { ModalControllerModule } from 'apps/core/src/components/modal-controller/modal-controller.module';
import { AuthenticateModule } from '@etop/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BillPrintingModule } from 'apps/faboshop/src/app/components/bill-printing/bill-printing.module';
import { CancelObjectModule } from 'apps/faboshop/src/app/components/cancel-object/cancel-object.module';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { SellOrdersComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders.component';
import { EtopCommonModule, EtopFilterModule, EtopPipesModule, MaterialModule, SideSliderModule } from '@etop/shared';
import { ConfirmOrderComponent } from './components/confirm-order/confirm-order.component';
import { DeliveryInfoComponent } from './components/confirm-order/components/delivery-info/delivery-info.component';
import { DeliveryServiceComponent } from './components/confirm-order/components/delivery-info/components/delivery-service/delivery-service.component';
import { ProductsOrderComponent } from './components/confirm-order/components/products-order/products-order.component';
import { ProductComponent } from './components/confirm-order/components/products-order/product/product.component';
import { CreateProductComponent } from './components/confirm-order/components/products-order/product/create-product/create-product.component';
import { CustomerInfoOrderComponent } from './components/confirm-order/components/customer-info-order/customer-info-order.component';
import {
  ConfirmOrderController,
  ConfirmOrderService,
  ConfirmOrderStore,
  ConnectionService,
  ConnectionStore, ImageLoaderModule
} from '@etop/features';
import { AddressApi } from '@etop/api';
import { CarrierConnectPopupModule } from '../../../components/modals/carrier-connect-popup/carrier-connect-popup.module';
import { VariantComponent } from './components/confirm-order/components/products-order/product/variant/variant.component';
import { RiskWarningCustomerModule } from '../../messages/components/confirm-order/components/customer-info-order/risk-warning-customer/risk-warning-customer.module';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';

@NgModule({
  declarations: [
    SellOrdersComponent,
    SellOrderListComponent,
    SellOrderRowComponent,
    MultipleOrderActionsComponent,
    ConfirmOrderComponent,
    DeliveryInfoComponent,
    DeliveryServiceComponent,
    ProductsOrderComponent,
    ProductComponent,
    CreateProductComponent,
    VariantComponent,
    PrintComponent,
    CustomerInfoOrderComponent,
    ImportOrderComponent,
    SelectAndUpdateCustomerAddressComponent
  ],
  entryComponents: [
    SelectAndUpdateCustomerAddressComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReturningFulfillmentsWarningModule,
    DropdownActionsModule,
    MultipleOrderDeliveryFastModule,
    MultipleOrderDeliveryNowModule,
    ReturningFulfillmentsWarningModule,
    SingleOrderDetailModule,
    SingleOrderEditFormModule,
    ModalControllerModule,
    AuthenticateModule,
    RouterModule,
    CarrierConnectPopupModule,
    NgbModule,
    BillPrintingModule,
    CancelObjectModule,
    EtopCommonModule,
    EtopPipesModule,
    RiskWarningCustomerModule,
    EtopFilterModule,
    SideSliderModule,
    MaterialModule,
    ImageLoaderModule,
  ],
  exports: [
    SellOrdersComponent
  ],
  providers: [
    SellOrdersControllerService,
    ConfirmOrderService,
    AddressApi,
    ConfirmOrderStore,
    ConfirmOrderController,
    ConnectionService,
    ConnectionStore,
    ShopSettingsService
  ]
})
export class SellOrdersModule { }
