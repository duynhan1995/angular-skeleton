import { Component, Input, OnInit } from '@angular/core';
import { Order } from '@etop/models';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { SingleOrderEditFormControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form-controller.service';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-order-info-edit',
  templateUrl: './order-info-edit.component.html',
  styleUrls: ['./order-info-edit.component.scss']
})
export class OrderInfoEditComponent implements OnInit {
  @Input() order = new Order({});
  loading = false;

  constructor(
    private orderService: OrderService,
    private orderEditFormController: SingleOrderEditFormControllerService,
    private traderStore: TraderStoreService,
    private router: Router,
    private util: UtilService
  ) { }

  ngOnInit() {}

  async updateOrder() {
    this.loading = true;
    try {
      const { basket_value, total_amount, total_items, id, order_note } = this.order;
      const body = {
        basket_value,
        total_amount,
        total_items,
        id,
        order_note
      };
      await this.orderService.updateOrder(body);
      await this.orderEditFormController.updateOrderData();
      this.orderService.switchOffCreatingFFM$.next();
      toastr.success('Cập nhật đơn hàng thành công.');
    } catch (e) {
      debug.error("ERROR in Updating Order", e);
      toastr.error(`Cập nhật đơn hàng thất bại: ${e.msg || e.message}`);
    }
    this.loading = false;
  }

  async viewDetailCustomer() {
    const customer = this.order.customer;
    if (customer && customer.deleted) { return; }
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`s/${slug}/customers?code=${this.order.customer_id}&type=customer`);
  }

}
