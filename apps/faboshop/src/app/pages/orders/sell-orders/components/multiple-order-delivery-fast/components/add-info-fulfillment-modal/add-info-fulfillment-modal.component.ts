import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Order } from '@etop/models';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { SelectAndUpdateCustomerAddressComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/select-and-update-customer-address/select-and-update-customer-address.component';
import { CustomerAddressModalComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'shop-add-info-fulfillment',
  templateUrl: './add-info-fulfillment-modal.component.html',
  styleUrls: ['./add-info-fulfillment-modal.component.scss']
})
export class AddInfoFulfillmentModalComponent implements OnInit {
  @ViewChild('chargeable_weight_input', {static: false}) chargeable_weight_input: MaterialInputComponent;
  @ViewChild('cod_amount_input', {static: false}) cod_amount_input: MaterialInputComponent;
  @Input() order = new Order({});

  format_number = {
    cod_amount: true,
    chargeable_weight: true
  };
  error = {
    cod: false,
    weight: false
  };

  constructor(
    private modalController: ModalController,
    private modalAction: ModalAction,
    private changeDetector: ChangeDetectorRef,
    private ordersController: SellOrdersControllerService,
    private orderService: OrderService,
    private locationCompact: LocationCompactPipe,
    private locationQuery: LocationQuery,
  ) { }

  get uni() {
    return this.ordersController.uni;
  }

  get customerHasAddresses() {
    return this.order.customer.addresses && this.order.customer.addresses.length;
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

  get addressDisplay() {
    const shipping = this.order.p_data.shipping;
    if (!shipping.shipping_address) {
      return '';
    }
    const shipping_address = shipping.shipping_address;
    const address1 = this.locationCompact.transform(shipping_address.address1);
    const ward = shipping_address.ward && this.locationCompact.transform(shipping_address.ward);
    const district = this.locationCompact.transform(shipping_address.district);
    const province = this.locationCompact.transform(shipping_address.province);

    return `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
  }

  ngOnInit() {
    const shipping = this.order.p_data.shipping;
    this.format_number = {
      cod_amount: shipping.cod_amount >= 0,
      chargeable_weight: shipping.chargeable_weight >= 0
    };
    this.error = {
      cod: shipping.cod_amount > 0 && shipping.cod_amount < 5000,
      weight: shipping.chargeable_weight < 50
    };

  }

  onChangeCOD() {
    const cod = this.order.p_data.shipping.cod_amount;
    this.error.cod = cod > 0 && cod < 5000;
    if (this.error.cod) {
      toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
    }
  }

  onChangeWeight() {
    const weight = this.order.p_data.shipping.chargeable_weight;
    this.error.weight = weight < 50;
    if (this.error.weight) {
      toastr.error('Vui lòng nhập khối lượng từ 50g trở lên!');
    }
  }

  formatNumber(number_type: string) {
    if (this.uni[number_type]) {
      return;
    }
    const _num = this.order.p_data.shipping[number_type];
    if (!_num && _num != 0) {
      this.format_number[number_type] = false;
      return;
    }
    this.format_number[number_type] = !this.format_number[number_type];
    this.changeDetector.detectChanges();
    if (!this.format_number[number_type] && this[number_type + '_input']) {
      this[number_type + '_input'].focusInput();
    }
  }

  editShippingAddress() {
    this.closeModal();
    this.openModalAddress();
  }

  openModalAddress() {
    const component = this.customerHasAddresses &&
      SelectAndUpdateCustomerAddressComponent ||
      CustomerAddressModalComponent;

    const shipping_address = this.order.p_data.shipping.shipping_address;
    const componentProps = this.customerHasAddresses && {
      customer: {...this.order.customer},
      addresses: this.order.customer.addresses,
      current_address: shipping_address
    } || {
      address: shipping_address,
      wardRequired: false,
      confirmBtnClass: 'btn-topship'
    };

    const cssClass = this.customerHasAddresses &&
      'modal-lg' || 'modal-md';

    const modal = this.modalController.create({
      component,
      showBackdrop: 'static',
      cssClass,
      componentProps
    });
    modal.show().then();
    modal.onDismiss().then(address => {
      this.modalAction.show();
      if (!address) { return; }
      address = {
        ...address,
        province: this.locationQuery.getProvince(address.province_code)?.name,
        district: this.locationQuery.getDistrict(address.district_code)?.name,
        ward: address.ward_code && this.locationQuery.getWard(address.ward_code)?.name
      };
      if (this.order.customer_id) {
        if (this.customerHasAddresses) {
          this.order.customer.addresses.unshift(address);
        } else {
          this.order.customer.addresses = [address];
        }
      }
      this.order.p_data.shipping.shipping_address = address;
    });
  }

  closeModal() {
    this.modalAction.close(false);
  }

  confirm() {
    if (this.error.cod) {
      return toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
    }
    if (this.error.weight) {
      return toastr.error('Vui lòng nhập khối lượng từ 50g trở lên!');
    }
    this.modalAction.dismiss(this.order);
  }

}
