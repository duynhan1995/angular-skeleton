import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Order, OrderAddress } from '@etop/models';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { GoogleMapService } from 'apps/faboshop/src/services/google-map.service';
import { SelectAndUpdateCustomerAddressComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/select-and-update-customer-address/select-and-update-customer-address.component';
import { CustomerAddressModalComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'shop-multi-delivery-now-row',
  templateUrl: './multi-delivery-now-row.component.html',
  styleUrls: ['./multi-delivery-now-row.component.scss']
})
export class MultiDeliveryNowRowComponent extends BaseComponent implements OnInit, OnChanges {
  @ViewChild('moneyInput', {static: false}) moneyInput: MaterialInputComponent;
  @Input() order = new Order({});
  @Input() isFirstItem: boolean;
  @Output() toGetServices = new EventEmitter();
  @Output() toRenderMap = new EventEmitter();

  format_money = true;
  errorCOD = false;

  processingData = false;
  loading_address = false;

  constructor(
    private modalController: ModalController,
    private ordersController: SellOrdersControllerService,
    private googleMap: GoogleMapService,
    private changeDetector: ChangeDetectorRef,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  private async setupShippingAddress() {
    this.loading_address = true;
    try {
      if (this.order.shipping_address) {
        this.order.p_data.shipping.shipping_address = await this.getCoorsForAddress(
          this.order.shipping.shipping_address
        );
      } else if (this.order.customer.addresses && this.order.customer.addresses.length) {
        const found_address: any = this.order.customer.addresses[0];
        this.order.p_data.shipping.shipping_address = await this.getCoorsForAddress(found_address);
      }
    } catch(e) {
      debug.error('ERROR in setting up shipping address', e);
      this.order.p_data.shipping.shipping_address = null;
    }
    this.order.p_data.valid_shipping_address = this.isValidAddress(
      this.order.p_data.shipping.shipping_address
    );
    this.toRenderMap.emit();
    this.loading_address = false;
  }

  private async prepareData() {
    this.processingData = true;
    await this.ordersController.listOrderCustomerAddresses([this.order]);
    this.format_money = this.order.p_data.shipping.cod_amount >= 0;
    this.setupShippingAddress();
    this.processingData = false;
  }

  ngOnInit() {
    this.ordersController.onRegetShipServices$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(order_id: string) => {
        if (order_id == this.order.id) {
          this.prepareData();
        }
      });
  }

  async ngOnChanges(changes: SimpleChanges) {
    this.prepareData();
  }

  formatMoney() {
    const cod = this.order.p_data.shipping.cod_amount;
    if (!cod && cod != 0) {
      this.format_money = false;
      return;
    }
    this.format_money = !this.format_money;
    this.changeDetector.detectChanges();
    if (!this.format_money && this.moneyInput) {
      this.moneyInput.focusInput();
    }
  }

  onChangeCOD() {
    const cod = this.order.p_data.shipping.cod_amount;
    this.errorCOD = cod > 0 && cod < 5000;
    if (this.errorCOD) {
      toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
    }
    this.toGetServices.emit();
  }

  noAddress(order: Order): boolean {
    return this.ordersController.noAddress(order);
  }

  createNewAddress(order) {
    const componentProps = {
      customer_id: order.customer.id,
      title: 'create',
      confirmBtnClass: 'btn-topship'
    };
    this.openModalAddress(order, componentProps);
  }

  editShippingAddress(order) {
    const shipping_address = order.p_data.shipping.shipping_address;
    const componentProps = {
      customer: {...order.customer},
      addresses: order.customer.addresses,
      current_address: shipping_address
    };
    this.openModalAddress(order, componentProps);
  }

  openModalAddress(order, componentProps) {
    const component = order.customer.addresses && order.customer.addresses.length > 0 &&
      SelectAndUpdateCustomerAddressComponent ||
      CustomerAddressModalComponent;

    const cssClass = order.customer.addresses && order.customer.addresses.length > 0 &&
      'modal-lg' || 'modal-md';

    const modal = this.modalController.create({
      component,
      showBackdrop: 'static',
      cssClass,
      componentProps
    });
    modal.show().then();
    modal.onDismiss().then(async(address) => {
      if (!address) { return; }
      address = {
        ...address,
        province: this.locationQuery.getProvince(address.province_code)?.name,
        district: this.locationQuery.getDistrict(address.district_code)?.name,
        ward: address.ward_code && this.locationQuery.getWard(address.ward_code)?.name
      };
      address = await this.getCoorsForAddress(address);
      if (order.customer.addresses && order.customer.addresses.length > 0) {
        this.order.customer.addresses.unshift(address);
      }
      order.p_data.shipping.shipping_address = address;
      order.p_data.valid_shipping_address = this.isValidAddress(order.p_data.shipping.shipping_address);
      this.toRenderMap.emit();
    });
  }

  async getCoorsForAddress(address: OrderAddress) {
    const { address1, ward, district, province } = address;
    const address_fulltext = `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
    const result: any = await this.googleMap.getLatLngFromAddress(address_fulltext);
    const { lat, lng } = result;
    address.coordinates = {
      latitude: lat,
      longitude: lng
    };
    return address;
  }

  isValidAddress(address) {
    if (!address) { return false; }
    // must be in SG or HN
    if (GoogleMapService.allowedProvinces.indexOf(address.province_code) == -1) {
      return false;
    }
    const pickup_address = this.ordersController.shipping_data.pickup_address;
    // must be in the same province with pickup_address
    if (address.province_code != pickup_address.province_code) {
      return false;
    }
    // must have both latitude and longitude
    const coors = address.coordinates;
    return coors && coors.latitude && coors.longitude;
  }

}
