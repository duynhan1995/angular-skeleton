import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineInfoEditComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-edit-form/line-info-edit/line-info-edit.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EditDropdownComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-edit-form/line-info-edit/edit-dropdown/edit-dropdown.component';
import { AuthenticateModule } from '@etop/core';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [LineInfoEditComponent, EditDropdownComponent],
  exports: [LineInfoEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule,
    AuthenticateModule,
    EtopMaterialModule,
    EtopPipesModule,
  ]
})
export class LineInfoEditModule { }
