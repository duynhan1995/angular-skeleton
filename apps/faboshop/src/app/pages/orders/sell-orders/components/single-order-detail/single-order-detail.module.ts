import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderInfoComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-detail/order-info/order-info.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LineInfoComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-detail/line-info/line-info.component';
import { ShippingInfoComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-detail/shipping-info/shipping-info.component';
import { RouterModule } from '@angular/router';
import { SingleOrderDetailComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-detail/single-order-detail.component';
import { ForceButtonModule } from 'apps/faboshop/src/app/components/force-button/force-button.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TicketModule } from 'apps/faboshop/src/app/components/ticket/ticket.module';
import { FulfillmentHistoriesModule } from 'apps/faboshop/src/app/components/fulfillment-histories/fulfillment-histories.module';
import { PaymentInfoModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/payment-info/payment-info.module';
import { AuthenticateModule } from '@etop/core';
import { CancelObjectModule } from 'apps/faboshop/src/app/components/cancel-object/cancel-object.module';
import { OrderFulfillmentsModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/order-fulfillments/order-fulfillments.module';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { InventoryInfoModule } from '../inventory-info/inventory-info.module';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { OrderFulfillmentDetailComponent } from './order-fulfillment-detail/order-fulfillment-detail.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { EditShippingInfoComponent } from './components/edit-shipping-info/edit-shipping-info.component';
import { EditPickupInfoComponent } from './components/edit-pickup-info/edit-pickup-info.component';
import { MatDialogModule } from '@angular/material/dialog';
import { EditCodComponent } from './components/edit-cod/edit-cod.component';
import { EditShippingNoteComponent } from './components/edit-shipping-note/edit-shipping-note.component';
import { EditInsuranceComponent } from './components/edit-insurance/edit-insurance.component';
import { EditWeightComponent } from './components/edit-weight/edit-weight.component';

@NgModule({
  declarations: [
    SingleOrderDetailComponent,
    OrderInfoComponent,
    LineInfoComponent,
    ShippingInfoComponent,
    OrderFulfillmentDetailComponent,
    EditShippingInfoComponent,
    EditPickupInfoComponent,
    EditCodComponent,
    EditShippingNoteComponent,
    EditInsuranceComponent,
    EditWeightComponent
  ],
  exports: [
    SingleOrderDetailComponent,
    OrderInfoComponent,
    LineInfoComponent,
    ShippingInfoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule,
    ForceButtonModule,
    TicketModule,
    NgbModule,
    FulfillmentHistoriesModule,
    PaymentInfoModule,
    AuthenticateModule,
    CancelObjectModule,
    OrderFulfillmentsModule,
    DropdownActionsModule,
    InventoryInfoModule,
    EtopMaterialModule,
    EtopPipesModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleOrderDetailModule {}
