import { Component, OnInit, Input, SimpleChanges, OnChanges, ViewChild } from '@angular/core';
import { Order } from 'libs/models/Order';
import { OrderReceiptsComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/order-receipts/order-receipts.component';

@Component({
  selector: 'shop-payment-info',
  templateUrl: './payment-info.component.html',
  styleUrls: ['./payment-info.component.scss']
})
export class PaymentInfoComponent implements OnInit, OnChanges {
  @ViewChild('orderReceipts', { static: true }) orderReceipts: OrderReceiptsComponent;
  @Input() order = new Order({});
  total_amount: number;

  constructor(
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  get isPaymentDone() {
    // TODO: temporarily doing this!!! wait for VALID order.payment_status
    return this.order.received_amount >= this.order.total_amount;
  }

  get codGreaterThanTotalAmount() {
    const ffm = this.order.activeFulfillment;
    return ffm && ffm.cod_amount >= this.order.total_amount && ffm.status != 'N';
  }

  get cannotCreateReceipt() {
    return this.isPaymentDone || this.codGreaterThanTotalAmount || this.order.status == 'N';
  }

  get disabledTooltip() {
    if (this.codGreaterThanTotalAmount) {
      return 'Tiền thu hộ không nhỏ hơn số tiền khách phải trả, không thể tạo phiếu thu!';
    }
    if (this.isPaymentDone) {
      return 'Đơn hàng này đã thanh toán đủ, không thể tạo phiếu thu!';
    }
    if (this.order.status == 'N') {
      return 'Đơn hàng đã huỷ, không thể tạo phiếu thu!';
    }
    return '';
  }

  openCreateReceiptModal() {
  }

}
