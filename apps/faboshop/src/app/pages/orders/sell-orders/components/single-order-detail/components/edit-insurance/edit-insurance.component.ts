import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FulfillmentApi } from '@etop/api';
import { ShippingService } from 'apps/faboshop/src/services/shipping.service';
import { OrdersService } from '@etop/state/order';

export interface DialogData {
  fulfillment: any;
  new_value: any;
}

enum Step {
  EDIT = 'edit',
  CONFIRM = 'confirm',
}

@Component({
  selector: 'faboshop-edit-insurance',
  templateUrl: './edit-insurance.component.html',
  styleUrls: ['./edit-insurance.component.scss']
})
export class EditInsuranceComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();
  _step = Step.EDIT;
  step = Step;
  shipping_fee;
  loadingContinue = false;
  loadingConfirm = false;
  formInsurance = this.fb.group({
    insurance_value: null,
    include_insurance: true
  });
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditInsuranceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private orderService: OrdersService,
    private shippingService: ShippingService
  ) {}

  ngOnInit(): void {
    this.formInsurance.setValue({
      insurance_value: this.data.fulfillment.insurance_value ? this.data.fulfillment.insurance_value : 0,
      include_insurance: true
    });
  }

  currentView(step: Step) {
    return this._step == step;
  }

  async submit() {
    try {
      this.loadingConfirm = true;
      await this.orderService.updateInsurance(this.data.fulfillment, this.formInsurance.controls['insurance_value'].value)
      this.loadingConfirm = false;
      this.submitClicked.emit(true);
      this.dialogRef.close();
    } catch (e) {
      this.loadingConfirm = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }

  async confirmStep() {
    if (
      this.formInsurance.controls['insurance_value'].value ==
      this.data.fulfillment.insurance_value
    ) {
      return toastr.error('Vui lòng thay đổi giá trị khai báo hàng hóa');
    }
    let data: any = this.shippingService.parseFulfillmentData(
      this.data.fulfillment
    );
    data = Object.assign({}, data, {
      insurance_value: Number(this.formInsurance.controls['insurance_value'].value),
      include_insurance: true,

      connection_ids: [this.data.fulfillment.connection_id]
    });
    this._step = Step.CONFIRM;
    try {
      this.loadingContinue = true;
      const services = await this.shippingService.getShippingServices(data);
      const service = this.shippingService.shippingServiceMap(
        services.services,
        this.data.fulfillment
      );
      this.shipping_fee = service[0]?.fee;
      this.loadingContinue = false;
    } catch (e) {
      this.shipping_fee = false;
      this.loadingContinue = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in getShippingServices', e);
    }
  }

  back() {
    this._step = Step.EDIT;
  }

  formatNumber() {
    const _value = this.formInsurance.controls['insurance_value'].value?.toString();
    if (!_value || !_value.length || Number(_value) < 0) {
      this.formInsurance.setValue({
        insurance_value: 0,
        include_insurance: true
      });
      return;
    }
  }
}
