import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Order, OrderAddress, ShipnowDeliveryPoint } from '@etop/models';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { MultiOrderNowController } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/multi-order-now-controller.service';
import { GoogleMapService } from 'apps/faboshop/src/services/google-map.service';
import { MultiOrderShippingNowComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/components/multi-order-shipping-now/multi-order-shipping-now.component';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { UserService } from 'apps/core/src/services/user.service';
import { VerifyAccountAhamovePopupComponent } from 'apps/faboshop/src/app/components/delivery/delivery-now/verify-account-ahamove-popup/verify-account-ahamove-popup.component';
import { takeUntil } from 'rxjs/operators';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: 'shop-multiple-order-delivery-now',
  templateUrl: './multiple-order-delivery-now.component.html',
  styleUrls: ['./multiple-order-delivery-now.component.scss']
})
export class MultipleOrderDeliveryNowComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('multiOrderShippingNow', {static: false}) multiOrderShippingNow: MultiOrderShippingNowComponent;

  @Input() orders: Array<Order> = [];
  @Input() scroll_id: string;

  getting_services = false;
  creating_ffm = false;

  constructor(
    private ordersController: SellOrdersControllerService,
    private modalController: ModalController,
    private orderService: OrderService,
    private promiseQueue: PromiseQueueService,
    private auth: AuthenticateStore,
    private dNowController: MultiOrderNowController,
    private googleMap: GoogleMapService,
    private ffmService: FulfillmentService,
    private user: UserService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  get totalCodAmount() {
    return this.orders.reduce((a,b) => {
      if (this.ordersController.orderToBeUpdated(b) && b.p_data.valid_shipping_address) {
        return a + Number(b.p_data.shipping.cod_amount);
      }
      return a;
    }, 0) || 0;
  }

  get totalOrders() {
    return this.orders.filter(o =>
      this.ordersController.orderToBeUpdated(o) && o.p_data.valid_shipping_address
    ).length;
  }

  get hasValidOrders() {
    return this.orders.some(o =>
      this.ordersController.orderToBeUpdated(o) && o.p_data.valid_shipping_address
    );
  }

  ngOnInit() {
    this.ordersController.removeMapMarkers$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.toRenderMap();
      });
    this.ordersController.onUpdateShippingData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        for (let o of this.orders) {
          if (this.ordersController.orderToBeUpdated(o)) {
            await this.getCoorsForAddress(o.p_data.shipping.shipping_address);
            o.p_data.valid_shipping_address = this.isValidAddress(o.p_data.shipping.shipping_address);
          }
        }
        this.toRenderMap();
      });
    this.dNowController.onCreatingFFM$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(data: any) => {
        this.createFFM(data.selected_service, data.orders);
      });
  }

  ngOnDestroy() {
    this.ordersController.deliveryFast = false;
    this.ordersController.deliveryNow = false;
    this.ffmStore.discardCreatingFulfillment();
  }

  async getCoorsForAddress(address: OrderAddress) {
    if (!address) {
      return null;
    }
    const { address1, ward, district, province } = address;
    const address_fulltext = `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
    const result: any = await this.googleMap.getLatLngFromAddress(address_fulltext);
    const { lat, lng } = result;
    address.coordinates = {
      latitude: lat,
      longitude: lng
    };
    return address;
  }

  noAddress(order: Order): boolean {
    return this.ordersController.noAddress(order);
  }

  selectedOrder(order: Order): boolean {
    return this.ordersController.isSelectedOrder(order);
  }

  canCreateFulfillment(order: Order) {
    return SellOrdersControllerService.canCreateFulfillment(order);
  }

  async reCreateFFM(order: Order) {
    order.activeFulfillment = null;
    order.p_data.un_updatable = false;
    this.getServices();
  }

  async getServices() {
    await this.multiOrderShippingNow.getServices();
  }

  onFailedCreatingFFM() {
    const _orders = this.orders.filter(o =>
      this.ordersController.orderToBeUpdated(o) && o.p_data.valid_shipping_address);
    this.dNowController.onFailedCreatingFFM$.next(_orders);
  }

  async createFFM(selected_service, orders: Array<Order>) {
    this.creating_ffm = true;
    try {
      const delivery_points: ShipnowDeliveryPoint[] = orders.map(o => {
        return {
          chargeable_weight: 1000,
          cod_amount: o.p_data.shipping.cod_amount,
          order_id: o.id,
          shipping_address: o.p_data.shipping.shipping_address
        };
      });
      const body = {
        delivery_points,
        carrier: selected_service && selected_service.carrier || null,
        shipping_service_code: selected_service && selected_service.code || null,
        shipping_service_fee: selected_service && selected_service.fee || null,
        shipping_note: '',
        pickup_address: this.ordersController.shipping_data.pickup_address
      };
      if (!body.carrier) {
        toastr.error('Vui lòng chọn gói vận chuyển!');
        throw new Error('Chưa chọn gói vận chuyển');
      }
      const shipnow_ffm = await this.ffmService.createShipnowFulfillment(body);
      await this.confirmShipnowFFM(shipnow_ffm.id);
      this.dNowController.onSuccessCreatingFFM$.next();
    } catch(e) {
      debug.error("ERROR in createShipnowFFM", e);
      this.onFailedCreatingFFM();
    }
    this.creating_ffm = false;
  }

  async confirmShipnowFFM(shipnow_id: string) {
    try {
      const message_data = await this.ffmService.confirmShipnowFulfillment(shipnow_id);
      toastr.success('Xác nhận đơn giao hàng tức thì thành công!');
      this.ordersController.reloadOrderList();
    } catch(e) {
      this.ffmService.cancelShipnowFulfillment(shipnow_id, 'Xác nhận đơn thất bại');
      debug.error("ERROR in confirmShipnowFFM", e);
      throw new Error(`Xác nhận đơn giao hàng tức thì thất bại. ${e.message || e.msg}`);
    }
  }

  isValidAddress(address) {
    if (!address) { return false; }
    // must be in SG or HN
    if (GoogleMapService.allowedProvinces.indexOf(address.province_code) == -1) {
      return false;
    }
    const pickup_address = this.ordersController.shipping_data.pickup_address;
    // must be in the same province with pickup_address
    if (address.province_code != pickup_address.province_code) {
      return false;
    }
    // must have both latitude and longitude
    const coors = address.coordinates;
    return coors && coors.latitude && coors.longitude;
  }

  openVerifyAhamovePopup() {
    let modal = this.modalController.create({
      component: VerifyAccountAhamovePopupComponent,
      showBackdrop: 'static',
      componentProps: {},
      cssClass: 'modal-xl'
    });
    modal.show().then(async() => {
    });
    modal.onDismiss().then(_ => {});
  }

  toRenderMap() {
    const _orders = this.orders.filter(o =>
      this.ordersController.orderToBeUpdated(o) && o.p_data.valid_shipping_address);
    this.dNowController.onRenderingMap$.next(_orders);
  }

}
