import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryInfoComponent } from './inventory-info.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EtopMaterialModule,
  ],
  exports: [
    InventoryInfoComponent
  ],
  declarations: [InventoryInfoComponent]
})
export class InventoryInfoModule { }
