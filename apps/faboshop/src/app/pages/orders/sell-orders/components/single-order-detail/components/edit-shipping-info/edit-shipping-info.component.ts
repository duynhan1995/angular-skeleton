import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FulfillmentApi } from '@etop/api';
import { ShippingService } from 'apps/faboshop/src/services/shipping.service';
import { OrdersService } from '@etop/state/order';
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";

export interface DialogData {
  fulfillment: any;
  new_value: any;
}

enum Step {
  EDIT = 'edit',
  CONFIRM = 'confirm',
}

@Component({
  selector: 'faboshop-edit-shipping-info',
  templateUrl: './edit-shipping-info.component.html',
  styleUrls: ['./edit-shipping-info.component.scss']
})
export class EditShippingInfoComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();
  provinces;
  districts;
  wards;
  shipping_address = this.fb.group({
    full_name: '',
    phone: '',
    address1: '',
    province_code: '',
    province: '',
    district_code: '',
    district: '',
    ward_code: '',
    ward: ''
  });
  _step = Step.EDIT;
  step = Step;
  shipping_fee;
  loadingContinue = false;
  loadingConfirm = false;
  checkDifferentAddress = false;
  checkDifferentName = false;
  checkDifferentPhone = false;

  valueMap = option => option && option.code || null;
  displayMap = option => option && option.name || null;
  constructor(
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditShippingInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private orderService: OrdersService,
    private shippingService: ShippingService
  ) {}

  ngOnInit(): void {
    const {
      full_name,
      phone,
      address1,
      province_code,
      province,
      district_code,
      district,
      ward_code,
      ward
    } = this.data.fulfillment.shipping_address;
    this.shipping_address.setValue({
      full_name,
      phone,
      address1,
      province_code,
      province,
      district_code,
      district,
      ward_code,
      ward
    });
    this._prepareLocationData();
    debug.log('ffm', this.data.fulfillment);
  }

  currentView(step: Step) {
    return this._step == step;
  }

  _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.districts = this.locationService.filterDistrictsByProvince(
      this.shipping_address.controls['province_code'].value
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.shipping_address.controls['district_code'].value
    );
  }

  onProvinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(
      this.shipping_address.controls['province_code'].value
    );
    this.shipping_address.patchValue({
      district_code: '',
      ward_code: ''
    });
  }

  onDistrictSelected() {
    this.wards = this.locationService.filterWardsByDistrict(
      this.shipping_address.controls['district_code'].value
    );
    this.shipping_address.patchValue({
      ward_code: ''
    });
  }

  async submit() {
    try {
      this.loadingConfirm = true;
      await this.orderService.updateShippingInfo(this.data.fulfillment, this.shipping_address.value)
      this.loadingConfirm = false;
      this.submitClicked.emit(true);
      this.dialogRef.close();
    } catch (e) {
      this.loadingConfirm = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }

  async confirmStep() {
    const province = this.provinces.find(
      p => p.code == this.shipping_address.controls['province_code'].value
    )?.name;
    const district = this.districts.find(
      d => d.code == this.shipping_address.controls['district_code'].value
    )?.name;
    const ward = this.wards.find(
      w => w.code == this.shipping_address.controls['ward_code'].value
    )?.name;
    this.shipping_address.patchValue({
      province,
      district,
      ward
    });
    let _checkDifferent = this.checkDifferent(
      this.data.fulfillment.shipping_address,
      this.shipping_address.value
    );
    if (_checkDifferent) {
      if (!this.shipping_address.controls['full_name'].value) {
        return toastr.error('Vui lòng nhập tên người nhận.');
      }
      if (!this.shipping_address.controls['phone'].value) {
        return toastr.error('Vui lòng nhập số điện thoại.');
      }
      if (!this.shipping_address.controls['address1'].value) {
        return toastr.error('Vui lòng nhập địa chỉ.');
      }
      if (!this.shipping_address.controls['province_code'].value) {
        return toastr.error('Vui lòng chọn tỉnh thành.');
      }
      if (!this.shipping_address.controls['district_code'].value) {
        return toastr.error('Vui lòng chọn quận huyện.');
      }
      if (!this.shipping_address.controls['ward_code'].value) {
        return toastr.error('Vui lòng chọn phường xã.');
      }
      let data: any = this.shippingService.parseFulfillmentData(
        this.data.fulfillment
      );
      const {
        province_code,
        district_code,
        ward_code
      } = this.shipping_address.value;
      data = Object.assign({}, data, {
        to_province_code: province_code,
        to_district_code: district_code,
        to_ward_code: ward_code,
        connection_ids: [this.data.fulfillment.connection_id]
      });
      this.data.new_value = this.shipping_address.value;
      this._step = Step.CONFIRM;
      try {
        this.loadingContinue = true;
        const services = await this.shippingService.getShippingServices(data);
        const service = this.shippingService.shippingServiceMap(
          services.services,
          this.data.fulfillment
        );
        this.shipping_fee = service[0]?.fee;
        this.loadingContinue = false;
      } catch (e) {
        this.shipping_fee = false;
        this.loadingContinue = false;
        toastr.error(e.message || e.msg);
        debug.log('ERROR in getShippingServices', e);
      }
    } else {
      return toastr.error('Vui lòng thay đổi thông tin để tiếp tục');
    }
  }

  back() {
    this._step = Step.EDIT;
  }

  checkDifferent(oldData, newData) {
    this.checkDifferentAddress = oldData.address1 != newData.address1 ||
    oldData.ward_code != newData.ward_code;
    this.checkDifferentName = oldData.full_name != newData.full_name;
    this.checkDifferentPhone = oldData.phone != newData.phone;
    return (
      this.checkDifferentName ||
      this.checkDifferentPhone ||
      this.checkDifferentAddress
    );
  }
}
