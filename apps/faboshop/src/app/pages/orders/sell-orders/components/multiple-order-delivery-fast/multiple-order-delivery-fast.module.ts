import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultipleOrderDeliveryFastComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/multiple-order-delivery-fast.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiOrderShippingFastComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/components/multi-order-shipping-fast/multi-order-shipping-fast.component';
import { SelectShippingServiceBoxComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/components/select-shipping-service-box/select-shipping-service-box.component';
import { MultiDeliveryFastRowComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/components/multi-delivery-fast-row/multi-delivery-fast-row.component';
import { AddInfoFulfillmentModalComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/components/add-info-fulfillment-modal/add-info-fulfillment-modal.component';
import { MoDeliveryFastStoreService } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/mo-delivery-fast.store.service';
import { EtopFilterModule, EtopMaterialModule, EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    MultipleOrderDeliveryFastComponent,
    MultiOrderShippingFastComponent,
    SelectShippingServiceBoxComponent,
    MultiDeliveryFastRowComponent,
    AddInfoFulfillmentModalComponent
  ],
  entryComponents: [
    AddInfoFulfillmentModalComponent
  ],
  exports: [
    MultipleOrderDeliveryFastComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    EtopMaterialModule,
    EtopPipesModule,
    EtopFilterModule,
  ],
  providers: [
    MoDeliveryFastStoreService
  ]
})
export class MultipleOrderDeliveryFastModule { }
