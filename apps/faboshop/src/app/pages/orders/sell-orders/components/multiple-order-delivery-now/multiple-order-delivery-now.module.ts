import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultipleOrderDeliveryNowComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/multiple-order-delivery-now.component';
import { MultiOrderNowController } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/multi-order-now-controller.service';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiOrderShippingNowComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/components/multi-order-shipping-now/multi-order-shipping-now.component';
import { DeliveryModule } from 'apps/faboshop/src/app/components/delivery/delivery.module';
import { DragdropListModule } from 'apps/shared/src/components/dragdrop-list/dragdrop-list.module';
import { MultiDeliveryNowRowComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/components/multi-delivery-now-row/multi-delivery-now-row.component';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    MultipleOrderDeliveryNowComponent,
    MultiOrderShippingNowComponent,
    MultiDeliveryNowRowComponent
  ],
  exports: [
    MultipleOrderDeliveryNowComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    DeliveryModule,
    DragdropListModule,
    EtopMaterialModule,
    EtopPipesModule,
  ],
  providers: [
    MultiOrderNowController
  ]
})
export class MultipleOrderDeliveryNowModule { }
