import { Component, OnInit } from '@angular/core';
import {CustomerApi, FulfillmentAPI, FulfillmentApi, OrderAPI, OrderApi} from '@etop/api';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { Address, Fulfillment, Order } from '@etop/models';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { CreateCustomerPopupComponent } from 'apps/faboshop/src/app/components/modals/create-customer-popup/create-customer-popup.component';
import { UtilService } from 'apps/core/src/services/util.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { AddressDisplayPipe } from '@etop/shared';
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { ListAddressesPopupComponent } from '../../../../../components/modals/list-addresses-popup/list-addresses-popup.component';
import { CreateUpdateAddressPopupComponent } from '../../../../../components/modals/create-update-address-popup/create-update-address-popup.component';
import { ConversationsService } from '@etop/state/fabo/conversation';
import { OrdersService } from '@etop/state/order';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { ShopSettings } from '@etop/models/ShopSettings';

@Component({
  selector: 'faboshop-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.scss']
})
export class ConfirmOrderComponent extends BaseComponent implements OnInit {

  confirmingOrder = false;
  modal: any;
  shopSettings: ShopSettings;

  private static validateOrderInfo(order: Order): boolean {
    const { total_amount, lines, customer } = order;

    if (!customer.full_name || !customer.full_name.trim()) {
      toastr.error('Chưa nhập tên khách hàng');
      return false;
    }

    if (!customer.phone || !customer.phone.trim()) {
      toastr.error('Chưa nhập số điện thoại khách hàng');
      return false;
    }

    if (!lines.length) {
      toastr.error('Chưa chọn sản phẩm');
      return false;
    }

    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];
      if (!line.product_name || !line.product_name.trim()) {
        toastr.error(`Chưa nhập tên cho sản phẩm thứ ${i + 1}`);
        return false;
      }
      if (!line.payment_price && line.payment_price != 0) {
        toastr.error(`Chưa nhập đơn giá cho sản phẩm thứ ${i + 1}`);
        return false;
      }
      if (!line.quantity && line.quantity != 0) {
        toastr.error(`Chưa nhập số lượng cho sản phẩm thứ ${i + 1}`);
        return false;
      }
    }

    if (total_amount < 0) {
      toastr.error('Thành tiền không được âm.');
      return false;
    }

    return true;

  }

  private static validateFulfillmentInfo(ffm: Fulfillment): boolean {
    const { pickup_address, shipping_address, chargeable_weight, cod_amount, shipping_service_code, insurance_value } = ffm;


    if (!pickup_address?.address1) {
      toastr.error('Chưa nhập địa chỉ lấy hàng.');
      return false;
    }

    if (insurance_value == 0) {
      toastr.error('Cần điền giá trị khai báo hàng hóa.');
      return false;
    }

    if (!shipping_address?.address1) {
      toastr.error('Chưa nhập địa chỉ giao hàng.');
      return false;
    }

    if (!pickup_address?.province_code) {
      toastr.error('Chưa chọn tỉnh thành lấy hàng.');
      return false;
    }

    if (!shipping_address?.province_code) {
      toastr.error('Chưa chọn tỉnh thành giao hàng.');
      return false;
    }

    if (!pickup_address?.district_code) {
      toastr.error('Chưa chọn quận huyện lấy hàng.');
      return false;
    }

    if (!shipping_address?.district_code) {
      toastr.error('Chưa chọn quận huyện giao hàng.');
      return false;
    }

    if (!pickup_address?.ward_code) {
      toastr.error('Chưa chọn phường xã lấy hàng.');
      return false;
    }

    if (!shipping_address?.ward_code) {
      toastr.error('Chưa chọn phường xã giao hàng.');
      return false;
    }

    if (!chargeable_weight) {
      toastr.error('Chưa nhập khối lượng.');
      return false;
    }

    if (cod_amount != 0 && cod_amount < 5000) {
      toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
      return false;
    }

    if (!insurance_value) {
      toastr.error('Vui lòng nhập Giá trị khai giá!');
      return false;
    }

    if (!shipping_service_code) {
      toastr.error('Chưa chọn gói vận chuyển.');
      return false;
    }

    return true;
  }

  constructor(
    private shopSettingService: ShopSettingsService,
    private util: UtilService,
    private modalController: ModalController,
    private conversationsService: ConversationsService,
    private orderApi: OrderApi,
    private addressDisplay: AddressDisplayPipe,
    private fulfillmentApi: FulfillmentApi,
    private customerApi: CustomerApi,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderStore: ConfirmOrderStore,
    private customerService: CustomerService,
    private orderService: OrdersService
  ) {
    super();
  }

  ngOnInit() {
    this.conversationsService.resetActiveFbUser();
    this.initData();
    this.confirmOrderStore.forceUpdateForm$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.initData();
      });
  }

  initData() {
    this.conversationsService.resetActiveFbUser()
    const _order = this.confirmOrderStore.snapshot.order;
    this.confirmOrderService.updateOrder({
      ..._order,
      id: null
    }, true);
  }


  openCreateCustomerPopup() {
    if (this.modal) {
      return;
    }
    const customer = this.confirmOrderStore.snapshot.customer;
    this.modal = this.modalController.create({
      component: CreateCustomerPopupComponent,
      componentProps: {
        customer: this.util.deepClone(customer)
      }
    });
    this.modal.show().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
    this.modal.onDismiss().then(async (success) => {
      this.modal = null;
      if (success) {
        await this.confirmOrder(success);
      }
    });
  }

  fullAddress(address) {
    return address?.address1 + address?.province_code + address?.district_code + address?.ward_code;
  }

  async confirmOrder(createCustomer?) {
    this.shopSettings = await this.shopSettingService.getSetting();
    const order = this.confirmOrderStore.snapshot.order;
    const customer = this.confirmOrderStore.snapshot.customer;
    const _ffm = this.confirmOrderStore.snapshot.fulfillment;

    this.confirmOrderService.setResetData(false);

    if (!ConfirmOrderComponent.validateOrderInfo(order)) {
      return;
    }
    if (!ConfirmOrderComponent.validateFulfillmentInfo(_ffm)) {
      return;
    }

    const oldAddress = this.confirmOrderStore.snapshot.activeCustomerAddress;
    const newAddress = _ffm.shipping_address;

    const _oldAddress = oldAddress && oldAddress?.address1?.toLowerCase() + oldAddress.ward_code + oldAddress.district_code + oldAddress.province_code + oldAddress.full_name + oldAddress.phone;
    const _newAddress = newAddress && newAddress?.address1?.toLowerCase() + newAddress.ward_code + newAddress.district_code + newAddress.province_code + newAddress.full_name + newAddress.phone;
    if (!createCustomer) {
      if (oldAddress) {
        if (_oldAddress != _newAddress) {
          _ffm.shipping_address = await this.customerApi.createCustomerAddress({
            ...newAddress,
            phone: customer.phone,
            full_name: customer.full_name,
            customer_id: oldAddress?.customer_id
          });
        }
      } else {
        this.confirmOrderService.activeToAddress(newAddress);
        return this.openCreateCustomerPopup();
      }
    }

    const ffm = {
      ..._ffm,
      shipping_address: {
        ..._ffm.shipping_address,
        phone: customer.phone,
        name: customer.full_name
      }
    }

    let customerId = customer.id ? customer.id : oldAddress.customer_id;
    this.confirmingOrder = true;
    try {
      const {
        id,
        total_items, total_amount, total_fee, total_discount,
        basket_value, order_discount,
        fee_lines, lines, shipping
      } = order;

      const body: OrderAPI.CreateOrderRequest = {
        source: 'etop_pos',
        payment_method: 'cod',
        customer_id: customerId,
        total_items, total_amount, total_fee, total_discount,
        basket_value, order_discount,
        fee_lines, lines
      };

      let res;
      if (!id) {
        res = await this.orderApi.createOrder(body);
        order.id = res.id;

        const _order = this.confirmOrderStore.snapshot.order;
        this.confirmOrderService.updateOrder({
          ..._order,
          id: res.id
        });
      } else {
        res = await this.orderApi.updateOrder({
          ...body,
          id
        });
      }
      await this.orderApi.confirmOrder(res.id);

      const {
        pickup_address, shipping_address,
        chargeable_weight, cod_amount,
        coupon, insurance_value,
        shipping_service_code, shipping_service_fee, shipping_service_name, connection_id,
        try_on, shipping_note, include_insurance
      } = ffm;

      const ffmBody: FulfillmentAPI.CreateShipmentFulfillmentRequest = {
        pickup_address, shipping_address,
        coupon: coupon,
        insurance_value: insurance_value,
        chargeable_weight, cod_amount,
        shipping_service_code, shipping_service_fee, shipping_service_name, connection_id,
        try_on, shipping_note, include_insurance,
        order_id: order.id, shipping_type: 'shipment', shipping_payment_type: shipping.shipping_payment_type
      };
      if (this.shopSettings?.return_address) {
        ffmBody.return_address = this.shopSettings.return_address
      }
      await this.fulfillmentApi.createShipmentFulfillment(ffmBody);
      this.orderService.addNewOrder(res);
      toastr.success('Chốt đơn thành công.');
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: null
      })
      this.confirmOrderService.forceUpdateForm();
      this.confirmOrderService.setResetData(true);
      this.confirmOrderService.activeCustomerAddress(null);
    } catch (e) {
      debug.error('ERROR in confirmOrder', e);
      this.confirmOrderService.setResetData(false);
      toastr.error(`Chốt đơn không thành công. ${e.code && (e.message || e.msg)}`);
    }
    this.confirmingOrder = false;
  }

  async changeAddress(type: 'from' | 'to') {
    const fromAddresses = this.confirmOrderStore.snapshot.fromAddresses;
    const toAddresses = this.confirmOrderStore.snapshot.toAddresses;
    //TODO: case when no customer.id & focus to input => directly open createAddressPopup
    const modal = this.modalController.create({
      component: ListAddressesPopupComponent,
      componentProps: {
        type,
        addresses: type == 'from' ? fromAddresses : toAddresses
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data?.createAddress) {
        this.createOrUpdateAddress(type, new Address({}));
      }
      if (data?.updateAddress) {
        this.createOrUpdateAddress(type, data?.updateAddress);
      }
      if (data?.address) {
        const _address = data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.confirmOrderService.selectFromAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.confirmOrderService.selectToAddress(_address);
        }
      }

    });
    if ((toAddresses.length && type == 'to') || (fromAddresses.length && type == 'from')) {
      modal.show().then();
    } else {
      this.createOrUpdateAddress(type, new Address({}));
    }
  }

  async createOrUpdateAddress(type: 'from' | 'to', address: Address) {
    const modal = this.modalController.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data?.closed) {
        this.changeAddress(type);
      }
      if (data?.address) {
        const _address = data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.createOrUpdateShopAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.createOrUpdateCustomerAddress(_address);
        }
      }
    });
    modal.show().then();
  }

  createCustomerAddress(customer, address) {
    const { province_code, district_code, ward_code, address1 } = address;
    const customerAddress: any = {
      customer_id: customer.id,
      province_code: province_code,
      district_code: district_code,
      ward_code: ward_code,
      address1: address1,
      email: customer.email,
      phone: customer.phone,
      full_name: customer.full_name
    };
    if (province_code && district_code && ward_code && address1) {
      this.customerService.createCustomerAddress(customerAddress);
    }
  }

  async createOrUpdateShopAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateFromAddress(address);
    } else {
      await this.confirmOrderService.createFromAddress(address);
    }
  }

  async createOrUpdateCustomerAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateToAddress(address);
    } else {
      await this.confirmOrderService.createToAddress(address);
    }
  }

  get pickupAddressDisplay() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    return this.addressDisplay.transform(ffm.pickup_address);
  }

}
