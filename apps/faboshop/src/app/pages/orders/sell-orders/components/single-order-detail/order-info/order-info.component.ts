import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent implements OnInit {
  @Input() order = new Order({});

  constructor(
    private traderStore: TraderStoreService,
    private router: Router,
    private util: UtilService
  ) {}

  ngOnInit() {}

  async viewDetailCustomer() {
    const customer = this.order.customer;
    if (!this.order.customer_id) {
      return;
    }
    if (customer && customer.deleted) {
      return;
    }
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`s/${slug}/customers?code=${this.order.customer_id}&type=customer`);
  }
}
