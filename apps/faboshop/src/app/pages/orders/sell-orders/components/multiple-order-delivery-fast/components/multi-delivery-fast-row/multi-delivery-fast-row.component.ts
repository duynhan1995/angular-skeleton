import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Order } from '@etop/models';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { SelectAndUpdateCustomerAddressComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/select-and-update-customer-address/select-and-update-customer-address.component';
import { CustomerAddressModalComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { AddInfoFulfillmentModalComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/components/add-info-fulfillment-modal/add-info-fulfillment-modal.component';
import { UtilService } from 'apps/core/src/services/util.service';
import { MoDeliveryFastStoreService } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/mo-delivery-fast.store.service';
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'shop-multi-delivery-fast-row',
  templateUrl: './multi-delivery-fast-row.component.html',
  styleUrls: ['./multi-delivery-fast-row.component.scss']
})
export class MultiDeliveryFastRowComponent extends BaseComponent implements OnInit, OnChanges {
  @ViewChild('cod_amount_input', {static: false}) cod_amount_input: MaterialInputComponent;

  @Input() order = new Order({});
  @Input() isFirstItem = false;

  @Output() selectShippingService = new EventEmitter();

  format_number = {
    cod_amount: true,
    chargeable_weight: true
  };
  errorCOD = false;

  processingData = false;

  constructor(
    private locationQuery: LocationQuery,
    private auth: AuthenticateStore,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private modalController: ModalController,
    private ordersController: SellOrdersControllerService,
    private moFastStore: MoDeliveryFastStoreService,
    private locationCompact: LocationCompactPipe,
  ) {
    super();
  }

  get customerHasAddresses() {
    return this.order.customer.addresses && this.order.customer.addresses.length;
  }

  get addressDisplay() {
    const shipping = this.order.p_data.shipping;
    if (!shipping.shipping_address) {
      return '';
    }
    const shipping_address = shipping.shipping_address;
    const address1 = this.locationCompact.transform(shipping_address.address1);
    const ward = this.locationCompact.transform(shipping_address.ward);
    const district = this.locationCompact.transform(shipping_address.district);
    const province = this.locationCompact.transform(shipping_address.province);

    return `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
  }

  private setupShippingAddress() {
    if (this.order.shipping_address) {
      this.order.p_data.shipping.shipping_address = this.order.shipping.shipping_address;
    } else if (this.customerHasAddresses) {
      const found_address: any = this.order.customer.addresses[0];
      this.order.p_data.shipping.shipping_address = found_address || null;
    }
    const { pickup_address, include_insurance } = this.ordersController.shipping_data;
    this.order.p_data.shipping = {
      ...this.order.p_data.shipping,
      pickup_address,
      include_insurance
    };
    this.selectShippingService.emit(this.order);
  }

  private async prepareData() {
    this.processingData = true;
    await this.ordersController.listOrderCustomerAddresses([this.order]);
    this.format_number = {
      cod_amount: this.order.p_data.shipping.cod_amount >= 0,
      chargeable_weight: this.order.p_data.shipping.chargeable_weight >= 0
    };
    this.setupShippingAddress();
    this.processingData = false;
  }

  ngOnInit() {
    this.ordersController.onChangeUniData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.setupShippingAddress();
      });
    this.ordersController.onUpdateShippingData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        const { pickup_address, include_insurance } = this.ordersController.shipping_data;
        this.order.p_data.shipping = {
          ...this.order.p_data.shipping,
          pickup_address,
          include_insurance
        };
        this.moFastStore.updateSingleOrderItem(this.order);
        this.setupShippingAddress();
      });
  }

  async ngOnChanges(changes: SimpleChanges) {
    this.prepareData();
  }

  displayMap() {
    return option => option && option.display_name || null;
  }

  editMoreInfo() {
    let orderClone = this.util.deepClone(this.order);
    const modal = this.modalController.create({
      component: AddInfoFulfillmentModalComponent,
      showBackdrop: 'static',
      cssClass: 'modal-md',
      componentProps: {
        order: orderClone
      }
    });
    modal.show().then();
    modal.onDismiss().then(async(order) => {
      this.order = order;
      this.moFastStore.updateSingleOrderItem(this.order);
      this.setupShippingAddress();
    });
  }

  formatNumber(number_type: string) {
    const _num = this.order.p_data.shipping[number_type];
    if (!_num && _num != 0) {
      this.format_number[number_type] = false;
      return;
    }
    this.format_number[number_type] = !this.format_number[number_type];
    this.changeDetector.detectChanges();
    if (!this.format_number[number_type] && this[number_type + '_input']) {
      this[number_type + '_input'].focusInput();
    }
  }

  onChangeCOD() {
    const cod = this.order.p_data.shipping.cod_amount;
    this.errorCOD = cod > 0 && cod < 5000;
    if (this.errorCOD) {
      toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
    }
    this.selectShippingService.emit(this.order);
  }

  onChangeShippingService(order: Order) {
    order.p_data.selected_service = order.p_data.services.find(s =>
      s.external_id == order.p_data.selected_service_id
    );
  }

  noAddress(order: Order): boolean {
    return this.ordersController.noAddress(order);
  }

  noWeight(order: Order): boolean {
    return this.ordersController.noWeight(order);
  }

  createNewAddress(order) {
    const componentProps = {
      customer_id: order.customer.id,
      title: 'create',
      confirmBtnClass: 'btn-topship'
    };
    this.openModalAddress(order, componentProps);
  }

  editShippingAddress(order) {
    const shipping_address = order.p_data.shipping.shipping_address;

    const componentProps = this.customerHasAddresses && {
      customer: {...order.customer},
      addresses: order.customer.addresses,
      current_address: shipping_address
    } || {
      customer_id: null,
      title: 'edit',
      confirmBtnClass: 'btn-topship',
      address: shipping_address
    };
    this.openModalAddress(order, componentProps);
  }

  openModalAddress(order, componentProps) {
    const component = this.customerHasAddresses &&
      SelectAndUpdateCustomerAddressComponent ||
      CustomerAddressModalComponent;

    const cssClass = this.customerHasAddresses &&
      'modal-lg' || 'modal-md';

    const modal = this.modalController.create({
      component,
      showBackdrop: 'static',
      cssClass,
      componentProps
    });
    modal.show().then();
    modal.onDismiss().then(address => {
      if (!address) { return; }
      address = {
        ...address,
        province: this.locationQuery.getProvince(address.province_code)?.name,
        district: this.locationQuery.getDistrict(address.district_code)?.name,
        ward: address.ward_code && this.locationQuery.getWard(address.ward_code)?.name
      };
      if (this.order.customer_id) {
        if (this.customerHasAddresses) {
          this.order.customer.addresses.unshift(address);
        } else {
          this.order.customer.addresses = [address];
        }
      }
      order.p_data.shipping.shipping_address = address;
      this.selectShippingService.emit(this.order);
    });
  }

}
