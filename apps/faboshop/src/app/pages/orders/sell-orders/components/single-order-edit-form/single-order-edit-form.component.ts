import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Order } from 'libs/models/Order';
import { Fulfillment } from 'libs/models/Fulfillment';
import { DeliveryComponent } from 'apps/faboshop/src/app/components/delivery/delivery.component';
import { SingleOrderEditFormControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form-controller.service';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { AddressService } from 'apps/core/src/services/address.service';
import { CustomerApi, FulfillmentApi, OrderApi } from '@etop/api';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { takeUntil } from 'rxjs/operators';
import { Customer } from 'libs/models/Customer';

@Component({
  selector: 'shop-order-edit-form',
  templateUrl: './single-order-edit-form.component.html',
  styleUrls: ['./single-order-edit-form.component.scss']
})
export class SingleOrderEditFormComponent extends BaseComponent implements OnInit, OnChanges {
  @ViewChild('delivery', {static: false}) delivery: DeliveryComponent;
  @Input() order = new Order({});

  fulfillment: Fulfillment;
  loading = false;

  constructor(
    private auth: AuthenticateStore,
    private orderEditFormController: SingleOrderEditFormControllerService,
    private ffmService: FulfillmentService,
    private orderService: OrderService,
    private customerService: CustomerService,
    private addressService: AddressService,
    private ffmApi: FulfillmentApi,
    private orderApi: OrderApi,
    private customerApi: CustomerApi,
    private ffmStore: FulfillmentStore,
    private orderStore: OrderStoreService,
    private customerStore: CustomerStoreService
  ) {
    super();
  }

  get to_create_ffm() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  ngOnInit() {
    this.ffmStore.fufillmentValueChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(fulfillment => {
        this.fulfillment = fulfillment;
      });
  }

  async ngOnChanges(changes: SimpleChanges) {
    const last_order = changes.order.previousValue;
    const order = changes.order.currentValue;
    if (!last_order || last_order.id != order.id) {
      this.customerStore.resetSelectedCustomer();
      this.customerStore.resetSelectedCustomerAddress();
    }

    this.orderStore.selectOrder(order);
    if (order.customer_id) {
      const customer: Customer = await this.customerApi.getCustomer(order.customer_id);
      customer.addresses = await this.customerApi.getCustomerAddresses(order.customer_id);

      this.customerStore.selectCustomer(customer);
    }
    if (order.total_amount && order.total_amount != 0) {
      this.orderStore.updateTotalAmount(order.total_amount);
    }
    this.ffmStore.resetState();
    this.ffmStore.createCustomerByShippingAddress(false);
  }

  get isCreatingFFM() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  resetData() {
    this.ffmStore.resetState();
    this.ffmService.resetData();
  }

  async confirmOrder() {
    this.loading = true;
    try {
      await this.orderApi.confirmOrder(this.order.id, "confirm");
      await this.orderEditFormController.updateOrderData();
      this.orderService.createOrderSuccess$.next();
      toastr.success("Tạo hoá đơn thành công!");
    } catch(e) {
      this.orderService.createOrderFailed$.next();
      debug.error('ERROR in confirming order', e);
      toastr.error(`Tạo hoá đơn không thành công.`, e.code && (e.message || e.msg) || '');
    }
    this.loading = false;
  }

  async createFFM(needConfirm: boolean) {
    this.loading = true;
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;
      const { cod_amount, shipping_address, shipping_service_code } = fulfillment;
      if (!shipping_address) {
        return this.handleErrorManually("Thiếu thông tin địa chỉ giao hàng!");
      }
      if (!cod_amount && cod_amount != 0) {
        this.ffmStore.errorCOD(true);
        return this.handleErrorManually("Vui lòng nhập tiền Thu hộ (COD)!");
      }
      if (cod_amount < 5000 && cod_amount > 0) {
        return this.handleErrorManually("Vui lòng nhập tiền thu hộ bằng 0đ hoặc từ 5.000đ trở lên!");
      }
      if (!shipping_service_code) {
        return this.handleErrorManually("Vui lòng chọn gói vận chuyển!");
      }

      if (needConfirm) {
        await this.orderApi.confirmOrder(this.order.id, "confirm");
      }
      if (this.ffmService.shipping_type == 'shipment') {
        await this.createShipmentFFM(this.order.id, this.order.confirm_status);
      }
      if (this.ffmService.shipping_type == 'shipnow') {
        await this.createShipnowFFM(this.order.id, this.order.confirm_status);
      }
      await this.orderEditFormController.updateOrderData();
      this.orderService.createOrderSuccess$.next();
    } catch (e) {
      this.orderService.createOrderFailed$.next();
      debug.error('ERROR in creating FFM', e);
      toastr.error(`Tạo hoá đơn và giao hàng không thành công.`, e.code && (e.message || e.msg) || '');
    }
    this.loading = false;
  }

  async createShipmentFFM(order_id: string, confirm_status: string) {
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;

      const body: any = {
        order_id,
        ...fulfillment,
        shipping_type: 'shipment',
        chargeable_weight: this.ffmService.weight_calc_type == 1
          && fulfillment.chargeable_weight || null,
        length: this.ffmService.weight_calc_type == 2
          && fulfillment.length || null,
        width: this.ffmService.weight_calc_type == 2
          && fulfillment.width || null,
        height: this.ffmService.weight_calc_type == 2
          && fulfillment.height || null
      };

      const res = await this.ffmApi.createShipmentFulfillment(body);
      toastr.success(`${confirm_status != 'P' && 'Tạo hoá đơn và ' || 'Tạo đơn '}giao hàng thành công!`);
    } catch (e) {
      throw e;
    }
  }

  async createShipnowFFM(order_id: string, confirm_status: string) {
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;

      const {
        shipping_address,
        pickup_address,
        cod_amount,
        shipping_note,
        shipping_service_code,
        shipping_service_fee,
        carrier
      } = fulfillment;

      const body: any = {
        delivery_points: [{
          order_id,
          chargeable_weight: 1000,
          cod_amount,
          shipping_address
        }],
        pickup_address,
        shipping_note,
        shipping_service_code,
        shipping_service_fee,
        carrier
      };

      const shipnow_ffm = await this.ffmService.createShipnowFulfillment(body);
      await this.confirmShipnowFFM(shipnow_ffm.id);
      toastr.success(`${confirm_status != 'P' && 'Tạo hoá đơn và ' || 'Tạo đơn '}giao tức thì thành công!`);
    } catch(e) {
      throw e;
    }
  }

  async confirmShipnowFFM(shipnow_id: string) {
    try {
      const message_data = await this.ffmService.confirmShipnowFulfillment(shipnow_id);
    } catch (e) {
      this.ffmService.cancelShipnowFulfillment(shipnow_id, 'Xác nhận đơn thất bại');
      debug.error("ERROR in confirmShipnowFFM", e);
      throw e;
    }
  }

  private handleErrorManually(error_message: string) {
    toastr.error(error_message);
    this.loading = false;
  }

  toggleSwitch() {
    this.ffmStore.createFulfillment();
  }

}
