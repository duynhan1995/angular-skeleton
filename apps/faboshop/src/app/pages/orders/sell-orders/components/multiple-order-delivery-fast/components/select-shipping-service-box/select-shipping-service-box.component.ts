import { Component, OnInit } from '@angular/core';
import {
  MoDeliveryFastStoreService, ShipServiceCarrier,
  ShipServiceSelectMethod, ShipServiceShippingType
} from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/mo-delivery-fast.store.service';

@Component({
  selector: 'shop-select-shipping-service-box',
  templateUrl: './select-shipping-service-box.component.html',
  styleUrls: ['./select-shipping-service-box.component.scss']
})
export class SelectShippingServiceBoxComponent implements OnInit {
  ship_service_options = [
    {
      title: 'Tự động chọn giá tốt nhất',
      select_method: ShipServiceSelectMethod.bpf
    },
    {
      title: 'Tuỳ chọn gói dịch vụ',
      select_method: ShipServiceSelectMethod.custom
    }
  ];

  shipping_types = [
    {
      title: 'NHANH',
      shipping_type: ShipServiceShippingType.fast
    },
    {
      title: 'CHUẨN',
      shipping_type: ShipServiceShippingType.standard
    }
  ];

  shipping_providers = [
    {
      title: 'GHN',
      shipping_provider: ShipServiceCarrier.ghn
    },
    {
      title: 'GHTK',
      shipping_provider: ShipServiceCarrier.ghtk
    },
    {
      title: 'VTPOST',
      shipping_provider: ShipServiceCarrier.vtpost
    }
  ];

  constructor(
    private moFastStore: MoDeliveryFastStoreService
  ) { }

  get shipServiceOption() {
    return this.moFastStore.snapshot.ship_service_option;
  }

  ngOnInit() {}

  onChangeSelectMethod(method: ShipServiceSelectMethod) {
    this.moFastStore.updateShipServiceSelectMethod(method);
  }

  onChooseShippingType(type: ShipServiceShippingType) {
    this.moFastStore.updateShipServiceShippingType(type);
  }

  onChooseShippingProvider(carrier: ShipServiceCarrier) {
    this.moFastStore.updateShipServiceCarrier(carrier);
  }

}
