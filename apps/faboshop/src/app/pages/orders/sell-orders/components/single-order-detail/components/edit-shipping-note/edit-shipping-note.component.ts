import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FulfillmentApi } from '@etop/api/shop';
import { ShippingService } from 'apps/faboshop/src/services/shipping.service';
import { OrdersService } from '@etop/state/order';
import {TRY_ON} from "@etop/models";

export interface DialogData {
  fulfillment: any;
  new_value: any;
}

enum Step {
  EDIT = 'edit',
  CONFIRM = 'confirm',
}

@Component({
  selector: 'faboshop-edit-shipping-note',
  templateUrl: './edit-shipping-note.component.html',
  styleUrls: ['./edit-shipping-note.component.scss']
})
export class EditShippingNoteComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();
  _step = Step.EDIT;
  step = Step;
  shipping_fee;
  loadingContinue = false;
  loadingConfirm = false;
  valueMap = option => (option && option.code) || null;
  // tslint:disable-next-line: member-ordering
  tryOnOptions = [
    {
      code: 'try',
      value: 'try',
      name: 'Cho thử hàng'
    },
    {
      code: 'open',
      value: 'open',
      name: 'Cho xem hàng không thử'
    },
    {
      code: 'none',
      value: 'none',
      name: 'Không cho xem hàng'
    }
  ];
  // tslint:disable-next-line: member-ordering
  noteForm = this.fb.group({
    try_on: '',
    shipping_note: ''
  });

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditShippingNoteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private orderService: OrdersService,
    private shippingService: ShippingService
  ) { }

  ngOnInit(): void {
    const { try_on, shipping_note } = this.data.fulfillment;
    this.noteForm.setValue({
      try_on,
      shipping_note
    });
  }

  currentView(step: Step) {
    return this._step == step;
  }

  async submit() {
    try {
      this.loadingConfirm = true;
      const note = {
        shipping_note: this.noteForm.controls['shipping_note'].value,
        try_on: this.noteForm.controls['try_on'].value
      }
      await this.orderService.updateShippingNote(this.data.fulfillment,note)
      this.loadingConfirm = false;
      this.submitClicked.emit(true);
      this.dialogRef.close();
    } catch (e) {
      this.loadingConfirm = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }

  }

  async confirmStep() {
    const _checkDifferent = this.checkDifferent(this.data.fulfillment, this.noteForm.value);
    if (_checkDifferent) {
      let data: any = this.shippingService.parseFulfillmentData(
        this.data.fulfillment
      );
      const { shipping_note, try_on } = this.noteForm.value;
      data = Object.assign({}, data, {
        shipping_note,
        try_on,
        connection_ids: [this.data.fulfillment.connection_id]
      });
      this._step = Step.CONFIRM;
      try {
        this.loadingContinue = true;
        const services = await this.shippingService.getShippingServices(data);
        const service = this.shippingService.shippingServiceMap(
          services.services,
          this.data.fulfillment
        );
        this.shipping_fee = service[0]?.fee;
        this.loadingContinue = false;
      } catch (e) {
        this.shipping_fee = false;
        this.loadingContinue = false;
        toastr.error(e.message || e.msg);
        debug.log('ERROR in getShippingServices', e);
      }
    } else {
      return toastr.error('Vui lòng thay đổi thông tin để tiếp tục');
    }

  }

  back() {
    this._step = Step.EDIT;
  }

  checkDifferent(oldData, newData) {
    return (
      oldData.shipping_note != newData.shipping_note ||
      oldData.try_on != newData.try_on
    );
  }

  getTryOn(try_on) {
    return TRY_ON[try_on];
  }
}
