import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderFulfillmentsComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/order-fulfillments/order-fulfillments.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';

@NgModule({
  declarations: [
    OrderFulfillmentsComponent
  ],
  exports: [
    OrderFulfillmentsComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule
  ]
})
export class OrderFulfillmentsModule { }
