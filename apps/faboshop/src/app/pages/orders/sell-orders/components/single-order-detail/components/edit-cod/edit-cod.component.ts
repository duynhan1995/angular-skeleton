import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FulfillmentApi } from '@etop/api';
import { ShippingService } from 'apps/faboshop/src/services/shipping.service';
import { OrdersService } from '@etop/state/order';

export interface DialogData {
  fulfillment: any;
  new_value: any;
}

enum Step {
  EDIT = 'edit',
  CONFIRM = 'confirm',
}
@Component({
  selector: 'faboshop-edit-cod',
  templateUrl: './edit-cod.component.html',
  styleUrls: ['./edit-cod.component.scss']
})
export class EditCodComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();
  _step = Step.EDIT;
  step = Step;
  shipping_fee;
  loadingContinue = false;
  loadingConfirm = false;
  formCod = this.fb.group({
    cod_amount: 0
  });
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditCodComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private orderService: OrdersService,
    private shippingService: ShippingService
  ) {}

  ngOnInit(): void {
    this.formCod.setValue({
      cod_amount: this.data.fulfillment.cod_amount
    });
  }

  currentView(step: Step) {
    return this._step == step;
  }

  async submit() {
    try {
      this.loadingConfirm = true;
      await this.orderService.updateCOD(this.data.fulfillment.id, Number(this.formCod.controls['cod_amount'].value));
      this.loadingConfirm = false;
      this.submitClicked.emit(true);
      this.dialogRef.close();
    } catch (e) {
      this.loadingConfirm = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }

  async confirmStep() {
    if (
      this.formCod.controls['cod_amount'].value ==
      this.data.fulfillment.cod_amount
    ) {
      return toastr.error('Vui lòng thay đổi giá trị thu hộ');
    }
    let data: any = this.shippingService.parseFulfillmentData(
      this.data.fulfillment
    );
    const cod = this.formCod.controls['cod_amount'].value;
    if (cod > 0 && cod < 5000){
      toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5000đ!')
      this.loadingConfirm = false;
      return
    }
    data = Object.assign({}, data, {
      cod_amount: cod,
      connection_ids: [this.data.fulfillment.connection_id]
    });
    this._step = Step.CONFIRM;
    try {
      this.loadingContinue = true;
      const services = await this.shippingService.getShippingServices(data);
      const service = this.shippingService.shippingServiceMap(
        services.services,
        this.data.fulfillment
      );
      this.shipping_fee = service[0]?.fee;
      this.loadingContinue = false;
    } catch (e) {
      this.shipping_fee = false;
      this.loadingContinue = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in getShippingServices', e);
    }
  }

  back() {
    this._step = Step.EDIT;
  }

  formatNumber() {
    const _value = this.formCod.controls['cod_amount'].value?.toString();
    if (!_value || !_value.length || Number(_value) < 0) {
      this.formCod.setValue({
        cod_amount: 0
      });
      return;
    }
  }
}
