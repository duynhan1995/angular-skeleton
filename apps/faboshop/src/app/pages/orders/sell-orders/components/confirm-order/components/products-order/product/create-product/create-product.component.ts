import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Product, ShopProduct, Variant } from '@etop/models/Product';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ToastController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'fabo-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})

export class CreateProductComponent extends BaseComponent implements OnInit {
  @Output() createProductEvent = new EventEmitter<Variant>()
  @Input() p_product_name;
  product_name = '';
  product_price :Number;
  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private toastController: ToastController,
    private authStore: AuthenticateStore,
    private util: UtilService,
    private modalDismiss: ModalAction,
  ) {
    super();
  }
  ngOnInit() {
    this.product_name = this.p_product_name ? this.p_product_name : '';
  }


  dismissModal() {
    this.modalDismiss.dismiss(null);
  }

  async createProduct() {
    if (!this.product_name) {
      return toastr.error('Tạo sản phẩm không thành công', 'Chưa nhập tên sản phẩm!');
    }
    if (!this.product_price){
      return toastr.error('Tạo sản phẩm không thành công', 'Chưa nhập giá bán sản phẩm!');
    }
    if (this.product_name.length < 2) {
      return toastr.error('Tạo sản phẩm không thành công', 'Tên sản phẩm phải có ít nhất 2 kí tự không tính dấu cách');
    }
    if (this.product_price < 0) {
      return toastr.error('Tạo sản phẩm không thành công', 'Giá sản phẩm không được âm!');
    }
    try {
      const body = new Product({
        name:this.product_name,
        list_price: Number(this.product_price),
        retail_price: Number(this.product_price),
      });

      let product = await this.confirmOrderService.createProduct(body);
      await this.createVariants(product);
      product = await this.confirmOrderService.getProduct(product.id);
      if (product.id != ''){
        toastr.success('Tạo sản phẩm thành công!');
      }
    } catch (e) {
      debug.error('ERROR in creating product', e);
      const msg =
        (e.code == 'failed_precondition' && e.message) ||
        'Tạo sản phẩm thất bại';
      toastr.error(msg);
    }
  }
  async createVariants(product: Product | ShopProduct) {
    try {
      const body: any = {
        product_id: product.id,
        name: product.name,
        product_name:product.name,
        retail_price:product.retail_price,
        list_price:product.list_price,
        code: product.code
      };
      const res = await this.confirmOrderService.createVariant(body);
      const any = await this.confirmOrderService.getProduct(res.product_id)
      this.modalDismiss.dismiss(any.variants[0]);
    } catch (e) {
      debug.error('ERROR in creating variants', e);
      throw e;
    }
  }
  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }
}
