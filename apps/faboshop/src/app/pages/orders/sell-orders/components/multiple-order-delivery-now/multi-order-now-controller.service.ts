import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MultiOrderNowController {
  onRenderingMap$ = new Subject();
  onCreatingFFM$ = new Subject();
  onSuccessCreatingFFM$ = new Subject();
  onFailedCreatingFFM$ = new Subject();

  constructor() { }
}
