import { Injectable } from '@angular/core';
import { Order } from '@etop/models';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { UtilService } from 'apps/core/src/services/util.service';

export enum ShipServiceSelectMethod {
  bpf = 'best_price_first',
  custom = 'custom'
}

export enum ShipServiceShippingType {
  fast = 'fast',
  standard = 'standard'
}

export enum ShipServiceCarrier {
  ghn = 'ghn',
  ghtk = 'ghtk',
  vtpost = 'vtpost'
}

export interface ShipServiceOption {
  select_method?: ShipServiceSelectMethod;
  shipping_type?: ShipServiceShippingType;
  shipping_provider?: ShipServiceCarrier;
}

export interface MoDeliveryFastData {
  selected_orders: Order[];
  ship_service_option: ShipServiceOption;
}

@Injectable()
export class MoDeliveryFastStoreService extends AStore<MoDeliveryFastData> {
  initState = {
    selected_orders: [],
    ship_service_option: {
      select_method: ShipServiceSelectMethod.bpf,
      shipping_type: ShipServiceShippingType.fast,
      shipping_provider: ShipServiceCarrier.ghn
    }
  };

  readonly selectedOrdersUpdated$ = this.state$.pipe(
    map(({selected_orders}) => selected_orders),
    distinctUntilChanged((prev, curr) => {
      return JSON.stringify(prev) == JSON.stringify(curr);
    })
  );

  readonly shipServiceOptionUpdated$ = this.state$.pipe(
    map(({ship_service_option}) => ship_service_option),
    distinctUntilChanged((prev, curr) => {
      return JSON.stringify(prev) == JSON.stringify(curr);
    })
  );

  constructor(
    private util: UtilService
  ) {
    super();
    this.state = this.initState;
  }

  updateSingleOrderItem(order: Order) {
    const selected_orders = this.snapshot.selected_orders;
    const orderIndex = selected_orders.findIndex(o => o.id == order.id);
    if (orderIndex != -1) {
      selected_orders[orderIndex] = order;
    }
    this.updateSelectedOrders(selected_orders);
  }

  updateSelectedOrders(orders: Order[]) {
    this.setState({ selected_orders: orders });
  }

  updateShipServiceSelectMethod(select_method: ShipServiceSelectMethod) {
    const ship_service_option = this.util.deepClone(this.snapshot.ship_service_option);
    ship_service_option.select_method = select_method;
    this.updateShipServiceOption(ship_service_option);
  }

  updateShipServiceShippingType(shipping_type: ShipServiceShippingType) {
    const ship_service_option = this.util.deepClone(this.snapshot.ship_service_option);
    ship_service_option.shipping_type = shipping_type;
    this.updateShipServiceOption(ship_service_option);
  }

  updateShipServiceCarrier(carrier: ShipServiceCarrier) {
    const ship_service_option = this.util.deepClone(this.snapshot.ship_service_option);
    ship_service_option.shipping_provider = carrier;
    this.updateShipServiceOption(ship_service_option);
  }

  updateShipServiceOption(ship_service_option: ShipServiceOption) {
    this.setState({ ship_service_option });
  }

}
