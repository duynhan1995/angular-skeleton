import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { debounceTime, distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { BaseComponent } from '@etop/core';
import { AddressAPI } from '@etop/api';
import { ConversationsQuery } from '@etop/state/fabo/conversation';
import { Address, CustomerAddress } from '@etop/models';
import { ListAddressesPopupComponent } from '../../../../../../../components/modals/list-addresses-popup/list-addresses-popup.component';
import { CreateUpdateAddressPopupComponent } from '../../../../../../../components/modals/create-update-address-popup/create-update-address-popup.component';
import { CustomerService } from '../../../../../../../../services/customer.service';
import { ModalController } from '../../../../../../../../../../core/src/components/modal-controller/modal-controller.service';
import { AddressDisplayPipe } from '@etop/shared';
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";
import { CustomerReturnRate, FbCustomerReturnRate } from '@etop/models/faboshop/FbCustomerReturnRate';
import { ConnectionStore } from '@etop/features';
import { MatDialog } from '@angular/material/dialog';
import { RiskWarningCustomerComponent } from '../../../../../../messages/components/confirm-order/components/customer-info-order/risk-warning-customer/risk-warning-customer.component';
import GetCustomerAddressesRequest = AddressAPI.GetCustomerAddressesRequest;

@Component({
  selector: 'faboshop-customer-info-order',
  templateUrl: './customer-info-order.component.html',
  styleUrls: ['./customer-info-order.component.scss']
})
export class CustomerInfoOrderComponent extends BaseComponent implements OnInit {
  dropDown = false;
  fulfillment$ = this.confirmOrderStore.state$.pipe(map(s => s?.fulfillment));
  resetdata$ = this.confirmOrderStore.state$.pipe(map(s => s?.resetData), distinctUntilChanged());

  provinces;
  districts;
  wards;
  address1;
  customerForm = this.fb.group({
    full_name: '',
    phone: '',
    province_code: '',
    address1: '',
    district_code: '',
    ward_code: ''
  });
  customerInitializing = true;
  customerAddresses: CustomerAddress[] = [];
  customerAddresses$ = this.confirmOrderStore.state$.pipe(
    map(s => s.customerAddress), distinctUntilChanged());
  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0])
  customerReturnRating: CustomerReturnRate;
  customerReturnError = false;
  errorMessage = '';

  @HostListener('document:click', ['$event'])
  handleKeyDown(event) {
    if (event.target.id != 'dropdowm-address') {
      this.hideDropDown();
    }
  }

  valueMap = option => option && option.code || null;
  displayMap = option => option && option.name || null;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private modalController: ModalController,
    private confirmOrderService: ConfirmOrderService,
    private addressDisplay: AddressDisplayPipe,
    private connectionStore: ConnectionStore,
    public dialog: MatDialog,
  private conversationsQuery: ConversationsQuery,
    private confirmOrderStore: ConfirmOrderStore,
    private locationQuery: LocationQuery,
    private locationService: LocationService
  ) {
    super();
  }

  ngOnInit() {
    this.customerForm.valueChanges.subscribe(customer => {
      const order = this.confirmOrderStore.snapshot.order;
      this.confirmOrderService.updateOrder({
        ...order,
        customer
      });
    });
    this.customerForm.controls['province_code'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      this.provinceSelected();
      this.districtSelected();
    });
    this.customerForm.controls['district_code'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      this.districtSelected();
    });
    this.customerForm.controls['ward_code'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      this.wardSelected();
    });
    this.customerForm.controls['address1'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(address1 => {
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      const { shipping_address } = ffm;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: {
          ...shipping_address,
          address1:address1,
        },
      });
    });
    this.customerForm.controls['phone'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(phone => {
      this.customerReturnRating = null;
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      const { shipping_address } = ffm;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: {
          ...shipping_address,
          phone: phone
        }
      });
    });
    this.customerForm.controls['full_name'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(full_name => {
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      const { shipping_address } = ffm;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        shipping_address: {
          ...shipping_address,
          full_name: full_name
        }
      });
    });

    this.customerInitializing = false;
    this.getProvinces();
    this.activeFbUser$.subscribe(fbUser => {
      if (!fbUser) {
        this.resetForm();
      }
    });
    this.resetdata$.subscribe(reset => {
      if (reset) {
        this.resetForm();
      }
    });
    this.confirmOrderStore.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.hideDropDown();
      });
    this.checkCustomer();
  }

  getProvinces() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.provinces.forEach(province => {
      province.value = province.code;
    });
  }

  activeCustomerAddress(customerAddress: CustomerAddress) {
    this.customerForm.patchValue({
      full_name: customerAddress.full_name,
      phone: customerAddress.phone
    }, { emitEvent: false });
    this.getCustomerAddress(customerAddress);
    this.getCustomerReturnRate();
    this.confirmOrderService.activeCustomerAddress(customerAddress);
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      shipping_address: {
        ...ffm.shipping_address,
        ...customerAddress
      }
    });
    this.hideDropDown();
  }

  showDropDown() {
    this.dropDown = true;
  }

  hideDropDown() {
    this.dropDown = false;
  }

  getCustomerAddress(address, emitEvent = true) {
    this.customerForm.patchValue({
      address1: address?.address1,
      province_code: address?.province_code
    }, { emitEvent });
    this.provinceSelected();
    this.customerForm.patchValue({
      district_code: address?.district_code
    }, { emitEvent });
    this.districtSelected();
    this.customerForm.patchValue({
      ward_code: address?.ward_code
    }, { emitEvent });
  }

  async checkCustomer() {
    this.customerForm.get('phone').valueChanges.pipe(debounceTime(500), takeUntil(this.destroy$))
      .subscribe(async (value: string) => {
        if (value == '' || !value) {
          this.hideDropDown();
          return;
        }
        const filter = {
          phone: value
        };
        let query: GetCustomerAddressesRequest = {
          filter: filter
        };
        this.customerAddresses = await this.confirmOrderService.getCustomerAddress(query);
        if (!this.customerAddresses.length) {
          this.hideDropDown();
        } else {
          this.showDropDown();
        }
      });
  }

  resetData() {
    const _order = this.confirmOrderStore.snapshot.order;
    this.confirmOrderService.updateOrder({
      ..._order,
      id: null
    }, true);
    this.confirmOrderService.updateFulfillment(this.confirmOrderStore.initState.fulfillment);
    this.customerForm.patchValue({
      phone: '',
      full_name: '',
      address1: '',
      ward_code: '',
      province_code: '',
      district_code: ''
    }, { emitEvent: false });
  }

  provinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(this.customerForm.get('province_code').value);
    this.districts.forEach(district => {
      district.value = district.code;
    });
    this.customerForm.patchValue({
      district_code: ''
    });
  }

  districtSelected() {
    this.wards = this.locationService.filterWardsByDistrict(this.customerForm.get('district_code').value);
    this.wards.forEach(ward => {
      ward.value = ward.code;
    });
    this.customerForm.patchValue({
      ward_code: ''
    });
  }

  wardSelected() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    const { shipping_address } = ffm;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      shipping_address: {
        ...shipping_address,
        address1: this.customerForm.controls['address1'].value,
        province_code: this.customerForm.controls['province_code'].value,
        district_code: this.customerForm.controls['district_code'].value,
        ward_code: this.customerForm.controls['ward_code'].value
      }
    });
  }

  resetForm() {
    this.customerForm.patchValue({
      full_name: '',
      phone: '',
      province_code: '',
      address1: '',
      district_code: '',
      ward_code: ''
    });
  }

  async changeAddress(type: 'from' | 'to') {
    const fromAddresses = this.confirmOrderStore.snapshot.fromAddresses;
    const toAddresses = this.confirmOrderStore.snapshot.toAddresses;
    //TODO: case when no customer.id & focus to input => directly open createAddressPopup
    const modal = this.modalController.create({
      component: ListAddressesPopupComponent,
      componentProps: {
        type,
        addresses: type == 'from' ? fromAddresses : toAddresses
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data?.createAddress) {
        this.createOrUpdateAddress(type, new Address({}));
      }
      if (data?.updateAddress) {
        this.createOrUpdateAddress(type, data?.updateAddress);
      }
      if (data?.address) {
        const _address = data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.confirmOrderService.selectFromAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.confirmOrderService.selectToAddress(_address);
        }
      }

    });
    if ((toAddresses.length && type == 'to') || (fromAddresses.length && type == 'from')) {
      modal.show().then();
    } else {
      this.createOrUpdateAddress(type, new Address({}));
    }
  }

  async createOrUpdateAddress(type: 'from' | 'to', address: Address) {
    const modal = this.modalController.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data?.closed) {
        this.changeAddress(type);
      }
      if (data?.address) {
        const _address = data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.createOrUpdateShopAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.createOrUpdateCustomerAddress(_address);
        }
      }
    });
    modal.show().then();
  }

  createCustomerAddress(customer, address) {
    const { province_code, district_code, ward_code, address1 } = address;
    const customerAddress: any = {
      customer_id: customer.id,
      province_code: province_code,
      district_code: district_code,
      ward_code: ward_code,
      address1: address1,
      email: customer.email,
      phone: customer.phone,
      full_name: customer.full_name
    };
    if (province_code && district_code && ward_code && address1) {
      this.customerService.createCustomerAddress(customerAddress);
    }
  }

  async createOrUpdateShopAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateFromAddress(address);
    } else {
      await this.confirmOrderService.createFromAddress(address);
    }
  }

  async createOrUpdateCustomerAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateToAddress(address);
    } else {
      await this.confirmOrderService.createToAddress(address);
    }
  }

  get pickupAddressDisplay() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    return this.addressDisplay.transform(ffm.pickup_address);
  }

  async getCustomerReturnRate(){
    const phone = this.customerForm.controls['phone'].value;
    if (phone?.length > 9) {
      let customerReturnRate;
      this.customerReturnError = false;
      try {
        customerReturnRate = await this.confirmOrderService.customerReturnRate(phone);
      }catch (e) {
        this.errorMessage = e.message
        this.customerReturnError = true;
      }
      if (customerReturnRate){
        customerReturnRate.customer_return_rates.sort(function(a,b) {
          return b.customer_return_rate.rate - a.customer_return_rate.rate;
        })
        this.customerReturnRating = customerReturnRate.customer_return_rates[0]?.customer_return_rate;
      }
    }
  }

  hideWaring() {
    const phone = this.customerForm.controls['phone'].value;
    this.dialog.open(RiskWarningCustomerComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: {customerReturnRate: this.customerReturnRating, phone:phone,
        customerReturnError: this.customerReturnError, errorMessage: this.errorMessage}
    });
  }

}
