import { Component, Input, OnInit } from '@angular/core';
import { Order, OrderLine } from 'libs/models/Order';

@Component({
  selector: 'shop-line-info-edit',
  templateUrl: './line-info-edit.component.html',
  styleUrls: ['./line-info-edit.component.scss']
})
export class LineInfoEditComponent implements OnInit {
  @Input() order = new Order({});
  loading = false;
  to_edit = {
    order_discount: false,
    fee_shipping: false,
    fee_tax: false,
    fee_other: false
  };

  constructor(
  ) { }

  ngOnInit() {
  }

  toEdit(type) {
    for (let key in this.to_edit) {
      if (key == type) { continue; }
      this.to_edit[key] = false;
    }
    this.to_edit[type] = !this.to_edit[type];
  }

  onCloseDropdown() {
    for (let key in this.to_edit) {
      this.to_edit[key] = false;
    }
  }

  displayAttributes(line: OrderLine) {
    return line.attributes.map(attr => attr.value).join(' - ');
  }

}
