import { Injectable } from '@angular/core';
import { Order } from 'libs/models/Order';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';

@Injectable()
export class SingleOrderEditFormControllerService {
  order = new Order({});

  constructor(private ordersController: SellOrdersControllerService) { }

  async updateOrderData() {
    await this.ordersController.reloadOrderList();
  }

}
