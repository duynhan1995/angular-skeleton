import { Component, Input, OnInit } from '@angular/core';
import { Order, OrderLine } from 'libs/models/Order';

@Component({
  selector: 'shop-line-info',
  templateUrl: './line-info.component.html',
  styleUrls: ['./line-info.component.scss']
})
export class LineInfoComponent implements OnInit {
  @Input() order = new Order({});

  constructor() { }

  ngOnInit() {
  }

  displayAttributes(line: OrderLine) {
    return line.attributes.map(attr => attr.value).join(' - ');
  }

}
