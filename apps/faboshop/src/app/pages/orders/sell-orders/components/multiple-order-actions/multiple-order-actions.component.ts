import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import {
  PrintType,
  SellOrdersControllerService
} from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { UserService } from 'apps/core/src/services/user.service';
import { ExportFulfillmentModalComponent } from 'apps/faboshop/src/app/components/modals/export-fulfillment-modal/export-fulfillment-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ExportOrderModalComponent } from 'apps/faboshop/src/app/components/modals/export-order-modal/export-order-modal.component';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: 'shop-multiple-order-actions',
  templateUrl: './multiple-order-actions.component.html',
  styleUrls: ['./multiple-order-actions.component.scss']
})
export class MultipleOrderActionsComponent implements OnInit, OnChanges {
  @Input() orders: Array<Order> = [];

  allNullActiveFfm = false;
  constructor(
    private ordersController: SellOrdersControllerService,
    private user: UserService,
    private modalController: ModalController,
    private ffmStore: FulfillmentStore
  ) {}

  ngOnInit() {

  }

  ngOnChanges() {
    this.allNullActiveFfm = this.orders.every(o => !o.activeFulfillment || !o.activeFulfillment.shipping_code);
  }

  preCheckOrdersCondition() {
    const _orders = this.orders.filter(
      o => this.ordersController.isSelectedOrder(o) && SellOrdersControllerService.canCreateFulfillment(o)
    );
    if (!_orders.length) {
      throw new Error('pre conditioned failed');
    }
    this.orders.forEach(o => {
      if (!this.ordersController.isSelectedOrder(o) || !SellOrdersControllerService.canCreateFulfillment(o)) {
        o.p_data.un_updatable = true;
      }
    });
  }

  deliveryFast() {
    try {
      this.ffmStore.createFulfillment();
      this.preCheckOrdersCondition();
      this.ordersController.deliveryFast = true;
    } catch (e) {
      if (e.message == 'pre conditioned failed') {
        toastr.warning(
          'Bạn không có đơn hàng phù hợp để tạo đơn giao nhanh!'
        );
      }
      debug.error('ERROR in open delivery fase', e);
    }
  }

  async deliveryNow() {
    try {
      this.ffmStore.createFulfillment();
      this.preCheckOrdersCondition();
      this.ordersController.deliveryNow = true;
    } catch (e) {
      if (e.message == 'pre conditioned failed') {
        toastr.warning(
          'Bạn không có đơn hàng phù hợp để tạo đơn giao tức thì!'
        );
      }
      debug.error('ERROR in open delivery now', e);
    }
  }



  printOrders() {
    this.ordersController.print(PrintType.orders, { orders: this.orders });
  }

  printFulfillments() {
    this.ordersController.print(PrintType.fulfillments, { orders: this.orders });
  }

  printOrderAndFfm() {
    this.ordersController.print(PrintType.o_and_f, { orders: this.orders });
  }

  exportOrder() {
    let modal = this.modalController.create({
      component: ExportOrderModalComponent,
      componentProps: {
        ids: this.orders.map(o => o.id)
      },
      showBackdrop: 'static'
    });
    modal.show();
  }

  exportFFM() {
    let modal = this.modalController.create({
      component: ExportFulfillmentModalComponent,
      componentProps: {
        ids: this.orders
          .filter(o => o.activeFulfillment)
          .map(o => o.activeFulfillment.id)
      },
      showBackdrop: 'static'
    });
    modal.show();
  }
}
