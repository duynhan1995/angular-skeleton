import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';

@Component({
  selector: '[shop-order-row]',
  templateUrl: './sell-order-row.component.html',
  styleUrls: ['./sell-order-row.component.scss']
})
export class SellOrderRowComponent implements OnInit {
  @Input() order = new Order({});
  @Input() liteMode = false;

  constructor(
    public util: UtilService,
    private router: Router,
    private customerService: CustomerService,
  ) { }

  get isNormalCustomer() {
    if (!this.customerService.independent_customer) {
      return true;
    }
    return !!this.order.customer_id
      && this.order.customer_id != this.customerService.independent_customer.id;
  }

  ngOnInit() {
  }

  viewDetailCustomer() {
    const customer = this.order.customer;
    if (!this.order.customer_id) { return; }
    if (customer && customer.deleted) { return; }
    const slug = this.util.getSlug();
    this.router.navigateByUrl(`s/${slug}/customers?code=${this.order.customer_id}&type=customer`);
  }

  viewDetailFfm() {
    const ffm_type = this.order.fulfillment_type;
    const ffm_code = this.order.activeFulfillment.shipping_code;
    this.router.navigateByUrl(`s/${this.util.getSlug()}/fulfillments?code=${ffm_code}&type=${ffm_type}`);
  }

}
