import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderReceiptsComponent } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/order-receipts/order-receipts.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';

@NgModule({
  declarations: [OrderReceiptsComponent],
  exports: [
    OrderReceiptsComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule
  ]
})
export class OrderReceiptsModule { }
