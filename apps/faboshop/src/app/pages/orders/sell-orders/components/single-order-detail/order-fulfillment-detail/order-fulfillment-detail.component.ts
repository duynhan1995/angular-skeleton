import { Component, OnInit, Input, OnChanges, SimpleChanges, TemplateRef } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import * as moment from 'moment';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { MatDialog } from '@angular/material/dialog';
import { EditShippingInfoComponent } from '../components/edit-shipping-info/edit-shipping-info.component';
import { EditInsuranceComponent } from '../components/edit-insurance/edit-insurance.component';
import { EditCodComponent } from '../components/edit-cod/edit-cod.component';
import { EditWeightComponent } from '../components/edit-weight/edit-weight.component';
import { EditShippingNoteComponent } from '../components/edit-shipping-note/edit-shipping-note.component';
import { EditPickupInfoComponent } from '../components/edit-pickup-info/edit-pickup-info.component';
import { SellOrdersControllerService } from '../../../sell-orders-controller.service';
import { OrderQuery } from '@etop/state/order';

const editComponent = {
  shipping: EditShippingInfoComponent,
  pickup: EditPickupInfoComponent,
  cod: EditCodComponent,
  weight: EditWeightComponent,
  shipping_note: EditShippingNoteComponent,
  insurance: EditInsuranceComponent
};

@Component({
  selector: 'faboshop-order-fulfillment-detail',
  templateUrl: './order-fulfillment-detail.component.html',
  styleUrls: ['./order-fulfillment-detail.component.scss']
})
export class OrderFulfillmentDetailComponent implements OnInit, OnChanges {
  @Input() ffm = new Fulfillment({});

  ffm$ = this.orderQuery.selectActive((order) => order.activeFulfillment)

  constructor(
    private ffmService: FulfillmentService,
    public dialog: MatDialog,
    private ordersController: SellOrdersControllerService,
    private orderQuery: OrderQuery
  ) {}

  ngOnInit() {
    this.initBarcode();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initBarcode();
  }

  initBarcode() {
    setTimeout(() => JsBarcode('#shipping-barcode', this.ffm.shipping_code), 0);
  }

  trackingLink() {
    window.open(`https://donhang.ghn.vn/?order_code=${this.ffm.shipping_code}&code=${this.ffm.shipping_code}`);
  }

  get estimatedPickupTime() {
    const { estimated_pickup_at } = this.ffm;
    if (!estimated_pickup_at) {
      return '--';
    }
    return `trước ${moment(estimated_pickup_at).format('HH:mm DD/MM')}`;
  }

  get estimatedDeliveryTime() {
    const { estimated_delivery_at } = this.ffm;
    if (!estimated_delivery_at) {
      return '--';
    }
    return `${moment(estimated_delivery_at).format('DD/MM')}`;
  }

  get isShipment() {
    return true;
  }

  get isShipnow() {
    return false;
  }

  editFulfillment(action) {
    let component: TemplateRef<any> = this.getComponent(action);
    this.ffm = this.orderQuery.getActive().activeFulfillment
    const dialogRef = this.dialog.open(component, {
      // disableClose: true,
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: { fulfillment: this.ffm }
    });
    const componentInstance = dialogRef.componentInstance.submitClicked;
    if (componentInstance) {
      componentInstance.subscribe(async result => {
        await this.ordersController.reloadOrderList();
      });
    }
  }

  getComponent(component) {
    return editComponent[component];
  }
}
