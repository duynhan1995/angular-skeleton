import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { AuthenticateStore } from '@etop/core';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { Order } from 'libs/models/Order';
import { OrderApi } from '@etop/api';

@Component({
  selector: 'shop-import-order',
  templateUrl: './import-order.component.html',
  styleUrls: ['./import-order.component.scss']
})
export class ImportOrderComponent implements OnInit {
  @ViewChild('inputImport', {static: false}) inputImport: ElementRef;
  @ViewChild('importResult', {static: false}) importResult: ElementRef;
  ordersImportFile: any;
  importResults = [];
  importErrors = [];
  total_rows: number;
  importSuccess = false;
  importing = false;
  orders: Array<Order> = [];

  constructor(
    private orderService: OrderService,
    private orderApi: OrderApi,
    private ubt: UserBehaviourTrackingService,
    private auth: AuthenticateStore,
    private ordersController: SellOrdersControllerService
  ) { }

  ngOnInit() {
  }

  uploadFile() {
    this.inputImport.nativeElement.click();
  }

  onSelectFile(event: any): void {
    try {
      if (event.target.files && event.target.files.length > 0) {
        this.ordersImportFile = event.target.files[0];
      }
    } catch (error) {
      toastr.error(error.message);
    }
  }

  reset() {
    this.importSuccess = false;
    this.ordersImportFile = null;
    this.importResults = [];
    this.importErrors = [];
    this.orders = [];
  }

  async import() {
    if (!this.ordersImportFile) {
      toastr.error('Vui lòng chọn file import!');
      return;
    }
    this.importResults = [];
    this.importSuccess = false;
    this.importing = true;
    try {
      let formData = new FormData();
      formData.append('files', this.ordersImportFile);
      formData.append('mode', 'only_order');
      formData.append('code_mode', 'custom');
      let res = await this.orderService.importOrder(formData);

      this.total_rows = res.data.rows.length;
      this.importErrors = res.import_errors || [];
      let cellErrors = res.cell_errors || [];
      let orders = res.orders.filter(o => o && o.id && o.id !== '0') || [];
      this.orders = orders;

      cellErrors.forEach(err => {
        if (err.code != 'ok') {
          this.addToImportResult(
            'error',
            `Lỗi ở hàng ${err.meta.row}, cột ${err.meta.col}: ${err.msg}`
          );
        }
      });

      this.importErrors.forEach(err => {
        if (err.code != 'ok') {
          this.addToImportResult('error', `Lỗi khi import đơn hàng : ${err.msg}`);
        }
      });

      orders.forEach(o => {
        this.addToImportResult(
          'success',
          `Import thành công đơn hàng <strong>${o.code}</strong> - KH: <strong>${
            o.shipping_address.full_name
          }</strong>`
        );
        this.ubt.sendUserBehaviour('CreateOrder');
      });

      await this.ordersController.reloadOrderList();

      if ((!cellErrors && !this.importErrors) || orders.length > 0) {
        this.importSuccess = true;
      }
    } catch (e) {
      toastr.error(e.error && e.error.msg);
    }
    this.importing = false;
  }

  addToImportResult(type, content) {
    switch (type) {
      case 'success':
        content = `<i class="fa fa-check-circle-o text-success"></i> ` + content;
        break;
      case 'error':
        content = `<i class="fa fa-times-circle text-danger"></i> ` + content;
        break;
      case 'warning':
        content = `<i class="fa fa-exclamation-triangle text-warning"></i> ` + content;
        break;
    }

    this.importResults.push({
      type: type,
      content: content
    });

    this.scrollToBottomOfImportResult();
  }

  scrollToBottomOfImportResult() {
    setTimeout(() => {
      const height = this.importResult.nativeElement.scrollHeight;
      this.importResult.nativeElement.scrollTo(
        { left: 0, top: height, behavior: 'smooth' }
      );
    });
  }

  createFFM() {
    const _orders = this.orders.map(o => Order.orderMap(o, this.auth));
    this.ordersController.createFfmAfterImporting$.next(_orders);
  }

  get hasImportResult() {
    return this.importResults.length > 0;
  }

}
