import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FulfillmentApi } from '@etop/api';
import { ShippingService } from 'apps/faboshop/src/services/shipping.service';
import { OrdersService } from '@etop/state/order';


export interface DialogData {
  fulfillment: any;
  new_value: any;
}

enum Step {
  EDIT = 'edit',
  CONFIRM = 'confirm',
}

@Component({
  selector: 'faboshop-edit-weight',
  templateUrl: './edit-weight.component.html',
  styleUrls: ['./edit-weight.component.scss']
})
export class EditWeightComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();
  _step = Step.EDIT;
  step = Step;
  shipping_fee;
  loadingContinue = false;
  loadingConfirm = false;
  formWeight = this.fb.group({
    chargeable_weight: 0
  });
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditWeightComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private orderService: OrdersService,
    private shippingService: ShippingService
  ) {}

  ngOnInit(): void {
    this.formWeight.setValue({
      chargeable_weight: this.data.fulfillment.chargeable_weight
    });
  }

  currentView(step: Step) {
    return this._step == step;
  }

  async submit() {
    try {
      this.loadingConfirm = true;
      await this.orderService.updateGrossWeight(this.data.fulfillment, Number(this.formWeight.controls['chargeable_weight'].value));
      this.loadingConfirm = false;
      this.submitClicked.emit(true);
      this.dialogRef.close();
    } catch (e) {
      this.loadingConfirm = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }

  async confirmStep() {
    if (this.formWeight.controls['chargeable_weight'].value == null 
    || this.formWeight.controls['chargeable_weight'].value == '') {
      return toastr.error('Vui lòng nhập khối lượng hàng hóa');
    }
    if (
      this.formWeight.controls['chargeable_weight'].value ==
      this.data.fulfillment.chargeable_weight
    ) {
      return toastr.error('Vui lòng thay đổi khối lượng hàng hóa');
    }

    let data: any = this.shippingService.parseFulfillmentData(
      this.data.fulfillment
    );
    data = Object.assign({}, data, {
      chargeable_weight: this.formWeight.controls['chargeable_weight'].value,
      connection_ids: [this.data.fulfillment.connection_id]
    });
    this._step = Step.CONFIRM;
    try {
      this.loadingContinue = true;
      const services = await this.shippingService.getShippingServices(data);
      const service = this.shippingService.shippingServiceMap(
        services.services,
        this.data.fulfillment
      );
      this.shipping_fee = service[0]?.fee;
      this.loadingContinue = false;
    } catch (e) {
      this.shipping_fee = false;
      this.loadingContinue = false;
      toastr.error(e.message || e.msg);
      debug.log('ERROR in getShippingServices', e);
    }
  }

  back() {
    this._step = Step.EDIT;
  }
}
