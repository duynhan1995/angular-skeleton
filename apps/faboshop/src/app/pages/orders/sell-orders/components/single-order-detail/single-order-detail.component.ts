import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { Fulfillment } from 'libs/models/Fulfillment';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: ' shop-order-detail',
  templateUrl: './single-order-detail.component.html',
  styleUrls: ['./single-order-detail.component.scss']
})
export class SingleOrderDetailComponent extends BaseComponent implements OnInit {
  @Input() order = new Order({});

  show_table = false;

  active_tab: 'detail_info' | 'history' = 'detail_info';

  constructor(
    private ordersController: SellOrdersControllerService
  ) {
    super();
  }

  async ngOnInit() {
    this.ordersController.onChangeTab$
      .pipe(takeUntil(this.destroy$))
      .subscribe((active_tab: any) => {
        this.active_tab = active_tab;
      });
  }

  get fulfillment(): Fulfillment {
    return this.order.activeFulfillment || null;
  }

  toggleTable() {
    this.show_table = !this.show_table;
  }

}
