import { Component, OnInit } from '@angular/core';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';

@Component({
  selector: 'shop-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.scss']
})
export class PrintComponent implements OnInit {

  constructor(
    private ordersController: SellOrdersControllerService
  ) { }

  ngOnInit() {
    this.ordersController.registerPrint(this);
  }

  print() {
    setTimeout(() => {
      window.print();
    }, 500);
  }

}
