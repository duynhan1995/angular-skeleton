import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder} from "@angular/forms";
import {ConfirmOrderService} from "@etop/features/fabo/confirm-order/confirm-order.service";
import {ConfirmOrderStore} from "@etop/features/fabo/confirm-order/confirm-order.store";
import { debounceTime, map, takeUntil } from 'rxjs/operators';
import {BaseComponent} from "@etop/core";
import {  Variant } from 'libs/models/Product';
import { ConversationsQuery } from '@etop/state/fabo/conversation';
import { ModalController } from '../../../../../../../../../../core/src/components/modal-controller/modal-controller.service';
import { UtilService } from '../../../../../../../../../../core/src/services/util.service';


@Component({
  selector: 'faboshop-products-order',
  templateUrl: './products-order.component.html',
  styleUrls: ['./products-order.component.scss']
})
export class ProductsOrderComponent extends BaseComponent implements OnInit {
  orderForm = this.fb.group({
    lines: this.fb.array([]),
    order_discount: null,
    fee_lines: this.fb.array([
      this.fb.group({
        type: 'other',
        name: 'Phụ thu',
        amount: null
      })
    ]),
    basket_value: 0,
    total_items: 0,
    total_discount: 0,
    total_fee: 0,
    total_amount: 0,
  });

  hideItem = false;
  order$ = this.confirmOrderStore.state$.pipe(map(s => s?.order));
  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0]);


  constructor(
    private fb: FormBuilder,
    private confirmOrderService: ConfirmOrderService,
    private modalController: ModalController,
    private util: UtilService,
    private conversationsQuery: ConversationsQuery,
    private confirmOrderStore: ConfirmOrderStore

) {
    super();
  }

  get lines() {
    return this.orderForm?.controls['lines'] as FormArray;
  }

  get feeLines() {
    return this.orderForm?.controls['fee_lines'] as FormArray;
  }

  ngOnInit() {
    this.getFormDataFromStore();
    this.orderForm.valueChanges.pipe(debounceTime(500)).subscribe(order => {
      const {lines, fee_lines, order_discount} = order;
      lines?.forEach(line => {
        if (line.payment_price != null) {
          line.payment_price = Number(line.payment_price);
        }
        if (line.quantity != null) {
          line.quantity = Number(line.quantity);
        }
        line.list_price = line.payment_price;
        line.retail_price = line.payment_price;
        line.total_price = line.payment_price * line.quantity;
      });

      const basketValue = lines && lines.reduce((a,b) => a + b.payment_price * b.quantity, 0) || 0;
      const totalItems = lines && lines.reduce((a,b) => a + b.quantity, 0) || 0;
      const totalDiscount = Number(order_discount) || 0;
      const totalFee = fee_lines && fee_lines.reduce((a,b) => a + Number(b.amount), 0) || 0;
      const totalAmount = basketValue - totalDiscount + totalFee;

      order = {
        ...order,
        basket_value: basketValue,
        total_items: totalItems,
        total_discount: totalDiscount,
        total_fee: totalFee,
        total_amount: totalAmount,
      };

      // NOTE: Must patch each fields, if not, input-format-number will be flicked
      this.orderForm.patchValue({
        basket_value: basketValue,
        total_items: totalItems,
        total_discount: totalDiscount,
        total_fee: totalFee,
        total_amount: totalAmount,
      }, {
        emitEvent: false
      });


      const _order = this.confirmOrderStore.snapshot.order;
      this.confirmOrderService.updateOrder({
        ..._order,
        ...order
      });
    });

    this.confirmOrderStore.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getFormDataFromStore();
      });
    this.activeFbUser$.subscribe(fbUser => {
      if (!fbUser){
        this.resetForm();
      }
    });
  }

  getFormDataFromStore() {
    this.lines.clear();
    this.orderForm.patchValue({
      order_discount: null,
      fee_lines: [{
        type: 'other',
        name: 'Phụ thu',
        amount: null
      }],
      basket_value: 0,
      total_items: 0,
      total_discount: 0,
      total_fee: 0,
      total_amount: 0,
    });
  }
  resetForm(){
    this.orderForm.controls['basket_value'].patchValue('');
    this.lines.patchValue([]);
    this.feeLines.patchValue([]);
    this.orderForm.controls['total_amount'].patchValue('')
    this.orderForm.controls['total_fee'].patchValue('')
    this.orderForm.controls['total_items'].patchValue('')
  }

  addOrderLine(variant?: Variant) {
    if (variant) {
      const lines = this.lines.value;
      const exist = lines.find(l => l.variant_id == variant.id);
      if (exist) {
        exist.quantity++;
        this.lines.setValue(lines);
      } else {
        this.lines.push(this.fb.group({
          variant_id: variant.id,
          product_name: variant.product_name,
          payment_price: variant.retail_price,
          list_price: variant.retail_price,
          retail_price: variant.retail_price,
          total_price: variant.retail_price,
          quantity: 1
        }))
      }
    } else {
      this.lines.push(
        this.fb.group({
          variant_id: '',
          product_name: '',
          list_price: null,
          retail_price: null,
          payment_price: null,
          total_price: null,
          quantity: null
        })
      );
    }
  }
  removeVariant(index){
    this.lines.removeAt(index)
  }
  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  hideItemFull(){
    this.hideItem = true;
  }
  unhideItemFull(){
    this.hideItem = false;
  }

}
