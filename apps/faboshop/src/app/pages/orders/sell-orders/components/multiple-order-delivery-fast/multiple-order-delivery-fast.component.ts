import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { Order } from '@etop/models';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { VND } from 'libs/shared/pipes/etop.pipe';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { FulfillmentApi, OrderApi } from '@etop/api';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import {
  MoDeliveryFastStoreService,
  ShipServiceOption
} from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-fast/mo-delivery-fast.store.service';

@Component({
  selector: 'shop-multiple-order-delivery-fast',
  templateUrl: './multiple-order-delivery-fast.component.html',
  styleUrls: ['./multiple-order-delivery-fast.component.scss']
})
export class MultipleOrderDeliveryFastComponent extends BaseComponent implements OnInit, OnDestroy, OnChanges {
  @Input() orders: Array<Order> = [];
  @Input() scroll_id: string;

  ship_service_option: ShipServiceOption;

  getting_services = false;
  creating_ffm = false;

  getServicesIndex = 0;
  notHavingShippingServices = 0;

  display_progress_bar = false;
  interval: any;
  progress_percent = 0;

  vndTransform = new VND();

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private ordersController: SellOrdersControllerService,
    private modalController: ModalController,
    private orderService: OrderService,
    private fulfillmentService: FulfillmentService,
    private ffmApi: FulfillmentApi,
    private orderApi: OrderApi,
    private promiseQueue: PromiseQueueService,
    private ffmStore: FulfillmentStore,
    private moFastStore: MoDeliveryFastStoreService
  ) {
    super();
  }

  ngOnInit() {
    this.moFastStore.shipServiceOptionUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe((option: any) => {
        this.ship_service_option = option;
        this.orders.filter(o => this.ordersController.orderToBeUpdated(o))
          .forEach(o => this.selectSuitableService(this.ship_service_option, o));
      });
    this.ordersController.onRegetShipServices$
      .pipe(takeUntil(this.destroy$))
      .subscribe((order_id: string) => {
        this.getServices(this.ship_service_option, order_id ? order_id : null);
      });
    this.moFastStore.selectedOrdersUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(selected_orders => {
        this.orders = selected_orders;
      });
  }

  ngOnDestroy() {
    this.ordersController.deliveryFast = false;
    this.ordersController.deliveryNow = false;
    this.ffmStore.discardCreatingFulfillment();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.moFastStore.updateSelectedOrders(this.orders);
  }

  noAddress(order: Order): boolean {
    return this.ordersController.noAddress(order);
  }

  noWeight(order: Order): boolean {
    return this.ordersController.noWeight(order);
  }

  selectedOrder(order: Order): boolean {
    return this.ordersController.isSelectedOrder(order);
  }

  canCreateFulfillment(order: Order) {
    return SellOrdersControllerService.canCreateFulfillment(order);
  }

  reCreateFFM(order: Order) {
    order.activeFulfillment = null;
    order.p_data.un_updatable = false;
    this.getServices(this.ship_service_option, order.id);
  }

  async getServices(ship_service_option: ShipServiceOption, order_id?: string) {
    this.getting_services = true;
    this.changeDetector.detectChanges();
    try {
      this.getServicesIndex++;
      const promises = this.orders.filter(o => this.ordersController.orderToBeUpdated(o))
        .map(o => async() => {
          if (order_id && o.id != order_id) { return; }
          o.p_data.selected_service = null;

          const pka = o.p_data.shipping.pickup_address;
          const spa = o.p_data.shipping.shipping_address;
          const weight = this.ordersController.uni.chargeable_weight ?
            this.ordersController.shipping_data.chargeable_weight :
            o.p_data.shipping.chargeable_weight;
          if (!pka || !pka.province_code || !pka.district_code) {
            return;
          }
          if (!spa || !spa.province_code || !spa.district_code) {
            return;
          }
          if (!weight) {
            return;
          }

          const index = this.getServicesIndex;
          o.p_data.getting_services = true;
          this.changeDetector.detectChanges();

          const body = {
            from_province_code: o.p_data.shipping.pickup_address.province_code,
            from_district_code: o.p_data.shipping.pickup_address.district_code,
            from_ward_code: o.p_data.shipping.pickup_address.ward_code,
            to_province_code: o.p_data.shipping.shipping_address.province_code,
            to_district_code: o.p_data.shipping.shipping_address.district_code,
            to_ward_code: o.p_data.shipping.shipping_address.ward_code,
            chargeable_weight: weight,
            total_cod_amount: o.p_data.shipping.cod_amount,
            basket_value:  o.basket_value,
            include_insurance: o.p_data.shipping.include_insurance,
            index
          };

          try {
            const res = await this.ffmApi.getShipmentServices(body);
            if (res.index == index) {
              const carriers = this.fulfillmentService.groupShipmentServicesByConnection(res.services);
              o.p_data.services = [].concat(...carriers.map(c => c.services)).filter(s => s.is_available);
              /* NOTE: p_data now contains:
              * - all_services (all services received from server)
              * - services (all available services of an order)
              * - selected_service (pre-select an optimized service for an order)
              * - selected_service_id (service_id of "selected_service")
              */
              o.p_data.services.forEach(s => {
                const from_topship = s.connection_info.name.toLowerCase().indexOf('topship') != -1;
                s.display_name = `
                  <div class="d-flex align-items-center">
                    <div class="carrier-image">
                      <img src="assets/images/provider_logos/${s.carrier}-s.png" alt="">
                      ${from_topship ? `<img src="assets/images/r-topship_256.png" class="topship-logo" alt="">` : ''}
                    </div>
                    <div class="pl-2">${s.connection_info.name.toUpperCase()} ${s.name} (${this.vndTransform.transform(s.fee)}đ)</div>
                  </div>`;
                s.value = s.code;
              });
              this.selectSuitableService(ship_service_option, o);
              o.p_data.getting_services = false;
              this.changeDetector.detectChanges();
            }
          } catch(e) {
            debug.error(`ERROR in Getting ExternalShippingServices #${o.code}`, e);
            o.p_data.getting_services = false;
            this.changeDetector.detectChanges();
          }

        });
      this.start();
      await this.promiseQueue.run(promises, 10);
    } catch(e) {
      debug.error('Lấy gói dịch vụ không thành công.', e);
    }
    if (!this.orders.some(o => o.p_data.getting_services)) {
      this.done();
      this.getting_services = false;
      this.changeDetector.detectChanges();
    }
  }

  selectSuitableService(ship_service_option: ShipServiceOption, order) {
    try {
      const services = order.p_data.services;
      if (!services || !services.length) {
        order.p_data.selected_service = {};
        order.p_data.selected_service_id = '';
        return;
      }
      const {select_method, shipping_type, shipping_provider} = ship_service_option;

      if (select_method == "best_price_first") {
        order.p_data.selected_service = this.fulfillmentService.getMinPacksByBestPrice(
          order.p_data.services,
          shipping_provider,
          shipping_type
        );
      } else {
        order.p_data.selected_service = this.fulfillmentService.sortPacksByCustomPriority(
          order.p_data.services,
          shipping_provider,
          shipping_type
        );
      }
      order.p_data.selected_service_id = order.p_data.selected_service.code;
    } catch(e) {
      debug.error('ERROR in selecting suitable service', e);
      throw e;
    }
  }

  preValidateOrderInfo() {
    const _orders = this.orders.filter(o => this.ordersController.orderToBeUpdated(o));
    _orders.forEach(o => {
        if (!o.p_data.selected_service.carrier) {
          this.notHavingShippingServices += 1;
        }
      });
    if (this.notHavingShippingServices > 0) {
      const invalid = `${this.notHavingShippingServices}/${_orders.length}`;
      toastr.error(`Chưa chọn gói vận chuyển cho ${invalid} đơn hàng. Vui lòng kiểm tra lại.`);
      return false;
    }
    return true;
  }

  async createFFM() {
    /* TODO:
    * - validate info: phone, COD
    */
    if (!this.preValidateOrderInfo()) {
      this.notHavingShippingServices = 0;
      return;
    }
    this.creating_ffm = true;
    try {
      let success = 0;
      const promises = this.orders.filter(o => this.ordersController.orderToBeUpdated(o))
        .map(o => async() => {
          try {
            if (o.confirm_status == 'Z') {
              await this.orderApi.confirmOrder(o.id, 'confirm');
            }

            const {
              cod_amount,
              shipping_address,
              pickup_address,
              include_insurance
            } = o.p_data.shipping;

            const chargeable_weight = this.ordersController.uni.chargeable_weight ?
              this.ordersController.shipping_data.chargeable_weight :
              o.p_data.shipping.chargeable_weight;

            const try_on = this.ordersController.uni.try_on ?
              this.ordersController.shipping_data.try_on :
              o.p_data.shipping.try_on;

            const shipping_note = this.ordersController.uni.shipping_note ?
              this.ordersController.shipping_data.shipping_note :
              o.p_data.shipping.shipping_note;

            const selected_service = o.p_data.selected_service;

            const body: any = {
              order_id: o.id,
              shipping_type: 'shipment',
              cod_amount,
              include_insurance,
              chargeable_weight,
              try_on,
              shipping_note,
              shipping_address,
              pickup_address,
              shipping_service_name: selected_service && selected_service.name || null,
              shipping_service_code: selected_service && selected_service.code || null,
              shipping_service_fee: selected_service && selected_service.fee || null,
              connection_id: (selected_service && selected_service.connection_info)
                && selected_service.connection_info.id || null,
            };

            await this.ffmApi.createShipmentFulfillment(body);
            o.p_data.un_updatable = true;
            success += 1;
          } catch(e) {
            debug.error(`ERROR in Updating Order #${o.code}`, e);
          }
        });
      await this.promiseQueue.run(promises, 10);

      const orders = this.moFastStore.snapshot.selected_orders;
      const totalOrders = orders.filter(o => this.ordersController.orderToBeUpdated(o)).length;
      if (success == totalOrders) {
        toastr.success(`Tạo thành công ${totalOrders} đơn giao nhanh.`);
      } else if (success > 0 && success < totalOrders) {
        toastr.warning(`Tạo thành công ${success}/${totalOrders} đơn giao nhanh.`);
      } else {
        toastr.error(`Tạo thất bại ${totalOrders} đơn giao nhanh.`);
      }
      await this.ordersController.reloadOrderList();
      if (success < totalOrders) {
        await this.getServices(this.ship_service_option);
      }
    } catch(e) {
      toastr.error('Tạo đơn giao nhanh không thành công. Xin vui lòng thử lại!');
    }
    this.creating_ffm = false;
  }

  private start() {
    if (!this.display_progress_bar) {
      this.progress_percent = 0;
      this.display_progress_bar = true;
      this.interval = window.setInterval(() => this.increase(), 500);
    }
  }

  private increase() {
    while(this.progress_percent < 100) {
      this.progress_percent += 10;
    }
    window.clearInterval(this.interval);
  }

  private done() {
    if (this.display_progress_bar) {
      this.progress_percent = 100;
      this.display_progress_bar = false;
    }
  }

}
