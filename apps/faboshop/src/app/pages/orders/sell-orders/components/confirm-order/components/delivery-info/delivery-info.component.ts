import { Component, OnInit } from '@angular/core';
import {
  ShipmentCarrier, FulfillmentShipmentService,
  TRY_ON_OPTIONS,
  TRY_ON_OPTIONS_SHIPPING
} from 'libs/models/Fulfillment';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from "@etop/features/fabo/confirm-order/confirm-order.service";
import { Address } from "libs/models/Address";
import { AuthenticateStore, BaseComponent } from "@etop/core";
import { FormBuilder } from "@angular/forms";
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { ConnectionService } from "@etop/features/connection/connection.service";
import { ConnectionStore } from "@etop/features/connection/connection.store";
import { Router } from "@angular/router";
import { UtilService } from "apps/core/src/services/util.service";
import { ModalController } from "apps/core/src/components/modal-controller/modal-controller.service";
import { ListAddressesPopupComponent } from "apps/faboshop/src/app/components/modals/list-addresses-popup/list-addresses-popup.component";
import { CreateUpdateAddressPopupComponent } from "apps/faboshop/src/app/components/modals/create-update-address-popup/create-update-address-popup.component";
import { CarrierConnectPopupComponent } from "apps/faboshop/src/app/components/modals/carrier-connect-popup/carrier-connect-popup.component";
import { ConnectionAPI, FulfillmentApi } from '@etop/api';
import { FulfillmentService } from '@etop/features/fabo/fulfillment/fulfillment.service';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';

@Component({
  selector: 'faboshop-delivery-info',
  templateUrl: './delivery-info.component.html',
  styleUrls: ['./delivery-info.component.scss']
})
export class DeliveryInfoComponent extends BaseComponent implements OnInit {
  loggedInConnections$ = this.connectionStore.state$.pipe(map(s => s?.loggedInConnections));

  activeShipmentService$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  orderTotalAmountChanged$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.order?.total_amount),
    distinctUntilChanged((prev, next) => prev == next)
  );

  carriers: ShipmentCarrier[] = [];
  carrierServices: FulfillmentShipmentService[] = [];

  getServicesIndex = 0;
  gettingServices = false;

  errorMessage = '';

  shipmentServicesRequest$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.shipmentServicesRequest),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  connectionInitializing = true;
  addressInitializing = true;
  shipping_service_name;
  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0]);


  fulfillmentForm = this.fb.group({
    chargeable_weight: null,
    shipping_service_fee: null,
    coupon: null,
    cod_amount: null,
    insurance_value: null,
    selected_service_unique_id: null,
    shipping_payment_type: null,
    try_on: null,
    shipping_note: '',
    include_insurance: false,

    codAmountAndInsurance: null
  });

  from_ward_code;
  to_ward_code;
  connectionIds: string[] = [];


  carrierConnecting = false;
  displayMap = option => option && ((option.carrier ? option.carrier.toUpperCase() + ' - ' : '') + option.name) || null;
  valueMap = option => option && option.unique_id

  constructor(
    private util: UtilService,
    private conversationsQuery:ConversationsQuery,
    private router: Router,
    private fb: FormBuilder,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private fulfillmentService: FulfillmentService,
    private connectionService: ConnectionService,
    private fulfillmentApi: FulfillmentApi,
    private connectionStore: ConnectionStore,
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private shopSettingsService: ShopSettingsService,
    ) {
    super();
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  get tryOnOptionsShipping() {
    return TRY_ON_OPTIONS_SHIPPING;
  }

  async ngOnInit() {
    this.fulfillmentForm.controls['shipping_payment_type'].valueChanges.subscribe((value)=>{
      const order = this.confirmOrderStore.snapshot.order;
      this.confirmOrderService.updateOrder({
        ...order,
        shipping : {
          ...order.shipping,
          shipping_payment_type : value,
        }
      })
    });
    this.fulfillmentForm.controls['try_on'].valueChanges.subscribe((try_on)=>{
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      this.confirmOrderService.updateFulfillment({
        ...ffm,
        try_on,
      });
    });
    this.connectionService.getConnections().then(connections => {
      this.connectionIds = connections.map(conn => conn.id);
      this.connectionInitializing = false;
    });

    this.getFormDataFromStore();

    this.shipmentServicesRequest$.pipe(takeUntil(this.destroy$))
      .subscribe(async (request) => {
        this.carrierServices =[]
        this.fulfillmentForm.patchValue({
          shipping_service_fee: null,
        })
        const shippingRequest = this.confirmOrderStore.snapshot.shipmentServicesRequest
        this.from_ward_code = shippingRequest.from_ward_code
        this.to_ward_code = shippingRequest.to_ward_code
        await this.getServices(request).then();
        if (this.to_ward_code && this.from_ward_code && !this.errorMessage){
          this.getServiceGHN();
        }
      });
    this.fulfillmentForm.controls['selected_service_unique_id'].valueChanges.subscribe(value => {
      if (value) {
        let service = this.carrierServices.find(carrierService => carrierService.unique_id == value);
        this.fulfillmentForm.patchValue({
          shipping_service_fee: service.fee,
        })
        this.selectService(service);
      }
    });

    this.fulfillmentForm.controls['codAmountAndInsurance'].valueChanges.subscribe(amount => {
      if (amount) {
        const ffm = this.confirmOrderStore.snapshot.fulfillment;
        this.confirmOrderService.updateFulfillment({
          ...ffm,
          cod_amount: amount,
          include_insurance: true,
          insurance_value: amount,
        });
      }
    });

    this.activeShipmentService$.pipe(takeUntil(this.destroy$))
      .subscribe(service => {
        const ffm = this.confirmOrderStore.snapshot.fulfillment;
        this.confirmOrderService.updateFulfillment({
          ...ffm,
          shipping_service_name: service?.name,
          shipping_service_code: service?.code,
          shipping_service_fee: service?.fee,
          connection_id: service?.connection_info?.id,
        });
      });

    this.orderTotalAmountChanged$.pipe(takeUntil(this.destroy$))
      .subscribe(totalAmount => {
        const ffm = this.confirmOrderStore.snapshot.fulfillment;
        if (!ffm?.p_data?.cod_amount_edited) {
          this.fulfillmentForm.patchValue({
            codAmountAndInsurance: totalAmount,
            cod_amount:totalAmount,
            insurance_value:totalAmount,
          });
        }
      });

    this.confirmOrderStore.forceUpdateForm$.pipe(takeUntil(this.destroy$))
      .subscribe(async () => {
        this.addressInitializing = true;
        await this.prepareFromAddress();
        await this.prepareToAddress();
        this.getFormDataFromStore();
        this.addressInitializing = false;
      });

    await this.prepareFromAddress();
    await this.prepareToAddress();
    this.activeFbUser$.subscribe(fbUser => {
      if(!fbUser){
        this.resetForm()
      }
    })
    this.prepareTryOn();
    this.addressInitializing = false;
  }

  async getFormDataFromStore() {
    const settings = await this.shopSettingsService.getSetting();
    settings.payment_type_id = settings?.payment_type_id == 'none'? null : settings?.payment_type_id;
    settings.try_on = settings?.try_on == 'unknown' ? null : settings?.try_on;
    this.fulfillmentForm.patchValue({
      chargeable_weight: settings?.weight || 500,
      cod_amount: null,
      try_on: settings?.try_on || this.auth.snapshot.shop.try_on,
      coupon:null,
      shipping_service_fee: null,
      shipping_note: settings?.shipping_note || '',
      insurance_value: null,
      include_insurance: false,
      shipping_payment_type : settings?.payment_type_id || 'seller'
    });
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      coupon:null,
    });
    this.carrierServices=[];
  }

  onCodAmountInputed() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      p_data: { cod_amount_edited: true }
    });
  }

  onInsurance() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    const insurance_value = this.fulfillmentForm.get('insurance_value').value
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      include_insurance: true,
      insurance_value: insurance_value,
    });
  }

  onCoupon() {
    const coupon = this.fulfillmentForm.get("coupon").value;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      coupon,
    });
  }
  onShippingNote(){
    const shipping_note = this.fulfillmentForm.get("shipping_note").value;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      shipping_note,
    });
  }

  changeCodAmount(){
    const cod_amount = this.fulfillmentForm.get("cod_amount").value;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      cod_amount,
    });
  }

  changeChargeableWeight(){
    const chargeable_weight = this.fulfillmentForm.get("chargeable_weight").value;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      chargeable_weight,
    });
  }

  async prepareFromAddress() {
    await this.confirmOrderService.getFromAddresses();
    const shopDefaultAddressID = this.auth.snapshot.shop.ship_from_address_id;
    let pickup_address;
    if (shopDefaultAddressID && shopDefaultAddressID != '0'){
      pickup_address = this.confirmOrderStore.snapshot.fromAddresses.find(address => address.id === shopDefaultAddressID);
    }else {
      pickup_address = this.confirmOrderStore.snapshot.fromAddresses[0];
    }
    this.confirmOrderService.updatePickupAddress(pickup_address);
    this.confirmOrderService.selectFromAddress(pickup_address);
  }

  async prepareToAddress() {
    await this.confirmOrderService.getToAddresses();
    const shipping_address = this.confirmOrderStore.snapshot.toAddresses[0];
    this.confirmOrderService.updateShippingAddress(shipping_address);
  }

  async prepareTryOn() {
    const shop = this.auth.snapshot.shop;
    const settings = await this.shopSettingsService.getSetting();
    settings.try_on = settings?.try_on == 'unknown' ? null : settings?.try_on;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      try_on: settings?.try_on || shop.try_on,
    });
  }

  async changeAddress(type: 'from' | 'to') {
    const fromAddresses = this.confirmOrderStore.snapshot.fromAddresses;
    const toAddresses = this.confirmOrderStore.snapshot.toAddresses;
    //TODO: case when no customer.id & focus to input => directly open createAddressPopup
    const modal = this.modalController.create({
      component: ListAddressesPopupComponent,
      componentProps: {
        type,
        addresses: type == 'from' ? fromAddresses : toAddresses
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data?.createAddress) {
        this.createOrUpdateAddress(type, new Address({}))
      }
      if (data?.updateAddress) {
        this.createOrUpdateAddress(type, data?.updateAddress);
      }
      if (data?.address) {
        const _address = data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.confirmOrderService.selectFromAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.confirmOrderService.selectToAddress(_address);
        }
      }

    });
    if ((toAddresses.length && type == "to") || (fromAddresses.length && type == "from")) {
      modal.show().then();
    } else {
      this.createOrUpdateAddress(type, new Address({}))
    }
  }

  async createOrUpdateAddress(type: 'from' | 'to', address: Address) {
    const modal = this.modalController.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data?.closed) {
        this.changeAddress(type);
      }
      if (data?.address) {
        const _address = data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.createOrUpdateShopAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.createOrUpdateCustomerAddress(_address);
        }
      }
    });
    modal.show().then();
  }

  async createOrUpdateShopAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateFromAddress(address);
    } else {
      await this.confirmOrderService.createFromAddress(address);
    }
  }

  async createOrUpdateCustomerAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateToAddress(address);
    } else {
      await this.confirmOrderService.createToAddress(address);
    }
  }

  showCarriersList(lastState?: any) {
    const modal = this.modalController.create({
      component: CarrierConnectPopupComponent,
      componentProps: {
        ...lastState
      }
    });
    modal.onDismiss().then(async (data) => {
      if (data) {
        this.loggedInConnections$.pipe().subscribe(async (connections) =>{
          if (connections) {
            this.carrierConnecting = false;
            this.connectionIds = connections.map(connection => connection.connection_id)
            const shippingRequest = this.confirmOrderStore.snapshot.shipmentServicesRequest
            this.from_ward_code = shippingRequest.from_ward_code
            this.to_ward_code = shippingRequest.to_ward_code
            await this.getServices(shippingRequest).then();
            if (this.to_ward_code && this.from_ward_code && !this.errorMessage){
              this.getServiceGHN();
            }
          }
        })
      }
    });
    modal.show().then();
  }

  async loginConnection(data: any) {
    this.carrierConnecting = true;
    try {
      const loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest = {
        ...data.loginOTP
      };
      await this.connectionService.loginShopConnectionWithOTP(loginOTP);
      toastr.success('Kết nối thành công');
    } catch (e) {
      debug.error('ERROR in loginConnection', e);
      toastr.error('Kết nối không thành công');
      this.showCarriersList(data);
    }
    this.carrierConnecting = false;
  }

  selectService(service) {
    this.confirmOrderService.setShippingServiceFee(service?.fee)
    this.confirmOrderService.selectShipmentService(service);
  }

  async getServices(data) {
    if (!data || !data.to_ward_code  || !data.from_ward_code || !data.basket_value) {
      this.errorMessage = null;
      this.fulfillmentForm.patchValue({
        shipping_service_fee: null,
      })
      this.carriers = null;
      return;
    }
    this.selectService(null);

    this.gettingServices = true;
    try {
      this.errorMessage = null;
      const connection_ids: string[] = this.connectionIds
      this.getServicesIndex++;
      const body = {
        ...data,
        connection_ids: connection_ids,
        index: this.getServicesIndex
      };
      const loggedInConnections: any = this.connectionStore.snapshot.loggedInConnections;
      const shopConnectionIds = loggedInConnections && loggedInConnections.map(con => con.connection_id);
      const res = await this.fulfillmentApi.getShipmentServices(body);
      if (res.index == this.getServicesIndex) {
        this.carriers = this.fulfillmentService.groupShipmentServicesByConnection(
          res.services,
          shopConnectionIds
        );
        this.gettingServices = false;
      }
      this.errorMessage = null;
    } catch (e) {
      debug.error('ERROR in Getting Services', e);
      this.errorMessage = e.message;
      this.fulfillmentForm.patchValue({
        shipping_service_fee: null,
      })
      this.gettingServices = false;
    }
  }

  getServiceGHN() {
    const carrierTemp = this.carriers?.find(carrier => carrier.carrier_name = 'ghn');
    this.carrierServices = carrierTemp?.services;
    if (this.carrierServices?.length) {
      const _services: FulfillmentShipmentService[] = JSON.parse(JSON.stringify(this.carrierServices))
      _services.sort((a, b) => a.fee - b.fee)
      this.fulfillmentForm.patchValue({
        selected_service_unique_id: _services[0].unique_id
      })
    }
  }
  resetForm(){
    this.fulfillmentForm.patchValue({
      cod_amount:'',
      insurance_value:0,
    })
  }
}
