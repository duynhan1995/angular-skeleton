import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { AddressService } from 'apps/core/src/services/address.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { MultiOrderNowController } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/multiple-order-delivery-now/multi-order-now-controller.service';
import { GoogleMapService } from 'apps/faboshop/src/services/google-map.service';
import {Address, FulfillmentShipnowService, Order, OrderAddress} from '@etop/models';
import { ShipnowServiceOptionsComponent } from 'apps/faboshop/src/app/components/delivery/delivery-now/shipnow-service-options/shipnow-service-options.component';
import { takeUntil } from 'rxjs/operators';
import { FromAddressModalComponent } from 'apps/faboshop/src/app/pages/settings/components/from-address-modal/from-address-modal.component';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { SellOrdersControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';

@Component({
  selector: 'shop-multi-order-shipping-now',
  templateUrl: './multi-order-shipping-now.component.html',
  styleUrls: ['./multi-order-shipping-now.component.scss']
})
export class MultiOrderShippingNowComponent extends BaseComponent implements OnInit {
  @ViewChild('googleMap', { static: false }) gmapElement: any;
  @ViewChild('shipnowServiceOptions', { static: false }) shipnowServiceOptions: ShipnowServiceOptionsComponent;

  @Input() total_orders = 0;
  @Input() total_cod_amount = 0;
  total_fee = 0;
  @Input() has_valid_orders = false;
  @Input() loading = false;
  @Input() calculating_fee = false;

  orders: Order[] = [];

  fromAddresses = [];
  fromAddressIndex: number;

  shipping_data = {
    pickup_address: new OrderAddress({})
  };

  addressLoading = true;
  rendering_map = true;
  getting_services = true;
  invalid_pickup_address = false;
  selected_service: FulfillmentShipnowService;

  constructor(
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private router: Router,
    private util: UtilService,
    private modalController: ModalController,
    private googleMap: GoogleMapService,
    private ordersController: SellOrdersControllerService,
    private addressService: AddressService,
    private orderService: OrderService,
    private dNowController: MultiOrderNowController,
    private locationCompact: LocationCompactPipe
  ) {
    super();
  }

  get validFromAddress() {
    const { province_code } = this.shipping_data.pickup_address;
    return GoogleMapService.allowedProvinces.indexOf(province_code) != -1;
  }

  async ngOnInit() {
    await this.getListAddress();
    this.googleMapRenderer();

    this.dNowController.onRenderingMap$
      .pipe(takeUntil(this.destroy$))
      .subscribe((orders: Order[]) => {
        this.orders = orders;
        this.googleMapRenderer();
      });
    this.dNowController.onSuccessCreatingFFM$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.total_fee = 0;
        this.googleMap.resetMap();
        this.setFromAddressMarker();
        this.orders = [];
        this.getServices();
      });
    this.dNowController.onFailedCreatingFFM$
      .pipe(takeUntil(this.destroy$))
      .subscribe((orders: Order[]) => {
        this.orders = orders;
        this.googleMapRenderer();
        this.getServices();
      });
  }

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (address) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
        await this.getListAddress();
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
      }
    });
  }

  async getListAddress() {
    this.addressLoading = true;
    try {
      let { fromAddresses, fromAddressIndex } = await this.addressService.getFromAddresses();
      this.fromAddresses = fromAddresses;
      this.fromAddressIndex = fromAddressIndex;
    } catch(e) {
      debug.error('ERROR in getListAddress()', e);
    }
    this.addressLoading = false;
    this.shipping_data.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.updateShippingData();
  }

  setShopAddress() {
    this.shipping_data.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.updateShippingData();
  }

  updateShippingData() {
    this.ordersController.shipping_data = this.shipping_data;
    this.ordersController.onUpdateShippingData$.next(this.shipping_data);
  }

  async googleMapRenderer() {
    this.rendering_map = true;
    try {
      if (!this.validFromAddress) {
        this.googleMap.initDefaultMap(this.gmapElement);
        this.invalid_pickup_address = true;
        throw new Error('Unsupported province. Init default map!');
      }
      const { province_code } = this.shipping_data.pickup_address;
      const { invalid } = await this.setFromAddressMarker();

      await this.googleMap.getBoundCornersOfCity(province_code);

      this.invalid_pickup_address = invalid;
      if (invalid) {
        // TODO step #2: check case pickup address is not valid
      }

      await this.setToAddressesMarkers();
      await this.calculateAndDisplayRoute();
      this.getServices();
    } catch(e) {
      debug.error('ERROR in Rendering Map', e);
    }
    this.rendering_map = false;
  }

  async setFromAddressMarker() {
    try {
      const { address1, ward, district, province, coordinates } = this.shipping_data.pickup_address;
      const from_address_fulltext = `${address1}, ${ward}, ${district}, ${province}`;
      let res: { lng: number; invalid: boolean; lat: number };
      if  (!(coordinates && coordinates.latitude && coordinates.longitude)) {
        const result: any = await this.googleMap.getLatLngFromAddress(from_address_fulltext);
        const { lat: lat1, lng: lng1, invalid: invalid1 } = result;
        res = { lat: lat1, lng: lng1, invalid: invalid1 };
        this.shipping_data.pickup_address = {
          ...this.shipping_data.pickup_address,
          coordinates: {
            latitude: lat1,
            longitude: lng1
          }
        };
        this.updateListAddresses();
        await this.addressService.updateAddress(this.shipping_data.pickup_address);
      } else {
        res = { lat: coordinates.latitude, lng: coordinates.longitude, invalid: false };
      }
      const { lat, lng, invalid } = res;

      this.googleMap.initMap(this.gmapElement, lat, lng);

      this.googleMap.getMarkerFromLatLng(lat, lng, 'assets/images/from-item-0.png');

      if (!this.googleMap.map_points.length) {
        this.googleMap.map_points.push({ lat, lng });
      } else this.googleMap.map_points[0] = {lat, lng};
      return { lat, lng, invalid };
    } catch (e) {
      debug.error('ERROR in setFromAddressMarker', e);
    }
    return null;
  }

  async setToAddressesMarkers() {
    try {
      this.googleMap.markers.forEach(m => m.setMap(null)); // remove markers from map
      this.googleMap.markers = [];
      this.googleMap.map_points.splice(1);
      for (let i = 0; i < this.orders.length; i ++) {
        const shipping_address = this.orders[i].p_data.shipping.shipping_address;
        if (!shipping_address || !shipping_address.coordinates) {
          continue;
        }
        const { coordinates } = shipping_address;
        const { latitude, longitude } = coordinates;
        this.googleMap.map_points.push({ lat: latitude, lng: longitude });
        const marker = this.googleMap.getMarkerFromLatLng(
          latitude, longitude, `assets/images/to-item-${i + 1}.png`);
        this.googleMap.markers.push(marker);
      }
    } catch (e) {
      debug.error('ERROR in setToAddressesMarkers', e);
      throw e;
    }
  }

  async calculateAndDisplayRoute() {
    if (!this.orders.length) {
      this.googleMap.directionsDisplay.setMap(null);
      return;
    }
    const pickup = this.shipping_data.pickup_address;
    const shipping = this.orders[this.orders.length - 1].p_data.shipping.shipping_address;
    if (!pickup || !shipping || !pickup.coordinates || !shipping.coordinates) {
      return;
    }
    const coors_start = pickup.coordinates;
    const coors_end = shipping.coordinates;
    const start = new google.maps.LatLng(coors_start.latitude, coors_start.longitude);
    const end = new google.maps.LatLng(coors_end.latitude, coors_end.longitude);
    const waypoints = [];
    for (let order of this.orders) {
      const shipping_address = order.p_data.shipping.shipping_address;
      if (!shipping_address || !shipping.coordinates) {
        continue;
      }
      waypoints.push({
        location: new google.maps.LatLng(
          shipping_address.coordinates.latitude,
          shipping_address.coordinates.longitude
        ),
        stopover: true
      });
    }
    await this.googleMap.calculateAndDisplayRoute(start, end, waypoints);
  }

  drop(orders: Order[]) {
    this.orders = orders;
    this.googleMapRenderer();
  }

  updateListAddresses() {
    const index = this.fromAddresses.findIndex(a => a.id == this.shipping_data.pickup_address.id);
    this.fromAddresses[index] = this.shipping_data.pickup_address;
  }

  async getServices(orders?) {
    this.getting_services = true;
    try {
      const _orders = orders || this.orders;
      this.shipnowServiceOptions.delivery_points = _orders.map(o => {
        return {
          shipping_address: o.p_data.shipping.shipping_address,
          cod_amount: o.p_data.shipping.cod_amount,
        }
      });
      this.shipnowServiceOptions.pickup_address = this.shipping_data.pickup_address;
      await this.shipnowServiceOptions.getShipnowServices();
    } catch(e) {
      debug.error('ERROR in getting services', e);
      throw e;
    }
    this.getting_services = false;
  }

  onSelectedService(service) {
    this.selected_service = service;
    if (!service) { return; }
    this.total_fee = service.fee;
  }

  createFFM() {
    this.dNowController.onCreatingFFM$.next({
      selected_service: this.selected_service,
      orders: this.orders
    });
  }

  addressDisplay(address: OrderAddress | Address, show_full: false) {
    if (!address) {
      return;
    }
    address = {
      ...address,
      address1: address.address1 && this.locationCompact.transform(address.address1),
      ward: address.ward && this.locationCompact.transform(address.ward),
      district: address.district && this.locationCompact.transform(address.district),
      province: address.province && this.locationCompact.transform(address.province)
    };
    const { address1, ward, district, province } = address;
    if (show_full) {
      return `${address1}, ${ward && ward + ', ' || ''}${district}, ${province}`;
    }
    return `${ward && ward + ', ' || ''}${district}, ${province}`;
  }

}
