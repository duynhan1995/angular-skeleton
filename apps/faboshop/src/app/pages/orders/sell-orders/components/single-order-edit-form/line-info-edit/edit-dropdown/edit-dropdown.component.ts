import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Order } from '@etop/models';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { SingleOrderEditFormControllerService } from 'apps/faboshop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form-controller.service';

@Component({
  selector: 'shop-edit-dropdown',
  templateUrl: './edit-dropdown.component.html',
  styleUrls: ['./edit-dropdown.component.scss']
})
export class EditDropdownComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Input() order = new Order({});
  @Input() fee_type: string;
  @Input() fee_name: string;
  loading = false;

  constructor(
    private orderService: OrderService,
    private orderEditFormController: SingleOrderEditFormControllerService,
  ) { }

  ngOnInit() {
  }

  onClose() {
    this.close.emit();
  }

  updateTotalDiscount() {
    this.order.p_data.total_discount = Number(this.order.p_data.order_discount) + this.order.lines.reduce(
      (a, b) => a + b.quantity * (b.retail_price - b.payment_price), 0) || 0;
  }

  updateTotalFee() {
    const idx = this.order.p_data.fee_lines.findIndex(fl => fl.type == this.fee_type);
    if (idx != -1) {
      this.order.p_data.fee_lines[idx].amount = this.order.p_data['fee_amount_' + this.fee_type];
    } else if (this.fee_type) {
      this.order.p_data.fee_lines.push({
        name: this.fee_name,
        type: this.fee_type,
        amount: Number(this.order.p_data['fee_amount_' + this.fee_type])
      });
    }
    this.order.p_data.total_fee = this.order.p_data.fee_lines.reduce((a, b) =>  a + Number(b.amount), 0) || 0;
  }

  updateTotalAmount() {
    this.updateTotalDiscount();
    this.updateTotalFee();
    const { basket_value, p_data } = this.order;
    this.order.p_data.total_amount = basket_value + p_data.total_fee - p_data.total_discount;
  }

  async updateOrder() {
    this.loading = true;
    try {
      this.updateTotalAmount();
      const { basket_value, total_items, id, p_data, lines } = this.order;
      const body = {
        basket_value,
        total_items,
        order_discount: p_data.order_discount,
        total_discount: p_data.total_discount,
        fee_lines: p_data.fee_lines,
        total_fee: p_data.total_fee,
        total_amount: p_data.total_amount,
        lines,
        id,
      };
      await this.orderService.updateOrder(body);
      await this.orderEditFormController.updateOrderData();
      this.orderService.switchOffCreatingFFM$.next();
      this.onClose();
      toastr.success('Cập nhật đơn hàng thành công.');
    } catch (e) {
      debug.error("ERROR in Updating Order", e);
      toastr.error('Cập nhật đơn hàng thất bại: ', e.msg || e.message);
    }
    this.loading = false;
  }

}
