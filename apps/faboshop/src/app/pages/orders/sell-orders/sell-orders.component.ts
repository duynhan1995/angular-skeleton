import { Component, OnDestroy, OnInit } from '@angular/core';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { takeUntil } from 'rxjs/operators';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import {
  PrintType,
  SellOrdersControllerService
} from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { FilterOperator, FilterOptions, Filters } from '@etop/models';
import { RelationshipService } from '@etop/state/relationship';

@Component({
  selector: 'shop-sell-orders',
  templateUrl: './sell-orders.component.html',
  styleUrls: ['./sell-orders.component.scss']
})

export class SellOrdersComponent extends PageBaseComponent implements OnInit, OnDestroy {
  filters: FilterOptions = [
    {
      label: 'Mã đơn hàng',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Mã đơn giao hàng',
      name: 'fulfillment.shipping_code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
    {
      label: 'Trạng thái đơn hàng',
      name: 'status',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: 'Đặt hàng', value: 'Z' },
        { name: 'Đang xử lý', value: 'S' },
        { name: 'Trả hàng', value: 'NS' },
        { name: 'Hoàn thành', value: 'P' },
        { name: 'Hủy', value: 'N' }
      ]
    },
    {
      label: 'Trạng thái giao hàng',
      name: 'fulfillment.shipping_state',
      type: 'select',
      fixed: true,
      operator: FilterOperator.n,
      options: [
        { name: 'Tất cả', value: '' },
        { name: 'Đã tạo', value: 'created' },
        { name: 'Đã xác nhận', value: 'confirmed' },
        { name: 'Đang xử lý', value: 'processing' },
        { name: 'Đang lấy hàng', value: 'picking' },
        { name: 'Chờ giao', value: 'holding' },
        { name: 'Đang giao hàng', value: 'delivering' },
        { name: 'Đang trả hàng', value: 'returning' },
        { name: 'Đã giao hàng', value: 'delivered' },
        { name: 'Đã trả hàng', value: 'returned' },
        { name: 'Không giao được', value: 'undeliverable' },
        { name: 'Hủy', value: 'cancelled' },
        { name: 'Không xác định', value: 'unknown' },
        { name: 'Không có giao hàng', value: '{}' }
      ]
    },
    {
      label: 'Tên người nhận',
      name: 'customer.name',
      type: 'input',
      operator: FilterOperator.contains
    },
    {
      label: 'Số điện thoại người nhận',
      name: 'customer.phone',
      type: 'input',
      operator: FilterOperator.eq
    },
    {
      label: 'Giá trị đơn hàng',
      name: 'total_amount',
      type: 'input',
      operator: FilterOperator.eq
    },
  ];
  returningOrdersCount = 0;

  constructor(
    private ffmService: FulfillmentService,
    private orderService: OrderService,
    private sellOrdersController: SellOrdersControllerService,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private customerStoreService:CustomerStoreService,
    private relationshipsService: RelationshipService
    ) {
    super();
  }

  get printingOrders() {
    return this.sellOrdersController.printing_orders;
  }

  get isPrintingOrders() {
    return this.sellOrdersController.print_type == PrintType.orders;
  }

  get isPrintingFfms() {
    return this.sellOrdersController.print_type == PrintType.fulfillments;
  }

  get isPrintingOrderAndFfm() {
    return this.sellOrdersController.print_type == PrintType.o_and_f;
  }

  async ngOnInit() {
    await this.relationshipsService.getRelationships();
    this.listReturningOrders();
    this.ffmService.onViewReturningFfms$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        const _filter = this.filters.find(f => f.name == 'fulfillment.shipping_state');
        if (_filter) {
          _filter.value = 'returning';
        }
      });
  }

  ngOnDestroy() {
    this.orderStore.selectOrder(null);
    this.ffmStore.createCustomerByShippingAddress(true);
    this.customerStoreService.resetSelectedCustomer();
  }

  async listReturningOrders() {
    try {
      const filters: Filters = [{
        name: 'fulfillment.shipping_state',
        op: FilterOperator.n,
        value: 'returning'
      }];
      const res = await this.orderService.getOrders(0, 1000, filters);
      this.returningOrdersCount = res.length;
    } catch (e) {
      debug.error('ERROR in listing Returning Orders', e);
    }
  }

}
