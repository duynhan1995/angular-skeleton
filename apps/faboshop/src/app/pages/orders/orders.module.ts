import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { OrdersComponent } from 'apps/faboshop/src/app/pages/orders/orders.component';
import { SellOrdersModule } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders.module';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent
  }
];

const components = [
  OrdersComponent,
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    SellOrdersModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule {}
