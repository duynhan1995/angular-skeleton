import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'shop-orders',
  template: `
    <shop-sell-orders></shop-sell-orders>
  `
})
export class OrdersComponent extends BaseComponent implements OnInit {
  allowView = true;

  constructor() {
    super();
  }

  async ngOnInit() {}

}
