import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../../shared/src/shared.module';
import { NotificationDetailComponent } from './notification-detail/notification-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: NotificationsComponent },
      { path: ':id', component: NotificationDetailComponent }
    ]
  }
];

@NgModule({
  declarations: [
    NotificationsComponent,
    NotificationListComponent,
    NotificationDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule,
  ]
})
export class NotificationsModule { }
