import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../../services/notification.service';
import { AuthenticateStore } from '@etop/core';
import misc from '../../../../../../core/src/libs/misc';

@Component({
  selector: 'shop-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.scss']
})
export class NotificationDetailComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private notification: NotificationService,
    private router: Router,
    private auth: AuthenticateStore
  ) { }

  ngOnInit() {
    const { params, queryParams } = this.activatedRoute.snapshot;
    const id = params.id;
    const shop_id_encoded = queryParams.shop_id ? this.auth.getStringTokenByShop(queryParams.shop_id) : null;
    const token = shop_id_encoded ? misc.decodeBase64(shop_id_encoded) : null;
    this.openNotificationHandler(id, token, shop_id_encoded);
  }

  async openNotificationHandler(id, token?, shop_id_encoded?) {
    try {
      let noti = await this.notification.getNotification(id, token);
      this.notification.readNotification(noti, shop_id_encoded);
    } catch (e) {
      debug.error('ERROR in opening notification', e);
    }
  }

}
