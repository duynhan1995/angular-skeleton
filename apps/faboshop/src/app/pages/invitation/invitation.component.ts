import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';
import { FilterOperator, Invitation } from '@etop/models';
import { AuthorizationApi, NotificationApi, UserApi } from '@etop/api';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { resetStores } from '@datorama/akita';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'shop-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {
  invitation = new Invitation({});

  loading = false;
  refusing = false;
  accepting = false;
  phone = '';

  accountAvailable = false;


  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private auth: AuthenticateStore,
    private authorizationApi: AuthorizationApi,
    private userApi: UserApi,
    private util: UtilService,
    private navCtrl: NavController,
    private router: Router,
    private ref: ChangeDetectorRef,
    private commonUseCase: CommonUsecase,
    private zone: NgZone,
    private notificationApi: NotificationApi,
  ) { }

  get user() {
    return this.auth.snapshot.user;
  }

  async ngOnInit() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.phone = id.substring(1);
    if (this.compareAccount()) {
      await this.getUserInvitations();
    }
  }

  displayRole(role) {
    switch (role) {
      case 'salesman':
        return 'Bán hàng';
      case 'staff_management' :
        return 'Quản lý nhân viên';
      default:
        return '';
    }
  }


  compareAccount() {
    return this.auth.snapshot.user.phone == this.phone;
  }

  async getUserInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const userInvitations = await this.authorizationApi.getUserInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
      let _userInvitations: any = userInvitations.filter(
        user => user.phone == this.phone
      );
      this.invitation = _userInvitations[0];
    } catch (e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }


  async rejectInvitation(invitation: Invitation) {
    this.refusing = true;
    try {
      await this.authorizationApi.rejectInvitation(invitation.token);
      await this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings/accounts`);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in refusing Invitation', e);
    }
    this.refusing = false;
  }


  async accept(invitation: Invitation) {
    this.accepting = true;
    try {
      await this.authorizationApi.acceptInvitation(invitation.token);
      await this.commonUseCase.updateSessionInfo(true);
      await this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings/accounts`);
    } catch(e) {
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        toastr.error(e.message || e.msg);
      }
      debug.error('ERROR in accepting Invitation', e);
    }
    this.accepting = false;
  }

  async loginAnotherAccount() {
    const oneSignal = this.auth.snapshot.oneSignal;
    if (oneSignal) {
      this.notificationApi.deleteDevice(oneSignal).then();
    }
    this.auth.clear();
    this.zone.run(async () => {
      resetStores();
      await this.navCtrl.navigateForward(`/login?invitation=${this.phone}&type=phone`, {
        animated: false
      });
    });
  }

  async back(){
    await this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings/accounts`);
  }

}

