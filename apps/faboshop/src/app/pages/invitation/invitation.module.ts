import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InvitationComponent } from 'apps/faboshop/src/app/pages/invitation/invitation.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ShopPipesModule } from 'apps/faboshop/src/app/pipes/shop-pipes.module';
import { EtopPipesModule } from '@etop/shared';

const routes: Routes = [
  {
    path: ':id',
    component: InvitationComponent
  }
];

@NgModule({
  declarations: [
    InvitationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ShopPipesModule,
    EtopPipesModule,
  ],
  providers: [
  ]
})
export class InvitationModule { }
