import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { FilterOperator, FilterOptions } from '@etop/models';

@Component({
  selector: 'shop-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent extends PageBaseComponent implements OnInit {

  filters: FilterOptions = [
    {
      label: 'Tên khách hàng',
      name: 'full_name',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
    {
      label: 'Số điện thoại khách hàng',
      name: 'phone',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
  ];

  constructor() {
    super();
  }

  ngOnInit() {}

}
