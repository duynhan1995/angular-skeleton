import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';
import * as moment from 'moment';
import { BaseComponent } from '@etop/core';
import { Customer } from '@etop/models';
import { LocationQuery } from '@etop/state/location/location.query';
import { LocationService } from '@etop/state/location';

@Component({
  selector: 'shop-create-customer-form',
  templateUrl: './create-customer-form.component.html',
  styleUrls: ['./create-customer-form.component.scss']
})
export class CreateCustomerFormComponent extends BaseComponent implements OnInit {
  customer = new Customer({});
  @Output() createCustomer = new EventEmitter();

  genders = [{ name: 'Nam', value: 'male' }, { name: 'Nữ', value: 'female' }, { name: 'Khác', value: 'other' }];
  provinces;
  province_code;
  districts;
  district_code;
  wards;
  ward_code;
  address1;

  loading = false;

  valueMap = option => option && option.code || null;
  displayMap = option => option && option.name || null;

  constructor(
    private customerService: CustomerService,
    private locationQuery: LocationQuery,
    private locationService: LocationService
  ) {
    super();
  }

  ngOnInit() {
    this.getProvinces();
  }

  async create() {
    this.loading = true;
    try {
      if (this.customer.birthday) {
        this.customer.birthday = moment(this.customer.birthday)
          .format('YYYY-MM-DD');
      }
      if (!this.customer.full_name) {
        this.loading = false;
        return toastr.error('Vui lòng nhập tên!');
      }
      if (!this.customer.phone) {
        this.loading = false;
        return toastr.error('Vui lòng nhập số điện thoại!');
      }
      let res = await this.customerService.createCustomer(this.customer);
      const customerAddress: any = {
        customer_id: res.id,
        province_code: this.province_code,
        district_code: this.district_code,
        ward_code: this.ward_code,
        address1: this.address1,
        email: res.email,
        phone: res.phone,
        full_name: res.full_name
      };
      const { province_code, district_code, ward_code, address1 } = customerAddress;
      if (province_code && district_code && ward_code && address1) {
        this.customerService.createCustomerAddress(customerAddress);
      }
      toastr.success('Tạo khách hàng mới thành công');
      this.createCustomer.emit();
      this.customer = new Customer({});
    } catch (e) {
      toastr.error('Tạo khách hàng thất bại!', e.message);
    }
    this.loading = false;
  }

  getProvinces() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.provinces.forEach(province => {
      province.value = province.code;
    });
  }

  provinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(this.province_code);
    this.districts.forEach(district => {
      district.value = district.code;
    });
  }

  districtSelected() {
    this.wards = this.locationService.filterWardsByDistrict(this.district_code);
    this.wards.forEach(ward => {
      ward.value = ward.code;
    });
  }
}
