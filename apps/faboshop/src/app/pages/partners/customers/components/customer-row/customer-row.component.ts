import { Component, OnInit, Input } from '@angular/core';
import { Customer } from "@etop/models";

@Component({
  selector: '[shop-customer-row]',
  templateUrl: './customer-row.component.html',
  styleUrls: ['./customer-row.component.scss']
})
export class CustomerRowComponent implements OnInit {
  @Input() customer = new Customer({});
  @Input() liteMode = false;
  async ngOnInit() { }
}
