import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';
import { CustomerAddressModalComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import {CustomerAddress} from "@etop/models";

@Component({
  selector: 'shop-customer-addresses',
  templateUrl: './customer-addresses.component.html',
  styleUrls: ['./customer-addresses.component.scss']
})
export class CustomerAddressesComponent implements OnInit, OnChanges {
  @Input() customer_id: string;
  @Input() customer_name: string;
  addresses: Array<CustomerAddress> = [];
  loading = false;

  constructor(
    private customerService: CustomerService,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private locationCompact: LocationCompactPipe
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getCustomerAddresses();
  }

  async getCustomerAddresses() {
    this.loading = true;
    try {
      this.addresses = await this.customerService.getCustomerAddresses(this.customer_id);
    } catch(e) {
      this.addresses = [];
      debug.error('ERROR in getting customer addresses', e);
    }
    this.loading = false;
  }

  async createNewAddress(customer_id) {
    let modal = await this.modalController.create({
      component: CustomerAddressModalComponent,
      showBackdrop: true,
      componentProps: {
        customer_id,
        title: 'create'
      }
    });
    modal.show().then();
    modal.onDismiss().then(_ => {
      this.getCustomerAddresses();
    });
  }

  async editCustomerAddress(address) {
    let modal = await this.modalController.create({
      component: CustomerAddressModalComponent,
      showBackdrop: true,
      componentProps: {
        address: address,
        title: 'edit'
      }
    });
    modal.show().then();
    modal.onDismiss().then(_ => {
      this.getCustomerAddresses();
    });
  }

  delCustomerAddress(address: CustomerAddress) {
    const dialog = this.dialog.createConfirmDialog({
      title: 'Xoá địa chỉ khách hàng',
      body: `
      <div class="mb-1">${address.full_name} - ${address.phone}</div>
      <div class="mb-1">${this.addressDisplay(address)}</div>
      <div class="font-weight-bold">
        <i class="fa fa-exclamation-triangle text-danger"></i>
        Bạn có chắc muốn xóa địa chỉ này?
      </div>
      `,
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      cancelTitle: 'Đóng',
      onConfirm: async () => {
        try {
          await this.customerService.deleteCustomerAddress(address.id);
          toastr.success('Xoá địa chỉ khách hàng thành công.');
          this.getCustomerAddresses();
          dialog.close();
        } catch(e) {
          debug.error('ERROR in deleting customer address', e);
          toastr.error('Xoá địa chỉ khách hàng không thành công', e.code ? (e.message || e.msg) : '');
        }
      },
      closeAfterAction: false
    });
    dialog.show().then();
  }

  addressDisplay(address: CustomerAddress) {
    if (!address) {
      return '';
    }
    const address1 = this.locationCompact.transform(address.address1);
    const ward = this.locationCompact.transform(address.ward);
    const district = this.locationCompact.transform(address.district);
    const province = this.locationCompact.transform(address.province);

    return `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
  }

}
