import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SingleCustomerEditFormComponent } from './single-customer-edit-form.component';
import { CustomerOrdersComponent } from './components/customer-orders/customer-orders.component';
import { CustomerAddressesComponent } from './components/customer-addresses/customer-addresses.component';
import { CustomerReceiptsComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-receipts/customer-receipts.component';
import { AuthenticateModule } from '@etop/core';
import { EtopPipesModule, EtopMaterialModule, EtopFilterModule } from '@etop/shared';
import { CustomerAddressModalComponent } from './components/customer-address-modal/customer-address-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    AuthenticateModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopFilterModule
  ],
  exports: [SingleCustomerEditFormComponent],
  entryComponents: [
    CustomerAddressModalComponent
  ],
  declarations: [
    SingleCustomerEditFormComponent,
    CustomerOrdersComponent,
    CustomerAddressesComponent,
    CustomerReceiptsComponent,
    CustomerAddressModalComponent
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleCustomerEditFormModule {}
