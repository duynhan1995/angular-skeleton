import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { CreateCustomerFormComponent } from './create-customer-form.component';
import { EtopMaterialModule } from '@etop/shared';


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule],
  exports: [CreateCustomerFormComponent],
  entryComponents: [],
  declarations: [CreateCustomerFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateCustomerFormModule {}