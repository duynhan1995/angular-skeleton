import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { MultipleCustomerEditFormComponent } from './multiple-customer-edit-form.component';
import { EtopMaterialModule } from '@etop/shared';


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule],
  exports: [MultipleCustomerEditFormComponent],
  entryComponents: [],
  declarations: [MultipleCustomerEditFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MultipleCustomerEditFormModule {}
