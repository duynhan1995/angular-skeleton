import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';
import { UtilService } from 'apps/core/src/services/util.service';
import {CustomerAddress} from "@etop/models";
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";

@Component({
  selector: 'shop-customer-address-modal',
  templateUrl: './customer-address-modal.component.html',
  styleUrls: ['./customer-address-modal.component.scss']
})
export class CustomerAddressModalComponent implements OnInit, OnChanges {
  @Input() address = new CustomerAddress({});
  @Input() wardRequired = true;
  @Input() title: string;
  @Input() customer_id: string;
  @Input() confirmBtnClass = 'btn-primary';

  provinces = [];
  districts = [];
  wards = [];
  editData = new CustomerAddress({});
  loading = false;

  constructor(
    private modalDismiss: ModalAction,
    private customerService: CustomerService,
    private util: UtilService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {}

  private validateAddress(data) {
    const {full_name, phone, province_code, district_code, ward_code, address1} = data;
    if (!full_name || !full_name.trim()) {
      toastr.error('Chưa nhập tên!');
      return false;
    }
    if (!phone || !phone.trim()) {
      toastr.error('Chưa nhập số điện thoại!');
      return false;
    }
    if (!province_code) {
      toastr.error('Chưa chọn tỉnh thành!');
      return false;
    }
    if (!district_code) {
      toastr.error('Chưa chọn quận huyện!');
      return false;
    }
    if (!ward_code && this.wardRequired) {
      toastr.error('Chưa chọn phường xã!');
      return false;
    }
    if (!address1) {
      toastr.error('Chưa nhập địa chỉ!');
      return false;
    }
    return true;
  }

  async ngOnInit() {
    this.loadLocation();
    this._prepareEditingCustomer();
  }

  ngOnChanges(changes: SimpleChanges) {
    this._prepareEditingCustomer();
  }

  private _prepareEditingCustomer() {
    if (this.address) {
      this.editData = this.util.deepClone(this.address);
      this.onProvinceSelected();
      this.onDistrictSelected();
    } else {
      this.editData = new CustomerAddress({});
    }
    this.editData.customer_id = this.customer_id;
  }

  loadLocation() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  onProvinceSelected() {
    this.districts = this.locationService.filterDistrictsByProvince(this.editData.province_code);
    if (this.editData.province_code != this.address.province_code) {
      this.editData.district_code = '';
      this.editData.ward_code = '';
    }
  }

  onDistrictSelected() {
    this.wards = this.locationService.filterWardsByDistrict(this.editData.district_code);
    if (
      this.editData.province_code != this.address.province_code &&
      this.editData.district_code != this.address.district_code
    ) {
      this.editData.ward_code = '';
    }
  }

  getTitle() {
    if (this.title == 'create') {
      return 'Thêm địa chỉ mới';
    } else {
      return 'Chỉnh sửa địa chỉ';
    }
  }

  async createAddress() {
    try {
      const res = await this.customerService.createCustomerAddress(this.editData);
      toastr.success('Tạo địa chỉ mới thành công!');
      return res;
    } catch (e) {
      debug.error('ERROR in Creating New Address', e);
      throw e;
    }
  }

  async updateAddress() {
    try {
      const res = await this.customerService.updateCustomerAddress(this.editData);
      toastr.success('Cập nhật địa chỉ thành công!');
      return res;
    } catch (e) {
      debug.error('ERROR in Updating New Address', e);
      throw e;
    }
  }

  async confirm() {
    this.loading = true;
    try {
      const validAddress = this.validateAddress(this.editData);
      if (!validAddress) {
        return this.loading = false;
      }
      if (!this.customer_id && !this.address.id) {
        this.modalDismiss.dismiss(this.editData);
        return this.loading = false;
      }

      let res: any;
      if (this.title == 'create') {
        res = await this.createAddress();
      } else {
        res = await this.updateAddress();
      }
      this.modalDismiss.dismiss(res);
    } catch (e) {
      toastr.error(
        `${this.title == 'create' ? 'Tạo' : 'Cập nhật'} địa chỉ không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }

  numberOnly(event) {
    const num = Number(event.key);
    if (Number.isNaN(num)) {
      event.preventDefault();
      return false;
    }
  }

  closeModal() {
    this.modalDismiss.close(false);
  }

}
