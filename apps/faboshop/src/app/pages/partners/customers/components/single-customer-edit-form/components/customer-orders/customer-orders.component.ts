import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { FilterOperator, Filters, Fulfillment, Order } from '@etop/models';

@Component({
  selector: 'shop-customer-orders',
  templateUrl: './customer-orders.component.html',
  styleUrls: ['./customer-orders.component.scss']
})
export class CustomerOrdersComponent implements OnInit, OnChanges {
  @Input() customer_id: string;
  @Input() customer_name: string;
  orders: Array<Order> = [];
  loading = false;

  constructor(
    private orderService: OrderService,
    private util: UtilService,
    private router: Router,
    private orderStore: OrderStoreService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getOrdersByCustomerId();
  }

  async getOrdersByCustomerId() {
    this.loading = true;
    try {
      const filters: Filters = [{
        name: "customer.id",
        op: FilterOperator.eq,
        value: this.customer_id
      }];
      this.orders = await this.orderService.getOrders(0, 1000, filters);
    } catch(e) {
      debug.error('ERROR in getting orders by customer_id', e);
      this.orders = [];
    }
    this.loading = false;
  }

  viewDetailFFM(order: Order, fulfillment: Fulfillment) {
    const ffm_id = fulfillment.id;
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/orders/${ffm_id}`
    );
  }

  viewDetailOrder(order_code: string) {
    this.orderStore.navigate('so', order_code);
  }

}
