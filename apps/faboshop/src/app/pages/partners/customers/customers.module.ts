import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleCustomerEditFormModule } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/single-customer-edit.module';
import { CreateCustomerFormModule } from 'apps/faboshop/src/app/pages/partners/customers/components/create-customer-form/create-customer-form.module';
import { MultipleCustomerEditFormModule } from 'apps/faboshop/src/app/pages/partners/customers/components/multiple-customer-edit-form/multiple-customer-edit-form.module';
import { CustomerListComponent } from 'apps/faboshop/src/app/pages/partners/customers/customer-list/customer-list.component';
import { CustomerRowComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/customer-row/customer-row.component';
import { CustomersComponent } from 'apps/faboshop/src/app/pages/partners/customers/customers.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { AuthenticateModule } from '@etop/core';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {
  EtopCommonModule,
  EtopPipesModule,
  SideSliderModule,
  EtopMaterialModule,
  EtopFilterModule
} from '@etop/shared';
import { ImageLoaderModule } from '@etop/features';

@NgModule({
  declarations: [
    CustomersComponent,
    CustomerListComponent,
    CustomerRowComponent
  ],
  imports: [
    SingleCustomerEditFormModule,
    CreateCustomerFormModule,
    MultipleCustomerEditFormModule,
    CommonModule,
    SharedModule,
    AuthenticateModule,
    DropdownActionsModule,
    EtopCommonModule,
    EtopPipesModule,
    SideSliderModule,
    EtopFilterModule,
    EtopMaterialModule,
    ImageLoaderModule
  ],
  exports: [CustomersComponent],
  providers: []
})
export class CustomersModule {}
