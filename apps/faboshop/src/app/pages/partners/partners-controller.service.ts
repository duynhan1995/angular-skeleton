import { Injectable } from '@angular/core';
import { CustomerListComponent } from 'apps/faboshop/src/app/pages/partners/customers/customer-list/customer-list.component';
import {Customer} from "@etop/models";

@Injectable()
export class PartnersControllerService {
  private customerListComponent: CustomerListComponent;

  constructor() { }

  registerCustomerList(instance: CustomerListComponent) {
    this.customerListComponent = instance;
  }

  async reloadCustomerList() {
    await this.customerListComponent.reloadCustomers();
  }

  setupDataCustomer(customer: Customer): Customer {
    customer.p_data = {
      ...customer.p_data,
      id: customer.id,
      full_name: customer.full_name,
      phone: customer.phone,
      gender: customer.gender,
      email: customer.email,
      birthday: customer.birthday,
      note: customer.note,
      edited: false,
      editedField: {},
      fb_users : customer.fb_users
    };
    return customer;
  }


}
