import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PartnersComponent } from './partners.component';
import { PartnersControllerService } from 'apps/faboshop/src/app/pages/partners/partners-controller.service';
import { CustomersModule } from 'apps/faboshop/src/app/pages/partners/customers/customers.module';
import { AuthenticateModule } from '@etop/core';
import { EtopMaterialModule, NotPermissionModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: PartnersComponent,
    children: []
  }
];

@NgModule({
  declarations: [PartnersComponent],
  imports: [
    CommonModule,
    CustomersModule,
    RouterModule.forChild(routes),
    AuthenticateModule,
    NotPermissionModule,
    EtopMaterialModule
  ],
  providers: [PartnersControllerService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PartnersModule {}
