import { OnInit, Component, OnDestroy } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-partners',
  template: `
    <etop-not-permission *ngIf="noPermission; else Permission"></etop-not-permission>
    <ng-template #Permission>
      <shop-customers></shop-customers>
    </ng-template>

  `
})
export class PartnersComponent extends BaseComponent
  implements OnInit, OnDestroy {

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private router: Router,
    private traderStore: TraderStoreService
  ) {
    super();
  }

  checkTab(tabName) {
    return this.traderStore.headerTabChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('shop/customer:view');
  }

  ngOnInit() {
    // this.setupTabs();
    // if (this.auth.snapshot.permission.permissions.includes('shop/customer:view')) {
    //   this.traderStore.changeHeaderTab('customer');
    // }
    // else if (this.auth.snapshot.permission.permissions.includes('shop/supplier:view')) {
    //   this.traderStore.changeHeaderTab('supplier');
    // }
    // else {
    //   this.allowView = false;
    // }
    // this.traderStore.headerTabChanged$.subscribe(_ => {
    //   this.setupTabs();
    // });
    // this._paramsHandling();
    // this._routerHandling();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  // NOTE: in view Create/Update PurchaseOrder and click in Order MenuItem in Sidebar
  private _routerHandling() {
    this.router.events.pipe(takeUntil(this.destroy$)).subscribe(event => {
      if (event instanceof NavigationEnd) {
        const acc_slug = this.auth.snapshot.account.url_slug;
        const acc_index = this.auth.currentAccountIndex();
        if (event.url == `/s/${acc_slug || acc_index}/traders`) {
          this.traderStore.changeHeaderTab('customer');
        }
      }
    });
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    if (!queryParams.type) {
      return;
    }
    this.traderStore.changeHeaderTab(queryParams.type);
  }

  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Khách hàng',
          name: 'customer',
          active: false,
          onClick: () => {
            this.tabClick('customer');
          },
          permissions: ['shop/customer:view']
        },
        {
          title: 'Nhà cung cấp',
          name: 'supplier',
          active: false,
          onClick: () => {
            this.tabClick('supplier');
          },
          permissions: ['shop/supplier:view']
        },
        {
          title: 'Nhà vận chuyển',
          name: 'carrier',
          active: false,
          onClick: () => {
            this.tabClick('carrier');
          },
          permissions: ['shop/carrier:view']
        }
      ].map(tab => {
        this.checkTab(tab.name).subscribe(active => (tab.active = active));
        return tab;
      })
    );
  }

  private tabClick(type) {
    this.traderStore.changeHeaderTab(type);
  }
}
