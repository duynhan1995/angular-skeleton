import { Component, OnInit } from '@angular/core';
import { CreateAndUpdateTagComponent } from '../../../components/create-and-update-tag/create-and-update-tag.component';
import { MatDialog } from '@angular/material/dialog';
import { FacebookUserTagQuery, FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'faboshop-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {
  listTags = this.facebookUserTagQuery.selectAll();

  constructor(
    public dialog: MatDialog,
    private auth: AuthenticateStore,
    private facebookUserTagService:FacebookUserTagService,
    private facebookUserTagQuery: FacebookUserTagQuery,
  ) { }

  checkPermission() {
    return this.auth.snapshot.permission.permissions.includes('facebook/shoptag:create');
  }

 async ngOnInit() {
    await this.facebookUserTagService.listTags();
  }


  createTag() {
     this.dialog.open(CreateAndUpdateTagComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
    });
  }


}
