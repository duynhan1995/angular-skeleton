import { Component, Input, OnInit } from '@angular/core';
import { DialogControllerService } from '../../../../../../../../core/src/components/modal-controller/dialog-controller.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateAndUpdateTagComponent } from '../../../../../components/create-and-update-tag/create-and-update-tag.component';
import { FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { FbUserTag } from '@etop/models/faboshop/FbUserTag';

@Component({
  selector: 'faboshop-tag-row',
  templateUrl: './tag-row.component.html',
  styleUrls: ['./tag-row.component.scss']
})
export class TagRowComponent implements OnInit {
  @Input() tag: FbUserTag;


  dropdownActions = [];


  constructor(
    private dialogController: DialogControllerService,
    public dialog: MatDialog,
    private facebookUserTagService: FacebookUserTagService,

  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa thẻ',
        cssClass: 'text',
        onClick: () => this.updateTag()
      },
      {
        title: 'Xóa thẻ',
        cssClass: 'text-danger',
        onClick: () => this.deleteTag()
      },
    ];
  }

  async deleteTag(){
    const dialog = this.dialogController.createConfirmDialog({
      title: `Xóa thẻ`,
      body: `
        <div>Bạn có chắc muốn xóa thẻ <strong></strong>?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.facebookUserTagService.deleteTag(this.tag.id)
          toastr.success(`Xóa thẻ thành công.`);
          dialog.close();
        } catch (e) {
          toastr.error(`Xóa thẻ không thành công.`, e.message || e.msg);
        }
      }
    });
    dialog.show().then();
  }

  updateTag(){
    this.dialog.open(CreateAndUpdateTagComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: {tag: this.tag},
    });
  }

}
