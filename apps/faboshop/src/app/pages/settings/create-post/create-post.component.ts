import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { FbPages } from 'libs/models/faboshop/FbPage';
import { PageApi } from '@etop/api';
import { ChatConnectionModalComponent } from '../../../components/modals/chat-connection-modal/chat-connection-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { PostService } from '@etop/features/fabo/post/post.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { FilterOperator } from '@etop/models';


@Component({
  selector: 'fabo-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent extends BaseComponent implements OnInit {
  pages: FbPages = [];

  loading = true;

  external_page_id ='';
  content ='';

  displayMap = option => option && option.external_name || null;
  valueMap = option => option && option.external_id || null;


  constructor(
   private pageApi:PageApi,
   private modalController: ModalController,
   private dialog: DialogControllerService,
   private postService:PostService,
 ) {
   super();
 }

  async ngOnInit() {
    await this._reload();
    if (this.pages?.length){
      this.external_page_id = this.pages[0].external_id;
    }
  }

  async _reload() {
    this.loading = true;
    try {
      this.pages = await this.pageApi.listPages({
        filters: [{ name: 'status', op: FilterOperator.eq, value: 'P' }]
      });
    } catch(e) {
      debug.error('ERROR in list FanPages', e);
    }
    this.loading = false;
  }

  showResult(data) {
    const modal = this.modalController.create({
      component: ChatConnectionModalComponent,
      cssClass: 'modal-radius',
      componentProps: data
    });
    modal.onDismiss().then(async() => {
      await this._reload();
    });
    modal.onClosed().then(async() => {
      await this._reload();
    })
    modal.show().then();
  }

  async connectFB() {
    try {
      let res = await this.loginFB();
      this.loading = true;
      const connectRes = await this.pageApi.connectPages(res.accessToken);
      this.showResult({
        errors: connectRes.fb_error_pages,
        successes: connectRes.fb_pages
      });
      await this._reload();
    } catch(e) {
      debug.error('ERROR in list FanPages', e);
    }
  }

  loginFB(): any {
    return new Promise((res, rej) => {
      FB.login(
        function(response) {
          debug.log('RESPONSE LOGIN', response);
          if (response.authResponse) {
            res(response.authResponse);
          } else {
            rej('User cancelled login or did not fully authorize.');
          }
        },
        {
          scope: 'pages_show_list,pages_messaging,pages_manage_metadata,pages_read_engagement,pages_read_user_content,pages_manage_engagement,pages_manage_posts'
        }
      );
    });
  }
  async createFbPost(){
    try{
      const post = await this.postService.createPost(this.external_page_id,this.content)
      this.presentDialog(post.external_url);
    }catch (e) {
      debug.error('ERROR in creating post', e);
      const msg =
        (e.code == 'failed_precondition' && e.message) ||
        'đăng bài viết thất bại';
      toastr.error(msg);
    }
  }

  async presentDialog(postUrl){
    const page_name = this.pageNameSelected()
   const  modal = this.dialog.createConfirmDialog({
      title: `Thành công`,
      body: `
        <div><i class="fa fa-check-circle text-success" style="font-size:1.5rem;margin-right: 0.7rem;" aria-hidden="true"></i>Tạo bài viết thành công trên Page ${page_name}</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      confirmTitle: 'Xem trên Facebook',
      onConfirm: async () => {
        window.open(postUrl,'_blank');
      },
      onCancel: () => {
      }
    });
    modal.show();
  }

  pageNameSelected(){
    for (let i=0;i<this.pages.length;i++){
      if (this.pages[i].external_id == this.external_page_id){
        return this.pages[i].external_name;
      }
    }
  }
}
