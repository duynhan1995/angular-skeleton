import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from 'apps/faboshop/src/app/pages/settings/settings.component';
import { ChangePasswordModalComponent } from './components/change-password-modal/change-password-modal.component';
import { ModalControllerModule } from 'apps/core/src/components/modal-controller/modal-controller.module';
import { EditCategoryModalComponent } from './components/edit-category-modal/edit-category-modal.component';
import { EditCollectionModalComponent } from './components/edit-collection-modal/edit-collection-modal.component';
import { ProductSrcCategoryLineComponent } from './components/product-src-category-line/product-src-category-line.component';
import { StaffManagementComponent } from './staff-management/staff-management.component';
import { AuthorizationApi } from '@etop/api';
import { StaffManagementRowComponent } from './components/staff-management-row/staff-management-row.component';
import { AddStaffModalComponent } from 'apps/faboshop/src/app/pages/settings/components/add-staff-modal/add-staff-modal.component';
import { NotMyShopsManagamentComponent } from './components/not-my-shops-managament/not-my-shops-managament.component';
import { AccountInvitationRowComponent } from 'apps/faboshop/src/app/pages/settings/components/account-invitation-row/account-invitation-row.component';
import { UserInvitationRowComponent } from './components/user-invitation-row/user-invitation-row.component';
import { UpdateStaffModalComponent } from './components/update-staff-modal/update-staff-modal.component';
import { AuthenticateModule } from '@etop/core';
import { ShippingInfoComponent } from 'apps/faboshop/src/app/pages/settings/shipping-info/shipping-info.component';
import { TransactionInfoComponent } from 'apps/faboshop/src/app/pages/settings/transaction-info/transaction-info.component';
import { CarrierConnectModalComponent } from './components/carrier-connect-modal/carrier-connect-modal.component';
import { IntegratedInfoComponent } from './integrated-info/integrated-info.component';
import { UserInfoComponent } from 'apps/faboshop/src/app/pages/settings/user-info/user-info.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { FromAddressComponent } from './components/from-address/from-address.component';
import { CarrierConnectComponent } from './components/carrier-connect/carrier-connect.component';
import { ShopsManagementRowComponent } from 'apps/faboshop/src/app/pages/settings/components/not-my-shops-row/shops-management-row.component';
import { CarrierConnectRowComponent } from './components/carrier-connect-row/carrier-connect-row.component';
import { ShopInfoComponent } from 'apps/faboshop/src/app/pages/settings/shop-info/shop-info.component';
import { AccountsInfoComponent } from './accounts-info/accounts-info.component';
import { ShopPipesModule } from 'apps/faboshop/src/app/pipes/shop-pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateUserInfoModalComponent } from 'apps/faboshop/src/app/pages/settings/components/update-user-info-modal/update-user-info-modal.component';
import { ShopCommonUsecase } from 'apps/faboshop/src/app/usecases/shop-common.usecase.service';
import { EtopCommonModule, EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { FacebookConnectComponent } from './facebook-connect/facebook-connect.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { PostService } from '@etop/features/fabo/post/post.service';
import { SettingNotificationComponent } from './setting-notification/setting-notification.component';
import { TagsComponent } from './tags/tags.component';
import { TagRowComponent } from './tags/component/tag-row/tag-row.component';
import { CreateAndUpdateTagModule } from '../../components/create-and-update-tag/create-and-update-tag.module';
import { MyShopsManagementComponent } from './components/my-shops-management/my-shops-management.component';
import { TemplateComponent } from './template/template.component';
import { TemplateRowComponent } from './template/template-row/template-row.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateUpdateTemplateModule } from '../../components/create-update-template/create-update-template.module';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { ReturnAddressModalComponent } from './components/return-address-modal/return-address-modal.component';
import { ImageLoaderModule } from '@etop/features';
const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'shop',
        component: ShopInfoComponent
      },
      {
        path: 'shipping',
        component: ShippingInfoComponent
      },
      {
        path: 'transaction',
        component: TransactionInfoComponent
      },
      {
        path: 'staff',
        component: StaffManagementComponent
      },
      {
        path: 'integrated',
        component: IntegratedInfoComponent
      },
      {
        path: 'create-post',
        component: CreatePostComponent
      },
      {
        path: 'user',
        component: UserInfoComponent
      },
      {
        path: 'message-template',
        component: TemplateComponent
      },
      {
        path: 'accounts',
        component: AccountsInfoComponent
      },
      {
        path: 'facebook',
        component: FacebookConnectComponent
      },
      {
        path: 'carrier',
        component: CarrierConnectComponent
      },
      {
        path: 'setting-notification',
        component: SettingNotificationComponent
      },
      {
        path: 'tags',
        component: TagsComponent,
      },
      {
        path: '**',
        redirectTo: 'facebook'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ModalControllerModule,
    AuthenticateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    ShopPipesModule,
    NgbModule,
    MatIconModule,
    MatDialogModule,
    EtopPipesModule,
    CreateAndUpdateTagModule,
    EtopCommonModule,
    EtopMaterialModule,
    CreateUpdateTemplateModule,
    ImageLoaderModule
  ],
  exports: [],
  entryComponents: [
    ChangePasswordModalComponent,
    EditCategoryModalComponent,
    EditCollectionModalComponent,
    AddStaffModalComponent,
    UpdateStaffModalComponent,
    CarrierConnectModalComponent,
    UpdateUserInfoModalComponent,
  ],
  declarations: [
    SettingsComponent,
    ChangePasswordModalComponent,
    EditCategoryModalComponent,
    EditCollectionModalComponent,
    ProductSrcCategoryLineComponent,
    StaffManagementComponent,
    StaffManagementRowComponent,
    AddStaffModalComponent,
    NotMyShopsManagamentComponent,
    AccountInvitationRowComponent,
    UserInvitationRowComponent,
    UpdateStaffModalComponent,
    MyShopsManagementComponent,
    ShopInfoComponent,
    CreatePostComponent,
    ShippingInfoComponent,
    TransactionInfoComponent,
    CarrierConnectModalComponent,
    IntegratedInfoComponent,
    UserInfoComponent,
    FromAddressComponent,
    CarrierConnectComponent,
    ShopsManagementRowComponent,
    CarrierConnectRowComponent,
    AccountsInfoComponent,
    UpdateUserInfoModalComponent,
    FacebookConnectComponent,
    SettingNotificationComponent,
    TagsComponent,
    TagRowComponent,
    TemplateComponent,
    TemplateRowComponent,
    ReturnAddressModalComponent
  ],
  providers: [
    AuthorizationApi,
    ShopSettingsService,
    PostService,
    ShopCommonUsecase
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {}
