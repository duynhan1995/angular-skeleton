import { Component, OnInit } from '@angular/core';
import { PageApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ChatConnectionModalComponent } from '../../../components/modals/chat-connection-modal/chat-connection-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {FbPage, FbPages} from "libs/models/faboshop/FbPage";
import { FilterOperator } from '@etop/models';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { AuthenticateStore } from '@etop/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'faboshop-facebook-connect',
  templateUrl: './facebook-connect.component.html',
  styleUrls: ['./facebook-connect.component.scss']
})
export class FacebookConnectComponent implements OnInit {
  pages: FbPages = [];

  loading = true;

  constructor(
    private pageApi: PageApi,
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private conversationsService: ConversationsService,
    private dialog: DialogControllerService
  ) {}

  async ngOnInit() {
    await this._reload();
  }

  checkPermission() {
    return this.auth.snapshot.permission.permissions.includes('facebook/fanpage:create');
  }

  async _reload() {
    this.loading = true;
    try {
      this.pages = await this.pageApi.listPages({
        filters: [{ name: 'status', op: FilterOperator.eq, value: 'P' }]
      });
    } catch(e) {
      debug.error('ERROR in list FanPages', e);
    }
    this.loading = false;
  }

  async connectFB() {
    try {
      let res = await this.loginFB();
      this.loading = true;
      const connectRes = await this.pageApi.connectPages(res.accessToken);
      this.showResult({
        errors: connectRes.fb_error_pages,
        successes: connectRes.fb_pages
      });
      await this._reload();
    } catch(e) {
      debug.error('ERROR in list FanPages', e);
    }
  }

  loginFB(): any {
    return new Promise((res, rej) => {
      FB.login(
        function(response) {
          debug.log('RESPONSE LOGIN', response);
          if (response.authResponse) {
            res(response.authResponse);
          } else {
            rej('User cancelled login or did not fully authorize.');
          }
        },
        {
          scope: 'pages_show_list,pages_messaging,pages_manage_metadata,pages_read_engagement,pages_read_user_content,pages_manage_engagement,pages_manage_posts',
        }
      );
    });
  }

  showResult(data) {
    const modal = this.modalController.create({
      component: ChatConnectionModalComponent,
      cssClass: 'modal-radius',
      componentProps: data
    });
    modal.onDismiss().then(async() => {
      await this._reload();
    });
    modal.onClosed().then(async() => {
      await this._reload();
    })
    modal.show().then();
  }

  confirmRemovePage(page: FbPage, index: number) {
    const modal = this.dialog.createConfirmDialog({
      title: `Xóa fanpage`,
      body: `
        <div>Bạn thực sự muốn xóa fanpage "<strong>${page.external_name}</strong>"?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmCss: 'btn-danger text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removePage(page, index);
        modal.close().then();
        await this._reload();
      }
    });
    modal.show().then();
  }

  async removePage(page: FbPage, index: number) {
    try {
      await this.pageApi.removePages(page.external_id);
      this.pages.splice(index, 1);
      toastr.success('Gỡ fanpage thành công.');
      this.conversationsService.removeConversationOfPage(page)
    } catch (e) {
      debug.error('ERROR in removePage', e);
      toastr.error('Gỡ fanpage không thành công.', e.code ? (e.message || e.msg) : '');
    }
  }
}
