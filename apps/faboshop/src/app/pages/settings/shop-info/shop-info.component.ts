import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import {AccountApi} from '@etop/api';
import { Router } from '@angular/router';
import { AddressService } from 'apps/core/src/services/address.service';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { BankAccount } from 'libs/models/Bank';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';
import { Address } from 'libs/models/Address';
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";

@Component({
  selector: 'shop-shop-info',
  templateUrl: './shop-info.component.html',
  styleUrls: ['./shop-info.component.scss']
})
export class ShopInfoComponent extends BaseComponent implements OnInit {
  accounts: Account[] = [];
  shop: ExtendedAccount;
  provinces: any[] = [];
  districts: any[] = [];
  wards: any[] = [];

  uploading = false;
  preparing_data = true;

  updating = {
    shop_info: false
  };

  selectedProvinceCode: string = null;
  selectedDistrictCode: string = null;
  selectedWardCode: string = null;

  currentAccount: Account;

  constructor(
    private auth: AuthenticateStore,
    public util: UtilService,
    private shopService: ShopService,
    private accountApi: AccountApi,
    private router: Router,
    private addressesService: AddressService,
    private settingStore: SettingStore,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {
    super();
  }

  displayMap() {
    return option => option && option.name || null;
  }

  valueMap() {
    return option => option && option.code || null;
  }

  ngOnInit() {
    debug.log('1234',this.auth.snapshot.permission)
    this.auth.authenticatedData$.subscribe(info => {
      this.shop = {...info.shop} || new ExtendedAccount({});
      this.checkNullShop();
      this.currentAccount = info.account || new Account({});
      this.accounts = info.accounts;
    });

    this.prepareLocationData();
  }

  private prepareLocationData() {
    this.loadShopAddress();
  }

  loadShopAddress() {
    if (!this.shop.address) {
      this.shop.address = new Address({})
      this.preparing_data = false;
      return;
    }
    const _provinces = this.locationQuery.getValue().provincesList;
    const _districts = this.locationQuery.getValue().districtsList;
    const _wards = this.locationQuery.getValue().wardsList;

    if (!_provinces || !_provinces.length) {
      return;
    }
    if (!_districts || !_districts.length) {
      return;
    }
    if (!_wards || !_wards.length) {
      return;
    }

    this.provinces = _provinces;
    this.districts = this.locationService.filterDistrictsByProvince(this.shop.address.province_code);
    this.wards = this.locationService.filterWardsByDistrict(this.shop.address.district_code);
    this.selectedProvinceCode = this.shop.address.province_code;
    this.selectedDistrictCode = this.shop.address.district_code;
    this.selectedWardCode = this.shop.address.ward_code;
    this.preparing_data = false;
  }

  onProvinceSelected() {
    this.selectedDistrictCode = null;
    this.selectedWardCode = '';
    if (!this.selectedProvinceCode) {
      this.districts = [];
      this.wards = [];
      return;
    }
    this.districts = this.locationService.filterDistrictsByProvince(this.selectedProvinceCode);
  }

  onDistrictSelected() {
    this.selectedWardCode = null;
    if (!this.selectedDistrictCode) {
      this.wards = [];
      return;
    }
    this.wards = this.locationService.filterWardsByDistrict(this.selectedDistrictCode);
  }

  async onFileSelected($e) {
    this.uploading = true;
    try {
      const { files } = $e.target;
      const res_images = await this.util.uploadImages([files[0]], 250);
      this.shop.image_url = res_images[0].url;

      const data = {
        image_url: this.shop.image_url
      };
      const res = await this.shopService.updateShopField(data);
      this.auth.updateShop(res.shop);
      toastr.success('Cập nhật logo thành công!');
    } catch (e) {
      debug.error(e);
      toastr.error('Cập nhật logo không thành công.', e.code ? (e.message || e.msg) : '');
    }
    this.uploading = false;
  }

  validateShopInfo(data) {
    const { name, phone, website_url, email, address } = data;
    if (!name) {
      toastr.error('Vui lòng nhập tên cửa hàng');
      return false;
    }

    let _phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    _phone = (_phone && _phone.split('-test')[0]) || '';
    const validPhone = this.util.validatePhoneNumber(_phone);
    if (!validPhone) {
      return false;
    }

    if (website_url &&
      !website_url.match(
        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
      )
    ) {
      toastr.error(`Vui lòng điền địa chỉ website hợp lệ`);
      return false;
    }

    let _email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    _email = (_email && _email.split('-test')[0]) || '';
    if (_email &&
      !_email.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      toastr.error('Vui lòng nhập email hợp lệ!');
      return false;
    }

    if (!address.province_code) {
      toastr.error('Vui lòng chọn tỉnh thành');
      return false;
    }
    if (!address.district_code) {
      toastr.error('Vui lòng chọn quận huyện');
      return false;
    }
    if (!address.ward_code) {
      toastr.error('Vui lòng chọn phường xã');
      return false;
    }
    if (!address.address1) {
      toastr.error('Vui lòng nhập địa chỉ');
      return false;
    }
    return true;
  }

  async updateShopInfo() {
    this.updating.shop_info = true;
    try {
      const { name, phone, website_url, email, address } = this.shop;

      const _address = {
        address1: address.address1,
        province_code: this.selectedProvinceCode,
        district_code: this.selectedDistrictCode,
        ward_code: this.selectedWardCode
      };

      const body = {
        name, phone, website_url, email,
        address: _address
      };

      const validInfo = this.validateShopInfo(body);
      if (!validInfo) {
        return this.updating.shop_info = false;
      }

      const res = await this.shopService.updateShopField(body);

      if (this.currentAccount.url_slug) {
        let urlSlug = this.currentAccount.url_slug;
        urlSlug = this.util.createHandle(urlSlug);

        let slugExisted = this.accounts
          .filter(account => account.id != this.currentAccount.id)
          .reduce((x, y) => x || y.url_slug == urlSlug, false);

        if (slugExisted) {
          let index: any = this.accounts
            .findIndex(account => account.id == this.currentAccount.id);
          index = index.toString();

          urlSlug += index ? '-' + index : '-0';
        }

        await this.accountApi.updateURLSlug({
          account_id: this.currentAccount.id,
          url_slug: urlSlug
        });
        this.currentAccount.url_slug = urlSlug;
        this.auth.updateAccount(this.currentAccount);
      }

      this.auth.updateShop(res.shop);
      this.checkNullShop();
      this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings/shop`);
      toastr.success('Cập nhật thông tin cửa hàng thành công!');
    } catch (e) {
      debug.error('ERROR in Updating Shop Info', e);
      toastr.error('Cập nhật thông tin cửa hàng không thành công.', e.code ? (e.message || e) : '');
    }
    this.updating.shop_info = false;
  }

  onChangeShopName() {
    this.currentAccount.url_slug = this.util.createHandle(
      this.shop.name
    );
  }

  checkNullShop() {
    if (!this.shop.bank_account) {
      this.shop.bank_account = new BankAccount();
    }
  }

  formatAddress(address) {
    return this.addressesService.joinAddress(address);
  }

  updateFromAddress() {
    this.settingStore.changeMenu(SettingMenu.shipping_info);
  }

}
