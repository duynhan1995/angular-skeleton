import {AfterViewInit, Component, OnInit} from '@angular/core';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute } from '@angular/router';
import {DEFAULT_RRULE, ExtendedAccount} from 'libs/models/Account';
import {Balance} from 'libs/models/Bank';
import {map, takeUntil} from 'rxjs/operators';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { RequireStokenModalComponent } from 'apps/faboshop/src/app/components/modals/require-stoken-modal/require-stoken-modal.component';
import {BankQuery} from "@etop/state/bank";
import {combineLatest} from "rxjs";
import {FormBuilder} from "@angular/forms";
import {GoogleAnalyticsService, USER_BEHAVIOR} from "apps/core/src/services/google-analytics.service";

@Component({
  selector: 'shop-bank-info',
  templateUrl: './transaction-info.component.html',
  styleUrls: ['./transaction-info.component.scss']
})
export class TransactionInfoComponent extends BaseComponent implements OnInit, AfterViewInit {
  currentShop: ExtendedAccount;
  balance : Balance;
  updating = {
    bank_account: false,
    money_transaction_calendar: false,
  };

  money_transaction_rrule = DEFAULT_RRULE;
  money_transaction_rrule_options = [
    {
      name: 'Hàng ngày (thứ 2-3-4-5-6)',
      value: 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR',
      disabled: true
    },
    {
      name: 'Thứ 2-4-6 hàng tuần',
      value: 'FREQ=WEEKLY;BYDAY=MO,WE,FR'
    },
    {
      name: 'Thứ 3-5 hàng tuần',
      value: 'FREQ=WEEKLY;BYDAY=TU,TH'
    },
    {
      name: '1 tuần 1 lần vào thứ 6',
      value: 'FREQ=WEEKLY;BYDAY=FR'
    },
    {
      name: '1 tháng 2 lần vào thứ 6 (tuần thứ 2 và tuần cuối cùng của tháng)',
      value: 'FREQ=MONTHLY;BYDAY=+2FR,-1FR'
    },
    {
      name: '1 tháng 1 lần vào thứ 6 (cuối cùng của tháng)',
      value: 'FREQ=MONTHLY;BYDAY=-1FR'
    },
    {
      name: '1 tháng 1 lần vào ngày cuối cùng của tháng',
      value: 'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1'
    }
  ];

  mt_calendar_suggest: boolean;

  bankReady$ = this.bankQuery.select(state => !!state.bankReady);

  bankForm = this.fb.group({
    bankCode: '',
    bankProvinceCode: '',
    bankBranchCode: '',
    accountName: '',
    accountNumber: ''
  });

  formInitializing = true;

  banksList$ = this.bankQuery.select("banksList");
  bankProvincesList$ = combineLatest([
    this.bankQuery.select("bankProvincesList"),
    this.bankForm.controls['bankCode'].valueChanges]).pipe(
    map(([bankProvinces, bankCode]) => {
      if (!bankCode) { return []; }
      return bankProvinces?.filter(prov => prov.bank_code == bankCode);
    })
  );
  bankBranchesList$ = combineLatest([
    this.bankQuery.select("bankBranchesList"),
    this.bankForm.controls['bankCode'].valueChanges,
    this.bankForm.controls['bankProvinceCode'].valueChanges]).pipe(
    map(([bankBranches, bankCode, bankProvinceCode]) => {
      if (!bankProvinceCode) { return []; }
      return bankBranches?.filter(branch => branch.bank_code == bankCode && branch.province_code == bankProvinceCode);
    })
  );

  bankValueMap = bank => bank && bank.code || null;
  bankDisplayMap = bank => bank && bank.name || null;

  constructor(
    private fb: FormBuilder,
    private shopService: ShopService,
    private gaService: GoogleAnalyticsService,
    private auth: AuthenticateStore,
    public util: UtilService,
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private bankQuery: BankQuery,
  ) {
    super();
  }

  async ngOnInit() {
    let mtc_sug = this.activatedRoute.snapshot.queryParamMap.get('mtc_sug');
    if (mtc_sug == '1') {
      this.mt_calendar_suggest = true;
    }

    this.bankForm.controls['bankCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.formInitializing) {
          this.bankForm.patchValue({
            bankProvinceCode: '',
            bankBranchCode: '',
          });
        }
      });

    this.bankForm.controls['bankProvinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.formInitializing) {
          this.bankForm.patchValue({
            bankBranchCode: '',
          });
        }
      });

    this.auth.authenticatedData$.subscribe(info => {
      this.currentShop = {...info.shop} || new ExtendedAccount({});
      this.prepareBankForm();
      this.money_transaction_rrule = this.currentShop.money_transaction_rrule;
    });

    this.balance = await this.shopService.calcShopBalance();
  }

  ngAfterViewInit() {
    this.prepareBankForm();
  }

  prepareBankForm() {
    if (!this.currentShop?.bank_account) {
      return;
    }

    this.formInitializing = true;

    const bankCode = this.bankQuery.getBankByName(this.currentShop.bank_account.name)?.code;
    const bankProvinceCode = this.bankQuery.getBankProvinceByName(this.currentShop.bank_account.province)?.code;
    const bankBranchCode = this.bankQuery.getBankBranchByName(this.currentShop.bank_account.branch, bankProvinceCode)?.code;

    this.bankForm.patchValue({
      bankCode,
      bankProvinceCode,
      bankBranchCode,
      accountName: this.currentShop.bank_account.account_name,
      accountNumber: this.currentShop.bank_account.account_number,
    });

    this.formInitializing = false;
  }

  formatName() {
    const accountName = this.bankForm.getRawValue().accountName;
    this.bankForm.patchValue({
      accountName: this.util.formatName(accountName)
    }, {emitEvent: false});
  }

  validateBankInfo() {
    const {bankCode, bankProvinceCode, bankBranchCode, accountName, accountNumber} = this.bankForm.getRawValue();
    if (!bankCode) {
      toastr.error('Vui lòng chọn ngân hàng');
      return false;
    }
    if (!bankProvinceCode) {
      toastr.error('Vui lòng chọn tỉnh của ngân hàng!');
      return false;
    }
    if (!bankBranchCode) {
      toastr.error('Vui lòng chọn chi nhánh ngân hàng!');
      return false;
    }
    if (!accountName) {
      toastr.error('Vui lòng nhập tên chủ tài khoản!');
      return false;
    }
    if (!accountNumber) {
      toastr.error('Vui lòng nhập số tài khoản!');
      return false;
    } else {
      const _accountNumber = this.util.cleanNumberString(accountNumber);
      if (!_accountNumber.match(/^[0-9]{5,30}$/)) {
        toastr.error('Vui lòng nhập số tài khoản hợp lệ!');
        return false;
      }
    }

    return true;
  }

  async updateBankAccount(stoken?) {
    this.updating.bank_account = true;
    try {
      this.gaService.sendUserBehavior(
        USER_BEHAVIOR.ACTION_SHOP_SETTING,
        USER_BEHAVIOR.LABEL_UPDATE_BANK_ACCOUNT
      );

      if (!this.validateBankInfo()) {
        this.updating.bank_account = false;
        return;
      }

      const bankData = this.bankForm.getRawValue();

      const body = {
        name: this.bankQuery.getBank(bankData.bankCode)?.name,
        province: this.bankQuery.getBankProvince(bankData.bankBranchCode)?.name,
        branch: this.bankQuery.getBankBranch(bankData.bankBranchCode)?.name,
        account_name: bankData.accountName,
        account_number: bankData.accountNumber
      };

      const token = stoken || this.auth.snapshot.shop.token;

      const res = await this.shopService.updateBankAccount(body, token);
      this.auth.updateShop(res.shop);

      toastr.success('Cập nhật thành công!');

    } catch (e) {
      debug.error('ERROR in Updating Bank Account', e);
      if (e && e.meta && e.meta.xcode == 'stoken_required') {
        this.openStokenModal();
      } else {
        toastr.error('Cập nhật thông tin ngân hàng không thành công.', e.code ? (e.message || e.msg) : '');
      }
    }
    this.updating.bank_account = false;
  }

  openStokenModal() {
    const modal = this.modalController.create({
      component: RequireStokenModalComponent
    });
    modal.show().then();
    modal.onDismiss().then(async(stoken) => {
      await this.updateBankAccount(stoken);
    });
  }

  async updateMoneyTransactionCalendar() {
    this.updating.money_transaction_calendar = true;

    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_SHOP_SETTING,
      USER_BEHAVIOR.LABEL_UPDATE_MONTHLY_MT
    );

    try {
      if (!this.money_transaction_rrule) {
        throw new Error('Vui lòng chọn lịch chuyển khoản');
      }
      let body = {
        money_transaction_rrule: this.money_transaction_rrule
      };
      await this.shopService.updateMoneyTransactionCalendar(body);
      this.currentShop.money_transaction_rrule = this.money_transaction_rrule;
      this.auth.updateShop(this.currentShop);
      toastr.success(
        'Cập nhật lịch chuyển khoản thành công, thay đổi sẽ có tác dụng trong vòng 24h.'
      );
    } catch (e) {
      toastr.error('Cập nhật lịch chuyển khoản không thành công.');
    }
    this.updating.money_transaction_calendar = false;
    this.mt_calendar_suggest = false;
  }

}
