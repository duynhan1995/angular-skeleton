import { Component, OnInit } from '@angular/core';
import { Address } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { AuthenticateStore } from '@etop/core';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { FromAddressModalComponent } from 'apps/faboshop/src/app/pages/settings/components/from-address-modal/from-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { ExtendedAccount } from 'libs/models/Account';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { AppService } from '@etop/web/core/app.service';
import { TRY_ON_OPTIONS_SHIPPING } from '@etop/models';
import { ReturnAddressModalComponent } from '../components/return-address-modal/return-address-modal.component';
import { ShopSettings } from '@etop/models/ShopSettings';

@Component({
  selector: 'shop-delivery-info',
  templateUrl: './shipping-info.component.html',
  styleUrls: ['./shipping-info.component.scss']
})
export class ShippingInfoComponent implements OnInit {
  addressesFrom: Array<Address> = [];
  addressesTo: Array<Address> = [];
  addressesLoading = true;
  currentShop = new ExtendedAccount({});
  dropdownActions = [];
  settings = new ShopSettings({});
  constructor(
    private addressService: AddressService,
    private shopSettingsService: ShopSettingsService,
    private appService: AppService,
    private addressesService: AddressService,
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private modalController: ModalController,
    private orderService: OrderService
  ) {}

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }

  async ngOnInit() {
    this.dropdownActions = [
      {
        onClick: () => this.editReturnAddress(this.settings?.return_address),
        title: 'Chỉnh sửa',
        permissions: ['shop/settings/shipping_setting:update'],
      }
    ];
    let auth = this.auth.snapshot;
    this.currentShop = auth.shop;
    await this.getAddresses();
    this.settings = await this.shopSettingsService.getSetting();
  }

  async getAddresses() {
    try {
      let addresses = await this.addressesService.getAddresses();
      this.addressesTo = addresses.filter(a => a.type == 'shipto');
      this.addressesFrom = addresses.filter(a => a.type == 'shipfrom');
      this.addressesLoading = false;
    } catch (e) {
      debug.log(e);
    }
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

  get tryOnOptionsShipping() {
    return TRY_ON_OPTIONS_SHIPPING;
  }

  async updateTryOn() {
    try {
      await this.shopService.updateShopField({ try_on: this.currentShop.try_on });
      this.auth.updateTryOn(this.currentShop.try_on);
      toastr.success('Cập nhật thành công!');
    } catch (e) {
      toastr.error('Cập nhật thất bại: ' + e.message);
    }
  }

  openAddAddressForm(type) {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: type,
        title: 'Thêm địa chỉ'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (!address) {
        return;
      }
      if (this.addressesFrom.length == 0) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
      }
      this.addressesFrom.push(address);
    });
  }

  onRemoveAddress(address_id) {
    this.addressesFrom = this.addressesFrom.filter(a => a.id != address_id);
  }

  formatAddress(address) {
    return this.addressService.joinAddress(address);
  }

  editReturnAddress(address: Address) {
    let modal = this.modalController.create({
      component: ReturnAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: address,
        title: 'Cập nhật địa chỉ trả hàng',
        setting: this.settings
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({settings: settings}) => {
      this.settings = settings
    });
  }

  openAddReturnAddressForm() {
    let modal = this.modalController.create({
      component: ReturnAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: new Address({}),
        title: 'Tạo địa chỉ trả hàng',
        setting: this.settings
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({settings: settings}) => {
      this.settings = settings
    });
  }

  async updateSetting() {
    try{
      const res = await this.shopSettingsService.updateSetting(this.settings);
      this.settings = res;
      toastr.success('Cập nhật thông số mặc định thành công.');
    } catch(e) {
      toastr.error(e.msg || e.message || 'Cập nhật thất bại.');
    }
  }
}


