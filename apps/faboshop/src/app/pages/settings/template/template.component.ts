import { Component, OnInit } from '@angular/core';
import { MessageTemplateQuery, MessageTemplateService } from '@etop/state/fabo/message-template';
import { MatDialog } from '@angular/material/dialog';
import { CreateUpdateTemplateComponent } from '../../../components/create-update-template/create-update-template.component';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'faboshop-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {
  messageTemplates$ = this.messageTemplateQuery.selectAll();

  constructor(
    public dialog: MatDialog,
    private auth: AuthenticateStore,
    private messageTemplateService: MessageTemplateService,
    private messageTemplateQuery: MessageTemplateQuery,
  ) {
  }

  async ngOnInit() {
    await this.initDataToStore()
  }

  async initDataToStore() {
    await this.messageTemplateService.getMessageTemplateVariables()
    await this.messageTemplateService.getMessageTemplate()
  }

  createTemplate() {
    this.dialog.open(CreateUpdateTemplateComponent, {
      hasBackdrop: true,
      minWidth: '500px',
      maxWidth: '700px',
      position: {
        top: '8%'
      },
      data: null,
    });
  }

  checkPermission() {
    return this.auth.snapshot.permission.permissions.includes('facebook/message_template:create');
  }
}
