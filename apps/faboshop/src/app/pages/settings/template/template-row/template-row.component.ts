import { Component, Input, OnInit } from '@angular/core';
import { MessageTemplate } from '@etop/models';
import { MatDialog } from '@angular/material/dialog';
import { DialogControllerService } from '../../../../../../../core/src/components/modal-controller/dialog-controller.service';
import { MessageTemplateService } from '@etop/state/fabo/message-template';
import { CreateUpdateTemplateComponent } from '../../../../components/create-update-template/create-update-template.component';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'faboshop-template-row',
  templateUrl: './template-row.component.html',
  styleUrls: ['./template-row.component.scss']
})
export class TemplateRowComponent implements OnInit {
  @Input() messageTemplate: MessageTemplate;

  dropdownActions = [];

  constructor(
    private dialogController: DialogControllerService,
    public dialog: MatDialog,
    private auth: AuthenticateStore,
    private messageTemplateService: MessageTemplateService,
  ) { }

  ngOnInit() {
    if (this.checkPermission('facebook/message_template:update')) {
      this.dropdownActions = [
        {
          title: 'Chỉnh sửa',
          cssClass: 'text',
          onClick: () => this.updateMessageTemplate()
        }];
    }
    if (this.checkPermission('facebook/message_template:delete')) {
      this.dropdownActions.push( {
        title: 'Xóa',
        cssClass: 'text-danger',
        onClick: () => this.deleteMessageTemplate()
      });
    }
  }


  async deleteMessageTemplate(){
    const dialog = this.dialogController.createConfirmDialog({
      title: `Xóa`,
      body: `
        <div>Bạn có chắc muốn xóa mẫu <strong>/` + this.messageTemplate.short_code +`</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.messageTemplateService.deleteMessageTemplate(this.messageTemplate.id)
          toastr.success(`Xóa thành công.`);
          dialog.close();
        } catch (e) {
          toastr.error(`Xóa không thành công.`, e.message || e.msg);
        }
      }
    });
    dialog.show().then();
  }

  updateMessageTemplate(){
      this.dialog.open(CreateUpdateTemplateComponent, {
        hasBackdrop: true,
        minWidth: '500px',
        position: {
          top: '8%'
        },
        data: this.messageTemplate,
      });
  }

  checkPermission(perm) {
    return this.auth.snapshot.permission.permissions.includes(perm);
  }
}
