import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MessageTemplateQuery } from '@etop/state/fabo/message-template';
import { MatInput } from '@angular/material/input';
import { MessageTemplateVariable } from '@etop/models';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'faboshop-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.scss']
})
export class CreateTemplateComponent implements OnInit {
  messageTemplateVariables$ = this.messageTemplateQuery.select(s => s.messageTemplateVariables);

  messageTemplate = this.fb.group({
    short_code: '',
    template: '',
    templateSendToServer: ''
  });

  constructor(
    private fb: FormBuilder,
    private messageTemplateQuery: MessageTemplateQuery,
    public dialogRef: MatDialogRef<CreateTemplateComponent>
  ) {
  }

  ngOnInit() {
    this.messageTemplate.controls['template'].valueChanges.pipe().subscribe(selectedValue => {
      debug.log('sec',selectedValue)
      const basd = this.messageTemplate.value
      debug.log('sec',basd.template)
      const subString = selectedValue - basd.template;
      debug.log('sub',subString)
    })
  }

  addTemplate(messageTemplateVariable: MessageTemplateVariable) {
    const template = this.messageTemplate.controls['template'].value
    this.messageTemplate.patchValue({
      template: template + ' [' + messageTemplateVariable.label + ']',
    })
  }


  async confirm() {

  }

  close(){
    this.dialogRef.close();
  }

}
