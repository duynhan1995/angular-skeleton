import { Component, Input, OnInit } from '@angular/core';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { AuthorizationApi } from '@etop/api';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';

@Component({
  selector: '[shop-shops-management-row]',
  templateUrl: './shops-management-row.component.html',
  styleUrls: ['./shops-management-row.component.scss']
})
export class ShopsManagementRowComponent implements OnInit {
  @Input() account: Account;
  @Input() currentShop: ExtendedAccount;

  dropdownActions = [];

  constructor(
    private settingStore: SettingStore
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa',
        cssClass: this.account.id != this.currentShop.id && 'cursor-not-allowed',
        disabled: this.account.id != this.currentShop.id,
        onClick: () => this.edit()
      }
    ];
  }

  rolesDisplay(roles: string[]) {
    if (roles && roles.length) {
      return roles.map(r => AuthorizationApi.roleMapFabo(r)).join(", ");
    }
    return 'Chủ shop';
  }

  switchAccount(index) {
    window.open(`/s/${index}/settings/shop`, '_blank');
  }

  edit() {
    this.settingStore.changeMenu(SettingMenu.shop);
  }

}
