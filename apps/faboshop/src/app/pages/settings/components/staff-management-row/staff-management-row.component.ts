import { Component, Input, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { Relationship, StaffRole } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { UpdateStaffModalComponent } from 'apps/faboshop/src/app/pages/settings/components/update-staff-modal/update-staff-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: '[shop-staff-management-row]',
  templateUrl: './staff-management-row.component.html',
  styleUrls: ['./staff-management-row.component.scss']
})
export class StaffManagementRowComponent implements OnInit {
  @Input() staff = new Relationship();

  isCurrentUser = false;

  removing = false;

  dropdownActions = [];

  private modal: any;

  constructor(
    private authorizationApi: AuthorizationApi,
    private authorizationService: AuthorizationService,
    private authStore: AuthenticateStore,
    private dialog: DialogControllerService,
    private modalController: ModalController
  ) { }

  get isOwner() {
    return this.staff.roles.indexOf('owner') != -1;
  }

  checkPermission() {
    return this.authStore.snapshot.permission.permissions.includes('relationship/relationship:remove') && this.authStore.snapshot.permission.permissions.includes('relationship/relationship:update')
    && !!this.staff.roles.find(role => {
      return !this.authStore.snapshot.permission.roles.includes(role);
      })
  }

  ngOnInit() {
    this.isCurrentUser = this.checkCurrentUser();
    this.dropdownActions = [
      {
        title: 'Cập nhật',
        cssClass: this.isCurrentUser && 'cursor-not-allowed',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:update','relationship/permission:update'],
        onClick: () => this.updateStaff()
      },
      {
        title: 'Xoá khỏi shop',
        cssClass: this.isCurrentUser && 'cursor-not-allowed' || 'text-danger',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:remove'],
        onClick: () => this.confirmRemoveStaff()
      }
    ];
  }

  checkCurrentUser() {
    return this.staff.user_id == this.authStore.snapshot.user.id;
  }

  updateStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: UpdateStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        staff: {
          ...this.staff,
          p_data: {
            ...this.staff,
          }
        }
      }
    });
    this.modal.show();
    this.modal.onDismiss().then(updated => {
      this.modal = null;
      if (updated) {
        this.authorizationService.updateRelationship();
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async removeStaff() {
    this.removing = true;
    try {
      await this.authorizationApi.removeUserFromAccount(this.staff.user_id);
      this.authorizationService.removeRelationship();
      toastr.success('Xoá nhân viên thành công.')
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Staff', e);
    }
    this.removing = false;
  }

  confirmRemoveStaff() {
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá nhân viên`,
      body: `
        <div>Bạn thực sự muốn xóa nhân viên "<strong>${this.staff?.full_name}</strong>"?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmCss: 'btn-danger text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removeStaff();
        modal.close().then();
      }
    });
    modal.show().then();
  }
}
