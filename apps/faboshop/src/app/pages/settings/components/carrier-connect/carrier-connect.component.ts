import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConnectionApi } from '@etop/api';
import {Connection, ShopConnection, STATUS} from 'libs/models/Connection';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'shop-carrier-connect',
  templateUrl: './carrier-connect.component.html',
  styleUrls: ['./carrier-connect.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarrierConnectComponent extends BaseComponent implements OnInit {
  connections: Connection[] = [];

  carriers = ['ghn'];

  constructor(
    private connectionApi: ConnectionApi
  ) {
    super();
  }

  async ngOnInit() {
    this.getConnections();
  }

  async getConnections() {
    try {
      this.connections = [];
      const available_connections: Connection[] = await this.connectionApi.getAvailableConnections();
      const shop_connections: ShopConnection[] = await this.connectionApi.getShopConnections();

      available_connections.forEach(conn => {
        const connected = shop_connections.find(s_conn => s_conn.connection_id == conn.id);
        conn.connect_status = connected ? 'P' : 'Z';
        conn.status_display = STATUS[conn.connect_status];
        conn.connection_email = connected && connected.external_data && connected.external_data.email;
      });
      this.carriers.forEach(c => {
        available_connections.forEach(conn => {
          if (conn.connection_provider == c) {
            this.connections.push(conn);
          }
        });
      });

    } catch(e) {
      debug.error('ERROR in Getting Connections', e);
    }
  }

}
