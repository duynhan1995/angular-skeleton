import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { AuthenticateStore } from '@etop/core';
import { UserApi } from '@etop/api';
import { ShopCommonUsecase } from 'apps/faboshop/src/app/usecases/shop-common.usecase.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-update-user-info',
  templateUrl: './update-user-info-modal.component.html',
  styleUrls: ['./update-user-info-modal.component.scss']
})
export class UpdateUserInfoModalComponent implements OnInit {
  @Input() info_to_update: 'email' | 'phone';

  step = 1;
  authentication_method: 'email' | 'phone';

  new_email = '';
  new_phone = '';

  first_code = '';
  second_code = '';

  countdown = 60;
  loading = false;

  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private commonUsecase: ShopCommonUsecase,
    private modalAction: ModalAction,
    private userApi: UserApi,
  ) { }

  get title() {
    switch (this.step) {
      case 1:
        return 'Chọn phương thức';
      case 2:
        switch (this.info_to_update) {
          case 'email':
            return 'Cập nhật email';
          case 'phone':
            return 'Cập nhật số điện thoại';
          default:
            return '';
        }
      case 3:
        switch (this.info_to_update) {
          case 'email':
            return 'Xác thực email mới';
          case 'phone':
            return 'Xác thực số điện thoại mới';
          default:
            return '';
        }
      default:
        return 'Chọn phương thức';
    }
  }

  get user() {
    return this.auth.snapshot.user;
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  countDown() {
    this.countdown = 60;
    let interval = setInterval(() => {
      if (this.countdown < 1) {
        clearInterval(interval);
      } else {
        this.countdown -= 1;
      }
    }, 1000);
  }

  async resendFirstCode() {
    this.countDown();
    this.sendFirstCode();
  }

  async resendSecondCode() {
    this.countDown();
    this.sendSecondCode();
  }

  async sendFirstCode() {
    try {
      const { authentication_method } = this;
      const body = {
        authentication_method
      };

      switch (this.info_to_update) {
        case 'email':
          await this.userApi.updateUserEmail(body);
          break;
        case 'phone':
          await this.userApi.updateUserPhone(body);
          break;
        default:
          return this.loading = false;
      }
    } catch(e) {
      throw e;
    }
  }

  async sendSecondCode() {
    try {
      const { authentication_method, new_phone, new_email, first_code } = this;
      const body = {
        authentication_method,
        first_code
      };

      switch (this.info_to_update) {
        case 'email':
          await this.userApi.updateUserEmail({ ...body, email: new_email});
          break;
        case 'phone':
          await this.userApi.updateUserPhone({ ...body, phone: new_phone});
          break;
        default:
          return this.loading = false;
      }
    } catch(e) {
      throw e;
    }
  }

  async stepTwo() {
    this.loading = true;
    try {
      if (!this.authentication_method) {
        toastr.error('Vui lòng chọn phương thức xác thực!');
        return this.loading = false;
      }

      await this.sendFirstCode();

      this.step = 2;
      this.countDown();
    } catch(e) {
      debug.error('ERROR in stepTwo', e);
      let err_mes = 'Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!';
      if (e.code) {
        err_mes = e.msg || e.message;
      }
      toastr.error(err_mes);
    }
    this.loading = false;
  }

  async stepThree() {
    this.loading = true;
    try {
      if (this.info_to_update == 'email') {
        if (!this.new_email || !this.new_email.trim()) {
          toastr.error('Vui lòng nhập email mới!');
          return this.loading = false;
        }
        let email = this.new_email.split(/-[0-9a-zA-Z]+-test/)[0] || '';
        email = email && email.split('-test')[0] || '';
        if (!this.util.validEmail(email)) {
          return this.loading = false;
        }
      }
      if (this.info_to_update == 'phone') {
        if (!this.new_phone || !this.new_phone.trim()) {
          toastr.error('Vui lòng nhập số điện thoại mới!');
          return this.loading = false;
        }
        let phone = this.new_phone.split(/-[0-9a-zA-Z]+-test/)[0] || '';
        phone = phone && phone.split('-test')[0] || '';
        if (!this.util.validatePhoneNumber(phone)) {
          return this.loading = false;
        }
      }
      if (!this.first_code) {
        toastr.error('Vui lòng nhập mã xác thực!');
        return this.loading = false;
      }

      await this.sendSecondCode();

      this.step = 3;
      this.countDown();
    } catch(e) {
      debug.error('ERROR in stepThree', e);
      let err_mes = 'Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!';
      if (e.code) {
        err_mes = e.msg || e.message;
      }
      toastr.error(err_mes);
    }
    this.loading = false;
  }

  async confirm() {
    this.loading = true;
    try {
      if (!this.second_code) {
        toastr.error('Vui lòng nhập mã xác thực!');
        return this.loading = false;
      }

      const { authentication_method, new_phone, new_email, first_code, second_code } = this;
      const body = {
        authentication_method,
        first_code,
        second_code
      };

      switch (this.info_to_update) {
        case 'email':
          await this.userApi.updateUserEmail({ ...body, email: new_email});
          break;
        case 'phone':
          await this.userApi.updateUserPhone({ ...body, phone: new_phone});
          break;
        default:
          return this.loading = false;
      }

      toastr.success(`Cập nhật ${this.info_to_update == 'email' ? 'email' : 'số điện thoại'} thành công.`);
      this.modalAction.dismiss(null);
      this.commonUsecase.checkAuthorization(true);

    } catch(e) {
      debug.error('ERROR in stepThree', e);
      let err_mes = 'Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!';
      if (e.code) {
        err_mes = e.msg || e.message;
      }
      toastr.error(err_mes);
    }
    this.loading = false;
  }

}
