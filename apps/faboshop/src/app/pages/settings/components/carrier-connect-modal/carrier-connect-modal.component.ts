import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Connection } from 'libs/models/Connection';
import { ConnectionAPI, ConnectionApi } from '@etop/api';
import { BaseComponent } from '@etop/core';
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";

enum Step {
  carrierLogin = 'carrierLogin',
  carrierOTP = 'carrierOTP'
}

@Component({
  selector: 'shop-carrier-connect-modal',
  templateUrl: './carrier-connect-modal.component.html',
  styleUrls: ['./carrier-connect-modal.component.scss']
})
export class CarrierConnectModalComponent extends BaseComponent
  implements OnInit {
  @Input() connection: Connection;

  tab: 'login' | 'register' = 'login';

  infomationConnectionUrl: string;
  policyConnectionUrl: string;
  login_info = new ConnectionAPI.LoginShopConnectionRequest();
  register_info = new ConnectionAPI.RegisterShopConnectionRequest();
  loginOTP = new ConnectionAPI.LoginShopConnectionWithOTPRequest();

  provinces: any[] = [];
  districts: any[] = [];

  selected_province_code: string = null;
  selected_district_code: string = null;

  loading = false;
  loadingOTP = false;
  step = 'carrierLogin';
  countDown = 600;
  timer;


  // tslint:disable-next-line: member-ordering
  private static validateLoginInfo(data) {
    if (!data.identifier) {
      toastr.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    return true;
  }

  private static validateRegisterInfo(data, provider: string) {
    if (!data.name) {
      toastr.error('Vui lòng nhập họ tên!');
      return false;
    }
    if (!data.phone) {
      toastr.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    if (!data.email) {
      toastr.error('Vui lòng nhập số email!');
      return false;
    }
    if (!data.password && provider == 'ghn') {
      toastr.error('Vui lòng nhập mật khẩu!');
      return false;
    }
    if (!data.province) {
      toastr.error('Vui lòng nhập tỉnh thành!');
      return false;
    }
    if (!data.district) {
      toastr.error('Vui lòng nhập quận huyện!');
      return false;
    }
    if (!data.address) {
      toastr.error('Vui lòng nhập địa chỉ!');
      return false;
    }
    return true;
  }


  constructor(
    private modalAction: ModalAction,
    private connectionApi: ConnectionApi,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {
    super();
  }

  get loginStep() {
    return this.step == Step.carrierLogin;
  }

  get otpStep() {
    return this.step == Step.carrierOTP;
  }

  displayMap() {
    return option => option && option.name;
  }

  valueMap() {
    return option => option && option.code;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  ngOnInit() {
    this.prepareConnectionUrl();
    this.prepareLocationData();
  }

  prepareConnectionUrl() {
    if (this.connection.connection_provider === 'ghn') {
      this.infomationConnectionUrl = 'https://ghn.vn/';
      this.policyConnectionUrl = 'https://ghn.vn/pages/dieu-khoan-su-dung';
    } else if (this.connection.connection_provider === 'ghtk') {
      this.infomationConnectionUrl = 'https://giaohangtietkiem.vn/';
      this.policyConnectionUrl =
        'https://giaohangtietkiem.vn/dich-vu-giao-hang-ghtk/';
    }
  }
  private prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  provinceSelected() {
    try {
      const province = this.provinces.find(
        pr => pr.code == this.selected_province_code
      );
      if (province && province.name != this.register_info.province) {
        this.register_info.province = province.name;
        this.selected_district_code = null;
      }

      this.districts = this.locationService.filterDistrictsByProvince(this.selected_province_code);
    } catch(e) {
      debug.error(e);
    }
  }

  districtSelected() {
    const district = this.districts.find(
      dt => dt.code == this.selected_district_code
    );
    this.register_info.district = district && district.name;
  }

  async loginShopConnection() {
    try {
      if (!this.loginOTP.identifier) {
        return toastr.error('Vui lòng nhập số điện thoại.');
      }
      this.loading = true;
      const body: ConnectionAPI.LoginShopConnectionWithOTPRequest = {
        ...this.loginOTP,
        connection_id: this.connection.id
      };
      this.loginOTP.connection_id = this.connection.id;
      await this.connectionApi.loginShopConnectionWithOTP(body);

      toastr.success(`Kết nối với ${this.connection.name} thành công.`);
      this.loading = false;
      this.modalAction.dismiss({ connected: true, connection: this.loginOTP });
    } catch (e) {
      this.loading = false;
      toastr.error(e.message);
    }
  }

  back() {
    this.step = Step.carrierLogin;
  }
  async continue() {
    this.loadingOTP = true;
    try {
      const valid = CarrierConnectModalComponent.validateLoginInfo(
        this.login_info
      );
      if (!valid) {
        return (this.loadingOTP = false);
      }
      const body: ConnectionAPI.LoginShopConnectionRequest = {
        ...this.login_info,
        connection_id: this.connection.id
      };
      await this.connectionApi.loginShopConnection(body);
      this.loginOTP.identifier = this.login_info.identifier;
      this.loadingOTP = false;
      this.countDown = 600;
      this.startTimer();
      this.step = Step.carrierOTP;
    } catch (e) {
      this.loadingOTP = false;
      debug.error('ERROR in carrier connecting', e);
      toastr.error(
        `Kết nối với ${this.connection.name} không thành công.`,
        e.code ? e.message || e.msg : ''
      );
    }
  }

  async retrySendOtp() {
    const body: ConnectionAPI.LoginShopConnectionRequest = {
      ...this.login_info,
      connection_id: this.connection.id,
      identifier: this.loginOTP.identifier
    };
    await this.connectionApi.loginShopConnection(body);
    this.countDown = 600;
    this.startTimer();
  }

  startTimer() {
    const interval = setInterval(() => {
      if (this.countDown < 1) {
        clearInterval(interval);
      } else {
        this.timer = this.displayCount(this.countDown);
        this.countDown -= 1;
      }
    }, 1000);
  }

  displayCount(timer) {
    const minutes = Math.floor(timer / 60);
    const seconds = Math.floor(timer % 60);
    let _seconds: any = seconds;
    if (seconds < 10) {
      _seconds = '0' + seconds;
    }
    return minutes + ':' + _seconds + 's';
  }

  async registerShopConnection() {
    this.loading = true;
    try {
      const valid = CarrierConnectModalComponent.validateRegisterInfo(
        this.register_info,
        this.connection.connection_provider
      );
      if (!valid) {
        return (this.loading = false);
      }
      const body: ConnectionAPI.RegisterShopConnectionRequest = {
        ...this.register_info,
        connection_id: this.connection.id
      };
      await this.connectionApi.registerShopConnection(body);
      toastr.success(`Kết nối với ${this.connection.name_display} thành công.`);
      this.modalAction.dismiss(true);
    } catch (e) {
      debug.error('ERROR in carrier connecting', e);
      toastr.error(
        `Kết nối với ${this.connection.name_display} không thành công.`,
        e.code ? e.message || e.msg : ''
      );
    }
    this.loading = false;
  }

  switchTab(tab) {
    this.tab = tab;
  }
}
