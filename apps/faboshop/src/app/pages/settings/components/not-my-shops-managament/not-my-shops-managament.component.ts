import { Component, Input, OnInit } from '@angular/core';
import { AuthorizationApi } from '@etop/api';
import { Account } from 'libs/models/Account';
import { Invitation } from 'libs/models/Authorization';
import { FilterOperator } from '@etop/models';

@Component({
  selector: 'shop-not-my-shops-managament',
  templateUrl: './not-my-shops-managament.component.html',
  styleUrls: ['./not-my-shops-managament.component.scss']
})
export class NotMyShopsManagamentComponent implements OnInit {
  @Input() accounts: Account[] = [];
  @Input() currentShop: any = {};

  userInvitations: Invitation[] = [];

  constructor(
    private authorizationApi: AuthorizationApi
  ) { }

  get noNotMyShopsFound() {
    return !this.accounts.some(a =>
      a.user_account && a.user_account.permission && a.user_account.permission.roles &&
      a.user_account.permission.roles.indexOf('owner') == -1
    );
  }

  async ngOnInit() {
    await this.getUserInvitations();
  }

  async getUserInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      this.userInvitations = await this.authorizationApi.getUserInvitations({
        filters: [
          {
            name: "status",
            op: FilterOperator.eq,
            value: "Z"
          },
          {
            name: "expires_at",
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
    } catch(e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }

  notMyShop(roles: string[]) {
    if (roles && roles.length) {
      return roles.indexOf('owner') == -1;
    }
    return false;
  }

}
