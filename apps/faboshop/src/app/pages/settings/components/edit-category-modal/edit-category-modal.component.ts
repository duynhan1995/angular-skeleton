import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shop-edit-category-modal',
  templateUrl: './edit-category-modal.component.html',
  styleUrls: ['./edit-category-modal.component.scss']
})
export class EditCategoryModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
