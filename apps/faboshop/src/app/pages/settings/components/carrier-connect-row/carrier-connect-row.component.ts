import { Component, Input, OnInit } from '@angular/core';
import {Connection, STATUS} from 'libs/models/Connection';
import { CarrierConnectModalComponent } from 'apps/faboshop/src/app/pages/settings/components/carrier-connect-modal/carrier-connect-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ConnectionApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: '[shop-carrier-connect-row]',
  templateUrl: './carrier-connect-row.component.html',
  styleUrls: ['./carrier-connect-row.component.scss']
})
export class CarrierConnectRowComponent implements OnInit {
  @Input() connection: Connection;

  dropdownActions = [];

  constructor(
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private dialogController: DialogControllerService,
    private connectionApi: ConnectionApi
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Gỡ kết nối',
        cssClass: 'text-danger',
        onClick: () => this.deleteShopConnection()
      }
    ];
  }

  checkPermission(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }

  carrierConnect(connection: Connection) {
    const modal = this.modalController.create({
      component: CarrierConnectModalComponent,
      componentProps: {
        title: 'Kết nối nhà vận chuyển',
        connection
      },
    });
    modal.show().then();
    modal.onDismiss().then(data => {
      this.connection.connect_status = data.connected ? 'P' : 'Z';
      this.connection.status_display = STATUS[this.connection.connect_status];
      this.connection.connection_email = data.connection.identifier;
    });
  }

  deleteShopConnection() {
    const dialog = this.dialogController.createConfirmDialog({
      title: `Gỡ kết nối`,
      body: `
        <div>Bạn có chắc muốn gỡ kết nối với nhà vận chuyển <strong>${this.connection.name}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.connectionApi.deleteShopConnection(this.connection.id);
          toastr.success(`Gỡ kết nối với nhà vận chuyển ${this.connection.name} thành công.`);
          dialog.close().then();
          this.connection.connect_status = 'Z';
          this.connection.status_display = STATUS['Z'];
          this.connection.connection_email = '';
        } catch (e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Gỡ kết nối với nhà vận chuyển ${this.connection.name} không thành công.`, e.message || e.msg);
        }
      }
    });
    dialog.show().then();
  }

}
