import { Component, Input, OnInit } from '@angular/core';
import { Address } from 'libs/models/Address';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { BaseComponent } from '@etop/core';
import {LocationQuery} from "@etop/state/location/location.query";
import {LocationService} from "@etop/state/location";
import { ShopSettings } from '@etop/models/ShopSettings';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';

@Component({
  selector: 'shop-return-address-modal',
  templateUrl: './return-address-modal.component.html',
  styleUrls: ['./return-address-modal.component.scss']
})
export class ReturnAddressModalComponent extends BaseComponent implements OnInit {
  @Input() address: Address;
  @Input() action: string;
  @Input() settings: ShopSettings;

  provinces: any[] = [];
  districts: any[] = [];
  wards: any[] = [];

  selectedProvinceCode: string = null;
  selectedDistrictCode: string = null;
  selectedWardCode: string = null;

  actionType;

  loading = false;

  constructor(
    private shopSettingsService: ShopSettingsService,
    private modalDismiss: ModalAction,
    private utilService: UtilService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {
    super();
  }

  displayMap() {
    return option => option && option.name || null;
  }

  valueMap() {
    return option => option && option.code || null;
  }

  ngOnInit() {
    if (this.address.district_code) {
      this.actionType = 'update'
    }
    else {
      this.actionType = 'save'
    }

    this.prepareLocationData();
  }

  closeModal() {
    this.modalDismiss.close(false);
  }

  private prepareLocationData() {
    const _provinces = this.locationQuery.getValue().provincesList;
    const _districts = this.locationQuery.getValue().districtsList;
    const _wards = this.locationQuery.getValue().wardsList;

    if (!_provinces || !_provinces.length) {
      return;
    }
    if (!_districts || !_districts.length) {
      return;
    }
    if (!_wards || !_wards.length) {
      return;
    }

    this.provinces = _provinces;
    this.districts = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.wards = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.selectedProvinceCode = this.address.province_code;
    this.selectedDistrictCode = this.address.district_code;
    this.selectedWardCode = this.address.ward_code;

  }

  onProvinceSelected() {
    this.selectedDistrictCode = null;
    this.selectedWardCode = '';
    if (!this.selectedProvinceCode) {
      this.districts = [];
      this.wards = [];
      return;
    }
    this.districts = this.locationService.filterDistrictsByProvince(this.selectedProvinceCode);
  }

  onDistrictSelected() {
    this.selectedWardCode = null;
    if (!this.selectedDistrictCode) {
      this.wards = [];
      return;
    }
    this.wards = this.locationService.filterWardsByDistrict(this.selectedDistrictCode);
  }

  validate(data: Address) {
    if (!data.province_code) {
      toastr.error('Vui lòng chọn tỉnh thành!');
      return false;
    }

    if (!data.district_code) {
      toastr.error('Vui lòng chọn quận huyện!');
      return false;
    }

    if (!data.ward_code) {
      toastr.error('Vui lòng chọn phường xã!');
      return false;
    }

    if (!data.address1) {
      toastr.error('Vui lòng nhập địa chỉ!');
      return false;
    }

    return true;
  }

  async save() {
    this.loading = true;
    try {
      const valid_phone = this.utilService.validatePhoneNumber(this.address.phone);
      if (!valid_phone) {
        return this.loading = false;
      }

      this.address = this.utilService.trimFields(this.address, ['address1', 'full_name', 'phone']);
      const address: Address = {
        ...this.address,
        province_code: this.selectedProvinceCode,
        district_code: this.selectedDistrictCode,
        ward_code: this.selectedWardCode,
      };
      const settings: ShopSettings = {
        ...this.settings,
        return_address: address
      }
      const valid_address = this.validate(address);
      if (!valid_address) {
        return this.loading = false;
      }

      let res;
      res = await this.shopSettingsService.updateSetting(settings);

      toastr.success(`${this.actionType == 'update' ? 'Cập nhật' : 'Tạo'} địa chỉ thành công!`);
      this.modalDismiss.dismiss({
        settings: res
      });
    } catch (e) {
      debug.log('ERROR in Saving Address', e);
      toastr.error(
        `${this.actionType == 'update' ? 'Cập nhật' : 'Tạo'} địa chỉ không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }
}
