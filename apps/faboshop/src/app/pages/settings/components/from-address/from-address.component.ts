import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, OnChanges } from '@angular/core';
import { Address } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { ExtendedAccount } from 'libs/models/Account';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { AuthenticateStore } from '@etop/core';
import { FromAddressModalComponent } from 'apps/faboshop/src/app/pages/settings/components/from-address-modal/from-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';

@Component({
  selector: 'shop-from-address',
  templateUrl: './from-address.component.html',
  styleUrls: ['./from-address.component.scss']
})
export class FromAddressComponent implements OnInit, OnChanges {
  @Output() addressRemoved = new EventEmitter();
  @Input() isDefaultFromAddress = false;
  @Input() address: Address;
  @Input() currentShop: ExtendedAccount;

  dropdownActions = [];

  constructor(
    private addressService: AddressService,
    private shopService: ShopService,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit() {
   
  }

  ngOnChanges() {
    this.dropdownActions = [
      {
        onClick: () => this.editFromAddress(this.address),
        title: 'Chỉnh sửa',
        permissions: ['shop/settings/shipping_setting:update'],
      },
      {
        onClick: () => this.onRemoveAddress(this.address),
        title: 'Xóa địa chỉ',
        cssClass: 'text-danger',
        hidden: this.isDefaultFromAddress
      }
    ];
    this.ref.detectChanges();
  }
  
  formatAddress(address) {
    return this.addressService.joinAddress(address);
  }

  async setDefaultAddress(address: Address, type) {
    try {
      await this.shopService.setDefaultAddress(address.id, type);
      await this.auth.setDefaultAddress(address.id, type);

      //TODO update shop info trigger
      this.currentShop = this.auth.snapshot.shop;

      toastr.success('Cập nhật địa chỉ mặc định thành công!');
    } catch (e) {
      toastr.error('Chọn địa chỉ thất bại: ' + e.message);
    }
  }

  editFromAddress(address: Address) {
    let modal = this.modalController.create({
      component: FromAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: address,
        type: 'shipfrom',
        title: 'Cập nhật địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({address: addr}) => {
      this.address = addr
    });
  }

  onRemoveAddress(address: Address) {
    const dialog = this.dialogController.createConfirmDialog({
        title: 'Xóa địa chỉ',
        body: `
        <p>Bạn có chắc muốn xóa địa chỉ này?</p>
        <strong>${this.formatAddress(address)}</strong>
        `,
        closeAfterAction: false,
        onConfirm: async () => {
          await this.removeAddress(address);
          dialog.close();
        }
      });

    dialog.show().then();
  }

  async removeAddress(address: Address) {
    try {
      await this.addressService.removeAddress(address.id);
      this.addressRemoved.emit(address.id);
      toastr.success('Xóa địa chỉ thành công!');
    } catch (e) {
      toastr.error('Xóa địa chỉ thất bại: ' + e.message);
    }
  }


}
