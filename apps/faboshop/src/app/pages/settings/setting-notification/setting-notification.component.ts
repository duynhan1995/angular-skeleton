import { Component, OnInit } from '@angular/core';
import { NotificationApi, Notification } from '@etop/api/fabo-api/notification.api';
const mapTopics = [
  {
    title:'Bình luận',
    topic: 'fb_external_comment',
    description: 'Nhận thông báo khi có tin nhắn Comment mới từ các Page mà shop đang quản lý'
  },
  {
    title:'Tin nhắn',
    topic: 'fb_external_message',
    description: 'Nhận thông báo khi có tin nhắn Inbox mới từ các Page mà shop đang quản lý'
  },
  {
    title:'Đơn hàng',
    topic: 'fulfillment',
    description: 'Nhận thông báo khi có cập nhật mới về trạng thái, thông tin đơn hàng'
  },
  {
    title:'Hệ thống',
    topic: 'system',
    description: 'Nhận thông báo chung từ Faboshop về chính sách, tính năng, chương trình khuyến mãi, ...'
  }
]
@Component({
  selector: 'faboshop-notification',
  templateUrl: './setting-notification.component.html',
  styleUrls: ['./setting-notification.component.scss']
})
export class SettingNotificationComponent implements OnInit {

  notify_topics:Notification[];


  constructor(
    private notificationApi:NotificationApi,
  ) { }

  mapData(topics: Notification[]) {
    return topics.map(topic => this.mapTopic(topic));
  }

  mapTopic(topic: Notification) {
    return {
      ...topic,
      title: this.getTopic(topic).title,
      description: this.getTopic(topic).description
    };
  }

  getTopic(topic): any {
    return mapTopics.find(t => t.topic == topic.topic) || '';
  }

  async ngOnInit() {
    const noti = await this.notificationApi.getNotifySetting();
    this.notify_topics = this.mapData(noti.topics)
  }

  async changeEnable(notification:Notification){
    toastr.remove()
    try{
      if(notification.enable){
        await this.disable(notification.topic)
      }else{
        await this.enable(notification.topic)
      }
      toastr.remove()
      const noti = await this.notificationApi.getNotifySetting();
      this.notify_topics = this.mapData(noti.topics)
      toastr.remove()
      toastr.success('Thay đổi thành công')
    }catch (e) {
      toastr.error('Thay đổi không thành công',e)
    }

  }

  async enable(topic){
    await this.notificationApi.enableNotifyTopic(topic)
  }
  async disable(topic){
    await this.notificationApi.disableNotifyTopic(topic)
  }

}
