import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
  token = '';
  loading = false;
  error = false;
  errorMsg = '';
  phoneVerified = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private auth: AuthenticateStore,
    private util: UtilService
  ) { }

  async ngOnInit() {
    this.token = this.activatedRoute.snapshot.queryParamMap.get('t');

    if (this.token) {
      this.loading = true;
      try {
        if (this.auth) {
          await this.userService.verifyEmailUsingToken(this.token);
          let user = this.auth.snapshot.user;
          let d = new Date();
          user.email_verified_at = d.toISOString();
          this.auth.updateUser(user);
        }
      } catch (e) {
        this.error = true;
        this.errorMsg = e.message;
      }
      this.loading = false;
    }
  }

  gotoDashboard() {
    let slug = this.util.getSlug();
    location.href = `/s/${slug}/orders`;
  }
}
