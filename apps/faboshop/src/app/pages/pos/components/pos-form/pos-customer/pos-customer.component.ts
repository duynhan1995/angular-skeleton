import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Order } from 'libs/models/Order';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { PosCreateCustomerModalComponent } from '../../pos-create-customer-modal/pos-create-customer-modal.component';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { CustomerService } from '../../../../../../services/customer.service';

@Component({
  selector: 'shop-pos-customer',
  templateUrl: './pos-customer.component.html',
  styleUrls: ['./pos-customer.component.scss']
})
export class PosCustomerComponent extends BaseComponent implements OnInit {
  @Output() updateOrder = new EventEmitter();
  @Input() order = new Order({});

  customerIndex: number;
  customers = [];
  provinces = [];
  districts = [];
  wards = [];
  showCreateCustomer = true;

  constructor(
    private customerService: CustomerService,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private modalController: ModalController,
    private authStore: AuthenticateStore,
    private customerStore: CustomerStoreService
  ) {
    super();
  }

  async ngOnInit() {
    this.showCreateCustomer = this.authStore.snapshot.permission.permissions.includes('shop/customer:create');
    await this.getCustomers();

    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        await this.getCustomers();
      });
    this.orderService.createOrderFailed$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async () => {
        await this.getCustomers();
        if (this.order.customer_id) {
          this.customerIndex = this.customers.map(c => c.id).indexOf(this.order.customer_id);
          this.selectCustomer();
        }
      });

    this.ffmService.onResetData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.customerIndex = null;
      });

    this.customerStore.selectCustomer$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        const customer = this.customerStore.snapshot.selected_customer;
        if (customer) {
          await this.getCustomers();
          this.customerIndex = this.customers.map(c => c.id).indexOf(this.order.customer_id);
        }
      });
  }

  async getCustomers() {
    try {
      this.customers = await this.customerService.getCustomers();
    } catch (e) {
      this.customers = [];
    }
  }

  showAddCustomerModal() {
    let modal = this.modalController.create({
      component: PosCreateCustomerModalComponent,
      showBackdrop:'static'
    });
    modal.show().then();
    modal.onDismiss().then(async(customer) => {
      modal = null;
      if (customer && customer.id) {
        await this.getCustomers();
        this.customerIndex = this.customers.findIndex(c => c.id == customer.id);
        this.selectCustomer();
      }
    });
  }

  async selectCustomer() {
    if (this.customers[this.customerIndex]) {
      const customer = this.customers[this.customerIndex];
      customer.addresses = await this.customerService.getCustomerAddresses(customer.id);
      this.customerStore.selectCustomer(customer);
      this.customerStore.selectCustomerAddress(customer.addresses[0]);
    }
  }

  initValue(e) {
    this.order.customer.full_name = e;
  }
}
