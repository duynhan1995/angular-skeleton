import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { takeUntil } from 'rxjs/operators';
import { Order } from 'libs/models/Order';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { PrintType } from 'apps/faboshop/src/app/pages/orders/sell-orders/sell-orders-controller.service';

@Component({
  selector: 'shop-pos-form',
  templateUrl: './pos-form.component.html',
  styleUrls: ['./pos-form.component.scss']
})
export class PosFormComponent extends BaseComponent implements OnInit {
  @Output() updateOrder = new EventEmitter();
  @Output() createOrder = new EventEmitter<boolean>();

  @Input() order: Order;
  loading = false;

  print_types: {
    display: string;
    type: PrintType;
  }[] = [
    {
      display: 'Mẫu hoá đơn',
      type: PrintType.orders,
    },
    {
      display: 'Mẫu giao hàng',
      type: PrintType.fulfillments,
    },
    {
      display: 'Mẫu hoá đơn và giao hàng',
      type: PrintType.o_and_f,
    }
  ];
  print_type: {
    display: string;
    type: PrintType
  } = {
    display: 'Mẫu hoá đơn',
    type: PrintType.orders
  };

  orderToggle = true;
  createAndConfirm = false;

  constructor(
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private posStore: PosStoreService
  ) {
    super();
  }

  get to_create_ffm() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  get to_print() {
    return this.posStore.snapshot.to_print;
  }

  isDisabledPrintType(type) {
    if (type == PrintType.orders) {
      return false;
    }
    return !this.ffmStore.snapshot.to_create_ffm;
  }

  ngOnInit() {
    this.orderStore.creatingOrder$
      .pipe(takeUntil(this.destroy$))
      .subscribe(loading => this.loading = loading);
    this.ffmStore.toCreateFfmToggled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(to_create_ffm => {
        if (!to_create_ffm) {
          this.print_type = this.print_types[0];
          this.selectPrintType(this.print_type);
        } else if (localStorage.getItem('print_type')) {
          const print_type_local = localStorage.getItem('print_type');
          this.print_type = this.print_types.find(pt => pt.type == print_type_local);
          this.selectPrintType(this.print_type);
        }
      });
  }

  updateNewOrder() {
    this.updateOrder.emit();
  }

  onToggle(e) {
    this.orderToggle = e;
  }

  create(createAndConfirm: boolean) {
    this.createAndConfirm = createAndConfirm;
    this.createOrder.emit(createAndConfirm);
  }

  createOrderWithFFM() {
    this.createOrder.emit(true);
  }

  toggleSwitch() {
    this.ffmStore.createFulfillment();
  }

  togglePrint() {
    this.posStore.toPrint();
  }

  selectPrintType(print_type) {
    if (this.isDisabledPrintType(print_type.type)) {
      return;
    }
    this.print_type = print_type;
    this.posStore.changePrintType(print_type.type, this.to_create_ffm);
  }

}
