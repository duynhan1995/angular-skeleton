import { OnInit, Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-pos-header',
  templateUrl: './pos-header.component.html',
  styleUrls: ['./pos-header.component.scss']
})
export class PosHeaderComponent implements OnInit {
  constructor(
    @Inject(DOCUMENT) private document: any,
    private util: UtilService,
    private router: Router,
    private auth: AuthenticateStore
  ) {}
  elem;
  fullScreen = false;
  user;
  ngOnInit() {
    this.elem = document.documentElement;
    this.user = this.auth.snapshot.user;
  }

  toggleFullscreen() {
    if (!this.fullScreen) {
      if (this.elem.requestFullscreen) {
        this.elem.requestFullscreen();
      } else if (this.elem.mozRequestFullScreen) {
        this.elem.mozRequestFullScreen();
      } else if (this.elem.webkitRequestFullscreen) {
        this.elem.webkitRequestFullscreen();
      } else if (this.elem.msRequestFullscreen) {
        this.elem.msRequestFullscreen();
      }
      this.fullScreen = true;
    } else {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        this.document.msExitFullscreen();
      }
      this.fullScreen = false;
    }
  }

  gotoOrders() {
    let slug = this.util.getSlug();
    this.router.navigateByUrl(`/s/${slug}/orders`);
  }
}
