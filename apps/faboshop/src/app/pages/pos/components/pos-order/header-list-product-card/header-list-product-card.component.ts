import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'shop-header-list-product-card',
  templateUrl: './header-list-product-card.component.html',
  styleUrls: ['./header-list-product-card.component.scss']
})
export class HeaderListProductCardComponent implements OnInit {
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  @Output() next = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() searchOption = new EventEmitter();
  @Output() searchSKU = new EventEmitter<string>();
  @Input() page_total;

  keyword = '';
  barcode = false;
  page_numb = 1;

  @HostListener('document:click', ['$event'])
  public clickOutsideComponent(event) {
    if (event.target.id != 'search-input') {
      if (event.target.className.indexOf('barcode-click') != -1 && !this.barcode) {
        this.barcodeOption();
      } else {
        this.onOutFocus();
      }
    }
  }

  constructor() {}

  ngOnInit() {}

  nextbtn() {
    if (this.page_numb == this.page_total) {
      return;
    }
    this.page_numb++;
    this.next.emit(this.page_numb);
  }

  backbtn() {
    if (this.page_numb == 1) {
      return;
    }
    this.page_numb--;
    this.next.emit(this.page_numb);
  }

  search() {
    this.searchOption.emit(this.keyword);
  }

  searchVariant() {
    if (!this.barcode) { return; }
    this.searchSKU.emit(this.keyword);
    this.keyword = '';
  }

  onOutFocus() {
    this.barcode = false;
  }

  barcodeOption() {
    this.barcode = !this.barcode;
    this.searchOption.emit('');
    this.searchInput.nativeElement.focus();
  }
}
