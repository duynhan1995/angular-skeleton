import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderLineComponent } from './order-line.component';
import { OrderLineEditableComponent } from './components/order-line-editable/order-line-editable.component';
import { OrderLineUneditableComponent } from './components/order-line-uneditable/order-line-uneditable.component';
import { SharedModule } from '../../../../../../../../shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticateModule } from '@etop/core';
import { EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    OrderLineComponent,
    OrderLineEditableComponent,
    OrderLineUneditableComponent
  ],
  exports: [
    OrderLineComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    AuthenticateModule,
    EtopPipesModule,
  ]
})
export class OrderLineModule { }
