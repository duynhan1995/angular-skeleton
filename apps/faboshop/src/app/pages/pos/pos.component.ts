import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';

@Component({
  selector: 'shop-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosComponent implements OnInit, OnDestroy {
  allowView = true;
  constructor(
    private authStore: AuthenticateStore,
    private ffmStore: FulfillmentStore,
    private customerStore: CustomerStoreService
  ) {}

  ngOnInit() {
    this.ffmStore.resetState();
    this.allowView = this.authStore.snapshot.permission.permissions.includes('shop/order:view');
  }

  ngOnDestroy() {
    this.customerStore.resetState();
  }

}
