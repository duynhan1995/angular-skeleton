import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map, filter, take } from 'rxjs/operators';
import { CustomerConversation } from 'libs/models/faboshop/CustomerConversation';
import { FbPages, FbPage } from 'libs/models/faboshop/FbPage';
import { AsyncSubject } from 'rxjs';
import { StorageService } from 'apps/core/src/services/storage.service';

export interface ConversationData {
  conversations: CustomerConversation[];
  selectedConversation: CustomerConversation;
  pages: FbPages;
  currentPage: FbPage;
  storageReady: boolean;
  uptodate?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ConversationStoreService extends AStore<ConversationData> {
  __storage_key = 'conversation-data';

  readonly selectedConversationChanged$ = this.state$.pipe(
    map(({ selectedConversation }) => selectedConversation),
    distinctUntilChanged()
  );

  readonly conversationsUpdated$ = this.state$.pipe(
    map(({ conversations }) => conversations),
    distinctUntilChanged()
  );

  readonly currentPageUpdated$ = this.state$.pipe(
    map(({ currentPage }) => currentPage),
    distinctUntilChanged()
  );

  constructor(private storage: StorageService) {
    super();
    storage.onLocalStorageUpdated.subscribe(({ data, loaded }) => {
      if (loaded) {
        this.load();
      }
    });

    if (storage.ready) {
      this.load();
    }
    this.state$.subscribe(() => {
      this.store();
    });
    this.state$
      .pipe(
        map(({ uptodate }) => uptodate),
        distinctUntilChanged(),
        filter(uptodate => uptodate),
        take(1)
      )
      .subscribe(() => {
        this._ready$.next(true);
        this._ready$.complete();
      });
  }

  private _ready$ = new AsyncSubject();

  changeSelectedConversation(selectedConversation: CustomerConversation) {
    this.setState({ selectedConversation });
  }

  updateConversation(conversations: CustomerConversation[]) {
    this.setState({ conversations });
  }

  updateListFbPages(pages: FbPages) {
    this.setState({ pages });
  }

  updateCurentFbPage(currentPage: FbPage) {
    this.setState({ currentPage });
  }

  load() {
    let conversationData = this.storage.get(this.__storage_key);
    this.state = {
      ...conversationData,
      storageReady: true
    };
  }

  store() {
    let data = this.snapshot;
    delete data.storageReady;
    this.storage.set(this.__storage_key, data);
  }

  clear() {
    this.storage.clear();
    this.state = this.initState;
  }

  getFanpageById(id: string) {
    if (!id) {
      return null;
    }
    return this.snapshot.pages.find(p => p.id == id);
  }
}
