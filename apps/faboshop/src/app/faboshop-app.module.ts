import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { CoreModule } from 'apps/core/src/core.module';
import { SharedModule } from 'apps/shared/src/shared.module';

import { FaboshopAppComponent } from 'apps/faboshop/src/app/faboshop-app.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { ShopCommonUsecase } from './usecases/shop-common.usecase.service';
import { RegisterComponent } from './pages/register/register.component';
import { IframeService } from 'apps/core/src/services/iframe.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { TimerService } from 'apps/core/src/services/timer.service';
import { FulfillmentService } from '../services/fulfillment.service';
import { OrderService } from '../services/order.service';
import { ShopCrmService } from '../services/shop-crm.service';
import { ShopService } from '../services/shop.service';
import { AddressService } from 'apps/core/src/services/address.service';
import { MoneyTransactionService } from '../services/money-transaction.service';
import { MaterialModule } from '@etop/shared/components/etop-material/material';
import { StatisticService } from 'apps/faboshop/src/services/statistic.service';
import { CmsService } from '../services/cms.service';
import { ProductService } from 'apps/faboshop/src/services/product.service';
import { TicketModalComponent } from './components/modals/ticket-modal/ticket-modal.component';
import { NotiModalComponent } from './components/modals/noti-modal/noti-modal.component';
import { ForceButtonModule } from './components/force-button/force-button.module';
import { DeliveryModule } from './components/delivery/delivery.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { NotificationService } from '../services/notification.service';
import { ExportFulfillmentModalComponent } from './components/modals/export-fulfillment-modal/export-fulfillment-modal.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ExportOrderModalComponent } from './components/modals/export-order-modal/export-order-modal.component';
import { LoginModule } from 'apps/shared/src/pages/login/login.module';
import { AnnouncementModalComponent } from './components/modals/announcement-modal/announcement-modal.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { VerifyEmailComponent } from './pages/verify-email/verify-email.component';
import { VerifyPhoneComponent } from './pages/verify-phone/verify-phone.component';
import { ReceiptService } from '../services/receipt.service';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';
import {
  CategoryApi,
  CollectionApi,
  CustomerApi,
  FulfillmentApi,
  NotificationApi,
  OrderApi,
  ProductApi,
  ReceiptApi
} from '@etop/api';
import { LoginComponent } from './pages/login/login.component';
import { CreateShopComponent } from './pages/create-shop/create-shop.component';
import { FaboshopAppGuard } from 'apps/faboshop/src/app/faboshop-app.guard';
import { DesktopOnlyComponent } from './components/desktop-only/desktop-only.component';
import { RequireStokenModalComponent } from './components/modals/require-stoken-modal/require-stoken-modal.component';
import { FromAddressModalComponent } from 'apps/faboshop/src/app/pages/settings/components/from-address-modal/from-address-modal.component';
import { IsUserActiveGuard } from './is-user-active.guard';
import { CONFIG_TOKEN } from '@etop/core/services/config.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { EtopCommonModule, EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { ChatConnectionModalComponent } from './components/modals/chat-connection-modal/chat-connection-modal.component';
import { BaseModuleHmr } from '@etop/core/base/base-module-hmr';
import { ListAddressesPopupComponent } from 'apps/faboshop/src/app/components/modals/list-addresses-popup/list-addresses-popup.component';
import { CreateUpdateAddressPopupComponent } from './components/modals/create-update-address-popup/create-update-address-popup.component';
import { CreateCustomerPopupComponent } from './components/modals/create-customer-popup/create-customer-popup.component';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { ImageLoaderModule } from '@etop/features';

const apis = [
  OrderApi,
  FulfillmentApi,
  ProductApi,
  CollectionApi,
  NotificationApi,
  CategoryApi,
  ReceiptApi,
  CustomerApi
];
const services = [
  FulfillmentService,
  OrderService,
  StatisticService,
  UtilService,
  IframeService,
  TimerService,
  ShopCrmService,
  ShopService,
  AddressService,
  MoneyTransactionService,
  CmsService,
  ProductService,
  PromiseQueueService,
  NotificationService,
  ReceiptService,
  CustomerService
];

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsUserActiveGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [IsUserActiveGuard]
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'verify-email',
    component: VerifyEmailComponent
  },
  {
    path: 'i',
    loadChildren: () =>
      import('./pages/invitation/invitation.module').then(
        m => m.InvitationModule
      )
  },
  {
    path: 'verify-phone',
    component: VerifyPhoneComponent
  },
  {
    path: 'notifications',
    redirectTo: 's/-1/notify'
  },
  {
    path: 's',
    loadChildren: () =>
      import('apps/faboshop/src/app/shop/shop.module').then(m => m.ShopModule)
  },
  {
    path: '**',
    redirectTo: 's/0/message'
  }
];

@NgModule({
  declarations: [
    FaboshopAppComponent,
    RegisterComponent,
    ResetPasswordComponent,
    TicketModalComponent,
    NotiModalComponent,
    ExportFulfillmentModalComponent,
    ExportOrderModalComponent,
    AnnouncementModalComponent,
    VerifyEmailComponent,
    VerifyPhoneComponent,
    LoginComponent,
    CreateShopComponent,
    DesktopOnlyComponent,
    RequireStokenModalComponent,
    FromAddressModalComponent,
    ChatConnectionModalComponent,
    ListAddressesPopupComponent,
    CreateUpdateAddressPopupComponent,
    CreateCustomerPopupComponent,
  ],
  entryComponents: [
    NotiModalComponent,
    TicketModalComponent,
    ExportFulfillmentModalComponent,
    ExportOrderModalComponent,
    AnnouncementModalComponent,
    RequireStokenModalComponent,
    FromAddressModalComponent,
    ChatConnectionModalComponent,
    ListAddressesPopupComponent,
    CreateUpdateAddressPopupComponent,
  ],
  imports: [
    CoreModule.forRoot(),
    SharedModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    ForceButtonModule,
    DeliveryModule,
    RouterModule.forRoot(routes),
    NgbModule,
    LoginModule,
    ImageLoaderModule,
    MyDatePickerModule,
    EtopPipesModule,
    EtopCommonModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    EtopMaterialModule,
    environment.production ? [] : AkitaNgDevtools.forRoot()
  ],
  providers: [
    { provide: CommonUsecase, useClass: ShopCommonUsecase },
    { provide: CONFIG_TOKEN, useValue: environment },
    ...apis,
    ...services,
    FaboshopAppGuard,
    IsUserActiveGuard
  ],
  bootstrap: [FaboshopAppComponent],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FaboshopAppModule extends BaseModuleHmr{
  constructor(public appRef: ApplicationRef) {
    super(appRef)
  }
}
