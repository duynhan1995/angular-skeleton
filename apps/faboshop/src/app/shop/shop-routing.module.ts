import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop.component';
import { ShopGuard } from './shop.guard';
import { shopRoutes } from './shop.route';

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [ShopGuard],
    resolve: {
      account: ShopGuard
    },
    component: ShopComponent,
    children: shopRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
