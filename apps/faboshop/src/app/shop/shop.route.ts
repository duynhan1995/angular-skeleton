import { Routes } from '@angular/router';

export const shopRoutes: Routes = [
  {
    path: 'dashboards',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'products',
    loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'pos',
    loadChildren: () => import('../pages/pos/pos.module').then(m => m.PosModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'orders/:id',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('../pages/partners/partners.module').then(m => m.PartnersModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'settings/:type',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('../pages/notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: 'notify',
    loadChildren: () => import('../pages/notify/notify.module').then(m => m.NotifyModule)
  },
  {
    path: 'messages',
    loadChildren: () => import('../pages/messages/messages.module').then(m => m.MessagesModule)
  },
  {
    path: '**',
    redirectTo: 'messages'
  }
];
