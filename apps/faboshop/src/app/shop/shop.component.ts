import { Component, OnInit } from '@angular/core';
import { CommonLayout } from 'apps/core/src/app/CommonLayout';
import { AuthenticateService, AuthenticateStore } from '@etop/core';
import { NavigationEnd, Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { NavigationData } from 'apps/core/src/components/header/header.interface';
import { NotificationService } from '../../services/notification.service';
import misc from '../../../../core/src/libs/misc';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { MenuItem } from 'apps/core/src/components/menu-item/menu-item.interface';
import { AppService } from '@etop/web/core/app.service';
import { StorageService } from 'apps/core/src/services/storage.service';
import {BankService} from "@etop/state/bank";

@Component({
  selector: 'fabo-shop',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent extends CommonLayout implements OnInit {
  showNotificationIcon = false;
  sideMenus: MenuItem[] = [
    {
      name: 'Tổng quan',
      route: ['dashboards'],
      icon: 'assets/images/icon_dashboard.png'
    },
    {
      name: 'Tin nhắn',
      route: ['messages'],
      icon: 'assets/images/icon_chat.png',
      permissions: ['facebook/message:view','facebook/comment:view'],
    },
    {
      name: 'Đơn hàng',
      route: ['orders'],
      icon: 'assets/images/icon_order.png',
      permissions: ['shop/order:view']
    },
    {
      name: 'Khách hàng',
      route: ['customers'],
      icon: 'assets/images/icon_customer.png',
      permissions: ['shop/customer:view']
    },
    {
      name: 'Sản phẩm',
      route: ['products'],
      icon: 'assets/images/icon_product.png',
      permissions: ['shop/product/basic_info:view']
    },
    {
      name: 'Thiết lập',
      route: ['settings'],
      icon: 'assets/images/icon_settings.png'
    }
  ];

  customSidebar: any = {};

  hideSidebar = !this.permissionsOfActionsArray?.length;
  hideHeader = false;
  showVerifyWarning = true;

  noti_page = 1;
  private announcement_modal: any;

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private util: UtilService,
    private headerController: HeaderControllerService,
    private notification: NotificationService,
    private commonUsecase: CommonUsecase,
    private appService: AppService,
    private storageService: StorageService,
    private bankService: BankService,
    private authenticateService: AuthenticateService,
  ) {
    super(auth);
  }

  get permissionsOfActionsArray() {
    let permissions = [];
    this.sideMenus.forEach(m => {
      if (m.permissions?.length && this.authenticateService.checkPermissions(m.permissions,'or')) {
        permissions = permissions.concat(m.permissions);
      }else if(!m.permissions?.length) {
        permissions = permissions.concat(['default']);
      }
    });
    return permissions;
  }

  async ngOnInit() {
    this.bankService.initBanks().then();

    if (this.appService.appID != 'etop.vn') {
      this.showVerifyWarning = false;
    }
    this.adminLoginMode = this.storageService.get('adminLogin');

    this.layoutHandlingByUrl();
    this.auth.isAuthenticated$.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.getNotifications(1).then();
      } else {
        this.notification.deleteDevice().then();
      }
    });
    this.router.events.subscribe(event => {
      if (
        event instanceof NavigationEnd &&
        this.auth.snapshot.isAuthenticated
      ) {
        this.getNotifications(1);
      }
    });

    this.headerController.onFilterNotisByAccount.subscribe((token: string) => {
      this.getNotifications(1, token);
    });
    this.headerController.onNavigate.subscribe(({ target, data }) => {
      this.onNavigate(target, data);
    });
    this.headerController.onRequestMoreNotis.subscribe((token: string) => {
      this.getNotifications(null, token);
    });
  }

  getTopShipConfig() {
    return this.appService.appID == 'etop.vn' ? 'TOPSHIP' : 'Giao hàng';
  }

  getIconTopShipConfig() {
    return this.appService.appID == 'etop.vn'
      ? 'assets/images/icon_topship.png'
      : 'assets/images/icon_ffm.png';
  }

  getIconColorTopShipConfig() {
    return this.appService.appID == 'etop.vn' ? '#e67e22' : '';
  }

  async getNotifications(page?: number, token?: string) {
    try {
      if (!page) {
        this.noti_page += 1;
        page = this.noti_page;
      }
      const res: any = await this.notification.getNotifications(
        (page - 1) * 10,
        10,
        token
      );
      if (page > 1) {
        this.notifications = this.notifications.concat(res.notifications);
      } else {
        this.notifications = res.notifications;
      }
      this.headerController.loadNotifications(this.notifications);
    } catch (e) {
      debug.log('ERROR in getting notifications in shop', e);
    }
  }

  layoutHandlingByUrl() {
    if (this.router.url.indexOf('/pos') != -1) {
      this.hideHeader = true;
      this.hideSidebar = true;
    }
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('/pos') != -1) {
          this.hideHeader = true;
          this.hideSidebar = true;
        } else {
          this.hideHeader = false;
          this.hideSidebar = false;
        }
      }
    });
  }

  async onNavigate(target, data: NavigationData) {
    let slug: any = '';
    if (data.payload) {
      let accIndex = this.auth.snapshot.accounts.findIndex(
        a => a.id == data.payload.id
      );
      accIndex = accIndex < 0 ? 0 : accIndex;
      const acc = this.auth.snapshot.accounts[accIndex];
      slug = (acc && acc.url_slug) || accIndex;
    }
    let url = '';
    switch (data.target) {
      case 'account':
        await this.notification.deleteDevice();
        url = `/s/${slug || this.util.getSlug()}/settings/shop`;
        window.open(url, '_blank');
        break;
      case 'setting':
        url = `/s/${slug || this.util.getSlug()}/settings/user`;
        this.router.navigateByUrl(url);
        break;
      case 'notification':
        let shop_id_encoded = '';
        if (data.payload.token) {
          shop_id_encoded = misc.encodeBase64(data.payload.token);
        }
        await this.notification.readNotification(data.payload, shop_id_encoded);
        break;
      case 'notifications':
        url = `/s/${slug || this.util.getSlug()}/notifications`;
        this.router.navigateByUrl(url);
        break;
      default:
        url = '';
    }
  }
}
