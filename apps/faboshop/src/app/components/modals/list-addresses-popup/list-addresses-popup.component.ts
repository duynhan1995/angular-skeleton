import {Component, Input, OnInit} from '@angular/core';
import {Address} from "libs/models/Address";
import {ConfirmOrderStore} from "@etop/features/fabo/confirm-order/confirm-order.store";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";

@Component({
  selector: 'faboshop-list-addresses-popup',
  templateUrl: './list-addresses-popup.component.html',
  styleUrls: ['./list-addresses-popup.component.scss']
})
export class ListAddressesPopupComponent implements OnInit {
  @Input() type: 'from' | 'to' = 'from';
  @Input() addresses: Address[];

  constructor(
    private modalAction: ModalAction,
    private confirmOrderStore: ConfirmOrderStore
  ) { }

  get typeDisplay() {
    return this.type == 'from' ? 'lấy' : 'giao';
  }

  get popoverTitle() {
    return `Chọn địa chỉ ${this.typeDisplay} hàng`;
  }

  get emptyTitle() {
    return `Chưa có địa chỉ ${this.typeDisplay} hàng`;
  }

  isSelectedAddress(address: Address) {
    let selectedAddress = this.type == 'from' ?
      this.confirmOrderStore.snapshot.activeFromAddress :
      this.confirmOrderStore.snapshot.activeToAddress;

    return selectedAddress.id == address.id;
  }

  ngOnInit() {}

  close() {
    this.modalAction.close(false);
  }

  selectAddress(address: Address) {
    this.modalAction.dismiss({address});
  }

  newAddress() {
    this.modalAction.dismiss({createAddress: true});
  }

}
