import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import {FbPages} from "libs/models/faboshop/FbPage";

@Component({
  selector: 'faboshop-chat-connection-modal',
  templateUrl: './chat-connection-modal.component.html',
  styleUrls: ['./chat-connection-modal.component.scss']
})
export class ChatConnectionModalComponent implements OnInit {
  @Input() successes: FbPages = [];
  @Input() errors: FbPages = [];

  constructor(
    private modalAction: ModalAction
  ) {}

  get empty() {
    return (this.successes.length + this.errors.length) == 0;
  }

  ngOnInit() {
    debug.log('EMPTY', this.empty);
    debug.log('SUCC', this.successes);
    debug.log('ERR', this.errors);
  }

  closeModal() {
    this.modalAction.close(false);
  }
}
