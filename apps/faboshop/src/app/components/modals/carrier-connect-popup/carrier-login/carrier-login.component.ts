import {Component, Input, OnInit, Output} from '@angular/core';
import {Connection} from "libs/models/Connection";
import {ConnectionAPI} from "@etop/api";
import {Subject} from "rxjs";

@Component({
  selector: 'faboshop-carrier-login',
  templateUrl: './carrier-login.component.html',
  styleUrls: ['./carrier-login.component.scss']
})
export class CarrierLoginComponent implements OnInit {
  @Output() loginInfoChanged = new Subject<ConnectionAPI.LoginShopConnectionRequest>()
  @Input() carrierConnection: Connection;

  loginInfo = new ConnectionAPI.LoginShopConnectionRequest();

  constructor() { }

  ngOnInit() {
  }
}
