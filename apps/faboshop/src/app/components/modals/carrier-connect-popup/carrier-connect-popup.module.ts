import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {CarriersListComponent} from "apps/faboshop/src/app/components/modals/carrier-connect-popup/carriers-list/carriers-list.component";
import {CarrierLoginComponent} from "apps/faboshop/src/app/components/modals/carrier-connect-popup/carrier-login/carrier-login.component";
import {CarrierConnectPopupComponent} from "apps/faboshop/src/app/components/modals/carrier-connect-popup/carrier-connect-popup.component";
import {EtopMaterialModule} from "@etop/shared";
import { CarrierOtpComponent } from './carrier-otp/carrier-otp.component';

@NgModule({
  declarations: [
    CarriersListComponent,
    CarrierLoginComponent,
    CarrierConnectPopupComponent,
    CarrierOtpComponent,
  ],
  entryComponents: [
    CarrierConnectPopupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    EtopMaterialModule
  ]
})
export class CarrierConnectPopupModule { }
