import {Component, Input, OnInit} from '@angular/core';
import {Connection} from "libs/models/Connection";
import {ConnectionAPI} from "@etop/api";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import { ConnectionService } from '@etop/features/connection/connection.service';

enum Step {
  carriersList = 'carriersList',
  carrierLogin = 'carrierLogin',
  carrierOTP = 'carrierOTP'
}

@Component({
  selector: 'faboshop-carrier-connect-popup',
  templateUrl: './carrier-connect-popup.component.html',
  styleUrls: ['./carrier-connect-popup.component.scss']
})
export class CarrierConnectPopupComponent implements OnInit {
  @Input() step: Step = Step.carriersList;
  @Input() selectedCarrierConnection: Connection;
  @Input() loginInfo = new ConnectionAPI.LoginShopConnectionRequest();
  loginOTP = new ConnectionAPI.LoginShopConnectionWithOTPRequest();
  loading = false;

  constructor(
    private modalAction: ModalAction,
    private connectionService: ConnectionService
  ) {}

  get popoverTitle() {
    return this.step == Step.carriersList
      ? 'Chọn nhà vận chuyển'
      : `Kết nối ${this.selectedCarrierConnection.name}`;
  }

  get firstStep() {
    return this.step == Step.carriersList;
  }

  get secondStep() {
    return this.step == Step.carrierLogin;
  }

  get thirdStep() {
    return this.step == Step.carrierOTP;
  }

  ngOnInit() {}

  close() {
    if (this.secondStep) {
      this.step = Step.carriersList;
    } else {
      this.modalAction.close(false);
    }
  }

  back() {
    this.step = Step.carrierLogin;
  }

  carrierLogin(selectedCarrierConnection: Connection) {
    this.step = Step.carrierLogin;
    this.selectedCarrierConnection = selectedCarrierConnection;
  }

  loginInfoChanged(loginInfo: ConnectionAPI.LoginShopConnectionRequest) {
    this.loginInfo = loginInfo;
  }

  loginOTPChanged(loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest) {
    this.loginOTP = {
      ...this.loginOTP,
      otp:loginOTP.otp,
      connection_id: loginOTP.connection_id,
    }
  }

  async continue() {
    try {
      if (!this.loginInfo.identifier) {
        return toastr.error('Vui lòng nhập số điện thoại.');
      }
      this.loginInfo.connection_id = this.selectedCarrierConnection.id;
      await this.connectionService.loginConnection(this.loginInfo);
      this.step = Step.carrierOTP;
    } catch (e) {
      toastr.error(e.message);
    }
  }

  async retryOTP(e) {
    try {
      this.loginInfo.connection_id = this.selectedCarrierConnection.id;
      await this.connectionService.loginConnection(this.loginInfo);
    } catch (e) {
      toastr.error(e.message);
    }
  }

  async confirm() {
    if (!this.loginOTP.otp) {
      this.loading = false;
      return toastr.error('Vui lòng nhập mã xác thực.');
    }
    const data ={
      loginOTP: {
        ...this.loginOTP,
        identifier: this.loginOTP.identifier || this.loginInfo.identifier,
        connection_id: this.selectedCarrierConnection.id
      },
      step: this.step,
      selectedCarrierConnection: this.selectedCarrierConnection
    };
    await this.loginConnection(data)
  }

  async loginConnection(data: any) {
    try {
      this.loading = true;
      const loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest = {
        ...data.loginOTP
      };
      await this.connectionService.loginShopConnectionWithOTP(loginOTP);
      toastr.success('Kết nối thành công');
      this.modalAction.dismiss(data);
    } catch (e) {
      debug.error('ERROR in loginConnection', e);
      toastr.error(e.message,'Kết nối không thành công');
    }
    this.loading = false;
  }
}
