import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ConnectionAPI } from "@etop/api";
import { Connection } from 'libs/models/Connection';
import { Subject } from 'rxjs';

@Component({
  selector: 'faboshop-carrier-otp',
  templateUrl: './carrier-otp.component.html',
  styleUrls: ['./carrier-otp.component.scss']
})
export class CarrierOtpComponent implements OnInit, OnChanges {
  @Output() loginInfoChanged = new Subject<ConnectionAPI.LoginShopConnectionWithOTPRequest>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onRetry = new EventEmitter();
  @Input() carrierConnection: Connection;
  @Input() identifier;
  countDown = 600;
  timer;

  loginInfo = new ConnectionAPI.LoginShopConnectionWithOTPRequest();

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.startTimer();
    this.loginInfo = {
      ...this.loginInfo,
      connection_id: this.carrierConnection.id,
      identifier: this.identifier
    };
  }

  retrySendOtp() {
    this.countDown = 600;
    this.startTimer();
    this.onRetry.emit();
  }

  startTimer() {
    const interval = setInterval(() => {
      if (this.countDown < 0) {
        clearInterval(interval);
      } else {
        this.timer = this.displayCount(this.countDown);
        this.countDown -= 1;
      }
    }, 1000);
  }

  displayCount(timer) {
    const minutes = Math.floor(timer / 60);
    const seconds = Math.floor(timer % 60);
    let _seconds: any = seconds;
    if (seconds < 10) {
      _seconds = '0' + seconds;
    }
    return minutes + ':' + _seconds + 's';
  }
}
