import {Component, Input, OnInit} from '@angular/core';
import {ConversationsQuery} from "@etop/state/fabo/conversation";
import {Customer} from "libs/models/Customer";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import { ConfirmOrderService } from '@etop/features';

@Component({
  selector: 'faboshop-create-customer-popup',
  templateUrl: './create-customer-popup.component.html',
  styleUrls: ['./create-customer-popup.component.scss']
})
export class CreateCustomerPopupComponent implements OnInit {
  @Input() customer: Customer;
  activeFbUser = this.conversationsQuery.getActive()?.fbUsers[0]
  param = '';

  loading = false;
  constructor(
    private modalAction: ModalAction,
    private conversationsQuery: ConversationsQuery,
    private confirmOrderService: ConfirmOrderService,
  ) { }

  ngOnInit() {
    if(this.activeFbUser) {
      this.param = 'Tạo khách hàng liên kết với người dùng Facebook';
    }
  }

  close() {
    this.modalAction.close(false);
  }

  async create() {
    this.loading = true;
    if (!this.customer.full_name) {
      this.loading = false;
      return toastr.error('Chưa nhập tên.');
    }
    if (!this.customer.phone) {
      this.loading = false;
      return toastr.error('Chưa nhập số điện thoại.')
    }
    await this.createCustomer(this.customer)
    this.loading = false;
  }

  async createCustomer(customer: Customer) {
    try {
      await this.confirmOrderService.createCustomerWeb(customer);
      this.modalAction.dismiss(true)
    } catch (e) {
      debug.error('ERROR in createCustomer', e);
      toastr.error(`Tạo khách hàng không thành công. ${e.code && (e.message || e.msg)}`);
    }
  }

}
