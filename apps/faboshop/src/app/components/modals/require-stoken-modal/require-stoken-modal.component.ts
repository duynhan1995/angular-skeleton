import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { AuthenticateStore } from '@etop/core';
import { UserApi } from '@etop/api';

@Component({
  selector: 'shop-require-stoken-modal',
  templateUrl: './require-stoken-modal.component.html',
  styleUrls: ['./require-stoken-modal.component.scss']
})
export class RequireStokenModalComponent implements OnInit {
  loading = false;
  email_verified = '';
  sToken = '';

  constructor(
    private modalAction: ModalAction,
    private auth: AuthenticateStore,
    private userApi: UserApi
  ) { }

  ngOnInit() {
    this.email_verified = this.auth.snapshot.user.email;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async submitSToken() {
    this.loading = true;
    try {
      let res = await this.userApi.upgradeAccessToken(this.sToken);
      this.auth.updateSToken(res.access_token);
      this.modalAction.dismiss(res.access_token);
    } catch (e) {
      debug.error('ERROR in submitting sToken', e);
      toastr.error('Có lỗi xảy ra', e.code ? (e.message || e.msg) : '');
    }
    this.loading = false;
  }

}
