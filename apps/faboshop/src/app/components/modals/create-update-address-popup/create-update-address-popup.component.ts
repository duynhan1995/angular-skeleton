import {Component, Input, OnInit} from '@angular/core';
import {Address} from "libs/models/Address";
import {District, Province, Ward} from "libs/models/Location";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {LocationService} from "@etop/state/location";
import {LocationQuery} from "@etop/state/location/location.query";

@Component({
  selector: 'faboshop-create-update-address-popup',
  templateUrl: './create-update-address-popup.component.html',
  styleUrls: ['./create-update-address-popup.component.scss']
})
export class CreateUpdateAddressPopupComponent implements OnInit {
  @Input() type: 'from' | 'to' = 'from';
  @Input() address = new Address({});

  provincesList: Province[] = [];
  districtsList: District[] = [];
  wardsList: Ward[] = [];

  displayLocationMap = option => option?.name;
  valueLocationMap = option => option?.code;

  constructor(
    private modalAction: ModalAction,
    private locationQuery: LocationQuery,
    private locationService: LocationService
  ) { }

  get typeDisplay() {
    return this.type == 'from' ? 'lấy' : 'giao';
  }

  get popoverTitle() {
    return this.address?.id && `Cập nhật địa chỉ ${this.typeDisplay} hàng` || `Tạo địa chỉ ${this.typeDisplay} hàng`;
  }

  get confirmBtnTitle() {
    return this.address?.id && `Cập nhật địa chỉ` || `Tạo địa chỉ`;
  }

  ngOnInit() {
    this.provincesList = this.locationQuery.getValue().provincesList;
  }

  close() {
    this.modalAction.close(false);
  }

  onProvinceSelected() {
    this.districtsList = this.locationService.filterDistrictsByProvince(this.address.province_code);
    this.address = {
      ...this.address,
      province: this.locationQuery.getProvince(this.address.province_code)?.name,
      district_code: null,
      district: null,
      ward_code: null,
      ward: null
    };
  }

  onDistrictSelected() {
    this.wardsList = this.locationService.filterWardsByDistrict(this.address.district_code);
    this.address.district = this.locationQuery.getDistrict(this.address.district_code)?.name;
    this.address = {
      ...this.address,
      district: this.locationQuery.getDistrict(this.address.district_code)?.name,
      ward_code: null,
      ward: null
    };
  }

  onWardSelected() {
    this.address.ward = this.locationQuery.getWard(this.address.ward_code)?.name;
  }

  confirm() {
    const {full_name, phone, province_code, district_code, ward_code, address1} = this.address;
    if (!full_name) {
      return toastr.error('Chưa nhập tên');
    }
    if (!phone) {
      return toastr.error('Chưa nhập số điện thoại');
    }
    if (!province_code) {
      return toastr.error('Chưa nhập tỉnh thành');
    }
    if (!district_code) {
      return toastr.error('Chưa nhập quận huyện');
    }
    if (!ward_code) {
      return toastr.error('Chưa nhập phường xã');
    }
    if (!address1) {
      return toastr.error('Chưa nhập địa chỉ cụ thể');
    }

    this.modalAction.dismiss({address: this.address});
  }

}
