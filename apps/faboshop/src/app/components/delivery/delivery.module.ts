import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeliveryComponent} from './delivery.component';
import {DeliveryFastComponent} from './delivery-fast/delivery-fast.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FormsModule} from '@angular/forms';
import {ShippingServiceOptionsComponent} from './delivery-fast/shipping-service-options/shipping-service-options.component';
import {PopupCreateAddressCustomerComponent} from './popup-create-address-customer/popup-create-address-customer.component';
import {DeliveryNowComponent} from './delivery-now/delivery-now.component';
import {ShipnowServiceOptionsComponent} from './delivery-now/shipnow-service-options/shipnow-service-options.component';
import {VerifyAccountAhamovePopupComponent} from './delivery-now/verify-account-ahamove-popup/verify-account-ahamove-popup.component';
import {VerifyAccountExternalServiceComponent} from './delivery-now/verify-account-external-service/verify-account-external-service.component';
import {DeliverySelfComponent} from './delivery-self/delivery-self.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ShopPipesModule} from 'apps/faboshop/src/app/pipes/shop-pipes.module';
import {ShipmentServiceOptionsComponent} from './delivery-fast/shipment-service-options/shipment-service-options.component';
import {ShipmentServiceOptionsPlaceholderComponent} from './delivery-fast/shipment-service-options/shipment-service-options-placeholder/shipment-service-options-placeholder.component';
import {EtopFilterModule, EtopFormsModule, EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {FulfillmentService} from "@etop/features/fabo/fulfillment/fulfillment.service";

@NgModule({
  declarations: [
    DeliveryComponent,
    DeliveryFastComponent,
    ShippingServiceOptionsComponent,
    PopupCreateAddressCustomerComponent,
    DeliveryNowComponent,
    ShipnowServiceOptionsComponent,
    VerifyAccountAhamovePopupComponent,
    VerifyAccountExternalServiceComponent,
    DeliverySelfComponent,
    ShipmentServiceOptionsComponent,
    ShipmentServiceOptionsPlaceholderComponent
  ],
  entryComponents: [
    PopupCreateAddressCustomerComponent,
    VerifyAccountAhamovePopupComponent
  ],
  exports: [DeliveryComponent, ShipnowServiceOptionsComponent, ShippingServiceOptionsComponent, ShipmentServiceOptionsComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    ShopPipesModule,
    EtopMaterialModule,
    EtopFormsModule,
    EtopPipesModule,
    EtopFilterModule,
    MaterialModule,
  ],
  providers: [
    FulfillmentService
  ]
})
export class DeliveryModule {
}
