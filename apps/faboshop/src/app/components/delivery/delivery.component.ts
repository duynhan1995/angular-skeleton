import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { DeliveryNowComponent } from './delivery-now/delivery-now.component';
import { FulfillmentService } from '../../../services/fulfillment.service';
import { UserService } from 'apps/core/src/services/user.service';
import { OrderService } from '../../../services/order.service';
import { BaseComponent } from '@etop/core';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: 'shop-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent extends BaseComponent implements OnInit {
  shipping_type = 'shipment';

  constructor(
    private changeDetector: ChangeDetectorRef,
    private ffmService: FulfillmentService,
    private orderService: OrderService,
    private user: UserService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  get to_create_ffm() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  ngOnInit() {}

  switchShippingType(type) {
    this.ffmService.shipping_type = type;
    this.shipping_type = type;
    this.changeDetector.detectChanges();
  }

}
