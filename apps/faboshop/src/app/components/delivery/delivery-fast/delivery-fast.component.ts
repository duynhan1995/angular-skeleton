import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import List from 'identical-list';
import { AddressService } from 'apps/core/src/services/address.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { PopupCreateAddressCustomerComponent } from '../popup-create-address-customer/popup-create-address-customer.component';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { Subject } from 'rxjs';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { Fulfillment } from 'libs/models/Fulfillment';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { CustomerApi, FulfillmentAPI } from '@etop/api';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { ShipmentServiceOptionsComponent } from 'apps/faboshop/src/app/components/delivery/delivery-fast/shipment-service-options/shipment-service-options.component';
import { AppService } from '@etop/web/core/app.service';
import { Address } from 'libs/models/Address';
import { FromAddressModalComponent } from 'apps/faboshop/src/app/pages/settings/components/from-address-modal/from-address-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { CustomerAddressModalComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import {LocationQuery} from "@etop/state/location";

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'shop-delivery-fast',
  templateUrl: './delivery-fast.component.html',
  styleUrls: ['./delivery-fast.component.scss']
})
export class DeliveryFastComponent extends BaseComponent implements OnInit {

  @ViewChild('shipmentServiceOptions', { static: false })
  shipmentServiceOptions: ShipmentServiceOptionsComponent;

  @Output() updateData = new EventEmitter();

  fulfillment = new Fulfillment({});

  cod_amount: number;

  include_insurance = false;

  suggestCODs = new List<FilterOption>([]);

  fromAddresses = [];
  fromAddressIndex: number;
  toAddresses = [];
  toAddressIndex: number;

  productWeights = new List<FilterOption>([
    { id: '100', value: 100 },
    { id: '200', value: 200 },
    { id: '300', value: 300 },
    { id: '500', value: 500 },
    { id: '1000', value: 1000 },
    { id: '1500', value: 1500 },
    { id: '2000', value: 2000 },
    { id: '2500', value: 2500 },
    { id: '3000', value: 3000 },
    { id: '4000', value: 4000 },
    { id: '5000', value: 5000 }
  ]);
  minAcceptWeight = 50;
  defaultWeight = 500;
  defaultLength = 5;
  defaultWidth = 5;
  defaultHeight = 5;
  weight_calc_type: number;

  selectCODInput: Subject<void> = new Subject<void>();

  private dialogModal: any;
  private modal: any;

  constructor(
    private appService: AppService,
    private shopService: ShopService,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private addressService: AddressService,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private customerApi: CustomerApi,
    private customerStore: CustomerStoreService,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

  get errorCOD() {
    return this.ffmStore.snapshot.error_cod;
  }

  async ngOnInit() {
    await this.setupData();

    this.customerStore.selectCustomer$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(customer) => {
        if (customer && customer.id) {
          this.toAddresses = customer.addresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = null;
          if (this.shipmentServiceOptions) {
            // this.shipmentServiceOptions.reset();
          }
        }
      });
    this.customerStore.selectCustomerAddress$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(selected_address) => {
        if (selected_address && selected_address.id) {
          const index = this.toAddresses.findIndex(
            addr => addr.id == selected_address.id
          );
          if (index >= 0) {
            if (this.toAddressIndex != index) {
              this.toAddressIndex = index;
              this.setCustomerAddress();
            }
          } else {
            this.toAddressIndex = null
          }
        }
      });

    this.orderStore.updateTotalAmount$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(total_amount) => {
        this.changeCODFromOutside(total_amount);
        this.cod_amount = null;
        this.onChangeCod();
        this.updateDataShippingService();
      });
    this.orderService.createOrderFailed$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        this.getShipServices();
      });
    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        this.setupData();
      });
  }

  async setupData() {
    this.weight_calc_type = this.ffmService.weight_calc_type;
    this.include_insurance = false;

    const res = await this.addressService.getFromAddresses();

    this.fromAddresses = res.fromAddresses;
    this.fromAddressIndex = res.fromAddressIndex;
    if (this.fromAddresses.length == 0) {
      this.requireAddress();
    }

    const order = this.orderStore.snapshot.selected_order;
    if (order && order.shipping_address && order.shipping_address.province_code) {
      this.toAddresses = [order.shipping_address].map(
        (a, index) => this.addressService.mapAddressInfo(a, index)
      );
      this.toAddressIndex = 0;
    } else {
      this.toAddresses = [];
      this.toAddressIndex = null;
    }

    this.fulfillment = {
      ...new Fulfillment({}),
      cod_amount: null,
      chargeable_weight: this.defaultWeight,
      length: this.defaultLength,
      width: this.defaultWidth,
      height: this.defaultHeight,
      try_on: this.auth.snapshot.shop.try_on,
      pickup_address: this.fromAddresses[this.fromAddressIndex],
      shipping_address: this.toAddresses[this.toAddressIndex]
    };
  }

  changeCODFromOutside(cod_amount) {
    this.suggestCODs = new List<FilterOption>([{ id: '0', value: 0 }]);
    if (cod_amount > 0) {
      this.suggestCODs.add({ id: cod_amount.toString(), value: cod_amount });
    }
  }

  async weightCalcTypeChange() {
    this.ffmService.weight_calc_type = this.weight_calc_type;
    this.updateDataShippingService();
  }

  focusToAddress() {
    if (this.toAddresses.length) { return; }
    this.newToAddress();
  }

  setupFulfillmentAddress() {
    try {
      this.fulfillment.pickup_address = this.fromAddresses[this.fromAddressIndex];
      this.fulfillment.shipping_address = this.toAddresses[this.toAddressIndex];
      this.ffmStore.updateFulfillment(this.fulfillment);
    } catch (e) {
      debug.error('ERROR in getting list addresses', e);
    }
  }

  async setShopAddress() {
    this.fulfillment.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getShipServices();
  }

  async setCustomerAddress() {
    this.customerStore.selectCustomerAddress(this.toAddresses[this.toAddressIndex]);
    this.fulfillment.shipping_address = this.toAddresses[this.toAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getShipServices();
  }

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (address) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
        const res = await this.addressService.getFromAddresses();
        this.fromAddresses = res.fromAddresses;
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
        this.setupFulfillmentAddress();
      }
    });
  }

  requireAddress() {
    this.dialogModal = this.dialog.createConfirmDialog({
      title: `Thiết lập địa chỉ lấy hàng`,
      body: `
        <div class="text-big">Bạn chưa thiết lập địa chỉ lấy hàng?</div>
        <div class="text-big">Vui lòng tạo địa chỉ lấy hàng đầu tiên để sử dụng dịch vị giao hàng.</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Tạo địa chỉ',
      closeAfterAction: true,
      onConfirm: async() => {
        this.newFromAddress();
      },
      onCancel: () => {
        this.dialogModal.close();
        this.dialogModal = null;
      }
    });
    this.dialogModal.show();
  }

  newToAddress() {
    if (this.modal) {
      return;
    }
    if (!this.ffmStore.snapshot.to_create_customer_by_shipping_address) {
      const customer = this.customerStore.snapshot.selected_customer;
      this.modal = this.modalController.create({
        component: CustomerAddressModalComponent,
        componentProps: {
          wardRequired: false,
          customer_id: customer && customer.id || null,
          title: 'create',
          confirmBtnClass: 'btn-topship'
        }
      });
      if (!customer || !customer.id) {
        this.modal.onDismiss().then(address => {
          address = {
            ...address,
            province: this.locationQuery.getProvince(address.province_code)?.name,
            district: this.locationQuery.getDistrict(address.district_code)?.name,
            ward: address.ward_code && this.locationQuery.getWard(address.ward_code)?.name,
          };
          this.toAddresses.push(address);
          this.toAddresses = this.toAddresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = this.toAddresses.length - 1;
        });
      } else {
        this.modal.onDismiss().then(async(address) => {
          this.customerStore.resetSelectedCustomer();
          customer.addresses = await this.customerApi.getCustomerAddresses(customer.id);
          this.customerStore.selectCustomer(customer);
          this.customerStore.selectCustomerAddress(address);
        });
      }
    } else {
      this.modal = this.modalController.create({
        component: PopupCreateAddressCustomerComponent,
        componentProps: {
          wardRequired: false
        }
      });
    }

    this.modal.show().then();
    this.modal.onDismiss().then(_ => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async onChangeCod() {
    this.fulfillment.cod_amount = this.cod_amount;
    this.ffmStore.updateFulfillment(this.fulfillment);
    this.updateDataShippingService();
  }

  onShipServiceChange(e) {
    this.fulfillment.shipping_service_name = e && e.name || null;
    this.fulfillment.shipping_service_code = e && e.code || null;
    this.fulfillment.shipping_service_fee = e && e.fee || null;
    this.fulfillment.connection_id =
      (e && e.connection_info && e.connection_info.id) || null;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  toggleInsurance() {
    this.include_insurance = !this.include_insurance;
    this.fulfillment.include_insurance = this.include_insurance;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  async updateDataShippingService() {
    this.getShipServices();
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  validateWeightFn = (s: string) => {
    if (s) {
      const num = Number(s);
      if (Number.isNaN(num) || num < this.minAcceptWeight) {
        return false;
      }
    }
    return true;
  };

  async getShipServices() {
    try {
      const from_address = this.fromAddresses[this.fromAddressIndex];
      const to_address = this.toAddresses[this.toAddressIndex];
      if (from_address && to_address) {
        const data: FulfillmentAPI.GetShipmentServicesRequest = {
          coupon:'',
          from_province_code: from_address.province_code,
          to_province_code: to_address.province_code,
          from_district_code: from_address.district_code,
          to_district_code: to_address.district_code,
          from_ward_code: from_address.ward_code,
          to_ward_code: to_address.ward_code,
          chargeable_weight: (
            this.ffmService.weight_calc_type == 1 && this.fulfillment.chargeable_weight
            ) || null,
          total_cod_amount: this.cod_amount || 0,
          basket_value: this.orderStore.snapshot.basket_value,
          include_insurance: this.include_insurance,
          length: (
            this.ffmService.weight_calc_type == 2 && this.fulfillment.length
            ) || null,
          width: (
            this.ffmService.weight_calc_type == 2 && this.fulfillment.width
            ) || null,
          height: (
            this.ffmService.weight_calc_type == 2 && this.fulfillment.height
            ) || null
        };
        if (this.shipmentServiceOptions) {
          await this.shipmentServiceOptions.getServices(data);
        }
      } else {
        if (this.shipmentServiceOptions) {
          // return this.shipmentServiceOptions.reset();
        }
      }
    } catch (e) {
      debug.error('ERROR in getting ship services', e);
    }
  }
}
