import { Component, OnInit} from '@angular/core';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { FulfillmentApi } from '@etop/api';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import {ConfirmOrderService} from "@etop/features/fabo/confirm-order/confirm-order.service";
import {FulfillmentService} from "@etop/features/fabo/fulfillment/fulfillment.service";
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { ShipmentCarrier, FulfillmentShipmentService } from 'libs/models/Fulfillment';

@Component({
  selector: 'faboshop-shipment-service-options',
  templateUrl: './shipment-service-options.component.html',
  styleUrls: ['./shipment-service-options.component.scss'],
})
export class ShipmentServiceOptionsComponent extends BaseComponent implements OnInit {
  carriers: ShipmentCarrier[] = [];

  getServicesIndex = 0;
  gettingServices = false;

  errorMessage = '';

  shipmentServicesRequest$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.shipmentServicesRequest),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  activeShipmentService$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => prev?.unique_id == next?.unique_id)
  );

  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private fulfillmentApi: FulfillmentApi,
    private fulfillmentService: FulfillmentService,
    private connectionStore: ConnectionStore,
  ) {
    super();
  }

  get headerTitle() {
    const shipmentServicesRequest = this.confirmOrderStore.snapshot.shipmentServicesRequest;
    return shipmentServicesRequest?.to_ward_code && 'Chọn gói vận chuyển' || 'Vui lòng điền đầy đủ thông tin để lấy gói vận chuyển';
  }

  get isEnoughData() {
    return !!this.confirmOrderStore.snapshot.shipmentServicesRequest.to_ward_code;
  }

  ngOnInit() {
    this.shipmentServicesRequest$.pipe(takeUntil(this.destroy$))
      .subscribe(request => {
        this.getServices(request).then();
      });
  }

  carrierHasSelectedService(carrier) {
    const selectedService = this.confirmOrderStore.snapshot.activeShipmentService;
    return carrier.services.some(s => selectedService?.unique_id == s?.unique_id);
  }

  selectService(service: FulfillmentShipmentService) {
    this.confirmOrderService.selectShipmentService(service);
  }

  async getServices(data) {
    if (!data || !data.to_district_code) {
      return;
    }

    this.selectService(null);
    this.errorMessage = '';
    this.gettingServices = true;
    try {

      this.getServicesIndex++;
      const body = {
        ...data,
        index: this.getServicesIndex
      };
      const loggedInConnections: any = this.connectionStore.snapshot.loggedInConnections;
      const shopConnectionIds = loggedInConnections && loggedInConnections.map(con => con.connection_id);
      const res = await this.fulfillmentApi.getShipmentServices(body);
      if (res.index == this.getServicesIndex) {
        this.carriers = this.fulfillmentService.groupShipmentServicesByConnection(
          res.services,
          shopConnectionIds
        );
        this.gettingServices = false;
      }
    } catch (e) {
      debug.error('ERROR in Getting Services', e);
      this.errorMessage = e.message;
      this.gettingServices = false;
    }
  }

}
