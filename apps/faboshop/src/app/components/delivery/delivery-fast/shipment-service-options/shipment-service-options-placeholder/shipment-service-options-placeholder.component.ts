import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shop-shipment-service-options-placeholder',
  templateUrl: './shipment-service-options-placeholder.component.html',
  styleUrls: ['./shipment-service-options-placeholder.component.scss']
})
export class ShipmentServiceOptionsPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
