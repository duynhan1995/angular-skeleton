import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { OrderAddress } from 'libs/models/Address';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { CmsService } from 'apps/faboshop/src/services/cms.service';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { FulfillmentApi } from '@etop/api';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: 'shop-shipping-service-options',
  templateUrl: './shipping-service-options.component.html',
  styleUrls: ['./shipping-service-options.component.scss']
})
export class ShippingServiceOptionsComponent extends BaseComponent implements OnInit {
  @Input() canSelect = true;
  @Input() fromAddress: OrderAddress;
  @Input() weight;
  @Input() showNote = true;
  @Input() publicAPI = false;
  @Output() change = new EventEmitter();
  @ViewChild("balanceWarningModal", {static: false}) balanceWarningModal;

  shipProviders: any[] = [];
  shipServices: any[] = [];

  toDistrict = '';
  chargeableWeight: number;

  selectedShipService: any = {};
  loading = false;
  shipServicesErrorMsg = '';

  shopBalance;
  note: string;

  getServicesIndex = 0;
  providerOff = {
    ghn: false,
    ghtk: false,
    vtpost: false,
  };

  carrier_description = {
    ghn: '',
    ghtk: '',
    vtpost: ''
  };

  constructor(
    private ffmApi: FulfillmentApi,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private shop: ShopService,
    private cms: CmsService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  async ngOnInit() {
    const dayOff1 = new Date(2019, 0, 27, 16).getTime();
    const dayOff2 = new Date(2019, 0, 29, 16).getTime();
    const dayOff3 = new Date(2019, 1, 1, 16).getTime();
    const dayOpen = new Date(2019, 1, 5).getTime();
    const now = Date.now();
    if (now >= dayOff1 && now < dayOpen) {
      this.providerOff.vtpost = true;
      if (now >= dayOff2) {
        this.providerOff.ghtk = true;
      }
      if (now >= dayOff3) {
        this.providerOff.ghn = true;
      }
    }

    this.cmsDataRetrieve();
    if (!this.publicAPI) {
      this.shopBalance = await this.shop.calcShopBalance();
    }

    this.ffmService.onResetData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.reset();
      });
  }

  cmsDataRetrieve() {
    if (this.cms.banner_loaded) {
      this.note = this.cms.getPOSNote();
      this.getCarrierDescription();
    } else {
      this.cms.onBannersLoaded
        .pipe(takeUntil(this.destroy$))
        .subscribe(_ => {
          this.note = this.cms.getPOSNote();
          this.getCarrierDescription();
        });
    }
  }

  reset() {
    this.shipProviders = [];
    this.shipServices = [];
    this.toDistrict = '';
    this.chargeableWeight = null;
    this.shipServicesErrorMsg = '';
    this.selectService(null);
    debug.log('toDistrict', this.toDistrict);
  }

  isValidShopBalance() {
    return this.shopBalance >= -200 * 1000;
  }

  async getServices(data) {
    if (!data.to_district_code || !this.ffmStore.snapshot.to_create_ffm) {
      return;
    }

    this.reset();
    this.toDistrict = data.to_district_code;

    if (this.providerOff.ghn && this.providerOff.ghtk && this.providerOff.vtpost) {
      this.shipProviders = [
        {
          name: 'ghn',
          imageSrc: "/assets/images/provider_logos/ship-ghn.png"
        },
        {
          name: 'ghtk',
          imageSrc: "/assets/images/provider_logos/ship-ghtk.png"
        },
        {
          name: 'vtpost',
          imageSrc: "/assets/images/provider_logos/ship-vtpost.png"
        }
      ];
      return;
    }
    try {
      this.loading = true;
      if (!this.publicAPI) {
        if (!(this.shopBalance / 10)) {
          this.shopBalance = await this.shop.calcShopBalance();
        }
        if (!this.isValidShopBalance()) {
          $(this.balanceWarningModal.nativeElement).modal("show");
        }
      }
      this.getServicesIndex++;
      const body = {
        ...data,
        index: this.getServicesIndex
      };
      const res = !this.publicAPI ? await this.ffmApi.getShipmentServices(body) : await this.ffmApi.getPublicExternalShippingServices(body);

      if (res.index == this.getServicesIndex) {
        const shipServices = (res && res.services) || [];
        this.shipProviders = this.ffmService.filterShippingService(shipServices);

          this.shipProviders.forEach(sp => {
            if (!this.publicAPI) {
              if (sp.name == 'ghtk' && !this.isAvailable(sp.name)) {
                sp.services = [{ is_available: false }];
              }
            }
            this.shipServices = this.shipServices.concat(sp.services);
            sp.description = this.carrier_description[sp.name];
          });
        this.loading = false;
      }
    } catch (e) {
      debug.error(e);
      this.shipServicesErrorMsg = e.message;
      this.loading = false;
    }
  }

  expectedPickingTime(service) {
    const { expected_pick_at } = service;
    return expected_pick_at ? `trước ${moment(expected_pick_at).format('HH:mm DD/MM')}` : '--';
  }

  expectedDeliveryTime(service) {
    const { expected_delivery_at } = service;
    return expected_delivery_at ? moment(expected_delivery_at).format('DD/MM') : '--';
  }

  selectService(service) {
    this.selectedShipService = service;
    this.change.emit(this.selectedShipService);
  }

  getCarrierDescription() {
    const result = this.cms.getCarrierDescription();
    const _carriers = result.split('<div>####</div>');
    for (let c of _carriers) {
      const desc = c.split(/<div>[0-9a-zA-Z]+##<\/div>/)[1];
      const _c = c.split('##</div>')[0].split('<div>')[1];
      this.carrier_description[_c] = desc;
    }
  }

  isAvailable(carrier_name) {
    if (carrier_name == 'ghtk') {
      const ffm = this.ffmStore.snapshot.fulfillment;
      if (ffm.chargeable_weight > 5000) {
        return false;
      }
      const { width, length, height } = ffm;
      const _weight = Math.ceil(length * width * height / 5);
      if (length > 30 || width > 30 || height > 30 || _weight > 5000) {
        return false;
      }
    }
    return true;
  }

  unavailableText(carrier_name) {
    if (this.publicAPI) {
      return this.cms.getGHTKUnavailableText() && carrier_name == 'ghtk' ? this.cms.getGHTKUnavailableText() : 'KHÔNG KHẢ DỤNG';
    }
    if (carrier_name == 'ghtk') {
      const ffm = this.ffmStore.snapshot.fulfillment;
      if (ffm.chargeable_weight > 5000) {
        return 'GHTK không hỗ trợ giao gói hàng lớn hơn 30x30x30cm hoặc nặng hơn 5kg.';
      }
      const { width, length, height } = ffm;
      const _weight = Math.ceil(length * width * height / 5);
      if (length > 30 || width > 30 || height > 30 || _weight > 5000) {
        return 'GHTK không hỗ trợ giao gói hàng lớn hơn 30x30x30cm hoặc nặng hơn 5kg.';
      }
    }
    return this.cms.getGHTKUnavailableText() && carrier_name == 'ghtk' ? this.cms.getGHTKUnavailableText() : 'KHÔNG KHẢ DỤNG';
  }
}
