import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import List from 'identical-list';
import { ShipnowServiceOptionsComponent } from './shipnow-service-options/shipnow-service-options.component';
import { Address, OrderAddress } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { UserService } from 'apps/core/src/services/user.service';
import { GoogleMapService } from 'apps/faboshop/src/services/google-map.service';
import { VerifyAccountAhamovePopupComponent } from './verify-account-ahamove-popup/verify-account-ahamove-popup.component';
import { PopupCreateAddressCustomerComponent } from '../popup-create-address-customer/popup-create-address-customer.component';
import { OrderService } from '../../../../services/order.service';
import { Subject } from 'rxjs';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { CustomerAddressModalComponent } from 'apps/faboshop/src/app/pages/partners/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { FromAddressModalComponent } from 'apps/faboshop/src/app/pages/settings/components/from-address-modal/from-address-modal.component';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { CustomerApi, FulfillmentAPI } from '@etop/api';
import {LocationQuery} from "@etop/state/location";

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'shop-delivery-now',
  templateUrl: './delivery-now.component.html',
  styleUrls: ['./delivery-now.component.scss']
})
export class DeliveryNowComponent extends BaseComponent implements OnInit {
  @ViewChild('shipnowServiceOptions', { static: false }) shipnowServiceOptions: ShipnowServiceOptionsComponent;

  @ViewChild('updateShippingAddressInput', {static: false}) updateShippingAddressInput: ElementRef;
  @ViewChild('updatePickupAddressInput', {static: false}) updatePickupAddressInput: ElementRef;

  @Output() updateData = new EventEmitter();

  fulfillment = new Fulfillment({});

  cod_amount: number;

  suggestCODs = new List<FilterOption>([]);

  fromAddresses = [];
  fromAddressIndex: number;
  toAddresses = [];
  toAddressIndex: number;

  address_temp = new OrderAddress({});
  invalid_pickup_address = false;
  invalid_shipping_address = false;
  to_update_pickup_address = '';
  to_update_shipping_address = '';
  re_get_by_invalid_address = false;
  autocomplete: any;

  selectCODInput: Subject<void> = new Subject<void>();

  private dialogModal: any;
  private modal: any;

  constructor(
    private shopService: ShopService,
    private user: UserService,
    private auth: AuthenticateStore,
    private googleMap: GoogleMapService,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private addressService: AddressService,
    private orderService: OrderService,
    private customerApi: CustomerApi,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private customerStore: CustomerStoreService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  get fromAddress() {
    return this.fromAddresses[this.fromAddressIndex];
  }

  get toAddress() {
    return this.toAddresses[this.toAddressIndex];
  }

  set fromAddress(from_address) {
    this.fromAddresses[this.fromAddressIndex] = from_address;
    this.fulfillment.pickup_address = from_address;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  set toAddress(to_address) {
    this.toAddresses[this.toAddressIndex] = to_address;
    this.fulfillment.shipping_address = to_address;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  get notSupportedShipnow() {
    const allowed_provinces = ['79', '01'];
    const not_supported_pickup = !this.fromAddress || allowed_provinces.indexOf(this.fromAddress.province_code) == -1;
    const not_supported_deliver = !this.toAddress || allowed_provinces.indexOf(this.toAddress.province_code) == -1;
    const not_same_province = !this.fromAddress || !this.toAddress ||
      (this.fromAddress.province_code != this.toAddress.province_code);
    return not_supported_pickup || not_supported_deliver || not_same_province;
  }

  get isShopOwner() {
    return this.auth.snapshot.permission.roles.includes('owner');
  }

  get errorCOD() {
    return this.ffmStore.snapshot.error_cod;
  }

  async ngOnInit() {
    await this.setupData();

    this.customerStore.selectCustomer$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(customer) => {
        if (customer && customer.id) {
          this.toAddresses = customer.addresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = null;
          if (this.shipnowServiceOptions) {
            this.shipnowServiceOptions.resetData();
          }
        }
      });
    this.customerStore.selectCustomerAddress$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(selected_address) => {
        if (selected_address && selected_address.id) {
          const index = this.toAddresses.findIndex(
            addr => addr.id == selected_address.id
          );
          if (index >= 0) {
            if (this.toAddressIndex != index) {
              this.toAddressIndex = index;
              this.setCustomerAddress();
            }
          } else {
            this.toAddressIndex = null
          }
        }
      });

    this.orderStore.updateTotalAmount$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(total_amount) => {
        this.changeCODFromOutside(total_amount);
        this.cod_amount = null;
        this.onChangeCod();
        this.updateDataShippingService();
      });
    this.orderService.createOrderFailed$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        this.getShipServices();
      });
    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        this.setupData();
      });
  }

  async setupData() {
    const res = await this.addressService.getFromAddresses();

    this.fromAddresses = res.fromAddresses;
    this.fromAddressIndex = res.fromAddressIndex;
    if (this.fromAddresses.length == 0) {
      this.requireAddress();
    }

    const order = this.orderStore.snapshot.selected_order;
    if (order && order.shipping_address && order.shipping_address.province_code) {
      this.toAddresses = [order.shipping_address].map(
        (a, index) => this.addressService.mapAddressInfo(a, index)
      );
      this.toAddressIndex = 0;
    } else {
      this.toAddresses = [];
      this.toAddressIndex = null;
    }

    this.fulfillment = {
      ...new Fulfillment({}),
      cod_amount: null,
      chargeable_weight: 1000,
      pickup_address: this.fromAddresses[this.fromAddressIndex],
      shipping_address: this.toAddresses[this.toAddressIndex]
    };
  }

  changeCODFromOutside(cod_amount) {
    this.suggestCODs = new List<FilterOption>([{ id: '0', value: 0 }]);
    if (cod_amount > 0) {
      this.suggestCODs.add({ id: cod_amount.toString(), value: cod_amount });
    }
  }

  focusToAddress() {
    if (this.toAddresses.length) { return; }
    this.newToAddress();
  }

  async setupFulfillmentAddress() {
    try {
      this.fulfillment.pickup_address = this.fromAddresses[this.fromAddressIndex];
      this.fulfillment.shipping_address = this.toAddresses[this.toAddressIndex];
      this.ffmStore.updateFulfillment(this.fulfillment);

      await this.getCoorsForAddressFrom();
      await this.getCoorsForAddressTo();
    } catch(e) {
      debug.error('ERROR in getting from & to addresses', e);
    }
  }

  async setShopAddress() {
    this.fulfillment.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getCoorsForAddressFrom();
    await this.getShipServices();
  }

  async setCustomerAddress() {
    this.customerStore.selectCustomerAddress(this.toAddresses[this.toAddressIndex]);
    this.fulfillment.shipping_address = this.toAddresses[this.toAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getCoorsForAddressTo();
    await this.getShipServices();
  }

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (address) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
        const res = await this.addressService.getFromAddresses();
        this.fromAddresses = res.fromAddresses;
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
        this.setupFulfillmentAddress();
      }
    });
  }

  requireAddress() {
    this.dialogModal = this.dialog.createConfirmDialog({
      title: `Thiết lập địa chỉ lấy hàng`,
      body: `
        <div class="text-big">Bạn chưa thiết lập địa chỉ lấy hàng?</div>
        <div class="text-big">Vui lòng tạo địa chỉ lấy hàng đầu tiên để sử dụng dịch vị giao hàng.</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Tạo địa chỉ',
      closeAfterAction: true,
      onConfirm: async() => {
        this.newFromAddress();
      },
      onCancel: () => {
        this.dialogModal.close();
        this.dialogModal = null;
      }
    });
    this.dialogModal.show();
  }

  newToAddress() {
    if (this.modal) {
      return;
    }
    if (!this.ffmStore.snapshot.to_create_customer_by_shipping_address) {
      const customer = this.customerStore.snapshot.selected_customer;
      this.modal = this.modalController.create({
        component: CustomerAddressModalComponent,
        componentProps: {
          customer_id: customer && customer.id || null,
          title: 'create',
          confirmBtnClass: 'btn-topship'
        }
      });
      if (!customer || !customer.id) {
        this.modal.onDismiss().then(address => {
          address = {
            ...address,
            province: this.locationQuery.getProvince(address.province_code).name,
            district: this.locationQuery.getDistrict(address.district_code).name,
            ward: address.ward_code && this.locationQuery.getWard(address.ward_code).name,
          };
          this.toAddresses.push(address);
          this.toAddresses = this.toAddresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = this.toAddresses.length - 1;
        });
      } else {
        this.modal.onDismiss().then(async(address) => {
          this.customerStore.resetSelectedCustomer();
          customer.addresses = await this.customerApi.getCustomerAddresses(customer.id);
          this.customerStore.selectCustomer(customer);
          this.customerStore.selectCustomerAddress(address);
        });
      }
    } else {
      this.modal = this.modalController.create({
        component: PopupCreateAddressCustomerComponent
      });
    }
    this.modal.show().then();
    this.modal.onDismiss().then(_ => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async onChangeCod() {
    this.fulfillment.cod_amount = this.cod_amount;
    this.ffmStore.updateFulfillment(this.fulfillment);
    this.updateDataShippingService();
  }

  onShipServiceChange(e) {
    this.fulfillment.carrier = e && e.carrier || null;
    this.fulfillment.shipping_service_name = e && e.name || null;
    this.fulfillment.shipping_service_code = e && e.code || null;
    this.fulfillment.shipping_service_fee = e && e.fee || null;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  async updateDataShippingService() {
    this.getShipServices();
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  async getShipServices() {
    try {
      const from_address = this.fromAddresses[this.fromAddressIndex];
      const to_address = this.toAddresses[this.toAddressIndex];
      if (from_address && to_address && !this.notSupportedShipnow) {
        const { cod_amount, shipping_address } = this.fulfillment;
        const data: FulfillmentAPI.GetShipnowServicesRequest = {
          pickup_address: from_address,
          delivery_points: [{
            cod_amount,
            shipping_address,
          }]
        };
        this.shipnowServiceOptions.resetData();

        this.shipnowServiceOptions.delivery_points = data.delivery_points;
        this.shipnowServiceOptions.pickup_address = data.pickup_address;
        await this.shipnowServiceOptions.getShipnowServices();
      } else {
        return this.shipnowServiceOptions && this.shipnowServiceOptions.resetData();
      }
    } catch(e) {
      debug.error('ERROR in getting shipnow services', e);
    }
  }

  async getCoorsForAddressFrom() {
    if (!this.fromAddress) { return; }
    let { coordinates, province, province_code } = this.fromAddress;
    if (!(coordinates && coordinates.latitude && coordinates.longitude)) {
      const { address1, ward, district } = this.fromAddress;
      const from_address_fulltext = `${address1}, ${ward}, ${district}, ${province}`;
      const res: any = await this.googleMap.getLatLngFromAddress(from_address_fulltext);
      const { lat, lng, invalid } = res;
      this.fromAddress = {
        ...this.fromAddress,
        coordinates: {
          latitude: lat,
          longitude: lng
        }
      };
      if (!(lat && lng)) {
        this.invalid_pickup_address = true;
      }
    }
    $('.pac-container').remove();
    await this.googleMap.getBoundCornersOfCity(province_code);
    this.initPlacesAutocomplete('updatePickupAddressInput');
  }

  async getCoorsForAddressTo() {
    if (!this.toAddress) { return; }
    let { coordinates, province, province_code } = this.toAddress;
    if (!(coordinates && coordinates.latitude && coordinates.longitude)) {
      const { address1, ward, district } = this.toAddress;
      const to_address_fulltext = `${address1}, ${ward}, ${district}, ${province}`;
      const res: any = await this.googleMap.getLatLngFromAddress(to_address_fulltext);
      const { lat, lng, invalid } = res;
      this.toAddress = {
        ...this.toAddress,
        coordinates: {
          latitude: lat,
          longitude: lng
        }
      };
      if (!(lat && lng)) {
        this.invalid_shipping_address = true;
      }
    }
    $('.pac-container').remove();
    await this.googleMap.getBoundCornersOfCity(province_code);
    this.initPlacesAutocomplete('updateShippingAddressInput');
  }

  initPlacesAutocomplete(input_type: string) {
    try {
      if (!this[input_type]) { return; }
      this.autocomplete = new google.maps.places.Autocomplete(this[input_type].nativeElement, {
        componentRestrictions: { country: 'vn' },
        bounds: this.googleMap.bounds,
        strictBounds: true
      });
      this.autocomplete.addListener('place_changed', (e) => {
        let place = this.autocomplete.getPlace();
        const addr_comp = place.address_components;
        const name = place.name;
        const coordinates = {
          latitude: place.geometry.location.lat(),
          longitude: place.geometry.location.lng(),
        };
        this.makeValidAddress(addr_comp, name, coordinates);
      });
    } catch (e) {
      debug.error("ERROR in init Autocomplete", e);
    }
  }

  makeValidAddress(address_components, address_name, coordinates) {
    const ward = address_components.find(a => a.types[0] == "sublocality_level_1");
    const district = address_components.find(a => a.types[0] == "administrative_area_level_2");
    this.address_temp = {
      ...this.toAddress,
      ward: ward ? ward.short_name : this.toAddress.ward,
      district: district ? district.short_name : this.toAddress.district,
      address1: address_name,
      coordinates,
    };
    this.re_get_by_invalid_address = true;
    this.getShipServices();
  }

  openVerifyAhamovePopup() {
    let modal = this.modalController.create({
      component: VerifyAccountAhamovePopupComponent,
      showBackdrop: 'static',
      componentProps: {},
      cssClass: 'modal-xl'
    });
    modal.show().then(async() => {
    });
    modal.onDismiss().then(_ => {

    });
  }

}
