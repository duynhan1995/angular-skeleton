import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { VerifyAccountExternalServiceComponent } from '../verify-account-external-service/verify-account-external-service.component';
import { UserService } from '../../../../../../../core/src/services/user.service';

@Component({
  selector: 'shop-verify-account-ahamove-popup',
  templateUrl: './verify-account-ahamove-popup.component.html',
  styleUrls: ['./verify-account-ahamove-popup.component.scss']
})
export class VerifyAccountAhamovePopupComponent implements OnInit {
  @ViewChild('verifyAccountAhamovePopup', {static: false}) verifyAccountAhamovePopup: ElementRef;
  @ViewChild('verifyAccountExternalService', {static: false}) verifyAccountExternalService: VerifyAccountExternalServiceComponent;
  wait_for_verifying = false;
  loading = false;

  display = false;
  percent = 0;
  interval;

  constructor(
    private user: UserService,
    // private progressBar: ProgressBarBottomService,
  ) { }

  ngOnInit() {
  }

  async checkAccountAhamove() {

  }

  async createExternalAccountAhamove() {

  }

  async verifyAccountAhamove() {
    
  }

  private areInfoProvidedProperly() {
    const { img_src, img_name, selected_verify_shop_method, verify_shop_methods } = this.verifyAccountExternalService;
    for (let key in img_src) {
      if (!img_src[key]) {
        throw new Error(`Vui lòng upload ${img_name[key]}`);
      }
    }
    const _method = verify_shop_methods.find(m => m.type == selected_verify_shop_method);
    if (['business_license', 'company_images'].indexOf(selected_verify_shop_method) != -1) {
      if (!img_src[selected_verify_shop_method].length) {
        throw new Error(`Vui lòng upload ảnh ${_method.title}`);
      }
    } else {
      if (!this.verifyAccountExternalService[selected_verify_shop_method]) {
        throw new Error(`Vui lòng upload ${_method.title}`);
      }
    }
  }

  startLoading() {
    this.display = true;
    this.percent = 0;
    this.increase();
    this.interval = setInterval(() => this.increase(), 200);
  }

  increase() {
    if (this.percent < 90) {
      this.percent += 10;
    } else {
      clearInterval(this.interval);
    }
  }

  endLoading() {
    this.percent = 100;
    setTimeout(() => {
      this.display = false;
    }, 1000);
  }

}
