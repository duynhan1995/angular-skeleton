import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { Address, OrderAddress } from 'libs/models/Address';
import {
  FulfillmentShipnowService,
  ShipnowDeliveryPoint
} from 'libs/models/Fulfillment';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: 'shop-shipnow-service-options',
  templateUrl: './shipnow-service-options.component.html',
  styleUrls: ['./shipnow-service-options.component.scss']
})
export class ShipnowServiceOptionsComponent extends BaseComponent implements OnInit {
  @Output() selectedService = new EventEmitter();
  @Input() total_col = 2;

  loading = false;
  getServicesIndex = 0;

  pickup_address: OrderAddress | Address;
  delivery_points: Array<ShipnowDeliveryPoint> = [];
  services: FulfillmentShipnowService[] = [];
  carriers = [];

  constructor(
    private ffm: FulfillmentService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  ngOnInit() {
    this.ffm.onResetData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.resetData()
      });
  }

  resetData() {
    this.delivery_points = [];
    this.services = [];
    this.carriers = [];
  }

  async getShipnowServices() {
    if (!this.ffmStore.snapshot.to_create_ffm) {
      return;
    }
    this.loading = true;
    this.getServicesIndex += 1;
    this.carriers = [];
    try {
      this.services = [];
      this.selectService(null);
      let { pickup_address, delivery_points } = this;
      delivery_points = delivery_points.filter(dp => !dp.invalid);
      if (!delivery_points.length) {
        this.loading = false;
        return;
      }
      const body = {
        pickup_address,
        delivery_points,
        index: this.getServicesIndex
      };
      let res = await this.ffm.getShipnowServices(body);
      if (res.index == this.getServicesIndex) {
        this.services = res.services;
        this.carriers = [
          {
            name: 'ahamove',
            services: []
          },
          {
            name: 'grab',
            services: []
          }
        ];
        this.carriers.forEach(c => {
          c.services = res.services.filter(s => s.carrier == c.name);
        });
        this.loading = false;
      }
    } catch (e) {
      toastr.error('Có lỗi xảy ra trong quá trình lấy gói vận chuyển');
      debug.error(e);
      this.loading = false;
    }
  }

  selectService(service: FulfillmentShipnowService) {
    if (service) {
      this.services.forEach(s => (s.selected = false));
      service.selected = true;
    }
    this.selectedService.emit(service);
  }
}
