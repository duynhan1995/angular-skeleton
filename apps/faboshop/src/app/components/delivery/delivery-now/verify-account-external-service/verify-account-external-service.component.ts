import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { UserService } from 'apps/core/src/services/user.service';

const ACCEPT_IMAGE_FILE_EXTENSION = 'image/png,image/jpg,image/jpeg';

@Component({
  selector: 'shop-verify-account-external-service',
  templateUrl: './verify-account-external-service.component.html',
  styleUrls: ['./verify-account-external-service.component.scss']
})
export class VerifyAccountExternalServiceComponent implements OnInit {
  @ViewChild('id_front', {static: false}) id_front: ElementRef;
  @ViewChild('id_back', {static: false}) id_back: ElementRef;
  @ViewChild('portrait', {static: false}) portrait: ElementRef;
  @Input() wait_for_verifying: boolean;
  @Input() loading: boolean;

  images_type = [
    'id_front',
    'id_back',
    'portrait'
  ];

  img_name = {
    id_front: 'Ảnh mặt trước CMND',
    id_back: 'Ảnh mặt sau CMND',
    portrait: 'Ảnh chân dung bạn cầm CMND'
  };

  img_src = {
    id_front: '',
    id_back: '',
    portrait: '',
    company_images: [],
    business_license: []
  };

  uploading = {
    id_front: false,
    id_back: false,
    portrait: false,
    company_images: [],
    business_license: []
  };

  fanpage_url = '';
  website_url = '';

  display = false;
  percent = 0;
  interval: any;

  verify_shop_methods = [
    {
      type: 'business_license',
      title: 'Giấy phép kinh doanh',
      subtitle: 'Doanh nghiệp đang hoạt động'
    },
    {
      type: 'company_images',
      title: 'Hình ảnh công ty',
      subtitle: 'Bảng hiệu, văn phòng, ...'
    },
    {
      type: 'fanpage_url',
      title: 'Link fanpage Facebook',
      subtitle: 'Tối thiểu 500 lượt like'
    },
    {
      type: 'website_url',
      title: 'Link website',
      subtitle: 'Website đang hoạt động'
    }
  ];
  selected_verify_shop_method = 'business_license';

  constructor(
    private util: UtilService,
    private user: UserService,
    // private progressBar: ProgressBarBottomService
  ) { }

  ngOnInit() {
  }

  browseImages(type) {
    document.getElementById(type).click();
  }

  doLoading(isStart: boolean, type, index?) {
    if (isStart) {
      if (!isNaN(index) && index >= 0) {
        this.uploading[type][index] = true;
      } else {
        this.uploading[type] = true;
      }
      this.progressStartLoading();
    } else {
      if (!isNaN(index) && index >= 0) {
        this.uploading[type][index] = false;
      } else {
        this.uploading[type] = false;
      }
      this.progressEndLoading();
    }
  }

  async onFileSelected($e, type, index?) {
    this.doLoading(true, type, index);
    try {
      const { files } = $e.target;
      if (files.length == 0) return;
      // NOTE file that has invalid extension
      if (Object.values(files).some((file: File) => ACCEPT_IMAGE_FILE_EXTENSION.indexOf(file.type) == -1)) {
        toastr.error('Định dạng file không hợp lệ. Chỉ chấp nhận các định dạng: .png, .jpg, .jpeg');
        return;
      }
      const res = await this.util.uploadImages([files[0]], 250, 'ahamove_verification');

      if (!isNaN(index) && index >= 0) {
        this.img_src[type][index] = res[0].url;
      } else {
        this.img_src[type] = res[0].url;
      }
    } catch (e) {
      debug.error('ERROR in onFileSelected', e);
    }
    this.doLoading(false, type, index);
  }

  async onImageSelected($e) {
    // TODO progress bar service
    // this.progressBar.startLoading();
    try {
      const { files } = $e.target;
      if (files.length == 0) return;
      // NOTE file that has invalid extension
      if (Object.values(files).some((file: File) => ACCEPT_IMAGE_FILE_EXTENSION.indexOf(file.type) == -1)) {
        toastr.error('Định dạng file không hợp lệ. Chỉ chấp nhận các định dạng: .png, .jpg, .jpeg');
        return;
      }
      const res = await this.util.uploadImages(Object.values(files).splice(0, 6), 250, 'ahamove_verification');
      if (res) {
        let imgs = res.map(img => img.url);
        const new_images_list = this.img_src[this.selected_verify_shop_method].concat(imgs);
        if (new_images_list.length > 6) {
          new_images_list.splice(6);
        }
        this.img_src[this.selected_verify_shop_method] = new_images_list;
      }
    } catch (e) {
      debug.error("Chọn hình ảnh thất bại: ", e);
      toastr.error("Chọn hình ảnh thất bại: " + e.message);
    }
    // TODO progress bar service
    // this.progressBar.finishLoading();
  }

  deleteImage(index) {
    this.img_src[this.selected_verify_shop_method].splice(index, 1);
  }

  async updateExternalAccountAhamoveVerification() {

  }

  changeVerifyShopMethod(method) {
    this.selected_verify_shop_method = method.type;
  }

  private progressStartLoading() {
    this.display = true;
    this.percent = 0;
    this.increase();
    this.interval = setInterval(() => this.increase(), 200);
  }

  private increase() {
    if (this.percent < 90) {
      this.percent += 30;
    } else {
      clearInterval(this.interval);
    }
  }

  private progressEndLoading() {
    this.percent = 100;
    setTimeout(() => {
      this.display = false;
    }, 1000);
  }

}
