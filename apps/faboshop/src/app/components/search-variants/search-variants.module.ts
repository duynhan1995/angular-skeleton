import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchVariantsComponent } from 'apps/faboshop/src/app/components/search-variants/search-variants.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    SearchVariantsComponent
  ],
  exports: [
    SearchVariantsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
  ],
  providers: [
    InventoryStoreService,
    PurchaseOrderStoreService
  ]
})
export class SearchVariantsModule { }
