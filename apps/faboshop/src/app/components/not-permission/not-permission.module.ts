import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotPermissionComponent } from './not-permission.component';

@NgModule({
  declarations: [
    NotPermissionComponent
  ],
  exports: [
    NotPermissionComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class NotPermissionModule { }
