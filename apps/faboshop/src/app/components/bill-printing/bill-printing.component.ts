import { Fulfillment } from 'libs/models/Fulfillment';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Order } from 'libs/models/Order';
import { AuthenticateStore } from '@etop/core';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { UtilService } from 'apps/core/src/services/util.service';
import { Address, OrderAddress } from 'libs/models/Address';

@Component({
  selector: 'shop-bill-printing',
  templateUrl: './bill-printing.component.html',
  styleUrls: ['./bill-printing.component.scss']
})
export class BillPrintingComponent implements OnInit, OnChanges {
  @Input() printOnlyOrders: boolean;
  @Input() printOnlyFulfillments: boolean;
  @Input() printOrderAndFfm: boolean;
  @Input() orders: Order[] = [];
  @Input() fulfillments: Fulfillment[] = [];


  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private locationCompact: LocationCompactPipe
  ) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    setTimeout(() => JsBarcode(".shipping-barcode").init());
  }

  get shop() {
    return this.auth.snapshot.shop;
  }

  get shopAddress() {
    return this.displayAddress(this.shop.address);
  }

  money2Text(money_number) {
    return this.util.moneyToText(money_number);
  }

  displayAddress(address: OrderAddress | Address) {
    let { address1, ward, district, province } = address;
    address1 = this.locationCompact.transform(address1);
    ward = ward && this.locationCompact.transform(ward);
    district = this.locationCompact.transform(district);
    province = this.locationCompact.transform(province);
    return `${address1}${ward && ', ' + ward || ''}, ${district}, ${province}`;
  }

}
