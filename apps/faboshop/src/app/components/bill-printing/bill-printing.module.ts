import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillPrintingComponent } from 'apps/faboshop/src/app/components/bill-printing/bill-printing.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';

@NgModule({
  declarations: [
    BillPrintingComponent
  ],
  exports: [
    BillPrintingComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule
  ]
})
export class BillPrintingModule { }
