import { Component, OnInit, Input } from '@angular/core';
import { Ticket } from 'libs/models/Ticket';
import { TicketModalComponent } from 'apps/faboshop/src/app/components/modals/ticket-modal/ticket-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShopCrmService } from 'apps/faboshop/src/services/shop-crm.service';
@Component({
  selector: 'shop-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {
  @Input() tickets: Array<Ticket>;

  constructor(
    private modalController: ModalController,
    public util: UtilService,
    private shopCRM: ShopCrmService
  ) { }

  ngOnInit() {
  }

  getStatusSnake(status) {
    return status.replace(' ', '-');
  }

  async ticketDetail($e: Event, ticket: Ticket) {
    $e.preventDefault();
    const ticketComments = await this.getTicketComment(ticket.id);
    let modal = this.modalController.create({
      component: TicketModalComponent,
      showBackdrop: true,
      componentProps: {
        ticket: ticket,
        ticketComments
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  async getTicketComment(ticket_id) {
    try {
      const ticketComments = await this.shopCRM.getTicketComment(ticket_id);
      if (ticketComments) {
        ticketComments.comments = ticketComments.comments
          .slice()
          .reverse();
      }
      return ticketComments;
    } catch (e) {
      debug.error(e.message);
    }
  }

}
