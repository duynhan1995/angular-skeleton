import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketComponent } from 'apps/faboshop/src/app/components/ticket/ticket.component';
import { TicketListComponent } from 'apps/faboshop/src/app/components/ticket/ticket-list/ticket-list.component';
import { ForceButtonModule } from 'apps/faboshop/src/app/components/force-button/force-button.module';

@NgModule({
  declarations: [
    TicketComponent,
    TicketListComponent
  ],
  exports: [
    TicketComponent
  ],
  imports: [
    CommonModule,
    ForceButtonModule
  ]
})
export class TicketModule { }
