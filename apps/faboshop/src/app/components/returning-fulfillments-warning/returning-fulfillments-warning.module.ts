import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturningFulfillmentsWarningComponent } from 'apps/faboshop/src/app/components/returning-fulfillments-warning/returning-fulfillments-warning.component';

@NgModule({
  declarations: [
    ReturningFulfillmentsWarningComponent
  ],
  exports: [
    ReturningFulfillmentsWarningComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ReturningFulfillmentsWarningModule { }
