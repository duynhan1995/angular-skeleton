import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAndUpdateTagComponent } from './create-and-update-tag.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { EtopMaterialModule, MaterialModule } from '@etop/shared';



@NgModule({
  declarations: [
    CreateAndUpdateTagComponent
  ],
  exports: [
    CreateAndUpdateTagComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MaterialModule,
    EtopMaterialModule,
  ]
})
export class CreateAndUpdateTagModule { }
