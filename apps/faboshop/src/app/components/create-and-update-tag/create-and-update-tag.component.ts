import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { FbUserTag } from '@etop/models/faboshop/FbUserTag';
import { FbUser } from '@etop/models';

export interface DialogTagData {
  tag: any;
  fbUser: FbUser;
}

@Component({
  selector: 'faboshop-create-and-update-tag',
  templateUrl: './create-and-update-tag.component.html',
  styleUrls: ['./create-and-update-tag.component.scss']
})

export class CreateAndUpdateTagComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();

  listColor = [
    {
      color: '#2ecc71',
      isChecked: true,
      name: 'green'
    },
    {
      color: '#3498db',
      isChecked: false,
      name: 'blue'
    },
    {
      color: '#9b59b6',
      isChecked: false,
      name: 'purple'
    },
    {
      color: '#f39c12',
      isChecked: false,
      name : 'orange'
    },
    {
      color: '#e74c3c',
      isChecked: false,
      name: 'red'
    },
    {
      color: '#95a5a6',
      isChecked: false,
      name: 'grey'
    }
  ];

  selected;

  currentTag = new FbUserTag({});

  constructor(
    public dialogRef: MatDialogRef<CreateAndUpdateTagComponent>,
    private facebookUserTagService: FacebookUserTagService,
    @Inject(MAT_DIALOG_DATA) public data: DialogTagData,
  ) {
  }

  ngOnInit() {
    if (this.data?.tag) {
      this.listColor.map(color => {
        color.isChecked = false;
      });
      const index = this.listColor.findIndex(
        color => color.color === this.data.tag.color
      );
      this.listColor[index].isChecked = true;
      this.currentTag = JSON.parse(JSON.stringify(this.data.tag))
    }else{
      this.currentTag.color = this.listColor[0]?.color
    }
  }

  changeColor(index) {
    this.listColor.map(color => {
      color.isChecked = false;
    });
    this.listColor[index].isChecked = true;
    this.currentTag.color = this.listColor[index].color
  }


 async confirm() {
    let _data;
    if (!this.currentTag.name) {
      return toastr.error('Vui lòng nhập tên thẻ');
    }
    if (!this.currentTag.color) {
      return toastr.error('Vui lòng chọn màu');
    }
    if (this.data?.tag) {
      await this.facebookUserTagService.updateTag(this.currentTag)
      toastr.success('Chỉnh sửa thẻ thành công');
    } else {
      let tag = new FbUserTag({})
      tag.name = this.currentTag.name
      tag.color = this.currentTag.color
      _data = await this.facebookUserTagService.createTag(tag)
      this.facebookUserTagService.setNewFbUserTag(_data)
      this.submitClicked.emit(true);
      toastr.success('Thêm thẻ thành công');
    }
    this.dialogRef.close();
  }

  close(){
    this.dialogRef.close();
  }
}
