import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Message } from '@etop/models/faboshop/CustomerConversation';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';

@Component({
  selector: 'fabo-send-private-reply',
  templateUrl: './send-private-reply.component.html',
  styleUrls: ['./send-private-reply.component.scss']
})
export class SenPrivateReplyComponent implements OnInit {
  activeConversation = this.conversationsQuery.getActive();

  text: string;
  disableButton = '';

  constructor(
    private conversationsService: ConversationsService,
    private conversationsQuery: ConversationsQuery,
    @Inject(MAT_DIALOG_DATA) public data: Message,
    public dialogRef: MatDialogRef<SenPrivateReplyComponent>
  ) {
  }

  ngOnInit() {
  }

  async senMessage() {
    this.disableButton = 'disabled';
    if (!this.text) {
      toastr.error('Vui lòng nhập nội dung tin nhắn');
      this.disableButton = '';
      return;
    }
    try {
      await this.conversationsService.sendPrivateReply(this.data, this.text)
      toastr.success('Gửi tin nhắn riêng thành công.')
    } catch(e) {
      toastr.error(e.code && (e.message || e.msg) || 'Gửi tin nhắn riêng thất bại.')
    }
    this.close();
  }

  close(){
    this.dialogRef.close();
  }

}
