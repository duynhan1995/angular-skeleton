import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { EtopMaterialModule, MaterialModule } from '@etop/shared';
import { SenPrivateReplyComponent } from './send-private-reply.component';



@NgModule({
  declarations: [
    SenPrivateReplyComponent
  ],
  exports: [
    SenPrivateReplyComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MaterialModule,
    EtopMaterialModule,
  ]
})
export class SenPrivateReplyModule { }
