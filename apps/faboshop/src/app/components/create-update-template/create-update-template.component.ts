import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageTemplateQuery, MessageTemplateService } from '@etop/state/fabo/message-template';
import { MessageTemplate, MessageTemplateVariable } from '@etop/models';
import { FormBuilder } from '@angular/forms';
import { MaterialInputComponent } from '@etop/shared/components/etop-material/material-input';

@Component({
  selector: 'faboshop-create-update-template',
  templateUrl: './create-update-template.component.html',
  styleUrls: ['./create-update-template.component.scss']
})
export class CreateUpdateTemplateComponent implements OnInit {
  @ViewChild('templateInput', { static: false }) templateInput: MaterialInputComponent;
  @Output() submitClicked = new EventEmitter<any>();

  messageTemplateVariables$ = this.messageTemplateQuery.select(s => s.messageTemplateVariables);

  messageTemplate = this.fb.group({
    short_code: '',
    template: ''
  });

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: MessageTemplate,
    private messageTemplateQuery: MessageTemplateQuery,
    private messageTemplateService: MessageTemplateService,
    public dialogRef: MatDialogRef<CreateUpdateTemplateComponent>
  ) {
  }

  ngOnInit() {
    if (this.data) {
      this.messageTemplate.patchValue({
        template: this.data.template,
        short_code: this.data.short_code
      })
    }
  }

  addTemplate(messageTemplateVariable: MessageTemplateVariable) {
    const template = this.messageTemplate.controls['template'].value
    this.messageTemplate.patchValue({
      template: template + ' [' + messageTemplateVariable.label + ']',
    })
    this.templateInput.focusInput();
  }


  async confirm() {
    if (!this.messageTemplate.value.short_code) {
      toastr.error('Vui lòng nhập tiêu đề');
      return; 
    }
    if (!this.messageTemplate.value.template) {
      toastr.error('Vui lòng nhập nội dung');
      return; 
    }
    if (this.data) {
      const template: MessageTemplate = {
        ...this.data,
        short_code: this.messageTemplate.value.short_code,
        template: this.messageTemplate.value.template,
      }
      await this.messageTemplateService.updateMessageTemplate(template);
      toastr.success('Chỉnh sửa mẫu trả lời nhanh thành công.');
    } else {
      await this.messageTemplateService.createMessageTemplate(this.messageTemplate.value);
      toastr.success('Thêm mẫu trả lời nhanh thành công.');
    }
    this.submitClicked.emit(true);
    this.close();
  }

  close(){
    this.dialogRef.close();
  }

}
