import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';

@Component({
  selector: 'shop-fulfillment-shipnow-routes',
  templateUrl: './fulfillment-shipnow-routes.component.html',
  styleUrls: ['./fulfillment-shipnow-routes.component.scss']
})
export class FulfillmentShipnowRoutesComponent implements OnInit {
  @Input() ffm = new Fulfillment({});

  constructor(
    private ffmService: FulfillmentService,
    private orderStore: OrderStoreService
  ) { }

  ngOnInit() {
  }

  viewDetailOrder(order_code) {
    this.orderStore.navigate('so', order_code);
  }

  trackingLink() {
    const link = this.ffmService.trackingLink(this.ffm);
    if (!link) { return; }
    window.open(link);
  }

}
