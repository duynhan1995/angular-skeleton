import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancelObjectComponent } from 'apps/faboshop/src/app/components/cancel-object/cancel-object.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';

@NgModule({
  declarations: [
    CancelObjectComponent
  ],
  entryComponents: [
    CancelObjectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    EtopMaterialModule
  ]
})
export class CancelObjectModule { }
