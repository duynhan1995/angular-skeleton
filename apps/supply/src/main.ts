import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { SupplyRootModule } from './app/supply-root.module';
import { environment } from './environments/environment';
import * as debug from '@etop/utils/debug';
import { hmrBootstrap } from '../../core/src/hmr';


(window as any).__debug_host = '';
debug.setupDebug(environment);

if (environment.production) {
  enableProdMode();
}

(window as any).__debug_host = '';

const bootstrap = () => platformBrowserDynamic().bootstrapModule(SupplyRootModule);

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  bootstrap().catch(err => console.log(err));
}
