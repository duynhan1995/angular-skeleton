import { Injectable } from '@angular/core';
import { TradingApi } from '../api/trading-api.service';
import { SupplyCommissionSetting } from 'libs/models/affiliate/SupplyCommissionSetting';

@Injectable({ providedIn: 'root' })
export class TradingService {
  constructor(private tradingApi: TradingApi) {}

  TradingGetProducts(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 50
    };
    return this.tradingApi.TradingGetProducts({ paging, filters });
  }

  CreateOrUpdateTradingCommissionSetting(body) {
    return this.tradingApi.CreateOrUpdateTradingCommissionSetting(body);
  }

  CreateTradingProductPromotion(body) {
    return this.tradingApi.CreateTradingProductPromotion(body);
  }

  UpdateTradingProductPromotion(body) {
    return this.tradingApi.UpdateTradingProductPromotion(body);
  }
}
