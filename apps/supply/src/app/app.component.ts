import { Component, OnInit } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';

@Component({
  selector: 'supply-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit  {
  title = 'supply';

  constructor(
    private commonUsecase: CommonUsecase
  ) {}


  ngOnInit() {
    this.checkAuthorization();
  }

  checkAuthorization() {
    this.commonUsecase.checkAuthorization();
  }
}

