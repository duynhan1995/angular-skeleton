import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplyComponent } from './supply.component';

const routes: Routes = [
  {path: '', component: SupplyComponent,
    children: [{
      path: 'dashboard',
      loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
    },
    {
      path: 'products',
      loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
    },
    {
      path: 'orders',
      loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
    }
  ]
}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplyRoutingModule {}
