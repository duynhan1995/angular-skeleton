import { Component, OnInit } from '@angular/core';
import { CommonLayout } from 'apps/core/src/app/CommonLayout';
import { AuthenticateStore } from '@etop/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { map } from 'rxjs/operators';



@Component({
  selector: 'supply-app',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./supply.component.css']
})
export class SupplyComponent extends CommonLayout implements OnInit {
  sideMenus = [
    {
      name: 'Sản phẩm',
      route: ['/products'],
      icon: 'assets/images/icon_product.png'
    }
  ];

  customSidebar;

  constructor(private auth: AuthenticateStore, private commonUsecase: CommonUsecase) {
    super(auth);
  }

  async ngOnInit() {
    this.commonUsecase.checkAuthorization(true).then();
  }
}
