import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplyRoutingModule } from './supply-routing.module';
import { SupplyComponent } from './supply.component';
import { CoreModule } from '../../../../core/src/core.module';
import { TradingService } from '../../services/trading.service';
import { TradingApi } from '../../api/trading-api.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const apis = [
  TradingApi
];

const services = [
  TradingService
];

@NgModule({
  declarations: [SupplyComponent],
  imports: [CommonModule, NgbModule, BrowserModule, BrowserAnimationsModule, CoreModule.forRoot(), SupplyRoutingModule],
  providers: [
    ...apis,
    ...services
  ]
})
export class SupplyModule {
}
