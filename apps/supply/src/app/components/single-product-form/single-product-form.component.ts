import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { TradingService } from 'apps/supply/src/services/trading.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ProductApi } from '@etop/api';

@Component({
  selector: 'supply-single-product-form',
  templateUrl: './single-product-form.component.html',
  styleUrls: ['./single-product-form.component.scss']
})
export class SingleProductFormComponent implements OnInit, OnChanges {
  @ViewChild('moneyInput', { static: false })
  moneyInput: MaterialInputComponent;
  @Input() item;
  @Output() updateProduct = new EventEmitter();

  promotion;
  supply_commission;
  promotionEdited = false;
  commissionEdited = false;
  productTypes = [
    {
      name: 'Hàng hóa',
      value: 'goods'
    },
    {
      name: 'Dịch vụ',
      value: 'services'
    }
  ];

  commissionTypes = [
    {
      name: 'Theo Sản phẩm',
      value: 'product'
    },
    {
      name: 'Theo Khách hàng',
      value: 'customer'
    }
  ];

  durationType = [
    {
      name: 'Ngày',
      value: 'day'
    },
    {
      name: 'Tháng',
      value: 'month'
    },
    {
      name: 'Năm',
      value: 'year'
    }
  ];

  depend_on;
  exchangeType = 'counts';
  onTypeCount = true;
  onTypeTime = false;
  lifetime_duration_type = 'day';
  level2_direct_commission;
  level2_indirect_commission;
  level1_direct_commission;
  level1_indirect_commission;
  level1_limit_count;
  lifetime_duration;
  level1_limit_duration;
  level1_limit_duration_type = 'day';
  format_money = true;
  scs: any;
  group;

  descTabSeller = [];
  descTabMerchant = [];
  priceEdited = false;

  get noPrice() {
    // because material input send ngModel as a string **
    const _price_str: any = this.item.product.retail_price;
    return !_price_str || _price_str.length == 0;
  }

  constructor(
    private tradingService: TradingService,
    private sanitizer: DomSanitizer,
    private productService: ProductApi
  ) {}

  ngOnInit() {
    debug.log('item', this.item);
    this.scs = this.item.supply_commission_setting;
    this._prepareData();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.scs = this.item.supply_commission_setting;
    this._prepareData();
  }

  clear() {
    this.level1_direct_commission = 0;
    this.level1_indirect_commission = 0;
    this.level2_direct_commission = 0;
    this.level2_indirect_commission = 0;
    this.depend_on = 'product';
    this.level1_limit_count = 0;
    this.level1_limit_duration = 0;
    this.level1_limit_duration_type = 'day';
    this.onTypeCount = true;
    this.onTypeTime = false;
    this.group = null;
    this.descTabSeller = [];
    this.descTabMerchant = [];
  }

  _prepareData() {
    this.clear();
    if (this.item.promotion) {
      this.promotion = this.item.promotion.amount;
    }
    if (this.scs) {
      this.level1_direct_commission = this.scs.level1_direct_commission;
      this.level1_indirect_commission = this.scs.level1_indirect_commission;
      this.level2_direct_commission = this.scs.level2_direct_commission;
      this.level2_indirect_commission = this.scs.level2_indirect_commission;
      this.depend_on = this.scs.depend_on;
      this.level1_limit_count = this.scs.level1_limit_count;
      this.level1_limit_duration = this.scs.m_level1_limit_duration.duration;
      this.level1_limit_duration_type = this.scs.m_level1_limit_duration.type;
      this.group = this.scs.group;

      if (this.level1_limit_duration > 0) {
        this.onTypeTime = true;
      }
    }
    if (this.item.product.description) {
      let desc = this.item.product.description.split('&&&&');
      debug.log('text', desc[0]);
      if (desc.length > 0) {
        if (desc[0]) {
          desc[0].split('###').map(d => {
            this.descTabSeller.push({
              title: d.split('##')[0],
              link: d.split('##')[1]
            });
          });
        }
        if (desc[1]) {
          desc[1].split('###').map(d => {
            this.descTabMerchant.push({
              title: d.split('##')[0],
              link: d.split('##')[1]
            });
          });
        }
      }
    }
  }

  checkValidPrice() {
    if (this.item.product.o_data.retail_price < 0) {
      return 0;
    }
  }
  formatMoney() {
    this.item.product.retail_price = this.item.product.o_data.retail_price;
    this.format_money = !this.format_money;
    if (!this.format_money && this.moneyInput) {
      this.moneyInput.focusInput();
    }
  }
  async update() {
    try {
      if (this.promotionEdited) {
        if (this.item.promotion) {
          await this.tradingService.UpdateTradingProductPromotion({
            id: this.item.promotion.id,
            product_id: this.item.product.id,
            amount: Math.round(Number(this.promotion) * 100),
            unit: 'percent',
            type: 'cashback'
          });
        } else {
          await this.tradingService.CreateTradingProductPromotion({
            product_id: this.item.product.id,
            amount: Math.round(Number(this.promotion) * 100),
            unit: 'percent',
            type: 'cashback'
          });
        }
      }
      if (this.commissionEdited) {
        if (this.depend_on == 'product') {
          this.group = null;
        }
        await this.tradingService.CreateOrUpdateTradingCommissionSetting({
          product_id: this.item.product.id,
          level1_direct_commission: Number(this.level1_direct_commission),
          level1_indirect_commission: Number(this.level1_indirect_commission),
          level2_direct_commission: Number(this.level2_direct_commission),
          level2_indirect_commission: Number(this.level2_indirect_commission),
          depend_on: this.depend_on,
          level1_limit_count: this.level1_limit_count,
          level1_limit_duration: this.level1_limit_duration,
          level1_limit_duration_type: this.level1_limit_duration_type,
          lifetime_duration_type: this.lifetime_duration_type,
          group: this.group
        });
      }
      toastr.success('Cập nhật thành công!');
      this.updateProduct.emit();
    } catch (e) {
      toastr.error('Cập nhật thất bại', e.message);
    }
  }

  async onChangePromtion() {
    this.promotionEdited = true;
  }

  async onChangeCommission(value) {
    if (value > 100 || value < 0) {
      toastr.error('Giá trị không hợp lệ!');
      return;
    }
    this.commissionEdited = true;
  }

  onChangeDependOn() {
    this.commissionEdited = true;
  }

  onChangePrice(type) {
    let revenue;
    if (this.promotion > 0) {
      revenue = Math.round(
        this.item.product.retail_price * (1 - this.promotion / 100)
      );
    } else {
      revenue = this.item.product.retail_price;
    }
    switch (type) {
      case 'oldDirect':
        return (this.level1_direct_commission * revenue) / 100;
      case 'oldIndirect':
        return (this.level1_indirect_commission * revenue) / 100;
      case 'newDirect':
        return (this.level2_direct_commission * revenue) / 100;
      case 'newIndirect':
        return (this.level2_indirect_commission * revenue) / 100;
      default:
        break;
    }
  }

  safeUrl(url) {
    let _url = this.sanitizer.bypassSecurityTrustUrl(url);
    return _url;
  }

  timeDisplay(type) {
    switch (type) {
      case 'day':
        return 'Số ngày';
      case 'month':
        return 'Số tháng';
      case 'year':
        return 'Số năm';
      default:
        break;
    }
  }

  checkValue(value) {
    debug.log('value', value);
  }

  addSellerTab() {
    this.descTabSeller.push({ title: '', link: '' });
  }

  delSellerTab(index) {
    this.descTabSeller.splice(index, 1);
  }

  addMerchantTab() {
    this.descTabMerchant.push({ title: '', link: '' });
  }

  delMerchantTab(index) {
    this.descTabMerchant.splice(index, 1);
  }
  async updateTab() {
    let descTabSeller = '';
    let descTabMerchant = '';

    this.descTabSeller.forEach(e => {
      if (e.title == '') {
        toastr.error('Vui lòng không để tab rỗng!');
        throw 0;
      }
      if (e.title != this.descTabSeller[this.descTabSeller.length - 1].title) {
        descTabSeller += e.title + '##' + e.link + '###';
      } else {
        descTabSeller += e.title + '##' + e.link;
      }
    });
    this.descTabMerchant.forEach(e => {
      if (e.title == '') {
        toastr.error('Vui lòng không để tab rỗng!');
        throw 0;
      }
      if (
        e.title != this.descTabMerchant[this.descTabMerchant.length - 1].title
      ) {
        descTabMerchant += e.title + '##' + e.link + '###';
      } else {
        descTabMerchant += e.title + '##' + e.link;
      }
    });
    let desc = descTabSeller + '&&&&' + descTabMerchant;
    await this.productService.updateProduct({
      description: desc,
      id: this.item.product.o_data.id
    });
    this.updateProduct.emit();
    toastr.success('Cập nhật tab mở rộng thành công!');
  }

  async updateProductInfo() {
    try {
      const data = {
        code: this.item.product.o_data.code,
        id: this.item.product.o_data.id,
        name: this.item.product.o_data.name,
        short_desc: this.item.product.o_data.short_desc,
        product_type: this.item.product.product_type
      };
      await this.productService.updateProduct(data);
      if (this.priceEdited) {
        await this.productService.updateVariant({
          id: this.item.product.variants[0].id,
          retail_price: this.item.product.o_data.retail_price
        });
      }
      this.updateProduct.emit();
      toastr.success('Cập nhật sản phẩm thành công!');
    } catch (e) {
      toastr.error('Lỗi cập nhật sản phẩm: ', e.message);
    }
  }

  onChangePriceProduct () {
    this.priceEdited = true;
  }
}
