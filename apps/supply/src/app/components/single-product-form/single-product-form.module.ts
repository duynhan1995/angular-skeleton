import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SingleProductFormComponent } from './single-product-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [SingleProductFormComponent],
  imports: [CommonModule, NgbModule, FormsModule, SharedModule],
  exports: [SingleProductFormComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleProductFormModule {}
