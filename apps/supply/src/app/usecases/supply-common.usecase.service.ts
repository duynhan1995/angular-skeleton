import { Injectable } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AuthenticateStore } from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { Router } from '@angular/router';
import { Account } from 'libs/models/Account';
import { UserApi } from '@etop/api';
import { ConfigService } from '@etop/core/services/config.service';

const SPECIAL_ROUTES = {
  login: {
    display_on_mobile: true,
    require_login: false
  },
  register: {
    display_on_mobile: true,
    require_login: false
  },
  'top-ship': {
    display_on_mobile: true,
    require_login: false
  },
  tracking: {
    display_on_mobile: true,
    require_login: false
  },
  'reset-password': {
    display_on_mobile: true,
    require_login: false
  },
  'on-boarding': {
    display_on_mobile: true,
    require_login: true
  },
  'survey': {
    display_on_mobile: true,
    require_login: true
  },
  'admin-login': {
    display_on_mobile: true,
    require_login: false
  },
  'verify-email': {
    display_on_mobile: true,
    require_login: true
  }
};

@Injectable()
export class SupplyCommonUsecase extends CommonUsecase {
  app = 'dashboard';
  full_name;
  password;

  error = false;
  errorMessage = '';

  email;
  confirm;

  signupData: any = {};

  loading = false;

  provinces;
  resetPasswordLoading = false;
  formForgot: any = {};

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore,
    private config: ConfigService
  ) {
    super();
  }

  async checkAuthorization(fetchAccounts = false) {
    const ref = this.router.url.split('?')[0];
    try {
      const url = window.location.pathname.replace('/', '');
      const special = SPECIAL_ROUTES[url];
      if (special && !special.needLogin) {
        this.auth.setRef(this.router.url);
        return;
      }
      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(fetchAccounts);
    } catch (e) {
      debug.error('ERROR in checking authorization', e);
      this.auth.clear();
      this.auth.setRef(ref);
      this.router.navigateByUrl('/login');
    }
  }

  async navigateAfterAuthorized() {
    debug.log('navigateAfterAuthorized');
    await this.router.navigateByUrl('/products');
  }

  async updateSessionInfo(fetchAccounts = false) {
    const res = await this.userService.checkToken(this.auth.snapshot.token);
    const { access_token, account, shop, user, available_accounts } = res;
    if (shop) {
      const shop_accounts = available_accounts.filter(a => a.id === this.config.getConfig().shop_trading);
      const accounts: Account[] = fetchAccounts ? await Promise.all(shop_accounts.map(async (a, index) => {
        const accRes = await this.userService.switchAccount(a.id, 'APP COMPONENT');
        a.token = accRes.access_token;
        a.shop = accRes.shop;
        return a;
      })) : this.auth.snapshot.accounts;
      this.auth.updateInfo({
        token: access_token,
        account,
        shop,
        user,
        accounts: accounts,
        isAuthenticated: true
      });
      let selected = this.auth.selectAccount(0);
      this.navigateAfterAuthorized();
    } else {
      throw new Error('Shop is not_found');
    }
  }

  async login(data: { login: string; password: string }) {
    try {
      const res = await this.userApi.login({
        login: data.login,
        password: data.password,
        account_type: 'shop'
      });
      this.auth.updateToken(res.access_token);
      await this.updateSessionInfo(true).then(() => this.router.navigateByUrl('/products'));
    } catch (e) {
      debug.error('login', e);
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
  }

  async register(data: any, source = '') {
    const res = await this.userService.signUp({ ...data, source });
    this.signupData = Object.assign({}, data, res.user);
    toastr.success('Đăng ký thành công!');
    this.login({
      login: this.signupData.email, password: this.signupData.password
    });
  }

  async redirectIfAuthenticated(): Promise<any> {
    return undefined;
  }
}
