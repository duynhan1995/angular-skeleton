import { Component, OnInit, ViewChild } from '@angular/core';
import { TradingService } from 'apps/supply/src/services/trading.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { Product } from 'libs/models/Product';
import { pipe } from 'rxjs';

@Component({
  selector: 'supply-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  products: any;
  checkList = [];
  page: number;
  perpage: number;
  checkAll = false;

  @ViewChild('productTable', { static: true }) productTable: EtopTableComponent;
  @ViewChild('productSlide', { static: true })
  productSlide: SideSliderComponent;

  private _selectMode = false;
  private _onSelectModeChanged(value) {
    this.productTable.toggleLiteMode(value);
    this.productSlide.toggleLiteMode(value);
  }
  constructor(private tradingService: TradingService) {}

  async ngOnInit() {
  }

  onSlideClosed() {
    this.products.forEach(p => (p.product.p_data.selected = false));
    this.checkList = [];
    this.selectMode = false;
  }
  onPagination({ page, perpage }) {
    this.page = page;
    this.perpage = perpage;
    setTimeout(async () => {
      this.productTable.loading = true;
      this.perpage = perpage;
      await this.TradingGetProducts(page, perpage);
      this.productTable.loading = false;
    }, 0);
  }

  checkAllProduct() {
    this.checkAll = !this.checkAll;
    this._checkSelectMode();
  }

  detail(item) {
    this.checkAll = false;
    this.products.forEach(p => {
      p.product.p_data.selected = false;
    });
    item.product.p_data.selected = true;
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    this.checkList = this.products.filter(function(item) {
      return item.product.p_data.selected == true;
    });

    this.selectMode = this.checkList.length > 0;
  }

  async TradingGetProducts(page, perpage) {
    this.products = await this.tradingService.TradingGetProducts(
      (page - 1) * perpage,
      perpage
    );
    this.products.map(item => {
      item.product = this.productMap(item.product);
    });
  }

  productMap(product) {
    return (product = new Product(product));
  }

  async reloadProduct() {
    let _products = await this.tradingService.TradingGetProducts(
      (this.page - 1) * this.perpage,
      this.perpage
    );
    _products.map(item => {
      item.product = this.productMap(item.product);
    });
    _products.map(p => {
      if (p.product.id == this.checkList[0].product.id) {
        p.product.p_data.selected = true
      }
    });
    this.products = _products;
  }
}
