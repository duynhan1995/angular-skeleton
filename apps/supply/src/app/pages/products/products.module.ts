import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductsComponent } from './products.component';
import { ProductRowComponent } from './product-row/product-row.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SingleProductFormModule } from '../../components/single-product-form/single-product-form.module';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [
    ProductsComponent,
    ProductRowComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    SharedModule,
    SingleProductFormModule,
    RouterModule.forChild(routes)
  ],
  exports: [ProductsComponent]
})
export class ProductsModule {}
