import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[supply-product-row]',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss']
})
export class ProductRowComponent implements OnInit {
  @Input() item;
  @Input() liteMode = false;
  @Output() change = new EventEmitter();

  promotion_convert;
  level1_direct_commission_convert;

  constructor() {}

  ngOnInit() {
    this.item = this.productMap(this.item);
  }

  check(item, e) {
    item.product.p_data.selected = !item.product.p_data.selected;
    this.change.emit({ item, e });
  }
  productMap(item) {
    let etop_revenue;
    if (item.promotion) {
      etop_revenue = Math.round(item.product.retail_price * (1 - item.promotion.amount / 100));
      this.promotion_convert = Math.round(item.promotion.amount * item.product.retail_price / 100);
    } else {
      etop_revenue = item.product.retail_price;
    }
    if (item.supply_commission_setting) {
      this.level1_direct_commission_convert = item.supply_commission_setting.level1_direct_commission * etop_revenue / 100;
    }
    return Object.assign(item, {
      etop_revenue: etop_revenue,
      promotion_convert: this.promotion_convert,
      level1_direct_commission_convert: this.level1_direct_commission_convert
    });
  }
}
