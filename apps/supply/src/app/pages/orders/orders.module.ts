import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrdersComponent } from './orders.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [OrdersComponent],
  imports: [CommonModule, NgbModule, RouterModule.forChild(routes)],
  exports: [OrdersComponent]
})
export class OrdersModule {}
