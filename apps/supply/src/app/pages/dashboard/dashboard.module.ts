import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [{ path: '' }]
  }
];

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, NgbModule, RouterModule.forChild(routes)],
  exports: [DashboardComponent]
})
export class DashboardModule {}
