import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SupplyModule } from './supply/supply.module';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'apps/shared/src/pages/login/login.component';
import { LoginModule } from 'apps/shared/src/pages/login/login.module';
import { CommonModule } from '@angular/common';
import { SupplyCommonUsecase } from './usecases/supply-common.usecase.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { SharedModule } from 'apps/shared/src/shared.module';
import { CONFIG_TOKEN } from '@etop/core/services/config.service';
import { environment } from '../environments/environment';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    SupplyModule,
    LoginModule,
    CommonModule,
    SharedModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    { provide: CommonUsecase, useClass: SupplyCommonUsecase },
    {
      provide: CONFIG_TOKEN,
      useValue: environment
    }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupplyRootModule {
}
