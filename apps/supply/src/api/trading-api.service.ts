import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { ListQueryDTO } from 'libs/models/CommonQuery';
import { DurationType, SupplyCommissionSetting } from 'libs/models/affiliate/SupplyCommissionSetting';
import { Product } from 'libs/models/Product';
import { Promotion } from 'libs/models/affiliate/Promotion';

class SupplyCommissionSettingRequest extends SupplyCommissionSetting {
  level1_limit_duration: number;
  level1_limit_duration_type: DurationType;
  lifetime_duration: number;
  lifetime_duration_type: DurationType;
}

@Injectable({ providedIn: 'root' })
export class TradingApi {
  constructor(private http: HttpService) {}

  TradingGetProducts(query: Partial<ListQueryDTO>, token?: string) {
    return this.http
      .post('api/affiliate.Trading/TradingGetProducts', query, token)
      .toPromise()
      .then(res => res.products.map(p => {
        const scs = p.supply_commission_setting;
        return {
          product: new Product({
            ...p.product,
            retail_price: p.product.variants[0].retail_price,
            list_price: p.product.variants[0].list_price,
            cost_price: p.product.variants[0].cost_price
          }),
          promotion: p.promotion && new Promotion({...p.promotion,
            amount: p.promotion.amount / 100
          }),
          supply_commission_setting: scs && new SupplyCommissionSetting({
            ...scs,
            level1_direct_commission: scs.level1_direct_commission / 100,
            level1_indirect_commission: scs.level1_indirect_commission / 100,
            level2_direct_commission: scs.level2_direct_commission / 100,
            level2_indirect_commission: scs.level2_indirect_commission / 100,
          })
        };
      }));
  }

  async CreateOrUpdateTradingCommissionSetting(body: SupplyCommissionSettingRequest) {
    body.level1_direct_commission = Math.round(Number(body.level1_direct_commission) * 100);
    body.level1_indirect_commission = Math.round(Number(body.level1_indirect_commission) * 100);
    body.level2_direct_commission = Math.round(Number(body.level2_direct_commission) * 100);
    body.level2_indirect_commission = Math.round(Number(body.level2_indirect_commission) * 100);
    return this.http
      .post(`api/affiliate.Trading/CreateOrUpdateTradingCommissionSetting`, body)
      .toPromise()
      .then(res => new SupplyCommissionSetting({
        ...res,
        level1_direct_commission: res.level1_direct_commission / 100,
        level1_indirect_commission: res.level1_indirect_commission / 100,
        level2_direct_commission: res.level2_direct_commission / 100,
        level2_indirect_commission: res.level2_indirect_commission / 100,
      }))
  }

  async CreateTradingProductPromotion(body) {
    return this.http
      .post(`api/affiliate.Trading/CreateTradingProductPromotion`, body)
      .toPromise()
      .then(res => res);
  }

  async UpdateTradingProductPromotion(body) {
    return this.http
      .post(`api/affiliate.Trading/UpdateTradingProductPromotion`, body)
      .toPromise()
      .then(res => res);
  }
}
