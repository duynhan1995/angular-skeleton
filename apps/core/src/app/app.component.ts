import { AfterViewInit, Component } from '@angular/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { HelloWorldComponent } from 'apps/core/src/app/hello-world/hello-world.component';

@Component({
  selector: 'core-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'core';
  sideMenus = [
    {
      name: 'Tổng quan',
      route: ['./dashboard'],
      icon: 'assets/images/e_dashboard.png'
    },
    {
      name: 'Sản phẩm',
      route: ['./shop-product'],
      icon: 'assets/images/e_product.png'
    },
    {
      name: 'POS - Tạo đơn hàng',
      route: ['./create-order'],
      icon: 'assets/images/e_pos.png'
    },
    {
      name: 'Đơn hàng của tôi',
      route: ['./orders'],
      icon: 'assets/images/e_order.png'
    },
    {
      name: 'TOP SHIP',
      route: ['./fulfillments'],
      icon: 'assets/images/e_topship.png',
      submenus: [
        {
          name: 'Mới',
          params: { active: 'created' }
        },
        {
          name: 'Đang lấy',
          params: { active: 'picking' }
        },
        {
          name: 'Đang giao',
          params: { active: 'delivering' }
        },
        {
          name: 'Đang trả',
          params: { active: 'returning' }
        },
        {
          name: 'Chờ giao',
          params: { active: 'holding' }
        },
        {
          name: 'Đã giao hàng',
          params: { active: 'delivered' }
        },
        {
          name: 'Đã trả hàng',
          params: { active: 'returned' }
        },
        {
          name: 'Đã hủy',
          params: { active: 'cancelled' }
        }
      ],
      otherMenus: [
        {
          name: 'Hướng dẫn',
          href: 'https://topship.vn/huong-dan'
        }
      ]
    },
    {
      name: 'Phiên chuyển tiền',
      route: ['./money-transactions'],
      icon: 'assets/images/e_transaction.png'
    },
    {
      name: 'Thiết lập cửa hàng',
      route: ['./shop-setting'],
      icon: 'assets/images/e_settings_shop.png',
      submenus: [
        {
          name: 'Cửa hàng',
          params: { active: 'shop-info' }
        },
        {
          name: 'Giao hàng',
          params: { active: 'shipping-info' }
        },
        {
          name: 'Danh mục',
          params: { active: 'categories' }
        },
        {
          name: 'Bộ sưu tập',
          params: { active: 'collections' }
        }
      ]
    },
    {
      name: 'Thiết lập tài khoản',
      route: ['./account-setting'],
      icon: 'assets/images/e_settings_account.png',
      submenus: [
        {
          name: 'Danh sách cửa hàng',
          params: { active: 'shop' }
        },
        {
          name: 'Tài khoản',
          params: { active: 'account' }
        }
      ]
    },
    {
      name: 'Hướng dẫn sử dụng',
      href: 'https://www.etop.vn/huong-dan',
      route: ['https://www.etop.vn/huong-dan'],
      icon: 'assets/images/e_help.png'
    },
    {
      name: 'Báo lỗi',
      route: ['./bug-report'],
      icon: 'assets/images/e_bug.png'
    }
  ];

  constructor(private modalController: ModalController) {
  }

  ngAfterViewInit(): void {
    this.showModal();
  }

  async showModal() {
    let modal = this.modalController.create({
      component: HelloWorldComponent,
      showBackdrop: false,
      componentProps: {
        name: 'Mạnh'
      },
    });
    modal.show().then(e => console.log("MODAL SHOWED"));
    modal.onDismiss().then(e => console.log('Modal dismiss', e));
  }
}

