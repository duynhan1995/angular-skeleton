import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from 'apps/core/src/core.module';
import { HelloWorldComponent } from 'apps/core/src/app/hello-world/hello-world.component';
import { ContentOptionComponent } from 'apps/core/src/components/header/components/content-option/content-option.component';
import { MaterialModule } from '@etop/shared';

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    ContentOptionComponent
  ],
  entryComponents: [HelloWorldComponent],
  imports: [BrowserModule, RouterModule.forRoot([]), FormsModule, CoreModule, MaterialModule],
  providers: [],
  bootstrap: [AppComponent],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
