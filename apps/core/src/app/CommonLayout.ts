import { Account, ExtendedAccount } from 'libs/models/Account';
import { User } from 'libs/models';
import { Noti } from 'libs/models/Noti';
import { Observable } from 'rxjs';
import { AuthenticateStore } from '@etop/core';
import { map } from 'rxjs/operators';
import { MenuItem } from 'apps/core/src/components/menu-item/menu-item.interface';

export abstract class CommonLayout {
  public sideMenus: Array<MenuItem> = [];
  public currentAccount$: Observable<Account>;
  public accounts$: Observable<Array<Account>>;
  public user$: Observable<User>;
  public notifications: Array<Noti>;
  public hideSidebar = false;
  public hideHeader = false;
  public isSingle = false;
  public showNotificationIcon = true;
  public showVerifyWarning = false;
  public adminLoginMode = false;
  public showForgotPassword = true;
  public showNewSidebar = false;
  public showTenantWarning = false;
  public config: LayoutConfig = {account_type_display: 'cửa hàng'};

  protected constructor(auth: AuthenticateStore) {
    this.currentAccount$ = auth.state$.pipe(map(({account}) => account));
    this.accounts$ = auth.authenticatedData$.pipe(map(({ accounts }) => accounts));
    this.user$ = auth.authenticatedData$.pipe(map(({user}) => user));
  }
}

export interface LayoutConfig{
  account_type_display: string
}
