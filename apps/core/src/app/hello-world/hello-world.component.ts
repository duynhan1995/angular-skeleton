import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'core-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {
  dismiss: (data) => void;

  @Input() name: string;

  constructor(private modalController: ModalController, private modalDismiss: ModalAction) { }

  ngOnInit() {
  }

  click(e) {
    this.modalDismiss.dismiss(this);
  }

}
