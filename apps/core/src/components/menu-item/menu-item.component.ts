import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from 'apps/core/src/components/menu-item/menu-item.interface';
import { takeUntil } from 'rxjs/operators';
import { MenuItemStoreService } from 'apps/core/src/components/menu-item/menu-item.store.service';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'core-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent extends BaseComponent implements OnInit {
  @Input() menu: MenuItem;
  hovered = false;

  showAll = false;

  constructor(
    private menuItemStore: MenuItemStoreService
  ) {
    super();
  }

  ngOnInit() {
    this.menuItemStore.onMenuHovered$
      .pipe(takeUntil(this.destroy$))
      .subscribe(menu_string => {
        this.hovered = menu_string == JSON.stringify(this.menu);
      });
  }

  onHoverMenu(menu: MenuItem) {
    this.menuItemStore.hoverMenu(menu && JSON.stringify(menu) || null);
  }

  showMore() {
    this.showAll = true;
  }

  collapse() {
    this.showAll = false;
  }

  get firstItems(): any[] {
    return (
      this.menu && this.menu.submenus && this.menu.submenus.slice(0, 4) || []
    );
  }

  get canExpand(): boolean {
    return (
      this.menu && this.menu.submenus && this.menu.submenus.length > 4
    );
  }

  checkFunction(menu){
    if(typeof menu.onClick === 'function') {
      return  menu.onClick();
    }
    return ;
  }
}
