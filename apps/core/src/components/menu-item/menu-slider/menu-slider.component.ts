import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from 'apps/core/src/components/menu-item/menu-item.interface';
import { BaseComponent } from '@etop/core';
import { MenuItemStoreService } from 'apps/core/src/components/menu-item/menu-item.store.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'core-menu-slider',
  templateUrl: './menu-slider.component.html',
  styleUrls: ['./menu-slider.component.scss']
})
export class MenuSliderComponent extends BaseComponent implements OnInit {
  @Input() menu: MenuItem;
  hovered = false;

  constructor(
    private menuItemStore: MenuItemStoreService
  ) {
    super();
  }

  ngOnInit() {
    this.menuItemStore.onMenuHovered$
      .pipe(takeUntil(this.destroy$))
      .subscribe(menu_string => {
        this.hovered = menu_string == JSON.stringify(this.menu);
      });
  }

  onHoverMenu(menu: MenuItem) {
    this.menuItemStore.hoverMenu(menu && JSON.stringify(menu) || null);
  }

}
