import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface MenuItemData {
  hoveredMenuString: string
}

@Injectable({
  providedIn: 'root'
})
export class MenuItemStoreService extends AStore<MenuItemData> {
  initState = {
    hoveredMenuString: null
  };

  readonly onMenuHovered$ = this.state$.pipe(
    map(({hoveredMenuString}) => hoveredMenuString),
    distinctUntilChanged()
  );

  constructor() {
    super();
    this.state = this.initState;
  }

  hoverMenu(menu_string: string) {
    this.setState({hoveredMenuString: menu_string});
  }

}
