import {Component, OnInit, Input, ChangeDetectorRef} from '@angular/core';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import {BreadcrumbMenu} from 'apps/core/src/components/header/components/breadcrumb/breadcrumb.component';
import {MenuItem} from 'apps/core/src/components/menu-item/menu-item.interface';

@Component({
  selector: 'core-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() menus: Array<MenuItem> = [];

  currentItem: BreadcrumbMenu = {name: '', name_custom: ''};

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private ref: ChangeDetectorRef
  ) {
  }


  ngOnInit() {
    this.updateActiveItem();
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.updateActiveItem();
      }
    });
  }

  updateActiveItem() {
    let activeMenuItem = this.menus.find(menu =>
      this.router.isActive(this.router.createUrlTree(menu.route, {relativeTo: this.activatedRoute}), false)
    );
    let activeSubMenuItem = activeMenuItem && activeMenuItem.submenus && activeMenuItem.submenus.length
      && activeMenuItem.submenus.find(
        submenu => this.router.isActive(
          this.router.createUrlTree(activeMenuItem.route.concat(submenu.route),
            {relativeTo: this.activatedRoute}),
          false
        )
      );

    this.currentItem = (activeSubMenuItem && {
      name: activeSubMenuItem.name,
      name_custom: ''
    }) || (activeMenuItem && {name: activeMenuItem.name, name_custom: activeMenuItem.name_custom}) || {
      name: '',
      name_custom: ''
    };
    this.ref.markForCheck();
  }

}
