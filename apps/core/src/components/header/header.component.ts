import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges, OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';

import {HeaderControllerService} from './header-controller.service';
import {ExtendedAccount, User, Noti} from '@etop/models';
import {ActionOpt} from './components/action-button/action-button.interface';
import {TabOpt} from './components/tab-options/tab-options.interface';
import {AuthenticateStore} from '@etop/core';
import {Subscription} from 'rxjs';
import {HeaderButtonOpt} from 'apps/core/src/components/header/components/header-button/header-button.interface';
import {MenuItem} from 'apps/core/src/components/menu-item/menu-item.interface';
import {LayoutConfig} from "../../app/CommonLayout";
import {UtilService} from "../../services/util.service";

@Component({
  selector: 'core-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnChanges, OnDestroy {
  @Input() activatedMenu: MenuItem;
  @Input() currentAccount: ExtendedAccount = new ExtendedAccount({});
  @Input() accounts = [];
  @Input() user: User = new User({});
  @Input() isSingle;
  @Input() showForgotPassword = true;
  @Input() showNotificationIcon;
  @Input() config: LayoutConfig;

  notificationList: Array<Noti> = [];
  loadingNotis = false;

  tabs: TabOpt[] = [];
  // NOTE: buttons which go everywhere, not depending on any view!
  header_buttons: HeaderButtonOpt[] = [];
  actions: ActionOpt[] = [];
  load_done_subscription = new Subscription();
  contents: any;
  mobileContents: any;

  constructor(
    private headerController: HeaderControllerService,
    private ref: ChangeDetectorRef,
    public authStore: AuthenticateStore,
  ) {
  }

  ngOnInit() {
    this.headerController.registerInstance(this);
  }

  ngOnDestroy() {
    this.load_done_subscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ref.markForCheck();
  }

  setHeaderButtons(btns: HeaderButtonOpt[]) {
    this.header_buttons = btns;
    this.ref.markForCheck();
  }

  clearHeaderButtons() {
    this.header_buttons = [];
    this.ref.markForCheck();
  }

  setActions(actions: ActionOpt[]) {
    this.actions = actions;
    this.ref.markForCheck();
  }

  clearActions() {
    this.actions = [];
    this.ref.markForCheck();
  }

  setTabs(tabs: TabOpt[]) {
    this.tabs = tabs;
    this.ref.markForCheck();
  }

  setContent(content) {
    this.contents = content;
    this.ref.markForCheck();
  }

  clearContent() {
    this.contents = '';
    this.ref.markForCheck();
  }

  setMobileContent(content) {
    this.mobileContents = content;
    this.ref.markForCheck();
  }

  clearMobileContent() {
    this.mobileContents = '';
    this.ref.markForCheck();
  }

  clearTabs() {
    this.tabs = [];
    this.ref.markForCheck();
  }

  onGoToSettings() {
    this.headerController.navigate(this, {target: 'setting'});
  }

  onSwitchAccount(account) {
    this.headerController.navigate(this, {
      payload: account,
      target: 'account'
    });
  }

  get notificationCount() {
    return this.notificationList.filter(n => !n.is_read).length;
  }

  get rolesOfActionsArray() {
    let roles = [];
    this.actions.forEach(a => {
      if (a.roles && a.roles.length) {
        roles = roles.concat(a.roles);
      }
    });
    return roles;
  }

  get permissionsOfActionsArray() {
    let permissions = [];
    this.actions.forEach(a => {
      if (a.permissions && a.permissions.length) {
        permissions = permissions.concat(a.permissions);
      }
    });
    return permissions;
  }

  loadNotifications(notifications: Array<Noti>) {
    this.notificationList = notifications;
    this.loadingNotis = false;
    this.ref.markForCheck();
  }

  requestMoreNotis(token) {
    this.loadingNotis = true;
    this.headerController.requestMoreNotis(token);
  }

  onReadNoti(noti) {
    this.headerController.navigate(this, {payload: noti, target: 'notification'})
  }

  filterNotisByAccount(token) {
    this.headerController.filterNotisByAccount(token);
  }

  onViewAllNotis() {
    this.headerController.navigate(this, {target: 'notifications'});
  }

}
