import { Component, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'core-content-option',
  templateUrl: './content-option.component.html',
  styleUrls: ['./content-option.component.scss']
})
export class ContentOptionComponent implements OnInit {
  @Input() contentOption;

  constructor() {}

  ngOnInit() {}
}
