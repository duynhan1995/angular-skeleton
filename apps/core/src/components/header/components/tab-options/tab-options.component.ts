import { Component, Input, OnInit } from '@angular/core';
import { TabOpt } from './tab-options.interface';

@Component({
  selector: 'core-tab-options',
  templateUrl: './tab-options.component.html',
  styleUrls: ['./tab-options.component.scss']
})
export class TabOptionsComponent implements OnInit {

  @Input() tabs: TabOpt[];

  constructor() {
  }

  get permissionsOfTabsArray() {
    let permissions = [];
    this.tabs.forEach(t => {
      if (t.permissions && t.permissions.length) {
        permissions = permissions.concat(t.permissions);
      }
    });
    return permissions;
  }

  ngOnInit() {
  }

  handleClick(tab: TabOpt) {
    this.tabs.forEach(t => t.active = false);
    tab.active = true;
    if (tab.onClick) {
      tab.onClick();
    }
  }

}
