export interface TabOpt {
  title: string;
  name?: string;
  active: boolean;
  onClick: () => void;
  permissions?: string[];
  cssClass?: string;

  icon?: string; // NOTE: use with <mat-icon>{{icon}}</mat-icon> refer https://material.io/resources/icons for more icons
  iconOutline?: boolean; // NOTE: use with <mat-icon fontSet="material-icons-outlined">{{icon}}</mat-icon> refer https://material.io/resources/icons for more icons
}
