import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HeaderButtonOpt } from 'apps/core/src/components/header/components/header-button/header-button.interface';

@Component({
  selector: 'core-header-button',
  templateUrl: './header-button.component.html',
  styleUrls: ['./header-button.component.scss']
})
export class HeaderButtonComponent implements OnInit {
  @Input() button: HeaderButtonOpt;

  @Output() click = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  handleClick() {
    this.click.emit();
    if (this.button && this.button.onClick) {
      this.button.onClick();
    }
  }

}
