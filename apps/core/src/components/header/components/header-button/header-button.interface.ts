export interface HeaderButtonOpt {
  title: string;
  onClick?: () => void;
  cssClass: string;
  titleClass?: string;
  icon?: string;
  loading?: boolean;
}
