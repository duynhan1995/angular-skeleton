import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionDropdown, ActionOpt } from './action-button.interface';

@Component({
  selector: 'core-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.scss']
})
export class ActionButtonComponent implements OnInit {

  @Input() actionOpt: ActionOpt;

  @Output() click = new EventEmitter();

  constructor() { }

  get permissionsOfDropdownsArray() {
    let permissions = [];
    if (this.actionOpt && this.actionOpt.dropdownItems)
    this.actionOpt.dropdownItems.forEach(item => {
      if (item.permissions && item.permissions.length) {
        permissions = permissions.concat(item.permissions);
      }
    });
    return permissions;
  }

  ngOnInit() {
  }

  handleClick(item?: ActionDropdown) {
    this.click.emit();
    if (item && item.onClick) {
      item.onClick();
    }
    if (this.actionOpt && this.actionOpt.onClick) {
      this.actionOpt.onClick();
    }
  }

}
