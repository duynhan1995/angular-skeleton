export interface ActionOpt {
  title: string;
  onClick?: () => void;
  cssClass: string;
  icon?: string;
  loading?: boolean;
  isDropdown?: boolean;
  tooltips?: string;
  disabled?: boolean;
  dropdownItems?: Array<ActionDropdown>;
  permissions?: string[];
  roles?: string[];
  mobileHidden?: boolean;
}

export interface ActionDropdown {
  title: string;
  tags?: Array<string>;
  images?: Array<ActionImage>;
  onClick?: () => void;
  tooltips?: string;
  disabled?: boolean;
  permissions?: string[];
}

export interface ActionImage {
  url: string;
  unavailable?: boolean;
}
