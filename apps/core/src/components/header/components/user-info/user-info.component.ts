import {Component, OnInit, Input, OnChanges, SimpleChanges, HostListener, Output, EventEmitter} from '@angular/core';
import {ExtendedAccount, Account} from 'libs/models/Account';
import {Router} from '@angular/router';
import {User} from 'libs/models';
import {AuthenticateStore} from '@etop/core';
import {UtilService} from 'apps/core/src/services/util.service';
import {ModalController} from '../../../modal-controller/modal-controller.service';
import {ChangePasswordModalComponent} from 'apps/shared/src/pages/login/modals/change-password-modal/change-password-modal.component';
import {AppService} from '@etop/web/core/app.service';
import {StorageService} from 'apps/core/src/services/storage.service';
import {NotificationApi} from '@etop/api';
import {LayoutConfig} from "../../../../app/CommonLayout";

@Component({
  selector: 'core-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit, OnChanges {
  @Output() goToSettings = new EventEmitter();
  @Output() switchAccount = new EventEmitter();

  @Input() currentAccount: ExtendedAccount;
  @Input() accounts: Array<ExtendedAccount>;
  @Input() user: User;
  @Input() config: LayoutConfig;

  isMobile = false;
  show_dropdown = false;
  more_shops = false;
  click_inside = false;

  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    if (event.target.id == "settings") {
      this.click_inside = false;
      this.show_dropdown = false;
    }
    this.click_inside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.click_inside) {
      this.show_dropdown = false;
    }
    this.click_inside = false;
  }

  constructor(
    public router: Router,
    private util: UtilService,
    private notificationApi: NotificationApi,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private appService: AppService,
    private storageService: StorageService
  ) {
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }

  ngOnInit() {
    this.auth.authenticatedData$.subscribe(info => {
      this.currentAccount = {...info.shop} || new ExtendedAccount({});
    });
  }


  ngOnChanges(changes: SimpleChanges) {
  }

  onSwitchAccount(account: ExtendedAccount) {
    this.switchAccount.emit(account);
  }

  onChangePassword() {
    if (this.appService.appID == "imgroup") {
      let win = window.open(`https://id.imgroup.vn/profile`, '_blank');
      win.focus();
    } else {
      this.openChangePasswordModal()
    }
  }

  openChangePasswordModal() {
    let modal = this.modalController.create({
      component: ChangePasswordModalComponent
    });
    modal.show().then();
    modal.onDismiss().then()
  }

  async signout() {
    await this.deleteDivice();
    this.storageService.set('adminLogin', false);
    this.auth.clear();
    const appConfig = await this.appService.getAppConfig();
    setTimeout(() => {
      if (appConfig && appConfig.redirectAfterLogout) {
        location.href = appConfig.redirectAfterLogout;
        return;
      }
      this.router.navigateByUrl('/login');
    });
  }

  async deleteDivice() {
    try {
      const oneSignal = this.auth.snapshot.oneSignal
      if (oneSignal) {
        this.notificationApi.deleteDevice(oneSignal);
      }
    } catch (e) {
      debug.error(e)
    }
  }

  toggleDropdown() {
    this.show_dropdown = !this.show_dropdown;
  }

  toggleMoreShops() {
    this.more_shops = !this.more_shops;
  }

  onGoingToSettings() {
    this.goToSettings.emit();
  }

}
