import { Component, OnInit, Input } from '@angular/core';

export interface BreadcrumbMenu {
  name: string;
  name_custom: string;
}

@Component({
  selector: 'core-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input() menu: BreadcrumbMenu;

  constructor() {}

  ngOnInit() {}
}
