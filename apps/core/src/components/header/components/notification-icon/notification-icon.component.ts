import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { NavigationEnd, Router } from '@angular/router';
import { Noti } from 'libs/models/Noti';

@Component({
  selector: 'core-notification-icon',
  templateUrl: './notification-icon.component.html',
  styleUrls: ['./notification-icon.component.scss']
})
export class NotificationIconComponent implements OnInit, OnChanges {
  @Output() loadMoreNotis = new EventEmitter();
  @Output() navigate = new EventEmitter();
  @Output() switchAccount = new EventEmitter();
  @Output() viewAllNotis = new EventEmitter();

  @Input() count: number;
  @Input() notifications: Array<Noti> = [];
  @Input() loading = false;
  @Input() accounts = [];

  show_dropdown = false;
  click_inside = false;

  account_id = '';
  token = '';

  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    this.click_inside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.click_inside) {
      this.show_dropdown = false;
    }
    this.click_inside = false;
  }

  constructor(
    private auth: AuthenticateStore,
    private router: Router
  ) { }

  ngOnInit() {
    const _acc = this.accounts.find(a => a.id == this.auth.snapshot.account.id);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.account_id = _acc ? _acc.id : '';
        this.token = _acc ? _acc.token : '';
        this.show_dropdown = false;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.accounts && changes.accounts.currentValue) {
      this.accounts = changes.accounts.currentValue;
      const _acc = this.accounts.find(a => a.id == this.auth.snapshot.account.id);
      this.account_id = _acc ? _acc.id : '';
      this.token = _acc ? _acc.token : '';
    }
  }

  toogleDropdown() {
    this.show_dropdown = !this.show_dropdown;
  }

  onScroll(event) {
    const { offsetHeight, scrollTop, scrollHeight } = event.target;
    if (offsetHeight + scrollTop >= scrollHeight) {
      this.loadMoreNotis.next(this.token);
    }
  }

  onNavigate(noti, index) {
    this.notifications[index].is_read = true;
    noti.token = this.account_id;
    this.navigate.emit(noti);
  }

  onSwitchAccount() {
    const _acc = this.accounts.find(a => a.id == this.account_id);
    this.switchAccount.emit(_acc.token);
  }

  onViewAllNotis() {
    this.viewAllNotis.emit()
  }

}
