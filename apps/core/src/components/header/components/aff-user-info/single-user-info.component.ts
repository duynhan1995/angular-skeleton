import { Component, OnInit, SimpleChanges, OnChanges, Output, Input, HostListener, EventEmitter } from '@angular/core';
import { ExtendedAccount } from 'libs/models/Account';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { User } from 'libs/models';
import { ChangePasswordModalComponent } from 'apps/shared/src/components/change-password-modal/change-password-modal.component';
import { ModalController } from '../../../modal-controller/modal-controller.service';

@Component({
  selector: 'core-single-user-info',
  templateUrl: './single-user-info.component.html',
  styleUrls: ['./single-user-info.component.scss']
})
export class SingleUserInfoComponent implements OnInit, OnChanges {
  @Output() goToSettings = new EventEmitter();
  @Output() switchAccount = new EventEmitter();

  @Input() currentAccount: ExtendedAccount;
  @Input() accounts: Array<ExtendedAccount>;
  @Input() user: User;
  @Input() showForgotPassword = true;

  isMobile = false;
  show_dropdown = false;
  more_shops = false;
  click_inside = false;

  @HostListener('click', ['$event'])
  public clickInsideComponent(event) {
    if (event.target.id == 'settings') {
      this.click_inside = false;
      this.show_dropdown = false;
    }
    this.click_inside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.click_inside) {
      this.show_dropdown = false;
    }
    this.click_inside = false;
  }

  constructor(
    public router: Router,
    private auth: AuthenticateStore,
    private modalController: ModalController
  ) {}

  ngOnInit() {}

  onSwitchAccount(account: ExtendedAccount) {
    this.switchAccount.emit(account);
  }

  openChangePasswordModal() {
    this.modalController
      .create({ component: ChangePasswordModalComponent })
      .show()
      .then();
  }

  signout() {
    this.auth.clear();
    this.router.navigateByUrl('/login');
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  toggleDropdown() {
    this.show_dropdown = !this.show_dropdown;
  }

  toggleMoreShops() {
    this.more_shops = !this.more_shops;
  }

  onGoingToSettings() {
    this.router.navigateByUrl('/settings');
  }
}
