import { ExtendedAccount } from '../../../../../libs/models/Account';

export interface NavigationData {
  payload?: ExtendedAccount | any;
  target: string;
}
