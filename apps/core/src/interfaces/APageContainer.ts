import { HostBinding, Directive } from '@angular/core';

/**
 * @deprecated use PageBaseComponent instead
 */
@Directive()
export class APageContainer {
  @HostBinding('class') clazz = 'page-container'
}
