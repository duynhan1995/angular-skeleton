import * as moment from 'moment';

export class CacheData {
  expiredAt: Date;
  data: any
}
export class ServiceCacheBase {
  private cachedDatas: {
    [key: string]: CacheData;
  } = {};

  private _getExpritedDate() {
    return  moment().add(30, 'm').toDate();
  }

  cache(key, data) {
    let cache = new CacheData();
    cache.expiredAt = this._getExpritedDate();
    cache.data = data;
    this.cachedDatas[key] = data;
  }

  expireData(key) {
    delete this.cachedDatas[key];
  }

  get(key): CacheData {
    let data = this.cachedDatas[key];
    if (data && data.expiredAt > new Date()) {
      return data;
    }
    delete this.cachedDatas[key];
    return null;
  }

  findInData(key, find: (any) => boolean) {
    let data = this.get(key);
    if (data.data instanceof Array) {
      return data.data.find(d => find(d));
    }
    return null;
  }
}
