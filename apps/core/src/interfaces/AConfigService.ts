import { Injectable } from '@angular/core';

@Injectable()
export abstract class AConfigService {

  abstract getConfig();

  get(name: string) {
    let config = this.getConfig();
    return config && config[name];
  }
}
