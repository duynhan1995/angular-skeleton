import { Injectable } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from './util.service';

@Injectable()
export class TimerService {
  private timerHash: {
    [key: string]: any;
  } = {};

  constructor(private utils: UtilService, private auth: AuthenticateStore) {}

  setTimeout(fn: Function, interval: number, name?: string, needLogin = false) {
    if (needLogin && !this.auth.snapshot.token) {
      return;
    }
    if (!name) {
      name = this.utils.uuid();
    }

    this._clear('timeout', name);

    const id = setTimeout(fn, interval);
    return this.setTimer(name, id, 'timeout');
  }

  setInterval(fn: Function, interval: number, name?: string) {
    if (!name) {
      name = this.utils.uuid();
    }

    this._clear('interval', name);
    const id = setInterval(fn, interval);
    return this.setTimer(name, id, 'interval');
  }

  clear(name?: string) {
    if (name) {
      const timerObj = this.timerHash[name];
      if (!timerObj) {
        return false;
      }

      this._clear(timerObj.type, timerObj.name);
      return true;
    }

    console.log('CLEAR ALL TIMER ', this.timerHash);
    Object.values(this.timerHash).forEach(timerObj =>
      this._clear(timerObj.type, timerObj.name)
    );
    this.timerHash = {};
    return true;
  }

  private _clear(type: 'interval' | 'timeout', name: string) {
    const timerObj = this.timerHash[name];
    if (!timerObj) {
      return;
    }

    if (timerObj.type !== type) {
      throw new Error('Invalid name');
    }

    if (type == 'interval') {
      clearInterval(timerObj.id);
    }

    if (type == 'timeout') {
      clearTimeout(timerObj.id);
    }

    delete this.timerHash[name];
  }

  private setTimer(name: string, id: any, type: 'interval' | 'timeout') {
    const timerObj = {
      name,
      id,
      type
    };

    this.timerHash[name] = timerObj;
    return timerObj;
  }
}
