import { Injectable } from '@angular/core';
import { EventMessage } from "libs/models/Message";
import { Subject } from 'rxjs';

export function getWindowRef() {
  return (typeof window !== "undefined") ? window : null;
}

@Injectable()
export class IframeService {

  onParentMessage = new Subject();

  constructor() {
    this._init();
  }

  private _init() {
    let listenMessages = ["hello", "initialized", "ready",
      "access_granted",
      "init_session",
      "get_shipping_services",
      "update_order", "submit_order", "new_order",
      "get_products", "set_products",
      "order_created", "get_order_data"];
    let windowRef = getWindowRef();
    if (!windowRef || !windowRef.parent) {
      console.error("Parent reference is undefined");
    }

    this._bindEvent(windowRef, "message", (e: MessageEvent) => {
      let data = e.data;
      if (typeof data == "string") {
        try {
          data = JSON.parse(data);
        } catch (error) {
          return;
        }
      }
      if (listenMessages.indexOf(data.event) > -1) {
        this.onParentMessage.next(new EventMessage(data));
      }
    });

    this.onParentMessage.subscribe((data: any) => {
      if (data.event == "hello") {
        console.log("MERCHANT RECEIVE TEST MESSAGE", "hello", data);
        this.sendMessage(new EventMessage({
          requestId: data.requestId,
          event: 'hello',
          data: {
            greeting: "Hi, this is message from Merchant client"
          }
        }));
      }
    });
    this.sendMessage(new EventMessage({
      event: "initialized",
      data: null
    }));
  }

  private _bindEvent(element, eventName, eventHandler) {
    if (element.addEventListener) {
      element.addEventListener(eventName, eventHandler, false);
    } else if (element.attachEvent) {
      element.attachEvent('on' + eventName, eventHandler);
    }
  }

  public sendMessage(data: EventMessage) {
    let windowRef = getWindowRef();
    let parentRef = windowRef && windowRef.parent;
    if (!windowRef || !windowRef.parent) {
      console.error("Parent reference is undefined");
    }
    if (data instanceof EventMessage) {
      data.requestId = '' + new Date().getTime() + Math.random();
    }
    let _data = typeof data == "string" ? data : JSON.stringify(data);
    parentRef.postMessage(_data, "*");
  }

  public sendResponse(reqEvent: EventMessage, data, error) {
    this.sendMessage(new EventMessage({
      requestId: reqEvent.requestId,
      event: reqEvent.event,
      data: data,
      error: error
    }));
  }

  public sendError(errorData, requestID = null) {
    return this.sendMessage(new EventMessage({
      requestId: requestID,
      event: 'error',
      error: errorData
    }));
  }
}
