import * as validator from 'validator';

const NRIC_REGEX = /^[SFTG]\d{7}[A-Z]$/;
const POSTAL_CODE_REGEX = /^(\d{6})$/;
const PHONE_NUMBER_REGEX = /^\+[0-9]{9,14}$/;
const VIETNAME_PHONE_NUMBER_REGEX = /^(01[2689]|09)[0-9]{8}$/;

export class ValidateModel {
  check: { (variable: any): boolean };
  error: string;
}

export class Constraint {
  type?: string;
  typeError?: string;
  validates: Array<ValidateModel>;
  excludeKey?: boolean;
  customKey?: string;
}

export default {
  ...validator,

  isNRIC(str) {
    return NRIC_REGEX.test(str);
  },

  isPhoneNumber(str) {
    return (
      VIETNAME_PHONE_NUMBER_REGEX.test(str) || PHONE_NUMBER_REGEX.test(str)
    );
  },

  isSingPostal(str: string) {
    return POSTAL_CODE_REGEX.test(str);
  },

  runValidating(variable, constraint: Constraint) {
    if (!constraint) {
      return null;
    }

    if (constraint.type) {
      if (typeof variable !== constraint.type) {
        return constraint.typeError;
      }
    }

    for (let validate of constraint.validates) {
      if (!validate.check(variable)) {
        return validate.error;
      }
    }

    return null;
  },

  runValidatingOnObject(obj, constrants) {
    for (let key of Object.keys(obj)) {
      if (constrants[key]) {
        let constraint = constrants[key];
        let result = this.runValidating(obj[key], constraint);
        if (result) {
          return {
            error:
              (!constraint.excludeKey ? constraint.customKey || key : '') +
              ' ' +
              result,
            key: constraint.customKey || key,
            data: obj[key]
          };
        }
      } else {
        if (obj[key] == null || obj[key] === undefined) {
          return {
            error: key + ' can not be empty',
            key: key,
            data: obj[key]
          };
        }
      }
    }
    return null;
  },

  runValidatingOnObjectArray(obj, constrants) {
    let errors = [];
    for (let key of Object.keys(obj)) {
      if (constrants[key]) {
        let constraint = constrants[key];
        let result = this.runValidating(obj[key], constraint);
        if (result) {
          errors.push({
            error:
              (!constraint.excludeKey ? constraint.customKey || key : '') +
              ' ' +
              result,
            key: constraint.customKey || key,
            data: obj[key]
          });
        }
      } else {
        if (obj[key] == null || obj[key] === undefined) {
          errors.push({
            error: key + ' can not be empty',
            key: key,
            data: obj[key]
          });
        }
      }
    }
    return errors;
  }
};
