import {Injectable} from '@angular/core';
import Queue from 'queue';

@Injectable({
  providedIn: "root"
})
export class PromiseQueueService {
  private _queue = new Queue({
    concurrency: 3,
    autostart: true
  });

  constructor() {
  }

  async run(promises: Array<Function>, concurrent) {
    while (promises.length) {
      const chunk = promises.splice(0, concurrent);
      try {
        await Promise.all(chunk.map(p => p()));
      } catch (e) {
        debug.error(e);
      }
    }
  }

  destroyQ() {
    this._queue.end({
      name: 'queue_stopped',
      message: 'Queue was stopped!'
    });
  }

  enQ(task: Function) {
    return new Promise((res, rej) => {
      if (!this._queue) { res(null); }
      else {
        this._queue.push(async (cb) => {
          try {
            let result = await task();
            res(result);
          } catch (e) {
            rej(e)
          }
          cb();
        });
      }
    });
  }

}
