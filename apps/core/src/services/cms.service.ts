import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {StringHandler} from "@etop/utils";
import {Subject} from 'rxjs';
import {AppService} from '@etop/web/core/app.service';

const {trimHtml, trimNewLineCharacters, trimAllSpaceOutsideQuotes} = StringHandler;

interface SurveyQuestion {
  [_key: string]: {
    key: string;
    image_url: string;
    question: string;
    answersList: { text: string; value: string | number }[];
    answer: string | number;
  }
}

@Injectable({
  providedIn: "root"
})
export class CmsService {
  cms_banners: any = {};

  banners = [];
  banners_management = {
    'shipping_policy': 1,
    'transaction_note': 6,
    'pos_note': 9,
    'no_transaction_days': 11,
    'announcement_content': 19,
    'ghn_priority_package': 25,
    'ghtk_unavailable_text': 26,
    'carrier_description': 27,
    'header_announcement': 28,
    'user_manuals': 34,
    'welcome_contents_prod': 38,
    'welcome_contents_dev': 39,
    'ticket_note': 41,
    'paycard_tutorial': 43,
    'etop_pos_survey': 44,
    'topship_survey': 45,
    'default_weight': 46,
    'welcome_ionic_topship': 48,
    'import_ffm_badge': 49,
    'not_subscription': 42,
    'not_subscription_app': 39,
    'message_not_subscription': 29
  };

  onBannersLoaded = new Subject();
  bannersLoaded = false;

  constructor(
    private router: Router,
    private appService: AppService
  ) {
    this.getAppConfig();
    this.handlingRouterChange();
  }

  getAppConfig() {
    this.cms_banners = this.appService.cms_banners;
  }

  handlingRouterChange() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.initBanners().then();
      }
    });
  }

  async initBanners() {
    try {
      for (let banner in this.cms_banners) {
        if (!this.cms_banners.hasOwnProperty(banner)) {
          continue;
        }
        if (this.cms_banners[banner] != 'cms_flexbox_settings') {
          this.bannersLoaded = true;
          this.onBannersLoaded.next();
          return;
        }
      }
      const res = await fetch('https://cms-setting.etop.vn/api/get-banner').then(r =>
        r.json()
      );
      this.banners = res.data;
    } catch (e) {
      debug.error('ERROR in getting banners', e);
      this.banners = [];
    }
    this.bannersLoaded = true;
    this.onBannersLoaded.next();
  }

  private getBannerInfo(banner_name) {
    try {
      const cms_banners = this.cms_banners;
      if (cms_banners[`cms_${banner_name}`] && cms_banners[`cms_${banner_name}`] != 'cms_flexbox_settings') {
        return {
          not_flexbox: true,
          content: cms_banners[`cms_${banner_name}`]
        };
      }
      if (!this.bannersLoaded) {
        this.initBanners().then();
      }

      return this.banners?.length && this.banners[this.banners_management[banner_name]];
    } catch (e) {
      debug.log(`ERROR in getting banner info ${banner_name.toUpperCase()}`, e);
      return null;
    }
  }

  getDefaultWeight() {
    const banner = this.getBannerInfo('default_weight');
    return banner?.is_active === '1' && Number(trimNewLineCharacters(trimHtml(banner?.description)));
  }

  getEtopPosSurvey() {
    try {
      const banner = this.getBannerInfo('etop_pos_survey');
      if (banner?.is_active === '1') {
        const questions = JSON.parse(trimAllSpaceOutsideQuotes(
          trimNewLineCharacters(trimHtml(banner?.description))
        ));

        let questionHash: SurveyQuestion = null;
        for (const item of questions) {
          if (!questionHash) { questionHash = {} }
          questionHash[item.key] = {
            key: item.key,
            image_url: item.image,
            question: item.question,
            answersList: item.answer,
            answer: null
          };
        }
        return questionHash;
      } else {
        return null;
      }
    } catch (e) {
      debug.error('ERROR in getEtopPosSurvey', e);
      return null;
    }
  }

  getTopshipSurvey() {
    try {
      const banner = this.getBannerInfo('topship_survey');
      if (banner?.is_active === '1') {
        const questions = JSON.parse(trimAllSpaceOutsideQuotes(
          trimNewLineCharacters(trimHtml(banner?.description))
        ));

        let questionHash: SurveyQuestion = null;
        for (const item of questions) {
          if (!questionHash) { questionHash = {} }
          questionHash[item.key] = {
            key: item.key,
            image_url: item.image,
            question: item.question,
            answersList: item.answer,
            answer: null
          };
        }
        return questionHash;
      } else {
        return null;
      }
    } catch (e) {
      debug.error('ERROR in getTopshipSurvey', e);
      return null;
    }
  }

  // SECTION TOPSHIP references
  getGHNPriorityPackage() {
    const banner = this.getBannerInfo('ghn_priority_package');
    return banner?.is_active === '1' && trimNewLineCharacters(trimHtml(banner?.description)) || '';
  }

  getAnnouncementIonicTopship() {
    const banner = this.getBannerInfo('welcome_ionic_topship');
    if (banner?.not_flexbox) {
      return banner.content;
    }
    return banner?.is_active === '1' && banner?.description || '';
  }

  getPOSNote() {
    const banner = this.getBannerInfo('pos_note');
    return banner?.is_active === '1' && banner?.description || '';
  }

  getTicketNote() {
    const banner = this.getBannerInfo('ticket_note');
    return banner?.is_active === '1' && banner?.description || '';
  }

  getCarrierDescription() {
    const banner = this.getBannerInfo('carrier_description');
    return banner?.is_active === '1' && banner?.description || '';
  }

  getPayCardTutorial() {
    const banner = this.getBannerInfo('paycard_tutorial');
    return banner?.is_active === '1' && banner?.description || '';
  }

  getMessageNotSubscription(){
    const banner = this.getBannerInfo('message_not_subscription');
    if (banner?.is_active === '1') {
      const data = JSON.parse(trimAllSpaceOutsideQuotes(
        trimNewLineCharacters(trimHtml(banner?.description))
      ))
      return data;
    }
    else
      return null;
  }
  getNotSubscription() {
    const banner = this.getBannerInfo('not_subscription');
    return banner?.is_active === '1' && banner?.description || '';
  }

  getNotSubscriptionApp() {
    const banner = this.getBannerInfo('not_subscription_app');
    return banner?.is_active === '1' && banner?.description || '';
  }

  getGHTKUnavailableText() {
    const banner = this.getBannerInfo('ghtk_unavailable_text');
    return banner?.is_active === '1' && trimNewLineCharacters(trimHtml(banner?.description)) || '';
  }

  // END Section TOPSHIP references

  // SECTION SELLER AFFILIATE references
  getWelcomeContentsProd() {
    const banner = this.getBannerInfo('welcome_contents_prod');
    return banner?.is_active === '1' && banner || '';
  }

  getWelcomeContentsDev() {
    const banner = this.getBannerInfo('welcome_contents_dev');
    return banner?.is_active === '1' && banner || '';
  }

  // END Section SELLER AFFILIATE references

  getAnnouncementContents() {
    const banner = this.getBannerInfo('announcement_content');
    if (banner?.not_flexbox) {
      return banner.content;
    }
    return banner?.is_active === '1' && banner?.description || '';
  }

  getShippingPolicy() {
    const banner = this.getBannerInfo('shipping_policy');
    if (banner?.not_flexbox) {
      return banner.content;
    }
    return banner?.is_active === '1' && banner.url || '';
  }

  getTransactionNote() {
    const banner = this.getBannerInfo('transaction_note');
    if (banner?.not_flexbox) {
      return banner.content;
    }
    return banner?.is_active === '1' && banner?.description || '';
  }

  getNoTransactionDays() {
    const banner = this.getBannerInfo('no_transaction_days');
    return banner?.is_active === '1' && trimNewLineCharacters(trimHtml(banner?.description)) || '';
  }

  getUserManuals() {
    const banner = this.getBannerInfo('user_manuals');
    return banner?.is_active === '1' && banner || null;
  }

  getImportFfmBadge() {
    const banner = this.getBannerInfo('import_ffm_badge');
    return banner && banner.is_active === '1' && trimNewLineCharacters(trimHtml(banner.description)) || '';
  }
}
