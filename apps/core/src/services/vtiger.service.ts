import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { Ticket } from '../../../../libs/models/Ticket';
import misc from 'apps/core/src/libs/misc';
import { OrderService } from 'apps/shop/src/services/order.service';
import { ConfigService } from '@etop/core';
import { environment } from '../environments/environment';

@Injectable()
export class VTigerService {
  url = '';
  environment = '';
  constructor(private orderService: OrderService, private http: HttpService, private config: ConfigService) {
    this.url = this.config.getConfig().crm_url || environment.crm_url;
    this.environment = this.config.getConfig().ticket_environment || environment.ticket_environment;
  }

  async createTicket(body: {}) {
    return this.http.post(`${this.url}/api.crm/ticket`, body).toPromise();
  }

  async updateTicket(body: Ticket) {
    return this.http
      .put(`${this.url}/api.crm/ticket`, body)
      .toPromise()
      .then(res => res.data);
  }

  async getTickets(query: {}) {
    try {
      let tickets: Array<any> = await this.http
        .get(
          `${this.url}/api.crm/ticket`,
          Object.assign({}, query, {
            environment: this.environment
          })
        )
        .toPromise()
        .then(tks => tks.data);
      let ids = tickets.map(ticket => ticket?.order_id);
      ids = misc.uniqueArray(ids);
      let orders = await this.orderService.getOrdersByIds(ids);
      let orderMap = {};
      orders.forEach(order => {
        orderMap[order.id] = order;
      });

      tickets = tickets.map(ticket => {
        ticket['order'] = orderMap[ticket?.order_id];
        if (ticket.code == 'change-shop-cod') {
          ticket.old_value = ticket.old_value.replace(/\,/g, '.');
        }
        return ticket;
      });
      let open_tickets = tickets.filter(t => t.ticketstatus == 'Open');
      let closed_tickets = tickets.filter(t => t.ticketstatus == 'Closed');
      open_tickets.sort((a, b) => {
        if (new Date(a.createdtime).getTime() > new Date(b.createdtime).getTime()) {
          return 1;
        }
        return -1;
      });
      return open_tickets.concat(closed_tickets);
    } catch (e) {
      debug.error('ERROR IN GET TICKETS', e);
      // toastr.error('Có lỗi xảy ra khi load Tickets. F5 để load lại.');
    }
  }
}
