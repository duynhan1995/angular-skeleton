import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  constructor() {}

  log(...params) {}

  warn(...params) {}

  error(...params) {}
}