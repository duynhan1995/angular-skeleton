import { Injectable } from '@angular/core';
import {AddressAPI, AddressApi} from "@etop/api";
import { HttpService } from "@etop/common";
import { AuthenticateStore } from "@etop/core";
import { Subject } from 'rxjs';
import { UtilService } from './util.service';
import { Address, OrderAddress } from '../../../../libs/models/Address';
import { AddressDisplayPipe } from 'libs/shared/pipes/location.pipe';
import { CustomerAddress } from 'libs/models/Customer';

@Injectable({
  providedIn: "root"
})
export class AddressService {

  loading = false;
  shop_address = new Address({});
  ready = false;
  onServiceReady = new Subject();

  private addressChange = new Subject();

  addressesList: {fromAddresses: Address[], fromAddressIndex: number};
  addressesList$ = new Subject<{fromAddresses: Address[], fromAddressIndex: number}>();

  constructor(
    private auth: AuthenticateStore,
    private http: HttpService,
    private util: UtilService,
    private addressApi: AddressApi,
    private addressDisplay: AddressDisplayPipe
  ) {}

  get change() {
    return this.addressChange.asObservable();
  }

  createAddress(body: AddressAPI.CreateAddressRequest, token?: string) {
    return this.addressApi.createAddress(body, token);
  }

  updateAddress(body: AddressAPI.UpdateAddressRequest) {
    return this.addressApi.updateAddress(body);
  }

  getAddresses() {
    return this.addressApi.getAddresses();
  }

  removeAddress(id) {
    return this.addressApi.removeAddress(id);
  }

  joinAddress(address) {
    let res = '';
    if (address && address.address) {
      res += address.address + ", ";
    } else if (address && address.address1) {
      res += address.address1 + ", ";
    }

    if (address && address.ward) {
      res += address.ward + ", ";
    }

    if (address && address.district) {
      res += address.district + ", ";
    }

    if (address && address.province) {
      res += address.province;
    }

    return res;
  }

  async getFromAddresses() {
    const addresses = await this.getAddresses();
    const froms = addresses.filter(a => a.type === 'shipfrom');
    let fromAddresses: any[] = froms.map((a, index) => this.mapAddressInfo(a, index));
    let fromAddressIndex = froms.findIndex(a => a.id === this.auth.snapshot.shop?.ship_from_address_id);
    if (fromAddressIndex == -1) { fromAddressIndex = 0; }
    this.addressesList$.next({ fromAddresses, fromAddressIndex });
    this.addressesList = { fromAddresses, fromAddressIndex };
    return { fromAddresses, fromAddressIndex };
  }

  mapAddressInfo(address: Address | OrderAddress | CustomerAddress, index) {
    return {
      ...address,
      index,
      info: `
<strong class="text-topship">${address.full_name} - ${address.phone}</strong>
<span class="lower-medium-font">
  ${this.addressDisplay.transform(address)}
</span>`,
      detail: `
<strong class="text-topship">${address.full_name} - ${address.phone}</strong>
<span class="lower-medium-font">
  ${this.addressDisplay.transform(address)}
</span>`,
      search_text: this.util.makeSearchText(
        `${address.full_name +
        address.phone +
        address.address1 +
        address.ward +
        address.district +
        address.province}`
      )
    }
  }

}
