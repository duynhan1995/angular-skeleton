import { Injectable } from '@angular/core';
import { ConfigService } from '@etop/core';

declare var ga: Function;
declare var gtag: Function;
const CATEGORY_CREATE_ORDER = 'Tạo đơn hàng';
const CATEGORY_GENERAL = 'Tổng quan';
const CATEGORY_FEATURE = 'Tính năng';
const CATEGORY_ACCOUNT = 'Tài khoản';

const ACTION_SHOP_INFO = 'Thông tin Shop';
const ACTION_USER_INFO = 'Thông tin User';
const ACTION_ORDER_SRC = 'Nguồn đơn hàng';
const ACTION_SHIPPING_TO = 'Khu vực giao hàng';
const ACTION_SHIPPING_FROM = 'Khu vực lấy hàng';
const ACTION_ORDER_INFO = 'Thông tin đơn hàng';

const ACTION_CUSTOMER_INFO = 'Thông tin khách hàng';

const ACTION_REGISTER = 'Đăng ký';
const ACTION_LOGIN = 'Đăng nhập';

const LABEL_EMAIL = ' | Email';
const LABEL_NAME = ' | Tên';
const LABEL_PHONE = ' | SDT';

const LABEL_ADD_CITY = ' | Tỉnh thành';
const LABEL_ADD_DISTRICT = ' | Quận huyện';
const LABEL_ADD_WARD = ' | Phường xã';
const LABEL_ADD_STREET = ' | Địa chỉ';

const LABEL_ORDER_ID = ' | Mã đơn hàng';
const LABEL_ORDER_FULFILL = ' | Mã giao hàng';
const LABEL_ORDER_COUNT = ' | Số lượng sản phẩm';
const LABEL_ORDER_PRODUCT = ' | Tên sản phẩm';
const LABEL_ORDER_TOTAL = ' | Giá trị';


export const USER_BEHAVIOR = {

  // ACTION
  ACTION_MY_SHOP: 'Cửa hàng của tôi',
  ACTION_POS: 'POS',
  ACTION_ORDERS: 'Đơn hàng',
  ACTION_FULFILLMENTS: 'Đơn giao hàng',
  ACTION_SHOP_SETTING: 'Thiết lập cửa hàng',
  ACTION_DELIVER_SETTING: 'Thiết lập giao hàng',
  ACTION_SHOP_LIST_SETTING: 'Thiết lập danh sách cửa hàng',
  ACTION_CATEGORIES_SETTING: 'Thiết lập danh mục',
  ACTION_COLLECTION_SETTING: 'Thiết lập bộ sưu tập',
  ACTION_ACCOUNT_SETTING: 'Thiết lập tài khoản',
  ACTION_GUIDE: 'Hướng dẫn sử dụng',
  ACTION_BUG_REPORT: 'Báo lỗi',

  // LABEL OF ACTION_MY_SHOP
  LABEL_PRODUCT_TAB: 'Duyệt tab sản phẩm',
  LABEL_SEARCH_PRODUCT: 'Tìm kiếm sản phẩm',
  LABEL_IMPORT_PRODUCT: 'Import sản phẩm',
  LABEL_ADD_PRODUCT: 'Thêm sản phẩm',

  // LABEL OF ACTION_POS
  LABEL_PSX_DOWNLOAD: 'Tải POS Chrome Extension',
  LABEL_FROM_ADDRESS_SETTING: 'Thiết lập địa chỉ lấy hàng',
  LABEL_ADD_TO_CART: 'Thêm sản phẩm vào giỏ',
  LABEL_REMOVE_FROM_CART: 'Xoá sản phẩm khỏi giỏ',
  LABEL_UPDATE_QUANTITY: 'Điều chỉnh số lượng',

  // LABEL OF ACTION_ORDERS
  LABEL_CREATE_ORDER: 'Tạo đơn hàng',
  LABEL_SEARCH_ORDER: 'Tìm kiếm đơn hàng',
  LABEL_SORT_ORDER: 'Sắp xếp đơn hàng',
  LABEL_ORDER_TAB: 'Duyệt tab đơn hàng',
  LABEL_VIEW_ORDER: 'Xem đơn hàng',
  LABEL_PRINT_ORDER: 'In đơn hàng',
  LABEL_PRINT_FULFILLMENT: 'In đơn giao hàng',
  LABEL_CANCEL_ORDER: 'Huỷ đơn hàng',
  LABEL_CANCEL_AND_CREATE_ORDER: 'Huỷ và đặt lại đơn hàng',
  LABEL_COPY_ORDER: 'Sao chép đơn hàng',
  LABEL_UPDATE_ORDER_INFO: 'Lưu thông tin đơn hàng',
  LABEL_CREATE_FULFILLMENT: 'Tạo đơn giao hàng',

  // LABEL OF ACTION_FULFILLMENTS
  LABEL_SEARCH_FULFILLMENT: 'Tìm kiếm đơn giao hàng',
  LABEL_EXPORT_EXCEL: 'Xuất excel',
  LABEL_VIEW_FULFILLMENT: 'Xem đơn giao hàng',

  // LABEL OF ACTION_SHOP_SETTING
  LABEL_UPDATE_CONTACT_INFO: 'Cập nhật thông tin liên hệ',
  LABEL_UPDATE_MONTHLY_MT: 'Cập nhật lịch chuyển khoản',
  LABEL_UPDATE_BANK_ACCOUNT: 'Cập nhật tài khoản ngân hàng',
  LABEL_CONNECT_HARAVAN: 'Kết nối Haravan',
  LABEL_DOWNLOAD_PRODUCT_FEED: 'Tải Product Feed',

  // LABEL OF ACTION_DELIVER_SETTING
  LABEL_UPDATE_AUTO_FFM: 'Cập nhật phương thức tạo đơn giao hàng',
  LABEL_UPDATE_GHN_NOTE_CODE: 'Cập nhật ghi chú bắt buộc',
  LABEL_ADD_FROM_ADDRESS: 'Thêm địa chỉ lấy hàng',
  LABEL_UPDATE_FROM_ADDRESS: 'Cập nhật địa chỉ lấy hàng',
  LABEL_REMOVE_FROM_ADDRESS: 'Xoá địa chỉ lấy hàng',
  LABEL_UPDATE_DEFAULT_FROM_ADDRESS: 'Thay đổi địa chỉ lấy hàng mặc định',
  LABEL_ADD_TO_ADDRESS: 'Thêm địa chỉ giao hàng',
  LABEL_UPDATE_TO_ADDRESS: 'Cập nhật địa chỉ giao hàng',
  LABEL_REMOVE_TO_ADDRESS: 'Xoá địa chỉ giao hàng',
  LABEL_UPDATE_DEFAULT_TO_ADDRESS: 'Thay đổi địa chỉ giao hàng mặc định',

  // LABEL OF ACTION_SHOP_LIST_SETTING
  LABEL_ADD_SHOP: 'Thêm cửa hàng',
  LABEL_REMOVE_SHOP: 'Xoá cửa hàng',
  LABEL_UPDATE_SHOP: 'Chỉnh sửa cửa hàng',
  LABEL_ACCESS_SHOP: 'Truy cập cửa hàng',

  // LABEL OF ACTION_CATEGORIES_SETTING
  LABEL_ADD_CATEOGRY: 'Thêm danh mục',
  LABEL_UPDATE_CATEGORY: 'Cập nhật danh mục',
  LABEL_REMOVE_CATEGORY: 'Xoá danh mục',
  LABEL_SEARCH_CATEGORY: 'Tìm kiếm danh mục',

  // LABEL OF ACTION_COLLECTION_SETTING
  LABEL_ADD_COLLECTION: 'Thêm bộ sưu tập',
  LABEL_UPDATE_COLLECTION: 'Cập nhật bộ sưu tập',
  LABEL_REMOVE_COLLECTION: 'Xoá bộ sưu tập',
  LABEL_SEARCH_COLLECTION: 'Tìm kiếm bộ sưu tập',

  // LABEL OF ACTION_ACCOUNT_SETTING
  LABEL_VIEW_ACCOUNT_INFO: 'Xem thông tin tài khoản',

  // LABEL OF ACTION_GUIDE
  LABEL_GO_TO_GUIDE_SITE: 'Đến trang hướng dẫn sử dụng',

  // LABEL OF ACTION_BUG_REPORT
  LABEL_BUG_REPORT: 'Báo lỗi'

};

@Injectable()
export class GoogleAnalyticsService {

  isProduction: boolean;

  constructor(private configService: ConfigService) {
    this.isProduction = configService.getConfig().production && window.location.href.indexOf('d.etop.vn') < 0;
  }

  sendUserId(userId) {
    if (!userId || !this.isProduction) {
      return;
    }

    ga('set', 'userId', userId);
    ga('send', 'pageview');
  }

  sendOrderInfo(order, fulfillment, shop, toAddress, fromAddress) {
    if (!fulfillment) {
      return;
    }
    if (this.isProduction) {

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHOP_INFO + LABEL_NAME, shop.name);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHOP_INFO + LABEL_PHONE, shop.phone);

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_CUSTOMER_INFO + LABEL_NAME, toAddress.full_name);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_CUSTOMER_INFO + LABEL_PHONE, toAddress.phone);

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_TO + LABEL_ADD_CITY, toAddress.province);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_TO + LABEL_ADD_DISTRICT, toAddress.district);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_TO + LABEL_ADD_WARD, toAddress.ward);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_TO + LABEL_ADD_STREET, toAddress.address1);

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_FROM + LABEL_ADD_CITY, fromAddress.province);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_FROM + LABEL_ADD_DISTRICT, fromAddress.district);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_FROM + LABEL_ADD_WARD, fromAddress.ward);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_SHIPPING_FROM + LABEL_ADD_STREET, fromAddress.address1);

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_ORDER_INFO + LABEL_ORDER_ID, order.id);

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_ORDER_INFO + LABEL_ORDER_FULFILL, fulfillment.shipping_code);

      const totalItems = order.lines.reduce((x, y) => x + y.quantity, 0);
      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_ORDER_INFO + LABEL_ORDER_COUNT, totalItems, totalItems);

      ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_ORDER_INFO + LABEL_ORDER_TOTAL, order.total_amount, order.total_amount);

      order.lines.forEach(line => {
        ga('send', 'event', CATEGORY_CREATE_ORDER, ACTION_ORDER_INFO + LABEL_ORDER_PRODUCT, line.product_name);
      });

      ga('ecommerce:addTransaction', {
        'id': '1234',
        'affiliation': shop.name,
        'revenue': order.total_amount,
        'shipping': fulfillment.shipping_fee_shop,
        'tax': null,
        'currency': 'VND'
      });

      order.lines.forEach(line => {
        ga('ecommerce:addItem', {
          'id': line.product_id,
          'name': line.product_name,
          'sku': null,
          'category': null,
          'price': line.retail_price,
          'quantity': line.quantity,
          'currency': 'VND'
        });
      });

      ga('ecommerce:send');
      ga('ecommerce:clear');

    }

  }

  sendCurrentShop(shop) {
    if (!shop) {
      return;
    }
    if (this.isProduction) {
      ga('send', 'event', CATEGORY_FEATURE, ACTION_SHOP_INFO + LABEL_NAME, shop.name);
      ga('send', 'event', CATEGORY_FEATURE, ACTION_SHOP_INFO + LABEL_PHONE, shop.phone);
    }
  }

  sendUserBehavior(action, label) {
    if (this.isProduction) {
      ga('send', 'event', CATEGORY_FEATURE, action, label);
    }
  }

  login(email) {
    if (this.isProduction) {
      ga('send', 'event', CATEGORY_ACCOUNT, ACTION_LOGIN + LABEL_EMAIL, email);
    }
  }

  registerSuccessfully(email, phone, name) {
    if (this.isProduction) {
      ga('send', 'event', CATEGORY_ACCOUNT, ACTION_REGISTER + LABEL_EMAIL, email);
      ga('send', 'event', CATEGORY_ACCOUNT, ACTION_REGISTER + LABEL_PHONE, phone);
      ga('send', 'event', CATEGORY_ACCOUNT, ACTION_REGISTER + LABEL_NAME, name);
      gtag('event', 'conversion', { 'send_to': 'AW-778919810/3HGSCLC3wI8BEIK_tfMC' });
    }
  }
}
