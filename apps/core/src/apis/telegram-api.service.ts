import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilService } from 'apps/core/src/services/util.service';
import { ConfigService } from '@etop/core';
import { StringHandler } from '@etop/utils';

@Injectable()
export class TelegramApi {

  constructor(
    private http: HttpClient,
    private util: UtilService,
    private configService: ConfigService
  ) {}

  sendMessage(fastBuyGroupId, msg, enableHTML = true) {
    const info = `\nSource: ${window.location.hostname} - ${
      window.innerWidth
    }px - ${navigator.platform}`;
    msg += info;
    //token eb2b_dev_bot
    let token = '1849144725:AAEp7LkYIB0X63hC-ph2ytZSLgG8E7NGZZw';
    if (this.configService.getConfig().production) {
      //token eb2b_bot
      token = '1746704472:AAHuIwK-0IZjRPKub_QirlCpoQmp6dAiN7M';
    }
    const fastBuyUrl = `https://api.telegram.org/bot${token}/sendMessage`;
    const fastbuyData: any = {
      chat_id: fastBuyGroupId,
      text: StringHandler.parseHtmlSpecialCharacters(msg)
    };
    if (enableHTML) {
      fastbuyData.parse_mode = 'HTML';
    }

    return this.http.post(fastBuyUrl, fastbuyData).toPromise();
  }

}
