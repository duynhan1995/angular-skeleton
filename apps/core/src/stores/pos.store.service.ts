import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { OrderLine } from 'libs/models/Order';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { PrintType } from '@etop/shared/typings/print';

export interface PosData {
  order_lines: OrderLine[];
  discount: number;
  fee: number;
  recentlyCreatedOrderID: string;
  confirm?: boolean;
  to_print?: boolean;
  print_type?: PrintType;
}

@Injectable()
export class PosStoreService extends AStore<PosData> {
  initState = {
    order_lines: [],
    discount: 0,
    fee: 0,
    recentlyCreatedOrderID: '',
    to_print: false,
    print_type: PrintType.orders
  };

  readonly onOrderCreated$ = this.state$.pipe(
    map(({recentlyCreatedOrderID}) => recentlyCreatedOrderID),
    distinctUntilChanged()
  );

  constructor() {
    super();
    this.state = this.initState;
    if (localStorage.getItem('to_print')) {
      this.setState({ to_print: localStorage.getItem('to_print') != 'false' });
    }
    if (localStorage.getItem('print_type')) {
      this.setState({ print_type: PrintType[localStorage.getItem('print_type')] });
    }
  }

  changeLines(order_lines: OrderLine[]) {
    this.setState({order_lines});
  }

  changeDiscount(discount: number) {
    this.setState({discount});
  }

  changeFee(fee: number) {
    this.setState({fee});
  }

  setConfirm(confirm: boolean) {
    this.setState({confirm});
  }

  recentlyCreatedOrderID(id: string) {
    this.setState({recentlyCreatedOrderID: id});
  }

  toPrint() {
    const to_print = !this.snapshot.to_print;
    this.setState({ to_print });
    localStorage.setItem('to_print', to_print.toString());
  }

  changePrintType(print_type: PrintType, store_local = false) {
    this.setState({ print_type });
    if (store_local) {
      localStorage.setItem('print_type', print_type);
    }
  }

  resetState() {
    this.changeLines([]);
    this.changeDiscount(null);
    this.changeFee(null);
  }

}
