import { Injectable } from '@angular/core';
import { Ledger, Receipt } from 'libs/models/Receipt';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface ReceiptData {
  ledgers?: Array<Ledger>;
  selected_ledger?: Ledger;
  receipts?: Array<Receipt | any>;
  selected_ledger_ids?: Array<string>;
}

@Injectable({
  providedIn: 'root'
})
export class ReceiptStoreService extends AStore<ReceiptData> {
  initState = {
    receipts: [],
    selected_ledger_ids: []
  };

  readonly receiptsUpdated$ = this.state$.pipe(
    map(({receipts}) => receipts),
    distinctUntilChanged()
  );

  constructor() {
    super();
    this.state = this.initState;
  }

  updateLedgers(ledgers: Ledger[]) {
    this.setState({ ledgers });
  }

  updateSelectedLedger(selected_ledger: Ledger) {
    this.setState({ selected_ledger });
  }

  updateReceipts(receipts: Receipt[] | any[]) {
    this.setState({ receipts });
  }

  updateSelectedLedgerIDs(selected_ledger_ids: string[]) {
    this.setState({ selected_ledger_ids });
  }

  resetData() {
    this.updateReceipts([]);
    this.updateSelectedLedgerIDs([]);
  }

}
