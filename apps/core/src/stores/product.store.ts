import { Injectable } from '@angular/core';
import { Product } from 'libs/models/Product';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface ProductData {
  selectedProducts: Product[];
  productList: Product[];
}

@Injectable({
  providedIn: 'root'
})
export class ProductStore extends AStore<ProductData> {
  initState = {
    selectedProducts: [],
    productList: []
  };

  readonly selectedProductsChanged$ = this.state$.pipe(
    map(({selectedProducts}) => selectedProducts),
    distinctUntilChanged()
  );

  readonly productListUpdated$ = this.state$.pipe(
    map(({productList}) => productList),
    distinctUntilChanged((prev, current) => {
      return JSON.stringify(current) == JSON.stringify(prev);
    })
  );

  constructor() {
    super();
    this.state = this.initState;
  }

  changeSelectedProducts(selectedProducts: Product[]) {
    this.setState({selectedProducts});
  }

  updateProductList(products: Product[]) {
    this.setState({productList: products});
  }

}
