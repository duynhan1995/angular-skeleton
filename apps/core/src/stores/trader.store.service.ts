import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';

export declare type HeaderTab = 'customer' | 'supplier' | 'carrier';

export interface TraderData {
  header_tab?: HeaderTab | string;
  queryParams?: string;
}

@Injectable({
  providedIn: 'root'
})
export class TraderStoreService extends AStore<TraderData> {
  initState = { header_tab: 'customer' };

  readonly headerTabChanged$ = this.state$.pipe(
    map(({header_tab}) => header_tab),
    distinctUntilChanged()
  );

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private route: ActivatedRoute,
    private util: UtilService
  ) {
    super();
    this.state = this.initState;

    const { queryParams } = this.route.snapshot;
    if (queryParams.type) {
      this.changeHeaderTab(queryParams.type);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('traders') != -1) {
          const { queryParams } = this.route.snapshot;
          if (queryParams.type) {
            return this.changeHeaderTab(queryParams.type);
          } else {
            this.changeHeaderTab('customer');
          }
        }
      }
    });

  }

  changeHeaderTab(header_tab: HeaderTab | string) {
    this.setState({ header_tab });
  }

  async navigate(type: string, code: string, id?: string) {
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`s/${slug}/traders?code=${code || id}&type=${type}`);
  }

}
