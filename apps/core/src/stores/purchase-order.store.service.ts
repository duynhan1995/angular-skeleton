import { Injectable } from '@angular/core';
import { PurchaseOrder, PurchaseOrderLine } from 'libs/models/PurchaseOrder';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface PurchaseOrderData {
  basket_value: number | string;
  lines: PurchaseOrderLine[];
  recentlyCreated: boolean;
  recentlyUpdated: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderStoreService extends AStore<PurchaseOrderData> {
  initState = {
    basket_value: 0, lines: [], recentlyCreated: false, recentlyUpdated: false
  };

  readonly basketValueChanged$ = this.state$.pipe(
    map(({basket_value}) => basket_value),
    distinctUntilChanged()
  );

  readonly linesChanged$ = this.state$.pipe(
    map(({lines}) => lines),
    distinctUntilChanged()
  );

  readonly recentlyCreated$ = this.state$.pipe(
    map(({recentlyCreated}) => recentlyCreated),
    distinctUntilChanged()
  );

  readonly recentlyUpdated$ = this.state$.pipe(
    map(({recentlyUpdated}) => recentlyUpdated),
    distinctUntilChanged()
  );

  constructor() {
    super();
    this.state = this.initState;
  }

  changeBasketValue(basket_value: number | string) {
    this.setState({basket_value});
  }

  changeLines(lines: PurchaseOrderLine[]) {
    this.setState({lines});
  }

  recentlyCreated() {
    this.setState({recentlyCreated: true});
    // NOTE: why do that? because the next time another PO is created {recentlyCreated: true} will be raised again!!!
    this.setState({recentlyCreated: false});
  }

  recentlyUpdated() {
    this.setState({recentlyUpdated: true});
    // NOTE: why do that? because the next time another PO is updated {recentlyUpdated: true} will be raised again!!!
    this.setState({recentlyUpdated: false});
  }

  setupDataPurchaseOrder(purchase_order: PurchaseOrder): PurchaseOrder {
    purchase_order.p_data = {
      ...purchase_order.p_data,
      basket_value: purchase_order.basket_value,
      total_discount: purchase_order.total_discount,
      total_amount: purchase_order.total_amount,
      lines: purchase_order.lines,
      note: purchase_order.note,
    };
    return purchase_order;
  }

}
