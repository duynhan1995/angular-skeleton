import { Injectable } from '@angular/core';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface CustomerData {
  selected_customer: Customer;
  selected_customer_address: CustomerAddress;
}

@Injectable({
  providedIn: 'root'
})
export class CustomerStoreService extends AStore<CustomerData> {
  initState = {
    selected_customer: null,
    selected_customer_address: null
  };

  readonly selectCustomer$ = this.state$.pipe(
    map(({selected_customer}) => selected_customer),
    distinctUntilChanged((prev, curr) => {
      return JSON.stringify(prev) == JSON.stringify(curr);
    })
  );

  readonly selectCustomerAddress$ = this.state$.pipe(
    map(({selected_customer_address}) => selected_customer_address),
    distinctUntilChanged((prev, curr) => {
      return JSON.stringify(prev) == JSON.stringify(curr);
    })
  );

  constructor() {
    super();
    this.state = this.initState;
  }

  selectCustomer(selected_customer: Customer) {
    this.setState({ selected_customer });
  }

  resetSelectedCustomer() {
    this.setState({ selected_customer: null });
  }

  selectCustomerAddress(selected_customer_address: CustomerAddress) {
    this.setState({ selected_customer_address });
  }

  resetSelectedCustomerAddress() {
    this.setState({ selected_customer_address: null });
  }

  resetState() {
    this.state = this.initState;
  }

}
