import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Stocktake, StocktakeLine } from 'libs/models/Stocktake';
import { AuthenticateStore } from '@etop/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

export declare type InventoryType = 'all' | 'in' | 'out' | 'stocktake' ;

export interface InventoryData {
  inventory_type: InventoryType | string;
  stocktakeLines: StocktakeLine[];
  stocktakeQuantity: number;
  discardQuantity: number;
  stocktakeRecentlyCreated: boolean;
  stocktakeRecentlyUpdated: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class InventoryStoreService extends AStore<InventoryData> {
  initState = {
    inventory_type: 'all',
    stocktakeLines: [],
    stocktakeQuantity: 0,
    discardQuantity: 0,
    stocktakeRecentlyCreated: false,
    stocktakeRecentlyUpdated: false
  };

  readonly inventoryTypeChanged$ = this.state$.pipe(
    map(({inventory_type}) => inventory_type),
    distinctUntilChanged()
  );

  readonly stocktakeLinesChanged$ = this.state$.pipe(
    map(({stocktakeLines}) => stocktakeLines),
    distinctUntilChanged()
  );

  readonly stocktakeQuantityChanged$ = this.state$.pipe(
    map(({stocktakeQuantity}) => stocktakeQuantity),
    distinctUntilChanged()
  );

  readonly discardQuantityChanged$ = this.state$.pipe(
    map(({discardQuantity}) => discardQuantity),
    distinctUntilChanged()
  );

  readonly stocktakeRecentlyCreated$ = this.state$.pipe(
    map(({stocktakeRecentlyCreated}) => stocktakeRecentlyCreated),
    distinctUntilChanged()
  );

  readonly stocktakeRecentlyUpdated$ = this.state$.pipe(
    map(({stocktakeRecentlyUpdated}) => stocktakeRecentlyUpdated),
    distinctUntilChanged()
  );

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private route: ActivatedRoute,
    private util: UtilService
  ) {
    super();
    this.state = this.initState;

    const { queryParams } = this.route.snapshot;
    if (queryParams.type) {
      this.changeInventoryType(queryParams.type);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('inventory') != -1) {
          const { queryParams } = this.route.snapshot;
          if (queryParams.type) {
            return this.changeInventoryType(queryParams.type);
          } else {
            this.changeInventoryType('all');
          }
        }
      }
    });

  }

  changeInventoryType(inventory_type: InventoryType | string) {
    this.setState({ inventory_type });
  }

  changeStocktakeLines(stocktakeLines: StocktakeLine[]) {
    this.setState({stocktakeLines});
  }

  changeStocktakeQuantity(stocktakeQuantity: number) {
    this.setState({stocktakeQuantity});
  }

  changeDiscardQuantity(discardQuantity: number) {
    this.setState({discardQuantity});
  }

  stocktakeRecentlyCreated() {
    this.setState({stocktakeRecentlyCreated: true});
    // NOTE: why do that? because the next time another Stocktake is created {stocktakeRecentlyCreated: true} will be raised again!!!
    this.setState({stocktakeRecentlyCreated: false});
  }

  stocktakeRecentlyUpdated() {
    this.setState({stocktakeRecentlyUpdated: true});
    // NOTE: why do that? because the next time another Stocktake is updated {stocktakeRecentlyUpdated: true} will be raised again!!!
    this.setState({stocktakeRecentlyUpdated: false});
  }

  resetData() {
    this.changeStocktakeLines([]);
    this.changeStocktakeQuantity(null);
  }

  setupDataStocktake(stocktake: Stocktake): Stocktake {
    stocktake.p_data = {
      ...stocktake.p_data,
      total_quantity: stocktake.total_quantity,
      discard_quantity: stocktake.discard_quantity,
      lines: stocktake.lines,
      note: stocktake.note,
    };
    return stocktake;
  }

  async navigate(type: string, code: string, id?: string) {
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`s/${slug}/inventory?code=${code || id}&type=${type}`);
  }

}
