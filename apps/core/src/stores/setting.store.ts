import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

export enum SettingMenu {
  shop = 'shop',
  shipping_info = 'shipping',
  transaction = 'transaction',
  staff = 'staff',
  integrated = 'integrated',
  user = 'user',
  accounts = 'accounts',
}

export interface SettingData {
  current_menu: SettingMenu;
}

@Injectable({
  providedIn: 'root'
})
export class SettingStore extends AStore<SettingData> {
  initState = {
    current_menu: SettingMenu.shipping_info
  };

  readonly menuChanged$ = this.state$.pipe(
    map(({current_menu}) => current_menu),
    distinctUntilChanged()
  );

  constructor(
    private router: Router
  ) {
    super();
    this.state = this.initState;

    const menu: any = window.location.pathname.split('/')[4];
    if (menu) {
      this.changeMenu(menu);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.urlAfterRedirects.indexOf('settings') != -1) {
          const menu: any = event.urlAfterRedirects.split('/')[4];
          if (menu) {
            this.changeMenu(menu);
          } else {
            this.changeMenu(SettingMenu.shop);
          }
        }
      }
    });
  }

  changeMenu(menu: SettingMenu) {
    this.setState({current_menu: menu});
  }

}
