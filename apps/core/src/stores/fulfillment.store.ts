import { Injectable } from '@angular/core';
import {CmsService} from "apps/core/src/services/cms.service";
import { Fulfillment } from 'libs/models/Fulfillment';
import { AStore } from 'apps/core/src/interfaces/AStore';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthenticateStore } from '@etop/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

export interface FulfillmentData {
  fulfillment: Fulfillment;

  defaultWeight: number;

  to_create_ffm: boolean;
  to_create_customer_by_shipping_address?: boolean;

  error_cod: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class FulfillmentStore extends AStore<FulfillmentData> {
  initState = {
    fulfillment: null,

    defaultWeight: null,

    error_cod: false,
    to_create_ffm: false,
    to_create_customer_by_shipping_address: true,
  };

  readonly fufillmentValueChanged$ = this.state$.pipe(
    map(({fulfillment}) => fulfillment),
    distinctUntilChanged()
  );

  readonly toCreateFfmToggled$ = this.state$.pipe(
    map(({to_create_ffm}) => to_create_ffm),
    distinctUntilChanged()
  );

  fulfillmentCancelled$ = new Subject();

  constructor(
    private auth: AuthenticateStore,
    private cms: CmsService,
    private router: Router,
    private util: UtilService
  ) {
    super();
    this.state = this.initState;
  }

  initDefaultWeight() {
    const shopDefaultWeight = this.auth.snapshot.shop.survey_info.find(info => info.key == 'default_weight')?.answer;
    if (this.cms.bannersLoaded) {
      this.setState({ defaultWeight: (shopDefaultWeight && Number(shopDefaultWeight)) || this.cms.getDefaultWeight() || 250 });
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        this.setState({ defaultWeight: (shopDefaultWeight && Number(shopDefaultWeight)) || this.cms.getDefaultWeight() || 250 });
      });
    }
  }

  createFulfillment() {
    const to_create_ffm = !this.snapshot.to_create_ffm;
    this.setState({ to_create_ffm });
  }

  discardCreatingFulfillment() {
    this.setState({ to_create_ffm: false });
  }

  updateFulfillment(fulfillment: Fulfillment) {
    this.setState({ fulfillment });
  }

  cancelFulfillment() {
    this.fulfillmentCancelled$.next();
  }

  createCustomerByShippingAddress(to_create_customer_by_shipping_address: boolean) {
    this.setState({ to_create_customer_by_shipping_address });
  }

  errorCOD(error_cod: boolean) {
    this.setState({ error_cod });
  }

  resetState() {
    const _defaultWeight = this.snapshot.defaultWeight;
    this.setState({
      ...this.initState,
      defaultWeight: _defaultWeight
    });
  }

  async navigate(type: string, code: string, id?: string, use_slug = true) {
    if (!use_slug) {
      await this.router.navigateByUrl(`fulfillments?code=${code || id}&type=${type}`);
      return;
    }
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`s/${slug}/fulfillments?code=${code || id}&type=${type}`);
  }

}
