import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { Order } from 'libs/models/Order';

export declare type HeaderTab = 'orders' | 'purchase_orders';

export interface OrderData {
  header_tab?: HeaderTab | string;
  queryParams?: string;

  basket_value?: number;
  total_amount?: number;

  creating_order: boolean;
  selected_order: Order;
}

const headerTabMap = {
  so: 'orders',
  po: 'purchase_orders'
};

@Injectable({
  providedIn: 'root'
})
export class OrderStoreService extends AStore<OrderData> {
  initState = {
    header_tab: 'orders',
    creating_order: false,
    selected_order: null
  };

  static get headerTabMap() {
    return headerTabMap;
  }

  readonly headerTabChanged$ = this.state$.pipe(
    map(({header_tab}) => header_tab),
    distinctUntilChanged()
  );

  readonly creatingOrder$ = this.state$.pipe(
    map(({creating_order}) => creating_order),
    distinctUntilChanged()
  );

  readonly updateTotalAmount$ = this.state$.pipe(
    map(({total_amount}) => total_amount),
    distinctUntilChanged()
  );

  readonly selectOrder$ = this.state$.pipe(
    map(({selected_order}) => selected_order),
    distinctUntilChanged((prev, curr) => {
      return JSON.stringify(prev) == JSON.stringify(curr);
    })
  );

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private route: ActivatedRoute,
    private util: UtilService
  ) {
    super();
    this.state = this.initState;

    const { queryParams } = this.route.snapshot;
    if (queryParams.type) {
      this.changeHeaderTab(OrderStoreService.headerTabMapByParams(queryParams.type));
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('orders') != -1) {
          const { queryParams } = this.route.snapshot;
          if (queryParams.type) {
            return this.changeHeaderTab(OrderStoreService.headerTabMapByParams(queryParams.type));
          } else {
            this.changeHeaderTab('orders');
          }
        }
      }
    });

  }

  static headerTabMapByParams(params_type: string) {
    return this.headerTabMap[params_type];
  }

  changeHeaderTab(header_tab: HeaderTab | string) {
    this.setState({ header_tab });
  }

  creatingOrder(creating_order: boolean) {
    this.setState({ creating_order });
  }

  updateBasketValue(basket_value: number) {
    this.setState({ basket_value });
  }

  updateTotalAmount(total_amount: number) {
    this.setState({ total_amount });
  }

  selectOrder(selected_order: Order) {
    this.setState({ selected_order });
  }

  async navigate(type: string, code: string, id?: string, use_slug = true) {
    if (!use_slug) {
      await this.router.navigateByUrl(`orders?code=${code || id}&type=${type}`);
      return;
    }
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`s/${slug}/orders?code=${code || id}&type=${type}`);
  }

}
