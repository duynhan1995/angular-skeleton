import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export declare type HeaderTab = 'pos' | 'topship';

export interface DashboardData {
  header_tab?: HeaderTab | string;
}

@Injectable({
  providedIn: 'root'
})
export class DashboardStoreService extends AStore<DashboardData> {
  initState = { header_tab: 'pos' };

  readonly headerTabChanged$ = this.state$.pipe(
    map(({ header_tab }) => header_tab),
    distinctUntilChanged()
  );

  constructor() {
    super();
    this.state = this.initState;
  }

  changeHeaderTab(header_tab: HeaderTab | string) {
    this.setState({ header_tab });
  }
}
