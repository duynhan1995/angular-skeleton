import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from "@/views/Login.vue";
import LoginVerify from "@/views/LoginVerify.vue";
import Register from "@/views/Register.vue";
import Success from "@/views/Success.vue";
import ErrorPage from "@/views/Error.vue";
import GrantAccess from "@/views/GrantAccess.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/login/verify",
    name: "verify",
    component: LoginVerify,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/success",
    name: "success",
    component: Success,
  },
  {
    path: "/error",
    component: ErrorPage,
    name: 'error',
  },
  {
    path: "/grant-access",
    name: "grant-access",
    component: GrantAccess,
  },
  {
    path: "/test",
    name: "test",
    component: GrantAccess,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
