import util from '../helpers/util';

const createAsyncMutation = (type: string) => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  PENDING: `${type}_PENDING`,
  loadingKey: util.toCamelcase(`${type}_PENDING`)
});

export const LOGIN_REQUEST_STATUS = createAsyncMutation('LOGIN_REQUEST_STATUS');
export const LOGIN_REQUEST_TOKEN = 'login_request_token';
export const AUTH_REQUEST_STATUS = createAsyncMutation('AUTH_REQUEST_STATUS');
export const AUTH_CHECK_TOKEN_STATUS = createAsyncMutation('AUTH_CHECK_TOKEN_STATUS');
export const AUTH_ACCESS_TOKEN = 'auth_access_token';
export const AUTH_ACCESS_ACTIONS = 'auth_access_actions';
export const REGISTER_ACCOUNT_STATUS = createAsyncMutation('REGISTER_ACCOUNT_STATUS');
export const REGISTER_SHOP_STATUS = createAsyncMutation('REGISTER_SHOP_STATUS');
export const LOGIN_VERIFY_STATUS = createAsyncMutation('LOGIN_VERIFY_STATUS');
export const AVAILABLE_ACCOUNTS = 'available_accounts';
export const PARTNER_INFO = 'partner_info';
export const SHOP_ID = 'shop_id';
export const SHOP_ACCOUNT = 'shop_account';
export const SHOP_ACCOUNT_TOKEN = 'shop_account_token';
export const LOGIN_STEP = 'login_step';
export const LOGIN_USERNAME = 'login_username';
export const GRANT_ACCESS_STATUS = 'grant_access_statue';
export const REDIRECT_URL = 'redirect_url';
export const PARTNER_TOKEN = 'partner_token';
export const RECAPTCHA_TOKEN = 'recaptcha_token';
export const ERROR_MESSAGE = 'error_message';
