import * as types from './mutation-types';
import util from '../helpers/util';

export default {
  // [types.AUTH_REQUEST_STATUS.PENDING]: (state) => {
  //   state[types.AUTH_REQUEST_STATUS.loadingKey] = true
  // },
  // [types.AUTH_REQUEST_STATUS.SUCCESS]: (state, data) => {
  //   state[types.AUTH_REQUEST_STATUS.stateKey] = data;
  // },
  [types.LOGIN_REQUEST_STATUS.PENDING]: (state: any) => {
    state.loginRequestStatusPending = true;
  },
  [types.LOGIN_REQUEST_STATUS.SUCCESS]: (state: any) => {
    state.loginRequestStatusPending = false;
  },
  [types.LOGIN_REQUEST_STATUS.FAILURE]: (state: any) => {
    state.loginRequestStatusPending = false;
  },
  [types.LOGIN_REQUEST_TOKEN]: (state: any, data: any) => {
    state[util.toCamelcase(types.LOGIN_REQUEST_TOKEN)] = data;
  },
  [types.AUTH_CHECK_TOKEN_STATUS.PENDING]: (state: any) => {
    state[types.AUTH_CHECK_TOKEN_STATUS.loadingKey] = true;
  },
  [types.AUTH_CHECK_TOKEN_STATUS.SUCCESS]: (state: any) => {
    state[types.AUTH_CHECK_TOKEN_STATUS.loadingKey] = false;
  },
  [types.AUTH_ACCESS_TOKEN]: (state: any, token: any) => {
    state[util.toCamelcase(types.AUTH_ACCESS_TOKEN)] = token;
  },
  [types.AUTH_ACCESS_ACTIONS]: (state: any, actions: any) => {
    state[util.toCamelcase(types.AUTH_ACCESS_ACTIONS)] = actions;
  },
  [types.REGISTER_ACCOUNT_STATUS.PENDING]: (state: any) => {
    state[util.toCamelcase(types.REGISTER_ACCOUNT_STATUS.loadingKey)] = true;
  },
  [types.REGISTER_ACCOUNT_STATUS.SUCCESS]: (state: any) => {
    state[util.toCamelcase(types.REGISTER_ACCOUNT_STATUS.loadingKey)] = false;
  },
  [types.REGISTER_SHOP_STATUS.PENDING]: (state: any) => {
    state[util.toCamelcase(types.REGISTER_SHOP_STATUS.loadingKey)] = true;
  },
  [types.REGISTER_SHOP_STATUS.SUCCESS]: (state: any) => {
    state[util.toCamelcase(types.REGISTER_SHOP_STATUS.loadingKey)] = false;
  },
  [types.REGISTER_SHOP_STATUS.FAILURE]: (state: any) => {
    state[util.toCamelcase(types.REGISTER_SHOP_STATUS.loadingKey)] = false;
  },
  [types.LOGIN_VERIFY_STATUS.PENDING]: (state: any) => {
    state[util.toCamelcase(types.LOGIN_VERIFY_STATUS.loadingKey)] = true;
  },
  [types.LOGIN_VERIFY_STATUS.FAILURE]: (state: any) => {
    state[util.toCamelcase(types.LOGIN_VERIFY_STATUS.loadingKey)] = false;
  },
  [types.LOGIN_VERIFY_STATUS.SUCCESS]: (state: any) => {
    state[util.toCamelcase(types.LOGIN_VERIFY_STATUS.loadingKey)] = false;
  },
  [types.AVAILABLE_ACCOUNTS]: (state: any, available_accounts: any) => {
    state[util.toCamelcase(types.AVAILABLE_ACCOUNTS)] = available_accounts;
  },
  [types.PARTNER_INFO]: (state: any, partner: any) => {
    state[util.toCamelcase(types.PARTNER_INFO)] = partner;
  },
  [types.SHOP_ID]: (state: any, shopId: any) => {
    state[util.toCamelcase(types.SHOP_ID)] = shopId;
  },
  [types.SHOP_ACCOUNT]: (state: any, account: any) => {
    state[util.toCamelcase(types.SHOP_ACCOUNT)] = account;
  },
  [types.SHOP_ACCOUNT_TOKEN]: (state: any, token: any) => {
    state[util.toCamelcase(types.SHOP_ACCOUNT_TOKEN)] = token;
  },
  [types.LOGIN_USERNAME]: (state: any, username: any) => {
    state[util.toCamelcase(types.LOGIN_USERNAME)] = username;
  },
  [types.GRANT_ACCESS_STATUS]: (state: any, status: any) => {
    state[util.toCamelcase(types.GRANT_ACCESS_STATUS)] = status;
  },
  [types.LOGIN_STEP]: (state: any, step: any) => {
    state[util.toCamelcase(types.LOGIN_STEP)] = step;
  },
  [types.REDIRECT_URL]: (state: any, url: any) => {
    state[util.toCamelcase(types.REDIRECT_URL)] = url;
  },
  [types.PARTNER_TOKEN]: (state: any, token: any) => {
    state.partnerToken = token;
  },
  [types.PARTNER_INFO]: (state: any, partner_info: any) => {
    state.partnerInfo = partner_info;
  },
  [types.RECAPTCHA_TOKEN]: (state: any, token: any) => {
    state.recaptchaToken = token;
  },
  [types.ERROR_MESSAGE]: (state: any, message: any) => {
    state.errorMessage = message;
  }
};
