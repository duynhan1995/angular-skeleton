import userService from "../apis/user-service";
import * as types from "./mutation-types";
import util from "../helpers/util";
import {InitException} from "./error-types";

export default {
  updatePartnerToken(context: any, token: any) {
    context.commit(types.PARTNER_TOKEN, token);
  },
  updateRecaptchaToken({commit}: any, token: string) {
    console.log('action:updateRecaptchaToken', token);
    commit(types.RECAPTCHA_TOKEN, token)
  },
  setErrorMessage({commit}: any, message: any) {
    commit(types.ERROR_MESSAGE, message);
  },
  setAuthAccessToken({commit}, authToken) {
    commit(types.AUTH_ACCESS_TOKEN, authToken);
  },
  authorizeShop({commit}: any, token: any) {

  },
  checkToken({commit, state}: any) {
    return userService.initIntergration(state.partnerToken, commit)
      .then((resp: any) => {
        const {data} = resp;
        if (!data.redirect_url) {
          throw new InitException("Thiếu redirect_url", "invalid_redirect_url");
        }

        commit(types.AUTH_ACCESS_TOKEN, data.access_token);
        commit(types.AUTH_ACCESS_ACTIONS, data.actions);
        commit(types.REDIRECT_URL, data.redirect_url);
        commit(types.LOGIN_REQUEST_TOKEN, data.access_token);
        commit(types.PARTNER_INFO, data.auth_partner);

        return resp;
      });
  },
  registerShop({commit, state}: any, data: any) {
    const shop = data;
    const shopAccountToken = state[util.toCamelcase(types.SHOP_ACCOUNT_TOKEN)];
    if (!shopAccountToken) {
      return Promise.reject("missing shop account token");
    }

    return userService
      .registerShop(shop, shopAccountToken, commit)
      .then((resp: any) => {
        const shopId = resp.data.shop && resp.data.shop.id;
        commit(types.SHOP_ID, shopId);
        return shopId;
      });
  },
  registerIntergrationAccount({commit, state}: any, data: any) {
    // const {email, name, phone} = data;
    if (!data) {
      const actions = state[util.toCamelcase(types.AUTH_ACCESS_ACTIONS)];
      const action = actions[0];
      if (!action) {
        return Promise.reject("Invalid actions");
      }

      data = action.meta;
    }

    const token = state[util.toCamelcase(types.LOGIN_REQUEST_TOKEN)];
    return userService
      .registerIntegrationAccount(data, token, commit)
      .then((resp: any) => {
        const {user, access_token} = resp.data;
        commit(types.SHOP_ACCOUNT, user);
        commit(types.SHOP_ACCOUNT_TOKEN, access_token);
        return resp;
      });
  },
  requestLogin(context: any, data: any) {
    const {commit, state} = context;
    const {username, resend} = data;
    const token = state[util.toCamelcase(types.AUTH_ACCESS_TOKEN)];
    const recapt = state[util.toCamelcase(types.RECAPTCHA_TOKEN)];

    console.log('request Login', state, recapt);

    return (resend ? userService.getReCaptcha() : Promise.resolve(recapt))
      .then((recaptcha: any) => userService.requestLogin(username, token, recaptcha, commit))
      .then((resp: any) => {
        commit(types.LOGIN_STEP, 2);
        commit(types.LOGIN_USERNAME, username);
        return resp;
      });
  },
  loginUsingToken({commit, state}: any, data: any) {
    const {code} = data;
    const loginUsername = state[util.toCamelcase(types.LOGIN_USERNAME)];
    const authAccessToken = state[util.toCamelcase(types.AUTH_ACCESS_TOKEN)];
    return userService.loginUsingTokenWL(loginUsername, code, authAccessToken, commit)
      .then((resp: any) => {
        if (resp.data.code === '"unauthenticated"') {
          throw Error(resp.data.msg);
        }

        const result = resp.data;
        const shopAccounts = result.available_accounts.filter(a => a.type == "shop");

        commit(types.LOGIN_REQUEST_TOKEN, result.access_token);
        commit(types.AVAILABLE_ACCOUNTS, shopAccounts);
        commit(types.PARTNER_INFO, result.auth_partner);
        if (shopAccounts.length && result.user) {
          const shopId = shopAccounts[0].id;
          commit(types.SHOP_ID, shopId);
          commit(types.SHOP_ACCOUNT_TOKEN, result.access_token);
        }

        return result;
      });
  },
  grantAccess({commit, state}: any, shopId: any) {
    console.log("GRANT ACCESSS", shopId);
    const shopAccountToken = state[util.toCamelcase(types.SHOP_ACCOUNT_TOKEN)];
    return userService.grantAccess(shopId, shopAccountToken)
      .then((resp: any) => {
        if (resp.data.access_token) {
          commit(types.GRANT_ACCESS_STATUS, 'success');
        }

        return resp;
      });
  },
};
