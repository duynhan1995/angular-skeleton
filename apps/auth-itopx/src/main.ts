import Vue from 'vue';
import CxltToastr from 'cxlt-vue2-toastr';
import App from './App.vue';
import router from './router';
import store from './store';
import "./assets/scss/global.scss";
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';

declare const window: any;

Vue.config.productionTip = false;

function updatePartnerToken(token) {
  return store.dispatch('updatePartnerToken', token);
}

function updateRecaptchaToken(token) {
  console.log('updateRecaptchaToken', token);
  return store.dispatch('updateRecaptchaToken', token);
}

function setErrorMessage(msg) {
  return store.dispatch('setErrorMessage', msg);
}

function partnerToken() {
  return store.state.partnerToken;
}

function hasQueryParams(route, key) {
  return !!route.query[key];
}

function getGlobalQuery() {
  return {token: partnerToken()};
}

router.beforeEach((to, from, next) => {
  if (to.name === "test") {
    return next();
  }

  if (to.name === "login") {
    console.log('query', to.query.recaptcha_token);
    updatePartnerToken(to.query.token || "");
    updateRecaptchaToken(to.query.recaptcha_token || "")
  }

  if (!partnerToken() && to.name !== 'error') {
    setErrorMessage("Thiếu token, vui lòng kiểm tra lại đường dẫn đúng với cấu trúc: <code>/login?token=yourparnertoken</code>");
    return next({name: 'error'});
  }

  if (!hasQueryParams(to, 'token') && to.name !== 'error') {
    next({name: to.name, query: Object.assign(getGlobalQuery(), to.query)} as any);
  } else {
    next();
  }
});

const toastrConfigs = {
  position: 'top right',
  showDuration: 500,
  timeOut: 5000,
};

Vue.use(CxltToastr, toastrConfigs);

window.vuewApp = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
