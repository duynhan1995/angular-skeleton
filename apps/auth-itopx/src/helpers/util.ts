const camelcase = require('lodash.camelcase');

export default {
  toCamelcase: (string: string) => camelcase(string),
  isEmail(email: string) {
    return /(.+)@(.+)\.(.+)/.test(email);
  },
  isPhone(phone: string) {
    return /\d{10,11}/.test(phone);
  },
  toQueryUrl(baseUrl: string, params: any) {
    const queryString = Object.keys(params).map((key) => `${key}=${params[key]}`).join('&');
    const lastSplash = baseUrl.lastIndexOf("/");
    if (baseUrl.substr(lastSplash + 1).indexOf("=") > -1) {
      baseUrl = `${baseUrl}&`;
    } else {
      baseUrl = `${baseUrl}?`;
    }
    return baseUrl + queryString;
  },
  decodeBase64(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), (c) => `%${(`00${c.charCodeAt(0).toString(16)}`).slice(-2)}`).join(''));
  },
  encodeBase64(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => String.fromCharCode(parseInt(`0x${p1}`, 16))));
  },
}
