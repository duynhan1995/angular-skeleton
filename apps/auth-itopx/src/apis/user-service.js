import request from './request';
import * as mutationTypes from '../store/mutation-types';

const clientRequest = request();

export default {
  authorizeShop() {
    return clientRequest.post('api/partner.Shop/AuthorizeShop', {});
  },

  initIntergration(auth_token, commit) {
    return clientRequest.post('api/integration.Integration/Init', {
      auth_token,
    }, {
      commit,
      mutationType: mutationTypes.AUTH_CHECK_TOKEN_STATUS,
    });
  },

  registerIntegrationAccount(data, token, commit) {
    const req = request(token);
    return req.post(
      'api/integration.Integration/Register',
      data,
      {
        commit,
        mutationType: mutationTypes.REGISTER_ACCOUNT_STATUS,
      },
    );
  },

  registerShop(data, shopAccountToken, commit) {
    const req = request(shopAccountToken);
    return req.post('api/shop.Account/RegisterShop', data, {
      commit,
      mutationType: mutationTypes.REGISTER_SHOP_STATUS,
    });
  },

  requestLogin(login, token, captcha, commit) {
    const req = request();
    req.setToken(token);
    return req.post('api/integration.Integration/RequestLogin', {
      login,
      recaptcha_token: captcha,
    }, {
      commit,
      mutationType: mutationTypes.LOGIN_REQUEST_STATUS,
    });
  },

  loginUsingToken(login, verification_code, token, commit) {
    const req = request();
    req.setToken(token);
    return req.post('api/integration.Integration/LoginUsingToken', {
      login,
      verification_code,
    }, {
      commit,
      mutationType: mutationTypes.LOGIN_VERIFY_STATUS,
    });
  },

  loginUsingTokenWL(login, verification_code, token, commit) {
    const req = request();
    req.setToken(token);
    return req.post('api/integration.Integration/LoginUsingTokenWL', {
      login,
      verification_code,
    }, {
      commit,
      mutationType: mutationTypes.LOGIN_VERIFY_STATUS,
    });
  },

  grantAccess(shop_id, shopAccountToken) {
    const req = request(shopAccountToken);
    return req.post('api/integration.Integration/GrantAccess', {
      shop_id,
    });
  },

  getReCaptcha() {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line no-undef
      if (!grecaptcha) {
        return reject('Please include Google Recaptcha script!');
      }

      if (process.env.NODE_ENV === 'development') {
        return resolve('recaptcha_token');
      }

      // eslint-disable-next-line no-undef
      grecaptcha.ready(async () => {
        try {
          // eslint-disable-next-line no-undef
          const token = await grecaptcha.execute(process.env.VUE_APP_RECAPTCHA_KEY, { action: 'login' });
          resolve(token);
        } catch (e) {
          reject(`get captcha failed`);
        }
      });
    });
  },
};
