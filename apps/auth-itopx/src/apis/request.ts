import axios from 'axios';

declare const location: any;

const baseURL = process.env.VUE_APP_API_BASE_URL || location.origin;

function createClient(token?) {
  const headers: any = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  if (token) {
    headers.Authorization = token;
  }
  return axios.create({
    baseURL,
    timeout: 20000,
    withCredentials: false,
    headers,
  });
}

export default (token) => {
  let authToken = token;
  const client = createClient();

  function setToken(tkn) {
    if (!tkn) {
      return;
    }
    authToken = tkn;
    client.defaults.headers.Authorization = `Bearer ${tkn}`;
  }

  function doRequest(method, url, data?) {
    return (reduxStuff, options?) => {
      let commit;
      let mutationType;
      if (reduxStuff) {
        commit = reduxStuff.commit;
        mutationType = reduxStuff.mutationType;
      }

      if (commit) {
        commit(mutationType.PENDING);
      }

      return client[method](url, data, options)
        .then(resp => {
          if (typeof resp.data === 'string') {
            throw Error('failed');
          }

          if (commit) {
            commit(mutationType.SUCCESS, resp.data);
          }
          return resp;
        })
        .catch(err => {
          if (commit) {
            commit(mutationType.FAILURE, err);
          }
          throw Error(err.response && err.response.data.msg || err.message);
        });
    };
  }

  setToken(token);

  return {
    setToken,
    get(url, reduxStuff) {
      return doRequest('get', url)(reduxStuff);
    },
    post(url, data, reduxStuff) {
      return doRequest('post', url, data)(reduxStuff);
    },
    put(url, data, reduxStuff) {
      return doRequest('put', url, data)(reduxStuff);
    },
    delete(url, reduxStuff) {
      return doRequest('delete', url)(reduxStuff);
    },
    postForm(url, formData, reduxStuff) {
      return doRequest('post', url, formData)({
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }, reduxStuff);
    },
    putForm(url, formData, reduxStuff) {
      return doRequest('put', url, formData)({
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }, reduxStuff);
    },
  };
};
