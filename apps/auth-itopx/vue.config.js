const path = require('path');
const argv = require('yargs').argv;

function getConfig(env) {
  const configs = {
    'production': {
      name: 'v1prod',
      mode: 'production',
      sourceMap: false,
      itopxAuthUrl: '//itopx.vn/welcome',
      itopxRedirectUrl: '//id.imgroup.vn?p=itopx',
    },
    'development': {
      name: 'dev',
      mode: 'development',
      sourceMap: 'inline-source-map',
      itopxAuthUrl: '//itopx.d.etop.vn/welcome',
      itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
    },
    'sandbox': {
      name: 'sandbox',
      mode: 'production',
      sourceMap: 'inline-source-map',
      itopxAuthUrl: '//itopx.sandbox.etop.vn/welcome',
      itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
    },
    'stage': {
      name: 'stage',
      mode: 'development',
      sourceMap: 'inline-source-map',
      itopxAuthUrl: '//itopx.g.etop.vn/welcome',
      itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
    }
  };
  const config = {
    name: 'dev',
    mode: 'development',
    sourceMap: 'inline-source-map',
    itopxAuthUrl: '//itopx.d.etop.vn/welcome',
    itopxRedirectUrl: 'http://id.webitop.com/itopx/login?p=itopx',
  };
  return {
    ...config,
    ...configs[env]
  };
}

const cfg = getConfig(argv.configuration);

module.exports = {
  devServer: {
    proxy: {
      '^/api': {
        target: 'https://pos.itopx.vn',
        crossOrigin: true,
      },
      '^/v1': {
        target: 'https://pos.itopx.vn',
        crossOrigin: true,
      },
    },
  },
  outputDir: path.join(__dirname, '../../dist', cfg.name, 'whitelabel', 'itopx.vn', 'welcome'),
  publicPath: '/welcome/'
};
