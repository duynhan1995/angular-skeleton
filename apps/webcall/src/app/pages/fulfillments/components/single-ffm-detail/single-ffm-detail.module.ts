import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { SingleFfmDetailComponent } from './single-ffm-detail.component';
import { DetailInfoComponent } from './detail-info/detail-info.component';
import { FulfillmentHistoriesModule } from 'apps/webcall/src/app/components/fulfillment-histories/fulfillment-histories.module';
import { FulfillmentShipnowRoutesModule } from 'apps/webcall/src/app/components/fulfillment-shipnow-routes/fulfillment-shipnow-routes.module';


@NgModule({
  declarations: [
    SingleFfmDetailComponent,
    DetailInfoComponent,
  ],
  exports: [
    SingleFfmDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    FulfillmentHistoriesModule,
    FulfillmentShipnowRoutesModule,
    EtopMaterialModule,
    EtopPipesModule,
  ],
})
export class SingleFfmDetailModule { }
