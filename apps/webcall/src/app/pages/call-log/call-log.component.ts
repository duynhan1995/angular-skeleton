import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CallLogQuery, CallLogService, CallLogStore } from '@etop/state/shop/call-log';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from '../../../../../core/src/components/modal-controller/modal-controller.service';
import { HotlineService } from '@etop/state/shop/hotline';
import { ContactService } from '@etop/state/shop/contact';
import { RelationshipService } from '@etop/state/relationship';
import { ExtensionService } from '@etop/state/shop/extension';
import { MobileHeader } from '../../components/mobile-header/mobile-header.component';
import { AuthenticateStore } from '@etop/core';
@Component({
  selector: 'webcall-call-log',
  template: `
    <webcall-mobile-header [header]='mobileHeader'></webcall-mobile-header>
    <webcall-desktop-call-log class='hide-768' style="height: 100%"></webcall-desktop-call-log>
    <webcall-mobile-call-log class='show-768'></webcall-mobile-call-log>`,
  styleUrls: ['./call-log.component.scss']
})
export class CallLogComponent implements OnInit {
  mobileHeader: MobileHeader = {
    name: 'Lịch sử cuộc gọi',
  }
  constructor(
    private callLogService: CallLogService,
    private ref: ChangeDetectorRef,
    private authorizationApi: AuthorizationApi,
    private modalController: ModalController,
    private hotlineService: HotlineService,
    private contactService: ContactService,
    private relationshipService: RelationshipService,
    private extensionService: ExtensionService,
    private callLogQuery: CallLogQuery,
    private callLogStore: CallLogStore,
    private cdr: ChangeDetectorRef,
    private auth: AuthenticateStore
  ) { }

  async ngOnInit() {
    this.callLogStore.setLoading(true);
    await this._prepareData();
    this.callLogStore.setLoading(false);
  }
  async _prepareData() {
    const paging = {
      limit: 20,
      after: '.'
    };
    this.callLogService.setPaging(paging);
    await this.relationshipService.getRelationships();
    await this.hotlineService.getHotlines();
    await this.contactService.getContacts();
    await this.extensionService.getExtensions();
    await this.callLogService.getCallLogs();
    this.mappingCallLogs();
    this.cdr.detectChanges();
  }

  mappingCallLogs() {
    let callLogs = this.callLogQuery.getAll();
    callLogs = this.callLogService.callLogsMap(callLogs);
    this.callLogService.setCallLogs(callLogs);
  }
}
