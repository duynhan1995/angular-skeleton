import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from 'apps/shared/src/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {SettingsComponent} from 'apps/webcall/src/app/pages/settings/settings.component';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {AuthenticateModule} from '@etop/core';
import {UserInfoComponent} from 'apps/webcall/src/app/pages/settings/components/user-info/user-info.component';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {AccountsInfoComponent} from './components/accounts-info/accounts-info.component';
import {MyShopsManagementComponent} from './components/my-shops-management/my-shops-management.component';
import {NotMyShopsManagamentComponent} from './components/not-my-shops-managament/not-my-shops-managament.component';
import {ShopsManagementRowComponent} from './components/not-my-shops-row/shops-management-row.component';
import {AccountInvitationRowComponent} from './components/account-invitation-row/account-invitation-row.component';
import {GoogleAnalyticsService} from 'apps/core/src/services/google-analytics.service';
import {MobileHeaderModule} from '../../components/mobile-header/mobile-header.module';
import {MobileAccountInvitationRowComponent} from './components/mobile-account-invitation-row/mobile-account-invitation-row.component';
import {ChangePasswordModalComponent} from './components/change-password-modal/change-password-modal.component';
import {CreateShopModalComponent} from './components/create-shop-modal/create-shop-modal.component';
import {ShippingInfoComponent} from './shipping-info/shipping-info.component';
import {FromAddressComponent} from './from-address/from-address.component';
import {CarrierConnectComponent} from './carrier-connect/carrier-connect.component';
import {CarrierConnectRowComponent} from './carrier-connect-row/carrier-connect-row.component';
import {ShopInfoComponent} from './shop-info/shop-info.component';
import {CarrierConnectModalComponent} from './carrier-connect-modal/carrier-connect-modal.component';
import {ConnectCarrierUsingPhoneModalComponent} from './connect-carrier-using-phone-modal/connect-carrier-using-phone-modal.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'user',
        component: UserInfoComponent
      },
      {
        path: 'accounts',
        component: AccountsInfoComponent
      },
      {
        path: 'shop',
        component: ShopInfoComponent,
      },
      {
        path: 'shipping_info',
        component: ShippingInfoComponent,
      },
      // {
      //   path: 'shipping',
      //   component: ShippingInfoComponent,
      // },
      {
        path: '**', redirectTo: 'user'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ModalControllerModule,
    AuthenticateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    NgbModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    MobileHeaderModule,
    MaterialModule
  ],
  exports: [],
  declarations: [
    ChangePasswordModalComponent,
    SettingsComponent,
    UserInfoComponent,
    ShopInfoComponent,
    MyShopsManagementComponent,
    NotMyShopsManagamentComponent,
    ShopsManagementRowComponent,
    AccountsInfoComponent,
    AccountInvitationRowComponent,
    MobileAccountInvitationRowComponent,
    CreateShopModalComponent,
    ShippingInfoComponent,
    FromAddressComponent,
    CarrierConnectComponent,
    CarrierConnectRowComponent,
    CarrierConnectModalComponent,
    ConnectCarrierUsingPhoneModalComponent,
  ],
  providers: [
    GoogleAnalyticsService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {
}
