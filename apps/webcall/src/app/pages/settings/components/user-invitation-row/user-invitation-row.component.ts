import { Component, Input, OnInit } from '@angular/core';
import { Invitation } from 'libs/models/Authorization';
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: '[webcall-user-invitation-row]',
  templateUrl: './user-invitation-row.component.html',
  styleUrls: ['./user-invitation-row.component.scss']
})
export class UserInvitationRowComponent implements OnInit {
  @Input() invitation = new Invitation({});
  deleting = false;

  dropdownActions = [];

  constructor(
    private authorizationService: AuthorizationService
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Huỷ lời mời',
        permissions: ['relationship/invitation:delete'],
        cssClass: 'text-danger',
        onClick: () => this.deleteInvitation()
      }
    ];
  }

  async deleteInvitation() {
    this.deleting = true;
    try {
      await this.authorizationService.deleteInvitation(this.invitation.token);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in deleting Invitation', e);
    }
    this.deleting = false;
  }

}
