import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AuthenticateStore } from '@etop/core';
import { Account } from 'libs/models/Account';
import { AppService } from '@etop/web/core/app.service';
import { CreateShopModalComponent } from '../create-shop-modal/create-shop-modal.component';

@Component({
  selector: 'webcall-my-shops-management',
  templateUrl: './my-shops-management.component.html',
  styleUrls: ['./my-shops-management.component.scss']
})
export class MyShopsManagementComponent implements OnInit, OnChanges {
  @Input() accounts: Account[] = [];
  @Input() currentShop: any = {};
  currentAccount: Account;
  constructor(
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private appService: AppService
  ) {}

  ngOnInit() {
  }

  ngOnChanges() {
    this.currentAccount = this.accounts.find(acc => acc?.id == this?.currentShop?.id);
  }

  openCreateShopModal() {
    const modal = this.modalController.create({
      component: CreateShopModalComponent,
      componentProps: {
        shopCurrent: this.auth.snapshot.shop
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  isMyShop(roles: string[]) {
    if (roles && roles.length) {
      return roles.indexOf('owner') != -1;
    }
    return true;
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }
}
