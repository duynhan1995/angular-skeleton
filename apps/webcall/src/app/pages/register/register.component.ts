import { Component, OnInit } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'webcall-register',
  template: `
    <webcall-mobile-register class='show-768'></webcall-mobile-register>
    <webcall-desktop-register class='hide-768'></webcall-desktop-register>
  `,
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private util: UtilService
  ) { }

  ngOnInit(): void {
  }
}
