import { Injectable } from '@angular/core';
import { Customer } from 'libs/models/Customer';
import { CustomerListComponent } from './customer-list/customer-list.component';

@Injectable()
export class TradersControllerService {
  private customerListComponent: CustomerListComponent;

  constructor() { }

  registerCustomerList(instance: CustomerListComponent) {
    this.customerListComponent = instance;
  }

  async reloadCustomerList() {
    await this.customerListComponent.reloadCustomers();
  }

  setupDataCustomer(customer: Customer): Customer {
    customer.p_data = {
      ...customer.p_data,
      id: customer.id,
      full_name: customer.full_name,
      phone: customer.phone,
      gender: customer.gender,
      email: customer.email,
      birthday: customer.birthday,
      note: customer.note,
      edited: false,
      editedField: {}
    };
    return customer;
  }

}
