import { Component, OnInit, ViewChild } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { MobileHeader } from '../../components/mobile-header/mobile-header.component';
import { CustomerListComponent } from './customer-list/customer-list.component';

@Component({
  selector: 'webcall-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent extends PageBaseComponent implements OnInit {
  @ViewChild('customerList', {static: true}) customerList: CustomerListComponent;
  mobileHeader : MobileHeader = {
    name: 'Khách hàng',
    actions: [
      {
        name: 'Thêm khách hàng',
        handleAction: () => this.customerList.createNewCustomer(),
      },
    ]
  }

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
