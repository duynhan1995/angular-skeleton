import { Component, Input, OnInit } from '@angular/core';
import { Customer } from '@etop/models';

@Component({
  selector: '[webcall-customer-row]',
  templateUrl: './customer-row.component.html',
  styleUrls: ['./customer-row.component.scss']
})
export class CustomerRowComponent implements OnInit {
  @Input() customer = new Customer({});
  @Input() liteMode = false;

  constructor() {}

  ngOnInit() {}

}
