import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { CustomerAddress } from '@etop/models';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { DeleteCustomerModalComponent } from '../delete-customer-modal/delete-customer-modal.component';
import { CustomerService } from 'apps/webcall/src/services/customer.service';
import { CustomerAddressModalComponent } from '../customer-address-modal/customer-address-modal.component';

@Component({
  selector: 'webcall-customer-addresses',
  templateUrl: './customer-addresses.component.html',
  styleUrls: ['./customer-addresses.component.scss']
})
export class CustomerAddressesComponent implements OnInit, OnChanges {
  @Input() customer_id: string;
  @Input() customer_name: string;
  addresses: Array<CustomerAddress> = [];
  loading = false;

  constructor(
    private customerService: CustomerService,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private locationCompact: LocationCompactPipe
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getCustomerAddresses();
  }

  async getCustomerAddresses() {
    this.loading = true;
    try {
      this.addresses = await this.customerService.getCustomerAddresses(this.customer_id);
    } catch(e) {
      this.addresses = [];
      debug.error('ERROR in getting customer addresses', e);
    }
    this.loading = false;
  }

  createNewAddress(customer_id) {
    let modal = this.modalController.create({
      component: CustomerAddressModalComponent,
      showBackdrop: true,
      componentProps: {
        customer_id,
        title: 'create'
      }
    });
    modal.show().then();
    modal.onDismiss().then(_ => {
      this.getCustomerAddresses();
    });
  }

  editCustomerAddress(address) {
    let modal = this.modalController.create({
      component: CustomerAddressModalComponent,
      showBackdrop: true,
      componentProps: {
        address: address,
        title: 'edit'
      }
    });
    modal.show().then();
    modal.onDismiss().then(_ => {
      this.getCustomerAddresses();
    });
  }

  delCustomerAddress(address: CustomerAddress) {
    let modal = this.modalController.create({
      component: DeleteCustomerModalComponent,
      showBackdrop: true,
      componentProps: {
        address: address,
      }
    });
    modal.show().then();
    modal.onDismiss().then(async (isDelete) => {
      if(isDelete) {
        try {
          await this.customerService.deleteCustomerAddress(address.id);
          toastr.success('Xoá địa chỉ khách hàng thành công.');
          this.getCustomerAddresses();
        } catch(e) {
          debug.error('ERROR in deleting customer address', e);
          toastr.error('Xoá địa chỉ khách hàng không thành công', e.code ? (e.message || e.msg) : '');
        }
      }
    });
  }

}
