import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseComponent, AuthenticateStore } from '@etop/core';
import { FilterOperator, Filters } from '@etop/models';
import { RelationshipService } from '@etop/state/relationship';
import { HotlineService } from '@etop/state/shop/hotline';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { ExtensionService } from '@etop/state/shop/extension';
import { MobileAction, MobileHeader } from '../../components/mobile-header/mobile-header.component';
import { AddStaffModalComponent } from './components/add-staff-modal/add-staff-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AuthorizationApi } from '@etop/api';
import { InvitationStore } from '@etop/state/invitation/invitation.store';

@Component({
  selector: 'webcall-staff-management',
  template: `
      <webcall-mobile-header [header]='mobileHeader'></webcall-mobile-header>
      <webcall-desktop-staff-management class='hide-768'></webcall-desktop-staff-management>
      <webcall-mobile-staff-management class='show-768'></webcall-mobile-staff-management>`,
  styleUrls: ['./staff-management.component.scss']
})
export class StaffManagementComponent extends BaseComponent implements OnInit {
  private modal: any;
  mobileHeader : MobileHeader =  {
    name: 'Nhân viên',
    actions: [
      {
        name: 'Thêm nhân viên',
        handleAction: () => this.addStaff(),
        permissions: ['relationship/invitation:create'],
      },
    ]
  };
  constructor(
    private invitationService: InvitationService,
    private relationshipService: RelationshipService,
    private hotlineService: HotlineService,
    private extensionService: ExtensionService,
    private cdr: ChangeDetectorRef,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private authorizationApi: AuthorizationApi,
    private invitationStore: InvitationStore,
  ) {
    super();
  }

  async ngOnInit() {
    await this.relationshipService.getRelationships();
    await this.hotlineService.getHotlines();
    await this.extensionService.getExtensions();
    await this.getShopInvitations();
    this.cdr.detectChanges();
  }

  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const filters : Filters = [
        {
          name: "status",
          op: FilterOperator.eq,
          value: "Z"
        },
        {
          name: "expires_at",
          op: FilterOperator.gt,
          value: dateString
        }
      ]
      this.invitationService.setFilters(filters);
      this.invitationService.getShopInvitations();
    } catch(e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }

  addStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show();
    this.modal.onDismiss().then(invitation => {
      this.modal = null;
      if (invitation) {
        invitation = this.authorizationApi.invitationMap(invitation);
        this.invitationStore.add(invitation);
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }
}
