import { Component, OnInit } from '@angular/core';
import { AuthorizationApi } from '@etop/api';
import { BaseComponent } from '@etop/core';
import { AppService } from '@etop/web';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { takeUntil } from 'rxjs/operators';
import { AddStaffModalComponent } from '../components/add-staff-modal/add-staff-modal.component';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { HotlineService } from '@etop/state/shop/hotline';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { InvitationQuery } from '@etop/state/invitation/invitation.query';
import { InvitationStore } from '@etop/state/invitation/invitation.store';
import {ExtensionService} from "@etop/state/shop/extension";
import {AuthorizationQuery} from "@etop/state/authorization";

@Component({
  selector: 'webcall-desktop-staff-management',
  templateUrl: './desktop-staff-management.component.html',
  styleUrls: ['./desktop-staff-management.component.scss']
})
export class DesktopStaffManagementComponent extends BaseComponent implements OnInit {
  relationships$ = this.relationshipQuery.selectAll();
  shopInvitations$ = this.invitationQuery.selectAll();
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');
  private modal: any;
  constructor(
    private authorizationApi: AuthorizationApi,
    private authorizationQuery: AuthorizationQuery,
    private modalController: ModalController,
    private appService: AppService,
    private extensionService: ExtensionService,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private hotlineService: HotlineService,
    private invitationService: InvitationService,
    private invitationQuery: InvitationQuery,
    private invitationStore: InvitationStore,
  ) {
    super();
  }

  async ngOnInit() {
    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(cancelled => {
        if (cancelled) { this.invitationService.getShopInvitations(); }
      });

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe(deleted => {
        if (deleted) { this.relationshipService.getRelationships(); }
      });
    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(updated => {
        if (updated) { this.relationshipService.getRelationships(); }
      });
  }

  addStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show();
    this.modal.onDismiss().then(invitation => {
      this.modal = null;
      if (invitation) {
        invitation = this.authorizationApi.invitationMap(invitation);
        this.invitationStore.add(invitation);
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }
}
