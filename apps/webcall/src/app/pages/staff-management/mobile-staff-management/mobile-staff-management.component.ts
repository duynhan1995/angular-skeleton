import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AuthorizationApi } from '@etop/api';
import { ModalController } from '../../../../../../core/src/components/modal-controller/modal-controller.service';
import { AppService } from '@etop/web';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { FilterOperator, Invitation, Relationship } from '@etop/models';
import { AddStaffModalComponent } from '../components/add-staff-modal/add-staff-modal.component';
import { UpdateStaffModalComponent } from '../components/update-staff-modal/update-staff-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { InvitationQuery } from '@etop/state/invitation/invitation.query';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { InvitationStore } from '@etop/state/invitation/invitation.store';
import { ExtensionQuery, ExtensionStore } from '@etop/state/shop/extension';
import { CreateExtensionModalComponent } from '../components/create-hotline-modal/create-hotline-modal.component';
import { HotlineQuery } from '@etop/state/shop/hotline';
import {AuthorizationQuery, AuthorizationService} from "@etop/state/authorization";

enum segments {
  list = 'list',
  response= 'response'
}

@Component({
  selector: 'webcall-mobile-staff-management',
  templateUrl: './mobile-staff-management.component.html',
  styleUrls: ['./mobile-staff-management.component.scss']
})
export class MobileStaffManagementComponent extends BaseComponent implements OnInit {
  segments = ["list", "response"];
  segment = '';

  shopInvitations$ = this.invitationQuery.selectAll();
  relationships$ = this.relationshipQuery.selectAll();
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');
  private modal: any;

  constructor(
    private authorizationApi: AuthorizationApi,
    private authorizationQuery: AuthorizationQuery,
    private authorizationService: AuthorizationService,
    private modalController: ModalController,
    private appService: AppService,
    private dialog: DialogControllerService,
    private invitationQuery: InvitationQuery,
    private invitationService: InvitationService,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private invitationStore: InvitationStore,
    private cdr: ChangeDetectorRef,
    private extensionQuery: ExtensionQuery,
    private hotlineQuery: HotlineQuery,
    private extensionStore: ExtensionStore
  ) {
    super();
  }

  async ngOnInit() {
    this.relationships$.subscribe( _ => this.cdr.detectChanges());
    this.segment = segments.list;
    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(cancelled => {
        if (cancelled) { this.invitationService.getShopInvitations(); }
      });

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async (deleted) => {
        if (deleted) { this.relationshipService.getRelationships(); }
      });

    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(updated => {
        if (updated) { this.relationshipService.getRelationships(); }
      });
  }

  addStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show();
    this.modal.onDismiss().then(invitation => {
      this.modal = null;
      if (invitation) {
        invitation = this.authorizationService.invitationMap(invitation);
        this.invitationStore.add(invitation);
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  changeSegment(segment) {
    if(this.segment == segment) {
      return;
    }
    this.segment = segment;
  }

  updateStaff(staff: Relationship) {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: UpdateStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        staff: {
          ...staff,
          p_data: {
            ...staff,
          }
        }
      }
    });
    this.modal.show();
    this.modal.onDismiss().then(updated => {
      this.modal = null;
      if (updated) {
        this.authorizationService.updateRelationship();
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async removeStaff(staff: Relationship) {
    try {
      await this.authorizationApi.removeUserFromAccount(staff.user_id);
      this.authorizationService.removeRelationship();
      toastr.success('Xoá nhân viên thành công.')
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Staff', e);
    }
  }

  confirmRemoveStaff(staff:Relationship) {
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá nhân viên`,
      body: `
        <div>Bạn thực sự muốn xóa nhân viên "<strong>${staff?.full_name}</strong>"?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmCss: 'btn-danger text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removeStaff(staff);
        modal.close().then();
      }
    });
    modal.show().then();
  }

  async deleteInvitation(invitation: Invitation) {
    try {
      await this.authorizationService.deleteInvitation(invitation.token);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in deleting Invitation', e);
    }
  }

  isOwner(relationship) {
    return relationship.roles.indexOf('owner') != -1;
  }

  // Check employee have extension or not.
  hasUserExtension(staff): boolean {
    const extensions = this.extensionQuery.getAll();
    let currentExtension;
    if(extensions?.length){
      currentExtension = extensions.find(ex => ex.user_id == staff.user_id);
    }
    return !!currentExtension?.extension_number;
  }

  getExtensionNumber(staff) {
    const extensions = this.extensionQuery.getAll();
    let extension;
    if(extensions?.length){
      extension = extensions.find(ex => ex.user_id == staff.user_id);
    }
    return extension?.extension_number;
  }
  async openCreateExtension(staff) {
    const hotlines = this.hotlineQuery.getAll();
    this.modal = this.modalController.create({
      component: CreateExtensionModalComponent,
      componentProps: {
        hotlines: hotlines,
        staff: staff
      },
      showBackdrop: true,
      cssClass: 'modal-md'
    })
    this.modal.show();
    this.modal.onDismiss().then(extension => {
      this.extensionStore.add(extension);
      // this.currentExtension = extension;
    })
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }
}
