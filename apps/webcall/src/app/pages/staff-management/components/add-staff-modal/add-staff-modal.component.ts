import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Invitation } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';

@Component({
  selector: 'webcall-add-staff-modal',
  templateUrl: './add-staff-modal.component.html',
  styleUrls: ['./add-staff-modal.component.scss']
})
export class AddStaffModalComponent implements OnInit {
  invitation = new Invitation({});
  loading = false;
  roleList = [
    {name: 'Chăm sóc khách hàng', value: 'telecom_customerservice'},
    {name: 'Chăm sóc khách hàng - Bán hàng', value: 'salesman,telecom_customerservice'},
  ];

  constructor(
    private modalAction: ModalAction,
    private authorizationAPI: AuthorizationApi
  ) { }

  ngOnInit() {
  }

  async createInvitation() {
    this.loading = true;
    try {
      let { roles, phone, full_name} = this.invitation.p_data;
      if (!phone) {
        this.loading = false;
        return toastr.error('Chưa nhập số điện thoại!');
      }
      if (!full_name) {
        this.loading = false;
        return toastr.error('Chưa nhập tên nhân viên!');
      }
      if (!roles || !roles.length) {
        this.loading = false;
        return toastr.error('Chưa chọn vai trò!');
      }
      roles = roles.split(',');
      const body  = {
        phone, full_name, roles
      };
      const res = await this.authorizationAPI.createInvitation(body);
      toastr.success('Thêm nhân viên thành công');
      this.modalAction.dismiss(res);
    } catch(e) {
      toastr.error(e.message, 'Thêm nhân viên không thành công. Vui lòng bấm F5 để load lại trang và thử lại!');
      debug.error('ERROR in creating Invitation', e);
    }
    this.loading = false;
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  closeModal() {
    this.modalAction.close(false);
  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }

}
