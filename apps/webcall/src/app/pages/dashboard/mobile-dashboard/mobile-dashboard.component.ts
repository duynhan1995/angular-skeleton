import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'webcall-mobile-dashboard',
  templateUrl: './mobile-dashboard.component.html',
  styleUrls: ['./mobile-dashboard.component.scss']
})
export class MobileDashboardComponent implements OnInit {
  @Input() dashboard: any;
  @Input() loading = true;
  @Output() filter = new EventEmitter();
  @Input() telecomBalance: number;
  selected;

  constructor() { }

  ngOnInit(): void {
 
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
      this.filter.emit(event)
    }
  }
}
