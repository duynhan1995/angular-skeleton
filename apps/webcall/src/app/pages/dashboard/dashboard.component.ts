import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RelationshipQuery } from '@etop/state/relationship/relationship.query';
import { RelationshipService } from '@etop/state/relationship/relationship.service';
import { DashboardService } from 'apps/webcall/src/services/dashboard.service';
import { ShopService } from '@etop/features';
import { PaycardTurtorialModalComponent } from './components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import { ModalController } from '../../../../../core/src/components/modal-controller/modal-controller.service';
import { MobileHeader } from '../../components/mobile-header/mobile-header.component';
import * as moment from 'moment';
import {ExtensionApi} from "@etop/api/shop/extension.api";

@Component({
  selector: 'webcall-dashboard-pos',
  template: `
    <webcall-mobile-header [header]='mobileHeader'></webcall-mobile-header>
    <webcall-desktop-dashboard class='hide-768' [dashboard]='dashboard' [loading]='loading' (filter)="getDashboard($event)" [telecomBalance]='telecomBalance'>
    </webcall-desktop-dashboard>
    <webcall-mobile-dashboard class='show-768' [dashboard]='dashboard' [loading]='loading' (filter)="getDashboard($event)" [telecomBalance]='telecomBalance'>
    </webcall-mobile-dashboard>`,
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  private modal: any;
  mobileHeader : MobileHeader = {
    name: 'Thống kê',
    actions: [
      {
        name: 'Nạp tiền',
        handleAction: () => this.openPayCardTurtorialModal(),
      },
    ]
  };
  dashboard: any;
  loading = true;
  extensions: any = [];
  telecomBalance;
  constructor(
    private dashboardService: DashboardService,
    private extensionApi: ExtensionApi,
    private relationshopService: RelationshipService,
    private relationshopQuery: RelationshipQuery,
    private changeDef: ChangeDetectorRef,
    private shopService: ShopService,
    private modalController: ModalController
  ) {}

  async ngOnInit() {
    this.loading = true;
    let relationships: any = this.relationshopQuery.getAll();
    if (!relationships?.length) {
      relationships = await this.relationshopService.getRelationships();
    }
    let userCalcBalance = await this.shopService.calcShopBalance("telecom");
    this.telecomBalance = userCalcBalance.telecom_balance;
    this.loading = false;
    const ranges = {
      startDate: moment().startOf('isoWeek'),
      endDate: moment()
    }
    this.getDashboard(ranges);
  }

  async getExtensions() {
    try {
      let extensions = await this.extensionApi.getExtensions();
      extensions = extensions.map(extension => {
        return {
          ...extension,
          user_name: this.relationshopQuery.getRelationshipNameById(extension.user_id)
        }
      })
      return extensions
    } catch(e) {
      debug.log('ERROR in getExtensions', e)
    }
  }

  async getDashboard(ranges) {
    try {

      this.dashboard = await this.dashboardService.summaryEtelecom({
        date_from: ranges.startDate.format('yy-MM-DD'),
        date_to: ranges.endDate.add(1, 'days').format('yy-MM-DD')
      })
      ranges.endDate.subtract(1, 'days').format('yy-MM-DD')
      if (this.extensions) {
        this.extensions = await this.getExtensions();
      }
      this.dashboard.staff = this.dashboard.staff.map(d => {
        return {
          ...d,
          full_name: this.extensions?.find(e => e.id == d.extension_id).user_name || 'Không xác định'
        }
      })
      this.changeDef.detectChanges();
    } catch(e) {
      debug.log('ERROR in getDashboard', e)
    }
  }

  openPayCardTurtorialModal() {
    if(this.modal) {
      return;
    }

    this.modal = this.modalController.create({
      component: PaycardTurtorialModalComponent,
    });

    this.modal.show().then();
    this.modal.onDismiss().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

}
