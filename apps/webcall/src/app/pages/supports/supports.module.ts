import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FormsModule} from '@angular/forms';
import {EtopPipesModule} from '@etop/shared';
import {SupportsComponent} from './supports.component';
import {MobileHeaderModule} from '../../components/mobile-header/mobile-header.module';

const routes: Routes = [
  {
    path: '',
    component: SupportsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    MobileHeaderModule,
  ],
  exports: [],
  declarations: [
    SupportsComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class SupportsModule {
}
