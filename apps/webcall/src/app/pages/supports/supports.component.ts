import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { MobileHeader } from '../../components/mobile-header/mobile-header.component';

@Component({
  selector: 'webcall-supports',
  templateUrl: './supports.component.html',
  styleUrls: ['./supports.component.scss']
})
export class SupportsComponent implements OnInit {
  mobileHeader : MobileHeader = {
    name: 'Thiết lập',
  };

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
  ) { }

  ngOnInit(): void {
  }
  async goBack() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    await this.router.navigateByUrl(`/s/${slug}/settings/user`);
  }
}
