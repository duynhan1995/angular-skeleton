import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactService } from '@etop/state/shop/contact';
import { ContactsListComponent } from './component/contacts-list/contacts-list.component';
import { MobileHeader } from '../../components/mobile-header/mobile-header.component';

@Component({
  selector: 'webcall-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  @ViewChild('contactsList', {static: true}) contactsList: ContactsListComponent;
  mobileHeader : MobileHeader = {
    name: 'Danh bạ',
    actions: [
      {
        name: 'Thêm liên hệ',
        handleAction: () => this.contactsList.createNewContacts(),
      },
    ]
  }
  constructor(
    private contactService: ContactService,
  ) { }

  async ngOnInit(): Promise<void> {
    await this.contactService.getContacts();
  }

}
