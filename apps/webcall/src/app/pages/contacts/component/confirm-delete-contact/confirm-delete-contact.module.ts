import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';
import { ConfirmDeleteContactComponent } from './confirm-delete-contact.component';

@NgModule({
  declarations: [
    ConfirmDeleteContactComponent
  ],
  entryComponents: [
    ConfirmDeleteContactComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    EtopMaterialModule
  ]
})
export class ConfirmDeleteContactModule { }
