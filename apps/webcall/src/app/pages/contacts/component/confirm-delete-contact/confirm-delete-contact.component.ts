import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'webcall-confirm-delete-contact',
  templateUrl: './confirm-delete-contact.component.html',
  styleUrls: ['./confirm-delete-contact.component.scss']
})
export class ConfirmDeleteContactComponent implements OnInit {
  @Input() name: string;
  @Input() deleteFn: () => Promise<any>;
  loading = false;

  constructor(
    private modalAction: ModalAction
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async confirm() {
    this.loading = true;
    await this.deleteFn();
    this.modalAction.dismiss(null);
    this.loading = false;
  }
}
