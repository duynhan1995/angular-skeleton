import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '@etop/models/Contact';
import { ContactQuery, ContactService } from '@etop/state/shop/contact';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core/base';
import { Paging } from '@etop/shared';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ConfirmDeleteContactComponent } from '../confirm-delete-contact/confirm-delete-contact.component';

@Component({
  selector: 'webcall-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('contactsTable', { static: true }) contactsTable: EtopTableComponent;
  @ViewChild('contactsSlider', { static: true }) contactsSlider: SideSliderComponent;

  isFirstRequestNewPage = true;
  contacts$ = this.contactQuery.selectAll();
  contacts = [];
  selectedContacts:Contact = null;
  // filters: Filters = [];
  creatingNewContacts: boolean = false;

  page: number;
  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  private modal: any;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private dropdownController: DropdownActionsControllerService,
    private router: Router,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private modalController: ModalController,
  ) { super()}

  private _onSelectModeChanged(value) {
    this.contactsTable.toggleLiteMode(value);
    this.contactsSlider.toggleLiteMode(value);
  }
  ngOnInit() {
    this.contacts$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      this.reloadContactList();
    });


    this.page = this.contactsTable.currentPage;
    this.perpage = this.contactsTable.perpage;
    this.headerController.setActions([
      {
        title: 'Thêm liên hệ',
        cssClass: 'btn btn-primary',
        onClick: () => this.createNewContacts(),
      }
    ]);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  reloadContactList() {
    const { perpage, page } = this;
    this.contacts = this.contactQuery.getAll().slice((page - 1) * perpage, page * perpage);
  }

  async getContacts(page?:number , perpage?: number) {
    try {
      this.selectMode = false;
      this.contactsTable.toggleNextDisabled(false);

      let _contacts = [];

      const paging : Paging ={
        offset: ((page || this.page) - 1) * perpage,
        limit: perpage || this.perpage,
      }
      const filter = {
        filter: null,
        paging: paging
      }
      await this.contactService.setFilter(filter)
      await this.contactService.getContacts();
      _contacts = this.contactQuery.getAll()

      if (page > 1 && _contacts.length == 0) {
        this.contactsTable.toggleNextDisabled(true);
        this.contactsTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả liên hệ.');
        return;
      }
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list contacts', e);
    }
  }

  async loadPage({ page, perpage }) {
    if(this.isFirstRequestNewPage) {
      return this.isFirstRequestNewPage = false;
    }
    this.contactsTable.loading = true;
    this.changeDetector.detectChanges();
    await this.getContacts(page, perpage);
    this.contactsTable.loading = false;
    this.changeDetector.detectChanges();
  }

  detail(event, contacts: Contact) {
    this.creatingNewContacts = false;
    this.contactService.setActive(contacts.id);
    this.selectedContacts = contacts;
    this._checkSelectMode();
  }

  private async _checkSelectMode() {
    this.selectedContacts = this.contactQuery.getActive();
    this.setupDropdownActions();
    this.selectMode = !!this.selectedContacts || this.creatingNewContacts;
    if (this.queryParams.code && this.selectedContacts) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/contacts`);
      this.contactsTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.creatingNewContacts = false;
    this.contactService.setActive(null);
    this._checkSelectMode();
  }

  createNewContacts() {
    this.contactService.setActive(null);
    this.creatingNewContacts = true;
    this._checkSelectMode();
  }

  async deleteContacts() {
    try {
      const id = this.contactQuery.getActiveId();
      await this.contactService.deleteContact(id);
      toastr.success('Xóa liên hệ thành công!');
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/contacts`);
      this.contactsTable.resetPagination()
    } catch (e) {
      debug.error('ERROR in deleting contacts', e);
      toastr.error(e, 'Xóa liên hệ không thành công');
    }
  }

  private setupDropdownActions() {
    if (!this.selectedContacts) {
      this.dropdownController.clearActions();
      return;
    }
    this.dropdownController.setActions([
      {
        onClick: () => this.confirmDeleteContact(),
        title: `Xóa liên hệ`,
        cssClass: 'text-danger',
        roles: ['owner', 'telecom_customerservice']
      }
    ]);
  }

  confirmDeleteContact() {
    const name = this.selectedContacts.full_name;
    this.modal = this.modalController.create({
      component: ConfirmDeleteContactComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        name,
        deleteFn: async () => this.deleteContacts()
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async () => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  get sliderTitle() {
    if (this.creatingNewContacts) {
      return 'Thêm liên hệ mới';
    }
    return 'Chi tiết liên hệ';
  }

  get emptyTitle() {
    return 'Chưa có liên hệ';
  }

  get showPaging() {
    return !this.contactsTable.liteMode && !this.contactsTable.loading;
  }

  get showDetailContacts() {
    return !!this.selectedContacts;
  }

  get sliderWidth() {
    return this.util.isMobile ? "100%" : "50%";
  }
}
