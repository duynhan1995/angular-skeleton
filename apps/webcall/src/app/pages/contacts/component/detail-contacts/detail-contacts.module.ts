import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';
import { DetailContactsComponent } from './detail-contacts.component';
import { AuthenticateModule } from '@etop/core';


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule, AuthenticateModule],
  exports: [DetailContactsComponent],
  entryComponents: [],
  declarations: [DetailContactsComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DetailContactsFormModule {}