import { Component, OnInit, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { Contact } from '@etop/models/Contact';
import { ContactQuery, ContactService } from '@etop/state/shop/contact';

@Component({
  selector: 'webcall-detail-contacts',
  templateUrl: './detail-contacts.component.html',
  styleUrls: ['./detail-contacts.component.scss']
})
export class DetailContactsComponent extends BaseComponent implements OnInit {
  @Input() contact = new Contact({}) ;

  activeContact$ = this.contactQuery.selectActive();
  full_name: string;
  phone: string;

  constructor(
    private contactService: ContactService,
    private contactQuery: ContactQuery,
  ) {
    super();
  }

  ngOnInit() {
    this.activeContact$.subscribe(a => {
      this.full_name = a?.full_name;
      this.phone = a?.phone;
    })
    this.full_name = this.contact?.full_name;
    this.phone = this.contact?.phone;
  }

  ngOnChanges(changes: SimpleChanges): void {}

  async updateContact() {
    try {
        const body = {
          ...this.contact,
          full_name: this.full_name,
          phone: this.phone
        };
        if (!body.full_name) {
          return toastr.error('Chưa nhập tên khách hàng!');
        }
        if (!body.phone) {
          return toastr.error('Chưa nhập số điện thoại khách hàng!');
        }
        await this.contactService.updateContact(body);
        this.contactService.getContacts();
      toastr.success('Cập nhật liên hệ thành công!');
    } catch (e) {
      toastr.error('Cập nhật liên hệ thất bại', e.message);
    }
  }

}
