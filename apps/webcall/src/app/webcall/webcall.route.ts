import {Routes} from '@angular/router';

export const eTelecomRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'staffs',
    loadChildren: () => import('../pages/staff-management/staff-management.module').then(m => m.StaffManagementModule)
  },
  {
    path: 'supports',
    loadChildren: () => import('../pages/supports/supports.module').then(m => m.SupportsModule)
  },
  {
    path: 'call-log',
    loadChildren: () => import('../pages/call-log/call-log.module').then(m => m.CallLogModule)
  },
  {
    path: 'contacts',
    loadChildren: () => import('../pages/contacts/contacts.module').then(m => m.ContactsModule)
  },
  {
    path: 'fulfillments',
    loadChildren: () => import('../pages/fulfillments/fulfillments.module').then(m => m.FulfillmentsModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('../pages/customers/customers.module').then(m => m.CustomersModule)
  },
  {
    path: '**',
    redirectTo: 'settings'
  }
];
