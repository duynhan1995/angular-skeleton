import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WebcallRoutingModule} from './webcall-routing.module';
import {WebcallComponent} from './webcall.component';
import {CoreModule} from 'apps/core/src/core.module';
import {WebcallGuard} from './webcall.guard';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FulfillmentService} from "../../services/fulfillment.service";
import {FromAddressModalModule} from "@etop/shared/components/from-address-modal/from-address-modal.module";
import {CustomerService} from '../../services/customer.service';

const pages = [];

@NgModule({
  declarations: [WebcallComponent, ...pages],
  providers: [
    WebcallGuard,
    FulfillmentService,
    CustomerService,
  ],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    WebcallRoutingModule,
    FromAddressModalModule
  ]
})
export class WebcallModule {
}
