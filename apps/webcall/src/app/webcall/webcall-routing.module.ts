import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WebcallComponent} from './webcall.component';
import {WebcallGuard} from './webcall.guard';
import {eTelecomRoutes} from './webcall.route';

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [WebcallGuard],
    resolve: {
      account: WebcallGuard
    },
    component: WebcallComponent,
    children: eTelecomRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebcallRoutingModule {
}
