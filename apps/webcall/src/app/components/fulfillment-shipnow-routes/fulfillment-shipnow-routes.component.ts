import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from '@etop/models';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';

@Component({
  selector: 'webcall-fulfillment-shipnow-routes',
  templateUrl: './fulfillment-shipnow-routes.component.html',
  styleUrls: ['./fulfillment-shipnow-routes.component.scss']
})
export class FulfillmentShipnowRoutesComponent implements OnInit {
  @Input() ffm = new Fulfillment({});

  constructor(
    private orderStore: OrderStoreService
  ) { }

  ngOnInit() {
  }

  viewDetailOrder(order_code) {
    this.orderStore.navigate('so', order_code);
  }

  trackingLink() {
    const link = Fulfillment.trackingLink(this.ffm);
    if (!link) { return; }
    window.open(link);
  }

}
