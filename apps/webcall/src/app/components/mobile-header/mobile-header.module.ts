import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileHeaderComponent } from './mobile-header.component';
import { EtopPipesModule, MaterialModule } from '@etop/shared';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthenticateModule } from '@etop/core';

@NgModule({
  declarations: [MobileHeaderComponent],
  imports: [
    CommonModule,
    EtopPipesModule,
    MaterialModule,
    FormsModule,
    MaterialModule,
    RouterModule,
    AuthenticateModule,
  ],
  exports: [
    MobileHeaderComponent
  ],
})
export class MobileHeaderModule { }
