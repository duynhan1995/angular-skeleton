import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
export class MobileAction {
  name: string;
  isHidden?: boolean;
  permissions?: Array<string>;
  handleAction: () => void;
}
export class MobileHeader {
  name: string;
  actions?: Array<MobileAction>;
}
@Component({
  selector: 'webcall-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.scss']
})
export class MobileHeaderComponent implements OnInit {
  @Input() header: MobileHeader;
  @Input() canBack = false;
  @Output() toGoBack = new EventEmitter();
  constructor(
  ) {}

  ngOnInit(): void {
  }

  goBack() {
    this.toGoBack.emit();
  }
}
