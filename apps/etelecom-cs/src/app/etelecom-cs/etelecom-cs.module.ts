import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EtelecomCsRoutingModule} from './etelecom-cs-routing.module';
import {EtelecomCsComponent} from './etelecom-cs.component';
import {CoreModule} from 'apps/core/src/core.module';
import {EtelecomCsGuard} from './etelecom-cs.guard';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FulfillmentService} from "../../services/fulfillment.service";
import {ExtensionService} from '@etop/state/shop/extension';
import {FromAddressModalModule} from "@etop/shared/components/from-address-modal/from-address-modal.module";
import {CustomerService} from '../../services/customer.service';


const pages = [];

@NgModule({
  declarations: [EtelecomCsComponent, ...pages],
  providers: [
    EtelecomCsGuard,
    FulfillmentService,
    ExtensionService,
    CustomerService,
  ],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    EtelecomCsRoutingModule,
    FromAddressModalModule
  ]
})
export class EtelecomCsModule {
}
