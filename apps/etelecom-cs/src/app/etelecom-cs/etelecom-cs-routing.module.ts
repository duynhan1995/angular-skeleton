import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EtelecomCsComponent} from './etelecom-cs.component';
import {EtelecomCsGuard} from './etelecom-cs.guard';
import {eTelecomCsRoutes} from './etelecom-cs.route';

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [EtelecomCsGuard],
    resolve: {
      account: EtelecomCsGuard
    },
    component: EtelecomCsComponent,
    children: eTelecomCsRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtelecomCsRoutingModule {
}
