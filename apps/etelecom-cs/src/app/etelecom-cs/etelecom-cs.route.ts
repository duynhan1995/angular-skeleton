import {Routes} from '@angular/router';

export const eTelecomCsRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'staffs',
    loadChildren: () => import('../pages/staff-management/staff-management.module').then(m => m.StaffManagementModule)
  },
  {
    path: 'supports',
    loadChildren: () => import('../pages/tickets-system/tickets-system.module').then(m => m.TicketsSystemModule)
  },
  {
    path: 'tickets',
    loadChildren: () => import('../pages/tickets-internal/tickets-internal.module').then(m => m.TicketsInternalModule)
  },
  {
    path: 'call-log',
    loadChildren: () => import('../pages/call-log/call-log.module').then(m => m.CallLogModule)
  },
  {
    path: 'contacts',
    loadChildren: () => import('../pages/contacts/contacts.module').then(m => m.ContactsModule)
  },
  {
    path: 'invoice-transaction',
    loadChildren: () => import('../pages/invoice-transaction/invoice-transaction.module').then(m => m.InvoiceTransactionModule)
  },
  {
    path: '**',
    redirectTo: 'settings'
  }
];
