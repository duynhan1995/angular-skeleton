import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CommonLayout} from 'apps/core/src/app/CommonLayout';
import {AuthenticateStore} from '@etop/core';
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {NavigationData} from "apps/core/src/components/header/header.interface";
import {MenuItem} from 'apps/core/src/components/menu-item/menu-item.interface';
import {ConnectionService} from "@etop/features/connection/connection.service";
import {TenantService} from "@etop/state/shop/tenant/tenant.service";
import {BankService} from "@etop/state/bank";
import {UtilService} from 'apps/core/src/services/util.service';
import {HotlineService} from "@etop/state/shop/hotline";
import {JiraService} from "@etop/state/shop/jira";
import {StorageService} from "../../../../core/src/services/storage.service";

@Component({
  selector: 'etelecom-cs-etelecom-cs',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./etelecom-cs.component.scss']
})
export class EtelecomCsComponent extends CommonLayout implements OnInit {
  sideMenus: MenuItem[] = [
    {
      name: 'Thống kê',
      route: ['dashboard'],
      matIcon: 'insert_chart',
      matIconOutlined: true,
      roles: ['owner','staff_management'],
      isDivider: true
    },
    {
      name: 'Phiếu yêu cầu',
      route: ['tickets'],
      matIcon: 'confirmation_number',
      matIconOutlined: true,
    },
    {
      name: 'Lịch sử cuộc gọi',
      route: ['call-log'],
      matIcon: 'list',
      matIconOutlined: true,
      roles: ['owner', 'telecom_customerservice']
    },
    {
      name: 'Danh bạ',
      route: ['contacts'],
      matIcon: 'contact_phone',
      matIconOutlined: true,
      roles: ['owner', 'telecom_customerservice'],
      isDivider: true,
      onlyDesktop: true
    },
    {
      name: 'Nhân viên',
      route: ['staffs'],
      matIcon: 'account_circle',
      matIconOutlined: true,
      roles: ['owner','staff_management'],
    },
    {
      name: 'Hóa đơn và Giao dịch',
      route: ['invoice-transaction'],
      matIcon: 'receipt',
      matIconOutlined: true,
      display_submenus: true,
      submenus: [
        {name: 'Hóa đơn', route: ['invoice-transaction','invoice'], hidden: false},
        {name: 'Giao dịch', route: ['invoice-transaction','transaction'], hidden: false},
      ],
      roles: ['owner','staff_management'],
      onlyDesktop: true
    },

    {
      name: 'Thiết lập',
      route: ['settings'],
      matIcon: 'settings',
      matIconOutlined: true,
      isDivider: true,
      submenus: [
        {name: 'Thông tin công ty', route: ['settings','shop'], hidden: false},
        {name: 'Loại ticket', route: ['settings','ticket-labels'], hidden: false},
        {name: 'Hotline', route: ['settings','hotlines'], hidden: false},
        {name: 'Thông tin tài khoản', route: ['settings','user'], hidden: false},
        {name: 'Danh sách công ty', route: ['settings','accounts'], hidden: false},
      ],
      display_submenus: true
    },
    {
      name: 'Yêu cầu hỗ trợ',
      route: ['supports'],
      matIcon: 'support',
      matIconOutlined: true,
      onlyDesktop: true
    },
  ];

  showVerifyWarning = false;
  showForgotPassword = false;
  isSingle = false;
  showTenantWarning = false;
  showNewSidebar = true;
  config = {account_type_display:'công ty'};

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private headerController: HeaderControllerService,
    private connectionService: ConnectionService,
    private bankService: BankService,
    private util: UtilService,
    private hotlineService: HotlineService,
    private storageService: StorageService,
    private tenantService: TenantService,
    private jiraService: JiraService,
  ) {
    super(auth);
  }

  ngOnInit() {
    this.bankService.initBanks().then();
    this.adminLoginMode = this.storageService.get('adminLogin');
    this.connectionService.getValidConnections(true).then();
    this.hotlineService.getHotlines().then();
    this.jiraService.initCustomFields().then();
    this.headerController.onNavigate.subscribe(({ target, data }) => {
      this.onNavigate(target, data);
    });

    this.tenantService.getTenant().then(tenant => {
      if (!tenant && this.auth.snapshot.account?.user_account?.permission?.roles.indexOf('owner') > -1) {
        this.showTenantWarning = true;
      }
    });
  }

  onNavigate(target, data: NavigationData) {
    let slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    if (data.payload) {
      let accIndex = this.auth.snapshot.accounts.findIndex(a => a.id == data.payload.id);
      accIndex = accIndex < 0 ? 0 : accIndex;
      const acc = this.auth.snapshot.accounts[accIndex];
      slug = acc && acc.url_slug || accIndex;
    }

    switch (data.target) {
      case 'account':
        window.open(`/s/${slug}/settings/user`, '_blank');
        break;
      case 'setting':
        this.router.navigateByUrl(`/s/${slug}/settings/user`).then();
        break;
      default:
        return;
    }
  }

}
