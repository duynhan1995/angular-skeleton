import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../../../../core/src/services/cms.service';
import { MatDialogBaseComponent } from '@etop/web';
import { MatDialogRef } from '@angular/material/dialog';
import * as moment from "moment";
import {CallLogApi} from "@etop/api";

@Component({
  selector: 'etelecom-cs-export-call-log-modal',
  templateUrl: './export-call-log-modal.component.html',
  styleUrls: ['./export-call-log-modal.component.scss']
})
export class ExportCallLogModalComponent extends MatDialogBaseComponent implements OnInit {
  datePickerOpts = {
    editableDateField: false,
    showClearDateBtn: true,
    openSelectorOnInputClick: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '12px',
    inline: false
  };

  fromDate = null;
  toDate = null;
  exportLink = null;
  loading = false;
  delimiter = ',';
  excel_compatible_mode = true;
  display_progress = false;
  error_progress = false;
  progress_percent = 0;

  constructor(
    private dialogRef: MatDialogRef<ExportCallLogModalComponent>,
    private callLogApi: CallLogApi,
  ) {
    super()
  }

  ngOnInit(): void {
    this.fromDate = { jsdate: new Date(moment().subtract(15, 'days').format('YYYY/MM/DD')) };
    this.toDate = { jsdate: new Date() };
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onConfirmExportExcel() {
    this.loading = true;
    this.display_progress = true;
    this.error_progress = false;
    try {
      let to_date = new Date(this.toDate.jsdate.getTime());
      to_date.setDate(to_date.getDate() + 1);

      let queryData = {
        date_from: moment(this.fromDate.jsdate).format('YYYY-MM-DD'),
        date_to: moment(to_date).format('YYYY-MM-DD'),
        excel_compatible_mode: this.excel_compatible_mode,
        filters: [],
      };
      queryData['delimiter'] = this.delimiter;

      let progress = this.callLogApi.requestExportCallLogExcel(queryData).subscribe({
        next: (step: any) => {
          if (step.error) {
            this.progress_percent = step.progress_error / step.progress_max * 100;
            progress.unsubscribe();
          } else {
            this.progress_percent = step.progress_value / step.progress_max * 100;
          }
          if (step.download_url) {
            this.loading = false;
            this.display_progress = false;
            this.exportLink = step.download_url;
            this.progress_percent = 100;
          }
        },
        complete: () => {
          this.progress_percent = 100;
          this.loading = false;
          this.display_progress = false;
          progress.unsubscribe();
        },
        error: (err: any) => {
          this.loading = false;
          this.error_progress = true;
          toastr.error(err.message, 'Export lịch sử cuộc gọi thất bại!');
          progress.unsubscribe();
        }
      });
    } catch (e) {
      debug.error('error in export', e);
    }
    this.loading = false;
  }

}
