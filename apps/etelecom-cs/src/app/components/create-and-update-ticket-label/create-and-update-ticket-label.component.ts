import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TicketLabel } from '@etop/models';
import {TicketLabelService} from "@etop/state/shop/ticket-labels";
import {ShopTicketAPI} from "@etop/api";
import {UtilService} from "apps/core/src/services/util.service";

export interface DialogTicketLabelData {
  ticketLabel: TicketLabel;
}

@Component({
  selector: 'etelecom-cs-create-and-update-ticket',
  templateUrl: './create-and-update-ticket-label.component.html',
  styleUrls: ['./create-and-update-ticket-label.component.scss']
})

export class CreateAndUpdateTicketLabelComponent implements OnInit {
  loading = false;

  listColor = [
    {
      color: '#2ecc71',
      isChecked: true,
      name: 'green'
    },
    {
      color: '#3498db',
      isChecked: false,
      name: 'blue'
    },
    {
      color: '#9b59b6',
      isChecked: false,
      name: 'purple'
    },
    {
      color: '#f39c12',
      isChecked: false,
      name : 'orange'
    },
    {
      color: '#e74c3c',
      isChecked: false,
      name: 'red'
    },
    {
      color: '#95a5a6',
      isChecked: false,
      name: 'grey'
    }
  ];


  currentTicketLabel = new TicketLabel();

  constructor(
    public dialogRef: MatDialogRef<CreateAndUpdateTicketLabelComponent>,
    private ticketLabelService: TicketLabelService,
    private util: UtilService,
    @Inject(MAT_DIALOG_DATA) public data: DialogTicketLabelData,
  ) {
  }

  ngOnInit() {
    if (this.data?.ticketLabel) {
      this.listColor.map(color => {
        color.isChecked = false;
      });
      const index = this.listColor.findIndex(
        color => color.color === this.data.ticketLabel.color
      );
      this.listColor[index].isChecked = true;
      this.currentTicketLabel = JSON.parse(JSON.stringify(this.data.ticketLabel))
    }else{
      this.currentTicketLabel.color = this.listColor[0]?.color
    }
  }

  changeColor(index) {
    this.listColor.map(color => {
      color.isChecked = false;
    });
    this.listColor[index].isChecked = true;
    this.currentTicketLabel.color = this.listColor[index].color
  }


 async confirm() {
    this.loading = true;
    if (!this.currentTicketLabel.name) {
      this.loading = false;
      return toastr.error('Vui lòng nhập tên loại Ticket');
    }
    if (!this.currentTicketLabel.color) {
      this.loading = false;
      return toastr.error('Vui lòng chọn màu');
    }
    if (this.data?.ticketLabel) {
      try {
        await this.ticketLabelService.updateShopTicketLabel(this.currentTicketLabel)
        toastr.success('Chỉnh sửa loại Ticket thành công');
        this.dialogRef.close();
      }catch(e) {
        toastr.error(e.message,'Chỉnh sửa loại Ticket thất bại');
      }
    } else {
      try {
        const ticketLabel: ShopTicketAPI.CreateTicketLabelRequest = {
          name: this.currentTicketLabel.name,
          color: this.currentTicketLabel.color,
        };
        await this.ticketLabelService.createShopTicketLabel(ticketLabel);
        toastr.success('Thêm loại Ticket thành công');
        this.dialogRef.close();
      } catch(e) {
        console.log(e)
        toastr.error(e.message,'Thêm loại Ticket thất bại');
      }
    }
    this.loading = false;
  }

  close(){
    this.dialogRef.close();
  }
}
