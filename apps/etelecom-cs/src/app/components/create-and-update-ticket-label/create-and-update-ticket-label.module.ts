import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAndUpdateTicketLabelComponent } from './create-and-update-ticket-label.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { EtopMaterialModule, MaterialModule } from '@etop/shared';



@NgModule({
  declarations: [
    CreateAndUpdateTicketLabelComponent
  ],
  exports: [
    CreateAndUpdateTicketLabelComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MaterialModule,
    EtopMaterialModule,
  ]
})
export class CreateAndUpdateTicketLabelModule { }
