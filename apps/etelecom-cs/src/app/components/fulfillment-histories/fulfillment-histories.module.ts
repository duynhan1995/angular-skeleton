import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FulfillmentHistoriesComponent } from './fulfillment-histories.component';

@NgModule({
  declarations: [
    FulfillmentHistoriesComponent
  ],
  exports: [
    FulfillmentHistoriesComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class FulfillmentHistoriesModule { }
