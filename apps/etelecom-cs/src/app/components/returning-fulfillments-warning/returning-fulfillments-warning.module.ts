import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturningFulfillmentsWarningComponent } from './returning-fulfillments-warning.component';
import { FulfillmentService } from 'apps/etelecom-cs/src/services/fulfillment.service';

@NgModule({
  declarations: [
    ReturningFulfillmentsWarningComponent
  ],
  exports: [
    ReturningFulfillmentsWarningComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    FulfillmentService,
  ]
})
export class ReturningFulfillmentsWarningModule { }
