import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../../../../core/src/services/cms.service';
import { MatDialogBaseComponent } from '@etop/web';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'etelecom-cs-paycard-turtorial-modal',
  templateUrl: './paycard-turtorial-modal.component.html',
  styleUrls: ['./paycard-turtorial-modal.component.scss']
})
export class PaycardTurtorialModalComponent extends MatDialogBaseComponent implements OnInit {
  banner ='';

  constructor(
    private cms: CmsService,
    private dialogRef: MatDialogRef<PaycardTurtorialModalComponent>,
  ) {
    super()
  }

  ngOnInit(): void {
    this.getPayCardTurtorial();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  async getPayCardTurtorial() {
    await this.cms.initBanners();
    const result = this.cms.getPayCardTutorial();
    if (result) {
      this.banner = result;
    }
  }
}
