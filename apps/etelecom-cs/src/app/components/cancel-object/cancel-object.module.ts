import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';
import { CancelObjectComponent } from './cancel-object.component';

@NgModule({
  declarations: [
    CancelObjectComponent
  ],
  entryComponents: [
    CancelObjectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    EtopMaterialModule
  ]
})
export class CancelObjectModule { }
