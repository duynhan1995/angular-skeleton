import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ContactQuery, ContactService} from '@etop/state/shop/contact';
import {ContactsListComponent} from './component/contacts-list/contacts-list.component';
import {distinctUntilChanged, map} from 'rxjs/operators';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {RelationshipService} from '@etop/state/relationship';
import {CallLogTab, ContactTab} from '@etop/models';
import {ExtensionService} from "@etop/state/shop/extension";

@Component({
  selector: 'etelecom-cs-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit, OnDestroy {
  @ViewChild('contactsList', {static: true}) contactsList: ContactsListComponent;
  tab$ = this.contactQuery.select(state => state.ui.tab);
  tab = {
    general: ContactTab.general,
    internal: ContactTab.internal
  }

  constructor(
    private headerController: HeaderControllerService,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private relationshipService: RelationshipService,
    private extensionService: ExtensionService
  ) {
  }

  async ngOnInit(): Promise<void> {
    await this._prepareData();
    this.setupTabs();
  }


  checkTab(tabName) {
    return this.tab$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  async _prepareData() {
    await this.extensionService.getExtensions();
    await this.relationshipService.getRelationshipsHaveExtension();
    await this.reloadContacts();
  }

  async reloadContacts() {
    const paging = {
      limit: 20,
      after: '.',
    };
    this.contactService.setPaging(paging);
    this.contactService.setCurrentPage(1);
    await this.contactService.getContacts();
  }

  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Danh bạ chung',
          name: ContactTab.general,
          active: false,
          onClick: () => {
            this.contactService.changeHeaderTab(ContactTab.general);
          }
        },
        {
          title: 'Danh bạ nội bộ',
          name: ContactTab.internal,
          active: false,
          onClick: () => {
            this.contactService.changeHeaderTab(ContactTab.internal);
          }
        },
      ].map(tab => {
        this.checkTab(tab.name).subscribe(active => (tab.active = active));
        return tab;
      })
    );
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }
}
