import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import {
  EmptyPageModule,
  EtopCommonModule, EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule,
  SideSliderModule
} from '@etop/shared';
import { ContactsComponent } from './contacts.component';
import { ContactsListComponent } from './component/contacts-list/contacts-list.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { CustomerRowComponent } from './component/contacts-rows/contacts-row.component';
import { CreateContactsFormModule } from './component/create-contacts-form/create-contacts-form.module';
import { ConfirmDeleteContactModule } from './component/confirm-delete-contact/confirm-delete-contact.module';
import { AuthenticateModule } from '@etop/core';
import { InternalContactsListComponent } from './component/internal-contact/internal-contact.component';
import { InternalContactRowComponent } from './component/internal-contact/component/internal-contact-row/internal-contact-row.component';
import {ContactDetailFormModule} from "./component/contact-detail/contact-detail.module";

const routes: Routes = [
  {
    path: '',
    component: ContactsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    EtopCommonModule,
    EtopPipesModule,
    SideSliderModule,
    EtopMaterialModule,
    CreateContactsFormModule,
    ContactDetailFormModule,
    ConfirmDeleteContactModule,
    MaterialModule,
    AuthenticateModule,
    EmptyPageModule,
    EtopFilterModule,
  ],
  exports: [],
  declarations: [
    ContactsListComponent,
    ContactsComponent,
    CustomerRowComponent,
    InternalContactsListComponent,
    InternalContactRowComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class ContactsModule {}
