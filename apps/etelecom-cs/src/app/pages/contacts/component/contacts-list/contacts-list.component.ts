import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '@etop/models/Contact';
import { ContactQuery, ContactService } from '@etop/state/shop/contact';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core/base';
import { Paging } from '@etop/shared';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ConfirmDeleteContactComponent } from '../confirm-delete-contact/confirm-delete-contact.component';
import {CursorPaging, FilterOperator, FilterOptions, Filters} from "@etop/models";
import {ContactAPI} from "@etop/api";

@Component({
  selector: 'etelecom-cs-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('contactsTable', { static: true }) contactsTable: EtopTableComponent;
  @ViewChild('contactsSlider', { static: true }) contactsSlider: SideSliderComponent;

  isFirstRequestNewPage = true;
  contacts$ = this.contactQuery.selectAll();
  contactActive$ = this.contactQuery.selectActive();
  contacts = [];
  selectedContacts:Contact = null;
  creatingNewContacts: boolean = false;
  filters: FilterOptions;

  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  private modal: any;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private dropdownController: DropdownActionsControllerService,
    private router: Router,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private modalController: ModalController,
    private route: ActivatedRoute
  ) { super()}

  private _onSelectModeChanged(value) {
    this.contactsTable.toggleLiteMode(value);
    this.contactsSlider.toggleLiteMode(value);
  }
  ngOnInit() {
    this.filters = [
      {
        label: 'Tên',
        name: 'name',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
      {
        label: 'Số điện thoại',
        name: 'phone',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
    ];

    this.perpage = this.contactsTable.perpage;
    this.headerController.setActions([
      {
        title: 'Thêm liên hệ',
        cssClass: 'btn btn-primary',
        onClick: () => this.createNewContacts(),
      }
    ]);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.contactService.setActive(null);
  }

  async ngAfterViewInit() {
    const { queryParams } = this.route.snapshot;
    let id = queryParams?.id;
    if (id) {
      let contact = await this.contactService.getContact(id);
      if (contact) {
        setTimeout(async _ => {
          await this.detail(contact);
        }, 1000)
      }
    }
    this.router.navigate([], {
      queryParams: {
        'id': null,
      },
      queryParamsHandling: 'merge'
    })
  }

  async getContacts() {
    try {
      this.selectMode = false;
      this.contactsTable.toggleNextDisabled(false);
      const paging = {
        limit: 20,
        after: '.',
      };

      const filter = {
        filter: null,
        paging
      }
      await this.contactService.setFilter(filter)
      await this.contactService.getContacts();
    } catch (e) {
      debug.error('ERROR in getting list contacts', e);
    }
  }

  async loadPage({ page, perpage }) {
    this.perpage = perpage;
    if(this.isFirstRequestNewPage) {
      return this.isFirstRequestNewPage = false;
    }

    let paging: CursorPaging;
    let currentPaging = this.contactQuery.Ui.paging;
    let currentPage = this.contactQuery.Ui.currentPage;

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    this.contactService.setPaging(paging);
    await this.contactService.getContacts();
    this.contactService.setCurrentPage(page);
  }

  detail(contacts: Contact) {
    this.creatingNewContacts = false;
    this.contactService.setActive(contacts.id);
    this.selectedContacts = contacts;
    this._checkSelectMode();
  }

  private async _checkSelectMode() {
    this.selectedContacts = this.contactQuery.getActive();
    const selected = !!this.contactQuery.getActive();
    this.contactsTable.toggleLiteMode(selected);
    this.contactsSlider.toggleLiteMode(selected);
    this.setupDropdownActions();
    this.selectMode = !!this.selectedContacts || this.creatingNewContacts;
    if (this.queryParams.code && this.selectedContacts) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/contacts`);
      this.contactsTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.creatingNewContacts = false;
    this.contactService.setActive(null);
    this._checkSelectMode();
  }

  createNewContacts() {
    this.contactService.setActive(null);
    this.creatingNewContacts = true;
    this._checkSelectMode();
  }

  async deleteContacts() {
    try {
      const id = this.contactQuery.getActiveId();
      await this.contactService.deleteContact(id);
      toastr.success('Xóa liên hệ thành công!');
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/contacts`);
      this.contactsTable.resetPagination()
    } catch (e) {
      debug.error('ERROR in deleting contacts', e);
      toastr.error(e, 'Xóa liên hệ không thành công');
    }
  }

  private setupDropdownActions() {
    if (!this.selectedContacts) {
      this.dropdownController.clearActions();
      return;
    }
    this.dropdownController.setActions([
      {
        onClick: () => this.confirmDeleteContact(),
        title: `Xóa liên hệ`,
        cssClass: 'text-danger',
        roles: ['owner', 'telecom_customerservice']
      }
    ]);
  }

  confirmDeleteContact() {
    const name = this.selectedContacts.full_name;
    this.modal = this.modalController.create({
      component: ConfirmDeleteContactComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        name,
        deleteFn: async () => this.deleteContacts()
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async () => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async filter(filters: Filters) {
    const _filter: ContactAPI.GetContactsFilter = {};
    filters.forEach(f => {
      if(f.value){
        _filter[f.name] = f.value;
      }
    })
    this.contactService.setFilter({filter: _filter});
    await this.contactService.getContacts();
  }

  get sliderTitle() {
    if (this.creatingNewContacts) {
      return 'Thêm liên hệ mới';
    }
    return 'Chi tiết liên hệ';
  }

  get emptyDescription() {
    return 'Danh bạ chung của công ty sẽ xuất hiện tại đây.';
  }

  get showPaging() {
    return !this.contactsTable.liteMode && !this.contactsTable.loading;
  }

  get showDetailContacts() {
    return !!this.selectedContacts;
  }

  get sliderWidth() {
    return this.util.isMobile ? "100%" : "50%";
  }
}
