import {Component, OnInit, EventEmitter, Output, Input, SimpleChanges, OnChanges} from '@angular/core';
import {Router} from '@angular/router';
import {BaseComponent} from '@etop/core';
import {Ticket, TicketType} from '@etop/models';
import {Contact} from '@etop/models/Contact';
import {ContactQuery, ContactService} from '@etop/state/shop/contact';
import {UtilService} from 'apps/core/src/services/util.service';
import {TicketQuery, TicketService} from "@etop/state/shop/ticket";
import {ShopTicketApi} from "@etop/api";

@Component({
  selector: 'etelecom-cs-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() contact = new Contact({});

  activeContact$ = this.contactQuery.selectActive();
  full_name: string;
  phone: string;
  ticketsContact = [];
  loading = true;

  constructor(
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private ticketService: TicketService,
    private ticketApi: ShopTicketApi,
    private ticketQuery: TicketQuery,
    private router: Router,
    private util: UtilService
  ) {
    super();
  }

  async ngOnInit() {
    this.activeContact$.subscribe(a => {
      this.full_name = a?.full_name;
      this.phone = a?.phone;
    })
    this.full_name = this.contact?.full_name;
    this.phone = this.contact?.phone;

  }

  async ngOnChanges(changes: SimpleChanges) {
    this.loading = true;
    const paging = {
      limit: 1000,
      offset: 0,
    };
    const filter = {
      ref_id: this.contact.id,
      types: [TicketType.internal],
    };
    this.ticketsContact = await this.contactService.getTickets({paging, filter});
    this.loading = false;
  }

  ticketDetail(ticket: Ticket) {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/tickets?id=${ticket.id}`);
  }

  async updateContact() {
    try {
      const body = {
        ...this.contact,
        full_name: this.full_name,
        phone: this.phone
      };
      if (!body.full_name) {
        return toastr.error('Chưa nhập tên khách hàng!');
      }
      if (!body.phone) {
        return toastr.error('Chưa nhập số điện thoại khách hàng!');
      }
      await this.contactService.updateContact(body);
      this.contactService.getContacts();
      toastr.success('Cập nhật liên hệ thành công!');
    } catch (e) {
      toastr.error(e.message, 'Cập nhật liên hệ thất bại');
    }
  }

  createTicket(contact: Contact) {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/tickets?contact_id=${contact.id}`);
  }

}
