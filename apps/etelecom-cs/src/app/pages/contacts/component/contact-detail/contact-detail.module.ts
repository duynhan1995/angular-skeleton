import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';
import { AuthenticateModule } from '@etop/core';
import {ContactDetailComponent} from "./contact-detail.component";


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule, AuthenticateModule],
  exports: [ContactDetailComponent],
  entryComponents: [],
  declarations: [ContactDetailComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContactDetailFormModule {}
