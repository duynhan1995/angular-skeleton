import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule } from '@etop/shared';
import { CreateContactsFormComponent } from './create-contacts-form.component';


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule],
  exports: [CreateContactsFormComponent],
  entryComponents: [],
  declarations: [CreateContactsFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateContactsFormModule {}