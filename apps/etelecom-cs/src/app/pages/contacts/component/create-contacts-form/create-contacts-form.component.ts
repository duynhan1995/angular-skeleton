import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {BaseComponent} from '@etop/core';
import {ContactService} from '@etop/state/shop/contact';
import {Contact} from "@etop/models";

@Component({
  selector: 'etelecom-cs-create-contacts-form',
  templateUrl: './create-contacts-form.component.html',
  styleUrls: ['./create-contacts-form.component.scss']
})
export class CreateContactsFormComponent extends BaseComponent implements OnInit {
  contact = new Contact({});
  @Output() createContacts = new EventEmitter();

  loading = false;

  constructor(
    private contactService: ContactService,
  ) {
    super();
  }

  ngOnInit() {
  }

  async create() {
    this.loading = true;
    try {
      if (!this.contact.full_name) {
        this.loading = false;
        return toastr.error('Vui lòng nhập tên!');
      }
      if (!this.contact.phone) {
        this.loading = false;
        return toastr.error('Vui lòng nhập số điện thoại!');
      }
      const data = {
        full_name: this.contact.full_name,
        phone: this.contact.phone
      }
      await this.contactService.createContacts(data);
      toastr.success('Tạo liên hệ mới thành công');
      this.createContacts.emit();
      this.contact = new Contact({});
    } catch (e) {
      toastr.error(e.message, 'Tạo liên hệ thất bại!');
    }
    this.loading = false;
  }

}
