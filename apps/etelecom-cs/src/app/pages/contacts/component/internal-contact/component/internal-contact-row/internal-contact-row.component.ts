import { Component, OnInit, Input } from '@angular/core';
import {Extension, Relationship} from '@etop/models';
import {ExtensionQuery} from "@etop/state/shop/extension";

@Component({
  selector: '[etelecom-cs-internal-contact-row]',
  templateUrl: './internal-contact-row.component.html',
  styleUrls: ['./internal-contact-row.component.scss']
})
export class InternalContactRowComponent implements OnInit {
  @Input() contact = new Relationship();
  @Input() liteMode = false;

  extension: Extension;
  constructor(
    private extensionQuery: ExtensionQuery
  ){}
  async ngOnInit() {
    this.extension = this.extensionQuery.getExtensionsByUserId(this.contact.id)[0];
   }
}
