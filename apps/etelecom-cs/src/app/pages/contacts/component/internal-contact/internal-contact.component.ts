import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { BaseComponent } from '@etop/core/base';
import {Extension, FilterOperator, FilterOptions, Filters} from '@etop/models';
import { ExtensionQuery, ExtensionService } from '@etop/state/shop/extension';
import {RelationshipQuery, RelationshipService} from "@etop/state/relationship";
import {ContactAPI} from "@etop/api";

@Component({
  selector: 'etelecom-cs-internal-contacts-list',
  templateUrl: './internal-contact.component.html',
  styleUrls: ['./internal-contact.component.scss']
})
export class InternalContactsListComponent extends BaseComponent implements OnInit, OnDestroy, OnDestroy {
  @ViewChild('contactsTable', { static: true }) contactsTable: EtopTableComponent;
  @ViewChild('contactsSlider', { static: true }) contactsSlider: SideSliderComponent;

  isFirstRequestNewPage = true;
  relationship$ = this.relationshipQuery.selectAll();
  selectedContacts:Extension = null;
  page: number;
  perpage: number;
  filters: FilterOptions;

  queryParams = {
    code: '',
    type: '',
  };

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private changeDetector: ChangeDetectorRef,
    private extensionQuery: ExtensionQuery,
    private extensionService: ExtensionService,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery
  ) { super()}

  private _onSelectModeChanged(value) {
    this.contactsTable.toggleLiteMode(value);
    this.contactsSlider.toggleLiteMode(value);
  }

  async ngOnInit() {
    this.filters = [
      {
        label: 'Tên',
        name: 'name',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
      {
        label: 'Số điện thoại',
        name: 'phone',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
      {
        label: 'Số máy nhánh',
        name: 'extension_number',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
    ]
    await this.extensionService.getExtensions();
    this.page = this.contactsTable.currentPage;
    this.perpage = this.contactsTable.perpage;
  }

  async getExtensions(page?:number , perpage?: number) {
    try {
      this.selectMode = false;
      this.contactsTable.toggleNextDisabled(false);

      let _contacts = [];

      await this.extensionService.getExtensions();
      _contacts = this.extensionQuery.getAll()

      if (page > 1 && _contacts.length == 0) {
        this.contactsTable.toggleNextDisabled(true);
        this.contactsTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả liên hệ.');
        return;
      }
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list contacts', e);
    }
  }

  async filter(filters: Filters){
    const _filter: ContactAPI.GetContactsFilter = {};
    filters.forEach(f => {
      if(f.value && f.name){
        _filter[f.name] = f.value;
      }
    })
    this.relationshipService.setFilter(_filter);
    await this.relationshipService.getRelationshipsHaveExtension();
  }

  async loadPage({ page, perpage }) {
    if(this.isFirstRequestNewPage) {
      return this.isFirstRequestNewPage = false;
    }
    this.contactsTable.loading = true;
    this.changeDetector.detectChanges();
    await this.getExtensions(page, perpage);
    this.contactsTable.loading = false;
    this.changeDetector.detectChanges();
  }

  get emptyDescription() {
    return 'Danh bạ nội bộ của công ty sẽ xuất hiện tại đây.';
  }

  get showPaging() {
    return !this.contactsTable.liteMode && !this.contactsTable.loading;
  }

  get showDetailContacts() {
    return !!this.selectedContacts;
  }


}
