import { Component, OnInit, Input } from '@angular/core';
import { Contact } from '@etop/models/Contact';

@Component({
  selector: '[etelecom-cs-contacts-row]',
  templateUrl: './contacts-row.component.html',
  styleUrls: ['./contacts-row.component.scss']
})
export class CustomerRowComponent implements OnInit {
  @Input() contact = new Contact({}) ;
  @Input() liteMode = false;
  async ngOnInit() { }
}
