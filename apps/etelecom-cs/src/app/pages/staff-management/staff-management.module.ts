import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import {EmptyPageModule, EtopCommonModule, EtopFilterModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import { StaffManagementComponent } from './staff-management.component'
import { MobileStaffManagementComponent } from './mobile-staff-management/mobile-staff-management.component';
import { AuthenticateModule } from '@etop/core';
import { DesktopStaffManagementComponent } from './desktop-staff-management/desktop-staff-management.component';
import { AddStaffDialogComponent } from './components/add-staff-dialog/add-staff-dialog.component';
import { StaffManagementRowComponent } from './components/staff-management-row/staff-management-row.component';
import { UserInvitationRowComponent } from './components/user-invitation-row/user-invitation-row.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { CreateHotlineDialogComponent } from './components/create-hotline-dialog/create-hotline-dialog.component';
import { UpdateStaffDialogComponent } from './components/update-staff-dialog/update-staff-dialog.component';
import { DesktopExtensionListComponent } from './desktop-extension-list/desktop-extension-list.component';
import { DesktopExtensionRowComponent } from './components/desktop-extension-row/desktop-extension-row.component';
import {MobileExtensionListComponent} from "./mobile-extension-list/mobile-extension-list.component";
import {MobileExtensionRowComponent} from "./components/mobile-extension-row/mobile-extension-row.component";
import { InitializationExtensionDialogComponent } from './components/initialization-extension-dialog/initialization-extension-dialog.component';
import { ExtendExtensionComponent } from './components/extend-extension-dialog/extend-extension-dialog.component';
import {NgbTooltipModule} from "@ng-bootstrap/ng-bootstrap";
import {DesktopShopInvitationComponent} from "./desktop-shop-invitation/desktop-shop-invitation.component";

const routes: Routes = [
  {
    path: '',
    component: StaffManagementComponent
  }
];

@NgModule({
  imports: [
    AuthenticateModule,
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    MaterialModule,
    DropdownActionsModule,
    EmptyPageModule,
    EtopCommonModule,
    EtopFilterModule,
    NgbTooltipModule,
  ],
  exports: [],
  declarations: [
    UpdateStaffDialogComponent,
    AddStaffDialogComponent,
    StaffManagementComponent,
    MobileStaffManagementComponent,
    StaffManagementRowComponent,
    UserInvitationRowComponent,
    DesktopStaffManagementComponent,
    CreateHotlineDialogComponent,
    InitializationExtensionDialogComponent,
    DesktopExtensionListComponent,
    MobileExtensionListComponent,
    DesktopExtensionRowComponent,
    MobileExtensionRowComponent,
    ExtendExtensionComponent,
    DesktopShopInvitationComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class StaffManagementModule {}
