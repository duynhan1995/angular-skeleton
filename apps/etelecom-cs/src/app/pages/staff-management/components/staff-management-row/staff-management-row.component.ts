import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {Extension, Hotline, Relationship} from '@etop/models';
import { CreateHotlineDialogComponent } from '../create-hotline-dialog/create-hotline-dialog.component';
import { ExtensionService } from '@etop/state/shop/extension';
import { UpdateStaffDialogComponent } from '../update-staff-dialog/update-staff-dialog.component';
import { ExtensionQuery, ExtensionStore } from '@etop/state/shop/extension';
import { HotlineQuery } from '@etop/state/shop/hotline';
import {ExtensionApi} from "@etop/api/shop/extension.api";
import {DropdownActionOpt} from "../../../../../../../shared/src/components/dropdown-actions/dropdown-actions.interface";
import {takeUntil} from "rxjs/operators";
import {MatDialogController} from "@etop/shared";
import { InitializationExtensionDialogComponent } from '../initialization-extension-dialog/initialization-extension-dialog.component';
import { CreateExtensionWithSubcriptionComponent } from 'apps/etelecom-cs/src/app/components/create-extension-with-subcription/create-extension-with-subcription.component';
import {AuthorizationService} from "@etop/state/authorization";
import { ExtendExtensionComponent } from '../extend-extension-dialog/extend-extension-dialog.component';


@Component({
  selector: '[etelecom-cs-staff-management-row]',
  templateUrl: './staff-management-row.component.html',
  styleUrls: ['./staff-management-row.component.scss']
})
export class StaffManagementRowComponent extends BaseComponent implements OnInit, OnDestroy {
  @Input() staff = new Relationship();
  timeToExpire = 10;

  currentHotline: Hotline;
  extensions$ = this.extensionQuery.selectAll();
  currentExtension: Extension;

  loading = true;
  removing = false;

  dropdownActions: DropdownActionOpt[] = [];

  get isCurrentUser() {
    return this.staff.user_id == this.authStore.snapshot.user.id;
  }

  get hasCurrentExtension() {
    const extensions = this.extensionQuery.getAll();
    return extensions.find(ext => ext.user_id == this.staff.user_id);
  }

  get isOwner() {
    return this.staff.roles.indexOf('owner') != -1;
  }

  get roleDisplay() {
    if (this.staff.roles.includes('owner'))
      return 'Chủ công ty';
    if(this.staff.roles.includes('m_admin'))
      return 'Admin'
    if(this.staff.roles.includes('salesman')) {
      if(this.staff.roles.includes('telecom_customerservice_management') && this.staff.roles.includes('staff_management'))
        return 'Quản lý'
      if (this.staff.roles.includes('telecom_customerservice') && this.staff.roles.length == 2)
        return 'Nhân viên';
    }
    return 'Khác'
  }

  //Check employee have extension or not.
  get hasUserExtension(): boolean {
    const extensions = this.extensionQuery.getAll();
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline.connection_method == "direct");
    if (extensions?.length) {
      this.currentExtension = extensions.find(ex => ex.user_id == this.staff.user_id);
      this.currentHotline = hotlines.find(hotline => hotline.id == this.currentExtension?.hotline_id);
    }
    return !!this.currentExtension?.extension_number;
  }

  constructor(
    private authorizationService: AuthorizationService,
    private authStore: AuthenticateStore,
    private matDialogController: MatDialogController,
    private extensionService: ExtensionService,
    private extensionQuery: ExtensionQuery,
    private extensionApi: ExtensionApi,
    private hotlineQuery: HotlineQuery,
    private extensionStore: ExtensionStore,
  ) {
      super();
  }

  async ngOnInit() {
    this.loading = true;
    this.extensions$.pipe(takeUntil(this.destroy$)).subscribe(extensions => {
      this.currentExtension =  extensions.find(ext => ext.user_id == this.staff.user_id);
      this.setDropdownActions();
    });
    this.loading = false;
  }


  setDropdownActions() {
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa',
        cssClass: this.isCurrentUser && 'cursor-not-allowed',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:update', 'relationship/permission:update'],
        hidden: this.isOwner,
        onClick: () => this.updateStaff()
      },
      {
        title: 'Gia hạn máy nhánh',
        hidden: !this.currentExtension,
        permissions: ['relationship/relationship:update'],
        onClick: () =>  this.extendExtention()
      },
      {
        title: 'Gỡ máy nhánh',
        hidden: !this.currentExtension,
        permissions: ['relationship/relationship:remove'],
        onClick: () => this.confirmRemoveExtension()
      },
      {
        title: 'Xoá khỏi công ty',
        cssClass: this.isCurrentUser && 'cursor-not-allowed' || 'text-danger',
        disabled: this.isCurrentUser,
        permissions: ['relationship/relationship:remove'],
        hidden: this.isOwner,
        onClick: () => this.confirmRemoveStaff()
      }
    ];
  }

  updateStaff() {
    const dialog = this.matDialogController.create({
      component: UpdateStaffDialogComponent,
      config: {
        data: {
          staff: {
            ...this.staff,
            p_data: {
              ...this.staff,
            }
          }
        }
      },
      afterClosedCb: (updated) => {
        if(updated) {
          this.authorizationService.updateRelationship();
        }
      },
    });
    dialog.open();
  }

  async removeStaff() {
    this.removing = true;
    try {
      if (this.currentExtension) {
        await this.extensionApi.removeUserOfExtension(this.currentExtension.id, this.staff.user_id);
      }
      await this.authorizationService.removeUserFromAccount(this.staff.user_id);
      this.authorizationService.removeRelationship();
      toastr.success('Xoá nhân viên thành công.')
    } catch (e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Staff', e);
    }
    this.removing = false;
  }

  async removeExtension() {
    try {
      await this.extensionApi.removeUserOfExtension(this.currentExtension.id,this.staff.user_id);
      this.extensionService.getExtensions(null, true);
      toastr.success('Gỡ máy nhánh thành công.');
    } catch (e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Extension', e);
    }
  }

  confirmRemoveStaff() {
    const dialog = this.matDialogController.create({
      template:{
        title: `Xoá nhân viên`,
        content: `
        <div>Bạn thực sự muốn xóa nhân viên "<strong>${this.staff?.full_name}</strong>"?</div>
      `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmStyle: 'btn-danger text-white',
      afterClosedCb: () => {},
      onConfirm: async () => {
        await this.removeStaff();
      }
    });
    dialog.open();
  }

  confirmRemoveExtension() {
    const dialog = this.matDialogController.create({
      template:{
        title: `Gỡ máy nhánh`,
        content: `<div>Bạn thực sự muốn gỡ máy nhánh của nhân viên "<strong>${this.staff?.full_name}</strong>"?</div>`,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Gỡ',
      confirmStyle: 'btn-danger text-white',
      onConfirm: async () => {
        await this.removeExtension();
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async initializationExtension() {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline?.connection_method == "direct");
    if(!hotlines?.length){
      toastr.error('Không thể tạo máy nhánh do tổng đài của bạn chưa được gắn Hotline');
      return;
    }
    const dialog = this.matDialogController.create({
      component: InitializationExtensionDialogComponent,
      config: {
        data: {staff: this.staff },
      },

      afterClosedCb: async (result: {extension: Extension, staff: Relationship, new_extension: boolean}) => {
        if (!result) return;
        if (result?.extension?.id) {
          this.confirmAssignExtension(result?.extension);
        }
        if (result?.new_extension) {
          await this.createExtensionWithSubcription();
        }
      }
    });

    dialog.open();
  }

  confirmAssignExtension(extension: Extension) {
    const dialog = this.matDialogController.create({
      template: {
        title: 'Gán máy nhánh',
        content: `<div>Bạn có chắc muốn gán máy nhánh <strong>${extension?.extension_number}</strong> cho <strong>${this.staff?.full_name}</strong>?</div>`
      },
      onConfirm: async () => {
        await this.createExtensionWithUserId(extension?.id, this.staff?.user_id);
      },
      config: {
        data: {staff: this.staff },
      },
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  async createExtensionWithSubcription() {
    const dialog = this.matDialogController.create({
      component: CreateExtensionWithSubcriptionComponent,
      config: {
        data: {staff: this.staff },
      },
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  async createExtensionWithUserId(extension_id: string, user_id: string) {
    try {
      await this.extensionService.assignUserToExtension(extension_id, user_id)
      await this.extensionService.getExtensions(null, true);
      toastr.success('Gán máy nhánh thành công');
    } catch(e) {
      toastr.error(e.message || e.msg);
    }
  }


  async createExtension() {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline?.connection_method == "direct");
    if(!hotlines?.length){
      toastr.error('Không thể tạo máy nhánh do tổng đài của bạn chưa được gắn Hotline');
      return;
    }
    const dialog = this.matDialogController.create({
      component: CreateHotlineDialogComponent,
      config: {
        data: {
          hotlines: hotlines,
          staff: this.staff,
        }
      },
      afterClosedCb: (extension) => {
        this.extensionStore.add(extension);
        this.currentExtension = extension;
        this.setDropdownActions();
      },
    })
    dialog.open();
  }

  expireTimeLeft(date) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const toDate = new Date();
    const checkDate = new Date(date);

    const diffDays = Math.ceil(
      (checkDate.getTime() -toDate.getTime()) / oneDay
    );
    return diffDays;
  }

  isUnlimited(date) {
    return date == '0001-01-01T00:00:00Z';
  }

  extendExtention() {
    this.extensionService.setActive(this.currentExtension.id);
    const dialog = this.matDialogController.create({
      component: ExtendExtensionComponent,
      afterClosedCb: () => {},
    });
    dialog.open();
  }
}
