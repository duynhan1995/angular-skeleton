import { Component, Input, OnInit } from '@angular/core';
import { Extension } from '@etop/models';
import { MatDialogController } from '@etop/shared/components/etop-material/material-dialog/mat-dialog.controller';
import { ExtensionService } from '@etop/state/shop/extension';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import { ExtendExtensionComponent } from '../extend-extension-dialog/extend-extension-dialog.component';

@Component({
  selector: '[etelecom-cs-extension-row]',
  templateUrl: './desktop-extension-row.component.html',
  styleUrls: ['./desktop-extension-row.component.scss'],
})

export class DesktopExtensionRowComponent implements OnInit {
  @Input() extension: Extension;
  timeToExpire = 10;
  dropdownActions: DropdownActionOpt[] = [];

  constructor(
    private matDialogController: MatDialogController,
    private extensionService: ExtensionService
    ) {}

  ngOnInit(): void {
    this.setDropdownActions();
  }

  expireTimeLeft(date) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const toDate = new Date();
    const checkDate = new Date(date);

    const diffDays = Math.ceil(
      (checkDate.getTime() - toDate.getTime()) / oneDay
    );
    return diffDays;
  }

  isUnlimited(date) {
    return date == '0001-01-01T00:00:00Z';
  }
  setDropdownActions() {
    this.dropdownActions = [
      {
        title: 'Gia hạn máy nhánh',
        onClick: () => this.extendExtention(),
      },
    ];
  }

  extendExtention() {
    this.extensionService.setActive(this.extension.id);
    const dialog = this.matDialogController.create({
      component: ExtendExtensionComponent,
      afterClosedCb: () => {},
    });
    dialog.open();
  }
}
