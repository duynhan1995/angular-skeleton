import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SubscriptionService } from '@etop/state/shop/subscription';
import { HotlineQuery } from '@etop/state/shop/hotline';
import { ProductSubscriptionType } from '@etop/api';
import { Extension, SubscriptionPlan, SubscriptionProduct } from '@etop/models';
import { DecimalPipe } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { ExtensionAPI, PaymentMethod } from '@etop/api/shop/extension.api';
import { ExtensionQuery, ExtensionService } from '@etop/state/shop/extension';
import { ShopService, TelegramService } from '@etop/features';
import { MatDialogBaseComponent } from '@etop/web';
import { MatDialogController } from '@etop/shared';

@Component({
  selector: 'etelecom-cs-extend-extension-dialog',
  templateUrl: './extend-extension-dialog.component.html',
  styleUrls: ['./extend-extension-dialog.component.scss']
})
export class ExtendExtensionComponent extends MatDialogBaseComponent implements OnInit {
  hotlines = this.hotlineQuery.getAll().filter(hotline => hotline?.connection_method == "direct");
  subscriptionPlans: SubscriptionPlan[] = [];

  displayMapHotline = option => option && `${option.name} - ${option.hotline}` || null;
  displayMapPlan = option => option && `${option.name} - ${this.decimalPipe.transform(option.price)}đ` || null;
  valueMap = option => option && option.id || null;
  extensionWithSubForm = this.fb.group({
    extension_number: '',
    hotline_id: '',
    payment_method: PaymentMethod.BALANCE,
    subscription_plan_id: '',
  });
  telecom_balance = 0;
  warningEnoughBalance = '';
  currentExtension: Extension;

  constructor(
    private hotlineQuery: HotlineQuery,
    private dialogRef: MatDialogRef<ExtendExtensionComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private subscriptionService: SubscriptionService,
    private decimalPipe: DecimalPipe,
    private fb: FormBuilder,
    private extensionService: ExtensionService,
    private extensionQuery: ExtensionQuery,
    private shopService: ShopService,
    private matDialogController: MatDialogController,
    private telegramService: TelegramService,
  ) {
    super();
  }

  async ngOnInit() {
    let subscriptionProducts:SubscriptionProduct[] =  await this.subscriptionService.getSubscriptionProducts(ProductSubscriptionType.EXTENSION);
    if (subscriptionProducts.length > 0) {
      this.subscriptionPlans = await this.subscriptionService.getSubscriptionPlans(subscriptionProducts[0].id);
    }
    this.currentExtension = this.extensionQuery.getActive();
    this.extensionWithSubForm.patchValue({
      hotline_id: this.currentExtension.hotline_id,
      subscription_plan_id: this.subscriptionPlans[0].id,
      extension_number: this.currentExtension.extension_number
    }, { emitEvent: true })

    this.extensionWithSubForm.controls['subscription_plan_id'].valueChanges.subscribe(value => {
      this.checkBalance();
    })
    let _balance = await this.shopService.calcShopBalance('telecom');
    this.telecom_balance = _balance?.telecom_balance;
    this.checkBalance();
  }

  checkBalance() {
    const plan = this.subscriptionPlans.find(plan => plan.id == this.extensionWithSubForm.controls['subscription_plan_id'].value);
    if (this.telecom_balance < Number(plan.price)) {
      this.warningEnoughBalance = 'Số dư không đủ để thanh toán';
    } else {
      this.warningEnoughBalance = '';
    }
  }

  async confirmCreateExtension() {
    this.dialogRef.close();
    const plan = this.subscriptionPlans.find(plan => plan.id == this.extensionWithSubForm.controls['subscription_plan_id'].value);
    let content = `Bạn có chắc muốn gia hạn máy nhánh 
    <strong>${this.currentExtension.extension_number}</strong> với thời gian sử dụng 
    <strong>${plan?.name} (${this.decimalPipe.transform(plan?.price)}đ)</strong>?`;
    
    const dialog = this.matDialogController.create({
      template: {
        title: 'Gia hạn máy nhánh',
        content
      },
      onConfirm: async () => {
        await this.extendExtension();
      },
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  async extendExtension() {
    try {
      const data = this.extensionWithSubForm.getRawValue();
      const { hotline_id, subscription_plan_id, payment_method, extension_number} =  data;
      let request: ExtensionAPI.ExtendExtensionRequest = {
        extension_id : this.currentExtension.id,
        subscription_plan_id,
        payment_method
      }
      if (this.data?.staff?.user_id) {
        request.user_id = this.data?.staff?.user_id;
      }
      
      const extensionExtended = await this.extensionService.extendExtension(request);
      await this.extensionService.getExtensions(null, true);
      this.dialogRef.close();
      const subscriptionPlan = this.subscriptionPlans.find(plan => plan.id === request.subscription_plan_id);
      const hotline = this.hotlines.find(h => h.id === hotline_id);
      const staffName = this.data?.staff?.id ? this.data?.staff?.full_name : '';
      const extensionNumber = extension_number ? extension_number : extensionExtended.extension_number;
      this.telegramService.extendExtension(subscriptionPlan, extensionNumber, staffName, hotline);
      toastr.success('Thanh toán và gia hạn máy nhánh thành công.');
    } catch(e) {
      toastr.error(e.msg || e.message);
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
