import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Extension} from '@etop/models';
import { ExtensionQuery } from '@etop/state/shop/extension';
import { HotlineQuery } from '@etop/state/shop/hotline';
import { MatDialogBaseComponent } from '@etop/web';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'etelecom-cs-initialization-extension-modal',
  templateUrl: './initialization-extension-dialog.component.html',
  styleUrls: ['./initialization-extension-dialog.component.scss']
})
export class InitializationExtensionDialogComponent extends MatDialogBaseComponent implements OnInit {
  hotlines = this.hotlineQuery.getAll().filter(hotline => hotline.connection_method == 'direct' && hotline.status == 'P');
  _extensions = this.extensionQuery.getAll();
  initExtensionForm = this.fb.group({
    hotline_id: '',
    extension_id: ''
  });
  extensions: Extension[];
  displayMapHotline = option => option && `${option.name} - ${option.hotline}` || null;
  valueMap = option => option && option.id || null;

  constructor(
    private hotlineQuery: HotlineQuery,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<InitializationExtensionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private cdr: ChangeDetectorRef,
    private extensionQuery: ExtensionQuery
  ) {
    super();
  }

  async ngOnInit() {
    this.extensions = this._extensions.filter(extension => extension.user_id == '0' && this.hotlines[0].id == extension.hotline_id);
    this.initExtensionForm.patchValue({
      hotline_id: this.hotlines[0]?.id,
    });
    if (this.extensions.length) {
      this.initExtensionForm.patchValue({
        extension_id: this.extensions[0]?.id
      });
    }
    this.initExtensionForm.controls['hotline_id'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(async (hotline_id) => {
        this.extensions = this._extensions.filter(extension => extension.user_id == '0' && hotline_id == extension.hotline_id);
        this.initExtensionForm.patchValue({
          extension_id: this.extensions[0]?.id
        });
        this.cdr.detectChanges();
      });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  confirmAssignExtension() {
    if (!this.initExtensionForm.controls['extension_id'].value) {
      return toastr.error('Vui lòng chọn máy nhánh. Nếu chưa có máy nhánh, tạo máy nhánh ngay!')
    }
    this.dialogRef.close({
      extension: this.extensions[this.findExtById(this.initExtensionForm.controls['extension_id'].value)],
      staff: this.data?.staff,
      hotline_id: this.initExtensionForm.controls['hotline_id'].value
    })
  }


  newExtension() {
    this.dialogRef.close({new_extension: true});
  }



  findExtById(id) {
    return this.extensions.findIndex(extension => extension.id == id);
  }

}
