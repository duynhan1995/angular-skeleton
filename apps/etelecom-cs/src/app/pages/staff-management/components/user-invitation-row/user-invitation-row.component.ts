import { Component, Input, OnInit } from '@angular/core';
import { Invitation } from '@etop/models/Authorization';
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: '[etelecom-cs-user-invitation-row]',
  templateUrl: './user-invitation-row.component.html',
  styleUrls: ['./user-invitation-row.component.scss']
})
export class UserInvitationRowComponent implements OnInit {
  @Input() invitation = new Invitation({});
  deleting = false;

  dropdownActions = [];

  constructor(
    private authorizationService: AuthorizationService
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Huỷ lời mời',
        permissions: ['relationship/invitation:delete'],
        cssClass: 'text-danger',
        onClick: () => this.deleteInvitation()
      },
      {
        title: 'Gửi lại SMS',
        onClick: () => this.resendInvitation()
      }
    ];
  }

  async deleteInvitation() {
    this.deleting = true;
    try {
      await this.authorizationService.deleteInvitation(this.invitation.token);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in deleting Invitation', e);
    }
    this.deleting = false;
  }

  get roleDisplay(){
    if(this.invitation.roles.includes('m_admin'))
      return 'Admin'
    if(this.invitation.roles.includes('salesman')) {
      if(this.invitation.roles.includes('telecom_customerservice_management') && this.invitation.roles.includes('staff_management'))
        return 'Quản lý'
      if (this.invitation.roles.includes('telecom_customerservice') && this.invitation.roles.length == 2)
        return 'Nhân viên';
    }
    return 'Khác'
  }

  async resendInvitation() {
    try {
      await this.authorizationService.resendInvitation(this.invitation.email, this.invitation.phone);
      toastr.success('Gửi lại lời mời thành công');
    } catch (e) {
      toastr.error(e.message || e.msg)
    }
  }

}
