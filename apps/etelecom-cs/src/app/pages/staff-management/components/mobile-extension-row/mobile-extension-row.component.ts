import {Component, Input, OnInit} from '@angular/core';
import {Extension} from "@etop/models";

@Component({
  selector: '[etelecom-cs-mobile-extension-row]',
  templateUrl: './mobile-extension-row.component.html',
  styleUrls: ['./moblie-extension-row.component.scss']
})
export class MobileExtensionRowComponent implements OnInit {
  @Input() extension: Extension;
  constructor() { }

  ngOnInit(): void {
  }

}
