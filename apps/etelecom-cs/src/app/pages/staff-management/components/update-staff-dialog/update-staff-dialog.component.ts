import { FormBuilder, Validators } from '@angular/forms';
import {Component, Inject, Input, OnInit} from '@angular/core';
import { Relationship } from 'libs/models/Authorization';
import { AuthorizationApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MatDialogBaseComponent} from "@etop/web";


enum TypeUpdate {
  Add = "add",
  RemoveAndAdd = "remove_and_add"
}
@Component({
  selector: 'etelecom-cs-update-staff-modal',
  templateUrl: './update-staff-dialog.component.html',
  styleUrls: ['./update-staff-dialog.component.scss']
})

export class UpdateStaffDialogComponent extends MatDialogBaseComponent implements OnInit {
  @Input() staff = new Relationship();
  loading = false;
  roleMap: string;
  dataEdited = false;
  roleList = [
    {name: 'Admin', value: 'm_admin'},
    {name: 'Nhân viên', value: 'salesman,telecom_customerservice'},
    {name: 'Quản lý', value: 'salesman,staff_management,telecom_customerservice_management'},
  ];
  staffForm = this.fb.group({
    full_name: ['', Validators.required],
    type_update: ['', Validators.required],
    roles: ['', Validators.required]
  });

  constructor(
    private dialogRef: MatDialogRef<UpdateStaffDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private authorizationAPI: AuthorizationApi,
    private auth: AuthenticateStore,
    private fb: FormBuilder
  ) {
    super();
  }

  get infoUpdatable() {
    return this.auth.snapshot.permission.permissions.includes('relationship/relationship:update');
  }

  get permissionUpdatable() {
    return this.auth.snapshot.permission.permissions.includes('relationship/permission:update');
  }

  ngOnInit() {
    this.staff = this.data.staff;
    this.staffForm.valueChanges.subscribe(_ => {
      this.dataEdited = true;
    })
    if(this.roleEtelecomDisplay){
      let etelecomRole = [...this.staff.roles];
      etelecomRole.sort();
      this.roleMap = etelecomRole.join(',');
    }
    this.staffForm.patchValue({
      full_name: this.staff.full_name,
      roles: this.roleMap,
    }, {emitEvent: false});
  }

  async updateStaff() {
    this.loading = true;
    try {
      if(this.dataEdited){
        const data = this.staffForm.getRawValue();
        let {full_name, type_update, roles} = data;
        if(!full_name){
          this.loading = false;
          return toastr.error('Chưa nhập tên nhân viên');
        }
        if(!this.roleEtelecomDisplay && !type_update){
          this.loading = false;
          return toastr.error('Chưa chọn loại cập nhật');
        }
        if(!roles){
          this.loading = false;
          return toastr.error('Chưa chọn vai trò');
        }
        if(type_update == TypeUpdate.RemoveAndAdd || (this.roleEtelecomDisplay && !type_update)){
          roles = roles.split(',');
        }
        if(type_update == TypeUpdate.Add){
          const roleList = this.staff.roles.concat(roles.split(','));
          roles = Array.from(new Set(roleList));
        }
        let user_id = this.staff.user_id;
        await this.authorizationAPI.updateRelationship(full_name, user_id);
        await this.authorizationAPI.updatePermission(roles, user_id);
        this.dialogRef.close(true);
        toastr.success('Chỉnh sửa thông tin nhân viên thành công');
      }
      else{
        this.closeDialog();
      }
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in creating Invitation', e);
    }
    this.loading = false;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }



  get roleEtelecomDisplay(){
    if(this.staff.roles.includes('m_admin'))
      return 'Admin';
    if(this.staff.roles.includes('salesman')) {
      if(this.staff.roles.includes('telecom_customerservice_management') && this.staff.roles.includes('staff_management'))
        return 'Quản lý';
      if (this.staff.roles.includes('telecom_customerservice') && this.staff.roles.length == 2)
        return 'Nhân viên';
    }
    return;
  }

  get roleNote(){
    let roles = this.staffForm.getRawValue().roles;
    if(roles == "salesman,telecom_customerservice")
      return 'eTelecom quy định vai trò <strong>Nhân viên</strong> sẽ bao gồm các quyền: <strong>Bán hàng</strong> và <strong>Chăm sóc khách hàng</strong>';
    if(roles == "salesman,staff_management,telecom_customerservice")
      return 'eTelecom quy định vai trò <strong>Quản lý</strong> sẽ bao gồm các quyền: <strong>Bán hàng</strong>, <strong>Chăm sóc khách hàng</strong> và <strong>Quản lý nhân viên</<strong>';
    return '';
  }
}
