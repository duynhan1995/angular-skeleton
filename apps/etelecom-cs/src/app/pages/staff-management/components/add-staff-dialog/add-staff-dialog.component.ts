import { Component, Inject, OnInit } from '@angular/core';
import { Invitation } from 'libs/models/Authorization';
import { AuthorizationAPI, AuthorizationApi } from '@etop/api';
import {MatDialogBaseComponent} from "@etop/web";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { AuthenticateStore } from '@etop/core';
import { MatDialogController } from '@etop/shared';
import { RelationshipService } from '@etop/state/relationship';

@Component({
  selector: 'etelecom-cs-add-staff-modal',
  templateUrl: './add-staff-dialog.component.html',
  styleUrls: ['./add-staff-dialog.component.scss']
})
export class AddStaffDialogComponent extends MatDialogBaseComponent implements OnInit {
  staff : {
    role: string,
    phone: string,
    full_name: string,
    password: string
  };

  loading = false;
  copied = false;
  roleList = [
    {name: 'Admin', value: 'm_admin'},
    {name: 'Nhân viên', value: 'salesman,telecom_customerservice'},
    {name: 'Quản lý', value: 'salesman,telecom_customerservice_management,staff_management'},
  ];

  constructor(
    private auth: AuthenticateStore,
    private matDialogController: MatDialogController,
    private dialogRef: MatDialogRef<AddStaffDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private relationshipService: RelationshipService,
    ) {
    super();
   }

  ngOnInit() {
    this.staff = {
      role: this.roleList[0].value,
      phone: '',
      full_name: '',
      password: Math.random().toString(36).substr(2, 8).toUpperCase()
    };
  }

  async createStaff() {
    this.loading = true;
    try {
      let { role, phone, full_name, password} = this.staff;
      if (!full_name) {
        this.loading = false;
        return toastr.error('Chưa nhập tên nhân viên!');
      }
      if (!phone) {
        this.loading = false;
        return toastr.error('Chưa nhập số điện thoại!');
      }
      if (!role) {
        this.loading = false;
        return toastr.error('Chưa chọn vai trò!');
      }
      if (!password) {
        this.loading = false;
        return toastr.error('Chưa nhập mật khẩu!');
      }
      const roles = role.split(',');
      const request: AuthorizationAPI.CreateAccountUserRequest  = {
        phone, full_name, password, roles
      };
      await this.relationshipService.createAccountUser(request);
      this.relationshipService.getRelationships();
      this.confirmSuccess();
    } catch(e) {
      toastr.error(e.message, 'Thêm nhân viên không thành công. Vui lòng bấm F5 để load lại trang và thử lại!');
      debug.error('ERROR in creating Staff', e);
    }
    this.loading = false;
  }

  confirmSuccess() {
    this.dialogRef.close();
    let content = `
    <div class="p-2">
      <div class="mb-4 p-2">
      Thêm nhân viên thành công
      </div>
      <div class="mb-2 p-2">
        <span class="mr-2">Tên:</span>
        <span><strong>${this.staff?.full_name}</strong></span>
      </div>
      <div class="mb-2 p-2">
        <span class="mr-2">Số điện thoại:</span>
        <span><strong>${this.staff?.phone}</strong></span>
      </div>
      <div class="mb-2 p-2">
        <span class="mr-2">Vai trò:</span>
        <span><strong>${this.roleList.find(role => role.value == this.staff?.role)?.name}</strong></span>
      </div>
      <div class="p-2">
        <span class="mr-2">Mật khẩu:</span>
        <span><strong>${this.staff?.password}</strong></span>
      </div>
    </div>
    `;

    const dialog = this.matDialogController.create({
      template: {
        title: 'Thêm nhân viên thành công',
        content
      },
      onConfirm: null,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }

}
