import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ExtensionQuery } from "@etop/state/shop/extension";
import { EtopTableComponent, MatDialogController } from "@etop/shared";
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { CreateExtensionWithSubcriptionComponent } from '../../../components/create-extension-with-subcription/create-extension-with-subcription.component';
import {HotlineQuery, HotlineService} from '@etop/state/shop/hotline';
import {ShopService} from '@etop/features';
import { DecimalPipe } from '@angular/common';
import { PaycardTurtorialModalComponent } from '../../../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {ActionOpt} from "../../../../../../core/src/components/header/components/action-button/action-button.interface";
import {takeUntil} from "rxjs/internal/operators/takeUntil";

@Component({
  selector: 'etelecom-cs-desktop-extension-list',
  templateUrl: './desktop-extension-list.component.html',
  styleUrls: ['./desktop-extension-list.component.scss']
})
export class DesktopExtensionListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('extensionTable', { static: true }) extensionTable: EtopTableComponent;
  extensions$ = this.extensionQuery.selectAll();
  loading$ = this.extensionQuery.selectLoading();
  hotlineRequested$ = this.hotlineQuery.select(state => state.hotlineRequested);

  constructor(
    private extensionQuery: ExtensionQuery,
    private headerController: HeaderControllerService,
    private matDialogController: MatDialogController,
    private hotlineQuery: HotlineQuery,
    private shopService: ShopService,
    private decimalPipe: DecimalPipe,
    private auth: AuthenticateStore,
    private hotlineService: HotlineService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.shopService.calcShopBalance("telecom").then(userCalcBalance => {
      const telecomBalance = userCalcBalance.telecom_balance;
      this.headerController.setContent(`
        <div class="text-telecom-balance">SỐ DƯ:</div>
        <div class="font-weight-bold telecom-balance">${this.decimalPipe.transform(telecomBalance)}đ</div>
    `);
    });
    let headerActions: ActionOpt[]= [
      {
        title: 'Nạp tiền',
        cssClass: 'btn btn-primary btn-outline',
        onClick: () => this.openPayCardTurtorialModal(),
      },
      {
        title: 'Thêm máy nhánh',
        cssClass: 'btn btn-primary',
        onClick: () => this.createExtensionWithSubcription(),
      }
    ];
    this.hotlineService.checkHotLineRequested();
    this.hotlineRequested$.pipe(takeUntil(this.destroy$)).subscribe(hotlineRequested => {
      if(!this.checkHaveHotlines() && !hotlineRequested){
        headerActions.unshift({
          title: 'Mua Hotline',
          cssClass: 'btn btn-primary btn-outline',
          onClick: () => this.hotlineService.hotlineRequest(),
          roles: ['owner'],
        });
      } else {
        if(headerActions.length > 2) {
          headerActions.shift();
        }
      }
    });
    this.headerController.setActions(headerActions);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearContent();
  }

  async createExtensionWithSubcription() {
    if(!this.checkHaveHotlines()){
      toastr.error('Không thể tạo máy nhánh do tổng đài của bạn chưa được gắn Hotline');
      return;
    }
    const dialog = this.matDialogController.create({
      component: CreateExtensionWithSubcriptionComponent,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  openPayCardTurtorialModal() {
    const dialog = this.matDialogController.create({
      component: PaycardTurtorialModalComponent,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  checkHaveHotlines() {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline?.connection_method == "direct");
    return !!hotlines?.length;
  }
}
