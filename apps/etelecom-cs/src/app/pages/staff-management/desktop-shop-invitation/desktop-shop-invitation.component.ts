import {Component, OnDestroy, OnInit} from '@angular/core';
import { BaseComponent } from '@etop/core';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { takeUntil } from 'rxjs/operators';
import { AddStaffDialogComponent } from '../components/add-staff-dialog/add-staff-dialog.component';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { InvitationQuery } from '@etop/state/invitation/invitation.query';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { PaycardTurtorialModalComponent } from '../../../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import { MatDialogController } from '@etop/shared';
import {AuthorizationQuery, AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: 'etelecom-cs-desktop-shop-invitation',
  templateUrl: './desktop-shop-invitation.component.html',
  styleUrls: ['./desktop-shop-invitation.component.scss'],
})
export class DesktopShopInvitationComponent
  extends BaseComponent
  implements OnInit, OnDestroy {
  loading$ = this.invitationQuery.selectLoading();
  shopInvitations$ = this.invitationQuery.selectAll();
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');

  private modal: any;
  constructor(
    private authorizationService: AuthorizationService,
    private authorizationQuery: AuthorizationQuery,
    private modalController: ModalController,
    private invitationService: InvitationService,
    private invitationQuery: InvitationQuery,
    private headerController: HeaderControllerService,
    private matDialogController: MatDialogController,
  ) {
    super();
  }

  ngOnInit() {
    this.headerController.setActions([
      {
        title: 'Nạp tiền',
        cssClass: 'btn btn-primary btn-outline',
        mobileHidden: true,
        onClick: () => this.openPayCardTurtorialModal(),
      },
      {
        title: 'Thêm nhân viên',
        cssClass: 'btn btn-primary',
        onClick: () => this.addStaff(),
        permissions: ['relationship/invitation:create']
      }
    ]);
    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe((cancelled) => {
        if (cancelled) {
          this.invitationService.getShopInvitations();
        }
      });
  }

  addStaff() {
    const dialog = this.matDialogController.create({
      component: AddStaffDialogComponent,
      afterClosedCb: (invitation) => {
        if (invitation) {
          invitation = this.authorizationService.invitationMap(invitation);
          this.invitationService.addInvitation(invitation);
        }
      }
    });
    dialog.open();
  }

  openPayCardTurtorialModal() {
    const dialog = this.matDialogController.create({
      component: PaycardTurtorialModalComponent,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }
}
