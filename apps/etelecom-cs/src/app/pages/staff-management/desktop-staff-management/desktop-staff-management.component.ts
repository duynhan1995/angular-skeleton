import {Component, OnDestroy, OnInit} from '@angular/core';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { AddStaffDialogComponent } from '../components/add-staff-dialog/add-staff-dialog.component';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { InvitationQuery } from '@etop/state/invitation/invitation.query';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { PaycardTurtorialModalComponent } from '../../../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import { MatDialogController } from '@etop/shared';
import {AuthorizationQuery, AuthorizationService} from "@etop/state/authorization";
import {CursorPaging, FilterOperator, FilterOptions, Filters} from "@etop/models";
import {AuthorizationAPI} from "@etop/api";
import {ExtensionQuery} from "@etop/state/shop/extension";

@Component({
  selector: 'etelecom-cs-desktop-staff-management',
  templateUrl: './desktop-staff-management.component.html',
  styleUrls: ['./desktop-staff-management.component.scss'],
})
export class DesktopStaffManagementComponent
  extends BaseComponent
  implements OnInit, OnDestroy {
  relationships$ = this.relationshipQuery.selectAll();
  loading$ = this.relationshipQuery.selectLoading();
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');
  filters: FilterOptions = [];
  perpage: number;

  constructor(
    private authorizationService: AuthorizationService,
    private authorizationQuery: AuthorizationQuery,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private invitationService: InvitationService,
    private invitationQuery: InvitationQuery,
    private extensionQuery: ExtensionQuery,
    private headerController: HeaderControllerService,
    private matDialogController: MatDialogController,
  ) {
    super();
  }

  ngOnInit() {
    this.headerController.setActions([
      {
        title: 'Nạp tiền',
        cssClass: 'btn btn-primary btn-outline',
        mobileHidden: true,
        onClick: () => this.openPayCardTurtorialModal(),
      },
      {
        title: 'Thêm nhân viên',
        cssClass: 'btn btn-primary',
        onClick: () => this.addStaff(),
        permissions: ['relationship/invitation:create']
      }
    ]);

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe((deleted) => {
        if (deleted) {
          this.relationshipService.getRelationships();
        }
      });
    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe((updated) => {
        if (updated) {
          this.relationshipService.getRelationships();
        }
      });

    this.filters = [
      {
        label: 'Tên',
        name: 'name',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains,
      },
      {
        label: 'Số điện thoại',
        name: 'phone',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains,
      },
      {
        label: 'Số máy nhánh',
        name: 'extension_number',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains,
      },
      {
        label: 'Vai trò',
        name: 'exact_roles',
        type: 'select',
        fixed: true,
        operator: FilterOperator.eq,
        options: [
          { name: 'Tất cả', value: '' },
          { name: 'Chủ công ty', value: 'owner'},
          { name: 'Admin', value: 'm_admin'},
          { name: 'Quản lý', value: ['telecom_customerservice_management', 'salesman', 'staff_management'] },
          { name: 'Nhân viên', value: ['telecom_customerservice', 'salesman'] },
        ]
      },
      {
        label: 'Gán máy nhánh',
        name: 'has_extension',
        type: 'select',
        fixed: true,
        operator: FilterOperator.eq,
        options: [
          { name: 'Tất cả', value: null },
          { name: 'Đã được gán', value: "true" },
          { name: 'Chưa được gán', value: "false" }
        ]
      },
      ]
  }

  filter(filters: Filters) {
    const _filter: AuthorizationAPI.RelationshipFilter = {};
    filters.forEach((f) => {
      if (f.value && f.name != "has_extension") {
        _filter[f.name] = f.value;
      }
      if (f.name == "has_extension") {
        _filter[f.name] = f.value == "true";
      }
    });
    const paging = {
      limit: this.perpage,
      after: '.',
    }
    this.relationshipService.setPaging(paging);
    this.relationshipService.setCurrentPage(1);
    this.relationshipService.setFilter(_filter);
    this.relationshipService.getRelationships().then();
  }

  addStaff() {
    const dialog = this.matDialogController.create({
      component: AddStaffDialogComponent,
      afterClosedCb: (invitation) => {
        if (invitation) {
          invitation = this.authorizationService.invitationMap(invitation);
          this.invitationService.addInvitation(invitation);
        }
      }
    });
    dialog.open();
  }

  async loadPage({perpage, page}) {
    this.perpage = perpage;

    let paging: CursorPaging;
    let currentPaging = this.relationshipQuery.currentUi.paging;
    let currentPage = this.relationshipQuery.currentUi.currentPage;

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    this.relationshipService.setPaging(paging);
    await this.relationshipService.getRelationships(true);
    this.relationshipService.setCurrentPage(page);
  }

  openPayCardTurtorialModal() {
    const dialog = this.matDialogController.create({
      component: PaycardTurtorialModalComponent,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }
}
