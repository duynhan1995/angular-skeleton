import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ExtensionQuery, ExtensionService} from "@etop/state/shop/extension";
import {EtopTableComponent, Paging} from "@etop/shared";

@Component({
  selector: 'etelecom-cs-mobile-extension-list',
  templateUrl: './mobile-extension-list.component.html',
  styleUrls: ['./mobile-extension-list.component.scss']
})
export class MobileExtensionListComponent implements OnInit {
  @ViewChild('extensionTable', { static: true }) extensionTable: EtopTableComponent;
  extensions$ = this.extensionQuery.selectAll();
  loading$ = this.extensionQuery.selectLoading();

  constructor(
    private extensionQuery: ExtensionQuery,
  ) { }

  ngOnInit(): void {
  }
}
