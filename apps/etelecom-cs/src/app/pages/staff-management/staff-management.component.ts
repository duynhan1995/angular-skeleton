import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { BaseComponent } from '@etop/core';
import {FilterOperator, Filters, RelationshipTab} from '@etop/models';
import {RelationshipQuery, RelationshipService} from '@etop/state/relationship';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { ExtensionService } from '@etop/state/shop/extension';
import { AddStaffDialogComponent } from './components/add-staff-dialog/add-staff-dialog.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { InvitationStore } from '@etop/state/invitation/invitation.store';
import {ShopService} from "@etop/features";
import {distinctUntilChanged, map} from "rxjs/operators";
import {HeaderControllerService} from "../../../../../core/src/components/header/header-controller.service";
import {DecimalPipe} from "@angular/common";
import { AuthorizationService } from '@etop/state/authorization/authorization.service';

@Component({
  selector: 'etelecom-cs-staff-management',
  template: `
      <etelecom-cs-desktop-staff-management *ngIf="(tab$ | async) == relationshipTab.staff" class="hide-768"></etelecom-cs-desktop-staff-management>
      <etelecom-cs-desktop-extension-list *ngIf="(tab$ | async) == relationshipTab.extension" class="hide-768"></etelecom-cs-desktop-extension-list>
      <etelecom-cs-desktop-shop-invitation *ngIf="(tab$ | async) == relationshipTab.shop_invitation" class="hide-768"></etelecom-cs-desktop-shop-invitation>
      <etelecom-cs-mobile-staff-management class="show-768"></etelecom-cs-mobile-staff-management>`,
  styleUrls: ['./staff-management.component.scss']
})
export class StaffManagementComponent extends BaseComponent implements OnInit, OnDestroy {
  private modal: any;
  relationshipTab = RelationshipTab;
  tab$ = this.relationshipQuery.select(state => state.ui.tab);
  telecomBalance: number;

  constructor(
    private invitationService: InvitationService,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private extensionService: ExtensionService,
    private cdr: ChangeDetectorRef,
    private modalController: ModalController,
    private authorizationService: AuthorizationService,
    private invitationStore: InvitationStore,
    private shopService: ShopService,
    private decimalPipe: DecimalPipe,
    private headerController: HeaderControllerService
  ) {
    super();
  }

  async ngOnInit() {
    this.setupTabs();
    this.relationshipService.changeHeaderTab(RelationshipTab.staff);
    this.relationshipService.setFilter(null);
    await this.extensionService.getExtensions(null, true);
    await this.getShopInvitations();
    this.shopService.calcShopBalance("telecom").then(userCalcBalance => {
      this.telecomBalance = userCalcBalance.telecom_balance;
      this.headerController.setMobileContent(`
        <div class="mobile-header-content">
            <span class="material-icons-outlined">account_balance_wallet</span>
            <span class="telecom-balance">${this.decimalPipe.transform(this.telecomBalance)}đ</span>
        </div>
    `);
    });
    this.cdr.detectChanges();
  }

  checkTab(tabName) {
    return this.tab$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }


  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Nhân viên',
          name: RelationshipTab.staff,
          active: false,
          onClick: () => {
            this.relationshipService.changeHeaderTab(RelationshipTab.staff);
            this.reloadRelationship().then();
          }
        },
        {
          title: 'Máy nhánh',
          name: RelationshipTab.extension,
          active: false,
          onClick: () => {
            this.relationshipService.changeHeaderTab(RelationshipTab.extension);
          }
        }
      ].map(tab => {
        this.checkTab(tab.name).subscribe(active => (tab.active = active));
        return tab;
      })
    );
  }

  async reloadRelationship() {
    const paging = {
      limit: 20,
      after: '.',
    };
    this.relationshipService.setPaging(paging);
    this.relationshipService.setCurrentPage(1);
    this.relationshipService.setFilter(null);
    await this.relationshipService.getRelationships(true);
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.headerController.clearMobileContent();
  }

  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const filters : Filters = [
        {
          name: "status",
          op: FilterOperator.eq,
          value: "Z"
        },
        {
          name: "expires_at",
          op: FilterOperator.gt,
          value: dateString
        }
      ]
      this.invitationService.setFilters(filters);
      this.invitationService.getShopInvitations();
    } catch(e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }

  addStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddStaffDialogComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show();
    this.modal.onDismiss().then(invitation => {
      this.modal = null;
      if (invitation) {
        invitation = this.authorizationService.invitationMap(invitation);
        this.invitationStore.add(invitation);
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }
}
