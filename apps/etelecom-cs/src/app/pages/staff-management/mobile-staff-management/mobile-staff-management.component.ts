import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { ModalController } from '../../../../../../core/src/components/modal-controller/modal-controller.service';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { Extension, Invitation, Relationship } from '@etop/models';
import { AddStaffDialogComponent } from '../components/add-staff-dialog/add-staff-dialog.component';
import { UpdateStaffDialogComponent } from '../components/update-staff-dialog/update-staff-dialog.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { InvitationQuery } from '@etop/state/invitation/invitation.query';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { InvitationStore } from '@etop/state/invitation/invitation.store';
import { ExtensionQuery, ExtensionService, ExtensionStore } from '@etop/state/shop/extension';
import { CreateHotlineDialogComponent } from '../components/create-hotline-dialog/create-hotline-dialog.component';
import { HotlineQuery } from '@etop/state/shop/hotline';
import { InitializationExtensionDialogComponent } from '../components/initialization-extension-dialog/initialization-extension-dialog.component';
import { MatDialogController } from '@etop/shared';
import { CreateExtensionWithSubcriptionComponent } from '../../../components/create-extension-with-subcription/create-extension-with-subcription.component';
import {AuthorizationQuery, AuthorizationService} from "@etop/state/authorization";
import {ExtensionApi} from "@etop/api/shop/extension.api";
import {HeaderControllerService} from "../../../../../../core/src/components/header/header-controller.service";
import {ExtendExtensionComponent} from "../components/extend-extension-dialog/extend-extension-dialog.component";

enum Segments {
  STAFF = 'staff',
  EXTENSION= 'extension',
  SHOP_INVITATION = 'shop_invitation',
}

@Component({
  selector: 'etelecom-cs-mobile-staff-management',
  templateUrl: './mobile-staff-management.component.html',
  styleUrls: ['./mobile-staff-management.component.scss']
})
export class MobileStaffManagementComponent extends BaseComponent implements OnInit, OnDestroy {
  segment: Segments;
  segmentName = Segments;

  shopInvitations$ = this.invitationQuery.selectAll();
  relationships$ = this.relationshipQuery.selectAll();
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');
  private modal: any;

  constructor(
    private authorizationQuery: AuthorizationQuery,
    private authorizationService: AuthorizationService,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private invitationQuery: InvitationQuery,
    private invitationService: InvitationService,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private invitationStore: InvitationStore,
    private cdr: ChangeDetectorRef,
    private extensionQuery: ExtensionQuery,
    private hotlineQuery: HotlineQuery,
    private extensionStore: ExtensionStore,
    private extensionService: ExtensionService,
    private matDialogController: MatDialogController,
    private extensionApi: ExtensionApi,
    private headerController: HeaderControllerService
  ) {
    super();
  }

  async ngOnInit() {
    this.relationships$.subscribe( _ => this.cdr.detectChanges());
    this.segment = Segments.STAFF;
    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(cancelled => {
        if (cancelled) { this.invitationService.getShopInvitations(); }
      });

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async (deleted) => {
        if (deleted) { this.relationshipService.getRelationships(); }
      });

    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(updated => {
        if (updated) { this.relationshipService.getRelationships(); }
      });
  }

  roleDisplay(staff: Relationship | Invitation) {
    if (staff.roles.includes('owner'))
      return 'Chủ công ty';
    if(staff.roles.includes('m_admin'))
      return 'Admin'
    if(staff.roles.includes('salesman')) {
      if(staff.roles.includes('telecom_customerservice_management') && staff.roles.includes('staff_management'))
        return 'Quản lý'
      if (staff.roles.includes('telecom_customerservice') && staff.roles.length == 2)
        return 'Nhân viên';
    }
    return 'Khác'
  }

  addStaff() {
    const dialog = this.matDialogController.create({
      component: AddStaffDialogComponent,
      afterClosedCb: (invitation) => {
        if (invitation) {
          invitation = this.authorizationService.invitationMap(invitation);
          this.invitationService.addInvitation(invitation);
        }
      }
    });
    dialog.open();
  }

  changeSegment(segment) {
    if(this.segment == segment) {
      return;
    }
    this.segment = segment;
    if(this.segment == 'extension') {
      this.headerController.setActions([
        {
          title: 'Thêm máy nhánh',
          cssClass: 'btn btn-primary',
          onClick: () => this.createExtension(),
        }
      ])
    } else {
      this.headerController.setActions([
        {
          title: 'Thêm nhân viên',
          cssClass: 'btn btn-primary',
          onClick: () => this.addStaff(),
        }
      ])
    }
  }

  updateStaff(staff: Relationship) {
    const dialog = this.matDialogController.create({
      component: UpdateStaffDialogComponent,
      config: {
        data: {
          staff: {
            ...staff,
            p_data: {
              ...staff,
            }
          }
        }
      },
      afterClosedCb: (updated) => {
        if(updated) {
          this.authorizationService.updateRelationship();
        }
      },
    });
    dialog.open();
  }

  confirmRemoveExtension(relationship: Relationship) {
    const dialog = this.matDialogController.create({
      template:{
        title: `Gỡ máy nhánh`,
        content: `<div>Bạn thực sự muốn gỡ máy nhánh của nhân viên "<strong>${relationship?.full_name}</strong>"?</div>`,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Gỡ',
      confirmStyle: 'btn-danger text-white',
      onConfirm: async () => {
        await this.removeExtension(relationship);
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  extendExtension(relationship: Relationship) {
    const extensions = this.extensionQuery.getAll();
    let currentExtension;
    if(extensions?.length){
      currentExtension = extensions.find(ex => ex.user_id == relationship.user_id);
    }
    this.extensionService.setActive(currentExtension.id);
    const dialog = this.matDialogController.create({
      component: ExtendExtensionComponent,
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async removeExtension(relationship: Relationship) {
    try {
      const extensionId = this.extensionQuery.getAll().find(extension => extension.user_id == relationship.user_id).id;
      await this.extensionApi.removeUserOfExtension(extensionId,relationship.user_id);
      this.extensionService.getExtensions(null, true);
      toastr.success('Gỡ máy nhánh thành công.');
    } catch (e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Extension', e);
    }
  }

  async removeStaff(staff: Relationship) {
    try {
      await this.authorizationService.removeUserFromAccount(staff.user_id);
      this.authorizationService.removeRelationship();
      toastr.success('Xoá nhân viên thành công.')
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing Staff', e);
    }
  }

  confirmRemoveStaff(staff:Relationship) {
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá nhân viên`,
      body: `
        <div>Bạn thực sự muốn xóa nhân viên "<strong>${staff?.full_name}</strong>"?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmCss: 'btn-danger text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removeStaff(staff);
        modal.close().then();
      }
    });
    modal.show().then();
  }

  async deleteInvitation(invitation: Invitation) {
    try {
      await this.authorizationService.deleteInvitation(invitation.token);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in deleting Invitation', e);
    }
  }

  isOwner(relationship) {
    return relationship.roles.indexOf('owner') != -1;
  }

  // Check employee have extension or not.
  hasUserExtension(staff): boolean {
    const extensions = this.extensionQuery.getAll();
    let currentExtension;
    if(extensions?.length){
      currentExtension = extensions.find(ex => ex.user_id == staff.user_id);
    }
    return !!currentExtension?.extension_number;
  }

  getExtensionNumber(staff) {
    const extensions = this.extensionQuery.getAll();
    let extension;
    if(extensions?.length){
      extension = extensions.find(ex => ex.user_id == staff.user_id);
    }
    return extension?.extension_number;
  }
  async openCreateExtension(staff) {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline.connection_method == "direct");
    const dialog = this.matDialogController.create({
      component: CreateHotlineDialogComponent,
      config: {
        data: {
          hotlines: hotlines,
          staff: staff,
        }
      },
      afterClosedCb: (extension) => {
        this.extensionStore.add(extension);
      },
    })
    dialog.open();
  }

  async initializationExtension(staff: Relationship) {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline?.connection_method == "direct");
    if(!hotlines?.length){
      toastr.error('Không thể tạo máy nhánh do tổng đài của bạn chưa được gắn Hotline');
      return;
    }
    const dialog = this.matDialogController.create({
      component: InitializationExtensionDialogComponent,
      config: {
        data: { staff },
      },

      afterClosedCb: async (result: {extension: Extension, staff: Relationship, new_extension: boolean}) => {
        if (!result) return;
        if (result?.extension?.id) {
          this.confirmAssignExtension(result?.extension, result?.staff);
        }
        if (result?.new_extension) {
          await this.createExtensionWithSubcription(staff);
        }
      }
    });

    dialog.open();
  }

  confirmAssignExtension(extension, staff) {
    const dialog = this.matDialogController.create({
      template: {
        title: 'Gán máy nhánh',
        content: `<div>Bạn có chắc muốn gán máy nhánh <strong>${extension?.extension_number}</strong> cho <strong>${staff?.full_name}</strong>?</div>`
      },
      onConfirm: async () => {
        await this.createExtensionWithUserId(extension?.id, staff?.user_id);
      },
      config: {
        data: { staff },
      },
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  async createExtensionWithSubcription(staff: Relationship) {
    const dialog = this.matDialogController.create({
      component: CreateExtensionWithSubcriptionComponent,
      config: {
        data: { staff },
      },
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  async createExtension() {
    if(!this.checkHaveHotlines()){
      toastr.error('Không thể tạo máy nhánh do tổng đài của bạn chưa được gắn Hotline');
      return;
    }
    const dialog = this.matDialogController.create({
      component: CreateExtensionWithSubcriptionComponent,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

  checkHaveHotlines() {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline?.connection_method == "direct");
    return !!hotlines?.length;
  }

  async createExtensionWithUserId(extension_id, user_id) {
    try {
      //if success to refresh list staff
      await this.extensionService.assignUserToExtension(extension_id, user_id);
      await this.extensionService.getExtensions(null, true);
      toastr.success('Gán máy nhánh thành công');
    } catch(e) {
      toastr.error(e)
    }
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }
}
