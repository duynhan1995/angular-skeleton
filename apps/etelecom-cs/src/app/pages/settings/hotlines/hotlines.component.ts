import { Component, OnInit } from '@angular/core';
import {HotlineQuery, HotlineService} from "@etop/state/shop/hotline";
import {Observable} from "rxjs";
import {Hotline} from "@etop/models";
import {JiraService} from "@etop/state/shop/jira";
import {TelegramService} from "@etop/features";
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'etelecom-cs-hotlines',
  templateUrl: './hotlines.component.html',
  styleUrls: ['./hotlines.component.scss']
})
export class HotlinesComponent implements OnInit {
  hotlineRequested$ = this.hotlineQuery.select(state => state.hotlineRequested);
  hotlines$: Observable<Hotline[]> = this.hotlineQuery.selectAll({filterBy: hotline => hotline.connection_method != 'builtin'});

  constructor(
    private hotlineQuery: HotlineQuery,
    private hotlineService: HotlineService,
  ) { }

  ngOnInit() {
    this.hotlineService.checkHotLineRequested();
  }

  hotlineRequest() {
    this.hotlineService.hotlineRequest();
  }

}
