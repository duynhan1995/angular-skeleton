import { Component, Input, OnInit } from '@angular/core';
import {Hotline} from "@etop/models";


@Component({
  selector: '[etelecom-cs-hotline-row]',
  templateUrl: './hotline-row.component.html',
  styleUrls: ['./hotline-row.component.scss']
})
export class HotlineRowComponent implements OnInit {

  @Input() hotline: Hotline;
  @Input() index: number;

  constructor(
  ) { }

  ngOnInit() {
  }

}
