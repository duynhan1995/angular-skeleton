import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Invitation } from 'libs/models/Authorization';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { TelegramService } from '@etop/features';
import { AuthorizationService } from '@etop/state/authorization/authorization.service';

@Component({
  selector: '[etelecom-cs-account-invitation-row]',
  templateUrl: './account-invitation-row.component.html',
  styleUrls: ['./account-invitation-row.component.scss']
})
export class AccountInvitationRowComponent implements OnInit {
  @Output() rejected = new EventEmitter();
  @Output() accepted = new EventEmitter();
  @Input() invitation = new Invitation({});

  accepting = false;
  rejecting = false;

  dropdownActions = [];

  constructor(
    private authorizationService: AuthorizationService,
    private shopCommonUsecase: CommonUsecase,
    private telegramService: TelegramService,
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Từ chối',
        cssClass: 'text-danger',
        disabled: this.accepting || this.rejecting,
        onClick: () => this.rejectInvitation()
      }
    ];
  }

  async acceptInvitation() {
    this.accepting = true;
    try {
      await this.authorizationService.acceptInvitation(this.invitation.token);
      this.telegramService.acceptInvitation(this.invitation);
      await this.shopCommonUsecase.updateSessionInfo(true);
      this.accepted.emit();
    } catch(e) {
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        toastr.error('Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!');
      }
      debug.error('ERROR in Accepting Invitation', e);
    }
    this.accepting = false;
  }

  async rejectInvitation() {
    this.rejecting = true;
    try {
      await this.authorizationService.rejectInvitation(this.invitation.token);
      this.rejected.emit();
    } catch(e) {
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        toastr.error('Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!');
      }
      debug.error('ERROR in Rejecting Invitation', e);
    }
    this.rejecting = false;
  }

  get roleDisplay() {
    if (this.invitation.roles.includes('telecom_customerservice') && this.invitation.roles.includes('salesman')) {
      if (this.invitation.roles.includes('staff_management') && this.invitation.roles.length == 3)
        return 'Quản lý';
      if (this.invitation.roles.length == 2)
        return 'Nhân viên';
    }
    return 'Khác'
  }
}
