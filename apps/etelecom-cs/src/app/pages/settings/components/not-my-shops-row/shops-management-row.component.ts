import { Component, Input, OnInit } from '@angular/core';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { AuthorizationApi } from '@etop/api';
import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: '[etelecom-cs-shops-management-row]',
  templateUrl: './shops-management-row.component.html',
  styleUrls: ['./shops-management-row.component.scss']
})
export class ShopsManagementRowComponent implements OnInit {
  @Input() account: Account;
  @Input() currentShop: ExtendedAccount;
  dropdownActions = [];

  get isMyShop() {
    const roles = this.account?.user_account?.permission?.roles;
    if (roles && roles.length) {
      return roles.indexOf('owner') != -1;
    }
    return true;
  }

  get isOneShop() {
    return this.auth.snapshot.accounts.length == 1
  }

  constructor(
    private gaService: GoogleAnalyticsService,
    private settingStore: SettingStore,
    private router: Router,
    public util: UtilService,
    private auth: AuthenticateStore,
  ) { }

  ngOnInit() {
    this.setupDropdownActions();
  }

  roleDisplay(roles) {
    if (roles.includes('owner'))
      return 'Chủ công ty';
    if (roles.includes('telecom_customerservice') && roles.includes('salesman')) {
      if (roles.includes('staff_management') && roles.length == 3)
        return 'Quản lý';
      if (roles.length == 2)
        return 'Nhân viên';
    }
    return 'Khác'
  }

  switchAccount(index) {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_SHOP_LIST_SETTING,
      USER_BEHAVIOR.LABEL_ACCESS_SHOP
    );
    window.open(`/s/${index}/settings/shop`, '_blank');
  }

  edit() {
    this.settingStore.changeMenu(SettingMenu.shop);
  }

  async openDeleteShopView() {
    const index = this.account?.url_slug || this.account?.id;
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_SHOP_LIST_SETTING,
      USER_BEHAVIOR.LABEL_ACCESS_SHOP
    );
    this.auth.selectAccount(index)
    await this.router.navigateByUrl(`/delete-shop`);
  }

  private setupDropdownActions() {
    this.dropdownActions =[
      {
        onClick: () => this.openDeleteShopView(),
        title: `Xóa công ty`,
        cssClass: 'text-danger',
      }
    ];
  }

}
