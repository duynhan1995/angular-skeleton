import {Component, OnInit} from '@angular/core';
import {AuthenticateStore} from '@etop/core';
import {User} from "@etop/models";
import {AppService} from '@etop/web/core/app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ChangePasswordModalComponent } from '../change-password-modal/change-password-modal.component';

@Component({
  selector: 'etelecom-cs-account-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  mobileMenus = [
    {
      mobileTitle: 'Công ty',
      title: 'Thông tin công ty',
      slug: 'shop',
      icon: 'account_circle'
    },
    {
      mobileTitle: 'Danh sách công ty',
      title: 'Danh sách công ty',
      slug: 'accounts',
      icon: 'list'
    },
    {
      mobileTitle: 'Danh bạ',
      title: 'Danh bạ',
      slug: 'contacts',
      icon: 'contact_phone',
      roles: ['owner', 'telecom_customerservice']
    },
    {
      mobileTitle: 'Hóa đơn và Giao dịch',
      title: 'Hóa đơn và Giao dịch',
      slug: 'invoice-transaction',
      icon: 'receipt',
      iconOutlined: true,
      roles: ['owner', 'telecom_customerservice']
    },
    {
      mobileTitle: 'Loại Ticket',
      title: 'Loại Ticket',
      slug: 'ticket-labels',
    },
    {
      mobileTitle: 'Hotline',
      title: 'Hotline',
      slug: 'hotlines',
    },
    {
      mobileTitle: 'Hỗ trợ',
      title: 'Hỗ trợ',
      slug: 'supports',
      icon: 'supports',
    },
  ];

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private appService: AppService,
    private router: Router,
    private modalController: ModalController
  ) {
  }

  get user(): User {
    return this.auth.snapshot.user;
  }

  get appName() {
    return this.appService.appID != 'etop.vn' ? 'eTelecom' : '';
  }

  onChangePassword() {
    this.openChangePasswordModal();
  }

  openChangePasswordModal() {
    this.modalController
      .create({
        component: ChangePasswordModalComponent
      })
      .show().then();
  }

  ngOnInit() {}

  signout() {
    this.auth.clear();
    this.router.navigateByUrl('/login');
  }

  async changeMenu(menu) {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    if(menu == "supports") {
      return await this.router.navigateByUrl(`/s/${slug}/supports`);
    }
    if(menu == "contacts") {
      return await this.router.navigateByUrl(`/s/${slug}/contacts`);
    }
    if(menu == "invoice-transaction") {
      return await this.router.navigateByUrl(`/s/${slug}/invoice-transaction`);
    }
    await this.router.navigateByUrl(`/s/${slug}/settings/${menu}`);
  }
}
