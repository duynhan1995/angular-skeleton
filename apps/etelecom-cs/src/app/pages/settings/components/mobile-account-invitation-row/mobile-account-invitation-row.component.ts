import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Invitation } from '@etop/models';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { TelegramService } from '@etop/features';
import { AuthorizationService } from '@etop/state/authorization/authorization.service';

@Component({
  selector: 'etelecom-cs-mobile-account-invitation-row',
  templateUrl: './mobile-account-invitation-row.component.html',
  styleUrls: ['./mobile-account-invitation-row.component.scss']
})
export class MobileAccountInvitationRowComponent implements OnInit {
  @Output() rejected = new EventEmitter();
  @Output() accepted = new EventEmitter();
  @Input() invitation = new Invitation({});

  accepting = false;
  rejecting = false;

  dropdownActions = [];
  constructor(
    private authorizationService: AuthorizationService,
    private shopCommonUsecase: CommonUsecase,
    private telegramService: TelegramService,
  ) { }

  ngOnInit(): void {
  }

  async acceptInvitation() {
    this.accepting = true;
    try {
      await this.authorizationService.acceptInvitation(this.invitation.token);
      this.telegramService.acceptInvitation(this.invitation);
      await this.shopCommonUsecase.updateSessionInfo(true);
      this.accepted.emit();
    } catch(e) {
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        toastr.error('Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!');
      }
      debug.error('ERROR in Accepting Invitation', e);
    }
    this.accepting = false;
  }

  async rejectInvitation() {
    this.rejecting = true;
    try {
      await this.authorizationService.rejectInvitation(this.invitation.token);
      this.rejected.emit();
    } catch(e) {
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        toastr.error('Có lỗi xảy ra. Vui lòng bấm F5 để load lại trang và thử lại!');
      }
      debug.error('ERROR in Rejecting Invitation', e);
    }
    this.rejecting = false;
  }

}
