import { Component, OnInit } from '@angular/core';
import { CreateAndUpdateTicketLabelComponent } from '../../../components/create-and-update-ticket-label/create-and-update-ticket-label.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticateStore } from '@etop/core';
import {TicketLabelQuery, TicketLabelService} from "@etop/state/shop/ticket-labels";
import {TicketType} from "@etop/models";

@Component({
  selector: 'etelecom-cs-ticket-labels',
  templateUrl: './ticket-labels.component.html',
  styleUrls: ['./ticket-labels.component.scss']
})
export class TicketLabelsComponent implements OnInit {
  listTicketLabel$ = this.ticketLabelQuery.selectAll();

  constructor(
    public dialog: MatDialog,
    private auth: AuthenticateStore,
    private ticketLabelService: TicketLabelService,
    private ticketLabelQuery: TicketLabelQuery,
  ) { }

 async ngOnInit() {
    const filter = {type: TicketType.internal};
    this.ticketLabelService.setFilter(filter);
    this.ticketLabelService.getTicketLabels();
  }


  createTicketLabel() {
     this.dialog.open(CreateAndUpdateTicketLabelComponent, {
       hasBackdrop: true,
       panelClass: "dialog-responsive",
       minWidth: '500px',
       position: {
         top: '8%'
      },
    });
  }


}
