import { Component, Input, OnInit } from '@angular/core';
import { DialogControllerService } from '../../../../../../../../core/src/components/modal-controller/dialog-controller.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateAndUpdateTicketLabelComponent } from '../../../../../components/create-and-update-ticket-label/create-and-update-ticket-label.component';
import {TicketLabelService} from "@etop/state/shop/ticket-labels";
import {TicketLabel} from "@etop/models";

@Component({
  selector: 'etelecom-cs-ticket-label-row',
  templateUrl: './ticket-label-row.component.html',
  styleUrls: ['./ticket-label-row.component.scss']
})
export class TicketLabelRowComponent implements OnInit {
  @Input() ticketLabel: TicketLabel;


  dropdownActions = [];


  constructor(
    private dialogController: DialogControllerService,
    public dialog: MatDialog,
    private ticketLabelService: TicketLabelService,
  ) { }

  ngOnInit() {
    this.dropdownActions = [
      {
        title: 'Chỉnh sửa loại Ticket',
        cssClass: 'text',
        onClick: () => this.updateTicketLabel()
      },
      {
        title: 'Xóa loại Ticket',
        cssClass: 'text-danger',
        onClick: () => this.deleteTicketLabel()
      },
    ];
  }

  async deleteTicketLabel(){
    const dialog = this.dialogController.createConfirmDialog({
      title: `Xóa loại Ticket`,
      body: `
        <div>Bạn có chắc muốn xóa loại Ticket <strong></strong>?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.ticketLabelService.deleteShopTicketLabel(this.ticketLabel.id);
          toastr.success(`Xóa loại Ticket thành công.`);
          dialog.close();
        } catch (e) {
          toastr.error(`Xóa loại Ticket không thành công.`, e.message || e.msg);
        }
      }
    });
    dialog.show().then();
  }

  updateTicketLabel(){
    this.dialog.open(CreateAndUpdateTicketLabelComponent, {
      panelClass: "dialog-responsive",
      hasBackdrop: true,
      minWidth: '500px',
      position: {
        top: '8%'
      },
      data: {ticketLabel: this.ticketLabel},
    });
  }

}
