import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from 'apps/shared/src/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {SettingsComponent} from 'apps/etelecom-cs/src/app/pages/settings/settings.component';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {AuthenticateModule} from '@etop/core';
import {UserInfoComponent} from 'apps/etelecom-cs/src/app/pages/settings/components/user-info/user-info.component';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EmptyPageModule, EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {AccountsInfoComponent} from './components/accounts-info/accounts-info.component';
import {MyShopsManagementComponent} from './components/my-shops-management/my-shops-management.component';
import {NotMyShopsManagamentComponent} from './components/not-my-shops-managament/not-my-shops-managament.component';
import {ShopsManagementRowComponent} from './components/not-my-shops-row/shops-management-row.component';
import {AccountInvitationRowComponent} from './components/account-invitation-row/account-invitation-row.component';
import {GoogleAnalyticsService} from 'apps/core/src/services/google-analytics.service';
import {MobileAccountInvitationRowComponent} from './components/mobile-account-invitation-row/mobile-account-invitation-row.component';
import {ChangePasswordModalComponent} from './components/change-password-modal/change-password-modal.component';
import {CreateShopModalComponent} from './components/create-shop-modal/create-shop-modal.component';
import {FromAddressComponent} from './from-address/from-address.component';
import {ShopInfoComponent} from './components/shop-info/shop-info.component';
import {ConnectCarrierUsingPhoneModalComponent} from './connect-carrier-using-phone-modal/connect-carrier-using-phone-modal.component';
import {TicketLabelsComponent} from "./ticket-labels/ticket-labels.component";
import {TicketLabelRowComponent} from "./ticket-labels/component/ticket-label-row/ticket-label-row.component";
import {CreateAndUpdateTicketLabelModule} from "../../components/create-and-update-ticket-label/create-and-update-ticket-label.module";
import {HotlinesComponent} from "./hotlines/hotlines.component";
import {HotlineRowComponent} from "./hotlines/component/hotline-row/hotline-row.component";

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'user',
        component: UserInfoComponent
      },
      {
        path: 'accounts',
        component: AccountsInfoComponent
      },
      {
        path: 'shop',
        component: ShopInfoComponent,
      },
      {
        path: 'ticket-labels',
        component: TicketLabelsComponent,
      },
      {
        path: 'hotlines',
        component: HotlinesComponent,
      },
      {
        path: '**', redirectTo: 'user'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ModalControllerModule,
    AuthenticateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    NgbModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    MaterialModule,
    CreateAndUpdateTicketLabelModule,
    EmptyPageModule,
  ],
  exports: [],
  declarations: [
    ChangePasswordModalComponent,
    SettingsComponent,
    UserInfoComponent,
    ShopInfoComponent,
    MyShopsManagementComponent,
    NotMyShopsManagamentComponent,
    ShopsManagementRowComponent,
    AccountsInfoComponent,
    AccountInvitationRowComponent,
    MobileAccountInvitationRowComponent,
    CreateShopModalComponent,
    FromAddressComponent,
    ConnectCarrierUsingPhoneModalComponent,
    TicketLabelsComponent,
    TicketLabelRowComponent,
    HotlinesComponent,
    HotlineRowComponent
  ],
  providers: [
    GoogleAnalyticsService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {
}
