import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthenticateStore} from "@etop/core";
import {TicketsInternalListComponent} from './tickets-internal-list/tickets-internal-list.component';
import {HeaderControllerService} from "../../../../../core/src/components/header/header-controller.service";
import {TicketTab} from "@etop/models";
import {TicketQuery, TicketService} from "@etop/state/shop/ticket";
import {distinctUntilChanged, map} from "rxjs/operators";
import {TicketLabelService} from "@etop/state/shop/ticket-labels";

@Component({
  selector: 'etelecom-cs-tickets-internal',
  template: `
    <etop-not-permission *ngIf="noPermission"></etop-not-permission>
    <etelecom-cs-tickets-internal-list
      *ePermissions="['shop/shop_ticket_comment:view']"
      #ticketList>
    </etelecom-cs-tickets-internal-list>`
})
export class TicketsInternalComponent implements OnInit, OnDestroy {
  @ViewChild('ticketList', {static: true}) ticketList: TicketsInternalListComponent;
  tab$ = this.ticketQuery.select((state) => state.ui.tab);

  constructor(
    private auth: AuthenticateStore,
    private headerController: HeaderControllerService,
    private ticketService: TicketService,
    private ticketLabelService: TicketLabelService,
    private ticketQuery: TicketQuery,
  ) {
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('shop/shop_ticket_comment:view');
  }

  ngOnInit() {
    this.setupTabs();
    this.headerController.setActions([
      {
        title: 'Tạo phiếu yêu cầu',
        cssClass: 'btn btn-primary',
        onClick: () => this.ticketList.createTicket(),
      },
    ]);
  }

  checkTab(tabName) {
    return this.tab$.pipe(
      map((tab) => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  async prepareData(tab: TicketTab) {
    if(tab == TicketTab.general) {
      this.ticketService.setFilter({assigned_user_id: null});
    } else {
      this.ticketService.setFilter({assigned_user_id: [this.auth.snapshot.user.id]});
    }
    await this.ticketService.getTickets();
  }

  setupTabs() {
    const tabs = [
      {
        title: 'Tất cả',
        name: TicketTab.general,
        active: false,
        onClick: async () => {
          this.ticketService.changeHeaderTab(TicketTab.general);
          await this.prepareData(TicketTab.general);
        },
      },
      {
        title: 'Được phân công',
        name: TicketTab.assigned,
        active: false,
        onClick: async () => {
          this.ticketService.changeHeaderTab(TicketTab.assigned);
          await this.prepareData(TicketTab.assigned).then();
        },
      }
    ]
    this.headerController.setTabs(tabs.map((tab) => {
        this.checkTab(tab.name).subscribe((active) => (tab.active = active));
        return tab;
      })
    );
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

}
