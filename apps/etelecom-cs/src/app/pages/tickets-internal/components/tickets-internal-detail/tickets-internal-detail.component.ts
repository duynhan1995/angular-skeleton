import { Component, OnInit } from '@angular/core';
import { TicketQuery, TicketService } from '@etop/state/shop/ticket';
import { Contact, Relationship, Ticket, TicketState } from '@etop/models';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import { Observable } from 'rxjs';
import {RelationshipQuery, RelationshipService} from '@etop/state/relationship';
import { ContactQuery } from '@etop/state/shop/contact';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';

@Component({
  selector: 'etelecom-cs-tickets-internal-detail',
  templateUrl: './tickets-internal-detail.component.html',
  styleUrls: ['./tickets-internal-detail.component.scss'],
})
export class TicketsInternalDetailComponent
  extends BaseComponent
  implements OnInit {
  activeTicket$: Observable<Ticket> = this.ticketQuery.selectActive();

  activeTicketCommentsLoading$ = this.ticketQuery.select(
    (state) => state.ui.getTicketCommentsLoading
  );
  activeTicketComments$ = this.ticketQuery.selectActive(
    (ticket) => ticket.comments
  );
  relationships$ = this.relationshipQuery.selectAll();

  loading = false;
  relationships: Relationship[] = [];
  contact: Contact;
  ticketState = {
    new : TicketState.new,
    processing: TicketState.processing,
    ignore: TicketState.ignore,
    success: TicketState.success,
    fail: TicketState.fail,
  };

  get canChangeToNew() {
    const activeTicket = this.ticketQuery.getActive();
    return activeTicket.state == TicketState.success ||
    activeTicket.state == TicketState.fail ||
    activeTicket.state == TicketState.ignore;
  }

  get canChangeToProcessing() {
    const activeTicket = this.ticketQuery.getActive();
    return activeTicket.state == TicketState.new ||
    activeTicket.state == TicketState.received;
  }

  get canComplete() {
    const activeTicket = this.ticketQuery.getActive();
    return activeTicket.state == TicketState.new ||
    activeTicket.state == TicketState.received ||
    activeTicket.state == TicketState.processing;
  }

  get canChangeState() {
    const activeTicket = this.ticketQuery.getActive();
    if(this.auth.snapshot.permission.roles.includes("telecom_customerservice")){
      return activeTicket.assigned_user_ids[0] == this.auth.snapshot.user.id;
    }
    return true;
  }

  get isRoleCS() {
    return this.auth.snapshot.permission.roles.includes("telecom_customerservice");
  }

  get currentRelationship() {
    const relationships = this.relationshipQuery.getAll();
    return relationships.find(relationship => relationship.user_id == this.auth.snapshot.user.id);
  }

  constructor(
    private ticketQuery: TicketQuery,
    private relationshipQuery: RelationshipQuery,
    private relationshipService: RelationshipService,
    private ticketService: TicketService,
    private contactQuery: ContactQuery,
    private router: Router,
    private util: UtilService,
    private dialogController: DialogControllerService,
    private auth: AuthenticateStore,
  ) {
    super();
  }

  async ngOnInit() {
    this.activeTicket$.pipe(takeUntil(this.destroy$)).subscribe((ticket) => {
      this.contact = this.contactQuery.findContactById(ticket?.ref_id);
    });
    if(!this.relationshipQuery.getAll().length){
      await this.relationshipService.getRelationships();
    }
  }

  async assignTicket(user: Relationship) {
    const ticket = this.ticketQuery.getActive();
    let _ticket: Ticket;
    try {
      if (user) {
        _ticket = await this.ticketService.assignTicket(
          [user?.user_id],
          ticket.id
        );
      } else {
        _ticket = await this.ticketService.assignTicket(
          [],
          ticket.id
        );
      }

      this.ticketService.updateAssignTicket(_ticket);
      if (user) {
        toastr.success(
          `Phân công phiếu yêu cầu cho ${user?.full_name} thành công`
        );
      } else {
        toastr.success(`Hủy phân công phiếu yêu cầu thành công.`);
      }
    } catch (e) {
      toastr.error(e?.message);
    }
  }

  async updateState(state: TicketState) {
    try {
      const _ticket = await this._updateTicket(state);
      this.ticketService.updateStateTicket(_ticket);
      this.ticketService.getTickets(false);
      toastr.success('Chuyển trạng thái thành công');
    } catch (e) {
      toastr.error(e?.message);
    }
  }

  async _updateTicket(state: TicketState) {
    let _ticket;
    const ticket = this.ticketQuery.getActive();
    switch (state) {
      case TicketState.new: {
        _ticket = await this.ticketService.reopenTicket(null, ticket.id);
        return _ticket;
      }
      case TicketState.processing: {
        _ticket = await this.ticketService.confirmTicket(null, ticket.id);
        return _ticket;
      }
      case TicketState.success: {
        _ticket = await this.ticketService.closeTicket(
          state,
          ticket.id
        );
        this.ticketService.updateCloseTicket(_ticket);
        return _ticket;
      }
      case TicketState.fail: {
        _ticket = await this.ticketService.closeTicket(
          state,
          ticket.id
        );
        this.ticketService.updateCloseTicket(_ticket);
        return _ticket;
      }
      case TicketState.ignore: {
        _ticket = await this.ticketService.closeTicket(
          state,
          ticket.id
        );
        this.ticketService.updateCloseTicket(_ticket);
        return _ticket;
      }
    }
  }

  contactDetail(contact: Contact) {
    this.router.navigateByUrl(
      `/s/${this.util.getSlug()}/contacts?id=${contact.id}`
    );
  }

  unAssignTicket() {
    const user = this.ticketQuery.getActive().assignedUsers[0];
    const dialog = this.dialogController.createConfirmDialog({
      title: `Hủy phân công`,
      body: `
        <div>Bạn có chắc muốn hủy phân công <strong>${user?.full_name}</strong> khỏi ticket này?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: () => {
        this.assignTicket(null);
        dialog.close().then();
      }
    });
    dialog.show().then();
  }
}
