import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from '@etop/core';
import {Ticket, TicketRefType, TicketType} from '@etop/models/Ticket';
import {TicketService} from '@etop/state/shop/ticket';
import {TicketLabelQuery, TicketLabelService} from '@etop/state/shop/ticket-labels';
import {ShopTicketAPI} from "@etop/api";
import {Contact} from '@etop/models';
import {ContactQuery, ContactService} from '@etop/state/shop/contact';

@Component({
  selector: 'etelecom-cs-tickets-internal-create',
  templateUrl: './tickets-internal-create.component.html',
  styleUrls: ['./tickets-internal-create.component.scss']
})
export class CreateTicketAdminComponent extends BaseComponent implements OnInit {
  @Output() createTicket = new EventEmitter();
  @Input() contactId;

  ticket = new Ticket();
  labels = [];
  selectedLabel: any;
  loading = false;
  contact = new Contact({});

  constructor(
    private ticketService: TicketService,
    private ticketLabelService: TicketLabelService,
    private ticketLabelQuery: TicketLabelQuery,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
  ) {
    super();
  }

  async ngOnInit() {
    this.loading = true;
    const filter: ShopTicketAPI.GetTicketLabelsFilter = {
      type: TicketType.internal,
    }
    this.ticketLabelService.setFilter(filter);
    await this.ticketLabelService.getTicketLabels();
    await this.contactService.getContacts();
    this.labels = this.ticketLabelQuery.getValue().ticketProductLabels.map(t => {
      const label = {
        value: t.id,
        name: t.name
      }
      return label
    });
    if (this.contactId) {
      this.contact = {...this.contactQuery.findContactById(this.contactId)};
    }
    this.loading = false;
  }

  validateColor(color: string) {
    return TicketLabelService.validateColor(color);
  }

  resetData() {
    this.contact = new Contact({});
    this.selectedLabel = null;
    this.contactService.setFilter({});
  }

  async create() {
    this.loading = true;
    try {
      if ((this.contact.full_name && !this.contact.phone) || (!this.contact.full_name && this.contact.phone)) {
        this.loading = false;
        return toastr.error('Họ tên và số điện thoại phải để trống hoặc điền đầy đủ!');
      }
      if (!this.ticket.title) {
        this.loading = false;
        return toastr.error('Vui lòng nhập tiêu đề!');
      }
      if (!this.ticket.description) {
        this.loading = false;
        return toastr.error('Vui lòng nhập nội dung!');
      }
      let data: any = {
        source: null,
        label_ids: this.selectedLabel ? [this.selectedLabel] : [],
        title: this.ticket.title,
        description: this.ticket.description,
        type: TicketType.internal
      }
      if (this.contact.full_name && this.contact.phone) {
        let contacts = await this.ticketService.getContactsByPhone(this.contact.phone);
        if (contacts.length) {
          let contactCheck = contacts.filter(c => {
            return this.contact.full_name == c.full_name && this.contact.phone == c.phone
          });
          if (contactCheck.length) {
            this.contact = contactCheck[0];
          } else {
            this.contact = contacts[0];
          }
        } else {
          let contact = await this.contactService.createContacts(this.contact);
          this.contact = contact;
        }
      }
      data.ref_type = TicketRefType.contact;
      data.ref_id = this.contact.id;
      await this.ticketService.createEtelecomAdminTicket(data);
      this.resetData();
      toastr.success('Tạo phiếu yêu cầu mới thành công');
      this.createTicket.emit();
      this.ticket = new Ticket();
    } catch (e) {
      debug.log(e)
      toastr.error('Tạo phiếu yêu cầu thất bại!', e.message);
    }

    this.loading = false;
  }

}
