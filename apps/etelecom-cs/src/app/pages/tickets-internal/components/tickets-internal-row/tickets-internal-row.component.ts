import {Component, Input, OnInit} from '@angular/core';
import {Ticket} from "@etop/models";

@Component({
  selector: '[etelecom-cs-tickets-internal-row]',
  templateUrl: './tickets-internal-row.component.html',
  styleUrls: ['./tickets-internal-row.component.scss']
})
export class TicketsInternalRowComponent implements OnInit {
  @Input() ticket: Ticket;
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }
}
