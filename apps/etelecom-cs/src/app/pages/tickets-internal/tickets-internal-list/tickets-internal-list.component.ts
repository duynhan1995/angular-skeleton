import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild,} from '@angular/core';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {PageBaseComponent} from '@etop/web/core/base/page.base-component';
import {EtopTableComponent, SideSliderComponent} from '@etop/shared';
import {
  FilterOperator,
  FilterOptions,
  Filters,
  Relationship,
  Shop,
  Ticket,
  TicketLabel,
  TicketType
} from '@etop/models';
import {takeUntil} from 'rxjs/operators';
import {TicketQuery, TicketService} from '@etop/state/shop/ticket';
import {ShopTicketAPI} from '@etop/api';
import {ActivatedRoute, Router} from '@angular/router';
import {TicketLabelQuery, TicketLabelService} from '@etop/state/shop/ticket-labels';
import {ContactQuery, ContactService} from "@etop/state/shop/contact";
import {RelationshipQuery, RelationshipService} from "@etop/state/relationship";


@Component({
  selector: 'etelecom-cs-tickets-internal-list',
  templateUrl: './tickets-internal-list.component.html',
  styleUrls: ['./tickets-internal-list.component.scss'],
})
export class TicketsInternalListComponent extends PageBaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('ticketTable', {static: true}) ticketTable: EtopTableComponent;
  @ViewChild('ticketSlider', {static: true})
  ticketSlider: SideSliderComponent;
  ticketLabels: TicketLabel[] = [];
  staffs: Relationship[] = [];
  createdBy: string;
  assignedBy: string;
  closedBy: string;
  tab$ = this.ticketQuery.select((state) => state.ui.tab);

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  createTicketMode: boolean;

  filters: FilterOptions = [
    {
      label: 'Mã ticket',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Trạng thái',
      name: 'state',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        {name: 'Tất cả', value: null},
        {name: "Mới", value: 'new'},
        {name: 'Đã tiếp nhận', value: 'received'},
        {name: 'Đang xử lý', value: 'processing'},
        {name: 'Xử lý thành công', value: 'success'},
        {name: 'Xử lý thất bại', value: 'fail'},
        {name: 'Từ chối xử lý', value: 'ignore'},
      ]
    },
  ];

  finishPrepareData$ = this.ticketQuery.select(
    (state) => state.ui.finishPrepareData
  );

  ticketsLoading$ = this.ticketQuery.selectLoading();
  ticketsList$ = this.ticketQuery.selectAll();
  activeTicket$ = this.ticketQuery.selectActive();

  isLastPage$ = this.ticketQuery.select((state) => state.ui.isLastPage);

  emptyTitle = 'Chưa có yêu cầu hỗ trợ nào.';
  contactId: string;

  isInAllView = true;
  showFilterBar = true;

  subjectLabels$ = this.labelQuery.select(state => state.ticketSubjectLabels
    .filter(label => {
      const filterProductLabelsIds = this.ticketQuery.getValue().ui.filterProductLabelsIds;
      return !filterProductLabelsIds.length || filterProductLabelsIds.includes(label.parent_id);
    })
  );
  subjectLabelId: string = null;

  detailLabels$ = this.labelQuery.select(state => state.ticketDetailLabels
    .filter(label => {
      const filterSubjectLabelsIds = this.ticketQuery.getValue().ui.filterSubjectLabelsIds;

      if (!filterSubjectLabelsIds.length) {
        const filterProductLabelsIds = this.ticketQuery.getValue().ui.filterProductLabelsIds;
        const _subjectLabels = this.labelQuery.getValue().ticketSubjectLabels.filter(_label => {
          return !filterProductLabelsIds.length || filterProductLabelsIds.includes(_label.parent_id);
        });
        const _subjectLabelsIds = _subjectLabels.map(l => l.id);
        return _subjectLabelsIds.includes(label.parent_id);
      }
      return filterSubjectLabelsIds.includes(label.parent_id);
    })
  );
  detailLabelId: string = null;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private ticketQuery: TicketQuery,
    private ticketService: TicketService,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private ticketLabelService: TicketLabelService,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private route: ActivatedRoute,
    private router: Router,
    private labelQuery: TicketLabelQuery,
  ) {
    super();
  }

  get showPaging() {
    return !this.ticketTable.liteMode && !this.ticketTable.loading;
  }

  get sliderTitle() {
    return this.createTicketMode ? 'Tạo phiếu yêu cầu' : 'Chi tiết phiếu yêu cầu';
  }

  private _onSelectModeChanged(value) {
    this.ticketTable.toggleLiteMode(value);
    this.ticketSlider.toggleLiteMode(value);
  }

  staffDisplayMap = (staff: Relationship) => staff && `${this.roleDisplay(staff)} - ${staff.full_name}` || null;
  staffValueMap = (staff: Relationship) => staff && staff.id || null;

  async ngOnInit() {
    await this.prepareData();
    this.ticketLabels = this.labelQuery.getAll();
    let optionsFilterByTicketLabels = [
      {name: 'Tất cả', value: null},
    ];
    this.ticketLabels.forEach(ticketLabel => {
      optionsFilterByTicketLabels.push({
        name: ticketLabel.name,
        value: ticketLabel.id,
      })
    });
    this.filters.push({
      label: 'Loại',
      name: 'label_ids',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: optionsFilterByTicketLabels,
    })
    this.ticketsLoading$.pipe(takeUntil(this.destroy$)).subscribe((loading) => {
      this.ticketTable.loading = loading;
      this.changeDetector.detectChanges();
      if (loading) {
        this.resetState();
      }
    });

    this.isLastPage$.pipe(takeUntil(this.destroy$)).subscribe((isLastPage) => {
      if (isLastPage) {
        this.ticketTable.toggleNextDisabled(true);
        this.ticketTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả các yêu cầu hỗ trợ.');
      }
    });

    this.headerController.setActions([
      {
        title: 'Tạo phiếu yêu cầu',
        cssClass: 'btn btn-primary',
        onClick: () => this.createTicket(),
      },
    ]);
  }

  async prepareData() {
    this.ticketLabelService.setFilter({type: TicketType.internal});
    await this.ticketLabelService.getTicketLabels();
    await this.ticketService.prepareData();
    this.ticketService.setFilter({});
  }

  async onStaffSearching(searchText: string) {
    this.relationshipService.setFilter({name: searchText});
    await this.relationshipService.getRelationships(false);
    this.staffs = this.relationshipQuery.getAll();
  }

  onCreatedBySelect() {
    this.ticketService.setFilter({created_by: this.createdBy});
    this.ticketService.getTickets().then();
  }

  onClosedBySelect() {
    this.ticketService.setFilter({closed_by: this.closedBy});
    this.ticketService.getTickets().then();
  }

  onAssignedBySelect() {
    this.ticketService.setFilter({assigned_user_id: this.assignedBy ? [this.assignedBy] : null});
    this.ticketService.getTickets().then();
  }

  roleDisplay(staff: Relationship) {
    if (staff.roles.includes('owner'))
      return 'Chủ công ty';
    if (staff.roles.includes('telecom_customerservice') && staff.roles.includes('salesman')) {
      if (staff.roles.includes('staff_management') && staff.roles.length == 3)
        return 'Quản lý';
      if (staff.roles.length == 2)
        if (staff.roles.includes('m_admin'))
          return 'Admin'
      if (staff.roles.includes('salesman')) {
        if (staff.roles.includes('telecom_customerservice_management') && staff.roles.includes('staff_management'))
          return 'Quản lý'
        if (staff.roles.includes('telecom_customerservice') && staff.roles.length == 2)
          return 'Nhân viên';
      }
      return 'Khác'
    }
  }

  async ngAfterViewInit() {
    const {queryParams} = this.route.snapshot;
    let id = queryParams?.id;
    if (id) {
      let ticket = await this.ticketService.getTicket(id);
      if (ticket) {
        setTimeout(async _ => {
          await this.detail(ticket);
        }, 1000)
      }
    }
    let contact_id = queryParams?.contact_id;
    if (contact_id) {
      setTimeout(async _ => {
        this.contactId = contact_id;
        await this.createTicket();
      }, 1000);
    }
    this.router.navigate([], {
      queryParams: {
        'id': null,
        'contact_id': null
      },
      queryParamsHandling: 'merge'
    })
  }

  filterTicketsBySubjectLabels() {
    if (!this.subjectLabelId) {
      this.ticketService.setFilterSubjectLabels([]);
    } else {
      this.ticketService.setFilterSubjectLabels([this.subjectLabelId]);
      this.ticketService.setFilterDetailLabels([]);
    }
    this.detailLabelId = null;
    this.ticketTable.resetPagination();
  }

  filterTicketsByDetailLabels(labelId: string) {
    if (this.detailLabelId == labelId) {
      this.detailLabelId = null;
    } else {
      this.detailLabelId = labelId;
    }
    if (!this.detailLabelId) {
      this.ticketService.setFilterDetailLabels([]);
    } else {
      this.ticketService.setFilterDetailLabels([this.detailLabelId]);
      const label = this.labelQuery.getValue().ticketDetailLabels.find(lab => lab.id == this.detailLabelId);
      this.subjectLabelId = label.parent_id;
    }
    this.ticketTable.resetPagination();
  }

  toggleFilter() {
    this.showFilterBar = !this.showFilterBar;
  }

  filter(filters: Filters) {
    this.ticketService.setFilters(filters);
    this.ticketService.getTickets().then();
    this.ticketTable.resetPagination();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
    this.ticketService.setFilter({});
  }

  resetState() {
    this.selectMode = false;
    this.ticketTable.toggleNextDisabled(false);
  }

  async getTickets() {
    const filter: ShopTicketAPI.GetTicketsFilter = {
      types: [TicketType.internal],
    }
    this.ticketService.setFilter(filter);

    this.finishPrepareData$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        if (loading) {
          this.ticketService.getTickets().then();
        }
      });
  }

  loadPage({page, perpage}) {
    this.ticketService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage,
    });
    this.getTickets();
  }

  async detail(ticket: Ticket) {
    this.createTicketMode = false;
    await this.ticketService.selectTicket(ticket);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.createTicketMode = false;
    this.ticketService.selectTicket(null);
    this._checkSelectMode();
    this.contactId = '';
  }

  private _checkSelectMode() {
    const selected = !!this.ticketQuery.getActive();
    this.ticketTable.toggleLiteMode(selected);
    this.ticketSlider.toggleLiteMode(selected);
  }

  createTicket() {
    this.createTicketMode = true;
    this.ticketService.selectTicket(null);
    this.selectMode = true;
  }

  get emptyDescription() {
    return 'Phiếu yêu cầu được nhân viên tạo ra trong quá trình gọi điện CSKH sẽ xuất hiện tại đây.'
  }

}
