import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from "@etop/shared";
import {AuthenticateModule} from "@etop/core";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {TicketsInternalComponent} from './tickets-internal.component';
import {TicketsInternalListComponent} from './tickets-internal-list/tickets-internal-list.component';
import {TicketsInternalRowComponent} from './components/tickets-internal-row/tickets-internal-row.component';
import {TicketsInternalDetailComponent} from './components/tickets-internal-detail/tickets-internal-detail.component';
import {CreateTicketAdminComponent} from './components/tickets-internal-create/tickets-internal-create.component';
import {ShopTicketCommentsModule} from "@etop/shared/components/shop-ticket-comments/shop-ticket-comments.module";

const routes: Routes = [
  {
    path: '',
    component: TicketsInternalComponent
  }
];

@NgModule({
  declarations: [
    TicketsInternalComponent,
    TicketsInternalListComponent,
    TicketsInternalRowComponent,
    TicketsInternalDetailComponent,
    CreateTicketAdminComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    EtopMaterialModule,
    EtopFilterModule,
    EtopCommonModule,
    EtopPipesModule,
    SideSliderModule,
    AuthenticateModule,
    NotPermissionModule,
    NgbModule,
    ShopTicketCommentsModule,
    EmptyPageModule
  ]
})
export class TicketsInternalModule {
}
