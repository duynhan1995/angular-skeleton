import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import misc from 'apps/core/src/libs/misc';
import { StorageService } from 'apps/core/src/services/storage.service';

@Component({
  selector: 'etelecom-cs-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  constructor(
    private auth: AuthenticateStore,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storageService: StorageService
  ) {}

  async ngOnInit() {
    try {
      let query = this.activatedRoute.snapshot.queryParams;
      let session = query['session'];

      let token = misc.decodeBase64(session);
      this.auth.clear();

      if (!session || !token) {
        throw new Error('token not found');
      }
      this.auth.updateToken(token);
      let res = await this.userService.checkToken();
      let accounts = res.available_accounts.filter(e => e.type == 'shop').sort((a, b) => a.id > b.id);
      for (let i = 0; i < accounts.length; i++) {
        let account = accounts[i];
        let accountRes = await this.userService.switchAccount(
          account.id,
          'ADMIN LOGIN'
        );
        account.token = accountRes.access_token;
        account.shop = accountRes.shop;
      }

      this.auth.updateInfo({
        token: res.access_token,
        account: res.account,
        user: res.user,
        accounts: accounts,
        shop: res.shop,
        isAuthenticated: true,
        permission: res.account.user_account.permission
      });
      this.storageService.set('adminLogin',true);
      location.href = window.location.origin;
    } catch (e) {
      this.router.navigateByUrl('/login');
      debug.error('ADMIN LOGIN', e);
    }
  }
}
