import {Component, OnInit} from '@angular/core';
import {TicketQuery} from "@etop/state/shop/ticket";
import {BaseComponent} from "@etop/core";
import {Observable} from "rxjs";
import {Ticket} from "@etop/models";

@Component({
  selector: 'etelecom-cs-tickets-system-detail',
  templateUrl: './tickets-system-detail.component.html',
  styleUrls: ['./tickets-system-detail.component.scss']
})
export class TicketsSystemDetailComponent extends BaseComponent implements OnInit {

  activeTicket$: Observable<Ticket> = this.ticketQuery.selectActive();

  activeTicketCommentsLoading$ = this.ticketQuery.select(state => state.ui.getTicketCommentsLoading);
  activeTicketComments$ = this.ticketQuery.selectActive(ticket => ticket.comments);

  loading = false;

  constructor(
    private ticketQuery: TicketQuery,
  ) {
    super();
  }

  ngOnInit() {
  }

}
