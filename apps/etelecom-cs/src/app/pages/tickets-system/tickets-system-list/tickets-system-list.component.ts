import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import {Ticket, TicketType} from '@etop/models';
import { takeUntil } from 'rxjs/operators';
import { TicketService, TicketQuery } from '@etop/state/shop/ticket';
import {ShopTicketAPI} from "@etop/api";
import { ActivatedRoute, Router } from '@angular/router';
import {TicketLabelService} from "@etop/state/shop/ticket-labels";

@Component({
  selector: 'etelecom-cs-tickets-system-list',
  templateUrl: './tickets-system-list.component.html',
  styleUrls: ['./tickets-system-list.component.scss'],
})
export class TicketsSystemListComponent
  extends PageBaseComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('ticketTable', { static: true }) ticketTable: EtopTableComponent;
  @ViewChild('ticketSlider', { static: true })
  ticketSlider: SideSliderComponent;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  createTicketMode: boolean;

  finishPrepareData$ = this.ticketQuery.select(
    (state) => state.ui.finishPrepareData
  );

  ticketsLoading$ = this.ticketQuery.selectLoading();
  activeTicket$ = this.ticketQuery.selectActive();

  isLastPage$ = this.ticketQuery.select((state) => state.ui.isLastPage);
  ticketsList$ = this.ticketQuery.selectAll();

  emptyTitle = 'Chưa có phiếu yêu cầu.';

  constructor(
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private ticketQuery: TicketQuery,
    private ticketService: TicketService,
    private ticketLabelService: TicketLabelService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  get showPaging() {
    return !this.ticketTable.liteMode && !this.ticketTable.loading;
  }

  get sliderTitle() {
    return this.createTicketMode ? 'Gửi yêu cầu hỗ trợ' : 'Chi tiết yêu cầu';
  }

  get emptyDescription(){
    return 'Danh sách yêu cầu hỗ trợ gửi đến eTelecom sẽ xuất hiện tại đây. ' +
      'Nhân viên CSKH eTelecom sẽ tiếp nhận và xử lý các yêu cầu trong thời gian sớm nhất.'
  }

  private _onSelectModeChanged(value) {
    this.ticketTable.toggleLiteMode(value);
    this.ticketSlider.toggleLiteMode(value);
  }

  async ngOnInit() {
    this.ticketLabelService.setFilter({type: TicketType.system});
    await this.ticketLabelService.getTicketLabels();
    this.ticketsLoading$.pipe(takeUntil(this.destroy$)).subscribe((loading) => {
      this.ticketTable.loading = loading;
      this.changeDetector.detectChanges();
      if (loading) {
        this.resetState();
      }
    });

    this.isLastPage$.pipe(takeUntil(this.destroy$)).subscribe((isLastPage) => {
      if (isLastPage) {
        this.ticketTable.toggleNextDisabled(true);
        this.ticketTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả các yêu cầu hỗ trợ.');
      }
    });

    this.headerController.setActions([
      {
        title: 'Gửi yêu cầu hỗ trợ',
        cssClass: 'btn btn-primary',
        onClick: () => this.createTicket(),
      },
    ]);

  }

  async ngAfterViewInit() {
    const { queryParams } = this.route.snapshot;
    let id = queryParams?.id;
    if (id) {
      let ticket = await this.ticketService.getTicket(id);
      if (ticket) {
        setTimeout(async _ => {
          await this.detail(ticket);
        }, 1000)
      }
    }
    this.router.navigate([], {
      queryParams: {
        'id': null,
      },
      queryParamsHandling: 'merge'
    })
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.ticketService.setFilter({});
  }

  resetState() {
    this.selectMode = false;
    this.ticketTable.toggleNextDisabled(false);
  }

  getTickets() {
    const finishPrepareData = this.ticketQuery.getValue().ui.finishPrepareData;
    const filter: ShopTicketAPI.GetTicketsFilter = {
      types: [TicketType.system]
    }
    this.ticketService.setFilter(filter);
    if (finishPrepareData) {
      this.ticketService.getTickets().then();
    } else {
      this.finishPrepareData$.pipe(takeUntil(this.destroy$))
        .subscribe(loading => {
          if (loading) {
            this.ticketService.getTickets().then();
          }
        });
    }
  }

  loadPage({ page, perpage }) {
    this.ticketService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage,
    });
    this.getTickets();
  }

  async detail(ticket: Ticket) {
    this.createTicketMode = false;
    await this.ticketService.selectTicket(ticket);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.createTicketMode = false;
    this.ticketService.selectTicket(null);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.ticketQuery.getActive();
    this.ticketTable.toggleLiteMode(selected);
    this.ticketSlider.toggleLiteMode(selected);
  }

  createTicket() {
    this.createTicketMode = true;
    this.ticketService.selectTicket(null);
    this.selectMode = true;
  }

}
