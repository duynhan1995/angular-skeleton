import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import {EmptyPageModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import { MobileDashboardComponent } from './mobile-dashboard/mobile-dashboard.component';
import { DesktopDashboardComponent } from './desktop-dashboard/desktop-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    MaterialModule,
    EmptyPageModule,
  ],
  exports: [],
  declarations: [
    DashboardComponent,
    DashboardComponent,
    MobileDashboardComponent,
    DesktopDashboardComponent,
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class DashboardModule {}
