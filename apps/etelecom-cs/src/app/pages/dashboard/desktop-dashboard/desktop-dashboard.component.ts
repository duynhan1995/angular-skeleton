import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { HeaderControllerService } from '../../../../../../core/src/components/header/header-controller.service';
import { UtilService } from '../../../../../../core/src/services/util.service';
import { PaycardTurtorialModalComponent } from '../../../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import { Moment } from 'moment';
import {ShopService} from "@etop/features";
import {DecimalPipe} from "@angular/common";
import { MatDialogController } from '@etop/shared';

@Component({
  selector: 'etelecom-cs-desktop-dashboard',
  templateUrl: './desktop-dashboard.component.html',
  styleUrls: ['./desktop-dashboard.component.scss']
})
export class DesktopDashboardComponent implements OnInit, OnDestroy {
  @Input() dashboard: any;
  @Input() loading = true;
  @Input() telecomBalance: number;
  @Output() filter = new EventEmitter();
  private modal: any;
  guideline = false;
  selected: { startDate: Moment; endDate: Moment };
  banner = '';

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private matDialogController: MatDialogController,
    private shopService: ShopService,
    private decimalPipe: DecimalPipe
  ) {
  }

  ngOnInit() {
    this.shopService.calcShopBalance("telecom").then(userCalcBalance => {
      const telecomBalance = userCalcBalance.telecom_balance;
      this.headerController.setContent(`
        <div class="text-telecom-balance">SỐ DƯ:</div>
        <div class="font-weight-bold telecom-balance">${this.decimalPipe.transform(telecomBalance)}đ</div>
    `);
    });
  }

  get isMobileVersion() {
    return this.util.isMobile;
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
      this.filter.emit(event)
    }
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
    this.headerController.clearContent();
  }

  openPayCardTurtorialModal() {
    const dialog = this.matDialogController.create({
      component: PaycardTurtorialModalComponent,
      afterClosedCb: () => {}
    });

    dialog.open();
  }

}
