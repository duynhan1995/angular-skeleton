import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {RelationshipQuery} from '@etop/state/relationship/relationship.query';
import {RelationshipService} from '@etop/state/relationship/relationship.service';
import {DashboardService} from 'apps/etelecom-cs/src/services/dashboard.service';
import {ShopService} from '@etop/features';
import {PaycardTurtorialModalComponent} from '../../components/paycard-turtorial-modal/paycard-turtorial-modal.component';
import * as moment from 'moment';
import {ExtensionApi} from "@etop/api/shop/extension.api";
import {DecimalPipe} from "@angular/common";
import {MatDialogController} from '@etop/shared';
import {HeaderControllerService} from "../../../../../core/src/components/header/header-controller.service";
import {HotlineQuery, HotlineService} from "@etop/state/shop/hotline";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {ActionOpt} from "../../../../../core/src/components/header/components/action-button/action-button.interface";
import {ExportCallLogModalComponent} from "../../components/export-call-log-modal/export-call-log-modal.component";
import {takeUntil} from "rxjs/internal/operators/takeUntil";

@Component({
  selector: 'etelecom-cs-dashboard-pos',
  template: `
    <etelecom-cs-desktop-dashboard class="hide-768" [dashboard]="dashboard" [loading]="loading"
                                   (filter)="getDashboard($event)">
    </etelecom-cs-desktop-dashboard>
    <etelecom-cs-mobile-dashboard class="show-768" [dashboard]="dashboard" [loading]="loading"
                                  (filter)="getDashboard($event)">
    </etelecom-cs-mobile-dashboard>`,
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseComponent implements OnInit, OnDestroy {
  telecomBalance: number;
  dashboard: any;
  loading = true;
  extensions: any = [];
  hotlineRequested$ = this.hotlineQuery.select(state => state.hotlineRequested);

  constructor(
    private dashboardService: DashboardService,
    private extensionApi: ExtensionApi,
    private relationshopService: RelationshipService,
    private relationshopQuery: RelationshipQuery,
    private changeDef: ChangeDetectorRef,
    private shopService: ShopService,
    private matDialogController: MatDialogController,
    private decimalPipe: DecimalPipe,
    private headerController: HeaderControllerService,
    private hotlineQuery: HotlineQuery,
    private hotlineService: HotlineService
  ) {
    super();
  }

  async ngOnInit() {
    this.shopService.calcShopBalance("telecom").then((userCalcBalance) => {
        this.telecomBalance = userCalcBalance?.telecom_balance;
        this.headerController.setMobileContent(
          `
        <div class="mobile-header-content">
            <span class="material-icons-outlined">account_balance_wallet</span>
            <span class="telecom-balance">${this.decimalPipe.transform(this.telecomBalance)}đ</span>
        </div>
    `
        )
      }
    );

    let headerActions: ActionOpt[] = [
      {
        title: 'Nạp tiền',
        cssClass: 'btn btn-primary',
        onClick: () => this.openPayCardTurtorialModal(),
      },
      {
        title: 'Xuất excel',
        cssClass: 'btn btn-primary btn-outline',
        onClick: () => this.openExportCallLogModal(),
        mobileHidden: true,
      },
    ];

    this.hotlineService.checkHotLineRequested();
    this.hotlineRequested$.pipe(takeUntil(this.destroy$)).subscribe(hotlineRequested => {
      if(!this.checkHaveHotlines() && !hotlineRequested){
        headerActions.unshift({
          title: 'Mua Hotline',
          cssClass: 'btn btn-primary btn-outline',
          onClick: () => this.hotlineService.hotlineRequest(),
          roles: ['owner'],
        });
      } else {
        if(headerActions.length > 2) {
          headerActions.shift();
        }
      }
    });

    this.headerController.setActions(headerActions);

    this.loading = true;
    let relationships: any = this.relationshopQuery.getAll();
    if (!relationships?.length) {
      relationships = await this.relationshopService.getRelationships();
    }
    this.loading = false;
  }

  async getExtensions() {
    try {
      let extensions = await this.extensionApi.getExtensions({});
      extensions = extensions.map(extension => {
        return {
          ...extension,
          user_name: this.relationshopQuery.getRelationshipNameById(extension.user_id)
        }
      })
      return extensions
    } catch (e) {
      debug.log('ERROR in getExtensions', e)
    }
  }

  openExportCallLogModal() {
    const dialog = this.matDialogController.create({
      component: ExportCallLogModalComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: () => {
      }
    });
    dialog.open();
  }

  async getDashboard(ranges) {
    try {

      this.dashboard = await this.dashboardService.summaryEtelecom({
        date_from: ranges.startDate.format('yy-MM-DD'),
        date_to: ranges.endDate.add(1, 'days').format('yy-MM-DD')
      })
      ranges.endDate.subtract(1, 'days').format('yy-MM-DD')
      if (this.extensions) {
        this.extensions = await this.getExtensions();
      }
      this.dashboard.staff = this.dashboard.staff.map(d => {
        const extension = this.extensions?.find(e => e.id == d.extension_id);
        return {
          ...d,
          full_name: extension?.user_name,
          extension_number: extension?.extension_number
        }
      })
      this.changeDef.detectChanges();
    } catch (e) {
      debug.log('ERROR in getDashboard', e)
    }
  }

  openPayCardTurtorialModal() {
    const dialog = this.matDialogController.create({
      component: PaycardTurtorialModalComponent,
      afterClosedCb: () => {
      }
    });

    dialog.open();
  }

  checkHaveHotlines() {
    let hotlines = this.hotlineQuery.getAll();
    hotlines = hotlines.filter(hotline => hotline?.connection_method == "direct");
    return !!hotlines?.length;
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearMobileContent();
  }

}
