import {AfterViewInit, ChangeDetectorRef, Component, NgZone, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ProductSubscriptionType} from '@etop/api';
import {CursorPaging, SubscriptionLine} from '@etop/models';
import {InvoiceRefType} from '@etop/models/Invoice';
import {EmptyType, EtopTableComponent, SideSliderComponent} from '@etop/shared';
import {InvoiceQuery, InvoiceService} from '@etop/state/shop/invoice';
import {SubscriptionQuery, SubscriptionService} from '@etop/state/shop/subscription';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etelecom-cs-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
})
export class InvoiceListComponent implements OnInit, AfterViewInit {
  @ViewChild('invoiceTable', {static: true}) invoiceTable: EtopTableComponent;
  @ViewChild('invoiceSlider', { static: true }) invoiceSlider: SideSliderComponent;
  isFirstRequestNewPage = true;
  invoices$ = this.invoiceQuery.selectAll();
  loading$ = this.invoiceQuery.selectLoading();
  ui$ = this.invoiceQuery.select((state) => state.ui);
  activeInvoice$ = this.invoiceQuery.selectActive();

  constructor(
    private invoiceService: InvoiceService,
    private invoiceQuery: InvoiceQuery,
    private subscriptionService: SubscriptionService,
    private subscriptionQuery: SubscriptionQuery,
    private util: UtilService,
    private route: ActivatedRoute,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private zone: NgZone,
  ) {}

  ngOnInit() {
    this.zone.run(async _ => {
      const {queryParams} = this.route.snapshot;
      let id = queryParams?.id;
      if (id) {
        this.invoiceService.getInvoice(id, true).then(async invoice => {
          if (!invoice) { //not exist invoice
            await this.loadPage({perpage: 20, page: 1});
            this.router.navigate([], {
              queryParams: {
                'id': null,
              },
              queryParamsHandling: 'merge'
            });
          } else {
            await this.invoiceDetail(id);
          }
        });
      } else {
        await this.loadPage({perpage: 20, page: 1});
      }
    })
    this.cdr.detectChanges();
  }

  async loadPage({perpage, page}) {
    if (this.isFirstRequestNewPage) {
      return (this.isFirstRequestNewPage = false);
    }
    let paging: CursorPaging;
    let currentPaging = this.invoiceQuery.currentUi.paging;
    let currentPage = this.invoiceQuery.currentUi.currentPage;

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    this.invoiceService.setPaging(paging); // paging dùng để gửi request lên server
    await this.invoiceService.getInvoices();
    this.mapRefInfo();
    this.invoiceService.setCurrentPage(page); // set số trang hiện tại
  }

  async mapRefInfo() {
    await this.subscriptionService.getSubscriptions(false);
    const subscriptionProducts = await this.subscriptionService.getSubscriptionProducts(
      ProductSubscriptionType.EXTENSION);
    let subscriptionPlans = [];
    await Promise.all(subscriptionProducts.map(async subproduct => {
      const plan = await this.subscriptionService.getSubscriptionPlans(subproduct.id);
      subscriptionPlans = subscriptionPlans.concat(plan);
    }));
    const invoices = this.invoiceQuery.getAll();
    invoices.forEach(invoice => {
      if (invoice.referral_type == InvoiceRefType.subscription && invoice?.referral_ids.length > 0) {
        const subscriptionLines: Array<SubscriptionLine> = this.subscriptionQuery.getEntity(
          invoice?.referral_ids[0])?.lines;
        if (subscriptionLines.length > 0) {
          const subscriptionPlanId = subscriptionLines[0].plan_id;
          const {name,price} = subscriptionPlans.find(plan => plan.id == subscriptionPlanId);
          invoice = {
            ...invoice,
            referral_info: {name,price}
          }
          this.invoiceService.updateInvoice(invoice);
        }
      }
    });
  }

  get emptyType() {
    return EmptyType.default;
  }

  get emptyDescription() {
    return 'Hoá đơn thanh toán dịch vụ, nạp tiền vào tài khoản sẽ xuất hiện tại đây.';
  }

  get sliderTitle() {
    return 'Chi tiết hóa đơn';
  }

  get sliderWidth() {
    return this.util.isMobile ? "100%" : "50%";
  }

  async onSliderClosed() {
    this.invoiceService.setActive(null);
    this._checkSelectMode();

    const {queryParams} = this.route.snapshot;
    let id = queryParams?.id;
    if (id) {
      this.router.navigate([], {
        queryParams: {
          'id': null,
        },
        queryParamsHandling: 'merge'
      });
      await this.loadPage({perpage: 20, page: 1});
    }
  }

  async invoiceDetail(id) {
    this.invoiceService.setActive(id);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.invoiceQuery.getActive();
    this.invoiceTable.toggleLiteMode(selected);
    this.invoiceSlider.toggleLiteMode(selected);
  }

  async goToTransaction() {
    await this.router.navigateByUrl(
      `s/${this.util.getSlug()}/invoice-transaction/transaction`
    );
  }

  async ngAfterViewInit() {}

}
