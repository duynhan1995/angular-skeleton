import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from '@etop/models';
import { Invoice, InvoiceType } from '@etop/models/Invoice';
import { SubscriptionQuery } from '@etop/state/shop/subscription';
import { Observable } from 'rxjs';

@Component({
  selector: '[etelecom-cs-invoice-row]',
  templateUrl: './invoice-row.component.html',
  styleUrls: ['./invoice-row.component.scss'],
})
export class InvoiceRowComponent implements OnInit {
  @Input() invoice = new Invoice({});
  @Input() liteMode = false;

  subscriptions$: Observable<Subscription>;
  invoiceType = {
    in: InvoiceType.in,
    out: InvoiceType.out,
  };

  constructor(
    private subscriptionQuery: SubscriptionQuery
  ) {}
  
  async ngOnInit() {
    
  }
}
