import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  OnChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '@etop/core';
import { SubscriptionLine, SubscriptionPlan, SubscriptionProduct, Ticket, TicketType } from '@etop/models';
import { Contact } from '@etop/models/Contact';
import { UtilService } from 'apps/core/src/services/util.service';
import { Invoice, InvoiceRefType, InvoiceType } from '@etop/models/Invoice';
import { InvoiceQuery, InvoiceService } from '@etop/state/shop/invoice';
import { ProductSubscriptionType } from '@etop/api/shop/subscription.api';
import {
  SubscriptionQuery,
  SubscriptionService,
} from '@etop/state/shop/subscription';

@Component({
  selector: 'etelecom-cs-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.scss'],
})
export class InvoiceDetailComponent extends BaseComponent implements OnInit {
  activeInvoice$ = this.invoiceQuery.selectActive();
  loading = true;
  invoiceType = {
    in: InvoiceType.in,
    out: InvoiceType.out,
  };
  paymentInfoList = [];
  paymentIndex = 1;
  constructor(
    private invoiceQuery: InvoiceQuery,
    private invoiceService: InvoiceService,
    private subscriptionService: SubscriptionService,
    private subscriptionQuery: SubscriptionQuery,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  async ngOnInit() {
    this.activeInvoice$.subscribe(async (invoice) => {
      this.loading = true;
      this.paymentInfoList = [];
      this.paymentIndex = 1;
      if (
        invoice?.referral_type == InvoiceRefType.subscription &&
        invoice?.referral_ids.length > 0
      ) {
        await this.getSubscriptionInfo(invoice);
      } else {
        await this.getCreditInfo(invoice);
      }
      this.loading = false;
    });
    
  }

  async getSubscriptionInfo(invoice) {
    const subscriptionProducts:SubscriptionProduct[] = await this.getSubscriptionProducts();
    const subscriptionPlans:SubscriptionPlan[] = await this.getSubscriptionPlans(subscriptionProducts);

    invoice?.referral_ids.forEach((referral_id) => {
      const subscriptionLines: Array<SubscriptionLine> = this.subscriptionQuery.getEntity(
        referral_id
      )?.lines;
      if (subscriptionLines.length > 0) {
        subscriptionLines.forEach((line) => {
          const paymentInfo = this.getSubscriptionLineInfo(
            line.plan_id,
            subscriptionPlans,
            subscriptionProducts
          );
          this.paymentInfoList.push(paymentInfo);
          this.paymentIndex++;
        });
      }
    });
  }

  getSubscriptionLineInfo(
    subscriptionPlanId: string,
    subscriptionPlans: SubscriptionPlan[],
    subscriptionProducts: SubscriptionProduct[]
  ) {
    const planInfo = subscriptionPlans.find(
      (plan) => plan.id == subscriptionPlanId
    );
    const productInfo = subscriptionProducts.find(
      (p) => (p.id = planInfo.product_id)
    );
    const paymentInfo = {
      index: this.paymentIndex,
      content: productInfo.name + ' - ' + planInfo.name,
      value: planInfo.price,
    };
    return paymentInfo;
  }

  getCreditInfo(invoice: Invoice) {
    this.paymentInfoList = [
      {
        index: 1,
        content: 'Nạp tiền vào tài khoản',
        value: invoice?.total_amount,
      },
    ];
  }

  async getSubscriptionProducts() {
    return await this.subscriptionService.getSubscriptionProducts(
      ProductSubscriptionType.EXTENSION
    );
  }

  async getSubscriptionPlans(subscriptionProducts: SubscriptionProduct[]) {
    let subscriptionPlans: SubscriptionPlan[] = [];
    await Promise.all(
      subscriptionProducts.map(async (subproduct) => {
        const plan = await this.subscriptionService.getSubscriptionPlans(
          subproduct.id
        );
        subscriptionPlans = subscriptionPlans.concat(plan);
      })
    );
    return subscriptionPlans;
  }
}
