import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { AuthenticateModule } from '@etop/core';
import { InvoiceDetailComponent } from './invoice-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    EtopMaterialModule,
    AuthenticateModule,
    EtopPipesModule
  ],
  exports: [InvoiceDetailComponent],
  entryComponents: [],
  declarations: [InvoiceDetailComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class InvoiceDetailFormModule {}
