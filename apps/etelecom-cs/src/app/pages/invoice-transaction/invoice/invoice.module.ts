import { CommonModule } from '@angular/common';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopPipesModule,
  MaterialModule,
  SideSliderModule,
} from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';
import { InvoiceDetailFormModule } from './component/invoice-detail/invoice-detail.module';
import { InvoiceListComponent } from './component/invoice-list/invoice-list.component';
import { InvoiceRowComponent } from './component/invoice-row/invoice-row.component';
import { InvoiceComponent } from './invoice.component';

const routes: Routes = [
  {
    path: '',
    component: InvoiceComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    EtopPipesModule,
    MaterialModule,
    MatTableModule,
    EtopCommonModule,
    EmptyPageModule,
    InvoiceDetailFormModule,
    SideSliderModule,
  ],
  exports: [
    InvoiceListComponent
  ],
  declarations: [InvoiceComponent, InvoiceListComponent, InvoiceRowComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class InvoiceModule {}
