import { Component, OnInit } from '@angular/core';
import { InvoiceTransactionTab } from '@etop/models/Invoice';
import { InvoiceService } from '@etop/state/shop/invoice';

@Component({
  selector: 'etelecom-cs-invoice',
  template: `
    <etelecom-cs-invoice-list></etelecom-cs-invoice-list>
  `,
})
export class InvoiceComponent implements OnInit {

  constructor(
    private invoiceService: InvoiceService,
    ) {}

  async ngOnInit() {
    this.invoiceService.changeHeaderTab(InvoiceTransactionTab.invoice);
  }
}
