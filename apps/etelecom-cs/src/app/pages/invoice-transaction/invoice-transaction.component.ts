import {InvoiceTransactionTab} from '@etop/models/Invoice';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {InvoiceQuery} from '@etop/state/shop/invoice';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {distinctUntilChanged, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UtilService} from 'apps/core/src/services/util.service';
import {ShopService} from '@etop/features/services';
import {DecimalPipe} from '@angular/common';

@Component({
  selector: 'etelecom-cs-invoice-transaction',
  template: `
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./invoice-transaction.component.scss']
})
export class InvoiceTransactionComponent implements OnInit, OnDestroy {
  tab$ = this.invoiceQuery.select((state) => state.ui.tab);

  constructor(
    private headerController: HeaderControllerService,
    private invoiceQuery: InvoiceQuery,
    private shopService: ShopService,
    private decimalPipe: DecimalPipe,
    private util: UtilService,
    private router: Router,
  ) {
  }

  async ngOnInit() {
    this.shopService.calcShopBalance('telecom').then((userCalcBalance) => {
      const telecomBalance = userCalcBalance.telecom_balance;
      this.headerController.setContent(`
<div class="text-telecom-balance">SỐ DƯ:</div>
<div class="font-weight-bold telecom-balance">
  ${this.decimalPipe.transform(telecomBalance)}đ
</div>`
      );
    });
    this.setupTabs();
  }

  checkTab(tabName) {
    return this.tab$.pipe(
      map((tab) => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Hóa đơn',
          name: InvoiceTransactionTab.invoice,
          active: false,
          onClick: () => {
            this.tabClick(InvoiceTransactionTab.invoice).then((_) => {
            });
          },
        },
        {
          title: 'Giao dịch',
          name: InvoiceTransactionTab.transaction,
          active: false,
          onClick: () => {
            this.tabClick(InvoiceTransactionTab.transaction).then((_) => {
            });
          },
        },
      ].map((tab) => {
        this.checkTab(tab.name).subscribe((active) => (tab.active = active));
        return tab;
      })
    );
  }

  private async tabClick(tabName: InvoiceTransactionTab) {
    await this.router.navigateByUrl(
      `s/${this.util.getSlug()}/invoice-transaction/${tabName}`
    );
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.headerController.clearContent();
  }
}
