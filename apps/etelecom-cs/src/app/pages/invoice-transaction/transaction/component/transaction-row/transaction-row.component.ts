import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { Transaction, TransactionType } from '@etop/models/Transaction';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: '[etelecom-cs-transaction-row]',
  templateUrl: './transaction-row.component.html',
  styleUrls: ['./transaction-row.component.scss'],
})
export class TransactionRowComponent implements OnInit {
  @Input() transaction = new Transaction({});
  @Input() liteMode = false;

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private util: UtilService,
  ) { }

  async ngOnInit() {}

  invoiceDetail() {
    if (this.transaction?.type == TransactionType.invoice) {
      this.router.navigateByUrl(`/s/${this.util.getSlug()}/invoice-transaction/invoice?id=${this.transaction?.referral_ids[0]}`);
    };
  }
}
