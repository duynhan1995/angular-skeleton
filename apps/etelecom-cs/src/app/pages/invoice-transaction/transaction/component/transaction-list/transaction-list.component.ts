import { Component, OnInit, ViewChild } from '@angular/core';
import { CursorPaging } from '@etop/models';
import { EmptyType, EtopTableComponent } from '@etop/shared';
import { SubscriptionService } from '@etop/state/shop/subscription';
import {
  TransactionQuery,
  TransactionService,
} from '@etop/state/shop/transaction';
import {Router} from "@angular/router";
import {UtilService} from "../../../../../../../../core/src/services/util.service";

@Component({
  selector: 'etelecom-cs-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss'],
})
export class TransactionListComponent implements OnInit {
  @ViewChild('transactionTable', { static: true })
  transactionTable: EtopTableComponent;
  isFirstRequestNewPage = true;
  transactions$ = this.transactionQuery.selectAll();
  loading$ = this.transactionQuery.selectLoading();
  ui$ = this.transactionQuery.select((state) => state.ui);

  constructor(
    private transactionService: TransactionService,
    private transactionQuery: TransactionQuery,
    private router: Router,
    private util: UtilService,
  ) {}

  async loadPage({ perpage, page }) {
    if (this.isFirstRequestNewPage) {
      return (this.isFirstRequestNewPage = false);
    }

    let paging: CursorPaging;
    let currentPaging = this.transactionQuery.currentUi.paging;
    let currentPage = this.transactionQuery.currentUi.currentPage;

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    this.transactionService.setPaging(paging); // paging dùng để gửi request lên server
    await this.transactionService.getTransactions();
    this.transactionService.setCurrentPage(page); // set số trang hiện tại
  }

  async goToInvoice() {
    await this.router.navigateByUrl(
      `s/${this.util.getSlug()}/invoice-transaction/invoice`
    );
  }

  async ngOnInit() {}

  get emptyType() {
    return EmptyType.default;
  }

  get emptyDescription() {
    return 'Tất cả các giao dịch phát sinh trong quá trình sử dụng sẽ xuất hiện tại đây.';
  }
}
