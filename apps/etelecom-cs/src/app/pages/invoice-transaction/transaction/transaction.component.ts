import { Component, OnInit } from '@angular/core';
import { InvoiceTransactionTab } from '@etop/models/Invoice';
import { InvoiceService } from '@etop/state/shop/invoice';
import { TransactionService } from '@etop/state/shop/transaction';

@Component({
  selector: 'etelecom-cs-transaction',
  template: `
    <etelecom-cs-transaction-list></etelecom-cs-transaction-list>
  `,
})
export class TransactionComponent implements OnInit {

  constructor(
    private invoiceService: InvoiceService,
    private transactionService: TransactionService,
  ) {}

  async ngOnInit() {
    this.invoiceService.changeHeaderTab(InvoiceTransactionTab.transaction);
    this.prepareData();
  }

  async prepareData() {
    const paging = {
      limit: 20,
      after: '.',
    };
    this.transactionService.setPaging(paging);
    await this.transactionService.getTransactions();
  }
}
