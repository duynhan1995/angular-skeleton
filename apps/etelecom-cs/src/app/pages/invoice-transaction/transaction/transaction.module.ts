import { CommonModule } from '@angular/common';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopPipesModule,
  MaterialModule,
} from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';
import { TransactionListComponent } from './component/transaction-list/transaction-list.component';
import { TransactionRowComponent } from './component/transaction-row/transaction-row.component';
import { TransactionComponent } from './transaction.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    EtopPipesModule,
    MaterialModule,
    MatTableModule,
    EtopCommonModule,
    EmptyPageModule,
  ],
  exports: [
    TransactionListComponent
  ],
  declarations: [
    TransactionComponent,
    TransactionListComponent,
    TransactionRowComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class TransactionModule {}
