import { CommonModule } from "@angular/common";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AuthenticateModule } from "@etop/core";
import { MaterialModule } from "@etop/shared";
import { EtopPipesModule } from "@etop/shared/pipes";
import { ModalControllerModule } from "apps/core/src/components/modal-controller/modal-controller.module";
import { SharedModule } from "apps/shared/src/shared.module";
import { DeleteShopComponent } from "./delete-shop.component";

const routes: Routes = [
    {
      path: '',
      component: DeleteShopComponent,
    }
  ];
  
  @NgModule({
    imports: [
      CommonModule,
      FormsModule,
      SharedModule,
      ModalControllerModule,
      AuthenticateModule,
      RouterModule.forChild(routes),
      EtopPipesModule,
      MaterialModule
    ],
    exports: [],
    declarations: [
        DeleteShopComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class DeleteShopModule {
  }
  