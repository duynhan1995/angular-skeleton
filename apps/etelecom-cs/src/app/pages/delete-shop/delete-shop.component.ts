import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationApi } from '@etop/api/shop/authorization.api';
import { BaseComponent } from '@etop/core';
import { AuthenticateStore } from '@etop/core/modules/authenticate';
import { ShopService, TelegramService } from '@etop/features';
import { FilterOperator, Filters } from '@etop/models';
import { MatDialogController } from '@etop/shared/components';
import { InvitationQuery } from '@etop/state/invitation/invitation.query';
import { InvitationService } from '@etop/state/invitation/invitation.service';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { CallLogQuery, CallLogService } from '@etop/state/shop/call-log';
import { ExtensionQuery, ExtensionService } from '@etop/state/shop/extension';
import { InvoiceQuery, InvoiceService } from '@etop/state/shop/invoice';
import { TransactionQuery, TransactionService } from '@etop/state/shop/transaction';
import { UserService } from 'apps/core/src/services/user.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { Account } from 'libs/models/Account';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import {AuthorizationQuery, AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: 'etelecom-cs-delete-shop',
  templateUrl: './delete-shop.component.html',
  styleUrls: ['./delete-shop.component.scss'],
})
export class DeleteShopComponent extends BaseComponent implements OnInit {
  account: Account;
  relationships$ = this.relationshipQuery.selectCount();
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');
  shopInvitations$ = this.invitationQuery.selectCount();
  extensions$ = this.extensionQuery.selectCount();
  invoices$ = this.invoiceQuery.selectCount();
  callLogs$ = this.callLogQuery.selectCount();

  loading = true;

  invitationCount: number;
  staffCount: number;
  invoiceCount: number;
  extensionCount: number;
  callLogCount: number;
  transactionCount: number;

  get isActiveDelete() {
    return !this.invitationQuery.getCount()  && ((this.relationshipQuery.getCount() - 1) < 1);
  }
  constructor(
    private auth: AuthenticateStore,
    private authorizationService: AuthorizationService,
    private authorizationQuery: AuthorizationQuery,
    private relationshipService: RelationshipService,
    private relationshipQuery: RelationshipQuery,
    private invitationService: InvitationService,
    private invitationQuery: InvitationQuery,
    private extensionService: ExtensionService,
    private extensionQuery: ExtensionQuery,
    private invoiceService: InvoiceService,
    private invoiceQuery: InvoiceQuery,
    private transactionService: TransactionService,
    private transactionQuery: TransactionQuery,
    private callLogService: CallLogService,
    private callLogQuery: CallLogQuery,
    private matDialogController: MatDialogController,
    private router: Router,
    public util: UtilService,
    private shopService: ShopService,
    private userService: UserService,
    private commonUsecase: CommonUsecase,
    private telegramService: TelegramService,
    ) {
      super();
    }

  async ngOnInit() {
    await this.prepareData();
    this.auth.authenticatedData$.subscribe((info) => {
      this.account = info.account;
    });
    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe((cancelled) => {
        if (cancelled) {
          this.invitationService.getShopInvitations();
        }
      });

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe((deleted) => {
        if (deleted) {
          this.relationshipService.getRelationships();
        }
      });
    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe((updated) => {
        if (updated) {
          this.relationshipService.getRelationships();
        }
      });
  }

  async prepareData() {
    this.loading = true;
    try {
      await this.relationshipService.getRelationships();
      await this.invoiceService.getInvoices();
      await this.callLogService.getCallLogs();
      await this.extensionService.getExtensions(null, true);
      await this.transactionService.getTransactions();
      await this.getShopInvitations();
      this.invitationCount = this.invitationQuery.getCount();
      this.staffCount = this.relationshipQuery.getCount() - 1;
      this.invoiceCount = this.invoiceQuery.getCount();
      this.extensionCount = this.extensionQuery.getCount();
      this.callLogCount = this.callLogQuery.getCount();
      this.transactionCount = this.transactionQuery.getCount();
    } catch (e) {
      debug.error('ERROR in prepareData', e);
    }

    this.loading = false;
  }

  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const filters : Filters = [
        {
          name: "status",
          op: FilterOperator.eq,
          value: "Z"
        },
        {
          name: "expires_at",
          op: FilterOperator.gt,
          value: dateString
        }
      ]
      this.invitationService.setFilters(filters);
      await this.invitationService.getShopInvitations();
    } catch(e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }

  confirmRemoveStaff() {
    const staffCount = this.relationshipQuery.getCount() - 1;
    const dialog = this.matDialogController.create({
      template:{
        title: `Gỡ nhân viên`,
        content: `
        <div>Bạn có chắc muốn gỡ tất cả ${staffCount} nhân viên ra khỏi công ty <strong>${this.account?.display_name}</strong>?</div>
      `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Gỡ',
      confirmStyle: 'btn-danger text-white',
      afterClosedCb: () => {},
      onConfirm: async () => {
        await this.removeAllStaff();
      }
    });
    dialog.open();
  }

  async removeAllStaff() {
    const extensions = this.extensionQuery.getAll();
    const staffs = this.relationshipQuery.getAll();
    const staffCount = this.relationshipQuery.getCount() - 1;
    let isDeleteAll = true;
    await Promise.all(staffs.map( async staff => {
      if (staff.user_id != this.auth.snapshot.user.id) {
        try {
          const staffExtension =  extensions.find(ext => ext.user_id == staff.user_id);
          if(staffExtension) {
            this.extensionService.removeUserOfExtension(staffExtension.id, staff.user_id);
          }
          await this.authorizationService.removeUserFromAccount(staff.user_id);
          this.authorizationService.removeRelationship();
        } catch (e) {
          isDeleteAll = false;
          toastr.error('Có lỗi xảy ra khi gỡ nhân viên ' + staff.full_name,e.message || e.msg);
          debug.error('ERROR in removing Staff', e);
        }
      }
    }));
    if (isDeleteAll) {
      await this.relationshipService.getRelationships();
      toastr.success('Gỡ tất cả ' + staffCount + 'nhân viên thành công.')
    }
  }

  confirmRemoveInvitation() {
    const invitationCount = this.invitationQuery.getCount();
    const dialog = this.matDialogController.create({
      template:{
        title: `Gỡ lời mời quản trị`,
        content: `
        <div>Bạn có chắc muốn xóa tất cả ${invitationCount} lời mời quản trị chưa được chấp nhận của công ty <strong>${this.account.display_name}</strong>?</div>
      `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmStyle: 'btn-danger text-white',
      afterClosedCb: () => {},
      onConfirm: async () => {
        await this.removeInvitation();
      }
    });
    dialog.open();
  }

  async removeInvitation() {
    const invitations = this.invitationQuery.getAll();
    const invitationCount = this.invitationQuery.getCount();
    let isDeleteAll = true;
    await Promise.all(invitations.map(async invitation => {
      try {
        await this.authorizationService.deleteInvitation(invitation.token);
      } catch(e) {
        toastr.error('Có lỗi xảy ra khi xoá lời mời nhân viên ' + invitation.full_name,e.message || e.msg);
        isDeleteAll = false
        debug.error('ERROR in deleting Invitation', e);
      }
    }));
    if(isDeleteAll) {
      await this.getShopInvitations();
      toastr.success('Gỡ tất cả ' + invitationCount + 'lời mời thành công.')
    }
  }

  confirmRemoveShop() {
    const dialog = this.matDialogController.create({
      template:{
        title: `Xoá công ty`,
        content: `
        <div>Xoá công ty là hành động không thể hoàn tác(khôi phục). Bạn có chắc muốn xoá công ty <strong>${this.account?.display_name}</strong>?</div>
      `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xóa',
      confirmStyle: 'btn-danger text-white',
      afterClosedCb: () => {},
      onConfirm: async () => {
        await this.removeShop();
      }
    });
    dialog.open();
  }

  async removeShop() {
    try {
      const {shop, user} = this.auth.snapshot;
      const otherAccount = this.auth.snapshot.accounts.find(account => account.id != this.account.id);
      const account = await this.userService.switchAccount(otherAccount?.id);

      await this.shopService.deleteShop(shop?.id, shop?.token);
      this.auth.updateInfo({
        token: account.access_token,
      })

      this.telegramService.deleteShopMessage(
        user,
        shop,
        this.extensionCount,
        this.callLogCount,
        this.invoiceCount,
        this.transactionCount,
        this.staffCount,
        this.invitationCount
        );
      toastr.success('Xoá công ty thành công.');
      await this.commonUsecase.updateSessionInfo(true);
      await this.router.navigateByUrl(`/s/${otherAccount?.url_slug}/settings/accounts`);
    } catch (e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in removing shop', e);
    }
  }

  async back() {
    await this.router.navigateByUrl(`/s/${this.account.url_slug}/settings/accounts`);
  }
}
