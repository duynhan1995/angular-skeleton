import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import {CallLogTab, CursorPaging, FilterOperator, FilterOptions, Filters, Relationship} from '@etop/models';
import { ModalController } from '../../../../../../core/src/components/modal-controller/modal-controller.service';
import { AudioModalComponent } from '../components/audio-modal/audio-modal.component';
import {ActionButton, EmptyType, EtopTableComponent} from '@etop/shared';
import { CallLogQuery, CallLogService } from '@etop/state/shop/call-log';
import {RelationshipQuery} from "@etop/state/relationship";
import {ExtensionQuery} from "@etop/state/shop/extension";
import {CallLogAPI} from "@etop/api";
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'etelecom-cs-mobile-call-log',
  templateUrl: './mobile-call-log.component.html',
  styleUrls: ['./mobile-call-log.component.scss']
})
export class MobileCallLogComponent implements OnInit {
  @ViewChild('callLogTable', {static: true}) callLogTable: EtopTableComponent;

  isFirstRequestNewPage = true;
  callLogs$ = this.callLogQuery.selectAll();
  ui$ = this.callLogQuery.select('ui');
  emptyAction: ActionButton[] = [];
  filters: FilterOptions = [];

  constructor(
    private callLogService: CallLogService,
    private ref: ChangeDetectorRef,
    private modalController: ModalController,
    private callLogQuery: CallLogQuery,
    private relationshipQuery: RelationshipQuery,
    private extensionQuery: ExtensionQuery,
    private auth: AuthenticateStore,
  ) {
  }

  async loadPage({perpage, page}) {
    if (this.isFirstRequestNewPage) {
      return this.isFirstRequestNewPage = false;
    }

    let paging: CursorPaging;
    let currentPaging = this.callLogQuery.getValue().ui.paging;
    let currentPage = this.callLogQuery.getValue().ui.currentPage;

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next
      }
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev
      }
    }

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.'
      };
    }

    this.callLogService.setPaging(paging) // paging dùng để gửi request lên server

    this.ref.detectChanges();
    await this.callLogService.getCallLogs(true);
    this.callLogService.setCurrentPage(page); // set số trang hiện tại
    this.ref.detectChanges();
  }

  async ngOnInit() {
    this.callLogQuery.selectLoading().subscribe(loading => this.callLogTable.loading = loading);
    this.emptyAction = [
      {
        title: 'iOS App',
        cssClass: 'btn-primary btn-outline',
        icon: 'fa-download',
        onClick: () => window.open('https://apps.apple.com/us/app/etelecom-t%E1%BB%95ng-%C4%91%C3%A0i-ch%E1%BB%91t-%C4%91%C6%A1n/id1545508792')
      },
      {
        title: 'Android App',
        cssClass: 'btn-primary btn-outline',
        icon: 'fa-download',
        onClick: () => window.open('https://play.google.com/store/apps/details?id=vn.etelecom.appcall.cs')
      },
      {
        title: 'Chrome Extension',
        cssClass: 'btn-primary btn-outline',
        icon: 'fa-download',
        onClick: () => window.open('https://chrome.google.com/webstore/detail/etelecom-t%E1%BB%95ng-%C4%91%C3%A0i-cskh/mnpcbkglhjmaeekcphikigpoiejdnipg?hl=en-US')
      }
    ];

    this.filters = [{
      label: 'Số điện thoại',
      name: 'call_number',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq,
    }];
  }

  filter(filters: Filters) {
    const _filter: CallLogAPI.GetCallLogsFilter = {};
    filters.forEach((f) => {
      if (f.value) {
        if (f.name == 'call_number') {
          _filter[f.name] = f.value;
        } else {
          _filter[f.name] = [f.value];
        }
      }
    });
    if (this.callLogQuery.getValue().ui.tab == CallLogTab.personal) {
      _filter.user_id = this.auth.snapshot.user.id;
    }

    this.callLogService.setFilter(_filter);
    this.callLogTable.resetPagination();
  }

  openAudioModal(callLog) {
    let modal = this.modalController.create({
      component: AudioModalComponent,
      showBackdrop: 'static',
      componentProps: {
        callLog
      },
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get emptyDescription() {
    if (!this.emptyResultFilter) {
      return 'Lịch sử cuộc gọi của tất cả nhân viên sẽ xuất hiện tại đây. ' +
        'Bạn có thể thực hiện cuộc gọi thông qua các kênh sau'
    }
  }

  get emptyResultFilter() {
    const page = this.callLogQuery.getValue().ui.currentPage;
    const callLogList = this.callLogQuery.getAll();
    const filter = this.callLogQuery.getValue().ui.filter;
    return page == 1 && callLogList.length == 0 && !!filter;
  }
}
