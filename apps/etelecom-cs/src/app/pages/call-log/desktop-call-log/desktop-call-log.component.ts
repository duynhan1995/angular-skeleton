import {Component, OnInit, ViewChild} from '@angular/core';
import {CallLogTab, CursorPaging, Extension, FilterOperator, FilterOptions, Filters, Relationship,} from '@etop/models';
import {CallLogQuery, CallLogService} from '@etop/state/shop/call-log';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {AudioModalComponent} from '../components/audio-modal/audio-modal.component';
import {ActionButton, EmptyType, EtopTableComponent} from '@etop/shared';
import {ExtensionQuery, ExtensionService} from '@etop/state/shop/extension';
import {RelationshipQuery,} from '@etop/state/relationship';
import {CallLogAPI} from '@etop/api/shop/call-log.api';
import {AuthenticateStore} from '@etop/core';
import {PageBaseComponent} from '@etop/web/core/base/page.base-component';

@Component({
  selector: 'etelecom-cs-desktop-call-log',
  templateUrl: './desktop-call-log.component.html',
  styleUrls: ['./desktop-call-log.component.scss'],
})
export class DesktopCallLogComponent extends PageBaseComponent implements OnInit {
  @ViewChild('callLogTable', {static: true}) callLogTable: EtopTableComponent;
  isFirstRequestNewPage = true;
  emptyAction: ActionButton[] = [];
  callLogs$ = this.callLogQuery.selectAll();
  loading$ = this.callLogQuery.selectLoading();
  ui$ = this.callLogQuery.select('ui');
  tab$ = this.callLogQuery.select(state => state.ui.tab);
  perpage: number;

  tab = {
    personal: CallLogTab.personal,
    general: CallLogTab.general
  }

  filters: FilterOptions = [];



  get showPaging() {
    return !this.callLogTable.liteMode && !this.callLogTable.loading;
  }

  get userExtension() {
    const currentUserID = this.auth.snapshot.user.id;
      const currentExtensionIDs = this.extensionQuery.getExtensionsIdsByUserId(
        currentUserID
      );
      return currentExtensionIDs.length > 0
  }

  get isNotRoleStaff() {
    const roles = this.auth.snapshot.permission.roles;
    if (roles.includes('owner')) {
      return true;
    }
    if (roles.includes('telecom_customerservice') && roles.includes('salesman')) {
      if (roles.includes('staff_management') && roles.length == 3)
        return true;
      if (roles.length == 2)
        return false;
    }
    return false;
  }

  constructor(
    private callLogService: CallLogService,
    private modalController: ModalController,
    private callLogQuery: CallLogQuery,
    private extensionQuery: ExtensionQuery,
    private relationshipQuery: RelationshipQuery,
    private extensionService: ExtensionService,
    private auth: AuthenticateStore,
  ) {
    super();
  }

  async ngOnInit() {
    await this.extensionService.getExtensions();
    this.emptyAction = [
      {
        title: 'iOS App',
        cssClass: 'btn-primary btn-outline',
        icon: 'fa-download',
        onClick: () =>
          window.open(
            'https://apps.apple.com/us/app/etelecom-t%E1%BB%95ng-%C4%91%C3%A0i-ch%E1%BB%91t-%C4%91%C6%A1n/id1545508792'
          ),
      },
      {
        title: 'Android App',
        cssClass: 'btn-primary btn-outline',
        icon: 'fa-download',
        onClick: () =>
          window.open(
            'https://play.google.com/store/apps/details?id=vn.etelecom.appcall.cs'
          ),
      },
      {
        title: 'Chrome Extension',
        cssClass: 'btn-primary btn-outline',
        icon: 'fa-download',
        onClick: () =>
          window.open(
            'https://chrome.google.com/webstore/detail/etelecom-t%E1%BB%95ng-%C4%91%C3%A0i-cskh/mnpcbkglhjmaeekcphikigpoiejdnipg?hl=en-US'
          ),
      },
    ];

    this.filters = [{
      label: 'Số điện thoại',
      name: 'call_number',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq,
    }]
  }

  filter(filters: Filters) {
    const _filter: CallLogAPI.GetCallLogsFilter = {};
    filters.forEach((f) => {
      if (f.value) {
        if (f.name == 'call_number') {
          _filter[f.name] = f.value;
        } else {
          _filter[f.name] = [f.value];
        }
      }
    });
    if (this.callLogQuery.getValue().ui.tab == CallLogTab.personal) {
      _filter.user_id = this.auth.snapshot.user.id;
    }

    this.callLogService.setFilter(_filter);
    this.callLogTable.resetPagination();
  }

  async loadPage({perpage, page}) {
    this.perpage = perpage;
    if (this.isFirstRequestNewPage) {
      return (this.isFirstRequestNewPage = false);
    }

    let paging: CursorPaging;
    let currentPaging = this.callLogQuery.Ui.paging;
    let currentPage = this.callLogQuery.Ui.currentPage;

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    this.callLogService.setPaging(paging); // paging dùng để gửi request lên server
    await this.callLogService.getCallLogs(true);
    this.callLogService.setCurrentPage(page); // set số trang hiện tại
  }

  openAudioModal(callLog) {
    let modal = this.modalController.create({
      component: AudioModalComponent,
      showBackdrop: 'static',
      componentProps: {
        callLog,
      },
      cssClass: 'modal-md',
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  get emptyType() {
    return EmptyType.default;
  }

  get emptyDescription() {
    if (this.callLogTab == CallLogTab.general) {
      return 'Lịch sử cuộc gọi của tất cả nhân viên sẽ xuất hiện tại đây. Vui lòng tải các ứng dụng sau để thực hiện cuộc gọi.';
    }
    if (this.callLogTab == CallLogTab.personal) {
      return 'Lịch sử cuộc gọi của cá nhân bạn sẽ xuất hiện tại đây. Vui lòng tải các ứng dụng sau để thực hiện cuộc gọi.';
    }
  }

  get callLogTab() {
    return this.callLogQuery.Ui.tab;
  }

}
