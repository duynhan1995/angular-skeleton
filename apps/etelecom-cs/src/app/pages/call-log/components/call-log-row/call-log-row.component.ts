import { Component, Input, OnInit } from '@angular/core';
import { CallLog, Order } from '@etop/models';
import { AudioModalComponent } from '../audio-modal/audio-modal.component';
import { ModalController } from '../../../../../../../core/src/components/modal-controller/modal-controller.service';

@Component({
  selector: '[etelecom-cs-call-log-row]',
  templateUrl: './call-log-row.component.html',
  styleUrls: ['./call-log-row.component.scss']
})
export class CallLogRowComponent implements OnInit {
  @Input() callLog = new CallLog({});
  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit(): void {
  }

  openAudioModal(callLog) {
    let modal = this.modalController.create({
      component: AudioModalComponent,
      showBackdrop: 'static',
      componentProps: {
        callLog
      },
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  isOutDirection(callLog: CallLog) {
    return callLog.direction == "out";
  }

  isAnsweredCall(callLog: CallLog) {
    return callLog.call_state == "answered";
  }

  get hasAudio() {
    return this.callLog.audio_urls?.length && this.callLog.audio_urls[0]
  }
}
