import {Component, OnDestroy, OnInit} from '@angular/core';
import {CallLogQuery, CallLogService} from '@etop/state/shop/call-log';
import {ContactService} from '@etop/state/shop/contact';
import {RelationshipService} from '@etop/state/relationship';
import {ExtensionQuery, ExtensionService} from '@etop/state/shop/extension';
import {AuthenticateStore} from '@etop/core';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {distinctUntilChanged, map} from 'rxjs/operators';
import {CallLogAPI} from '@etop/api';
import {CallLogTab} from '@etop/models';
import {MatDialogController} from "@etop/shared";
import {ExportCallLogModalComponent} from "../../components/export-call-log-modal/export-call-log-modal.component";

@Component({
  selector: 'etelecom-cs-call-log',
  template: `
    <etelecom-cs-desktop-call-log
      class="hide-768"
    ></etelecom-cs-desktop-call-log>
    <etelecom-cs-mobile-call-log
      class="show-768"
    ></etelecom-cs-mobile-call-log>`,
  styleUrls: ['./call-log.component.scss'],
})
export class CallLogComponent implements OnInit, OnDestroy {
  tab$ = this.callLogQuery.select((state) => state.ui.tab);

  get canViewCallLogGeneral() {
    const roles = this.auth.snapshot.permission.roles;
    if (roles.includes('owner')) {
      return true;
    }
    if (roles.includes('telecom_customerservice') && roles.includes('salesman')) {
      if (roles.includes('staff_management') && roles.length == 3)
        return true;
      if (roles.length == 2)
        return false;
    }
    return false;
  }

  constructor(
    private callLogService: CallLogService,
    private contactService: ContactService,
    private relationshipService: RelationshipService,
    private extensionService: ExtensionService,
    private callLogQuery: CallLogQuery,
    private matDialogController: MatDialogController,
    private headerController: HeaderControllerService,
    private auth: AuthenticateStore,
    private extensionQuery: ExtensionQuery
  ) {
  }

  async ngOnInit() {
    await this._prepareData();
    this.setupTabs();
    if (this.canExportExcel()) {
      this.headerController.setActions([
        {
          title: 'Xuất excel',
          cssClass: 'btn btn-primary btn-outline',
          onClick: () => this.openExportCallLogModal(),
          mobileHidden: true,
        }
      ])
    }
  }

  canExportExcel() {
    const roles = this.auth.snapshot.permission.roles;
    if (roles.includes('owner'))
      return true;
    if (roles.includes('telecom_customerservice') && roles.includes('salesman')) {
      if (roles.includes('staff_management') && roles.length == 3)
        return true;
      if (roles.length == 2)
        return false;
    }
    return false;
  }

  async _prepareData() {
    await this.relationshipService.getRelationships();
    await this.contactService.getContacts();
    await this.extensionService.getExtensions();
    if (this.canViewCallLogGeneral) {
      await this.reloadCallLogs();
      this.callLogService.changeHeaderTab(CallLogTab.general);
    } else {
      await this.reloadCallLogs(CallLogTab.personal);
      this.callLogService.changeHeaderTab(CallLogTab.personal);
    }
  }

  checkTab(tabName) {
    return this.tab$.pipe(
      map((tab) => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  setupTabs() {
    let tabs = [];
    if (this.canViewCallLogGeneral) {
      tabs = [
        {
          title: 'Lịch sử chung',
          name: CallLogTab.general,
          active: false,
          onClick: () => {
            this.callLogService.changeHeaderTab(CallLogTab.general);
            this.reloadCallLogs().then();
          },
        },
        {
          title: 'Lịch sử cá nhân',
          name: CallLogTab.personal,
          active: false,
          onClick: () => {
            this.callLogService.changeHeaderTab(CallLogTab.personal);
            this.reloadCallLogs(CallLogTab.personal).then();
          },
        }
      ]
    }
    this.headerController.setTabs(tabs.map((tab) => {
        this.checkTab(tab.name).subscribe((active) => (tab.active = active));
        return tab;
      })
    );
  }

  openExportCallLogModal() {
    const dialog = this.matDialogController.create({
      component: ExportCallLogModalComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: () => {
      }
    });
    dialog.open();
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  async reloadCallLogs(tab: CallLogTab = CallLogTab.general) {
    const paging = {
      limit: 20,
      after: '.',
    };
    let filter: CallLogAPI.GetCallLogsFilter;
    if (tab == CallLogTab.general) {
      this.callLogService.setPaging(paging);
      this.callLogService.setCurrentPage(1);
      this.callLogService.setFilter(null);
      await this.callLogService.getCallLogs(true);
    } else {
      filter = {
        user_id: this.auth.snapshot.user.id,
      };
      this.callLogService.setPaging(paging);
      this.callLogService.setCurrentPage(1);
      this.callLogService.setFilter(filter);
      await this.callLogService.getCallLogs(true);
    }
  }
}
