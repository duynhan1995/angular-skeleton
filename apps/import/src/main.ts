import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ImportAppModule } from 'apps/import/src/app/import-app.module';
import { environment } from './environments/environment';
import { hmrBootstrap } from 'apps/core/src/hmr';
import * as debug from '@etop/utils/debug';
import * as querystring from "querystring";


(window as any).__debug_host = '';
debug.setupDebug(environment);

const script = document.createElement('script');
script.src = `https://www.google.com/recaptcha/api.js?render=${environment.recaptcha_key}`;
document.getElementsByTagName('head')[0].appendChild(script);

if (environment.production) {
  enableProdMode();
}

let debugHost = querystring.parse(location.search.slice(1))?.debug_host;
__debug_host = debugHost as string;

const bootstrap = () => platformBrowserDynamic().bootstrapModule(ImportAppModule);

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    // eslint-disable-next-line no-console
    console.error('HMR is not enabled for webpack-dev-server!');
    // eslint-disable-next-line no-console
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  // eslint-disable-next-line no-console
  bootstrap().catch(err => console.log(err));
}
