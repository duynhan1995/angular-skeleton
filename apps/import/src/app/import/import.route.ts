import { Routes } from '@angular/router';

export const importRoutes: Routes = [
  {
    path: 'import',
    loadChildren: () => import('apps/import/src/app/pages/import-page/import-page.module').then(m => m.ImportPageModule)
  },
  {
    path: '**',
    redirectTo: 'import'
  }
];
