import {Component, OnInit} from '@angular/core';
import {ConnectionService} from "@etop/features/connection/connection.service";
import {BankService} from "@etop/state/bank";

@Component({
  selector: 'import-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {
  hideSidebar = true;

  constructor(
    private connectionService: ConnectionService,
    private bankService: BankService
  ) {}

  ngOnInit() {
    this.bankService.initBanks().then();
    this.connectionService.getValidConnections().then();
  }

}
