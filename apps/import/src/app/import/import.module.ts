import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "@etop/shared";
import {FromAddressModalModule} from "@etop/shared/components/from-address-modal/from-address-modal.module";
import {HeaderModule} from "apps/import/src/app/components/header/header.module";

import {ImportRoutingModule} from './import-routing.module';
import {ImportComponent} from 'apps/import/src/app/import/import.component';
import {CoreModule} from 'apps/core/src/core.module';
import {ImportGuard} from 'apps/import/src/app/import/import.guard';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const pages = [];

@NgModule({
  declarations: [ImportComponent, ...pages],
  providers: [ImportGuard],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    ImportRoutingModule,
    MaterialModule,
    HeaderModule,
    FromAddressModalModule
  ]
})
export class ImportModule {
}
