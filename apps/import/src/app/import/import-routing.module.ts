import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImportComponent } from 'apps/import/src/app/import/import.component';
import { ImportGuard } from 'apps/import/src/app/import/import.guard';
import {importRoutes} from "apps/import/src/app/import/import.route";

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [ImportGuard],
    resolve: {
      account: ImportGuard
    },
    component: ImportComponent,
    children: importRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportRoutingModule { }
