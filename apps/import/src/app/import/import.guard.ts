import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticateStore } from '@etop/core';

@Injectable()
export class ImportGuard implements CanActivate, Resolve<Promise<any>> {

  private static queryParamsHandling(route: ActivatedRouteSnapshot) {
    const keys = Object.keys(route?.queryParams);

    if (['brand', 'connection_method', 'connection_id'].some(item => keys.includes(item))) {
      const {brand, connection_method, connection_id} = route?.queryParams;

      localStorage.setItem('queryParams', JSON.stringify({
        brand, connection_method, connection_ids: connection_id?.split(",")
      }));
    } else if (keys.includes('forceClear')) {
      localStorage.removeItem('queryParams');
    }
  }

  constructor(
    private auth: AuthenticateStore,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {

    ImportGuard.queryParamsHandling(route);

    if (!this.auth.snapshot.account) {
      this.router.navigate(['/login']);
      return false;
    }
    return !!this.auth.snapshot.account;
  }

  async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    await this.auth.waitForReady();
    const params = route.params;
    const index = params['shop_index'];
    const result = this.auth.selectAccount(index);
    if (!result) {
      const frags = state.url.split('/');
      frags.splice(
        2,
        1,
        this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex().toString()
      );
      const nextUrl = frags.join('/');
      this.router.navigateByUrl(nextUrl);
      return false;
    }
    return result;
  }

}
