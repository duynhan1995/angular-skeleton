import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule} from '@angular/core';

import {CoreModule} from 'apps/core/src/core.module';
import {DesktopOnlyComponent} from "apps/import/src/app/components/desktop-only/desktop-only.component";
import {SharedModule} from 'apps/shared/src/shared.module';

import {AppComponent} from './app.component';
import {CommonModule, DatePipe, DecimalPipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {ImportCommonUsecase} from 'apps/import/src/app/usecases/import-common.usecase.service';
import {UtilService} from 'apps/core/src/services/util.service';
import {ShopService} from '@etop/features/services/shop.service';
import {AddressService} from 'apps/core/src/services/address.service';
import {MaterialModule} from '@etop/shared/components/etop-material/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PromiseQueueService} from 'apps/core/src/services/promise-queue.service';
import {LoginModule} from 'apps/shared/src/pages/login/login.module';
import {LoginComponent} from './pages/login/login.component';
import {ImportAppGuard} from 'apps/import/src/app/import-app.guard';
import {CONFIG_TOKEN} from '@etop/core/services/config.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../environments/environment';
import {EtopPipesModule} from 'libs/shared/pipes/etop-pipes.module';
import {EtopFilterModule} from '@etop/shared/components/etop-filter/etop-filter.module';
import {EtopFormsModule, EtopMaterialModule} from '@etop/shared';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';

import {registerLocaleData} from '@angular/common';
import localeVi from '@angular/common/locales/vi';

registerLocaleData(localeVi);

const services = [
  UtilService,
  ShopService,
  AddressService,
  PromiseQueueService,
];

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 's',
    loadChildren: () =>
      import('apps/import/src/app/import/import.module').then(m => m.ImportModule)
  },
  {
    path: '**',
    redirectTo: 's/-1/import'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DesktopOnlyComponent
  ],
  imports: [
    CoreModule.forRoot(),
    SharedModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgbModule,
    LoginModule,
    EtopFilterModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopFormsModule,
    environment.production ? [] : AkitaNgDevtools.forRoot()
  ],
  providers: [
    {provide: CommonUsecase, useClass: ImportCommonUsecase},
    {provide: CONFIG_TOKEN, useValue: environment},
    {provide: LOCALE_ID, useValue: 'vi'},
    ...services,
    ImportAppGuard,
    DecimalPipe,
    DatePipe
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ImportAppModule {
}
