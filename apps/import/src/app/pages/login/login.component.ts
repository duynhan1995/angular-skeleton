import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {UserApi} from "@etop/api";
import {AuthenticateStore} from "@etop/core";
import {MaterialInput} from "@etop/shared/components/etop-material/material-input.interface";
import {StringHandler} from "@etop/utils";
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {ConfigService} from "../../../services/config.service";

enum LoginStep {
  LOGIN = 1,
  OTP = 2
}

@Component({
  selector: 'import-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('otpInput', {static: false}) otpInput: MaterialInput;

  carriers = [
    {
      name: 'ghn',
      image_url: 'assets/images/provider_logos/ghn-s.png'
    },
    {
      name: 'ghtk',
      image_url: 'assets/images/provider_logos/ghtk-s.png'
    },
    {
      name: 'j&t',
      image_url: 'assets/images/provider_logos/jt-s.png'
    }
  ];

  loadingView = true;
  loading = false;

  phone: string;
  verifyCode: string;

  loginStep = LoginStep.LOGIN;
  countdown = 60;

  constructor(
    private commonUsecase: CommonUsecase,
    private auth: AuthenticateStore,
    private userApi: UserApi,
    private router: Router,
    private configService: ConfigService
  ) {
  }

  get config() {
    return this.configService.config;
  }

  get allCarriersDisplay() {
    return this.carriers.map(c => c.name).join(', ').toUpperCase();
  }

  get loginBoxHeaderTitle() {
    switch (this.loginStep) {
      case LoginStep.LOGIN:
        return 'Nhập số điện thoại để bắt đầu';
      case LoginStep.OTP:
        return 'Nhập mã xác thực (OTP)';
    }
  }

  ngOnInit() {
    this.commonUsecase.redirectIfAuthenticated().then(() => (this.loadingView = false));
  }

  async onPhoneSubmit() {
    this.loading = true;
    try {
      const _phone = this.phone.split(/-[0-9a-zA-Z]+-test$/)[0];
      if (!StringHandler.validatePhoneNumber(_phone)) {
        this.loading = false;
        return toastr.error('Vui lòng nhập số điện thoại hợp lệ!');
      }
      await this.checkUserRegistration();
      this.verifyCode = '';
      this.countTimeVerify();

    } catch (e) {
      toastr.error('Có lỗi xảy ra', e.code && (e.message || e.msg));
      debug.error('ERROR in onPhoneSubmit', e);
    }
    this.loading = false;
  }

  async checkUserRegistration() {
    try {
      await this.userApi.requestRegisterSimplify(this.phone);

      this.loginStep = LoginStep.OTP;
      setTimeout(_ => {
        this.otpInput?.focus();
      }, 0);
    } catch (e) {
      throw e;
    }
  }

  async reSendPhoneVerification() {
    if (this.countdown) {
      return;
    }
    await this.userApi.requestRegisterSimplify(this.phone);
    this.countTimeVerify();
  }

  changePhone() {
    this.loginStep = 1;
    this.phone = '';
  }

  countTimeVerify() {
    this.countdown = 60;
    let interval = setInterval(() => {
      if (this.countdown < 1) {
        clearInterval(interval);
      } else {
        this.countdown -= 1;
      }
    }, 1000);
  }

  async onOtpSubmit() {
    if (!this.verifyCode) {
      return toastr.error('Vui lòng nhập mã xác thực để tiếp tục!');
    }

    this.loading = true;
    try {
      const res = await this.userApi.registerSimplify(this.phone, this.verifyCode);

      this.auth.updateToken(res.access_token);
      this.auth.updateUser(res.user);

      await this.commonUsecase.updateSessionInfo(true);

      const ref = this.auth.getRef(true).replace('/', '');
      if (ref == 'login') {
        await this.router.navigateByUrl(`/s/${this.auth.currentAccountIndex()}/import`);
      } else {
        await this.router.navigateByUrl(ref || `/s/${this.auth.currentAccountIndex()}/import`);
      }
    } catch (e) {
      toastr.error('Có lỗi xảy ra', e.code && (e.message || e.msg));
      debug.error('ERROR in onOtpSubmit', e);
    }
    this.loading = false;
  }
}
