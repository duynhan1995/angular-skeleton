import {Component, OnDestroy, OnInit} from "@angular/core";
import {NavigationEnd, Router} from "@angular/router";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {TabOpt} from "apps/core/src/components/header/components/tab-options/tab-options.interface";
import {HeaderController} from "apps/import/src/app/components/header/header.controller";

enum HeaderTab {
  new = 'new',
  list = 'list',
  setting = 'setting',
}

@Component({
  selector: 'import-import-page',
  template: `<router-outlet></router-outlet>`
})
export class ImportPageComponent extends BaseComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private headerController: HeaderController
  ) {
    super();
  }

  ngOnInit() {
    this.setupTabs();
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Lên đơn',
        icon: 'post_add',
        iconOutline: true,
        name: 'new',
        active: true,
        onClick: () => {
          this.router.navigateByUrl(`/s/${this.auth.currentAccountIndex()}/import/new`).then();
        }
      },
      {
        title: 'Đơn giao hàng',
        icon: 'featured_video',
        iconOutline: true,
        name: 'list',
        active: false,
        onClick: () => {
          this.headerController.clearActions();
          this.router.navigateByUrl(`/s/${this.auth.currentAccountIndex()}/import/list`).then();
        }
      },
      {
        title: 'Thiết lập',
        icon: 'settings',
        iconOutline: true,
        name: 'setting',
        active: false,
        onClick: () => {
          this.headerController.clearActions();
          this.router.navigateByUrl(`/s/${this.auth.currentAccountIndex()}/import/setting`).then();
        }
      }
    ].map(tab => this.checkTab(tab)));
  }

  private checkTab(tab: TabOpt) {
    const headerTab: any = window.location.pathname.split('/')[4];
    tab.active = headerTab ? headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.new;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const _headerTab: any = event.url.split('/')[4];
        tab.active = _headerTab ? _headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.new;
      }
    });
    return tab;
  }
}
