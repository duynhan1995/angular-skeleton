import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges,
} from '@angular/core';
import {Address} from '@etop/models/Address';
import {AddressDisplayPipe} from "@etop/shared";
import {AddressService} from 'apps/core/src/services/address.service';
import {ShopService} from '@etop/features';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {FromAddressModalComponent} from '@etop/shared/components/from-address-modal/from-address-modal.component';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {DialogControllerService} from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {map, takeUntil} from "rxjs/operators";

@Component({
  selector: 'import-from-address-row',
  templateUrl: './from-address-row.component.html',
  styleUrls: ['./from-address-row.component.scss']
})
export class FromAddressRowComponent extends BaseComponent implements OnInit, OnChanges {
  @Output() addressRemoved = new EventEmitter<number>();

  @Input() address: Address;
  @Input() index: number;

  dropdownActions = [];
  shipFromAddressId$ = this.auth.state$.pipe(map(s => s.shop?.ship_from_address_id));

  isDefault = false;

  constructor(
    private addressService: AddressService,
    private shopService: ShopService,
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private ref: ChangeDetectorRef,
    private addressDisplay: AddressDisplayPipe
  ) {
    super();
  }

  ngOnInit() {
    this.shipFromAddressId$.pipe(takeUntil(this.destroy$))
      .subscribe(id => {
        this.isDefault = id == this.address?.id;
        this.dropdownActions[1].hidden = this.isDefault;
      });

    this.ref.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    const address = changes.address.currentValue;
    this.isDefault = this.auth.snapshot.shop?.ship_from_address_id == address?.id;

    this.dropdownActions = [
      {
        onClick: () => this.editFromAddress(),
        title: 'Chỉnh sửa'
      },
      {
        onClick: () => this.onRemoveAddress(),
        title: 'Xóa địa chỉ',
        cssClass: 'text-danger',
        hidden: this.isDefault
      }
    ];
  }

  async setDefaultAddress() {
    try {
      await this.shopService.setDefaultAddress(this.address.id, 'shipfrom');
      await this.auth.setDefaultAddress(this.address.id, 'shipfrom');

      toastr.success('Cập nhật địa chỉ mặc định thành công.');
    } catch (e) {
      debug.error('ERROR in setDefaultAddress', e);
      toastr.error('Cập nhật địa chỉ mặc định không thành công.', e.code && (e.message || e.msg));
    }
  }

  editFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: {...this.address},
        type: 'shipfrom',
        title: 'Cập nhật địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({address: addr}) => {
      this.address = addr
    });
  }

  onRemoveAddress() {
    const dialog = this.dialogController.createConfirmDialog({
      title: 'Xóa địa chỉ',
      body: `
        <p>Bạn có chắc muốn xóa địa chỉ này?</p>
        <strong>${this.addressDisplay.transform(this.address)}</strong>
        `,
      closeAfterAction: false,
      onConfirm: async () => {
        await this.removeAddress();
        dialog.close().then();
      }
    });

    dialog.show().then();
  }

  async removeAddress() {
    try {
      await this.addressService.removeAddress(this.address.id);
      this.addressRemoved.emit(this.index);
      toastr.success('Xóa địa chỉ thành công.');
    } catch (e) {
      toastr.error('Xóa địa chỉ không thành công.', e.code && (e.message || e.msg));
    }
  }
}
