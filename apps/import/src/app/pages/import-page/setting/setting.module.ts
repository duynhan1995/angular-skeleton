import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CarrierConnectModule} from "apps/import/src/app/components/carrier-connect/carrier-connect.module";
import {FromAddressRowComponent} from "apps/import/src/app/pages/import-page/setting/components/from-address-row/from-address-row.component";
import {SettingComponent} from "apps/import/src/app/pages/import-page/setting/setting.component";
import {SharedModule} from 'apps/shared/src/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {AuthenticateModule} from '@etop/core';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: SettingComponent,
  }
];

@NgModule({
  declarations: [
    SettingComponent,
    FromAddressRowComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ModalControllerModule,
    AuthenticateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    NgbModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    CarrierConnectModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingModule {
}
