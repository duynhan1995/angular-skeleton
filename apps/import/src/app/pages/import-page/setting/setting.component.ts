import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ShopAccountApi, UserApi} from '@etop/api';
import {AuthenticateStore} from '@etop/core';
import {ShopService} from "@etop/features";
import {Address, TRY_ON_OPTIONS} from '@etop/models';
import {FromAddressModalComponent} from "@etop/shared/components/from-address-modal/from-address-modal.component";
import {PageBaseComponent} from "@etop/web";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {AddressService} from "apps/core/src/services/address.service";
import {UserService} from 'apps/core/src/services/user.service';

@Component({
  selector: 'import-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SettingComponent extends PageBaseComponent implements OnInit {
  tryOn: string;
  addressesLoading = true;

  fromAddresses: Address[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private userApi: UserApi,
    private accountApi: ShopAccountApi,
    private auth: AuthenticateStore,
    private addressService: AddressService,
    private shopService: ShopService,
    private modalController: ModalController
  ) {
    super();
  }

  ngOnInit() {
    this.tryOn = this.auth.snapshot.shop.try_on;
    this.getAddresses().then();
  }

  async getAddresses() {
    try {
      const addresses = await this.addressService.getAddresses();
      this.fromAddresses = addresses.filter(a => a.type == 'shipfrom');
    } catch (e) {
      debug.error('ERROR in getAddresses', e);
    }
    this.addressesLoading = false;
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  async updateTryOn() {
    try {
      await this.shopService.updateShopField({try_on: this.tryOn});
      this.auth.updateTryOn(this.tryOn);
      toastr.success('Cập nhật thành công.');
    } catch (e) {
      toastr.error('Cập nhật không thành công.', e.code && (e.message || e.msg));
    }
  }

  openAddAddressForm(type) {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: type,
        title: 'Thêm địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({address}) => {
      if (!address) {
        return;
      }
      if (!this.fromAddresses?.length) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
      }
      this.fromAddresses.push(address);
    });
  }

  onRemoveAddress(index: number) {
    this.fromAddresses.splice(index, 1);
  }
}
