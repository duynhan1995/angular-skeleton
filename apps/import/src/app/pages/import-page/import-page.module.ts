import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ImportPageComponent} from "apps/import/src/app/pages/import-page/import-page.component";

const routes: Routes = [
  {
    path: '',
    component: ImportPageComponent,
    children: [
      {
        path: 'new',
        loadChildren: () =>
          import('apps/import/src/app/pages/import-page/new/new.module').then(m => m.NewModule)
      },
      {
        path: 'list',
        loadChildren: () =>
          import('apps/import/src/app/pages/import-page/list/list.module').then(m => m.ListModule)
      },
      {
        path: 'setting',
        loadChildren: () =>
          import('apps/import/src/app/pages/import-page/setting/setting.module').then(m => m.SettingModule)
      },
      {
        path: '**',
        redirectTo: 'new'
      }
    ]
  }
];

@NgModule({
  declarations: [
    ImportPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ImportPageModule { }
