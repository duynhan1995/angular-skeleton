import {FulfillmentRowComponent} from "apps/import/src/app/pages/import-page/list/components/fulfillment-row/fulfillment-row.component";
import {SingleFfmDetailModule} from "apps/import/src/app/pages/import-page/list/components/single-ffm-detail/single-ffm-detail.module";
import {FulfillmentListComponent} from "apps/import/src/app/pages/import-page/list/fulfillment-list/fulfillment-list.component";
import {ListComponent} from "apps/import/src/app/pages/import-page/list/list.component";
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FulfillmentService} from "apps/import/src/services/fulfillment.service";
import {SharedModule} from 'apps/shared/src/shared.module';
import {FormsModule} from '@angular/forms';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from '@etop/core';
import {EtopCommonModule, EtopFilterModule, EtopPipesModule, MaterialModule, SideSliderModule} from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: ListComponent
  }
];

@NgModule({
  declarations: [
    ListComponent,
    FulfillmentListComponent,
    FulfillmentRowComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgbTooltipModule,
    SingleFfmDetailModule,
    AuthenticateModule,
    EtopPipesModule,
    SideSliderModule,
    EtopFilterModule,
    EtopCommonModule,
    MaterialModule,
    SingleFfmDetailModule,
  ],
  providers: [
    FulfillmentService
  ]
})
export class ListModule {
}
