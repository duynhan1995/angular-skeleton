import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from '@etop/models';
import {FulfillmentService} from "apps/import/src/services/fulfillment.service";

@Component({
  selector: 'import-history-info',
  templateUrl: './history-info.component.html',
  styleUrls: ['./history-info.component.scss']
})
export class HistoryInfoComponent implements OnInit {
  @Input() ffm: Fulfillment;

  hasXShippingLogs = false;
  fulfillmentHistories = [];
  getting_histories = false;

  constructor(
    private ffmService: FulfillmentService
  ) {
  }

  async ngOnInit() {
    await this.getFulfillmentHistories(this.ffm);
  }

  async getFulfillmentHistories(ffm: Fulfillment) {
    this.getting_histories = true;
    try {
      let _histories = [];
      if (!ffm) {
        this.hasXShippingLogs = false;
        _histories = [];
      }
      if (ffm.x_shipping_logs && ffm.x_shipping_logs.length) {
        this.hasXShippingLogs = true;
        _histories = ffm.x_shipping_logs;
      } else {
        this.hasXShippingLogs = false;
        _histories = await this.ffmService.getFulfillmentHistory(ffm);
      }
      this.fulfillmentHistories = _histories;
    } catch (e) {
      debug.error('ERROR in getting ffm histories', e);
      this.fulfillmentHistories = [];
    }
    this.getting_histories = false;
  }

  get trackingLink() {
    return this.ffmService.trackingLink(this.ffm);
  }

}
