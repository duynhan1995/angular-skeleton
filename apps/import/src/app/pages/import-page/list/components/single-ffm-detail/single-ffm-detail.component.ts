import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from '@etop/models';

@Component({
  selector: 'import-single-ffm-detail',
  templateUrl: './single-ffm-detail.component.html',
  styleUrls: ['./single-ffm-detail.component.scss']
})
export class SingleFfmDetailComponent implements OnInit {
  @Input() activeTab: 'detail_info' | 'history' = 'detail_info';
  @Input() ffm: Fulfillment;

  constructor() {
  }

  ngOnInit() {
  }

}
