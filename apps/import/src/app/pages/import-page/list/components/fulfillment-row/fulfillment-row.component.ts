import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from '@etop/models';

@Component({
  selector: '[import-fulfillment-row]',
  templateUrl: './fulfillment-row.component.html',
  styleUrls: ['./fulfillment-row.component.scss']
})
export class FulfillmentRowComponent implements OnInit {
  @Input() fulfillment: Fulfillment;
  @Input() liteMode = false;

  constructor() {
  }

  get showCancelReason() {
    return this.fulfillment.shipping_state == 'cancelled' && this.fulfillment.cancel_reason;
  }

  get hasMoneyTransaction() {
    const id = this.fulfillment.money_transaction_shipping_id;
    return id && id != "0";
  }

  ngOnInit() {
  }

}
