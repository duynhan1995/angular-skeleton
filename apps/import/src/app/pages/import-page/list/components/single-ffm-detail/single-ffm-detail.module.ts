import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailInfoComponent} from "apps/import/src/app/pages/import-page/list/components/single-ffm-detail/detail-info/detail-info.component";
import {HistoryInfoComponent} from "apps/import/src/app/pages/import-page/list/components/single-ffm-detail/history-info/history-info.component";
import {SingleFfmDetailComponent} from "apps/import/src/app/pages/import-page/list/components/single-ffm-detail/single-ffm-detail.component";
import {FulfillmentService} from "apps/import/src/services/fulfillment.service";
import {SharedModule} from 'apps/shared/src/shared.module';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EtopMaterialModule, EtopPipesModule} from '@etop/shared';


@NgModule({
  declarations: [
    SingleFfmDetailComponent,
    DetailInfoComponent,
    HistoryInfoComponent
  ],
  exports: [
    SingleFfmDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    EtopMaterialModule,
    EtopPipesModule,
  ],
  providers: [
    FulfillmentService
  ]
})
export class SingleFfmDetailModule {
}
