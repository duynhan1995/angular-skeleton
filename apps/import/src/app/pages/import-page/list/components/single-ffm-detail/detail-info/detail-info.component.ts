import {Component, Input, OnChanges, OnInit, SimpleChanges,} from '@angular/core';
import {Fulfillment} from '@etop/models/Fulfillment';
import {FulfillmentService} from "apps/import/src/services/fulfillment.service";

@Component({
  selector: 'import-detail-info',
  templateUrl: './detail-info.component.html',
  styleUrls: ['./detail-info.component.scss'],
})
export class DetailInfoComponent implements OnInit, OnChanges {
  @Input() ffm: Fulfillment;

  constructor(
    private ffmService: FulfillmentService,
  ) {
  }

  ngOnInit() {
    this.initBarcode();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initBarcode();
  }

  initBarcode() {
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.ffm.shipping_code),
      0
    );
  }

  trackingLink() {
    const link = this.ffmService.trackingLink(this.ffm);
    if (!link) {
      return;
    }
    window.open(link);
  }

}
