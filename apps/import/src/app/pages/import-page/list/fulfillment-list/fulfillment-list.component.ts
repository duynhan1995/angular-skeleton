import {
  ChangeDetectorRef,
  Component, EventEmitter,
  OnInit, Output,
  ViewChild
} from '@angular/core';
import {Fulfillment} from '@etop/models';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {
  Filters
} from '@etop/models';
import {EtopTableComponent} from '@etop/shared/components/etop-common/etop-table/etop-table.component';
import {FulfillmentService} from "apps/import/src/services/fulfillment.service";
import {SideSliderComponent} from '@etop/shared/components/side-slider/side-slider.component';
import {PageBaseComponent} from "@etop/web";

@Component({
  selector: 'import-fulfillment-list',
  templateUrl: './fulfillment-list.component.html',
  styleUrls: ['./fulfillment-list.component.scss']
})
export class FulfillmentListComponent extends PageBaseComponent implements OnInit {
  @ViewChild('fulfillmentTable', {static: true}) fulfillmentTable: EtopTableComponent;
  @ViewChild('slider', {static: true}) slider: SideSliderComponent;

  @Output() toImport = new EventEmitter();

  filters: Filters = [];

  tabs = [
    {name: 'Thông tin', value: 'detail_info'},
    {name: 'Lịch sử GH', value: 'history'},
  ];
  activeTab: 'detail_info' | 'history' = 'detail_info';

  fulfillmentList: Fulfillment[] = [];
  selectedFfms: Fulfillment[] = [];

  page: number;
  perpage: number;

  constructor(
    private fulfillmentService: FulfillmentService,
    private modalController: ModalController,
    private changeDetector: ChangeDetectorRef,
  ) {
    super();
  }

  get sliderTitle() {
    return 'Chi tiết đơn giao hàng';
  }

  get showPaging() {
    return !this.fulfillmentTable.liteMode && !this.fulfillmentTable.loading;
  }

  get emptyResultFilter() {
    return (
      this.page == 1 &&
      this.fulfillmentList.length == 0 &&
      this.filters.length > 0
    );
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy đơn giao hàng phù hợp';
    }
    return 'Cửa hàng của bạn chưa có đơn giao hàng';
  }

  ngOnInit() {
  }

  isActive(ffm: Fulfillment) {
    return this.selectedFfms.some(_ffm => _ffm.id == ffm.id);
  }

  resetState() {
    this.slider.toggleLiteMode(false);
    this.fulfillmentTable.toggleLiteMode(false);
  }

  resetFilter() {
    this.filters = [];
  }

  filter(filters: Filters) {
    this.selectedFfms = [];
    this.filters = filters;
    this.fulfillmentTable.resetPagination();
  }

  async getShipmentFfms(page, perpage) {
    try {
      this.resetState();
      this.fulfillmentTable.toggleNextDisabled(false);

      let res: any = await this.fulfillmentService.getFulfillments(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
      if (page > 1 && res.fulfillments.length == 0) {
        this.fulfillmentTable.toggleNextDisabled(true);
        this.fulfillmentTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả đơn giao hàng.');
        return;
      }

      this.fulfillmentList = res.fulfillments.map(ffm => ({...ffm, p_data: {}}));

      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list ffms', e);
    }
  }

  async loadPage({page, perpage}) {
    this.fulfillmentTable.loading = true;
    this.changeDetector.detectChanges();

    await this.getShipmentFfms(page, perpage);

    this.fulfillmentTable.loading = false;
    this.changeDetector.detectChanges();
  }

  detail(ffm: Fulfillment) {
    this.changeTab('detail_info');

    this.selectedFfms = [ffm];
    this.slider.toggleLiteMode(true);
    this.fulfillmentTable.toggleLiteMode(true);
  }

  onSliderClosed() {
    this.selectedFfms = [];
    this.slider.toggleLiteMode(false);
    this.fulfillmentTable.toggleLiteMode(false);
  }

  gotoImport() {
    this.toImport.emit();
  }

  changeTab(tab_value) {
    this.activeTab = tab_value;
  }
}
