import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ImportFfmModule} from "@etop/shared/components/import-ffm/import-ffm.module";
import {CarrierConnectModule} from "apps/import/src/app/components/carrier-connect/carrier-connect.module";
import {NewComponent} from "apps/import/src/app/pages/import-page/new/new.component";

const routes: Routes = [
  {
    path: '',
    component: NewComponent
  }
];

@NgModule({
  declarations: [
    NewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ImportFfmModule,
    CarrierConnectModule,
  ]
})
export class NewModule {
}
