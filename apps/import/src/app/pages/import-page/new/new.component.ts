import {Component, OnInit, ViewChild} from "@angular/core";
import {ConnectionService, ConnectionStore} from "@etop/features";
import {ImportFfmComponent} from "@etop/shared/components/import-ffm/import-ffm.component";
import {ImportFfmQuery} from "@etop/state/shop/import-ffm";
import {PageBaseComponent} from "@etop/web";
import {HeaderController} from "apps/import/src/app/components/header/header.controller";
import {map, takeUntil} from "rxjs/operators";
import {ConfigService} from "../../../../services/config.service";

@Component({
  selector: 'import-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent extends PageBaseComponent implements OnInit {
  @ViewChild('importFfm', {static: false}) importFfm: ImportFfmComponent;

  connectedConnectionsList$ = this.connectionStore.state$.pipe(map(s => {
    return ConnectionService.filterConnections(s?.initConnections, this.isBuiltin, this.isDirect, this.connectionIDs);
  }));

  connectionsLoading$ = this.connectionStore.state$.pipe(map(s => s?.loadingConnections));

  constructor(
    private connectionStore: ConnectionStore,
    private headerController: HeaderController,
    private importFfmQuery: ImportFfmQuery,
    private configService: ConfigService
  ) {
    super();
  }

  get config() {
    return this.configService.config;
  }

  get connectionMethod() {
    const queryParams = localStorage.getItem('queryParams');
    if (queryParams) {
      const {connection_method} = JSON.parse(queryParams);
      return connection_method;
    }
    return null;
  }

  get connectionIDs() {
    const queryParams = localStorage.getItem('queryParams')
    if (queryParams) {
      const {connection_ids} = JSON.parse(queryParams);
      return connection_ids;
    }
    return [];
  }

  get isBuiltin() {
    return this.connectionMethod == 'builtin' || this.isNothing;
  }

  get isDirect() {
    return this.connectionMethod == 'direct' || this.isNothing;
  }

  get isNothing() {
    return !this.connectionMethod || !['builtin', 'direct'].includes(this.connectionMethod);
  }

  ngOnInit() {
    this.importFfmQuery.selectAll().pipe(takeUntil(this.destroy$))
      .subscribe(ffms => {
        if (ffms?.length) {
          this.headerController.setActions([
            {
              onClick: () => setTimeout(_ => {
                this.importFfm.openImportWarningDialog();
              }, 0),
              title: 'Import file khác',
              cssClass: 'btn btn-outline btn-primary'
            }
          ]);
        } else {
          this.headerController.clearActions();
        }
      });
  }
}
