import {Injectable} from '@angular/core';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {AuthenticateStore} from '@etop/core';
import {Router} from '@angular/router';
import {UserService} from 'apps/core/src/services/user.service';
import {Account} from 'libs/models/Account';
import {UserApi} from '@etop/api';

@Injectable()
export class ImportCommonUsecase extends CommonUsecase {
  app = 'dashboard';
  full_name;
  password;
  email;
  confirm;
  error = false;

  signupData: any = {};

  loading = false;

  provinces;

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore
  ) {
    super();
    (window as any).injectToken = (token) => {
      this.auth.updateToken(token);
      this.checkAuthorization(true).then(() => setTimeout(() => {
        location.href = location.origin;
      }, 500));
    };
  }

  async register() {}
  async login(data: { login: string; password: string }) {}

  redirectIfAuthenticated() {
    return this.checkAuthorization(true);
  }

  async checkAuthorization(fetchAccount = false) {
    let ref = this.router.url.split('?')[0];
    try {
      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(fetchAccount);

      ref = this.auth.getRef(true).replace('/', '');
      if (ref == 'login') {
        await this.router.navigateByUrl(`/s/${this.auth.currentAccountIndex()}/import`);
      } else {
        await this.router.navigateByUrl(ref || `/s/${this.auth.currentAccountIndex()}/import`);
      }
    } catch (e) {
      debug.error('ERROR in checkAuthorization', e);

      const queryParams = localStorage.getItem('queryParams');

      this.auth.clear();
      this.auth.setRef(ref);

      localStorage.setItem('queryParams', queryParams);
    }
  }

  async updateSessionInfo(fetchAccounts = false) {
    const res = await this.userService.checkToken(this.auth.snapshot.token);
    let {access_token, account, shop, user, available_accounts} = res;
    const shop_accounts = available_accounts.filter(a => a.type === 'shop').sort((a, b) => a.id > b.id);

    if (!shop && available_accounts && available_accounts.length) {
      shop = shop_accounts[0];
      account = shop_accounts[0];
    }
    const accounts: Account[] = fetchAccounts ? await Promise.all(shop_accounts.map(async (a) => {
      const accRes = await this.userService.switchAccount(a.id);
      a.token = accRes.access_token;
      a.shop = accRes.shop;
      a.id = accRes.shop && accRes.shop.id;
      a.image_url = a.shop.image_url;
      a.display_name = `${a.shop.code} - ${a.shop.name}`;
      a.permission = accRes.account.user_account.permission;
      return new Account(a);
    })) : this.auth.snapshot.accounts;

    if (accounts.length > 0) {
      this.auth.updateInfo({
        token: accounts[0].token || access_token,
        account: {
          ...account,
          ...shop
        },
        accounts,
        shop,
        user,
        permission: account.user_account.permission,
        isAuthenticated: true,
        uptodate: true
      });
    }
  }

}
