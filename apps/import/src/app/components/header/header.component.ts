import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {resetStores} from "@datorama/akita";
import {AuthenticateStore} from "@etop/core";
import {ActionOpt} from "apps/core/src/components/header/components/action-button/action-button.interface";
import {TabOpt} from "apps/core/src/components/header/components/tab-options/tab-options.interface";
import {HeaderController} from "apps/import/src/app/components/header/header.controller";

@Component({
  selector: 'import-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  tabs: TabOpt[] = [];
  actionButtons: ActionOpt[] = [];

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private headerController: HeaderController,
    private cdr: ChangeDetectorRef
  ) { }

  get user() {
    return this.auth.snapshot.user;
  }

  ngOnInit() {
    this.headerController.registerInstance(this);
  }

  setTabs(tabs: TabOpt[]) {
    this.tabs = tabs;
    this.cdr.detectChanges();
  }

  clearTabs() {
    this.tabs = [];
    this.cdr.detectChanges();
  }

  setActions(actions: ActionOpt[]) {
    this.actionButtons = actions;
    this.cdr.detectChanges();
  }

  clearActions() {
    this.actionButtons = [];
    this.cdr.detectChanges();
  }

  tabClick(tab: TabOpt) {
    this.tabs.forEach(t => t.active = false);
    tab.active = true;
    if (tab.onClick) {
      tab.onClick();
    }
  }

  actionClick(action: ActionOpt) {
    if (action.onClick) {
      action.onClick();
    }
  }

  logout() {
    const queryParams = localStorage.getItem('queryParams');

    this.auth.clear();
    resetStores();

    localStorage.setItem('queryParams', queryParams);

    this.router.navigateByUrl('/login').then();
  }

}
