import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EtopPipesModule, MaterialModule} from "@etop/shared";
import {HeaderComponent} from 'apps/import/src/app/components/header/header.component';

@NgModule({
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    EtopPipesModule,
  ]
})
export class HeaderModule {
}
