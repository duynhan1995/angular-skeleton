import { Injectable } from '@angular/core';
import {ActionOpt} from "apps/core/src/components/header/components/action-button/action-button.interface";
import {TabOpt} from "apps/core/src/components/header/components/tab-options/tab-options.interface";
import {HeaderComponent} from "apps/import/src/app/components/header/header.component";

@Injectable({
  providedIn: 'root'
})
export class HeaderController {
  private _headerComponents: Array<HeaderComponent> = [];

  constructor() { }

  registerInstance(instance: HeaderComponent) {
    this._headerComponents.push(instance);
  }

  setTabs(tabs: TabOpt[]) {
    this._headerComponents.forEach(comp => comp.setTabs(tabs));
  }

  clearTabs() {
    this._headerComponents.forEach(comp => comp.clearTabs());
  }

  setActions(actions: ActionOpt[]) {
    this._headerComponents.forEach(comp => comp.setActions(actions));
  }

  clearActions() {
    this._headerComponents.forEach(comp => comp.clearActions());
  }
}
