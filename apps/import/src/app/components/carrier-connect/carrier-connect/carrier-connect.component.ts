import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Connection} from '@etop/models';
import {BaseComponent} from '@etop/core';
import {ConnectionService, ConnectionStore} from "@etop/features";
import {map} from "rxjs/operators";

@Component({
  selector: 'import-carrier-connect',
  templateUrl: './carrier-connect.component.html',
  styleUrls: ['./carrier-connect.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarrierConnectComponent extends BaseComponent implements OnInit {
  connectionsList$ = this.connectionStore.state$.pipe(
    map(s => {
      const queryParams = localStorage.getItem('queryParams');
      const connectionsList = s?.initConnections?.filter(c => c.connection_method == 'direct' && c.status == 'P');

      if (queryParams) {
        const {connection_ids} = JSON.parse(queryParams);

        const validIDs = connection_ids?.some(id => connectionsList.map(c => c.id)?.includes(id));
        if (validIDs) {
          return connectionsList.filter(conn => connection_ids?.includes(conn.id));
        }
      }

      return connectionsList;
    })
  );

  constructor(
    private connectionStore: ConnectionStore,
    private connectionService: ConnectionService
  ) {
    super();
  }

  get connectionMethod() {
    const queryParams = localStorage.getItem('queryParams');
    if (queryParams) {
      const {connection_method} = JSON.parse(queryParams);
      return connection_method;
    }
    return null;
  }
  
  get isBuiltin() {
    return this.connectionMethod == 'builtin' || this.isNothing;
  }

  get isDirect() {
    return this.connectionMethod == 'direct' || this.isNothing;
  }

  get isNothing() {
    return !this.connectionMethod || !['builtin', 'direct'].includes(this.connectionMethod);
  }

  ngOnInit() {
  }

  getConnections() {
    this.connectionService.getValidConnections().then();
  }

}
