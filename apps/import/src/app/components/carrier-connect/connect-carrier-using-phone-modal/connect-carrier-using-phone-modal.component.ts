import {Component, Input, OnInit} from '@angular/core';
import {ConnectionApi, ConnectionAPI} from "@etop/api";
import {Connection} from "@etop/models";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";

enum LoginByOTPStep {
  phone= 'phone',
  otp = 'otp'
}

@Component({
  selector: 'import-connect-carrier-otp-modal',
  templateUrl: './connect-carrier-using-phone-modal.component.html',
  styleUrls: ['./connect-carrier-using-phone-modal.component.scss']
})
export class ConnectCarrierUsingPhoneModalComponent implements OnInit {
  @Input() connection: Connection;
  @Input() usingOTP = true;

  loginByOtp = new ConnectionAPI.LoginShopConnectionWithOTPRequest();
  loginByOTPStep: LoginByOTPStep = LoginByOTPStep.phone;

  loading = false;

  countDown = 600;
  timer: string;

  constructor(
    private modalAction: ModalAction,
    private connectionApi: ConnectionApi
  ) { }

  get confirmTitle() {
    if (this.loginByOTPStep == LoginByOTPStep.phone) {
      return 'Tiếp tục';
    }
    return 'Kết nối';
  }

  private static validateLoginByOTPInfo(data: ConnectionAPI.LoginShopConnectionWithOTPRequest, step: LoginByOTPStep) {
    if (!data.identifier) {
      toastr.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    if (!data.otp && step == LoginByOTPStep.otp) {
      toastr.error('Vui lòng nhập OTP!');
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  editPhone() {
    this.loginByOTPStep = LoginByOTPStep.phone;
    this.loginByOtp.otp = '';
    this.loading = false;
  }

  async loginUsingOTP() {
    this.loading = true;
    try {
      if (!ConnectCarrierUsingPhoneModalComponent.validateLoginByOTPInfo(this.loginByOtp, this.loginByOTPStep)) {
        return this.loading = false;
      }

      const { identifier, otp } = this.loginByOtp;

      if (this.loginByOTPStep == LoginByOTPStep.phone) {
        await this.connectionApi.loginShopConnection({
          connection_id: this.connection.id,
          identifier
        });

        if (this.usingOTP) {
          this.loginByOTPStep = LoginByOTPStep.otp;
          this.countDown = 600;
          this.startTimer();
          return this.loading = false;
        }

      } else {
        await this.connectionApi.loginShopConnectionWithOTP({
          connection_id: this.connection.id,
          identifier, otp
        });
      }

      toastr.success(`Kết nối với ${this.connection.name} thành công.`);
      this.modalAction.dismiss(true);
    } catch(e) {
      debug.error('ERROR in loginUsingOTP', e);
      toastr.error(
        `Kết nối với ${this.connection.name} không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }

  async retrySendOtp() {
    const { identifier } = this.loginByOtp;
    await this.connectionApi.loginShopConnection({
      connection_id: this.connection.id,
      identifier
    });

    this.countDown = 600;
    this.startTimer();
  }

  startTimer() {
    const interval = setInterval(() => {
      if (this.countDown < 1) {
        clearInterval(interval);
      } else {
        this.timer = this.displayCount(this.countDown);
        this.countDown -= 1;
      }
    }, 1000);
  }

  displayCount(timer) {
    const minutes = Math.floor(timer / 60);
    const seconds = Math.floor(timer % 60);
    let _seconds: any = seconds;
    if (seconds < 10) {
      _seconds = '0' + seconds;
    }
    return minutes + ':' + _seconds + 's';
  }

}
