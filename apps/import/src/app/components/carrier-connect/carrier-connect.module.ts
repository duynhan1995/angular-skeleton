import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticateModule} from "@etop/core";
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from "@etop/shared";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CarrierConnectModalComponent} from "apps/import/src/app/components/carrier-connect/carrier-connect-modal/carrier-connect-modal.component";
import {CarrierConnectRowComponent} from "apps/import/src/app/components/carrier-connect/carrier-connect-row/carrier-connect-row.component";
import {CarrierConnectComponent} from "apps/import/src/app/components/carrier-connect/carrier-connect/carrier-connect.component";
import {ConnectCarrierUsingPhoneModalComponent} from "apps/import/src/app/components/carrier-connect/connect-carrier-using-phone-modal/connect-carrier-using-phone-modal.component";
import {DropdownActionsModule} from "apps/shared/src/components/dropdown-actions/dropdown-actions.module";

@NgModule({
  declarations: [
    CarrierConnectComponent,
    CarrierConnectRowComponent,
    CarrierConnectModalComponent,
    ConnectCarrierUsingPhoneModalComponent
  ],
  exports: [
    CarrierConnectComponent
  ],
  imports: [
    CommonModule,
    DropdownActionsModule,
    AuthenticateModule,
    MaterialModule,
    EtopMaterialModule,
    EtopPipesModule,
    NgbModule
  ]
})
export class CarrierConnectModule {
}
