import {Inject, Injectable} from "@angular/core";
import {DOCUMENT} from "@angular/common";

export interface QueryParamsConfig {
  name: string;
  'primary-color': string;
  'primary-color-rgb': string;
  logo: string;
  loadingSvg: string;
}

const CONFIG = {
  etop: {
    name: 'ETOP IMPORT',
    'primary-color': '#33B6D4',
    'primary-color-rgb': '51, 182, 212',
    logo: 'assets/images/logo_medium.png',
    loadingSvg: 'assets/images/loading-etop.svg'
  },
  topship: {
    name: 'TOPSHIP IMPORT',
    'primary-color': '#E67E22',
    'primary-color-rgb': '231, 126, 35',
    logo: 'assets/images/topship-logo-medium.png',
    loadingSvg: 'assets/images/loading-topship.svg'
  }
}

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  config: QueryParamsConfig;

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) {}

  getConfigFromQueryParams() {
    const queryParams = localStorage.getItem('queryParams');
    if (queryParams) {
      const {brand} = JSON.parse(queryParams);

      this.config = CONFIG[['etop', 'topship'].includes(brand) ? brand : 'etop'];


      const root = this.document.documentElement;
      root.style.setProperty(`--primary-color`, this.config["primary-color"]);
      root.style.setProperty(`--primary-color-rgb`, this.config["primary-color-rgb"]);
    } else {
      this.config = CONFIG.etop;
    }
  }
}
