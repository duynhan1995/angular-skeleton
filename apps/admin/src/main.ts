import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AdminAppModule } from 'apps/admin/src/app/admin-app.module';
import querystring from "querystring";
import { environment } from './environments/environment';
import * as debug from '@etop/utils/debug';
import { hmrBootstrap } from '../../core/src/hmr';


(window as any).__debug_host = '';
debug.setupDebug(environment);

if (environment.production) {
  enableProdMode();
}

let debugHost = querystring.parse(location.search.slice(1))?.debug_host;
__debug_host = debugHost as string;

const bootstrap = () => platformBrowserDynamic().bootstrapModule(AdminAppModule);
if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    // eslint-disable-next-line no-console
    console.error('HMR is not enabled for webpack-dev-server!');
    // eslint-disable-next-line no-console
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  // eslint-disable-next-line no-console
  bootstrap().catch(err => console.log(err));
}
