import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {environment} from "apps/admin/src/environments/environment";
import misc from "apps/core/src/libs/misc";
import {Contact} from "apps/admin/src/app/pages/call-center-old/models/Contact";
import {CallLog} from "apps/admin/src/app/pages/call-center-old/models/CallCenter";
import {Ticket} from "apps/admin/src/app/pages/call-center-old/models/Ticket";
import {CallService} from "apps/admin/src/app/modules/callcenter/call.service";
import {HttpService} from "@etop/common";

const url = environment.crm_url;

@Injectable()
export class VTigerService {
  constructor(
    private callService: CallService,
    private http: HttpService
  ) {}

  private static _contactMap(contact) {
    return new Contact(contact);
  }

  filterContact(queryString = "") {
    return this.getContacts(queryString, [], 1, 100);
  }

  findContactByPhoneNumber(phoneNumber: string) {
    return this.getContacts(phoneNumber).then(contacts => contacts[0]);
  }

  private _callLogMap(call_log) {
    let map_from_contact = {
      lastname: "Không xác định",
      company: "Không xác định",
      phone: ""
    };

    if (call_log.direction == 1) {
      map_from_contact.phone = call_log.from_number;
    } else if (call_log.direction === 3) {
      map_from_contact.phone = call_log.to_number;
    }

    const { lastname, company, phone } = map_from_contact;
    const { time_started, time_connected, time_ended, duration } = call_log;
    return new CallLog({
      id: call_log.call_id,
      ...call_log,
      lastname,
      company,
      phone,
      fulltext_search: `${lastname} ${phone} ${company}`,
      date_started_parsed: moment(time_started * 1000).format("YYYY/MM/DD"),
      date_connected_parsed: moment(time_connected * 1000).format("YYYY/MM/DD"),
      date_ended_parsed: moment(time_ended * 1000).format("YYYY/MM/DD"),
      time_started_parsed: moment(time_started * 1000).format("HH:mm"),
      time_connected_parsed: moment(time_connected * 1000).format("HH:mm"),
      time_ended_parsed: moment(time_ended * 1000).format("HH:mm")
    });
  }

  filterCallLog(queryString = "") {
    return this.getCallLogs(queryString, [], 1, 100);
  }

  getCallLogs(q = "", filters = [], page = 1, perpage = 1000) {
    return this.http
      .get(`${url}/api.callcenter/call-history`, {
        q,
        filters: filters.map(f => JSON.stringify(f)),
        page,
        perpage
      })
      .toPromise()
      .then(res => res.data.map(callLog => this._callLogMap(callLog)));
  }

  async updateTicket(body: Ticket) {
    return this.http
      .put(`${url}/api.crm/ticket`, body)
      .toPromise()
      .then(res => res.data);
  }

  getContacts(query = "", filters = [], page = 1, perpage = 1000) {
    return this.http
      .get(`${url}/api.crm/contact`, {
        q: query,
        filters: filters.map(f => JSON.stringify(f)),
        page,
        perpage
      })
      .toPromise()
      .then(res => res.data.map(contact => VTigerService._contactMap(contact)));
  }

  async getTickets(query: {}) {
    try {
      let tickets: Array<any> = await this.http
        .get(
          `${url}/api.crm/ticket`,
          Object.assign({}, query)
        )
        .toPromise()
        .then(tks => tks.data.map(ticket => new Ticket(ticket)));
      let ids = tickets.map(ticket => ticket.order_id);
      ids = misc.uniqueArray(ids);
      let orders = await this.getOrdersByIds(ids);
      let orderMap = {};
      orders.forEach(order => {
        orderMap[order.id] = order;
      });

      tickets = tickets.map(ticket => {
        return this.ticketMap(ticket, orderMap);
      });

      let open_tickets = tickets.filter(t => t.ticketstatus == "Open");
      let confirmed_tickets = tickets.filter(
        t => t.ticketstatus == "Confirmed"
      );
      let closed_tickets = tickets.filter(t => t.ticketstatus == "Closed");
      closed_tickets.sort((a, b) => {
        if (
          new Date(a.created_at).getTime() < new Date(b.created_at).getTime()
        ) { return 1; }
        return -1;
      });
      open_tickets.sort((a, b) => {
        if (
          new Date(a.created_at).getTime() > new Date(b.created_at).getTime()
        ) { return 1; }
        return -1;
      });
      confirmed_tickets.sort((a, b) => {
        if (
          new Date(a.created_at).getTime() < new Date(b.created_at).getTime()
        ) { return 1; }
        return -1;
      });
      let inProcess_tickets = tickets.filter(
        t => t.ticketstatus == "In Progress"
      );
      inProcess_tickets.sort((a, b) => {
        if (
          new Date(a.created_at).getTime() < new Date(b.created_at).getTime()
        ) { return 1; }
        return -1;
      });
      return open_tickets.concat(confirmed_tickets).concat(inProcess_tickets).concat(closed_tickets);
    } catch (e) {
      debug.error("ERROR IN GET TICKETS", e);
      toastr.error("Có lỗi xảy ra khi load Tickets. F5 để load lại.");
    }
  }

  async getTicket(id) {
    try {
      return this.http.get(`${url}/api.crm/ticket/${id}`)
        .toPromise()
        .then(res => res.data);
    } catch (e) {
      debug.error("ERROR IN GET TICKET ID", e);
    }
  }
  ticketMap(ticket: Ticket, orderMap) {
    ticket["order"] = orderMap[ticket.order_id];
    if (ticket.code == "change-shop-cod") {
      ticket.old_value = ticket.old_value.replace(/\,/g, ".");
    }
    ticket.substatus_display = this.subStatusMap(ticket.substatus);
    return ticket;
  }

  subStatusMap(substatus) {
    return substatus
      ? {
        reject: "Từ chối xử lý",
        failed: "Xử lý thất bại",
        success: "Xử lý thành công"
      }[substatus]
      : "";
  }

  async createComment(body: {}) {
    return this.http
      .post(`${url}/api.crm/ticket/comment`, body)
      .toPromise()
      .then(res => res.data);
  }

  async getTicketComment(ticket_id) {
    return this.http
      .get(`${url}/api.crm/ticket/${ticket_id}`)
      .toPromise()
      .then(res => res.data);
  }
  async getVtigerProfile() {
    return this.http.get(`${url}/api.crm/user/profile`)
      .toPromise()
      .then(res => res.data);
  }

  getOrdersByIds(ids) {
    return this.http.post("api/admin.Order/GetOrdersByIDs", {"ids": ids}).toPromise()
      .then(res => res.orders);
  }

}
