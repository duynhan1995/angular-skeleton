import {Injectable} from "@angular/core";
import {LineConnector} from "apps/admin/src/app/modules/callcenter/LineConnector.class";
import {CallingPopupComponent} from "apps/admin/src/app/modules/callcenter/calling-popup/calling-popup.component";
import {LineSelectorPopupComponent} from "apps/admin/src/app/modules/callcenter/line-selector-popup/line-selector-popup.component";
import {Subject} from "rxjs";
import {environment} from "apps/admin/src/environments/environment";
import {HttpService} from "@etop/common";

export class SignalState {
  code: number;
  reason: string;
  sipCode: number;
  sipReason: string;
}

@Injectable({
  providedIn: "root"
})
export class CallService {

  callingPopup: CallingPopupComponent;
  selectorPopup: LineSelectorPopupComponent;

  lines: Array<LineConnector> = [];

  incomingSignal = new Subject();
  outcomingSignal = new Subject();
  onStateUpdated = new Subject<any>();

  hangupSignal = new Subject();
  busySignal = new Subject();
  onStream = new Subject();
  onOtherDevicePickup = new Subject();

  currentLine: LineConnector;
  currentCall = null;

  constructor(private http: HttpService) {
  }

  async bootstrap() {
    try {
      let lines = await this.http.get(`${environment.crm_url}/api.callcenter/line`)
        .toPromise()
        .then(res => res.data);
      this.lines = await lines.map(line => new LineConnector(line));

      this.lines.forEach(line => {
        line.incomingSignal.subscribe(call => {
          if (!this.currentCall) {
            this.currentCall = call;
            this.currentLine = line;
            this.incomingSignal.next({
              call,
              line: line.lineInfo
            });
          }
        });
        line.outcomingSignal.subscribe(call => {
          this.currentCall = call;
          this.currentLine = line;
          this.outcomingSignal.next({
            call,
            line: line.lineInfo
          });
        });
        line.onStateUpdated.subscribe(nextState => {
          if (this.currentLine == line || nextState.code == 6) {
            this.onStateUpdated.next(nextState);
          }
        });
        line.hangupSignal.subscribe((data) => {
          this.currentCall = null;
          this.currentLine = null;
          this.hangupSignal.next(data);
        });
        line.busySignal.subscribe(data => {
          this.currentCall = null;
          this.currentLine = null;
          this.hangupSignal.next(data);
        });
        line.onStream.subscribe(stream => {
          if (this.currentLine == line) {
            this.onStream.next(stream);
          }
        })
        this.onOtherDevicePickup.subscribe(this.onOtherDevicePickup.next);
      });
    } catch (e) {
      debug.error(e);
    }
  }

  registerCallingPopup(callingPopup) {
    this.callingPopup = callingPopup;
  }

  registerLineSelectorPopup(selectorPopup) {
    this.selectorPopup = selectorPopup;
  }

  clear() {
    this.currentCall = null;
    this.currentLine = null;
  }

  getLines() {
    return this.lines;
  }

  hangup() {
    // eslint-disable-next-line no-unused-expressions,@typescript-eslint/no-unused-expressions
    this.currentLine && this.currentLine.hangup();
  }

  reject() {
    // eslint-disable-next-line no-unused-expressions, @typescript-eslint/no-unused-expressions
    this.currentLine && this.currentLine.reject();
  }

  makeCall(phone) {
    this.selectorPopup.show(phone);
  }
}
