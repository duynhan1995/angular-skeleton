import {NgModule} from "@angular/core";
import {CallingPopupComponent} from "apps/admin/src/app/modules/callcenter/calling-popup/calling-popup.component";
import {CommonModule} from "@angular/common";
import {TimerDurationPipe} from "apps/admin/src/app/modules/callcenter/timer-duration.pipe";
import { LineSelectorPopupComponent } from 'apps/admin/src/app/modules/callcenter/line-selector-popup/line-selector-popup.component';
import {CallButtonDirective} from "apps/admin/src/app/modules/callcenter/call-button.directive";
import { WebphoneContainerComponent } from 'apps/admin/src/app/modules/callcenter/webphone-container/webphone-container.component';
import {VTigerService} from "apps/admin/src/services/vtiger.service";

@NgModule({
  declarations: [
    TimerDurationPipe,
    CallingPopupComponent,
    LineSelectorPopupComponent,
    CallButtonDirective,
    WebphoneContainerComponent
  ],
  providers: [
    VTigerService
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    TimerDurationPipe,
    CallingPopupComponent,
    CallButtonDirective,
    WebphoneContainerComponent,

  ]
})
export class CallCenterModule {

}
