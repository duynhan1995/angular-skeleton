import {Directive, HostListener, ElementRef, Input} from '@angular/core';
import {CallService} from "apps/admin/src/app/modules/callcenter/call.service";

// eslint-disable-next-line @angular-eslint/directive-selector
@Directive({ selector: '[eCallButton]' })
export class CallButtonDirective {
  constructor(private el: ElementRef, private callService: CallService) {}

  phoneNumber: string;

  @Input()
  set eCallButton(phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @HostListener('click', ['$event'])
  onclick($e) {
    $e.preventDefault();
    if (this.phoneNumber) {
      this.callService.makeCall(this.phoneNumber);
    }
  }
}
