import { Pipe, PipeTransform } from '@angular/core';
import * as moment from "moment";

@Pipe({
  name: 'duration'
})
export class TimerDurationPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    let duration = moment.duration(value);
    return `${this.pad(duration.minutes(), 2)}:${this.pad(
      duration.seconds(),
      2
    )}`;
  }

  pad(n, width, z?) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
  }
}
