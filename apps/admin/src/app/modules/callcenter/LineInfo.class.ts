export class LineInfo {
  account: any;
  assigned_user_id: string;
  createdAt: string;
  line_number: number;
  name: string;
  phone_number: string;
  server: string;
  token: string;
  updatedAt: string;
  username: string;
}
