import {Component, OnInit, ViewChild} from '@angular/core';
import {CallService} from "apps/admin/src/app/modules/callcenter/call.service";
import {LineConnector} from "apps/admin/src/app/modules/callcenter/LineConnector.class";

@Component({
  selector: 'admin-line-selector-popup',
  templateUrl: './line-selector-popup.component.html',
  styleUrls: ['./line-selector-popup.component.scss']
})
export class LineSelectorPopupComponent implements OnInit {

  phone = '';
  lines: LineConnector[] = [];

  @ViewChild('modal') modal;

  constructor(private callService: CallService) {
  }

  ngOnInit() {
    this.callService.registerLineSelectorPopup(this);
  }

  show(phone: string) {
    this.phone = phone;
    this.lines = this.callService.getLines();
    $(this.modal.nativeElement).modal('show');
  }

  call(line: LineConnector) {
    $(this.modal.nativeElement).modal('hide');
    line.makeCall(this.phone);
  }

}
