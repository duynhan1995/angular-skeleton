import {LineInfo} from "apps/admin/src/app/modules/callcenter/LineInfo.class";
import {Subject} from "rxjs";
import {SignalState} from "apps/admin/src/app/modules/callcenter/call.service";

export class LineConnector {
  incomingSignal = new Subject();
  outcomingSignal = new Subject();
  hangupSignal = new Subject();
  busySignal = new Subject();

  onStateUpdated = new Subject<SignalState>();
  onStream = new Subject();

  onOtherDevicePickup = new Subject();

  private _stringeeClient;
  currentCall;

  constructor(public lineInfo: LineInfo) {
    this._stringeeClient = new StringeeClient();
    this._settingClientEvents(this._stringeeClient);
    this._stringeeClient.connect(lineInfo.token);
  }

  _settingClientEvents(client) {
    client.on('connect', () => {
      debug.log('CallService:connected to StringeeServer', this._stringeeClient);
    });

    client.on('authen', function(res) {
      debug.log('CallService:authen: ', res);
      $('#loggedUserId').html(res.userId);
    });

    client.on('disconnect', () => {
      debug.log('CallService:disconnected');
      this._stringeeClient = null;
    });

    client.on('incomingcall', incomingcall => {
      debug.log("CallService:incomingcall", incomingcall);
      if (this.currentCall) {
        return;
      }
      let call = incomingcall;

      // incomingcall.hangup();

      this._settingCallEvents(call);
      this.currentCall = call;
      this.incomingSignal.next(call);
    });

    client.on('requestnewtoken', function() {});

    client.on('otherdeviceauthen', function(data) {
      debug.log('CallService:otherdeviceauthen: ', data);
    });
  }

  _settingCallEvents(call1) {
    call1.on('addlocalstream', stream => {});

    call1.on('addremotestream', stream => {
      debug.log('CallService:addremotestream', stream);
      this.onStream.next(stream);
    });

    call1.on('signalingstate', state => {
      debug.log('CallService:signalingstate ', state);
      if (state.code == 5) {
        this.busySignal.next(state);
        this.currentCall = null;
      }
      if (state.code == 6) {
        // this.hangupSignal.next(state);
        this.currentCall = null;
      }
      this.onStateUpdated.next(state);
    });

    call1.on('mediastate', function(state) {
      debug.log('CallService:mediastate', state);
    });

    call1.on('info', function(info) {
      debug.log('CallService:info', info);
    });

    call1.on('otherdevice', data => {
      debug.log('CallService:on otherdevice:' + JSON.stringify(data));
      if (
        (data.type === 'CALL_STATE' && data.code >= 200) ||
        data.type === 'CALL_END'
      ) {
        this.currentCall = null;
        this.onOtherDevicePickup.next();
      }
    });
  }

  makeCall(number: string) {
    if (this.currentCall) {
      throw new Error('Vui lòng ngắt cuộc gọi trước khi bắt đầu cuộc gọi mới');
    }
    let call = new StringeeCall(this._stringeeClient, this.lineInfo.username, number);

    this._settingCallEvents(call);

    return new Promise((res, rej) => {
      try {
        call.makeCall(callRes => {
          debug.log('CallService:make call: ' + JSON.stringify(callRes), call);
          this.currentCall = call;
          this.outcomingSignal.next(call);
          res(call);
        });
      } catch (e) {
        rej(e);
      }
    });
  }

  hangup() {
    // eslint-disable-next-line no-unused-expressions,@typescript-eslint/no-unused-expressions
    this.currentCall &&
    this.currentCall.hangup(res => {
      debug.log('CallService:hangup res', res);
      this.currentCall = null;
      this.hangupSignal.next();
      this.onStateUpdated.next({
        code: 6,
        reason: 'Self End',
        sipCode: 0,
        sipReason: ''
      });
    });
  }

  reject() {
    // eslint-disable-next-line no-unused-expressions, @typescript-eslint/no-unused-expressions
    this.currentCall && this.currentCall.reject(res => {
      debug.log('CallService:Reject incoming call:', res);
      this.onStateUpdated.next({
        code: 6,
        reason: 'Self End',
        sipCode: 0,
        sipReason: ''
      });
      this.currentCall = null;
    });
  }
}
