import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {CallService} from "apps/admin/src/app/modules/callcenter/call.service";
import {Contact} from "apps/admin/src/app/pages/call-center-old/models/Contact";
import {VTigerService} from "apps/admin/src/services/vtiger.service";

@Component({
  selector: 'admin-calling-popup',
  templateUrl: './calling-popup.component.html',
  styleUrls: ['./calling-popup.component.scss']
})
export class CallingPopupComponent implements OnInit, AfterViewInit {
  private _show = true;

  @ViewChild('ringstonePlayer') ringstonePlayer;
  @ViewChild('callendPlayer') callendPlayer;
  @ViewChild('remoteVideo') remoteVideo;

  get show() {
    return this._show;
  }

  set show(value) {
    this._show = value;
    this.onShowChange(value);
  }

  contact: Contact = null;
  currentCall = null;
  callState;
  isHolding = false;

  timerInterval = null;
  timer = 0;

  constructor(
    private callService: CallService,
    private vtiger: VTigerService,
    private eleRef: ElementRef
  ) {}

  ngOnInit() {
    window.addEventListener('beforeunload', e => {
      event.preventDefault();

      if (this.currentCall) {
        const confirmationMessage =
          'It looks like you have been editing something. ' +
          'If you leave before saving, your changes will be lost.';

        (e || window.event).returnValue = confirmationMessage;
        return confirmationMessage;
      }
    });

    window.addEventListener('unload', e => {
      this.hangup();
    });

    this.callService.registerCallingPopup(this);

    this.callService.incomingSignal.subscribe((callInfo: any) => {
      debug.log("Webphone:InCommingSignal", callInfo);
      this.loadContact(callInfo.call.fromNumber);
      this.currentCall = callInfo;
      this.show = true;
      this.callState = null;

      this.ringstone(true);
    });

    this.callService.outcomingSignal.subscribe((callInfo: any) => {
      debug.log("Webphone:OutCommingSignal", callInfo);
      this.loadContact(callInfo.call.toNumber);
      this.currentCall = callInfo;
      this.show = true;
    });

    this.callService.onStateUpdated.subscribe(nextState => {
      debug.log('Webphone:NextState', nextState);
      switch (nextState.code) {
        case 3: {
          this.resetTimer();
          break;
        }
        case 6: {
          this.stopTimer();
          this.endsound();
          this.ringstone(false);
          break;
        }
      }
      this.callState = nextState;
    });

    this.callService.hangupSignal.subscribe(() => {
      debug.log("Webphone:HangupSignal");
      this.setStream(null);
      this.ringstone(false);
    });

    this.callService.busySignal.subscribe(() => {
      debug.log("Webphone:BusySignal")
      this.setStream(null);
      this.ringstone(false);
    });

    this.callService.onStream.subscribe(stream => {
      this.setStream(stream);
    });
    this.callService.onOtherDevicePickup.subscribe(() => {
      debug.log("Webphone:OtherDevicePickup");
      this.setStream(null);
      this.ringstone(false);
      this.close();
    });
  }

  ngAfterViewInit() {
    this.ringstonePlayer.nativeElement.volume = 0.4;
  }

  async loadContact(phoneNumber) {
    this.contact = await this.vtiger.findContactByPhoneNumber(phoneNumber);
  }

  onShowChange(show) {
    if (show) {
      this.eleRef.nativeElement.style.visibility = "visible";
      this.eleRef.nativeElement.style.zIndex = 10000;
    } else {
      this.eleRef.nativeElement.style.visibility = "hidden";
      this.eleRef.nativeElement.style.zIndex = -1;
    }
  }

  setStream(stream) {
    debug.log("Webphone:STREAM", stream);
    this.remoteVideo.nativeElement.srcObject = null;
    this.remoteVideo.nativeElement.srcObject = stream;
  }

  hangup() {
    debug.log("Webphone:hangup called");
    this.callService.hangup();
    this.stopTimer();
  }

  reject() {
    debug.log("Webphone:reject called");
    if (!this.currentCall) {
      return;
    }
    this.callService.reject();
  }

  toggleHold() {
    debug.log('Webphone:Togle hold');
    if (!this.currentCall) {
      return;
    }
    if (this.currentCall.call.isOnHold) {
      this.currentCall.call.unhold();
    } else {
      this.currentCall.call.hold();
    }
  }

  answer() {
    this.currentCall.call.answer(res => {
      debug.log('Webphone:answer', res);
      this.callState = {
        code: 3
      };
      this.resetTimer();
    });
    this.ringstone(false);
  }

  close() {
    debug.log("Webphone:close called");
    this.currentCall = null;
    this.setStream(null);
    this.show = false;
    this.timer = 0;
  }

  getCurrentCallState() {
    if (!this.callState) {
      return '';
    }
    const stateMap = [
      0,
      'Đang gọi',
      'Đổ chuông',
      'Đang nghe',
      'Đang nghe',
      'Máy bận',
      'Đã gác máy'
    ];

    return stateMap[this.callState.code];
  }

  async ringstone(play = false) {
    try {
      if (play) {
        await this.ringstonePlayer.nativeElement.play();
      } else {
        await this.ringstonePlayer.nativeElement.pause();
      }
    } catch (e) {
      debug.error('unable to play ringstone', e.message);
    }
  }

  async endsound() {
    this.callendPlayer.nativeElement.play();
  }

  resetTimer() {
    debug.log('Webphone:resetTimer');
    this.stopTimer();
    this.timer = 0;
    this.timerInterval = setInterval(() => this.timer++, 1000);
  }

  stopTimer() {
    clearInterval(this.timerInterval);
  }

  showCommonAction(actionName) {
    const callState = this.callState;
    if (!callState) {
      return false;
    }
    const buttonStateMap = {
      hangup: [1, 2, 3],
      hold: [3],
      close: [5, 6]
    };
    return (
      buttonStateMap[actionName] &&
      buttonStateMap[actionName].indexOf(callState.code) > -1
    );
  }

  showIncomingAction() {
    let currentCall = this.currentCall &&  this.currentCall.call;
    if (
      !currentCall ||
      !currentCall.isIncomingCall ||
      currentCall.isAnswered ||
      currentCall.ended
    ) {
      return false;
    }
    return true;
  }

  showTimer() {
    const callState = this.callState;
    const call = this.currentCall && this.currentCall.call;
    return (
      this.timer > 0 &&
      callState &&
      (callState.code == 3 || callState.code == 4 || callState.code == 6)
      || (call && call.isAnswered)
    );
  }
}
