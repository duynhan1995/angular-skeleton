import { Component, OnInit, Input } from '@angular/core';
import { Ticket } from 'libs/models/Ticket';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { CrmApi } from 'apps/core/src/apis/crm-api.service';

@Component({
  selector: 'admin-ticket-list-in-detail',
  templateUrl: './ticket-list-in-detail.component.html',
  styleUrls: ['./ticket-list-in-detail.component.scss']
})
export class TicketListInDetailComponent implements OnInit {
  @Input() tickets: Ticket[];

  constructor(
    private modalController: ModalController,
    public util: UtilService,
    private crmApi: CrmApi
  ) { }

  ngOnInit() {
  }

  getStatusSnake(status) {
    return status.replace(' ', '-');
  }

  async ticketDetail($e: Event, ticket: Ticket) {
    // $e.preventDefault();
    // const ticketComments = await this.getTicketComment(ticket.id);
    // let modal = this.modalController.create({
    //   component: TicketModalComponent,
    //   showBackdrop: true,
    //   componentProps: {
    //     ticket: ticket,
    //     ticketComments
    //   },
    //   cssClass: 'modal-lg'
    // });
    // modal.show().then();
    // modal.onDismiss().then();
  }

  async getTicketComment(ticket_id) {
    try {
      const ticketComments = await this.crmApi.getTicketComment(ticket_id);
      if (ticketComments) {
        ticketComments.comments = ticketComments.comments
          .slice()
          .reverse();
      }
      return ticketComments;
    } catch (e) {
      debug.error('ERROR in getting ticket comment', e);
    }
  }

}
