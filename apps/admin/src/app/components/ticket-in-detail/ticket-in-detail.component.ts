import { Component, Input, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Ticket } from 'libs/models/Ticket';
import { CrmApi } from 'apps/core/src/apis/crm-api.service';

@Component({
  selector: 'admin-ticket-in-detail',
  templateUrl: './ticket-in-detail.component.html',
  styleUrls: ['./ticket-in-detail.component.scss']
})
export class TicketInDetailComponent extends BaseComponent implements OnInit {
  @Input() ffm = new Fulfillment({});

  tickets: Ticket[] = [];
  getting_tickets = false;

  constructor(
    private auth: AuthenticateStore,
    private crmApi: CrmApi
  ) {
    super();
  }

  async ngOnInit() {
    this.getTickets();
  }

  async getTickets() {
    this.getting_tickets = true;
    try {
      this.tickets = await this.crmApi.getTickets({
        order_id: this.ffm.order.id
      });
    } catch (e) {
      debug.error('ERROR in getting tickets', e);
    }
    this.getting_tickets = false;
  }

}
