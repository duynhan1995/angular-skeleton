import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketInDetailComponent } from 'apps/admin/src/app/components/ticket-in-detail/ticket-in-detail.component';
import { TicketListInDetailComponent } from 'apps/admin/src/app/components/ticket-in-detail/ticket-list-in-detail/ticket-list-in-detail.component';

@NgModule({
  declarations: [
    TicketInDetailComponent,
    TicketListInDetailComponent
  ],
  exports: [
    TicketInDetailComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TicketInDetailModule { }
