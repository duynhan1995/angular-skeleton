import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from "@etop/models/Fulfillment";
import {FeeType} from "@etop/models/Fee";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {AdminFulfillmentApi, AdminFulfillmentAPI, ShipmentPriceApi} from "@etop/api";

interface ShippingFeeToUpdate {
  type: FeeType;
  typeDisplay: string;
  oldValue: number;
  newValue: number;
  willBeUpdated: boolean;
}

@Component({
  selector: 'admin-update-shipping-fee-modal',
  templateUrl: './update-shipping-fee-modal.component.html',
  styleUrls: ['./update-shipping-fee-modal.component.scss']
})
export class UpdateShippingFeeModalComponent implements OnInit {
  @Input() ffm: Fulfillment;

  shippingFeesToUpdate: ShippingFeeToUpdate[] = [
    {
      type: FeeType.main,
      typeDisplay: `Phí ${ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(FeeType.main).toLowerCase()}`,
      oldValue: null,
      newValue: null,
      willBeUpdated: false
    },
    {
      type: FeeType.insurance,
      typeDisplay: `Phí ${ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(FeeType.insurance).toLowerCase()}`,
      oldValue: null,
      newValue: null,
      willBeUpdated: false
    },
    {
      type: FeeType.cods,
      typeDisplay: `Phí ${ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(FeeType.cods).toLowerCase()}`,
      oldValue: null,
      newValue: null,
      willBeUpdated: false
    },
    {
      type: FeeType.redelivery,
      typeDisplay: `Phí ${ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(FeeType.redelivery).toLowerCase()}`,
      oldValue: null,
      newValue: null,
      willBeUpdated: false
    },
    {
      type: FeeType.return,
      typeDisplay: `Phí ${ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(FeeType.return).toLowerCase()}`,
      oldValue: null,
      newValue: null,
      willBeUpdated: false
    },
    {
      type: FeeType.adjustment,
      typeDisplay: `Phí ${ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(FeeType.adjustment).toLowerCase()}`,
      oldValue: null,
      newValue: null,
      willBeUpdated: false
    }
  ];

  admin_note: string;

  loading = false;

  constructor(
    private modalAction: ModalAction,
    private fulfillmentApi: AdminFulfillmentApi
  ) { }

  private static validate(shippingFees: ShippingFeeToUpdate[]): boolean {
    for (let fee of shippingFees) {
      if (fee.willBeUpdated && !fee.newValue && fee.newValue != 0) {
        toastr.error(`Vui lòng nhập giá trị mới cho ${fee.typeDisplay.toLowerCase()}!`);
        return false;
      }
    }
    return true;
  }

  ngOnInit() {
    this._initShippingFeesToUpdate();
  }

  closeModal() {
    this.modalAction.close(true);
  }

  private _initShippingFeesToUpdate() {
    this.shippingFeesToUpdate.forEach(fee => {
      const shopFee = this.ffm.shipping_fee_shop_lines.find(l => l.shipping_fee_type == fee.type);
      if (!shopFee) {
        fee.oldValue = null;
      } else {
        fee.oldValue = (shopFee.cost || shopFee.cost == 0) ? shopFee.cost : null;
      }
    });
  }

  async confirm() {
    this.loading = true;
    try {
      if (!this.admin_note?.trim()) {
        toastr.error('Vui lòng nhập lý do cập nhật!');
        return this.loading = false;
      }

      if (!UpdateShippingFeeModalComponent.validate(this.shippingFeesToUpdate)) {
        return this.loading = false;
      }

      const request: AdminFulfillmentAPI.UpdateFulfillmentShippingFeesRequest = {
        id: this.ffm.id,
        admin_note: this.admin_note,
        shipping_fee_lines: this.shippingFeesToUpdate
          .filter(fee => fee.oldValue || fee.oldValue == 0 || fee.newValue || fee.newValue == 0)
          .map(fee => {
            return { shipping_fee_type: fee.type, cost: (fee.newValue || fee.newValue == 0) ? fee.newValue : fee.oldValue }
          })
      }

      if (request.shipping_fee_lines.length) {
        await this.fulfillmentApi.updateFulfillmentShippingFees(request);
      }

      toastr.success('Cập nhật cước phí đơn giao hàng thành công.');
      this.modalAction.dismiss(this.ffm.id);
    } catch(e) {
      debug.error('ERROR in updating ffm', e);
      toastr.error('Cập nhật cước phí đơn giao hàng không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

}
