import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment, FULFILLMENT_STATE, ShippingState} from '@etop/models/Fulfillment';
import {ModalAction} from 'apps/core/src/components/modal-controller/modal-action.service';
import {AdminFulfillmentApi} from '@etop/api';
import {UtilService} from 'apps/core/src/services/util.service';
import {TelegramService} from '@etop/features';

@Component({
  selector: 'admin-update-shipping-state-modal',
  templateUrl: './update-shipping-state-modal.component.html',
  styleUrls: ['./update-shipping-state-modal.component.scss']
})
export class UpdateShippingStateModalComponent implements OnInit {
  @Input() ffm: Fulfillment;

  shipping_state: ShippingState;
  admin_note: string;

  shippingStates = [
    {
      name: 'Mới',
      value: 'new',
    },
    {
      name: 'Đã tạo',
      value: 'created'
    },
    {
      name: 'Đã xác nhận',
      value: 'confirmed'
    },
    {
      name: 'Đang lấy hàng',
      value: 'picking'
    },
    {
      name: 'Chờ giao',
      value: 'holding'
    },
    {
      name: 'Đang giao',
      value: 'delivering'
    },
    {
      name: 'Đã giao',
      value: 'delivered'
    },
    {
      name: 'Đang trả hàng',
      value: 'returning'
    },
    {
      name: 'Đã trả hàng',
      value: 'returned'
    },
    {
      name: 'Hủy',
      value: 'cancelled'
    },
    {
      name: 'Không xác định',
      value: 'unknown'
    }
  ];

  loading = false;

  constructor(
    private util: UtilService,
    private modalAction: ModalAction,
    private telegram: TelegramService,
    private ffmApi: AdminFulfillmentApi
  ) { }

  ngOnInit() {}

  closeModal() {
    this.modalAction.close(true);
  }

  private validateDataFfm() {
    if (!this.shipping_state) {
      toastr.error('Vui lòng chọn trạng thái giao hàng mới!');
      return false;
    }
    if (!this.admin_note || !this.admin_note.trim()) {
      toastr.error('Vui lòng nhập lý do cập nhật!');
      return false;
    }
    return true;
  }

  async confirm() {
    this.loading = true;
    try {
      const valid = this.validateDataFfm();
      if (!valid) {
        return this.loading = false;
      }

      await this.ffmApi.updateFulfillmentShippingState({
        id: this.ffm.id,
        admin_note: this.admin_note,
        shipping_state: this.shipping_state
      });

      toastr.success('Cập nhật trạng thái đơn giao hàng thành công.');
      this.telegramMessage();
      this.modalAction.dismiss(this.ffm.id);
    } catch(e) {
      debug.error('ERROR in updating ffm', e);
      toastr.error('Cập nhật trạng thái đơn giao hàng không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  private telegramMessage() {
    const updatedContent = {
      carrier: this.ffm.carrier_display.toUpperCase(),
      shipping_code: this.ffm.shipping_code,
      shop_name: this.ffm.shop ? this.ffm.shop.name : '',
      customer_name: this.ffm.shipping_address.full_name,
      updated_fields: [
        {
          type: 'Trạng thái giao hàng',
          old_value: this.ffm.shipping_state_display,
          new_value: this.ffm.shipping_code ? FULFILLMENT_STATE[this.shipping_state] : FULFILLMENT_STATE.error,
          checked: true
        }
      ],
      admin_note: this.admin_note
    };
    this.telegram.updateFfmMessage(updatedContent).then();
  }

}
