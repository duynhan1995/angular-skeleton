import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UpdateFfmModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-ffm-modal/update-ffm-modal.component";
import {UpdateShippingStateModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-state-modal/update-shipping-state-modal.component";
import {UpdateShippingFeeModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-fee-modal/update-shipping-fee-modal.component";
import {UpdateCompensationAmountModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-compensation-amount-modal/update-compensation-amount-modal.component";
import {EtopMaterialModule, MaterialModule} from "@etop/shared";
import {FormsModule} from "@angular/forms";
import {AuthenticateModule} from "@etop/core";

@NgModule({
  declarations: [
    UpdateFfmModalComponent,
    UpdateShippingStateModalComponent,
    UpdateShippingFeeModalComponent,
    UpdateCompensationAmountModalComponent
  ],
  imports: [
    CommonModule,
    AuthenticateModule,
    EtopMaterialModule,
    MaterialModule,
    FormsModule
  ]
})
export class UpdateFulfillmentModule { }
