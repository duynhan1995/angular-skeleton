import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from "@etop/models/Fulfillment";
import {UtilService} from "apps/core/src/services/util.service";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {TelegramService} from "@etop/features";
import {AdminFulfillmentApi} from "@etop/api";

@Component({
  selector: 'admin-update-compensation-amount-modal',
  templateUrl: './update-compensation-amount-modal.component.html',
  styleUrls: ['./update-compensation-amount-modal.component.scss']
})
export class UpdateCompensationAmountModalComponent implements OnInit {
  @Input() ffm: Fulfillment;

  actual_compensation_amount: number;
  admin_note: string;

  loading = false;

  constructor(
    private util: UtilService,
    private modalAction: ModalAction,
    private telegram: TelegramService,
    private ffmApi: AdminFulfillmentApi
  ) { }

  ngOnInit() {}

  closeModal() {
    this.modalAction.close(true);
  }

  private validateDataFfm() {
    if (!this.actual_compensation_amount && this.actual_compensation_amount != 0) {
      toastr.error('Vui lòng nhập số tiền bồi hoàn!');
      return false;
    }
    if (!this.admin_note || !this.admin_note.trim()) {
      toastr.error('Vui lòng nhập lý do cập nhật!');
      return false;
    }
    return true;
  }

  async confirm() {
    this.loading = true;
    try {
      const valid = this.validateDataFfm();
      if (!valid) {
        return this.loading = false;
      }
      await this.ffmApi.updateFulfillmentShippingState({
        id: this.ffm.id,
        admin_note: this.admin_note,
        shipping_state: 'undeliverable',
        actual_compensation_amount: this.actual_compensation_amount
      });

      toastr.success('Xử lý bồi hoàn thành công.');
      this.telegramMessage();
      this.modalAction.dismiss(this.ffm.id);
    } catch(e) {
      debug.error('ERROR in updating ffm', e);
      toastr.error('Xử lý bồi hoàn không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  private telegramMessage() {
    this.telegram.updateShippingState(
      this.ffm, this.actual_compensation_amount, this.admin_note
    ).then();
  }

}
