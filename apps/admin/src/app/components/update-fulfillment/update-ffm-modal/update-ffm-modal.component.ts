import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from '@etop/models/Fulfillment';
import {ModalAction} from 'apps/core/src/components/modal-controller/modal-action.service';
import {AdminFulfillmentApi} from '@etop/api';
import {UtilService} from 'apps/core/src/services/util.service';
import {TelegramService} from '@etop/features';
import {VND} from '@etop/shared/pipes/etop.pipe';
import {FeeType} from "@etop/models/Fee";

export enum UpdateFulfillmentType {
  full_name = 'full_name',
  phone = 'phone',
  total_cod_amount = 'total_cod_amount',
}

@Component({
  selector: 'admin-update-ffm-modal',
  templateUrl: './update-ffm-modal.component.html',
  styleUrls: ['./update-ffm-modal.component.scss']
})
export class UpdateFfmModalComponent implements OnInit {
  @Input() ffm: Fulfillment;

  updateFulfillmentType: UpdateFulfillmentType;

  editData = {
    full_name: '',
    phone: '',
    total_cod_amount: null,
    is_partial_delivery: false,
    admin_note: ''
  };

  addShippingFeeAdjustment = false;

  loading = false;

  constructor(
    private util: UtilService,
    private modalAction: ModalAction,
    private telegram: TelegramService,
    private ffmApi: AdminFulfillmentApi,
    private moneyFormat: VND
  ) { }

  ngOnInit() {}


  closeModal() {
    this.modalAction.close(true);
  }

  selectUpdateType() {
    this.editData = {
      full_name: '',
      phone: '',
      total_cod_amount: null,
      is_partial_delivery: false,
      admin_note: ''
    };
  }

  private validateDataFfm() {
    this.editData = this.util.trimFields(this.editData, ['full_name', 'phone', 'admin_note']);
    if (this.updateFulfillmentType == UpdateFulfillmentType.full_name && !this.editData.full_name) {
      toastr.error('Vui lòng nhập tên mới!');
      return false;
    }
    if (this.updateFulfillmentType == UpdateFulfillmentType.phone && !this.editData.phone) {
      toastr.error('Vui lòng nhập số điện thoại mới!');
      return false;
    }
    if (this.updateFulfillmentType == UpdateFulfillmentType.total_cod_amount
      && !this.editData.total_cod_amount && this.editData.total_cod_amount != 0) {
      toastr.error('Vui lòng nhập tiền thu hộ mới!');
      return false;
    }
    if (!this.editData.admin_note) {
      toastr.error('Vui lòng nhập lý do cập nhật!');
      return false;
    }
    return true;
  }

  async confirm() {
    this.loading = true;
    try {
      const valid = this.validateDataFfm();
      if (!valid) {
        return this.loading = false;
      }
      switch (this.updateFulfillmentType) {
        case UpdateFulfillmentType.total_cod_amount:
          await this.ffmApi.updateFulfillmentCODAmount({
            id: this.ffm.id,
            admin_note: this.editData.admin_note,
            is_partial_delivery: this.editData.is_partial_delivery,
            total_cod_amount: this.editData.total_cod_amount
          });
          break;

        case UpdateFulfillmentType.full_name:
          await this.ffmApi.updateFulfillmentInfo({
            id: this.ffm.id,
            full_name: this.editData.full_name,
            admin_note: this.editData.admin_note,
          });
          break;

        case UpdateFulfillmentType.phone:
          await this.ffmApi.updateFulfillmentInfo({
            id: this.ffm.id,
            phone: this.editData.phone,
            admin_note: this.editData.admin_note,
          });
          break;
      }

      if (this.addShippingFeeAdjustment) {
        await this.ffmApi.addShippingFee({
          id: this.ffm.id,
          shipping_fee_type: FeeType.adjustment
        });
      }

      toastr.success('Cập nhật thông tin đơn giao hàng thành công.');
      this.telegramMessage();
      this.modalAction.dismiss({ffmId: this.ffm.id, addShippingFeeAdjustment: this.addShippingFeeAdjustment});
    } catch(e) {
      debug.error('ERROR in updating ffm', e);
      toastr.error('Cập nhật thông tin đơn giao hàng không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  private telegramMessage() {
    const updatedContent = {
      carrier: this.ffm.carrier_display.toUpperCase(),
      shipping_code: this.ffm.shipping_code,
      shop_name: this.ffm.shop ? this.ffm.shop.name : '',
      customer_name: this.ffm.shipping_address.full_name,
      updated_fields: [
        {
          type: 'Tên khách hàng',
          old_value: this.ffm.shipping_address.full_name,
          new_value: this.editData.full_name,
          checked: this.updateFulfillmentType == UpdateFulfillmentType.full_name
        },
        {
          type: 'Số điện thoại',
          old_value: this.ffm.shipping_address.phone,
          new_value: this.editData.phone,
          checked: this.updateFulfillmentType == UpdateFulfillmentType.phone
        },
        {
          type: 'COD',
          old_value: this.moneyFormat.transform(this.ffm.total_cod_amount) + 'đ',
          new_value: this.moneyFormat.transform(this.editData.total_cod_amount) + 'đ',
          checked: this.updateFulfillmentType == UpdateFulfillmentType.total_cod_amount
        }
      ],
      admin_note: this.editData.admin_note
    };
    this.telegram.updateFfmMessage(updatedContent).then();
  }

}
