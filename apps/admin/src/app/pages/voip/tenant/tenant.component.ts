import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'admin-tenant',
  template: ` 
  <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
    <admin-tenant-list></admin-tenant-list> 
    </ng-template>`,
})
export class TenantComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) {}

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/tenant:view');
  }
  ngOnInit() {}
}
