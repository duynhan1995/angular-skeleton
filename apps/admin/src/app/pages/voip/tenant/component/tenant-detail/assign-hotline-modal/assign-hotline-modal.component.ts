import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { HotlinesApi } from '@etop/api/admin/admin-hotline.api';
import { Hotline } from '@etop/models';
import { HotlineService } from '@etop/state/admin/hotline';
import { TenantQuery, TenantService } from '@etop/state/admin/tenant';
import { MatDialogBaseComponent } from '@etop/web';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'admin-assign-hotline-modal',
  templateUrl: './assign-hotline-modal.component.html',
  styleUrls: ['./assign-hotline-modal.component.scss']
})
export class AssignHotlineModalComponent extends MatDialogBaseComponent implements OnInit {
  hotlineData = new Hotline({});
  hotlines: Hotline[];
  loading = false;

  constructor(
    private dialogRef: MatDialogRef<AssignHotlineModalComponent>,
    private hotlineService: HotlineService,
    private hotlineApi: HotlinesApi,
    private tenantService: TenantService,
    private tenantQuery: TenantQuery
  ) { super() }

  async ngOnInit() {
    let res = await this.hotlineApi.getHotlines({});
    this.hotlines = res?.hotlines?.filter(hotline => hotline.owner_id == '0');
    this.hotlineData = this.hotlines[0];
  }

  closeDialog() {
    this.dialogRef.close(false);
  }

  async addHotlineToTenant() {
    this.loading = true;
    try {
      await this.hotlineService.addHotlineToTenant(this.hotlineData.id, this.tenantQuery.getActive().id);
      await this.tenantService.getTenants();
      await this.hotlineService.getHotlines();
      toastr.success(`Gán hotline thành công.`);
      this.closeDialog();
    } catch (e) {
      await this.hotlineService.getHotlines();
      debug.error('ERROR in activateTenant', e);
      toastr.error(
        `Gán hotline thất bại.`,
        e.code ? e.message || e.msg : ''
      );
    }
    this.loading = false;
  }

  addHotline() {
    this.dialogRef.close(true);
  }

}
