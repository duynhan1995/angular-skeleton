import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { TenantQuery, TenantService } from '@etop/state/admin/tenant';
import { HotlineQuery, HotlineService } from '@etop/state/admin/hotline';
import { AddHotlineModalComponent } from './add-hotline-modal/add-hotline-modal.component';
import { MatDialogController } from '@etop/shared/components/etop-material/material-dialog/mat-dialog.controller';
import { UserQuery } from '@etop/state/admin/user';
import { takeUntil } from 'rxjs/operators';
import { Hotline } from '@etop/models';
import { EditHotlineModalComponent } from './edit-hotline-modal/edit-hotline-modal.component';
import { Router } from '@angular/router';
import { AssignHotlineModalComponent } from './assign-hotline-modal/assign-hotline-modal.component';

@Component({
  selector: 'admin-tenant-detail',
  templateUrl: './tenant-detail.component.html',
  styleUrls: ['./tenant-detail.component.scss'],
})
export class TenantDetailComponent extends BaseComponent implements OnInit {
  tenant$ = this.tenantQuery.selectActive();
  hotlines$ = this.hotlineQuery.selectAll();

  constructor(
    private tenantQuery: TenantQuery,
    private tenantService: TenantService,
    private hotlineQuery: HotlineQuery,
    private hotlineService: HotlineService,
    private matDialogController: MatDialogController,
    private userQuery: UserQuery,
    private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    this.tenant$.pipe(takeUntil(this.destroy$)).subscribe(async (tenant) => {
      const ownerId = tenant?.user?.id;
      await this.getHotlines(ownerId);
    });
  }

  async getHotlines(ownerId) {
    this.hotlineService.setFilter({
      ...this.hotlineQuery.filter,
      owner_id: ownerId,
    });
    await this.hotlineService.getHotlines();
  }

  addHotline() {
    const dialog = this.matDialogController.create({
      component: AddHotlineModalComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  confirmCreateTenant() {
    const name = this.userQuery.getActive()?.full_name;
    const dialog = this.matDialogController.create({
      template: {
        title: `Xác nhận thêm tenant`,
        content: `
              <div>Bạn có chắc chắn muốn thêm tenant cho user <strong>${name}</strong>?</div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: () => {
        this.createTenant();
      },
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async createTenant() {
    try {
      await this.tenantService.createTenant(this.userQuery.getActive());
      toastr.success(`Tạo tenant thành công.`);
    } catch (e) {
      debug.error('ERROR in createTenant', e);
      toastr.error(e.code ? e.message || e.msg : '', `Tạo tenant thất bại.`);
    }
  }

  assignHotline() {
    const dialog = this.matDialogController.create({
      component: AssignHotlineModalComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: (data) => {
        if (data) {
          this.addHotline();
        }
      },
    });
    dialog.open();
  }

  editHotline(hotline: Hotline) {
    this.hotlineService.setActive(hotline);
    const dialog = this.matDialogController.create({
      component: EditHotlineModalComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  userDetail() {
    const id = this.tenantQuery.getActive()?.user?.id;
    this.router.navigateByUrl(`/accounts/users?id=${id}`).then();
  }

  confirmActivateHotline(hotline: Hotline) {
    const dialog = this.matDialogController.create({
      template: {
        title: `Xác nhận kích hoạt Hotline`,
        content: `
              <div>Bạn có chắc chắn muốn kích hoạt Hotline <strong>${hotline.hotline}</strong>?</div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: () => {
        this.activateHotline(hotline);
      },
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async activateHotline(hotline: Hotline) {
    try {
      await this.tenantService.activateTenant(hotline);
      await this.tenantService.getTenants();
      toastr.success(`Kích hoạt hotline thành công.`);
    } catch (e) {
      debug.error('ERROR in activateHotline', e);
      toastr.error(
        `Kích hoạt hotline thất bại.`,
        e.code ? e.message || e.msg : ''
      );
    }
    await this.hotlineService.getHotlines();
  }

  confirmDeleteHotline(hotline: Hotline) {
    const dialog = this.matDialogController.create({
      template: {
        title: `Xác nhận xóa Hotline`,
        content: `
              <div>Bạn có chắc chắn muốn xóa Hotline <strong>${hotline.hotline}</strong>?</div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: () => {
        this.deleteHotline(hotline.id);
      },
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async deleteHotline(id: string) {
    try {
      await this.hotlineService.deleteHotline(id);
      toastr.success(`Xóa hotline thành công.`);
    } catch(e) {
      debug.error('ERROR in removeHotline', e);
      toastr.error(
        `Xóa hotline thất bại.`,
        e.code ? e.message || e.msg : ''
      );
    }
    await this.hotlineService.getHotlines();
  }

  isActive(hotline: Hotline) {
    return hotline.status == 'P';
  } 
}
