import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TenantsAPI } from '@etop/api/admin/admin-tenant.api';
import {CursorPaging, Shop, Tenant, User} from '@etop/models';
import {
  EtopTableComponent,
  SideSliderComponent,
} from '@etop/shared';
import { TenantQuery, TenantService } from '@etop/state/admin/tenant';
import { PageBaseComponent } from '@etop/web';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import {Observable} from "rxjs";

@Component({
  selector: 'admin-tenant-list',
  templateUrl: './tenant-list.component.html',
  styleUrls: ['./tenant-list.component.scss'],
})
export class TenantListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('tenantTable', { static: true }) tenantTable: EtopTableComponent;
  @ViewChild('tenantSlider', { static: true }) tenantSlider: SideSliderComponent;

  emptyTitle: string;
  tenantList$ = this.tenantQuery.selectAll();
  activeTenant$: Observable<Tenant> = this.tenantQuery.selectActive();
  onCreateTenant = false;

  users: User[] = [];
  userId: string;
  userDisplayMap = (user: User) => user && `${user.full_name} - ${user.phone}` || null;
  userValueMap = (user: User) => user && user.id || null;

  get sliderTitle() {
    return this.onCreateTenant ? 'Tạo Tenant' : 'Chi tiết Tenant';
  }
  constructor(
    private headerController: HeaderControllerService,
    private tenantQuery: TenantQuery,
    private tenantService: TenantService,
  ) {
    super();
  }

  get showPaging() {
    return !this.tenantTable.liteMode && !this.tenantTable.loading;
  }

  async ngOnInit() {
    await this.getTenants();
    this.headerController.setActions([
      {
        title: 'Tạo tenant',
        cssClass: 'btn btn-primary',
        onClick: () => this.createTenant(),
        permissions: ['admin/credit:create'],
      },
    ]);
    this.resetState();
  }

  resetState() {
    this.tenantTable.toggleLiteMode(false);
    this.tenantTable.toggleNextDisabled(false);
    this.tenantSlider.toggleLiteMode(false);
    this.tenantService.setActive(null);
  }

  async getTenants() {
    await this.tenantService.getTenants();
  }

  async loadPage({ page, perpage }) {
    let paging: CursorPaging;
    let currentPaging = this.tenantQuery.paging;
    let currentPage = this.tenantQuery.currentPage;

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }
    const filter : TenantsAPI.GetTenantsFilter = {
      owner_id: null
    };
    this.tenantService.setFilter(filter);
    this.tenantService.setPaging(paging); // paging dùng để gửi request lên server
    await this.getTenants();
    this.tenantService.setCurrentPage(page); // set số trang hiện tại
  }

  createTenant() {
    this.onCreateTenant = true;
    this.tenantService.setActive(null);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onCreateTenant = false;
    this.tenantService.setActive(null);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.tenantQuery.getActive() || this.onCreateTenant;
    this.tenantTable.toggleLiteMode(selected);
    this.tenantSlider.toggleLiteMode(selected);
  }

  detail(tenant: Tenant){
    this.tenantService.setActive(tenant);
    this.onCreateTenant = false;
    this._checkSelectMode();
  }

  async onUserSearching(searchText: string) {
    this.users = await this.tenantService.getUsersFilter(searchText);
  }

  onUserSelect() {
    this.tenantService.setFilter({
      owner_id: this.userId || null
    });
    this.tenantService.getTenants(true).then();
    this.resetState();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }
}
