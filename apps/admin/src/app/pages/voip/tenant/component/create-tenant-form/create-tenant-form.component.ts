import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { FilterOperator, Filters, Shop, Tenant, User } from '@etop/models';
import { TenantService } from '@etop/state/admin/tenant';
import { MatDialogController } from '@etop/shared';
import { UserService } from '@etop/state/admin/user';
import { ShopService } from '@etop/state/admin/shop';
import { AdminUserAPI } from '@etop/api/admin/admin-user.api';
import { AdminShopAPI } from '@etop/api/admin/admin-shop.api';

@Component({
  selector: 'admin-create-tenant-form',
  templateUrl: './create-tenant-form.component.html',
  styleUrls: ['./create-tenant-form.component.scss'],
})
export class CreateTenantFormComponent extends BaseComponent implements OnInit {
  newTenant: Tenant;
  selectedUser: User;
  users: User[] = [];
  userId: string;

  selectedShop: Shop;
  shops: Shop[] = [];
  shopId: string;

  loading = false;

  nameDisplayMap = (option) =>
    (option && option.full_name) || null;
  phoneDisplayMap = (option) =>
    (option && `${option.phone}`) || null;
  emailDisplayMap = (option) =>
    (option && `${option.email}`) || null;
  codeDisplayMap = (option) =>
    (option && `${option.code}`) || null;
  shopNameDisplayMap = (option) =>
    (option && `${option.name}`) || null;
  valueMap = (option) => (option && option.id) || null;

  constructor(
    private userService: UserService,
    private shopService: ShopService,
    private tenantService: TenantService,
    private matDialogController: MatDialogController
  ) {
    super();
  }

  ngOnInit() {}

  createTenantConfirmPopup() {
    if (!this.selectedUser && !this.selectedShop) {
      toastr.error('Vui lòng chọn user hoặc shop.');
      return;
    }
    let target = ``;
   
    if(this.selectedShop) {
      target = `shop <strong>${this.selectedShop.name} - ${this.selectedShop.code}</strong>`
    } else {
      target = `user <strong>${this.selectedUser.full_name}</strong>`;
    }
    const dialog = this.matDialogController.create({
      template: {
        title: `Xác nhận thêm tenant`,
        content: `
        <div>Bạn có chắc chắn muốn thêm tenant cho ${target}?</div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: () => {
        this.createTenant();
      },
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async createTenant() {
    try {
      await this.tenantService.createTenant(this.selectedUser,this.selectedShop);
      toastr.success(`Tạo tenant thành công.`);
    } catch (e) {
      debug.error('ERROR in createTenant', e);
      toastr.error(e.code ? e.message || e.msg : '', `Tạo tenant thất bại.`);
    }
  }

  async onUserSearch(searchBy: 'name' | 'phone' | 'email', searchText: string) {
    const filters: AdminUserAPI.GetUserFilters = {};
    filters[searchBy] = searchText;
    this.users =  await this.userService.getUsersList(filters);
  }

  onUserSelect() {
    this.selectedUser = this.users.find((s) => s.id == this.userId);
    this.users = [];
    this.selectedShop = null;
    this.shopId = null;
  }

  async onShopSearch(searchBy: 'name' | 'code',searchText: string) {
    const filter: AdminShopAPI.GetShopsFilter = {}
    let filters: Filters = []
    switch (searchBy) {
      case 'name':
        filter.name = searchText;
        break;
      case 'code':
        filters = [
          { name: searchBy, op: FilterOperator.eq, value: searchText }
        ]
    }
    this.shops =  await this.shopService.getShopList(filters, filter);
  }

  onShopSelect() {
    this.selectedShop = this.shops.find((s) => s.id == this.shopId);
    this.selectedUser = this.selectedShop?.user;
    this.shops = [];
    this.userId = null
  }
}
