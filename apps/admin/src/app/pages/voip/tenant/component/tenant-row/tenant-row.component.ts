import { Component, Input, OnInit } from "@angular/core";
import { Tenant } from "@etop/models/Tenant";

@Component({
  selector: '[admin-tenant-row]',
  templateUrl: './tenant-row.component.html',
  styleUrls: ['./tenant-row.component.scss'],
})
export class TenantRowComponent implements OnInit {
  @Input() tenant = new Tenant({});
  @Input() liteMode = false;
  constructor(
  ) {}

  ngOnInit() {
  }
}
