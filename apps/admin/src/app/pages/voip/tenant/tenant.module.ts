import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule,
  NotPermissionModule,
  SideSliderModule,
} from '@etop/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateTenantFormComponent } from './component/create-tenant-form/create-tenant-form.component';
import { TenantListComponent } from './component/tenant-list/tenant-list.component';
import { TenantRowComponent } from './component/tenant-row/tenant-row.component';
import { TenantComponent } from './tenant.component';
import {TenantDetailComponent} from "./component/tenant-detail/tenant-detail.component";
import {AddHotlineModalComponent} from "./component/tenant-detail/add-hotline-modal/add-hotline-modal.component";
import {EditHotlineModalComponent} from "./component/tenant-detail/edit-hotline-modal/edit-hotline-modal.component";
import { AssignHotlineModalComponent } from './component/tenant-detail/assign-hotline-modal/assign-hotline-modal.component';

const routes: Routes = [
  {
    path: '',
    component: TenantComponent,
  },
];

@NgModule({
  declarations: [
    TenantComponent,
    TenantListComponent,
    TenantRowComponent,
    CreateTenantFormComponent,
    TenantDetailComponent,
    AddHotlineModalComponent,
    EditHotlineModalComponent,
    AssignHotlineModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    MaterialModule,
    EtopMaterialModule,
    EtopCommonModule,
    NotPermissionModule,
    NgbModule,
    EmptyPageModule,
    SideSliderModule,
    EtopPipesModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class TenantModule {}
