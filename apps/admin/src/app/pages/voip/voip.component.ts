import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { TabOpt } from 'apps/core/src/components/header/components/tab-options/tab-options.interface';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';

export enum HeaderTab {
  tenant = 'tenant',
  hotline = 'hotline',
}

@Component({
  selector: 'admin-voip',
  template: `<router-outlet></router-outlet>`
})
export class VoipComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private auth: AuthenticateStore
  ) { }

  ngOnInit() {
    const permissions = this.auth.snapshot.permission.permissions;
    if (permissions.includes('admin/shop:view') && permissions.includes('admin/user:view')) {
      this.setupTabs();
    }
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  setupTabs() {
    let tabs = [
      {
        title: 'Tenant',
        name: 'tenant',
        active: false,
        permissions: ['admin/tenant:view'],
        onClick: () => {
          this.tabClick('tenant').then();
        }
      },
      {
        title: 'Hotline',
        name: 'hotline',
        active: false,
        permissions: ['admin/hotline:view'],
        onClick: () => {
          this.tabClick('hotline').then();
        }
      }
    ];
    const roles = this.auth.snapshot.permission.roles;
      if (roles.includes('ad_voip') && roles.length == 1) {
        tabs = []
      }
    this.headerController.setTabs(tabs.map(tab => this.checkTab(tab)));
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`voip/${type}`);
  }

  private checkTab(tab: TabOpt) {
    const headerTab: any = window.location.pathname.split('/')[2];
    tab.active = headerTab ? headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.tenant;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const _headerTab: any = event.url.split('?')[0].split('/')[2];
        tab.active = _headerTab ? _headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.hotline;
      }
    });
    return tab;
  }

}
