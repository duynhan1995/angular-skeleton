import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VoipComponent } from './voip.component';

const routes: Routes = [
  {
    path: '',
    component: VoipComponent,
    children: [
      {
        path: 'tenant',
        loadChildren: () => import('apps/admin/src/app/pages/voip/tenant/tenant.module').then(m => m.TenantModule)
      },
      {
        path: 'hotline',
        loadChildren: () => import('apps/admin/src/app/pages/voip/hotline/hotline.module').then(m => m.HotlineModule)
      },
      {
        path: '**',
        redirectTo: 'tenant'
      }
    ]
  },
];

@NgModule({
  declarations: [
    VoipComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class VoipModule { }
