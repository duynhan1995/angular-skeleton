import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'admin-hotline',
  template: ` 
  <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
    <admin-hotline-list></admin-hotline-list> 
    </ng-template>`,
})
export class HotlineComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) {}

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/hotline:view');
  }
  ngOnInit() {}

}
