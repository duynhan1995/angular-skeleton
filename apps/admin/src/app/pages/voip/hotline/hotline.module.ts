import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HotlineComponent } from './hotline.component';
import { HotlineListComponent } from './component/hotline-list/hotline-list.component';
import { HotlineRowComponent } from './component/hotline-row/hotline-row.component';
import { FormsModule } from '@angular/forms';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule,
  NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateHotlineFormComponent } from './component/create-hotline-form/create-hotline-form.component';
import { HotlineDetailComponent } from './component/hotline-detail/hotline-detail.component';
import { EditHotlineComponent } from './component/edit-hotline/edit-hotline.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';

const routes: Routes = [
  {
    path: '',
    component: HotlineComponent,
  },
];

@NgModule({
  declarations: [
    HotlineComponent,
    HotlineListComponent,
    HotlineRowComponent,
    CreateHotlineFormComponent,
    HotlineDetailComponent,
    EditHotlineComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    MaterialModule,
    EtopMaterialModule,
    EtopCommonModule,
    NotPermissionModule,
    NgbModule,
    EmptyPageModule,
    SideSliderModule,
    EtopPipesModule,
    EtopFilterModule,
    DropdownActionsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class HotlineModule { }
