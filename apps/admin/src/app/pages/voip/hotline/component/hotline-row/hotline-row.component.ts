import { Component, Input, OnInit } from '@angular/core';
import { Hotline } from '@etop/models';

@Component({
  selector: '[admin-hotline-row]',
  templateUrl: './hotline-row.component.html',
  styleUrls: ['./hotline-row.component.scss']
})
export class HotlineRowComponent implements OnInit {
  @Input() hotline = new Hotline({});
  @Input() liteMode = false;

  constructor() { }

  ngOnInit(): void {
  }

}
