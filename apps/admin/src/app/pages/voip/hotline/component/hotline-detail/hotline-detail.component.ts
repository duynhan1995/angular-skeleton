import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { Tenant } from '@etop/models';
import { MatDialogController } from '@etop/shared';
import { HotlineQuery } from '@etop/state/admin/hotline';
import { TenantService } from '@etop/state/admin/tenant';
import { takeUntil } from 'rxjs/operators';
import { EditHotlineComponent } from '../edit-hotline/edit-hotline.component';

@Component({
  selector: 'admin-hotline-detail',
  templateUrl: './hotline-detail.component.html',
  styleUrls: ['./hotline-detail.component.scss']
})
export class HotlineDetailComponent extends BaseComponent implements OnInit {
  hotline$ = this.hotlineQuery.selectActive();
  tenants: Tenant[];

  constructor(
    private hotlineQuery: HotlineQuery,
    private tenantService: TenantService,
    private matDialogController: MatDialogController
  ) { super() }

  ngOnInit() {
    this.hotline$.pipe(takeUntil(this.destroy$)).subscribe(async _ => {
      if (this.hotlineQuery.getActive()?.owner_id != '0') {
        this.tenants = await this.tenantService.getTenantsByOwnerID(this.hotlineQuery.getActive()?.owner_id);
      } else {
        this.tenants = [];
      }
    })
  }

  editHotline() {
    const dialog = this.matDialogController.create({
      component: EditHotlineComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

}
