import { Component, OnInit } from '@angular/core';
import { Hotline, MobileNetwork } from '@etop/models';
import { HotlineService } from '@etop/state/admin/hotline';

@Component({
  selector: 'admin-create-hotline-form',
  templateUrl: './create-hotline-form.component.html',
  styleUrls: ['./create-hotline-form.component.scss']
})
export class CreateHotlineFormComponent implements OnInit {
  hotlineData = new Hotline({});
  loading = false;

  mobileNetwork = [
    {
      name: 'Không xác định',
      value: MobileNetwork.unknown
    },
    {
      name: 'Viettel',
      value: MobileNetwork.viettel
    },
    {
      name: 'Vinaphone',
      value: MobileNetwork.vinaphone
    },
    {
      name: 'Mobiphone',
      value: MobileNetwork.mobiphone
    }
  ]
  constructor(
    private hotlineService: HotlineService
  ) { }

  ngOnInit(): void {
  }

  async createHotline() {
    if (!this.hotlineData.name) {
      toastr.error('Vui lòng nhập tên hotline');
      return;
    }
    if (!this.hotlineData.hotline) {
      toastr.error('Vui lòng nhập số hotline');
      return;
    }
    this.loading = true;
    try {
      await this.hotlineService.createHotline(this.hotlineData);
      this.hotlineData = new Hotline({});
      await this.hotlineService.getHotlines();
      toastr.success(`Thêm hotline thành công.`);
    } catch (e) {
      debug.error('ERROR in create Hotline', e);
      toastr.error(`Thêm hotline thất bại.`, e.code ? e.message || e.msg : '');
    }
    this.loading = false;
  }
}
