import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HotlinesAPI } from '@etop/api/admin/admin-hotline.api';
import {CursorPaging, FilterOperator, FilterOptions, Filters, Hotline} from '@etop/models';
import { EtopTableComponent, MatDialogController, SideSliderComponent } from '@etop/shared';
import { HotlineQuery, HotlineService } from '@etop/state/admin/hotline';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-hotline-list',
  templateUrl: './hotline-list.component.html',
  styleUrls: ['./hotline-list.component.scss']
})
export class HotlineListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('hotlineTable', { static: true }) hotlineTable: EtopTableComponent;
  @ViewChild('hotlineSlider', { static: true }) hotlineSlider: SideSliderComponent;

  emptyTitle: string;
  hotlineList$ = this.hotlineQuery.selectAll();
  activeHotline$: Observable<Hotline> = this.hotlineQuery.selectActive();
  onCreateHotline = false;
  dropdownActions: DropdownActionOpt[] = [];

  filters: FilterOptions;

  get sliderTitle() {
    return this.onCreateHotline ? 'Tạo Hotline' : 'Chi tiết Hotline';
  }

  get showPaging() {
    return !this.hotlineTable.liteMode && !this.hotlineTable.loading;
  }

  constructor(
    private hotlineService: HotlineService,
    private hotlineQuery: HotlineQuery,
    private headerController: HeaderControllerService,
    private matDialogController: MatDialogController,
  ) { super()}

  async ngOnInit() {
    await this.hotlineService.getHotlines();
    this.headerController.setActions([
      {
        title: 'Tạo hotline',
        cssClass: 'btn btn-primary',
        onClick: () => this.createHotline(),
        permissions: ['admin/hotline:create'],
      },
    ]);
    this.filters =  [
      {
        label: 'Tên hotline',
        name: 'hotline',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
      {
        label: 'Số điện thoại',
        name: 'phone',
        type: 'input',
        fixed: true,
        operator: FilterOperator.contains
      },
    ];
    this.activeHotline$.pipe(takeUntil(this.destroy$))
      .subscribe(activeHotline => {
        if(activeHotline?.status != 'P'){
          this.dropdownActions = [
            {
              title: `Xóa Hotline`,
              cssClass: 'text-danger',
              onClick: () => this.confirmDeleteHotline(activeHotline),
              permissions: ['admin/hotline:delete']
            }
          ];
        } else {
          this.dropdownActions = []
        }
    });
  }

  async loadPage({ page, perpage }) {
    let paging: CursorPaging;
    const filter : HotlinesAPI.GetHotlinesFilter = {
      owner_id: null,
      tenant_id: null
    };
    this.hotlineService.setFilter(filter);
    this.hotlineService.setPaging(paging); // paging dùng để gửi request lên server
    await this.hotlineService.getHotlines();
  }

  createHotline() {
    this.onCreateHotline = true;
    this.hotlineService.setActive(null);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onCreateHotline = false;
    this.hotlineService.setActive(null);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.hotlineQuery.getActive() || this.onCreateHotline;
    this.hotlineTable.toggleLiteMode(selected);
    this.hotlineSlider.toggleLiteMode(selected);
  }

  detail(hotline: Hotline){
    this.hotlineService.setActive(hotline);
    this.onCreateHotline = false;
    this._checkSelectMode();
  }

  filterHotline(filters: Filters) {
    this.hotlineList$ = this.hotlineQuery.selectAll();
    filters.forEach((filter) => {
      if (filter.name == "hotline") {
        this.hotlineList$ = this.hotlineQuery.selectAll({filterBy: (hotline) =>
            hotline.name.includes(filter.value)
        })
      };
      if (filter.name == "phone") {
        this.hotlineList$ = this.hotlineQuery.selectAll({filterBy: (hotline) =>
            hotline.hotline.includes(filter.value)
        })
      }
    })
  }

  confirmDeleteHotline(hotline: Hotline) {
    const dialog = this.matDialogController.create({
      template: {
        title: `Xác nhận xóa Hotline`,
        content: `
              <div>Bạn có chắc chắn muốn xóa Hotline <strong>${hotline.hotline}</strong>?</div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: () => {
        this.deleteHotline(hotline.id);
      },
      config: {
        width: '500px',
      },
      afterClosedCb: () => {},
    });
    dialog.open();
  }

  async deleteHotline(id: string) {
    try {
      await this.hotlineService.deleteHotline(id);
      toastr.success(`Xóa hotline thành công.`);
    } catch(e) {
      debug.error('ERROR in removeHotline', e);
      toastr.error(
        `Xóa hotline thất bại.`,
        e.code ? e.message || e.msg : ''
      );
    }
    await this.hotlineService.getHotlines();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

}
