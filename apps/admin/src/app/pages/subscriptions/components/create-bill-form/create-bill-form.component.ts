import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { FilterOperator, Shop } from '@etop/models';
import { AdminShopApi, AdminShopAPI, AdminSubcriptionAPI, AdminUserApi } from '@etop/api';
import GetShopsRequest = AdminShopAPI.GetShopsRequest;
import CreateSubscriptionBillRequest = AdminSubcriptionAPI.CreateSubscriptionBillRequest;
import { SubscriptionPlan } from '@etop/models/Subscription';
import { SubscriptionQuery } from '@etop/state/admin/subscription/subscription.query';
import CreateSubscriptionRequest = AdminSubcriptionAPI.CreateSubscriptionRequest;
import { SubscriptionService } from '@etop/state/admin/subscription/subscription.service';
import {TelegramService} from "@etop/features";
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'admin-create-bill-form',
  templateUrl: './create-bill-form.component.html',
  styleUrls: ['./create-bill-form.component.scss']
})
export class CreateBillFormComponent implements OnInit {
  loading = false;
  subscriptionPlans$ = this.subscriptionQuery.select(state => state.subscriptionPlans);

  selectedShop: Shop;
  shops: Shop[] = [];
  shopId: string;

  planId:string;
  selectedPlan: SubscriptionPlan;
  billing_cycle_anchor_at: Date;

  newBill: CreateSubscriptionBillRequest = {
    account_id : "",
    customer: null,
    description: "",
    subscription_id: "",
    total_amount: 0
  }

  constructor(
    private shopApi: AdminShopApi,
    private subscriptionQuery: SubscriptionQuery,
    private subscriptionService: SubscriptionService,
    private adminUserApi: AdminUserApi,
    private telegramService: TelegramService,
    private number: DecimalPipe
  ) { }

  ngOnInit(): void {
  }

  displayMap = option => option && `${option.code} - ${option.name}` || null;
  valueMap = option => option && option.id || null;

  displayPlanMap = option => option && `${option.name} - ${this.number.transform(option.price)}đ` || null;
  valuePlanMap = option => option && option.id || null;

  onShopSelect() {
    this.selectedShop = this.shops.find(s => s.id == this.shopId);
    this.newBill.account_id = this.shopId;
    this.shops = [];
  }

  onPlanSelect() {
    const plans = this.subscriptionQuery.getValue().subscriptionPlans;
    this.selectedPlan = plans.find(s => s.id == this.planId);
    this.newBill.total_amount = this.selectedPlan?.price;
  }

  async onShopSearch(searchBy: 'name' | 'phone' | 'email', searchText: string) {
    const query: GetShopsRequest = {
      paging: { offset: 0, limit: 1000 }
    };
    switch (searchBy) {
      case "name":
        query.filter = {name: searchText};
        break;
      case "phone":
        query.filters = [
          {
            name: 'phone',
            op: FilterOperator.eq,
            value: searchText
          }
        ];
        break;
      case "email":
        query.filters = [
          {
            name: 'email',
            op: FilterOperator.eq,
            value: searchText
          }
        ];
        break;
    }
    this.shops = await this.shopApi.getShops(query);
  }

  async createBill() {
    this.loading = true;
    try {
      const customer = await this.adminUserApi.getUser(this.selectedShop.owner_id);
      const createSubscriptionReq: CreateSubscriptionRequest = {
        account_id: this.selectedShop.id,
        billing_cycle_anchor_at: this.billing_cycle_anchor_at,
        cancel_at_period_end: true,
        customer: customer,
        lines: [
          {
            plan_id: this.selectedPlan.id,
            quantity: 1,
          }
        ]
      };
      const createdSubscription = await this.subscriptionService.createSubscription(createSubscriptionReq)

      const createSubscriptionBillReq: CreateSubscriptionBillRequest = {
        account_id: this.selectedShop.id,
        subscription_id: createdSubscription.id,
        total_amount: this.selectedPlan.price,
        description: "",
        customer: customer
      }

      await this.subscriptionService.createSubscriptionBill(createSubscriptionBillReq)
      toastr.success('Tạo subscription bill thành công.');
      this.subscriptionService.getSubscriptions().then(_ => this.subscriptionService.getSubscriptionBills().then())
      this.telegramService.newSubcription(this.selectedShop,this.selectedPlan, customer)
    } catch(e) {
      debug.error('ERROR in createSubscriptionBill', e);
      toastr.error(`Tạo subscription bill không thành công.`, e.code ? (e.message || e.msg) : '');
    }
    this.loading = false;

  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }
}
