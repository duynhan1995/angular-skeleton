import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionsComponent } from './subscriptions.component';
import { RouterModule, Routes } from '@angular/router';
import { EtopMaterialModule, SideSliderModule } from '@etop/shared';
import { CreateBillFormComponent } from './components/create-bill-form/create-bill-form.component';
import { CallCenterModule } from '../../modules/callcenter/callcenter.module';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionsComponent,
    children: [
      {
        path: 'products',
        loadChildren: () =>
          import('apps/admin/src/app/pages/subscriptions/subcription-products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'plans',
        loadChildren: () =>
          import('apps/admin/src/app/pages/subscriptions/subscription-plans/subscription-plans.module').then(m => m.SubscriptionPlansModule)
      },
      {
        path: 'bills',
        loadChildren: () =>
          import('apps/admin/src/app/pages/subscriptions/subscription-bills/subscription-bills.module').then(m => m.SubscriptionBillsModule)
      },
      {
        path: '**',
        redirectTo: 'bills'
      }
    ]
  }
];

@NgModule({
  declarations: [
    SubscriptionsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SideSliderModule,
    EtopMaterialModule,
    CallCenterModule
  ]
})
export class SubscriptionsModule { }
