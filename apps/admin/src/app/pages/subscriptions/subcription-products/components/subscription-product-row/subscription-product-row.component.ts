import { Component, Input, OnInit } from '@angular/core';
import { SubscriptionProduct } from '@etop/models/Subscription';

@Component({
  selector: '[admin-subscription-product-row]',
  templateUrl: './subscription-product-row.component.html',
  styleUrls: ['./subscription-product-row.component.scss']
})
export class SubscriptionProductRowComponent implements OnInit {
  @Input() subscriptionProduct = new SubscriptionProduct();
  constructor() { }

  ngOnInit(): void {
  }

}
