import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'admin-products',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-product-list #subscriptionProductList></admin-product-list>
    </ng-template>`
})
export class ProductsComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) { }

  ngOnInit(): void {
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/subscription_product:view');
  }

}
