import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { SubscriptionStore } from '@etop/state/admin/subscription/subscription.store';
import { SubscriptionService } from '@etop/state/admin/subscription/subscription.service';
import { SubscriptionQuery } from '@etop/state/admin/subscription/subscription.query';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { DropdownActionOpt } from '../../../../../../../shared/src/components/dropdown-actions/dropdown-actions.interface';
import { takeUntil } from 'rxjs/operators';
import { PageBaseComponent } from '@etop/web';

@Component({
  selector: 'admin-product-list',
  templateUrl: './subscription-product-list.component.html',
  styleUrls: ['./subscription-product-list.component.scss']
})
export class SubscriptionProductListComponent extends PageBaseComponent implements OnInit {
  @ViewChild('subscriptionProductTable', { static: true }) subscriptionProductTable: EtopTableComponent;
  @ViewChild('subscriptionProductSlider', { static: true }) subscriptionProductSlider: SideSliderComponent;

  subscriptionsLoading$ = this.subscriptionQuery.selectLoading();
  subscriptionProductsList$ = this.subscriptionQuery.select("subscriptionProducts");
  activeSubscription$ = this.subscriptionQuery.selectActive();


  isLastPage$ = this.subscriptionQuery.select(state => state.ui.isLastPage);
  isEmptyFilterResult$ = this.subscriptionQuery.select(state => state.ui.isEmptyFilterResult);

  dropdownActions: DropdownActionOpt[] = [];

  emptyTitle = 'Chưa có subscription product nào';
  constructor(
    private subscriptionStore: SubscriptionStore,
    private subscriptionService: SubscriptionService,
    private subscriptionQuery: SubscriptionQuery,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptionsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.subscriptionProductTable.loading = loading;
        this.cdr.detectChanges();
      });
    this.subscriptionService.getSubscriptionProducts();
  }

  get showPaging() {
    return !this.subscriptionProductTable.liteMode && !this.subscriptionProductTable.loading;
  }

  get sliderTitle() {
    return 'Chi tiết subscription product';
  }
}
