import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionProductListComponent } from './subscription-product-list/subscription-product-list.component';
import { ProductsComponent } from './products.component';
import { RouterModule, Routes } from '@angular/router';
import {
  EtopCommonModule, EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule,
  NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { AuthenticateModule } from '@etop/core';
import { CallCenterModule } from '../../../modules/callcenter/callcenter.module';
import { DropdownActionsModule } from '../../../../../../shared/src/components/dropdown-actions/dropdown-actions.module';
import { SubscriptionProductRowComponent } from './components/subscription-product-row/subscription-product-row.component';


const routes: Routes = [
  {
    path: '',
    component: ProductsComponent
  }
];
@NgModule({
  declarations: [SubscriptionProductListComponent, ProductsComponent, SubscriptionProductRowComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    EtopFilterModule,
    AuthenticateModule,
    NotPermissionModule,
    CallCenterModule,
    DropdownActionsModule,
  ]
})
export class ProductsModule { }
