import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TabOpt } from '../../../../../core/src/components/header/components/tab-options/tab-options.interface';
import { NavigationEnd, Router } from '@angular/router';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { takeUntil } from 'rxjs/operators';
import { SubscriptionQuery } from '@etop/state/admin/subscription/subscription.query';
import { SideSliderComponent } from '@etop/shared';

export enum HeaderTab {
  bills = 'bills',
  products = 'products',
  plans = 'plans',
}
@Component({
  selector: 'admin-subscriptions',
  template: `
    <router-outlet></router-outlet>
  `
})
export class SubscriptionsComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
  ) { }

  ngOnInit() {
    this.setupTabs();
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Bills',
        name: 'bills',
        active: false,
        permissions: ['admin/subscription_bill:view'],
        onClick: () => {
          this.tabClick('bills').then(_ => {
          });
        }
      },
      {
        title: 'Plans',
        name: 'plans',
        active: false,
        permissions: ['admin/subscription_plan:view'],
        onClick: () => {
          this.tabClick('plans').then(_ => {
          });
        }
      },
      {
        title: 'Products',
        name: 'products',
        active: true,
        permissions: ['admin/subscription_product:view'],
        onClick: () => {
          this.tabClick('products').then(_ => {
          });
        }
      },
    ].map(tab => this.checkTab(tab)));
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`subscriptions/${type}`);
  }

  private checkTab(tab: TabOpt) {
    const headerTab: any = window.location.pathname.split('/')[2];
    tab.active = headerTab ? headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.bills;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const _headerTab: any = event.url.split('?')[0].split('/')[2];
        tab.active = _headerTab ? _headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.bills;
      }
    });
    return tab;
  }

}
