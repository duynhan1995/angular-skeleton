import { Component, Input, OnInit } from '@angular/core';
import { SubscriptionPlan, SubscriptionProduct } from '@etop/models/Subscription';

@Component({
  selector: '[admin-subscription-plan-row]',
  templateUrl: './subscription-plan-row.component.html',
  styleUrls: ['./subscription-plan-row.component.scss']
})
export class SubscriptionPlanRowComponent implements OnInit {
  @Input() subscriptionPlan = new SubscriptionPlan();
  constructor() { }

  ngOnInit(): void {
  }

}
