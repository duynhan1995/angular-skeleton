import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionPlanListComponent } from './subscription-plan-list/subscription-plan-list.component';
import { RouterModule, Routes } from '@angular/router';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { AuthenticateModule } from '@etop/core';
import { CallCenterModule } from '../../../modules/callcenter/callcenter.module';
import { DropdownActionsModule } from '../../../../../../shared/src/components/dropdown-actions/dropdown-actions.module';
import { SubscriptionPlansComponent } from './subscription-plans.component';
import { SubscriptionPlanRowComponent } from './components/subscription-plan-row/subscription-plan-row.component';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionPlansComponent
  }
];
@NgModule({
  declarations: [
    SubscriptionPlansComponent,
    SubscriptionPlanListComponent,
    SubscriptionPlanRowComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    EtopFilterModule,
    AuthenticateModule,
    NotPermissionModule,
    CallCenterModule,
    DropdownActionsModule,
  ]
})
export class SubscriptionPlansModule { }
