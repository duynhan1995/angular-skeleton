import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { DropdownActionOpt } from '../../../../../../../shared/src/components/dropdown-actions/dropdown-actions.interface';
import { SubscriptionStore } from '@etop/state/admin/subscription/subscription.store';
import { SubscriptionService } from '@etop/state/admin/subscription/subscription.service';
import { SubscriptionQuery } from '@etop/state/admin/subscription/subscription.query';
import { takeUntil } from 'rxjs/operators';
import { PageBaseComponent } from '@etop/web';

@Component({
  selector: 'admin-subscription-plan-list',
  templateUrl: './subscription-plan-list.component.html',
  styleUrls: ['./subscription-plan-list.component.scss']
})
export class SubscriptionPlanListComponent extends PageBaseComponent implements OnInit {
  @ViewChild('subscriptionProductTable', { static: true }) subscriptionProductTable: EtopTableComponent;
  @ViewChild('subscriptionProductSlider', { static: true }) subscriptionProductSlider: SideSliderComponent;

  subscriptionsLoading$ = this.subscriptionQuery.selectLoading();
  subscriptionPlansList$ = this.subscriptionQuery.select("subscriptionPlans");

  isLastPage$ = this.subscriptionQuery.select(state => state.ui.isLastPage);
  isEmptyFilterResult$ = this.subscriptionQuery.select(state => state.ui.isEmptyFilterResult);

  dropdownActions: DropdownActionOpt[] = [];

  emptyTitle = 'Chưa có subscription plan nào';
  constructor(
    private subscriptionStore: SubscriptionStore,
    private subscriptionService: SubscriptionService,
    private subscriptionQuery: SubscriptionQuery,
    private cdr: ChangeDetectorRef
  ) {
    super();
  }

  async ngOnInit() {
    this.subscriptionsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.subscriptionProductTable.loading = loading;
        this.cdr.detectChanges();
      });
    await this.subscriptionService.getSubscriptionProducts();
    await this.subscriptionService.getSubscriptionPlans();
  }

  get sliderTitle() {
    return 'Chi tiết subscription plan';
  }

}
