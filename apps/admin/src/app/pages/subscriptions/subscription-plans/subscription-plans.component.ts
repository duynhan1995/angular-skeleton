import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'admin-subscription-plans',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-subscription-plan-list #subscriptionPlanList></admin-subscription-plan-list>
    </ng-template>`
})
export class SubscriptionPlansComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
  ) { }

  ngOnInit(): void {

  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/subscription_plan:view');
  }

}
