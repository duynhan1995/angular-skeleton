import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionBillsComponent } from './subscription-bills.component';
import { RouterModule, Routes } from '@angular/router';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { AuthenticateModule } from '@etop/core';
import { CallCenterModule } from '../../../modules/callcenter/callcenter.module';
import { DropdownActionsModule } from '../../../../../../shared/src/components/dropdown-actions/dropdown-actions.module';
import { SubscriptionBillListComponent } from './subscription-bill-list/subscription-bill-list.component';
import { SubscriptionBillRowComponent } from './components/subscription-bill-row/subscription-bill-row.component';
import { CreateBillFormComponent } from '../components/create-bill-form/create-bill-form.component';


const routes: Routes = [
  {
    path: '',
    component: SubscriptionBillsComponent,
  }
];
@NgModule({
  declarations: [
    SubscriptionBillsComponent,
    SubscriptionBillListComponent,
    SubscriptionBillRowComponent,
    CreateBillFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    EtopFilterModule,
    AuthenticateModule,
    NotPermissionModule,
    CallCenterModule,
    DropdownActionsModule,
  ]
})
export class SubscriptionBillsModule { }
