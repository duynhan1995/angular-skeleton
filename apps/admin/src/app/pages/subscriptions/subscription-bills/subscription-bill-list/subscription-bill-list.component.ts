import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DropdownActionOpt } from '../../../../../../../shared/src/components/dropdown-actions/dropdown-actions.interface';
import { SubscriptionStore } from '@etop/state/admin/subscription/subscription.store';
import { SubscriptionService } from '@etop/state/admin/subscription/subscription.service';
import { SubscriptionQuery } from '@etop/state/admin/subscription/subscription.query';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { takeUntil } from 'rxjs/operators';
import { PageBaseComponent } from '@etop/web';
import { HeaderControllerService } from '../../../../../../../core/src/components/header/header-controller.service';

@Component({
  selector: 'admin-subscription-bill-list',
  templateUrl: './subscription-bill-list.component.html',
  styleUrls: ['./subscription-bill-list.component.scss']
})
export class SubscriptionBillListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('subscriptionBillTable', { static: true }) subscriptionBillTable: EtopTableComponent;
  @ViewChild('subscriptionBillSlider', { static: true }) subscriptionBillSlider: SideSliderComponent;

  isFirstLoadPage = true;

  subscriptionsLoading$ = this.subscriptionQuery.selectLoading();
  subscriptionBillsList$ = this.subscriptionQuery.select(state => state.subscriptionBills);
  activeSubscription$ = this.subscriptionQuery.selectActive();

  isLastPage$ = this.subscriptionQuery.select(state => state.subscriptionBillUi.isLastPage);
  isEmptyFilterResult$ = this.subscriptionQuery.select(state => state.subscriptionBillUi.isEmptyFilterResult);

  dropdownActions: DropdownActionOpt[] = [];

  onNewBill = false;

  constructor(
    private subscriptionStore: SubscriptionStore,
    private subscriptionService: SubscriptionService,
    private subscriptionQuery: SubscriptionQuery,
    private cdr: ChangeDetectorRef,
    private headerController: HeaderControllerService,
  ) {
    super();
  }

  async ngOnInit() {
    this.headerController.setActions([
      {
        title: 'Tạo bill',
        cssClass: 'btn btn-primary',
        onClick: () => this.createBill(),
        permissions: ['admin/subscription_bill:create']
      }
    ]);
    this.subscriptionsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.subscriptionBillTable.loading = loading;
        this.cdr.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.subscriptionBillTable.toggleNextDisabled(true);
          this.subscriptionBillTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả subscription bill.');
        }
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  get showPaging() {
    return !this.subscriptionBillTable.liteMode && !this.subscriptionBillTable.loading;
  }

  get emptyTitle() {
    return 'Chưa có subscription bill nào';
  }

  get sliderTitle() {
    if (this.onNewBill) {
      return 'Tạo subcription bill mới'
    }
    return 'Chi tiết subcription bill';
  }

  resetState() {
    this.subscriptionBillTable.toggleLiteMode(false);
    this.subscriptionBillSlider.toggleLiteMode(false);
    this.subscriptionBillTable.toggleNextDisabled(false);
    this.onNewBill = false;
  }

  onSliderClosed() {
    this.onNewBill = false;
    this._checkSelectMode();
  }

  createBill() {
    this.onNewBill = true;
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    this.subscriptionBillTable.toggleLiteMode(this.onNewBill);
    this.subscriptionBillSlider.toggleLiteMode(this.onNewBill);
  }

  async loadPage({ page, perpage }) {
    if (this.isFirstLoadPage) {
      await this.subscriptionService.getSubscriptionPlans();
      await this.subscriptionService.getSubscriptions();
      this.isFirstLoadPage = false;
    }

    this.subscriptionService.setBillPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    await this.subscriptionService.getSubscriptionBills().then();
  }

}
