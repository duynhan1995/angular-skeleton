import { Component, Input, OnInit } from '@angular/core';
import { SubscriptionBill } from '@etop/models/Subscription';
import { DialogControllerService } from '../../../../../../../../core/src/components/modal-controller/dialog-controller.service';
import { DatePipe, DecimalPipe } from '@angular/common';
import { SubscriptionService } from '@etop/state/admin/subscription/subscription.service';

@Component({
  selector: '[admin-subscription-bill-row]',
  templateUrl: './subscription-bill-row.component.html',
  styleUrls: ['./subscription-bill-row.component.scss']
})
export class SubscriptionBillRowComponent implements OnInit {
  @Input() subscriptionBill: SubscriptionBill = new SubscriptionBill();
  @Input() liteMode = false;
  constructor(
    private dialog: DialogControllerService,
    private number: DecimalPipe,
    private date: DatePipe,
    private subscriptionService: SubscriptionService,
  ) { }

  ngOnInit(): void {
  }

  confirmBill() {
    const {shop, created_at, total_amount} = this.subscriptionBill;
    const {code, name, email, phone} = shop;
    const modal = this.dialog.createConfirmDialog({
      title: `Xác nhận bill`,
      body: `
<div><strong>Shop:</strong> ${code ? code + ' - ' : ''}${name}</div>
<div><strong>Số điện thoại:</strong> ${phone}</div>
<div><strong>Email:</strong> ${email || '-'}</div>
<div>
  <strong>Số tiền:
    <span class="${total_amount < 0 ? 'text-danger' : (total_amount > 0 ? 'text-success' : '')}">${this.number.transform(total_amount)}đ</span>
  </strong>
</div>
<div><strong>Ngày tạo:</strong> ${this.date.transform(created_at, 'dd/MM/yyyy')}</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          // await this.api.confirmCredit(this.credit.id);
          toastr.success('Xác nhận bill thành công.');
          modal.close().then();
          this.subscriptionService.getSubscriptionBills().then();
        } catch (e) {
          debug.error('ERROR in confirmBill', e);
          toastr.error(`Xác nhận bill không thành công.`, e.code ? (e.message || e.msg) : '');
        }
      }
    });
    modal.show().then();
  }
}
