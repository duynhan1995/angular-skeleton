import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { SubscriptionService } from '@etop/state/admin/subscription/subscription.service';

@Component({
  selector: 'admin-subscription-bills',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-subscription-bill-list #subscriptionBillList></admin-subscription-bill-list>
    </ng-template>`
})
export class SubscriptionBillsComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private subscriptionService: SubscriptionService
  ) { }

  async ngOnInit() {
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/subscription_bill:view');
  }

}
