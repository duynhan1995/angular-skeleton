import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CallCenterComponent } from 'apps/admin/src/app/pages/call-center/call-center.component';

const routes: Routes = [
  {
    path: '',
    component: CallCenterComponent
  }
];

@NgModule({
  declarations: [CallCenterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class CallCenterModule { }
