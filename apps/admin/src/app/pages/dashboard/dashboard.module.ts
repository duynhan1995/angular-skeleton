import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { EtopCommonModule, NotPermissionModule } from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    NotPermissionModule,
    SharedModule,
    EtopCommonModule,
    RouterModule.forChild([{
      path: '',
      component: DashboardComponent
    }])
  ]
})
export class DashboardModule { }
