import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/dashboard:view');
  }

  constructor(private auth: AuthenticateStore) { }

  ngOnInit() {
  }

}
