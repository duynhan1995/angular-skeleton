import {Component, Input, OnInit} from '@angular/core';
import {TicketLabel} from "@etop/models";
import {ManageTicketLabelModalComponent} from "apps/admin/src/app/pages/settings/setting-ticket-labels/components/manage-ticket-label-modal/manage-ticket-label-modal.component";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {TicketLabelService} from "@etop/state/shop/ticket-labels";

@Component({
  selector: '[admin-ticket-label-row]',
  templateUrl: './ticket-label-row.component.html',
  styleUrls: ['./ticket-label-row.component.scss']
})
export class TicketLabelRowComponent implements OnInit {
  @Input() label: TicketLabel;
  @Input() parent: TicketLabel;

  constructor(
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private ticketLabelService: TicketLabelService
  ) { }

  validateColor(color: string) {
    return TicketLabelService.validateColor(color);
  }

  ngOnInit() {
  }

  updateLabel() {
    const modal = this.modalController.create({
      component: ManageTicketLabelModalComponent,
      componentProps: {
        parent: this.parent && JSON.parse(JSON.stringify(this.parent)),
        label: JSON.parse(JSON.stringify(this.label))
      }
    });
    modal.show().then();
  }

  deleteLabel() {
    const dialog = this.dialogController.createConfirmDialog({
      title: 'Xoá label',
      body: `
<div>Bạn có chắc muốn xoá label <b>${this.label.name}</b>?</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async() => {
        try {
          await this.ticketLabelService.deleteTicketLabel(this.label.id);
          toastr.success('Xoá label thành công.');
          dialog.close().then();
        } catch(e) {
          toastr.error('Xoá label không thành công.', e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

}
