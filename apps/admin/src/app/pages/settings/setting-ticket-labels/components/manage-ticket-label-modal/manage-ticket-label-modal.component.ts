import {Component, Input, OnInit} from '@angular/core';
import {TicketLabel} from "@etop/models";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {AdminTicketAPI} from "@etop/api/admin/admin-ticket.api";
import {TicketLabelService} from "@etop/state/shop/ticket-labels";

@Component({
  selector: 'admin-create-ticket-label-modal',
  templateUrl: './manage-ticket-label-modal.component.html',
  styleUrls: ['./manage-ticket-label-modal.component.scss']
})
export class ManageTicketLabelModalComponent implements OnInit {
  @Input() parent: TicketLabel;
  @Input() label = new TicketLabel();

  loading = false;

  constructor(
    private modalAction: ModalAction,
    private ticketLabelService: TicketLabelService
  ) { }

  private static validateLabel(label: TicketLabel) {
    const {name, code} = label;
    if (!name || !name.trim()) {
      toastr.error('Vui lòng nhập tên cho label.');
      return false;

    }
    if (!code || !code.trim()) {
      toastr.error('Vui lòng nhập mã cho label.');
      return false;
    }
    return true;
  }

  validateColor(color: string) {
    return TicketLabelService.validateColor(color);
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async confirm() {
    this.loading = true;
    try {
      if (!ManageTicketLabelModalComponent.validateLabel(this.label)) {
        return this.loading = false;
      }
      const {name, code, color, id} = this.label;
      const parent_id = this.parent?.id;

      if (this.label.id) {
        const request: AdminTicketAPI.UpdateTicketLabelRequest = {
          id, name, code, color, parent_id
        };
        await this.ticketLabelService.updateTicketLabel(request);
        toastr.success('Cập nhật label thành công.')
      } else {
        const request: AdminTicketAPI.CreateTicketLabelRequest = {
          name, code, color, parent_id
        };
        await this.ticketLabelService.createTicketLabel(request);
        toastr.success('Tạo label thành công.');
      }

      this.modalAction.dismiss(true);
    } catch(e) {
      toastr.error(`${this.label.id ? 'Cập nhật' : 'Tạo'} label không thành công.`, e.code && (e.message || e.msg));
    }
    this.loading = false;
  }
}
