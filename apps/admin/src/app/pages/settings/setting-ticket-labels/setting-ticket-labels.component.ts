import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {TicketLabel} from "@etop/models";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {ManageTicketLabelModalComponent} from "apps/admin/src/app/pages/settings/setting-ticket-labels/components/manage-ticket-label-modal/manage-ticket-label-modal.component";
import {TicketLabelQuery, TicketLabelService} from "@etop/state/shop/ticket-labels";

@Component({
  selector: 'admin-ticket-labels',
  templateUrl: './setting-ticket-labels.component.html',
  styleUrls: ['./setting-ticket-labels.component.scss']
})
export class SettingTicketLabelsComponent extends PageBaseComponent implements OnInit {

  labelsLoading$ = this.ticketLabelQuery.selectLoading();

  productLabelsList$ = this.ticketLabelQuery.select("ticketProductLabels");
  activeProductLabel$ = this.ticketLabelQuery.select("activeProductLabel");

  subjectLabelsList$ = this.ticketLabelQuery.select(state => state.ticketSubjectLabels
    .filter(l => l.parent_id == this.ticketLabelQuery.getValue().activeProductLabel?.id)
  );
  activeSubjectLabel$ = this.ticketLabelQuery.select("activeSubjectLabel");

  detailLabelsList$ = this.ticketLabelQuery.select(state => state.ticketDetailLabels
    .filter(l => l.parent_id == this.ticketLabelQuery.getValue().activeSubjectLabel?.id));

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private modalController: ModalController,
    private ticketLabelQuery: TicketLabelQuery,
    private ticketLabelService: TicketLabelService
  ) {
    super();
  }

  ngOnInit() {
    this.ticketLabelService.getTicketLabels(true).then();
  }

  selectProductLabel(label: TicketLabel) {
    this.ticketLabelService.selectTicketProductLabel(label);
    this.ticketLabelService.selectTicketSubjectLabel(label.children[0]);
  }

  selectSubjectLabel(label: TicketLabel) {
    this.ticketLabelService.selectTicketSubjectLabel(label);
  }

  trackLabel(index: number, label: TicketLabel) {
    return label.id;
  }

  createProductLabel() {
    const modal = this.modalController.create({
      component: ManageTicketLabelModalComponent
    });
    modal.show().then();
  }

  createSubjectLabel() {
    const modal = this.modalController.create({
      component: ManageTicketLabelModalComponent,
      componentProps: {
        parent: this.ticketLabelQuery.getValue().activeProductLabel
      }
    });
    modal.show().then();
  }

  createDetailLabel() {
    const modal = this.modalController.create({
      component: ManageTicketLabelModalComponent,
      componentProps: {
        parent: this.ticketLabelQuery.getValue().activeSubjectLabel
      }
    });
    modal.show().then();
  }

}
