import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SettingTicketLabelsComponent} from "apps/admin/src/app/pages/settings/setting-ticket-labels/setting-ticket-labels.component";
import {TicketLabelRowComponent} from "apps/admin/src/app/pages/settings/setting-ticket-labels/components/ticket-label-row/ticket-label-row.component";
import {ManageTicketLabelModalComponent} from "apps/admin/src/app/pages/settings/setting-ticket-labels/components/manage-ticket-label-modal/manage-ticket-label-modal.component";
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from "@etop/shared";

const routes: Routes = [
  {
    path: '',
    component: SettingTicketLabelsComponent
  }
];

@NgModule({
  declarations: [
    SettingTicketLabelsComponent,
    TicketLabelRowComponent,
    ManageTicketLabelModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    EtopMaterialModule,
    EtopPipesModule,
  ]
})
export class SettingTicketLabelsModule { }
