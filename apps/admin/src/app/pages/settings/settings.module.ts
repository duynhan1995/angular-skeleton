import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from 'apps/admin/src/app/pages/settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'accounts',
        loadChildren: () =>
          import('apps/admin/src/app/pages/settings/setting-accounts/setting-accounts.module')
            .then(m => m.SettingAccountsModule)
      },
      {
        path: 'labels',
        loadChildren: () =>
          import('apps/admin/src/app/pages/settings/setting-ticket-labels/setting-ticket-labels.module')
            .then(m => m.SettingTicketLabelsModule)
      },
      {
        path: 'callcenter',
        loadChildren: () =>
          import('apps/admin/src/app/pages/settings/setting-callcenter/setting-callcenter.module')
            .then(m => m.SettingCallcenterModule)
      },
      {
        path: '**',
        redirectTo: 'accounts'
      }
    ]
  }
];

@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class SettingsModule { }
