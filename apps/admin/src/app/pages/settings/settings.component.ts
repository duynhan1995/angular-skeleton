import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";

interface SettingMenu {
  title: string;
  route: MenuRoute;
  active: boolean;
  onClick: () => any;
}

export enum MenuRoute {
  accounts = 'accounts',
  labels = 'labels',
  callcenter = 'callcenter',
}

@Component({
  selector: 'admin-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {

  menus: SettingMenu[] = [
    {
      title: 'Quản lý admin',
      route: MenuRoute.accounts,
      active: false,
      onClick: () => {
        this.changeMenu(MenuRoute.accounts)
      }
    },
    {
      title: 'Quản lý label',
      route: MenuRoute.labels,
      active: false,
      onClick: () => {
        this.changeMenu(MenuRoute.labels)
      }
    },
    // {
    //   title: 'Quản lý tổng đài',
    //   route: MenuRoute.callcenter,
    //   active: false,
    //   onClick: () => {
    //     this.changeMenu(MenuRoute.callcenter)
    //   }
    //   hidden: true
    // }
  ];

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    this.menus.map(menu => this.checkMenu(menu));
  }

  changeMenu(route: MenuRoute) {
    this.router.navigateByUrl(`/settings/${route}`).then();
  }

  private checkMenu(menu: SettingMenu) {
    const menuRoute: any = window.location.pathname.split('/')[2];
    menu.active = menuRoute == MenuRoute[menu.route];

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const _menuRoute: any = event.url.split('/')[2];
        menu.active = _menuRoute == MenuRoute[menu.route];
      }
    });
    return menu;
  }

}
