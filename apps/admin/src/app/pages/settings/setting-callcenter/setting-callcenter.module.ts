import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingCallcenterComponent } from './setting-callcenter.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SettingCallcenterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SettingCallcenterComponent
      }
    ])
  ]
})
export class SettingCallcenterModule { }
