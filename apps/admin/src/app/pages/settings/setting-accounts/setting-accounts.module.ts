import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingAccountsComponent } from 'apps/admin/src/app/pages/settings/setting-accounts/setting-accounts.component';
import {RouterModule, Routes} from "@angular/router";
import {AuthenticateModule} from "@etop/core";
import { AccountRowComponent } from './components/account-row/account-row.component';
import {DropdownActionsModule} from "apps/shared/src/components/dropdown-actions/dropdown-actions.module";
import { ManageAdminUserModalComponent } from 'apps/admin/src/app/pages/settings/setting-accounts/components/manage-admin-user-modal/manage-admin-user-modal.component';
import {EtopMaterialModule} from "@etop/shared";

const routes: Routes = [
  {
    path: '',
    component: SettingAccountsComponent
  }
];

@NgModule({
  declarations: [
    SettingAccountsComponent,
    AccountRowComponent,
    ManageAdminUserModalComponent
  ],
  entryComponents: [
    ManageAdminUserModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AuthenticateModule,
    DropdownActionsModule,
    EtopMaterialModule
  ]
})
export class SettingAccountsModule { }
