import { Component, OnInit } from '@angular/core';
import {AdminUser} from "libs/models/admin/AdminUser";
import {AdminAccountApi} from "@etop/api";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {ManageAdminUserModalComponent} from "apps/admin/src/app/pages/settings/setting-accounts/components/manage-admin-user-modal/manage-admin-user-modal.component";

@Component({
  selector: 'admin-setting-accounts',
  templateUrl: './setting-accounts.component.html',
  styleUrls: ['./setting-accounts.component.scss']
})
export class SettingAccountsComponent implements OnInit {
  admins: AdminUser[] = [];

  initializing = true;

  constructor(
    private adminAccountApi: AdminAccountApi,
    private modal: ModalController
  ) { }

  ngOnInit() {
    this.getAdmins().then();
  }

  async getAdmins() {
    this.initializing = true;
    try {
      this.admins = await this.adminAccountApi.getAdminUsers();
    } catch(e) {
      debug.error('ERROR in getAdmins()', e);
    }
    this.initializing = false;
  }

  createAdmin() {
    const modal = this.modal.create({
      component: ManageAdminUserModalComponent
    });
    modal.onDismiss().then(success => {
      if (success) {
        this.getAdmins().then();
      }
    });
    modal.show().then();
  }

}
