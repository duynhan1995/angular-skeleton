import {Component, Input, OnInit} from '@angular/core';
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {AdminUser, AdminUserRole} from "libs/models/admin/AdminUser";
import {AdminAccountAPI, AdminAccountApi} from "@etop/api";
import {FormBuilder} from "@angular/forms";
import CreateAdminUserRequest = AdminAccountAPI.CreateAdminUserRequest;
import UpdateAdminUserRequest = AdminAccountAPI.UpdateAdminUserRequest;

@Component({
  selector: 'admin-create-admin-user-modal',
  templateUrl: './manage-admin-user-modal.component.html',
  styleUrls: ['./manage-admin-user-modal.component.scss']
})
export class ManageAdminUserModalComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private modalAction: ModalAction,
    private adminAccountApi: AdminAccountApi
  ) { }
  @Input() admin: AdminUser;

  adminUserForm = this.fb.group({
    email: '',
    roles: []
  });

  loading = false;

  rolesList = [
    AdminUserRole.admin,
    AdminUserRole.ad_salelead,
    AdminUserRole.ad_sale,
    AdminUserRole.ad_customerservice_lead,
    AdminUserRole.ad_customerservice,
    AdminUserRole.ad_accountant,
    AdminUserRole.ad_voip
  ];

  private static validateAdminUser(adminUser: AdminUser): boolean {
    const {email, roles} = adminUser;
    if (!email || !email.trim()) {
      toastr.error('Vui lòng nhập email!');
      return false;
    }
    if (!roles || !roles.length) {
      toastr.error('Vui lòng chọn vai trò!');
      return false;
    }
    return true;
  }

  displayMap = option => option && AdminAccountApi.adminUserRoleMap([option]) || null;

  ngOnInit() {
    this.prepareForm();
  }

  closeModal() {
    this.modalAction.close(false);
  }

  private prepareForm() {
    if (!this.admin) {
      return;
    }
    this.adminUserForm.patchValue({
      email: this.admin.email,
      roles: this.admin.roles
    })
  }

  async confirm() {
    this.loading = true;
    try {
      const rawData = this.adminUserForm.getRawValue();
      if (!ManageAdminUserModalComponent.validateAdminUser(rawData)) {
        return this.loading = false;
      }
      if (this.admin?.user_id) {
        await this.updateAdminUser({
          user_id: this.admin.user_id,
          roles: rawData.roles
        });
      } else {
        await this.createAdminUser({
          email: rawData.email,
          roles: rawData.roles
        });
      }
      this.modalAction.dismiss(true);
    } catch(e) {
      debug.error('ERROR in confirm()', e);
    }
    this.loading = false;
  }

  async createAdminUser(request: CreateAdminUserRequest) {
    try {
      await this.adminAccountApi.createAdminUser(request);
      toastr.success('Thêm admin thành công.');
    } catch(e) {
      toastr.error('Thêm admin không thành công.');
      throw e;
    }
  }

  async updateAdminUser(request: UpdateAdminUserRequest) {
    try {
      await this.adminAccountApi.updateAdminUser(request);
      toastr.success('Cập nhật admin thành công.');
    } catch(e) {
      toastr.error('Cập nhật admin không thành công.');
      throw e;
    }
  }

}
