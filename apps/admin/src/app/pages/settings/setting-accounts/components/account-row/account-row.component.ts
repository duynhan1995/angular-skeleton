import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AdminUser} from "libs/models/admin/AdminUser";
import {AuthenticateStore} from "@etop/core";
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {ManageAdminUserModalComponent} from "apps/admin/src/app/pages/settings/setting-accounts/components/manage-admin-user-modal/manage-admin-user-modal.component";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {AdminAccountApi} from "@etop/api";

@Component({
  selector: '[admin-account-row]',
  templateUrl: './account-row.component.html',
  styleUrls: ['./account-row.component.scss']
})
export class AccountRowComponent implements OnInit {
  @Input() admin: AdminUser;
  @Output() changed = new EventEmitter();

  dropdownActions: DropdownActionOpt[] = [
    {
      title: 'Cập nhật',
      cssClass: this.isAdminSelf ? 'cursor-not-allowed' : '',
      disabled: this.isAdminSelf,
      permissions: ['admin/admin_user:update'],
      onClick: () => this.updateAdmin()
    },
    {
      title: 'Xoá',
      cssClass: this.isAdminSelf ? 'cursor-not-allowed' : 'text-danger',
      disabled: this.isAdminSelf,
      permissions: ['admin/admin_user:delete'],
      onClick: () => this.deleteAdmin()
    }
  ];

  constructor(
    private adminAccountApi: AdminAccountApi,
    private auth: AuthenticateStore,
    private modal: ModalController,
    private dialog: DialogControllerService
  ) { }

  get isAdminSelf() {
    return this.admin?.user_id == this.auth.snapshot.user?.id;
  }

  ngOnInit() {
  }

  updateAdmin() {
    const modal = this.modal.create({
      component: ManageAdminUserModalComponent,
      componentProps: {
        admin: this.admin
      }
    });
    modal.onDismiss().then(success => {
      if (success) {
        this.changed.emit();
      }
    })
    modal.show().then();
  }

  deleteAdmin() {
    const dialog = this.dialog.createConfirmDialog({
      title: `Xoá admin`,
      body: `
<div>Bạn có chắc muốn xoá <b>${this.admin.full_name}</b> khỏi danh sách admin?</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.adminAccountApi.deleteAdminUser(this.admin.user_id);
          toastr.success('Xoá admin thành công.');
          dialog.close().then();
          this.changed.emit();
        } catch(e) {
          debug.error('ERROR in deleteAdmin()', e);
          toastr.error('Xoá admin không thành công.');
        }
      }
    });
    dialog.show().then();
  }

}
