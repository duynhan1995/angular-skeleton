import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BaseComponent } from '@etop/core';
import {TransactionsService} from "apps/admin/src/app/pages/transactions/transactions.service";

@Component({
  selector: 'admin-transactions',
  template: `<router-outlet></router-outlet>`
})
export class TransactionsComponent extends BaseComponent implements OnInit, OnDestroy {

  headerTabChanged$ = this.transactionStore.state$.pipe(
    map(s => s?.headerTab),
    distinctUntilChanged()
  );

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private transactionStore: TransactionsStore,
    private transactionService: TransactionsService
  ) {
    super();
  }

  ngOnInit() {
    this.setupTabs();
    this.headerTabChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(tab => {
        if (!tab) {
          this.headerController.clearTabs();
        } else {
          this.setupTabs();
        }
      });
    this.transactionService.getConnections().then();
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  checkTab(tabName) {
    return this.headerTabChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Nhà vận chuyển',
        name: 'externals',
        active: false,
        permissions: ['admin/money_transaction_shipping_external:view'],
        onClick: () => {
          this.transactionService.setMtShippingExternalsFilters([])
          this.tabClick('externals').then();
        }
      },
      {
        title: 'Shop',
        name: 'shops',
        active: false,
        permissions: ['admin/money_transaction:view'],
        onClick: () => {
          this.transactionService.setMtShopsFilters([])
          this.tabClick('shops').then();
        }
      },
      {
        title: 'Chuyển khoản',
        name: 'transfers',
        active: false,
        permissions: ['admin/money_transaction_shipping_etop:view'],
        onClick: () => {
          this.transactionService.setMtTransfersFilters([])
          this.tabClick('transfers').then();
        }
      },
      {
        title: 'Dự trù',
        name: 'scheduleds',
        active: false,
        permissions: ['admin/money_transaction:view', 'admin/money_transaction_shipping_etop:view'],
        onClick: () => {
          this.tabClick('scheduleds').then();
        }
      },
      {
        title: 'Tách phiên NVC',
        name: 'splits',
        active: false,
        onClick: () => {
          this.tabClick('splits').then();
        }
      }
    ].map(tab => {
      this.checkTab(tab.name).subscribe(active => tab.active = active);
      return tab;
    }));
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`transactions/${type}`);
    this.transactionStore.changeHeaderTab(type);
  }

}
