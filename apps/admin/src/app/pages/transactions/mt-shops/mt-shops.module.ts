import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule, MaterialModule, NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import {MtShopDetailComponent} from 'apps/admin/src/app/pages/transactions/mt-shops/mt-shop-detail/mt-shop-detail.component';
import {MtShopListComponent} from 'apps/admin/src/app/pages/transactions/mt-shops/mt-shop-list/mt-shop-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'apps/shared/src/shared.module';
import {MultiMtShopsActionComponent} from './components/multi-mt-shops-action/multi-mt-shops-action.component';
import {MtShopRowComponent} from './components/mt-shop-row/mt-shop-row.component';
import {MtShopsComponent} from 'apps/admin/src/app/pages/transactions/mt-shops/mt-shops.component';
import {MtTransferCreateComponent} from './components/mt-transfer-create/mt-transfer-create.component';
import {FormsModule} from '@angular/forms';
import {MtShopFfmRowComponent} from 'apps/admin/src/app/pages/transactions/mt-shops/components/mt-shop-ffm-row/mt-shop-ffm-row.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from "@etop/core";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";

const routes: Routes = [
  {
    path: '',
    component: MtShopsComponent
  },
  {
    path: ':id',
    component: MtShopDetailComponent
  }
];

@NgModule({
  declarations: [
    MtShopDetailComponent,
    MtShopListComponent,
    MultiMtShopsActionComponent,
    MtShopRowComponent,
    MtShopsComponent,
    MtTransferCreateComponent,
    MtShopFfmRowComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    NgbModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopCommonModule,
    SideSliderModule,
    EtopFilterModule,
    NotPermissionModule,
    AuthenticateModule,
    CallCenterModule,
    MaterialModule
  ],
  exports: [
    MtShopRowComponent
  ]
})
export class MtShopsModule {
}
