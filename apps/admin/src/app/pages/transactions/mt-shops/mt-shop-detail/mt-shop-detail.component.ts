import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { EtopTableComponent } from '@etop/shared';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { AdminMoneyTransactionApi, TransactionAPI } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { TelegramService } from '@etop/features';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import {TransactionsService} from "apps/admin/src/app/pages/transactions/transactions.service";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {AuthenticateStore} from "@etop/core";
import {PageBaseComponent} from "@etop/web";

@Component({
  selector: 'admin-mt-shop-detail',
  templateUrl: './mt-shop-detail.component.html',
  styleUrls: ['./mt-shop-detail.component.scss']
})
export class MtShopDetailComponent extends PageBaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('ffmTable', { static: false }) ffmTable: EtopTableComponent;

  sum = {
    delivered_count: 0,
    returned_count: 0,
    cod_count: 0,
    none_cod_count: 0,
  };

  fulfillmentsLoading$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillmentsLoading),
    distinctUntilChanged()
  );
  fulfillments$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillments),
    distinctUntilChanged((a,b) => a?.length == b?.length)
  );

  mtShop$ = this.transactionsStore.state$.pipe(map(s => s?.activeMtShop));

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private headerController: HeaderControllerService,
    private dialog: DialogControllerService,
    private telegram: TelegramService,
    private transactionApi: AdminMoneyTransactionApi,
    private transactionsStore: TransactionsStore,
    private transactionService: TransactionsService,
    private ref: ChangeDetectorRef,
    private router: Router,
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction:view');
  }

  ngOnInit() {
    this.fulfillments$.pipe(takeUntil(this.destroy$))
      .subscribe(ffms => {
        ffms.forEach(ffm => {
          switch (ffm.shipping_state) {
            case 'delivered':
              this.sum.delivered_count += 1;
              break;
            case 'returning':
              this.sum.returned_count += 1;
              break;
            case 'returned':
              this.sum.returned_count += 1;
              break;
            default:
              break;
          }

          if (ffm.total_cod_amount > 0 && !['returned', 'returning'].includes(ffm.shipping_state)) {
            this.sum.cod_count += 1;
          } else {
            this.sum.none_cod_count += 1;
          }
        });
      });

    this.transactionsStore.changeHeaderTab(null);
    const id = this.route.snapshot.paramMap.get('id');
    this.transactionService.getMtShop(id).then();
  }

  ngAfterViewInit() {
    this.fulfillmentsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.ffmTable.loading = loading;
        this.ref.detectChanges();
        if (loading) {
          this.resetState();
        } else {
          this.setupActions();
        }
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  setupActions() {
    const activeMtShop = this.transactionsStore.snapshot.activeMtShop;
    if (['P', 'N'].includes(activeMtShop?.status) || activeMtShop.money_transaction_shipping_etop_id) {
      return this.headerController.clearActions();
    }
    this.headerController.setActions([
      {
        title: 'Xác nhận',
        cssClass: 'btn btn-primary',
        onClick: () => this.confirmTransaction(),
        permissions: ['admin/money_transaction:confirm']
      }
    ]);
  }

  resetState() {
    this.ffmTable.toggleLiteMode(false);
  }

  confirmTransaction() {
    const {activeMtShop} = this.transactionsStore.snapshot;
    const modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiên shop`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiên shop <strong>${activeMtShop.code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const body: TransactionAPI.ConfirmMoneyTransactionShopRequest = {
            money_transaction_id: activeMtShop.id,
            shop_id: activeMtShop.shop_id,
            total_orders: activeMtShop.total_orders,
            total_cod: activeMtShop.total_cod,
            total_amount: activeMtShop.total_amount
          };
          await this.transactionApi.confirmMoneyTransactionShop(body);
          toastr.success('Xác nhận phiên shop thành công.');
          modal.close().then();
          this.transactionService.getMtShop(activeMtShop.id).then();
          this.telegram.confirmMtShop(activeMtShop).then();
        } catch (e) {
          debug.error('ERROR in confirmMoneyTransactionShop', e);
          toastr.error(
            'Xác nhận phiên shop không thành công.',
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  goToTransactionShops() {
    this.router.navigateByUrl('transactions/shops')
  }

}
