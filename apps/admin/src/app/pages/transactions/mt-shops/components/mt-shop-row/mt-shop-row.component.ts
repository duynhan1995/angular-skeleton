import { Component, Input, OnInit } from '@angular/core';
import { MoneyTransactionShop } from 'libs/models/MoneyTransactionShop';
import { Router } from '@angular/router';

@Component({
  selector: '[admin-mt-shop-row]',
  templateUrl: './mt-shop-row.component.html',
  styleUrls: ['./mt-shop-row.component.scss']
})
export class MtShopRowComponent implements OnInit {
  @Input() transaction: MoneyTransactionShop;
  @Input() liteMode = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  transactionDetail() {
    this.router.navigateByUrl(`transactions/shops/${this.transaction.id}`);
  }

}
