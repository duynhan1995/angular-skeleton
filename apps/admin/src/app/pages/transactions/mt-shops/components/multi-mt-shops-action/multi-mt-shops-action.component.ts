import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'admin-multi-mt-shops-action',
  templateUrl: './multi-mt-shops-action.component.html',
  styleUrls: ['./multi-mt-shops-action.component.scss']
})
export class MultiMtShopsActionComponent implements OnInit {
  @Output() toCreateMtTransfer = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  createMoneyTransactionEtop() {
    this.toCreateMtTransfer.emit();
  }

}
