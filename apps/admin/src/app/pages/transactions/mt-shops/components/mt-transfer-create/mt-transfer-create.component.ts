import { Component, OnInit } from '@angular/core';
import { AdminMoneyTransactionApi } from '@etop/api';
import { Router } from '@angular/router';
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {distinctUntilChanged, map} from "rxjs/operators";

@Component({
  selector: 'admin-mt-transfer-create',
  templateUrl: './mt-transfer-create.component.html',
  styleUrls: ['./mt-transfer-create.component.scss']
})
export class MtTransferCreateComponent implements OnInit {
  note: string;
  loading = false;

  activeMtShops$ = this.store.state$.pipe(
    map(s => s?.activeMtShops),
    distinctUntilChanged((a,b) => a?.length == b?.length)
  );

  constructor(
    private router: Router,
    private api: AdminMoneyTransactionApi,
    private store: TransactionsStore
  ) { }

  get sum_orders() {
    const {activeMtShops} = this.store.snapshot;
    return activeMtShops.reduce((a, b) => {
      return a + Number(b.total_orders)
    }, 0)
  }

  get sum_cod() {
    const {activeMtShops} = this.store.snapshot;
    return activeMtShops.reduce((a, b) => {
      return a + Number(b.total_cod)
    }, 0)
  }

  get sum_total_amount() {
    const {activeMtShops} = this.store.snapshot;
    return activeMtShops.reduce((a, b) => {
      return a + Number(b.total_amount)
    }, 0)
  }

  ngOnInit() {
  }

  async createMoneyTransactionEtop() {
    this.loading = true;
    try {
      if (!this.note || !this.note.trim()) {
        toastr.error('Chưa nhập ghi chú.');
        return this.loading = false;
      }
      const {activeMtShops} = this.store.snapshot;
      const ids = activeMtShops.map(mt => mt.id);
      const res = await this.api.createMoneyTransactionTransfer(ids);
      const body = {
        id: res.id,
        note: this.note
      };
      await this.api.updateMoneyTransactionTransfer(body);
      toastr.success('Tạo phiên chuyển khoản thành công.');
      this.router.navigateByUrl('transactions/transfers');
    } catch(e) {
      toastr.error('Tạo phiên chuyển khoản không thành công.', e.code ? (e.message || e.msg) : '');
      debug.error('ERROR in createMoneyTransactionEtop', e);
    }
    this.loading = false;
  }

}
