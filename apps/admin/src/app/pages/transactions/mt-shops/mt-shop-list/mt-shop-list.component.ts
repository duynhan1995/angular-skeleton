import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { MoneyTransactionShop } from 'libs/models/MoneyTransactionShop';
import { TransactionsService } from 'apps/admin/src/app/pages/transactions/transactions.service';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {AuthenticateStore} from "@etop/core";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import { Filters } from '@etop/models';

@Component({
  selector: 'admin-mt-shop-list',
  templateUrl: './mt-shop-list.component.html',
  styleUrls: ['./mt-shop-list.component.scss']
})
export class MtShopListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('transactionTable', { static: true }) transactionTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  shop_id: string;

  mtShopsLoading$ = this.store.state$.pipe(
    map(s => s?.mtShopsLoading),
    distinctUntilChanged()
  );
  mtShopsList$ = this.store.state$.pipe(map(s => s?.mtShops));
  activeMtShops$ = this.store.state$.pipe(
    map(s => s?.activeMtShops),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  isLastPage$ = this.store.state$.pipe(
    map(s => s?.mtShopsIsLastPage),
    distinctUntilChanged()
  );

  onCreateMtTransfer = false;

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private transactionService: TransactionsService,
    private store: TransactionsStore
  ) {
    super();
  }

  get noPermission() {
    const permissions = this.auth.snapshot.permission.permissions;
    return !permissions.includes('admin/money_transaction_shipping_etop:create');
  }

  get sliderTitle() {
    const activeMts = this.store.snapshot.activeMtShops;
    return `Thao tác trên <strong>${activeMts.length}</strong> phiên`;
  }

  get showPaging() {
    return !this.transactionTable.liteMode && !this.transactionTable.loading;
  }

  get emptyResultFilter() {
    const {
      mtShopsPaging,
      mtShopsFilters,
      mtShops
    } = this.store.snapshot;
    return mtShopsPaging.offset == 0 &&
      mtShopsFilters.length > 0 &&
      mtShops.length == 0 &&
      this.shop_id;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy phiên shop phù hợp';
    }
    return 'Chưa có phiên shop';
  }

  isActiveMt(mt: MoneyTransactionShop) {
    const activeMts = this.store.snapshot.activeMtShops;
    return activeMts.some(item => item.id == mt.id);
  }

  ngOnInit() {
    this.mtShopsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.transactionTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.transactionTable.toggleNextDisabled(true);
          this.transactionTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả phiên shop.');
        }
      });
  }

  ngOnDestroy() {
    this.transactionService.selectMtShops([]);
  }

  resetState() {
    this.transactionTable.toggleLiteMode(false);
    this.slider.toggleLiteMode(false);
    this.transactionTable.toggleNextDisabled(false);
    this.onCreateMtTransfer = false;
  }

  filter(filters: Filters, shop_id?: string) {
    this.transactionService.setMtShopsFilters(filters);
    this.shop_id = shop_id;
    this.transactionTable.resetPagination();
  }

  loadPage({ page, perpage }) {
    this.transactionService.setMtShopsPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this.getMtShops();
  }

  getMtShops() {
    this.transactionService.getMtShops(true, this.shop_id).then();
  }

  itemSelected(item: MoneyTransactionShop) {
    const activeMts = this.store.snapshot.activeMtShops;
    activeMts.push(item);
    this.transactionService.selectMtShops(activeMts);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onCreateMtTransfer = false;
    this.transactionService.selectMtShops([]);

    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const activeMts = this.store.snapshot.activeMtShops;
    const selecteds = activeMts.length > 0;
    this.transactionTable.toggleLiteMode(selecteds);
    this.slider.toggleLiteMode(selecteds);
  }

}
