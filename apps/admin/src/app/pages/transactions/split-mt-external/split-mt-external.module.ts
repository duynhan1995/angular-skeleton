import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {EtopMaterialModule, EtopPipesModule, NotPermissionModule} from '@etop/shared';
import { SplitMtExternalComponent } from 'apps/admin/src/app/pages/transactions/split-mt-external/split-mt-external.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import {AuthenticateModule} from "@etop/core";

const routes: Routes = [
  {
    path: '',
    component: SplitMtExternalComponent
  }
]

@NgModule({
  declarations: [SplitMtExternalComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    EtopPipesModule,
    EtopMaterialModule,
    AuthenticateModule,
    NotPermissionModule
  ]
})
export class SplitMtExternalModule { }
