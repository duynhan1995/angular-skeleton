import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import * as XLSX from 'xlsx';
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-split-mt-external',
  templateUrl: './split-mt-external.component.html',
  styleUrls: ['./split-mt-external.component.scss']
})
export class SplitMtExternalComponent extends PageBaseComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private ref: ChangeDetectorRef
  ) {
    super();
  }

  @ViewChild("fileSelector", {static: false}) fileSelector;

  fulfillments: any[] = [];
  abandoneds: any[] = [];
  kepts: any[] = [];

  split_amount = 10;
  split_amount_display: number;

  not_copy: string;
  to_copy: string;

  loading = false;

  private static divideByShop(arr: any[]) {
    const hash = {};
    for (let item of arr) {
      if (!hash[item.key]) {
        hash[item.key] = [item];
      } else {
        hash[item.key] = hash[item.key].concat([item]);
      }
    }
    const result = [];
    for (let key in hash) {
      result.push({
        shop: key,
        value: hash[key]
      });
    }
    return result;
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction:view') &&
      !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_etop:view') &&
      !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_external:view');
  }

  ngOnInit() {
  }

  async fileSelected($event) {
    this.ref.detectChanges();
    this.loading = true;
    try {
      const target: DataTransfer = <DataTransfer>($event.target);
      const reader: FileReader = new FileReader();
      reader.onload = async (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        /* save data */
        let imported_data = (XLSX.utils.sheet_to_json(ws, {header: 1}));
        imported_data.shift();
        imported_data = imported_data.map(item => {
          return {
            shipping_code: item[0].toString(),
            total_cod_amount: item[1],
            created_at: item[2],
            shop_code: item[3].toString(),
            shop_name: item[4],
            key: `${item[3].toString()} - ${item[4]}`
          };
        });
        this.fulfillments = SplitMtExternalComponent.divideByShop(imported_data);
        debug.log('fulfillments', this.fulfillments);
      };
      reader.readAsBinaryString(target.files[0]);
    } catch (e) {
      toastr.error(
        'Có lỗi xảy ra, vui lòng thử lại sau ít phút!',
        e.code ? (e.message || e.msg) : ''
      );
      debug.error('ERROR in selected files', e);
    }
    this.loading = false;
  }

  split() {
    try {
      if (!this.split_amount && this.split_amount != 0) {
        return toastr.error('Nhập số muốn chia đi nào!');
      }
      if (!this.fileSelector.nativeElement.files[0]) {
        return toastr.error('Chưa up file kìa!');
      }
      this.split_amount_display = this.split_amount;
      this.abandoneds = [];
      this.kepts = [];
      const _ffms = [];

      let max_length = 0;
      for (let ffm of this.fulfillments) {
        if (ffm.value.length > max_length) {
          max_length = ffm.value.length;
        }
      }

      for (let i = 0; i < max_length; i++) {
        for (let ffm of this.fulfillments) {
          if (ffm.value[i]) {
            _ffms.push(ffm.value[i]);
          }
        }
      }

      const offset = Math.floor(_ffms.length * this.split_amount / 100);
      const abandon = _ffms.splice(0, offset);
      this.abandoneds.push(...abandon);
      this.kepts.push(..._ffms);

    } catch (e) {
      debug.error('ERROR in splitted', e);
    }
  }

  formatToCopy() {
    if (this.not_copy) {
      this.to_copy = this.not_copy.replace(/\n/g, ', ');
    }
    let to_copies = this.to_copy.split(',');
    to_copies = to_copies.filter(item => item.trim().length).map(item => {
      const _split = item.split('.');
      if (_split && _split.length) {
        item = _split[_split.length - 1];
      }
      return `'${item.trim()}'`;
    });
    this.to_copy = to_copies.join(', ');
  }

}
