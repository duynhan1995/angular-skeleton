import { Injectable } from '@angular/core';
import {AdminConnectionApi, AdminFulfillmentApi, AdminShopApi, AdminMoneyTransactionApi} from '@etop/api';
import {Fulfillment} from "libs/models/Fulfillment";
import {Connection} from "libs/models/Connection";
import {HeaderTab, TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {NavigationEnd, Router} from "@angular/router";
import {Paging} from "@etop/shared";
import {
  MoneyTransactionShippingExternal,
  MoneyTransactionShippingExternalLine
} from "libs/models/admin/MoneyTransactionShippingExternal";
import {MoneyTransactionShop} from "libs/models/MoneyTransactionShop";
import {BangKe, MoneyTransactionTransfer} from "libs/models/admin/MoneyTransactionTransfer";
import {BankAccount} from "libs/models/Bank";
import { StringHandler } from '@etop/utils';
import {DatePipe} from "@angular/common";
import {BankQuery} from "@etop/state/bank";
import { FilterOperator, Filters } from '@etop/models';
import {TelegramService} from "@etop/features";

@Injectable()
export class TransactionsService {

  constructor(
    private router: Router,
    private transactionApi: AdminMoneyTransactionApi,
    private shopApi: AdminShopApi,
    private store: TransactionsStore,
    private connectionApi: AdminConnectionApi,
    private fulfillmentApi: AdminFulfillmentApi,
    private dateFormat: DatePipe,
    private bankQuery: BankQuery,
    private telegram: TelegramService
  ) {
    const tab: any = window.location.pathname.split('/')[2];
    if (tab) {
      this.store.changeHeaderTab(tab);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('transactions') != -1) {
          const _tab: any = event.url.split('/')[2];
          if (_tab) {
            this.store.changeHeaderTab(_tab);
          } else {
            this.store.changeHeaderTab(HeaderTab.externals);
          }
        }
      }
    });
  }

  private static mtShippingExternalReMap(
    mt: MoneyTransactionShippingExternal, connections: Connection[]
  ): MoneyTransactionShippingExternal {

    const _connection = connections.find(conn => conn.id == mt.connection_id);

    return {
      ...mt,
      carrier_display: _connection ? _connection.name : mt.carrier,
      provider_logo: _connection ? _connection.provider_logo : Connection.getProviderLogo(mt.carrier)
    };
  }

  static trackingLink(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return null;
    }
    switch (fulfillment.carrier) {
      case 'ghn':
        return `https://donhang.ghn.vn/?order_code=${fulfillment.shipping_code}&code=${fulfillment.shipping_code}`;
      case 'vtpost':
        return `https://old.viettelpost.com.vn/Tracking?KEY=${fulfillment.shipping_code}`;
      case 'ahamove':
        return fulfillment.shipping_shared_link;
      default:
        return null;
    }
  }

  static mapFulfillmentsByMtShippingExternalLines(ffms: Fulfillment[], lines: MoneyTransactionShippingExternalLine[]) {
    return lines.map(line => {
      const _found_ffm = ffms.find(f => f.id == line.etop_fulfillment_id);
      if (!_found_ffm) {
        return new Fulfillment({
          not_found_on_etop: true,
          external_code: line.external_code,
          external_customer: line.external_customer,
          external_address: line.external_address,
          external_total_cod: line.external_total_cod,
          external_total_shipping_fee: line.external_total_shipping_fee,
          external_import_error: line.import_error,
          money_transaction_shipping_external_line_id: line.id,
          id: line.id
        });
      }
      return {
        ..._found_ffm,
        external_import_error: line.import_error,
        money_transaction_shipping_external_line_id: line.id
      };
    });
  }

  getMoneyTransactionTransfers(start?: number, perpage?: number, filters?: Filters) {
    const paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return this.transactionApi.getMoneyTransactionTransfers({paging, filters});
  }

  async getEveryMoneyTransactionShop(filters: Filters, shop_id?: string) {
    let mts = [];
    let offset = 0;

    while (true) {
      const _mts = await this.transactionApi.getMoneyTransactionShops({
        paging: { offset, limit: 1000 },
        filters,
        shop_id
      });

      mts = mts.concat(_mts);
      if (!_mts.length || _mts.length < 1000) break;
      offset += 1000;
    }
    return mts;
  }

  async getConnections() {
    try {
      const connections = await this.connectionApi.getConnections();
      this.store.setConnections(connections);
    } catch(e) {
      debug.error('ERROR in getting Connections', e);
    }
  }

  async getEveryFulfillment(doLoading = true, filters: Filters) {
    let ffms = [];
    let offset = 0;

    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      while (true) {
        const res: any = await this.fulfillmentApi.getFulfillments({filters, paging: {limit: 1000, offset}});

        ffms = ffms.concat(res.fulfillments);
        if (!res.fulfillments.length || res.fulfillments.length < 1000) break;
        offset += 1000;
      }

    } catch(e) {
      debug.error('ERROR in getting every Fulfillments', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
    return ffms.map(f => Fulfillment.fulfillmentMap(f, this.store.snapshot.connections));
  }

  selectFulfillments(ffms: Fulfillment[]) {
    this.store.selectFulfillments(ffms);
  }

  // NOTE: phiên NVC

  setMtShippingExternalsFilters(filters: Filters) {
    this.store.setMtShippingExternalsFilters(filters);
  }

  setMtShippingExternalsPaging(paging: Paging) {
    this.store.setMtShippingExternalsPaging(paging);
  }

  async getMtShippingExternal(id: string, doLoading = true) {
    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      const {connections} = this.store.snapshot;
      const transaction = await this.transactionApi.getMoneyTransactionShippingExternal(id);
      this.store.detailMtShippingExternal(TransactionsService.mtShippingExternalReMap(transaction, connections));

      const filters: Filters = [{
        name: 'money_transaction_shipping_external_id',
        op: FilterOperator.eq,
        value: transaction.id
      }];
      const _ffms = await this.getEveryFulfillment(false, filters);

      this.store.setFulfillments(TransactionsService.mapFulfillmentsByMtShippingExternalLines(_ffms, transaction.lines));

    } catch(e) {
      debug.error('ERROR in getMtShippingExternal Detail', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
  }

  async getMtShippingExternals(doLoading = true) {
    this.store.setMtShippingExternalsLastPage(false);
    if (doLoading) {
      this.store.setMtShippingExternalsLoading(true);
    }
    try {
      const {mtShippingExternalsFilters, mtShippingExternalsPaging, connections} = this.store.snapshot;
      const transactions = await this.transactionApi.getMoneyTransactionShippingExternals({
        filters: mtShippingExternalsFilters,
        paging: mtShippingExternalsPaging
      });
      if (mtShippingExternalsPaging.offset > 0 && transactions.length == 0) {
        this.store.setMtShippingExternalsLastPage(true);
      } else {
        const mts = transactions.map(mt => TransactionsService.mtShippingExternalReMap(mt,connections));
        this.store.setMtShippingExternals(mts);
        if (doLoading) {
          this.selectMtShippingExternals([]);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Transactions', e);
    }
    if (doLoading) {
      this.store.setMtShippingExternalsLoading(false);
    }
  }

  selectMtShippingExternals(mts: MoneyTransactionShippingExternal[]) {
    this.store.selectMtShippingExternals(mts);
  }

  async importMoneyTransactionShippingExternal(form: FormData) {
    await this.transactionApi.importMoneyTransactionShippingExternal(form);
    this.getMtShippingExternals().then();
  }

  async confirmMoneyTransactionShippingExternals(ids: string[]) {
    await this.transactionApi.confirmMoneyTransactionShippingExternals(ids);
    this.getMtShippingExternals().then();
  }

  async splitMoneyTransactionShippingExternal(transaction: MoneyTransactionShippingExternal) {
    await this.transactionApi.splitMoneyTransactionShippingExternal(transaction.id);
    this.telegram.splitMtShippingExternalMessage(transaction).then();
  }

  // NOTE: phiên SHOP

  setMtShopsFilters(filters: Filters) {
    this.store.setMtShopsFilters(filters);
  }

  setMtShopsPaging(paging: Paging) {
    this.store.setMtShopsPaging(paging);
  }

  async getMtShop(id: string, doLoading = true) {
    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      const transaction = await this.transactionApi.getMoneyTransactionShop(id);
      transaction.shop = await this.shopApi.getShop(transaction.shop_id);

      this.store.detailMtShop(transaction);

      const filters: Filters = [{
        name: 'money_transaction.id',
        op: FilterOperator.eq,
        value: transaction.id
      }];
      const _ffms = await this.getEveryFulfillment(false, filters);


      this.store.setFulfillments(_ffms);

    } catch(e) {
      debug.error('ERROR in getMtShop Detail', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
  }

  async getMtShops(doLoading = true, shop_id?: string) {
    this.store.setMtShopsLastPage(false);
    if (doLoading) {
      this.store.setMtShopsLoading(true);
    }
    try {
      const {mtShopsFilters, mtShopsPaging} = this.store.snapshot;
      const transactions = await this.transactionApi.getMoneyTransactionShops({
        filters: mtShopsFilters,
        paging: mtShopsPaging,
        shop_id
      });
      if (mtShopsPaging.offset > 0 && transactions.length == 0) {
        this.store.setMtShopsLastPage(true);
      } else {
        const shopIds = transactions.map(mt => mt.shop_id);
        const shops = await this.shopApi.getShopsByIDs(shopIds);

        this.store.setMtShops(transactions.map(mt => {
          mt.shop = shops && shops.find(s => s.id == mt.shop_id);
          return mt;
        }));

        if (doLoading) {
          this.selectMtShops([]);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Transactions', e);
    }
    if (doLoading) {
      this.store.setMtShopsLoading(false);
    }
  }

  selectMtShops(mts: MoneyTransactionShop[]) {
    this.store.selectMtShops(mts);
  }

  // NOTE: phiên ETOP

  setMtTransfersFilters(filters: Filters) {
    this.store.setMtTransfersFilters(filters);
  }

  setMtTransfersPaging(paging: Paging) {
    this.store.setMtTransfersPaging(paging);
  }

  async getMtTransfer(id: string, doLoading = true) {
    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      const transaction = await this.transactionApi.getMoneyTransactionTransfer(id);

      const mtShops = await this.getEveryMoneyTransactionShop([
        { name: 'money_transaction_shipping_etop_id', op: FilterOperator.eq, value: transaction.id }
      ]);
      const shopIds = mtShops.map(mt => mt.shop_id);
      const shops = await this.shopApi.getShopsByIDs(shopIds);
      mtShops.forEach(mt => {
        mt.shop = shops && shops.find(s => s.id == mt.shop_id);
        mt.bank_account = mt.shop.bank_account;
      });

      const res = this.getBangKeLines(transaction, mtShops);
      if (res) {
        const { validBangKeLines, invalidBangKeLines } = res;
        transaction.valid_bang_ke_lines = validBangKeLines;
        transaction.invalid_bang_ke_lines = invalidBangKeLines;
      }

      this.store.detailMtTransfer(transaction);
      this.store.setMtShops(mtShops);

      const filters: Filters = [{
        name: 'money_transaction.id',
        op: FilterOperator.in,
        value: mtShops.map(mt => mt.id).join(',')
      }];
      const _ffms = await this.getEveryFulfillment(false, filters);


      this.store.setFulfillments(_ffms);

    } catch(e) {
      debug.error('ERROR in getMtTransfer Detail', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
  }

  async getMtTransfers(doLoading = true) {
    this.store.setMtTransfersLastPage(false);
    if (doLoading) {
      this.store.setMtTransfersLoading(true);
    }
    try {
      const {mtTransfersFilters, mtTransfersPaging} = this.store.snapshot;
      const transactions = await this.transactionApi.getMoneyTransactionTransfers({
        filters: mtTransfersFilters,
        paging: mtTransfersPaging
      });
      if (mtTransfersPaging.offset > 0 && transactions.length == 0) {
        this.store.setMtTransfersLastPage(true);
      } else {
        const mtTransferIds = transactions.map(mt => mt.id).join(', ');
        const mtShops = await this.getEveryMoneyTransactionShop([
          {
            name: 'money_transaction_shipping_etop_id',
            op: FilterOperator.in,
            value: mtTransferIds
          }
        ]);

        this.store.setMtTransfers(transactions.map(mt => {
          mt.money_transactions = mtShops.filter(mtShop => mtShop.money_transaction_shipping_etop_id == mt.id);
          return mt;
        }));

        if (doLoading) {
          this.selectMtTransfers([]);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Transactions', e);
    }
    if (doLoading) {
      this.store.setMtTransfersLoading(false);
    }
  }

  selectMtTransfers(mts: MoneyTransactionTransfer[]) {
    this.store.selectMtTransfers(mts);
  }

  private getBangKeLines(mtTransfer: MoneyTransactionTransfer, mtShops: MoneyTransactionShop[]) {
    try {
      const bangKeLines: BangKe[] = [];
      for (let mt of mtShops) {
        const line = bangKeLines.find(l => l.shop_id == mt.shop_id);
        if (line) {
          line.total_amount += mt.total_amount;
        } else {
          bangKeLines.push(this.createBangKeLine(mtTransfer, mt));
        }
      }
      const validBangKeLines = [];
      const invalidBangKeLines = [];
      bangKeLines.forEach(l => {

        let fee = AdminMoneyTransactionApi.transferFee.liennganhang;
        if (l.bank_account && l.bank_account.name.includes('Vietcombank')) {
          fee = AdminMoneyTransactionApi.transferFee.vietcombank;
        }

        l.total_amount -= fee;
        if (l.total_amount >= 50000) {
          validBangKeLines.push(l);
        } else {
          invalidBangKeLines.push(l);
        }
      });
      return {
        validBangKeLines, invalidBangKeLines
      };
    } catch (e) {
      debug.error('ERROR in getBangKeLines', e);
      return null;
    }
  }

  private createBangKeLine(mtTransfer: MoneyTransactionTransfer, mtShop: MoneyTransactionShop): BangKe {
    try {
      const tcbBranches = this.bankQuery.getValue().techcombankBranches;
      let TCB = mtShop.bank_account && tcbBranches[mtShop.bank_account.branch];
      if (mtShop.bank_account && mtShop.bank_account.name && mtShop.bank_account.name.includes('Vietcombank')) {
        TCB = tcbBranches['NHTMCP Ngoai Thuong VN'];
      }
      return new BangKe({
        id: mtShop.shop_id,
        shop_id: mtShop.shop_id,
        shop_name: mtShop.shop.name,
        ref: `${mtTransfer.code.replace('Mt-', '').replace('ETOP.', '')}.${mtShop.shop.code}`,
        bank_account: mtShop.bank_account,
        total_amount: mtShop.total_amount,
        currency_unit: 'vnd',
        content: StringHandler.convertVietnameseString(
`TOPSHIP CHUYEN TIEN ${this.dateFormat.transform(mtTransfer.created_at, 'yyyy-MM-dd')} COD DA CAN TRU CHO SHOP ${mtShop.shop.name}`
        ).toUpperCase(),
        bank_account_TCB: TCB ? {
          ...mtShop.bank_account,
          name: TCB.name,
          branch: TCB.region == 'Ca nuoc' ? '-' : TCB.branch,
          region: TCB.region == 'Ca nuoc' ? '-' : TCB.region
        } : new BankAccount()
      });
    } catch(e) {
      debug.error('ERROR in create BangKe', e);
    }
  }

}
