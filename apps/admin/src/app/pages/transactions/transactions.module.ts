import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TransactionsComponent } from './transactions.component';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import {TransactionsService} from "apps/admin/src/app/pages/transactions/transactions.service";

const routes: Routes = [
  {
    path: '',
    component: TransactionsComponent,
    children: [
      {
        path: 'externals',
        loadChildren: () =>
          import('apps/admin/src/app/pages/transactions/mt-shipping-externals/mt-shipping-externals.module').then(m => m.MtShippingExternalsModule)
      },
      {
        path: 'shops',
        loadChildren: () =>
          import('apps/admin/src/app/pages/transactions/mt-shops/mt-shops.module').then(m => m.MtShopsModule)
      },
      {
        path: 'transfers',
        loadChildren: () =>
          import('apps/admin/src/app/pages/transactions/mt-transfers/mt-transfers.module').then(m => m.MtTransfersModule)
      },
      {
        path: 'scheduleds',
        loadChildren: () =>
          import('apps/admin/src/app/pages/transactions/mt-scheduleds/mt-scheduleds.module').then(m => m.MtScheduledsModule)
      },
      {
        path: 'splits',
        loadChildren: () =>
          import('apps/admin/src/app/pages/transactions/split-mt-external/split-mt-external.module').then(m => m.SplitMtExternalModule)
      },
      {
        path: '**',
        redirectTo: 'externals'
      }
    ]
  }
];

@NgModule({
  declarations: [TransactionsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    TransactionsStore,
    TransactionsService,
    DatePipe
  ]
})
export class TransactionsModule { }
