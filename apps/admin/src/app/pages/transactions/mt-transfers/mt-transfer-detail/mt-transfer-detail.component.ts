import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminMoneyTransactionApi, TransactionAPI } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { TelegramService } from '@etop/features';
import { MoneyTransactionShop } from '@etop/models';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import * as XLSX from 'xlsx';
import { TransactionsService } from 'apps/admin/src/app/pages/transactions/transactions.service';
import { StringHandler } from '@etop/utils';
import {BankQuery} from "@etop/state/bank";

@Component({
  selector: 'admin-mt-transfer-detail',
  templateUrl: './mt-transfer-detail.component.html',
  styleUrls: ['./mt-transfer-detail.component.scss']
})
export class MtTransferDetailComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('mtShopTable', { static: false }) mtShopTable: EtopTableComponent;
  @ViewChild('slider', { static: false }) slider: SideSliderComponent;

  activeTab: 'detail' | 'bang_ke' | 'fulfillment' = 'detail';

  onAddMoreMtShop = false;

  dropdownActions = [
    {
      title: `Xoá phiên shop`,
      cssClass: 'text-danger',
      onClick: () => this.removeMtShops()
    }
  ];

  fulfillmentsLoading$ = this.transactionStore.state$.pipe(
    map(s => s?.fulfillmentsLoading),
    distinctUntilChanged()
  );
  fulfillments$ = this.transactionStore.state$.pipe(
    map(s => s?.fulfillments),
    distinctUntilChanged((a,b) => a?.length == b?.length)
  );

  mtShops$ = this.transactionStore.state$.pipe(map(s => s?.mtShops));
  activeMtShops$ = this.transactionStore.state$.pipe(map(s => s?.activeMtShops));

  mtTransfer$ = this.transactionStore.state$.pipe(map(s => s?.activeMtTransfer));

  bankReady$ = this.bankQuery.select(state => !!state.bankReady);

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private router: Router,
    private ref: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private dialog: DialogControllerService,
    private telegram: TelegramService,
    private transactionApi: AdminMoneyTransactionApi,
    private transactionStore: TransactionsStore,
    private transactionService: TransactionsService,
    private dateFormat: DatePipe,
    private bankQuery: BankQuery
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_etop:view');
  }

  get noPermissionUpdate() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_etop:update');
  }

  get sliderTitle() {
    if (this.onAddMoreMtShop) {
      return 'Thêm phiên shop';
    }
    const {activeMtShops} = this.transactionStore.snapshot;
    return `Thao tác trên <strong>${activeMtShops.length}</strong> phiên shop`;
  }

  get isTCB() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    return activeMtTransfer?.note?.indexOf('Techcombank') != -1;
  }

  get isVCB() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    return activeMtTransfer?.note?.indexOf('Vietcombank') != -1;
  }

  get notTCBExportData() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    const data = [];
    for (let list of activeMtTransfer.valid_bang_ke_lines) {
      data.push({
        'Txn Reference': `${list.ref}`,
        'Amount (VND)': `${list.total_amount}`,
        'Benificiary Name': `${list.bank_account.account_name ?
          StringHandler.convertVietnameseString(list.bank_account.account_name).toUpperCase() : ''}`,
        'Account Number': `${list.bank_account.account_number}`,
        'Remarks': `${list.content}`,
        'Ben Bank': `${list.bank_account_TCB.name}`,
        'Province': `${list.bank_account_TCB.region == '-' ? '' : list.bank_account_TCB.region}`,
        'Branch': `${list.bank_account_TCB.branch == '-' ? '' : list.bank_account_TCB.branch}`,
        'Validation': ''
      });
    }
    return data;
  }

  get TCBExportData() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    const data = [];
    for (let list of activeMtTransfer.valid_bang_ke_lines) {
      data.push({
        'TÀI KHOẢN ĐÍCH': `${list.bank_account.account_number}`,
        'SỐ TIỀN CHUYỂN': `${list.total_amount}`,
        'DIỄN GIẢI': `${list.content}`
      });
    }
    return data;
  }

  isActiveMtShop(mtShop: MoneyTransactionShop) {
    const activeMtShops = this.transactionStore.snapshot.activeMtShops;
    return activeMtShops.some(item => item.id == mtShop.id);
  }

  ngOnInit() {
    this.transactionStore.changeHeaderTab(null);

    if (this.bankQuery.getValue().bankReady) {
      const id = this.route.snapshot.paramMap.get('id');
      this.transactionService.getMtTransfer(id).then();
    } else {
      this.bankReady$.pipe(takeUntil(this.destroy$))
        .subscribe(async() => {
          const id = this.route.snapshot.paramMap.get('id');
          this.transactionService.getMtTransfer(id).then();
        });
    }
  }

  ngAfterViewInit() {
    this.fulfillmentsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.mtShopTable.loading = loading;
        this.ref.detectChanges();
        if (loading) {
          this.resetState();
        } else {
          this.setupTabs();
          this.setupActions();
        }
      });
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.headerController.clearActions();
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Chi tiết phiên',
        name: 'detail',
        active: this.activeTab == 'detail',
        onClick: () => {
          this.tabClick('detail');
        }
      },
      {
        title: 'Bảng kê',
        name: 'bang_ke',
        active: this.activeTab == 'bang_ke',
        onClick: () => {
          this.tabClick('bang_ke');
        }
      },
      {
        title: 'Đơn giao hàng',
        name: 'fulfillment',
        active: this.activeTab == 'fulfillment',
        onClick: () => {
          this.tabClick('fulfillment');
        }
      }
    ]);
  }

  private tabClick(tab) {
    this.activeTab = tab;
    switch (this.activeTab) {
      case 'detail':
        return this.setupActions();
      case 'bang_ke':
        return this.headerController.setActions([
          {
            title: 'Xuất excel',
            cssClass: 'btn btn-primary',
            onClick: () => this.exportExcel()
          }
        ]);
      default:
        return this.headerController.clearActions();
    }
  }

  setupActions() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    if (['P', 'N'].includes(activeMtTransfer.status)) {
      return this.headerController.clearActions();
    }
    this.headerController.setActions([
      {
        title: 'Xoá',
        cssClass: 'btn btn-outline btn-danger',
        onClick: () => this.deleteTransaction(),
        permissions: ['admin/money_transaction_shipping_etop:delete']
      },
      {
        title: `<i class="fa fa-plus text-primary"></i> Thêm phiên shop`,
        cssClass: 'btn btn-outline btn-primary',
        onClick: () => this.addMoreMtShop(),
        permissions: ['admin/money_transaction_shipping_etop:update']
      },
      {
        title: 'Xác nhận',
        cssClass: 'btn btn-primary',
        onClick: () => this.confirmTransaction(),
        permissions: ['admin/money_transaction_shipping_etop:confirm']
      }
    ]);
  }

  resetState() {
    this.slider.toggleLiteMode(false);
    this.mtShopTable.toggleLiteMode(false);
    this.onAddMoreMtShop = false;
  }

  deleteTransaction() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá phiên chuyển khoản`,
      body: `
        <div>Bạn có chắc muốn xoá phiên chuyển khoản <strong>${activeMtTransfer.code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmCss: 'btn-danger',
      confirmTitle: 'Xoá',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.transactionApi.deleteMoneyTransactionTransfer(activeMtTransfer.id);
          toastr.success(`Xoá phiên chuyển khoản thành công.`);
          modal.close().then();
          this.router.navigateByUrl('transactions/transfers').then();
        } catch (e) {
          debug.error('ERROR in deleteMoneyTransactionTransfer', e);
          toastr.error(
            `Xoá phiên chuyển khoản không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  confirmTransaction() {
    const {activeMtTransfer} = this.transactionStore.snapshot;
    const modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiên chuyển khoản`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiên chuyển khoản <strong>${activeMtTransfer.code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const { id, total_amount, total_cod, total_orders } = activeMtTransfer;
          const body: TransactionAPI.ConfirmMoneyTransactionTransferRequest = {
            id,
            total_orders,
            total_cod,
            total_amount
          };
          await this.transactionApi.confirmMoneyTransactionTransfer(body);
          toastr.success('Xác nhận phiên chuyển khoản thành công.');
          modal.close().then();
          this.transactionService.getMtTransfer(activeMtTransfer.id).then();
          this.telegram.confirmMtTransfer(activeMtTransfer).then();
        } catch (e) {
          debug.error('ERROR in confirmMoneyTransactionTransfer', e);
          toastr.error(
            'Xác nhận phiên chuyển khoản không thành công.',
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  removeMtShops() {
    const {activeMtShops, activeMtTransfer} = this.transactionStore.snapshot;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá phiên shop`,
      body: `
<div>
  Bạn có chắc muốn xoá
  &nbsp;<strong>${activeMtShops.length}</strong>&nbsp;
  phiên shop ra khỏi phiên chuyển khoản <strong>${activeMtTransfer.code}</strong>?
</div>`,
      cancelTitle: 'Đóng',
      confirmCss: 'btn-danger',
      confirmTitle: 'Xoá',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const body: Partial<TransactionAPI.UpdateMoneyTransactionTransferRequest> = {
            deletes: activeMtShops.map(mt => mt.id),
            id: activeMtTransfer.id
          };
          await this.transactionApi.updateMoneyTransactionTransfer(body);
          toastr.success('Xoá phiên shop ra khỏi phiên chuyển khoản thành công.')
          modal.close().then();
          this.transactionService.getMtTransfer(activeMtTransfer.id).then();
        } catch (e) {
          debug.error('ERROR in deleteMoneyTransactionShippingExternals', e);
          toastr.error(
            'Xoá phiên shop ra khỏi phiên chuyển khoản không thành công.',
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  addMoreMtShop() {
    this.onAddMoreMtShop = true;

    this.transactionService.selectMtShops([]);
    this._checkSelectMode();
  }

  mtShopSelected(item: MoneyTransactionShop) {
    this.onAddMoreMtShop = false;

    const activeMtShops = this.transactionStore.snapshot.activeMtShops;
    activeMtShops.push(item);
    this.transactionService.selectMtShops(activeMtShops);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onAddMoreMtShop = false;

    this.transactionService.selectMtShops([]);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const activeMtShops = this.transactionStore.snapshot.activeMtShops;
    const selecteds = activeMtShops.length > 0 || this.onAddMoreMtShop;
    this.slider.toggleLiteMode(selecteds);
    this.mtShopTable.toggleLiteMode(selecteds);
    this.dropdownActions[0].title = `Xoá ${activeMtShops.length} phiên shop`;
  }

  // NOTE: xuất bảng kê
  exportExcel() {
    const activeMtTransfer = this.transactionStore.snapshot.activeMtTransfer;
    let bank_type = 'LNH';
    if (this.isVCB) {
      bank_type = 'Vietcombank';
    }
    if (this.isTCB) {
      bank_type = 'Techcombank';
    }
    const fileName = `DOISOAT-SHOP_${this.dateFormat.transform(activeMtTransfer.created_at, 'yyyy-MM-dd')}-${bank_type}`;

    let data = this.isTCB ? this.TCBExportData : this.notTCBExportData;

    return new Promise((r, _) => {
      setTimeout(() => {
        const _template = AdminMoneyTransactionApi.bangkeTemplate;
        const headers = [...(this.isTCB ? _template.techcombank_headers : _template.not_techcombank_headers)];
        /* generate worksheet */
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data, {
          header: headers
        });
        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, `${fileName}.xls`);
        r(true);
      }, 100);
    });
  }

  goToTransactionTransfers() {
    this.router.navigateByUrl('transactions/transfers');
  }

}
