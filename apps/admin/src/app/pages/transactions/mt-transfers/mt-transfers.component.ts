import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {AuthenticateStore} from "@etop/core";
import { FilterOperator, FilterOptions } from '@etop/models';

@Component({
  selector: 'admin-mt-transfers',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <etop-filter [filters]="filters" (filterChanged)="transactionList.filter($event)"></etop-filter>
      <admin-mt-transfer-list #transactionList></admin-mt-transfer-list>
    </ng-template>`
})
export class MtTransfersComponent extends PageBaseComponent implements OnInit {
  filters: FilterOptions = [
    {
      label: 'Mã phiên',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Trạng thái',
      name: 'status',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: "Chưa xác nhận", value: 'Z' },
        { name: "Đã xác nhận", value: 'P' }
      ]
    }
  ];

  constructor(
    private auth: AuthenticateStore
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_etop:view');
  }

  ngOnInit() {
  }

}
