import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
    EtopCommonModule,
    EtopFilterModule,
    EtopMaterialModule,
    EtopPipesModule, MaterialModule, NotPermissionModule,
    SideSliderModule
} from '@etop/shared';
import { MtTransferDetailComponent } from 'apps/admin/src/app/pages/transactions/mt-transfers/mt-transfer-detail/mt-transfer-detail.component';
import { MtTransferListComponent } from 'apps/admin/src/app/pages/transactions/mt-transfers/mt-transfer-list/mt-transfer-list.component';
import { MtTransfersComponent } from 'apps/admin/src/app/pages/transactions/mt-transfers/mt-transfers.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { MtTransferRowComponent } from './components/mt-transfer-row/mt-transfer-row.component';
import { MtTransferBangkeComponent } from './components/mt-transfer-bangke/mt-transfer-bangke.component';
import { MtTransferFfmsComponent } from './components/mt-transfer-ffms/mt-transfer-ffms.component';
import { MtTransferMtshopRowComponent } from './components/mt-transfer-mtshop-row/mt-transfer-mtshop-row.component';
import { MtTransferMultiMtshopsActionComponent } from './components/mt-transfer-multi-mtshops-action/mt-transfer-multi-mtshops-action.component';
import { AddMoreMtshopComponent } from './components/add-more-mtshop/add-more-mtshop.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { MtTransferBangkeRowComponent } from './components/mt-transfer-bangke-row/mt-transfer-bangke-row.component';
import { MtTransferFfmRowComponent } from 'apps/admin/src/app/pages/transactions/mt-transfers/components/mt-transfer-ffm-row/mt-transfer-ffm-row.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";

const routes: Routes = [
  {
    path: '',
    component: MtTransfersComponent
  },
  {
    path: ':id',
    component: MtTransferDetailComponent
  }
];

@NgModule({
  declarations: [
    MtTransfersComponent,
    MtTransferListComponent,
    MtTransferDetailComponent,
    MtTransferRowComponent,
    MtTransferBangkeComponent,
    MtTransferFfmsComponent,
    MtTransferMtshopRowComponent,
    MtTransferMultiMtshopsActionComponent,
    AddMoreMtshopComponent,
    MtTransferBangkeRowComponent,
    MtTransferFfmRowComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        DropdownActionsModule,
        NgbModule,
        FormsModule,
        EtopPipesModule,
        EtopMaterialModule,
        EtopCommonModule,
        SideSliderModule,
        EtopFilterModule,
        MaterialModule,
        NotPermissionModule,
        CallCenterModule
    ],
  providers: [
    DatePipe
  ]
})
export class MtTransfersModule { }
