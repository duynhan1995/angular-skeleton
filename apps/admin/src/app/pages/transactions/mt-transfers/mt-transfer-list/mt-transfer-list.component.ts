import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { EtopTableComponent } from '@etop/shared';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { TransactionsService } from 'apps/admin/src/app/pages/transactions/transactions.service';
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import { Filters } from '@etop/models';

@Component({
  selector: 'admin-mt-transfer-list',
  templateUrl: './mt-transfer-list.component.html',
  styleUrls: ['./mt-transfer-list.component.scss']
})
export class MtTransferListComponent extends PageBaseComponent implements OnInit {
  @ViewChild('transactionTable', { static: true }) transactionTable: EtopTableComponent;

  mtTransfersLoading$ = this.transactionsStore.state$.pipe(
    map(s => s?.mtTransfersLoading),
    distinctUntilChanged()
  );
  mtTransfersList$ = this.transactionsStore.state$.pipe(map(s => s?.mtTransfers));

  isLastPage$ = this.transactionsStore.state$.pipe(
    map(s => s?.mtTransfersIsLastPage),
    distinctUntilChanged()
  );

  constructor(
    private changeDetector: ChangeDetectorRef,
    private transactionsService: TransactionsService,
    private transactionsStore: TransactionsStore
  ) {
    super();
  }

  get showPaging() {
    return !this.transactionTable.liteMode && !this.transactionTable.loading;
  }

  get emptyResultFilter() {
    const {
      mtTransfersPaging,
      mtTransfersFilters,
      mtTransfers
    } = this.transactionsStore.snapshot;
    return mtTransfersPaging.offset == 0 &&
      mtTransfersFilters.length > 0 &&
      mtTransfers.length == 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy phiên chuyển khoản phù hợp';
    }
    return 'Chưa có phiên chuyển khoản';
  }

  ngOnInit() {
    this.mtTransfersLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.transactionTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.transactionTable.toggleNextDisabled(true);
          this.transactionTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả phiên chuyển khoản.');
        }
      });
  }

  resetState() {
    this.transactionTable.toggleLiteMode(false);
    this.transactionTable.toggleNextDisabled(false);
  }

  filter(filters: Filters) {
    this.transactionsService.setMtTransfersFilters(filters);
    this.transactionTable.resetPagination();
  }

  loadPage({ page, perpage }) {
    this.transactionsService.setMtTransfersPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this.getMtTransfers();
  }

  getMtTransfers() {
    this.transactionsService.getMtTransfers(true).then();
  }

}
