import { Component, OnInit } from '@angular/core';
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {distinctUntilChanged, map} from "rxjs/operators";
import {PageBaseComponent} from "@etop/web";

@Component({
  selector: 'admin-mt-transfer-ffms',
  templateUrl: './mt-transfer-ffms.component.html',
  styleUrls: ['./mt-transfer-ffms.component.scss']
})
export class MtTransferFfmsComponent extends PageBaseComponent implements OnInit {

  fulfillments$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillments),
    distinctUntilChanged((a,b) => a?.length == b?.length)
  );

  constructor(
    private transactionsStore: TransactionsStore
  ) {
    super();
  }

  ngOnInit() {
  }

}
