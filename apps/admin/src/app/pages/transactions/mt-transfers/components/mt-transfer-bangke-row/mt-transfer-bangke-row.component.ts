import { Component, Input, OnInit } from '@angular/core';
import { BangKe } from 'libs/models/admin/MoneyTransactionTransfer';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: '[admin-mt-transfer-bangke-row]',
  templateUrl: './mt-transfer-bangke-row.component.html',
  styleUrls: ['./mt-transfer-bangke-row.component.scss']
})
export class MtTransferBangkeRowComponent implements OnInit {
  @Input() bang_ke: BangKe;
  @Input() liteMode = false;
  @Input() isTCB = false;

  constructor(
    private util: UtilService
  ) { }

  ngOnInit() {
  }

  formatName(str: string) {
    return this.util.formatName(str);
  }

}
