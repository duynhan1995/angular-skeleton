import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: '[admin-mt-transfer-ffm-row]',
  templateUrl: './mt-transfer-ffm-row.component.html',
  styleUrls: ['./mt-transfer-ffm-row.component.scss']
})
export class MtTransferFfmRowComponent implements OnInit {
  @Input() ffm = new Fulfillment({});
  @Input() liteMode = false;

  constructor(
    private ffmStore: FulfillmentStore
  ) { }

  get showCancelReason() {
    return this.ffm && this.ffm.shipping_state == 'cancelled' && this.ffm.cancel_reason;
  }

  ngOnInit() {
  }

  viewDetailOrder(order_code) {

  }

  async viewDetailFfm() {
    await this.ffmStore.navigate('shipment', this.ffm.shipping_code, null, false);
  }

}
