import { Component, OnInit } from '@angular/core';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'admin-mt-transfer-multi-mtshops-action',
  templateUrl: './mt-transfer-multi-mtshops-action.component.html',
  styleUrls: ['./mt-transfer-multi-mtshops-action.component.scss']
})
export class MtTransferMultiMtshopsActionComponent implements OnInit {

  activeMtShops$ = this.transactionsStore.state$.pipe(
    map(s => s?.activeMtShops),
    distinctUntilChanged((a,b) => a?.length == b?.length)
  )

  constructor(
    private transactionsStore: TransactionsStore
  ) { }

  ngOnInit() {
  }

}
