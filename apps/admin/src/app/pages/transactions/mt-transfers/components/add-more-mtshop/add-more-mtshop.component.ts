import { Component, OnInit } from '@angular/core';
import { Shop } from 'libs/models/Account';
import { AdminShopAPI, AdminShopApi, AdminMoneyTransactionApi, TransactionAPI } from '@etop/api';
import { MoneyTransactionShop } from 'libs/models/MoneyTransactionShop';
import { TransactionsService } from 'apps/admin/src/app/pages/transactions/transactions.service';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import GetShopsRequest = AdminShopAPI.GetShopsRequest;
import { FilterOperator } from '@etop/models';

@Component({
  selector: 'admin-add-more-mtshop',
  templateUrl: './add-more-mtshop.component.html',
  styleUrls: ['./add-more-mtshop.component.scss']
})
export class AddMoreMtshopComponent implements OnInit {

  constructor(
    private transactionService: TransactionsService,
    private transactionApi: AdminMoneyTransactionApi,
    private transactionsStore: TransactionsStore,
    private shopApi: AdminShopApi
  ) { }

  shops: Shop[] = [];
  mtShops: MoneyTransactionShop[] = [];

  selectedShops: Shop[] = [];
  selectedMtShops: MoneyTransactionShop[] = [];

  shopId: string;

  loading = false;

  displayMap = option => option && `${option.code} - ${option.name}` || null;
  valueMap = option => option && option.id || null;

  inputMapName = (option: Shop) => option && option.name || null;
  inputMapPhone = (option: Shop) => option && option.phone || null;
  inputMapEmail = (option: Shop) => option && option.email || null;

  ngOnInit() {}

  async onShopSearch(searchBy: 'name' | 'phone' | 'email', searchText: string) {
    const query: GetShopsRequest = {
      paging: { offset: 0, limit: 1000 }
    };
    switch (searchBy) {
      case "name":
        query.filter = {name: searchText};
        break;
      case "phone":
        query.filters = [
          {
            name: 'phone',
            op: FilterOperator.eq,
            value: searchText
          }
        ];
        break;
      case "email":
        query.filters = [
          {
            name: 'email',
            op: FilterOperator.eq,
            value: searchText
          }
        ];
        break;
    }
    this.shops = await this.shopApi.getShops(query);
  }

  async onShopSelect() {
    if (!this.shopId) {
      return;
    }
    const foundShop = this.selectedShops.find(s => s.id == this.shopId);
    if (foundShop) {
      return;
    }

    const shop = this.shops.find(s => s.id == this.shopId);
    let _mtShops = await this.transactionService.getEveryMoneyTransactionShop([
        { name: 'status', op: FilterOperator.eq, value: 'Z' }
      ],
      this.shopId
    );
    _mtShops = _mtShops.filter(mt => !mt.money_transaction_shipping_etop_id);
    this.mtShops.push(..._mtShops);
    this.selectedShops.push(shop);
    this.shops = [];
  }

  unSelectShop(index: number) {
    const deleted_shop = this.selectedShops[index];
    this.mtShops = this.mtShops.filter(mt => mt.shop_id != deleted_shop.id);
    this.selectedMtShops = this.selectedMtShops.filter(mt => mt.shop_id != deleted_shop.id);
    this.selectedShops.splice(index, 1);
  }

  mtOfShop(shop_id: string) {
    return this.mtShops.filter(mt => mt.shop_id == shop_id);
  }

  toggleMtShop() {
    this.selectedMtShops = this.mtShops.filter(mt => mt.p_data.selected);
  }

  async addMoreMtShop() {
    const {activeMtTransfer} = this.transactionsStore.snapshot;
    this.loading = true;
    try {
      const body: Partial<TransactionAPI.UpdateMoneyTransactionTransferRequest> = {
        id: activeMtTransfer.id,
        adds: this.selectedMtShops.map(mt => mt.id)
      };
      await this.transactionApi.updateMoneyTransactionTransfer(body);
      toastr.success('Thêm phiên shop thành công.');
      this.transactionService.getMtTransfer(activeMtTransfer.id).then();
    } catch(e) {
      debug.error('ERROR in addMoreMtShop', e);
      toastr.error('Thêm phiên shop không thành công.', e.code ? (e.message || e.msg) : '');
    }
    this.loading = false;
  }

}
