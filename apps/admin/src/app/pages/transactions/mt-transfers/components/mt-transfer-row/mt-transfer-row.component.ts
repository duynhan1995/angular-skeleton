import { Component, Input, OnInit } from '@angular/core';
import { MoneyTransactionTransfer } from 'libs/models/admin/MoneyTransactionTransfer';
import { Router } from '@angular/router';

@Component({
  selector: '[admin-mt-transfer-row]',
  templateUrl: './mt-transfer-row.component.html',
  styleUrls: ['./mt-transfer-row.component.scss']
})
export class MtTransferRowComponent implements OnInit {
  @Input() transaction: MoneyTransactionTransfer;
  @Input() liteMode = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  transactionDetail() {
    this.router.navigateByUrl(`transactions/transfers/${this.transaction.id}`);
  }

}
