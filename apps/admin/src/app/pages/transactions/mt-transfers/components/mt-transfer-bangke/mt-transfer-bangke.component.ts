import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { EtopTableComponent } from '@etop/shared';
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {distinctUntilChanged, map} from "rxjs/operators";
import {PageBaseComponent} from "@etop/web";

@Component({
  selector: 'admin-mt-transfer-bangke',
  templateUrl: './mt-transfer-bangke.component.html',
  styleUrls: ['./mt-transfer-bangke.component.scss']
})
export class MtTransferBangkeComponent extends PageBaseComponent implements OnInit, OnChanges {
  @ViewChild('validBangkeTable', { static: false }) validBangkeTable: EtopTableComponent;
  @ViewChild('invalidBangkeTable', { static: false }) invalidBangkeTable: EtopTableComponent;

  @Input() isTCB = false;
  @Input() isVCB = false;

  bangke_template = {
    not_techcombank_headers: [
      "Txn Reference",
      "Amount (VND)",
      "Benificiary Name",
      "Account Number",
      "Remarks",
      "Ben Bank",
      "Province",
      "Branch",
      "Validation"
    ],
    techcombank_headers: [
      "TÀI KHOẢN ĐÍCH",
      "SỐ TIỀN CHUYỂN",
      "DIỄN GIẢI"
    ]
  };

  headers: any;

  transfer_fee: number;

  activeMtTransfer$ = this.store.state$.pipe(
    map(s => s?.activeMtTransfer),
    distinctUntilChanged((a,b) => a?.id == b?.id)
  );

  constructor(
    private store: TransactionsStore
  ) {
    super();
  }

  get sum_bangke() {
    const {activeMtTransfer} = this.store.snapshot;
    return activeMtTransfer?.valid_bang_ke_lines.reduce((a, b) => a + Number(b.total_amount), 0);
  }

  get sum_invalid_bangke() {
    const {activeMtTransfer} = this.store.snapshot;
    return activeMtTransfer?.invalid_bang_ke_lines.reduce((a, b) => a + Number(b.total_amount), 0);
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    this.headers = this.isTCB ?
      this.bangke_template.techcombank_headers :
      this.bangke_template.not_techcombank_headers;

    this.transfer_fee = this.isVCB ? 3300 : 5500;
  }

}
