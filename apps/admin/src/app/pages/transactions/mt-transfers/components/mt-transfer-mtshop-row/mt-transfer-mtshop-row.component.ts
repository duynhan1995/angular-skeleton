import { Component, Input, OnInit } from '@angular/core';
import { MoneyTransactionShop } from '@etop/models';
import { Router } from '@angular/router';

@Component({
  selector: '[admin-mt-transfer-mtshop-row]',
  templateUrl: './mt-transfer-mtshop-row.component.html',
  styleUrls: ['./mt-transfer-mtshop-row.component.scss']
})
export class MtTransferMtshopRowComponent implements OnInit {
  @Input() transaction: MoneyTransactionShop;
  @Input() liteMode = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  transactionDetail() {
    this.router.navigateByUrl(`transactions/shops/${this.transaction.id}`);
  }

}
