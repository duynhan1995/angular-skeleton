import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {EtopCommonModule, EtopMaterialModule, EtopPipesModule, MaterialModule, NotPermissionModule} from '@etop/shared';
import { MtScheduledListComponent } from './mt-scheduled-list/mt-scheduled-list.component';
import { RouterModule, Routes } from '@angular/router';
import { MtScheduledRowComponent } from 'apps/admin/src/app/pages/transactions/mt-scheduleds/components/mt-scheduled-row/mt-scheduled-row.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { MtScheduledDetailModalComponent } from './components/mt-scheduled-detail-modal/mt-scheduled-detail-modal.component';
import {AuthenticateModule} from "@etop/core";

const routes: Routes = [
  {
    path: '',
    component: MtScheduledListComponent
  }
]

@NgModule({
  declarations: [
    MtScheduledListComponent,
    MtScheduledRowComponent,
    MtScheduledDetailModalComponent,
  ],
  entryComponents: [
    MtScheduledDetailModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopCommonModule,
    NotPermissionModule,
    AuthenticateModule,
    MaterialModule
  ]
})
export class MtScheduledsModule { }
