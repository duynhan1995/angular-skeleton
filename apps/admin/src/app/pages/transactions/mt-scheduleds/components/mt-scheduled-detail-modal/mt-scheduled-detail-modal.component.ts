import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AdminMoneyTransactionApi} from '@etop/api';
import {MoneyTransactionRrule, MoneyTransactionScheduledBank, MoneyTransactionShop} from '@etop/models';
import {ModalAction} from 'apps/core/src/components/modal-controller/modal-action.service';
import {UtilService} from 'apps/core/src/services/util.service';
import * as moment from 'moment';

@Component({
  selector: 'admin-mt-scheduled-detail-modal',
  templateUrl: './mt-scheduled-detail-modal.component.html',
  styleUrls: ['./mt-scheduled-detail-modal.component.scss']
})
export class MtScheduledDetailModalComponent implements OnInit {

  constructor(
    private router: Router,
    private util: UtilService,
    private modalAction: ModalAction,
    private api: AdminMoneyTransactionApi
  ) { }
  @Input() transaction = new MoneyTransactionScheduledBank();
  @Input() bank: string;
  @Input() date: Date;
  @Input() day_display: string;

  abandoneds: MoneyTransactionShop[] = [];
  kepts: MoneyTransactionShop[] = [];

  tab: 'transaction' | 'bang_ke' = 'transaction';

  split_amount = 10;
  split_amount_display = 90;

  mt_transfer_note: string;

  loading = false;

  private static groupMtByShop(arr: any[]) {
    let hash = {};
    for (let item of arr) {
      if (!hash[item.shop_id]) {
        hash[item.shop_id] = [item];
      } else {
        hash[item.shop_id] = hash[item.shop_id].concat([item]);
      }
    }

    debug.log('hash', hash);
    const result = [];
    for (let key in hash) {
      result.push(hash[key]);
    }
    return result;
  }

  ngOnInit() {
    this.mt_transfer_note = `eTop - ${this.bank} - ${moment(this.date).format('DD/MM/YYYY')}`;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  getTotalOrders(mts: MoneyTransactionShop[]) {
    return mts.reduce((a, b) => a + b.total_orders, 0);
  }

  getTotalCod(mts: MoneyTransactionShop[]) {
    return mts.reduce((a, b) => a + b.total_cod, 0);
  }

  getTotalAmount(mts: MoneyTransactionShop[]) {
    return mts.reduce((a, b) => a + b.total_amount, 0);
  }

  split() {
    try {
      if (!this.split_amount && this.split_amount != 0) {
        return toastr.error('Chưa nhập tỉ lệ dùng để tách phiên.');
      }
      this.kepts = [];
      this.abandoneds = [];
      this.split_amount_display = 100 - this.split_amount;
      const sorted_transactions: MoneyTransactionShop[] = this.util.deepClone(this.transaction.money_transactions)
        .sort((a, b) => {
          if ((new Date(b.created_at).getTime() - new Date(a.created_at).getTime()) > 0) {
            return 1;
          }
          return -1;
        });

      if (sorted_transactions && sorted_transactions.length) {
        const mtGroupedByShops: Array<MoneyTransactionShop[]> = MtScheduledDetailModalComponent.groupMtByShop(sorted_transactions);
        for (let list_mt of mtGroupedByShops) {

          // NOTE: [2, 4].indexOf(this.modal.day) != -1: Nếu phiên dự trù rơi vào thứ 3,5 và
          // NOTE: lịch chuyển khoản của shop là hàng ngày (FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR)
          // NOTE: => loại những phiên này!
          // NOTE: 0% luôn giữ lại các phiên

          if (
            this.split_amount == 0 ||
            list_mt[0].shop.is_prior_money_transaction ||
            list_mt[0].shop.money_transaction_count <= 2
          ) {
            this.kepts.push(...list_mt);
            continue;
          }

          if (
            list_mt[0].shop.money_transaction_rrule == MoneyTransactionRrule.everyDay &&
            ['Thứ 3', 'Thứ 5'].includes(this.day_display)
          ) {
            this.abandoneds.push(...list_mt);
            continue;
          }

          let limit = list_mt.length * this.split_amount / 100;
          if (limit < 1) {
            this.kepts.push(...list_mt);
            continue;
          }
          limit = Math.round(limit);
          for (let i = 0; i < limit; i++) {
            const sum_amount = list_mt.reduce((a, b) => {
              return a + Number(b.total_amount);
            }, 0);

            const remained_amount = sum_amount - list_mt[i].total_amount;

            if (list_mt[i].total_amount > 0 && remained_amount >= 50000) {
              this.abandoneds.push(list_mt[i]);
              list_mt.splice(i, 1);
            }
          }
          this.kepts.push(...list_mt);
        }

      }
    } catch (e) {
      debug.error('ERROR in splitting', e);
    }
  }

  async createMtTransfer() {
    this.loading = true;
    try {
      if (!this.kepts || !this.kepts.length) {
        toastr.error('Chưa tách phiên!');
        return this.loading = false;
      }
      const ids = this.kepts.map(mt => mt.id);
      const res = await this.api.createMoneyTransactionTransfer(ids);

      const body = {
        id: res.id,
        note: this.mt_transfer_note
      }
      await this.api.updateMoneyTransactionTransfer(body);
      toastr.success('Tạo phiên chuyển khoản thành công.');
      this.modalAction.dismiss(null);
      this.router.navigateByUrl(`transactions/transfers/${res.id}`);

    } catch (e) {
      toastr.error(
        'Tạo phiên chuyển khoản không thành công.',
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }

}
