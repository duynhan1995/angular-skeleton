import { Component, Input, OnInit } from '@angular/core';
import { MtScheduledDetailModalComponent } from 'apps/admin/src/app/pages/transactions/mt-scheduleds/components/mt-scheduled-detail-modal/mt-scheduled-detail-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { MoneyTransactionScheduled, MoneyTransactionScheduledBank } from '@etop/models';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: '[admin-mt-scheduled-row]',
  templateUrl: './mt-scheduled-row.component.html',
  styleUrls: ['./mt-scheduled-row.component.scss']
})
export class MtScheduledRowComponent implements OnInit {
  @Input() transaction: MoneyTransactionScheduled;
  @Input() liteMode = false;

  constructor(
    private auth: AuthenticateStore,
    private modalController: ModalController
  ) { }

  get noPermission() {
    const permissions = this.auth.snapshot.permission.permissions;
    return !permissions.includes('admin/money_transaction_shipping_external:view') &&
      !permissions.includes('admin/money_transaction:view') &&
      !permissions.includes('admin/money_transaction_shipping_etop:view');
  }

  ngOnInit() {
  }

  viewDetailMtScheduledBank(mt: MoneyTransactionScheduledBank, bank: string) {
    if (!this.noPermission) {
      const modal = this.modalController.create({
        component: MtScheduledDetailModalComponent,
        componentProps: {
          transaction: mt,
          bank,
          date: this.transaction.date,
          day_display: this.transaction.day_display
        },
        cssClass: 'modal-xxl'
      });
      modal.show().then();
    }
  }

}
