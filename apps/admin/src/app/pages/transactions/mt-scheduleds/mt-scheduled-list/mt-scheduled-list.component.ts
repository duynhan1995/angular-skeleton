import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { TransactionsService } from 'apps/admin/src/app/pages/transactions/transactions.service';
import * as moment from 'moment';
import {
  BangKe,
  BankAccount, FilterOperator, Filters,
  MoneyTransactionScheduled,
  MoneyTransactionScheduledBank,
  MoneyTransactionShop,
  Shop
} from '@etop/models';
import { AdminShopApi, AdminMoneyTransactionApi } from '@etop/api';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import RRule from 'rrule';
import { takeUntil } from 'rxjs/operators';
import { UtilService } from 'apps/core/src/services/util.service';
import { EtopTableComponent} from '@etop/shared';
import { AuthenticateStore } from '@etop/core';
import {BankQuery} from "@etop/state/bank";
import {FormBuilder} from "@angular/forms";
import misc from "../../../../../../../core/src/libs/misc";

const DAYS_OF_WEEK = ['Chủ Nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'];

const RRULE_MAP = {
  'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR': 'Hàng ngày',
  'FREQ=WEEKLY;BYDAY=MO,WE,FR': 'Thứ 2-4-6 hàng tuần',
  'FREQ=WEEKLY;BYDAY=TU,TH': 'Thứ 3-5 hàng tuần',
  'FREQ=WEEKLY;BYDAY=FR': 'Thứ 6 hàng tuần',
  'FREQ=MONTHLY;BYDAY=+2FR,-1FR': 'Thứ 6 thứ 2 và thứ 6 cuối cùng của tháng',
  'FREQ=MONTHLY;BYDAY=-1FR': 'Thứ 6 cuối cùng của tháng',
  'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1': 'Ngày làm việc cuối cùng của tháng'
};

@Component({
  selector: 'admin-mt-scheduled-list',
  templateUrl: './mt-scheduled-list.component.html',
  styleUrls: ['./mt-scheduled-list.component.scss']
})
export class MtScheduledListComponent extends PageBaseComponent implements OnInit {

  bankReady$ = this.bankQuery.select(state => !!state.bankReady);

  filterRange = this.fb.group({
    from: "",
    to: ""
  });

  reservedTransactionIds: string[] = [];
  rawMoneyTransactionShops: MoneyTransactionShop[] = [];

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private util: UtilService,
    private transactionService: TransactionsService,
    private shopApi: AdminShopApi,
    private bankQuery: BankQuery,
    private fb: FormBuilder
  ) {
    super();
  }

  get noPermission() {
    const permissions = this.auth.snapshot.permission.permissions;
    return !permissions.includes('admin/money_transaction:view') &&
      !permissions.includes('admin/money_transaction_shipping_etop:view');
  }

  @ViewChild('transactionTable', { static: false }) transactionTable: EtopTableComponent;

  transactions: MoneyTransactionScheduled[] = [];

  rrule_arr = [
    'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR',
    'FREQ=WEEKLY;BYDAY=MO,WE,FR',
    'FREQ=WEEKLY;BYDAY=TU,TH',
    'FREQ=WEEKLY;BYDAY=FR',
    'FREQ=MONTHLY;BYDAY=+2FR,-1FR',
    'FREQ=MONTHLY;BYDAY=-1FR',
    // last working day of months
    'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1'
  ];

  private static checkNullMtScheduledBank(mt_scheduled_bank: MoneyTransactionScheduledBank) {
    if (!mt_scheduled_bank) {
      return null;
    }
    const { total_amount, money_transactions, bang_ke_lines } = mt_scheduled_bank;
    const is_null = total_amount + money_transactions.length + bang_ke_lines.length == 0;

    return is_null ? null : mt_scheduled_bank;
  }

  ngOnInit() {
    this.filterRange.patchValue({
      from: new Date("08-14-2021"),
      to: new Date(new Date().setHours(23, 59, 59, 999))
    });
  }

  async startFilterTransactions() {
    const from: Date = this.filterRange.get("from").value;
    const to: Date = this.filterRange.get("to").value;

    if (!from || !to) {
      return toastr.error("Vui lòng chọn khoảng thời gian");
    }

    if (!this.transactionTable) {
      return;
    }
    this.transactionTable.loading = true;

    if (this.bankQuery.getValue().bankReady) {
      await this.getMtScheduleds(1, 7);
      this.transactionTable.loading = false;
      this.changeDetector.detectChanges();
    } else {
      this.bankReady$.pipe(takeUntil(this.destroy$)).subscribe(async() => {
        await this.getMtScheduleds(1, 7);
        this.transactionTable.loading = false;
        this.changeDetector.detectChanges();
      });
    }
  }

  async getMtScheduleds(page, perpage) {
    try {
      this.reservedTransactionIds = [];
      this.rawMoneyTransactionShops = [];

      const _transactions: MoneyTransactionScheduled[] = [];

      for (let i = 0; i < perpage; i++) {
        _transactions.push(
          await this.listMt(i + perpage * (page - 1))
        );
      }

      this.transactions = _transactions;
    } catch(e) {
      debug.error('ERROR in getMtScheduleds', e);
    }
  }

  async listMt(offset: number): Promise<MoneyTransactionScheduled> {
    try {
      const current_rrules = this.rruleMatch(offset);

      // cover the case that doesn't exclude Saturday & Sunday
      if (current_rrules.includes('FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1')) {
        current_rrules.push('FREQ=MONTHLY;BYMONTHDAY=-1');
      }

      // SECTION: Lấy về tất cả phiên có thể có trong ngày hiện tại

      for (let rrule of current_rrules) {
        const filters: Filters = [
          {
            name: 'shop.money_transaction_rrule',
            op: FilterOperator.eq,
            value: rrule
          },
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          }
        ];
        let _mt_shops: MoneyTransactionShop[] = await this.transactionService.getEveryMoneyTransactionShop(filters);
        const from: Date = this.filterRange.get("from").value;
        const to: Date = this.filterRange.get("to").value.setHours(23, 59, 59, 999);

        _mt_shops = _mt_shops.filter(mt => !mt.money_transaction_shipping_etop_id
          && new Date(mt.created_at).getTime() >= new Date(from).getTime()
          && new Date(mt.created_at).getTime() <= new Date(to).getTime());

        const shop_ids = _mt_shops.map(mt => mt.shop_id);
        const shops = await this.getShops(shop_ids);

        _mt_shops = _mt_shops.map(mt => {
          mt.shop = shops && shops.find(s => s.id == mt.shop_id);
          mt.bank_account = mt.shop && mt.shop.bank_account;
          return mt;
        });
        this.rawMoneyTransactionShops.push(..._mt_shops);
      }
      // NOTE: remove duplicated transactions
      this.rawMoneyTransactionShops = this.rawMoneyTransactionShops.filter(e => !this.reservedTransactionIds.includes(e.id));
      this.reservedTransactionIds = misc.uniqueArray(this.reservedTransactionIds.concat(this.rawMoneyTransactionShops.map(e => e.id)));
      // END Section: Lấy về tất cả phiên có thể có trong ngày hiện tại

      return this.handleRawMoneyTransactionShops(offset, current_rrules);

    } catch(e) {
      debug.error('ERROR in listMt', e);
      return null;
    }
  }

  private handleRawMoneyTransactionShops(offset: number, current_rrules: string[]): MoneyTransactionScheduled {
    // SECTION: Group lại bằng shop_id để bước sau bỏ ra những phiên ko đủ điều kiện
    let mtsGroupByShopHash = {};

    for (let mt of this.rawMoneyTransactionShops) {
      if (mtsGroupByShopHash[mt.shop_id]) {
        mtsGroupByShopHash[mt.shop_id].total_amount += mt.total_amount;
        mtsGroupByShopHash[mt.shop_id].money_transactions.push(mt);
      } else {
        mtsGroupByShopHash[mt.shop_id] = {
          total_amount: mt.total_amount,
          money_transactions: [mt],
          shop: mt.shop
        };
      }
    }
    // END Section: Group lại bằng shop_id để bước sau bỏ ra những phiên ko đủ điều kiện

    const mt_scheduled = new MoneyTransactionScheduled({
      date: moment().add(offset, 'days').toDate(),
      day_display: DAYS_OF_WEEK[moment().add(offset, 'days').day()],
      is_today: moment().add(offset, 'days').toString() == moment().add(0, 'days').toString(),
      rrules_applied: current_rrules.map(r => RRULE_MAP[r]),
      techcombank_transaction: {
        total_amount: 0,
        money_transactions: [],
        bang_ke_lines: []
      },
      vietcombank_transaction: {
        total_amount: 0,
        money_transactions: [],
        bang_ke_lines: []
      },
      other_bank_transaction: {
        total_amount: 0,
        money_transactions: [],
        bang_ke_lines: []
      },
      summary_transaction: {
        total_amount: 0,
        money_transactions: [],
        bang_ke_lines: []
      }
    });

    for (let key in mtsGroupByShopHash) {
      // NOTE: không có info của shop hoặc ngân hàng => LOẠI
      const item = mtsGroupByShopHash[key];
      if (!item.shop || !item.shop.bank_account) {
        continue;
      }

      // NOTE: số tiền chuyển không >= 50.000đ => LOẠI
      let fee = AdminMoneyTransactionApi.transferFee.liennganhang;
      if (item.shop.bank_account.name.includes('Vietcombank')) {
        fee = AdminMoneyTransactionApi.transferFee.vietcombank;
      }
      item.total_amount -= fee;
      if (item.total_amount < 50000) { continue; }

      // SECTION: Bảng Kê
      const bang_ke_line = this.createBangKeLine(item.shop, item.total_amount, offset);
      // END Section: Bảng Kê

      // SECTION: MoneyTransactionScheduledBank: chia phiên theo ngân hàng
      if (item.shop.bank_account.name.includes('Vietcombank')) {
        const { total_amount, money_transactions, bang_ke_lines } = mt_scheduled.vietcombank_transaction;
        mt_scheduled.vietcombank_transaction = {
          total_amount: total_amount + item.total_amount,
          money_transactions: money_transactions.concat(item.money_transactions),
          bang_ke_lines: bang_ke_lines.concat([bang_ke_line])
        };
      } else if (item.shop.bank_account.name.includes('Techcombank')) {
        const { total_amount, money_transactions, bang_ke_lines } = mt_scheduled.techcombank_transaction;
        mt_scheduled.techcombank_transaction = {
          total_amount: total_amount + item.total_amount,
          money_transactions: money_transactions.concat(item.money_transactions),
          bang_ke_lines: bang_ke_lines.concat([bang_ke_line])
        };
      } else {
        const { total_amount, money_transactions, bang_ke_lines } = mt_scheduled.other_bank_transaction;
        mt_scheduled.other_bank_transaction = {
          total_amount: total_amount + item.total_amount,
          money_transactions: money_transactions.concat(item.money_transactions),
          bang_ke_lines: bang_ke_lines.concat([bang_ke_line])
        };
      }
      // END Section: MoneyTransactionScheduledBank: chia phiên theo ngân hàng
    }

    // SECTION: SUM OF MoneyTransactionScheduledBanks: sum các phiên chia theo ngân hàng
    const { vietcombank_transaction, techcombank_transaction, other_bank_transaction } = mt_scheduled;
    const sums = [
      vietcombank_transaction,
      techcombank_transaction,
      other_bank_transaction
    ];
    mt_scheduled.summary_transaction = {
      total_amount: sums.reduce((a,b) => a + b.total_amount, 0),
      money_transactions: sums.reduce((a,b) => a.concat(b.money_transactions), []),
      bang_ke_lines: sums.reduce((a,b) => a.concat(b.bang_ke_lines), [])
    };
    // END Section: SUM OF MoneyTransactionScheduledBanks: sum các phiên chia theo ngân hàng

    return {
      ...mt_scheduled,
      vietcombank_transaction: MtScheduledListComponent.checkNullMtScheduledBank(vietcombank_transaction),
      techcombank_transaction: MtScheduledListComponent.checkNullMtScheduledBank(techcombank_transaction),
      other_bank_transaction: MtScheduledListComponent.checkNullMtScheduledBank(other_bank_transaction),
      summary_transaction: MtScheduledListComponent.checkNullMtScheduledBank(mt_scheduled.summary_transaction)
    };
  }

  private createBangKeLine(shop: Shop, total_amount: number, offset: number): BangKe {
    try {
      const tcbBranches = this.bankQuery.getValue().techcombankBranches;
      let TCB = shop.bank_account && tcbBranches[shop.bank_account.branch];
      if (shop.bank_account && shop.bank_account.name && shop.bank_account.name.includes('Vietcombank')) {
        TCB = tcbBranches['NHTMCP Ngoai Thuong VN'];
      }
      return new BangKe({
        id: shop.id,
        shop_id: shop.id,
        shop_name: shop.name,
        ref: '',
        bank_account: shop.bank_account,
        total_amount,
        currency_unit: 'vnd',
        content: this.util.formatName(
          `TOPSHIP CHUYEN TIEN ${moment().add(offset, 'days').format('YYYY-MM-DD')} COD DA CAN TRU CHO SHOP ${shop.name}`
        ),
        bank_account_TCB: TCB ? {
          ...shop.bank_account,
          name: TCB.name,
          branch: TCB.region == 'Ca nuoc' ? '-' : TCB.branch,
          region: TCB.region == 'Ca nuoc' ? '-' : TCB.region
        } : new BankAccount()
      });
    } catch(e) {
      debug.error('ERROR in create BangKe', e);
    }
  }

  async getShops(shop_ids: string[]) {
    try {
      return await this.shopApi.getShopsByIDs(shop_ids);
    } catch(e) {
      debug.error('ERROR in getting shops', e);
    }
  }

  private rruleMatch(days: number) {
    const todayutc = moment().add(days, 'days').utc().startOf('day'); // today in UTC
    return this.rrule_arr.filter(rrule => {
      const rule = RRule.fromString(rrule);
      const _before: any = moment().add(days, 'days').toDate();
      const lastOccurrence = rule.before(_before, true); // last rule date including today
      const lastOccurutc = moment(lastOccurrence).utc(); // lastOccurrence in utc
      const match = moment(lastOccurutc).isSame(todayutc, 'day');
      if (match) {
        return rrule;
      }
    });
  }

}
