import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    EtopCommonModule,
    EtopFilterModule,
    EtopMaterialModule,
    EtopPipesModule, MaterialModule, NotPermissionModule,
    SideSliderModule
} from '@etop/shared';
import {
  MtShippingExternalListComponent
} from 'apps/admin/src/app/pages/transactions/mt-shipping-externals/mt-shipping-external-list/mt-shipping-external-list.component';
import { RouterModule, Routes } from '@angular/router';
import { MtShippingExternalsComponent } from 'apps/admin/src/app/pages/transactions/mt-shipping-externals/mt-shipping-externals.component';
import { MtShippingExternalDetailComponent } from './mt-shipping-external-detail/mt-shipping-external-detail.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { MtShippingExternalRowComponent } from './components/mt-shipping-external-row/mt-shipping-external-row.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { MultiMtExternalsActionComponent } from 'apps/admin/src/app/pages/transactions/mt-shipping-externals/components/multi-mt-externals-action/multi-mt-externals-action.component';
import { MtExternalFfmRowComponent } from 'apps/admin/src/app/pages/transactions/mt-shipping-externals/components/mt-external-ffm-row/mt-external-ffm-row.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MtExternalMultiFfmsActionComponent } from 'apps/admin/src/app/pages/transactions/mt-shipping-externals/components/mt-external-multi-ffms-action/mt-external-multi-ffms-action.component';
import { MtShippingExternalCreateComponent } from './components/mt-shipping-external-create/mt-shipping-external-create.component';
import { FormsModule } from '@angular/forms';
import {AuthenticateModule} from "@etop/core";

const routes: Routes = [
  {
    path: '',
    component: MtShippingExternalsComponent
  },
  {
    path: ':id',
    component: MtShippingExternalDetailComponent
  }
];

@NgModule({
  declarations: [
    MtShippingExternalsComponent,
    MtShippingExternalListComponent,
    MtShippingExternalDetailComponent,
    MtShippingExternalRowComponent,
    MultiMtExternalsActionComponent,
    MtExternalFfmRowComponent,
    MtExternalMultiFfmsActionComponent,
    MtShippingExternalCreateComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        DropdownActionsModule,
        NgbModule,
        FormsModule,
        EtopPipesModule,
        EtopMaterialModule,
        SideSliderModule,
        EtopCommonModule,
        EtopFilterModule,
        NotPermissionModule,
        AuthenticateModule,
        MaterialModule
    ]
})
export class MtShippingExternalsModule { }
