import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {AuthenticateStore} from "@etop/core";
import { FilterOperator, FilterOptions } from '@etop/models';

@Component({
  selector: 'admin-mt-shipping-externals',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <etop-filter [filters]="filters" (filterChanged)="transactionList.filter($event)"></etop-filter>
      <admin-mt-shipping-external-list #transactionList></admin-mt-shipping-external-list>
    </ng-template>`
})
export class MtShippingExternalsComponent extends PageBaseComponent implements OnInit {
  filters: FilterOptions = [
    {
      label: 'Mã phiên',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Trạng thái',
      name: 'status',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: "Chưa xác nhận", value: 'Z' },
        { name: "Đã xác nhận", value: 'P' }
      ]
    },
    {
      label: 'Nhà vận chuyển',
      name: 'provider',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: 'Giao hàng nhanh', value: 'ghn' },
        { name: 'Giao hàng tiết kiệm', value: 'ghtk' },
        { name: 'Viettel Post', value: 'vtpost' }
      ]
    }
  ];

  constructor(
    private auth: AuthenticateStore
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_external:view');
  }

  ngOnInit() {
  }

}
