import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AdminMoneyTransactionApi } from '@etop/api';
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {map} from "rxjs/operators";
import {TransactionsService} from "apps/admin/src/app/pages/transactions/transactions.service";
import {Connection} from "libs/models/Connection";
import * as moment from "moment";

@Component({
  selector: 'admin-mt-shipping-external-create',
  templateUrl: './mt-shipping-external-create.component.html',
  styleUrls: ['./mt-shipping-external-create.component.scss']
})
export class MtShippingExternalCreateComponent implements OnInit {

  constructor(
    private transactionApi: AdminMoneyTransactionApi,
    private transactionsStore: TransactionsStore,
    private transactionsService: TransactionsService
  ) { }

  @ViewChild('inputImport', {static: false}) inputImport: ElementRef;

  @Output() transactionCreated = new EventEmitter();

  builtinConnections$ = this.transactionsStore.state$.pipe(
    map(s => s?.connections?.filter(c => c.connection_method == 'builtin'))
  );

  selectedConnectionId: string;
  externalPaidAt: Date;
  note = '';
  importedFile: any;

  loading = false;

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }

  displayMap = option => option && MtShippingExternalCreateComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  ngOnInit() {
  }

  uploadFile() {
    this.inputImport.nativeElement.click();
  }

  onSelectFile(event: any): void {
    try {
      if (event.target.files && event.target.files.length > 0) {
        this.importedFile = event.target.files[0];
      }
    } catch (error) {
      toastr.error(error.message);
    }
  }

  async importFile() {
    this.loading = true;
    try {
      if (!this.selectedConnectionId) {
        toastr.error("Vui lòng chọn nhà vận chuyển!");
        return this.loading = false;
      }

      if (!this.externalPaidAt) {
        toastr.error("Vui lòng chọn ngày thanh toán!");
        return this.loading = false;
      }

      if (!this.importedFile) {
        toastr.error("Vui lòng chọn file đối soát!");
        return this.loading = false;
      }

      const form = new FormData();

      form.set("connection_id", this.selectedConnectionId);
      form.set('external_paid_at', this.externalPaidAt.toISOString());
      form.set('note', this.note);
      form.set("files", this.importedFile);

      await this.transactionsService.importMoneyTransactionShippingExternal(form);
      toastr.success('Tạo phiên nhà vận chuyển thành công.');

    } catch(e) {
      debug.error('ERROR in creating MT Externals', e);
      toastr.error(
        'Tạo phiên nhà vận chuyển không thành công.',
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }

}
