import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from '@etop/models';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: '[admin-mt-external-ffm-row]',
  templateUrl: './mt-external-ffm-row.component.html',
  styleUrls: ['./mt-external-ffm-row.component.scss']
})
export class MtExternalFfmRowComponent implements OnInit {
  @Input() ffm: Fulfillment;
  @Input() liteMode = false;

  constructor(
    private ffmStore: FulfillmentStore
  ) { }

  get showCancelReason() {
    return this.ffm.shipping_state == 'cancelled' && this.ffm.cancel_reason;
  }

  ngOnInit() {
  }

  async viewDetailFfm() {
    if (this.ffm.not_found_on_etop) {
      return;
    }
    await this.ffmStore.navigate('shipment', this.ffm.shipping_code, null, false);
  }

}
