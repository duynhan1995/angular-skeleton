import { Component, OnInit } from '@angular/core';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { AdminMoneyTransactionApi } from '@etop/api';
import { TelegramService } from '@etop/features';
import {distinctUntilChanged, map} from "rxjs/operators";
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {TransactionsService} from "apps/admin/src/app/pages/transactions/transactions.service";

@Component({
  selector: 'admin-multi-mt-externals-action',
  templateUrl: './multi-mt-externals-action.component.html',
  styleUrls: ['./multi-mt-externals-action.component.scss']
})
export class MultiMtExternalsActionComponent implements OnInit {

  activeMtShippingExternals$ = this.transactionsStore.state$.pipe(
    map(s => s?.activeMtShippingExternals),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  constructor(
    private dialog: DialogControllerService,
    private telegram: TelegramService,
    private transactionApi: AdminMoneyTransactionApi,
    private transactionsStore: TransactionsStore,
    private transactionService: TransactionsService
  ) { }

  get sum_orders() {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    return activeMts.reduce((a, b) => {
      return a + Number(b.total_orders)
    }, 0)
  }

  get sum_cod() {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    return activeMts.reduce((a, b) => {
      return a + Number(b.total_cod)
    }, 0)
  }

  ngOnInit() {
  }

  confirmTransactions() {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    const valid = !activeMts.some(mt => mt.lines.some(l => l.import_error));
    if (!valid) {
      return toastr.error("Có phiên bị lỗi, vui lòng kiểm tra lại!");
    }
    const modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiên nhà vận chuyển`,
      body: `
        <div>Bạn có chắc muốn xác nhận <strong>${activeMts.length}</strong> phiên nhà vận chuyển?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const ids = activeMts.map(mt => mt.id);
          await this.transactionService.confirmMoneyTransactionShippingExternals(ids);
          toastr.success(`Xác nhận phiên nhà vận chuyển thành công.`);
          this.telegram.confirmMtShippingExternal(activeMts).then();
          modal.close().then();
        } catch (e) {
          debug.error('ERROR in confirming MoneyTransactionShippingExternals', e);
          toastr.error(
            `Xác nhận phiên nhà vận chuyển không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

}
