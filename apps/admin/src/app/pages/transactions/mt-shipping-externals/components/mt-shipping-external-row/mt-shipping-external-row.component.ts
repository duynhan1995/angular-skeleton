import { Component, Input, OnInit } from '@angular/core';
import { MoneyTransactionShippingExternal } from '@etop/models';
import { Router } from '@angular/router';

@Component({
  selector: '[admin-mt-shipping-external-row]',
  templateUrl: './mt-shipping-external-row.component.html',
  styleUrls: ['./mt-shipping-external-row.component.scss']
})
export class MtShippingExternalRowComponent implements OnInit {
  @Input() transaction: MoneyTransactionShippingExternal;
  @Input() liteMode = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  transactionDetail() {
    this.router.navigateByUrl(`transactions/externals/${this.transaction.id}`);
  }

}
