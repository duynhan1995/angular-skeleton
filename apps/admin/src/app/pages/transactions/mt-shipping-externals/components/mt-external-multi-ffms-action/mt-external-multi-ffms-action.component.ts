import { Component, OnInit } from '@angular/core';
import {distinctUntilChanged, map} from "rxjs/operators";
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";

@Component({
  selector: 'admin-mt-external-multi-ffms-action',
  templateUrl: './mt-external-multi-ffms-action.component.html',
  styleUrls: ['./mt-external-multi-ffms-action.component.scss']
})
export class MtExternalMultiFfmsActionComponent implements OnInit {
  activeFulfillments$ = this.transactionsStore.state$.pipe(
    map(s => s?.activeFulfillments),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  constructor(
    private transactionsStore: TransactionsStore
  ) { }

  ngOnInit() {
  }

}
