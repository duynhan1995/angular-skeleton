import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { AdminMoneyTransactionApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { UtilService } from 'apps/core/src/services/util.service';
import { TransactionsStore } from 'apps/admin/src/app/pages/transactions/transactions.store';
import {TransactionsService} from "apps/admin/src/app/pages/transactions/transactions.service";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {Fulfillment} from "libs/models/Fulfillment";
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import {MatDialogController} from "@etop/shared";

@Component({
  selector: 'admin-mt-shipping-external-detail',
  templateUrl: './mt-shipping-external-detail.component.html',
  styleUrls: ['./mt-shipping-external-detail.component.scss']
})
export class MtShippingExternalDetailComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('ffmTable', { static: false }) ffmTable: EtopTableComponent;
  @ViewChild('slider', { static: false }) slider: SideSliderComponent;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: `Xoá đơn giao hàng`,
      cssClass: 'text-danger',
      onClick: () => this.removeFfms(),
      permissions: ['admin/money_transaction_shipping_external_lines:remove']
    }
  ];

  filters = [
    {
      name: 'Tất cả',
      value: 'all'
    },
    {
      name: 'Đối soát đúng',
      value: 'valid'
    },
    {
      name: 'Đối soát sai',
      value: 'invalid'
    }
  ];
  selectedFilter = 'all';

  fulfillmentsLoading$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillmentsLoading),
    distinctUntilChanged()
  );
  fulfillments$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillments)
  );

  mtShippingExternal$ = this.transactionsStore.state$.pipe(map(s => s?.activeMtShippingExternal));

  fulfillments: Fulfillment[] = [];

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private router: Router,
    private ref: ChangeDetectorRef,
    private dialog: DialogControllerService,
    private headerController: HeaderControllerService,
    private util: UtilService,
    private transactionApi: AdminMoneyTransactionApi,
    private transactionsStore: TransactionsStore,
    private transactionsService: TransactionsService,
    private matDialogController: MatDialogController
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_external:view');
  }

  get noPermissionLines() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction_shipping_external_lines:remove');
  }

  get sliderTitle() {
    const activeFfms = this.transactionsStore.snapshot.activeFulfillments;
    return `Thao tác trên <strong>${activeFfms.length}</strong> đơn giao hàng`;
  }

  isActiveFfm(ffm: Fulfillment) {
    if (!ffm?.id) {
      return false;
    }
    const activeFfms = this.transactionsStore.snapshot.activeFulfillments;
    return activeFfms.some(item => item.id == ffm.id);
  }

  ngOnInit() {
    this.fulfillments$.pipe(takeUntil(this.destroy$))
      .subscribe(ffms => {
        this.fulfillments = ffms;
      });

    this.transactionsStore.changeHeaderTab(null);
    const id = this.route.snapshot.paramMap.get('id');
    this.transactionsService.getMtShippingExternal(id).then();
  }

  ngAfterViewInit() {
    this.fulfillmentsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.ffmTable.loading = loading;
        this.ref.detectChanges();
        if (loading) {
          this.resetState();
        } else {
          this.setupActions();
        }
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  setupActions() {
    const activeMtShippingExternal = this.transactionsStore.snapshot.activeMtShippingExternal;
    if (['P', 'N'].includes(activeMtShippingExternal?.status)) {
      return this.headerController.clearActions();
    }
    this.headerController.setActions([
      {
        title: 'Xoá',
        cssClass: 'btn btn-outline btn-danger',
        onClick: () => this.deleteTransaction(),
        permissions: ['admin/money_transaction_shipping_external:delete']
      },
      {
        title: 'Tách phiên shop ưu tiên',
        cssClass: 'btn btn-primary',
        onClick: () => this.splitTransaction(),
        disabled: this.fulfillments.some(f => f.external_import_error),
        tooltips: this.fulfillments.some(f => f.external_import_error) && 'Còn đơn bị lỗi'
      }
    ]);
  }

  resetState() {
    this.ffmTable.toggleLiteMode(false);
    this.slider.toggleLiteMode(false);
  }

  deleteTransaction() {
    const activeMtShippingExternal = this.transactionsStore.snapshot.activeMtShippingExternal;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá phiên nhà vận chuyển`,
      body: `
        <div>Bạn có chắc muốn xoá phiên nhà vận chuyển <strong>${activeMtShippingExternal.code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmCss: 'btn-danger',
      confirmTitle: 'Xoá',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.transactionApi.deleteMoneyTransactionShippingExternal(activeMtShippingExternal.id);
          toastr.success(`Xoá phiên nhà vận chuyển thành công.`);
          this.router.navigateByUrl('transactions/externals').then();
          modal.close().then();
        } catch (e) {
          debug.error('ERROR in deleteMoneyTransactionShippingExternal', e);
          toastr.error(
            `Xoá phiên nhà vận chuyển không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  splitTransaction() {
    const activeMtExternal = this.transactionsStore.snapshot.activeMtShippingExternal;

    const dialog = this.matDialogController.create({
      template: {
        title: 'Tách phiên shop ưu tiên',
        content: `
<div>Bạn đang thực hiện tách phiên NVC <strong>${activeMtExternal?.code}</strong> theo điều kiện shop ưu tiên.</div>
<div>Phiên NVC hiện tại sẽ bị huỷ và thay thế bằng 2 phiên NVC mới.</div>
<div>Vui lòng xác nhận hành động trước khi thực hiện.</div>`,
      },
      afterClosedCb: (success: boolean) => {
        if (success) {
          this.router.navigateByUrl('transactions/externals').then();
        }
      },

      onConfirm: async() => {
        try {
          await this.transactionsService.splitMoneyTransactionShippingExternal(activeMtExternal);
          toastr.success(`Tách phiên shop ưu tiên thành công.`);
        } catch (e) {
          debug.error('ERROR in splitMoneyTransactionShippingExternal', e);
          toastr.error(
            `Tách phiên shop ưu tiên không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
          throw e;
        }
      },

    });

    dialog.open();
  }

  itemSelected(item: Fulfillment) {
    const activeFfms = this.transactionsStore.snapshot.activeFulfillments;

    const index = activeFfms.findIndex(ffm => ffm.id == item.id);
    if (index >= 0) {
      activeFfms.splice(index, 1);
    } else {
      activeFfms.push(item);
    }
    this.transactionsService.selectFulfillments(activeFfms);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.transactionsService.selectFulfillments([]);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const activeFfms = this.transactionsStore.snapshot.activeFulfillments;
    const selecteds = activeFfms.length > 0;
    this.slider.toggleLiteMode(selecteds);
    this.ffmTable.toggleLiteMode(selecteds);
    this.dropdownActions[0].title = `Xoá ${activeFfms.length} đơn giao hàng`;
  }

  removeFfms() {
    const {activeFulfillments, activeMtShippingExternal} = this.transactionsStore.snapshot;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá đơn giao hàng`,
      body: `
        <div>
          Bạn có chắc muốn xoá
          &nbsp;<strong>${activeFulfillments.length}</strong>&nbsp;
          đơn giao hàng ra khỏi phiên <strong>${activeMtShippingExternal.code}</strong>?
        </div>
      `,
      cancelTitle: 'Đóng',
      confirmCss: 'btn-danger',
      confirmTitle: 'Xoá',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const lineIds = activeFulfillments.map(ffm => ffm.money_transaction_shipping_external_line_id);
          await this.transactionApi.removeMoneyTransactionShippingExternalLines(lineIds, activeMtShippingExternal.id);
          toastr.success('Xoá đơn ra khỏi phiên nhà vận chuyển thành công.')
          this.transactionsService.getMtShippingExternal(activeMtShippingExternal.id).then();
          modal.close().then();
        } catch (e) {
          debug.error('ERROR in deleteMoneyTransactionShippingExternals', e);
          toastr.error(
            'Xoá đơn ra khỏi phiên nhà vận chuyển không thành công.',
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  onChangeFilter() {
    const {fulfillments, activeMtShippingExternal} = this.transactionsStore.snapshot;
    switch (this.selectedFilter) {
      case 'all':
        return this.fulfillments = fulfillments;
      case 'valid':
        const validLines = activeMtShippingExternal.lines.filter(l => !l.import_error);
        this.fulfillments = TransactionsService.mapFulfillmentsByMtShippingExternalLines(fulfillments, validLines);
        return;
      case 'invalid':
        const invalidLines = activeMtShippingExternal.lines.filter(l => l.import_error);
        return this.fulfillments = TransactionsService.mapFulfillmentsByMtShippingExternalLines(fulfillments, invalidLines);
      default:
        return this.fulfillments = fulfillments;
    }
  }

  goToShippingExternalList() {
    this.router.navigateByUrl('transactions/externals');
  }

}
