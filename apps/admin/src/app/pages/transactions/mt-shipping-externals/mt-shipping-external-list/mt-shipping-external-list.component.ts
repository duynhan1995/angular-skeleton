import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { MoneyTransactionShippingExternal } from 'libs/models/admin/MoneyTransactionShippingExternal';
import { TransactionsService } from 'apps/admin/src/app/pages/transactions/transactions.service';
import { Filters } from '@etop/models';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { AdminMoneyTransactionApi } from '@etop/api';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { Router } from '@angular/router';
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {TransactionsStore} from "apps/admin/src/app/pages/transactions/transactions.store";
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-mt-shipping-external-list',
  templateUrl: './mt-shipping-external-list.component.html',
  styleUrls: ['./mt-shipping-external-list.component.scss']
})
export class MtShippingExternalListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('transactionTable', { static: true }) transactionTable: EtopTableComponent;
  @ViewChild('transactionSlider', { static: true }) transactionSlider: SideSliderComponent;

  connections$ = this.transactionsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  mtShippingExternalsLoading$ = this.transactionsStore.state$.pipe(
    map(s => s?.mtShippingExternalsLoading),
    distinctUntilChanged()
  );
  mtShippingExternalsList$ = this.transactionsStore.state$.pipe(map(s => s?.mtShippingExternals));
  activeMtShippingExternals$ = this.transactionsStore.state$.pipe(
    map(s => s?.activeMtShippingExternals),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  isLastPage$ = this.transactionsStore.state$.pipe(
    map(s => s?.mtShippingExternalsIsLastPage),
    distinctUntilChanged()
  );

  onNewMtShippingExternal = false;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: `Xoá phiên`,
      cssClass: 'text-danger',
      onClick: () => this.deleteTransactions(),
      permissions: ['admin/money_transaction_shipping_external:delete']
    }
  ];

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private dialog: DialogControllerService,
    private changeDetector: ChangeDetectorRef,
    private promiseQueue: PromiseQueueService,
    private headerController: HeaderControllerService,
    private transactionService: TransactionsService,
    private transactionApi: AdminMoneyTransactionApi,
    private transactionsStore: TransactionsStore
  ) {
    super();
  }

  get noPermission() {
    const permissions = this.auth.snapshot.permission.permissions;
    return !permissions.includes('admin/money_transaction_shipping_external:delete') &&
      !permissions.includes('admin/money_transaction_shipping_external:confirm')
  }

  get sliderTitle() {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    if (this.onNewMtShippingExternal) {
      return 'Tạo phiên nhà vận chuyển';
    }
    return `Thao tác trên <strong>${activeMts.length}</strong> phiên`;
  }

  get showPaging() {
    return !this.transactionTable.liteMode && !this.transactionTable.loading;
  }

  get emptyResultFilter() {
    const {
      mtShippingExternalsPaging,
      mtShippingExternalsFilters,
      mtShippingExternals
    } = this.transactionsStore.snapshot;
    return mtShippingExternalsPaging.offset == 0 &&
      mtShippingExternalsFilters.length > 0 &&
      mtShippingExternals.length == 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy phiên nhà vận chuyển phù hợp';
    }
    return 'Chưa có phiên nhà vận chuyển';
  }

  isActiveMt(mt: MoneyTransactionShippingExternal) {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    return activeMts.some(item => item.id == mt.id);
  }

  ngOnInit() {
    this.mtShippingExternalsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.transactionTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        } else {
          this.headerController.setActions([
            {
              title: 'Tạo phiên',
              cssClass: 'btn btn-primary',
              onClick: () => this. createMtShippingExternal(),
              permissions: ['admin/money_transaction_shipping_external:create']
            }
          ]);
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.transactionTable.toggleNextDisabled(true);
          this.transactionTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả phiên nhà vận chuyển.');
        }
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.transactionService.selectMtShippingExternals([]);
  }

  resetState() {
    this.transactionTable.toggleLiteMode(false);
    this.transactionSlider.toggleLiteMode(false);
    this.transactionTable.toggleNextDisabled(false);
    this.onNewMtShippingExternal = false;
  }

  filter(filters: Filters) {
    this.transactionService.setMtShippingExternalsFilters(filters);
    this.transactionTable.resetPagination();
  }

  loadPage({ page, perpage }) {
    this.transactionService.setMtShippingExternalsPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this.getMtShippingExternals();
  }

  getMtShippingExternals() {
    const connections = this.transactionsStore.snapshot.connections;
    if (connections?.length) {
      this.transactionService.getMtShippingExternals().then();
    } else {
      this.connections$.pipe(takeUntil(this.destroy$))
        .subscribe(conns => {
          if (conns?.length) {
            this.transactionService.getMtShippingExternals().then();
          }
        });
    }
  }

  itemSelected(item: MoneyTransactionShippingExternal) {
    this.onNewMtShippingExternal = false;

    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    activeMts.push(item);
    this.transactionService.selectMtShippingExternals(activeMts);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onNewMtShippingExternal = false;
    this.transactionService.selectMtShippingExternals([]);

    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    const selecteds = activeMts.length > 0 || this.onNewMtShippingExternal;
    this.transactionTable.toggleLiteMode(selecteds);
    this.transactionSlider.toggleLiteMode(selecteds);
    this.dropdownActions[0].title = `Xoá ${activeMts.length} phiên`;
  }

  createMtShippingExternal() {
    this.transactionService.selectMtShippingExternals([]);
    this.onNewMtShippingExternal = true;
    this._checkSelectMode();
  }


  deleteTransactions() {
    const activeMts = this.transactionsStore.snapshot.activeMtShippingExternals;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá phiên nhà vận chuyển`,
      body: `
        <div>Bạn có chắc muốn xoá <strong>${activeMts.length}</strong> phiên nhà vận chuyển?</div>
      `,
      cancelTitle: 'Đóng',
      confirmCss: 'btn-danger',
      confirmTitle: 'Xoá',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          let success = 0;
          const promises = activeMts.map(mt => async() => {
            try {
              await this.transactionApi.deleteMoneyTransactionShippingExternal(mt.id);
              success += 1;
            } catch(e) {
              debug.error('ERROR in deleteMoneyTransactionShippingExternal inside Promise.all', e);
            }
          });
          await this.promiseQueue.run(promises, 5);
          if (success == activeMts.length) {
            toastr.success(`Xoá phiên nhà vận chuyển thành công.`);
          } else if (success > 0) {
            toastr.warning(`Xoá thành công ${success}/${activeMts.length} phiên nhà vận chuyển.`);
          } else {
            toastr.error(`Xoá phiên nhà vận chuyển không thành công.`);
          }
          modal.close().then();
          this.getMtShippingExternals();
        } catch (e) {
          debug.error('ERROR in deleteMoneyTransactionShippingExternals', e);
          toastr.error(
            `Xoá phiên nhà vận chuyển không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

}
