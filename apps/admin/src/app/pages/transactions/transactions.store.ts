import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import {Connection} from "libs/models/Connection";
import {MoneyTransactionShippingExternal} from "libs/models/admin/MoneyTransactionShippingExternal";
import {Paging} from "@etop/shared";
import {Fulfillment} from "libs/models/Fulfillment";
import {MoneyTransactionShop} from "libs/models/MoneyTransactionShop";
import {MoneyTransactionTransfer} from "libs/models/admin/MoneyTransactionTransfer";
import { Filters } from '@etop/models';

export enum HeaderTab {
  externals = 'externals',
  shops = 'shops',
  transfers = 'transfers',
  scheduleds = 'scheduleds',
  splits = 'splits',
}

export interface MoneyTransactionData {
  headerTab: HeaderTab;
  connections: Connection[];

  mtShippingExternalsFilters: Filters;
  mtShippingExternalsPaging: Paging;
  mtShippingExternalsIsLastPage: boolean;
  mtShippingExternalsLoading: boolean;
  activeMtShippingExternal: MoneyTransactionShippingExternal;
  activeMtShippingExternals: MoneyTransactionShippingExternal[];
  mtShippingExternals: MoneyTransactionShippingExternal[];

  mtShopsFilters: Filters;
  mtShopsPaging: Paging;
  mtShopsIsLastPage: boolean;
  mtShopsLoading: boolean;
  activeMtShop: MoneyTransactionShop;
  activeMtShops: MoneyTransactionShop[];
  mtShops: MoneyTransactionShop[];

  mtTransfersFilters: Filters;
  mtTransfersPaging: Paging;
  mtTransfersIsLastPage: boolean;
  mtTransfersLoading: boolean;
  activeMtTransfer: MoneyTransactionTransfer;
  activeMtTransfers: MoneyTransactionTransfer[];
  mtTransfers: MoneyTransactionTransfer[];

  fulfillmentsLoading: boolean;
  fulfillments: Fulfillment[];
  activeFulfillments: Fulfillment[];
}

@Injectable()
export class TransactionsStore extends AStore<MoneyTransactionData> {
  initState = {
    headerTab: HeaderTab.externals,
    connections: [],

    mtShippingExternalsFilters: [],
    mtShippingExternalsPaging: null,
    mtShippingExternalsIsLastPage: false,
    mtShippingExternalsLoading: true,
    activeMtShippingExternal: null,
    activeMtShippingExternals: [],
    mtShippingExternals: [],

    mtShopsFilters: [],
    mtShopsPaging: null,
    mtShopsIsLastPage: false,
    mtShopsLoading: true,
    activeMtShop: null,
    activeMtShops: [],
    mtShops: [],

    mtTransfersFilters: [],
    mtTransfersPaging: null,
    mtTransfersIsLastPage: false,
    mtTransfersLoading: true,
    activeMtTransfer: null,
    activeMtTransfers: [],
    mtTransfers: [],

    fulfillmentsLoading: true,
    fulfillments: [],
    activeFulfillments: [],

    to_create_mt_transfer: false
  };

  constructor() {
    super();
  }

  changeHeaderTab(headerTab: HeaderTab) {
    if (headerTab && !Object.values(HeaderTab).includes(headerTab)) {
      this.setState({headerTab: HeaderTab.externals});
      return;
    }
    this.setState({ headerTab });
  }

  setConnections(connections: Connection[]) {
    this.setState({connections});
  }

  // NOTE: phiên NVC
  setMtShippingExternalsFilters(filters: Filters) {
    this.setState({mtShippingExternalsFilters: filters});
  }

  setMtShippingExternalsPaging(paging: Paging) {
    this.setState({mtShippingExternalsPaging: paging});
  }

  setMtShippingExternalsLastPage(lastPage: boolean) {
    this.setState({ mtShippingExternalsIsLastPage: lastPage })
  }

  setMtShippingExternalsLoading(loading: boolean) {
    this.setState({mtShippingExternalsLoading: loading});
  }

  setMtShippingExternals(transactions: MoneyTransactionShippingExternal[]) {
    this.setState({mtShippingExternals: transactions});
  }

  detailMtShippingExternal(transaction: MoneyTransactionShippingExternal) {
    this.setState({activeMtShippingExternal: transaction});
  }

  selectMtShippingExternals(transactions: MoneyTransactionShippingExternal[]) {
    this.setState({activeMtShippingExternals: transactions});
  }

  // NOTE: phiên SHOP
  setMtShopsFilters(filters: Filters) {
    this.setState({mtShopsFilters: filters});
  }

  setMtShopsPaging(paging: Paging) {
    this.setState({mtShopsPaging: paging});
  }

  setMtShopsLastPage(lastPage: boolean) {
    this.setState({ mtShopsIsLastPage: lastPage })
  }

  setMtShopsLoading(loading: boolean) {
    this.setState({mtShopsLoading: loading});
  }

  setMtShops(transactions: MoneyTransactionShop[]) {
    this.setState({mtShops: transactions});
  }

  detailMtShop(transaction: MoneyTransactionShop) {
    this.setState({activeMtShop: transaction});
  }

  selectMtShops(transactions: MoneyTransactionShop[]) {
    this.setState({activeMtShops: transactions});
  }

  //NOTE: phiên CK
  setMtTransfersFilters(filters: Filters) {
    this.setState({mtTransfersFilters: filters});
  }

  setMtTransfersPaging(paging: Paging) {
    this.setState({mtTransfersPaging: paging});
  }

  setMtTransfersLastPage(lastPage: boolean) {
    this.setState({ mtTransfersIsLastPage: lastPage })
  }

  setMtTransfersLoading(loading: boolean) {
    this.setState({mtTransfersLoading: loading});
  }

  setMtTransfers(transactions: MoneyTransactionTransfer[]) {
    this.setState({mtTransfers: transactions});
  }

  detailMtTransfer(transaction: MoneyTransactionTransfer) {
    this.setState({activeMtTransfer: transaction});
  }

  selectMtTransfers(transactions: MoneyTransactionTransfer[]) {
    this.setState({activeMtTransfers: transactions});
  }

  // NOTE: Fulfillment
  setFulfillmentsLoading(loading: boolean) {
    this.setState({fulfillmentsLoading: loading});
  }

  setFulfillments(fulfillments: Fulfillment[]) {
    this.setState({fulfillments});
  }

  selectFulfillments(fulfillments: Fulfillment[]) {
    this.setState({activeFulfillments: fulfillments});
  }

}
