import {
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FilterOperator, Filters } from '@etop/models';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";
import {FulfillmentService} from "apps/admin/src/app/pages/fulfillments/fulfillments.service";
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-fulfillment-list',
  templateUrl: './fulfillment-list.component.html',
  styleUrls: ['./fulfillment-list.component.scss']
})
export class FulfillmentListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('fulfillmentTable', { static: true }) fulfillmentTable: EtopTableComponent;
  @ViewChild('fulfillmentSlider', { static: true }) fulfillmentSlider: SideSliderComponent;

  tabs = [
    { name: 'Thông tin', value: 'detail_info' },
    { name: 'Yêu cầu hỗ trợ', value: 'support_request' },
    { name: 'Hàng hóa', value: 'product_info'},
  ];
  active_tab: 'detail_info' | 'support_request' | 'product_info' = 'detail_info';

  ffmsLoading$ = this.ffmsStore.state$.pipe(
    map(s => s?.fulfillmentsLoading),
    distinctUntilChanged()
  );
  ffmsList$ = this.ffmsStore.state$.pipe(map(s => s?.fulfillments));
  activeFulfillment$ = this.ffmsStore.state$.pipe(
    map(s => s?.activeFulfillment),
    distinctUntilChanged((prev, next) => prev?.id == next?.id)
  );

  isLastPage$ = this.ffmsStore.state$.pipe(map(s => s?.isLastPage), distinctUntilChanged());

  connections$ = this.ffmsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  queryParams = {
    code: '',
    type: '',
  };

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private util: UtilService,
    private router: Router,
    private route: ActivatedRoute,
    private ffmsStore: FulfillmentsStore,
    private ffmsService: FulfillmentService
  ) {
    super();
  }

  get sliderTitle() {
    return 'Chi tiết đơn giao hàng';
  }

  get showPaging() {
    return !this.fulfillmentTable.liteMode && !this.fulfillmentTable.loading;
  }

  get emptyResultFilter() {
    const {paging, filters, fulfillments} = this.ffmsStore.snapshot;
    return paging.offset == 0 && filters.length > 0 && fulfillments.length == 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy đơn giao hàng phù hợp';
    }
    return 'Chưa có đơn giao hàng';
  }

  ngOnInit() {
    this.ffmsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.fulfillmentTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.fulfillmentTable.toggleNextDisabled(true);
          this.fulfillmentTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả đơn giao hàng.');
        }
      });
  }

  ngOnDestroy() {
    this.ffmsService.selectFulfillment(null);
    this.ffmsService.setFilters([]);
  }

  resetState() {
    this.fulfillmentTable.toggleLiteMode(false);
    this.fulfillmentSlider.toggleLiteMode(false);
    this.fulfillmentTable.toggleNextDisabled(false);
  }

  async filter(filters: Filters) {
    if (this.queryParams.code) {
      await this.router.navigateByUrl('/fulfillments');
    }
    this.ffmsService.setFilters(filters);
    this.fulfillmentTable.resetPagination();
  }

  getShipmentFfms() {
    const connections = this.ffmsStore.snapshot.connections;
    if (connections?.length) {
      this.ffmsService.getFulfillments().then(_ => {
        if (this.queryParams.code) {
          this.detail(this.ffmsStore.snapshot.fulfillments[0]);
        }
      });
    } else {
      this.connections$.pipe(takeUntil(this.destroy$))
        .subscribe(conns => {
          if (conns?.length) {
            this.ffmsService.getFulfillments().then(_ => {
              if (this.queryParams.code) {
                this.detail(this.ffmsStore.snapshot.fulfillments[0]);
              }
            });
          }
        });
    }
  }

  loadPage({ page, perpage }) {
    this.ffmsService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this._paramsHandling();
    this.getShipmentFfms();
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    /* NOTE: required jobs when have queryParams.code:
    * 1/ Turn on state selected of an ffm row
    * 2/ Open Slide to view ffm detail
    * 3/ Reload ffm list when close slider OR when fill-in new values to FILTER INPUTS
    * */
    this.ffmsService.setFilters([{
      name: 'shipping_code',
      op: FilterOperator.eq,
      value: this.queryParams.code
    }])
  }

  detail(ffm: Fulfillment) {
    this.changeTab('detail_info');
    this.ffmsService.selectFulfillment(ffm);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.ffmsService.selectFulfillment(null);
    this._checkSelectMode();
    if (this.queryParams.code) {
      this.ffmsService.setFilters([]);
      this.router.navigateByUrl('/fulfillments').then(_ => {
        this.fulfillmentTable.resetPagination();
      });
    }
  }

  changeTab(tab_value) {
    this.active_tab = tab_value;
  }

  private _checkSelectMode() {
    const selected = !!this.ffmsStore.snapshot.activeFulfillment;
    this.fulfillmentTable.toggleLiteMode(selected);
    this.fulfillmentSlider.toggleLiteMode(selected);
  }

}
