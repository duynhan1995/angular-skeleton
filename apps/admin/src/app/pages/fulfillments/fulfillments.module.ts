import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule, MaterialModule, NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { FulfillmentsComponent } from 'apps/admin/src/app/pages/fulfillments/fulfillments.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FulfillmentListComponent } from 'apps/admin/src/app/pages/fulfillments/fulfillment-list/fulfillment-list.component';
import { FulfillmentRowComponent } from 'apps/admin/src/app/pages/fulfillments/components/fulfillment-row/fulfillment-row.component';
import { SingleFfmDetailModule } from 'apps/admin/src/app/pages/fulfillments/components/single-ffm-detail/single-ffm-detail.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {FulfillmentService} from "apps/admin/src/app/pages/fulfillments/fulfillments.service";
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";
import {AuthenticateModule} from "@etop/core";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";
import { UpdateFulfillmentModule } from '../../components/update-fulfillment/update-fulfillment.module';

const routes: Routes = [
  {
    path: '',
    component: FulfillmentsComponent
  }
];

@NgModule({
  declarations: [
    FulfillmentsComponent,
    FulfillmentListComponent,
    FulfillmentRowComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    SingleFfmDetailModule,
    NgbTooltipModule,
    FormsModule,
    EtopPipesModule,
    EtopMaterialModule,
    SideSliderModule,
    EtopCommonModule,
    EtopFilterModule,
    MaterialModule,
    AuthenticateModule,
    NotPermissionModule,
    CallCenterModule,
    UpdateFulfillmentModule
  ],
  providers: [
    FulfillmentService,
    FulfillmentsStore,
  ]
})
export class FulfillmentsModule { }
