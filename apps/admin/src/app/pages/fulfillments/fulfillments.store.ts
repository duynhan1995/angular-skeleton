import { Injectable } from '@angular/core';
import {Fulfillment} from "libs/models/Fulfillment";
import {AStore} from "apps/core/src/interfaces/AStore";
import {Connection} from "libs/models/Connection";
import {Paging} from "@etop/shared";
import { Filters } from '@etop/models';

export interface FulfillmentsData {
  filters: Filters;
  paging: Paging;
  isLastPage: boolean;
  fulfillmentsLoading: boolean;
  fulfillments: Fulfillment[];
  activeFulfillment: Fulfillment;
  connections: Connection[];
}

@Injectable()
export class FulfillmentsStore extends AStore<FulfillmentsData> {

  initState = {
    filters: null,
    paging: null,
    isLastPage: false,
    fulfillmentsLoading: true,
    fulfillments: [],
    activeFulfillment: null,

    connections: []
  }

  constructor() {
    super();
  }

  setConnections(connections: Connection[]) {
    this.setState({connections});
  }

  setFilters(filters: Filters) {
    this.setState({filters});
  }

  setPaging(paging: Paging) {
    this.setState({paging});
  }

  setLastPage(lastPage: boolean) {
    this.setState({ isLastPage: lastPage })
  }

  setFulfillmentsLoading(loading: boolean) {
    this.setState({fulfillmentsLoading: loading});
  }

  setFulfillments(fulfillments: Fulfillment[]) {
    this.setState({fulfillments});
  }

  selectFulfillment(activeFulfillment: Fulfillment) {
    this.setState({activeFulfillment});
  }

}
