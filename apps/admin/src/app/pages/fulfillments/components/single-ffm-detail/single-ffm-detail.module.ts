import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SingleFfmDetailComponent } from 'apps/admin/src/app/pages/fulfillments/components/single-ffm-detail/single-ffm-detail.component';
import { DetailInfoComponent } from 'apps/admin/src/app/pages/fulfillments/components/single-ffm-detail/detail-info/detail-info.component';
import { TicketInDetailModule } from 'apps/admin/src/app/components/ticket-in-detail/ticket-in-detail.module';
import {AuthenticateModule} from "@etop/core";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";
import {ProductInfoComponent} from "./product-info/product-info.component";


@NgModule({
  declarations: [
    SingleFfmDetailComponent,
    DetailInfoComponent,
    ProductInfoComponent,
  ],
  exports: [
    SingleFfmDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    TicketInDetailModule,
    EtopPipesModule,
    EtopMaterialModule,
    AuthenticateModule,
    CallCenterModule
  ]
})
export class SingleFfmDetailModule { }
