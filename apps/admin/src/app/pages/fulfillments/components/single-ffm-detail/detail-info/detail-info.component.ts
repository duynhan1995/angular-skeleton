import {Component, OnInit} from '@angular/core';
import {Fulfillment} from 'libs/models/Fulfillment';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {UpdateFfmModalComponent} from 'apps/admin/src/app/components/update-fulfillment/update-ffm-modal/update-ffm-modal.component';
import {UtilService} from 'apps/core/src/services/util.service';
import {FulfillmentService} from "apps/admin/src/app/pages/fulfillments/fulfillments.service";
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";
import {UpdateShippingFeeModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-fee-modal/update-shipping-fee-modal.component";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {FeeType} from "libs/models/Fee";
import {UpdateShippingStateModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-state-modal/update-shipping-state-modal.component";
import {UpdateCompensationAmountModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-compensation-amount-modal/update-compensation-amount-modal.component";
import {TelegramService} from "@etop/features";

@Component({
  selector: 'admin-detail-info',
  templateUrl: './detail-info.component.html',
  styleUrls: ['./detail-info.component.scss'],
})
export class DetailInfoComponent extends BaseComponent implements OnInit {

  activeFulfillment$ = this.ffmsStore.state$.pipe(
    map(s => s?.activeFulfillment),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  fulfillment: Fulfillment;

  constructor(
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private util: UtilService,
    private ffmsService: FulfillmentService,
    private ffmsStore: FulfillmentsStore,
    private telegramService: TelegramService
  ) {
    super();
  }

  get permissionsArray() {
    return [
      'admin/fulfillment:update',
      'admin/fulfillment_state:update',
      'admin/fulfillment_shipping_fees:update'
    ];
  }

  ngOnInit() {
    this.fulfillment = this.ffmsStore.snapshot.activeFulfillment;
    this.initBarcode();
    this.activeFulfillment$.pipe(takeUntil(this.destroy$))
      .subscribe(ffm => {
        this.fulfillment = ffm;
        this.initBarcode();
      });
  }

  initBarcode() {
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.fulfillment?.shipping_code),
      0
    );
  }

  updateFfm() {
    const modal = this.modalController.create({
      component: UpdateFfmModalComponent,
      componentProps: {
        ffm: this.util.deepClone(this.fulfillment)
      },
    });
    modal.show().then();
    modal.onDismiss().then(async(data) => {
      if (data?.ffmId) {
        this.ffmsService.getFulfillments(false).then(_ => {
          const _ffm = this.ffmsStore.snapshot.fulfillments.find(f => f.id == data.ffmId);
          this.ffmsService.selectFulfillment(_ffm);

          if (data.addShippingFeeAdjustment) {
            this.addShippingFeesTelegramMessage(_ffm, FeeType.adjustment);
          }
        });

      }
    });
  }

  updateCompensationAmount() {
    const modal = this.modalController.create({
      component: UpdateCompensationAmountModalComponent,
      componentProps: {
        ffm: this.util.deepClone(this.fulfillment)
      },
    });
    modal.show().then();
    modal.onDismiss().then(id => {
      if (id) {
        this.ffmsService.getFulfillments(false).then(_ => {
          const _ffm = this.ffmsStore.snapshot.fulfillments.find(f => f.id == id);
          this.ffmsService.selectFulfillment(_ffm);
        });
      }
    });
  }

  updateShippingState() {
    const modal = this.modalController.create({
      component: UpdateShippingStateModalComponent,
      componentProps: {
        ffm: this.util.deepClone(this.fulfillment)
      },
    });
    modal.show().then();
    modal.onDismiss().then(id => {
      if (id) {
        this.ffmsService.getFulfillments(false).then(_ => {
          const _ffm = this.ffmsStore.snapshot.fulfillments.find(f => f.id == id);
          this.ffmsService.selectFulfillment(_ffm);
        });
      }
    });
  }

  updateShippingFees() {
    const modal = this.modalController.create({
      component: UpdateShippingFeeModalComponent,
      componentProps: {
        ffm: this.util.deepClone(this.fulfillment)
      },
    });
    modal.show().then();
    modal.onDismiss().then(id => {
      if (id) {
        this.ffmsService.getFulfillments(false).then(_ => {
          const _ffm = this.ffmsStore.snapshot.fulfillments.find(f => f.id == id);
          this.ffmsService.selectFulfillment(_ffm);

          this.updateShippingFeesTelegramMessage(_ffm).then();
        });
      }
    });
  }

  doRedelivery() {
    const modal = this.dialog.createConfirmDialog({
      title: `Tính phí kích hoạt giao lại`,
      body: `
<div>Bạn có chắc muốn tính phí kích hoạt giao lại đơn giao hàng <strong>${this.fulfillment.shipping_code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmCss: 'btn btn-primary',
      closeAfterAction: false,
      onConfirm: async() => {
        try {
          await this.ffmsService.addShippingFee({id: this.fulfillment.id, shipping_fee_type: FeeType.redelivery});
          toastr.success('Tính phí kích hoạt giao lại thành công.');
          modal.close().then();
        } catch(e) {
          debug.error('ERROR in doRedelivery', e);
          toastr.error('Tính phí kích hoạt giao lại không thành công.', e.code && (e.message || e.msg));
        }
      }
    });
    modal.show().then();
  }

  trackingLink() {
    const link = FulfillmentService.trackingLink(this.fulfillment);
    if (!link) { return; }
    window.open(link);
  }

  updateShippingFeesTelegramMessage(fulfillment: Fulfillment) {
    const connection = this.ffmsStore.snapshot.connections.find(conn => conn.id == fulfillment.connection_id);
    return this.telegramService.updateFfmShippingFees(fulfillment, connection);
  }

  addShippingFeesTelegramMessage(fulfillment: Fulfillment, feeType: FeeType) {
    const connection = this.ffmsStore.snapshot.connections.find(conn => conn.id == fulfillment.connection_id);
    return this.telegramService.addShippingFees(fulfillment, connection, feeType);
  }

}
