import {Component, Input, OnInit} from '@angular/core';
import {Fulfillment} from 'libs/models/Fulfillment';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {UpdateFfmModalComponent} from 'apps/admin/src/app/components/update-fulfillment/update-ffm-modal/update-ffm-modal.component';
import {UtilService} from 'apps/core/src/services/util.service';
import {FulfillmentService} from "apps/admin/src/app/pages/fulfillments/fulfillments.service";
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";
import {UpdateShippingFeeModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-fee-modal/update-shipping-fee-modal.component";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {FeeType} from "libs/models/Fee";
import {UpdateShippingStateModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-state-modal/update-shipping-state-modal.component";
import {UpdateCompensationAmountModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-compensation-amount-modal/update-compensation-amount-modal.component";
import {TelegramService} from "@etop/features";

@Component({
  selector: 'admin-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss'],
})
export class ProductInfoComponent implements OnInit {
  fulfillment: Fulfillment;
  constructor(
    private ffmsStore: FulfillmentsStore,
  ) {}

  ngOnInit() {
    this.fulfillment = this.ffmsStore.snapshot.activeFulfillment;
  }
}
