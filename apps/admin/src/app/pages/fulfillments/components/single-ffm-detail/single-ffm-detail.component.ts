import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import {distinctUntilChanged, map} from "rxjs/operators";
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";

@Component({
  selector: 'admin-single-ffm-detail',
  templateUrl: './single-ffm-detail.component.html',
  styleUrls: ['./single-ffm-detail.component.scss']
})
export class SingleFfmDetailComponent extends BaseComponent implements OnInit {
  @Input() activeTab: 'detail_info' | 'support_request' = 'detail_info';

  activeFulfillment$ = this.ffmsStore.state$.pipe(
    map(s => s?.activeFulfillment),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  constructor(
    private ffmsStore: FulfillmentsStore
  ) {
    super();
  }

  ngOnInit() {}

}
