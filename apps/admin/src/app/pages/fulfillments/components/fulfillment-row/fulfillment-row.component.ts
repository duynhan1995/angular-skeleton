import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Router } from '@angular/router';
import {FulfillmentService} from "apps/admin/src/app/pages/fulfillments/fulfillments.service";

@Component({
  selector: '[admin-fulfillment-row]',
  templateUrl: './fulfillment-row.component.html',
  styleUrls: ['./fulfillment-row.component.scss']
})
export class FulfillmentRowComponent implements OnInit {
  @Input() fulfillment = new Fulfillment({});
  @Input() liteMode = false;

  constructor(
    private fulfillmentService: FulfillmentService,
    private router: Router,
  ) {}

  get showCancelReason() {
    return this.fulfillment.shipping_state == 'cancelled' && this.fulfillment.cancel_reason;
  }

  get hasMoneyTransaction() {
    const id = this.fulfillment.money_transaction_shipping_id;
    return id && id != "0";
  }

  ngOnInit() {
  }

  gotoMoneyTransactionDetail() {
    const id = this.fulfillment.money_transaction_shipping_id;
    if (!id || id == "0") {
      return;
    }
    this.router.navigateByUrl(`transactions/${id}`);
  }

  viewDetailOrder(order_code) {
    // this.orderStore.navigate('so', order_code);
  }

}
