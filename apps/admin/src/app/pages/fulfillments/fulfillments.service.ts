import { Injectable } from '@angular/core';
import {Fulfillment} from 'libs/models/Fulfillment';
import {AdminConnectionApi, AdminFulfillmentAPI, AdminFulfillmentApi} from '@etop/api';
import { Filters } from '@etop/models';
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";
import {Paging} from "@etop/shared";
import AddShippingFee = AdminFulfillmentAPI.AddShippingFee;
import {StringHandler} from "@etop/utils";
import {FeeType} from "libs/models/Fee";
import {TelegramService} from "@etop/features";

@Injectable()
export class FulfillmentService {

  constructor(
    private fulfillmentApi: AdminFulfillmentApi,
    private connectionApi: AdminConnectionApi,
    private store: FulfillmentsStore,
    private telegramService: TelegramService
  ) {}

  static trackingLink(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return null;
    }
    switch (fulfillment.carrier) {
      case 'ghn':
        return `https://donhang.ghn.vn/?order_code=${fulfillment.shipping_code}&code=${fulfillment.shipping_code}`;
      case 'vtpost':
        return `https://old.viettelpost.com.vn/Tracking?KEY=${fulfillment.shipping_code}`;
      case 'ahamove':
        return fulfillment.shipping_shared_link;
      default:
        return null;
    }
  }

  async getConnections() {
    try {
      const connections = await this.connectionApi.getConnections();

      const bias = {
        'topship-jt': 1,
        'topship-ghn': 2,
        'topship-ghn-v2': 2,
        'topship-ghtk': 3,
        'topship-vtp': 4
      };

      const builtinConnections = connections.filter(conn => conn.connection_method == 'builtin');
      const directConnections = connections.filter(conn => conn.connection_method != 'builtin');

      builtinConnections.sort((a, b) => {
        const _currName = StringHandler.createHandle(a.name);
        const _nextName = StringHandler.createHandle(b.name);
        return (bias[_currName] || 5) - (bias[_nextName] || 5);
      });

      this.store.setConnections(builtinConnections.concat(directConnections));
    } catch(e) {
      debug.error('ERROR in getting Connections', e);
    }
  }

  setFilters(filters: Filters) {
    this.store.setFilters(filters);
  }

  setPaging(paging: Paging) {
    this.store.setPaging(paging);
  }

  async getFulfillments(doLoading = true) {
    this.store.setLastPage(false);
    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      const {filters, paging, connections} = this.store.snapshot;
      const res = await this.fulfillmentApi.getFulfillments({filters, paging});
      if (paging.offset > 0 && res.fulfillments.length == 0) {
        this.store.setLastPage(true);
      } else {
        const fulfillments = res.fulfillments.map(f => Fulfillment.fulfillmentMap(f, connections));
        this.store.setFulfillments(fulfillments);
        if (doLoading) {
          this.selectFulfillment(null);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Fulfillments', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
  }

  selectFulfillment(ffm: Fulfillment) {
    this.store.selectFulfillment(ffm);
  }

  async addShippingFee(request: AddShippingFee) {
    await this.fulfillmentApi.addShippingFee(request);
    this.getFulfillments(false).then(_ => {
      const _ffm = this.store.snapshot.fulfillments.find(f => f.id == request.id);
      this.selectFulfillment(_ffm);
      this.addShippingFeesTelegramMessage(_ffm, request.shipping_fee_type);
    });
  }

  addShippingFeesTelegramMessage(fulfillment: Fulfillment, feeType: FeeType) {
    const connection = this.store.snapshot.connections.find(conn => conn.id == fulfillment.connection_id);
    return this.telegramService.addShippingFees(fulfillment, connection, feeType);
  }

}
