import { Component, OnInit } from '@angular/core';
import { FilterOperator, FilterOptions } from '@etop/models';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {FulfillmentService} from "apps/admin/src/app/pages/fulfillments/fulfillments.service";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {Connection} from "libs/models/Connection";
import {FulfillmentsStore} from "apps/admin/src/app/pages/fulfillments/fulfillments.store";
import {AuthenticateStore} from "@etop/core";
import {LocationQuery} from "@etop/state/location/location.query";

@Component({
  selector: 'admin-fulfillments',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <etop-filter [filters]="filters" (filterChanged)="fulfillmentList.filter($event)"></etop-filter>
      <admin-fulfillment-list #fulfillmentList></admin-fulfillment-list>
    </ng-template>`
})

export class FulfillmentsComponent extends PageBaseComponent implements OnInit {
  filters: FilterOptions;

  loading = false;

  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  connectionReady$ = this.fulfillmentsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((a, b) => a?.length == b?.length)
  );

  constructor(
    private auth: AuthenticateStore,
    private locationQuery: LocationQuery,
    private fulfillmentsStore: FulfillmentsStore,
    private fulfillmentService: FulfillmentService
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/fulfillment:view');
  }

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
    ${connection.connection_method == 'builtin' ? '<img class="topship-logo" alt="" src="assets/images/r-topship_256.png">' : ''}
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }

  ngOnInit() {
    this.fulfillmentService.getConnections().then();

    this.initFilters();
    this.locationReady$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.initFilters();
      });

    this.connectionReady$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.initFilters();
      });
  }

  private initFilters() {
    this.filters = [
      {
        label: 'Mã đơn giao hàng',
        name: 'shipping_code',
        type: 'input',
        fixed: true,
        operator: FilterOperator.eq
      },
      {
        label: 'Mã đơn hàng',
        name: 'order.code',
        type: 'input',
        fixed: true,
        operator: FilterOperator.eq
      },
      {
        label: 'Mã nội bộ',
        name: 'order.external_code, order.external_id',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: "Trạng thái giao hàng",
        name: "shipping_state",
        type: 'select',
        fixed: true,
        operator: FilterOperator.in,
        options: [
          { name: 'Tất cả', value: null },
          { name: "Đã tạo", value: 'created' },
          { name: 'Đã xác nhận', value: 'confirmed' },
          { name: 'Đang xử lý', value: 'processing' },
          { name: 'Đang lấy hàng', value: 'picking' },
          { name: 'Chờ giao', value: 'holding' },
          { name: 'Đang giao hàng', value: 'delivering' },
          { name: 'Đang trả hàng', value: 'returning' },
          { name: 'Đã giao hàng', value: 'delivered' },
          { name: 'Đã trả hàng', value: 'returned' },
          { name: 'Không giao được', value: 'undeliverable' },
          { name: 'Hủy', value: 'cancelled' },
          { name: 'Không xác định', value: 'unknown' }
        ]
      },
      {
        label: 'Trạng thái đối soát',
        name: 'etop_payment_status',
        type: 'select',
        fixed: true,
        operator: FilterOperator.in,
        options: [
          { name: 'Tất cả', value: null },
          { name: 'Chờ chuyển tiền', value: 'Z, S' },
          { name: 'Đã chuyển tiền', value: 'P, N' }
        ]
      },
      {
        label: 'Tên người nhận',
        name: 'address_to.full_name',
        type: 'input',
        operator: FilterOperator.contains
      },
      {
        label: 'Số điện thoại người nhận',
        name: 'address_to.phone',
        type: 'input',
        operator: FilterOperator.contains
      },
      {
        label: "Tên shop",
        name: "shop.id",
        type: "select",
        operator: FilterOperator.eq,
        options: [
          { name: 'Tất cả', value: null }
        ]
      },
      {
        label: "Tỉnh thành",
        name: "address_to.province_code",
        type: "autocomplete",
        operator: FilterOperator.eq,
        options: this.locationQuery.getValue().provincesList,
        displayMap: option => option && option.filter_name || null,
        valueMap: option => option && option.code || null
      },
      {
        label: "Quận huyện",
        name: "address_to.district_code",
        type: "autocomplete",
        operator: FilterOperator.eq,
        options: this.locationQuery.getValue().districtsList,
        displayMap: option => option && option.filter_name || null,
        valueMap: option => option && option.code || null
      },
      {
        label: 'Giá trị đơn hàng',
        name: 'basket_value',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Khối lượng (g)',
        name: 'chargeable_weight',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Bảo hiểm',
        name: 'include_insurance',
        type: 'select',
        operator: FilterOperator.eq,
        options: [
          { name: 'Tất cả', value: null },
          { name: 'Có', value: 'true' },
          { name: 'Không', value: 'false' }
        ]
      },
      {
        label: 'Tiền thu hộ',
        name: 'total_cod_amount',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Phí giao hàng',
        name: 'shipping_fee_shop',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Nhà vận chuyển',
        name: 'connection_id',
        type: 'select',
        operator: FilterOperator.eq,
        options: this.fulfillmentsStore.snapshot.connections,
        optionsHeight: '4rem',
        displayMap: option => option && FulfillmentsComponent.specialConnectionNameDisplay(option) || null,
        valueMap: option => option && option.id || null
      }
    ];
  }

}
