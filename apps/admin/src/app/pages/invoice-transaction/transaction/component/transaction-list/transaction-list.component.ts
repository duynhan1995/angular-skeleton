import {Component, OnInit, ViewChild} from '@angular/core';
import {AdminTransactionAPI} from '@etop/api/admin/admin-transaction.api';
import {CursorPaging, FilterOperator, FilterOptions, Filters, Shop} from '@etop/models';
import {EmptyType, EtopTableComponent} from '@etop/shared';
import {ShopQuery, ShopService} from '@etop/state/admin/shop';
import {
  TransactionQuery,
  TransactionService,
} from '@etop/state/admin/transaction';
import moment from 'moment';
import {takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";

@Component({
  selector: 'admin-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss'],
})
export class AdminTransactionListComponent extends BaseComponent implements OnInit {
  @ViewChild('transactionTable', {static: true})
  transactionTable: EtopTableComponent;
  isFirstRequestNewPage = true;
  transactions$ = this.transactionQuery.selectAll();
  loading$ = this.transactionQuery.selectLoading();
  ui$ = this.transactionQuery.select((state) => state.ui);
  filtersValue: AdminTransactionAPI.AdminGetTransactionFilter;


  selectedDate: any;

  filters: FilterOptions = [
    {
      label: 'Loại tham chiếu',
      name: 'ref_type',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        {name: 'Tất cả', value: ''},
        {name: 'Thanh toán dịch vụ', value: 'subscription'},
        {name: 'Nạp tiền', value: 'credit'}
      ]
    },
    {
      label: 'Mã tham chiếu',
      name: 'ref_id',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq,
    },
  ];
  shops: Shop[] = [];
  shopId: string;
  shopDisplayMap = (shop: Shop) => shop && `${shop.code} - ${shop.name}` || null;
  shopValueMap = (shop: Shop) => shop && shop.id || null;


  get emptyType() {
    return EmptyType.default;
  }

  get emptyDescription() {
    return 'Tất cả các giao dịch phát sinh trong quá trình sử dụng sẽ xuất hiện tại đây.';
  }

  constructor(
    private transactionService: TransactionService,
    private transactionQuery: TransactionQuery,
    private shopService: ShopService,
    private shopQuery: ShopQuery
  ) {
    super();
  }

  async loadPage({perpage, page}) {
    if (this.isFirstRequestNewPage) {
      return (this.isFirstRequestNewPage = false);
    }

    let paging: CursorPaging;
    let currentPaging = this.transactionQuery.currentUi.paging;
    let currentPage = this.transactionQuery.currentUi.currentPage;

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage && page != 1) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    this.transactionService.setPaging(paging); // paging dùng để gửi request lên server
    await this.transactionService.getTransactions();
    this.transactionService.setCurrentPage(page); // set số trang hiện tại
  }

  async ngOnInit() {
    this.transactions$.pipe(takeUntil(this.destroy$)).subscribe(async (transactions) => {
      let shopIds = [];
      transactions.forEach(transaction => {
        shopIds.push(transaction.account_id);
      });
      this.shopService.setFilter({shop_ids: shopIds});
      await this.shopService.getShops();
    });
    this.filter([]);
  }

  async onShopSearching(searchText: string) {
    this.shopService.setQuery({name: searchText})
    await this.shopService.getShops(false);
    this.shops = this.shopQuery.getAll();
  }

  onShopSelect() {
    this.filtersValue = {
      ...this.filtersValue,
      account_id: this.shopId || null,
    };
    this.transactionService.setFilter(this.filtersValue);
    this.transactionTable.resetPagination();
  }


  filter(filters: Filters) {
    let filter: AdminTransactionAPI.AdminGetTransactionFilter = {};
    this.filtersValue = {
      ...this.filtersValue,
      ref_type: null,
      ref_id: null,
    };
    filters.forEach((f) => {
      filter[f.name] = f.value;
    })
    this.filtersValue = {
      ...this.filtersValue,
      ...filter
    };
    this.transactionService.setFilter(this.filtersValue);
    this.transactionTable.resetPagination();
  }

  async onChangeFilter(event) {
    if (event.startDate?._isValid) {
      this.selectedDate = event;
      this.filtersValue = {
        ...this.filtersValue,
        date_from: event.startDate,
        date_to: event.endDate,
      }
      this.transactionService.setFilter(this.filtersValue);
      this.transactionTable.resetPagination();
    } else {
      this.filtersValue = {
        ...this.filtersValue,
        date_from: null,
        date_to: null,
      }
      this.transactionService.setFilter(this.filtersValue);
      this.transactionTable.resetPagination();
    }
  }
}
