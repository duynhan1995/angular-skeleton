import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';
import {Shop} from '@etop/models';
import {Transaction, TransactionType} from '@etop/models/Transaction';
import {ShopQuery, ShopService} from '@etop/state/admin/shop';
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import {BaseComponent} from "@etop/core";

@Component({
  selector: '[admin-transaction-row]',
  templateUrl: './transaction-row.component.html',
  styleUrls: ['./transaction-row.component.scss'],
})
export class AdminTransactionRowComponent extends BaseComponent implements OnInit {
  @Input() transaction = new Transaction({});
  @Input() liteMode = false;

  shop = new Shop({});

  constructor(
    private router: Router,
    private shopService: ShopService,
    private shopQuery: ShopQuery
  ) {
    super();
  }

  async ngOnInit() {
    let shops$ = this.shopQuery.selectAll({filterBy: shop => shop.id == this.transaction.account_id});
    shops$.pipe(takeUntil(this.destroy$)).subscribe(shops => {
      if (shops.length) {
        this.shop = shops[0];
      }
    });

  }

  invoiceDetail() {
    if (this.transaction?.type == TransactionType.invoice) {
      this.router.navigateByUrl(`invoice-transaction/invoice?id=${this.transaction?.referral_ids[0]}`);
    }
    ;
  }

  shopDetail() {
    if (this.shop?.id) {
      this.router.navigateByUrl(`accounts/shops?id=${this.shop?.id}`);
    }
    ;
  }
}
