import { CommonModule } from '@angular/common';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopFilterModule,
  EtopPipesModule,
  MaterialModule,
  NotPermissionModule,
} from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';
import { AdminTransactionListComponent } from './component/transaction-list/transaction-list.component';
import { AdminTransactionRowComponent } from './component/transaction-row/transaction-row.component';
import { AdminTransactionComponent } from './transaction.component';

const routes: Routes = [
  {
    path: '',
    component: AdminTransactionComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    EtopPipesModule,
    MaterialModule,
    MatTableModule,
    EtopCommonModule,
    EmptyPageModule,
    NotPermissionModule,
    EtopFilterModule,
  ],
  exports: [],
  declarations: [
    AdminTransactionComponent,
    AdminTransactionListComponent,
    AdminTransactionRowComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class TransactionModule {}
