import { Component, OnInit } from '@angular/core';
import { InvoiceTransactionTab } from '@etop/models/Invoice';
import { InvoiceService } from '@etop/state/admin/invoice';
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-transaction',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
    <admin-transaction-list></admin-transaction-list>
    </ng-template>
  `,
})
export class AdminTransactionComponent implements OnInit {

  constructor(
    private invoiceService: InvoiceService,
    private auth: AuthenticateStore
  ) {}

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/transaction:view');
  }

  async ngOnInit() {
    this.invoiceService.changeHeaderTab(InvoiceTransactionTab.transaction);
  }
  
}
