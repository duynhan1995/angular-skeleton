import {CommonModule} from "@angular/common";
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {EtopCommonModule} from "@etop/shared";
import {SharedModule} from "apps/shared/src/shared.module";
import {AdminInvoiceTransactionComponent} from "./invoice-transaction.component";

const routes: Routes = [
  {
    path: '',
    component: AdminInvoiceTransactionComponent,
    children: [
      {
        path: 'invoice',
        loadChildren: () => import('./invoice/invoice.module').then((m) => m.InvoiceModule),
      },
      {
        path: 'transaction',
        loadChildren: () => import('./transaction/transaction.module').then((m) => m.TransactionModule),
      },
      {
        path: '**',
        redirectTo: 'invoice',
      },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    EtopCommonModule,
  ],
  exports: [],
  declarations: [AdminInvoiceTransactionComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AdminInvoiceTransactionModule {
}
