import {InvoiceTransactionTab} from '@etop/models/Invoice';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {InvoiceQuery} from '@etop/state/admin/invoice';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {distinctUntilChanged, map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'admin-invoice-transaction',
  template: `
    <router-outlet></router-outlet> `,
  styleUrls: ['./invoice-transaction.component.scss']
})
export class AdminInvoiceTransactionComponent implements OnInit, OnDestroy {
  tab$ = this.invoiceQuery.select((state) => state.ui.tab);

  constructor(
    private headerController: HeaderControllerService,
    private invoiceQuery: InvoiceQuery,
    private router: Router,
  ) {
  }

  async ngOnInit() {
    this.setupTabs();
  }

  checkTab(tabName) {
    return this.tab$.pipe(
      map((tab) => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Hóa đơn',
          name: InvoiceTransactionTab.invoice,
          active: false,
          onClick: () => {
            this.tabClick(InvoiceTransactionTab.invoice).then((_) => {
            });
          },
        },
        {
          title: 'Giao dịch',
          name: InvoiceTransactionTab.transaction,
          active: false,
          onClick: () => {
            this.tabClick(InvoiceTransactionTab.transaction).then((_) => {
            });
          },
        },
      ].map((tab) => {
        this.checkTab(tab.name).subscribe((active) => (tab.active = active));
        return tab;
      })
    );
  }

  private async tabClick(tabName: InvoiceTransactionTab) {
    await this.router.navigateByUrl(
      `/invoice-transaction/${tabName}`
    );
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.headerController.clearContent();
  }
}
