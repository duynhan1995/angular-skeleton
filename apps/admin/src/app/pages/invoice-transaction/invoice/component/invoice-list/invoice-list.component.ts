import {Component, OnInit, ViewChild} from '@angular/core';
import {AdminInvoiceAPI, AdminTransactionAPI} from '@etop/api';
import {CursorPaging, FilterOperator, FilterOptions, Filters, Shop} from '@etop/models';
import {EmptyType, EtopTableComponent} from '@etop/shared';
import {InvoiceQuery, InvoiceService} from '@etop/state/admin/invoice';
import { ShopQuery, ShopService } from '@etop/state/admin/shop';
import {takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";

@Component({
  selector: 'admin-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
})
export class AdminInvoiceListComponent extends BaseComponent implements OnInit {
  @ViewChild('invoiceTable', {static: true}) invoiceTable: EtopTableComponent;
  isFirstRequestNewPage = true;
  invoices$ = this.invoiceQuery.selectAll();
  loading$ = this.invoiceQuery.selectLoading();
  ui$ = this.invoiceQuery.select((state) => state.ui);
  filtersValue: AdminInvoiceAPI.GetInvoiceFilter;

  selectedDate: any;

  filters: FilterOptions = [
    {
      label: 'Loại tham chiếu',
      name: 'ref_type',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: 'Thanh toán dịch vụ', value: 'subscription' },
        { name: 'Nạp tiền', value: 'credit' }
      ]
    },
    {
      label: 'Mã tham chiếu',
      name: 'ref_id',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq,
    },
  ];
  shops : Shop[] = [];
  shopId: string;
  shopDisplayMap = (shop: Shop) => shop && `${shop.code} - ${shop.name}` || null;
  shopValueMap = (shop: Shop) => shop && shop.id || null;

  get emptyType() {
    return EmptyType.default;
  }

  get emptyDescription() {
    return 'Hoá đơn thanh toán dịch vụ, nạp tiền vào tài khoản sẽ xuất hiện tại đây.';
  }


  constructor(
    private invoiceService: InvoiceService,
    private invoiceQuery: InvoiceQuery,
    private shopService: ShopService,
    private shopQuery: ShopQuery
  ) {
    super();
  }

  async loadPage({perpage, page}) {
    if (this.isFirstRequestNewPage) {
      return (this.isFirstRequestNewPage = false);
    }

    let paging: CursorPaging;
    let currentPaging = this.invoiceQuery.currentUi.paging;
    let currentPage = this.invoiceQuery.currentUi.currentPage;

    // perpage change or init page
    if (page == 1) {
      paging = {
        limit: perpage,
        after: '.',
      };
    }

    // navigate next page
    if (page > currentPage) {
      paging = {
        limit: perpage,
        after: currentPaging.next,
      };
    }

    // navigate previous page
    if (page < currentPage && page != 1) {
      paging = {
        limit: perpage,
        before: currentPaging.prev,
      };
    }

    this.invoiceService.setPaging(paging); // paging dùng để gửi request lên server
    await this.invoiceService.getInvoices();
    this.invoiceService.setCurrentPage(page); // set số trang hiện tại
  }

  async ngOnInit() {
    this.invoices$.pipe(takeUntil(this.destroy$)).subscribe(async (invoices) => {
      let shopIds = [];
      invoices.forEach(invoice => {
        shopIds.push(invoice.account_id);
      });
      this.shopService.setFilter({shop_ids: shopIds});
      await this.shopService.getShops();
    });
  }

  async onShopSearching(searchText: string) {
    this.shopService.setQuery({ name: searchText })
    await this.shopService.getShops(false);
    this.shops = this.shopQuery.getAll();
  }

  onShopSelect() {
    this.filtersValue = {
      ...this.filtersValue,
      account_id: this.shopId || null,
    };
    this.invoiceService.setFilter(this.filtersValue);
    this.invoiceTable.resetPagination();
  }


  filter(filters: Filters) {
    let filter: AdminInvoiceAPI.GetInvoiceFilter = {};
    this.filtersValue = {
      ...this.filtersValue,
      ref_type: null,
      ref_id: null,
    };
    filters.forEach((f) => {
      filter[f.name] = f.value;
    })
    this.filtersValue = {
      ...this.filtersValue,
      ...filter
    };
    this.invoiceService.setFilter(this.filtersValue);
    this.invoiceTable.resetPagination();
  }

  async onChangeFilter(event) {
    if (event.startDate?._isValid) {
      this.selectedDate = event;
      this.filtersValue = {
        ...this.filtersValue,
        date_from: event.startDate,
        date_to: event.endDate,
      }
      this.invoiceService.setFilter(this.filtersValue);
      this.invoiceTable.resetPagination();
    } else {
      this.filtersValue = {
        ...this.filtersValue,
        date_from: null,
        date_to: null,
      }
      this.invoiceService.setFilter(this.filtersValue);
      this.invoiceTable.resetPagination();
    }

  }
}
