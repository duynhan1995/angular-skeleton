import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';
import {Shop} from '@etop/models';
import {Invoice, InvoiceType} from '@etop/models/Invoice';
import {ShopQuery, ShopService} from '@etop/state/admin/shop';
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import {BaseComponent} from "@etop/core";

@Component({
  selector: '[admin-invoice-row]',
  templateUrl: './invoice-row.component.html',
  styleUrls: ['./invoice-row.component.scss'],
})
export class AdminInvoiceRowComponent extends BaseComponent implements OnInit {
  @Input() invoice = new Invoice({});
  @Input() liteMode = false;

  invoiceType = {
    in: InvoiceType.in,
    out: InvoiceType.out,
  };
  shop = new Shop({});

  constructor(
    private router: Router,
    private shopService: ShopService,
    private shopQuery: ShopQuery
  ) {
    super();
  }

  async ngOnInit() {
    let shops$ = this.shopQuery.selectAll({filterBy: shop => shop.id == this.invoice.account_id});
    shops$.pipe(takeUntil(this.destroy$)).subscribe(shops => {
      if (shops.length) {
        this.shop = shops[0];
      }
    });
  }

  shopDetail() {
    if (this.shop?.id) {
      this.router.navigateByUrl(`accounts/shops?id=${this.shop?.id}`);
    }
    ;
  }
}
