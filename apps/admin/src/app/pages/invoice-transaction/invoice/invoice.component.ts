import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { InvoiceTransactionTab } from '@etop/models/Invoice';
import { InvoiceService } from '@etop/state/admin/invoice';

@Component({
  selector: 'admin-invoice',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
    <admin-invoice-list></admin-invoice-list>
    </ng-template>
  `,
})
export class AdminInvoiceComponent implements OnInit {
  constructor(
    private invoiceService: InvoiceService,
    private auth: AuthenticateStore
    ) {}

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/invoice:view');
  }

  async ngOnInit() {
    this.invoiceService.changeHeaderTab(InvoiceTransactionTab.invoice);
  }

}
