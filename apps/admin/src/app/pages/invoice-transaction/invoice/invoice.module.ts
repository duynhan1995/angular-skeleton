import { CommonModule } from '@angular/common';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopFilterModule,
  EtopPipesModule,
  MaterialModule,
} from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';
import { AdminInvoiceListComponent } from './component/invoice-list/invoice-list.component';
import { AdminInvoiceRowComponent } from './component/invoice-row/invoice-row.component';
import { AdminInvoiceComponent } from './invoice.component';

const routes: Routes = [
  {
    path: '',
    component: AdminInvoiceComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    EtopPipesModule,
    MaterialModule,
    MatTableModule,
    EtopCommonModule,
    EmptyPageModule,
    EtopFilterModule,
  ],
  exports: [],
  declarations: [
    AdminInvoiceComponent,
    AdminInvoiceListComponent,
    AdminInvoiceRowComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class InvoiceModule {}
