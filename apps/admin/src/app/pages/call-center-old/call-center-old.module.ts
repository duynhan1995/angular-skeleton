import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {CallCenterComponent} from "apps/admin/src/app/pages/call-center-old/call-center.component";
import {ContactListComponent} from "apps/admin/src/app/pages/call-center-old/components/contact-list/contact-list.component";
import {FormsModule} from "@angular/forms";
import {ContactListItemComponent} from "apps/admin/src/app/pages/call-center-old/components/contact-list-item/contact-list-item.component";
import {ContactInfoComponent} from "apps/admin/src/app/pages/call-center-old/components/contact-info/contact-info.component";
import {ContactHistoryComponent} from "apps/admin/src/app/pages/call-center-old/components/contact-history/contact-history.component";
import {ContactHistoryItemComponent} from "apps/admin/src/app/pages/call-center-old/components/contact-history-item/contact-history-item.component";
import {HttpService} from "apps/admin/src/app/pages/call-center-old/services/http.service";
import {VTigerService} from "apps/admin/src/services/vtiger.service";
import {OrderService} from "apps/admin/src/app/pages/call-center-old/services/order.service";
import {FulfillmentService} from "apps/admin/src/app/pages/call-center-old/services/fulfillment.service";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";

const routes: Routes = [
  {
    path: '',
    component: CallCenterComponent
  }
];

@NgModule({
  declarations: [
    CallCenterComponent,
    ContactListComponent,
    ContactListItemComponent,
    ContactInfoComponent,
    ContactHistoryComponent,
    ContactHistoryItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    CallCenterModule
  ],
  providers: [
    HttpService,
    VTigerService,
    OrderService,
    FulfillmentService
  ]
})
export class CallCenterOldModule { }
