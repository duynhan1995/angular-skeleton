import { Component, OnInit } from '@angular/core';
import {Contact} from "apps/admin/src/app/pages/call-center-old/models/Contact";
import {VTigerService} from "apps/admin/src/services/vtiger.service";
import {CallService} from "apps/admin/src/app/modules/callcenter/call.service";

@Component({
  selector: 'admin-call-center',
  templateUrl: './call-center.component.html',
  styleUrls: ['./call-center.component.scss']
})
export class CallCenterComponent implements OnInit {
  _searchTimeout = null;

  selectedContact: Contact = null;

  constructor(
    private vtiger: VTigerService,
    private callService: CallService,
  ) {}

  ngOnInit() {
    this.callService.bootstrap();
  }

  call(contact: Contact) {
    // this.callService.makeCall(contact.phone)
    //   .then(callRes => console.log("CALL RES", callRes))
    //   .catch(e => toastr.error(e.message));
    const lines = this.callService.getLines();
    lines[0].makeCall(contact.phone);
  }

  contactSelected(contact) {
    this.selectedContact = contact;
  }

}
