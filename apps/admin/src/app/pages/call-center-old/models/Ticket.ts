import {BaseModel} from "@etop/core/base/base-model";
import {Order} from "apps/admin/src/app/pages/call-center-old/models/Order";

export class Ticket extends BaseModel {
  assigned_user: AssignedUser;
  assigned_user_id: string;
  code: string;
  company: string;
  contact_id: string;
  created_at: string;
  days: string;
  description: string;
  error_report?: string;
  etop_id: string;
  etop_note: string;
  ffm_code: string;
  ffm_id: string;
  ffm_url: string;
  from_portal: string;
  hours: string;
  id: string;
  note: string;
  old_value: string;
  order_code: string;
  order_id: string;
  order: Order;
  parent_id: string;
  product_id: string;
  provider: string;
  reason: string;
  solution: string;
  source: string;
  starred: string;
  substatus: string;
  substatus_display: string;
  tags: string;
  ticket_no: string;
  ticket_title: string;
  ticketcategories: string;
  ticketstatus: string;
  update_log: string;
  new_value: string;
  closed_by: string;

  closed_user: {
    email: string,
    first_name: string,
    last_name: string,
  };
}

export class AssignedUser {
  first_name: string;
  last_name: string;
  email: string;
}
