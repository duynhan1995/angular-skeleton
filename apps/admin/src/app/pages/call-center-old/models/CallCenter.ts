import {BaseModel} from "@etop/core/base/base-model";

export class CallLog extends BaseModel {
  cdr_id: string;
  call_id: string;
  sip_call_id: string;
  cause: string;
  q850_cause: string;
  from_extension: string;
  to_extension: string;
  from_number: string;
  to_number: string;
  duration: number;
  direction: number;
  time_started: number;
  time_connected: number;
  time_ended: number;
  recording_url: string;
  record_file_size: number;
  lastname: string;
  company: string;
  phone: string;
  fulltext_search: string;
  date_started_parsed: string;
  date_connected_parsed: string;
  date_ended_parsed: string;
  time_started_parsed: string;
  time_connected_parsed: string;
  time_ended_parsed: string;
}
