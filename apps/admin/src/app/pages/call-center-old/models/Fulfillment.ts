import {Shop} from "libs/models/Account";
import {BaseModel} from "@etop/core/base/base-model";
import {OrderAddress} from "libs/models/Address";
import {Order} from "apps/admin/src/app/pages/call-center-old/models/Order";

export class Fulfillment extends BaseModel {
  actual_compensation_amount: number;
  address_from: OrderAddress;
  address_to: OrderAddress;
  admin_note: string;
  cancelled_at: Date;
  carrier: string;
  chargeable_weight: number;
  closed_at: Date;
  cod_etop_transfered_at: string;
  created_at: Date;
  expected_delivery_at: string;
  expected_pick_at: string;
  expected_picking_time: Array<Date>;
  lines: Array<any>;
  logs: any[];
  o_data: any;
  order_id: string;
  order: Order;
  p_data: any;
  profit: number;
  provider_discount: number;
  provider_shipping_fee_lines: Array<ShippingFeeShopLine>
  return_fee: number;
  shipping_code: string;
  shipping_fee_customer: number;
  shipping_fee_provider: number;
  shipping_fee_shop_lines: Array<ShippingFeeShopLine>;
  shipping_fee_shop: number;
  shipping_note: string;
  shipping_provider: string;
  shipping_state: string;
  shop_id: string;
  shop: Shop;
  status_display: string;
  status: string;
  supplier_id: string;
  total_amount: number;
  total_cod_amount: number;
  total_fee: number;
  total_items: number;
  total_weight: number;
  updated_at: Date;
  x_shipping_delivered_at: string;
  x_shipping_logs: any[];
  x_shipping_picked_at: string;
  x_sync_status: string;
  x_shipping_note: string;

  pickup_address: OrderAddress;
  shipping_address: OrderAddress;
  shipping_service_name: string;
  try_on: string;

  status_mapped: {
    display: string,
    state: string,
  };

  estimated_pickup_at: string;
  estimated_delivery_at: string;
  x_shipping_sub_state: string;
}
export class ShippingFeeShopLine {
  cost: number;
  external_service_id: number;
  external_service_name: string;
  name: string;
  shipping_fee_type: string;
}
