import {BaseModel} from "@etop/core/base/base-model";
import {Address, ShopShippingAddress} from "libs/models/Address";
import {Fulfillment} from "apps/admin/src/app/pages/call-center-old/models/Fulfillment";

export class Order extends BaseModel {
  id: string;
  shop_id: string;
  partner_id: string;
  shop_name: string;
  code: string;
  ed_code: string;
  external_id: string;
  source: string;
  payment_method: string;
  created_at: string;
  processed_at: string;
  updated_at: string;
  closed_at: string;
  confirmed_at: string;
  cancelled_at: string;
  cancel_reason: string;
  created_by: string;
  updated_by: string;
  confirmed_by: string;
  cancelled_by: string;
  sh_confirm: string;
  confirm: string;
  status: string;
  fulfillment_status: string;
  payment_status: string;
  etop_payment_status: string;

  total_items: number;
  basket_value: number;
  total_weight: number;
  total_discount: number;
  total_amount: number;
  order_note: string;
  shipping_note: string;
  shop_shipping_fee: number;
  shop_cod: number;
  reference_url: string;
  ghn_note_code: string;

  status_display: string;
  payment_status_display: string;
  source_display: string;

  ghn_note_display: string;

  discount: Discount;

  customer: any;
  customer_address: Address;
  billing_address: Address;
  shipping_address: Address;
  shop_shipping: ShopShippingAddress;
  shipping: any;
  total_fee: number;

  fulfillments: Array<Fulfillment>;
  lines: Array<OrderLine>;
  fee_lines: Array<any>;

  o_data: any;
  p_data: any;
}

export class OrderLine {
  order_id: string;
  variant_id: string;
  supplier_id: string;
  x_variant_id: string;
  product_name: string;
  product_exists: boolean;
  updated_at: string;
  closed_at: string;
  confirmed_at: string;
  cancelled_at: string;
  cancel_reason: string;
  s_confirm: string;
  quantity: number;
  list_price: number;
  retail_price: number;
  payment_price: number;
  image_url: string;
  attributes: Array<any>;

  o_data: any;
  p_data: any;
}
export class Discount {
  code: string;
  type: string;
  amount: number;
}
