import {Injectable} from '@angular/core';
import List from "identical-list";
import {UtilService} from "apps/core/src/services/util.service";
import {Subject} from "rxjs";
import {ShopShippingAddress} from "libs/models/Address";
import {FulfillmentService} from "apps/admin/src/app/pages/call-center-old/services/fulfillment.service";
import {HttpService} from "apps/admin/src/app/pages/call-center-old/services/http.service";
import {Order} from "apps/admin/src/app/pages/call-center-old/models/Order";
import {Fulfillment} from "apps/admin/src/app/pages/call-center-old/models/Fulfillment";

@Injectable()
export class OrderService {

  orderList = new List<Order>();
  loading = true;

  onOrderListUpdated = new Subject();
  onReady = new Subject();

  partners: any = [];

  constructor(
    private fulfillmentService: FulfillmentService,
    private http: HttpService,
    private util: UtilService,
  ) {
    this.init();
  }

  async init() {
    this.partners = await this.getPartners();
  }

  getPartners() {
    return this.http.post('api/etop.Account/GetPublicPartners', {}).toPromise()
      .then(res => res.accounts)
      .catch(e => {
        debug.error('Error: ' + e);
        return [];
      });
  }

  orderMap(order) {
    let o = order;
    o.shop_shipping = order.shop_shipping || new ShopShippingAddress();
    o.payment_status_display = this.mapPaymentStatus(order);

    let partner = this.partners.find(p => p.id == o.partner_id);
    o.source_display = this.util.orderSourceMap(order.source, partner ? partner.name : '');

    o.ghn_note_display = this.util.orderGhnNoteMap(order.ghn_note_code);
    o.fulfillments = o.fulfillments.map(f => this.fulfillmentService.fulfillmentMap(f));
    o.status_display = this.mapOrderStatus(o);
    return o;
  }

  async getOrders(start?: number, perpage?: number, pFilters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    let filters = [];

    if (pFilters instanceof Array) {
      filters = pFilters.map(f => new Object({
        name: f.name_rule == "date" ? "created_at" : f.name_rule,
        op: this.util.filterOperatorMap(f.compair_rule, f.name_rule == "date"),
        value: f.value
      }));
    }

    return this.http.post("api/admin.Order/GetOrders", {
      paging,
      filters
    }).toPromise()
      .then(res => {
        return {
          paging: res.paging,
          orders: res.orders.map(o => this.orderMap(o))
        };
      });
  }

  mapOrderStatus(order: Order) {
    const statusMap = {
      "Z": "Mới",
      "S": "Đang xử lý",
      "P": "Thành công",
      "N": "Huỷ",
      "NS": "Trả hàng"
    };
    return statusMap[order.status] || "Không xác định";
  }

  mapPaymentStatus(order) {
    const statusMap = {
      N: "Chưa thanh toán",
      Z: "Không cần thanh toán",
      P: "Đã thanh toán",
      S: "Cần thanh toán"
    };
    return statusMap[order.etop_payment_status];
  }

  getOrderById(id: string) {
    return this.orderList.get(id);
  }

  getOrder(id) {
    return this.http.post("api/admin.Order/GetOrder", {"id": id})
      .toPromise()
      .then(order => this.orderMap(order));
  }

  getOrdersByIds(ids) {
    return this.http.post("api/admin.Order/GetOrdersByIDs", {"ids": ids}).toPromise()
      .then(res => res.orders);
  }

  updateOrderStatus(status) {
    return this.http.post("api/admin.Order/UpdateOrdersStatus", {"updates": [status]}).toPromise();
  }

  async getFulfillmentLogs(order: Order): Promise<Fulfillment[]> {
    if (!order.fulfillments) {
      return [];
    }
    return await Promise.all(order.fulfillments.map(async ffm => {
      const l = await this.fulfillmentService.getFulfillmentHistory(ffm);
      return new Fulfillment({...ffm, logs: l});
    }));
  }


}
