import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AuthenticateStore} from "@etop/core";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";

@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient,
    private authenServer: AuthenticateStore
  ) {
  }

  createHeader(token?: string) {
    const jwt = token || this.authenServer.snapshot.token;
    return new HttpHeaders().set('Authorization', 'Bearer ' + jwt)
      .set("Content-Type", "application/json");
  }

  createDefaultOption(headers?: any) {
    return {
      headers: headers || this.createHeader(),
      params: null,
      observe: 'response'
    };
  }

  get(link, query?: object): Observable<any> {
    const options = this.createDefaultOption();

    if (query) {
      let params = new HttpParams();
      (<any>Object).entries(query).forEach(entry => {
        const key = entry[0], value = entry[1];

        if (Array.isArray(value)) {
          value.forEach(val => {
            params = params.set(`${key}`, val);
          });
        } else {
          params = params.set(key, value);
        }
      });

      options.params = params;
    }


    return this.getWithOptions(link, options);
  }

  post(link, data): Observable<any> {
    const options = this.createDefaultOption();
    return this.postWithOptions(link, data, options);
  }

  postForm(link, formData: FormData): Observable<any> {
    const headers = new HttpHeaders().set('Authorization', "Bearer " + this.authenServer.snapshot.token);
    const options = {headers: headers};
    return this.http.post(this.getUrl(link), formData, options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  put(link, data?: any): Observable<any> {
    const headers = this.createHeader();
    const options = {headers: headers};
    return this.putWithOptions(link, data || {}, options);
  }

  putForm(link, formData?: FormData): Observable<any> {
    const headers = new HttpHeaders().set('Authorization', this.authenServer.snapshot.token);
    const options = {headers: headers};
    return this.http.put(this.getUrl(link), formData || new FormData(), options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  delete(link): Observable<any> {
    const options = this.createDefaultOption();
    return this.deleteWithOptions(link, options);
  }

  postWithOptions(link, data, options): Observable<any> {
    return this.http.post(this.getUrl(link), data, options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getWithOptions(link, options): Observable<any> {
    return this.http.get(this.getUrl(link), options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  putWithOptions(link, data, options): Observable<any> {
    return this.http.put(this.getUrl(link), data, options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  deleteWithOptions(link, options): Observable<any> {
    return this.http.delete(this.getUrl(link), options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  private getUrl(link) {
    let host = __debug_host || "";
    if (host) {
      if (!host.startsWith("http")) {
        let url = window.location.href;
        let arr = url.split("/");
        host = arr[0] + "//" + host;
      }
      if (!host.endsWith("/")) {
        host = host + "/";
      }
    }
    if (link.startsWith("/")) {
      link = link.slice(1);
    }
    return `${host}${link}`;
  }

  private extractData(res) {
    if (res.status >= 200 && res.status < 300 || res.result) {
      return res.body || res.result;
    }
    debug.log("ERROR, HTTP", res);
    if(res.error) {
      throw res;
    }
    return res;
  }

  private handleError(error: any) {
    // In a real world app, you might use a remote logging infrastructure
    const errMsg = {
      code: error.error.code,
      message: error.error.msg,
      ...error.error
    };
    return throwError(errMsg);
  }

  createCustomHeader(token) {
    return new HttpHeaders().set('Authorization', 'Bearer ' + token)
      .set("Content-Type", "application/json");
  }

  createCustomDefaultOption(token) {
    const headers = this.createHeader(token);
    return {
      headers: headers,
      params: null,
      observe: 'response'
    };
  }

  customPost(link, data, token): Observable<any> {
    const options = this.createCustomDefaultOption(token);
    return this.postWithOptions(link, data, options);
  }

  purePost(link, data, token): Observable<any> {
    const options: any = this.createCustomDefaultOption(token);
    return this.http.post(link, data, options);
  }

  purePostForm(link, formData: FormData): Observable<any> {
    const headers = new HttpHeaders().set('Authorization', "Bearer " + this.authenServer.snapshot.token);
    const options = {headers: headers};
    return this.http.post(`${link}`, formData, options);
  }
}
