import { Injectable } from '@angular/core';
import List from "identical-list";
import {Subject} from "rxjs";
import {UtilService} from "apps/core/src/services/util.service";
import {OrderAddress} from "libs/models/Address";
import {HttpService} from "apps/admin/src/app/pages/call-center-old/services/http.service";
import {Fulfillment} from "apps/admin/src/app/pages/call-center-old/models/Fulfillment";
import {LocationQuery} from "@etop/state/location";

const providerLogo = {
  ghn: "ghn-l.png",
  ghtk: "ghtk-l.png",
  vtpost: "vtpost-l.png",
  default: "placeholder.png",
};

const providerLogoSm = {
  ghn: "provider_logos/ghn-s.png",
  ghtk: "provider_logos/ghtk-s.png",
  vtpost: "provider_logos/vtpost-s.png",
  default: "placeholder_medium.png",
};

const shippingFeeShopMap = {
  main: "Phí giao hàng",
  return: "Phí trả hàng",
  adjustment: "Phí vượt cân",
  cods: "Phí thu hộ (COD)",
  insurance: "Phí bảo hiểm",
  address_change: "Phí đổi thông tin",
  discount: "Giảm giá",
  other: "Phụ phí khác"
};

const fulfillmentStatus = {
  new: {
    display: "Mới",
    state: "new"
  },
  default: {
    display: "Đã tạo",
    state: "created"
  },
  created: {
    display: "Đã tạo",
    state: "created"
  },
  confirmed: {
    display: "Đã xác nhận",
    state: "confirmed"
  },
  picking: {
    display: "Đang lấy hàng",
    state: "picking"
  },
  holding: {
    display: "Chờ giao",
    state: "holding"
  },
  delivering: {
    display: "Đang giao",
    state: "delivering"
  },
  delivered: {
    display: "Đã giao",
    state: "delivered"
  },
  returning: {
    display: "Đang trả hàng",
    state: "returning"
  },
  returned: {
    display: "Đã trả hàng",
    state: "returned"
  },
  undeliverable: {
    display: "Không giao được",
    state: "undeliverable"
  },
  cancelled: {
    display: "Hủy",
    state: "cancelled"
  },
  error: {
    display: "Lỗi vận đơn",
    state: "error"
  },
  unknown: {
    display: "Không xác định",
    state: "unknown"
  }
};

const providerDiscountMap = {
  'ghtk': 0.05,
  'ghn': 0.05,
  'vtpost': 0.15
};
@Injectable()
export class FulfillmentService {

  ready = false;
  onReady = new Subject();

  fulfillmentList = new List<Fulfillment>();

  constructor(
    private util: UtilService,
    private http: HttpService,
    private locationQuery: LocationQuery
  ) {
  }

  async getFulfillmentHistory(fulfillment: Fulfillment) {
    const ffmId = fulfillment.id;
    let ret = [];
    try {
      const res = await this.http.post(
        "api/shop.History/GetFulfillmentHistory",
        { paging: { limit: 100 }, id: ffmId }).toPromise();
      const history = res.data.map(h => {
        const time = new Date(h.last_sync_at || h._time).getTime();
        return { ...h, time };
      }).sort((h1, h2) => h1.time - h2.time);

      for (let h of history) {
        const date = h.last_sync_at || h._time;
        let event = "";
        const currentStatus = h.shipping_state;

        switch (currentStatus) {
          case "cancelled":
            event = "Hủy đơn";
            break;
          case "closed":
            event = "Hoàn tất";
            break;
          case "undeliuverable":
            event = "Không giao được";
            break;
          case "returned":
            event = "Đã trả hàng";
            break;
          case "returning":
            event = "Đang trả hàng";
            break;
          case "delivered":
            event = "Đã giao hàng";
            break;
          case "delivering":
            event = "Đang giao hàng";
            break;
          case "holding":
            event = "Chờ giao";
            break;
          case "picking":
            event = "Đang lấy hàng";
            break;
          case "created":
            event = "Tạo đơn";
            break;
          default:
            break;
        }

        if (event) {
          let isDuplicate = false;
          for (let r of ret) {
            if (r.date.substr(0, 16) == date.substr(0, 16) && r.event == event) {
              isDuplicate = true;
              break;
            }
          }

          if (isDuplicate) {
            continue;
          }
          ret.push({ date, event });
        }
      }
    } catch (e) {
      debug.error(e);
    }

    return ret;
  }

  updateFulfillment(body) {
    return this.http.post('/api/admin.Fulfillment/UpdateFulfillment', body)
      .toPromise()
      .then(res => {
        return res;
      }).catch(err => {
        throw err;
      });
  }

  public fulfillmentMap(fulfillment): Fulfillment {
    let f = new Fulfillment(Object.assign({}, fulfillment, {
      address_to: this._locationMap(fulfillment.address_to),
      o_data: fulfillment
    }));
    f.address_from = f.address_from || this._locationMap(f.order && f.order.shop_shipping && f.order.shop_shipping.sh_address);
    f.status_mapped = this._statusMap(f);
    f.shipping_fee_shop_lines = this._shippingFeeShopMap(f.shipping_fee_shop_lines);
    f.shipping_fee_provider = f.provider_shipping_fee_lines.reduce((a,b) => b.cost + a, 0);
    f.provider_discount = f.shipping_fee_provider * providerDiscountMap[f.carrier]; // chiết khấu
    f.profit = f.shipping_fee_shop - f.shipping_fee_provider + f.provider_discount;
    return f;
  }

  private _locationMap(address): OrderAddress {
    return new OrderAddress(Object.assign({}, address, {
      o_data: address,
      Province: this.locationQuery.getProvince(address.province_code),
      District: this.locationQuery.getDistrict(address.district_code),
      Ward: this.locationQuery.getWard(address.ward_code)
    }));
  }

  private _statusMap(ffm) {
    const { shipping_state, shipping_code } = ffm;
    if (!shipping_code) {
      return fulfillmentStatus.error;
    }
    return shipping_state ? fulfillmentStatus[shipping_state] : { display: "Không xác định", state: "unknown" };
  }

  canForcePicking(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return false;
    }
    const { shipping_state } = fulfillment;
    if (!shipping_state) {
      return false;
    }
    return shipping_state === 'picking' || shipping_state === 'created' || shipping_state === 'confirmed';
  }

  canForceDelivering(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return false;
    }
    const { shipping_state } = fulfillment;
    if (!shipping_state) {
      return false;
    }
    return shipping_state === 'delivering' || shipping_state === 'holding';
  }

  trackingLink(fulfillment) {
    if (!fulfillment) {
      return null;
    }
    switch (fulfillment.shipping_provider) {
      case 'ghn':
        return `https://track.ghn.vn/order/tracking?code=${fulfillment.shipping_code}`;
      case 'vtpost':
        return `https://viettelpost.com.vn/Tracking?KEY=${fulfillment.shipping_code}`;
      default:
        return null;
    }
  }

  providerLogo(fulfillment) {
    try {
      const { shipping_provider } = fulfillment;
      return providerLogo[shipping_provider];
    } catch (e) {
      return providerLogo.default;
    }
  }

  providerLogoSm(fulfillment) {
    try {
      const { shipping_provider } = fulfillment;
      return providerLogoSm[shipping_provider];
    } catch (e) {
      return providerLogoSm.default;
    }
  }

  loadFulfillment(id: string) {
    return this.http.post("api/admin.Fulfillment/GetFulfillment", {
      id
    }).toPromise()
      .then(res => this.fulfillmentMap(res))
      .catch(err => { throw err });
  }

  getFulfillmentByOrderID(orderId: string) {
    return this.fulfillmentList.filter(f => f.order_id == orderId);
  }

  async getFulfillments(start?: number, perpage?: number, pFilters?: Array<any>) {
    let mixed = {};
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    let filters = [];

    if (pFilters instanceof Array) {
      filters = pFilters.map(f => new Object({
        name: f.name_rule == "date" ? "created_at" : f.name_rule,
        op: this.util.filterOperatorMap(f.compair_rule, f.name_rule == "date"),
        value: f.value
      }));
    }

    return this.http.post('api/admin.Fulfillment/GetFulfillments', {
      paging,
      filters,
      mixed
    }).toPromise().then(res => {
      let fulfillments = res.fulfillments.map(o => this.fulfillmentMap(o));
      return {
        paging: res.paging,
        fulfillments: fulfillments
      };
    });
  }

  async getEveryFfm(pFilters?: Array<any>) {
    let ffms = [];
    let offset = 0;
    let filters = [];
    if (pFilters instanceof Array) {
      filters = pFilters.map(f => new Object({
        name: f.name_rule == "date" ? "created_at" : f.name_rule,
        op: this.util.filterOperatorMap(f.compair_rule, f.name_rule == "date"),
        value: f.value
      }));
    }
    while (true) {
      let _ffms = await this.http.post('api/admin.Fulfillment/GetFulfillments', {
        paging: {
          limit: 1000,
          offset
        },
        filters
      })
      .toPromise()
      .then(res => res.fulfillments.map(f => this.fulfillmentMap(f)));

      ffms = ffms.concat(_ffms);
      if (!_ffms.length || _ffms.length < 1000) break;
      offset += 1000;
    }
    return ffms;
  }

  private _shippingFeeShopMap(lines) {
    return lines.map(line => {
      const { external_service_name, shipping_fee_type } = line;
      let name = shippingFeeShopMap[shipping_fee_type] || external_service_name;

      return { ...line, name };
    });
  }
}
