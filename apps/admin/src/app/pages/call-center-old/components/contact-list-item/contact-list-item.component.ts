import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Contact} from "apps/admin/src/app/pages/call-center-old/models/Contact";

@Component({
  selector: 'admin-contact-list-item',
  templateUrl: './contact-list-item.component.html',
  styleUrls: ['./contact-list-item.component.scss']
})
export class ContactListItemComponent implements OnInit {
  @Input() contact: Contact = new Contact({});
  @Output() call = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
}
