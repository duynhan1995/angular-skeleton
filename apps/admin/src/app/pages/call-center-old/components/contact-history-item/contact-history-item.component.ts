import { Component, OnInit, Input } from '@angular/core';
import {CallLog} from "apps/admin/src/app/pages/call-center-old/models/CallCenter";
@Component({
  selector: 'admin-contact-history-item',
  templateUrl: './contact-history-item.component.html',
  styleUrls: ['./contact-history-item.component.scss']
})
export class ContactHistoryItemComponent implements OnInit {
  @Input() call_log: CallLog = new CallLog({});
  constructor() { }

  ngOnInit() {
  }

}
