import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {CallLog} from "apps/admin/src/app/pages/call-center-old/models/CallCenter";
import {VTigerService} from "apps/admin/src/services/vtiger.service";

@Component({
  selector: 'admin-contact-history',
  templateUrl: './contact-history.component.html',
  styleUrls: ['./contact-history.component.scss']
})
export class ContactHistoryComponent implements OnInit {
  @Output() call = new EventEmitter();
  _searchTimeout = null;
  callLogList: Array<CallLog> = [];
  queryString = '';
  selectedCallLog: CallLog = null;
  loading = true;

  constructor(
    private vtiger: VTigerService,
  ) {}

  ngOnInit() {
    this.loadTarget();
  }

  search() {
    clearTimeout(this._searchTimeout);
    this._searchTimeout = setTimeout(() => {
      this.loadTarget();
    }, 500);
  }

  async loadTarget() {
    this.loading = true;
    try {
      this.callLogList = await this.vtiger.filterCallLog(this.queryString);
    } catch (e) {
      toastr.error('Có lỗi xảy ra: ' + e.message);
    }
    this.loading = false;
  }

  callLogSelected(call_log: CallLog) {
    this.selectedCallLog = call_log;
  }
}
