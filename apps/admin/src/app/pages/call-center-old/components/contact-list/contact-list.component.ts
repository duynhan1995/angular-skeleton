import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {VTigerService} from "apps/admin/src/services/vtiger.service";
import {Contact} from "apps/admin/src/app/pages/call-center-old/models/Contact";

@Component({
  selector: 'admin-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  @Output() selectContact = new EventEmitter();
  @Output() call = new EventEmitter();
  _searchTimeout = null;
  contactList: Array<Contact> = [];
  queryString = '';
  selectedContact: Contact = null;
  loading = true;

  constructor(
    private vtiger: VTigerService,
  ) {}

  ngOnInit() {
    this.loadTarget();
  }

  isPhoneNumber(str: string) {
    if (isNaN(Number(str))) {
      return false;
    }
    if (str && str.match(/^0[0-9]{9,10}$/)) {
      if (str.length === 11 && str[1] === '1') {
        return false;
      }
      return true;
    }
    return false;
  }

  search() {
    clearTimeout(this._searchTimeout);
    this._searchTimeout = setTimeout(() => {
      this.loadTarget();
    }, 500);
  }

  async loadTarget() {
    this.loading = true;
    try {
      this.contactList = await this.vtiger.filterContact(this.queryString);
    } catch (e) {
      debug.error(e);
      toastr.error('Có lỗi xảy ra: ' + e.message);
    }
    this.loading = false;
  }

  contactSelected(contact: Contact) {
    this.selectedContact = contact;
    this.selectContact.next(contact);
  }
}
