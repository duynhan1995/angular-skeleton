import { Component, OnInit, Input } from '@angular/core';
import {Contact} from "apps/admin/src/app/pages/call-center-old/models/Contact";
@Component({
  selector: 'admin-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent implements OnInit {

  @Input() contact: Contact = new Contact({});

  constructor() { }

  ngOnInit() {
  }

}
