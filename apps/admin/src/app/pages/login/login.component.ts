import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';

@Component({
  selector: 'admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('passwordInput', {static: false}) passwordInput: ElementRef;
  login: string;
  password: string;

  loading = false;
  loadingView = true;

  constructor(
    private commonUsecase: CommonUsecase
  ) { }

  ngOnInit() {
    this.commonUsecase.redirectIfAuthenticated().then(() => (this.loadingView = false));
  }

  async onEnterLoginInput() {
    if (!this.password) {
      if (this.passwordInput) {
        setTimeout(_ => {
          this.passwordInput.nativeElement.focus();
        });
      }
    } else {
      this.onLogin();
    }
  }

  showPassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'text';
      });
    }
  }

  hidePassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'password';
      });
    }
  }

  async onLogin() {
    if (!this.login || !this.password) {
      toastr.error('Vui lòng nhập đầy đủ thông tin đăng nhập để tiếp tục.');
      return;
    }

    this.loading = true;
    try {
      await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
    this.loading = false;
  }

}
