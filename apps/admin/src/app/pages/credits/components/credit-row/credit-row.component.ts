import {Component, Input, OnInit} from '@angular/core';
import {Credit} from "libs/models/admin/Credit";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {DatePipe, DecimalPipe} from "@angular/common";
import {CreditApi} from "@etop/api/admin/credit.api";
import {CreditService} from "@etop/state/admin/credit";
import {TelegramService} from "@etop/features";
import {AdminUserApi} from "@etop/api";


@Component({
  selector: '[admin-credit-row]',
  templateUrl: './credit-row.component.html',
  styleUrls: ['./credit-row.component.scss']
})
export class CreditRowComponent implements OnInit {
  @Input() credit = new Credit();
  @Input() liteMode = false;

  constructor(
    private dialog: DialogControllerService,
    private number: DecimalPipe,
    private date: DatePipe,
    private api: CreditApi,
    private creditService: CreditService,
    private telegramService: TelegramService,
    private adminUserApi: AdminUserApi
  ) { }

  ngOnInit() {
  }

  classifyDisplay(classify){
    if(classify == "shipping")
      return "Vận chuyển"
    return "Viễn thông"
  }

  confirmCredit() {
    const {shop, amount, paid_at, created_at} = this.credit;
    const {code, name, email, phone} = shop;
    const modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu nạp tiền`,
      body: `
<div><strong>Shop:</strong> ${code ? code + ' - ' : ''}${name}</div>
<div><strong>Số điện thoại:</strong> ${phone}</div>
<div><strong>Email:</strong> ${email || '-'}</div>
<div>
  <strong>Số tiền:
    <span class="${amount < 0 ? 'text-danger' : (amount > 0 ? 'text-success' : '')}">${this.number.transform(amount)}đ</span>
  </strong>
</div>
<div><strong>Ngày giao dịch:</strong> ${this.date.transform(paid_at, 'dd/MM/yyyy')}</div>
<div><strong>Ngày tạo:</strong> ${this.date.transform(created_at, 'dd/MM/yyyy')}</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.api.confirmCredit(this.credit.id);
          toastr.success('Xác nhận phiếu nạp tiền thành công.');
          modal.close().then();
          this.creditService.getCredits().then();
          const user = await this.adminUserApi.getUser(shop.owner_id);
          this.telegramService.newCredit(user,this.credit);
        } catch (e) {
          debug.error('ERROR in confirmCredit', e);
          toastr.error(`Xác nhận phiếu nạp tiền không thành công.`, e.code ? (e.message || e.msg) : '');
        }
      }
    });
    modal.show().then();
  }

}
