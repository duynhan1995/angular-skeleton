import { Component, OnInit } from '@angular/core';
import {Shop} from "libs/models/Account";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {AdminShopApi, AdminShopAPI} from "@etop/api";
import {CreditApi, CreditAPI} from "@etop/api/admin/credit.api";
import CreateCreditRequest = CreditAPI.CreateCreditRequest;
import GetShopsRequest = AdminShopAPI.GetShopsRequest;
import { CreditService } from '@etop/state/admin/credit';
import { FilterOperator } from '@etop/models';
import {TelegramService} from "@etop/features";

@Component({
  selector: 'admin-create-credit-form',
  templateUrl: './create-credit-form.component.html',
  styleUrls: ['./create-credit-form.component.scss']
})
export class CreateCreditFormComponent extends BaseComponent implements OnInit {

  constructor(
    private shopApi: AdminShopApi,
    private creditApi: CreditApi,
    private creditService: CreditService,
    private auth: AuthenticateStore,
    private telegramService: TelegramService
  ) {
    super();
  }

  newCredit: CreateCreditRequest = {
    amount: null,
    classify: null,
    shop_id: null,
    paid_at: null,
    type: 'shop'
  }

  classifyList = [
    {name: 'Vận chuyển', value: 'shipping'},
    {name: 'Viễn thông', value: 'telecom'},
  ];

  selectedShop: Shop;
  shops: Shop[] = [];

  shopId: string;

  loading = false;

  private static validateCredit(credit: CreateCreditRequest): boolean {
    const {amount, classify, shop_id, paid_at} = credit;
    if (!amount && amount != 0) {
      toastr.error('Vui lòng nhập số tiền muốn nạp!');
      return false;
    }

    if (!classify){
      toastr.error("Vui lòng chọn loại phiếu nạp tiền");
      return false;
    }

    if (!shop_id) {
      toastr.error('Vui lòng chọn shop!');
      return false;
    }

    if (!paid_at) {
      toastr.error('Vui lòng chọn ngày giao dịch!');
      return false;
    }

    return true;
  }

  displayMap = option => option && `${option.code} - ${option.name}` || null;
  valueMap = option => option && option.id || null;

  ngOnInit() {
    const roles = this.auth.snapshot.permission.roles;
    if (roles.includes('ad_voip') && roles.length == 1) {
      this.classifyList.shift();
    }
  }

  async onShopSearch(searchBy: 'name' | 'phone' | 'email', searchText: string) {
    const query: GetShopsRequest = {
      paging: { offset: 0, limit: 1000 }
    };
    switch (searchBy) {
      case "name":
        query.filter = {name: searchText};
        break;
      case "phone":
        query.filters = [
          {
            name: 'phone',
            op: FilterOperator.eq,
            value: searchText
          }
        ];
        break;
      case "email":
        query.filters = [
          {
            name: 'email',
            op: FilterOperator.eq,
            value: searchText
          }
        ];
        break;
    }
    this.shops = await this.shopApi.getShops(query);
  }

  onShopSelect() {
    this.selectedShop = this.shops.find(s => s.id == this.shopId);
    this.newCredit.shop_id = this.shopId;
    this.shops = [];
  }

  async createCredit() {
    this.loading = true;
    try {
      if (!CreateCreditFormComponent.validateCredit(this.newCredit)) {
        return this.loading = false;
      }
      await this.creditApi.createCredit(this.newCredit);
      toastr.success('Tạo phiếu nạp tiền thành công.');
      this.creditService.getCredits().then();
    } catch(e) {
      debug.error('ERROR in createCredit', e);
      toastr.error(`Tạo phiếu nạp tiền không thành công.`, e.code ? (e.message || e.msg) : '');
    }
    this.loading = false;
  }

  nameDisplayMap() {
    return option => option && option.name || null;
  }

  valueDisplayMap() {
    return option => option && option.value || null;
  }

}
