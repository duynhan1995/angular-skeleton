import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CreditsComponent } from 'apps/admin/src/app/pages/credits/credits.component';
import {CreditListComponent} from "apps/admin/src/app/pages/credits/credit-list/credit-list.component";
import {CreditRowComponent} from "apps/admin/src/app/pages/credits/components/credit-row/credit-row.component";
import {CreateCreditFormComponent} from "apps/admin/src/app/pages/credits/components/create-credit-form/create-credit-form.component";
import {
    EtopCommonModule,
    EtopMaterialModule,
    EtopPipesModule,
    MaterialModule,
    NotPermissionModule,
    SideSliderModule
} from "@etop/shared";
import {AuthenticateModule} from "@etop/core";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";

const routes: Routes = [
  {
    path: '',
    component: CreditsComponent
  }
];

@NgModule({
  declarations: [
    CreditsComponent,
    CreditListComponent,
    CreditRowComponent,
    CreateCreditFormComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        EtopCommonModule,
        SideSliderModule,
        EtopPipesModule,
        EtopMaterialModule,
        MaterialModule,
        NotPermissionModule,
        AuthenticateModule,
        CallCenterModule
    ]
})
export class CreditsModule { }
