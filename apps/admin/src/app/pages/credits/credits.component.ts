import { Component, OnInit } from '@angular/core';
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-credits',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-credit-list></admin-credit-list>
    </ng-template>`,
})
export class CreditsComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) { }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/credit:view');
  }

  ngOnInit() {
  }

}
