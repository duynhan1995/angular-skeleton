import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {EtopTableComponent, SideSliderComponent} from "@etop/shared";
import {takeUntil} from "rxjs/operators";
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {CreditQuery, CreditService} from "@etop/state/admin/credit";
import { CLASSIFY, CreditAPI } from '@etop/api';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'admin-credit-list',
  templateUrl: './credit-list.component.html',
  styleUrls: ['./credit-list.component.scss']
})
export class CreditListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('creditTable', { static: true }) creditTable: EtopTableComponent;
  @ViewChild('creditSlider', { static: true }) creditSlider: SideSliderComponent

  creditsLoading$ = this.creditQuery.selectLoading();
  creditsList$ = this.creditQuery.selectAll();
  isLastPage$ = this.creditQuery.select(state => state.ui.isLastPage);

  onNewCredit = false;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private creditQuery: CreditQuery,
    private creditService: CreditService,
    private headerController: HeaderControllerService,
    private auth: AuthenticateStore,
  ) {
    super();
  }

  get showPaging() {
    return !this.creditTable.liteMode && !this.creditTable.loading;
  }

  get emptyTitle() {
    return 'Chưa có phiếu nạp tiền nào';
  }

  get sliderTitle() {
    if (this.onNewCredit) {
      return 'Tạo phiếu nạp tiền mới'
    }
    return 'Chi tiết phiếu nạp tiền';
  }

  async ngOnInit() {
    this.creditsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.creditTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        } else {
          this.headerController.setActions([
            {
              title: 'Tạo phiếu nạp tiền',
              cssClass: 'btn btn-primary',
              onClick: () => this.createCredit(),
              permissions: ['admin/credit:create']
            }
          ]);
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.creditTable.toggleNextDisabled(true);
          this.creditTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả phiếu nạp tiền.');
        }
      });

      const roles = this.auth.snapshot.permission.roles;
      let tabs = [
        {
          onClick: () => this.tabAll(),
          title: 'Tất cả',
          active: true,
          permissions: ['admin/credit:view']
        },
        {
          onClick: () => this.shippingTab(),
          title: 'Vận chuyển',
          active: false,
          permissions: ['admin/credit:view']
        },
        {
          onClick: () => this.etelecomTab(),
          title: 'Viễn thông',
          active: false,
          permissions: ['admin/credit:view']
        },
      ];

      if (roles.includes('ad_voip') && roles.length == 1) {
        tabs = [];
      }
    
      this.headerController.setTabs(tabs);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  resetState() {
    this.creditTable.toggleLiteMode(false);
    this.creditSlider.toggleLiteMode(false);
    this.creditTable.toggleNextDisabled(false);
    this.onNewCredit = false;
  }

  etelecomTab() {
    this.resetState();
    const filter : CreditAPI.GetCreditsFilters = {
        classify: CLASSIFY.telecom,
    }
    this.creditService.setFilter(filter);
    this.getCredits();
  }

  shippingTab() {
    this.resetState();
    const filter : CreditAPI.GetCreditsFilters = {
        classify: CLASSIFY.shipping,
    }
    this.creditService.setFilter(filter);
    this.getCredits();
  }

  tabAll() {
    this.resetState();
    this.creditService.setFilter(null);
    this.getCredits();
  }

  getCredits() {
    this.creditService.getCredits().then();
  }

  loadPage({ page, perpage }) {
    this.creditService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    const roles = this.auth.snapshot.permission.roles;
    if (roles.includes('ad_voip') && roles.length == 1) {
      const filter : CreditAPI.GetCreditsFilters = {
        classify: CLASSIFY.telecom,
      }
      this.creditService.setFilter(filter);
    }
    this.getCredits();
  }

  onSliderClosed() {
    this.onNewCredit = false;
    this._checkSelectMode();
  }

  createCredit() {
    this.onNewCredit = true;
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    this.creditTable.toggleLiteMode(this.onNewCredit);
    this.creditSlider.toggleLiteMode(this.onNewCredit);
  }

}
