import {Component, OnDestroy, OnInit} from '@angular/core';
import { BaseComponent } from '@etop/core';
import { Router } from '@angular/router';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";

@Component({
  selector: 'admin-shipping-settings',
  template: `<router-outlet></router-outlet>`
})
export class ShippingSettingsComponent extends BaseComponent implements OnInit, OnDestroy {

  headerTabChanged$ = this.store.state$.pipe(
    map(s => s?.headerTab),
    distinctUntilChanged()
  );

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private store: ShippingSettingsStore,
    private service: ShippingSettingsService,
  ) {
    super();
  }

  ngOnInit() {
    this.setupTabs();
    this.headerTabChanged$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.setupTabs();
      });

    this.service.getConnections()?.then();
    this.service.getCustomRegions()?.then();
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  checkTab(tabName) {
    return this.headerTabChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Thiết lập NVC',
        name: 'connections',
        active: false,
        permissions: ['admin/connection_service:view'],
        onClick: () => {
          this.tabClick('connections').then();
        }
      },
      {
        title: 'Dịch vụ',
        name: 'services',
        active: false,
        permissions: ['admin/shipment_service:view'],
        onClick: () => {
          this.tabClick('services').then();
        }
      },
      {
        title: 'Bảng giá',
        name: 'price-lists',
        active: false,
        permissions: ['admin/shipment_price_list:view'],
        onClick: () => {
          this.tabClick('price-lists').then();
        }
      },
      {
        title: 'Khuyến mãi',
        name: 'promotions',
        active: false,
        permissions: ['admin/shipment_price_list_promotion:view'],
        onClick: () => {
          this.tabClick('promotions').then();
        }
      },
      {
        title: 'Vùng tự định nghĩa',
        name: 'custom-regions',
        active: false,
        permissions: ['admin/custom_region:view'],
        onClick: () => {
          this.tabClick('custom-regions').then();
        }
      }
    ].map(tab => {
      this.checkTab(tab.name).subscribe(active => tab.active = active);
      return tab;
    }));
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`shipping-settings/${type}`);
    this.store.changeHeaderTab(type);
  }

}
