import {Component, Input, OnInit} from '@angular/core';
import {PriceModifierType, Rule, ShipmentPrice} from "libs/models/admin/ShipmentPrice";

@Component({
  selector: '[admin-shipment-price-row]',
  templateUrl: './shipment-price-row.component.html',
  styleUrls: ['./shipment-price-row.component.scss']
})
export class ShipmentPriceRowComponent implements OnInit {
  @Input() sp: ShipmentPrice;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

  hasAdditionalFee(type, shipmentPrice: ShipmentPrice) {
    return shipmentPrice.additional_fees.some(fee => fee.fee_type == type && fee.rules?.length);
  }

  getAdditionalFee(type, shipmentPrice: ShipmentPrice){
    const value = shipmentPrice.additional_fees.filter(fee => fee.fee_type == type);
    return value[0]?.rules;
  }

  isPercentage(rule: Rule) {
    return rule?.price_modifier_type == PriceModifierType.percentage;
  }

}
