import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {
  OverWeight,
  ShipmentPrice, ShipmentPriceAdditionalFee
} from 'libs/models/admin/ShipmentPrice';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ShipmentPriceAPI, ShipmentPriceApi } from '@etop/api';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {FeeType} from "libs/models/Fee";

@Component({
  selector: 'admin-shipment-price-actions',
  templateUrl: './shipment-price-actions.component.html',
  styleUrls: ['./shipment-price-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShipmentPriceActionsComponent implements OnInit {

  constructor(
    private util: UtilService,
    private promiseQueue: PromiseQueueService,
    private api: ShipmentPriceApi,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ) { }

  @Output() toBack = new EventEmitter();
  @Output() changed = new EventEmitter<{ creates: number, updates: number, deletes: number }>();
  @Input() action: 'create' | 'update' = 'update';

  shipmentPrices: ShipmentPrice[] = [];

  loading = false;

  private static validateOverweight(overweight: OverWeight[]) {
    for (let i = 0; i < overweight.length; i++) {
      const ow = overweight[i];
      const {
        min_weight,
        max_weight,
        price_step,
        weight_step
      } = ow;

      if (!max_weight && max_weight != 0) {
        return {
          invalidIndex: i + 1,
          invalidType: 'max_weight',
          valid: false
        };
      }

      if (max_weight < min_weight &&
        !((i == overweight.length - 1) && max_weight == -1)
      ) {
        return {
          invalidIndex: i + 1,
          invalidType: 'max_lowerthan_min',
          valid: false
        };
      }

      if (!price_step && price_step != 0) {
        return {
          invalidIndex: i + 1,
          invalidType: 'price_step',
          valid: false
        };
      }

      if (!weight_step && weight_step != 0) {
        return {
          invalidIndex: i + 1,
          invalidType: 'weight_step',
          valid: false
        };
      }

    }
    return {
      invalidIndex: null,
      invalidType: null,
      valid: true
    };
  }

  private static validateAdditionalFees(additionalFees: ShipmentPriceAdditionalFee[]) {
    for (let i = 0; i < additionalFees?.length; i++) {
      const fee = additionalFees[i];
      const rules = fee.rules;
      if (!rules || !rules.length) { continue; }

      if ([FeeType.insurance, FeeType.cods].includes(fee.fee_type)) {
        for (let j = 0; j < rules.length; j++) {
          const { max_value, min_value, amount } = rules[j];
          if (!max_value && max_value != 0) {
            return {
              invalidIndex: j + 1,
              invalidFeeType: fee.fee_type,
              invalidType: 'max_value',
              valid: false
            };
          }
          if (max_value < min_value &&
            !((j == rules.length - 1) && max_value == -1)
          ) {
            return {
              invalidIndex: j + 1,
              invalidFeeType: fee.fee_type,
              invalidType: 'max_lowerthan_min',
              valid: false
            };
          }
          if (!amount && amount != 0) {
            return {
              invalidIndex: j + 1,
              invalidFeeType: fee.fee_type,
              invalidType: 'increase_amount',
              valid: false
            };
          }
        }
      } else {
        if (!rules[0].amount && rules[0].amount != 0) {
          return {
            invalidIndex: i + 1,
            invalidFeeType: fee.fee_type,
            invalidType: 'amount',
            valid: false
          };
        }
      }
    }

    return {
      invalidIndex: null,
      invalidFeeType: null,
      invalidType: null,
      valid: true
    };
  }

  private static validateShipmentPrices(prices: ShipmentPrice[]) {
    for (let i = 0; i < prices.length; i++) {
      const sp = prices[i];
      sp.error = false;
      const {
        custom_region_types,
        province_types,
        region_types,
        urban_types,
        details,
        additional_fees,
        name
      } = sp;
      const location_type_count = custom_region_types?.length +
        province_types?.length +
        region_types?.length;

      if (!name || !name.trim()) {
        sp.error = true;
        toastr.error(`Vui lòng nhập tên cho cấu hình giá thứ ${i + 1}`);
        return false;
      }

      if (!location_type_count) {
        sp.error = true;
        toastr.error(`Vui lòng chọn tuyến cho cấu hình giá thứ ${i + 1}`);
        return false;
      }

      if (!urban_types.length) {
        sp.error = true;
        toastr.error(`Vui lòng chọn khu vực cho cấu hình giá thứ ${i + 1}`);
        return false;
      }

      if (!details || !details.length) {
        sp.error = true;
        toastr.error(`Vui lòng nhập đầy đủ thông tin về khối lượng và giá cho cấu hình giá thứ ${i + 1}`);
        return false;
      }

      const detail = details[0];
      if (!detail.weight && detail.weight != 0) {
        sp.error = true;
        toastr.error(`Vui lòng nhập khối lượng cho cấu hình giá thứ ${i + 1}`);
        return false;
      }

      if (!detail.price && detail.price != 0) {
        sp.error = true;
        toastr.error(`Vui lòng nhập giá cho cấu hình giá thứ ${i + 1}`);
        return false;
      }

      if (detail.overweight && detail.overweight.length) {
        const overweight = detail.overweight;
        const validOverweight = ShipmentPriceActionsComponent.validateOverweight(overweight);

        if (!validOverweight.valid) {
          sp.error = true;
          let errorType = '';

          switch (validOverweight.invalidType) {
            case 'max_weight':
              errorType = 'khối lượng tối đa';
              break;
            case 'max_lowerthan_min':
              errorType = 'khối lượng tối đa phải lớn hơn khối lượng tối thiểu';
              break;
            case 'price_step':
              errorType = 'số tiền tăng';
              break;
            case 'weight_step':
              errorType = 'mốc khối lượng';
              break;
          }

          toastr.error(
`Vui lòng nhập ${errorType} cho ngưỡng vượt cân thứ ${validOverweight.invalidIndex} của cấu hình giá thứ ${i + 1}`
          );
          return false;
        }
      }

      const validAdditionalFees = ShipmentPriceActionsComponent.validateAdditionalFees(additional_fees);
      if (!validAdditionalFees.valid) {
        sp.error = true;
        let errorType = '';

        switch (validAdditionalFees.invalidType) {
          case 'max_value':
            errorType = 'giá trị tối đa';
            break;
          case 'max_lowerthan_min':
            errorType = 'giá trị tối đa phải lớn hơn giá trị tối thiểu';
            break;
          case 'increase_amount':
            errorType = 'giá trị tăng';
            break;
          case 'amount':
            errorType = 'giá trị';
            break;
        }

        const invalidFeeTypeDisplay = ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(validAdditionalFees.invalidFeeType);
        if ([FeeType.insurance, FeeType.cods].includes(validAdditionalFees.invalidFeeType)) {
          toastr.error(
`Vui lòng nhập ${errorType} cho ngưỡng ${invalidFeeTypeDisplay.toLowerCase()} thứ ${validAdditionalFees.invalidIndex} của cấu hình giá thứ ${i + 1}`
          );
        } else {
          toastr.error(
`Vui lòng nhập ${errorType} cho phí ${invalidFeeTypeDisplay.toLowerCase()} của cấu hình giá thứ ${i + 1}`
          );
        }

        return false;
      }

    }
    return true;
  }

  private static reMapAdditionalFees(additionalFees: ShipmentPriceAdditionalFee[]) {
    if (!additionalFees) {
      return [];
    }

    additionalFees = additionalFees.filter(f => f.rules && f.rules.length);
    additionalFees.forEach(f => {
      if (![FeeType.insurance, FeeType.cods].includes(f.fee_type)) {
        f.rules[0].min_value = 0;
        f.rules[0].max_value = -1;
      }
    });

    return additionalFees;

  }

  ngOnInit() {
    this.shipmentPrices = this.util.deepClone(this.shippingSettingsStore.snapshot.activeShipmentPrices);
    if (this.action == 'create') {
      this.addMoreShipmentPrice();
    }
  }

  drop(shipmentPrices: ShipmentPrice[]) {
    this.shipmentPrices = shipmentPrices;
  }

  addMoreShipmentPrice() {
    this.shipmentPrices.push(new ShipmentPrice({
      custom_region_ids: [],
      custom_region_types: [],
      province_types: [],
      region_types: [],
      urban_types: [],
      details: [{
        overweight: [],
        weight: null,
        price: null
      }],
      additional_fees: []
    }));
  }

  removeShipmentPrice(index: number) {
    this.shipmentPrices.splice(index, 1);
  }

  async confirm() {
    this.loading = true;
    try {
      const valid = ShipmentPriceActionsComponent.validateShipmentPrices(this.shipmentPrices);
      if (!valid) {
        return this.loading = false;
      }

      const _prices = this.util.deepClone(this.shipmentPrices).reverse();
      const changes = {
        creates: _prices.filter(sp => !sp.id).length,
        updates: _prices.filter(sp => !!sp.id).length,
        deletes: 0
      };

      let deletedIds = [];
      if (this.action == 'create') {
        this.createShipmentPrices(_prices).then();
      }
      if (this.action == 'update') {
        const activeShipmentPrices = this.shippingSettingsStore.snapshot.activeShipmentPrices;
        const storedIds = activeShipmentPrices.filter(sp => !!sp.id).map(sp => sp.id);
        const nextIds = this.shipmentPrices.filter(sp => !!sp.id).map(sp => sp.id);
        deletedIds = storedIds.filter(id => !nextIds.includes(id));
        if (deletedIds.length) {
          await this.deleteShipmentPrices(deletedIds);
        }
        await this.updateShipmentPrices(_prices);
      }
      this.back();

      this.changed.emit({
        ...changes,
        deletes: deletedIds.length
      });

    } catch(e) {
      debug.error('ERROR in action confirm', e);
      toastr.error(
        `${this.action == 'create' ? 'Tạo' : 'Cập nhật'} cấu hình giá không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
  }

  async deleteShipmentPrices(ids: string[]) {
    try {
      const promises = ids.map(id => async() => {
        try {
          await this.api.deleteShipmentPrice(id);
        } catch(e) {
          debug.error('ERROR in deleting shipment price', e);
        }
      });
      await this.promiseQueue.run(promises, 2);
    } catch(e) {
      debug.error('ERROR in deleteShipmentPrices() Promises', e);
      throw e;
    }
  }

  async createShipmentPrices(prices: ShipmentPrice[]) {
    try {
      let success = 0;
      const promises = prices.map((sp, index) => async() => {
        try {
          const {
            custom_region_ids,
            custom_region_types,
            province_types,
            region_types,
            urban_types,
            details,
            additional_fees,
            location,
            name
          } = sp;

          const location_type = location.split(/_(.+)/)[0];

          const {activeShipmentPriceList, activeShipmentService} = this.shippingSettingsStore.snapshot;

          const body: ShipmentPriceAPI.CreateShipmentPriceRequest = {
            priority_point: index + 1,
            name,
            shipment_price_list_id: activeShipmentPriceList.id,
            shipment_service_id: activeShipmentService.id,
            custom_region_ids: ['province', 'region'].includes(location_type) ? [] : custom_region_ids,
            custom_region_types: ['province', 'region'].includes(location_type) ? [] : custom_region_types,
            province_types: location_type == 'region' ? [] : province_types,
            region_types: location_type == 'province' ? [] : region_types,
            urban_types,
            details,
            additional_fees: ShipmentPriceActionsComponent.reMapAdditionalFees(additional_fees),
          };

          await this.api.createShipmentPrice(body);

          success += 1;
        } catch(e) {
          sp.error = true;
          debug.error(`ERROR in create Shipment Price #${prices.length - (index + 1)}`, e);
        }
      });

      await this.promiseQueue.run(promises, 4);

      this.shipmentPrices = prices.reverse();
      this.shippingSettingsService.selectShipmentPrices(this.shipmentPrices);

      if (success == prices.length) {
        toastr.success('Tạo cấu hình giá thành công.');
      } else if (success > 0) {
        toastr.warning(`Tạo thành công ${success}/${prices.length} cấu hình giá.`);
      } else {
        toastr.error('Tạo cấu hình giá không thành công.');
      }
    } catch(e) {
      debug.error(`ERROR in Create Shipment Price Promises`, e);
      throw e;
    }
  }

  async updateShipmentPrices(prices: ShipmentPrice[]) {
    try {
      let success = 0;
      const promises = prices.map((sp, index) => async() => {
        try {
          const {
            id,
            custom_region_ids,
            custom_region_types,
            province_types,
            region_types,
            urban_types,
            details,
            additional_fees,
            location,
            name
          } = sp;

          const location_type = location.split(/_(.+)/)[0];

          const body: ShipmentPriceAPI.UpdateShipmentPriceRequest = {
            priority_point: index + 1,
            name,
            id,
            custom_region_ids: ['province', 'region'].includes(location_type) ? [] : custom_region_ids,
            custom_region_types: ['province', 'region'].includes(location_type) ? [] : custom_region_types,
            province_types: location_type == 'region' ? [] : province_types,
            region_types: location_type == 'province' ? [] : region_types,
            urban_types,
            additional_fees: ShipmentPriceActionsComponent.reMapAdditionalFees(additional_fees),
            details
          };

          let res: any;
          if (body.id) {
            res = await this.api.updateShipmentPrice(body);
          } else {
            delete body.id;
            const {activeShipmentPriceList, activeShipmentService} = this.shippingSettingsStore.snapshot;
            res = await this.api.createShipmentPrice({
              ...body,
              shipment_price_list_id: activeShipmentPriceList.id,
              shipment_service_id: activeShipmentService.id,
            });
          }
          prices[index] = res;
          prices[index].error = false;
          success += 1;
        } catch(e) {
          prices[index].error = true;
          debug.error(`ERROR in update Shipment Price #${prices.length - (index + 1)}`, e);
        }
      });

      await this.promiseQueue.run(promises, 4);

      this.shipmentPrices = prices.reverse();
      this.shippingSettingsService.selectShipmentPrices(this.shipmentPrices);

      if (success == prices.length) {
        toastr.success('Cập nhật cấu hình giá thành công.');
      } else if (success > 0) {
        toastr.warning(`Cập nhật thành công ${success}/${prices.length} cấu hình giá.`);
      } else {
        toastr.error('Cập nhật cấu hình giá không thành công.');
      }
    } catch(e) {
      debug.error(`ERROR in Update Shipment Price Promises`, e);
      throw e;
    }

  }

  back() {
    this.toBack.emit();
  }

}
