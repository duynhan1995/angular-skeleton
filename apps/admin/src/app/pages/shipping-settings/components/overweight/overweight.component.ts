import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {OverWeight} from "libs/models/admin/ShipmentPrice";

@Component({
  selector: 'admin-overweight',
  templateUrl: './overweight.component.html',
  styleUrls: ['./overweight.component.scss']
})
export class OverweightComponent implements OnInit, OnChanges {
  @Input() mainWeight: number;
  @Input() overweights: OverWeight[] = [];

  constructor() { }

  get canAddOverweight() {
    return !!this.nearestMaxWeight;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.mainWeight && changes.mainWeight.currentValue != changes.mainWeight.previousValue) {
      this.updateNearestMinWeight();
    }
  }

  get nearestMaxWeight() {
    const numOfOverweight = this.overweights && this.overweights.length || null;
    return !numOfOverweight ?
      this.mainWeight :
      this.overweights[numOfOverweight - 1].max_weight;
  }

  private validateWeight(maxWeight: number, minWeight: number, index: number) {
    if (!maxWeight && maxWeight != 0) {
      return true;
    }
    if (this.infiniteMaxEnable(index) && maxWeight == -1) {
      return true;
    }
    if (maxWeight <= minWeight) {
      toastr.error('Khối lượng tối đa phải lớn hơn khối lượng tối thiểu');
      return false;
    }
    return true;
  }

  infiniteMaxEnable(index: number) {
    if (!this.overweights || !this.overweights.length) {
      return false;
    }
    return index == this.overweights.length - 1;
  }

  onMaxWeightInputBlur(index: number) {
    const validWeight = this.validateWeight(
      this.overweights[index].max_weight,
      this.overweights[index].min_weight,
      index
    );
    this.overweights[index].max_weight_error = !validWeight;
    if (!validWeight) { return; }

    this.updateNearestMinWeight(index);
  }

  updateNearestMinWeight(index?: number) {
    const _overweights = this.overweights;
    if (!this.overweights || !this.overweights.length) { return; }

    if (index >= 0 && index == _overweights.length - 1) { return; }

    if (!index && index != 0) {
      _overweights[0].min_weight = this.mainWeight + 1;
      const validWeight = this.validateWeight(
        _overweights[0].max_weight,
        _overweights[0].min_weight,
        0
      );
      _overweights[0].max_weight_error = !validWeight;
    } else {
      _overweights[index + 1].min_weight = _overweights[index].max_weight + 1;
      const validWeight = this.validateWeight(
        _overweights[index + 1].max_weight,
        _overweights[index + 1].min_weight,
        index + 1
      );
      _overweights[index + 1].max_weight_error = !validWeight;
    }

    this.overweights = _overweights;
  }

  addMoreOverweight() {
    if (this.overweights) {
      this.overweights.push({
        max_weight: null,
        min_weight: this.nearestMaxWeight + 1,
        weight_step: null,
        price_step: null
      });
    } else {
      this.overweights = [{
        max_weight: null,
        min_weight: this.nearestMaxWeight + 1,
        weight_step: null,
        price_step: null
      }];
    }
  }

  removeOverWeight(index: number) {
    this.overweights.splice(index, 1);
  }

}
