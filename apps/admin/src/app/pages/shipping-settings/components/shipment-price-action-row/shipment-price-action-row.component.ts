import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {
  ProvinceType,
  RegionType,
  ShipmentPrice,
  ShipmentPriceLocation,
  ShipmentPriceUrban,
  UrbanType
} from 'libs/models/admin/ShipmentPrice';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AdvancedLocationModalComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-services/components/advanced-location-modal/advanced-location-modal.component';
import { UtilService } from 'apps/core/src/services/util.service';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";

@Component({
  selector: '[admin-shipment-price-action-row]',
  templateUrl: './shipment-price-action-row.component.html',
  styleUrls: ['./shipment-price-action-row.component.scss']
})
export class ShipmentPriceActionRowComponent implements OnInit, OnChanges {
  @Output() rowRemoved = new EventEmitter();
  @Input() shipmentPrice = new ShipmentPrice({});
  @Input() index: number;

  shipmentPriceLocations = [
    {
      value: 'province_noi_tinh',
      name: 'Nội tỉnh'
    },
    {
      value: 'province_lien_tinh',
      name: 'Liên tỉnh'
    },
    {
      value: 'region_noi_mien',
      name: 'Nội miền'
    },
    {
      value: 'region_lien_mien',
      name: 'Liên miền'
    },
    {
      value: 'region_can_mien',
      name: 'Cận miền'
    },
    {
      value: 'advanced',
      name: 'Tuỳ chọn nâng cao'
    }
  ];

  shipmentPriceUrbans = [
    {
      value: 'noi_thanh',
      name: 'Nội thành'
    },
    {
      value: 'ngoai_thanh_1',
      name: 'Ngoại thành 1'
    },
    {
      value: 'ngoai_thanh_2',
      name: 'Ngoại thành 2'
    },
    {
      value: 'all',
      name: 'Tất cả'
    }
  ];

  displayOverweight = false;
  displayAdditionalFee = false;

  previousWeight: number;

  constructor(
    private util: UtilService,
    private modalController: ModalController,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ) {}

  get numberOfAdditionalFees() {
    if (!this.shipmentPrice.additional_fees) {
      return 0;
    }
    return this.shipmentPrice.additional_fees.filter(f => f.rules?.length)
      .reduce((a,b) => a + b.rules.length, 0);
  }

  get advancedLocationDisplay() {
    return this.shippingSettingsService.shipmentPriceAdvancedLocationMap(this.shipmentPrice).trim();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.shipmentPrice && changes.shipmentPrice.currentValue) {
      const shipmentPrice = changes.shipmentPrice.currentValue;
      this.previousWeight = shipmentPrice?.details[0]?.weight;
    }
  }

  onWeightInputBlur() {
    const _weight = this.shipmentPrice.details[0].weight;
    if (!_weight && _weight != 0) {
      if (this.previousWeight) {
        toastr.error('Khối lượng của cấu hình giá không được để trống.');
        this.shipmentPrice.details[0].weight = this.previousWeight;
      }
      return;
    }
    this.previousWeight = _weight;
  }

  toggleOverWeight() {
    this.displayAdditionalFee = false;
    this.displayOverweight = !this.displayOverweight;
  }

  toggleAdditionalFee() {
    this.displayOverweight = false;
    this.displayAdditionalFee = !this.displayAdditionalFee;
  }

  onLocationChange() {
    if (this.shipmentPrice.location == ShipmentPriceLocation.advanced) {
      const {customRegions} = this.shippingSettingsStore.snapshot;

      const modal = this.modalController.create({
        component: AdvancedLocationModalComponent,
        cssClass: 'modal-xl',
        componentProps: {
          shipmentPrice: this.util.deepClone(this.shipmentPrice),
          customRegions
        }
      });
      modal.show().then();
      modal.onDismiss().then(latestLocationType => {
        const {
          region_types,
          province_types,
          custom_region_types,
          custom_region_ids
        } = latestLocationType;

        this.shipmentPrice.region_types = region_types;
        this.shipmentPrice.province_types = province_types;
        this.shipmentPrice.custom_region_types = custom_region_types;
        this.shipmentPrice.custom_region_ids = custom_region_ids;
        this.shipmentPrice.latest_location_type = latestLocationType;
      });
    } else {
      const slices = this.shipmentPrice.location.split(/_(.+)/);
      const location_type = slices[0];
      this.shipmentPrice.custom_region_types = [];
      this.shipmentPrice.custom_region_ids = [];
      if (location_type == 'province') {
        const province_type = slices[1];
        this.shipmentPrice.region_types = [];
        this.shipmentPrice.province_types = [ProvinceType[province_type]];
      }
      if (location_type == 'region') {
        const region_type = slices[1];
        this.shipmentPrice.province_types = [];
        this.shipmentPrice.region_types = [RegionType[region_type]];
      }
    }
  }

  onUrbanChange() {
    if (this.shipmentPrice.urban == ShipmentPriceUrban.all) {
      this.shipmentPrice.urban_types = [
        UrbanType.noi_thanh,
        UrbanType.ngoai_thanh_1,
        UrbanType.ngoai_thanh_2
      ];
    } else {
      const urban_type = this.shipmentPrice.urban;
      this.shipmentPrice.urban_types = [UrbanType[urban_type]];
    }
  }

}
