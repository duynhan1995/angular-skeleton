import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShipmentPriceActionsComponent} from "apps/admin/src/app/pages/shipping-settings/components/shipment-price-actions/shipment-price-actions.component";
import {ShipmentPriceActionRowComponent} from "apps/admin/src/app/pages/shipping-settings/components/shipment-price-action-row/shipment-price-action-row.component";
import {DragdropListModule} from "apps/shared/src/components/dragdrop-list/dragdrop-list.module";
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from "@etop/shared";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { OverweightComponent } from './overweight/overweight.component';
import { AdditionalFeeComponent } from './additional-fee/additional-fee.component';
import {ShipmentPriceRowComponent} from "apps/admin/src/app/pages/shipping-settings/components/shipment-price-row/shipment-price-row.component";
import {AuthenticateModule} from "@etop/core";

@NgModule({
  declarations: [
    ShipmentPriceActionsComponent,
    ShipmentPriceActionRowComponent,
    OverweightComponent,
    AdditionalFeeComponent,
    ShipmentPriceRowComponent
  ],
  exports: [
    ShipmentPriceActionsComponent,
    ShipmentPriceRowComponent
  ],
  imports: [
    CommonModule,
    DragdropListModule,
    EtopMaterialModule,
    EtopPipesModule,
    NgbModule,
    MaterialModule,
    AuthenticateModule
  ]
})
export class ComponentsModule { }
