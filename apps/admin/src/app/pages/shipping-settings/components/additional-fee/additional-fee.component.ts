import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {
  CalculationMethod,
  PriceModifierType,
  Rule,
  ShipmentPriceAdditionalFee
} from "libs/models/admin/ShipmentPrice";
import {ShipmentPriceApi} from "@etop/api";
import {FeeType} from "libs/models/Fee";

const DEFAULT_FEE_TYPES: FeeType[] = [
  FeeType.insurance, FeeType.cods, FeeType.redelivery, FeeType.return, FeeType.adjustment
];

@Component({
  selector: 'admin-additional-fee',
  templateUrl: './additional-fee.component.html',
  styleUrls: ['./additional-fee.component.scss']
})
export class AdditionalFeeComponent implements OnInit, OnChanges {

  constructor() {}

  @Input() additionalFees: ShipmentPriceAdditionalFee[] = [];

  private static infiniteMaxEnable(index: number, rules: Rule[]) {
    if (!rules || !rules.length) {
      return false;
    }
    return index == rules.length - 1;
  }

  private static validateValue(index: number, rules: Rule[]) {
    if (!rules[index].max_value && rules[index].max_value != 0) {
      return true;
    }
    if (AdditionalFeeComponent.infiniteMaxEnable(index, rules) && rules[index].max_value == -1) {
      return true;
    }
    if (rules[index].max_value <= rules[index].min_value) {
      toastr.error('Khối lượng tối đa phải lớn hơn khối lượng tối thiểu');
      return false;
    }
    return true;
  }

  isRangeFee(feeType: FeeType) {
    return [FeeType.insurance, FeeType.cods].includes(feeType);
  }

  canAddFee(feeType: FeeType) {
    return !!this.nearestMaxValue(feeType);
  }

  isPercentage(rule: Rule) {
    return rule?.price_modifier_type == PriceModifierType.percentage;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.prepareAdditionalFees();
  }

  prepareAdditionalFees() {
    const _feeTypes = this.additionalFees.map(f => f.fee_type);
    DEFAULT_FEE_TYPES.forEach(feeType => {
      if (!_feeTypes.includes(feeType)) {
        this.additionalFees.push({
          fee_type: feeType,
          fee_type_display: ShipmentPriceApi.shipmentPriceAdditionalFeeTypeMap(feeType),
          base_value_type: ShipmentPriceApi.mapBaseValueTypeByFeeType(feeType),
          base_value_type_display: ShipmentPriceApi.baseValueTypeMap(
            ShipmentPriceApi.mapBaseValueTypeByFeeType(feeType)
          ),
          calculation_method: CalculationMethod.first_satisfy
        });
      }
    });
  }

  nearestMaxValue(feeType: FeeType) {
    const _additionalFee = this.additionalFees.find(fee => fee.fee_type == feeType);
    const numOfRules = _additionalFee.rules.length;
    return !numOfRules ? -1 : _additionalFee.rules[numOfRules - 1].max_value;
  }

  addMoreFee(fee: ShipmentPriceAdditionalFee) {
    if (!fee.rules) {
      fee.rules = [];
    }
    if (this.isRangeFee(fee.fee_type)) {
      fee.rules.push({
        price_modifier_type: PriceModifierType.percentage,
        price_modifier_type_display: '%',
        min_value: this.nearestMaxValue(fee?.fee_type) + 1,
        max_value: null,
        amount: null,
      });
    } else {
      fee.rules.push({
        price_modifier_type: PriceModifierType.fixed_amount,
        price_modifier_type_display: 'đ',
        min_value: 0,
        max_value: -1,
        amount: null
      });
    }
  }

  onMaxValueInputBlur(index: number, rules: Rule[]) {
    const validValue = AdditionalFeeComponent.validateValue(
      index,
      rules
    );
    rules[index].max_value_error = !validValue;
    if (!validValue) { return; }

    this.updateNearestMinValue(index, rules);
  }

  updateNearestMinValue(index: number, rules: Rule[]) {
    if (index >= 0 && index == rules.length - 1) {
      return;
    }
    rules[index + 1].min_value = rules[index].max_value + 1;
    const validValue = AdditionalFeeComponent.validateValue(
      index + 1,
      rules
    );
    rules[index + 1].max_value_error = !validValue;

  }

  removeRule(index: number, rules: Rule[]) {
    rules.splice(index, 1);
  }

  changePriceModifierType(fee: ShipmentPriceAdditionalFee) {
    fee.rules[0].amount = null;
    fee.rules[0].min_price = null;
  }

}
