import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-shipment-price-lists',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-shipment-price-list-list></admin-shipment-price-list-list>
    </ng-template>`
})
export class ShipmentPriceListsComponent extends PageBaseComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/shipment_price_list:view');
  }

  ngOnInit() {
  }

}
