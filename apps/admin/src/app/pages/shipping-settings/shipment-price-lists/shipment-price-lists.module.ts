import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  EtopCommonModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule,
  NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { CreateShipmentPriceListFormComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-price-lists/component/create-shipment-price-list-form/create-shipment-price-list-form.component';
import { EditShipmentPriceListFormComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-price-lists/component/edit-shipment-price-list-form/edit-shipment-price-list-form.component';
import { ShipmentPriceListRowComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-price-lists/component/shipment-price-list-row/shipment-price-list-row.component';
import { ShipmentPriceListListComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-price-lists/shipment-price-list-list/shipment-price-list-list.component';
import { RouterModule, Routes } from '@angular/router';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { SharedModule } from 'apps/shared/src/shared.module';
import {ComponentsModule} from "apps/admin/src/app/pages/shipping-settings/components/components.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DuplicateSplModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-price-lists/component/duplicate-spl-modal/duplicate-spl-modal.component";
import {AuthenticateModule} from "@etop/core";
import {ShipmentPriceListsComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-price-lists/shipment-price-lists.component";

const routes: Routes = [
  {
    path: '',
    component: ShipmentPriceListsComponent
  }
];

@NgModule({
  declarations: [
    ShipmentPriceListsComponent,
    ShipmentPriceListListComponent,
    ShipmentPriceListRowComponent,
    CreateShipmentPriceListFormComponent,
    EditShipmentPriceListFormComponent,
    DuplicateSplModalComponent,
  ],
  entryComponents: [
    DuplicateSplModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    DropdownActionsModule,
    FormsModule,
    EtopPipesModule,
    EtopMaterialModule,
    SideSliderModule,
    EtopCommonModule,
    MaterialModule,
    ComponentsModule,
    NgbModule,
    NotPermissionModule,
    AuthenticateModule
  ]
})
export class ShipmentPriceListsModule { }
