import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ShipmentPriceAPI, ShipmentPriceApi} from '@etop/api';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ShipmentPriceList } from 'libs/models/admin/ShipmentPrice';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import {AuthenticateStore} from "@etop/core";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {DuplicateSplModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-price-lists/component/duplicate-spl-modal/duplicate-spl-modal.component";
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";

@Component({
  selector: 'admin-shipment-price-list-list',
  templateUrl: './shipment-price-list-list.component.html',
  styleUrls: ['./shipment-price-list-list.component.scss']
})
export class ShipmentPriceListListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('shipmentPriceListTable', { static: true }) shipmentPriceListTable: EtopTableComponent;
  @ViewChild('shipmentPriceListSlider', { static: true }) shipmentPriceListSlider: SideSliderComponent;

  connections$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  shipmentPriceListsLoading$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.shipmentPriceListsLoading),
    distinctUntilChanged()
  );
  shipmentPriceListsList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.shipmentPriceLists),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );
  activeShipmentPriceList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentPriceList),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  filterConnections = [];
  filterConnectionId: string = null;

  onNewShipmentPriceList = false;

  dropdownActions: DropdownActionOpt[] = [];

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private modal: ModalController,
    private dialog: DialogControllerService,
    private api: ShipmentPriceApi,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ) {
    super();
  }

  get showPaging() {
    return !this.shipmentPriceListTable.liteMode && !this.shipmentPriceListTable.loading;
  }

  get emptyTitle() {
    return 'Chưa có bảng giá nào';
  }

  get sliderTitle() {
    if (this.onNewShipmentPriceList) {
      return 'Tạo bảng giá mới'
    }
    return 'Chi tiết bảng giá';
  }

  ngOnInit() {
    this.headerController.setActions([
      {
        onClick: () => this.createShipmentPriceList(),
        title: 'Tạo bảng giá',
        cssClass: 'btn btn-primary',
        permissions: ['admin/shipment_price_list:create']
      }
    ]);

    this.shipmentPriceListsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        if (this.shipmentPriceListTable) {
          this.shipmentPriceListTable.loading = loading;
        }
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.filterConnections = [{name: 'Tất cả', id: null}].concat(this.shippingSettingsStore.snapshot.connections.map(conn => {
      return {
        name: conn.name,
        id: conn.id
      }
    }));
    this.connections$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.filterConnections = [{name: 'Tất cả', id: null}].concat(connections.map(conn => {
          return {
            name: conn.name,
            id: conn.id
          }
        }));
      });

    this.activeShipmentPriceList$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.setupDropdownActions();
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.shippingSettingsService.selectShipmentPriceList(null);
  }

  resetState() {
    this.shipmentPriceListTable.toggleLiteMode(false);
    this.shipmentPriceListSlider.toggleLiteMode(false);
    this.shipmentPriceListTable.toggleNextDisabled(false);
    this.onNewShipmentPriceList = false;
  }

  filterByConnectionId() {
    if (!this.filterConnectionId) {
      return this.loadPage({});
    }

    return this.getShipmentPriceLists({connection_id: this.filterConnectionId});
  }

  getShipmentPriceLists(filter?: ShipmentPriceAPI.GetShipmentPriceListsFilter) {
    const connections = this.shippingSettingsStore.snapshot.connections;
    if (connections?.length) {
      this.shippingSettingsService.getShipmentPriceLists(true, filter).then();
    } else {
      this.connections$.pipe(
        takeUntil(this.destroy$)
      ).subscribe(conns => {
        if (conns?.length) {
          this.shippingSettingsService.getShipmentPriceLists(true, filter).then();
        }
      });
    }
  }

  loadPage({}) {
    this.getShipmentPriceLists();
  }

  setupDropdownActions() {
    const activePriceList = this.shippingSettingsStore.snapshot.activeShipmentPriceList;
    this.dropdownActions = [
      {
        title: 'Sao chép bảng giá',
        onClick: () => this.duplicateShipmentPriceList(),
        permissions: ['admin/shipment_price_list:create']
      },
      {
        title: 'Xoá bảng giá',
        cssClass: 'text-danger',
        onClick: () => this.deleteShipmentPriceList(),
        hidden: !activePriceList || activePriceList.is_default,
        permissions: ['admin/shipment_price_list:delete']
      }
    ];
  }

  detail(spl: ShipmentPriceList) {
    this.onNewShipmentPriceList = false;
    this.shippingSettingsService.selectShipmentPriceList(spl);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onNewShipmentPriceList = false;
    this.shippingSettingsService.selectShipmentPriceList(null);

    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.shippingSettingsStore.snapshot.activeShipmentPriceList || this.onNewShipmentPriceList;
    this.shipmentPriceListTable.toggleLiteMode(selected);
    this.shipmentPriceListSlider.toggleLiteMode(selected);
  }

  createShipmentPriceList() {
    this.shippingSettingsService.selectShipmentPriceList(null);
    this.onNewShipmentPriceList = true;
    this._checkSelectMode();
  }

  deleteShipmentPriceList() {
    const { activeShipmentPriceList, shipmentPrices } = this.shippingSettingsStore.snapshot;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá bảng giá`,
      body: `
<div>Bạn có chắc muốn xoá bảng giá <strong>${activeShipmentPriceList.name}</strong>?</div>
<div>
  Bảng giá <strong>${activeShipmentPriceList.name}</strong> hiện đang có <strong>${shipmentPrices?.length || 0}</strong> cấu hình giá.
</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.api.deleteShipmentPriceList(activeShipmentPriceList.id);
          toastr.success('Xoá bảng giá thành công.');
          modal.close().then();
          this.loadPage({});
        } catch (e) {
          debug.error('ERROR in deleting ShipmentPriceList', e);
          toastr.error(`Xoá bảng giá không thành công.`, e.code ? (e.message || e.msg) : '');
        }
      }
    });
    modal.show().then();
  }

  duplicateShipmentPriceList() {
    const { activeShipmentPriceList, shipmentPrices } = this.shippingSettingsStore.snapshot;
    const modal = this.modal.create({
      component: DuplicateSplModalComponent,
      componentProps: {
        activeShipmentPriceList, shipmentPrices
      }
    });
    modal.onDismiss().then((success: string) => {
      if (success) {
        this.shippingSettingsService.getShipmentPriceLists().then();
      }
    });
    modal.show().then();
  }

}
