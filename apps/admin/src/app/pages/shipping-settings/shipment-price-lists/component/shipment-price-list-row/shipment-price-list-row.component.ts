import { Component, Input, OnInit } from '@angular/core';
import { ShipmentPriceList } from 'libs/models/admin/ShipmentPrice';

@Component({
  selector: '[admin-shipment-price-list-row]',
  templateUrl: './shipment-price-list-row.component.html',
  styleUrls: ['./shipment-price-list-row.component.scss']
})
export class ShipmentPriceListRowComponent implements OnInit {
  @Input() shipmentPriceList = new ShipmentPriceList({});
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
