import { Component, OnInit } from '@angular/core';
import { ShipmentPriceAPI, ShipmentPriceApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {
  ShipmentPrice,
  ShipmentPriceList,
  ShipmentService
} from 'libs/models/admin/ShipmentPrice';
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {FormBuilder} from "@angular/forms";
import {Connection} from "libs/models/Connection";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";
import {TelegramService} from "@etop/features";

export interface ShipmentPricesGroup {
  shipmentService: ShipmentService;
  shipmentPrices: ShipmentPrice[];
  onSettingShipmentPrices: boolean;
}

@Component({
  selector: 'admin-edit-shipment-service-form',
  templateUrl: './edit-shipment-price-list-form.component.html',
  styleUrls: ['./edit-shipment-price-list-form.component.scss']
})
export class EditShipmentPriceListFormComponent extends BaseComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private fb: FormBuilder,
    private dialog: DialogControllerService,
    private api: ShipmentPriceApi,
    private promiseQueue: PromiseQueueService,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore,
    private telegramService: TelegramService
  ) {
    super();
  }

  shipmentPricesGrouped: ShipmentPricesGroup[] = [];

  initializing = true;
  loading = false;
  activating = false;

  activeShipmentPriceList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentPriceList),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  connectionsList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  shipmentPriceListForm = this.fb.group({
    name: '',
    description: '',
    connection_id: '',
    is_default: false
  });

  modal: any;
  settingShipmentPricesAction: 'create' | 'update';

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${connection.name.toUpperCase()}</div>
      </div>`;
  }

  private static validShipmentPriceList(spl: ShipmentPriceList) {
    const { name, connection_id } = spl;
    if (!connection_id) {
      toastr.error('Chưa chọn nhà vận chuyển.');
      return false;
    }
    if (!name) {
      toastr.error('Chưa nhập tên bảng giá.');
      return false;
    }
    return true;
  }

  displayMap = option => option && EditShipmentPriceListFormComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/shipment_price_list:update');
  }

  async ngOnInit() {
    this.shipmentPriceListForm.controls['is_default'].valueChanges.subscribe(isDefault => {
      if (isDefault) {
        this.setDefaultShipmentPriceList();
      }
    });

    this.getFormDataStore();
    await this.groupByShipmentServices();
    this.activeShipmentPriceList$.pipe(takeUntil(this.destroy$))
      .subscribe(async(priceList) => {
        if (priceList) {
          this.getFormDataStore();
          await this.groupByShipmentServices();
        }
      });
  }

  getFormDataStore() {
    const _priceList = this.shippingSettingsStore.snapshot.activeShipmentPriceList;
    if (!_priceList) { return this.shipmentPriceListForm.reset(); }
    const {name, description, connection_id, is_default} = _priceList;

    if (is_default) {
      this.shipmentPriceListForm.controls['is_default'].disable({emitEvent: false});
    } else {
      this.shipmentPriceListForm.controls['is_default'].enable({emitEvent: false});
    }

    this.shipmentPriceListForm.patchValue({
      name, description, connection_id, is_default
    }, {emitEvent: false});
  }

  private async groupByShipmentServices() {
    this.initializing = true;
    try {
      this.shipmentPricesGrouped = [];

      const {connection_id, id} = this.shippingSettingsStore.snapshot.activeShipmentPriceList;

      await this.shippingSettingsService.getShipmentServices(true, {connection_id});

      const shipmentServices = this.shippingSettingsStore.snapshot.shipmentServices;
      const promises = shipmentServices.map(s => async() => {
          const shipmentPrices = await this.api.getShipmentPrices({
            shipment_service_id: s.id,
            shipment_price_list_id: id
          });
          this.shipmentPricesGrouped.push({
            shipmentService: s,
            shipmentPrices,
            onSettingShipmentPrices: false
          });

          this.shippingSettingsStore.addShipmentPrices(shipmentPrices);
        });

      await this.promiseQueue.run(promises, 5);

    } catch(e) {
      debug.error('ERROR in groupByShipmentServices', e);
    }
    this.initializing = false;
  }

  async updateShipmentPriceList() {
    this.loading = true;
    try {
      const rawShipmentPriceListData = this.shipmentPriceListForm.getRawValue();

      const valid = EditShipmentPriceListFormComponent.validShipmentPriceList(rawShipmentPriceListData);
      if (!valid) {
        return this.loading = false;
      }

      const { name, description, connection_id } = rawShipmentPriceListData;
      const body: ShipmentPriceAPI.UpdateShipmentPriceListRequest = {
        id: this.shippingSettingsStore.snapshot.activeShipmentPriceList.id,
        name,
        description,
        connection_id
      };
      await this.shippingSettingsService.updateShipmentPriceList(body);
      toastr.success('Cập nhật bảng giá thành công.');
    } catch(e) {
      toastr.error('Cập nhật bảng giá không thành công.', e.code ? (e.message || e.msg) : '');
      debug.error('ERROR in updateShipmentPriceList', e);
    }
    this.loading = false;
  }

  async setDefaultShipmentPriceList() {
    if (this.modal) { return; }
    const {activeShipmentPriceList, shipmentPrices, shipmentPriceLists} = this.shippingSettingsStore.snapshot;

    let body = `
<div>Bạn có chắc muốn đặt bảng giá <strong>${activeShipmentPriceList?.name}</strong> làm mặc định?</div>
<div>
  - Bảng giá
  <strong>${activeShipmentPriceList?.name}</strong> hiện đang có
  <strong>${shipmentPrices?.length}</strong> cấu hình giá.
</div>`;

    const currentActivatedPriceList = shipmentPriceLists
      .find(spl => spl.is_default && spl.connection_id == activeShipmentPriceList.connection_id);

    if (currentActivatedPriceList) {
      currentActivatedPriceList.shipment_prices = await this.api.getShipmentPrices(
        {shipment_price_list_id: currentActivatedPriceList.id}
      );
      body += `
<div>
  - Bảng giá mặc định hiện tại là <strong>${currentActivatedPriceList.name}</strong>, hiện đang có
  <strong>${currentActivatedPriceList.shipment_prices.length}</strong> cấu hình giá.
</div>`;

    }

    this.modal = this.dialog.createConfirmDialog({
      title: `Đặt bảng giá làm mặc định`,
      body: body + `
<div>Sau khi đặt làm mặc định, bảng giá sẽ có tác dụng <strong>ngay lập tức</strong> đến người dùng.</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmCss: 'btn-primary text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.shippingSettingsService.setDefaultShipmentPriceList(
            activeShipmentPriceList.id,
            activeShipmentPriceList.connection_id
          );
          toastr.success('Kích hoạt bảng giá thành công.');
          this.modal.close().then();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in activateShipmentPriceList', e);
          toastr.error(`Kích hoạt bảng giá không thành công.`, e.code ? (e.message || e.msg) : '');
        }
      },
      onCancel: () => {
        this.modal = null;
        this.shipmentPriceListForm.patchValue({
          is_default: false
        });
      }
    });
    this.modal.show().then();
  }

  settingShipmentPrices(shipmentPricesGroup: ShipmentPricesGroup, toClose = false) {
    if (toClose) {
      shipmentPricesGroup.onSettingShipmentPrices = false;
      this.settingShipmentPricesAction = null;
      this.shippingSettingsService.selectShipmentPrices([]);
      this.shippingSettingsService.selectShipmentService(null);
      return this.groupByShipmentServices();
    }
    this.shipmentPricesGrouped.forEach(group => {
      group.onSettingShipmentPrices = false;
    });
    shipmentPricesGroup.onSettingShipmentPrices = true;
    this.settingShipmentPricesAction = shipmentPricesGroup.shipmentPrices?.length ? 'update' : 'create';
    this.shippingSettingsService.selectShipmentPrices(shipmentPricesGroup.shipmentPrices);
    this.shippingSettingsService.selectShipmentService(shipmentPricesGroup.shipmentService);
  }

  sendTelegramMessage(changes, shipmentService: ShipmentService) {
    const {creates, updates, deletes} = changes;
    return this.telegramService.updateShipmentService(shipmentService, creates, updates, deletes);
  }

}
