import {Component, Input, OnInit} from '@angular/core';
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {ShipmentPriceAPI, ShipmentPriceApi} from "@etop/api";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";
import {ShipmentPrice, ShipmentPriceList} from "libs/models/admin/ShipmentPrice";

@Component({
  selector: 'admin-duplicate-spl-modal',
  templateUrl: './duplicate-spl-modal.component.html',
  styleUrls: ['./duplicate-spl-modal.component.scss']
})
export class DuplicateSplModalComponent implements OnInit {
  @Input() activeShipmentPriceList: ShipmentPriceList;
  @Input() shipmentPrices: ShipmentPrice[] = [];

  shipmentPriceListName: string;

  loading = false;

  constructor(
    private modalAction: ModalAction,
    private api: ShipmentPriceApi,
    private promiseQueue: PromiseQueueService
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async confirm() {
    this.loading = true;
    try {
      const { connection_id, description } = this.activeShipmentPriceList;
      const splBody: ShipmentPriceAPI.CreateShipmentPriceListRequest = {
        name: this.shipmentPriceListName,
        connection_id, description
      };
      const res = await this.api.createShipmentPriceList(splBody);

      let success = 0;

      const promises = this.shipmentPrices.map(sp => async() => {
        try {
          const {
            additional_fees, details,
            custom_region_ids, custom_region_types, region_types, province_types, urban_types,
            name, priority_point, shipment_service_id
          } = sp;

          const spBody: ShipmentPriceAPI.CreateShipmentPriceRequest = {
            additional_fees, details,
            custom_region_ids, custom_region_types, region_types, province_types, urban_types,
            name, priority_point,
            shipment_price_list_id: res.id, shipment_service_id
          };

          await this.api.createShipmentPrice(spBody);
          success += 1;

        } catch(e) {
          debug.error('ERROR in createShipmentPrice', e);
        }
      });
      await this.promiseQueue.run(promises, 3);

      if (success == this.shipmentPrices.length) {
        toastr.success('Sao chép bảng giá thành công.');
      } else if (success > 0) {
        toastr.warning(`Sao chép thành công ${success}/${this.shipmentPrices.length} cấu hình giá.`);
      } else {
        toastr.warning('Sao chép cấu hình giá không thành công.');
      }

      this.modalAction.dismiss(true);

    } catch(e) {
      debug.error('ERROR in duplicateShipmentPriceList', e);
      toastr.error(`Sao chép bảng giá không thành công.`, e.code ? (e.message || e.msg) : '');
    }
    this.loading = false;

  }

}
