import { Component, OnInit } from '@angular/core';
import { ShipmentPriceList } from 'libs/models/admin/ShipmentPrice';
import { ShipmentPriceAPI, ShipmentPriceApi } from '@etop/api';
import {Connection} from "libs/models/Connection";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {distinctUntilChanged, map} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";

@Component({
  selector: 'admin-create-shipment-service-form',
  templateUrl: './create-shipment-price-list-form.component.html',
  styleUrls: ['./create-shipment-price-list-form.component.scss']
})
export class CreateShipmentPriceListFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private api: ShipmentPriceApi,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore
  ) {}

  loading = false;

  connectionsList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  shipmentPriceListForm = this.fb.group({
    name: '',
    description: '',
    connection_id: ''
  });

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${connection.name.toUpperCase()}</div>
      </div>`;
  }

  private static validShipmentPriceList(spl: ShipmentPriceList) {
    const { name, connection_id } = spl;
    if (!connection_id) {
      toastr.error('Chưa chọn nhà vận chuyển.');
      return false;
    }
    if (!name) {
      toastr.error('Chưa nhập tên bảng giá.');
      return false;
    }
    return true;
  }

  displayMap = option => option && CreateShipmentPriceListFormComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  ngOnInit() {}

  async createShipmentPriceList() {
    this.loading = true;
    try {
      const rawShipmentPriceListData = this.shipmentPriceListForm.getRawValue();

      const valid = CreateShipmentPriceListFormComponent.validShipmentPriceList(rawShipmentPriceListData);
      if (!valid) {
        return this.loading = false;
      }

      const { name, description, connection_id } = rawShipmentPriceListData;
      const body: ShipmentPriceAPI.CreateShipmentPriceListRequest = {
        name,
        description,
        connection_id
      };
      await this.shippingSettingsService.createShipmentPriceList(body);
      toastr.success('Tạo bảng giá thành công.');
    } catch(e) {
      toastr.error('Tạo bảng giá không thành công.', e.code ? (e.message || e.msg) : '');
      debug.error('ERROR in createShipmentPriceList', e);
    }
    this.loading = false;
  }

}
