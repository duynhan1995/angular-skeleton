import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EtopMaterialModule, NotPermissionModule} from '@etop/shared';
import { ConnectionsComponent } from 'apps/admin/src/app/pages/shipping-settings/connections/connections.component';
import { RouterModule, Routes } from '@angular/router';
import { ConnectionRowComponent } from './components/connection-row/connection-row.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { UpdateBuiltinShopConnectionComponent } from './components/update-builtin-shop-connection/update-builtin-shop-connection.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import {AuthenticateModule} from "@etop/core";

const routes: Routes = [
  {
    path: '',
    component: ConnectionsComponent
  }
];

@NgModule({
  declarations: [
    ConnectionsComponent,
    ConnectionRowComponent,
    UpdateBuiltinShopConnectionComponent
  ],
  entryComponents: [
    UpdateBuiltinShopConnectionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    DropdownActionsModule,
    SharedModule,
    FormsModule,
    EtopMaterialModule,
    NotPermissionModule,
    AuthenticateModule
  ]
})
export class ConnectionsModule { }
