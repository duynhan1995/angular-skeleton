import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { AdminConnectionAPI, AdminConnectionApi } from '@etop/api';
import { Connection } from 'libs/models/Connection';

@Component({
  selector: 'admin-update-builtin-shop-connection',
  templateUrl: './update-builtin-shop-connection.component.html',
  styleUrls: ['./update-builtin-shop-connection.component.scss']
})
export class UpdateBuiltinShopConnectionComponent implements OnInit {
  @Input() connection: Connection;
  user_id: string;
  token: string;

  loading = false;

  constructor(
    private modalAction: ModalAction,
    private connectionApi: AdminConnectionApi
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async confirm() {
    this.loading = true;
    try {
      if (!this.user_id) {
        toastr.error('Vui lòng nhập user ID');
        return this.loading = false;
      }
      if (!this.token) {
        toastr.error('Vui lòng nhập token');
        return this.loading = false;
      }
      const body: AdminConnectionAPI.UpdateBuiltinShopConnectionRequest = {
        connection_id: this.connection.id,
        external_data: {
          user_id: this.user_id
        },
        token: this.token
      };

      await this.connectionApi.updateBuiltinShopConnection(body);
      toastr.success('Đăng nhập thành công.');
      this.modalAction.dismiss(null);

    } catch(e) {
      debug.error('ERROR in Updating BuiltinShopConnection', e);
      toastr.error('Đăng nhập không thành công.', e.code ? (e.message || e.msg) : '');
    }
    this.loading = false;
  }

}
