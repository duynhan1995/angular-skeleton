import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Connection } from 'libs/models/Connection';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { AdminConnectionApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { UpdateBuiltinShopConnectionComponent } from 'apps/admin/src/app/pages/shipping-settings/connections/components/update-builtin-shop-connection/update-builtin-shop-connection.component';

@Component({
  selector: '[admin-connection-row]',
  templateUrl: './connection-row.component.html',
  styleUrls: ['./connection-row.component.scss']
})
export class ConnectionRowComponent implements OnInit {
  @Output() connectionUpdated = new EventEmitter();
  @Input() connection: Connection;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: 'Đăng nhập tài khoản khác',
      onClick: () => this.updateBuiltinShopConnection()
    },
    {
      title: 'Gỡ kết nối',
      cssClass: 'text-danger',
      onClick: () => this.disableConnection()
    }
  ];

  constructor(
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private connectionApi: AdminConnectionApi
  ) { }

  get permissionsArray() {
    return ['admin/connection:disable', 'admin/connection_shop_builtin:update', 'admin/connection:confirm'];
  }

  ngOnInit() {
  }

  confirmConnection() {
    const modal = this.dialog.createConfirmDialog({
      title: `Kết nối nhà vận chuyển`,
      body: `
        <div>Bạn muốn kết nối với nhà vận chuyển <strong>${this.connection.name_display.toUpperCase()}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Kết nối',
      confirmCss: 'btn btn-primary text-white',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.connectionApi.confirmConnection(this.connection.id);
          toastr.success(
            `Kết nối với nhà vận chuyển <strong>${this.connection.name_display.toUpperCase()}</strong> thành công.`
          );
          modal.close().then();
          this.connectionUpdated.emit();
        } catch (e) {
          debug.error('ERROR in confirming Connection', e);
          toastr.error(
            `Kết nối với nhà vận chuyển <strong>${this.connection.name_display.toUpperCase()}</strong> không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  disableConnection() {
    const modal = this.dialog.createConfirmDialog({
      title: `Gỡ kết nối nhà vận chuyển`,
      body: `
        <div>Bạn có chắc muốn gỡ kết nối với nhà vận chuyển <strong>${this.connection.name_display.toUpperCase()}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Gỡ kết nối',
      confirmCss: 'btn btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.connectionApi.disableConnection(this.connection.id);
          toastr.success(
            `Gỡ kết nối với nhà vận chuyển <strong>${this.connection.name_display.toUpperCase()}</strong> thành công.`
          );
          modal.close().then();
          this.connectionUpdated.emit();
        } catch (e) {
          debug.error('ERROR in confirming Connection', e);
          toastr.error(
            `Gỡ kết nối với nhà vận chuyển <strong>${this.connection.name_display.toUpperCase()}</strong> không thành công.`,
            e.code ? (e.message || e.msg) : ''
          );
        }
      }
    });
    modal.show().then();
  }

  updateBuiltinShopConnection() {
    const modal = this.modalController.create({
      component: UpdateBuiltinShopConnectionComponent,
      componentProps: {
        connection: this.connection
      }
    });
    modal.show().then();
    modal.onDismiss().then(_ => {
      this.connectionUpdated.emit();
    });
  }

}
