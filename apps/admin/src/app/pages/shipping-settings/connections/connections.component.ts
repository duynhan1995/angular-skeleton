import { Component, OnInit } from '@angular/core';
import { Connection } from 'libs/models/Connection';
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {distinctUntilChanged, map} from "rxjs/operators";
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.scss']
})
export class ConnectionsComponent implements OnInit {
  connectionsLoading$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connectionsLoading),
    distinctUntilChanged()
  );
  connectionsList$ = this.shippingSettingsStore.state$.pipe(map(s => s?.connections));
  activeConnection$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeConnection),
    distinctUntilChanged((prev, next) => prev?.id == next?.id)
  );

  constructor(
    private auth: AuthenticateStore,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore
  ) { }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/connection_service:view');
  }

  ngOnInit() {
  }

  getConnections() {
    this.shippingSettingsService.getConnections().then();
  }

  async selectConnection(connection: Connection) {
    this.shippingSettingsService.selectConnection(connection);
  }

}
