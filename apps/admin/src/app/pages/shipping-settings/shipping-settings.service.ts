import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {HeaderTab, ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {
  AdminConnectionApi,
  AdminLocationAPI,
  AdminLocationApi,
  ShipmentPriceAPI,
  ShipmentPriceApi
} from "@etop/api";
import {Connection} from "libs/models/Connection";
import {
  AvailableLocation,
  BlacklistLocation,
  ShipmentPrice,
  ShipmentPriceList, ShipmentPriceLocation,
  ShipmentService,
  ShippingLocationType
} from "libs/models/admin/ShipmentPrice";
import {CustomRegion} from "libs/models/Location";
import {requireMethodPermissions} from "@etop/core";

@Injectable()
export class ShippingSettingsService {

  constructor(
    private router: Router,
    private connectionApi: AdminConnectionApi,
    private shipmentPriceApi: ShipmentPriceApi,
    private adminLocationApi: AdminLocationApi,
    private store: ShippingSettingsStore
  ) {
    const tab: any = window.location.pathname.split('/')[2];
    if (tab) {
      this.store.changeHeaderTab(tab);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('shipping-settings') != -1) {
          const _tab: any = event.url.split('/')[2];
          if (_tab) {
            this.store.changeHeaderTab(_tab);
          } else {
            this.store.changeHeaderTab(HeaderTab.connections);
          }
        }
      }
    });
  }

  // NOTE: Get nhà-vận-chuyển
  @requireMethodPermissions({
    skipError: true,
    valueOnError: [],
    permissions: ['admin/connection_service:view']
  })
  private async getConnectionServices(connection_id: string) {
    return await this.connectionApi.getConnectionServices(connection_id)
  }

  async getConnections() {
    this.store.setConnectionsLoading(true);
    try {
      const connections = await this.connectionApi.getConnections('builtin');
      await Promise.all(connections.map(async(conn) => {
        conn.connection_services = await this.getConnectionServices(conn.id);
      }));
      this.store.setConnections(connections);
      this.store.selectConnection(connections[0]);
    } catch(e) {
      debug.error('ERROR in getting Connections', e);
    }
    this.store.setConnectionsLoading(false);
  }

  selectConnection(connection: Connection) {
    this.store.selectConnection(connection);
  }

  // NOTE: Get gói-dịch-vụ
  async getShipmentServices(doLoading = true, filter?: ShipmentPriceAPI.GetShipmentServicesFilter) {
    if (doLoading) {
      this.store.setShipmentServicesLoading(true);
    }
    try {
      const shipmentServices = await this.shipmentPriceApi.getShipmentServices(filter);
      this.store.setShipmentServices(shipmentServices);
      if (doLoading) {
        this.selectShipmentService(null);
      }
    } catch(e) {
      debug.error('ERROR in getting ShipmentServices', e);
    }
    if (doLoading) {
      this.store.setShipmentServicesLoading(false);
    }
  }

  selectShipmentService(shipmentService: ShipmentService) {
    const connections = this.store.snapshot.connections;
    const connection = connections.find(conn => conn.id == shipmentService?.connection_id);

    this.store.selectConnection(connection || connections[0]);
    this.store.selectShipmentService(shipmentService);

    const _pick: AvailableLocation = shipmentService?.available_locations
      .find(loc => loc.shipping_location_type == 'pick');
    this.store.setActiveShipmentServiceAvailablePick(
      _pick || {shipping_location_type: ShippingLocationType.pick}
    );

    const _deliver: AvailableLocation = shipmentService?.available_locations
      .find(loc => loc.shipping_location_type == 'deliver');
    this.store.setActiveShipmentServiceAvailableDeliver(
      _deliver || {shipping_location_type: ShippingLocationType.deliver}
    );
  }

  async createShipmentService(request: ShipmentPriceAPI.CreateShipmentServiceRequest) {
    await this.shipmentPriceApi.createShipmentService(request);
    this.getShipmentServices().then();
  }

  async updateShipmentService(request: ShipmentPriceAPI.UpdateShipmentServiceRequest) {
    const { ed_code, name, description, connection_id, status, service_ids, id } = request;
    const body: ShipmentPriceAPI.UpdateShipmentServiceRequest = {
      id, status, ed_code, name, description,
      connection_id, service_ids
    };
    await this.shipmentPriceApi.updateShipmentService(body);

    this.getShipmentServices(false).then(_ => {
      const _service = this.store.snapshot.shipmentServices.find(s => s.id == id);
      this.selectShipmentService(_service);
    });
  }

  async updateShipmentServiceStatus(id: string, status: string) {
    try {
      await this.shipmentPriceApi.updateShipmentServiceStatus(id, status);
      toastr.success('Cập nhật trạng thái gói dịch vụ thành công');

      this.getShipmentServices(false).then(_ => {
        const _service = this.store.snapshot.shipmentServices.find(s => s.id == id);
        this.selectShipmentService(_service);
      });
    } catch(e) {
      debug.error('ERROR in updateShipmentServiceStatus', e);
      toastr.error('Cập nhật trạng thái gói dịch vụ không thành công', e.code && (e.message || e.msg));
      throw e;
    }
  }

  async updateShipmentServiceBlacklistLocations(request: {blacklist_locations: BlacklistLocation[], ids: string[]}) {
    await this.shipmentPriceApi.updateShipmentServicesBlacklistLocations(request);

    this.getShipmentServices(false).then(_ => {
      const _service = this.store.snapshot.shipmentServices.find(s => s.id == request.ids[0]);
      this.selectShipmentService(_service);
    });
  }

  async updateShipmentServiceAvailableLocations(request: {available_locations: AvailableLocation[],ids:string[]}){
    await this.shipmentPriceApi.updateShipmentServicesAvailableLocations(request);

    this.getShipmentServices(false).then(_ => {
      const _service = this.store.snapshot.shipmentServices.find(s => s.id == request.ids[0]);
      this.selectShipmentService(_service);
    });
  }

  // NOTE: Get bảng-giá
  async getShipmentPriceLists(doLoading = true, filter?: ShipmentPriceAPI.GetShipmentPriceListsFilter) {
    if (doLoading) {
      this.store.setShipmentPriceListsLoading(true);
    }
    try {
      const priceLists = await this.shipmentPriceApi.getShipmentPriceLists(filter);
      this.store.setShipmentPriceLists(priceLists);
      if (doLoading) {
        this.selectShipmentPriceList(null);
      }
    } catch(e) {
      debug.error('ERROR in getting ShipmentPriceLists', e);
    }
    if (doLoading) {
      this.store.setShipmentPriceListsLoading(false);
    }
  }

  selectShipmentPriceList(priceList: ShipmentPriceList) {
    this.store.selectShipmentPriceList(priceList)
  }

  async createShipmentPriceList(request: ShipmentPriceAPI.CreateShipmentPriceListRequest, doRefreshList = true) {
    await this.shipmentPriceApi.createShipmentPriceList(request);
    if (doRefreshList) {
      this.getShipmentPriceLists().then();
    }
  }

  async updateShipmentPriceList(request: ShipmentPriceAPI.UpdateShipmentPriceListRequest) {
    await this.shipmentPriceApi.updateShipmentPriceList(request);

    this.getShipmentPriceLists(false).then(_ => {
      const _priceList = this.store.snapshot.shipmentPriceLists.find(s => s.id == request.id);
      this.selectShipmentPriceList(_priceList);
    });
  }

  async setDefaultShipmentPriceList(id: string, connection_id: string) {
    await this.shipmentPriceApi.setDefaultShipmentPriceList(id, connection_id);

    this.getShipmentPriceLists(false).then(_ => {
      const _priceList = this.store.snapshot.shipmentPriceLists.find(s => s.id == id);
      this.selectShipmentPriceList(_priceList);
    });
  }

  // NOTE: Get vùng-tự-định-nghĩa

  @requireMethodPermissions({
    skipError: true,
    permissions: ['admin/custom_region:view']
  })
  async getCustomRegions(doLoading = true) {
    if (doLoading) {
      this.store.setCustomRegionsLoading(true);
    }
    try {
      const customRegions = await this.adminLocationApi.getCustomRegions();
      this.store.setCustomRegions(customRegions);
      if (doLoading) {
        this.selectCustomRegion(null);
      }
    } catch(e) {
      debug.error('ERROR in getting CustomRegions', e);
    }
    if (doLoading) {
      this.store.setCustomRegionsLoading(false);
    }
  }

  selectCustomRegion(customRegion: CustomRegion) {
    this.store.selectCustomRegion(customRegion);
  }

  async createCustomRegion(request: AdminLocationAPI.CreateCustomRegionRequest) {
    await this.adminLocationApi.createCustomRegion(request);
    this.getCustomRegions().then();
  }

  async updateCustomRegion(request: AdminLocationAPI.UpdateCustomRegionRequest) {
    await this.adminLocationApi.updateCustomRegion(request);

    this.getCustomRegions(false).then(_ => {
      const _customRegion = this.store.snapshot.customRegions.find(cr => cr.id == request.id);
      this.selectCustomRegion(_customRegion);
    });
  }

  // NOTE: Get cấu-hình-giá
  async getShipmentPrices(getBy: "shipmentPriceList" | "shipmentService") {
    if (getBy == "shipmentPriceList") {
      const activeShipmentPriceList = this.store.snapshot.activeShipmentPriceList;
      const shipmentPrices = await this.shipmentPriceApi.getShipmentPrices(
        {shipment_price_list_id: activeShipmentPriceList?.id}
      );
      this.store.selectShipmentPrices(shipmentPrices);
    } else {
      const activeShipmentService = this.store.snapshot.activeShipmentService;
      const shipmentPrices = await this.shipmentPriceApi.getShipmentPrices(
        {shipment_service_id: activeShipmentService?.id}
      );
      this.store.selectShipmentPrices(shipmentPrices);
    }
  }

  shipmentPriceAdvancedLocationMap(shipmentPrice: ShipmentPrice) {
    if (shipmentPrice.location == ShipmentPriceLocation.advanced) {
      const {province_types, region_types, custom_region_types, custom_region_ids} = shipmentPrice;

      const provinceTypeDisplay = province_types?.map(
        t => ShipmentPriceApi.shipmentPriceProvinceTypeMap(t)
      ).join(' và ');
      const regionTypeDisplay = region_types?.map(
        t => ShipmentPriceApi.shipmentPriceRegionTypeMap(t)
      ).join(' và ');
      const customRegionTypeDisplay = custom_region_types?.map(
        t => ShipmentPriceApi.shipmentPriceCustomRegionTypeMap(t)
      ).join(' và ');

      const display = [provinceTypeDisplay, regionTypeDisplay, customRegionTypeDisplay].filter(item => !!item).join(', ');

      const customRegionNames = custom_region_ids?.map(id => {
          const found = this.store.snapshot.customRegions.find(cr => cr.id == id);
          return found?.name;
        })
        .filter(name => !!name)
        .join(', ');

      return `${display} ${customRegionNames}`;
    }
    return '';
  }

  selectShipmentPrices(shipmentPrices: ShipmentPrice[]) {
    this.store.selectShipmentPrices(shipmentPrices);
  }

}
