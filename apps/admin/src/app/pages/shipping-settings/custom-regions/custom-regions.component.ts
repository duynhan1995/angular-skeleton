import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-custom-regions',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-custom-region-list></admin-custom-region-list>
    </ng-template>`
})
export class CustomRegionsComponent extends PageBaseComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/custom_region:view');
  }

  ngOnInit() {
  }

}
