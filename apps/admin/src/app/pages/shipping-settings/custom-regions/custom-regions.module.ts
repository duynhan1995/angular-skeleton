import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  EtopCommonModule,
  EtopMaterialModule,
  EtopPipesModule, MaterialModule,
  NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import {CustomRegionListComponent} from './custom-region-list/custom-region-list.component';
import {RouterModule, Routes} from '@angular/router';
import {CustomRegionRowComponent} from 'apps/admin/src/app/pages/shipping-settings/custom-regions/components/custom-region-row/custom-region-row.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {CreateCustomRegionFormComponent} from 'apps/admin/src/app/pages/shipping-settings/custom-regions/components/create-custom-region-form/create-custom-region-form.component';
import {FormsModule} from '@angular/forms';
import {EditCustomRegionFormComponent} from 'apps/admin/src/app/pages/shipping-settings/custom-regions/components/edit-custom-region-form/edit-custom-region-form.component';
import {CustomRegionsComponent} from "apps/admin/src/app/pages/shipping-settings/custom-regions/custom-regions.component";
import {AuthenticateModule} from "@etop/core";

const routes: Routes = [
  {
    path: '',
    component: CustomRegionsComponent
  }
];

@NgModule({
  declarations: [
    CustomRegionsComponent,
    CustomRegionListComponent,
    CustomRegionRowComponent,
    CreateCustomRegionFormComponent,
    EditCustomRegionFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    DropdownActionsModule,
    FormsModule,
    EtopMaterialModule,
    EtopPipesModule,
    SideSliderModule,
    EtopCommonModule,
    NotPermissionModule,
    AuthenticateModule,
    MaterialModule
  ]
})
export class CustomRegionsModule {
}
