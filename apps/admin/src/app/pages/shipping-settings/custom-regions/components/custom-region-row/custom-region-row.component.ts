import { Component, Input, OnInit } from '@angular/core';
import { CustomRegion } from 'libs/models/Location';

@Component({
  selector: '[admin-custom-region-row]',
  templateUrl: './custom-region-row.component.html',
  styleUrls: ['./custom-region-row.component.scss']
})
export class CustomRegionRowComponent implements OnInit {
  @Input() customRegion = new CustomRegion({});
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
