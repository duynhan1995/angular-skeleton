import { Component, OnInit } from '@angular/core';
import { CustomRegion } from 'libs/models/Location';
import { BaseComponent } from '@etop/core';
import {FormBuilder} from "@angular/forms";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'admin-create-custom-region-form',
  templateUrl: './create-custom-region-form.component.html',
  styleUrls: ['./create-custom-region-form.component.scss']
})
export class CreateCustomRegionFormComponent extends BaseComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ) {
    super();
  }

  loading = false;

  provincesList$ = this.locationQuery.select(state => state.provincesList);

  customRegionForm = this.fb.group({
    name: '',
    description: '',
    province_codes: [],
    selectedProvinceCode: ''
  });

  private static validateCustomRegion(region: CustomRegion) {
    const { name, province_codes } = region;
    if (!name || !name.trim()) {
      toastr.error('Chưa nhập tên vùng');
      return false;
    }

    if (!province_codes || !province_codes.length) {
      toastr.error('Chưa chọn tỉnh');
      return false;
    }

    return true;
  }

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  ngOnInit() {
    this.customRegionForm.controls['selectedProvinceCode'].valueChanges
      .subscribe(code => {
        if (!code) {
          return;
        }
        const currentProvinceCodes = this.customRegionForm.controls['province_codes'].value;
        if (!currentProvinceCodes) {
          return this.customRegionForm.patchValue({
            province_codes: [code]
          });
        }
        if (currentProvinceCodes.includes(code)) {
          return;
        }
        currentProvinceCodes.push(code);
        this.customRegionForm.patchValue({
          province_codes: currentProvinceCodes
        });
      });
  }

  getProvinceName(code: string) {
    return this.locationQuery.getProvince(code)?.filter_name;
  }

  removeProvince(index: number) {
    this.customRegionForm.controls['province_codes'].value.splice(index, 1);
  }

  async createCustomRegion() {
    this.loading = true;
    try {
      const rawCustomRegionData = this.customRegionForm.getRawValue();
      const valid = CreateCustomRegionFormComponent.validateCustomRegion(rawCustomRegionData);
      if (!valid) {
        return this.loading = false;
      }

      const { name, description, province_codes } = rawCustomRegionData;
      const body = {
        name,
        description,
        province_codes
      };
      await this.shippingSettingsService.createCustomRegion(body);
      toastr.success('Tạo vùng tự định nghĩa thành công.');
    } catch(e) {
      toastr.error('Tạo vùng tự định nghĩa không thành công.', e.code ? (e.message || e.msg) : '');
      debug.error('ERROR in createCustomRegion', e);
    }
    this.loading = false;
  }

}
