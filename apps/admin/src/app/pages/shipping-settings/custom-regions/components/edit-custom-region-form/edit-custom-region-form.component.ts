import { Component, OnInit } from '@angular/core';
import {CustomRegion} from 'libs/models/Location';
import { BaseComponent } from '@etop/core';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'admin-edit-custom-region-form',
  templateUrl: './edit-custom-region-form.component.html',
  styleUrls: ['./edit-custom-region-form.component.scss']
})
export class EditCustomRegionFormComponent extends BaseComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ) {
    super();
  }

  loading = false;

  provincesList$ = this.locationQuery.select(state => state.provincesList);

  activeCustomRegion$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeCustomRegion),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  customRegionForm = this.fb.group({
    name: '',
    description: '',
    province_codes: [],
    selectedProvinceCode: ''
  });

  private static validateCustomRegion(region: CustomRegion) {
    const { name, province_codes } = region;
    if (!name || !name.trim()) {
      toastr.error('Chưa nhập tên vùng');
      return false;
    }

    if (!province_codes || !province_codes.length) {
      toastr.error('Chưa chọn tỉnh');
      return false;
    }

    return true;
  }

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  ngOnInit() {
    this.customRegionForm.controls['selectedProvinceCode'].valueChanges
      .subscribe(code => {
        if (!code) {
          return;
        }
        const currentProvinceCodes = this.customRegionForm.controls['province_codes'].value;
        if (currentProvinceCodes.includes(code)) {
          return;
        }
        currentProvinceCodes.push(code);
        this.customRegionForm.patchValue({
          province_codes: currentProvinceCodes
        });
      });

    this.getFormDataStore();
    this.activeCustomRegion$.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getFormDataStore();
      });
  }

  getFormDataStore() {
    const _customRegion = this.shippingSettingsStore.snapshot.activeCustomRegion;
    if (!_customRegion) { return this.customRegionForm.reset(); }

    const {name, description, province_codes} = _customRegion;

    this.customRegionForm.patchValue({
      name, description, province_codes,
      selectedProvinceCode: ''
    }, {emitEvent: false});
  }

  getProvinceName(code: string) {
    return this.locationQuery.getProvince(code)?.filter_name;
  }

  removeProvince(index: number) {
    this.customRegionForm.controls['province_codes'].value.splice(index, 1);
  }

  async updateCustomRegion() {
    this.loading = true;
    try {
      const rawCustomRegionData = this.customRegionForm.getRawValue();
      const valid = EditCustomRegionFormComponent.validateCustomRegion(rawCustomRegionData);
      if (!valid) {
        return this.loading = false;
      }

      const { name, description, province_codes } = rawCustomRegionData;
      const body = {
        id: this.shippingSettingsStore.snapshot.activeCustomRegion.id,
        name,
        description,
        province_codes
      };
      await this.shippingSettingsService.updateCustomRegion(body);
      toastr.success('Cập nhật vùng tự định nghĩa thành công.');
    } catch(e) {
      toastr.error('Cập nhật vùng tự định nghĩa không thành công.', e.code ? (e.message || e.msg) : '');
      debug.error('ERROR in updateCustomRegion', e);
    }
    this.loading = false;
  }

}
