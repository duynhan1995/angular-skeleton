import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import { CustomRegion } from 'libs/models/Location';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { AdminLocationApi } from '@etop/api/admin/admin-location.api';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";

@Component({
  selector: 'admin-custom-region-list',
  templateUrl: './custom-region-list.component.html',
  styleUrls: ['./custom-region-list.component.scss']
})
export class CustomRegionListComponent extends PageBaseComponent  implements OnInit, OnDestroy {
  @ViewChild('customRegionTable', { static: true }) customRegionTable: EtopTableComponent;
  @ViewChild('customRegionSlider', { static: true }) customRegionSlider: SideSliderComponent;

  onNewCustomRegion = false;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: 'Xoá vùng',
      cssClass: 'text-danger',
      onClick: () => this.deleteCustomRegion(),
      permissions: ['admin/custom_region:delete']
    }
  ];

  customRegionsLoading$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.customRegionsLoading),
    distinctUntilChanged()
  );
  customRegionsList$ = this.shippingSettingsStore.state$.pipe(map(s => s?.customRegions));
  activeCustomRegion$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeCustomRegion),
    distinctUntilChanged((prev, next) => prev?.id == next?.id)
  );

  constructor(
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private dialog: DialogControllerService,
    private api: AdminLocationApi,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ) {
    super();
  }

  get showPaging() {
    return !this.customRegionTable.liteMode && !this.customRegionTable.loading;
  }

  get emptyTitle() {
    return 'Chưa có vùng tự định nghĩa nào';
  }

  get sliderTitle() {
    if (this.onNewCustomRegion) {
      return 'Tạo vùng mới'
    }
    return 'Chi tiết vùng tự định nghĩa';
  }

  async ngOnInit() {
    this.headerController.setActions([
      {
        onClick: () => this.createCustomRegion(),
        title: 'Tạo vùng mới',
        cssClass: 'btn-primary',
        permissions: ['admin/custom_region:create']
      }
    ]);

    this.customRegionsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.customRegionTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.shippingSettingsService.selectCustomRegion(null);
  }

  resetState() {
    this.customRegionTable.toggleLiteMode(false);
    this.customRegionSlider.toggleLiteMode(false);
    this.customRegionTable.toggleNextDisabled(false);
    this.onNewCustomRegion = false;
  }

  getCustomRegions() {
    this.shippingSettingsService.getCustomRegions()?.then();
  }

  loadPage({}) {
    this.getCustomRegions();
  }

  detail(customRegion: CustomRegion) {
    this.onNewCustomRegion = false;
    this.shippingSettingsService.selectCustomRegion(customRegion);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.shippingSettingsService.selectCustomRegion(null);
    this.onNewCustomRegion = false;
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.shippingSettingsStore.snapshot.activeCustomRegion || this.onNewCustomRegion;
    this.customRegionTable.toggleLiteMode(selected);
    this.customRegionSlider.toggleLiteMode(selected);
  }

  createCustomRegion() {
    this.shippingSettingsService.selectCustomRegion(null);
    this.onNewCustomRegion = true;
    this._checkSelectMode();
  }

  deleteCustomRegion() {
    const region = this.shippingSettingsStore.snapshot.activeCustomRegion;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá vùng tự định nghĩa`,
      body: `
        <div>Bạn có chắc muốn xoá vùng tự định nghĩa <strong>${region.name}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.api.deleteCustomRegion(region.id);
          toastr.success('Xoá vùng tự định nghĩa thành công.');
          modal.close().then();
          this.loadPage({});
        } catch (e) {
          debug.error('ERROR in deleting CustomRegion', e);
          toastr.error(`Xoá vùng tự định nghĩa không thành công.`, e.code ? (e.message || e.msg) : '');
        }
      }
    });
    modal.show().then();
  }

}
