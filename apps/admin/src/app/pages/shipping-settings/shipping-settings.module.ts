import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ShippingSettingsComponent } from 'apps/admin/src/app/pages/shipping-settings/shipping-settings.component';
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";

const routes: Routes = [
  {
    path: '',
    component: ShippingSettingsComponent,
    children: [
      {
        path: 'connections',
        loadChildren: () =>
          import('apps/admin/src/app/pages/shipping-settings/connections/connections.module').then(m => m.ConnectionsModule)
      },
      {
        path: 'services',
        loadChildren: () =>
          import('apps/admin/src/app/pages/shipping-settings/shipment-services/shipment-services.module').then(m => m.ShipmentServicesModule)
      },
      {
        path: 'price-lists',
        loadChildren: () =>
          import('apps/admin/src/app/pages/shipping-settings/shipment-price-lists/shipment-price-lists.module').then(m => m.ShipmentPriceListsModule)
      },
      {
        path: 'promotions',
        loadChildren: () =>
          import('apps/admin/src/app/pages/shipping-settings/promotions/promotions.module').then(m => m.PromotionsModule)
      },
      {
        path: 'custom-regions',
        loadChildren: () =>
          import('apps/admin/src/app/pages/shipping-settings/custom-regions/custom-regions.module').then(m => m.CustomRegionsModule)
      },
      {
        path: '**',
        redirectTo: 'connections'
      }
    ]
  }
];

@NgModule({
  declarations: [
    ShippingSettingsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    ShippingSettingsService,
    ShippingSettingsStore
  ]
})
export class ShippingSettingsModule { }
