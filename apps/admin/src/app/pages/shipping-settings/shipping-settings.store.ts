import { Injectable } from '@angular/core';
import {
  AvailableLocation,
  ShipmentPrice,
  ShipmentPriceList,
  ShipmentService, ShippingLocationType
} from "libs/models/admin/ShipmentPrice";
import {AStore} from "apps/core/src/interfaces/AStore";
import {Connection} from "libs/models/Connection";
import {CustomRegion} from "libs/models/Location";

export enum HeaderTab {
  connections = 'connections',
  services = 'services',
  'price-lists' = 'price-lists',
  'promotions' = 'promotions',
  'custom-regions' = 'custom-regions'
}

export interface ShipmentPriceData {
  headerTab: HeaderTab;

  shipmentServicesLoading: boolean;
  shipmentServices: ShipmentService[];
  activeShipmentService: ShipmentService;
  activeShipmentServiceAvailablePick: AvailableLocation;
  activeShipmentServiceAvailableDeliver: AvailableLocation;

  connectionsLoading: boolean;
  connections: Connection[];
  activeConnection: Connection;

  shipmentPriceListsLoading: boolean;
  shipmentPriceLists: ShipmentPriceList[];
  activeShipmentPriceList: ShipmentPriceList;

  shipmentPrices: ShipmentPrice[];
  activeShipmentPrices: ShipmentPrice[];

  customRegionsLoading: boolean;
  customRegions: CustomRegion[];
  activeCustomRegion: CustomRegion;
}

@Injectable()
export class ShippingSettingsStore extends AStore<ShipmentPriceData> {

  initState = {
    headerTab: HeaderTab.connections,

    shipmentServicesLoading: true,
    shipmentServices: [],
    activeShipmentService: null,
    activeShipmentServiceAvailablePick: {shipping_location_type: ShippingLocationType.pick},
    activeShipmentServiceAvailableDeliver: {shipping_location_type: ShippingLocationType.deliver},

    connectionsLoading: true,
    connections: [],
    activeConnection: null,

    shipmentPriceListsLoading: true,
    shipmentPriceLists: [],
    activeShipmentPriceList: null,

    shipmentPrices: [],
    activeShipmentPrices: [],

    customRegionsLoading: true,
    customRegions: [],
    activeCustomRegion: null
  };

  constructor() {
    super();
  }

  changeHeaderTab(headerTab: HeaderTab) {
    if (headerTab && !Object.values(HeaderTab).includes(headerTab)) {
      this.setState({headerTab: HeaderTab.connections});
      return;
    }
    this.setState({ headerTab });
  }

  //SECTION: Connections
  setConnectionsLoading(loading: boolean) {
    this.setState({connectionsLoading: loading});
  }

  setConnections(connections: Connection[]) {
    this.setState({connections});
  }

  selectConnection(activeConnection: Connection) {
    this.setState({activeConnection});
  }

  //SECTION: ShipmentServices
  setShipmentServicesLoading(loading: boolean) {
    this.setState({shipmentServicesLoading: loading});
  }

  setShipmentServices(shipmentServices: ShipmentService[]) {
    shipmentServices = shipmentServices.map(service => {
      const connection = this.snapshot.connections
        .find(conn => conn.id == service.connection_id);

      service = {
        ...service,
        provider_logo: connection && connection.provider_logo || 'assets/images/placeholder_medium.png',
        provider_name: connection && connection.name
      };
      return service;
    });
    this.setState({ shipmentServices });
  }

  selectShipmentService(activeShipmentService: ShipmentService) {
    this.setState({ activeShipmentService });
    this.setState({ shipmentPrices: [] });
  }

  setActiveShipmentServiceAvailablePick(availablePick: AvailableLocation) {
    this.setState({activeShipmentServiceAvailablePick: availablePick});
  }

  setActiveShipmentServiceAvailableDeliver(availableDeliver: AvailableLocation) {
    this.setState({activeShipmentServiceAvailableDeliver: availableDeliver});
  }

  //SECTION: ShipmentPriceLists
  setShipmentPriceListsLoading(loading: boolean) {
    this.setState({shipmentPriceListsLoading: loading});
  }

  setShipmentPriceLists(shipmentPriceLists: ShipmentPriceList[]) {
    shipmentPriceLists = shipmentPriceLists.map(priceList => {
      const connection = this.snapshot.connections
        .find(conn => conn.id == priceList.connection_id);

      priceList = {
        ...priceList,
        provider_logo: connection && connection.provider_logo || 'assets/images/placeholder_medium.png',
        provider_name: connection && connection.name,
      };
      return priceList;
    });
    this.setState({ shipmentPriceLists });
  }

  selectShipmentPriceList(activeShipmentPriceList: ShipmentPriceList) {
    this.setState({ activeShipmentPriceList });
    this.setState({ shipmentPrices: [] });
  }

  //SECTION: ShipmentPrices
  addShipmentPrices(shipmentPrices: ShipmentPrice[]) {
    const _shipmentPrices = this.snapshot.shipmentPrices;
    _shipmentPrices.push(...shipmentPrices);
    this.setState({ shipmentPrices: _shipmentPrices });
  }

  selectShipmentPrices(activeShipmentPrices: ShipmentPrice[]) {
    this.setState({ activeShipmentPrices });
  }

  //SECTION: CustomRegions
  setCustomRegionsLoading(loading: boolean) {
    this.setState({customRegionsLoading: loading});
  }

  setCustomRegions(customRegions: CustomRegion[]) {
    this.setState({ customRegions });
  }

  selectCustomRegion(activeCustomRegion: CustomRegion) {
    this.setState({activeCustomRegion});
  }

}
