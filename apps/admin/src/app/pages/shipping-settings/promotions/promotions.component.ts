import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {AuthenticateStore} from "@etop/core";
import {ShipmentPriceListPromotionService} from "@etop/state/admin/shipment-price-list-promotion";

@Component({
  selector: 'admin-promotions',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-promotion-list></admin-promotion-list>
    </ng-template>`
})
export class PromotionsComponent extends PageBaseComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private service: ShipmentPriceListPromotionService
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/shipment_price_list_promotion:view');
  }

  ngOnInit() {
    this.service.prepareData().then();
  }

}
