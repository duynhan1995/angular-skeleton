import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PromotionsComponent} from "apps/admin/src/app/pages/shipping-settings/promotions/promotions.component";
import { PromotionListComponent } from './promotion-list/promotion-list.component';
import { PromotionRowComponent } from './component/promotion-row/promotion-row.component';
import {
    EtopCommonModule,
    EtopMaterialModule,
    EtopPipesModule,
    MaterialModule,
    NotPermissionModule,
    SideSliderModule
} from "@etop/shared";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DropdownActionsModule} from "apps/shared/src/components/dropdown-actions/dropdown-actions.module";
import { CreatePromotionFormComponent } from './component/create-promotion-form/create-promotion-form.component';
import { EditPromotionFormComponent } from './component/edit-promotion-form/edit-promotion-form.component';
import {RouterModule, Routes} from "@angular/router";
import {AuthenticateModule} from "@etop/core";

const routes: Routes = [
  {
    path: '',
    component: PromotionsComponent
  }
]

@NgModule({
  declarations: [
    PromotionsComponent,
    PromotionListComponent,
    PromotionRowComponent,
    CreatePromotionFormComponent,
    EditPromotionFormComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule,
    FormsModule,
    EtopCommonModule,
    SideSliderModule,
    DropdownActionsModule,
    EtopPipesModule,
    ReactiveFormsModule,
    EtopMaterialModule,
    NotPermissionModule,
    AuthenticateModule
  ]
})
export class PromotionsModule { }
