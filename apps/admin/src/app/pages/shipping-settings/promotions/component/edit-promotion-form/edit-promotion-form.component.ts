import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {
  ConnectionShipmentPriceList,
  ShipmentPriceListPromotionQuery,
  ShipmentPriceListPromotionService
} from "@etop/state/admin/shipment-price-list-promotion";
import {ShipmentPriceListQuery} from "@etop/state/admin/shipment-price-list";
import {CustomRegionQuery} from "@etop/state/admin/custom-region";
import {ShipmentPriceAPI} from "@etop/api";
import {ShipmentPriceListPromotion} from "libs/models/admin/ShipmentPrice";
import {takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";

@Component({
  selector: 'admin-edit-promotion-form',
  templateUrl: './edit-promotion-form.component.html',
  styleUrls: ['./edit-promotion-form.component.scss']
})
export class EditPromotionFormComponent extends BaseComponent implements OnInit {

  activePromotion$ = this.promotionQuery.selectActive();

  connectionShipmentPriceListsList$ = this.promotionQuery.select(state => state.connectionShipmentPriceLists);
  activeConnectionShipmentPriceList$ = this.promotionQuery.select(
    state => state.activeConnectionShipmentPriceList
  );

  allShipmentPriceLists$ = this.shipmentPriceListQuery.selectAll();
  allCustomRegions$ = this.customRegionQuery.selectAll();

  promotionForm = this.fb.group({
    connection_id: '',
    shipment_price_list_id: '',
    name: '',
    date_from: '',
    date_to: '',
    description: '',
    from_custom_region_ids: [],
    using_price_list_ids: [],
    shop_created_date_from: '',
    shop_created_date_to: '',
    user_created_date_from: '',
    user_created_date_to: '',
    isTurnedOn: false
  });

  loading = false;

  constructor(
    private fb: FormBuilder,
    private promotionQuery: ShipmentPriceListPromotionQuery,
    private promotionService: ShipmentPriceListPromotionService,
    private shipmentPriceListQuery: ShipmentPriceListQuery,
    private customRegionQuery: CustomRegionQuery,
    private dialog: DialogControllerService
  ) {
    super();
  }

  private static specialConnectionNameDisplay(group: ConnectionShipmentPriceList) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${group.connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${group.connection.name.toUpperCase()}</div>
      </div>`;
  }

  private static validatePromotionRequest(request: ShipmentPriceAPI.UpdateShipmentPriceListPromotionRequest): boolean {
    const {connection_id, shipment_price_list_id, name, date_from, date_to} = request;
    if (!connection_id) {
      toastr.error('Vui lòng chọn nhà vận chuyển!');
      return false
    }
    if (!shipment_price_list_id) {
      toastr.error('Vui lòng chọn bảng giá!');
      return false
    }
    if (!name || !name.trim()) {
      toastr.error('Vui lòng nhập tên cho bảng giá khuyến mãi!');
      return false
    }
    if (!date_from) {
      toastr.error('Vui lòng chọn ngày bắt đầu!');
      return false
    }
    if (!date_to) {
      toastr.error('Vui lòng chọn ngày kết thúc!');
      return false
    }
    return true;
  }

  displayMap = option => option && EditPromotionFormComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  connectionShipmentPriceListDisplayMap = option => option && option.name || null;
  shipmentPriceListDisplayMap = option => option &&
    `<strong>${option?.connection?.name?.toUpperCase()}</strong> - ${option.name}` || null;
  shipmentPriceListValueMap = option => option && option.id || null;

  customRegionDisplayMap = option => option && option.name || null;
  customRegionValueMap = option => option && option.id || null;

  ngOnInit() {
    this.promotionForm.controls['connection_id'].valueChanges.subscribe(value => {
      const _group = this.promotionQuery.getValue().connectionShipmentPriceLists
        .find(gr => gr.id == value);
      this.promotionService.selectConnectionShipmentPriceList(_group);
      this.promotionForm.patchValue({
        shipment_price_list_id: null
      });
    });

    this.promotionForm.controls['isTurnedOn'].valueChanges.subscribe(_ => {
      this.updatePromotionStatus().then();
    });

    this.setupFormControls(this.promotionQuery.getActive());
    this.activePromotion$.pipe(takeUntil(this.destroy$))
      .subscribe(promo => {
        this.setupFormControls(promo);
      });
  }

  private setupFormControls(promotion: ShipmentPriceListPromotion) {
    if (!promotion) { return; }
    const {
      connection_id, shipment_price_list_id,
      name, date_from, date_to, description,
      applied_rules, status
    } = promotion;
    this.promotionForm.patchValue({
      connection_id: connection_id,
      shipment_price_list_id: shipment_price_list_id,
      name: name,
      date_from: date_from,
      date_to: date_to,
      description: description,
      from_custom_region_ids: applied_rules.from_custom_region_ids,
      using_price_list_ids: applied_rules.using_price_list_ids,
      shop_created_date_from: applied_rules.shop_created_date.from,
      shop_created_date_to: applied_rules.shop_created_date.to,
      user_created_date_from: applied_rules.user_created_date.from,
      user_created_date_to: applied_rules.user_created_date.to,
      isTurnedOn: status == 'P'
    }, {emitEvent: false});
  }

  async updatePromotionStatus() {
    const promo = this.promotionQuery.getActive();
    const modal = this.dialog.createConfirmDialog({
      title: `${promo?.status == 'P' ? 'Tắt' : 'Bật'} bảng giá khuyến mãi`,
      body: `
<div>Bạn có chắc muốn ${promo?.status == 'P' ? 'tắt' : 'bật'} bảng giá khuyến mãi
  <strong class="text-primary">${promo?.name}</strong>?
</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onCancel: () => {
        this.promotionForm.patchValue({
          isTurnedOn: promo?.status == 'P'
        }, {emitEvent: false});
      },
      onConfirm: async () => {
        try {
          const rawData = this.promotionForm.getRawValue();
          await this.promotionService.updateShipmentPriceListPromotionStatus(
            promo.id, rawData.isTurnedOn ? 'P' : 'N'
          );
          modal.close().then();
        } catch (e) {
          debug.error('ERROR in updatePromotionStatus', e);
        }
      }
    });
    modal.show().then();
  }

  async updatePromotion() {
    const rawData = this.promotionForm.getRawValue();
    const {
      connection_id, shipment_price_list_id,
      name, description, date_from, date_to,
      from_custom_region_ids, using_price_list_ids,
      shop_created_date_from, shop_created_date_to,
      user_created_date_from, user_created_date_to
    } = rawData;

    const request: ShipmentPriceAPI.UpdateShipmentPriceListPromotionRequest = {
      id: this.promotionQuery.getActive().id,
      name, description,
      connection_id, shipment_price_list_id,
      date_from, date_to,
      applied_rules: {
        from_custom_region_ids,
        using_price_list_ids,
        shop_created_date: {},
        user_created_date: {}
      }
    };

    if (!EditPromotionFormComponent.validatePromotionRequest(request)) {
      return;
    }

    if (shop_created_date_from) {
      request.applied_rules.shop_created_date.from = shop_created_date_from;
    }
    if (shop_created_date_to) {
      request.applied_rules.shop_created_date.to = shop_created_date_to;
    }
    if (user_created_date_from) {
      request.applied_rules.user_created_date.from = user_created_date_from;
    }
    if (user_created_date_to) {
      request.applied_rules.user_created_date.to = user_created_date_to;
    }

    this.loading = true;
    await this.promotionService.updateShipmentPriceListPromotion(request);
    this.loading = false;
  }

}
