import {Component, Input, OnInit} from '@angular/core';
import {ShipmentPriceListPromotion} from "libs/models/admin/ShipmentPrice";
import {ShipmentPriceListPromotionQuery} from "@etop/state/admin/shipment-price-list-promotion";

@Component({
  selector: '[admin-promotion-row]',
  templateUrl: './promotion-row.component.html',
  styleUrls: ['./promotion-row.component.scss']
})
export class PromotionRowComponent implements OnInit {
  @Input() index: number;
  @Input() promotion: ShipmentPriceListPromotion;
  @Input() liteMode = false;

  offset$ = this.promotionQuery.select(state => state.ui.paging.offset);

  constructor(
    private promotionQuery: ShipmentPriceListPromotionQuery
  ) { }

  ngOnInit() {
  }

  get promotionStatus() {
    return this.promotion.status == 'N' ? 'Z' : this.promotion.status;
  }

}
