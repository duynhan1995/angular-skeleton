import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {
  ConnectionShipmentPriceList,
  ShipmentPriceListPromotionQuery,
  ShipmentPriceListPromotionService
} from "@etop/state/admin/shipment-price-list-promotion";
import {ShipmentPriceListQuery} from "@etop/state/admin/shipment-price-list";
import {CustomRegionQuery} from "@etop/state/admin/custom-region";
import {ShipmentPriceAPI} from "@etop/api";

@Component({
  selector: 'admin-create-promotion-form',
  templateUrl: './create-promotion-form.component.html',
  styleUrls: ['./create-promotion-form.component.scss']
})
export class CreatePromotionFormComponent implements OnInit {

  connectionShipmentPriceListsList$ = this.promotionQuery.select(state => state.connectionShipmentPriceLists);
  activeConnectionShipmentPriceList$ = this.promotionQuery.select(
    state => state.activeConnectionShipmentPriceList
  );

  allShipmentPriceLists$ = this.shipmentPriceListQuery.selectAll();
  allCustomRegions$ = this.customRegionQuery.selectAll();

  promotionForm = this.fb.group({
    connection_id: '',
    shipment_price_list_id: '',
    name: '',
    date_from: '',
    date_to: '',
    description: '',
    from_custom_region_ids: [],
    using_price_list_ids: [],
    shop_created_date_from: '',
    shop_created_date_to: '',
    user_created_date_from: '',
    user_created_date_to: ''
  });

  loading = false;

  constructor(
    private fb: FormBuilder,
    private promotionQuery: ShipmentPriceListPromotionQuery,
    private promotionService: ShipmentPriceListPromotionService,
    private shipmentPriceListQuery: ShipmentPriceListQuery,
    private customRegionQuery: CustomRegionQuery
  ) { }

  private static specialConnectionNameDisplay(group: ConnectionShipmentPriceList) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${group.connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${group.connection.name.toUpperCase()}</div>
      </div>`;
  }

  private static validatePromotionRequest(request: ShipmentPriceAPI.CreateShipmentPriceListPromotionRequest): boolean {
    const {connection_id, shipment_price_list_id, name, date_from, date_to} = request;
    if (!connection_id) {
      toastr.error('Vui lòng chọn nhà vận chuyển!');
      return false
    }
    if (!shipment_price_list_id) {
      toastr.error('Vui lòng chọn bảng giá!');
      return false
    }
    if (!name || !name.trim()) {
      toastr.error('Vui lòng nhập tên cho bảng giá khuyến mãi!');
      return false
    }
    if (!date_from) {
      toastr.error('Vui lòng chọn ngày bắt đầu!');
      return false
    }
    if (!date_to) {
      toastr.error('Vui lòng chọn ngày kết thúc!');
      return false
    }
    return true;
  }

  displayMap = option => option && CreatePromotionFormComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  connectionShipmentPriceListDisplayMap = option => option && option.name || null;
  shipmentPriceListDisplayMap = option => option &&
    `<strong>${option?.connection?.name?.toUpperCase()}</strong> - ${option.name}` || null;
  shipmentPriceListValueMap = option => option && option.id || null;

  customRegionDisplayMap = option => option && option.name || null;
  customRegionValueMap = option => option && option.id || null;

  ngOnInit() {
    this.promotionForm.controls['connection_id'].valueChanges.subscribe(value => {
      const _group = this.promotionQuery.getValue().connectionShipmentPriceLists
        .find(gr => gr.id == value);
      this.promotionService.selectConnectionShipmentPriceList(_group);
      this.promotionForm.patchValue({
        shipment_price_list_id: null
      });
    });
    this.promotionService.selectConnectionShipmentPriceList(null);
  }

  async createPromotion() {
    const rawData = this.promotionForm.getRawValue();
    const {
      connection_id, shipment_price_list_id,
      name, description, date_from, date_to,
      from_custom_region_ids, using_price_list_ids,
      shop_created_date_from, shop_created_date_to,
      user_created_date_from, user_created_date_to
    } = rawData;

    const request: ShipmentPriceAPI.CreateShipmentPriceListPromotionRequest = {
      name, description,
      connection_id, shipment_price_list_id,
      date_from, date_to,
      applied_rules: {
        from_custom_region_ids,
        using_price_list_ids,
        shop_created_date: {},
        user_created_date: {}
      }
    }

    if (!CreatePromotionFormComponent.validatePromotionRequest(request)) {
      return;
    }

    if (shop_created_date_from) {
      request.applied_rules.shop_created_date.from = shop_created_date_from;
    }
    if (shop_created_date_to) {
      request.applied_rules.shop_created_date.to = shop_created_date_to;
    }
    if (user_created_date_from) {
      request.applied_rules.user_created_date.from = user_created_date_from;
    }
    if (user_created_date_to) {
      request.applied_rules.user_created_date.to = user_created_date_to;
    }

    this.loading = true;
    await this.promotionService.createShipmentPriceListPromotion(request);
    this.loading = false;
  }

}
