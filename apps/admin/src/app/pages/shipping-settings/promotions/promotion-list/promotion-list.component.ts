import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EtopTableComponent, SideSliderComponent} from "@etop/shared";
import {takeUntil} from "rxjs/operators";
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {
  ShipmentPriceListPromotionQuery,
  ShipmentPriceListPromotionService
} from "@etop/state/admin/shipment-price-list-promotion";
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {ShipmentPriceListPromotion} from "libs/models/admin/ShipmentPrice";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {ShipmentPriceApi} from "@etop/api";

@Component({
  selector: 'admin-promotion-list',
  templateUrl: './promotion-list.component.html',
  styleUrls: ['./promotion-list.component.scss']
})
export class PromotionListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('promotionTable', { static: true }) promotionTable: EtopTableComponent;
  @ViewChild('promotionSlider', { static: true }) promotionSlider: SideSliderComponent;

  connectionsGroups$ = this.promotionQuery.select('connectionShipmentPriceLists');

  promotionsLoading$ = this.promotionQuery.selectLoading();
  isLastPage$ = this.promotionQuery.select(state => state.ui.isLastPage);
  promotionsList$ = this.promotionQuery.selectAll();
  activePromotion$ = this.promotionQuery.selectActive();

  onNewPromotion = false;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: 'Ưu tiên',
      onClick: () => this.prioritizePromotion(),
      permissions: ['admin/shipment_price_list_promotion:update']
    },
    {
      title: 'Xoá',
      cssClass: 'text-danger',
      onClick: () => this.deletePromotion(),
      permissions: ['admin/shipment_price_list_promotion:delete']
    }
  ];

  filterConnections = [];
  filterConnectionId: string = null;

  constructor(
    private dialog: DialogControllerService,
    private ref: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private promotionQuery: ShipmentPriceListPromotionQuery,
    private promotionService: ShipmentPriceListPromotionService,
    private shipmentPriceApi: ShipmentPriceApi
  ) {
    super();
  }

  get showPaging() {
    return !this.promotionTable.liteMode && !this.promotionTable.loading;
  }

  get emptyTitle() {
    return 'Chưa có bảng giá khuyến mãi nào';
  }

  get sliderTitle() {
    if (this.onNewPromotion) {
      return 'Tạo bảng giá khuyến mãi mới'
    }
    return 'Chi tiết bảng giá khuyến mãi';
  }

  ngOnInit() {
    this.headerController.setActions([
      {
        onClick: () => this.createPromotion(),
        title: 'Tạo bảng giá khuyến mãi',
        cssClass: 'btn btn-primary',
        permissions: ['admin/shipment_price_list_promotion:create']
      }
    ]);

    this.promotionsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        if (this.promotionTable) {
          this.promotionTable.loading = loading;
        }
        this.ref.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.promotionTable.toggleNextDisabled(true);
          this.promotionTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả bảng giá khuyến mãi.');
        }
      });

    this.activePromotion$.pipe(takeUntil(this.destroy$))
      .subscribe(promo => {
        this.dropdownActions[0].hidden = promo?.priority_point == this.promotionQuery.getValue().highestPriorityPoint;
      });

    this.filterConnections = [{name: 'Tất cả', id: null}].concat(
      this.promotionQuery.getValue().connectionShipmentPriceLists.map(gr => {
        return {
          name: gr.connection.name,
          id: gr.connection.id
        }
      }));
    this.connectionsGroups$.pipe(takeUntil(this.destroy$))
      .subscribe(groups => {
        this.filterConnections = [{name: 'Tất cả', id: null}].concat(groups.map(gr => {
          return {
            name: gr.connection.name,
            id: gr.connection.id
          }
        }));
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.promotionService.selectShipmentPriceListPromotion(null);
    this.promotionService.setConnectionId(null);
  }

  resetState() {
    this.promotionTable.toggleLiteMode(false);
    this.promotionSlider.toggleLiteMode(false);
    this.promotionTable.toggleNextDisabled(false);
    this.onNewPromotion = false;
  }

  filterByConnectionId() {
    this.promotionService.setConnectionId(this.filterConnectionId);
    this.promotionTable.resetPagination();
  }

  getPromotions() {
    const connectionsGroups = this.promotionQuery.getValue().connectionShipmentPriceLists;
    if (connectionsGroups?.length) {
      this.promotionService.getShipmentPriceListPromotions(true).then();
    } else {
      this.connectionsGroups$.pipe(takeUntil(this.destroy$))
        .subscribe(groups => {
          if (groups?.length) {
            this.promotionService.getShipmentPriceListPromotions(true).then();
          }
        });
    }
  }

  loadPage({ page, perpage }) {
    this.promotionService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this.getPromotions();
  }

  detail(promotion: ShipmentPriceListPromotion) {
    this.onNewPromotion = false;
    this.promotionService.selectShipmentPriceListPromotion(promotion.id);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onNewPromotion = false;
    this.promotionService.selectShipmentPriceListPromotion(null);

    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.promotionQuery.getActive() || this.onNewPromotion;
    this.promotionTable.toggleLiteMode(selected);
    this.promotionSlider.toggleLiteMode(selected);
  }

  createPromotion() {
    this.promotionService.selectShipmentPriceListPromotion(null);
    this.onNewPromotion = true;
    this._checkSelectMode();
  }

  deletePromotion() {
    const promotion = this.promotionQuery.getActive();
    const dialog = this.dialog.createConfirmDialog({
      title: `Xoá bảng giá khuyến mãi`,
      body: `
<div>Bạn có chắc muốn xoá bảng giá khuyến mãi <strong>${promotion.name}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.shipmentPriceApi.deleteShipmentPriceListPromotion(promotion.id);
          dialog.close().then();
          this.promotionTable.resetPagination();
        } catch (e) {
          debug.error('ERROR in deletePromotion', e);
          toastr.error(`Xoá bảng giá khuyến mãi không thành công.`, e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  prioritizePromotion() {
    const promotion = this.promotionQuery.getActive();
    const dialog = this.dialog.createConfirmDialog({
      title: `Ưu tiên bảng giá khuyến mãi`,
      body: `
<div>Bạn có chắc muốn ưu tiên bảng giá khuyến mãi <strong>${promotion.name}</strong>?</div>
<div class="mt-3">Bảng giá sẽ có độ ưu tiên cao nhất!</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.promotionService.prioritizeShipmentPriceListPromotion(promotion.id);
          dialog.close().then();
        } catch (e) {
          debug.error('ERROR in prioritizePromotion', e);
        }
      }
    });
    dialog.show().then();
  }

}
