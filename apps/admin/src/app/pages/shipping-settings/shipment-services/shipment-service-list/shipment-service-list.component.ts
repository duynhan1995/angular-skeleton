import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ShipmentService } from 'libs/models/admin/ShipmentPrice';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import {ShipmentPriceAPI, ShipmentPriceApi} from '@etop/api';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";

@Component({
  selector: 'admin-shipment-service-list',
  templateUrl: './shipment-service-list.component.html',
  styleUrls: ['./shipment-service-list.component.scss']
})
export class ShipmentServiceListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('shipmentServiceTable', { static: true }) shipmentServiceTable: EtopTableComponent;
  @ViewChild('shipmentServiceSlider', { static: true }) shipmentServiceSlider: SideSliderComponent;

  connections$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  shipmentServicesLoading$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.shipmentServicesLoading),
    distinctUntilChanged()
  );
  shipmentServicesList$ = this.shippingSettingsStore.state$.pipe(map(s => s?.shipmentServices));
  activeShipmentService$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => prev?.id == next?.id)
  );

  onNewShipmentService = false;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: 'Xoá gói dịch vụ',
      cssClass: 'text-danger',
      onClick: () => this.deleteShipmentService(),
      permissions: ['admin/shipment_service:delete']
    }
  ];

  tabs = [
    { name: 'Thông tin', value: 'detail_info' },
    { name: 'Địa điểm khả dụng', value: 'available_location' },
    { name: 'Địa điểm chặn', value: 'blacklist_location' }
  ];
  activeTab: 'detail_info' | 'blacklist_location'  | 'available_location' = 'detail_info';

  filterConnections = [];
  filterConnectionId: string = null;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private dialog: DialogControllerService,
    private api: ShipmentPriceApi,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore
  ) {
    super();
  }

  get showPaging() {
    return !this.shipmentServiceTable.liteMode && !this.shipmentServiceTable.loading;
  }

  get emptyTitle() {
    return 'Chưa có gói dịch vụ nào';
  }

  get sliderTitle() {
    if (this.onNewShipmentService) {
      return 'Tạo gói dịch vụ mới'
    }
    return 'Chi tiết gói dịch vụ';
  }

  async ngOnInit() {
    this.headerController.setActions([
      {
        onClick: () => this.createShipmentService(),
        title: 'Tạo gói dịch vụ',
        cssClass: 'btn btn-primary',
        permissions: ['admin/shipment_service:create']
      }
    ]);

    this.shipmentServicesLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        if (this.shipmentServiceTable) {
          this.shipmentServiceTable.loading = loading;
        }
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.filterConnections = [{name: 'Tất cả', id: null}].concat(this.shippingSettingsStore.snapshot.connections.map(conn => {
      return {
        name: conn.name,
        id: conn.id
      }
    }));
    this.connections$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.filterConnections = [{name: 'Tất cả', id: null}].concat(connections.map(conn => {
          return {
            name: conn.name,
            id: conn.id
          }
        }));
      });

  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.shippingSettingsService.selectShipmentService(null);
  }

  resetState() {
    this.shipmentServiceTable.toggleLiteMode(false);
    this.shipmentServiceSlider.toggleLiteMode(false);
    this.shipmentServiceTable.toggleNextDisabled(false);
    this.onNewShipmentService = false;
  }

  filterByConnectionId() {
    if (!this.filterConnectionId) {
      return this.loadPage({});
    }

    return this.getShipmentServices({connection_id: this.filterConnectionId});
  }

  getShipmentServices(filter?: ShipmentPriceAPI.GetShipmentServicesFilter) {
    const connections = this.shippingSettingsStore.snapshot.connections;
    if (connections?.length) {
      this.shippingSettingsService.getShipmentServices(true, filter).then();
    } else {
      this.connections$.pipe(takeUntil(this.destroy$))
        .subscribe(conns => {
          if (conns?.length) {
            this.shippingSettingsService.getShipmentServices(true, filter).then();
          }
        });
    }
  }

  loadPage({}) {
    this.getShipmentServices();
  }

  detail(shipmentService: ShipmentService) {
    this.onNewShipmentService = false;
    this.shippingSettingsService.selectShipmentService(shipmentService);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onNewShipmentService = false;
    this.shippingSettingsService.selectShipmentService(null);

    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.shippingSettingsStore.snapshot.activeShipmentService || this.onNewShipmentService;
    this.shipmentServiceTable.toggleLiteMode(selected);
    this.shipmentServiceSlider.toggleLiteMode(selected);
  }

  changeTab(tab) {
    this.activeTab = tab;
  }

  createShipmentService() {
    this.shippingSettingsService.selectShipmentService(null);
    this.onNewShipmentService = true;
    this._checkSelectMode();
  }

  deleteShipmentService() {
    const service = this.shippingSettingsStore.snapshot.activeShipmentService;
    const modal = this.dialog.createConfirmDialog({
      title: `Xoá gói dịch vụ`,
      body: `
        <div>Bạn có chắc muốn xoá gói dịch vụ <strong>${service.ed_code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.api.deleteShipmentService(service.id);
          toastr.success('Xoá gói dịch vụ thành công.');
          modal.close().then();
          this.loadPage({});
        } catch (e) {
          debug.error('ERROR in deleting ShipmentService', e);
          toastr.error(`Xoá gói dịch vụ không thành công.`, e.code ? (e.message || e.msg) : '');
        }
      }
    });
    modal.show().then();
  }

}
