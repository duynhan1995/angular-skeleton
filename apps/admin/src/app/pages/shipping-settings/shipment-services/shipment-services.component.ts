import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-shipment-services',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <admin-shipment-service-list></admin-shipment-service-list>
    </ng-template>`
})
export class ShipmentServicesComponent extends PageBaseComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/shipment_service:view');
  }

  ngOnInit() {
  }

}
