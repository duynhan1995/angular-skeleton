import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  EtopCommonModule,
  EtopFormsModule,
  EtopMaterialModule,
  EtopPipesModule, MaterialModule, NotPermissionModule,
  SideSliderModule
} from '@etop/shared';
import { ShipmentServiceListComponent } from './shipment-service-list/shipment-service-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ShipmentServiceRowComponent } from './components/shipment-service-row/shipment-service-row.component';
import { CreateShipmentServiceFormComponent } from './components/create-shipment-service-form/create-shipment-service-form.component';
import { EditShipmentServiceFormComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-services/components/edit-shipment-service-form/edit-shipment-service-form.component';
import { FormsModule } from '@angular/forms';
import { ShipmentServiceDetailInfoComponent } from './components/shipment-service-detail-info/shipment-service-detail-info.component';
import { ShipmentServiceBlacklistLocationComponent } from 'apps/admin/src/app/pages/shipping-settings/shipment-services/components/shipment-service-blacklist-location/shipment-service-blacklist-location.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { DragdropListModule } from 'apps/shared/src/components/dragdrop-list/dragdrop-list.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { AdvancedLocationModalComponent } from './components/advanced-location-modal/advanced-location-modal.component';
import {BlacklistProvincesModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/components/blacklist-provinces-modal/blacklist-provinces-modal.component";
import {BlacklistDistrictsModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/components/blacklist-districts-modal/blacklist-districts-modal.component";
import {BlacklistWardsModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/components/blacklist-wards-modal/blacklist-wards-modal.component";
import {ComponentsModule} from "apps/admin/src/app/pages/shipping-settings/components/components.module";
import { ShipmentServiceAvailableLocationComponent } from './components/shipment-service-available-location/shipment-service-available-location.component';
import {AuthenticateModule} from "@etop/core";
import {ShipmentServicesComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/shipment-services.component";

const routes: Routes = [
  {
    path: '',
    component: ShipmentServicesComponent
  }
];

@NgModule({
  declarations: [
    ShipmentServicesComponent,
    ShipmentServiceListComponent,
    ShipmentServiceRowComponent,
    CreateShipmentServiceFormComponent,
    EditShipmentServiceFormComponent,
    ShipmentServiceDetailInfoComponent,
    ShipmentServiceBlacklistLocationComponent,
    AdvancedLocationModalComponent,
    BlacklistProvincesModalComponent,
    BlacklistDistrictsModalComponent,
    BlacklistWardsModalComponent,
    ShipmentServiceAvailableLocationComponent,
  ],
  entryComponents: [
    AdvancedLocationModalComponent,
    BlacklistProvincesModalComponent,
    BlacklistDistrictsModalComponent,
    BlacklistWardsModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    DropdownActionsModule,
    DragdropListModule,
    NgbTooltipModule,
    EtopMaterialModule,
    EtopFormsModule,
    EtopPipesModule,
    EtopCommonModule,
    SideSliderModule,
    MaterialModule,
    ComponentsModule,
    NotPermissionModule,
    AuthenticateModule
  ]
})
export class ShipmentServicesModule { }
