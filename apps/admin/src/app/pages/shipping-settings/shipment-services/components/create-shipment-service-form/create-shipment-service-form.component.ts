import { Component, OnInit } from '@angular/core';
import { ShipmentService } from 'libs/models/admin/ShipmentPrice';
import { Connection } from 'libs/models/Connection';
import { ShipmentPriceAPI } from '@etop/api';
import {distinctUntilChanged, map} from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import {FormBuilder} from "@angular/forms";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {LocationQuery} from "@etop/state/location/location.query";

@Component({
  selector: 'admin-create-shipment-service-form',
  templateUrl: './create-shipment-service-form.component.html',
  styleUrls: ['./create-shipment-service-form.component.scss']
})
export class CreateShipmentServiceFormComponent extends BaseComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore,
  ) {
    super();
  }

  get labelPick() {
    return this.availablePickForm.controls['filter_type'].value == 'exclude' ? 'loại trừ' : 'áp dụng';
  }

  get labelDeliver() {
    return this.availableDeliverForm.controls['filter_type'].value == 'exclude' ? 'loại trừ' : 'áp dụng';
  }

  loading = false;

  filterTypes = [
    {
      name: 'Bao gồm',
      value: 'include'
    },
    {
      name: 'Loại trừ',
      value: 'exclude'
    }
  ];

  regions = [
    {
      name: 'Miền Nam',
      value: 'south'
    },
    {
      name: 'Miền Trung',
      value: 'middle'
    },
    {
      name: 'Miền Bắc',
      value: 'north'
    }
  ];

  provincesList$ = this.locationQuery.select(state => state.provincesList);
  customRegionsList$ = this.shippingSettingsStore.state$.pipe(map(s => s?.customRegions));

  activeShipmentService$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  connectionsList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );
  activeConnection$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeConnection),
    distinctUntilChanged((prev, next) => prev?.id == next?.id)
  );

  shipmentServiceForm = this.fb.group({
    ed_code: '',
    name: '',
    description: '',
    connection_id: '',
    service_ids: []
  });

  availablePickForm = this.fb.group({
    filter_type: '',
    regions: [],
    custom_region_ids: [],
    province_codes: [],
    shipping_location_type: 'pick'
  });

  availableDeliverForm = this.fb.group({
    filter_type: '',
    regions: [],
    custom_region_ids: [],
    province_codes: [],
    shipping_location_type: 'deliver'
  });

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${connection.name.toUpperCase()}</div>
      </div>`;
  }

  private static validShipmentService(service: ShipmentService) {
    const { ed_code, name, connection_id, service_ids } = service;
    if (!ed_code) {
      toastr.error('Chưa nhập mã gói.');
      return false;
    }
    if (!name) {
      toastr.error('Chưa nhập tên gói.');
      return false;
    }
    if (!connection_id) {
      toastr.error('Chưa chọn nhà vận chuyển.');
      return false;
    }
    if (!service_ids || !service_ids.length) {
      toastr.error('Chưa chọn gói dịch vụ từ NVC.');
      return false;
    }
    return true;
  }

  displayMap = option => option && CreateShipmentServiceFormComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  connectionServiceDisplayMap = service => service && `${service.service_id} - ${service.name}` || null;
  connectionServiceValueMap = service => service && service.service_id || null;
  regionDisplayMap = region => region && region.name || null;
  regionValueMap = region => region && region.value || null;

  provinceDisplayMap = province => province && province.name || null;
  provinceValueMap = province => province && province.code || null;

  customRegionDisplayMap = customRegion => customRegion && customRegion.name || null;
  customRegionValueMap = customRegion => customRegion && customRegion.id || null;

  ngOnInit() {
    this.shipmentServiceForm.controls['connection_id'].valueChanges
      .subscribe(connection_id => {
        const _connection = this.shippingSettingsStore.snapshot.connections
          .find(conn => conn.id == connection_id);
        this.shippingSettingsService.selectConnection(_connection);
        this.shipmentServiceForm.patchValue({ service_ids: [] })
      });
  }

  async createShipmentService() {
    this.loading = true;
    try {
      const rawShipmentServiceData = this.shipmentServiceForm.getRawValue();
      const valid = CreateShipmentServiceFormComponent.validShipmentService(rawShipmentServiceData);
      if (!valid) {
        return this.loading = false;
      }

      const availablePick = this.makeAvailablePickRequest();
      const availableDeliver = this.makeAvailableDeliverRequest();
      const { ed_code, name, description, connection_id, service_ids } = rawShipmentServiceData;

      const body: ShipmentPriceAPI.CreateShipmentServiceRequest = {
        ed_code,
        name,
        description,
        connection_id,
        service_ids,
        available_locations: [availablePick, availableDeliver].filter(item => {
          const { province_codes, regions, custom_region_ids, filter_type } = item;
          return !!filter_type && (province_codes?.length + regions?.length + custom_region_ids?.length) > 0;
        })
      };

      await this.shippingSettingsService.createShipmentService(body);
      toastr.success('Tạo gói dịch vụ thành công.');

    } catch(e) {
      toastr.error('Tạo gói dịch vụ không thành công.', e.code ? (e.message || e.msg) : '');
      debug.error('ERROR in createShipmentService', e);
    }
    this.loading = false;
  }

  private makeAvailablePickRequest() {
    const rawAvailablePickData = this.availablePickForm.getRawValue();
    let { custom_region_ids, filter_type, province_codes, regions, shipping_location_type } = rawAvailablePickData;
    return {
      id: null,
      custom_region_ids: custom_region_ids?.length ? custom_region_ids : [],
      filter_type,
      province_codes: province_codes?.length ? province_codes : [],
      regions: regions?.length ? regions : [],
      shipping_location_type
    };
  }

  private makeAvailableDeliverRequest() {
    const rawAvailableDeliverData = this.availableDeliverForm.getRawValue();
    let { custom_region_ids, filter_type, province_codes, regions, shipping_location_type } = rawAvailableDeliverData;
    return {
      id: null,
      custom_region_ids: custom_region_ids?.length ? custom_region_ids : [],
      filter_type,
      province_codes: province_codes?.length ? province_codes : [],
      regions: regions?.length ? regions : [],
      shipping_location_type
    };
  }

}
