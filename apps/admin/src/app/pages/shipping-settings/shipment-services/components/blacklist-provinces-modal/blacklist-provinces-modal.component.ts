import {Component, Input, OnInit} from '@angular/core';
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {Province} from "libs/models/Location";
import {LocationQuery} from "@etop/state/location/location.query";

@Component({
  selector: 'admin-blacklist-provinces',
  templateUrl: './blacklist-provinces-modal.component.html',
  styleUrls: ['./blacklist-provinces-modal.component.scss']
})
export class BlacklistProvincesModalComponent implements OnInit {
  @Input() reason: string;
  @Input() provinceCodes: string[] = [];
  selectedProvinceCode: string;

  provinces: Province[] = [];

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  constructor(
    private modalAction: ModalAction,
    private locationQuery: LocationQuery,
  ) { }

  ngOnInit() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  onSelectProvince() {
    if (!this.selectedProvinceCode) {
      return;
    }
    if (this.provinceCodes.includes(this.selectedProvinceCode)) {
      return;
    }
    this.provinceCodes.push(this.selectedProvinceCode);
  }

  removeProvince(index: number) {
    this.provinceCodes.splice(index, 1);
  }

  getProvinceName(provinceCode: string) {
    return this.locationQuery.getProvince(provinceCode)?.filter_name;
  }

  confirm() {
    if (!this.provinceCodes || !this.provinceCodes.length) {
      return toastr.error('Chưa chọn tỉnh thành.');
    }
    if (!this.reason) {
      return toastr.error('Chưa nhập lý do.');
    }
    this.modalAction.dismiss({
      provinceCodes: this.provinceCodes,
      reason: this.reason
    });
  }

}
