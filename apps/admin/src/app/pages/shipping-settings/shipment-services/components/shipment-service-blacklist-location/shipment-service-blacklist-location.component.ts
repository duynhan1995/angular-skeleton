import { Component, OnInit } from '@angular/core';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {BlacklistProvincesModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/components/blacklist-provinces-modal/blacklist-provinces-modal.component";
import {BlacklistLocation, ShippingLocationType} from "libs/models/admin/ShipmentPrice";
import {UtilService} from "apps/core/src/services/util.service";
import {BlacklistDistrictsModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/components/blacklist-districts-modal/blacklist-districts-modal.component";
import {BlacklistWardsModalComponent} from "apps/admin/src/app/pages/shipping-settings/shipment-services/components/blacklist-wards-modal/blacklist-wards-modal.component";
import {ShipmentPriceApi} from "@etop/api";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'admin-shipment-service-blacklist-location',
  templateUrl: './shipment-service-blacklist-location.component.html',
  styleUrls: ['./shipment-service-blacklist-location.component.scss']
})
export class ShipmentServiceBlacklistLocationComponent extends BaseComponent implements OnInit {

  loading = false;
  updatingProvince = false;
  updatingDistrict = false;
  updatingWard = false;

  blackListLocationPick: {
    blackListProvinces: BlacklistLocation[];
    blackListDistricts: BlacklistLocation[];
    blackListWards: BlacklistLocation[];
  }

  blackListLocationDeliver: {
    blackListProvinces: BlacklistLocation[];
    blackListDistricts: BlacklistLocation[];
    blackListWards: BlacklistLocation[];
  }

  activeShipmentService$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private api: ShipmentPriceApi,
    private locationQuery: LocationQuery,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore
  ) {
    super();
  }

  ngOnInit() {
    this.prepareData();
    this.activeShipmentService$.pipe(takeUntil(this.destroy$))
      .subscribe(async(service) => {
        if (service) {
          this.prepareData();
        }
      });
  }

  private prepareData() {
    const activeShipmentService = this.shippingSettingsStore.snapshot.activeShipmentService;

    const _locationsPick = activeShipmentService.blacklist_locations
      .filter(loc => loc.shipping_location_type == ShippingLocationType.pick);

    const _locationsDeliver = activeShipmentService.blacklist_locations
      .filter(loc => loc.shipping_location_type == ShippingLocationType.deliver);

    this.blackListLocationPick = {
      blackListProvinces: _locationsPick.filter(loc => loc.province_codes && loc.province_codes.length),
      blackListDistricts: _locationsPick.filter(loc => loc.district_codes && loc.district_codes.length),
      blackListWards: _locationsPick.filter(loc => loc.ward_codes && loc.ward_codes.length)
    }

    this.blackListLocationDeliver = {
      blackListProvinces: _locationsDeliver.filter(loc => loc.province_codes && loc.province_codes.length),
      blackListDistricts: _locationsDeliver.filter(loc => loc.district_codes && loc.district_codes.length),
      blackListWards: _locationsDeliver.filter(loc => loc.ward_codes && loc.ward_codes.length)
    }
  }

  selectProvinces(type: string) {
    const modal = this.modalController.create({
      component: BlacklistProvincesModalComponent,
      componentProps: {
        provinceCodes: []
      }
    });
    modal.show().then();
    modal.onDismiss().then(async(data) => {
      if (data) {
        this.updatingProvince = true;
        if(type == ShippingLocationType.pick){
          this.blackListLocationPick.blackListProvinces.push({
            province_codes: data.provinceCodes,
            reason: data.reason,
            shipping_location_type: ShippingLocationType[type]
          });
        }
        else {
          this.blackListLocationDeliver.blackListProvinces.push({
            province_codes: data.provinceCodes,
            reason: data.reason,
            shipping_location_type: ShippingLocationType[type]
          });
        }
        await this.updateShipmentServicesBlacklistLocations('Tạo', type == ShippingLocationType.pick ? 'lấy' : 'giao');
      }
    });
  }

  editProvinces(provinceCodes: string[], reason: string, index: number, type: string) {
    if (this.auth.snapshot.permission.permissions.includes('admin/shipment_service:update')) {
      const modal = this.modalController.create({
        component: BlacklistProvincesModalComponent,
        componentProps: { provinceCodes, reason }
      });
      modal.show().then();
      modal.onDismiss().then(async(data) => {
        if (data) {
          if(type == ShippingLocationType.pick){
            this.blackListLocationPick.blackListProvinces[index] = {
            ...this.blackListLocationPick.blackListProvinces[index],
            province_codes: data.provinceCodes,
            reason: data.reason,
            }
          }
          else {
            this.blackListLocationDeliver.blackListProvinces[index] = {
              ...this.blackListLocationDeliver.blackListProvinces[index],
              province_codes: data.provinceCodes,
              reason: data.reason,
            };
          }
          await this.updateShipmentServicesBlacklistLocations('Cập nhật', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
      });
    }
  }

  removeProvinces(province_codes: string[], reason: string, index: number, type: string) {
    const dialog = this.dialogController.createConfirmDialog({
      title: `Xoá tỉnh thành chặn ${type == ShippingLocationType.pick ? 'lấy' : 'giao'}`,
      body: `
        <div>Bạn có chắc muốn xoá tỉnh thành chặn ${type == ShippingLocationType.pick ? 'lấy' : 'giao'}?</div>
        <div>Gồm có: <strong>${this.getProvincesDisplay(province_codes)}</strong></div>
        <div>Lý do: ${reason}</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        if(type == ShippingLocationType.pick){
          this.blackListLocationPick.blackListProvinces.splice(index, 1);
          await this.updateShipmentServicesBlacklistLocations('Xoá', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        } else {
          this.blackListLocationDeliver.blackListProvinces.splice(index, 1);
          await this.updateShipmentServicesBlacklistLocations('Xoá', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
        dialog.close().then();
      }
    });
    dialog.show().then();
  }

  selectDistricts(type: string) {
    const modal = this.modalController.create({
      component: BlacklistDistrictsModalComponent,
      cssClass: 'modal-lg',
      componentProps: {
        districtCodes: []
      }
    });
    modal.show().then();
    modal.onDismiss().then(async(data) => {
      if (data) {
        this.updatingDistrict = true;
        if(type == ShippingLocationType.pick){
          this.blackListLocationPick.blackListDistricts.push({
            district_codes: data.districtCodes,
            reason: data.reason,
            shipping_location_type: ShippingLocationType[type]
          });
        }
        else {
          this.blackListLocationDeliver.blackListDistricts.push({
            district_codes: data.districtCodes,
            reason: data.reason,
            shipping_location_type: ShippingLocationType[type]
          });
        }
        await this.updateShipmentServicesBlacklistLocations('Tạo',type == ShippingLocationType.pick ? 'lấy' : 'giao');
      }
    });
  }

  editDistricts(districtCodes: string[], reason: string, index: number, type: string) {
    if (this.auth.snapshot.permission.permissions.includes('admin/shipment_service:update')) {
      const modal = this.modalController.create({
        component: BlacklistDistrictsModalComponent,
        cssClass: 'modal-lg',
        componentProps: { districtCodes, reason }
      });
      modal.show().then();
      modal.onDismiss().then(async(data) => {
        if (data) {
          if(type == ShippingLocationType.pick){
            this.blackListLocationPick.blackListDistricts[index] = {
              ...this.blackListLocationPick.blackListDistricts[index],
              district_codes: data.districtCodes,
              reason: data.reason,
            }
          }
          else {
            this.blackListLocationDeliver.blackListDistricts[index] = {
              ...this.blackListLocationDeliver.blackListDistricts[index],
              district_codes: data.districtCodes,
              reason: data.reason,
            };
          }
          await this.updateShipmentServicesBlacklistLocations('Cập nhật', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
      });
    }
  }

  removeDistricts(district_codes: string[], reason: string, index: number, type: string) {
    const dialog = this.dialogController.createConfirmDialog({
      title: `Xoá quận huyện chặn ${type == ShippingLocationType.pick ? 'lấy' : 'giao'}`,
      body: `
        <div>Bạn có chắc muốn xoá quận huyện chặn ${type == ShippingLocationType.pick ? 'lấy' : 'giao'}?</div>
        <div>Gồm có: <strong>${this.getDistrictsDisplay(district_codes)}</strong></div>
        <div>Lý do: ${reason}</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        if(type == ShippingLocationType.pick){
          this.blackListLocationPick.blackListDistricts.splice(index, 1);
          await this.updateShipmentServicesBlacklistLocations('Xoá',type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
        else {
          this.blackListLocationDeliver.blackListDistricts.splice(index, 1);
          await this.updateShipmentServicesBlacklistLocations('Xoá',type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
        dialog.close().then();
      }
    });
    dialog.show().then();
  }

  selectWards(type: string) {
    const modal = this.modalController.create({
      component: BlacklistWardsModalComponent,
      cssClass: 'modal-lg',
      componentProps: {
        wardCodes: []
      }
    });
    modal.show().then();
    modal.onDismiss().then(async(data) => {
      if (data) {
        this.updatingWard = true;
        if(type == ShippingLocationType.pick){
          this.blackListLocationPick.blackListWards.push({
            ward_codes: data.wardCodes,
            reason: data.reason,
            shipping_location_type: ShippingLocationType[type]
          });
        }
        else {
          this.blackListLocationDeliver.blackListWards.push({
            ward_codes: data.wardCodes,
            reason: data.reason,
            shipping_location_type: ShippingLocationType[type]
          });
        }
        await this.updateShipmentServicesBlacklistLocations('Tạo', type == ShippingLocationType.pick ? 'lấy' : 'giao');
      }
    });
  }

  editWards(wardCodes: string[], reason: string, index: number, type: string) {
    if (this.auth.snapshot.permission.permissions.includes('admin/shipment_service:update')) {
      const modal = this.modalController.create({
        component: BlacklistWardsModalComponent,
        cssClass: 'modal-lg',
        componentProps: { wardCodes, reason }
      });
      modal.show().then();
      modal.onDismiss().then(async(data) => {
        if (data) {
          if(type == ShippingLocationType.pick){
            this.blackListLocationPick.blackListWards[index] = {
              ...this.blackListLocationPick.blackListWards[index],
              ward_codes: data.wardCodes,
              reason: data.reason,
            };
          }
          else {
            this.blackListLocationDeliver.blackListWards[index] = {
              ...this.blackListLocationDeliver.blackListWards[index],
              ward_codes: data.wardCodes,
              reason: data.reason,
            };
          }
          await this.updateShipmentServicesBlacklistLocations('Cập nhật', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
      });
    }
  }

  removeWards(ward_codes: string[], reason: string, index: number, type: string) {
    const dialog = this.dialogController.createConfirmDialog({
      title: `Xoá quận huyện chặn ${type == ShippingLocationType.pick ? 'lấy' : 'giao'}`,
      body: `
        <div>Bạn có chắc muốn xoá quận huyện chặn ${type == ShippingLocationType.pick ? 'lấy' : 'giao'}?</div>
        <div>Gồm có: <strong>${this.getWardsDisplay(ward_codes)}</strong></div>
        <div>Lý do: ${reason}</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xoá',
      confirmCss: 'btn-danger',
      closeAfterAction: false,
      onConfirm: async () => {
        if(type == ShippingLocationType.pick){
          this.blackListLocationPick.blackListWards.splice(index, 1);
          await this.updateShipmentServicesBlacklistLocations('Xoá', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
        else {
          this.blackListLocationDeliver.blackListWards.splice(index, 1);
          await this.updateShipmentServicesBlacklistLocations('Xoá', type == ShippingLocationType.pick ? 'lấy' : 'giao');
        }
        dialog.close().then();
      }
    });
    dialog.show().then();
  }

  getProvincesDisplay(codes: string[]) {
    return codes.map(code => this.locationQuery.getProvince(code)?.filter_name)?.join(', ');
  }

  getDistrictsDisplay(codes: string[]) {
    return codes.map(code => this.locationQuery.getDistrict(code)?.filter_name)?.join(', ');
  }

  getWardsDisplay(codes: string[]) {
    return codes.map(code => this.locationQuery.getWard(code)?.filter_name)?.join(', ');
  }

  async updateShipmentServicesBlacklistLocations(updateType, shippingLocationTypeDisplay: string){
    this.loading = true;
    try {
      const blackListProvinces = this.blackListLocationDeliver.blackListProvinces.concat(this.blackListLocationPick.blackListProvinces);
      const blackListDistricts = this.blackListLocationDeliver.blackListDistricts.concat(this.blackListLocationPick.blackListDistricts);
      const blackListWards     = this.blackListLocationDeliver.blackListWards.concat(this.blackListLocationPick.blackListWards);
      const blacklist_locations = blackListProvinces.concat(blackListDistricts).concat(blackListWards);
      const body = {
        blacklist_locations: blacklist_locations,
        ids: [this.shippingSettingsStore.snapshot.activeShipmentService.id],
      };
      await this.shippingSettingsService.updateShipmentServiceBlacklistLocations(body);

      toastr.success(`${updateType} địa điểm chặn ${shippingLocationTypeDisplay} thành công.`);
    } catch(e) {
      debug.error('ERROR in updateShipmentServiceAvailableLocations', e);
      toastr.error(
          `${updateType} địa điểm chặn ${shippingLocationTypeDisplay} không thành công.`,
          e.code ? (e.message || e.msg) : ''
      );
    }
    this.loading = false;
    this.updatingProvince = false;
    this.updatingDistrict = false;
    this.updatingWard = false;
  }

}
