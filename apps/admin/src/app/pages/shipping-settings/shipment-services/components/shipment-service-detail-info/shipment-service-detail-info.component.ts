import {Component, OnInit} from '@angular/core';
import {
  ShipmentPrice,
  ShipmentPriceList,
  ShipmentService
} from 'libs/models/admin/ShipmentPrice';
import {Connection} from 'libs/models/Connection';
import {ShipmentPriceAPI, ShipmentPriceApi} from '@etop/api';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {BaseComponent} from '@etop/core';
import {ShippingSettingsStore} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.store";
import {ShippingSettingsService} from "apps/admin/src/app/pages/shipping-settings/shipping-settings.service";
import {FormBuilder} from "@angular/forms";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";
import {TelegramService} from "@etop/features";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";

export interface ShipmentPricesGroup {
  shipmentPriceList: ShipmentPriceList;
  shipmentPrices: ShipmentPrice[];
  onSettingShipmentPrices: boolean;
}

@Component({
  selector: 'admin-shipment-service-detail-info',
  templateUrl: './shipment-service-detail-info.component.html',
  styleUrls: ['./shipment-service-detail-info.component.scss']
})
export class ShipmentServiceDetailInfoComponent extends BaseComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private api: ShipmentPriceApi,
    private shippingSettingsService: ShippingSettingsService,
    private shippingSettingsStore: ShippingSettingsStore,
    private promiseQueue: PromiseQueueService,
    private telegramService: TelegramService,
    private dialog: DialogControllerService
  ) {
    super();
  }

  loading = false;
  initializing = true;

  shipmentPricesGrouped: ShipmentPricesGroup[] = [];

  activeShipmentService$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  connectionsList$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.connections),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );
  activeConnection$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeConnection),
    distinctUntilChanged((prev, next) => prev?.id == next?.id)
  );

  shipmentServiceForm = this.fb.group({
    ed_code: '',
    name: '',
    description: '',
    connection_id: '',
    service_ids: [],
    status: '',
    isTurnedOn: false
  });

  settingShipmentPricesAction: 'create' | 'update';

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
      <div class="d-flex align-items-center">
        <div class="carrier-image">
          <img src="${connection.provider_logo}" alt="">
        </div>
        <div class="pl-2">${connection.name.toUpperCase()}</div>
      </div>`;
  }

  private static validShipmentService(service: ShipmentService) {
    const { ed_code, name, service_ids, connection_id } = service;
    if (!ed_code) {
      toastr.error('Chưa nhập mã gói.');
      return false;
    }
    if (!name) {
      toastr.error('Chưa nhập tên gói.');
      return false;
    }
    if (!connection_id) {
      toastr.error('Chưa chọn nhà vận chuyển.');
      return false;
    }
    if (!service_ids || !service_ids.length) {
      toastr.error('Chưa chọn gói dịch vụ từ NVC.');
      return false;
    }
    return true;
  }

  displayMap = option => option && ShipmentServiceDetailInfoComponent.specialConnectionNameDisplay(option) || null;
  valueMap = option => option && option.id || null;

  connectionServiceDisplayMap = service => service && `${service.service_id} - ${service.name}` || null;
  connectionServiceValueMap = service => service && service.service_id || null;

  async ngOnInit() {
    this.shipmentServiceForm.controls['connection_id'].valueChanges
      .subscribe(connection_id => {
        const _connection = this.shippingSettingsStore.snapshot.connections
          .find(conn => conn.id == connection_id);
        this.shippingSettingsService.selectConnection(_connection);
        this.shipmentServiceForm.patchValue({ service_ids: [] })
      });
    this.shipmentServiceForm.controls['isTurnedOn'].valueChanges.subscribe(_ => {
      this.updateShipmentServiceStatus().then();
    });

    this.getFormDataStore();
    await this.groupByShipmentPriceLists();
    this.activeShipmentService$.pipe(takeUntil(this.destroy$))
      .subscribe(async(service) => {
        if (service) {
          this.getFormDataStore();
          await this.groupByShipmentPriceLists();
        }
      });
  }

  getFormDataStore() {
    const _service = this.shippingSettingsStore.snapshot.activeShipmentService;
    if (!_service) { return this.shipmentServiceForm.reset(); }
    const {
      ed_code, name, description,
      connection_id, service_ids,
      status
    } = _service;

    this.shipmentServiceForm.patchValue({
      ed_code, name, description,
      connection_id, service_ids,
      status, isTurnedOn: status == 'P'
    }, {emitEvent: false});
  }

  private async groupByShipmentPriceLists() {
    this.initializing = true;
    try {
      this.shipmentPricesGrouped = [];

      const {connection_id, id} = this.shippingSettingsStore.snapshot.activeShipmentService;
      await this.shippingSettingsService.getShipmentPriceLists(true, {connection_id});

      const shipmentPriceLists = this.shippingSettingsStore.snapshot.shipmentPriceLists;
      const promises = shipmentPriceLists.map(spl => async() => {
        const shipmentPrices = await this.api.getShipmentPrices({
          shipment_price_list_id: spl.id,
          shipment_service_id: id
        });
        this.shipmentPricesGrouped.push({
          shipmentPriceList: spl,
          shipmentPrices,
          onSettingShipmentPrices: false
        });
      });

      await this.promiseQueue.run(promises, 5);

    } catch(e) {
      debug.error('ERROR in groupByShipmentPriceLists', e);
    }
    this.initializing = false;
  }

  async updateShipmentServiceStatus() {
    const service = this.shippingSettingsStore.snapshot.activeShipmentService;
    const modal = this.dialog.createConfirmDialog({
      title: `${service?.status == 'P' ? 'Tắt' : 'Bật'} gói dịch vụ`,
      body: `
<div>Bạn có chắc muốn ${service?.status == 'P' ? 'tắt' : 'bật'} gói dịch vụ
  <strong class="text-primary">${service?.name}</strong>?
</div>`,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      closeAfterAction: false,
      onCancel: () => {
        this.shipmentServiceForm.patchValue({
          isTurnedOn: service?.status == 'P'
        }, {emitEvent: false});
      },
      onConfirm: async () => {
        try {
          const rawData = this.shipmentServiceForm.getRawValue();
          await this.shippingSettingsService.updateShipmentServiceStatus(
            service.id, rawData.isTurnedOn ? 'P' : 'N'
          );
          modal.close().then();
        } catch (e) {
          debug.error('ERROR in updateShipmentServiceStatus', e);
        }
      }
    });
    modal.show().then();
  }

  async updateShipmentService() {
    this.loading = true;
    try {
      const rawShipmentServiceData = this.shipmentServiceForm.getRawValue();

      const valid = ShipmentServiceDetailInfoComponent.validShipmentService(rawShipmentServiceData);
      if (!valid) {
        return this.loading = false;
      }

      const { ed_code, name, description, connection_id, status, service_ids } = rawShipmentServiceData;

      const body: ShipmentPriceAPI.UpdateShipmentServiceRequest = {
        id: this.shippingSettingsStore.snapshot.activeShipmentService.id,
        status,
        ed_code,
        name,
        description,
        connection_id,
        service_ids
      };

      await this.shippingSettingsService.updateShipmentService(body);
      toastr.success('Cập nhật gói dịch vụ thành công.');

    } catch(e) {
      toastr.error('Cập nhật gói dịch vụ không thành công.');
      debug.error('ERROR in updateShipmentService', e);
    }
    this.loading = false;
  }

  settingShipmentPrices(shipmentPricesGroup: ShipmentPricesGroup, toClose = false) {
    if (toClose) {
      shipmentPricesGroup.onSettingShipmentPrices = false;
      this.settingShipmentPricesAction = null;
      this.shippingSettingsService.selectShipmentPrices([]);
      this.shippingSettingsService.selectShipmentPriceList(null);
      return this.groupByShipmentPriceLists();
    }
    this.shipmentPricesGrouped.forEach(group => {
      group.onSettingShipmentPrices = false;
    });
    shipmentPricesGroup.onSettingShipmentPrices = true;
    this.settingShipmentPricesAction = shipmentPricesGroup.shipmentPrices?.length ? 'update' : 'create';
    this.shippingSettingsService.selectShipmentPrices(shipmentPricesGroup.shipmentPrices);
    this.shippingSettingsService.selectShipmentPriceList(shipmentPricesGroup.shipmentPriceList);
  }

  sendTelegramMessage(changes, shipmentPriceList: ShipmentPriceList) {
    const {creates, updates, deletes} = changes;
    return this.telegramService.updateShipmentPriceList(shipmentPriceList, creates, updates, deletes);
  }

}
