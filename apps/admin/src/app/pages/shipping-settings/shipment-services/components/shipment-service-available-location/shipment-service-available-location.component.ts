import { BaseComponent } from '@etop/core';
import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import { ShippingSettingsStore } from '../../../shipping-settings.store';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { ShippingSettingsService } from '../../../shipping-settings.service';
import {LocationQuery} from "@etop/state/location/location.query";

@Component({
  selector: 'admin-shipment-service-available-location',
  templateUrl: './shipment-service-available-location.component.html',
  styleUrls: ['./shipment-service-available-location.component.scss']
})
export class ShipmentServiceAvailableLocationComponent extends BaseComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private shippingSettingsStore: ShippingSettingsStore,
    private shippingSettingsService: ShippingSettingsService
  ){
    super();
  }

  get labelPick() {
    return this.availablePickForm.controls['filter_type'].value == 'exclude' ? 'loại trừ' : 'áp dụng';
  }

  get labelDeliver() {
    return this.availableDeliverForm.controls['filter_type'].value == 'exclude' ? 'loại trừ' : 'áp dụng';
  }

  loading = false;

  filterTypes = [
    {
      name: 'Bao gồm',
      value: 'include'
    },
    {
      name: 'Loại trừ',
      value: 'exclude'
    }
  ];

  regions = [
    {
      name: 'Miền Nam',
      value: 'south'
    },
    {
      name: 'Miền Trung',
      value: 'middle'
    },
    {
      name: 'Miền Bắc',
      value: 'north'
    }
  ];

  availablePickForm = this.fb.group({
    filter_type: '',
    regions: [],
    custom_region_ids: [],
    province_codes: [],
    shipping_location_type: 'pick'
  });

  availableDeliverForm = this.fb.group({
    filter_type: '',
    regions: [],
    custom_region_ids: [],
    province_codes: [],
    shipping_location_type: 'deliver'
  });

  provincesList$ = this.locationQuery.select(state => state.provincesList);
  customRegionsList$ = this.shippingSettingsStore.state$.pipe(map(s => s?.customRegions));

  activePick$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentServiceAvailablePick),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  activeDeliver$ = this.shippingSettingsStore.state$.pipe(
    map(s => s?.activeShipmentServiceAvailableDeliver),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );

  regionDisplayMap = region => region && region.name || null;
  regionValueMap = region => region && region.value || null;

  provinceDisplayMap = province => province && province.name || null;
  provinceValueMap = province => province && province.code || null;

  customRegionDisplayMap = customRegion => customRegion && customRegion.name || null;
  customRegionValueMap = customRegion => customRegion && customRegion.id || null;

  ngOnInit() {
    this.activePick$.subscribe(_ => this.getAvailablePickForm());
    this.activeDeliver$.subscribe(_ => this.getAvailableDeliverForm());
  }

  getAvailablePickForm() {
    const _availablePick = this.shippingSettingsStore.snapshot.activeShipmentServiceAvailablePick;
    const { filter_type, regions, custom_region_ids, province_codes, shipping_location_type } = _availablePick;

    this.availablePickForm.patchValue({
      filter_type, regions, custom_region_ids, province_codes, shipping_location_type
    });
  }

  getAvailableDeliverForm() {
    const _availableDeliver = this.shippingSettingsStore.snapshot.activeShipmentServiceAvailableDeliver;
    const { filter_type, regions, custom_region_ids, province_codes, shipping_location_type } = _availableDeliver;

    this.availableDeliverForm.patchValue({
      filter_type, regions, custom_region_ids, province_codes, shipping_location_type
    });
  }

  async updateShipmentServicesAvailableLocations() {
    this.loading = true;
    try {
      const availablePick = this.makeAvailablePickRequest();
      const availableDeliver = this.makeAvailableDeliverRequest();
      const available_locations = [availablePick, availableDeliver].filter(item => {
        const { province_codes, regions, custom_region_ids, filter_type } = item;
        return !!filter_type && (province_codes?.length + regions?.length + custom_region_ids?.length) > 0;
      });
      const body = {
        available_locations,
        ids: [this.shippingSettingsStore.snapshot.activeShipmentService.id],
      };

      await this.shippingSettingsService.updateShipmentServiceAvailableLocations(body);
      toastr.success('Cập nhật địa điểm khả dụng thành công.');

    } catch(e) {
      toastr.error('Cập nhật địa điểm khả dụng không thành công.');
      debug.error('ERROR in updateShipmentService', e);
    }
    this.loading = false;
  }

  private makeAvailablePickRequest() {
    const rawAvailablePickData = this.availablePickForm.getRawValue();
    let { custom_region_ids, filter_type, province_codes, regions, shipping_location_type } = rawAvailablePickData;
    return {
      id: null,
      custom_region_ids: custom_region_ids?.length ? custom_region_ids : [],
      filter_type,
      province_codes: province_codes?.length ? province_codes : [],
      regions: regions?.length ? regions : [],
      shipping_location_type
    };
  }

  private makeAvailableDeliverRequest() {
    const rawAvailableDeliverData = this.availableDeliverForm.getRawValue();
    let { custom_region_ids, filter_type, province_codes, regions, shipping_location_type } = rawAvailableDeliverData;
    return {
      id: null,
      custom_region_ids: custom_region_ids?.length ? custom_region_ids : [],
      filter_type,
      province_codes: province_codes?.length ? province_codes : [],
      regions: regions?.length ? regions : [],
      shipping_location_type
    };
  }

}
