import {Component, Input, OnInit} from '@angular/core';
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {BaseComponent} from "@etop/core";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'admin-blacklist-wards',
  templateUrl: './blacklist-wards-modal.component.html',
  styleUrls: ['./blacklist-wards-modal.component.scss']
})
export class BlacklistWardsModalComponent extends BaseComponent implements OnInit {
  @Input() wardCodes: string[] = [];
  @Input() reason: string;

  blacklistForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
    wardCode: '',
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.blacklistForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.blacklistForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private modalAction: ModalAction
  ) {
    super();
  }

  ngOnInit() {
    this.blacklistForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.blacklistForm.patchValue({
          districtCode: '',
          wardCode: '',
        });
      });

    this.blacklistForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.blacklistForm.patchValue({
          wardCode: '',
        });
      });

    this.blacklistForm.controls['wardCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (this.wardCodes.includes(value)) {
          return;
        }
        this.wardCodes.push(value);
      });
  }

  closeModal() {
    this.modalAction.close(false);
  }

  removeWard(index: number) {
    this.wardCodes.splice(index, 1);
  }

  getWardName(wardCode: string) {
    return this.locationQuery.getWard(wardCode)?.filter_name;
  }

  confirm() {
    if (!this.wardCodes || !this.wardCodes.length) {
      return toastr.error('Chưa chọn phường xã.');
    }
    if (!this.reason) {
      return toastr.error('Chưa nhập lý do.');
    }
    this.modalAction.dismiss({
      wardCodes: this.wardCodes,
      reason: this.reason
    });
  }

}
