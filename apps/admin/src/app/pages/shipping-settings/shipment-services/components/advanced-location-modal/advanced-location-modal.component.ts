import {Component, Input, OnInit} from '@angular/core';
import {CustomRegion} from 'libs/models/Location';
import {BaseComponent} from '@etop/core';
import {ModalAction} from 'apps/core/src/components/modal-controller/modal-action.service';
import {ShipmentPriceApi} from '@etop/api';
import {
  CustomRegionType,
  LatestLocationType,
  ProvinceType,
  RegionType,
  ShipmentPrice
} from 'libs/models/admin/ShipmentPrice';
import {FormArray, FormBuilder} from "@angular/forms";
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'admin-advanced-location-modal',
  templateUrl: './advanced-location-modal.component.html',
  styleUrls: ['./advanced-location-modal.component.scss']
})
export class AdvancedLocationModalComponent extends BaseComponent implements OnInit {
  @Input() shipmentPrice: ShipmentPrice;
  @Input() customRegions: CustomRegion[];

  regionTypes = [
    {
      name: 'Tất cả',
      value: 'all'
    },
    {
      name: 'Nội miền',
      value: 'noi_mien'
    },
    {
      name: 'Liên miền',
      value: 'lien_mien'
    },
    {
      name: 'Cận miền',
      value: 'can_mien'
    }
  ];
  regionType: RegionType;

  provinceTypes = [
    {
      name: 'Tất cả',
      value: 'all'
    },
    {
      name: 'Nội tỉnh',
      value: 'noi_tinh'
    },
    {
      name: 'Liên tỉnh',
      value: 'lien_tinh'
    }
  ];
  provinceType: ProvinceType;

  customRegionTypes = [
    {
      name: 'Tất cả',
      value: 'all'
    },
    {
      name: 'Nội vùng',
      value: 'noi_vung'
    },
    {
      name: 'Liên vùng',
      value: 'lien_vung'
    }
  ];
  customRegionType: CustomRegionType;

  customRegionIds: string[] = [];

  advancedLocationForm = this.fb.group({
    regionType: '',
    customRegionType: '',
    provinceType: '',
    customRegions: this.fb.array([])
  });

  constructor(
    private fb: FormBuilder,
    private modalAction: ModalAction,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  get locationsSelected() {
    return this.regionType || this.customRegionType || this.provinceType;
  }

  get locationsSummarized() {
    const result: string[] = [];

    if (this.provinceType == 'all') {
      result.push('Nội tỉnh và Liên tỉnh');
    } else {
      result.push(ShipmentPriceApi.shipmentPriceProvinceTypeMap(this.provinceType));
    }

    if (this.regionType == 'all') {
      result.push('Nội miền và Liên miền');
    } else {
      result.push(ShipmentPriceApi.shipmentPriceRegionTypeMap(this.regionType));
    }

    if (this.customRegionType == 'all') {
      result.push('Nội vùng và Liên vùng');
    } else {
      result.push(ShipmentPriceApi.shipmentPriceCustomRegionTypeMap(this.customRegionType));
    }

    const display = result.filter(res => !!res).join(', ');

    const customRegionNames = this.customRegionIds
      .map(id => {
        const found = this.customRegions.find(cr => cr.id == id);
        return found && `<strong class="big-font">${found.name}</strong>` || null;
      })
      .filter(name => !!name)
      .join(', ');

    return `${display} ${customRegionNames}`;

  }

  get customRegionsForm() {
    return this.advancedLocationForm?.controls['customRegions'] as FormArray;
  }

  ngOnInit() {
    this.advancedLocationForm.valueChanges.subscribe(form => {
      this.customRegionIds = form.customRegions.map(cr => cr.selected ? cr.id : null)
        .filter(id => !!id);
      this.regionType = form.regionType;
      this.customRegionType = form.customRegionType;
      this.provinceType = form.provinceType;
    });
    this.prepareLocationData();
  }

  private prepareLocationData() {
    this.customRegions.forEach(_ => {
      this.customRegionsForm.push(
        this.fb.group({
          selected: false,
          name: '',
          province_codes: [],
          id: ''
        })
      );
    });

    if (this.shipmentPrice && this.shipmentPrice.latest_location_type) {
      const latestLocation = this.shipmentPrice.latest_location_type;
      if (latestLocation.province_types) {
        this.provinceType = latestLocation.province_types.length > 1 ?
          ProvinceType.all : latestLocation.province_types[0];
      }
      if (latestLocation.region_types) {
        this.regionType = latestLocation.region_types.length > 1 ?
          RegionType.all : latestLocation.region_types[0];
      }
      if (latestLocation.custom_region_types) {
        this.customRegionType = latestLocation.custom_region_types.length > 1 ?
          CustomRegionType.all : latestLocation.custom_region_types[0];
        this.customRegionIds = latestLocation.custom_region_ids;
      }
    }
    this.advancedLocationForm.patchValue({
      regionType: this.regionType || '',
      customRegionType: this.customRegionType || '',
      provinceType: this.provinceType || '',
      customRegions: this.customRegions.map(cr => {
        const {name, province_codes, id} = cr;
        return {
          selected: this.customRegionIds.includes(id),
          name, province_codes, id
        }
      })
    }, {emitEvent: false});
  }

  customRegionDisplay(cr: CustomRegion) {
    return cr.province_codes &&
      cr.province_codes.map(code => this.locationQuery.getProvince(code)?.name)
        .filter(name => !!name)
        .join(', ') || '';
  }

  closeModal() {
    this.modalAction.close(false);
  }

  confirm() {
    const latestLocationType: LatestLocationType = {
      custom_region_ids: [],
      custom_region_types: [],
      region_types: [],
      province_types: []
    };
    if (this.regionType == 'all') {
      latestLocationType.region_types = [RegionType.noi_mien, RegionType.lien_mien, RegionType.can_mien];
    } else {
      latestLocationType.region_types = this.regionType && [this.regionType] || null;
    }

    if (this.provinceType == 'all') {
      latestLocationType.province_types = [ProvinceType.noi_tinh, ProvinceType.lien_tinh];
    } else {
      latestLocationType.province_types = this.provinceType && [this.provinceType] || null;
    }

    if (this.customRegionType == 'all') {
      latestLocationType.custom_region_types = [CustomRegionType.noi_vung, CustomRegionType.lien_vung];
    } else {
      latestLocationType.custom_region_types = this.customRegionType && [this.customRegionType] || null;
    }

    latestLocationType.custom_region_ids = this.customRegionIds;

    this.modalAction.dismiss(latestLocationType);

  }

}
