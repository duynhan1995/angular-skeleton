import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'admin-edit-shipment-service-form',
  templateUrl: './edit-shipment-service-form.component.html',
  styleUrls: ['./edit-shipment-service-form.component.scss']
})
export class EditShipmentServiceFormComponent implements OnInit {
  @Input() activeTab: 'detail_info' | 'blacklist_location' | 'available_location' = 'detail_info';

  constructor() {}

  ngOnInit() {

  }

}
