import {Component, Input, OnInit} from '@angular/core';
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {BaseComponent} from "@etop/core";

@Component({
  selector: 'admin-blacklist-districts',
  templateUrl: './blacklist-districts-modal.component.html',
  styleUrls: ['./blacklist-districts-modal.component.scss']
})
export class BlacklistDistrictsModalComponent extends BaseComponent implements OnInit {
  @Input() districtCodes: string[] = [];
  @Input() reason: string;

  blacklistForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.blacklistForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private modalAction: ModalAction
  ) {
    super();
  }

  ngOnInit() {
    this.blacklistForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.blacklistForm.patchValue({
          districtCode: '',
        });
      });

    this.blacklistForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (this.districtCodes.includes(value)) {
          return;
        }
        this.districtCodes.push(value);
      });
  }

  closeModal() {
    this.modalAction.close(false);
  }

  removeDistrict(index: number) {
    this.districtCodes.splice(index, 1);
  }

  getDistrictName(districtCode: string) {
    return this.locationQuery.getDistrict(districtCode)?.filter_name;
  }

  confirm() {
    if (!this.districtCodes || !this.districtCodes.length) {
      return toastr.error('Chưa chọn quận huyện.');
    }
    if (!this.reason) {
      return toastr.error('Chưa nhập lý do.');
    }
    this.modalAction.dismiss({
      districtCodes: this.districtCodes,
      reason: this.reason
    });
  }

}
