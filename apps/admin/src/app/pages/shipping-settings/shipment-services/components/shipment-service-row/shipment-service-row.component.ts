import { Component, Input, OnInit } from '@angular/core';
import { ShipmentService } from 'libs/models/admin/ShipmentPrice';

@Component({
  selector: '[admin-shipment-service-row]',
  templateUrl: './shipment-service-row.component.html',
  styleUrls: ['./shipment-service-row.component.scss']
})
export class ShipmentServiceRowComponent implements OnInit {
  @Input() shipmentService = new ShipmentService({});
  @Input() liteMode = false;

  constructor() { }

  get shipmentServiceStatus() {
    return this.shipmentService.status == 'N' ? 'Z' : this.shipmentService.status;
  }

  ngOnInit() {
  }

}
