import {Component, OnDestroy, OnInit} from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import {NavigationEnd, Router} from '@angular/router';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import {TabOpt} from "apps/core/src/components/header/components/tab-options/tab-options.interface";
import { ShopService } from '@etop/state/admin/shop';

export enum HeaderTab {
  shops = 'shops',
  users = 'users',
}

@Component({
  selector: 'admin-users-shops',
  template: `<router-outlet></router-outlet>`
})
export class UsersShopsComponent extends BaseComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private shopService: ShopService,
    private auth: AuthenticateStore
  ) {
    super();
  }

  ngOnInit() {
    const permissions = this.auth.snapshot.permission.permissions;
    if (permissions.includes('admin/shop:view') && permissions.includes('admin/user:view')) {
      this.setupTabs();
    }
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
  }

  setupTabs() {
    let tabs = [
      {
        title: 'Shop',
        name: 'shops',
        active: false,
        permissions: ['admin/shop:view'],
        onClick: () => {
          this.tabClick('shops').then(_ => {
            this.shopService.resetFilter()
          });
        }
      },
      {
        title: 'User',
        name: 'users',
        active: false,
        permissions: ['admin/user:view'],
        onClick: () => {
          this.tabClick('users').then();
        }
      }
    ];
    this.headerController.setTabs(tabs.map(tab => this.checkTab(tab)));
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`accounts/${type}`);
  }

  private checkTab(tab: TabOpt) {
    const headerTab: any = window.location.pathname.split('/')[2];
    tab.active = headerTab ? headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.shops;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const _headerTab: any = event.url.split('?')[0].split('/')[2];
        tab.active = _headerTab ? _headerTab == HeaderTab[tab.name] : tab.name == HeaderTab.shops;
      }
    });
    return tab;
  }

}
