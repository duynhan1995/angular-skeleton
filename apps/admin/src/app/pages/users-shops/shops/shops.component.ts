import {Component, OnInit} from '@angular/core';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {AuthenticateStore} from "@etop/core";
import {ShopService} from "@etop/state/admin/shop";
import { FilterOperator, FilterOptions, QueryOption } from '@etop/models';

@Component({
  selector: 'admin-shops',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <etop-filter
        [filters]="filters"
        (filterChanged)="shopList.filter($event)"
        [query]="query"
        (queryChanged)="shopList.query($event)">
      </etop-filter>
      <admin-shop-list #shopList></admin-shop-list>
    </ng-template>`
})
export class ShopsComponent extends PageBaseComponent implements OnInit {
  query: QueryOption = {
    label: 'Tên hoặc mã'
  }

  filters: FilterOptions = [
    {
      label: 'Số điện thoại',
      name: 'phone',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Email',
      name: 'email',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Lịch chuyển khoản',
      name: 'money_transaction_rrule',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        {
          name: 'Hàng ngày',
          value: 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR'
        },
        {
          name: 'Thứ 2-4-6 hàng tuần',
          value: 'FREQ=WEEKLY;BYDAY=MO,WE,FR'
        },
        {
          name: 'Thứ 3-5 hàng tuần',
          value: 'FREQ=WEEKLY;BYDAY=TU,TH'
        },
        {
          name: '1 tuần 1 lần vào thứ 6',
          value: 'FREQ=WEEKLY;BYDAY=FR'
        },
        {
          name: '1 tháng 2 lần vào thứ 6',
          value: 'FREQ=MONTHLY;BYDAY=+2FR,-1FR'
        },
        {
          name: '1 tháng 1 lần vào thứ 6',
          value: 'FREQ=MONTHLY;BYDAY=-1FR'
        },
        {
          name: '1 tháng 1 lần vào ngày cuối cùng của tháng',
          value: 'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1'
        }
      ]
    },
    {
      label: 'Tài khoản ngân hàng',
      name: 'bank_account',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        {
          name: 'Đã có tài khoản',
          value: 'true'
        },
        {
          name: 'Chưa có tài khoản',
          value: 'false'
        }
      ]
    }
  ];

  constructor(
    private auth: AuthenticateStore,
    private shopService: ShopService
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/shop:view');
  }

  ngOnInit() {
    this.shopService.groupShipmentPriceListsByConnection().then();
  }

}
