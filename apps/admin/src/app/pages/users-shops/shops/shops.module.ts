import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ShopListComponent } from 'apps/admin/src/app/pages/users-shops/shops/shop-list/shop-list.component';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from "@etop/shared";
import { ShopRowComponent } from 'apps/admin/src/app/pages/users-shops/shops/components/shop-row/shop-row.component';
import { EditShopFormComponent } from 'apps/admin/src/app/pages/users-shops/shops/components/edit-shop-form/edit-shop-form.component';
import {ShopsComponent} from "apps/admin/src/app/pages/users-shops/shops/shops.component";
import {AuthenticateModule} from "@etop/core";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { UpdateShopMtRrulePopupComponent } from './components/update-shop-mt-rrule-popup/update-shop-mt-rrule-popup.component';
import { ShopTransactionComponent } from './components/shop-transaction/shop-transaction.component';
import { ShopPriceListComponent } from './components/shop-price-list/shop-price-list.component';
import { ShopDetailComponent } from './components/shop-detail/shop-detail.component';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import {DebugModeDialogComponent} from "./components/debug-mode-dialog/debug-mode-dialog.component";

const routes: Routes = [
  {
    path: '',
    component: ShopsComponent
  }
];

@NgModule({
  declarations: [
    ShopsComponent,
    ShopListComponent,
    ShopRowComponent,
    EditShopFormComponent,
    UpdateShopMtRrulePopupComponent,
    ShopTransactionComponent,
    ShopPriceListComponent,
    ShopDetailComponent,
    DebugModeDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    EtopFilterModule,
    AuthenticateModule,
    NotPermissionModule,
    CallCenterModule,
    DropdownActionsModule,
    NgbModule,
    NgbTooltipModule
  ]
})
export class ShopsModule { }
