import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {EtopTableComponent, MatDialogController, SideSliderComponent} from "@etop/shared";
import {takeUntil} from "rxjs/operators";
import {Shop} from "libs/models/Account";
import {ShopQuery, ShopService} from "@etop/state/admin/shop";
import { Filters, Query } from '@etop/models';
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AdminUserApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ManageUsersModalComponent } from '../../components/manage-users-modal/manage-users-modal.component';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticateStore} from "@etop/core";
import {DebugModeDialogComponent} from "../components/debug-mode-dialog/debug-mode-dialog.component";

@Component({
  selector: 'admin-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.scss']
})
export class ShopListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('shopTable', { static: true }) shopTable: EtopTableComponent;
  @ViewChild('shopSlider', { static: true }) shopSlider: SideSliderComponent;

  shopsLoading$ = this.shopQuery.selectLoading();
  shopsList$ = this.shopQuery.selectAll();
  activeShop$ = this.shopQuery.selectActive();

  shipmentPriceListsGroups$ = this.shopQuery.select('shipmentPriceListsGroups');

  isLastPage$ = this.shopQuery.select(state => state.ui.isLastPage);
  isEmptyFilterResult$ = this.shopQuery.select(state => state.ui.isEmptyFilterResult);

  dropdownActions: DropdownActionOpt[] = [];

  queryParams = {
    id: '',
  };

  emptyTitle = 'Chưa có shop nào';

  tabs = [
    { name: 'Giao dịch', value: 'transaction' },
  ];
  active_tab: 'detail_info' | 'price_list' | 'transaction' = 'detail_info';

  constructor(
    private changeDetector: ChangeDetectorRef,
    private shopService: ShopService,
    private shopQuery: ShopQuery,
    private modal: ModalController,
    private userApi: AdminUserApi,
    private dialog: DialogControllerService,
    private matDialog: MatDialogController,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthenticateStore,

  ) {
    super();
  }

  get showPaging() {
    return !this.shopTable.liteMode && !this.shopTable.loading;
  }

  get sliderTitle() {
    return 'Chi tiết shop';
  }

  async ngOnInit() {
    const permissions = this.auth.snapshot.permission.permissions;
    if(permissions.includes('admin/shop_shipment_price_list:view')) {
      this.tabs.unshift({ name: 'Vận chuyển', value: 'price_list'})
    }
    if(permissions.includes('admin/shop:view')) {
      this.tabs.unshift({ name: 'Thông tin', value: 'detail_info' })
    }
    this.shopsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.shopTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isEmptyFilterResult$.pipe(takeUntil(this.destroy$))
      .subscribe(isEmptyFilterResult => {
        this.emptyTitle = isEmptyFilterResult ? 'Không tìm thấy shop phù hợp' : 'Chưa có shop nào';
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.shopTable.toggleNextDisabled(true);
          this.shopTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả shop.');
        }
    });

    this.activeShop$.pipe(takeUntil(this.destroy$))
      .subscribe(activeShop => {
        if(activeShop?.user){
          this.dropdownActions = [
            {
              title: 'Debug Mode',
              onClick: () => this.debugMode(),
              roles: ['admin'],
            },
            {
              title: `${activeShop.user.is_blocked ? 'Bỏ chặn' : 'Chặn'} user ${activeShop.user.full_name}`,
              cssClass: `${activeShop.user.is_blocked ? '' : 'text-danger'}`,
              onClick: () => activeShop.user.is_blocked ? this.unblockUser() : this.blockUser(),
              permissions: ['admin/user:block']
            },
          ];
        }
    });

  }

  changeTab(tab_value) {
    this.active_tab = tab_value;
  }

  ngOnDestroy() {
    this.shopService.selectShop(null);
    this.shopService.setFilters([]);
  }

  resetState() {
    this.shopTable.toggleLiteMode(false);
    this.shopSlider.toggleLiteMode(false);
    this.shopTable.toggleNextDisabled(false);
  }

  async filter(filters: Filters) {
    if (this.queryParams.id) {
      await this.router.navigateByUrl('/accounts/shops');
    }
    this.shopService.setFilters(filters);
    this.shopTable.resetPagination();
  }

  query(query: Query) {
    this.shopService.setQuery(query);
    this.shopTable.resetPagination();
  }

  getShops() {
    const shipmentPriceListsGroups = this.shopQuery.getValue().shipmentPriceListsGroups;
    if (shipmentPriceListsGroups?.length) {
      this.shopService.getShops().then();
    } else {
      this.shipmentPriceListsGroups$.pipe(takeUntil(this.destroy$))
        .subscribe(groups => {
          if (groups?.length) {
            this.shopService.getShops().then();
          }
        });
    }
  }

  loadPage({ page, perpage }) {
    this.shopService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this._paramsHandling();
    if(this.queryParams?.id){
      this.shopService.getShop(this.queryParams.id);
      this.activeShop$.pipe(takeUntil(this.destroy$)).subscribe(activeShop => this.detail(activeShop));
    } else {
      this.getShops();
    }
  }

  detail(shop: Shop) {
    this.changeTab('detail_info');
    this.shopService.selectShop(shop);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.shopService.selectShop(null);
    this._checkSelectMode();
    if(this.queryParams.id){
      this.shopService.setFilters([]);
      this.router.navigateByUrl('/accounts/shops').then(_ => {
        this.shopTable.resetPagination();
      });
    }
  }

  private _checkSelectMode() {
    const selected = !!this.shopQuery.getActive();
    this.shopTable.toggleLiteMode(selected);
    this.shopSlider.toggleLiteMode(selected);
  }

  blockUser() {
    const modal = this.modal.create({
      component: ManageUsersModalComponent,
      componentProps: {
        user: this.shopQuery.getActive()?.user
      }
    });
    modal.show().then();
    modal.onDismiss().then(success => {
      if (success) {
        this.shopService.getShops(false).then(_ => {
          this.shopService.selectShop(this.shopQuery.getActive());
        })
      }
    })
  }

  unblockUser() {
    const user = this.shopQuery.getActive()?.user;
    const dialog = this.dialog.createConfirmDialog({
      title: `Bỏ chặn user`,
      body: `
        <div>Bạn có chắc muốn bỏ chặn user <strong class="text-primary">${user.full_name}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Bỏ chặn',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.userApi.unblockUser(user.id);
          toastr.success('Bỏ chặn user thành công.');
          dialog.close().then();
          this.shopService.getShops(false).then(_ => {
            this.shopService.selectShop(this.shopQuery.getActive());
          })
        } catch (e) {
          debug.error('ERROR in unblocking User', e);
          toastr.error(`Bỏ chặn user không thành công.`, e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  private _paramsHandling() {
    const { queryParams } = this.route?.snapshot;
    this.queryParams.id = queryParams.id;
  }

  debugMode() {
    const dialog = this.matDialog.create({
      component: DebugModeDialogComponent,
      config: {
        width: '500px',
      },
      afterClosedCb: () => {
      }
    });
    dialog.open();
  }

}
