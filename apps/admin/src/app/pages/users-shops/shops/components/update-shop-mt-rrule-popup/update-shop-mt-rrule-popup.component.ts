import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MONEY_TRANSACTION_RRULE_OPTIONS, Shop} from "@etop/models";
import {ShopService} from "@etop/state/admin/shop";
import {MatDialogBaseComponent} from "@etop/web";

@Component({
  selector: 'admin-update-shop-mt-rrule-popup',
  templateUrl: './update-shop-mt-rrule-popup.component.html',
  styleUrls: ['./update-shop-mt-rrule-popup.component.scss']
})
export class UpdateShopMtRrulePopupComponent extends MatDialogBaseComponent implements OnInit {
  loading = false;

  constructor(
    private dialogRef: MatDialogRef<UpdateShopMtRrulePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public shop: Shop,
    private shopService: ShopService
  ) {
    super();
  }

  get rruleOptions() {
    return MONEY_TRANSACTION_RRULE_OPTIONS;
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  async confirm() {
    this.loading = true;
    try {
      await this.shopService.updateShopMoneyTransactionRrule(this.shop);
      toastr.success('Thay đổi lịch chuyển khoản cho shop thành công.');
      this.dialogRef.close(true);
    } catch (e) {
      debug.error('ERROR in UpdateShopMtRrule', e);
      toastr.error('Thay đổi lịch chuyển khoản cho shop không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

}
