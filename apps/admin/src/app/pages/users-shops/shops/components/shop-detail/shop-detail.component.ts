import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'admin-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.scss']
})
export class ShopDetailComponent implements OnInit {

  @Input() activeTab: 'detail_info' | 'price_list' | 'transaction' = 'detail_info';
  constructor(
  ) {
  }

  ngOnInit() {}

}
