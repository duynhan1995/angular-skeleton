import {Component, Input, OnInit} from '@angular/core';
import {Shop} from "libs/models/Account";

@Component({
  selector: '[admin-shop-row]',
  templateUrl: './shop-row.component.html',
  styleUrls: ['./shop-row.component.scss']
})
export class ShopRowComponent implements OnInit {
  @Input() shop = new Shop({});
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
