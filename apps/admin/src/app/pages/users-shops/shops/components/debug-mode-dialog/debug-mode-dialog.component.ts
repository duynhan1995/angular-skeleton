import { Component, OnInit } from '@angular/core';
import {AuthenticateStore, ConfigService} from '@etop/core';
import { ShopQuery } from '@etop/state/admin/shop';
import {MatDialogRef} from "@angular/material/dialog";
import {MatDialogBaseComponent} from "@etop/web";
import {AdminAccountApi} from "@etop/api";
import {environment} from "../../../../../../../../shop/src/environments/environment";
import misc from "../../../../../../../../core/src/libs/misc";
import {Shop} from "@etop/models";

@Component({
  selector: 'admin-debug-mode-dialog',
  templateUrl: './debug-mode-dialog.component.html',
  styleUrls: ['./debug-mode-dialog.component.scss']
})
export class DebugModeDialogComponent extends MatDialogBaseComponent implements OnInit {
  clientSelected = '';
  adminPassword = '';
  clients = [];
  currentShop: Shop = this.shopQuery.getActive();

  constructor(
    private dialogRef: MatDialogRef<DebugModeDialogComponent>,
    private adminAccountApi: AdminAccountApi,
    private auth: AuthenticateStore,
    private config: ConfigService,
    private shopQuery: ShopQuery,
  ) {
    super();
  }


  async ngOnInit() {
    const isProduction = this.config.getConfig().production || environment.production;
    this.clients = [
      {
        name: 'pos.topship.vn',
        value: isProduction ? 'pos.topship.vn' : 'pos.d.topship.vn'
      },
      {
        name: 'shop.topship.vn',
        value: isProduction ? 'shop.topship.vn' : 'shop.d.etop.vn'
      },
      {
        name: 'pos.etop.vn',
        value: isProduction ? 'pos.etop.vn' : 'pos.d.etop.vn'
      },
      {
        name: 'cs.etelecom.vn',
        value: isProduction ? 'cs.etelecom.vn' : 'cs.d.etelecom.vn'
      },
    ]
  }

  closeDialog(){
    this.dialogRef.close();
  }

  async goToDebugMode(){
    try {
      if(!this.clientSelected){
        toastr.error('Vui lòng chọn client cần truy cập');
        return;
      }
      if(!this.adminPassword){
        toastr.error('Vui lòng nhập mật khẩu');
        return;
      }
      const res = await this.adminAccountApi.adminLoginAsAccount({
        'user_id': this.currentShop.owner_id,
        'password': this.adminPassword,
        'account_id': this.currentShop.id,
      })
      const token = misc.encodeBase64(res.access_token);
      if(token) {
        window.open(`https://${this.clientSelected}/admin-login?session=${token}`);
        this.dialogRef.close();
      }
    }
    catch (e) {
      debug.error('Error in goToDebugMode: ', e);
      toastr.error(e.msg || e.message);
    }
  }

}
