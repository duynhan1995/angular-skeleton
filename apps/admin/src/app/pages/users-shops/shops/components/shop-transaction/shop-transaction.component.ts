import { Component, OnInit } from '@angular/core';
import { AdminTransactionApi, AdminTransactionAPI } from '@etop/api';
import { BaseComponent } from '@etop/core';
import { MoneyTransactionShop, Shop } from '@etop/models';
import { ShopQuery } from '@etop/state/admin/shop';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-shop-transaction',
  templateUrl: './shop-transaction.component.html',
  styleUrls: ['./shop-transaction.component.scss']
})
export class ShopTransactionComponent extends BaseComponent implements OnInit {

  activeShop$: Observable<Shop> = this.shopQuery.selectActive();
  transactions: [];

  constructor(
    private shopQuery: ShopQuery,
    private transactionApi: AdminTransactionApi,
  ) { super() }

  async ngOnInit() {
    this.activeShop$.pipe(takeUntil(this.destroy$))
      .subscribe(async (activeShop) => {
        if (activeShop) {
          await this.getTransactions(activeShop.id)
        }
      });
  }

  async getTransactions(account_id: string) {
    try {
       let res = await this.transactionApi.getTransactions({filter: {account_id}});
       this.transactions = res?.transactions;
    } catch (e) {

    }
  }

}
