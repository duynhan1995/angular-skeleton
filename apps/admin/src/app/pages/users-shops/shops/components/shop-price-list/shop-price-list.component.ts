import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { BaseComponent } from '@etop/core';
import { Shop } from '@etop/models';
import { ShopQuery, ShopService } from '@etop/state/admin/shop';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {UpdateShopMtRrulePopupComponent} from "../update-shop-mt-rrule-popup/update-shop-mt-rrule-popup.component";
import {MatDialogController} from "@etop/shared";

@Component({
  selector: 'admin-shop-price-list',
  templateUrl: './shop-price-list.component.html',
  styleUrls: ['./shop-price-list.component.scss']
})
export class ShopPriceListComponent extends BaseComponent implements OnInit {

  shopShipmentPriceListsForm = this.fb.group({
    shopShipmentPriceLists: this.fb.array([])
  });
  initializing = true;
  loading = false;
  activeShop$: Observable<Shop> = this.shopQuery.selectActive();

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.id || null;

  constructor(
    private fb: FormBuilder,
    private shopQuery: ShopQuery,
    private shopService: ShopService,
    private matDialogController: MatDialogController
  ) { super() }

  get shopShipmentPriceListsFormArray() {
    return this.shopShipmentPriceListsForm.controls['shopShipmentPriceLists'] as FormArray;
  }

  async ngOnInit() {
    this.activeShop$.pipe(takeUntil(this.destroy$))
      .subscribe(async (activeShop) => {
        if (activeShop) {
          await this.getShopShipmentPriceLists();
          this.getFormDataStore();
        }
      });
  }

  getFormDataStore() {
    const { shipmentPriceListsGroups, shopShipmentPriceLists } = this.shopQuery.getValue();

    this.shopShipmentPriceListsForm = this.fb.group({
      shopShipmentPriceLists: this.fb.array([])
    });

    shipmentPriceListsGroups.forEach(_ => {
      this.shopShipmentPriceListsFormArray.push(
        this.fb.group({
          connection: null,
          shipmentPriceLists: [],
          note: '',
          shipment_price_list_id: ''
        })
      );
    });

    this.shopShipmentPriceListsForm.patchValue({
      shopShipmentPriceLists: shipmentPriceListsGroups.map(group => {
        const _selectedShopSpl = shopShipmentPriceLists.find(shopSpl => {
          return shopSpl.connection_id == group.connection.id;
        });
        const _defaultPriceList = group.shipmentPriceLists.find(spl => spl.is_default);
        return {
          connection: group.connection,
          shipmentPriceLists: group.shipmentPriceLists,
          note: '',
          shipment_price_list_id: _selectedShopSpl?.shipment_price_list_id || _defaultPriceList?.id || null
        };
      })
    });
  }

  async getShopShipmentPriceLists() {
    if (!this.shopQuery.getActive()) {
      return;
    }
    this.initializing = true;
    await this.shopService.getShopShipmentPriceLists();
    this.initializing = false;
  }

  async confirm() {
    this.loading = true;
    const rawFormData = this.shopShipmentPriceListsForm.getRawValue();
    const requests = rawFormData.shopShipmentPriceLists.map(data => {
      return {
        note: data.note,
        connection_id: data.connection.id,
        shipment_price_list_id: data.shipment_price_list_id,
        shop_id: this.shopQuery.getActiveId()
      };
    });
    try {
      await this.shopService.confirmShopShipmentPriceLists(requests);
      toastr.success('Thiết lập bảng giá cho shop thành công.');
    } catch (e) {
      toastr.error('Thiết lập bảng giá cho shop không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  openPopupUpdateShopMtRrule() {
    const dialog = this.matDialogController.create({
      component: UpdateShopMtRrulePopupComponent,
      config: {
        data: { ...this.shopQuery.getActive() }
      },
      afterClosedCb: (success: boolean) => {
        if (success) {
          this.shopService.getShops(false).then();
        }
      }
    });

    dialog.open();

  }

  openDialogUpdateShopPriority() {
    const activeShop = this.shopQuery.getActive();
    const {name, code, is_prior_money_transaction} = activeShop;
    let content = `Bạn có chắc muốn đặt shop <strong>${code} - ${name}</strong> làm shop <strong>Ưu tiên</strong>?`;
    if (is_prior_money_transaction) {
      content = `Bạn có chắc muốn <strong>Huỷ ưu tiên</strong> cho shop <strong>${code} - ${name}</strong>`;
    }
    const dialog = this.matDialogController.create({
      template: {
        title: 'Cập nhật shop ưu tiên',
        content,
      },
      afterClosedCb: (success: boolean) => {
        if (success) {
          this.shopService.getShops(false).then();
        }
      },
      onConfirm: async() => {
        await this.updateShopPriority(activeShop);
      },
      confirmTitle: is_prior_money_transaction && 'Huỷ ưu tiên',
      confirmStyle: is_prior_money_transaction && 'btn-danger',
    });

    dialog.open();
  }

  async updateShopPriority(shop: Shop) {
    try {
      await this.shopService.updateShopPriorityMoneyTransaction(shop);
      toastr.success('Cập nhật shop ưu tiên thành công.');
    } catch (e) {
      debug.error('ERROR in updateShopPriority', e);
      toastr.error('Cập nhật shop ưu tiên không thành công.', e.code && (e.message || e.msg));
      throw e;
    }
  }

}
