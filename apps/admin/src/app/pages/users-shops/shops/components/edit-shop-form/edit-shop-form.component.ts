import {Component, OnInit} from '@angular/core';
import {Shop} from '@etop/models';
import {Observable} from 'rxjs';
import {BaseComponent} from '@etop/core';
import {ShopQuery, ShopService} from '@etop/state/admin/shop';
import {Router} from '@angular/router';

@Component({
  selector: 'admin-edit-shop-form',
  templateUrl: './edit-shop-form.component.html',
  styleUrls: ['./edit-shop-form.component.scss']
})
export class EditShopFormComponent extends BaseComponent implements OnInit {
  activeShop$: Observable<Shop> = this.shopQuery.selectActive();

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.id || null;

  constructor(
    private shopService: ShopService,
    private shopQuery: ShopQuery,
    private router: Router,
  ) {
    super();
  }

  canDisplayBlockError(shop: Shop) {
    return shop?.user?.is_blocked && shop?.user?.block_reason;
  }

  ngOnInit() {}

  userDetail() {
    const id = this.shopQuery.getActive()?.user?.id;
    this.router.navigateByUrl(`/accounts/users?id=${id}`).then();
  }

}
