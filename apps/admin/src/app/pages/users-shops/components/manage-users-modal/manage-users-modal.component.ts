import {Component, Input, OnInit} from '@angular/core';
import {User} from "libs/models";
import {ModalAction} from "apps/core/src/components/modal-controller/modal-action.service";
import {AdminUserApi} from "@etop/api";

@Component({
  selector: 'admin-manage-users-modal',
  templateUrl: './manage-users-modal.component.html',
  styleUrls: ['./manage-users-modal.component.scss']
})
export class ManageUsersModalComponent implements OnInit {
  @Input() action: 'block' | 'unblock';
  @Input() user: User;

  loading = false;

  block_reason: string;

  constructor(
    private modalAction: ModalAction,
    private userApi: AdminUserApi
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async block() {
    this.loading = true;
    try {
      if (!this.block_reason) {
        toastr.error('Vui lòng nhập lý do chặn!');
        return this.loading = false;
      }
      await this.userApi.blockUser(this.user.id, this.block_reason);
      toastr.success('Chặn user thành công.');
      this.modalAction.dismiss(true);
    } catch(e) {
      debug.error('ERROR in block User', e);
      toastr.error('Chặn user không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

}
