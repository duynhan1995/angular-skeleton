import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {EtopTableComponent, SideSliderComponent} from "@etop/shared";
import {AuthenticateStore} from "@etop/core";
import {Observable} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {UserQuery, UserService, UserStore} from "@etop/state/admin/user";
import { FilterOperator, Filters, User } from 'libs/models';
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {AdminUserApi} from "@etop/api";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import { ManageUsersModalComponent} from '../../components/manage-users-modal/manage-users-modal.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'admin-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('userTable', { static: true }) userTable: EtopTableComponent;
  @ViewChild('userSlider', { static: true }) userSlider: SideSliderComponent;

  tabs = [
    { name: 'Thông tin', value: 'detail_info' },
    { name: 'Sale', value: 'sale_info' },
    { name: 'Tổng đài', value: 'tenant_info' },
    { name: 'Shop', value: 'shop_info'},
  ];
  active_tab: 'detail_info' | 'sale_info' | 'shop_info' = 'detail_info';


  usersLoading$ = this.userQuery.selectLoading();
  usersList$ = this.userQuery.selectAll();
  activeUser$: Observable<User> = this.userQuery.selectActive();

  isLastPage$ = this.userQuery.select(state => state.ui.isLastPage);

  dropdownActions: DropdownActionOpt[] = [];

  queryParams = {
    id: '',
  };

  constructor(
    private modal: ModalController,
    private dialog: DialogControllerService,
    private changeDetector: ChangeDetectorRef,
    private userService: UserService,
    private userQuery: UserQuery,
    private userApi: AdminUserApi,
    private route: ActivatedRoute,
    private router: Router,
    private authStore: AuthenticateStore
  ) {
    super();
  }

  get showPaging() {
    return !this.userTable.liteMode && !this.userTable.loading;
  }

  get emptyResultFilter() {
    const {paging, filters} = this.userQuery.getValue().ui;
    const users = this.userQuery.getAll();
    return paging.offset == 0 && filters && Object.keys(filters).length && !users.length;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy user phù hợp';
    }
    return 'Chưa có user nào';
  }

  get sliderTitle() {
    return 'Chi tiết user';
  }

  async ngOnInit() {
    this.usersLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.userTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.userTable.toggleNextDisabled(true);
          this.userTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả user.');
        }
      });

    this.activeUser$.pipe(takeUntil(this.destroy$))
      .subscribe(activeUser => {
        if (activeUser) {
          this.dropdownActions = [
            {
              title: `${activeUser.is_blocked ? 'Bỏ chặn' : 'Chặn'} user`,
              cssClass: `${activeUser.is_blocked ? '' : 'text-danger'}`,
              onClick: () => activeUser.is_blocked ? this.unblockUser() : this.blockUser(),
              permissions: ['admin/user:block']
            }
          ];
        }
      });

      const roles = this.authStore.snapshot.permission.roles;
      if (roles.includes('ad_voip') && roles.length == 1) {
        this.tabs.splice(1, 1);
      }
  }

  ngOnDestroy() {
    this.userService.selectUser(null);
    this.userService.setFilters([]);
  }

  resetState() {
    this.userTable.toggleLiteMode(false);
    this.userSlider.toggleLiteMode(false);
    this.userTable.toggleNextDisabled(false);
  }

  async filter(filters: Filters) {
    if (this.queryParams.id) {
      await this.router.navigateByUrl('/accounts/users');
    }
    this.userService.setFilters(filters);
    this.userTable.resetPagination();
  }

  async getUsers() {
    await this.authStore.waitForReady();
    await this.userService.getUsers();
  }

  getUser(id: string){
    this.userService.getUser(id).then();
  }

  loadPage({ page, perpage }) {
    this.userService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this._paramsHandling();
    if (this.queryParams.id) {
      this.getUser(this.queryParams.id);
      this.activeUser$.pipe(takeUntil(this.destroy$))
        .subscribe(activeUser => {
        this.detail(activeUser);
      })
    } else {
      this.getUsers().then();
    }
  }

  detail(user: User) {
    this.userService.selectUser(user);
    this._checkSelectMode();
  }

  changeTab(tab_value) {
    this.active_tab = tab_value;
  }


  onSliderClosed() {
    this.userService.selectUser(null);
    this._checkSelectMode();
    if (this.queryParams.id) {
      this.userService.setFilters([]);
      this.router.navigateByUrl('/accounts/users').then(_ => {
        this.userTable.resetPagination();
      });
    }
  }

  private _checkSelectMode() {
    const selected = !!this.userQuery.getActive();
    this.userTable.toggleLiteMode(selected);
    this.userSlider.toggleLiteMode(selected);
  }

  blockUser() {
    const modal = this.modal.create({
      component: ManageUsersModalComponent,
      componentProps: {
        user: this.userQuery.getActive()
      }
    });
    modal.show().then();
    modal.onDismiss().then(success => {
      if (success) {
        this.userService.getUsers(false).then(_ => {
          this.userService.selectUser(this.userQuery.getActive());
        });
      }
    })
  }

  unblockUser() {
    const user = this.userQuery.getActive();
    const dialog = this.dialog.createConfirmDialog({
      title: `Bỏ chặn user`,
      body: `
        <div>Bạn có chắc muốn bỏ chặn user <strong class="text-primary">${user.full_name}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Bỏ chặn',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.userApi.unblockUser(user.id);
          toastr.success('Bỏ chặn user thành công.');
          dialog.close().then();
          this.userService.getUsers(false).then(_ => {
            this.userService.selectUser(this.userQuery.getActive());
          });
        } catch (e) {
          debug.error('ERROR in unblocking User', e);
          toastr.error(`Bỏ chặn user không thành công.`, e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  private _paramsHandling() {
    const { queryParams } = this.route?.snapshot;
    this.queryParams.id = queryParams.id;
  }

}
