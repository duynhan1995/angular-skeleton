import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from "@etop/shared";
import {AuthenticateModule} from "@etop/core";
import {UsersComponent} from "apps/admin/src/app/pages/users-shops/users/users.component";
import { UserListComponent } from './user-list/user-list.component';
import {UserRowComponent} from "apps/admin/src/app/pages/users-shops/users/components/user-row/user-row.component";
import {EditUserFormComponent} from "apps/admin/src/app/pages/users-shops/users/components/edit-user-form/edit-user-form.component";
import {DropdownActionsModule} from "apps/shared/src/components/dropdown-actions/dropdown-actions.module";
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";
import { SaleInfoComponent } from './components/edit-user-form/sale-info/sale-info.component';
import { UserInfoComponent } from './components/edit-user-form/user-info/user-info.component';
import { TenantInfoComponent } from './components/edit-user-form/tenant-info/tenant-info.component';
import { AddHotlineModalComponent } from './components/edit-user-form/tenant-info/add-hotline-modal/add-hotline-modal.component';
import { EditHotlineModalComponent } from './components/edit-user-form/tenant-info/edit-hotline-modal/edit-hotline-modal.component';
import { FormsModule } from '@angular/forms';
import {ShopInfoComponent} from "./components/edit-user-form/shop-info/shop-info.component";
import { AssignHotlineModalComponent } from './components/edit-user-form/tenant-info/assign-hotline-modal/assign-hotline-modal.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent
  }
];

@NgModule({
  declarations: [
    UsersComponent,
    UserListComponent,
    UserRowComponent,
    EditUserFormComponent,
    SaleInfoComponent,
    UserInfoComponent,
    TenantInfoComponent,
    ShopInfoComponent,
    AddHotlineModalComponent,
    EditHotlineModalComponent,
    AssignHotlineModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    EtopMaterialModule,
    MaterialModule,
    EtopFilterModule,
    AuthenticateModule,
    NotPermissionModule,
    DropdownActionsModule,
    CallCenterModule,
    FormsModule
  ]
})
export class UsersModule { }
