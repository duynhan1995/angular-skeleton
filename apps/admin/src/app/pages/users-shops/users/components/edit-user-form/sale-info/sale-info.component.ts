import {Component, OnInit} from '@angular/core';
import {AdminUser, AdminUserRole, User} from '@etop/models';
import {BaseComponent} from '@etop/core';
import {AdminAccountApi, AdminUserApi, AdminUserAPI} from '@etop/api';
import GetUsersRequest = AdminUserAPI.GetUsersRequest;
import UpdateUserRef = AdminUserAPI.UpdateUserRef;
import {UserQuery, UserService} from '@etop/state/admin/user';
import {TelegramService} from "@etop/features";
import {takeUntil} from 'rxjs/operators';
import {stringify} from 'querystring';

@Component({
  selector: 'admin-sale-info',
  templateUrl: './sale-info.component.html',
  styleUrls: ['./sale-info.component.scss']
})
export class SaleInfoComponent extends BaseComponent implements OnInit {
  activeUserID$ = this.userQuery.selectActiveId();

  initializing = true;
  loading = false;

  refAffUsers: User[] = [];
  refSaleAdmins: AdminUser[] = [];

  refAffID: string;
  refSaleID: string;

  refAff: User;
  refSale: AdminUser;

  displayMap = option => option && `${option.full_name} - ${option.phone}` || null;

  refAffValueMap = (option: User) => option && option.phone || null;
  refAffInputMap = (option: User) => option && option.phone || null;

  refSaleValueMap = (option: AdminUser) => option && option.phone || null;
  refSaleInputMap = (option: AdminUser) => option && option.phone || null;

  constructor(
    private adminUserApi: AdminUserApi,
    private userQuery: UserQuery,
    private userService: UserService,
    private adminAccountApi: AdminAccountApi,
    private telegram: TelegramService,
  ) {
    super();
  }

  ngOnInit() {
    this.activeUserID$.subscribe(_ => {
      this.prepareData();
    });
  }

  selectUser() {
    this.refAff = this.refAffUsers.find(item => item.phone == this.refAffID);
  }

  selectAdmin() {
    this.refSale = this.refSaleAdmins.find(item => item.phone == this.refSaleID);
  }

  async onUserSearch(searchText: string) {
    const paging = {offset: 0, limit: 1000};
    const queryUser: GetUsersRequest = {
      paging,
      filters: {
        phone: searchText
      }
    };
    this.refAffUsers = await this.adminUserApi.getUsers(queryUser);
    this.refAff = this.refAffUsers.find(item => item.phone == this.refAffID);
  }

  prepareData() {
    const activeUser = this.userQuery.getActive();

    const queryUser: GetUsersRequest = {
      paging: {offset: 0, limit: 1000},
      filters: {
        phone: activeUser?.ref_aff
      }
    };

    this.refAffID = activeUser?.ref_aff;
    this.refSaleID = activeUser?.ref_sale;

    this.adminUserApi.getUsers(queryUser)
      .then(users => {
        this.refAffUsers = users;
        this.refAff = this.refAffUsers.find(s => s.phone == this.refAffID);
      })
      .catch(_ => {
        this.refAffUsers = [];
        this.refAff = null;
      });

    this.adminAccountApi.getAdminUsers([AdminUserRole.ad_sale, AdminUserRole.ad_salelead])
      .then(admins => {
        this.refSaleAdmins = admins;
        this.refSale = this.refSaleAdmins.find(admin => admin.phone == this.refSaleID);
      })
      .catch(_ => {
        this.refSaleAdmins = [];
        this.refSale = null;
      });

  }

  async submit() {
    this.loading = true;
    try {
      const user = this.userQuery.getActive();

      const query: UpdateUserRef = {
        ref_aff: this.refAffID,
        ref_sale: this.refSaleID,
        user_id: user.id,
      };
      await this.userService.updateUserRef(query);
      this.telegramMessage(user);
      toastr.success('Cập nhật thành công.');
    } catch (e) {
      toastr.error('Cập nhật không thành công.', e.code && (e.msg || e.message));
    }
    this.loading = false;
  }

  private telegramMessage(user){
    this.telegram.updateSales(user, this.refAff, this.refSale).then();
  }
}
