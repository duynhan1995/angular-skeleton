import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { UserQuery } from '@etop/state/admin/user';
import { User } from '@etop/models';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'admin-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent extends BaseComponent implements OnInit {
  user = new User({});
  activeUser$ = this.userQuery.selectActive();

  constructor(
    private userQuery: UserQuery,
  ) {
    super();
  }

  async ngOnInit() {
    const user = this.userQuery.getActive();
    if (user) {
      this.user = this.userQuery.getActive();
    }

    this.activeUser$.pipe(takeUntil(this.destroy$))
      .subscribe(async(activeUser) => {
        if (activeUser) {
          this.user = activeUser;
        }
      });
  }

}
