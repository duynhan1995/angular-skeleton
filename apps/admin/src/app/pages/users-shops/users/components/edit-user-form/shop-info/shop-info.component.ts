import {Component, OnDestroy, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {UserQuery, UserService} from '@etop/state/admin/user';
import {User} from '@etop/models';
import {BaseComponent} from '@etop/core';

@Component({
  selector: 'admin-shop-info',
  templateUrl: './shop-info.component.html',
  styleUrls: ['./shop-info.component.scss']
})
export class ShopInfoComponent extends BaseComponent implements OnInit {
  shopList$ = this.userQuery.select('userShopList');
  shopList = [];
  user = new User({});
  activeUser$ = this.userQuery.selectActive();
  loading = false;

  constructor(
    private userQuery: UserQuery,
    private userService: UserService,
  ) {
    super();
  }

  async ngOnInit() {
    this.activeUser$.pipe(takeUntil(this.destroy$))
      .subscribe(async (activeUser) => {
        if (activeUser) {
          this.loading = true;
          this.user = activeUser;
          await this.userService.getUserShopList(this.user);
          this.loading = false;
        }
      });
  }

  shopDetail(shopId) {
    window.open(`/accounts/shops?id=${shopId}`, '_blank');
  }
}
