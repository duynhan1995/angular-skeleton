import {Component, Input, OnInit} from '@angular/core';
import {User} from "libs/models";

@Component({
  selector: '[admin-user-row]',
  templateUrl: './user-row.component.html',
  styleUrls: ['./user-row.component.scss']
})
export class UserRowComponent implements OnInit {
  @Input() user = new User({});
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
