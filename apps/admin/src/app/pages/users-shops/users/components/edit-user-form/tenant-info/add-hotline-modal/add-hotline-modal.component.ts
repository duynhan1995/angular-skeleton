import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Hotline } from '@etop/models';
import { MatDialogController } from '@etop/shared';
import { HotlineService } from '@etop/state/admin/hotline';
import { MatDialogBaseComponent } from '@etop/web';
import { TenantService } from '@etop/state/admin/tenant';
import { UserQuery } from '@etop/state/admin/user';

@Component({
  selector: 'admin-add-hotline-modal',
  templateUrl: './add-hotline-modal.component.html',
  styleUrls: ['./add-hotline-modal.component.scss'],
})
export class AddHotlineModalComponent
  extends MatDialogBaseComponent
  implements OnInit {
  hotlineData = new Hotline({});
  activeUser = this.user.getActive();

  constructor(
    private dialogRef: MatDialogRef<AddHotlineModalComponent>,
    private dialog: MatDialogController,
    private hotlineService: HotlineService,
    private tenantService: TenantService,
    private user: UserQuery
  ) {
    super();
  }

  ngOnInit(): void {}

  closeDialog() {
    this.dialogRef.close(true);
  }

  confirm() {
    if (!this.hotlineData.name) {
      toastr.error('Vui lòng nhập tên hotline');
      return;
    }
    if (!this.hotlineData.hotline) {
      toastr.error('Vui lòng nhập số hotline');
      return;
    }
    const name = this.activeUser.full_name;
    const isFreeChargeText = this.hotlineData.is_free_charge ? 'Không ' : 'Có';
    this.closeDialog();
    const confirmDialog = this.dialog.create({
      template: {
        title: `Xác nhận thêm hotline`,
        content: `
              <div>Bạn có chắc chắn muốn thêm hotline sau vào tổng đài của user <strong>${name}</strong>?</div>
              <div>Tên hotline: <strong>${this.hotlineData.name}</strong></div>
              <div>Số hotline: <strong>${this.hotlineData.hotline}</strong></div>
              <div>Mô tả: <strong>${this.hotlineData.description}</strong></div>
              <div><strong>(${isFreeChargeText} kiểm tra số dư khi gọi)</strong></div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: async () => {
        await this.createHotline();
      },
      afterClosedCb: () => {},
    });
    confirmDialog.open();
  }

  async createHotline() {
    try {
      this.hotlineData.owner_id = this.activeUser.id;
      const hotline = await this.hotlineService.createHotline(this.hotlineData);
      toastr.success(`Thêm hotline thành công.`);
      await this.activateTenant(hotline);
    } catch (e) {
      debug.error('ERROR in create Hotline', e);
      toastr.error(`Thêm hotline thất bại.`, e.code ? e.message || e.msg : '');
    }
  }

  async activateTenant(hotline: Hotline) {
    try {
      await this.tenantService.activateTenant(hotline);
      await this.tenantService.getTenants();
      await this.hotlineService.getHotlines();
      toastr.success(`Kích hoạt tenant thành công.`);
    } catch (e) {
      await this.hotlineService.getHotlines();
      debug.error('ERROR in activateTenant', e);
      toastr.error(
        `Kích hoạt tenant thất bại.`,
        e.code ? e.message || e.msg : ''
      );
    }
  }
}
