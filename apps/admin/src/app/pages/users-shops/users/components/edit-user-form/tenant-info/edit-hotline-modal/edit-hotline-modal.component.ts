import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Hotline } from '@etop/models';
import { MatDialogController } from '@etop/shared';
import { HotlineQuery, HotlineService } from '@etop/state/admin/hotline';
import { MatDialogBaseComponent } from '@etop/web';

@Component({
  selector: 'admin-edit-hotline-modal',
  templateUrl: './edit-hotline-modal.component.html',
  styleUrls: ['./edit-hotline-modal.component.scss'],
})
export class EditHotlineModalComponent
  extends MatDialogBaseComponent
  implements OnInit {
  hotlineActive = this.hotlineQuery.getActive();
  hotlineData = new Hotline({});

  hotlineForm = this.fb.group({
    name: '',
    description: '',
    is_free_charge: false,
  });

  constructor(
    private dialogRef: MatDialogRef<EditHotlineModalComponent>,
    private dialog: MatDialogController,
    private hotlineService: HotlineService,
    private hotlineQuery: HotlineQuery,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this.hotlineForm.patchValue(
      {
        name: this.hotlineActive.name,
        description: this.hotlineActive.description,
        is_free_charge: this.hotlineActive.is_free_charge,
      },
      { emitEvent: false }
    );
  }

  closeDialog() {
    this.dialogRef.close(true);
  }

  confirm() {
    const {
      name,
      description,
      is_free_charge,
    } = this.hotlineForm.getRawValue();
    this.hotlineData = {
      ...this.hotlineActive,
      name,
      description,
      is_free_charge,
    };
    if (!this.hotlineData.name) {
      toastr.error('Vui lòng nhập tên hotline');
      return;
    }
    const isFreeChargeText = this.hotlineData.is_free_charge ? 'Không ' : 'Có';
    this.closeDialog();
    const confirmDialog = this.dialog.create({
      template: {
        title: `Xác nhận chỉnh sửa hotline`,
        content: `
              <div>Bạn có chắc chắn muốn thay đổi thông tin của hotline <strong>${this.hotlineData.hotline}</strong> như sau?</div>
              <div>Tên hotline: <strong>${this.hotlineData.name}</strong></div>
              <div>Mô tả: <strong>${this.hotlineData.description}</strong></div>
              <div><strong>(${isFreeChargeText} kiểm tra số dư khi gọi)</strong></div>
            `,
      },
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmStyle: 'btn btn-primary text-white',
      onConfirm: async () => {
        await this.editHotline();
      },
      afterClosedCb: () => {},
    });
    confirmDialog.open();
  }

  async editHotline() {
    try {
      await this.hotlineService.updateHotline(this.hotlineData);
      toastr.success(`Chỉnh sửa hotline thành công.`);
      this.hotlineService.getHotlines();
    } catch (e) {
      debug.error('ERROR in update Hotline', e);
      toastr.error(
        `Chỉnh sửa hotline thất bại.`,
        e.code ? e.message || e.msg : ''
      );
    }
  }
}
