import { Component, Input, OnInit } from '@angular/core';
import {BaseComponent} from "@etop/core";

@Component({
  selector: 'admin-edit-user-form',
  templateUrl: './edit-user-form.component.html',
  styleUrls: ['./edit-user-form.component.scss']
})
export class EditUserFormComponent extends BaseComponent implements OnInit {
  @Input() activeTab: 'detail_info' | 'sale_info' | 'tenant_info' | 'shop_info' = 'detail_info';

  constructor(
  ) {
    super();
  }

  async ngOnInit() {
  }

}
