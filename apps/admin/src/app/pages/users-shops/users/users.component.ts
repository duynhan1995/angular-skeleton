import { Component, OnInit } from '@angular/core';
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {AuthenticateStore} from "@etop/core";
import { FilterOperator, FilterOptions } from '@etop/models';
import {UserService} from "@etop/state/admin/user";

@Component({
  selector: 'admin-users',
  template: `
    <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
    <ng-template #enoughPermission>
      <etop-filter
        [filters]="filters"
        (filterChanged)="userList.filter($event)">
      </etop-filter>
      <admin-user-list #userList></admin-user-list>
    </ng-template>`
})
export class UsersComponent extends PageBaseComponent implements OnInit {

  filters: FilterOptions = [
    {
      label: 'Tên',
      name: 'name',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Số điện thoại',
      name: 'phone',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Email',
      name: 'email',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Ngày tạo từ',
      name: 'created_from',
      type: 'datetime',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Ngày tạo đến',
      name: 'created_to',
      type: 'datetime',
      fixed: true,
      operator: FilterOperator.eq
    },
  ];

  constructor(
    private auth: AuthenticateStore,
) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/user:view');
  }

  ngOnInit() {
  }

}
