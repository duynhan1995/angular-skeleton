import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {UsersShopsComponent} from "apps/admin/src/app/pages/users-shops/users-shops.component";
import { ManageUsersModalComponent } from './components/manage-users-modal/manage-users-modal.component';
import { EtopMaterialModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: UsersShopsComponent,
    children: [
      {
        path: 'shops',
        loadChildren: () =>
          import('apps/admin/src/app/pages/users-shops/shops/shops.module').then(m => m.ShopsModule)
      },
      {
        path: 'users',
        loadChildren: () =>
          import('apps/admin/src/app/pages/users-shops/users/users.module').then(m => m.UsersModule)
      },
      {
        path: '**',
        redirectTo: 'shops'
      }
    ]
  }
];

@NgModule({
  declarations: [
    UsersShopsComponent,
    ManageUsersModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EtopMaterialModule
  ]
})
export class UsersShopsModule { }
