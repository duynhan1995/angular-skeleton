import {Component, OnInit} from '@angular/core';
import {TicketService} from "@etop/state/admin/ticket";
import {AuthenticateStore} from "@etop/core";

@Component({
  selector: 'admin-tickets',
  template: `
  <etop-not-permission *ngIf="noPermission; else enoughPermission;"></etop-not-permission>
  <ng-template #enoughPermission>
    <admin-ticket-list></admin-ticket-list>
  </ng-template>`,
})
export class TicketsComponent implements OnInit {

  constructor(
    private auth: AuthenticateStore,
    private ticketService: TicketService
  ) {}

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/admin_ticket:view');
  }

  ngOnInit() {
    this.ticketService.prepareData().then();
  }

}
