import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TicketListComponent} from "apps/admin/src/app/pages/tickets/ticket-list/ticket-list.component";
import {TicketsComponent} from "apps/admin/src/app/pages/tickets/tickets.component";
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopMaterialModule,
  EtopPipesModule,
  MaterialModule, NotPermissionModule,
  SideSliderModule
} from "@etop/shared";
import {TicketRowComponent} from './components/ticket-row/ticket-row.component';
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";
import {TicketDetailComponent} from './components/ticket-detail/ticket-detail.component';
import {TicketFulfillmentComponent} from './components/ticket-fulfillment/ticket-fulfillment.component';
import {AuthenticateModule} from "@etop/core";
import {UpdateFulfillmentModule} from "apps/admin/src/app/components/update-fulfillment/update-fulfillment.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {TicketCommentsComponent} from './components/ticket-comments/ticket-comments.component';
import {UpdateTicketCommentPopupComponent} from './components/update-ticket-comment-popup/update-ticket-comment-popup.component';

const routes: Routes = [
  {
    path: '',
    component: TicketsComponent
  }
];

@NgModule({
  declarations: [
    TicketsComponent,
    TicketListComponent,
    TicketRowComponent,
    TicketDetailComponent,
    TicketFulfillmentComponent,
    TicketCommentsComponent,
    UpdateTicketCommentPopupComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    EtopMaterialModule,
    EtopFilterModule,
    EtopCommonModule,
    CallCenterModule,
    EtopPipesModule,
    SideSliderModule,
    AuthenticateModule,
    UpdateFulfillmentModule,
    NotPermissionModule,
    NgbModule
  ]
})
export class TicketsModule {
}
