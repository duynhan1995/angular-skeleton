import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {PageBaseComponent} from "@etop/web/core/base/page.base-component";
import {TicketQuery, TicketService} from "@etop/state/admin/ticket";
import {EtopTableComponent, SideSliderComponent} from "@etop/shared";
import {AuthenticateStore} from "@etop/core";
import { FilterOperator, FilterOptions, Filters, Shop, Ticket } from '@etop/models';
import {AdminShopAPI, AdminShopApi} from "@etop/api";
import {takeUntil} from "rxjs/operators";
import {TicketLabelQuery} from "@etop/state/shop/ticket-labels";
import { ArrayHandler } from '@etop/utils';

@Component({
  selector: 'admin-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('ticketTable', { static: true }) ticketTable: EtopTableComponent;
  @ViewChild('ticketSlider', { static: true }) ticketSlider: SideSliderComponent;

  isInAllView = true;
  showFilterBar = true;
  filters: FilterOptions = [
    {
      label: 'Mã ticket',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Trạng thái',
      name: 'state',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: null },
        { name: "Mới", value: 'new' },
        { name: 'Đã tiếp nhận', value: 'received' },
        { name: 'Đang xử lý', value: 'processing' },
        { name: 'Xử lý thành công', value: 'success' },
        { name: 'Xử lý thất bại', value: 'fail' },
        { name: 'Từ chối xử lý', value: 'ignore' },
        { name: 'Không xác định', value: 'unknown' }
      ]
    },
    {
      label: 'Phân công',
      name: 'assigned_user_id',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: null },
        { name: 'Ticket của tôi', value: this.auth.snapshot.user.id },
        { name: 'Chưa được phân công', value: this.auth.snapshot.user.id },
      ]
    },

  ];

  finishPrepareData$ = this.ticketQuery.select(state => state.ui.finishPrepareData);

  subjectLabels$ = this.labelQuery.select(state => state.ticketSubjectLabels
    .filter(label => {
      const filterProductLabelsIds = this.ticketQuery.getValue().ui.filterProductLabelsIds;
      return !filterProductLabelsIds.length || filterProductLabelsIds.includes(label.parent_id);
    })
  );
  subjectLabelId: string = null;

  detailLabels$ = this.labelQuery.select(state => state.ticketDetailLabels
    .filter(label => {
      const filterSubjectLabelsIds = this.ticketQuery.getValue().ui.filterSubjectLabelsIds;

      if (!filterSubjectLabelsIds.length) {
        const filterProductLabelsIds = this.ticketQuery.getValue().ui.filterProductLabelsIds;
        const _subjectLabels = this.labelQuery.getValue().ticketSubjectLabels.filter(_label => {
          return !filterProductLabelsIds.length || filterProductLabelsIds.includes(_label.parent_id);
        });
        const _subjectLabelsIds = _subjectLabels.map(l => l.id);
        return _subjectLabelsIds.includes(label.parent_id);
      }
      return filterSubjectLabelsIds.includes(label.parent_id);
    })
  );
  detailLabelId: string = null;

  ticketsLoading$ = this.ticketQuery.selectLoading();
  ticketsList$ = this.ticketQuery.selectAll();
  activeTicket$ = this.ticketQuery.selectActive();

  isLastPage$ = this.ticketQuery.select(state => state.ui.isLastPage);
  isEmptyFilterResult$ = this.ticketQuery.select(state => state.ui.isEmptyFilterResult);

  emptyTitle = 'Chưa có ticket nào';

  shopId: string;
  shops: Shop[] = [];
  shopDisplayMap = (shop: Shop) => shop && `${shop.code} - ${shop.name}` || null;
  shopValueMap = (shop: Shop) => shop && shop.id || null;

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private ticketQuery: TicketQuery,
    private ticketService: TicketService,
    private labelQuery: TicketLabelQuery,
    private shopApi: AdminShopApi
  ) {
    super();
  }

  get showPaging() {
    return !this.ticketTable.liteMode && !this.ticketTable.loading;
  }

  get sliderTitle() {
    return 'Chi tiết ticket';
  }
  initFilters() {
    this.filters = [
      {
        label: 'Mã ticket',
        name: 'code',
        type: 'input',
        fixed: true,
        operator: FilterOperator.eq
      },
      {
        label: 'Trạng thái',
        name: 'state',
        type: 'select',
        fixed: true,
        operator: FilterOperator.eq,
        options: [
          { name: 'Tất cả', value: null },
          { name: "Mới", value: 'new' },
          { name: 'Đã tiếp nhận', value: 'received' },
          { name: 'Đang xử lý', value: 'processing' },
          { name: 'Xử lý thành công', value: 'success' },
          { name: 'Xử lý thất bại', value: 'fail' },
          { name: 'Từ chối xử lý', value: 'ignore' },
          { name: 'Không xác định', value: 'unknown' }
        ]
      },
      {
        label: 'Phân công',
        name: 'assigned_user_id',
        type: 'select',
        fixed: true,
        operator: FilterOperator.eq,
        options: [
          { name: 'Tất cả', value: null },
          { name: 'Ticket của tôi', value: this.auth.snapshot.user.id },
          { name: 'Chưa được phân công', value: this.auth.snapshot.user.id },
        ]
      },

    ];
    this.initLabelFilter();
  }
  ngOnInit() {
    this.finishPrepareData$.pipe(takeUntil(this.destroy$))
      .subscribe(finishPrepareData => {
        if (finishPrepareData) {
          let ticketProductLabels = this.labelQuery.getValue().ticketProductLabels;

          ticketProductLabels.forEach((productLabel, pos) => {
            if (productLabel?.code == "topship") {
              ticketProductLabels = ArrayHandler.swap(ticketProductLabels, 0, pos);
            } else if (productLabel?.code == "etelecom") {
              ticketProductLabels = ArrayHandler.swap(ticketProductLabels, 1, pos);
            } else if (productLabel?.code == "etop_pos") {
              ticketProductLabels = ArrayHandler.swap(ticketProductLabels, 2, pos);
            }
          })

          let ticketHeaderOpts = ticketProductLabels.map(productLabel => {
            return {
              onClick: () => this.filterTicketsByProductLabels(productLabel?.code),
              title: productLabel?.name,
              active: false
            }
          });

          ticketHeaderOpts.unshift({
            onClick: () => this.filterTicketsByProductLabels(),
            title: "Tất cả",
            active: true
          })

          this.headerController.setTabs(ticketHeaderOpts);
          this.initLabelFilter();
        }
      });

    this.ticketsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.ticketTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isEmptyFilterResult$.pipe(takeUntil(this.destroy$))
      .subscribe(isEmptyFilterResult => {
        this.emptyTitle = isEmptyFilterResult ? 'Không tìm thấy ticket phù hợp' : 'Chưa có ticket nào';
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.ticketTable.toggleNextDisabled(true);
          this.ticketTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả ticket.');
        }
      });
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.ticketService.setFilter({});
  }

  resetState() {
    this.ticketTable.toggleLiteMode(false);
    this.ticketTable.toggleNextDisabled(false);
    this.ticketSlider.toggleLiteMode(false);
  }

  initLabelFilter() {
    let options = [];
    let {ticketSubjectLabels} = this.labelQuery.getValue();
    ticketSubjectLabels.forEach(subLabel => {
      if (subLabel.children.length) {
        subLabel.children.forEach(detLabel => {
          options.push({
            name: `${subLabel.name} / ${detLabel.name}`,
            value: detLabel.id
          });
        });
      } else {
        options.push({
          name: `${subLabel.name}`,
          value: subLabel.id
        });
      }
    })
    let labelFilter: any = {
      label: 'Label',
      name: 'label_ids',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: options
    };
    this.filters.push(labelFilter);
  }

  toggleFilter() {
    this.showFilterBar = !this.showFilterBar;
  }

  filter(filters: Filters) {
    filters.forEach((filter, pos) => {
      if (filter.name == "label_ids") {
        this.detailLabelId = filter.value;
        this.ticketService.setFilterDetailLabels([this.detailLabelId]);
      }
    })
    this.ticketService.setFilters(filters);
    this.ticketTable.resetPagination();
  }

  getTickets() {
    const finishPrepareData = this.ticketQuery.getValue().ui.finishPrepareData;
    if (finishPrepareData) {
      this.ticketService.getTickets().then();
    } else {
      this.finishPrepareData$.pipe(takeUntil(this.destroy$))
        .subscribe(loading => {
          if (loading) {
            this.ticketService.getTickets().then();
          }
        });
    }
  }

  loadPage({ page, perpage }) {
    this.ticketService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this.getTickets();
  }

  detail(ticket: Ticket) {
    this.ticketService.selectTicket(ticket).then();
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.ticketService.selectTicket(null);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.ticketQuery.getActive();
    this.ticketTable.toggleLiteMode(selected);
    this.ticketSlider.toggleLiteMode(selected);
  }

  filterTicketsByProductLabels(labelCode?: string) {
    if (!labelCode) {
      this.ticketService.setFilterProductLabels([]);
      this.isInAllView = true;
      this.showFilterBar = true;
    } else {
      const label = this.ticketService.findLabelByCode(labelCode);
      this.ticketService.setFilterProductLabels([label.id]);
      this.isInAllView = false;
      this.showFilterBar = false;
    }
    this.initFilters();
    this.ticketService.setFilter({
      assigned_user_id: null,
      code: null,
      state: null
    })
    this.ticketService.setFilterAccountId(null);
    this.shopId = null;
    this.subjectLabelId = null;
    this.detailLabelId = null;
    this.ticketService.setFilterSubjectLabels([]);
    this.ticketTable.resetPagination();
  }

  filterTicketsBySubjectLabels() {
    if (!this.subjectLabelId) {
      this.ticketService.setFilterSubjectLabels([]);
    } else {
      this.ticketService.setFilterSubjectLabels([this.subjectLabelId]);
      this.ticketService.setFilterDetailLabels([]);
    }
    this.detailLabelId = null;
    this.ticketTable.resetPagination();
  }

  filterTicketsByDetailLabels(labelId: string) {
    if (this.detailLabelId == labelId) {
      this.detailLabelId = null;
    } else {
      this.detailLabelId = labelId;
    }
    if (!this.detailLabelId) {
      this.ticketService.setFilterDetailLabels([]);
    } else {
      this.ticketService.setFilterDetailLabels([this.detailLabelId]);
      const label = this.labelQuery.getValue().ticketDetailLabels.find(lab => lab.id == this.detailLabelId);
      this.subjectLabelId = label.parent_id;
    }
    this.ticketTable.resetPagination();
  }

  async onShopSearching(searchText: string) {
    const query: AdminShopAPI.GetShopsRequest = {
      paging: { offset: 0, limit: 1000 },
      filter: { name: searchText }
    };
    this.shops = await this.shopApi.getShops(query);
  }

  onShopSelect() {
    this.ticketService.setFilterAccountId(this.shopId);
    this.ticketTable.resetPagination();
  }

}
