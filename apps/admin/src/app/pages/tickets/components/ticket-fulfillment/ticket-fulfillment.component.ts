import {Component, Input, OnInit} from '@angular/core';
import {FeeType, Fulfillment, Ticket} from "@etop/models";
import {TicketQuery, TicketService} from "@etop/state/admin/ticket";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {UpdateFfmModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-ffm-modal/update-ffm-modal.component";
import {UpdateCompensationAmountModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-compensation-amount-modal/update-compensation-amount-modal.component";
import {UpdateShippingStateModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-state-modal/update-shipping-state-modal.component";
import {UpdateShippingFeeModalComponent} from "apps/admin/src/app/components/update-fulfillment/update-shipping-fee-modal/update-shipping-fee-modal.component";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";

@Component({
  selector: 'admin-ticket-fulfillment',
  templateUrl: './ticket-fulfillment.component.html',
  styleUrls: ['./ticket-fulfillment.component.scss']
})
export class TicketFulfillmentComponent implements OnInit {
  @Input() fulfillment: Fulfillment;

  getTicketRefTypeLoading$ = this.ticketQuery.select(state => state.ui.getTicketRefTypeLoading);

  constructor(
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private ticketService: TicketService,
    private ticketQuery: TicketQuery
  ) { }

  ngOnInit() {}

  selectTicket(ticket: Ticket) {
    this.ticketService.selectTicket(ticket).then();
  }

  doRedelivery() {
    const modal = this.dialog.createConfirmDialog({
      title: `Tính phí kích hoạt giao lại`,
      body: `
<div>Bạn có chắc muốn tính phí kích hoạt giao lại đơn giao hàng <strong>${this.fulfillment.shipping_code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmCss: 'btn btn-primary',
      closeAfterAction: false,
      onConfirm: async() => {
        try {
          await this.ticketService.addFulfillmentShippingFee(this.fulfillment, FeeType.redelivery);
          toastr.success('Tính phí kích hoạt giao lại thành công.');
          modal.close().then();
        } catch(e) {
          debug.error('ERROR in doRedelivery', e);
          toastr.error('Tính phí kích hoạt giao lại không thành công.', e.code && (e.message || e.msg));
        }
      }
    });
    modal.show().then();
  }

  updateFfm() {
    const modal = this.modalController.create({
      component: UpdateFfmModalComponent,
      componentProps: {
        ffm: JSON.parse(JSON.stringify(this.fulfillment))
      },
    });
    modal.show().then();
    modal.onDismiss().then(async(data) => {
      if (data?.ffmId) {
        await this.ticketService.getFulfillmentForTicket(this.ticketQuery.getActive());
        if (data.addShippingFeeAdjustment) {
          this.ticketService.addShippingFeesTelegramMessage(this.fulfillment, FeeType.adjustment).then();
        }
      }
    });
  }

  updateCompensationAmount() {
    const modal = this.modalController.create({
      component: UpdateCompensationAmountModalComponent,
      componentProps: {
        ffm: JSON.parse(JSON.stringify(this.fulfillment))
      },
    });
    modal.show().then();
    modal.onDismiss().then(id => {
      if (id) {
        this.ticketService.getFulfillmentForTicket(this.ticketQuery.getActive()).then();
      }
    });
  }

  updateShippingState() {
    const modal = this.modalController.create({
      component: UpdateShippingStateModalComponent,
      componentProps: {
        ffm: JSON.parse(JSON.stringify(this.fulfillment))
      },
    });
    modal.show().then();
    modal.onDismiss().then(id => {
      if (id) {
        this.ticketService.getFulfillmentForTicket(this.ticketQuery.getActive()).then();
      }
    });
  }

  updateShippingFees() {
    const modal = this.modalController.create({
      component: UpdateShippingFeeModalComponent,
      componentProps: {
        ffm: JSON.parse(JSON.stringify(this.fulfillment))
      },
    });
    modal.show().then();
    modal.onDismiss().then(async(id) => {
      if (id) {
        await this.ticketService.getFulfillmentForTicket(this.ticketQuery.getActive());
        this.ticketService.updateShippingFeesTelegramMessage(this.fulfillment).then();
      }
    });
  }

}
