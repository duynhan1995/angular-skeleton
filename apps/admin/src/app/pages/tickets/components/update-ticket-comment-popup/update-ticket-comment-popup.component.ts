import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TicketComment} from "@etop/models";
import {TicketService} from "@etop/state/admin/ticket";
import {MatDialogBaseComponent} from "@etop/web";
import {UtilService} from "apps/core/src/services/util.service";

export interface UpdateTicketCommentData {
  comment: TicketComment;
  updateType: 'text' | 'image';
}

const ACCEPT_IMAGE_FILE_EXTENSION = 'image/png,image/jpg,image/jpeg,image/gif';

@Component({
  selector: 'admin-update-ticket-comment-popup',
  templateUrl: './update-ticket-comment-popup.component.html',
  styleUrls: ['./update-ticket-comment-popup.component.scss']
})
export class UpdateTicketCommentPopupComponent extends MatDialogBaseComponent implements OnInit {
  @ViewChild('imageMessageInput', {static: false}) imageMessageInput: ElementRef;

  constructor(
    private dialogRef: MatDialogRef<UpdateTicketCommentPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UpdateTicketCommentData,
    private util: UtilService,
    private ticketService: TicketService
  ) {
    super();
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  uploadImage() {
    this.imageMessageInput.nativeElement.click();
  }

  async onImageSelected(event) {
    try {
      if (Object.values(event.target.files).some((f: File) => !ACCEPT_IMAGE_FILE_EXTENSION.includes(f.type))) {
        toastr.error('Định dạng file không hợp lệ. Chỉ chấp nhận các định dạng: .png, .jpg, .jpeg');
        return;
      }

      const files: File[] = Object.values(event.target.files);
      const file = files[0];
      if (file) {
        const image: any = await this.util.fileToImage(file);
        this.data.comment.image_url = image.src;
        this.ticketService.updateTicketCommentPreUploadImagesFiles(this.data.comment.id, file);
      }
    } catch (e) {
      debug.error('ERROR in onImageSelected', e);
      toastr.error('Có lỗi xảy ra khi chọn hình ảnh.', e.code && (e.message || e.msg));
    }
  }

  confirm() {
    const {message, image_url} = this.data.comment;
    if (!(message || image_url)) {
      return toastr.error('Vui lòng nhập nội dung hoặc chọn hình ảnh để chỉnh sửa phản hồi!');
    }
    this.dialogRef.close(this.data);
  }

}
