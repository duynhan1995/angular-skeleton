import {Component, OnInit} from '@angular/core';
import {TicketQuery, TicketService} from "@etop/state/admin/ticket";
import {AdminUser, Fulfillment, Ticket} from "@etop/models";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {DialogControllerService} from "apps/core/src/components/modal-controller/dialog-controller.service";
import {Observable} from "rxjs";

@Component({
  selector: 'admin-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent extends BaseComponent implements OnInit {

  adminUsers$ = this.ticketQuery.select("adminUsers");
  activeTicket$: Observable<Ticket> = this.ticketQuery.selectActive();
  activeTicketFulfillment$ = this.ticketQuery.select("activeTicketFulfillment");

  activeTicketCommentsLoading$ = this.ticketQuery.select(state => state.ui.getTicketCommentsLoading);

  loading = false;

  constructor(
    private auth: AuthenticateStore,
    private dialogController: DialogControllerService,
    private ticketQuery: TicketQuery,
    private ticketService: TicketService
  ) {
    super();
  }

  get canConfirmTicket() {
    const ticket = this.ticketQuery.getActive();
    if (this.auth.snapshot.permission.permissions.includes('admin/admin_lead_ticket:update')) {
      return !['success', 'fail', 'ignore', 'processing'].includes(ticket.state);
    }
    return ticket.state == 'received' && (ticket.assigned_user_ids[0] == this.auth.snapshot.user.id);
  }

  get canCloseTicket() {
    const ticket = this.ticketQuery.getActive();
    if (this.auth.snapshot.permission.permissions.includes('admin/admin_lead_ticket:update')) {
      return !['success', 'fail', 'ignore'].includes(ticket.state);
    }
    return ticket.state == 'processing' && (ticket.confirmed_by == this.auth.snapshot.user.id);
  }

  get permissionLead() {
    return this.auth.snapshot.permission.permissions.includes('admin/admin_lead_ticket:update');
  }

  ngOnInit() {}

  selfAssignTicket() {
    const ticket = this.ticketQuery.getActive();
    const dialog = this.dialogController.createConfirmDialog({
      title: 'Tự phân công',
      body: `
<div>Bạn có chắc muốn tự phân công cho mình xử lý ticket <b>${ticket.code}</b></div>
<div>- ${ticket.title} ${ticket.ref_type_display?.toLowerCase()}
  <span class="text-primary text-bold">${ticket.ref_code}</span>
</div>
<div>- Nội dung: ${ticket.description}</div>`,
      closeAfterAction: false,
      confirmTitle: 'Xác nhận',
      onConfirm: async () => {
        try {
          await this.ticketService.assignUserToTicket([this.auth.snapshot.user.id], ticket.id);
          dialog.close().then();
          toastr.success('Tự phân công thành công.');
        } catch (e) {
          toastr.error('Tự phân công không thành công.', e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  assignTicket(admin: AdminUser) {
    const ticket = this.ticketQuery.getActive();
    const dialog = this.dialogController.createConfirmDialog({
      title: 'Phân công admin',
      body: `
<div>Bạn có chắc muốn phân công cho <b>${admin.full_name}</b> xử lý ticket <b>${ticket.code}</b></div>
<div>- ${ticket.title} ${ticket.ref_type_display?.toLowerCase()}
  <span class="text-primary text-bold">${ticket.ref_code}</span>
</div>
<div>- Nội dung: ${ticket.description}</div>`,
      closeAfterAction: false,
      confirmTitle: 'Xác nhận',
      onConfirm: async () => {
        try {
          await this.ticketService.assignUserToTicket([admin.user_id], ticket.id);
          dialog.close().then();
          toastr.success('Phân công admin cho ticket thành công.');
        } catch (e) {
          toastr.error('Phân công admin cho ticket không thành công.', e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  confirmTicket() {
    const ticket = this.ticketQuery.getActive();
    if (!ticket.assignedUsers[0]) {
      return;
    }
    const dialog = this.dialogController.createConfirmDialog({
      title: 'Bắt đầu xử lý ticket',
      body: `
<div>Bạn có chắc muốn xử lý ticket <b>${ticket.code}</b></div>
<div>- ${ticket.title} ${ticket.ref_type_display.toLowerCase()}
  <span class="text-primary text-bold">${ticket.ref_code}</span>
</div>
<div>- Nội dung: ${ticket.description}</div>`,
      closeAfterAction: false,
      confirmTitle: 'Bắt đầu xử lý',
      onConfirm: async () => {
        try {
          await this.ticketService.confirmTicket(ticket.id);
          dialog.close().then();
        } catch (e) {
          toastr.error('Có lỗi xảy ra.', e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  closeTicket(state) {
    const ticket = this.ticketQuery.getActive();
    const dialog = this.dialogController.createConfirmDialog({
      title: 'Hoàn thành ticket',
      body: `
<div>Bạn có chắc hoàn thành ticket <b>${ticket.code}</b></div>
<div>- ${ticket.title} ${ticket.ref_type_display.toLowerCase()}
  <span class="text-primary text-bold">${ticket.ref_code}</span>
</div>
<div class="d-flex align-items-center">- Trạng thái:&nbsp;
  <span class="status status-${state} text-bold">${Ticket.ticketStateMap(state)}</span>
</div>
<div>- Nội dung: ${ticket.description}</div>`,
      closeAfterAction: false,
      confirmTitle: 'Hoàn thành',
      onConfirm: async () => {
        try {
          await this.ticketService.closeTicket(state, ticket.id);
          dialog.close().then();
          toastr.success('Đã hoàn thành ticket.');
        } catch (e) {
          toastr.error('Không thể hoàn thành ticket.', e.code && (e.message || e.msg));
        }
      }
    });
    dialog.show().then();
  }

  shopDetail(event) {
    if (event.target.id == 'phone-call' || event.target.parentElement.id == 'phone-call') {
      return;
    }
    window.open(`/accounts/shops`, '_blank')
  }

  /**
   * @deprecated temporarily used while waiting for new APIs to push Ticket to Every Connection
   */
  async pushTicketToGHN() {
    this.loading = true;
    try {
      const ticket = this.ticketQuery.getActive();
      const fulfillment = this.ticketQuery.getValue().activeTicketFulfillment;

      if (!fulfillment) { return; }

      const body = {
        etop_id: fulfillment.shop?.owner_id,
        order_id: fulfillment.order?.id,
        order_code: fulfillment.order?.code,
        ffm_id: fulfillment.id,
        ffm_code: fulfillment.shipping_code,
        company: fulfillment.shop?.name,
        account: {
          ...this.auth.snapshot.user,
          company: fulfillment.shop.name
        },
        provider: fulfillment.carrier_display.toUpperCase(),

        code: ticket.labels.map(l => l.code).join(","),
        title: ticket.labels.map(l => l.name).join(","),
        reason: ticket.description
      };

      await this.ticketService.createVtigerTicket(body);

      toastr.success('Đẩy ticket qua GHN thành công.');

    } catch (e) {
      debug.error('ERROR in pushTicketToGHN', e);
      toastr.error('Đẩy ticket qua GHN không thành công.');
    }
    this.loading = false;
  }

}
