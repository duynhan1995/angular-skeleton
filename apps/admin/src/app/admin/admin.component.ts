import { Component, OnInit } from '@angular/core';
import { CommonLayout } from 'apps/core/src/app/CommonLayout';
import { AuthenticateStore } from '@etop/core';
import { MenuItem } from 'apps/core/src/components/menu-item/menu-item.interface';
import { ConnectionService } from '@etop/features';
import { BankService } from '@etop/state/bank';

@Component({
  selector: 'admin-admin',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent extends CommonLayout implements OnInit {
  sideMenus: MenuItem[] = [
    {
      name: 'Thống kê',
      route: ['/', 'dashboard'],
      matIcon: 'insert_chart',
      matIconOutlined: true,
      isDivider: true,
      permissions: ['admin/dashboard:view'],
    },
    {
      name: 'Đơn giao hàng ',
      route: ['/', 'fulfillments'],
      matIcon: 'local_shipping',
      matIconOutlined: true,
      permissions: ['admin/fulfillment:view'],
    },
    {
      name: 'Khách hàng',
      route: ['/', 'accounts'],
      matIcon: 'account_box',
      matIconOutlined: true,
      permissions: ['admin/shop:view', 'admin/user:view'],
      display_submenus: true,
      submenus: [
        { name: 'Shop', route: ['accounts','shops'],},
        { name: 'User', route: ['accounts','users']},
      ],
    },
    {
      name: 'Phiên đối soát',
      route: ['/', 'transactions'],
      matIcon: 'paid',
      matIconOutlined: true,
      display_submenus: true,
      submenus: [
        { name: 'Phiên nhà vận chuyển', route: ['transactions','externals'] },
        { name: 'Phiên shop', route: ['transactions','shops'] },
        { name: 'Phiên chuyển khoản', route: ['transactions','transfers'] },
        { name: 'Phiên dự trù', route: ['transactions','scheduleds'] },
        { name: 'Tách phiên', route: ['transactions','splits'] },
      ],
      permissions: [
        'admin/money_transaction_shipping_external:view',
        'admin/money_transaction_shipping_etop:view',
        'admin/money_transaction:view',
      ],
    },
    {
      name: 'Phiếu nạp tiền',
      route: ['/', 'credits'],
      matIcon: 'request_quote',
      matIconOutlined: true,
      permissions: ['admin/credit:view'],
    },
    {
      name: 'Subscription',
      route: ['/', 'subscriptions'],
      matIcon: 'subscriptions',
      matIconOutlined: true,
      permissions: ['admin/subscription:view'],
      display_submenus: true,
      submenus: [
        { name: 'Invoice', route: ['subscriptions','bills'] },
        { name: 'Plans', route: ['subscriptions','plans'] },
        { name: 'Product', route: ['subscriptions','products'] },
      ],
    },
    {
      name: 'Hóa đơn và giao dịch',
      route: ['/', 'invoice-transaction'],
      matIcon: 'receipt',
      matIconOutlined: true,
      display_submenus: true,
      submenus: [
        { name: 'Hóa đơn', route: ['invoice-transaction','invoice'] },
        { name: 'Giao dịch', route: ['invoice-transaction','transaction'] },
      ],
    },
    {
      name: 'Ticket',
      route: ['/', 'tickets'],
      matIcon: 'support',
      matIconOutlined: true,
      permissions: ['admin/admin_ticket:view'],
    },
    {
      name: 'Call Center',
      route: ['/', 'call-center'],
      icon: 'assets/images/admin/icon_callcenter.png',
      hidden: true,
    },
    {
      name: 'Tổng đài',
      route: ['/', 'voip'],
      matIcon: 'group_work',
      matIconOutlined: true,
      permissions: ['admin/tenant:view'],
      isDivider: true,
      submenus: [
        { name: 'Tenant', route: ['voip', 'tenant'] },
        { name: 'Hotline', route: ['voip','hotline'] },
      ],
    },
    {
      name: 'Thiết lập vận chuyển',
      route: ['/', 'shipping-settings'],
      matIcon: 'perm_data_setting',
      matIconOutlined: true,
      permissions: [
        'admin/connection_service:view',
        'admin/shipment_service:view',
        'admin/shipment_price_list:view',
        'admin/shipment_price_list_promotion:view',
        'admin/custom_region:view',
        'admin/shipment_price:view',
      ],
      display_submenus: true,
      submenus: [
        { name: 'Thiết lập NVC', route: ['shipping-settings','connections'] },
        { name: 'Dịch vụ', route: ['shipping-settings','services'] },
        { name: 'Bảng giá', route: ['shipping-settings','price-lists'] },
        { name: 'Khuyến mãi', route: ['shipping-settings','promotions'] },
        { name: 'Vùng tự định nghĩa', route: ['shipping-settings','custom-regions'] },
      ],
    },
    {
      name: 'Thiết lập',
      route: ['/', 'settings'],
      matIcon: 'settings',
      matIconOutlined: true,
      permissions: ['admin/admin_user:view', 'admin/admin_ticket:view'],
      display_submenus: true,
      submenus: [
        { name: 'Quản lý Admin', route: ['settings','accounts'] },
        { name: 'Quản lý Label', route: ['settings','labels'] },
        { name: 'Quản lý Tổng đài', route: ['settings','callcenter'], hidden: true },
      ],
    },
  ];
  showNewSidebar = true;

  constructor(
    private auth: AuthenticateStore,
    private connectionService: ConnectionService,
    private bankService: BankService
  ) {
    super(auth);
  }

  ngOnInit() {
    this.connectionService.initConnections(true).then();
    this.bankService.initBanks().then();
  }
}
