import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EtopPipesModule } from '@etop/shared';
import { AdminComponent } from './admin.component';
import { CoreModule } from 'apps/core/src/core.module';
import { AdminRoutingModule } from 'apps/admin/src/app/admin/admin-routing.module';
import { AdminGuard } from 'apps/admin/src/app/admin/admin.guard';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AdminCommonUsecase } from 'apps/admin/src/app/usecases/admin-common.usecase.service';
import { LoginComponent } from 'apps/admin/src/app/pages/login/login.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AdminComponent, LoginComponent],
  providers: [
    { provide: CommonUsecase, useClass: AdminCommonUsecase },
    AdminGuard
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    AdminRoutingModule,
    EtopPipesModule
  ]
})
export class AdminModule { }
