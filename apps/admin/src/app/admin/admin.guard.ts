import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticateStore } from '@etop/core';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(
    private auth: AuthenticateStore,
    private router: Router
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.auth.snapshot.account || !this.auth.snapshot.user) {
      this.router.navigate(['/login']);
      return false;
    }
    return !!this.auth.snapshot.account && !!this.auth.snapshot.user;
  }

}
