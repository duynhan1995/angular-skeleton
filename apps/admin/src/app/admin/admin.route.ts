import { Routes } from '@angular/router';

export const adminRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'fulfillments',
    loadChildren: () => import('../pages/fulfillments/fulfillments.module').then(m => m.FulfillmentsModule)
  },
  {
    path: 'accounts',
    loadChildren: () => import('apps/admin/src/app/pages/users-shops/users-shops.module').then(m => m.UsersShopsModule)
  },
  {
    path: 'transactions',
    loadChildren: () => import('../pages/transactions/transactions.module').then(m => m.TransactionsModule)
  },
  {
    path: 'credits',
    loadChildren: () => import('../pages/credits/credits.module').then(m => m.CreditsModule)
  },
  {
    path: 'subscriptions',
    loadChildren: () => import('../pages/subscriptions/subscriptions.module').then(m => m.SubscriptionsModule)
  },
  {
    path: 'tickets',
    loadChildren: () => import('../pages/tickets/tickets.module').then(m => m.TicketsModule)
  },
  {
    path: 'call-center',
    loadChildren: () => import('../pages/call-center-old/call-center-old.module').then(m => m.CallCenterOldModule)
  },
  {
    path: 'shipping-settings',
    loadChildren: () => import('../pages/shipping-settings/shipping-settings.module').then(m => m.ShippingSettingsModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'voip',
    loadChildren: () => import('../pages/voip/voip.module').then(m => m.VoipModule)
  },
  {
    path: 'invoice-transaction',
    loadChildren: () => import('../pages/invoice-transaction/invoice-transaction.module').then(m => m.AdminInvoiceTransactionModule)
  },
  {
    path: '**',
    redirectTo: 'accounts'
  }
];
