import {Component, OnInit} from '@angular/core';
import {CommonUsecase} from "apps/shared/src/usecases/common.usecase.service";
import {LocationService} from "@etop/state/location";

@Component({
  selector: 'admin-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private commonUsecase: CommonUsecase,
    private locationService: LocationService
  ) {}

  ngOnInit() {
    this.commonUsecase.checkAuthorization().then();
    this.locationService.initLocations().then();
  }
}
