import { Injectable } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AuthenticateStore } from '@etop/core';
import { Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { UserApi } from '@etop/api';

@Injectable()
export class AdminCommonUsecase extends CommonUsecase {

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore
  ) {
    super();
  }

  async checkAuthorization() {
    const ref = this.router.url.split('?')[0];
    try {
      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo();

      await this.navigateAfterAuthorized();
    } catch (e) {
      debug.error('ERROR in checkAuthorization', e);
      this.auth.clear();
      this.auth.setRef(ref);
      await this.router.navigate(['/login']);
    }
  }

  async navigateAfterAuthorized() {
    let ref = this.auth.getRef(true).replace('/', '');
    const defaultRef = 'accounts';
    if (ref == 'login') {
      await this.router.navigate([`/${defaultRef}`]);
    } else {
      await this.router.navigateByUrl(ref);
    }
  }

  async login(data: { login: string; password: string }) {
    try {
      const res = await this.userApi.login({
        login: data.login,
        password: data.password,
        account_type: 'etop'
      });

      this.auth.updateToken(res.access_token);
      this.auth.updateUser(res.user);
      await this.setupAndRedirect();
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
  }

  async setupAndRedirect() {
    await this.updateSessionInfo();
    const ref = this.auth.getRef(true).replace('/', '');
    if (ref == 'login') {
      await this.router.navigateByUrl('accounts/users');
    } else {
      await this.router.navigateByUrl(ref || 'accounts/users');
    }
  }

  async updateSessionInfo() {
    const res = await this.userService.checkToken(this.auth.snapshot.token);
    let { access_token, account, user } = res;

    if (!account || account.type != "etop") {
      toastr.error("You Do Not Have Sufficient Permissions To Access This Page.");
      this.auth.clear();
      const ref = this.router.url.split('?')[0];
      this.auth.setRef(ref);
      this.router.navigateByUrl('/login');
      return;
    }

    this.auth.updateInfo({
      token: access_token,
      permission: account.user_account.permission,
      account,
      user,
      isAuthenticated: true,
      uptodate: true,
    });
  }

  async redirectIfAuthenticated(): Promise<any> {
    return this.checkAuthorization();
  }

  async register() {}

}
