import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {CoreModule} from 'apps/core/src/core.module';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule, DatePipe, DecimalPipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@etop/shared/components/etop-material/material';
import {CONFIG_TOKEN} from '@etop/core';
import {environment} from '../environments/environment';
import {CallCenterModule} from "apps/admin/src/app/modules/callcenter/callcenter.module";

import {registerLocaleData} from '@angular/common';
import localeVi from '@angular/common/locales/vi';
import {LoginComponent} from "apps/admin/src/app/pages/login/login.component";
import {CommonUsecase} from "apps/shared/src/usecases/common.usecase.service";
import {AdminCommonUsecase} from "./usecases/admin-common.usecase.service";
import {UtilService} from "apps/core/src/services/util.service";
import {TelegramService} from "@etop/features";
import {PromiseQueueService} from "apps/core/src/services/promise-queue.service";

registerLocaleData(localeVi);

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    loadChildren: () =>
      import('apps/admin/src/app/admin/admin.module').then(m => m.AdminModule)
  }
];

const services = [
  UtilService,
  TelegramService,
  PromiseQueueService
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule.forRoot(),
    SharedModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    CallCenterModule,
  ],
  providers: [
    {provide: CommonUsecase, useClass: AdminCommonUsecase},
    {provide: CONFIG_TOKEN, useValue: environment},
    {provide: LOCALE_ID, useValue: 'vi'},
    ...services,
    DecimalPipe,
    DatePipe
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminAppModule {
}
