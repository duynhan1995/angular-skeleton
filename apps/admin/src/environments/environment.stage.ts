export const environment = {
  production: true,
  hmr: false,
  ticket_environment: "development",
  crm_url: "https://crm-service.g.etop.vn"
};
