export const environment = {
  production: true,
  hmr: false,
  ticket_environment: "production",
  crm_url: "https://crm-service.etop.vn"
};
