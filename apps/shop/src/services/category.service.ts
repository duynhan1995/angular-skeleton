import { Injectable } from '@angular/core';
import { CategoryApi } from '@etop/api';

@Injectable()
export class CategoryService {

  constructor(
    private categoryApi: CategoryApi
  ) { }

  getCategories(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };
    return this.categoryApi.getCategories({ paging, filters });
  }

  createCategory(data) {
    return this.categoryApi.createCategory(data);
  }

}
