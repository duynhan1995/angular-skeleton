import { Injectable } from '@angular/core';
import { StocktakeApi } from '@etop/api';

@Injectable()
export class StocktakeService {
  constructor(private stocktakeApi: StocktakeApi) { };

  async getStocktakes(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return await this.stocktakeApi.getStocktakes({ paging, filters });
  }

}

