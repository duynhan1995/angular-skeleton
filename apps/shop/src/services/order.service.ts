import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';
import { requireMethodRoles } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import List from 'identical-list';
import { Subject } from 'rxjs';
import {FulfillmentApi, OrderApi, AccountApi} from '@etop/api';
import {Order, ORDER_STATUS} from 'libs/models/Order';
import { FilterOperator, Filters } from '@etop/models';
import {UserBehaviourTrackingService} from "apps/core/src/services/user-behaviour-tracking.service";
import {Fulfillment} from "libs/models/Fulfillment";
import {Connection} from "libs/models/Connection";
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {distinctUntilChanged, map} from "rxjs/operators";

const ORDER_SOURCE = {
  ts_app: 'TOPSHIP App',
  unknown: 'Không xác định',
  default: 'Mặc định',
  etop_pos: 'eTop POS',
  haravan: 'Haravan',
  etop_pxs: 'eTop POS Extension',
  self: 'Nhập hàng',
  import: 'Import',
  etop_cmx: 'CMX',
  api: 'API'
};

@Injectable()
export class OrderService {
  onOrderLinesChanged$ = new Subject();
  onOrderInfoChanged$ = new Subject();
  onOrderCustomerChanged$ = new Subject();
  createOrderFailed$ = new Subject();
  createOrderSuccess$ = new Subject();
  switchOffCreatingFFM$ = new Subject();

  orderList = new List();
  tryOnOptions = [
    {
      code: 'try',
      value: 'try',
      name: 'Cho thử hàng'
    },
    {
      code: 'open',
      value: 'open',
      name: 'Cho xem hàng không thử'
    },
    {
      code: 'none',
      value: 'none',
      name: 'Không cho xem hàng'
    }
  ];

  constructor(
    private http: HttpService,
    private accountApi: AccountApi,
    private util: UtilService,
    private orderApi: OrderApi,
    private fulfillmentApi: FulfillmentApi,
    private ubtService: UserBehaviourTrackingService,
    private connectionStore: ConnectionStore
  ) {}

  async orderSourceMap(source, partner_id) {
    if (partner_id != '0' && ['etop_cmx', 'api'].indexOf(source) != -1) {
      const _partner = await this.accountApi.getPublicPartnerInfo(partner_id);
      return _partner
        ? `${ORDER_SOURCE[source]} - ${_partner.name}`
        : ORDER_SOURCE[source];
    }
    return source ? ORDER_SOURCE[source] : 'Không xác định';
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: true,
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  async checkEmptyOrders(shop_id) {
    return await this.orderApi.checkEmptyOrders(shop_id);
  }

  getOrder(id): Promise<Order> {
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(cons => {
        if (cons) {
          this.orderApi.getOrder(id)
            .then(order => {
              order.activeFulfillment = this.getActiveFulfillment(order, cons);
              resolve(order);
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  // TODO: Temporarily closed until dashboard works smoothly
  // @requireMethodRoles({
  //   skipError: true,
  //   valueOnError: [],
  //   roles: ['admin', 'owner', 'salesman', 'accountant']
  // })
  async getOrders(start?: number, perpage?: number, filters?: Filters): Promise<Order[]> {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };

    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.orderApi.getOrders({ paging, filters })
            .then(orders => {
              orders.map((order, index) => {
                order.activeFulfillment = this.getActiveFulfillment(order, connections);
                return this.mapOrderInfo(order, index);
              });
              resolve(orders);
            })
            .catch(err => reject(err));
        }
      });
    });
  }


  getOrdersMoveCrop(ids): Promise<Order[]> {
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(cons => {
        if (cons) {
          this.orderApi.getOrdersByIDs(ids).then(res =>{
            resolve(res);
          }).catch(err => reject(err));
        }
      });
    });
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: null,
    roles: ['admin', 'owner', 'salesman', 'accountant']
  })
  getShopOrders(token, {paging : {limit}}) {
    return this.orderApi.getShopOrders(token, { paging: { limit} });
  }

  createOrder(body) {
    const data = {...body};
    delete data.o_data;
    delete data.p_data;
    for (let key in data) {
      if (!data[key]) { continue; }
      if (data[key].o_data) { delete data[key].o_data; }
      if (data[key].p_data) { delete data[key].p_data; }
    }
    return this.orderApi.createOrder(data)
      .then(res => {
        if (res.id) {
          this.ubtService.sendUserBehaviour('CreateOrder');
        }
        return res;
      });
  }

  updateOrder(body) {
    return this.orderApi.updateOrder(body);
  }

  async importOrder(formData) {
    return await this.orderApi.importOrder(formData);
  }

  getOrdersByIds(ids) {
    let subset = this.orderList.asArray();
    return subset.filter(order => ids.indexOf(order.id) > -1);
  }

  mapOrderInfo(order: Order, index) {
    return {
      ...order,
      index,
      search_text: this.util.makeSearchText(`${order.code}${order.customer.full_name}${order.customer.phone}`)
    }
  }

  mapSpecialFilters(filters: Filters) {
    filters.forEach(f => {
      if (f.value == '{}') {
        f.op = FilterOperator.eq;
        f.name = 'fulfillment.ids'
      }
    });
  }

  getActiveFulfillment(order: Order, connections: Connection[]) {
    let activeFulfillment: any;
    switch (order.fulfillment_type) {
      case 'shipment':
        const shipmentFfms = order.fulfillments.map(ffm => ffm.shipment);
        const _activeShipmentFfm = shipmentFfms.find(ffm => ffm.status != 'N');
        activeFulfillment = _activeShipmentFfm || shipmentFfms[0];
        break;
      case 'shipnow':
        const shipnowFfms = order.fulfillments.map(ffm => ffm.shipnow);
        const _activeShipnowFfm = shipnowFfms.find(ffm => ffm.status != 'N');
        activeFulfillment = _activeShipnowFfm || shipnowFfms[0];
        break;
      default:
        const _activeFfm = order.fulfillments.find(ffm => ffm.status != 'N');
        activeFulfillment = _activeFfm || order.fulfillments[0];
        break;
    }

    return Fulfillment.fulfillmentMap(activeFulfillment, connections);
  }

  completeOrder (order: Order) {
    return this.orderApi.completeOrder(order.id);
  }

  statusMap (status) {
    return ORDER_STATUS[status] || 'Không xác định';
  }

  cancelOrder (order,reason) {
    return this.orderApi.cancelOrder(
      order.id,
      reason,
      'confirm'
    );
  }
}
