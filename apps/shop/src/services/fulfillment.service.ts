import { Injectable } from '@angular/core';
import * as moment from 'moment';
import List from 'identical-list';
import {Fulfillment, ShipmentCarrier, FulfillmentShipmentService, ShipnowCarrier} from 'libs/models/Fulfillment';
import { Subject } from 'rxjs';
import { CmsService } from 'apps/core/src/services/cms.service';
import {FulfillmentAPI, FulfillmentApi} from '@etop/api';
import {Filters} from "@etop/models";
import {distinctUntilChanged, map} from "rxjs/operators";
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {StringHandler} from "@etop/utils";

interface ForceItem {
  id: string;
  type: string;
  expireTime: number;
}

@Injectable()
export class FulfillmentService {
  shipping_type = 'shipment';

  weight_calc_type = 1;
  /* 1: Khoi luong (chargeable_weight)
  ** 2: The tich (length x width x height)
   */

  forceList: List<ForceItem>;

  onResetData$ = new Subject();
  onViewReturningFfms$ = new Subject();

  private __secretCode = 'fulfillment-data';

  static autoSelectShipmentServiceByConnectionId(connectionId: string, services: FulfillmentShipmentService[]) {
    return services.filter(s => s.connection_info.id == connectionId);
  }

  static autoSelectShipmentServiceByMinFee(services: FulfillmentShipmentService[]) {
    const _services: FulfillmentShipmentService[] = JSON.parse(JSON.stringify(services));
    _services.sort((a, b) => {
      if (a.fee == b.fee) {
        if (!a.estimated_delivery_at) {
          return 1;
        }
        return new Date(a.estimated_delivery_at).getTime() - new Date(b.estimated_delivery_at).getTime();
      }
      return a.fee - b.fee;
    });
    return _services;
  }

  static autoSelectShipmentServiceByMinDeliveryTime(services: FulfillmentShipmentService[]) {
    const _services: FulfillmentShipmentService[] = JSON.parse(JSON.stringify(services));
    const nullTimes = _services.filter(s => !s.estimated_delivery_at);
    const hasTimes = _services.filter(s => !!s.estimated_delivery_at);
    hasTimes.sort((a, b) => {
      if (new Date(a.estimated_delivery_at).getTime() == new Date(b.estimated_delivery_at).getTime()) {
        return a.fee - b.fee;
      }
      return new Date(a.estimated_delivery_at).getTime() - new Date(b.estimated_delivery_at).getTime();
    });
    return hasTimes.concat(nullTimes);
  }

  constructor(
    private fulfillmentApi: FulfillmentApi,
    private cms: CmsService,
    private connectionStore: ConnectionStore,
  ) {
    this.getLocalForceList();
  }

  resetData() {
    this.shipping_type = 'shipment';
    this.weight_calc_type = 1;
    this.onResetData$.next();
  }

  async getFulfillments(start?: number, perpage?: number, filters?: Filters) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.fulfillmentApi.getFulfillments({ paging, filters })
            .then(res => {
              res.fulfillments = res.fulfillments.map(f => Fulfillment.fulfillmentMap(f, connections));
              resolve(res);
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  getShipmentServices(body) {
    return this.fulfillmentApi.getShipmentServices(body);
  }

  async getShipnowFulfillments(start?: number, perpage?: number, filters?: Filters) {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };

    return this.fulfillmentApi.getShipnowFulfillments({paging, filters})
      .then(res => {
        res.shipnow_fulfillments = res.shipnow_fulfillments.map(f => Fulfillment.fulfillmentMap(f, []));
        return res;
      });
  }

  getShipnowFulfillment(id, token?) {
    return this.fulfillmentApi.getShipnowFulfillment(id, token)
      .then(res => Fulfillment.fulfillmentMap(res, []));
  }

  getShipnowServices(body) {
    return this.fulfillmentApi.getShipnowServices(body);
  }

  async createShipnowFulfillment(body: FulfillmentAPI.CreateShipnowFulfillmentRequest) {
    return this.fulfillmentApi.createShipnowFulfillment(body);
  }

  async confirmShipnowFulfillment(shipnow_id) {
    return this.fulfillmentApi.confirmShipnowFulfillment(shipnow_id);
  }

  async cancelShipnowFulfillment(shipnow_id, cancel_reason) {
    return this.fulfillmentApi.cancelShipnowFulfillment(shipnow_id, cancel_reason);
  }

  checkStatusForceItem(shipping_code: string) {
    const item = this.forceList.get(shipping_code);
    if (!item) {
      return false;
    }
    const now = moment.now();
    return item.expireTime > now;
  }

  getLocalForceList() {
    const data: any = localStorage.getItem(this.__secretCode);
    const list = data ? JSON.parse(data).forceList : [];
    const now = moment.now();
    this.forceList = new List(list.filter(i => i.expireTime > now));
  }

  addForceItem(shipping_code, type) {
    const expireTime = moment(moment.now())
      .add(6, 'hours')
      .valueOf();
    this.forceList.add({
      id: shipping_code,
      type,
      expireTime
    });
    this.updateLocalForceList();
  }

  updateLocalForceList() {
    const data = { forceList: this.forceList.asArray() };
    localStorage.setItem(this.__secretCode, JSON.stringify(data));
  }

  trackingLink(fulfillment: Fulfillment) {
    if (!fulfillment) {
      return null;
    }
    switch (fulfillment.carrier) {
      case 'ghn':
        return `https://donhang.ghn.vn/?order_code=${fulfillment.shipping_code}&code=${fulfillment.shipping_code}`;
      case 'vtpost':
        return `https://old.viettelpost.com.vn/Tracking?KEY=${fulfillment.shipping_code}`;
      case 'ahamove':
        return fulfillment.shipping_shared_link;
      default:
        return null;
    }
  }

  public exportFulfillment(export_filters) {
    return this.fulfillmentApi.requestExportFulfillmentExcel(export_filters);
  }

  canForcePicking(fulfillment: Fulfillment) {
    if (!fulfillment) { return false; }
    const { shipping_state } = fulfillment;
    return shipping_state === 'picking' || shipping_state === 'created' || shipping_state === 'confirmed';
  }

  canForceDelivering(fulfillment: Fulfillment) {
    if (!fulfillment) { return false; }
    const { shipping_state } = fulfillment;
    return shipping_state === 'delivering' || shipping_state === 'holding';
  }

  getFulfillment(id, token?): Promise<Fulfillment> {
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.fulfillmentApi.getFulfillment(id, token)
            .then(ffm => {
              resolve(Fulfillment.fulfillmentMap(ffm, connections));
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  canSendSupportRequest(fulfillment: Fulfillment) {
    if (!fulfillment || !fulfillment.from_topship) return false;
    const { shipping_state } = fulfillment;
    return (
      shipping_state === 'delivering' ||
      shipping_state === 'holding' ||
      shipping_state === 'picking' ||
      shipping_state === 'created' ||
      shipping_state === 'confirmed' ||
      shipping_state === 'undeliverable' ||
      shipping_state === 'returning' ||
      shipping_state === 'processing'
    );
  }

  async getFulfillmentHistory(ffm) {
    return this.fulfillmentApi.getFulfillmentHistory(ffm);
  }

  groupShipmentServicesByConnection(shipmentServices: FulfillmentShipmentService[]) {
    const hash = {};
    for (let service of shipmentServices) {
      if (hash[service.connection_info.id]) {
        hash[service.connection_info.id].services.push(service);
      } else {
        hash[service.connection_info.id] = {
          carrier_name: service.carrier,
          name: service.connection_info.name,
          id: service.connection_info.id,
          from_topship: service.connection_info.name.toLowerCase().includes('topship'),
          logo: service.provider_logo,
          services: [service]
        };
      }
    }

    const topshipResults: ShipmentCarrier[] = [];
    const directResults: ShipmentCarrier[] = [];

    // NOTE: Group
    for (let key in hash) {
      if (hash[key].from_topship) {
        topshipResults.push(hash[key]);
      } else {
        directResults.push(hash[key]);
      }
    }

    const directBias = {
      'jt-express': 1,
      'jt-test': 1,
      'ghn': 2,
      'ghn-v2': 2,
      'vtp': 3,
      'ghtk': 4
    }

    const bias = {
      'topship-jt': 1,
      'topship-ghn': 2,
      'topship-ghn-v2': 2,
      'topship-vtp': 3,
      'topship-ghtk': 4
    };

    directResults.sort((a, b) => {
      const _currName = StringHandler.createHandle(a.name);
      const _nextName = StringHandler.createHandle(b.name);
      return (directBias[_currName] || 5) - (directBias[_nextName] || 5);
    });

    topshipResults.sort((a, b) => {
      const _currName = StringHandler.createHandle(a.name);
      const _nextName = StringHandler.createHandle(b.name);
      return (bias[_currName] || 5) - (bias[_nextName] || 5);
    });

    // NOTE: Sort
    return this.sortAndFilterShipmentServices(directResults.concat(topshipResults));
  }

  sortAndFilterShipmentServices(shipmentCarriers: ShipmentCarrier[]) {

    // NOTE: Filter cheapest & fastest delivery if duplicated code
    for (let i = 0; i < shipmentCarriers.length; i++) {
      const carrier = shipmentCarriers[i];
      const hash = {};

      carrier.services = carrier.services.map(service => {
        if (service.shipment_service_info) {
          if (hash[service.shipment_service_info.code]) {
            hash[service.shipment_service_info.code].services.push(service);
          } else {
            hash[service.shipment_service_info.code] = {
              services: [service],
              connection_id: carrier.id
            };
          }
          service = null;
        }
        return service;
      }).filter(s => !!s);

      for (let key in hash) {
        if (hash[key].connection_id == carrier.id) {
          hash[key].services.sort((a,b) => {
            if (a.fee < b.fee) {
              return -1;
            }
            if (a.fee > b.fee) {
              return 1;
            }
            if (a.estimated_delivery_at && b.estimated_delivery_at) {
              const currentDeliveryTime = new Date(a.estimated_delivery_at).getTime();
              const nextDeliveryTime = new Date(b.estimated_delivery_at).getTime();
              if (currentDeliveryTime < nextDeliveryTime) {
                return -1;
              } else {
                return 1;
              }
            }
          });
          carrier.services.push(hash[key].services.shift());
        }
      }

      // NOTE: Final Sorted by fee
      carrier.services.sort((s1, s2) => s1.fee - s2.fee);
    }

    return shipmentCarriers;
  }

  sortAndFilterShipnowServices(shipnowCarriers: ShipnowCarrier[]) {
    for (let i = 0; i < shipnowCarriers.length; i++) {
      const carrier = shipnowCarriers[i];
      carrier.services.sort((s1, s2) => s1.fee - s2.fee);
    }

    return shipnowCarriers;
  }

}
