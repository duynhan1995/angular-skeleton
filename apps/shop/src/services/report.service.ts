import { ReportApi } from '@etop/api/shop/report.api';
import { Injectable } from '@angular/core';
import { HttpService } from '@etop/common';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private http: HttpService, private reportApi: ReportApi) {}

  async getIncome(type, time) {
    return await this.reportApi.getIncome(type, time);
  }
  async getEnddayIncome(time) {
    return await this.reportApi.getEnddayIncome(time);
  }
  async exportIncome(type, time, export_type) {
    return await this.reportApi.exportIncome(type, time, export_type);
  }
  async exportEnddayIncome(time, export_type) {
    return await this.reportApi.exportEnddayIncome(time, export_type);
  }
}
