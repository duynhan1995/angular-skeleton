import { Injectable } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { Supplier } from '@etop/models';
import { requireMethodRoles } from '@etop/core';
import { SupplierApi } from '@etop/api';

@Injectable()
export class SupplierService {

  constructor(
    private supplierApi: SupplierApi,
    private util: UtilService
  ) {}

  async createSupplier(data: Supplier | any) {
    return await this.supplierApi.createSupplier(data);
  }

  @requireMethodRoles({
    skipError: true,
    valueOnError: [],
    roles: ['admin', 'owner', 'purchasing_management', 'accountant']
  })
  async getSuppliers(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };
    return this.supplierApi.getSuppliers({ paging, filters })
      .then(res => res.suppliers.map((c, index) => this.mapSupplierInfo(c, index)));
  }

  async updateSupplier(data) {
    delete data.code;
    return await this.supplierApi.updateSupplier(data);
  }

  async deleteSupplier(id) {
    return await this.supplierApi.deleteSupplier(id);
  }

  mapSupplierInfo(supplier: Supplier, index) {
    return {
      ...supplier,
      index,
      info: `<strong>${supplier.full_name} - ${supplier.phone}</strong>`,
      detail: `<strong>${supplier.full_name} - ${supplier.phone}</strong>`,
      search_text: this.util.makeSearchText(`${supplier.full_name}${supplier.phone}`)
    }
  }

}
