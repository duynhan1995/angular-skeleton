import { Injectable } from '@angular/core';
import {CmsService} from "apps/core/src/services/cms.service";
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AuthenticateStore } from '@etop/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { ShopCrmService } from '../../services/shop-crm.service';
import { NotificationService } from '../../services/notification.service';
import {Account, MONEY_TRANSACTION_RRULE} from '@etop/models';
import { UserApi } from '@etop/api';
import { AppService } from '@etop/web/core/app.service';

const SPECIAL_ROUTES = {
  'top-ship': {
    display_on_mobile: true,
    require_login: false
  },
  tracking: {
    display_on_mobile: true,
    require_login: false
  },
  'reset-password': {
    display_on_mobile: true,
    require_login: false
  },
  'on-boarding': {
    display_on_mobile: true,
    require_login: true
  },
  'admin-login': {
    display_on_mobile: true,
    require_login: false
  },
  'verify-email': {
    display_on_mobile: true,
    require_login: true
  },
  'invitation': {
    display_on_mobile: true,
    require_login: false
  }
};

@Injectable()
export class ShopCommonUsecase extends CommonUsecase {
  app = 'dashboard';
  full_name;
  password;
  email;
  confirm;
  error = false;

  signupData: any = {};

  loading = false;

  provinces;

  constructor(
    private userService: UserService,
    private userApi: UserApi,
    private router: Router,
    private auth: AuthenticateStore,
    private gaService: GoogleAnalyticsService,
    private crmService: ShopCrmService,
    private notification: NotificationService,
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
    private cms: CmsService
  ) {
    super();
    (window as any).injectToken = (token) => {
      this.auth.updateToken(token);
      this.checkAuthorization(true).then(() => setTimeout(() => {
        location.href = location.origin
      }, 500));
    };
  }

  private checkInvitationToken() {
    return this.activatedRoute.snapshot.queryParamMap.get('invitation_token');
  }
  private checkRefAff(){
    return this.activatedRoute.snapshot.queryParamMap.get('e_aff');
  }

  async checkAuthorization(fetchAccount = false) {
    let appConfig = await this.appService.getAppConfig();

    const ref = this.router.url.split('?')[0];
    try {
      const url = window.location.pathname.replace('/', '');
      const special = SPECIAL_ROUTES[url];
      if (special && !special.require_login) {
        this.auth.setRef(this.router.url);
        return;
      }

      const route = window.location.pathname + window.location.search;
      this.auth.setRef(route);
      await this.updateSessionInfo(fetchAccount);
      if (!this.auth.snapshot.shop) {
        return this.toSurvey();
      }
      const pathname = this.router.url.split('?')[0];
      if (pathname.startsWith('/s/')) {
        const index = pathname.split('/')[2];
        this.auth.selectAccount(index);
      } else {
        this.auth.selectAccount(null);
      }

      await this.navigateAfterAuthorized();
    } catch (e) {
      debug.error('ERROR in checkAuthorization', e);
      this.auth.clear();
      this.auth.setRef(ref);

      if (appConfig.disableUser) {
        location.href = appConfig.redirectUserLink || '/';
        return;
      }

      if (ref.indexOf('register') == -1) {
        if (ref.indexOf('login') != -1) {
          await this.router.navigate(
            ['/login'],
            {
              queryParams: { invitation_token: this.checkInvitationToken(),e_aff:this.checkRefAff()  }
            });
        } else {
          await this.router.navigate(
            ['/register'],
            {
              queryParams: { invitation_token: this.checkInvitationToken(),e_aff:this.checkRefAff() }
            });
        }
      }
    }
  }

  async navigateAfterAuthorized() {
    let ref = this.auth.getRef(true).replace('/', '');
    const defaultRef = 'orders';
    if (ref == 'login' || ref == 'register') {
      await this.router.navigate([`./${defaultRef}`]);
    } else {
      await this.router.navigateByUrl(ref || `/s/${this.auth.currentAccountIndex()}/orders`);
    }
    this.registerDeviceForNoti();
  }

  async login(data: { login: string; password: string }, after_register?: boolean) {
    try {
      const res = await this.userApi.login({
        login: data.login,
        password: data.password,
        account_type: 'shop'
      });

      this.gaService.login(this.email);
      this.auth.updateToken(res.access_token);
      this.auth.updateUser(res.user);
      const invitation_token = this.activatedRoute.snapshot.queryParamMap.get('invitation_token');

      if (!res.account && !invitation_token) {
        this.loading = false;
        return this.toSurvey();
      }

      await this.setupAndRedirect(invitation_token, after_register);
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
  }

  async setupAndRedirect(invitation_token?: string, after_register?: boolean) {
    await this.updateSessionInfo(true);
    this.auth.selectAccount(null);
    if (invitation_token) {
      if (after_register) {
        await this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug || 0}/settings`);
      } else {
        await this.router.navigate(['/invitation'], { queryParams: { t: invitation_token } });
      }
    } else {
      const ordersCount = 0;
      const ref = this.auth.getRef(true);
      await this.router.navigateByUrl(
        `/s/${this.auth.snapshot.account.url_slug || 0}/${ref || (ordersCount >= 0 ? 'orders' : 'settings')}`
      );
    }
  }

  async updateSessionInfo(fetchAccounts = false) {
    const res = await this.userService.checkToken(this.auth.snapshot.token);
    let { access_token, account, shop, user, available_accounts } = res;
    const shop_accounts = available_accounts.filter(a => a.type === 'shop').sort((a, b) => a.id > b.id);

    let no_init_shop = true;

    if (shop) {
      no_init_shop = false;
      this.crmService.updateLeadAndContact(user, shop, shop.address).then();
    }
    if (!shop && available_accounts && available_accounts.length) {
      shop = shop_accounts[0];
      account = shop_accounts[0];
    }
    const accounts: Account[] = fetchAccounts ? await Promise.all(shop_accounts.map(async (a, index) => {
      const accRes = await this.userService.switchAccount(a.id);
      a.token = accRes.access_token;
      a.shop = accRes.shop;
      a.id = accRes.shop && accRes.shop.id;
      a.image_url = a.shop.image_url;
      a.display_name = `${a.shop.code} - ${a.shop.name}`;
      a.permission = accRes.account.user_account.permission;
      return new Account(a);
    })) : this.auth.snapshot.accounts;

    if (accounts.length > 0) {
      this.auth.updateInfo({
        token: no_init_shop && accounts[0].token || access_token,
        account: {
          ...account,
          ...shop,
          display_name: `${shop.code} - ${shop.name}`
        },
        accounts,
        shop: {...shop, money_transaction_rrule_display: MONEY_TRANSACTION_RRULE[shop.money_transaction_rrule]},
        user,
        permission: account.user_account.permission,
        isAuthenticated: true,
        uptodate: true
      });
    }
  }

  async register(data: any, source = '') {
    const res = await this.userService.signUpUsingToken({ ...data, source });
    this.signupData = Object.assign({}, data, res.user);
    toastr.success('Đăng ký thành công!');

    this.login({
      login: this.signupData.email, password: this.signupData.password
    }, true).then();
    this.gaService.registerSuccessfully(
      data.email,
      data.phone,
      data.full_name
    );
  }

  registerDeviceForNoti() {
    if (this.notification.oneSignalInitialized) {
      this.notification.createDevice().then();
    } else {
      this.notification.onOneSignalInitialized.subscribe(_ => {
        this.notification.createDevice().then();
      });
    }
  }

  redirectIfAuthenticated(): Promise<any> {
    return this.checkAuthorization(true);
  }

  toSurvey() {
    if (this.cms.bannersLoaded) {
      const _survey = this.cms.getEtopPosSurvey();
      const _savedSurvey = localStorage.getItem('survey');
      if (_survey && !_savedSurvey) {
        this.router.navigateByUrl('/survey').then();
      } else {
        this.router.navigateByUrl('/create-shop').then();
      }
    } else {
      const _subscription = this.cms.onBannersLoaded.subscribe(_ => {
        const _survey = this.cms.getEtopPosSurvey();
        const _savedSurvey = localStorage.getItem('survey');
        if (_survey && !_savedSurvey) {
          this.router.navigateByUrl('/survey').then(__ => {
            _subscription.unsubscribe();
          });
        } else {
          this.router.navigateByUrl('/create-shop').then(__ => {
            _subscription.unsubscribe();
          });
        }
      });
    }
  }

}
