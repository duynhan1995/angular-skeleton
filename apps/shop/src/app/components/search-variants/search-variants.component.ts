import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Variant } from '@etop/models';
import { ProductService } from 'apps/shop/src/services/product.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';

@Component({
  selector: 'shop-search-variants',
  templateUrl: './search-variants.component.html',
  styleUrls: ['./search-variants.component.scss']
})
export class SearchVariantsComponent extends BaseComponent implements OnInit {
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  @Output() variantSearched = new EventEmitter();

  @Input() filterCallback: (any) => boolean;

  variant_list: Variant[] = [];
  variantSearchText = '';
  results: Variant[] = [];

  barcode = false;
  click_inside = false;
  show_dropdown = false;

  @HostListener('document:click', ['$event'])
  public clickOutsideComponent(event) {
    if (!this.click_inside) {
      this.show_dropdown = false;
    }
    this.click_inside = false;
    if (event.target.id != 'search-input') {
      if (event.target.className.indexOf('barcode-click') != -1 && !this.barcode) {
        this.barcodeOption();
      } else {
        this.onOutFocus();
      }
    }
  }

  constructor(
    private productService: ProductService,
    private util: UtilService,
    private inventoryStore: InventoryStoreService,
    private purchaseOrderStore: PurchaseOrderStoreService
  ) {
    super();
  }

  ngOnInit() {
    this.loadVariants();
    this.inventoryStore.stocktakeRecentlyCreated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.loadVariants();
      });
    this.purchaseOrderStore.recentlyCreated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.loadVariants();
      });
  }

  async loadVariants() {
    const res = await this.productService.getProducts(0, 1000);
    this.variant_list = [];
    res.products.forEach(prod => {
      prod.variants.forEach(v => {
        v.name = prod.name;
        v.p_data = {
          search_name: this.util.makeSearchText(prod.name + v.code)
        };
      });
      this.variant_list = this.variant_list.concat(prod.variants);
    });
  }

  searchVariants() {
    setTimeout(() => {
      this.show_dropdown = !!this.variantSearchText && this.variantSearchText.length > 0;
      if (this.variantSearchText) {
        this.results = this.variant_list.filter(v => {
          const _search_name = this.util.makeSearchText(this.variantSearchText);
          return v.p_data.search_name.indexOf(_search_name) != -1;
        });
        if (this.filterCallback) {
          this.results = this.results.filter(result => this.filterCallback(result));
        }
      } else {
        this.results = this.variant_list;
      }
    }, 200);
  }

  selectVariant(variant) {
    this.variantSearched.emit(variant);
  }

  barcodeOption() {
    this.barcode = !this.barcode;
    this.searchInput.nativeElement.focus();
  }

  searchVariantBySKU() {
    if (!this.barcode) { return; }
    const variant = this.variant_list.find(v => v.code == this.variantSearchText);
    this.selectVariant(variant);
    this.variantSearchText = '';
  }

  onOutFocus() {
    this.barcode = false;
  }

}
