import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketComponent } from 'apps/shop/src/app/components/ticket/ticket.component';
import { TicketListComponent } from 'apps/shop/src/app/components/ticket/ticket-list/ticket-list.component';
import { ForceButtonModule } from 'apps/shop/src/app/components/force-button/force-button.module';
import {EtopPipesModule} from "@etop/shared";

@NgModule({
  declarations: [
    TicketComponent,
    TicketListComponent
  ],
  exports: [
    TicketComponent
  ],
  imports: [
    CommonModule,
    ForceButtonModule,
    EtopPipesModule
  ]
})
export class TicketModule { }
