import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { Ticket } from 'libs/models/Ticket';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ShopCrmService } from 'apps/shop/src/services/shop-crm.service';
import { Fulfillment } from 'libs/models/Fulfillment';
import { takeUntil } from 'rxjs/operators';
import {CmsService} from "apps/core/src/services/cms.service";

@Component({
  selector: 'shop-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent extends BaseComponent implements OnInit {
  @Input() ffm = new Fulfillment({});
  getting_tickets = false;
  tickets: Array<Ticket> = [];

  ticketNote = '';

  constructor(
    private ffmService: FulfillmentService,
    private auth: AuthenticateStore,
    private shopCRM: ShopCrmService,
    private cms: CmsService
  ) {
    super();
  }

  async ngOnInit() {
    this.ticketNote = this.cms.getTicketNote();
    await this.getTickets();
    this.shopCRM.onTicketCreated
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getTickets();
      });
  }

  canSendSupportRequest(fulfillment) {
    return this.ffmService.canSendSupportRequest(fulfillment);
  }

  async getTickets() {
    this.getting_tickets = true;
    try {
      let shop = this.auth.snapshot.shop;
      if(this.ffm?.order?.id == "0" || !this.ffm?.order?.id){
        this.tickets = [];
      }else{
        this.tickets = await this.shopCRM.getTickets({
          etop_id: shop.user.id,
          order_id: this.ffm?.order?.id
        });
      }
    } catch (e) {
      debug.error('ERROR in getting tickets', e);
    }
    this.getting_tickets = false;
  }

}
