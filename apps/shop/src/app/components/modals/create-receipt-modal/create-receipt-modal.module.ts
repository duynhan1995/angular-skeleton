import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateReceiptModalComponent } from 'apps/shop/src/app/components/modals/create-receipt-modal/create-receipt-modal.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    CreateReceiptModalComponent
  ],
  entryComponents: [
    CreateReceiptModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    EtopMaterialModule,
    EtopPipesModule,
  ]
})
export class CreateReceiptModalModule { }
