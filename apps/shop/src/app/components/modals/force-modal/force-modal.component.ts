import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Fulfillment } from 'libs/models/Fulfillment';
import { TelegramService } from '@etop/features';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { NotiModalComponent } from '../noti-modal/noti-modal.component';
import { VND } from 'libs/shared/pipes/etop.pipe';
import { ShopCrmService } from '../../../../services/shop-crm.service';
import { ConfigService } from '@etop/core/services/config.service';

@Component({
  selector: 'shop-force-modal',
  templateUrl: './force-modal.component.html',
  styleUrls: ['./force-modal.component.scss']
})
export class ForceModalComponent implements OnInit {
  dismiss: (data) => void;
  @Input() fulfillment: Fulfillment;

  vndTransform = new VND();

  loading = false;
  reason: string;
  money_formatted = false;

  support_requests = [
    {
      code: 'force-picking',
      title: 'Giục lấy hàng',
      placeholder: null,
      disabled: false
    },
    {
      code: 'force-delivering',
      title: 'Giục giao hàng',
      placeholder: null,
      disabled: false
    },
    {
      code: 'change-shop-cod',
      title: 'Thay đổi COD',
      placeholder: 'Nhập số tiền thu hộ mới',
      disabled: false
    },
    {
      code: 'change-phone',
      title: 'Thay đổi SDT',
      placeholder: 'Nhập số điện thoại mới',
      disabled: false
    },
    {
      code: 'others',
      title: 'Yêu cầu khác',
      placeholder: null,
      disabled: false
    }
  ];

  support_request = {
    code: '',
    title: '',
    value: '',
    old_value: null,
    reason: ''
  };
  inputShow = false;
  inputShowPlaceholder = '';
  requestName = 'Gửi yêu cầu hỗ trợ';

  force_warning = false;
  force_title = '';
  force_expected = '';

  constructor(
    private telegramService: TelegramService,
    public util: UtilService,
    private auth: AuthenticateStore,
    private cdRef: ChangeDetectorRef,
    private modalDismiss: ModalAction,
    private fulfillmentService: FulfillmentService,
    private modalController: ModalController,
    private shopCRM: ShopCrmService,
    private configService: ConfigService
  ) {
  }

  ngOnInit() {
    if (!this.canForcePicking(this.fulfillment)) {
      this.support_requests = this.support_requests.filter(
        req => req.code != 'force-picking'
      );
    }
    if (!this.canForceDelivering(this.fulfillment)) {
      this.support_requests = this.support_requests.filter(
        req => req.code != 'force-delivering'
      );
    }
    this.telegramService.onUpdateExpireTime.subscribe(res => {
      if (res) {
        this.support_requests[0].disabled = true;
        this.support_requests[0].title += ' (Đã tiếp nhận, đang xử lý)';
      }
    });
    if (this.checkStatusForceItem) {
      this.support_requests[0].disabled = true;
      this.support_requests[0].title += ' (Đã tiếp nhận, đang xử lý)';
    }

    if (this.fulfillment.shipping_provider == 'vtpost') {
      this.support_requests.forEach(req => {
        if (req.code.indexOf('change') >= 0 || req.code == 'others') {
          req.disabled = true;
          req.title += ' (Viettel Post không hỗ trợ)';
        }
      });
    }
  }

  reveiveSupportRequest() {
    let modal = this.modalController.create({
      component: NotiModalComponent,
      showBackdrop: true,
      componentProps: {
        title: 'Tiếp nhận yêu cầu hỗ trợ',
        content: `<p class="m-0">TOPSHIP tiếp nhận yêu cầu đơn hàng. Bộ phận CSKH sẽ tiếp nhận và xử lý từ 1-3h theo khung giờ làm
          việc từ 9h – 18h Thứ 2
          đến Thứ 7 (chậm nhất là 24h với sự cố khách quan không mong muốn).</p>
        <p class="m-0">Cảm ơn bạn đã sử dụng dịch vụ TOPSHIP.</p>`
      }
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  canForcePicking(fulfillment) {
    return this.fulfillmentService.canForcePicking(fulfillment);
  }

  canForceDelivering(fulfillment) {
    return this.fulfillmentService.canForceDelivering(fulfillment);
  }

  async selectRequest(support_request_code) {
    this.support_request.value = '';
    this.money_formatted = false;
    this.force_warning = false;

    let request = this.support_requests.find(
      req => req.code == support_request_code
    );
    if (request.placeholder) {
      this.inputShow = true;
      this.inputShowPlaceholder = request.placeholder;
    } else {
      this.inputShow = false;
      this.inputShowPlaceholder = '';
    }

    this.support_request.title = request.title;
    if (request.code != 'others') {
      this.requestName = this.support_request.title;
    } else {
      this.requestName = 'Gửi yêu cầu hỗ trợ';
    }
    let diff;
    switch (support_request_code) {
      case 'force-delivering':
        this.force_title = 'GIAO';
        this.force_expected = moment(
          this.fulfillment.estimated_delivery_at
        ).format('HH:mm DD/MM');
        diff = moment(moment.now()).diff(
          moment(this.fulfillment.estimated_delivery_at),
          'hours'
        );
        if (diff <= 0) {
          this.force_warning = true;
        }
        break;
      case 'force-picking':
        this.force_title = 'LẤY';
        this.force_expected = `trước ${moment(
          this.fulfillment.estimated_pickup_at
        ).format('HH:mm DD/MM')}`;
        diff = moment(moment.now()).diff(
          moment(this.fulfillment.estimated_pickup_at),
          'hours'
        );
        if (diff <= 0) {
          this.force_warning = true;
        }
        break;
      case 'change-shop-cod':
        this.support_request.old_value = `${this.vndTransform.transform(
          this.fulfillment.total_cod_amount
        )}`;
        break;
      case 'change-phone':
        this.support_request.old_value = `${this.fulfillment?.shipping_address?.phone}`;
        break;
      default:
        break;
    }
  }

  moneyFormat() {
    this.money_formatted = this.support_request.code == 'change-shop-cod' &&
      !!this.support_request.value;
  }

  showInput() {
    this.money_formatted = false;
  }

  async force() {
    this.loading = true;

    let request = this.support_request;
    request.reason.trim();
    let old_value = '';
    if (request.value) {
      switch (request.code) {
        case 'change-phone':
          try {
            this.util.validatePhoneNumber(`${request.value}`);
          } catch (e) {
            this.loading = false;
            return toastr.error(e.message);
          }
          old_value = this.fulfillment?.shipping_address?.phone;
          break;
        case 'change-shop-cod':
          const value = Number(request.value);
          if (value > 0 && value < 5000) {
            return toastr.error(
              'Vui lòng nhập tiền thu hộ bằng 0đ hoặc lớn hơn 5000đ.'
            );
          }
          old_value = this.vndTransform.transform(
            this.fulfillment.total_cod_amount
          );
          request.value = this.vndTransform.transform(value);
          break;
        default:
          break;
      }
      request = Object.assign({}, request, { old_value });
    }
    this.createVtigerTicket(request, this.fulfillment);
    this.initEverything();
    toastr.success(`Đã gửi yêu cầu ${request.title}`);
    this.loading = false;
    this.telegramService.forceMessage(request, this.fulfillment);
    this.cdRef.detectChanges();
    this.closePopup();
    this.reveiveSupportRequest();
  }

  async createVtigerTicket(request, fulfillment: Fulfillment) {
    try {
      const domain = (window && window.location.hostname) || '';
      const { shipping_provider, shipping_service_code } = fulfillment;
      let account = 'unknown';
      let login = '';
      switch (shipping_provider) {
        case 'ghn':
          if (shipping_service_code.charAt(0) === 'D') {
            account = 'TK 300 gr';
            login = '0938222489';
          } else if (shipping_service_code.charAt(0) === 'E') {
            account = 'TK 500 gr';
            login = '0976358769';
          }
          break;
        case 'ghtk':
          if (shipping_service_code.charAt(1) === 'D') {
            account = 'TK Default';
            login = 'giang2224@gmail.com';
          } else if (shipping_service_code.charAt(2) === 'S') {
            account = 'TK Đồng giá';
            login = 'giang@etop.vn';
          }
          break;
        default:
          account = 'vtpost';
      }

      let body = Object.assign({}, request, {
        etop_id: fulfillment.shop.owner_id,
        order_id: fulfillment?.order?.id,
        order_code: fulfillment?.order?.code,
        ffm_code: fulfillment.shipping_code,
        ffm_url: `${this.configService.getConfig().admin_url}/admin/fulfillments/${
          fulfillment.id
        }`,
        ffm_id: fulfillment.id,
        company: fulfillment.shop.name,
        account: {
          ...this.auth.snapshot.user,
          company: fulfillment.shop.name
        },
        provider: shipping_provider.toUpperCase(),
        note: `${shipping_provider.toUpperCase()} ${account}${
          login ? `Login ${login}` : ''
        }`,
        environment: this.configService.getConfig().ticket_environment,
        from_app: domain
      });
      await this.shopCRM.createTicket(body);
    } catch (e) {
      toastr.error(e.message, 'Tạo yêu cầu thất bại');
    }
  }

  initEverything() {
    this.support_request = {
      code: '',
      title: '',
      value: '',
      old_value: null,
      reason: ''
    };
    this.inputShow = false;
    this.inputShowPlaceholder = '';
    this.requestName = 'Gửi yêu cầu hỗ trợ';
    this.money_formatted = false;
    this.force_warning = false;
    this.reason = null;
  }

  closePopup() {
    this.modalDismiss.dismiss(this);
  }

  get checkStatusForceItem() {
    return this.fulfillmentService.checkStatusForceItem(
      this.fulfillment.shipping_code
    );
  }

}
