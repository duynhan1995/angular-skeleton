import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import misc from 'apps/core/src/libs/misc';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';

@Component({
  selector: 'shop-change-ledger-modal',
  templateUrl: './change-ledger-modal.component.html',
  styleUrls: ['./change-ledger-modal.component.scss']
})
export class ChangeLedgerModalComponent implements OnInit {
  @Input() totalAmountText: string;
  @Input() totalPaymentText: string;
  @Input() total_amount: number;
  @Input() receipts: any[] = [];

  loading = false;

  final_receipts: any[] = [];

  constructor(
    private modalAction: ModalAction,
    private changeDetector: ChangeDetectorRef,
    private receiptStore: ReceiptStoreService
  ) { }

  get totalPayment() {
    if (this.final_receipts.length) {
      return this.final_receipts.reduce((a, b) => {
        return a + Number(b.amount)
      }, 0);
    }
    return this.receipts.reduce((a, b) => {
      return a + Number(b.amount)
    }, 0);
  }

  get onlyCash() {
    return this.final_receipts.length == 1 && this.final_receipts[0].ledger_type == 'cash';
  }

  ngOnInit() {}

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  onAddLedgerToggled(opened: boolean) {
    if (opened) {
      this.modalAction.show();
    } else {
      this.modalAction.close(false);
    }
  }

  checkDuplicatedLedger() {
    const ledger_ids = this.final_receipts.filter(r => !!r.ledger_id)
      .map(r => r.ledger_id);
    const unique_ledger_ids = misc.uniqueArray(ledger_ids);
    return unique_ledger_ids.length < ledger_ids.length;

  }

  confirm() {
    if (!this.onlyCash && this.totalPayment > this.total_amount) {
      toastr.error(`${this.totalPaymentText} không được vượt quá ${this.totalAmountText}!`);
      return;
    }
    if (this.checkDuplicatedLedger()) {
      toastr.error('Có phương thức thanh toán bị trùng. Vui lòng kiểm tra lại!');
      return;
    }

    let has_error = false;
    this.final_receipts.forEach(r => {
      if (!r.amount) {
        has_error = true;
      }
    });
    if (has_error) {
      toastr.error('Số tiền nhập vào phải lớn hơn 0đ!');
      return;
    }

    this.modalAction.dismiss(this.final_receipts);
  }

  updateReceiptLines(receipt, index) {
    if (this.final_receipts[index]) {
      this.final_receipts[index] = receipt;
    } else {
      this.final_receipts.push(receipt);
    }
    this.changeDetector.detectChanges();
    this.receiptStore.updateSelectedLedgerIDs(
      this.final_receipts.filter(r => !!r.ledger_id)
        .map(r => r.ledger_id)
    );
  }

  addMethod() {
    this.receipts.push({
      ledger_name: '',
      ledger_id: '',
      amount: null,
    });
  }

  removeReceipt(index) {
    this.receipts.splice(index, 1);
    this.final_receipts.splice(index, 1);
    this.receiptStore.updateSelectedLedgerIDs(
      this.final_receipts.filter(r => !!r.ledger_id)
        .map(r => r.ledger_id)
    );
  }

}
