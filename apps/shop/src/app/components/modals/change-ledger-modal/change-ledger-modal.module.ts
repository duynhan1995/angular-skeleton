import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeLedgerModalComponent } from 'apps/shop/src/app/components/modals/change-ledger-modal/change-ledger-modal.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { ReceiptLinesModule } from 'apps/shop/src/app/components/receipt-lines/receipt-lines.module';
import { AuthenticateModule } from '@etop/core';
import { EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    ChangeLedgerModalComponent
  ],
  entryComponents: [
    ChangeLedgerModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReceiptLinesModule,
    AuthenticateModule,
    EtopPipesModule,
  ],
  providers: [
    ReceiptStoreService
  ]
})
export class ChangeLedgerModalModule { }
