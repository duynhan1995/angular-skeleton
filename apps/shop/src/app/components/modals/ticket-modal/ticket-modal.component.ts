import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ShopCrmService } from '../../../../services/shop-crm.service';

@Component({
  selector: 'shop-ticket-modal',
  templateUrl: './ticket-modal.component.html',
  styleUrls: ['./ticket-modal.component.scss']
})
export class TicketModalComponent implements OnInit {
  @ViewChild('scrollMe', { static: false })
  private myScrollContainer: ElementRef;
  @Input() ticket;
  @Input() ticketComments: any = [];
  comment = '';

  constructor(private shopCRM: ShopCrmService) {}

  ngOnInit() {
    this.scrollToBottom();
  }

  getStatusSnake(status) {
    return status ? status.replace(' ', '-') : '';
  }
  getStatusDisplay(status) {
    switch (status) {
      case 'Open':
        return 'Mới';
      case 'Confirmed':
        return 'Đã tiếp nhận';
      case 'In Progress':
        return 'Đang xử lý';
      case 'Wait For Response':
        return 'Chờ Shop phản hồi';
      case 'Closed':
        return 'Đóng';
    }
    return status;
  }

  subStatusMap(substatus) {
    return substatus
      ? {
          reject: 'Từ chối xử lý',
          failed: 'Xử lý thất bại',
          success: 'Xử lý thành công'
        }[substatus]
      : '';
  }

  async createComment(ticket_id) {
    try {
      if (!this.comment) {
        toastr.warning('Vui lòng nhập nội dung phản hồi!');
        return;
      }
      let body = {
        content: this.comment,
        ticket_id: ticket_id
      };
      this.comment = '';
      await this.shopCRM.createComment(body);
      toastr.success('Gửi phản hồi thành công!');
      await this.getTicketComment(ticket_id);
    } catch (e) {
      toastr.error(e.message);
    }
  }

  async getTicketComment(ticket_id) {
    try {
      const ticketComments = await this.shopCRM.getTicketComment(ticket_id);
      if (ticketComments) {
        ticketComments.comments = ticketComments.comments.slice().reverse();
      }
      this.ticketComments = ticketComments;
    } catch (e) {
      debug.error(e.message);
    }
  }

  scrollToBottom() {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (e) {}
  }
}
