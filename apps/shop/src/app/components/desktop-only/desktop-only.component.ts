import { Component, OnInit } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'shop-desktop-only',
  templateUrl: './desktop-only.component.html',
  styleUrls: ['./desktop-only.component.scss']
})
export class DesktopOnlyComponent implements OnInit {
  desktop_only: any = {};
  constructor(private util: UtilService, private appService: AppService) {}

  async ngOnInit() {
    const etop_content = await this.util.getEtopContent();
    if (
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i)
    ) {
      this.desktop_only = etop_content[22];
    } else if (
      navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/BlackBerry/i)
    ) {
      this.desktop_only = etop_content[23];
    }
    this.desktop_only.description = this.util.keepHtmlCssStyle(
      this.desktop_only.description
    );
  }
  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }
}
