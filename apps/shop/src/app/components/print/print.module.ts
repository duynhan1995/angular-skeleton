import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintComponent } from './print.component';
import { PrintControllerService } from './print.controller';


@NgModule({
  declarations: [
    PrintComponent
  ],
  exports: [
    PrintComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [
    PrintControllerService
  ]
})
export class PrintModule { }