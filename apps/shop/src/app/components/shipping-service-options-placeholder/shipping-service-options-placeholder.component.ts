import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shop-shipping-service-options-placeholder',
  templateUrl: './shipping-service-options-placeholder.component.html',
  styleUrls: ['./shipping-service-options-placeholder.component.scss']
})
export class ShippingServiceOptionsPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
