import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShippingServiceOptionsPlaceholderComponent} from "apps/shop/src/app/components/shipping-service-options-placeholder/shipping-service-options-placeholder.component";

@NgModule({
  declarations: [
    ShippingServiceOptionsPlaceholderComponent
  ],
  exports: [
    ShippingServiceOptionsPlaceholderComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ShippingServiceOptionsPlaceholderModule { }
