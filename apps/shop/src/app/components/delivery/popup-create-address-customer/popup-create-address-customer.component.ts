import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { BaseComponent } from '@etop/core';
import {map, takeUntil} from 'rxjs/operators';
import { UtilService } from 'apps/core/src/services/util.service';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import {combineLatest} from 'rxjs';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { CustomerApi } from '@etop/api';
import {LocationQuery} from "@etop/state/location";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'shop-popup-create-address-customer',
  templateUrl: './popup-create-address-customer.component.html',
  styleUrls: ['./popup-create-address-customer.component.scss']
})
export class PopupCreateAddressCustomerComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() customer: Customer

  loading = false;

  temp_customer = new Customer({});
  temp_address = new CustomerAddress({});
  customers: Customer[] = [];

  customerAddressForm = this.fb.group({
    customerId: '',
    provinceCode: '',
    districtCode: '',
    wardCode: '',
    address1: ''
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.customerAddressForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.customerAddressForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  initializing = true;

  private static validate(data: CustomerAddress) {
    if (!data.full_name) {
      toastr.error('Vui lòng nhập tên khách hàng!');
      return false;
    }

    if (!data.phone) {
      toastr.error('Vui lòng nhập số điện thoại khách hàng!');
      return false;
    }

    if (!data.province_code) {
      toastr.error('Vui lòng chọn tỉnh thành!');
      return false;
    }

    if (!data.district_code) {
      toastr.error('Vui lòng chọn quận huyện!');
      return false;
    }

    if (!data.ward_code) {
      toastr.error('Vui lòng chọn phường xã!');
      return false;
    }

    if (!data.address1) {
      toastr.error('Vui lòng nhập địa chỉ!');
      return false;
    }

    return true;
  }

  constructor(
    private fb: FormBuilder,
    private util: UtilService,
    private modalAction: ModalAction,
    private customerService: CustomerService,
    private customerApi: CustomerApi,
    private customerStore: CustomerStoreService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  displayMap = option => option && `${option.full_name}${option.phone && ' - ' + option.phone || ''}` || null;
  valueMap = option => option && option.id || null;

  inputMapName = (option: Customer) => option && option.full_name || null;
  inputMapPhone = (option: Customer) => option && option.phone || null;

  ngOnInit() {
    const customer = this.customerStore.snapshot.selected_customer || this.customer;
    const customer_address = this.customerStore.snapshot.selected_customer_address;
    this.temp_customer = customer && { ...customer } || new Customer({});
    this.customers = customer && [customer] || [];
    this.temp_address = customer_address && { ...customer_address } || new CustomerAddress({});
    this.customerAddressForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.initializing) {
          this.customerAddressForm.patchValue({
            districtCode: '',
            wardCode: ''
          });
        }
      });

    this.customerAddressForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.initializing) {
          this.customerAddressForm.patchValue({
            wardCode: ''
          });
        }
      });

    this.customerAddressForm.controls['customerId'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.initializing) {
          this.selectCustomer().then();
        }
      });
  }

  ngAfterViewInit() {
    this.prepareLocationData();
  }

  private prepareLocationData() {
    this.initializing = true;
    this.customerAddressForm.patchValue({
      customerId: this.temp_customer.id || '',
      provinceCode: '',
      districtCode: '',
      wardCode: '',
      address1: '',
    });

    this.initializing = false;
  }

  async onCustomerSearched(field, searchText: string) {
    switch (field) {
      case 'full_name':
        this.temp_customer.full_name = searchText;
        this.customers = await this.customerService.checkExistedCustomer({
          full_name: searchText
        });
        if(!this.customers || this.customers.length == 0) {
          this.temp_customer.full_name = searchText;
        }
        break;
      case 'phone':
        this.temp_customer.phone = searchText;
        this.customers = await this.customerService.checkExistedCustomer({
          phone: searchText
        });
        if(!this.customers || this.customers.length == 0) {
          this.temp_customer.phone = searchText;
        }
        break;
    }

  }

  async selectCustomer() {
    const customerId = this.customerAddressForm.getRawValue().customerId;
    this.temp_customer = this.customers.find(c => c.id == customerId);

    const customer_addresses = await this.customerApi.getCustomerAddresses(customerId);
    this.temp_customer.addresses = customer_addresses;

    if (customer_addresses.length) {
      this.temp_address = {
        ...this.temp_address,
        id: customer_addresses[0].id,
        province_code: customer_addresses[0].province_code,
        district_code: customer_addresses[0].district_code,
        ward_code: customer_addresses[0].ward_code,
        address1: customer_addresses[0].address1,
        province: '',
        district: '',
        ward: ''
      };
      this.prepareLocationData();
    }
  }

  async confirm() {
    this.loading = true;
    try {
      const {provinceCode, districtCode, wardCode, address1} = this.customerAddressForm.getRawValue();
      const data: any = {
        full_name: this.temp_customer.full_name,
        phone: this.temp_customer.phone,
        province_code: provinceCode,
        district_code: districtCode,
        ward_code: wardCode,
        address1: address1
      };
      const valid_address = PopupCreateAddressCustomerComponent.validate(data);
      if (!valid_address) {
        return this.loading = false;
      }

      const existed_customers = await this.customerService.checkExistedCustomer({
        phone: this.temp_customer.phone
      });
      const found = existed_customers.find(c => c.phone == this.temp_customer.phone);
      if (!existed_customers || !existed_customers.length || !found) {
        await this.createCustomer();
      } else {
        this.temp_customer.id = found.id;
        await this.updateCustomer(found);
      }

      this.customerStore.resetSelectedCustomer();
      this.customerStore.selectCustomer(this.temp_customer);
      this.customerStore.selectCustomerAddress(this.temp_address);
      this.modalAction.dismiss(null);

    } catch(e) {
      toastr.error(e.msg || e.message)
      debug.error('ERROR in confirm()', e);
    }
    this.loading = false;
  }

  async updateCustomer(found_customer: Customer) {
    try {
      let customer_address_body: any = {
        full_name: found_customer.full_name
      };

      if (this.temp_customer.full_name != found_customer.full_name) {
        this.temp_customer = await this.customerService.updateCustomer({
          id: this.temp_customer.id,
          full_name: this.temp_customer.full_name
        });
        customer_address_body.full_name = this.temp_customer.full_name;
      }
      const found_customer_addresses: CustomerAddress[] = await this.customerApi.getCustomerAddresses(found_customer.id);

      const {provinceCode, districtCode, wardCode, address1} = this.customerAddressForm.getRawValue();
      const same_customer_address = found_customer_addresses.find(addr =>
        addr.compare_text == address1 + wardCode + districtCode + provinceCode
      );

      if (!same_customer_address) {
        customer_address_body = {
          ...customer_address_body,
          province_code: provinceCode,
          district_code: districtCode,
          ward_code: wardCode,
          address1: address1
        };
      }
      if (!same_customer_address) {
        await this.createAddress(customer_address_body);
      } else {
        this.temp_customer.addresses = found_customer_addresses;
        this.temp_address = same_customer_address;
      }
    } catch(e) {
      debug.error('ERROR in updating Customer', e);
      throw new Error(e.msg || e.message);
    }
  }

  async createCustomer() {
    try {
      const { full_name, phone } = this.temp_customer;
      const body: any = {
        full_name,
        phone
      };
      this.temp_customer = await this.customerService.createCustomer(body);

      const {provinceCode, districtCode, wardCode, address1} = this.customerAddressForm.getRawValue();
      const customer_address_body: any = {
        province_code: provinceCode,
        district_code: districtCode,
        ward_code: wardCode,
        address1: address1
      };
      await this.createAddress(customer_address_body);
    } catch(e) {
      debug.error('ERROR in creating Customer', e);
      throw new Error(e.msg || e.message);
    }
  }

  async updateAddress(data) {
    try {
      const body: any = {
        ...data,
        id: this.temp_address.id
      };
      this.temp_address = await this.customerService.updateCustomerAddress(body);
      this.temp_customer.addresses = await this.customerApi.getCustomerAddresses(this.temp_customer.id);
    } catch(e) {
      debug.error('ERROR in updating address', e);
    }
  }

  async createAddress(data) {
    try {
      data = {
        ...data,
        customer_id: this.temp_customer.id,
        full_name: this.temp_customer.full_name,
        phone: this.temp_customer.phone
      };
      this.temp_address = await this.customerService.createCustomerAddress(data);
      this.temp_customer.addresses = await this.customerApi.getCustomerAddresses(this.temp_customer.id);
    } catch(e) {
      debug.error('ERROR in creating address', e);
    }
  }

  closeModal() {
    this.modalAction.close(false);
  }

}
