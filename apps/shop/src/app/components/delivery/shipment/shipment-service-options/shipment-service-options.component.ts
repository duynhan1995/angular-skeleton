import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import {map, takeUntil} from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import {FulfillmentAPI} from '@etop/api';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { UtilService } from 'apps/core/src/services/util.service';
import {ShipmentCarrier, FulfillmentShipmentService} from "libs/models/Fulfillment";
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {CmsService} from "apps/core/src/services/cms.service";

@Component({
  selector: 'shop-shipment-service-options',
  templateUrl: './shipment-service-options.component.html',
  styleUrls: ['./shipment-service-options.component.scss'],
})
export class ShipmentServiceOptionsComponent extends BaseComponent implements OnInit {
  @Output() shipmentServiceChange = new EventEmitter();

  connectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections.filter(conn => conn.connection_subtype == 'shipment'))
  );
  carriers: ShipmentCarrier[] = [];
  selectedShipService: FulfillmentShipmentService = null;

  fromWard = '';
  toWard = '';

  shipServicesErrorMsg = '';

  getServicesIndex = 0;

  specialNote = '';

  constructor(
    private ffmService: FulfillmentService,
    private ffmStore: FulfillmentStore,
    private util: UtilService,
    private connectionStore: ConnectionStore,
    private cmsService: CmsService
  ) {
    super();
  }

  ngOnInit() {
    this.ffmService.onResetData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.reset();
      });
    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.carriers = connections.map(conn => {
          return {
            name: conn.name,
            id: conn.id,
            logo: conn.provider_logo,
            services: [],
            from_topship: conn.connection_method == 'builtin'
          };
        });
      });
    this.specialNote = this.cmsService.getPOSNote();
  }

  reset() {
    this.fromWard = '';
    this.toWard = '';
    this.shipServicesErrorMsg = '';
    this.selectedShipService = null;
    this.shipmentServiceChange.emit(this.selectedShipService);

    this.carriers = this.connectionStore.snapshot.validConnections
      .filter(conn => conn.connection_subtype == 'shipment')
      .map(conn => {
        return {
          name: conn.name,
          id: conn.id,
          logo: conn.provider_logo,
          services: [],
          from_topship: conn.connection_method == 'builtin'
        };
      });
  }

  async getServices(data: FulfillmentAPI.GetShipmentServicesRequest) {
    this.reset();
    this.fromWard = data.from_ward_code;
    this.toWard = data.to_ward_code;

    if (!this.fromWard ||!this.toWard || !this.ffmStore.snapshot.to_create_ffm) {
      return;
    }

    try {
      this.getServicesIndex++;

      const promises = this.carriers.map(carrier => async() => {

        carrier.loading = true;

        try {
          const body: FulfillmentAPI.GetShipmentServicesRequest = {
            ...data,
            connection_ids: [carrier.id],
            index: this.getServicesIndex
          };

          const res = await this.ffmService.getShipmentServices(body);
          if (res.index == this.getServicesIndex) {
            carrier.services = res.services;
            carrier = this.ffmService.sortAndFilterShipmentServices([carrier])[0];
            carrier.loading = false;
          }
        } catch(e) {
          debug.error('ERROR in Getting Shipment Services', e);
          carrier.loading = false;
        }

      });

      await Promise.all(promises.map(p => p()));

    } catch (e) {
      debug.error(e);
      this.shipServicesErrorMsg = e.message;
    }
  }

  selectService(carrierIndex: number, serviceIndex: number) {
    if (!this.carriers[carrierIndex].services[serviceIndex].is_available) {
      return;
    }
    this.carriers.forEach(c => {
      c.services.forEach(s => {
        s.selected = false;
      });
    });
    this.carriers[carrierIndex].services[serviceIndex].selected = true;

    this.selectedShipService = this.carriers[carrierIndex].services[serviceIndex];
    this.shipmentServiceChange.emit(this.selectedShipService);
  }

  carrierHasSelectedService(carrier) {
    return this.selectedShipService && this.selectedShipService.code &&
      carrier.services.some(
        s => s.unique_id == this.selectedShipService?.unique_id
      );
  }

  toCarrierConnect() {
    const slug = this.util.getSlug();
    window.open(`/s/${slug}/settings/shipping`, '_blank');
  }

}
