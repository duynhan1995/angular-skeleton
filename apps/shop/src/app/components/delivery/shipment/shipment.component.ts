import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FromAddressModalComponent} from "@etop/shared/components/from-address-modal/from-address-modal.component";
import List from 'identical-list';
import {AddressService} from 'apps/core/src/services/address.service';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {PopupCreateAddressCustomerComponent} from '../popup-create-address-customer/popup-create-address-customer.component';
import {OrderService} from 'apps/shop/src/services/order.service';
import {Subject} from 'rxjs';
import {FulfillmentService} from 'apps/shop/src/services/fulfillment.service';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {Fulfillment} from 'libs/models/Fulfillment';
import {CustomerStoreService} from 'apps/core/src/stores/customer.store.service';
import {OrderStoreService} from 'apps/core/src/stores/order.store.service';
import {CustomerApi, FulfillmentAPI} from '@etop/api';
import {FulfillmentStore} from 'apps/core/src/stores/fulfillment.store';
import {ShipmentServiceOptionsComponent} from 'apps/shop/src/app/components/delivery/shipment/shipment-service-options/shipment-service-options.component';
import {Address} from 'libs/models/Address';
import {DialogControllerService} from 'apps/core/src/components/modal-controller/dialog-controller.service';
import {CustomerAddressModalComponent} from 'apps/shop/src/app/pages/traders/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {LocationQuery} from "@etop/state/location";

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'shop-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.scss']
})
export class ShipmentComponent extends BaseComponent implements OnInit {
  @ViewChild('shipmentServiceOptions', {static: false}) shipmentServiceOptions: ShipmentServiceOptionsComponent;

  @Output() updateData = new EventEmitter();

  fulfillment = new Fulfillment({});

  cod_amount: number;

  include_insurance = false;

  suggestCODs = new List<FilterOption>([]);

  fromAddresses = [];
  fromAddressIndex: number;
  toAddresses = [];
  toAddressIndex: number;

  productWeights = new List<FilterOption>([
    {id: '100', value: 100},
    {id: '200', value: 200},
    {id: '250', value: 250},
    {id: '300', value: 300},
    {id: '500', value: 500},
    {id: '1000', value: 1000},
    {id: '1500', value: 1500},
    {id: '2000', value: 2000},
    {id: '2500', value: 2500},
    {id: '3000', value: 3000},
    {id: '4000', value: 4000},
    {id: '5000', value: 5000}
  ]);
  minAcceptWeight = 50;

  defaultLength = 5;
  defaultWidth = 5;
  defaultHeight = 5;

  weight_calc_type: number;

  selectCODInput: Subject<void> = new Subject<void>();

  private dialogModal: any;
  private modal: any;

  connectionStoreReady$ = this.connectionStore.state$.pipe(map(s => s?.validConnections?.length));

  defaultWeight$ = this.ffmStore.state$.pipe(
    map(s => s?.defaultWeight), distinctUntilChanged()
  );

  constructor(
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private addressService: AddressService,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private customerApi: CustomerApi,
    private customerStore: CustomerStoreService,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private locationQuery: LocationQuery,
    private connectionStore: ConnectionStore,
  ) {
    super();
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

  get errorCOD() {
    return this.ffmStore.snapshot.error_cod;
  }

  async ngOnInit() {
    await this.setupData();

    this.customerStore.selectCustomer$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async (customer) => {
        if (customer && customer.id) {
          this.toAddresses = customer.addresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = null;
          if (this.shipmentServiceOptions) {
            this.shipmentServiceOptions.reset();
          }
        }
      });
    this.customerStore.selectCustomerAddress$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async (selected_address) => {
        if (selected_address && selected_address.id) {
          const index = this.toAddresses.findIndex(
            addr => addr.id == selected_address.id
          );
          if (index >= 0) {
            if (this.toAddressIndex != index) {
              this.toAddressIndex = index;
              this.setCustomerAddress().then();
            }
          } else {
            this.toAddressIndex = null
          }
        }
      });

    this.orderStore.updateTotalAmount$
      .pipe(takeUntil(this.destroy$))
      .subscribe(total_amount => {
        this.changeCODFromOutside(total_amount);
        this.cod_amount = null;
        this.onChangeCod().then();
        this.updateDataShippingService();
      });

    this.orderService.createOrderFailed$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getShipServices().then();
      });

    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.setupData().then();
      });
  }

  async setupData() {
    this.weight_calc_type = this.ffmService.weight_calc_type;
    this.include_insurance = false;

    const res = await this.addressService.getFromAddresses();

    this.fromAddresses = res.fromAddresses;
    this.fromAddressIndex = res.fromAddressIndex;
    if (this.fromAddresses.length == 0) {
      this.requireAddress();
    }

    const order = this.orderStore.snapshot.selected_order;
    if (order && order.shipping_address && order.shipping_address.province_code) {
      this.toAddresses = [order.shipping_address].map(
        (a, index) => this.addressService.mapAddressInfo(a, index)
      );
      this.toAddressIndex = 0;
    } else {
      this.toAddresses = [];
      this.toAddressIndex = null;
    }

    this.fulfillment = {
      ...new Fulfillment({}),
      cod_amount: null,
      length: this.defaultLength,
      width: this.defaultWidth,
      height: this.defaultHeight,
      try_on: this.auth.snapshot.shop.try_on,
      pickup_address: this.fromAddresses[this.fromAddressIndex],
      shipping_address: this.toAddresses[this.toAddressIndex]
    };

    const lastWeight = localStorage.getItem('lastWeight');
    const shopDefaultWeight = this.ffmStore.snapshot.defaultWeight;

    if (shopDefaultWeight) {
      this.fulfillment.chargeable_weight = order?.shipping?.chargeable_weight || (lastWeight && Number(lastWeight)) || shopDefaultWeight;
    } else {
      this.defaultWeight$.pipe(takeUntil(this.destroy$))
        .subscribe(value => {
          this.fulfillment.chargeable_weight = order?.shipping?.chargeable_weight || (lastWeight && Number(lastWeight)) || value;
        });
    }
  }

  changeCODFromOutside(cod_amount) {
    this.suggestCODs = new List<FilterOption>([{id: '0', value: 0}]);
    if (cod_amount > 0) {
      this.suggestCODs.add({id: cod_amount.toString(), value: cod_amount});
    }
  }

  async weightCalcTypeChange() {
    this.ffmService.weight_calc_type = this.weight_calc_type;
    this.updateDataShippingService();
  }

  focusToAddress() {
    if (this.toAddresses.length) {
      return;
    }
    this.newToAddress();
  }

  async setShopAddress() {
    this.fulfillment.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getShipServices();
  }

  async setCustomerAddress() {
    this.customerStore.selectCustomerAddress(this.toAddresses[this.toAddressIndex]);
    this.fulfillment.shipping_address = this.toAddresses[this.toAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getShipServices();
  }

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({address}) => {
      if (address) {
        const res = await this.addressService.getFromAddresses();
        this.fromAddresses = res.fromAddresses;
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
        this.setShopAddress().then();
      }
    });
  }

  requireAddress() {
    this.dialogModal = this.dialog.createConfirmDialog({
      title: `Thiết lập địa chỉ lấy hàng`,
      body: `
        <div class="text-big">Bạn chưa thiết lập địa chỉ lấy hàng?</div>
        <div class="text-big">Vui lòng tạo địa chỉ lấy hàng đầu tiên để sử dụng dịch vị giao hàng.</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Tạo địa chỉ',
      closeAfterAction: true,
      onConfirm: async () => {
        this.newFromAddress();
      },
      onCancel: () => {
        this.dialogModal.close();
        this.dialogModal = null;
      }
    });
    this.dialogModal.show();
  }

  newToAddress() {
    if (this.modal) {
      return;
    }
    if (!this.ffmStore.snapshot.to_create_customer_by_shipping_address) {
      const customer = this.customerStore.snapshot.selected_customer;
      const order = this.orderStore.snapshot.selected_order;
      let customer_address = {
        full_name: order.customer.full_name,
        phone: order.customer.phone
      }
      this.modal = this.modalController.create({
        component: CustomerAddressModalComponent,
        componentProps: {
          address: customer_address,
          customer_id: customer && customer.id || null,
          title: 'create',
          confirmBtnClass: 'btn-topship'
        }
      });

      if (!customer || !customer.id) {
        this.modal.onDismiss().then(address => {
          address = {
            ...address,
            province: this.locationQuery.getProvince(address.province_code)?.name,
            district: this.locationQuery.getDistrict(address.district_code)?.name,
            ward: this.locationQuery.getWard(address.ward_code)?.name,
          };
          this.toAddresses.push(address);
          this.toAddresses = this.toAddresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = this.toAddresses.length - 1;
          this.setCustomerAddress().then();
        });
      } else {
        this.modal.onDismiss().then(async (address) => {
          this.customerStore.resetSelectedCustomer();
          customer.addresses = await this.customerApi.getCustomerAddresses(customer.id);
          this.customerStore.selectCustomer(customer);
          this.customerStore.selectCustomerAddress(address);
        });
      }
    } else {
      const customer = this.orderStore.snapshot.selected_order?.customer;
      this.modal = this.modalController.create({
        component: PopupCreateAddressCustomerComponent,
        componentProps: {
          customer: customer,
        }
      });
    }

    this.modal.show().then();
    this.modal.onDismiss().then(_ => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async onChangeCod() {
    this.fulfillment.cod_amount = this.cod_amount;
    this.ffmStore.updateFulfillment(this.fulfillment);
    this.updateDataShippingService();
  }

  onShipServiceChange(e) {
    this.fulfillment.shipping_service_name = e && e.name || null;
    this.fulfillment.shipping_service_code = e && e.code || null;
    this.fulfillment.shipping_service_fee = e && e.fee || null;
    this.fulfillment.connection_id =
      (e && e.connection_info && e.connection_info.id) || null;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  toggleInsurance() {
    this.include_insurance = !this.include_insurance;
    this.fulfillment.include_insurance = this.include_insurance;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  updateDataShippingService(getServices = true) {
    this.ffmStore.updateFulfillment(this.fulfillment);
    if (getServices) {
      this.getShipServices().then();
    }
  }

  onWeightChange() {
    localStorage.setItem('lastWeight', this.fulfillment.chargeable_weight.toString());
  }

  validateWeightFn = (s: string) => {
    if (s) {
      const num = Number(s);
      if (Number.isNaN(num) || num < this.minAcceptWeight) {
        return false;
      }
    }
    return true;
  };

  async getShipServices() {
    try {
      const from_address = this.fromAddresses[this.fromAddressIndex];
      const to_address = this.toAddresses[this.toAddressIndex];
      if (from_address && to_address) {
        const data: FulfillmentAPI.GetShipmentServicesRequest = {
          from_province_code: from_address.province_code,
          from_district_code: from_address.district_code,
          from_ward_code: from_address.ward_code,

          to_province_code: to_address.province_code,
          to_district_code: to_address.district_code,
          to_ward_code: to_address.ward_code,

          chargeable_weight: (
            this.ffmService.weight_calc_type == 1 && this.fulfillment.chargeable_weight
          ) || null,
          total_cod_amount: this.cod_amount || 0,
          basket_value: this.orderStore.snapshot.basket_value,
          include_insurance: this.include_insurance,
          length: (
            this.ffmService.weight_calc_type == 2 && this.fulfillment.length
          ) || null,
          width: (
            this.ffmService.weight_calc_type == 2 && this.fulfillment.width
          ) || null,
          height: (
            this.ffmService.weight_calc_type == 2 && this.fulfillment.height
          ) || null
        };

        if (this.connectionStore.snapshot.validConnections?.length) {
          await this.shipmentServiceOptions.getServices(data);
        } else {
          this.connectionStoreReady$.pipe(takeUntil(this.destroy$))
            .subscribe(async () => {
              await this.shipmentServiceOptions.getServices(data);
            });
        }

      } else {
        return this.shipmentServiceOptions.reset();
      }
    } catch (e) {
      debug.error('ERROR in getting shipment services', e);
    }
  }
}
