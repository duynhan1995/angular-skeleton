import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryComponent } from './delivery.component';
import { ShipmentComponent } from 'apps/shop/src/app/components/delivery/shipment/shipment.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { PopupCreateAddressCustomerComponent } from './popup-create-address-customer/popup-create-address-customer.component';
import { ShipnowComponent } from 'apps/shop/src/app/components/delivery/shipnow/shipnow.component';
import { ShipnowServiceOptionsComponent } from './shipnow/shipnow-service-options/shipnow-service-options.component';
import { DeliverySelfComponent } from './delivery-self/delivery-self.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShopPipesModule } from 'apps/shop/src/app/pipes/shop-pipes.module';
import { ShipmentServiceOptionsComponent } from './shipment/shipment-service-options/shipment-service-options.component';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import { EtopFilterModule } from '@etop/shared/components/etop-filter/etop-filter.module';
import { EtopFormsModule } from '@etop/shared/components/etop-forms/etop-forms.module';
import {ShipnowVerifyWarningModule} from "apps/shop/src/app/components/shipnow-verify-warning/shipnow-verify-warning.module";
import {ShippingServiceOptionsPlaceholderModule} from "apps/shop/src/app/components/shipping-service-options-placeholder/shipping-service-options-placeholder.module";

@NgModule({
  declarations: [
    DeliveryComponent,
    ShipmentComponent,
    PopupCreateAddressCustomerComponent,
    ShipnowComponent,
    ShipnowServiceOptionsComponent,
    DeliverySelfComponent,
    ShipmentServiceOptionsComponent,
  ],
  entryComponents: [
    PopupCreateAddressCustomerComponent,
  ],
  exports: [DeliveryComponent, ShipmentComponent, ShipnowComponent, ShipnowServiceOptionsComponent],
  imports: [
    CommonModule,
    SharedModule,
    EtopMaterialModule,
    EtopFilterModule,
    EtopFormsModule,
    EtopPipesModule,
    FormsModule,
    NgbModule,
    ShopPipesModule,
    MaterialModule,
    ShipnowVerifyWarningModule,
    ShippingServiceOptionsPlaceholderModule
  ]
})
export class DeliveryModule { }
