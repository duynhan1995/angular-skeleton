import {
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import {
  ShipnowCarrier, FulfillmentShipnowService
} from 'libs/models/Fulfillment';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import {map, takeUntil} from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import {FulfillmentAPI, FulfillmentApi} from "@etop/api";
import {UtilService} from "apps/core/src/services/util.service";
import {ConnectionStore} from "@etop/features";
import {CmsService} from "apps/core/src/services/cms.service";

@Component({
  selector: 'shop-shipnow-service-options',
  templateUrl: './shipnow-service-options.component.html',
  styleUrls: ['./shipnow-service-options.component.scss']
})
export class ShipnowServiceOptionsComponent extends BaseComponent implements OnInit {
  @Output() shipnowServiceChange = new EventEmitter();

  connectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections.filter(conn => conn.connection_subtype == 'shipnow'))
  );
  carriers: ShipnowCarrier[] = [];
  selectedShipService: FulfillmentShipnowService = null;

  fromWard = '';
  toWards: string[] = [];

  shipServicesErrorMsg = '';

  getServicesIndex = 0;

  specialNote = '';

  fromProvince = '';
  toProvinces: string[] = [];

  constructor(
    private ffmApi: FulfillmentApi,
    private ffmService: FulfillmentService,
    private ffmStore: FulfillmentStore,
    private util: UtilService,
    private connectionStore: ConnectionStore,
    private cmsService: CmsService
  ) {
    super();
  }

  get notSupportedShipnow() {
    const allowedProvinces = ['79', '01'];
    const notSupportedPickup = !this.fromProvince || !allowedProvinces.includes(this.fromProvince);
    const notSupportedDeliver = !this.toProvinces?.length
      || !this.toProvinces?.some(prov => allowedProvinces.includes(prov));
    const notSameProvince = !this.fromProvince || !this.toProvinces?.length
      || !this.toProvinces?.some(prov => this.fromProvince == prov);
    return notSupportedPickup || notSupportedDeliver || notSameProvince;
  }

  ngOnInit() {
    this.ffmService.onResetData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.reset();
      });
    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.carriers = connections.map(conn => {
          return {
            name: conn.name,
            id: conn.id,
            logo: conn.provider_logo,
            services: [],
            from_topship: conn.connection_method == 'builtin'
          };
        });
      });
    this.specialNote = this.cmsService.getPOSNote();
  }

  reset() {
    this.fromWard = '';
    this.toWards = [];

    this.shipServicesErrorMsg = '';
    this.selectedShipService = null;
    this.shipnowServiceChange.emit(this.selectedShipService);

    this.carriers = this.connectionStore.snapshot.validConnections
      .filter(conn => conn.connection_subtype == 'shipnow')
      .map(conn => {
        return {
          name: conn.name,
          id: conn.id,
          logo: conn.provider_logo,
          services: [],
          from_topship: conn.connection_method == 'builtin'
        };
      });
  }

  async getServices(data: FulfillmentAPI.GetShipnowServicesRequest) {
    this.reset();
    this.fromProvince = data.pickup_address?.province_code;
    this.toProvinces = data.delivery_points.map(dp => dp?.shipping_address?.province_code);
    this.fromWard = data.pickup_address?.province_code;
    this.toWards = data.delivery_points.map(dp => dp?.shipping_address?.ward_code);

    if (
      !this.fromWard || !this.toWards?.length ||
      !this.ffmStore.snapshot.to_create_ffm ||
      this.notSupportedShipnow
    ) {
      return;
    }

    try {
      this.getServicesIndex++;

      const promises = this.carriers.map(carrier => async() => {

        carrier.loading = true;

        try {
          const body: FulfillmentAPI.GetShipnowServicesRequest = {
            ...data,
            connection_ids: [carrier.id],
            index: this.getServicesIndex
          };

          const res = await this.ffmService.getShipnowServices(body);
          if (res.index == this.getServicesIndex) {
            carrier.services = res.services;
            carrier = this.ffmService.sortAndFilterShipnowServices([carrier])[0];
            carrier.loading = false;
          }
        } catch(e) {
          debug.error('ERROR in Getting Shipnow Services', e);
          carrier.loading = false;
        }

      });

      await Promise.all(promises.map(p => p()));

    } catch (e) {
      debug.error(e);
      this.shipServicesErrorMsg = e.message;
    }
  }

  selectService(carrierIndex: number, serviceIndex: number) {
    if (!this.carriers[carrierIndex].services[serviceIndex].is_available) {
      return;
    }
    this.carriers.forEach(c => {
      c.services.forEach(s => {
        s.selected = false;
      });
    });
    this.carriers[carrierIndex].services[serviceIndex].selected = true;

    this.selectedShipService = this.carriers[carrierIndex].services[serviceIndex];
    this.shipnowServiceChange.emit(this.selectedShipService);
  }

  carrierHasSelectedService(carrier) {
    return this.selectedShipService && this.selectedShipService.code &&
      carrier.services.some(
        s => s.unique_id == this.selectedShipService?.unique_id
      );
  }

  toCarrierConnect() {
    const slug = this.util.getSlug();
    window.open(`/s/${slug}/settings/shipping`, '_blank');
  }
}
