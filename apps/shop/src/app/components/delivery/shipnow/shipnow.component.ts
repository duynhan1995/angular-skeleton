import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import List from 'identical-list';
import { ShipnowServiceOptionsComponent } from './shipnow-service-options/shipnow-service-options.component';
import { Address } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { GoogleMapService } from 'apps/shop/src/services/google-map.service';
import { PopupCreateAddressCustomerComponent } from '../popup-create-address-customer/popup-create-address-customer.component';
import { OrderService } from '../../../../services/order.service';
import { Subject } from 'rxjs';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import {map, takeUntil} from 'rxjs/operators';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { CustomerAddressModalComponent } from 'apps/shop/src/app/pages/traders/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { FromAddressModalComponent } from 'libs/shared/components/from-address-modal/from-address-modal.component';
import { CustomerApi, FulfillmentAPI } from '@etop/api';
import {LocationQuery} from "@etop/state/location";
import {ConnectionStore} from "@etop/features";

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'shop-shipnow',
  templateUrl: './shipnow.component.html',
  styleUrls: ['./shipnow.component.scss']
})
export class ShipnowComponent extends BaseComponent implements OnInit {
  @ViewChild('shipnowServiceOptions', { static: false }) shipnowServiceOptions: ShipnowServiceOptionsComponent;

  @ViewChild('updateShippingAddressInput', {static: false}) updateShippingAddressInput: ElementRef;
  @ViewChild('updatePickupAddressInput', {static: false}) updatePickupAddressInput: ElementRef;

  @Output() updateData = new EventEmitter();

  fulfillment = new Fulfillment({});

  cod_amount: number;

  suggestCODs = new List<FilterOption>([]);

  fromAddresses = [];
  fromAddressIndex: number;
  toAddresses = [];
  toAddressIndex: number;

  selectCODInput: Subject<void> = new Subject<void>();

  private dialogModal: any;
  private modal: any;

  connectionStoreReady$ = this.connectionStore.state$.pipe(map(s => s?.validConnections?.length));

  shipnowConnections$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections.filter(conn => conn?.connection_subtype == 'shipnow'))
  );

  selectedConnection$ = this.ffmStore.state$.pipe(
    map(s => {
      const connections = this.connectionStore.snapshot.validConnections;
      return connections.find(conn => conn.id == s?.fulfillment?.connection_id);
    })
  );
  notVerifiedConnection$ = this.ffmStore.state$.pipe(
    map(s => {
      const connections = this.connectionStore.snapshot.validConnections;
      const _selected = connections.find(conn => conn.id == s?.fulfillment?.connection_id);
      return _selected?.connection_subtype == 'shipnow' && !_selected?.external_verified;
    })
  );

  constructor(
    private auth: AuthenticateStore,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private addressService: AddressService,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private customerApi: CustomerApi,
    private customerStore: CustomerStoreService,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private locationQuery: LocationQuery,
    private connectionStore: ConnectionStore,
    private googleMap: GoogleMapService,
  ) {
    super();
  }

  get fromAddress() {
    return this.fromAddresses[this.fromAddressIndex];
  }

  get toAddress() {
    return this.toAddresses[this.toAddressIndex];
  }

  set fromAddress(from_address) {
    this.fromAddresses[this.fromAddressIndex] = from_address;
    this.fulfillment.pickup_address = from_address;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  set toAddress(to_address) {
    this.toAddresses[this.toAddressIndex] = to_address;
    this.fulfillment.shipping_address = to_address;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  get errorCOD() {
    return this.ffmStore.snapshot.error_cod;
  }

  async ngOnInit() {
    await this.setupData();

    this.customerStore.selectCustomer$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(customer) => {
        if (customer && customer.id) {
          this.toAddresses = customer.addresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = null;
          if (this.shipnowServiceOptions) {
            this.shipnowServiceOptions.reset();
          }
        }
      });
    this.customerStore.selectCustomerAddress$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(selected_address) => {
        if (selected_address && selected_address.id) {
          const index = this.toAddresses.findIndex(
            addr => addr.id == selected_address.id
          );
          if (index >= 0) {
            if (this.toAddressIndex != index) {
              this.toAddressIndex = index;
              this.setCustomerAddress().then();
            }
          } else {
            this.toAddressIndex = null
          }
        }
      });

    this.orderStore.updateTotalAmount$
      .pipe(takeUntil(this.destroy$))
      .subscribe(total_amount => {
        this.changeCODFromOutside(total_amount);
        this.cod_amount = null;
        this.onChangeCod().then();
        this.updateDataShippingService();
      });
    this.orderService.createOrderFailed$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getShipServices().then();
      });
    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.setupData().then();
      });
  }

  async setupData() {
    const res = await this.addressService.getFromAddresses();

    this.fromAddresses = res.fromAddresses;
    this.fromAddressIndex = res.fromAddressIndex;
    if (this.fromAddresses.length == 0) {
      this.requireAddress();
    }

    const order = this.orderStore.snapshot.selected_order;
    if (order && order.shipping_address && order.shipping_address.province_code) {
      this.toAddresses = [order.shipping_address].map(
        (a, index) => this.addressService.mapAddressInfo(a, index)
      );
      this.toAddressIndex = 0;
    } else {
      this.toAddresses = [];
      this.toAddressIndex = null;
    }

    this.fulfillment = {
      ...new Fulfillment({}),
      cod_amount: null,
      chargeable_weight: 1000,
      pickup_address: this.fromAddresses[this.fromAddressIndex],
      shipping_address: this.toAddresses[this.toAddressIndex]
    };

    this.getCoorsForAddressFrom().then();
    this.getCoorsForAddressTo().then();
  }

  changeCODFromOutside(cod_amount) {
    this.suggestCODs = new List<FilterOption>([{ id: '0', value: 0 }]);
    if (cod_amount > 0) {
      this.suggestCODs.add({ id: cod_amount.toString(), value: cod_amount });
    }
  }

  focusToAddress() {
    if (this.toAddresses.length) { return; }
    this.newToAddress();
  }

  async setShopAddress() {
    this.fulfillment.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getCoorsForAddressFrom();
    await this.getShipServices();
  }

  async setCustomerAddress() {
    this.customerStore.selectCustomerAddress(this.toAddresses[this.toAddressIndex]);
    this.fulfillment.shipping_address = this.toAddresses[this.toAddressIndex];
    this.ffmStore.updateFulfillment(this.fulfillment);
    await this.getCoorsForAddressTo();
    await this.getShipServices();
  }

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (address) {
        const res = await this.addressService.getFromAddresses();
        this.fromAddresses = res.fromAddresses;
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
        this.setShopAddress().then();
      }
    });
  }

  requireAddress() {
    this.dialogModal = this.dialog.createConfirmDialog({
      title: `Thiết lập địa chỉ lấy hàng`,
      body: `
        <div class="text-big">Bạn chưa thiết lập địa chỉ lấy hàng?</div>
        <div class="text-big">Vui lòng tạo địa chỉ lấy hàng đầu tiên để sử dụng dịch vị giao hàng.</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Tạo địa chỉ',
      closeAfterAction: true,
      onConfirm: async() => {
        this.newFromAddress();
      },
      onCancel: () => {
        this.dialogModal.close();
        this.dialogModal = null;
      }
    });
    this.dialogModal.show();
  }

  newToAddress() {
    if (this.modal) {
      return;
    }
    if (!this.ffmStore.snapshot.to_create_customer_by_shipping_address) {
      const customer = this.customerStore.snapshot.selected_customer;
      this.modal = this.modalController.create({
        component: CustomerAddressModalComponent,
        componentProps: {
          address: {
            full_name: customer?.full_name,
            phone: customer?.phone,
          },
          customer_id: customer && customer.id || null,
          title: 'create',
          confirmBtnClass: 'btn-topship'
        }
      });

      if (!customer || !customer.id) {
        this.modal.onDismiss().then(address => {
          address = {
            ...address,
            province: this.locationQuery.getProvince(address.province_code)?.name,
            district: this.locationQuery.getDistrict(address.district_code)?.name,
            ward: this.locationQuery.getWard(address.ward_code)?.name,
          };
          this.toAddresses.push(address);
          this.toAddresses = this.toAddresses.map(
            (a, index) => this.addressService.mapAddressInfo(a, index)
          );
          this.toAddressIndex = this.toAddresses.length - 1;
          this.setCustomerAddress().then();
        });
      } else {
        this.modal.onDismiss().then(async(address) => {
          this.customerStore.resetSelectedCustomer();
          customer.addresses = await this.customerApi.getCustomerAddresses(customer.id);
          this.customerStore.selectCustomer(customer);
          this.customerStore.selectCustomerAddress(address);
        });
      }
    } else {
      this.modal = this.modalController.create({
        component: PopupCreateAddressCustomerComponent
      });
    }

    this.modal.show().then();
    this.modal.onDismiss().then(_ => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async onChangeCod() {
    this.fulfillment.cod_amount = this.cod_amount;
    this.ffmStore.updateFulfillment(this.fulfillment);
    this.updateDataShippingService();
  }

  onShipServiceChange(e) {
    this.fulfillment.carrier = e && e.carrier || null;
    this.fulfillment.shipping_service_name = e && e.name || null;
    this.fulfillment.shipping_service_code = e && e.code || null;
    this.fulfillment.shipping_service_fee = e && e.fee || null;
    this.fulfillment.connection_id =
      (e && e.connection_info && e.connection_info.id) || null;
    this.ffmStore.updateFulfillment(this.fulfillment);
  }

  updateDataShippingService() {
    this.ffmStore.updateFulfillment(this.fulfillment);
    this.getShipServices().then();
  }

  async getShipServices() {
    try {
      const from_address = this.fromAddresses[this.fromAddressIndex];
      const to_address = this.toAddresses[this.toAddressIndex];
      if (from_address && to_address) {
        const { cod_amount, shipping_address, coupon } = this.fulfillment;
        const data: FulfillmentAPI.GetShipnowServicesRequest = {
          coupon,
          pickup_address: from_address,
          delivery_points: [{
            cod_amount,
            shipping_address,
          }]
        };
        if (this.connectionStore.snapshot.validConnections?.length) {
          await this.shipnowServiceOptions.getServices(data);
        } else {
          this.connectionStoreReady$.pipe(takeUntil(this.destroy$))
            .subscribe(async() => {
              await this.shipnowServiceOptions.getServices(data);
            });
        }

      } else {
        return this.shipnowServiceOptions?.reset();
      }
    } catch(e) {
      debug.error('ERROR in getting shipnow services', e);
    }
  }

  async getCoorsForAddressFrom() {
    if (!this.fromAddress) { return; }
    let { coordinates, province } = this.fromAddress;
    if (!(coordinates && coordinates.latitude && coordinates.longitude)) {
      const { address1, ward, district } = this.fromAddress;
      const from_address_fulltext = `${address1}, ${ward}, ${district}, ${province}`;
      const res: any = await this.googleMap.getLatLngFromAddress(from_address_fulltext);
      const { lat, lng } = res;
      this.fromAddress = {
        ...this.fromAddress,
        coordinates: {
          latitude: lat,
          longitude: lng
        }
      };
    }
  }

  async getCoorsForAddressTo() {
    if (!this.toAddress) { return; }
    let { coordinates, province } = this.toAddress;
    if (!(coordinates && coordinates.latitude && coordinates.longitude)) {
      const { address1, ward, district } = this.toAddress;
      const to_address_fulltext = `${address1}, ${ward}, ${district}, ${province}`;
      const res: any = await this.googleMap.getLatLngFromAddress(to_address_fulltext);
      const { lat, lng } = res;
      this.toAddress = {
        ...this.toAddress,
        coordinates: {
          latitude: lat,
          longitude: lng
        }
      };
    }
  }

  toCarrierConnect() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    window.open(`/s/${slug}/settings/shipping`, '_blank');
  }

}
