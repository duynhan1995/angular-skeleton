import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptLinesComponent } from 'apps/shop/src/app/components/receipt-lines/receipt-lines.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [ReceiptLinesComponent],
  imports: [
    CommonModule,
    SharedModule,
    EtopMaterialModule,
    FormsModule,
    EtopPipesModule,
  ],
  exports: [
    ReceiptLinesComponent
  ],
  providers: [
    ReceiptStoreService
  ]
})
export class ReceiptLinesModule { }
