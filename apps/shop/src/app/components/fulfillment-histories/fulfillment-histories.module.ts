import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FulfillmentHistoriesComponent } from 'apps/shop/src/app/components/fulfillment-histories/fulfillment-histories.component';

@NgModule({
  declarations: [
    FulfillmentHistoriesComponent
  ],
  exports: [
    FulfillmentHistoriesComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class FulfillmentHistoriesModule { }
