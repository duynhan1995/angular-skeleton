import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment, Order } from '@etop/models';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';

@Component({
  selector: 'shop-fulfillment-histories',
  templateUrl: './fulfillment-histories.component.html',
  styleUrls: ['./fulfillment-histories.component.scss']
})
export class FulfillmentHistoriesComponent implements OnInit {
  @Input() order = new Order({});
  @Input() ffm = new Fulfillment({});

  hasXShippingLogs = false;
  fulfillmentHistories = [];
  getting_histories = false;

  constructor(
    private ffmService: FulfillmentService
  ) { }

  async ngOnInit() {
    await this.getFulfillmentHistories(this.ffm);
  }

  async getFulfillmentHistories(ffm: Fulfillment) {
    this.getting_histories = true;
    try {
      let _histories = [];
      if (!ffm) { this.hasXShippingLogs = false; _histories = []; }
      if (ffm.x_shipping_logs && ffm.x_shipping_logs.length) {
        this.hasXShippingLogs = true;
        _histories = ffm.x_shipping_logs;
      } else {
        this.hasXShippingLogs = false;
        _histories = await this.ffmService.getFulfillmentHistory(ffm);
      }
      this.fulfillmentHistories = _histories;
    } catch(e) {
      debug.error('ERROR in getting ffm histories', e);
      this.fulfillmentHistories = [];
    }
    this.getting_histories = false;
  }

  get trackingLink() {
    return Fulfillment.trackingLink(this.ffm);
  }

}
