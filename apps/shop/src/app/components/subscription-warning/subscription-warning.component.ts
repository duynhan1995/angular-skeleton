import { SubscriptionService } from '@etop/features';
import { Component, OnInit } from '@angular/core';
import { CmsService } from 'apps/core/src/services/cms.service';
@Component({
  selector: 'shop-subscription-warning',
  templateUrl: './subscription-warning.component.html',
  styleUrls: ['./subscription-warning.component.scss']
})
export class SubscriptionWarningComponent implements OnInit {
  banner = '';
  trialSubscription;
  subscription
  constructor(
    private subscriptionService: SubscriptionService,
    private cms: CmsService,
  ) { }

  async ngOnInit() {
    this.subscription = await this.subscriptionService.checkSubcriptionsNoTrial();
    this.trialSubscription = await this.subscriptionService.checkTrialSubscriptions();
    this.getTrialSubscriptionBanner();
  }

  async getTrialSubscriptionBanner(){
    await this.cms.initBanners();
    const result = this.cms.getMessageNotSubscription();
    if(result){
      this.banner = result.message_web;
      this.banner = this.banner.replace('{{trial_remain}}', this.trialSubscription);
    }
  }

}
