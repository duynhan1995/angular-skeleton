import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionWarningComponent } from './subscription-warning.component'

@NgModule({
  declarations: [
    SubscriptionWarningComponent
  ],
  exports: [
    SubscriptionWarningComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SubscriptionWarningModule { }
