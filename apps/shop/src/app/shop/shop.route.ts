import { Routes } from '@angular/router';

export const shopRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'products',
    loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'inventory',
    loadChildren: () => import('../pages/inventory/inventory.module').then(m => m.InventoryModule)
  },
  {
    path: 'pos',
    loadChildren: () => import('../pages/pos/pos.module').then(m => m.PosModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'orders/:id',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'fulfillments',
    loadChildren: () => import('../pages/fulfillments/fulfillments.module').then(m => m.FulfillmentsModule)
  },
  {
    path: 'traders',
    loadChildren: () => import('apps/shop/src/app/pages/traders/traders.module').then(m => m.TradersModule)
  },
  {
    path: 'receipts',
    loadChildren: () => import('../pages/receipts/receipts.module').then(m => m.ReceiptsModule)
  },
  {
    path: 'money-transactions',
    loadChildren: () => import('../pages/money-transactions/money-transactions.module').then(m => m.MoneyTransactionsModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'settings/:type',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'supports',
    loadChildren: () => import('../pages/supports/supports.module').then(m => m.SupportsModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('../pages/notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: 'etop-trading',
    loadChildren: () => import('../pages/product-trading/product-trading.module').then(m => m.ProductTradingModule)
  },
  {
    path: '**',
    redirectTo: 'orders'
  }
];
