import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FromAddressModalModule} from "@etop/shared/components/from-address-modal/from-address-modal.module";

import { ShopRoutingModule } from './shop-routing.module';
import { ShopComponent } from './shop.component';
import { CoreModule } from 'apps/core/src/core.module';
import { ShopGuard } from './shop.guard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const pages = [];

@NgModule({
  declarations: [ShopComponent, ...pages],
  providers: [ShopGuard],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    ShopRoutingModule,
    FromAddressModalModule
  ]
})
export class ShopModule { }
