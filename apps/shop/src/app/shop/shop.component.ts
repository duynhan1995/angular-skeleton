import {Component, OnInit} from '@angular/core';
import {StringHandler} from "@etop/utils";
import {CommonLayout} from 'apps/core/src/app/CommonLayout';
import {AuthenticateService, AuthenticateStore} from '@etop/core';
import {NavigationEnd, Router} from '@angular/router';
import {UtilService} from 'apps/core/src/services/util.service';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {NavigationData} from 'apps/core/src/components/header/header.interface';
import {NotificationService} from '../../services/notification.service';
import misc from '../../../../core/src/libs/misc';
import {CmsService} from 'apps/core/src/services/cms.service';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {AnnouncementModalComponent} from 'apps/shop/src/app/components/modals/announcement-modal/announcement-modal.component';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {MenuItem} from 'apps/core/src/components/menu-item/menu-item.interface';
import {AppService} from '@etop/web/core/app.service';
import {StorageService} from 'apps/core/src/services/storage.service';
import {ConnectionService} from "@etop/features/connection/connection.service";
import {BankService} from "@etop/state/bank";
import {ArrayHandler} from '@etop/utils/array-handler';

@Component({
  selector: 'shop-shop',
  templateUrl: '../../../../core/src/app/common.layout.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent extends CommonLayout implements OnInit {
  sideMenus: MenuItem[] = [
    {
      name: 'Tổng quan',
      route: ['dashboard'],
      matIcon: 'insert_chart',
      matIconOutlined: true,
      permissions: ['shop/dashboard:view'],
      display_submenus: true,
      isDivider: true,
      submenus: [
        {name: 'Bán hàng', route: ['dashboard','d-pos'], permissions: ['shop/order:view'],hidden: false},
        {name: 'Giao hàng', route: ['dashboard','topship'], permissions: ['shop/fulfillment:view'], hidden: false},
        {name: 'Báo cáo', route: ['dashboard','report'], permissions: ['shop/order:view'], hidden: false},
      ]
    },
    {
      name: 'POS - Tạo đơn',
      route: ['pos'],
      matIcon: 'point_of_sale',
      matIconOutlined: true,
      href: `/s/${this.util.getSlug()}/pos`,
      permissions: ['shop/order:create']
    },
    {
      name: 'Hoá đơn',
      route: ['orders'],
      matIcon: 'receipt',
      matIconOutlined: true,
      permissions: ['shop/order:view', 'shop/purchase_order:view'],
      display_submenus: true,
      submenus: [
        {name: 'Bán hàng', route: ['orders', 'orders'],permissions: ['shop/order:view'], hidden: false},
        {name: 'Nhập hàng', route: ['orders', 'purchase-orders'],permissions: ['shop/purchase_order:view'], hidden: false},
      ],
    },
    {
      name: 'Giao hàng',
      route: ['fulfillments'],
      matIcon: 'local_shipping',
      matIconOutlined: true,
      display_submenus: true,
      permissions: ['shop/fulfillment:view','shop/money_transaction:view', 'shop/shipnow:view'],
      submenus: [
        {name: 'Giao nhanh', route: ['fulfillments','shipment'],permissions: ['shop/fulfillment:view'], hidden: false},
        {name: 'Giao tức thì', route: ['fulfillments','shipnow'],permissions: ['shop/shipnow:view'], hidden: false},
        {name: 'Import', route: ['fulfillments','import'],permissions: ['shop/fulfillment:view'], hidden: false},
        {name: 'Phiên đối soát', route: ['money-transactions'],permissions: ['shop/money_transaction:view'], hidden: false},
      ]
    },
    {
      name: 'Sản phẩm',
      route: ['products'],
      matIcon: 'sell',
      matIconOutlined: true,
      permissions: ['shop/product/basic_info:view'],
      display_submenus: false,
      submenus: [
        {name: 'Danh sách', route: ['products','list'], hidden: true},
        {name: 'Danh mục', route: ['products','category'], hidden: true},
        {name: 'Bộ sưu tập', route: ['products','collect'], hidden: true},
      ]
    },
    {
      name: 'Tồn kho',
      route: ['inventory'],
      matIcon: 'grid_view',
      matIconOutlined: true,
      display_submenus: true,
      submenus: [
        {name: 'Tất cả ', route: ['inventory','all'], permissions: ['shop/inventory:view'], hidden: false},
        {name: 'Nhập kho', route: ['inventory','in'], permissions: ['shop/inventory:view'], hidden: false},
        {name: 'Xuất kho', route: ['inventory','out'], permissions: ['shop/inventory:view'], hidden: false},
        {name: 'Kiểm kho', route: ['inventory','stocktake'], permissions: ['shop/stocktake:view'], hidden: false}
      ],
      permissions: ['shop/inventory:view','shop/stocktake:view']
    },
    {
      name: 'Thu chi',
      route: ['receipts'],
      matIcon: 'payments',
      matIconOutlined: true,
      permissions: ['shop/receipt:view'],
      display_submenus: true,
      submenus: [
        {name: 'Tất cả', route: ['receipts','all'],permissions: ['shop/receipt:view'], hidden: false},
        {name: 'Tiền mặt', route: ['receipts','cash'],permissions: ['shop/receipt:view'], hidden: false},
        {name: 'Chuyển khoản', route: ['receipts','bank'],permissions: ['shop/receipt:view'], hidden: false},
      ],
    },
    {
      name: 'Đối tác',
      route: ['traders'],
      matIcon: 'account_box',
      matIconOutlined: true,
      permissions: ['shop/customer:view', 'shop/supplier:view', 'shop/carrier:view'],
      display_submenus: true,
      isDivider: true,
      submenus: [
        {name: 'Khách hàng', route: ['traders','customers'],permissions: ['shop/customer:view'], hidden: false},
        {name: 'Nhà cung cấp', route: ['traders','suppliers'],permissions: ['shop/supplier:view'], hidden: false},
        {name: 'Nhà vận chuyển', route: ['traders','carriers'],permissions: ['shop/carrier:view'], hidden: false},
      ],
    },
    {
      name: 'Thiết lập',
      route: ['settings'],
      matIcon: 'settings',
      matIconOutlined: true,
      display_submenus: true,
      submenus: [
        {name: 'Thông tin cửa hàng', route: ['settings', 'shop'],permissions: ['shop/settings/shop_info:view','shop/settings/company_info:view'], hidden: false},
        {name: 'Giao hàng', route: ['settings','shipping'],permissions: ['shop/settings/shipping_setting:view'], hidden: false},
        {name: 'Nhân viên', route: ['settings', 'staff'],permissions: ['relationship/relationship:view','relationship/invitation:view'], hidden: false},
        {name: 'Tài khoản', route: ['settings', 'user'], hidden: false},
        {name: 'Danh sách cửa hàng', route: ['settings', 'accounts'], hidden: false},
      ],
    },
    {
      name: 'Hỗ trợ',
      route: ['supports'],
      matIcon: 'support',
      matIconOutlined: true,
      display_submenus: false,
      isDivider: true,
      submenus: [
        {name: 'Liên hệ', route: ['supports','contact'], hidden: true},
        {name: 'Yêu cầu hỗ trợ', route: ['supports','support'], hidden: true},
      ],
    },
    {
      name: 'Thông báo',
      route: ['notifications'],
      icon: 'assets/images/alarm_bell.png',
      hidden: true
    },
    {
      name: 'Nguồn hàng',
      route: ['etop-trading'],
      matIcon: 'storefront',
      matIconOutlined: true,
      hidden: false,
      icon_color: '#ff7300',
      display_submenus: false,
      submenus: [
        {name: 'eB2B', route: ['etop-trading','eb2b'], hidden: true},
        {name: 'eOrder', route: ['etop-trading','eorder'], hidden: true},
      ],
    },
    {
      name: 'Phiên chuyển tiền',
      route: ['money-transactions'],
      icon: 'assets/images/icon_transaction.png',
      permissions: ['shop/money_transaction:view'],
      hidden: true,
    },
  ];

  customSidebar: any = {};

  hideSidebar = !this.permissionsOfActionsArray?.length;
  hideHeader = false;
  showVerifyWarning = true;
  showNewSidebar = true;

  noti_page = 1;
  private announcement_modal: any;

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
    private util: UtilService,
    private headerController: HeaderControllerService,
    private notification: NotificationService,
    private cms: CmsService,
    private modalController: ModalController,
    private commonUsecase: CommonUsecase,
    private connectionService: ConnectionService,
    private appService: AppService,
    private storageService: StorageService,
    private bankService: BankService,
    private authenticateService: AuthenticateService,
  ) {
    super(auth);
  }

  get permissionsOfActionsArray() {
    let permissions = [];
    this.sideMenus.forEach(m => {
      if (m.permissions?.length && this.authenticateService.checkPermissions(m.permissions, 'or')) {
        permissions = permissions.concat(m.permissions);
      } else if (!m.permissions?.length) {
        permissions = permissions.concat(['default']);
      }
    });
    return permissions;
  }

  async ngOnInit() {
    this.bankService.initBanks().then();
    this.connectionService.getValidConnections(true).then();

    if (this.appService.appID != 'etop.vn') {
      this.showVerifyWarning = false;
    }
    this.adminLoginMode = this.storageService.get('adminLogin');

    this.layoutHandlingByUrl();
    this.auth.isAuthenticated$.subscribe((isAuthenticated) => {
      if (isAuthenticated) {
        this.getNotifications(1).then();
        this.getAnnouncementContent();
      } else {
        this.notification.deleteDevice().then();
      }
    });
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd && this.auth.snapshot.isAuthenticated) {
        this.getNotifications(1);
        this.noti_page = 1;
      }
    });

    if (this.cms.bannersLoaded) {
      this.displayUserManuals();
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        this.displayUserManuals();
      });
    }
    this.headerController.onFilterNotisByAccount.subscribe((token: string) => {
      this.getNotifications(1, token);
    });
    this.headerController.onNavigate.subscribe(({target, data}) => {
      this.onNavigate(target, data);
    });
    this.headerController.onRequestMoreNotis.subscribe((token: string) => {
      this.getNotifications(null, token);
    });
  }

  displayUserManuals() {
    const user_manuals = this.cms.getUserManuals();
    if (!user_manuals) {
      return;
    }
  }

  getTopShipConfig() {
    return this.appService.appID == 'etop.vn' ? 'TOPSHIP' : 'Giao hàng';
  }

  getIconTopShipConfig() {
    return this.appService.appID == 'etop.vn' ? 'assets/images/icon_topship.png' : 'assets/images/icon_ffm.png';
  }

  getIconColorTopShipConfig() {
    return this.appService.appID == 'etop.vn' ? '#e67e22' : '';
  }

  getAnnouncementContent() {
    if (this.cms.bannersLoaded) {
      const result = this.cms.getAnnouncementContents();
      if (result) {
        this.handleAndOpenAnnounceContent(result);
      }
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        const result = this.cms.getAnnouncementContents();
        if (result) {
          this.handleAndOpenAnnounceContent(result);
        }
      });
    }
  }

  handleAndOpenAnnounceContent(result) {
    const auth = this.auth.snapshot;
    if (!(auth && auth.user && auth.user.id)) {
      return;
    }
    if (this.announcement_modal) {
      return;
    }
    const content = this.util.keepHtmlCssStyle(
      StringHandler.trimNewLineCharacters(result.split(/content##<\/.>/)[1])
    );
    const content_structure = StringHandler.trimNewLineCharacters(StringHandler.trimHtml(result));
    const announcement_content: any = {};
    for (let item of content_structure.split('####')) {
      const key = item.split('##')[0];
      const value = item.split('##')[1];
      if (key == 'content') {
        announcement_content[key] = content;
      } else {
        announcement_content[key] = value;
      }
    }
    const {title, handle, loop, day_from, day_to, image_only} = announcement_content;
    let displayed_count_saved = Number(localStorage.getItem(handle));
    const from = new Date(day_from).getTime();
    const to = new Date(day_to).getTime();
    const now = new Date().getTime();
    if (!(title && content && handle)) return;
    if (now < from || now > to) return;
    if (displayed_count_saved && displayed_count_saved >= Number(loop)) return;

    this.announcement_modal = this.modalController.create({
      component: AnnouncementModalComponent,
      showBackdrop: true,
      cssClass: 'modal-lg',
      componentProps: {
        announcement_content
      }
    });
    this.announcement_modal.show().then();
    this.announcement_modal.onDismiss().then(_ => {
      this.announcement_modal = null;
      if (displayed_count_saved) displayed_count_saved += 1;
      else displayed_count_saved = 1;
      localStorage.setItem(handle, displayed_count_saved.toString());
    });
  }

  async getNotifications(page?: number, token?: string) {
    try {
      if (!page) {
        this.noti_page += 1;
        page = this.noti_page;
      }
      const res: any = await this.notification.getNotifications(
        (page - 1) * 10,
        10,
        token
      );
      if (res?.notifications?.length < 10) {
        this.noti_page -= 1;
      }
      if (page > 1) {
        res.notifications.forEach(noti => {
          ArrayHandler.upsert(this.notifications, noti);
        });
      } else {
        this.notifications = res.notifications;
      }
      this.headerController.loadNotifications(this.notifications);
    } catch (e) {
      debug.log('ERROR in getting notifications in shop', e);
    }
  }

  layoutHandlingByUrl() {
    if (this.router.url.indexOf('/pos') != -1) {
      this.hideHeader = true;
      this.hideSidebar = true;
    }
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.indexOf('/pos') != -1) {
          this.hideHeader = true;
          this.hideSidebar = true;
        } else {
          this.hideHeader = false;
          this.hideSidebar = false;
        }
      }
    });
  }

  async onNavigate(target, data: NavigationData) {
    let slug: any = '';
    if (data.payload) {
      let accIndex = this.auth.snapshot.accounts.findIndex(a => a.id == data.payload.id);
      accIndex = accIndex < 0 ? 0 : accIndex;
      const acc = this.auth.snapshot.accounts[accIndex];
      slug = acc && acc.url_slug || accIndex;
    } else {
      slug = this.util.getSlug();
    }
    let url = '';
    switch (data.target) {
      case 'account':
        this.notification.deleteDevice();
        url = `/s/${slug}/settings/shop`;
        window.open(url, '_blank');
        break;
      case 'setting':
        url = `/s/${slug}/settings/user`;
        this.router.navigateByUrl(url);
        break;
      case 'notification':
        let shop_id_encoded = '';
        if (data.payload.token) {
          shop_id_encoded = misc.encodeBase64(data.payload.token);
        }
        await this.notification.readNotification(data.payload, shop_id_encoded);
        break;
      case 'notifications':
        url = `/s/${slug || this.util.getSlug()}/notifications`;
        this.router.navigateByUrl(url);
        break;
      default:
        url = '';
    }
  }
}
