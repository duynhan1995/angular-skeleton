import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CarriersComponent } from './carriers/carriers.component';
import { TradersComponent } from 'apps/shop/src/app/pages/traders/traders.component';
import { TradersControllerService } from 'apps/shop/src/app/pages/traders/traders-controller.service';
import { SuppliersModule } from 'apps/shop/src/app/pages/traders/suppliers/suppliers.module';
import { CustomersModule } from 'apps/shop/src/app/pages/traders/customers/customers.module';
import { AuthenticateModule } from '@etop/core';
import { NotPermissionModule } from '@etop/shared';
import { NotSubscriptionModule } from '@etop/shared/components/not-subscription/not-subscription.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { CustomersComponent } from './customers/customers.component';

const routes: Routes = [
  {
    path: '',
    component: TradersComponent,
    children: [
      {
        path: 'customers',
        component: CustomersComponent
      },
      {
        path: 'carriers',
        component: CarriersComponent
      },
      {
        path: 'suppliers',
        component: SuppliersComponent
      },
    ]
  }
];

@NgModule({
  declarations: [
    CarriersComponent,
    TradersComponent,
  ],
  imports: [
    CommonModule,
    CustomersModule,
    SuppliersModule,
    RouterModule.forChild(routes),
    AuthenticateModule,
    NotPermissionModule,
    NotSubscriptionModule,
    SubscriptionWarningModule
  ],
  providers: [
    TradersControllerService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TradersModule {}
