import { Injectable } from '@angular/core';
import { Customer } from 'libs/models/Customer';
import { CustomerListComponent } from 'apps/shop/src/app/pages/traders/customers/customer-list/customer-list.component';
import { SupplierListComponent } from 'apps/shop/src/app/pages/traders/suppliers/supplier-list/supplier-list.component';
import { Supplier } from 'libs/models/Supplier';

@Injectable()
export class TradersControllerService {
  private customerListComponent: CustomerListComponent;
  private supplierListComponent: SupplierListComponent;

  constructor() { }

  registerCustomerList(instance: CustomerListComponent) {
    this.customerListComponent = instance;
  }

  registerSupplierList(instance: SupplierListComponent) {
    this.supplierListComponent = instance;
  }

  async reloadCustomerList() {
    await this.customerListComponent.reloadCustomers();
  }

  async reloadSupplierList() {
    await this.supplierListComponent.reloadSuppliers();
  }

  setupDataCustomer(customer: Customer): Customer {
    customer.p_data = {
      ...customer.p_data,
      id: customer.id,
      full_name: customer.full_name,
      phone: customer.phone,
      gender: customer.gender,
      email: customer.email,
      birthday: customer.birthday,
      note: customer.note,
      edited: false,
      editedField: {}
    };
    return customer;
  }

  setupDataSupplier(supplier: Supplier): Supplier {
    supplier.p_data = {
      ...supplier.p_data,
      id: supplier.id,
      full_name: supplier.full_name,
      phone: supplier.phone,
      company_name: supplier.company_name,
      email: supplier.email,
      tax_number: supplier.tax_number,
      headquater_address: supplier.headquater_address,
      note: supplier.note,
      edited: false,
      editedField: {}
    };
    return supplier;
  }

}
