import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';

@Component({
  selector: 'shop-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent extends PageBaseComponent implements OnInit {
  constructor(
    private traderStore: TraderStoreService,
  ) {
    super();
  }

  ngOnInit() {
    this.traderStore.changeHeaderTab('customer');
  }

}
