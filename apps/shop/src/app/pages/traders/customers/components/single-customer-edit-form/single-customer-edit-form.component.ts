import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Customer } from '@etop/models';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import * as moment from 'moment';
import { TradersControllerService } from 'apps/shop/src/app/pages/traders/traders-controller.service';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-single-customer-edit-form',
  templateUrl: './single-customer-edit-form.component.html',
  styleUrls: ['./single-customer-edit-form.component.scss']
})
export class SingleCustomerEditFormComponent implements OnInit, OnChanges {
  @Input() customer: Customer;
  genders = [{ name: 'Nam', value: 'male' }, { name: 'Nữ', value: 'female' }];
  minDate = new Date(1900, 0, 1);
  maxDate = new Date(2020, 0, 1);
  activeTab: 'order' | 'receipt' | 'address' = 'order';

  constructor(
    private customerService: CustomerService,
    private customersController: TradersControllerService,
    private authStore: AuthenticateStore
  ) {}

  ngOnInit() {
    if (this.authStore.snapshot.permission.permissions.includes('shop/order:view')) {
      this.activeTab = 'order';
    }
    else if (this.authStore.snapshot.permission.permissions.includes('shop/receipt:view')) {
      this.activeTab = 'receipt';
    }
    else {
      this.activeTab = 'address';
    }
  }

  ngOnChanges(changes: SimpleChanges): void {}

  onDataEdited(value, field) {
    this.customer.p_data.edited = true;
    this.customer.p_data.editedField[field] = true;
  }

  async updateCustomer() {
    try {
      if (this.customer.p_data.edited) {
        if (this.customer.p_data.birthday) {
          this.customer.p_data.birthday = moment(this.customer.p_data.birthday)
            .format('YYYY-MM-DD');
        }
        const body = {
          ...this.customer.p_data,
        };
        if (!body.full_name) {
          return toastr.error('Chưa nhập tên khách hàng!');
        }
        if (!body.phone) {
          return toastr.error('Chưa nhập số điện thoại khách hàng!');
        }
        await this.customerService.updateCustomer(body);
        this.customersController.reloadCustomerList();
      }
      toastr.success('Cập nhật thông tin thành công!');
    } catch (e) {
      toastr.error('Cập nhật thông tin thất bại', e.message);
    }
  }

  get isAnonymous() {
    return this.customer.type === 'anonymous';
  }

}
