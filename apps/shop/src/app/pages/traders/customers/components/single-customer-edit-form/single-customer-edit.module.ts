import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SingleCustomerEditFormComponent } from './single-customer-edit-form.component';
import { CustomerOrdersComponent } from './components/customer-orders/customer-orders.component';
import { CustomerAddressesComponent } from './components/customer-addresses/customer-addresses.component';
import { CustomerReceiptsComponent } from 'apps/shop/src/app/pages/traders/customers/components/single-customer-edit-form/components/customer-receipts/customer-receipts.component';
import { AuthenticateModule } from '@etop/core';
import {EtopMaterialModule, EtopPipesModule} from '@etop/shared';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        AuthenticateModule,
        EtopPipesModule,
        EtopMaterialModule,
    ],
  exports: [SingleCustomerEditFormComponent],
  entryComponents: [],
  declarations: [
    SingleCustomerEditFormComponent,
    CustomerOrdersComponent,
    CustomerAddressesComponent,
    CustomerReceiptsComponent
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleCustomerEditFormModule { }
