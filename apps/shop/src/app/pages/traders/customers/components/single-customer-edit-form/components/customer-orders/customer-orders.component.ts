import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Fulfillment, Order } from '@etop/models';
import { OrderService } from 'apps/shop/src/services/order.service';
import { FilterOperator, Filters } from '@etop/models';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';

@Component({
  selector: 'shop-customer-orders',
  templateUrl: './customer-orders.component.html',
  styleUrls: ['./customer-orders.component.scss']
})
export class CustomerOrdersComponent implements OnInit, OnChanges {
  @Input() customer_id: string;
  @Input() customer_name: string;

  orders: Array<Order> = [];

  loading = false;

  constructor(
    private orderService: OrderService,
    private util: UtilService,
    private router: Router,
    private orderStore: OrderStoreService
  ) { }

  ngOnInit() {}

  async ngOnChanges(changes: SimpleChanges) {
    await this.getOrdersByCustomerId();
  }

  async getOrdersByCustomerId() {
    this.loading = true;
    try {
      const filters: Filters = [{
        name: "customer.id",
        op: FilterOperator.eq,
        value: this.customer_id
      }];
      this.orders = await this.orderService.getOrders(0, 1000, filters);
    } catch(e) {
      debug.error('ERROR in getting orders by customer_id', e);
      this.orders = [];
    }
    this.loading = false;
  }

  viewDetailFFM(order: Order, fulfillment: Fulfillment) {
    const ffm_type = order.fulfillment_type;
    const ffm_code = fulfillment.shipping_code;
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/fulfillments?code=${ffm_code}&type=${ffm_type}`
    );
  }

  viewDetailOrder(order_code: string) {
    this.orderStore.navigate('so', order_code);
  }

}
