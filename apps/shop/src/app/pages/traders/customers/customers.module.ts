import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SingleCustomerEditFormModule} from 'apps/shop/src/app/pages/traders/customers/components/single-customer-edit-form/single-customer-edit.module';
import {CreateCustomerFormModule} from 'apps/shop/src/app/pages/traders/customers/components/create-customer-form/create-customer-form.module';
import {MultipleCustomerEditFormModule} from 'apps/shop/src/app/pages/traders/customers/components/multiple-customer-edit-form/multiple-customer-edit-form.module';
import {CustomerListComponent} from 'apps/shop/src/app/pages/traders/customers/customer-list/customer-list.component';
import {CustomerRowComponent} from 'apps/shop/src/app/pages/traders/customers/components/customer-row/customer-row.component';
import {CustomersComponent} from 'apps/shop/src/app/pages/traders/customers/customers.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {AuthenticateModule} from '@etop/core';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {SideSliderModule} from '@etop/shared/components/side-slider/side-slider.module';
import {EmptyPageModule, EtopCommonModule, EtopPipesModule, MaterialModule} from '@etop/shared';

@NgModule({
  declarations: [
    CustomersComponent,
    CustomerListComponent,
    CustomerRowComponent
  ],
  imports: [
    SingleCustomerEditFormModule,
    CreateCustomerFormModule,
    MultipleCustomerEditFormModule,
    CommonModule,
    SharedModule,
    AuthenticateModule,
    DropdownActionsModule,
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    MaterialModule,
    EmptyPageModule
  ],
  exports: [
    CustomersComponent
  ],
  providers: []
})
export class CustomersModule {
}
