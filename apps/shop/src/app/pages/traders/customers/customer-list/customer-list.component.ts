import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { Customer } from '@etop/models';
import { Filters } from '@etop/models';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { TradersControllerService } from 'apps/shop/src/app/pages/traders/traders-controller.service';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerApi } from '@etop/api';
import { ActionButton, EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit, OnDestroy {
  @ViewChild('customerTable', { static: true }) customerTable: EtopTableComponent;
  @ViewChild('customerSlider', { static: true }) customerSlider: SideSliderComponent;

  customers: Array<Customer> = [];
  selectedCustomers: Array<Customer> = [];
  filters: Filters = [];
  emptyAction: ActionButton[] = [];
  checkAll = false;
  creatingNewCustomer = false;

  page: number;
  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private customerService: CustomerService,
    private customerApi: CustomerApi,
    private customersController: TradersControllerService,
    private headerController: HeaderControllerService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private dropdownController: DropdownActionsControllerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  private _onSelectModeChanged(value) {
    this.customerTable.toggleLiteMode(value);
    this.customerSlider.toggleLiteMode(value);
  }
  ngOnInit() {
    this._paramsHandling();
    this.emptyAction = [
      {
        title: 'Tạo đơn hàng',
        cssClass: 'btn-primary btn-outline',
        onClick: () => this.gotoCreateOrder()
      },
      {
        title: 'Thêm khách hàng',
        cssClass: 'btn-primary',
        onClick: () => this.createNewCustomer()
      }
    ]
    this.customersController.registerCustomerList(this);
    this.headerController.setActions([
      {
        title: 'Thêm khách hàng',
        cssClass: 'btn btn-primary',
        onClick: () => this.createNewCustomer(),
        permissions: ['shop/customer:create']
      }
    ]);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  async filter($event: Filters) {
    this.selectedCustomers = [];
    if (this.queryParams.code) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/traders`);
    }
    this.filters = $event;
    this.customerTable.resetPagination();
  }

  async getCustomers(page?, perpage?) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.customerTable.toggleNextDisabled(false);

      let _customers = [];
      if (this.queryParams.code) {
        _customers = await this.customerApi.getCustomersByIds([this.queryParams.code]);
      } else {
        _customers = await this.customerService.getCustomers(
          ((page || this.page) - 1) * perpage,
          perpage || this.perpage,
          this.filters
        );
      }

      if (page > 1 && _customers.length == 0) {
        this.customerTable.toggleNextDisabled(true);
        this.customerTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả khách hàng.');
        return;
      }

      if (_customers.length) {
        this.customers = _customers.map(c => this.customersController.setupDataCustomer(c));
        if (this.queryParams.code) {
          this.detail(null, this.customers[0]);
        }
      }

      // Remember here in-order-to reload customer_list after updating customers
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list customers', e);
    }
  }

  async reloadCustomers() {
    const { perpage, page } = this;
    const selected_customer_ids = this.selectedCustomers.map(c => c.id);

    let _customers = [];
    if (this.queryParams.code) {
      _customers = await this.customerApi.getCustomersByIds([this.queryParams.code]);
    } else {
      _customers = await this.customerService.getCustomers(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
    }
    this.customers = _customers.map(c => {
      if (selected_customer_ids.indexOf(c.id) != -1) {
        const _customer = this.selectedCustomers.find(
          selected => selected.id == c.id
        );
        if (_customer) {
          c.p_data = _customer.p_data;
        }
      }
      return this.customersController.setupDataCustomer(c);
    });
    this.selectedCustomers = this.customers.filter(
      c => c.p_data.selected || c.p_data.detailed
    );
  }

  async loadPage({ page, perpage }) {
    this.customerTable.loading = true;
    this.changeDetector.detectChanges();
    this._paramsHandling();
    await this.getCustomers(page, perpage);
    this.customerTable.loading = false;
    this.changeDetector.detectChanges();
  }

  checkAllCustomer() {
    this.checkAll = !this.checkAll;
    this.creatingNewCustomer = false;
    this.customers.forEach(c => {
      c.p_data.selected = this.checkAll;
    });
    this._checkSelectMode();
  }

  customerSelected(customer: Customer) {
    this.creatingNewCustomer = false;
    this.customers.forEach(c => c.p_data.detailed = false);
    customer.p_data.selected = !customer.p_data.selected;
    if (!customer.p_data.selected) {
      this.checkAll = false;
    }
    this._checkSelectMode();
  }

  detail(event, customer: Customer) {
    this.creatingNewCustomer = false;
    if (event && event.target.type == 'checkbox' || this.checkedCustomers) { return; }
    this.customers.forEach(c => {
      c.p_data.detailed = false;
    });
    this.checkAll = false;
    customer = this.customersController.setupDataCustomer(customer);
    customer.p_data.detailed = true;
    this.selectedCustomers = [customer];
    this._checkSelectMode();
  }

  private async _checkSelectMode() {
    this.selectedCustomers = this.customers.filter(c =>
      c.p_data.selected || c.p_data.detailed
    );
    this.setupDropdownActions();
    this.selectMode = this.selectedCustomers.length > 0 || this.creatingNewCustomer;
    if (this.queryParams.code && this.selectedCustomers.length == 0) {
      this.filters = [];
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/traders`);
      this.customerTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.customers.forEach(c => {
      c.p_data.selected = false;
      c.p_data.detailed = false;
    });
    this.creatingNewCustomer = false;
    this.selectedCustomers = [];
    this.checkAll = false;
    this._checkSelectMode();
  }

  createNewCustomer() {
    this.checkAll = false;
    this.customers.forEach(c => {
      c.p_data.selected = false;
      c.p_data.detailed = false;
    });
    this.creatingNewCustomer = true;
    this._checkSelectMode();
  }

  async deleteCustomer() {
    try {
      let ids = this.selectedCustomers.map(c => c.id);
      for (const id of ids) {
        await this.customerService.deleteCustomer(id);
      }
      toastr.success('Xóa khách hàng thành công!');
      this.checkAll = false;
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/traders`);
      this.customerTable.resetPagination();
    } catch (e) {
      debug.error('ERROR in deleting customers', e);
      toastr.error(e, 'Xóa khách hàng không thành công');
    }
  }

  gotoCreateOrder() {
    let slug = this.util.getSlug();
    window.open(`/s/${slug}/pos`, '_blank');
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    if (this.queryParams.type != 'customer') {
      return;
    }
  }

  private setupDropdownActions() {
    if (!this.selectedCustomers.length || this.selectedCustomers[0].type == 'independent' || this.selectedCustomers[0].type == 'anonymous') {
      this.dropdownController.clearActions();
      return;
    }
    this.dropdownController.setActions([
      {
        onClick: () => this.deleteCustomer(),
        title: `Xóa khách hàng`,
        cssClass: 'text-danger',
        permissions: ['shop/customer:delete'],
      }
    ]);
  }

  get sliderTitle() {
    if (this.creatingNewCustomer) {
      return 'Thêm khách hàng mới';
    }
    if (this.selectedCustomers.length == 1 && this.showDetailCustomer) {
      return 'Chi tiết khách hàng';
    } else {
      return `Thao tác trên <span class="text-bold">${
        this.selectedCustomers.length
        }</span> khách hàng`;
    }
  }

  get emptyResultFilter() {
    return this.page == 1 && this.customers.length == 0
      && (this.filters.length > 0 || this.queryParams.code);
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy khách hàng phù hợp';
    }
    return 'Cửa hàng của bạn chưa có khách hàng';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get checkedCustomers() {
    return this.customers.some(c => c.p_data.selected);
  }

  get showPaging() {
    return !this.customerTable.liteMode && !this.customerTable.loading;
  }

  get showDetailCustomer() {
    return this.selectedCustomers.length && this.selectedCustomers.length == 1 && !this.checkedCustomers;
  }

}
