import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { Filters } from '@etop/models';
import { TradersControllerService } from 'apps/shop/src/app/pages/traders/traders-controller.service';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { Supplier } from 'libs/models/Supplier';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionButton, EmptyType } from '@etop/shared';
import { SupplierApi } from '@etop/api';

@Component({
  selector: 'shop-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.scss']
})
export class SupplierListComponent implements OnInit, OnDestroy {
  @ViewChild('supplierTable', { static: true }) supplierTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  suppliers: Array<Supplier> = [];
  selectedSuppliers: Array<Supplier> = [];

  filters: Filters = [];
  emptyAction: ActionButton[] = [];
  checkAll = false;
  creatingNewSupplier = false;

  page: number;
  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private supplierService: SupplierService,
    private supplierApi: SupplierApi,
    private suppliersController: TradersControllerService,
    private headerController: HeaderControllerService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private dropdownController: DropdownActionsControllerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  private _onSelectModeChanged(value) {
    this.supplierTable.toggleLiteMode(value);
    this.slider.toggleLiteMode(value);
  }
  ngOnInit() {
    this._paramsHandling();

    this.emptyAction = [
      {
        title: 'Tạo đơn nhập hàng',
        cssClass: 'btn-primary btn-outline',
        onClick: () => this.gotoCreatePurchaseOrder()
      },
      {
        title: 'Thêm nhà cung cấp',
        cssClass: 'btn-primary',
        onClick: () => this.createNewSupplier()
      }
    ]
    this.suppliersController.registerSupplierList(this);
    this.headerController.setActions([
      {
        title: 'Thêm nhà cung cấp',
        cssClass: 'btn btn-primary',
        onClick: () => this.createNewSupplier(),
        permissions: ['shop/supplier:create']
      }
    ]);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  async filter($event: Filters) {
    this.selectedSuppliers = [];
    if (this.queryParams.code) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/traders`);
    }
    this.filters = $event;
    this.supplierTable.resetPagination();
  }

  async getSuppliers(page?, perpage?) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.supplierTable.toggleNextDisabled(false);

      let _suppliers = [];
      if (this.queryParams.code) {
        _suppliers = await this.supplierApi.getSuppliersByIds([this.queryParams.code]);
      } else {
         _suppliers = await this.supplierService.getSuppliers(
          ((page || this.page) - 1) * perpage,
          perpage || this.perpage,
          this.filters
        );
      }
      if (page > 1 && _suppliers.length == 0) {
        this.supplierTable.toggleNextDisabled(true);
        this.supplierTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả nhà cung cấp.');
        return;
      }

      if (_suppliers.length) {
        this.suppliers = _suppliers.map(s => this.suppliersController.setupDataSupplier(s));
        if (this.queryParams.code) {
          this.detail(null, this.suppliers[0]);
        }
      }

      // Remember here in-order-to reload supplier_list after updating suppliers
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list suppliers', e);
    }
  }

  async reloadSuppliers() {
    const { perpage, page } = this;
    const selected_supplier_ids = this.selectedSuppliers.map(s => s.id);

    let _suppliers = [];
    if (this.queryParams.code) {
      _suppliers = await this.supplierApi.getSuppliersByIds([this.queryParams.code]);
    } else {
      _suppliers = await this.supplierService.getSuppliers(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
    }
    this.suppliers = _suppliers.map(s => {
      if (selected_supplier_ids.indexOf(s.id) != -1) {
        const _supplier = this.selectedSuppliers.find(
          selected => selected.id == s.id
        );
        if (_supplier) {
          s.p_data = _supplier.p_data;
        }
      }
      return this.suppliersController.setupDataSupplier(s);
    });
    this.selectedSuppliers = this.suppliers.filter(
      s => s.p_data.selected || s.p_data.detailed
    );
  }

  async loadPage({ page, perpage }) {
    this.supplierTable.loading = true;
    this.changeDetector.detectChanges();
    this._paramsHandling();
    await this.getSuppliers(page, perpage);
    this.supplierTable.loading = false;
    this.changeDetector.detectChanges();
  }

  checkAllSupplier() {
    this.checkAll = !this.checkAll;
    this.creatingNewSupplier = false;
    this.suppliers.forEach(s => {
      s.p_data.selected = this.checkAll;
    });
    this._checkSelectMode();
  }

  supplierSelected(supplier: Supplier) {
    this.creatingNewSupplier = false;
    this.suppliers.forEach(s => s.p_data.detailed = false);
    supplier.p_data.selected = !supplier.p_data.selected;
    if (!supplier.p_data.selected) {
      this.checkAll = false;
    }
    this._checkSelectMode();
  }

  detail(event, supplier: Supplier) {
    this.creatingNewSupplier = false;
    if (event && event.target.type == 'checkbox' || this.checkedSuppliers) { return; }
    this.suppliers.forEach(s => {
      s.p_data.detailed = false;
    });
    this.checkAll = false;
    supplier = this.suppliersController.setupDataSupplier(supplier);
    supplier.p_data.detailed = true;
    this.selectedSuppliers = [supplier];
    this._checkSelectMode();
  }

  private async _checkSelectMode() {
    this.selectedSuppliers = this.suppliers.filter(s =>
      s.p_data.selected || s.p_data.detailed
    );
    this.setupDropdownActions();
    this.selectMode = this.selectedSuppliers.length > 0 || this.creatingNewSupplier;
    if (this.queryParams.code && this.selectedSuppliers.length == 0) {
      this.filters = [];
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/traders`);
      this.supplierTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.suppliers.forEach(s => {
      s.p_data.selected = false;
      s.p_data.detailed = false;
    });
    this.creatingNewSupplier = false;
    this.selectedSuppliers = [];
    this.checkAll = false;
    this._checkSelectMode();
  }

  createNewSupplier() {
    this.checkAll = false;
    this.suppliers.forEach(s => {
      s.p_data.selected = false;
      s.p_data.detailed = false;
    });
    this.creatingNewSupplier = true;
    this._checkSelectMode();
  }

  async deleteSupplier() {
    try {
      let ids = this.selectedSuppliers.map(s => s.id);
      for (const id of ids) {
        await this.supplierService.deleteSupplier(id);
      }
      toastr.success('Xóa nhà cung cấp thành công!');
      this.checkAll = false;
      this.supplierTable.resetPagination();
    } catch (e) {
      debug.error('ERROR in deleting suppliers', e);
      toastr.error(e, 'Xóa nhà cung cấp không thành công');
    }
  }

  gotoCreatePurchaseOrder() {
    let slug = this.util.getSlug();
    window.open(`/s/${slug}/orders/purchase-orders/create`, '_blank');
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    if (this.queryParams.type != 'supplier') {
      return;
    }
  }

  private setupDropdownActions() {
    if (!this.selectedSuppliers.length) {
      this.dropdownController.clearActions();
      return;
    }
    this.dropdownController.setActions([
      {
        onClick: () => this.deleteSupplier(),
        title: `Xóa nhà cung cấp`,
        cssClass: 'text-danger',
        permissions: ['shop/supplier:delete']
      }
    ]);
  }

  get sliderTitle() {
    if (this.creatingNewSupplier) {
      return 'Thêm nhà cung cấp mới';
    }
    if (this.selectedSuppliers.length == 1 && this.showDetailSupplier) {
      return 'Chi tiết nhà cung cấp';
    } else {
      return `Thao tác trên <span class="text-bold">${
        this.selectedSuppliers.length
      }</span> nhà cung cấp`;
    }
  }

  get emptyResultFilter() {
    return this.page == 1 && this.suppliers.length == 0
      && (this.filters.length > 0 || this.queryParams.code);
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy nhà cung cấp phù hợp';
    }
    return 'Cửa hàng của bạn chưa có nhà cung cấp';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get checkedSuppliers() {
    return this.suppliers.some(s => s.p_data.selected);
  }

  get showPaging() {
    return !this.supplierTable.liteMode && !this.supplierTable.loading;
  }

  get showDetailSupplier() {
    return this.selectedSuppliers.length && this.selectedSuppliers.length == 1 && !this.checkedSuppliers;
  }

}
