import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuppliersComponent } from 'apps/shop/src/app/pages/traders/suppliers/suppliers.component';
import { SupplierListComponent } from 'apps/shop/src/app/pages/traders/suppliers/supplier-list/supplier-list.component';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SupplierRowComponent } from 'apps/shop/src/app/pages/traders/suppliers/components/supplier-row/supplier-row.component';
import { CreateCustomerFormModule } from 'apps/shop/src/app/pages/traders/suppliers/components/create-supplier-form/create-supplier-form.module';
import { SingleCustomerEditFormModule } from 'apps/shop/src/app/pages/traders/suppliers/components/single-supplier-edit-form/single-supplier-edit.module';
import { MultipleCustomerEditFormModule } from 'apps/shop/src/app/pages/traders/suppliers/components/multiple-supplier-edit-form/multiple-supplier-edit-form.module';
import { SupplierApi, PurchaseOrderApi } from '@etop/api';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { AuthenticateModule } from '@etop/core';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { SideSliderModule } from '@etop/shared/components/side-slider/side-slider.module';
import { EmptyPageModule, EtopCommonModule, EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    SuppliersComponent,
    SupplierListComponent,
    SupplierRowComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CreateCustomerFormModule,
    SingleCustomerEditFormModule,
    MultipleCustomerEditFormModule,
    AuthenticateModule,
    DropdownActionsModule,
    EtopCommonModule,
    SideSliderModule,
    EtopPipesModule,
    EmptyPageModule
  ],
  exports: [
    SuppliersComponent
  ],
  providers: [
    SupplierService,
    SupplierApi,
    PurchaseOrderService,
    PurchaseOrderApi
  ]
})
export class SuppliersModule { }
