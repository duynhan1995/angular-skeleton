import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FilterOperator, Filters } from '@etop/models';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { Receipt } from '@etop/models';
import { ReceiptApi } from '@etop/api';

@Component({
  selector: 'shop-supplier-receipt',
  templateUrl: './supplier-receipts.component.html',
  styleUrls: ['./supplier-receipts.component.scss']
})
export class SupplierReceiptsComponent implements OnInit, OnChanges {
  @Input() supplier_id: string;
  @Input() supplier_name: string;

  receipts: Array<Receipt> = [];

  loading = false;

  constructor(
    private receiptService: ReceiptService,
    private receiptApi: ReceiptApi,
    private router: Router,
    private util: UtilService
  ) { }

  ngOnInit() {
  }

  async ngOnChanges(changes: SimpleChanges) {
    await this.getReceipts();
  }

  async getReceipts() {
    this.loading = true;
    try {
      const filters: Filters = [{
        name: "trader_id",
        op: FilterOperator.eq,
        value: this.supplier_id
      }];
      const res = await this.receiptService.getReceipts(0, 1000, filters);
      this.receipts = res.receipts.map(r => this.receiptApi.receiptMap(r));
    } catch (e) {
      this.receipts = [];
    }
    this.loading = false;
  }

  viewDetailReceipt(code) {
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/receipts?code=${code}`
    );
  }

}
