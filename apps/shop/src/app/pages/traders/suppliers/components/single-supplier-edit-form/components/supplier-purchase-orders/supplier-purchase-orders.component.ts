import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FilterOperator, Filters } from '@etop/models';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { PurchaseOrder, Variant } from '@etop/models';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';

@Component({
  selector: 'shop-supplier-purchase-orders',
  templateUrl: './supplier-purchase-orders.component.html',
  styleUrls: ['./supplier-purchase-orders.component.scss']
})
export class SupplierPurchaseOrdersComponent implements OnInit, OnChanges {
  @Input() supplier_id: string;
  @Input() supplier_name: string;
  purchase_orders: Array<PurchaseOrder> = [];
  variants: Array<Variant> = [];
  loading = false;
  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private orderStore: OrderStoreService
  ) { }

  ngOnInit() {
  }

  async ngOnChanges(changes: SimpleChanges) {
    await this.getPurchaseOrdersBySupplierId();
  }

  async getPurchaseOrdersBySupplierId() {
    this.loading = true;
    try {
      const filters: Filters = [{
        name: "supplier_id",
        op: FilterOperator.eq,
        value: this.supplier_id
      }];
      this.purchase_orders = (await this.purchaseOrderService.getPurchaseOrders(0, 1000, filters)).purchase_orders;
    } catch (e) {
      debug.error('ERROR in getting purchase_orders by supplier_id', e);
      this.purchase_orders = [];
    }
    this.loading = false;
  }

  viewDetailPurchaseOrder(po_code: string) {
    this.orderStore.navigate('po', po_code);
  }

}
