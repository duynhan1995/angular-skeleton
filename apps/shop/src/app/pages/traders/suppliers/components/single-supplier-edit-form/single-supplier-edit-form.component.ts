import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { TradersControllerService } from 'apps/shop/src/app/pages/traders/traders-controller.service';
import { AuthenticateStore } from '@etop/core';
import { Supplier } from '@etop/models';

@Component({
  selector: 'shop-single-supplier-edit-form',
  templateUrl: './single-supplier-edit-form.component.html',
  styleUrls: ['./single-supplier-edit-form.component.scss']
})
export class SingleSupplierEditFormComponent implements OnInit, OnChanges {
  @Input() supplier: Supplier;
  activeTab: 'purchase_order' | 'receipt';

  constructor(
    private supplierService: SupplierService,
    private suppliersController: TradersControllerService,
    private authStore: AuthenticateStore
  ) { }

  ngOnInit() {
    if (this.authStore.snapshot.permission.permissions.includes('shop/purchase_order:view')) {
      this.activeTab = 'purchase_order';
    }
    else if (this.authStore.snapshot.permission.permissions.includes('shop/receipt:view')) {
      this.activeTab = 'receipt';
    }
  }

  ngOnChanges(changes: SimpleChanges): void { }

  onDataEdited(value, field) {
    this.supplier.p_data.edited = true;
    this.supplier.p_data.editedField[field] = true;
  }

  async updateSupplier() {
    try {
      if (this.supplier.p_data.edited) {
        const body = {
          ...this.supplier.p_data,
        };
        if (!body.full_name) {
          return toastr.error('Chưa nhập tên nhà cung cấp!');
        }
        if (!body.phone) {
          return toastr.error('Chưa nhập số điện thoại nhà cung cấp!');
        }
        await this.supplierService.updateSupplier(body);
        this.suppliersController.reloadSupplierList();
      }
      toastr.success('Cập nhật thông tin thành công!');
    } catch (e) {
      toastr.error('Cập nhật thông tin thất bại', e.message);
    }
  }

}
