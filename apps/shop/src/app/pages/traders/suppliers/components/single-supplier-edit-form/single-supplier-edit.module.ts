import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SupplierReceiptsComponent } from 'apps/shop/src/app/pages/traders/suppliers/components/single-supplier-edit-form/components/supplier-receipts/supplier-receipts.component';
import { SingleSupplierEditFormComponent } from 'apps/shop/src/app/pages/traders/suppliers/components/single-supplier-edit-form/single-supplier-edit-form.component';
import { SupplierPurchaseOrdersComponent } from './components/supplier-purchase-orders/supplier-purchase-orders.component';
import { AuthenticateModule } from '@etop/core';
import {EtopMaterialModule, EtopPipesModule} from '@etop/shared';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, AuthenticateModule, EtopPipesModule, EtopMaterialModule],
  exports: [
    SingleSupplierEditFormComponent
  ],
  entryComponents: [],
  declarations: [
    SupplierReceiptsComponent,
    SingleSupplierEditFormComponent,
    SupplierPurchaseOrdersComponent
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleCustomerEditFormModule { }
