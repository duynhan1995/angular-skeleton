import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { Supplier } from '@etop/models';

@Component({
  selector: 'shop-create-supplier-form',
  templateUrl: './create-supplier-form.component.html',
  styleUrls: ['./create-supplier-form.component.scss']
})
export class CreateSupplierFormComponent implements OnInit {
  supplier = new Supplier({});
  @Output() createSupplier = new EventEmitter();

  loading = false;

  constructor(
    private supplierService: SupplierService,
  ) {}

  ngOnInit() {}

  async create() {
    this.loading = true;
    try {
      if (!this.supplier.full_name) {
        this.loading = false;
        return toastr.error('Vui lòng nhập tên!');
      }
      if (!this.supplier.phone) {
        this.loading = false;
        return toastr.error('Vui lòng nhập số điện thoại!');
      }
      await this.supplierService.createSupplier(this.supplier);
      toastr.success('Tạo nhà cung cấp mới thành công');
      this.createSupplier.emit();
      this.supplier = new Supplier({});
    }
    catch (e) {
      toastr.error('Tạo nhà cung cấp thất bại!', e.message);
    }
    this.loading = false;
  }

}
