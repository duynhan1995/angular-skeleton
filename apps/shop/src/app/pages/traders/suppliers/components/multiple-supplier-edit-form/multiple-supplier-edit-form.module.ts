import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { MultipleSupplierEditFormComponent } from 'apps/shop/src/app/pages/traders/suppliers/components/multiple-supplier-edit-form/multiple-supplier-edit-form.component';


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule],
  exports: [MultipleSupplierEditFormComponent],
  entryComponents: [],
  declarations: [MultipleSupplierEditFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MultipleCustomerEditFormModule {}
