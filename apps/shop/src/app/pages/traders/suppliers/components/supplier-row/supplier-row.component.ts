import { Component, Input, OnInit } from '@angular/core';
import { Supplier } from '@etop/models';

@Component({
  selector: '[shop-supplier-row]',
  templateUrl: './supplier-row.component.html',
  styleUrls: ['./supplier-row.component.scss']
})
export class SupplierRowComponent implements OnInit {
  @Input() supplier = new Supplier({});
  @Input() liteMode = false;

  constructor() {}

  ngOnInit() {}

}
