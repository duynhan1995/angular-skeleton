import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { CreateSupplierFormComponent } from 'apps/shop/src/app/pages/traders/suppliers/components/create-supplier-form/create-supplier-form.component';
import {EtopMaterialModule} from "@etop/shared";


@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, EtopMaterialModule],
  exports: [
    CreateSupplierFormComponent
  ],
  entryComponents: [],
  declarations: [
    CreateSupplierFormComponent
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateCustomerFormModule {}
