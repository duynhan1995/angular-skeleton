import {OnInit, Component, OnDestroy} from '@angular/core';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {ActivatedRoute, Router} from '@angular/router';
import {distinctUntilChanged, map} from 'rxjs/operators';
import {TraderStoreService} from 'apps/core/src/stores/trader.store.service';
import {UtilService} from 'apps/core/src/services/util.service';
import {SubscriptionService} from '@etop/features';

@Component({
  selector: 'shop-partners',
  template: `
    <shop-subscription-warning></shop-subscription-warning>
    <ng-container *ngIf="subscription; else notSubscription">
      <ng-container *ngIf="allowView; else notPermission">
        <router-outlet></router-outlet>
      </ng-container>
      <ng-template #notPermission>
        <etop-not-permission></etop-not-permission>
      </ng-template>
    </ng-container>
    <ng-template #notSubscription>
      <etop-not-subscription></etop-not-subscription>
    </ng-template>
  `
})
export class TradersComponent extends BaseComponent implements OnInit, OnDestroy {
  allowView = true;
  subscription = false;

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private router: Router,
    private traderStore: TraderStoreService,
    private subscriptionService: SubscriptionService
  ) {
    super();
  }

  checkTab(tabName) {
    return this.traderStore.headerTabChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  async ngOnInit() {
    this.subscription = await this.subscriptionService.checkSubcriptions();
    if (this.subscription) {
      this.setupTabs();
      this.traderStore.headerTabChanged$.subscribe(_ => {
        this.setupTabs();
      });

      if (this.auth.snapshot.permission.permissions.includes('shop/customer:view')) {
        this.router.navigateByUrl(`/s/${this.util.getSlug()}/traders/customers`);
      } else if (this.auth.snapshot.permission.permissions.includes('shop/supplier:view')) {
        this.router.navigateByUrl(`/s/${this.util.getSlug()}/traders/suppliers`);
      } else {
        this.allowView = false;
      }
    }

  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Khách hàng',
        name: 'customer',
        active: false,
        onClick: () => {
          this.tabClick('customer');
        },
        permissions: ['shop/customer:view']
      },
      {
        title: 'Nhà cung cấp',
        name: 'supplier',
        active: false,
        onClick: () => {
          this.tabClick('supplier');
        },
        permissions: ['shop/supplier:view']
      },
      {
        title: 'Nhà vận chuyển',
        name: 'carrier',
        active: false,
        onClick: () => {
          this.tabClick('carrier');
        },
        permissions: ['shop/carrier:view']
      }
    ].map(tab => {
      this.checkTab(tab.name).subscribe(active => tab.active = active);
      return tab;
    }));
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`s/${this.util.getSlug()}/traders/${type}s`);
  }

}
