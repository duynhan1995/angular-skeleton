import { Component, OnInit } from '@angular/core';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';

@Component({
  selector: 'shop-carriers',
  templateUrl: './carriers.component.html',
  styleUrls: ['./carriers.component.scss']
})
export class CarriersComponent implements OnInit {
  constructor(
    private traderStore: TraderStoreService,
  ) {}

  ngOnInit() {
    this.traderStore.changeHeaderTab('carrier');
  }

}
