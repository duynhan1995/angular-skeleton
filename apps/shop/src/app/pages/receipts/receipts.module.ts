import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SingleReceiptEditFormModule } from './ledgers/components/single-receipt-edit-form/single-receipt-edit-form.module';
import { CreateReceiptFormModule } from './ledgers/components/create-receipt-form/create-receipt-form.module';
import { MultipleReceiptEditFormModule } from './ledgers/components/multiple-receipt-edit-form/multiple-receipt-edit-form.module';
import { ReceiptsComponent } from 'apps/shop/src/app/pages/receipts/receipts.component';
import { LedgersComponent } from 'apps/shop/src/app/pages/receipts/ledgers/ledgers.component';
import { ReceiptListComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/receipt-list/receipt-list.component';
import { ReceiptRowComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/receipt-row/receipt-row.component';
import { ReceiptsControllerService } from 'apps/shop/src/app/pages/receipts/receipts-controller.service';
import { FormsModule } from '@angular/forms';
import { LedgerListModalComponent } from './ledgers/components/ledger-list-modal/ledger-list-modal.component';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { PurchaseOrderApi, SupplierApi } from '@etop/api';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { AuthenticateModule } from '@etop/core';
import { NotPermissionModule } from '../../../../../../libs/shared/components/not-permission/not-permission.module';
import { NotSubscriptionModule } from '../../../../../../libs/shared/components/not-subscription/not-subscription.module';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { LedgerRowComponent } from './ledgers/components/ledger-row/ledger-row.component';
import { CancelObjectModule } from 'apps/shop/src/app/components/cancel-object/cancel-object.module';
import { EmptyPageModule, EtopCommonModule, EtopPipesModule, SideSliderModule } from '@etop/shared';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';

const routes: Routes = [
  {
    path: '', component: ReceiptsComponent,
    children: [
      {
        path: 'all',
        component: LedgersComponent
      },
      {
        path: 'cash',
        component: LedgersComponent
      },
      {
        path: 'bank',
        component: LedgersComponent
      },
      {
        path: '**', redirectTo: 'all'
      }
    ]
  }
];

@NgModule({
  declarations: [
    ReceiptsComponent,
    LedgersComponent,
    ReceiptListComponent,
    ReceiptRowComponent,
    LedgerListModalComponent,
    LedgerRowComponent,
  ],
  entryComponents: [
    LedgerListModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SingleReceiptEditFormModule,
    CreateReceiptFormModule,
    MultipleReceiptEditFormModule,
    RouterModule.forChild(routes),
    FormsModule,
    AuthenticateModule,
    NotPermissionModule,
    DropdownActionsModule,
    CancelObjectModule,
    EtopPipesModule,
    EtopCommonModule,
    SideSliderModule,
    NotSubscriptionModule,
    SubscriptionWarningModule,
    EmptyPageModule
  ],
  providers: [
    ReceiptsControllerService,
    SupplierService,
    SupplierApi,
    PurchaseOrderService,
    PurchaseOrderApi
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReceiptsModule { }
