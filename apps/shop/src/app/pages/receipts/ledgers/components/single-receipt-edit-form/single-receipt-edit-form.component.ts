import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Receipt, ReceiptLine, ReceiptTraderInfo } from 'libs/models/Receipt';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import {
  ReceiptsControllerService
} from 'apps/shop/src/app/pages/receipts/receipts-controller.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { MaterialInputAutocompleteComponent } from 'libs/shared/components/etop-material/material-input-autocomplete/material-input-autocomplete.component';
import { AddLedgerModalComponent } from 'apps/shop/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { FulfillmentApi, OrderApi, PurchaseOrderApi, ReceiptApi } from '@etop/api';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import {ConnectionStore} from "@etop/features";
import {Fulfillment} from "@etop/models";

@Component({
  selector: 'shop-single-receipt-edit-form',
  templateUrl: './single-receipt-edit-form.component.html',
  styleUrls: ['./single-receipt-edit-form.component.scss']
})
export class SingleReceiptEditFormComponent implements OnInit, OnChanges {
  @ViewChild('moneyInput', { static: false }) moneyInput: MaterialInputComponent;
  @ViewChild('ledgerInput', { static: false }) ledgerInput: MaterialInputAutocompleteComponent;

  @Input() receipt = new Receipt({});

  ledgerTypes = [
    { name: 'Tiền mặt', value: 'cash' },
    { name: 'Chuyển khoản', value: 'bank' },
  ];
  ledgers: any[] = [];

  temp_amount: number;
  format_money = true;
  receipt_amount_error = false;

  receipt_type_display = '';
  receipt_lines = [];
  ledger_type = '';

  loading = false;
  preparingReceiptLines = false;
  updateAndConfirm = false;

  ledgerIndex: number;

  private modal: any;

  constructor(
    private receiptsController: ReceiptsControllerService,
    private receiptService: ReceiptService,
    private changeDetector: ChangeDetectorRef,
    private modalController: ModalController,
    private orderApi: OrderApi,
    private purchaseOrderApi: PurchaseOrderApi,
    private fulfillmentApi: FulfillmentApi,
    private receiptApi: ReceiptApi,
    private dialog: DialogControllerService,
    private traderStore: TraderStoreService,
    private connectionStore: ConnectionStore
  ) { }

  get refDisplay() {
    return this.receipt.ref_type_display;
  }

  get isConfirmedReceipt() {
    return this.receipt.status == 'P';
  }

  get isCancelledReceipt() {
    return this.receipt.status == 'N';
  }

  get ledgerBankAccountDisplay() {
    const bank = this.receipt.ledger.bank_account;
    if (!bank) { return ''; }
    const {account_number, account_name, name} = bank;
    return `${account_number}${account_name && (' - ' + account_name) || ''}${name && (' - ' + name) || ''}`;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.format_money = !!this.receipt.p_data.amount;
    this.reset();
    this.temp_amount = this.receipt.amount;
    this.receipt_type_display = ReceiptApi.receiptTypeMap(this.receipt.type);

    this.ledger_type = this.receipt.ledger.type;
    this.displayLedgers();

    this.prepareReceiptLines();
  }

  reset() {
    this.ledgers = [];
  }

  async prepareReceiptLines() {
    this.preparingReceiptLines = true;
    try {
      const line_ids = this.receipt.lines.filter(l => l.ref_id && l.ref_id != "0").map(l => l.ref_id);

      let refs: any[];

      switch (this.receipt.ref_type) {
        case 'order':
          refs = await this.orderApi.getOrdersByIDs(line_ids);
          break;
        case 'purchase_order':
          refs = await this.purchaseOrderApi.getPurchaseOrdersByIDs(line_ids);
          break;
        case 'fulfillment':
          refs = await this.fulfillmentApi.getFulfillmentsByIDs(line_ids);
          refs = refs.map(r => {
            r.code = r.shipping_code;
            return Fulfillment.fulfillmentMap(r, this.connectionStore.snapshot.initConnections);
          });
          break;
        default:
          refs = [];
          break;
      }
      this.receipt_lines = this.receipt.lines.filter(l => l.ref_id && l.ref_id != "0").map(l => {
        const r = refs.find(ref => ref.id == l.ref_id);
        if (r) {
          return {
            ...l,
            total_amount: r.total_amount,
            received_amount: this.receipt.created_type == 'auto' ? l.amount : r.received_amount,
            paid_amount: this.receipt.created_type == 'auto' ? l.amount : r.paid_amount,
            created_at: r.created_at,
            title: r.code,
            code: r.code,
          }
        }
      });
    } catch(e) {
      debug.error('ERROR in preparing Receipt Lines', e);
      this.receipt_lines = [];
    }
    this.preparingReceiptLines = false;
  }

  formatMoney() {
    if (!this.receipt.p_data.amount) {
      this.format_money = false;
      return;
    }
    this.format_money = !this.format_money;
    this.changeDetector.detectChanges();
    if (!this.format_money && this.moneyInput) {
      this.moneyInput.focusInput();
    }
  }

  checkValidAmount() {
    this.receipt_amount_error = false;
    const sumAmount = this.receipt_lines.reduce((a, b) => a + Number(b.amount), 0);
    if (sumAmount > this.receipt.p_data.amount) {
      this.receipt_amount_error = true;
      toastr.error(
        `Giá trị của phiếu ${this.receipt_type_display} phải lớn hơn Tổng số tiền ${this.receipt_type_display} ở phiếu này!`
      );
      return false;
    }
    for (let l of this.receipt_lines) {
      const _amount = Number(l.amount);
      if (this.receipt.type == 'receipt' && _amount > l.total_amount - l.received_amount) {
        l.error = true;
        toastr.error(
          `Số tiền ${this.receipt_type_display} không được lớn hơn Số tiền phải thanh toán - Số tiền đã thanh toán.`
        );
        return false;
      }
    }
    return true;
  }

  displayLedgers() {
    if (this.ledger_type == 'cash') {
      const _ledger = this.receiptsController.ledgers.find(l => l.type == 'cash');
      this.receipt.p_data.ledger_id = _ledger && _ledger.id || null;
    } else {
      const ledgersBank = this.receiptsController.ledgers.filter(l => l.type != 'cash');
      this.ledgers = this.receiptsController.ledgerMapping(ledgersBank);
      this.ledgerIndex = this.ledgers.findIndex(l => l.id == this.receipt.p_data.ledger_id);
      // NOTE: when change from Cash to Bank
      if (this.ledgerIndex < 0) {
        this.ledgerIndex = 0;
        const ledger = this.ledgers[this.ledgerIndex];
        this.receipt.p_data.ledger_id = ledger && ledger.id || null;
      }
    }
  }

  onSelectLedger() {
    const ledger = this.ledgers[this.ledgerIndex];
    this.receipt.p_data.ledger_id = ledger && ledger.id || null;
  }

  focusLedger() {
    if (this.ledgers.length) {
      return;
    }
    this.addLedger();
  }

  private _preProcessingReceiptLines() {
    if (!this.checkValidAmount()) { return false; }

    const sumAmount = this.receipt_lines.reduce((a, b) => a + Number(b.amount), 0);
    this.receipt.p_data.lines = [];
    if (sumAmount < this.receipt.p_data.amount) {
      const additionalLine = {
        ...new ReceiptLine(),
        title: `${this.receipt.type == 'receipt' && 'Cộng' || 'Trừ'} vào tài khoản ${this.receipt.trader_type_display.toLowerCase()}`,
        amount: this.receipt.p_data.amount - sumAmount
      };
      this.receipt.p_data.lines = [additionalLine];
    }
    this.receipt_lines.forEach(l => {
      const _line = {
        title: `Thanh toán ${this.refDisplay.toLowerCase()}`,
        amount: l.amount,
        ref_id: l.ref_id
      };
      this.receipt.p_data.lines.push(_line);
    });
    return true;
  }

  private _validateReceiptInfo() {
    if (!this.receipt.p_data.paid_at) {
      toastr.error('Vui lòng nhập ngày thanh toán');
      return false;
    }
    if (!this.receipt.p_data.amount) {
      toastr.error('Vui lòng nhập giá trị cho phiếu');
      return false;
    }
    if (!this.receipt.p_data.ledger_id) {
      toastr.error('Vui lòng chọn phương thức thanh toán');
      return false;
    }
    if (!this.receipt.p_data.title) {
      toastr.error('Vui lòng nhập nội dung cho phiếu');
      return false;
    }
    return true;
  }

  async update(updateAndConfirm: boolean) {
    this.loading = true;
    this.updateAndConfirm = updateAndConfirm;
    try {
      if (this.receipt.status != 'P' && !this._preProcessingReceiptLines()) {
        this.loading = false;
        this.updateAndConfirm = false;
        return;
      }
      if (!this._validateReceiptInfo()) {
        this.loading = false;
        this.updateAndConfirm = false;
        return;
      }
      const body: Receipt = {
        ...this.receipt.p_data,
        ref_type: this.receipt.ref_type,
        id: this.receipt.id
      };

      if (updateAndConfirm) {
        this.confirm(body);
      } else {
        await this.receiptService.updateReceipt(body);
        toastr.success(`Cập nhật phiếu ${this.receipt_type_display} thành công.`);
        await this.receiptsController.reloadReceiptList();
      }
    } catch(e) {
      debug.error('ERROR in updating Receipt', e);
      toastr.error(`Cập nhật phiếu ${this.receipt_type_display} không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.updateAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu ${this.receipt_type_display}`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu ${this.receipt_type_display}?</div>
        <div>
          Sau khi xác nhận bạn chỉ có thể thay đổi thông tin về
          <strong>tài khoản thanh toán</strong>, <strong>nội dung</strong> và <strong>ghi chú</strong>.
        </div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const res = await this.receiptService.updateReceipt(body);
          toastr.success(`Cập nhật phiếu ${this.receipt_type_display} thành công.`);
          await this.receiptApi.confirmReceipt(res.id);
          toastr.success(`Xác nhận phiếu ${this.receipt_type_display} thành công.`);
          await this.receiptsController.reloadReceiptList();
          this.modal.close();
          this.modal = null;
        } catch(e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Xác nhận phiếu ${this.receipt_type_display} không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();

  }

  addLedger() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddLedgerModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
    })
    this.modal.onDismiss().then(ledger => {
      this.modal = null;
      if (ledger) {
        this.receiptsController.ledgers.push(ledger);

        this.receipt.p_data.ledger_id = ledger.id;

        const _ledgers = this.receiptsController.ledgers.filter(l => l.type != 'cash');
        this.ledgers = this.receiptsController.ledgerMapping(_ledgers);
        this.ledgerIndex = this.ledgers.findIndex(l => l.id == ledger.id);
      }
    });
  }

  viewDetailTrader(trader: ReceiptTraderInfo) {
    if (trader.deleted) { return; }
    this.traderStore.navigate(trader.type, null, trader.id);
  }

}
