import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptLinesComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/receipt-lines/receipt-lines.component';
import { FormsModule } from '@angular/forms';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { LineAmountInputComponent } from './line-amount-input/line-amount-input.component';
import { SearchRefsInputModule } from 'apps/shop/src/app/pages/receipts/ledgers/components/search-refs-input/search-refs-input.module';

@NgModule({
  declarations: [
    ReceiptLinesComponent,
    LineAmountInputComponent,
  ],
  exports: [
    ReceiptLinesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    EtopPipesModule,
    SearchRefsInputModule
  ]
})
export class ReceiptLinesModule { }
