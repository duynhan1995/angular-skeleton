import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { FilterOperator, Filters } from '@etop/models';
import { ReceiptsControllerService } from 'apps/shop/src/app/pages/receipts/receipts-controller.service';
import { Receipt } from '@etop/models';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { ReceiptApi } from '@etop/api';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { CancelObjectComponent } from 'apps/shop/src/app/components/cancel-object/cancel-object.component';
import { LedgerListModalComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/ledger-list-modal/ledger-list-modal.component';
import { ActionButton, EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-receipt-list',
  templateUrl: './receipt-list.component.html',
  styleUrls: ['./receipt-list.component.scss']
})
export class ReceiptListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('receiptTable', { static: true }) receiptTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  receiptList: Array<Receipt> = [];
  selectedReceipts: Array<Receipt> = [];
  totalConfirmedReceipt: number;
  totalConfirmedPayment: number;
  emptyAction: ActionButton[] = [];

  filters: Filters = [];
  checkAll = false;
  creatingNewReceipt = false;

  page: number;
  perpage: number;

  queryParams = {
    code: ''
  };

  private modal: any;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private router: Router,
    private receiptService: ReceiptService,
    private receiptsController: ReceiptsControllerService,
    private customerService: CustomerService,
    private supplierService: SupplierService,
    private modalController: ModalController,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private receiptApi: ReceiptApi,
    private dropdownController: DropdownActionsControllerService,
  ) {
    super();
  }

  private _onSelectModeChanged(value) {
    this.receiptTable.toggleLiteMode(value);
    this.slider.toggleLiteMode(value);
  }
  async ngOnInit() {
    const type = this.router.url.split('/')[4];
    switch(type) {
      case 'cash':
      case 'all':
      case 'bank':
        this.receiptsController.ledger_type = type;
        this.receiptsController.onChangeLedger$.next();
      break;
    }
    // TODO: after new filter implemented, change value in filter Receipt.code!!!
    this._paramsHandling();
    this.emptyAction = [
      {
        title: 'Tạo đơn hàng',
        cssClass: 'btn-primary btn-outline',
        onClick: () => this.gotoCreateOrder()
      },
      {
        title: 'Tạo phiếu thu/chi',
        cssClass: 'btn-primary',
        onClick: () => this.createNewReceipt()
      }
    ]
    this.receiptsController.registerReceiptList(this);
    this.headerController.setActions([
      {
        title: 'Tài khoản thanh toán',
        cssClass: 'btn btn-primary btn-outline',
        onClick: () => this.openLedgerList(),
        permissions: ['shop/ledger:view'],
        roles: [
          'owner', 'staff_management', 'inventory_management',
          'analyst', 'accountant', 'purchasing_management', 'salesman'
        ]
      },
      {
        title: 'Tạo phiếu thu/chi',
        cssClass: 'btn btn-primary',
        onClick: () => this.createNewReceipt(),
        permissions: ['shop/receipt:create'],
        roles: ['owner', 'staff_management', 'inventory_management', 'analyst', 'accountant']
      }
    ]);
    this.receiptsController.ledgers = await this.receiptService.getLedgers();
    this.receiptsController.customers = await this.customerService.getCustomers();
    this.receiptsController.suppliers = await this.supplierService.getSuppliers();
    // TODO: this.receiptsController.carriers = await this.carrierService.getCarriers();

    this.receiptsController.onChangeLedger$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async () => {
        this.filters = [];
        this.queryParams.code = '';
        this.receiptTable.resetPagination();
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    if (!this.queryParams.code) {
      return;
    }
    /* NOTE: required jobs when have queryParams.code:
    * 1/ Turn on state selected of an receipt row
    * 2/ Open Slide to view receipt detail
    * 3/ Reload receipt list when close slider || change tab || create new receipt
    * */
    this.filters = [{
      name: 'code',
      op: FilterOperator.eq,
      value: this.queryParams.code
    }];
  }

  async getReceipts(page?, perpage?) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.receiptTable.toggleNextDisabled(false);
      this.receiptsController.onSliderClosed$.next();

      let res: any = {};
      if (this.receiptsController.ledger_type != 'all') {
        res = await this.receiptService.getReceiptsByLedgerType(
          this.receiptsController.ledger_type,
          ((page || this.page) - 1) * perpage,
          perpage || this.perpage,
          this.filters
        );
      } else {
        res = await this.receiptService.getReceipts(
          ((page || this.page) - 1) * perpage,
          perpage || this.perpage,
          this.filters
        );
      }
      const _receipts = res.receipts;
      this.totalConfirmedReceipt = res.total_amount_confirmed_receipt;
      this.totalConfirmedPayment = res.total_amount_confirmed_payment;

      if (page > 1 && _receipts.length == 0) {
        this.receiptTable.toggleNextDisabled(true);
        this.receiptTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả phiếu thu/chi.');
        return;
      }

      this.receiptList = _receipts.map(r => this.receiptsController.setupDataReceipt(r));
      if (this.queryParams.code) {
        this.detail(null, this.receiptList[0]);
      }

      // NOTE: Remember here in-order-to reload receipt_list after updating receipts
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list receipts', e);
    }
  }

  async reloadReceipts() {
    this.creatingNewReceipt = false;
    const { perpage, page } = this;
    const selected_receipt_ids = this.selectedReceipts.map(r => r.id);

    let res: any = {};
    if (this.receiptsController.ledger_type != 'all') {
      res = await this.receiptService.getReceiptsByLedgerType(
        this.receiptsController.ledger_type,
        ((page || this.page) - 1) * perpage,
        perpage || this.perpage,
        this.filters
      );
    } else {
      res = await this.receiptService.getReceipts(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
    }
    const _receipts = res.receipts;
    this.totalConfirmedReceipt = res.total_amount_confirmed_receipt;
    this.totalConfirmedPayment = res.total_amount_confirmed_payment;

    this.receiptList = _receipts.map(r => {
      if (selected_receipt_ids.indexOf(r.id) != -1) {
        const _receipt = this.selectedReceipts.find(
          selected => selected.id == r.id
        );
        if (_receipt) {
          r.p_data = _receipt.p_data;
        }
      }
      return this.receiptsController.setupDataReceipt(r);
    });
    this.selectedReceipts = this.receiptList.filter(
      p => p.p_data.selected || p.p_data.detailed
    );
  }

  async loadPage({ page, perpage }) {
    this.receiptTable.loading = true;
    this.changeDetector.detectChanges();
    // TODO: after new filter implemented, change value in filter Receipt.code!!!
    this._paramsHandling();
    await this.getReceipts(page, perpage);
    this.receiptTable.loading = false;
    this.changeDetector.detectChanges();
  }

  checkAllReceipt() {
    this.creatingNewReceipt = false;
    this.checkAll = !this.checkAll;
    this.receiptList.forEach(r => {
      r.p_data = {
        selected: this.checkAll
      };
    });
    this._checkSelectMode();
  }

  receiptSelected(receipt) {
    this.creatingNewReceipt = false;
    this.receiptList.forEach(r => r.p_data.detailed = false);
    receipt.p_data.selected = !receipt.p_data.selected;
    if (!receipt.p_data.selected) {
      this.checkAll = false;
    }
  }

  detail(event, receipt: Receipt) {
    this.creatingNewReceipt = false;
    if (event && event.target.type == 'checkbox') { return; }
    if (this.checkedReceipts) { return; }
    this.receiptList.forEach(r => {
      r.p_data.detailed = false;
    });
    this.checkAll = false;
    receipt = this.receiptsController.setupDataReceipt(receipt);
    receipt.p_data.detailed = true;
    this.selectedReceipts = [receipt];
    this._checkSelectMode();
  }

  async createNewReceipt() {
    this.receiptList.forEach(r => {
      r.p_data.selected = false;
      r.p_data.detailed = false;
    });
    this.creatingNewReceipt = true;
    this._checkSelectMode();
  }

  private async _checkSelectMode() {
    this.selectedReceipts = this.receiptList.filter(r =>
      r.p_data.selected || r.p_data.detailed
    );
    const selected = this.selectedReceipts.length;
    this.selectMode = selected > 0 || this.creatingNewReceipt;
    this.setupDropdownActions();
    if (this.queryParams.code) {
      this.filters = [];
      this.queryParams.code = '';
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/receipts`);
      if (this.creatingNewReceipt) {
        await this.reloadReceipts();
      } else if (selected == 0) {
        this.receiptTable.resetPagination();
      }
    }
  }

  openLedgerList() {
    let modal = this.modalController.create({
      component: LedgerListModalComponent,
      showBackdrop: 'static',
      cssClass: 'modal-lg',
      componentProps: {
      }
    });
    modal.show().then();
    modal.onDismiss().then(_ => {
    });
  }

  onSliderClosed() {
    this.receiptList.forEach(r => {
      r.p_data.selected = false;
      r.p_data.detailed = false;
    });
    this.creatingNewReceipt = false;
    this.selectedReceipts = [];
    this.checkAll = false;
    this._checkSelectMode();
    this.receiptsController.onSliderClosed$.next();
  }

  gotoCreateOrder() {
    let slug = this.util.getSlug();
    window.open(`/s/${slug}/pos`, '_blank');
  }

  cancelReceipt() {
    if (this.modal) { return; }
    const receipt = this.selectedReceipts[0];
    const receipt_type_display = ReceiptApi.receiptTypeMap(receipt.type);
    this.modal = this.modalController.create({
      component: CancelObjectComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        title: `phiếu ${receipt_type_display} #${receipt.code}`,
        cancelFn: async (reason: string) => {
          try {
            await this.receiptApi.cancelReceipt(receipt.id, reason);
            toastr.success(`Huỷ phiếu ${receipt_type_display} thành công.`);
          } catch(e) {
            toastr.error(`Huỷ phiếu ${receipt_type_display} không thành công.`, e.code && (e.message || e.msg) || '');
            throw e;
          }
        }
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async() => {
      this.modal = null;
      await this.reloadReceipts();
      this.setupDropdownActions();
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  private setupDropdownActions() {
    if (!this.selectedReceipts.length) {
      this.dropdownController.clearActions();
      return;
    }
    const receipt = this.selectedReceipts[0];
    this.dropdownController.setActions([
      {
        onClick: () => this.cancelReceipt(),
        title: 'Huỷ phiếu',
        cssClass: `text-danger`,
        permissions: ['shop/receipt:cancel'],
        disabled: receipt.status == 'N'
      }
    ]);
  }

  get checkedReceipts() {
    return this.receiptList.some(r => r.p_data.selected);
  }

  get sliderTitle() {
    if (this.creatingNewReceipt) {
      return 'Tạo phiếu thu/chi';
    }
    if (this.selectedReceipts.length == 1 && this.showDetailReceipt) {
      return `Chi tiết phiếu ${this.selectedReceipts[0].type_display.toLowerCase()}`;
    } else {
      return `Thao tác trên <span class="text-bold">${
        this.selectedReceipts.length
        }</span> phiếu thu/chi`;
    }
  }

  get emptyResultFilter() {
    return this.page == 1 && this.receiptList.length == 0
      && (this.filters.length > 0 || this.receiptsController.ledger_type != 'all');
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy phiếu thu/chi phù hợp';
    }
    return 'Cửa hàng của bạn chưa có phiếu thu/chi';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get showPaging() {
    return !this.receiptTable.liteMode && !this.receiptTable.loading;
  }

  get showDetailReceipt() {
    return this.selectedReceipts.length && this.selectedReceipts.length == 1 && !this.checkedReceipts;
  }

}
