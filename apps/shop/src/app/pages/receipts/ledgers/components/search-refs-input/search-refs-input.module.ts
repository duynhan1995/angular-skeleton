import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchRefsInputComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/search-refs-input/search-refs-input.component';
import { FormsModule } from '@angular/forms';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    SearchRefsInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    EtopPipesModule,
    NgbModule
  ],
  exports: [
    SearchRefsInputComponent
  ]
})
export class SearchRefsInputModule { }
