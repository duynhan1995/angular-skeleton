import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Ledger } from 'libs/models/Receipt';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { ReceiptsControllerService } from 'apps/shop/src/app/pages/receipts/receipts-controller.service';
import { AddLedgerModalComponent } from 'apps/shop/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';

@Component({
  selector: '[shop-ledger-row]',
  templateUrl: './ledger-row.component.html',
  styleUrls: ['./ledger-row.component.scss']
})
export class LedgerRowComponent implements OnInit {
  @Output() closeModal = new EventEmitter();
  @Output() showModal = new EventEmitter();

  @Input() ledger: Ledger;

  dropdownActions = [
    {
      title: 'Xóa',
      cssClass: 'text-danger',
      permissions: ['shop/ledger:delete'],
      onClick: () => this.removeLedger()
    }
  ];

  constructor(
    private receiptService: ReceiptService,
    private receiptsController: ReceiptsControllerService,
    private modalController: ModalController,
  ) { }

  private modal: any;

  ngOnInit() {}

  async removeLedger() {
    try {
      await this.receiptService.deleteLedger(this.ledger.id);
      toastr.success('Xóa tài khoản thành công.');
      this.receiptsController.reloadLedgerList();
    }
    catch (e) {
      toastr.error(e.message, 'Xóa tài khoản thất bại.');
    }
  }

  openEditLedgerModal(ledger) {
    if (this.modal) { return; }
    this.closeModal.emit();
    this.modal = this.modalController.create({
      component: AddLedgerModalComponent,
      showBackdrop: 'static',
      cssClass: 'modal-lg',
      componentProps: {
        ledger
      }
    });
    this.modal.show().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
      this.showModal.emit();
    });
    this.modal.onDismiss().then(async () => {
      this.modal = null;
      this.showModal.emit();
      this.receiptsController.reloadLedgerList();
    });
  }

}
