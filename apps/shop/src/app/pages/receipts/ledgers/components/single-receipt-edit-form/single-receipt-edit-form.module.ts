import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { SingleReceiptEditFormComponent } from './single-receipt-edit-form.component';
import { ReceiptLinesModule } from 'apps/shop/src/app/pages/receipts/ledgers/components/receipt-lines/receipt-lines.module';
import { AuthenticateModule } from '@etop/core';
import {EtopMaterialModule, EtopPipesModule} from '@etop/shared';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReceiptLinesModule,
    AuthenticateModule,
    EtopPipesModule,
    EtopMaterialModule,
  ],
  exports: [SingleReceiptEditFormComponent],
  entryComponents: [],
  declarations: [SingleReceiptEditFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleReceiptEditFormModule {}
