import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { Ledger } from '@etop/models';
import { AddLedgerModalComponent } from 'apps/shop/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ReceiptsControllerService } from 'apps/shop/src/app/pages/receipts/receipts-controller.service';

@Component({
  selector: 'shop-ledger-list-modal',
  templateUrl: './ledger-list-modal.component.html',
  styleUrls: ['./ledger-list-modal.component.scss']
})
export class LedgerListModalComponent implements OnInit {
  ledgers: Array<Ledger> = [];
  permissions = ['shop/ledger:create'];

  private modal: any;
  constructor(
    private modalAction: ModalAction,
    private modalController: ModalController,
    private receiptService: ReceiptService,
    private receiptsController: ReceiptsControllerService
  ) { }

  async ngOnInit() {
    this.receiptsController.registerLedgerList(this);
    this.ledgers = await this.receiptService.getLedgers();
  }

  closeModal() {
    this.modalAction.close(false);
  }

  showModal() {
    this.modalAction.show();
  }

  openCreateLedgerModal() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddLedgerModalComponent,
      showBackdrop: 'static',
      cssClass: 'modal-lg',
    });
    this.modal.show().then();
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
    this.modal.onDismiss().then(async () => {
      this.modal = null;
      this.ledgers = await this.receiptService.getLedgers();
    });
  }

  async reloadLedgers() {
    this.ledgers = await this.receiptService.getLedgers();
  }

}
