import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FilterOperator } from '@etop/models';
import { OrderService } from 'apps/shop/src/services/order.service';
import { Order, RefType } from '@etop/models';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';

@Component({
  selector: 'shop-search-refs-input',
  templateUrl: './search-refs-input.component.html',
  styleUrls: ['./search-refs-input.component.scss']
})
export class SearchRefsInputComponent implements OnInit, OnChanges {
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  @Input() receipt_display: string;
  @Input() ref_type: RefType | string;
  @Input() ref_display: string;

  @Input() trader_id: string;
  @Input() trader_display: string;
  @Input() trader_name: string;
  @Input() trader_type: string;
  @Input() allow_input = true;

  @Output() refSelected = new EventEmitter();

  ref_code: string;
  results: any[] = [];

  default_result = [{
    code: "0",
    value: "Không tìm thấy"
  }];

  init_results = [];

  click_inside = false;
  show_dropdown = false;

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.click_inside) {
      this.show_dropdown = false;
    }
    this.click_inside = false;
  }

  constructor(
    private orderService: OrderService,
    private purchaseOrderService: PurchaseOrderService,
    private changeDetector: ChangeDetectorRef
  ) { }

  get placeHolder() {
    const ref = this.ref_display && this.ref_display.toLowerCase() || '';
    if (!this.allow_input) {
      return `${this.ref_display} không thể tạo phiếu ${this.receipt_display}`;
    }
    if (!this.trader_display || !this.trader_name) {
      return `Tìm ${ref} (Nhập chính xác mã ${ref} - Enter để tìm kiếm)`;
    }
    const trader = this.trader_display.toLowerCase();
    const name = this.trader_name;
    return `Tìm ${ref} cho ${trader} ${name} (Nhập chính xác mã ${ref} - Enter để tìm kiếm)`;
  }

  get noInitResultsText() {
    const ref = this.ref_display && this.ref_display.toLowerCase() || '';
    if (!this.allow_input) {
      return `Không thể tạo phiếu ${this.receipt_display} cho ${ref}!`;
    }
    if (!this.trader_display) {
      return `Không có ${ref} hợp lệ`;
    }
    return `${this.trader_display} này không có ${ref} hợp lệ`;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.createInitResults();
  }

  async createInitResults() {
    if (!this.trader_id) {
      return;
    }
    if (!this.allow_input) {
      this.init_results = [];
      return;
    }

    switch (this.trader_type) {
      case 'customer':
        let _orders = await this.orderService.getOrders(0, 5, [
          {
            name: "customer.id",
            op: FilterOperator.eq,
            value: this.trader_id
          },
          {
            name: "etop_payment_status",
            op: FilterOperator.in,
            value: 'Z, S'
          },
          {
            name: "status",
            op: FilterOperator.in,
            value: "Z, S, NS, P"
          }
        ]);
        _orders = _orders.filter(
          o => Number(o.received_amount) < Number(o.total_amount) && !this.codGreaterThanTotalAmount(o)
        );
        this.init_results = _orders;
        break;
      case 'supplier':
        const res = await this.purchaseOrderService.getPurchaseOrders(0, 5, [
          {
            name: "supplier_id",
            op: FilterOperator.eq,
            value: this.trader_id
          },
          // TODO: add filter by payment_status when server support
          {
            name: "status",
            op: FilterOperator.in,
            value: "Z, S, P"
          }
        ]);
        const _purchase_orders = res.purchase_orders.filter(
          p_o => Number(p_o.paid_amount) < Number(p_o.total_amount)
        );
        this.init_results = _purchase_orders;
        break;
      case 'carrier':
        break;
      default:
        break;
    }
    debug.log('init results', this.init_results);
  }

  showInitResults() {
    this.click_inside = true;
    this.show_dropdown = true;
    this.changeDetector.detectChanges();
    if (!this.ref_code) {
      this.results = this.init_results;
      this.changeDetector.detectChanges();
    }
  }

  onChangeRefCode() {
    this.results = [];
  }

  selectRef(res) {
    this.show_dropdown = false;
    this.changeDetector.detectChanges();
    this.refSelected.next(res);
  }

  async filter() {
    try {
      switch (this.ref_type) {
        case 'order':
          await this.filterOrders();
          break;
        case 'purchase_order':
          await this.filterPurchaseOrders();
          break;
        case 'fulfillment':
          await this.filterFulfillments();
          break;
        default:
          break;
      }
    } catch (e) {

    }
  }

  async filterOrders() {
    try {
      const filters = [
        {
          name: "customer.id",
          op: FilterOperator.eq,
          value: this.trader_id
        },
        {
          name: "etop_payment_status",
          op: FilterOperator.in,
          value: 'Z, S'
        },
        {
          name: "status",
          op: FilterOperator.in,
          value: "Z, S, NS, P"
        },
        {
          name: "code",
          op: FilterOperator.eq,
          value: this.ref_code
        }
      ];
      let orders = await this.orderService.getOrders(0, 1, filters);
      this.results = orders;
      if (!orders.length) {
        this.results = this.default_result;
      }
      if (this.codGreaterThanTotalAmount(orders[0])) {
        toastr.warning(
          `Tiền thu hộ của ${this.ref_display} #${this.ref_code} không nhỏ hơn số tiền khách phải trả, không thể tạo phiếu ${this.receipt_display}`
        );
        this.results = [];
      }
      if (orders[0].received_amount >= orders[0].total_amount) {
        toastr.warning(`${this.ref_display} #${this.ref_code} đã được thanh toán đủ.`);
        this.results = [];
      }
      this.show_dropdown = true;
    } catch (e) {
      this.results = this.default_result;
      debug.error('ERROR in filtering Orders', e);
    }
  }

  async filterPurchaseOrders() {
    try {
      const filters = [
        {
          name: "supplier_id",
          op: FilterOperator.eq,
          value: this.trader_id
        },
        // TODO: add filter by payment_status when server support
        {
          name: "status",
          op: FilterOperator.in,
          value: "Z, S, P"
        },
        {
          name: "code",
          op: FilterOperator.eq,
          value: this.ref_code
        }
      ];
      const res = await this.purchaseOrderService.getPurchaseOrders(0, 1, filters);
      if (!res.purchase_orders.length) {
        this.results = this.default_result;
      }

      if (res.purchase_orders[0].paid_amount >= res.purchase_orders[0].total_amount) {
        toastr.warning(`${this.ref_display} #${this.ref_code} đã được thanh toán đủ.`);
        this.results = [];
      } else {
        this.results = res.purchase_orders;
      }
      this.show_dropdown = true;
    } catch (e) {
      this.results = this.default_result;
      debug.error('ERROR in filtering Purchase Orders', e);
    }
  }

  codGreaterThanTotalAmount(order: Order) {
    return order.activeFulfillment && order.activeFulfillment.cod_amount >= order.total_amount;
  }

  async filterFulfillments() {

  }


}
