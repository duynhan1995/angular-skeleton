import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MultipleReceiptEditFormComponent } from './multiple-receipt-edit-form.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  exports: [MultipleReceiptEditFormComponent],
  entryComponents: [],
  declarations: [MultipleReceiptEditFormComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MultipleReceiptEditFormModule {}
