import { Component, OnInit, Input } from '@angular/core';
import { Receipt, ReceiptTraderInfo } from 'libs/models/Receipt';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';

@Component({
  selector: '[shop-receipt-row]',
  templateUrl: './receipt-row.component.html',
  styleUrls: ['./receipt-row.component.scss']
})
export class ReceiptRowComponent implements OnInit {
  @Input() receipt = new Receipt({});
  @Input() liteMode = false;

  constructor(
    private traderStore: TraderStoreService
  ) {}

  get isTopship() {
    const { created_type, trader } = this.receipt;
    if (!trader) { return false; }
    return created_type == 'auto'
      && trader.type == 'carrier' && trader.full_name.toLowerCase() == 'topship';
  }

  ngOnInit() {
  }

  viewDetailTrader(trader: ReceiptTraderInfo) {
    if (trader.deleted) { return; }
    this.traderStore.navigate(trader.type, null, trader.id);
  }

}
