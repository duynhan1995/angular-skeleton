import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Receipt, ReceiptLine } from 'libs/models/Receipt';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import {
  AutocompleteOpt,
  ReceiptsControllerService
} from 'apps/shop/src/app/pages/receipts/receipts-controller.service';
import { OrderService } from 'apps/shop/src/services/order.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { MaterialInputAutocompleteComponent } from 'libs/shared/components/etop-material/material-input-autocomplete/material-input-autocomplete.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AddLedgerModalComponent } from 'apps/shop/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ReceiptApi } from '@etop/api';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-create-receipt-form',
  templateUrl: './create-receipt-form.component.html',
  styleUrls: ['./create-receipt-form.component.scss']
})
export class CreateReceiptFormComponent extends BaseComponent implements OnInit, OnChanges {
  canCreateReceipt = true;

  constructor(
    private receiptsController: ReceiptsControllerService,
    private orderService: OrderService,
    private receiptService: ReceiptService,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private receiptApi: ReceiptApi,
    private changeDetector: ChangeDetectorRef,
    private authStore: AuthenticateStore
  ) {
    super();
  }

  get refDisplay() {
    const ref_type = this.new_receipt.p_data.ref_type;
    if (ref_type == 'order') {
      return 'Đơn hàng';
    }
    if (ref_type == 'fulfillment') {
      return 'Đơn giao hàng';
    }
    if (ref_type == 'purchase_order') {
      return 'Đơn nhập hàng';
    }
  }

  get traderDisplay() {
    const { trader_type } = this;
    if (trader_type == 'customer') {
      return 'Khách hàng';
    }
    if (trader_type == 'carrier') {
      return 'Nhà vận chuyển';
    }
    if (trader_type == 'supplier') {
      return 'Nhà cung cấp';
    }
  }

  get showReceiptLines() {
    return this.new_receipt.p_data.type
      && this.new_receipt.p_data.trader_id
      && this.new_receipt.p_data.amount > 0
  }

  get allowSearchLines() {
    return (this.trader_type == 'customer' && this.new_receipt.p_data.type == 'receipt') ||
      (this.trader_type == 'supplier' && this.new_receipt.p_data.type == 'payment');
  }

  @ViewChild('moneyInput', { static: false }) moneyInput: MaterialInputComponent;
  @ViewChild('ledgerInput', { static: false }) ledgerInput: MaterialInputAutocompleteComponent;
  @Output() createReceipt = new EventEmitter();
  new_receipt = new Receipt({});
  receipt_id: string;

  receiptTypes = [
    { name: 'Thu', value: 'receipt' },
    { name: 'Chi', value: 'payment' }
  ];

  traderTypes = [
    {
      name: 'Khách hàng',
      value: 'customer',
      disabled: false
    },
    {
      name: 'Nhà cung cấp',
      value: 'supplier',
      disabled: false
    },
    {
      name: 'Nhà vận chuyển (Đang phát triển)',
      value: 'carrier',
      disabled: true
    },
  ];

  ledgerTypes = [
    { name: 'Tiền mặt', value: 'cash' },
    { name: 'Chuyển khoản', value: 'bank' },
  ];

  ledgers: any[] = [];
  traders: Array<AutocompleteOpt> = [];
  receipt_lines = [];

  receipt_type_display = '';
  ledger_type = '';
  trader_type = '';

  format_money = false;
  receipt_amount_error = false;
  trader_name = '';

  loading = false;
  createAndConfirm = false;
  roles = [];

  ledgerIndex: number;

  private modal: any;

  ngOnInit() {
    this.roles = this.authStore.snapshot.permission.roles;
    if (this.roles.includes('purchasing_management')) {
      this.canCreateReceipt = false;
      this.traderTypes = this.traderTypes.filter(type => {
        return type.value != 'customer';
      })
    }
    if (this.roles.includes('salesman')) {
      this.canCreateReceipt = false;
      this.traderTypes = this.traderTypes.filter(type => {
        return type.value != 'supplier';
      })
    }

    this.receiptsController.onSliderClosed$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.reset();
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.reset();
  }

  private reset() {
    this.receipt_id = null;
    this.ledgers = [];
    this.traders = [];
    this.receipt_lines = [];

    this.receipt_type_display = '';
    this.trader_type = '';
    this.ledger_type = '';
    this.format_money = false;
    this.receipt_amount_error = false;
    this.new_receipt = new Receipt({});
    this.format_money = false;
  }

  private _preProcessingReceiptLines() {
    if (!this.checkValidAmount()) { return false; }

    const sumAmount = this.receipt_lines.reduce((a, b) => a + Number(b.amount), 0);
    this.new_receipt.p_data.lines = [];
    if (sumAmount < this.new_receipt.p_data.amount) {
      const additionalLine = {
        ...new ReceiptLine(),
        title: `${this.new_receipt.p_data.type == 'receipt' && 'Cộng' || 'Trừ'} vào tài khoản ${this.traderDisplay.toLowerCase()}`,
        amount: this.new_receipt.p_data.amount - sumAmount
      };
      this.new_receipt.p_data.lines = [additionalLine];
    }
    this.receipt_lines.forEach(l => {
      const _line = {
        title: `Thanh toán ${this.refDisplay.toLowerCase()}`,
        amount: l.amount,
        ref_id: l.ref_id
      };
      this.new_receipt.p_data.lines.push(_line);
    });
    return true;
  }

  private _validateReceiptInfo() {
    if (!this.new_receipt.p_data.type) {
      toastr.error('Vui lòng chọn loại phiếu');
      return false;
    }
    if (!this.new_receipt.p_data.paid_at) {
      toastr.error('Vui lòng nhập ngày thanh toán');
      return false;
    }
    if (!this.new_receipt.p_data.trader_id) {
      toastr.error('Vui lòng chọn đối tác');
      return false;
    }
    if (!this.new_receipt.p_data.amount) {
      toastr.error('Vui lòng nhập giá trị cho phiếu');
      return false;
    }
    if (!this.new_receipt.p_data.ledger_id) {
      toastr.error('Vui lòng chọn phương thức thanh toán');
      return false;
    }
    if (!this.new_receipt.p_data.title) {
      toastr.error('Vui lòng nhập nội dung cho phiếu');
      return false;
    }
    return true;
  }

  checkRefType(receipt_type, trader_type) {
    if (receipt_type == 'payment' && trader_type == 'customer') {
      return
    }
  }

  async create(createAndConfirm: boolean) {
    this.loading = true;
    this.createAndConfirm = createAndConfirm;
    try {
      if (!this._preProcessingReceiptLines()) {
        this.loading = false;
        this.createAndConfirm = false;
        return;
      }
      if (!this._validateReceiptInfo()) {
        this.loading = false;
        this.createAndConfirm = false;
        return;
      }
      const body: Receipt = {
        ...this.new_receipt.p_data,
      };
      debug.log('body ìnof', body);

      if (
        (body.type == 'payment' && this.trader_type == 'customer') ||
        (body.type == 'receipt' && this.trader_type == 'supplier') ||
        (body.lines.length === 1 && !body.lines[0].ref_id)
      ) {
        body.ref_type = "";
      }

      if (createAndConfirm) {
        this.confirm(body);
      } else {
        await this.receiptService.createReceipt(body);
        toastr.success(`Tạo phiếu ${this.receipt_type_display} thành công.`);
        this.createReceipt.emit();
      }
    } catch (e) {
      debug.error('ERROR in creating Receipt', e);
      toastr.error(`Tạo phiếu ${this.receipt_type_display} không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.createAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu ${this.receipt_type_display}`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu ${this.receipt_type_display}?</div>
        <div>
          Sau khi xác nhận bạn chỉ có thể thay đổi thông tin về
          <strong>tài khoản thanh toán</strong>, <strong>nội dung</strong> và <strong>ghi chú</strong>.
        </div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          let res: any;
          if (!this.receipt_id) {
            res = await this.receiptService.createReceipt(body);
            toastr.success(`Tạo phiếu ${this.receipt_type_display} thành công.`);
            this.receipt_id = res.id;
          } else {
            res = await this.receiptService.updateReceipt({ ...body, id: this.receipt_id });
          }

          await this.receiptApi.confirmReceipt(res.id);
          toastr.success(`Xác nhận phiếu ${this.receipt_type_display} thành công.`);
          this.createReceipt.emit();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Xác nhận phiếu ${this.receipt_type_display} không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  displayTraderList() {
    this.new_receipt.p_data.trader_id = null;
    this.receipt_lines = [];
    switch (this.trader_type) {
      case 'customer':
        this.new_receipt.p_data.ref_type = 'order';
        const customers = this.receiptsController.customers;
        this.traders = ReceiptsControllerService._traderMapping(customers);
        break;
      case 'supplier':
        this.new_receipt.p_data.ref_type = 'purchase_order';
        const suppliers = this.receiptsController.suppliers;
        this.traders = ReceiptsControllerService._traderMapping(suppliers);
        break;
      case 'carrier':
        this.new_receipt.p_data.ref_type = 'fulfillment';
        const carriers = this.receiptsController.carriers;
        this.traders = ReceiptsControllerService._traderMapping(carriers);
        break;
      default:
        break;
    }
  }

  displayLedgers() {
    if (this.ledger_type == 'cash') {
      const _ledger = this.receiptsController.ledgers.find(l => l.type == 'cash');
      this.new_receipt.p_data.ledger_id = _ledger && _ledger.id || null;
    } else {
      const ledgersBank = this.receiptsController.ledgers.filter(l => l.type != 'cash');
      this.ledgers = this.receiptsController.ledgerMapping(ledgersBank);
      this.ledgerIndex = 0;
      const ledger = this.ledgers[this.ledgerIndex];
      this.new_receipt.p_data.ledger_id = ledger && ledger.id || null;
    }
  }

  nameDisplayMap() {
    return option => option && option.display_name || null;
  }

  valueDisplayMap() {
    return option => option && option.id || null;
  }

  formatMoney() {
    if (!this.new_receipt.p_data.amount) {
      this.format_money = false;
      return;
    }
    this.format_money = !this.format_money;
    this.changeDetector.detectChanges();
    if (!this.format_money && this.moneyInput) {
      this.moneyInput.focusInput();
    }
  }

  onSelectLedger() {
    const ledger = this.ledgers[this.ledgerIndex];
    this.new_receipt.p_data.ledger_id = ledger && ledger.id || null;
  }

  focusLedger() {
    if (this.ledgers.length) {
      return;
    }
    this.addLedger();
  }

  checkValidAmount() {
    this.receipt_amount_error = false;
    const sumAmount = this.receipt_lines.reduce((a, b) => a + Number(b.amount), 0);
    if (sumAmount > this.new_receipt.p_data.amount) {
      this.receipt_amount_error = true;
      toastr.error(
        `Giá trị của phiếu ${this.receipt_type_display} phải lớn hơn Tổng số tiền ${this.receipt_type_display} ở phiếu này!`
      );
      return false;
    }
    for (let l of this.receipt_lines) {
      const _amount = Number(l.amount);
      if (this.new_receipt.p_data.type == 'receipt' && _amount > l.total_amount - l.received_amount) {
        l.error = true;
        toastr.error(
          `Số tiền ${this.receipt_type_display} không được lớn hơn Số tiền phải thanh toán - Số tiền đã thanh toán.`
        );
        return false;
      }
    }
    return true;
  }

  onChangeReceiptType() {
    this.receipt_lines = [];
    this.receipt_type_display = ReceiptApi.receiptTypeMap(this.new_receipt.p_data.type);
  }

  onTraderSelected() {
    this.receipt_lines = [];
  }

  addLedger() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddLedgerModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show().then();
    this.modal.onDismiss().then(ledger => {
      this.modal = null;
      if (ledger) {
        this.receiptsController.ledgers.push(ledger);

        this.new_receipt.p_data.ledger_id = ledger.id;

        const _ledgers = this.receiptsController.ledgers.filter(l => l.type != 'cash');
        this.ledgers = this.receiptsController.ledgerMapping(_ledgers);
        this.ledgerIndex = this.ledgers.findIndex(l => l.id == ledger.id);
      }
    });
  }

}
