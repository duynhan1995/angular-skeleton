import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ReceiptLine } from '@etop/models';

@Component({
  selector: 'shop-line-amount-input',
  templateUrl: './line-amount-input.component.html',
  styleUrls: ['./line-amount-input.component.scss']
})
export class LineAmountInputComponent implements OnInit {
  @ViewChild('input', {static: false}) input: ElementRef;
  @Input() receipt_type: string;
  @Input() receipt_type_display: string;
  @Input() ref_display: string;
  @Input() line = new ReceiptLine();
  @Input() updatable = true;
  @Output() changeLineAmount = new EventEmitter();

  format_money = true;

  constructor() { }

  ngOnInit() {
  }

  validateLineAmount(line) {
    line.error = false;
    const _amount = Number(line.amount);
    if (this.receipt_type == 'receipt' && _amount > line.total_amount - line.received_amount) {
      line.error = true;
      toastr.error(
        `Số tiền thu ở ${this.ref_display.toLowerCase()} #${line.title} không được lớn hơn Số tiền phải thu - Số tiền đã thu`
      );
      return false;
    }
  }

  numberOnly(event) {
    const num = Number(event.key);
    if (Number.isNaN(num)) {
      event.preventDefault();
      return false;
    }
  }

  focusInput() {
    setTimeout(_ => {
      this.input.nativeElement.focus();
    }, 0);
  }

}
