import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Receipt, ReceiptLine } from '@etop/models';
import { ReceiptApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: 'shop-receipt-lines',
  templateUrl: './receipt-lines.component.html',
  styleUrls: ['./receipt-lines.component.scss']
})
export class ReceiptLinesComponent implements OnInit, OnChanges {
  @Input() receipt_lines: ReceiptLine[] = [];
  @Input() trader_type = '';
  @Input() receipt_type = '';
  @Input() ref_type = '';
  @Input() receipt = new Receipt({});
  @Input() trader_name = '';
  @Input() loading = false;
  @Input() allow_input = true;

  receipt_type_display = '';

  constructor(
    private auth: AuthenticateStore,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore
  ) { }

  get tabName() {
    const { ref_type } = this;
    if (ref_type == 'order') {
      return 'ĐƠN BÁN HÀNG THAM CHIẾU';
    }
    if (ref_type == 'purchase_order') {
      return 'ĐƠN NHẬP HÀNG THAM CHIẾU';
    }
    if (ref_type == 'fulfillment') {
      return 'ĐƠN GIAO HÀNG THAM CHIẾU';
    }
  }

  get refDisplay() {
    const { ref_type } = this;
    if (ref_type == 'order') {
      return 'Đơn bán hàng';
    }
    if (ref_type == 'purchase_order') {
      return 'Đơn nhập hàng';
    }
    if (ref_type == 'fulfillment') {
      return 'Đơn giao hàng';
    }
  }

  get traderDisplay() {
    const { trader_type } = this;
    if (trader_type == 'customer') {
      return 'Khách hàng';
    }
    if (trader_type == 'supplier') {
      return 'Nhà cung cấp';
    }
    if (trader_type == 'carrier') {
      return 'Nhà vận chuyển';
    }
  }

  get refType() {
    return this.ref_type;
  }

  get traderID() {
    return this.receipt.p_data.trader_id;
  }

  get sumAmounts() {
    return this.receipt_lines.reduce((a, b) => a + Number(b.amount), 0);
  }

  get remainAmount() {
    return this.receipt.p_data.amount - this.sumAmounts;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.receipt_type_display = ReceiptApi.receiptTypeMap(this.receipt_type) || '';
  }

  removeLine(index) {
    this.receipt_lines.splice(index, 1);
  }

  onSelectRef(ref) {
    const line = this.receipt_lines.find(l => l.ref_id == ref.id);
    if (line) {
      toastr.warning(`${this.refDisplay} #${ref.code} đã được thêm vào phiếu.`);
      return;
    }
    const new_line = {
      total_amount: ref.total_amount,
      received_amount: ref.received_amount,
      paid_amount: ref.paid_amount,
      amount:
        (this.ref_type == 'order' && (ref.total_amount - ref.received_amount)) ||
        (this.ref_type == 'purchase_order' && (ref.total_amount - ref.paid_amount)),
      ref_id: ref.id,
      title: ref.code,
      created_at: ref.created_at
    };
    this.receipt_lines.push(new_line);
  }

  viewDetailLine(line) {
    if (!line.ref_id || !line.code || !this.receipt.ref_type) { return; }
    if (this.receipt.ref_type == 'fulfillment') {
      return this.ffmStore.navigate('shipment', line.code);
    }
    const refTypeMap = {
      order: 'so',
      purchase_order: 'po'
    };
    this.orderStore.navigate(refTypeMap[this.receipt.ref_type], line.code);
  }

  canExternalLink(line) {
    return line.ref_id && line.code && this.receipt.ref_type
  }

}
