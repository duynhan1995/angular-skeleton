import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { ReceiptsControllerService } from '../receipts-controller.service';

@Component({
  selector: 'shop-ledgers',
  templateUrl: './ledgers.component.html',
  styleUrls: ['./ledgers.component.scss']
})
export class LedgersComponent extends PageBaseComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private receiptsController: ReceiptsControllerService,
  ) {
    super();
  }

  async ngOnInit() {
    
  }

  ngOnDestroy() {

  }

}
