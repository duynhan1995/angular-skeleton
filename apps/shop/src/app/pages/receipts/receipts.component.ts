import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { ReceiptsControllerService } from 'apps/shop/src/app/pages/receipts/receipts-controller.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { SubscriptionService } from '@etop/features';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-receipts',
  template: `
    <shop-subscription-warning></shop-subscription-warning>
    <ng-container *ngIf="subscription;else notSubscription">
    <router-outlet *ePermissions="['shop/receipt:view']"></router-outlet>
      <etop-not-permission *ngIf="!allowViewReceipts"></etop-not-permission>
    </ng-container>
    <ng-template #notSubscription>
      <etop-not-subscription></etop-not-subscription>
    </ng-template>
  `
})
export class ReceiptsComponent extends BaseComponent implements OnInit, OnDestroy {
  allowViewReceipts = true;
  subscription = false;
  constructor(
    private headerController: HeaderControllerService,
    private receiptsController: ReceiptsControllerService,
    private authStore: AuthenticateStore,
    private subscriptionService: SubscriptionService,
    private util: UtilService,
    private router: Router,
  ) {
    super();
  }

  async ngOnInit() {
    this.allowViewReceipts = this.authStore.snapshot.permission.permissions.includes('shop/receipt:view');
    this.subscription = await this.subscriptionService.checkSubcriptions();
    if(this.subscription){
      this.setupTab()
    }
    this.receiptsController.onChangeLedger$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async () => {
        this.setupTab()
      });
  }

  setupTab() {
    this.headerController.setTabs([
      {
        title: 'Tất cả',
        active: this.receiptsController.ledger_type == 'all',
        onClick: () => {
         this.router.navigateByUrl(`s/${this.util.getSlug()}/receipts/all`);
        }
      },
      {
        title: 'Tiền mặt',
        active: this.receiptsController.ledger_type == 'cash',
        onClick: () => {
          this.router.navigateByUrl(`s/${this.util.getSlug()}/receipts/cash`);
        }
      },
      {
        title: 'Chuyển khoản',
        active: this.receiptsController.ledger_type == 'bank',
        onClick: () => {
          this.router.navigateByUrl(`s/${this.util.getSlug()}/receipts/bank`);
        }
      }
    ]);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

}
