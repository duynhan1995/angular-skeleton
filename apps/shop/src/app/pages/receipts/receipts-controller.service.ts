import { Injectable } from '@angular/core';
import { ReceiptListComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/receipt-list/receipt-list.component';
import { Ledger, Receipt } from 'libs/models/Receipt';
import { Customer } from 'libs/models/Customer';
import { Carrier } from 'libs/models/Carrier';
import { Supplier } from 'libs/models/Supplier';
import { UtilService } from 'apps/core/src/services/util.service';
import { Subject } from 'rxjs';
import { LedgerListModalComponent } from 'apps/shop/src/app/pages/receipts/ledgers/components/ledger-list-modal/ledger-list-modal.component';

export class AutocompleteOpt {
  display_name: string;
  name: string;
  id: string;
}

@Injectable({
  providedIn: "root"
})
export class ReceiptsControllerService {

  constructor(
    private util: UtilService
  ) { }

  private receiptListComponent: ReceiptListComponent;
  private ledgerListComponent: LedgerListModalComponent;

  ledger_type: 'all' | 'cash' | 'bank' = 'all';
  onChangeLedger$ = new Subject();
  onSliderClosed$ = new Subject();

  ledgers: Ledger[] = [];
  customers: Customer[] = [];
  carriers: Carrier[] = [];
  suppliers: Supplier[] = [];

  static _traderMapping(partners: Array<Customer | Carrier | Supplier>): Array<AutocompleteOpt> {
    return partners.map(p => {
      return { id: p.id, display_name: `${p.full_name}${p.phone && ' - ' + p.phone || ''}`, name: p.full_name }
    });
  }

  ledgerMapping(ledgers: Array<Ledger>): Array<any> {
    return ledgers.map((l, index) => {
      const bank = l.bank_account;
      return {
        index,
        id: l.id,
        info: `<strong>${l.name}</strong>
                <span>
                ${bank.account_number} - ${bank.account_name} - ${bank.name}
                </span>`,
        detail: `<strong>${l.name}</strong>
                <span>
                ${bank.account_number} - ${bank.account_name} - ${bank.name}
                </span>`,
        search_text: this.util.makeSearchText(
          `${l.name}${bank.account_number}${bank.account_name}${bank.name}`
        )
      };
    });
  }

  registerReceiptList(instance: ReceiptListComponent) {
    this.receiptListComponent = instance;
  }

  registerLedgerList(instance: LedgerListModalComponent) {
    this.ledgerListComponent = instance;
  }

  async reloadReceiptList() {
    await this.receiptListComponent.reloadReceipts();
  }

  async reloadLedgerList() {
    await this.ledgerListComponent.reloadLedgers()
  }

  setupDataReceipt(receipt: Receipt): Receipt {
    receipt.p_data = {
      ...receipt.p_data,
      paid_at: receipt.paid_at,
      ledger_id: receipt.ledger_id,
      trader_id: receipt.trader_id,
      amount: receipt.amount,
      title: receipt.title,
      description: receipt.description,
      user: receipt.user,
      trader: receipt.trader,
      note: receipt.note
    };
    return receipt;
  }

}
