import { Component, OnInit } from '@angular/core';
import {TopShipController} from "apps/shop/src/app/pages/top-ship/top-ship.controller";
import {PublicGetShipmentServicesRequest} from "apps/shop/src/app/pages/top-ship/top-ship.interface";
import {FulfillmentApi} from "@etop/api";
import {ShipmentCarrier} from "libs/models/Fulfillment";
import {FulfillmentService} from "apps/shop/src/services/fulfillment.service";

@Component({
  selector: 'shop-public-shipment-service-options',
  templateUrl: './public-shipment-service-options.component.html',
  styleUrls: ['./public-shipment-service-options.component.scss']
})
export class PublicShipmentServiceOptionsComponent implements OnInit {

  carriers: ShipmentCarrier[] = [];

  publicShipmentServicesRequest: PublicGetShipmentServicesRequest;
  getServicesIndex = 0;
  shipServicesErrorMsg = '';

  loading = false;

  constructor(
    private topShipController: TopShipController,
    private ffmApi: FulfillmentApi,
    private ffmService: FulfillmentService,
  ) { }

  get enoughInfo() {
    if (!this.publicShipmentServicesRequest) {
      return false;
    }
    const {from_ward_code, to_ward_code, total_cod_amount, basket_value, chargeable_weight} = this.publicShipmentServicesRequest;
    if (!from_ward_code || !to_ward_code) { return false; }
    if (!total_cod_amount && total_cod_amount != 0) { return false; }
    if (!basket_value && basket_value != 0) { return false; }
    if (!chargeable_weight && chargeable_weight != 0) { return false; }

    return true;
  }

  ngOnInit() {
    this.topShipController.getPublicShipmentServices$.subscribe(request => {
      this.publicShipmentServicesRequest = request;
      this.getServices().then();
    });
  }

  reset() {
    this.carriers = [
      {
        name: 'j&t',
        id: null,
        logo: 'assets/images/provider_logos/jt-s.png',
        services: []
      },
      {
        name: 'giao hàng nhanh',
        id: null,
        logo: 'assets/images/provider_logos/ghn-s.png',
        services: []
      },
      {
        name: 'viettel post',
        id: null,
        logo: 'assets/images/provider_logos/vtpost-s.png',
        services: []
      },
      {
        name: 'giao hàng tiết kiệm',
        id: null,
        logo: 'assets/images/provider_logos/ghtk-s.png',
        services: []
      }
    ]
  }

  async getServices() {
    if (!this.enoughInfo) {
      return;
    }
    this.reset();
    this.loading = true;
    try {
      this.getServicesIndex++;
      const request = {
        ...this.publicShipmentServicesRequest,
        index: this.getServicesIndex
      };
      const res = await this.ffmApi.getPublicExternalShippingServices(request);
      if (res.index == this.getServicesIndex) {
        const _carriers = this.ffmService.groupShipmentServicesByConnection(res.services);

        _carriers.forEach(c => {
          switch (c.name.toLowerCase()) {
            case 'topship - j&t':
              this.carriers[0].services = c.services;
              break;
            case 'topship - ghn':
              this.carriers[1].services = c.services;
              break;
            case 'topship - vtp':
              this.carriers[2].services = c.services;
              break;
            case 'topship - ghtk':
              this.carriers[3].services = c.services;
              break;
          }
        });

        this.loading = false;
      }
      debug.log('SERVICES', this.carriers);
    } catch(e) {
      debug.error(e);
      this.loading = false;
      this.shipServicesErrorMsg = e.message || e.msg;
    }
  }

}
