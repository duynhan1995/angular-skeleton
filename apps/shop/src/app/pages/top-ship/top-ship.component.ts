import { Component, OnInit } from '@angular/core';
import List from 'identical-list';
import {District, Province, Ward} from "libs/models/Location";
import {TopShipController} from "apps/shop/src/app/pages/top-ship/top-ship.controller";
import {PublicGetShipmentServicesRequest} from "apps/shop/src/app/pages/top-ship/top-ship.interface";
import {LocationQuery, LocationService} from "@etop/state/location";
import {FormBuilder} from "@angular/forms";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {BaseComponent} from "@etop/core";

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'shop-top-ship',
  templateUrl: './top-ship.component.html',
  styleUrls: ['./top-ship.component.scss']
})
export class TopShipComponent extends BaseComponent implements OnInit {
  locationReady$ = this.locationQuery.select(state => !!state.locationReady);

  shipmentServiceForm = this.fb.group({
    from_province_code: '',
    from_district_code: '',
    from_ward_code: '',
    to_province_code: '',
    to_district_code: '',
    to_ward_code: '',
    total_cod_amount: null,
    basket_value: null,
    chargeable_weight: 500,
    include_insurance: false
  });

  dimension: {
    length: number;
    width: number;
    height: number;
  };

  productWeights = new List<FilterOption>([
    { id: '100', value: 100 },
    { id: '200', value: 200 },
    { id: '300', value: 300 },
    { id: '500', value: 500 },
    { id: '1000', value: 1000 },
    { id: '1500', value: 1500 },
    { id: '2000', value: 2000 },
    { id: '2500', value: 2500 },
    { id: '3000', value: 3000 },
    { id: '4000', value: 4000 },
    { id: '5000', value: 5000 }
  ]);

  provincesList$ = this.locationQuery.select("provincesList");
  fromDistrictsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.shipmentServiceForm.controls['from_province_code'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  fromWardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.shipmentServiceForm.controls['from_district_code'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  toDistrictsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.shipmentServiceForm.controls['to_province_code'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  toWardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.shipmentServiceForm.controls['to_district_code'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private topShipController: TopShipController
  ) {
    super();
  }

  ngOnInit() {
    this.dimension = { length: 5, width: 5, height: 5 };

    this.shipmentServiceForm.controls['from_province_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.shipmentServiceForm.patchValue({
          from_district_code: '',
          from_ward_code: ''
        });
      });

    this.shipmentServiceForm.controls['from_district_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.shipmentServiceForm.patchValue({
          from_ward_code: ''
        });
      });

    this.shipmentServiceForm.controls['to_province_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.shipmentServiceForm.patchValue({
          to_district_code: '',
          to_ward_code: ''
        });
      });

    this.shipmentServiceForm.controls['to_district_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.shipmentServiceForm.patchValue({
          to_ward_code: ''
        });
      });

    combineLatest([
      this.shipmentServiceForm.controls['from_ward_code'].valueChanges,
      this.shipmentServiceForm.controls['to_ward_code'].valueChanges,
      this.shipmentServiceForm.controls['include_insurance'].valueChanges
    ]).pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getPublicShipmentServices();
      });
  }

  getPublicShipmentServices() {
    const request = JSON.parse(JSON.stringify(this.shipmentServiceForm.getRawValue()));
    this.topShipController.getPublicShipmentServices$.next(request);
  }

}
