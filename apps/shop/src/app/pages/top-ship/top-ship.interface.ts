export interface PublicGetShipmentServicesRequest {
  chargeable_weight: number;
  basket_value: number;
  include_insurance: boolean;
  total_cod_amount: number;

  from_province_code: string;
  from_district_code: string;
  from_ward_code: string;

  to_province_code: string;
  to_district_code: string;
  to_ward_code: string;

  width?: number;
  length?: number;
  height?: number;
  index?: number;
}
