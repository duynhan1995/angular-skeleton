import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {PublicGetShipmentServicesRequest} from "apps/shop/src/app/pages/top-ship/top-ship.interface";

@Injectable({
  providedIn: 'root'
})
export class TopShipController {

  getPublicShipmentServices$ = new Subject<PublicGetShipmentServicesRequest>();

  constructor() { }
}
