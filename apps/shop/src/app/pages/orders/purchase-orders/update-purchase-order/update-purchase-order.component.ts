import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { Variant } from 'libs/models/Product';
import { PurchaseOrder } from 'libs/models/PurchaseOrder';
import { PurchaseOrderApi } from '@etop/api';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { PurchaseOrderProductsComponent } from 'apps/shop/src/app/pages/orders/purchase-orders/components/purchase-order-products/purchase-order-products.component';

@Component({
  selector: 'shop-update-purchase-order',
  templateUrl: './update-purchase-order.component.html',
  styleUrls: ['./update-purchase-order.component.scss']
})
export class UpdatePurchaseOrderComponent extends BaseComponent implements OnInit {
  @ViewChild('purchaseOrderProducts', {static: true}) purchaseOrderProducts: PurchaseOrderProductsComponent;
  purchase_order = new PurchaseOrder({});
  search_variant_list: Array<Variant> = [];

  constructor(
    private util: UtilService,
    private router: Router,
    private route: ActivatedRoute,
    private orderStore: OrderStoreService,
    private headerController: HeaderControllerService,
    private purchaseOrderStore: PurchaseOrderStoreService,
    private purchaseOrderApi: PurchaseOrderApi,
    private authStore: AuthenticateStore
  ) {
    super();
  }

  get allowUpdate() {
    return this.authStore.snapshot.permission.permissions.includes('shop/purchase_order:update');
  }

  get isNew() {
    return ['N', 'P'].indexOf(this.purchase_order.status) == -1;
  }

  ngOnInit() {
    this.orderStore.changeHeaderTab(null);
    this.headerController.clearActions();
    this.headerController.clearTabs();

    this.purchaseOrderStore.recentlyUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(recentlyUpdated => {
        if (recentlyUpdated) {
          this.search_variant_list = [];
          this.getPurchaseOrderInfo();
        }
      });

    this.getPurchaseOrderInfo();
  }

  async getPurchaseOrderInfo() {
    try {
      const { id } = this.route.snapshot.params;
      this.purchase_order = await this.purchaseOrderApi.getPurchaseOrder(id);
      this.purchase_order = this.purchaseOrderStore.setupDataPurchaseOrder(this.purchase_order);

      this.search_variant_list = this.purchase_order.lines.map(l => {
        return {
          ...new Variant({}),
          id: l.variant_id,
          image: l.image_url,
          code: l.code,
          name: l.product_name,
          attributes: l.attributes,
          p_data: {
            quantity: l.quantity,
            payment_price: l.payment_price
          }
        };
      });
    } catch(e) {
      debug.error('ERROR in getting Purchase Order Info', e);
      toastr.error('Có lỗi xảy ra khi lấy thông tin đơn nhập hàng');
    }
  }

  onVariantSearched(variant) {
    const index = this.search_variant_list.findIndex(item => item.id == variant.id);
    if (index == -1) {
      this.search_variant_list.push(variant);
      const idx = this.search_variant_list.length - 1;
      this.search_variant_list[idx].p_data.payment_price = variant.cost_price;
      if (this.purchaseOrderProducts) {
        this.purchaseOrderProducts.lineUpdated();
      }
    }
  }

  gotoPurchaseOrders() {
    let slug = this.util.getSlug();
    this.router.navigateByUrl(`/s/${slug}/orders`);
    this.orderStore.changeHeaderTab('purchase_orders');
  }

}
