import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { Variant } from 'libs/models/Product';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { PurchaseOrderProductsComponent } from 'apps/shop/src/app/pages/orders/purchase-orders/components/purchase-order-products/purchase-order-products.component';

@Component({
  selector: 'shop-create-purchase-order',
  templateUrl: './create-purchase-order.component.html',
  styleUrls: ['./create-purchase-order.component.scss']
})
export class CreatePurchaseOrderComponent implements OnInit, OnDestroy {
  @ViewChild('purchaseOrderProducts', {static: true}) purchaseOrderProducts: PurchaseOrderProductsComponent;
  search_variant_list: Array<Variant> = [];

  constructor(
    private headerController: HeaderControllerService,
    private orderStore: OrderStoreService,
    private receiptStore: ReceiptStoreService,
    private purchaseOrderStore: PurchaseOrderStoreService,
    private util: UtilService,
    private router: Router,
    private authStore: AuthenticateStore
  ) { }

  get allowCreate() {
    return this.authStore.snapshot.permission.permissions.includes('shop/purchase_order:create');
  }

  ngOnInit() {
    this.receiptStore.resetData();
    this.orderStore.changeHeaderTab(null);
    this.headerController.clearActions();
    this.headerController.clearTabs();
    this.purchaseOrderStore.recentlyCreated$.subscribe(recentlyCreated => {
      if (recentlyCreated) {
        this.search_variant_list = [];
      }
    });
  }

  ngOnDestroy() {
  }

  onVariantSearched(variant) {
    const index = this.search_variant_list.findIndex(item => item.id == variant.id);
    if (index == -1) {
      this.search_variant_list.push(variant);
      const idx = this.search_variant_list.length - 1;
      this.search_variant_list[idx].p_data.payment_price = variant.cost_price;
      if (this.purchaseOrderProducts) {
        this.purchaseOrderProducts.lineUpdated();
      }
    }
  }

  gotoPurchaseOrders() {
    let slug = this.util.getSlug();
    this.router.navigateByUrl(`/s/${slug}/orders`);
    this.orderStore.changeHeaderTab('purchase_orders');
  }

}
