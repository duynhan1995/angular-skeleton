import { Component, Input, OnInit, OnChanges} from '@angular/core';
import { PurchaseOrder } from 'libs/models/PurchaseOrder';
import { PurchaseOrderApi, InventoryApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { InventoryVoucher } from 'libs/models/Inventory';


@Component({
  selector: 'shop-purchase-order-detail',
  templateUrl: './purchase-order-detail.component.html',
  styleUrls: ['./purchase-order-detail.component.scss']
})
export class PurchaseOrderDetailComponent implements OnInit, OnChanges {
  @Input() purchase_order = new PurchaseOrder({});
  inInventoryVoucher : InventoryVoucher;
  outInventoryVoucher : InventoryVoucher;
  private modal: any;

  constructor(
    private purchaseOrderApi: PurchaseOrderApi,
    private dialog: DialogControllerService,
    private purchaseOrderStore: PurchaseOrderStoreService,
    private util: UtilService,
    private traderStore: TraderStoreService,
    private inventoryStore: InventoryStoreService,
    private inventoryApi: InventoryApi

  ) { }

  get isNew() {
    return ['N', 'P'].indexOf(this.purchase_order.status) == -1;
  }

  ngOnInit(){}

  async ngOnChanges() {
    let inventoryVouchers = await this.inventoryApi.getInventoryVouchersByReference(this.purchase_order.id,'purchase_order');
    this.inInventoryVoucher = inventoryVouchers.find(inventoryVoucher =>  inventoryVoucher.type == "in")
    this.outInventoryVoucher = inventoryVouchers.find(inventoryVoucher =>  inventoryVoucher.type == "out")
  }
  async confirm() {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận đơn nhập hàng #${this.purchase_order.code}`,
      body: `
        <div>Bạn có chắc muốn xác nhận đơn nhập hàng #${this.purchase_order.code}?</div>
        <div>Sau khi xác nhận, 1 <strong>phiếu nhập kho</strong> sẽ được tự động sinh ra.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi phiếu nhập kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.purchaseOrderApi.confirmPurchaseOrder(this.purchase_order.id, "confirm");
          toastr.success(`Xác nhận đơn nhập hàng #${this.purchase_order.code} thành công.`);
          this.purchaseOrderStore.recentlyUpdated();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Purchase Order', e);
          toastr.error(`Xác nhận đơn nhập hàng #${this.purchase_order.code} không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  updatePurchaseOrder() {
    const slug = this.util.getSlug();
    window.open(`/s/${slug}/orders/purchase-orders/update/${this.purchase_order.id}`, '_blank');
  }

  viewDetailSupplier() {
    const supplier = this.purchase_order.supplier;
    if (!this.purchase_order.supplier_id) { return; }
    if (supplier && supplier.deleted) { return; }
    this.traderStore.navigate('supplier', null, this.purchase_order.supplier_id);
  }

  viewDetailVoucher(type: string, voucher_code: string) {
    this.inventoryStore.navigate(type, voucher_code);
  }

}
