import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PurchaseOrder, PurchaseOrderLine } from 'libs/models/PurchaseOrder';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { PurchaseOrderApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-purchase-order-info-update',
  templateUrl: './purchase-order-info-update.component.html',
  styleUrls: ['./purchase-order-info-update.component.scss']
})
export class PurchaseOrderInfoUpdateComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('purchaseOrderNote', {static: false}) purchaseOrderNote: HTMLInputElement;
  @Input() purchase_order = new PurchaseOrder({});

  discountPercent = false;
  discountDefault = true;
  discount_calc_type = 1;
  /*
   ** 1: default (đ)
   ** 2: percent (%)
   */

  loading = false;
  updateAndConfirm = false;

  private modal: any;

  constructor(
    private purchaseOrderStore: PurchaseOrderStoreService,
    private purchaseOrderService: PurchaseOrderService,
    private purchaseOrderApi: PurchaseOrderApi,
    private dialog: DialogControllerService
  ) {
    super();
  }

  get isNew() {
    return ['N', 'P'].indexOf(this.purchase_order.status) == -1;
  }

  get supplierDebt(): number {
    const result = this.purchase_order.p_data.total_amount - Number(this.purchase_order.paid_amount);
    return result > 0 && result || 0;
  }

  ngOnInit() {
    this.purchaseOrderStore.linesChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(lines => {
        this.purchase_order.p_data.lines = lines;
      });
    this.purchaseOrderStore.basketValueChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(basket_value => {
        this.purchase_order.p_data.basket_value = basket_value;
        this.updateTotalAmount();
      });
  }

  checkInvalidLines(lines: PurchaseOrderLine[]) {
    let invalid_index: number;
    const hasZeroQuantity = lines.some((l, index) => {
      if (!l.quantity) {
        invalid_index = index + 1;
        return true;
      }
    });
    return {invalid: hasZeroQuantity, invalid_index};
  }

  discountCalcTypeChange() {
    switch (Number(this.discount_calc_type)) {
      case 1:
        this.toggleDiscountOption('default');
        break;
      case 2:
        this.toggleDiscountOption('percent');
        break;
      default:
        this.toggleDiscountOption('default');
    }
  }

  toggleDiscountOption(type) {
    this.purchase_order.p_data.total_discount = 0;
    if (type == 'percent' && this.discountPercent) {
      return;
    }
    if (type == 'default' && this.discountDefault) {
      return;
    }
    this.discountDefault = !this.discountDefault;
    this.discountPercent = !this.discountPercent;
  }

  updateTotalAmount() {
    const { basket_value, total_discount } = this.purchase_order.p_data;
    let totalDiscount = total_discount;
    if (this.discountPercent) {
      totalDiscount = total_discount / 100 * basket_value;
    }
    this.purchase_order.p_data.total_amount = (basket_value || 0) - (totalDiscount || 0);
  }

  async update(updateAndConfirm: boolean) {
    this.loading = true;
    this.updateAndConfirm = updateAndConfirm;
    try {
      const body: PurchaseOrder = {
        id: this.purchase_order.id,
        ...this.purchase_order.p_data,
        total_discount: this.discountPercent
          && (this.purchase_order.p_data.total_discount / 100 * this.purchase_order.p_data.basket_value)
          || this.purchase_order.p_data.total_discount
      };

      if (!body.lines || !body.lines.length) {
        this.loading = false;
        this.updateAndConfirm = false;
        return toastr.error('Chưa chọn sản phẩm!');
      }
      const invalid_lines = this.checkInvalidLines(body.lines);
      if (invalid_lines.invalid) {
        this.loading = false;
        this.updateAndConfirm = false;
        return toastr.error(`Chưa nhập số lượng hoặc số lượng bằng 0 ở sản phẩm thứ ${invalid_lines.invalid_index}!`);
      }

      if (updateAndConfirm) {
        this.confirm(body);
      } else {
        await this.purchaseOrderApi.updatePurchaseOrder(body);
        toastr.success(`Cập nhật đơn nhập hàng thành công.`);
      }
    } catch (e) {
      debug.error('ERROR in creating Purchase Order', e);
      toastr.error(`Cập nhật đơn nhập hàng không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.updateAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận đơn nhập hàng`,
      body: `
        <div>Bạn có chắc muốn xác nhận đơn nhập hàng?</div>
        <div>Sau khi xác nhận, 1 <strong>phiếu nhập kho</strong> sẽ được tự động sinh ra.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi phiếu nhập kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const res = await this.purchaseOrderApi.updatePurchaseOrder(body);
          toastr.success(`Cập nhật đơn nhập hàng thành công.`);
          await this.purchaseOrderApi.confirmPurchaseOrder(res.id, "confirm");
          toastr.success(`Xác nhận đơn nhập hàng thành công.`);
          this.resetData();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Purchase Order', e);
          toastr.error(`Xác nhận đơn nhập hàng không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  resetData() {
    this.discountPercent = false;
    this.discountDefault = true;
    this.purchaseOrderStore.recentlyUpdated();
  }

}
