import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PurchaseOrder } from 'libs/models/PurchaseOrder';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { CreateReceiptModalComponent } from 'apps/shop/src/app/components/modals/create-receipt-modal/create-receipt-modal.component';
import { PurchaseOrderReceiptsComponent } from '../purchase-order-receipts/purchase-order-receipts.component';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';

@Component({
  selector: 'shop-purchase-order-payment-info',
  templateUrl: './purchase-order-payment-info.component.html',
  styleUrls: ['./purchase-order-payment-info.component.scss']
})
export class PurchaseOrderPaymentInfoComponent implements OnInit {
  @Input() purchase_order = new PurchaseOrder({});
  @ViewChild('purchaseOrderReceipts', { static: true }) purchaseOrderReceipts: PurchaseOrderReceiptsComponent;

  constructor(
    private modalController: ModalController,
    private purchaseOrderStore: PurchaseOrderStoreService) { }

  get isPaymentDone() {
    return Number(this.purchase_order.paid_amount) >= Number(this.purchase_order.total_amount);
  }

  get cannotCreateReceipt() {
    return this.isPaymentDone || this.purchase_order.status == 'N';
  }

  get disabledTooltip() {
    if (this.isPaymentDone) {
      return 'Đơn nhập hàng này đã thanh toán đủ, không thể tạo phiếu chi!';
    }
    if (this.purchase_order.status == 'N') {
      return 'Đơn nhập hàng đã huỷ, không thể tạo phiếu chi!';
    }
    return '';
  }

  ngOnInit() { }

  openCreateReceiptModal() {
    if (this.isPaymentDone) {
      toastr.error('Đơn hàng này đã được thanh toán đủ. Không thể tạo thêm phiếu thu.');
      return;
    }

    let modal = this.modalController.create({
      component: CreateReceiptModalComponent,
      cssClass: 'modal-lg',
      componentProps: {
        trader_id: this.purchase_order.supplier_id,
        type: 'payment',
        amount: Number(this.purchase_order.total_amount) - Number(this.purchase_order.paid_amount),
        ref: this.purchase_order
      }
    });

    modal.show().then();
    modal.onDismiss().then(async () => {
      this.purchaseOrderStore.recentlyUpdated();
      await this.purchaseOrderReceipts.getReceipts();
    });
  }

}
