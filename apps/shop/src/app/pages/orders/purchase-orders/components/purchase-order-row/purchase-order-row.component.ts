import { Component, Input, OnInit } from '@angular/core';
import { PurchaseOrder } from 'libs/models/PurchaseOrder';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';

@Component({
  selector: '[shop-purchase-order-row]',
  templateUrl: './purchase-order-row.component.html',
  styleUrls: ['./purchase-order-row.component.scss']
})
export class PurchaseOrderRowComponent implements OnInit {
  @Input() purchase_order = new PurchaseOrder({});
  @Input() liteMode = false;

  constructor(
    private traderStore: TraderStoreService,
    private inventoryStore: InventoryStoreService
  ) { }

  get isPaymentDone() {
    return this.purchase_order.paid_amount >= this.purchase_order.total_amount;
  }

  ngOnInit() {
  }

  viewDetailSupplier() {
    const supplier = this.purchase_order.supplier;
    if (!this.purchase_order.supplier_id) { return; }
    if (supplier && supplier.deleted) { return; }
    this.traderStore.navigate('supplier', null, this.purchase_order.supplier_id);
  }

  viewDetailVoucher() {
    const voucher = this.purchase_order.inventory_voucher;
    this.inventoryStore.navigate('in', voucher.code);
  }

}
