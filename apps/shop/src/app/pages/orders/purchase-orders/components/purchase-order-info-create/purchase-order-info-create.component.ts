import { Component, OnDestroy, OnInit } from '@angular/core';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { SupplierService } from 'apps/shop/src/services/supplier.service';
import { PurchaseOrder, PurchaseOrderLine } from 'libs/models/PurchaseOrder';
import { PurchaseOrderAPI, PurchaseOrderApi, ReceiptApi } from '@etop/api';
import { Receipt } from 'libs/models/Receipt';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { CreateSupplierModalComponent } from 'apps/shop/src/app/pages/orders/purchase-orders/components/create-supplier-modal/create-supplier-modal.component';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import CreatePurchaseOrderRequest = PurchaseOrderAPI.CreatePurchaseOrderRequest;

@Component({
  selector: 'shop-purchase-order-info-create',
  templateUrl: './purchase-order-info-create.component.html',
  styleUrls: ['./purchase-order-info-create.component.scss']
})
export class PurchaseOrderInfoCreateComponent extends BaseComponent implements OnInit, OnDestroy {
  purchase_order = new PurchaseOrder({});
  purchase_order_id: string;
  suppliers: any[] = [];
  supplierIndex: number;
  showCreateSupplier = true;

  discountPercent = false;
  discountDefault = true;
  discount_calc_type = 1;
  /*
   ** 1: default (đ)
   ** 2: percent (%)
   */

  loading = false;
  createAndConfirm = false;

  private receipts: any[] = [];
  private modal: any;

  constructor(
    private receiptStore: ReceiptStoreService,
    private supplierService: SupplierService,
    private purchaseOrderStore: PurchaseOrderStoreService,
    private purchaseOrderService: PurchaseOrderService,
    private purchaseOrderApi: PurchaseOrderApi,
    private receiptService: ReceiptService,
    private receiptApi: ReceiptApi,
    private dialog: DialogControllerService,
    private modalController: ModalController,
    private auth: AuthenticateStore
  ) {
    super();
  }

  get totalReceipts() {
    return this.receipts.length;
  }

  get supplierDebt(): number {
    const paid_amount = this.receipts.reduce((a, b) => {
      return a + Number(b.amount);
    }, 0);
    const result = this.purchase_order.p_data.total_amount - paid_amount;
    return result > 0 && result || 0;
  }

  async ngOnInit() {
    await this.getSuppliers();

    this.purchaseOrderStore.linesChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(lines => {
        this.purchase_order.p_data.lines = lines;
      });
    this.purchaseOrderStore.basketValueChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(basket_value => {
        this.purchase_order.p_data.basket_value = basket_value;
        this.updateTotalAmount();
      });
    this.receiptStore.receiptsUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(receipts => {
        this.receipts = receipts;
      });

    this.showCreateSupplier = this.auth.snapshot.permission.permissions.includes('shop/supplier:create');
  }

  async getSuppliers() {
    try {
      this.suppliers = await this.supplierService.getSuppliers();
    } catch (e) {
      this.suppliers = [];
    }
  }

  selectSupplier() {
    this.purchase_order.p_data.supplier_id = this.suppliers[this.supplierIndex] && this.suppliers[this.supplierIndex].id;
  }

  showAddSupplierModal() {
    this.modal = this.modalController.create({
      component: CreateSupplierModalComponent
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async(supplier) => {
      this.modal = null;
      if (supplier && supplier.id) {
        this.suppliers.unshift(supplier);
        this.suppliers = this.suppliers.map((s, index) => this.supplierService.mapSupplierInfo(s, index));
        this.supplierIndex = 0;
        this.selectSupplier();
      }
    });
  }

  discountCalcTypeChange() {
    switch (Number(this.discount_calc_type)) {
      case 1:
        this.toggleDiscountOption('default');
        break;
      case 2:
        this.toggleDiscountOption('percent');
        break;
      default:
        this.toggleDiscountOption('default');
    }
  }

  toggleDiscountOption(type) {
    this.purchase_order.p_data.total_discount = 0;
    if (type == 'percent' && this.discountPercent) {
      return;
    }
    if (type == 'default' && this.discountDefault) {
      return;
    }
    this.discountDefault = !this.discountDefault;
    this.discountPercent = !this.discountPercent;
  }

  updateTotalAmount() {
    const { basket_value, total_discount } = this.purchase_order.p_data;
    let totalDiscount = total_discount;
    if (this.discountPercent) {
      totalDiscount = total_discount / 100 * basket_value;
    }
    this.purchase_order.p_data.total_amount = (basket_value || 0) - (totalDiscount || 0);
  }

  resetData() {
    this.purchase_order_id = null;
    this.purchase_order = new PurchaseOrder({
      p_data: {}
    });
    this.receiptStore.resetData();
    this.supplierIndex = null;
    this.purchaseOrderStore.recentlyCreated();
  }

  checkInvalidLines(lines: PurchaseOrderLine[]) {
    let invalid_index: number;
    const hasZeroQuantity = lines.some((l, index) => {
      if (!l.quantity) {
        invalid_index = index + 1;
        return true;
      }
    });
    return {invalid: hasZeroQuantity, invalid_index};
  }

  async create(createAndConfirm: boolean) {
    this.loading = true;
    this.createAndConfirm = createAndConfirm;
    try {
      const body: PurchaseOrder = {
        ...this.purchase_order.p_data,
        total_discount: this.discountPercent
          && (this.purchase_order.p_data.total_discount / 100 * this.purchase_order.p_data.basket_value)
          || this.purchase_order.p_data.total_discount
      };

      if (!body.lines || !body.lines.length) {
        this.loading = false;
        this.createAndConfirm = false;
        return toastr.error('Chưa chọn sản phẩm!');
      }
      const invalid_lines = this.checkInvalidLines(body.lines);
      if (invalid_lines.invalid) {
        this.loading = false;
        this.createAndConfirm = false;
        return toastr.error(`Chưa nhập số lượng hoặc số lượng bằng 0 ở sản phẩm thứ ${invalid_lines.invalid_index}!`);
      }
      if (!body.supplier_id) {
        this.loading = false;
        this.createAndConfirm = false;
        return toastr.error('Chưa chọn nhà cung cấp!');
      }

      let createRequest = new CreatePurchaseOrderRequest();

      createRequest = {
        ...createRequest,
        ...body,
      }

      createRequest.discount_lines = [{
        amount: body.total_discount,
        note:''
      }
      ]

      if (createAndConfirm) {
        this.confirm(createRequest);
      } else {
        const res = await this.purchaseOrderApi.createPurchaseOrder(createRequest);
        toastr.success(`Tạo đơn nhập hàng thành công.`);
        await this.createReceipt(res);
        this.resetData();
      }
    } catch (e) {
      debug.error('ERROR in creating Purchase Order', e);
      toastr.error(`Tạo đơn nhập hàng không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.createAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận đơn nhập hàng`,
      body: `
        <div>Bạn có chắc muốn xác nhận đơn nhập hàng?</div>
        <div>Sau khi xác nhận, 1 <strong>phiếu nhập kho</strong> sẽ được tự động sinh ra.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi phiếu nhập kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          let res: any;
          if (!this.purchase_order_id) {
            res = await this.purchaseOrderApi.createPurchaseOrder(body);
            toastr.success(`Tạo đơn nhập hàng thành công.`);
            this.purchase_order_id = res.id;
          } else {
            res = await this.purchaseOrderApi.updatePurchaseOrder({...body, id: this.purchase_order_id});
          }

          await this.purchaseOrderApi.confirmPurchaseOrder(res.id, "confirm");
          toastr.success(`Xác nhận đơn nhập hàng thành công.`);
          await this.createReceipt(res);
          this.resetData();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Xác nhận đơn nhập hàng không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  async createReceipt(purchase_order: PurchaseOrder) {
    try {
      const only_cash = this.receiptStore.snapshot.receipts.length == 1 &&
        this.receiptStore.snapshot.receipts[0].ledger_type == 'cash';
      const receipts = this.receiptStore.snapshot.receipts.filter(r => r.amount && r.amount > 0)
        .map(r => {
          return new Receipt({
            ledger_id: r.ledger_id,
            type: 'payment',
            paid_at: new Date(),
            trader_id: purchase_order.supplier_id,
            amount: (only_cash && r.amount > purchase_order.total_amount) && purchase_order.total_amount || r.amount,
            title: 'Thanh toán đơn nhập hàng',
            ref_type: 'purchase_order',
            lines: [
              {
                title: 'Thanh toán đơn nhập hàng',
                amount: (only_cash && r.amount > purchase_order.total_amount) && purchase_order.total_amount || r.amount,
                ref_id: purchase_order.id
              }
            ]
          });
        });
      if (!receipts.length || !purchase_order.supplier_id || purchase_order.supplier_id == "0") {
        return;
      }

      let success = 0;
      for (let i = 0; i < receipts.length; i++) {
        let confirmed = false;
        let receipt = new Receipt({});
        try {
          receipt = await this.receiptService.createReceipt(receipts[i]);
          success += 1;
          await this.receiptApi.confirmReceipt(receipt.id);
          confirmed = true;
        } catch(e) {
          if (!confirmed) {
            toastr.error(`Xác nhận phiếu chi ${receipt.code ? '#' + receipt.code : ''} không thành công!`);
          }
          debug.error(`ERROR in creating receipt ${i}`, e.message || e.msg);
        }
      }
      if (success) {
        toastr.success('Tạo phiếu chi thành công!');
      } else {
        toastr.error('Tạo phiếu chi không thành công!');
      }

    } catch(e) {
      toastr.error('Tạo phiếu chi không thành công!');
      debug.error('ERROR in creating receipt', e.message || e.msg);
    }
  }

}
