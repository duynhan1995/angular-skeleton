import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Receipt } from 'libs/models/Receipt';
import { FilterOperator, Filters } from '@etop/models';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ReceiptApi } from '@etop/api';

@Component({
  selector: 'shop-purchase-order-receipts',
  templateUrl: './purchase-order-receipts.component.html',
  styleUrls: ['./purchase-order-receipts.component.scss']
})
export class PurchaseOrderReceiptsComponent implements OnInit, OnChanges {
  @Input() purchase_order_id: string;
  @Input() purchase_order_code: string;
  receipts: Array<Receipt> = [];
  loading = false;

  constructor(
    private router: Router,
    private receiptService: ReceiptService,
    private receiptApi: ReceiptApi,
    private util: UtilService
  ) { }

  ngOnInit() {
  }

  async ngOnChanges(changes: SimpleChanges) {
    await this.getReceipts();
  }

  async getReceipts() {
    this.loading = true;
    try {
      const filters: Filters = [{
        name: "ref_ids",
        op: FilterOperator.contains,
        value: this.purchase_order_id
      }];
      const res = await this.receiptService.getReceipts(0, 1000, filters);
      this.receipts = res.receipts.map(r => {
        const ref = r.lines.find(l => l.ref_id == this.purchase_order_id);
        return {
          ...this.receiptApi.receiptMap(r),
          amount: ref && ref.amount || 0
        }
      });
    } catch (e) {
      debug.error(e.message);
      this.receipts = [];
    }
    this.loading = false;
  }

  viewDetailReceipt(code) {
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/receipts?code=${code}`
    );
  }

}
