import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Supplier } from 'libs/models/Supplier';
import { SupplierApi } from '@etop/api';

@Component({
  selector: 'shop-create-supplier-modal',
  templateUrl: './create-supplier-modal.component.html',
  styleUrls: ['./create-supplier-modal.component.scss']
})
export class CreateSupplierModalComponent implements OnInit {
  supplier = new Supplier({});

  loading = false;

  constructor(
    private modalAction: ModalAction,
    private supplierApi: SupplierApi
  ) { }

  ngOnInit() {
  }

  dismissModal(supplier?) {
    this.modalAction.dismiss(supplier);
  }

  async createSupplier() {
    this.loading = true;
    try {
      const {full_name, phone} = this.supplier;

      if (!full_name || !phone) {
        this.loading = false;
        return toastr.error('Chưa nhập tên và số điện thoại nhà cung cấp!');
      }
      const supplier = await this.supplierApi.createSupplier(this.supplier);
      toastr.success('Thêm nhà cung cấp mới thành công!');
      this.dismissModal(supplier);
    } catch(e) {
      toastr.error('Tạo khách hàng mới không thành công.', e.message || e.msg);
    }
    this.loading = false;
  }

}
