import { Component, Input, OnInit } from '@angular/core';
import { PurchaseOrderLine } from 'libs/models/PurchaseOrder';

@Component({
  selector: 'shop-purchase-order-variants',
  templateUrl: './purchase-order-variants.component.html',
  styleUrls: ['./purchase-order-variants.component.scss']
})
export class PurchaseOrderVariantsComponent implements OnInit {
  @Input() lines: PurchaseOrderLine[] = [];

  constructor() { }

  ngOnInit() {
  }

}
