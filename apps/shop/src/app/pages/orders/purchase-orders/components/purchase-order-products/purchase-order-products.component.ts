import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Variant } from 'libs/models/Product';
import { PurchaseOrderLine } from 'libs/models/PurchaseOrder';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-purchase-order-products',
  templateUrl: './purchase-order-products.component.html',
  styleUrls: ['./purchase-order-products.component.scss']
})
export class PurchaseOrderProductsComponent implements OnInit, OnChanges {
  @Input() variants: Variant[] = [];
  @Input() isNewPurchaseOrder = true;

  constructor(
    private purchaseOrderStore: PurchaseOrderStoreService,
    private util: UtilService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.lineUpdated();
  }

  totalPrice(data) {
    if (!data) {
      return 0;
    }
    return (data.payment_price * data.quantity) || 0;
  }

  removeLine(index) {
    this.variants.splice(index, 1);
    this.lineUpdated();
  }

  lineUpdated() {
    const lines: PurchaseOrderLine[] = this.variants.map(v => {
      return {
        ...new PurchaseOrderLine(),
        variant_id: v.id,
        quantity: v.p_data.quantity || 0,
        payment_price: v.p_data.payment_price || 0
      }
    });
    const basket_value = lines.reduce((a, b) => a + Number(b.quantity) * Number(b.payment_price), 0);
    this.purchaseOrderStore.changeLines(lines);
    this.purchaseOrderStore.changeBasketValue(basket_value);
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

}
