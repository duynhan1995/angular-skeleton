import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { PurchaseOrder } from 'libs/models/PurchaseOrder';
import { FilterOperator, Filters } from '@etop/models';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { PurchaseOrderApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { CancelObjectComponent } from 'apps/shop/src/app/components/cancel-object/cancel-object.component';
import { ActionButton, EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-purchase-order-list',
  templateUrl: './purchase-order-list.component.html',
  styleUrls: ['./purchase-order-list.component.scss']
})
export class PurchaseOrderListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('purchaseOrderTable', { static: true }) purchaseOrderTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  purchaseOrderList: Array<PurchaseOrder> = [];
  selectedPurchaseOrders: Array<PurchaseOrder> = [];
  filters: Filters = [];
  emptyAction: ActionButton[] = [];
  checkAll = false;
  creatingNewPurchaseOrder = false;

  page: number;
  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  private modal: any;
  private _selectMode = false;

  constructor(
    private headerController: HeaderControllerService,
    private purchaseOrderService: PurchaseOrderService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private purchaseOrderStore: PurchaseOrderStoreService,
    private purchaseOrderApi: PurchaseOrderApi,
    private modalController: ModalController,
    private ordersController: SellOrdersController,
    private dropdownController: DropdownActionsControllerService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  private _onSelectModeChanged(value) {
    this.purchaseOrderTable.toggleLiteMode(value);
    this.slider.toggleLiteMode(value);
  }

  ngOnInit() {
    this.emptyAction = [
      {
        title: 'Nhập hàng',
        cssClass: 'btn-primary',
        onClick: () => this.createPurchaseOrder()
      }
    ]
    this.purchaseOrderStore.recentlyUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(recentlyUpdated => {
        if (recentlyUpdated) {
          this.reloadPurchaseOrders();
        }
      });

    this.headerController.setActions([
      {
        title: 'Nhập hàng',
        cssClass: 'btn btn-primary',
        onClick: () => this.createPurchaseOrder(),
        permissions: ['shop/purchase_order:create']
      }
    ]);
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  isNew(purchase_order: PurchaseOrder) {
    return purchase_order.status != 'N';
  }

  createPurchaseOrder() {
    window.open(`/s/${this.util.getSlug()}/orders/purchase-orders/create`, '_self');
  }

  async filter($event: Filters) {
    this.selectedPurchaseOrders = [];
    if (this.queryParams.code) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/orders`);
    }
    this.filters = $event;
    this.purchaseOrderTable.resetPagination();
  }

  async getPurchaseOrders(page?, perpage?) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.purchaseOrderTable.toggleNextDisabled(false);

      const res = await this.purchaseOrderService.getPurchaseOrders(
        ((page || this.page) - 1) * perpage,
        perpage || this.perpage,
        this.filters
      );
      const _purchaseOrderList = res.purchase_orders;
      if (page > 1 && _purchaseOrderList.length == 0) {
        this.purchaseOrderTable.toggleNextDisabled(true);
        this.purchaseOrderTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả đơn nhập hàng.');
        return;
      }

      this.purchaseOrderList = _purchaseOrderList.map(
        p_o => this.purchaseOrderStore.setupDataPurchaseOrder(p_o)
      );
      if (this.queryParams.code) {
        this.detail(null, this.purchaseOrderList[0]);
      }

      // Remember here in-order-to reload purchase_order_list after updating purchaseOrderList
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list purchaseOrderList', e);
    }
  }

  async reloadPurchaseOrders() {
    const { perpage, page } = this;
    const selectedPurchaseOrderIDs = this.selectedPurchaseOrders.map(r => r.id);

    let res = await this.purchaseOrderService.getPurchaseOrders(
      (page - 1) * perpage,
      perpage,
      this.filters
    );
    const _purchaseOrders = res.purchase_orders;

    this.purchaseOrderList = _purchaseOrders.map(p_o => {
      if (selectedPurchaseOrderIDs.indexOf(p_o.id) != -1) {
        const _purchaseOrder = this.selectedPurchaseOrders.find(
          selected => selected.id == p_o.id
        );
        if (_purchaseOrder) {
          p_o.p_data = _purchaseOrder.p_data;
        }
      }
      return this.purchaseOrderStore.setupDataPurchaseOrder(p_o);
    });
    this.selectedPurchaseOrders = this.purchaseOrderList.filter(
      p_o => p_o.p_data.selected || p_o.p_data.detailed
    );
  }

  async loadPage({ page, perpage }) {
    this.purchaseOrderTable.loading = true;
    this.changeDetector.detectChanges();
    this._paramsHandling();
    await this.getPurchaseOrders(page, perpage);
    this.purchaseOrderTable.loading = false;
    this.changeDetector.detectChanges();
  }

  checkAllPurchaseOrder() {
    this.checkAll = !this.checkAll;
    this.creatingNewPurchaseOrder = false;
    this.purchaseOrderList.forEach(p_o => {
      p_o.p_data.selected = this.checkAll;
    });
    this._checkSelectMode();
  }

  purchaseOrderSelected(purchase_order: PurchaseOrder) {
    this.creatingNewPurchaseOrder = false;
    this.purchaseOrderList.forEach(p_o => p_o.p_data.detailed = false);
    purchase_order.p_data.selected = !purchase_order.p_data.selected;
    if (!purchase_order.p_data.selected) {
      this.checkAll = false;
    }
    this._checkSelectMode();
  }

  detail(event, purchase_order: PurchaseOrder) {
    if (event && event.target.type == 'checkbox') { return; }
    if (this.checkedPurchaseOrders) { return; }
    this.purchaseOrderList.forEach(r => {
      r.p_data.detailed = false;
    });
    this.checkAll = false;
    purchase_order = this.purchaseOrderStore.setupDataPurchaseOrder(purchase_order);
    purchase_order.p_data.detailed = true;
    this.selectedPurchaseOrders = [purchase_order];
    this._checkSelectMode()
  }

  private async _checkSelectMode() {
    this.selectedPurchaseOrders = this.purchaseOrderList.filter(p_o =>
      p_o.p_data.selected || p_o.p_data.detailed
    );
    this.setupDropdownActions();
    this.selectMode = this.selectedPurchaseOrders.length > 0 || this.creatingNewPurchaseOrder;
    if (this.queryParams.code && this.selectedPurchaseOrders.length == 0) {
      this.filters = [];
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/orders`);
      this.purchaseOrderTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.purchaseOrderList.forEach(p_o => {
      p_o.p_data.selected = false;
      p_o.p_data.detailed = false;
    });
    this.selectedPurchaseOrders = [];
    this.checkAll = false;
    this._checkSelectMode();
  }

  async cancel() {
    if (this.modal) { return; }
    const purchase_order = this.selectedPurchaseOrders[0];
    this.modal = this.modalController.create({
      component: CancelObjectComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        title: `đơn nhập hàng #${purchase_order.code}`,
        cancelFn: async (reason: string) => {
          try {
            await this.purchaseOrderApi.cancelPurchaseOrder(purchase_order.id, reason, 'confirm');
            toastr.success(`Huỷ đơn nhập hàng thành công.`);
          } catch(e) {
            toastr.error(`Huỷ đơn nhập hàng không thành công.`, e.code && (e.message || e.msg) || '');
            throw e;
          }
        }
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async() => {
      this.modal = null;
      await this.reloadPurchaseOrders();
      this.setupDropdownActions();
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    if (this.queryParams.type != 'po') {
      return;
    }
    this.filters = [{
      name: 'code',
      op: FilterOperator.eq,
      value: this.queryParams.code
    }];
  }

  private setupDropdownActions() {
    if (!this.selectedPurchaseOrders.length) {
      this.dropdownController.clearActions();
      return;
    }
    this.dropdownController.setActions([
      {
        onClick: () => this.cancel(),
        title: `Huỷ đơn nhập hàng`,
        cssClass: 'text-danger',
        disabled: !this.isNew(this.selectedPurchaseOrders[0])
      }
    ]);
  }

  get sliderTitle() {
    if (this.selectedPurchaseOrders.length == 1 && this.showDetailPurchaseOrder) {
      return 'Chi tiết đơn nhập hàng';
    } else {
      return `Thao tác trên <span class="text-bold">${
        this.selectedPurchaseOrders.length
        }</span> đơn nhập hàng`;
    }
  }

  get emptyResultFilter() {
    return this.page == 1 && this.purchaseOrderList.length == 0 && this.filters.length > 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy đơn nhập hàng phù hợp';
    }
    return 'Cửa hàng của bạn chưa có đơn nhập hàng';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get checkedPurchaseOrders() {
    return this.purchaseOrderList.some(p_o => p_o.p_data.selected);
  }

  get showPaging() {
    return !this.purchaseOrderTable.liteMode && !this.purchaseOrderTable.loading;
  }

  get showDetailPurchaseOrder() {
    return this.selectedPurchaseOrders.length && this.selectedPurchaseOrders.length == 1 && !this.checkedPurchaseOrders;
  }

}
