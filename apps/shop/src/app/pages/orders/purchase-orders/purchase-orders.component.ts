import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';

@Component({
  selector: 'shop-purchase-orders',
  templateUrl: './purchase-orders.component.html',
  styleUrls: ['./purchase-orders.component.scss']
})
export class PurchaseOrdersComponent extends PageBaseComponent implements OnInit {

  constructor(
    private orderStore: OrderStoreService,
  ) {
    super();
  }

  ngOnInit() {
    this.orderStore.changeHeaderTab('purchase_orders');
  }

}
