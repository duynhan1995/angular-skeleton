import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatePurchaseOrderComponent} from 'apps/shop/src/app/pages/orders/purchase-orders/create-purchase-order/create-purchase-order.component';
import {UpdatePurchaseOrderComponent} from 'apps/shop/src/app/pages/orders/purchase-orders/update-purchase-order/update-purchase-order.component';
import {PurchaseOrderListComponent} from 'apps/shop/src/app/pages/orders/purchase-orders/purchase-order-list/purchase-order-list.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {CreateMultiReceiptModule} from 'apps/shop/src/app/components/create-multi-receipt/create-multi-receipt.module';
import {PurchaseOrderInfoCreateComponent} from 'apps/shop/src/app/pages/orders/purchase-orders/components/purchase-order-info-create/purchase-order-info-create.component';
import {PurchaseOrderProductsComponent} from './components/purchase-order-products/purchase-order-products.component';
import {PurchaseOrderRowComponent} from './components/purchase-order-row/purchase-order-row.component';
import {PurchaseOrderService} from 'apps/shop/src/services/purchase-order.service';
import {PurchaseOrderApi, SupplierApi} from '@etop/api';
import {PurchaseOrderStoreService} from 'apps/core/src/stores/purchase-order.store.service';
import {PurchaseOrderDetailComponent} from './purchase-order-detail/purchase-order-detail.component';
import {PurchaseOrderVariantsComponent} from './components/purchase-order-variants/purchase-order-variants.component';
import {SupplierService} from 'apps/shop/src/services/supplier.service';
import {CreateSupplierModalComponent} from './components/create-supplier-modal/create-supplier-modal.component';
import {PurchaseOrderPaymentInfoComponent} from './components/purchase-order-payment-info/purchase-order-payment-info.component';
import {PurchaseOrderInfoUpdateComponent} from './components/purchase-order-info-update/purchase-order-info-update.component';
import {PurchaseOrderReceiptsComponent} from './components/purchase-order-receipts/purchase-order-receipts.component';
import {SearchVariantsModule} from 'apps/shop/src/app/components/search-variants/search-variants.module';
import {AuthenticateModule} from '@etop/core';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {NotPermissionModule} from 'libs/shared/components/not-permission/not-permission.module';
import {CreateReceiptModalModule} from 'apps/shop/src/app/components/modals/create-receipt-modal/create-receipt-modal.module';
import {PurchaseOrdersComponent} from './purchase-orders.component';
import {CancelObjectModule} from 'apps/shop/src/app/components/cancel-object/cancel-object.module';
import {
  EtopCommonModule,
  EtopMaterialModule,
  EtopPipesModule,
  SideSliderModule,
  EtopFormsModule,
  EmptyPageModule
} from '@etop/shared';

@NgModule({
  declarations: [
    CreatePurchaseOrderComponent,
    UpdatePurchaseOrderComponent,
    PurchaseOrderListComponent,
    PurchaseOrderInfoCreateComponent,
    PurchaseOrderProductsComponent,
    PurchaseOrderRowComponent,
    PurchaseOrderDetailComponent,
    PurchaseOrderVariantsComponent,
    CreateSupplierModalComponent,
    PurchaseOrderPaymentInfoComponent,
    PurchaseOrderInfoUpdateComponent,
    PurchaseOrderReceiptsComponent,
    PurchaseOrdersComponent
  ],
  entryComponents: [
    CreateSupplierModalComponent
  ],
  exports: [
    PurchaseOrdersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    CreateMultiReceiptModule,
    SearchVariantsModule,
    AuthenticateModule,
    DropdownActionsModule,
    NotPermissionModule,
    CreateReceiptModalModule,
    CancelObjectModule,
    EtopPipesModule,
    EtopCommonModule,
    SideSliderModule,
    EtopMaterialModule,
    EtopFormsModule,
    EmptyPageModule
  ],
  providers: [
    PurchaseOrderService,
    PurchaseOrderApi,
    PurchaseOrderStoreService,
    SupplierService,
    SupplierApi
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PurchaseOrdersModule {
}
