import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { OrdersComponent } from 'apps/shop/src/app/pages/orders/orders.component';
import { PurchaseOrdersModule } from 'apps/shop/src/app/pages/orders/purchase-orders/purchase-orders.module';
import { CreatePurchaseOrderComponent } from 'apps/shop/src/app/pages/orders/purchase-orders/create-purchase-order/create-purchase-order.component';
import { UpdatePurchaseOrderComponent } from 'apps/shop/src/app/pages/orders/purchase-orders/update-purchase-order/update-purchase-order.component';
import { NotPermissionModule } from '@etop/shared';
import { SellOrdersModule } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.module';
import { NotSubscriptionModule } from '@etop/shared/components/not-subscription/not-subscription.module';
import { PurchaseOrdersComponent } from './purchase-orders/purchase-orders.component';
import { SellOrdersComponent } from './sell-orders/sell-orders.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    children: [
      {
        path: 'purchase-orders',
        component: PurchaseOrdersComponent
      },
      {
        path: 'purchase-orders/create',
        component: CreatePurchaseOrderComponent
      },
      {
        path: 'purchase-orders/update/:id',
        component: UpdatePurchaseOrderComponent
      },
      {
        path: 'orders',
        component: SellOrdersComponent
      },
      {
        path: '**', redirectTo: 'orders'
      }
    ]
  }
];

const components = [
  OrdersComponent
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    SellOrdersModule,
    PurchaseOrdersModule,
    NotPermissionModule,
    NotSubscriptionModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule {}
