import {Component, OnDestroy, OnInit} from '@angular/core';
import {SellOrdersController} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {OrderService} from 'apps/shop/src/services/order.service';
import {FulfillmentService} from 'apps/shop/src/services/fulfillment.service';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {OrderStoreService} from 'apps/core/src/stores/order.store.service';
import {distinctUntilChanged, map} from 'rxjs/operators';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FulfillmentStore} from 'apps/core/src/stores/fulfillment.store';
import {CustomerService} from 'apps/shop/src/services/customer.service';
import {UtilService} from 'apps/core/src/services/util.service';
import {SubscriptionService} from '@etop/features/services/subscription.service';

@Component({
  selector: 'shop-orders',
  template: `
    <ng-container *ngIf="allowView; else notPermission">
      <ng-container *ngIf="subscription || (checkTab('orders') | async); else notSubscription">
        <router-outlet></router-outlet>
      </ng-container>
      <ng-template #notSubscription>
        <etop-not-subscription></etop-not-subscription>
      </ng-template>
    </ng-container>
    <ng-template #notPermission>
      <etop-not-permission></etop-not-permission>
    </ng-template>
    <ng-container></ng-container>
  `,
})
export class OrdersComponent extends BaseComponent implements OnInit, OnDestroy {
  allowView = true;
  subscription = false;

  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private router: Router,
    private headerController: HeaderControllerService,
    private customerService: CustomerService,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private subscriptionService: SubscriptionService
  ) {
    super();
  }

  checkTab(tabName) {
    // const type = this.router.url.split('/')[4];
    // debug.log("TYPE", type)
    // switch (type) {
    //   case 'orders':
    //     this.orderStore.changeHeaderTab('orders');
    //     break;
    //   case 'purchase_orders':
    //     this.orderStore.changeHeaderTab('purchase_orders');
    //     break;
    // }
    return this.orderStore.headerTabChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  async ngOnInit() {
    this.subscription = await this.subscriptionService.checkSubcriptions();
    this.ffmStore.resetState();

    if (this.subscription) {
      this.setupTabs();
      this.orderStore.headerTabChanged$.subscribe(_ => {
        this.setupTabs();
      });

      if (this.auth.snapshot.permission.permissions.includes('shop/order:view')) {
        this.router.navigateByUrl(`s/${this.util.getSlug()}/orders/orders`).then();
      } else if (this.auth.snapshot.permission.permissions.includes('shop/purchase_order:view')) {
        this.router.navigateByUrl(`s/${this.util.getSlug()}/orders/purchase_orders`).then();
      } else {
        this.allowView = false;
      }
    }

    this.customerService.getCustomers().then();
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Bán hàng',
          name: 'orders',
          active: false,
          onClick: () => {
            this.tabClick('orders').then();
          },
          permissions: ['shop/order:view'],
        },
        {
          title: 'Nhập hàng',
          name: 'purchase_orders',
          active: false,
          onClick: () => {
            this.tabClick('purchase-orders').then();
          },
          permissions: ['shop/purchase_order:view'],
        },
      ].map((tab) => {
        this.checkTab(tab.name).subscribe((active) => (tab.active = active));
        return tab;
      })
    );
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`s/${this.util.getSlug()}/orders/${type}`);
  }
}
