import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {CANCEL_ORDER_REASONS, Order} from 'libs/models/Order';
import { OrderService } from 'apps/shop/src/services/order.service';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { FilterOperator, Filters } from '@etop/models';
import {
  PrintType,
  SellOrdersController
} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ExportOrderModalComponent } from 'apps/shop/src/app/components/modals/export-order-modal/export-order-modal.component';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { OrderApi } from '@etop/api';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { takeUntil } from 'rxjs/operators';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { CancelObjectComponent } from 'apps/shop/src/app/components/cancel-object/cancel-object.component';
import { TelegramService } from '@etop/features';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { ConnectionStore } from "@etop/features/connection/connection.store";
import { PrintControllerService } from 'apps/shop/src/app/components/print/print.controller';
import { SubscriptionService } from "@etop/features/services/subscription.service";
import { ActionButton, EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-sell-order-list',
  templateUrl: './sell-order-list.component.html',
  styleUrls: ['./sell-order-list.component.scss']
})
export class SellOrderListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('orderTable', { static: true }) orderTable: EtopTableComponent;
  /* NOTE: all the cases which OPEN the slider:
   * - viewing detail an order
   * - selecting multiple orders
   * - importing order
   */

  /* NOTE: all the cases which CLOSE the slider:
   * - click close button in slider
   * - uncheck all item
   * - uncheck one single item but leading to no order is selected
   */

  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  tabs = [
    { name: 'Thông tin', value: 'detail_info', permissions: ['shop/order:view']},
    { name: 'Phiếu thu chi', value: 'receipt' , permissions: ['shop/receipt:view']},
    { name: 'Tồn kho', value: 'inventory', permissions: ['shop/inventory:view'] },
    { name: 'Lịch sử GH', value: 'history', permissions: ['shop/fullfilment:view'] }
  ];
  active_tab: 'receipt' | 'inventory' | 'detail_info' | 'history' = 'detail_info';

  orderList: Array<Order> = [];
  filters: Filters = [];
  emptyAction: ActionButton[] = [];
  checkAll = false;
  selectedOrders: Array<Order> = [];
  page: number;
  perpage: number;

  toImportOrder = false;
  doingImport = false;
  forcedHidePaging = false;

  queryParams = {
    code: '',
    type: '',
  };
  params = {
    id: ''
  };

  private modal: any;

  constructor(
    private auth: AuthenticateStore,
    private telegram: TelegramService,
    private changeDetector: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private util: UtilService,
    private ordersController: SellOrdersController,
    private headerController: HeaderControllerService,
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private dropdownController: DropdownActionsControllerService,
    private promiseQueue: PromiseQueueService,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private orderApi: OrderApi,
    private connectionStore: ConnectionStore,
    private printController: PrintControllerService,
  ) {
    super();
  }



  get sliderTitle() {
    if (this.toImportOrder) {
      return `Import đơn hàng`;
    }
    return (
      (this.showDetailOrder && 'Chi tiết Hóa đơn') ||
      `Thao tác trên <strong>${this.selectedOrders.length}</strong> đơn hàng`
    );
  }

  get emptyResultFilter() {
    return this.page == 1 && this.orderList.length == 0 && this.filters.length > 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy đơn hàng phù hợp';
    }
    return 'Cửa hàng của bạn chưa có đơn hàng';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get checkedOrders() {
    return this.orderList.some(o => o.p_data.selected);
  }

  get showPaging() {
    return !this.orderTable.liteMode && !this.orderTable.loading;
  }

  get showDetailOrder() {
    return this.selectedOrders.length == 1 && !this.checkedOrders;
  }

  get multiShipment() {
    return this.ordersController.createShipmentFfm;
  }

  get multiShipnow() {
    return this.ordersController.createShipnowFfm;
  }

  get hasParams(): boolean {
    return !!this.queryParams.code || !!this.params.id;
  }

  async ngOnInit() {
    // TODO: after new filter implemented, change value in filter Order.code!!!
    this._paramsHandling();
    this.emptyAction = [
      {
        title: 'Import đơn hàng',
        cssClass: 'btn-primary btn-outline',
        onClick: () => this.importOrder()
      },
      {
        title: 'Tạo đơn hàng',
        cssClass: 'btn-primary',
        onClick: () => this.gotoPOS()
      }
    ]

    this.page = this.orderTable.currentPage;
    this.perpage = this.orderTable.perpage;

    this.ordersController.registerOrderList(this);
    this.headerController.setActions([
      {
        onClick: () => this.exportOrder(),
        title: 'Export',
        cssClass: 'btn btn-primary btn-outline',
        permissions: ['shop/order:export']
      },
      {
        onClick: () => this.importOrder(),
        title: 'Import',
        cssClass: 'btn btn-primary btn-outline',
        permissions: ['shop/order:import']
      },
      {
        onClick: () => this.gotoPOS(),
        title: 'Tạo đơn',
        cssClass: 'btn btn-primary',
        permissions: ['shop/order:create']
      }
    ]);

    this.ordersController.createFfmAfterImporting$
      .pipe(takeUntil(this.destroy$))
      .subscribe(orders => {
        this.orderList = orders.map(o => {
          o.p_data.just_imported = true;
          o.p_data.selected = true;
          return this.ordersController.setupDataOrder(o);
        });
        this.doingImport = true;
        this.toImportOrder = false;
        this.selectedOrders = this.orderList;
        this.checkAll = true;
      });
    this.ffmService.onViewReturningFfms$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.loadAllReturningOrders();
      });
    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.setupDropdownActions(true, this.selectedOrders);
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.doingImport = false;
  }

  changeTab(tab_value) {
    this.active_tab = tab_value;
    this.ordersController.onChangeTab$.next(tab_value);
  }

  resetState() {
    this.toImportOrder = false;
    this.selectedOrders = [];
    this.slider.toggleLiteMode(false);
    this.orderTable.toggleLiteMode(false);
    this.orderTable.toggleNextDisabled(false);
    this.checkAll = false;
    this.orderList.forEach(o => (o.p_data = { selected: false }));
  }

  resetOrderPData() {
    this.orderList.forEach(o => {
      o.p_data = {
        ...o.p_data,
        detailed: false,
        selected: false,
        just_imported: false,
        un_updatable: false
      };
    });
  }

  async filter($event: Filters) {
    this.selectedOrders = [];
    if (this.hasParams) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/orders`);
    }
    this.orderService.mapSpecialFilters($event);
    this.filters = $event;
    this.orderTable.resetPagination();
  }

  async loadAllReturningOrders() {
    this.filters = [{
      name: "fulfillment.shipping_state",
      op: FilterOperator.n,
      value: 'returning'
    }];
    await this.loadOrders(1, 1000);
    this.forcedHidePaging = true;
  }

  async loadOrders(page, perpage) {
    try {
      this.resetState();
      let _orders = [];

      if (this.params.id) {
        _orders = await this.orderApi.getOrdersByIDs([this.params.id]);
      } else {
        _orders = await this.orderService.getOrders(
          (page - 1) * perpage,
          perpage,
          this.filters
        );
      }

      if (page > 1 && _orders.length == 0) {
        this.orderTable.toggleNextDisabled(true);
        this.orderTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả đơn hàng.');
        return;
      }

      this.orderList = _orders.map(o => this.ordersController.setupDataOrder(o));
      if (this.hasParams) {
        this.detail(null, this.orderList[0]);
      }

      // NOTE: Remember here in-order-to reload order_list after canceling orders
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list orders', e);
    }
  }

  // NOTE: reload/refresh/re-render order list (after canceling order/ffm, updating order/ffm)
  async reloadOrders() {
    const { perpage, page } = this;
    const selected_order_ids = this.selectedOrders.map(o => o.id);

    let _orders = [];
    if (this.params.id) {
      _orders = await this.orderApi.getOrdersByIDs([this.params.id]);
    } else {
      _orders = await this.orderService.getOrders(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
    }
    this.orderList = _orders.map(o => {
      if (selected_order_ids.indexOf(o.id) != -1) {
        const _order = this.selectedOrders.find(
          selected => selected.id == o.id
        );
        if (_order) {
          o.p_data = _order.p_data;
        }
      }
      return this.ordersController.setupDataOrder(o);
    });
    this.selectedOrders = this.orderList.filter(
      o => o.p_data.selected || o.p_data.detailed
    );
  }

  async loadPage({ perpage, page }) {
    this.forcedHidePaging = false;
    this.orderTable.loading = true;
    this.changeDetector.detectChanges();
    this._paramsHandling();
    await this.loadOrders(page, perpage);
    this.orderTable.loading = false;
    this.changeDetector.detectChanges();
  }

  private _paramsHandling() {
    const { queryParams, params } = this.route.snapshot;
    if (params.id) {
      this.params.id = params.id;
      return;
    }
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    if (this.queryParams.type != 'so') {
      return;
    }
    this.filters = [{
      name: 'code',
      op: FilterOperator.eq,
      value: this.queryParams.code
    }];
  }

  private setupDropdownActions(single: boolean, orders: Order[]) {
    if (!this.selectedOrders.length) {
      this.dropdownController.clearActions();
      return;
    }
    if (single) {
      const order = orders[0];
      if (!order) { return; }
      this.dropdownController.setActions([
        {
          onClick: () => this.printOrder(),
          title: 'In hoá đơn',
          permissions: ['shop/order:view']
        },
        {
          onClick: () => this.printFulfillment(),
          title: 'In phiếu giao hàng',
          disabled: !order.activeFulfillment || !order.activeFulfillment.shipping_code,
          permissions: ['shop/order:view']
        },
        {
          onClick: () => this.printOrderAndFfm(),
          title: 'In hoá đơn và phiếu giao hàng',
          disabled: !order.activeFulfillment || !order.activeFulfillment.shipping_code,
          permissions: ['shop/order:view']
        },
        {
          onClick: () => this.completeOrder(),
          title: 'Hoàn thành đơn hàng',
          cssClass: 'text-success',
          disabled: ['N', 'NS', 'P'].includes(order.status),
          permissions: ['shop/order:confirm']
        },
        {
          onClick: () => this.cancelOrder(true),
          title: 'Huỷ đơn hàng',
          cssClass: 'text-danger',
          disabled: ['N', 'NS'].includes(order.status),
          permissions: ['shop/order:cancel']
        },
        {
          title: 'Sao chép đơn hàng',
          tooltips: 'Đang phát triển',
          disabled: true,
          permissions: ['shop/order:view']
        }
      ]);
      return;
    }
    const availableOrders = orders.filter(order => {
      return order.status == 'Z' || order.status == 'S' || order.status == 'P';
    });
    const allCanceled = orders.every(order => {
      return order.status == 'N' || order.status == 'NS' ;
    });
    this.dropdownController.setActions([
      {
        onClick: () => this.cancelOrder(false),
        title: `Huỷ ${availableOrders.length} đơn hàng`,
        cssClass: 'text-danger',
        hidden: allCanceled,
        permissions: ['shop/order:cancel']
      },
      {
        title: 'Sao chép đơn hàng',
        tooltips: 'Đang phát triển',
        disabled: true
      }
    ]);
  }

  // NOTE: check one single item
  async itemSelected(item: Order) {
    this.orderList.forEach(o => {
      o.p_data.detailed = false;
    });
    item = this._setUpData(item);
    item.p_data.selected = !item.p_data.selected;
    if (!item.p_data.selected) {
      this.checkAll = false;
      // NOTE: remove that item on map when unselect/uncheck it
      if (this.ordersController.createShipnowFfm) {
        this.ordersController.removeMapMarkers$.next();
      }
    } else {
      if (this.multiShipment || this.multiShipnow) {
        if (!SellOrdersController.canCreateFulfillment(item)) {
          item.p_data.un_updatable = true;
        }
      }
    }
    this._checkSelectMode(item.p_data.selected ? item.id : null);
  }

  // NOTE: check all item
  allItemSelected() {
    this.checkAll = !this.checkAll;
    this.orderList.forEach(order => {
      order = this._setUpData(order);
      order.p_data.selected = this.checkAll;
      if (!this.checkAll) {
        order.p_data.detailed = false;
        order.p_data.just_imported = false;
        this.ordersController.createShipmentFfm = false;
        this.ordersController.createShipnowFfm = false;
      }
    });
    this._checkSelectMode();
  }

  // NOTE: check/select order will shut down all other sliders (detail order, import order)
  private async _checkSelectMode(order_id?: string) {
    this.toImportOrder = false;
    this.selectedOrders = this.orderList.filter(order => order.p_data.selected);

    this.setupDropdownActions(false, this.selectedOrders);

    if (this.selectedOrders.length == this.orderList.length) {
      this.checkAll = true;
    }
    let selected = this.selectedOrders.length;
    this.slider.toggleLiteMode(selected > 0);
    this.orderTable.toggleLiteMode(selected > 0);
    if (this.hasParams && selected == 0) {
      this.filters = [];
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/orders`);
      this.orderTable.resetPagination();
    }
    if (order_id) {
      this.ordersController.onRegetShipServices$.next(
        this.selectedOrders.length > 1 ? order_id : null
      );
    }
    if (!this.selectedOrders.length) {
      this.resetOrderPData();
      this.undoDelivery();
      if (this.doingImport) {
        this.loadPage({
          perpage: this.orderTable.perpage,
          page: this.orderTable.currentPage
        });
        this.doingImport = false;
      }
    }
  }

  // NOTE: clode slider
  onSliderClosed() {
    this.ffmService.resetData();
    this.checkAll = false;
    this.toImportOrder = false;
    this.orderList.forEach(order => {
      order.p_data.selected = false;
      order.p_data.detailed = false;
      order.p_data.un_updatable = false;
      order.p_data.just_imported = false;

      order.activeFulfillment = this.orderService.getActiveFulfillment(
        order, this.connectionStore.snapshot.initConnections
      );
    });
    this._checkSelectMode();
    this.ordersController.createShipmentFfm = false;
    this.ordersController.createShipnowFfm = false;
    if (this.doingImport) {
      this.loadPage({
        perpage: this.orderTable.perpage,
        page: this.orderTable.currentPage
      });
      this.doingImport = false;
    }
  }

  // NOTE: detail order will shut down all other sliders (import order, multi orders)
  detail(event, order: Order) {
    this.toImportOrder = false;
    this.changeTab('detail_info');
    this.orderService.switchOffCreatingFFM$.next();

    if (event && event.target.type == 'checkbox') {
      return;
    }
    if (this.checkedOrders || this.toImportOrder) {
      return;
    }

    this.orderList.forEach(o => {
      o.p_data.detailed = false;
    });
    this.checkAll = false;
    order = this._setUpData(order);
    order.p_data.detailed = true;
    this.selectedOrders = [order];
    this.slider.toggleLiteMode(true);
    this.orderTable.toggleLiteMode(true);
    this.setupDropdownActions(true, this.selectedOrders);
  }

  private _setUpData(order: Order) {
    return this.ordersController.setupDataOrder(order);
  }

  isCancelledOrder(order: Order): boolean {
    return this.ordersController.isCancelledOrder(order);
  }

  hasFulfillment(order: Order): boolean {
    if(order.activeFulfillment) {
      const {status, shipping_code} = order.activeFulfillment;
      return status != 'N' && !!shipping_code;
    }
    return false;
  }

  undoDelivery() {
    this.ordersController.createShipmentFfm = false;
    this.ordersController.createShipnowFfm = false;
    this.orderList.forEach(o => (o.p_data.un_updatable = false));
  }

  exportOrder() {
    let modal = this.modalController.create({
      component: ExportOrderModalComponent,
      showBackdrop: 'static'
    });
    modal.show();
  }

  // NOTE: import order will shut down all other sliders (detail order, multi orders)
  importOrder() {
    this.checkAll = false;
    this.ordersController.createShipmentFfm = false;
    this.ordersController.createShipnowFfm = false;
    this.toImportOrder = true;
    this.orderList.forEach(o => {
      o.p_data.selected = false;
      o.p_data.detailed = false;
    });
    this.selectedOrders = [];
    this.slider.toggleLiteMode(true);
    this.orderTable.toggleLiteMode(true);
  }

  completeOrder() {
    const order = this.selectedOrders[0];
    const modal = this.dialogController.createConfirmDialog({
      title: `Hoàn thành đơn hàng #${order.code}`,
      body: `
        <div>Bạn có chắc muốn hoàn thành đơn hàng <strong>#${order.code}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.orderApi.completeOrder(order.id);
          toastr.success('Hoàn thành đơn hàng thành công.');
          await this.reloadOrders();
          this.setupDropdownActions(true, this.selectedOrders);
          modal.close();
        } catch (e) {
          debug.error('ERROR in confirming Order', e);
          toastr.error(`Hoàn thành đơn hàng không thành công.`, e.message || e.msg);
        }
      }
    });
    modal.show();
  }

  cancelOrder(single: boolean) {
    if (this.modal) { return; }
    const availableOrders = this.selectedOrders.filter(o => ['Z', 'S', 'P'].includes(o.status));
    const title = single ? `đơn hàng #${this.selectedOrders[0].code}` : `${availableOrders.length} đơn hàng`;
    this.modal = this.modalController.create({
      component: CancelObjectComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        title,
        subtitle: availableOrders.map(o => `#${o.code}`).join(', '),
        reasons: CANCEL_ORDER_REASONS,
        multiple: !single,
        cancelFn: async (reason: string) => {
          try {
            let success = 0;
            let failed = 0;
            let errorMessage: string;
            const promises = availableOrders.map(o => async () => {
              try {
                await this.orderApi.cancelOrder(o.id, reason, 'confirm');
                success += 1;
              } catch (e) {
                errorMessage = e.message;
                failed += 1;
              }
            });
            await this.promiseQueue.run(promises, 10);
            this.sendTelegramMessage(reason, single);
            if (success == this.selectedOrders.length) {
              toastr.success(`Hủy thành công ${success} đơn hàng`);
              this.changeTab('detail_info')
            }
            if (success && success < this.selectedOrders.length) {
              toastr.warning(
                `Huỷ thành công ${success}/${this.selectedOrders.length} đơn hàng`
              );
              this.changeTab('detail_info')
            }
            if (failed == this.selectedOrders.length) {
              toastr.error(
                `Hủy thất bại ${this.selectedOrders.length} đơn hàng. ${errorMessage}`
              );
              this.changeTab('detail_info')
            }
          } catch(e) {
            toastr.error('Huỷ đơn hàng không thành công.', e.code && (e.message || e.msg) || '');
            throw e;
          }
        }
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async() => {
      this.modal = null;
      await this.ordersController.reloadOrderList();
      this.setupDropdownActions(single, this.selectedOrders);
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  printOrder() {
    this.printController.print(PrintType.orders, {
      orders: this.selectedOrders
    });
  }

  printFulfillment() {
    this.printController.print(PrintType.fulfillments, {
      ffms: [this.selectedOrders[0].activeFulfillment]
    });
  }

  printOrderAndFfm() {
    this.printController.print(PrintType.o_and_f, {
      orders: this.selectedOrders
    });
  }

  gotoPOS() {
    let win = window.open(`/s/${this.util.getSlug()}/pos`, '_blank');
    win.focus();
  }

  noSupportRequest(tab) {
    const order = this.selectedOrders[0];
    const ffm = order.activeFulfillment;
    return tab.value == 'support_request' && (
      order.fulfillment_type == 'shipnow' ||
      (ffm && !ffm.from_topship)
    );
  }

  sendTelegramMessage(reason, single) {
    if (single) {
      if (!this.selectedOrders[0]?.activeFulfillment) {
        this.telegram.sendCancelSingleOrderMessage(this.selectedOrders[0], reason);
        return;
      }
      this.telegram.sendOrderTelegramMessage(this.selectedOrders[0], 'cancel', reason);
      return;
    }
    let totalItem = 0, totalBasketValue = 0, totalFee = 0 , totalDiscount = 0, totalAmount = 0;
    this.selectedOrders.forEach(o => {
      totalItem += o.total_items;
      totalBasketValue += o.basket_value;
      totalFee += o.total_fee;
      totalDiscount += o.total_discount;
      totalAmount += o.total_amount;
    })
    this.telegram.sendCancelMultiOrderMessage(
      this.selectedOrders.length,
      totalItem,
      totalBasketValue,
      totalFee,
      totalDiscount,
      totalAmount,
      this.auth.snapshot.shop.code,
      this.auth.snapshot.shop.name);
  }
}
