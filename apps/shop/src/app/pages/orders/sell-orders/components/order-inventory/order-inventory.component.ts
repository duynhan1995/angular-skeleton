import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { InventoryApi } from '@etop/api';
import { InventoryVoucher, Order } from '@etop/models';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';

@Component({
  selector: 'shop-order-inventory',
  templateUrl: './order-inventory.component.html',
  styleUrls: ['./order-inventory.component.scss']
})
export class OrderInventoryComponent implements OnInit, OnChanges {
  @Input() order = new Order({});
  inInventoryVouchers : InventoryVoucher[];
  outInventoryVouchers : InventoryVoucher[];
  inventoryVouchers : InventoryVoucher[];
  loading = false;
  constructor(
    private traderStore: TraderStoreService,
    private inventoryStore: InventoryStoreService,
    private inventoryApi: InventoryApi) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.detectInventoryVouchers();
  }

  async detectInventoryVouchers () {
    this.loading = true;
    try{
      this.inventoryVouchers = await this.inventoryApi.getInventoryVouchersByReference(this.order.id,'order');
      this.inInventoryVouchers = this.inventoryVouchers.filter(inventoryVoucher => inventoryVoucher.type == 'in');
      this.outInventoryVouchers = this.inventoryVouchers.filter(inventoryVoucher => inventoryVoucher.type == 'out');
    }
    catch (e) {
      debug.log(e.message);
    }
    this.loading = false;
  }

  viewDetailVoucher(type: string, voucher_code: string) {
    this.inventoryStore.navigate(type, voucher_code);
  }

  viewDetailCustomer() {
    const customer = this.order.customer;
    if (!this.order.customer_id) { return; }
    if (customer && customer.deleted) { return; }
    this.traderStore.navigate('customer', null, this.order.customer_id);
  }
}
