import {Component, OnInit} from '@angular/core';
import {
  MultiShipmentStore,
  SortShipmentServiceOrders, SortShipmentServiceType
} from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/multi-shipment.store';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {Connection} from "libs/models/Connection";
import {FormBuilder} from "@angular/forms";
import {BaseComponent} from "@etop/core";

@Component({
  selector: 'shop-select-shipping-service-box',
  templateUrl: './select-shipping-service-box.component.html',
  styleUrls: ['./select-shipping-service-box.component.scss']
})
export class SelectShippingServiceBoxComponent extends BaseComponent implements OnInit {

  sortShipmentServiceOrders: SortShipmentServiceOrders = [];

  tabs = [
    {
      name: 'Theo NVC',
      value: 'connection'
    },
    {
      name: 'Theo gói dịch vụ',
      value: 'service'
    }
  ];
  selectedTab = 'connection';

  connectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections?.filter(conn => conn.connection_subtype == 'shipment')),
    distinctUntilChanged((prev, next) => prev?.length == next?.length)
  );

  autoSelectShipmentServiceForm = this.fb.group({
    connectionId: '',
    serviceSortType: 'fee'
  });

  private static specialConnectionNameDisplay(connection: Connection) {
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }


  constructor(
    private fb: FormBuilder,
    private moFastStore: MultiShipmentStore,
    private connectionStore: ConnectionStore
  ) {
    super();
  }

  public displayMap = option => option && SelectShippingServiceBoxComponent.specialConnectionNameDisplay(option) || null;
  public valueMap = option => option && option.id || null;


  ngOnInit() {
    this.autoSelectShipmentServiceForm.valueChanges.subscribe(form => {
      switch (this.selectedTab) {
        case 'connection':
          this.sortShipmentServiceOrders = [
            { sortType: SortShipmentServiceType.connectionID, value: form.connectionId }
          ];
          if (form.serviceSortType == 'fee') {
            this.sortShipmentServiceOrders.push(
              { sortType: SortShipmentServiceType.minFee, value: null },
              { sortType: SortShipmentServiceType.minDeliveryTime, value: null }
            );
          } else {
            this.sortShipmentServiceOrders.push(
              { sortType: SortShipmentServiceType.minDeliveryTime, value: null },
              { sortType: SortShipmentServiceType.minFee, value: null }
            );
          }
          break;
        case 'service':
          if (form.serviceSortType == 'fee') {
            this.sortShipmentServiceOrders = [
              { sortType: SortShipmentServiceType.minFee, value: null },
              { sortType: SortShipmentServiceType.connectionID, value: form.connectionId },
              { sortType: SortShipmentServiceType.minDeliveryTime, value: null }
            ];
          } else {
            this.sortShipmentServiceOrders = [
              { sortType: SortShipmentServiceType.minDeliveryTime, value: null },
              { sortType: SortShipmentServiceType.connectionID, value: form.connectionId },
              { sortType: SortShipmentServiceType.minFee, value: null }
            ];
          }
          break;
      }
      this.moFastStore.updateSortShipmentServiceOrders(this.sortShipmentServiceOrders);
    });

    this.autoSelectShipmentServiceForm.setValue({
      connectionId: this.connectionStore.snapshot.initConnections[0].id,
      serviceSortType: 'fee'
    });
    this.connectionsList$.pipe(takeUntil(this.destroy$))
      .subscribe(connections => {
        this.autoSelectShipmentServiceForm.setValue({
          connectionId: connections[0].id,
          serviceSortType: 'fee'
        });
      });

  }

  selectTab(tab) {
    this.selectedTab = tab.value;
    this.autoSelectShipmentServiceForm.setValue({
      connectionId: this.connectionStore.snapshot.initConnections[0].id,
      serviceSortType: 'fee'
    });
  }


}
