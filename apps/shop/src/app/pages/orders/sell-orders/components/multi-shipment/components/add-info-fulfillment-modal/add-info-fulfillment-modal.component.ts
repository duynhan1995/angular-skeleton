import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Order } from 'libs/models/Order';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { SelectAndUpdateCustomerAddressComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/select-and-update-customer-address/select-and-update-customer-address.component';
import { CustomerAddressModalComponent } from 'apps/shop/src/app/pages/traders/customers/components/single-customer-edit-form/components/customer-address-modal/customer-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { OrderService } from 'apps/shop/src/services/order.service';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {LocationQuery} from "@etop/state/location";

@Component({
  selector: 'shop-add-info-fulfillment',
  templateUrl: './add-info-fulfillment-modal.component.html',
  styleUrls: ['./add-info-fulfillment-modal.component.scss']
})
export class AddInfoFulfillmentModalComponent implements OnInit {
  @Input() order = new Order({});

  error = {
    cod: false,
    weight: false
  };

  constructor(
    private modalController: ModalController,
    private modalAction: ModalAction,
    private changeDetector: ChangeDetectorRef,
    private ordersController: SellOrdersController,
    private orderService: OrderService,
    private locationCompact: LocationCompactPipe,
    private locationQuery: LocationQuery,
  ) { }

  get uni() {
    return this.ordersController.uni;
  }

  get customerHasAddresses() {
    return this.order.customer.addresses && this.order.customer.addresses.length;
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

  get addressDisplay() {
    const shipping = this.order.p_data.shipping;
    if (!shipping.shipping_address) {
      return '';
    }
    const shipping_address = shipping.shipping_address;
    const address1 = this.locationCompact.transform(shipping_address.address1);
    const ward = shipping_address.ward && this.locationCompact.transform(shipping_address.ward);
    const district = this.locationCompact.transform(shipping_address.district);
    const province = this.locationCompact.transform(shipping_address.province);

    return `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
  }

  ngOnInit() {
    const shipping = this.order.p_data.shipping;
    this.error = {
      cod: shipping.cod_amount > 0 && shipping.cod_amount < 5000,
      weight: shipping.chargeable_weight < 50
    };

  }

  onChangeCOD() {
    const cod = this.order.p_data.shipping.cod_amount;
    this.error.cod = cod > 0 && cod < 5000;
    if (this.error.cod) {
      toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
    }
  }

  onChangeWeight() {
    const weight = this.order.p_data.shipping.chargeable_weight;
    this.error.weight = weight < 50;
    if (this.error.weight) {
      toastr.error('Vui lòng nhập khối lượng từ 50g trở lên!');
    }
  }

  editShippingAddress() {
    this.closeModal();
    this.openModalAddress();
  }

  openModalAddress() {
    const component = this.customerHasAddresses &&
      SelectAndUpdateCustomerAddressComponent ||
      CustomerAddressModalComponent;

    const shipping_address = this.order.p_data.shipping.shipping_address;
    const componentProps = this.customerHasAddresses && {
      customer: {...this.order.customer},
      addresses: JSON.parse(JSON.stringify(this.order.customer.addresses)),
      currentAddress: shipping_address
    } || {
      address: shipping_address,
      confirmBtnClass: 'btn-topship'
    };

    const cssClass = this.customerHasAddresses &&
      'modal-lg' || 'modal-md';

    const modal = this.modalController.create({
      component,
      showBackdrop: 'static',
      cssClass,
      componentProps
    });
    modal.show().then();
    modal.onDismiss().then(address => {
      this.modalAction.show();
      if (!address) { return; }
      address = {
        ...address,
        province: this.locationQuery.getProvince(address.province_code)?.name,
        district: this.locationQuery.getDistrict(address.district_code)?.name,
        ward: address.ward_code && this.locationQuery.getWard(address.ward_code)?.name
      };
      if (this.order.customer_id) {
        if (this.customerHasAddresses) {
          this.order.customer.addresses.unshift(address);
        } else {
          this.order.customer.addresses = [address];
        }
      }
      this.order.p_data.shipping.shipping_address = address;
    });
  }

  closeModal() {
    this.modalAction.close(false);
  }

  confirm() {
    if (this.error.cod) {
      return toastr.error('Vui lòng nhập tiền thu hộ bằng 0 hoặc từ 5.000đ!');
    }
    if (this.error.weight) {
      return toastr.error('Vui lòng nhập khối lượng từ 50g trở lên!');
    }
    this.modalAction.dismiss(this.order);
  }

}
