import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Order } from '@etop/models';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { CreateReceiptModalComponent } from 'apps/shop/src/app/components/modals/create-receipt-modal/create-receipt-modal.component';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { OrderReceiptsComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/order-receipts/order-receipts.component';
import { CustomerService } from 'apps/shop/src/services/customer.service';

@Component({
  selector: 'shop-payment-info',
  templateUrl: './payment-info.component.html',
  styleUrls: ['./payment-info.component.scss']
})
export class PaymentInfoComponent implements OnInit, OnChanges {
  @ViewChild('orderReceipts', { static: true }) orderReceipts: OrderReceiptsComponent;
  @Input() order = new Order({});
  total_amount: number;
  private modal: any;

  constructor(
    private modalController: ModalController,
    private ordersController: SellOrdersController,
    private customerService: CustomerService,
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  get isPaymentDone() {
    // TODO: temporarily doing this!!! wait for VALID order.payment_status
    return this.order.received_amount >= this.order.total_amount;
  }

  get codGreaterThanTotalAmount() {
    const ffm = this.order.activeFulfillment;
    return ffm && ffm.cod_amount >= this.order.total_amount && ffm.status != 'N';
  }

  get cannotCreateReceipt() {
    return this.isPaymentDone || this.codGreaterThanTotalAmount || this.order.status == 'N';
  }

  get disabledTooltip() {
    if (this.codGreaterThanTotalAmount) {
      return 'Tiền thu hộ không nhỏ hơn số tiền khách phải trả, không thể tạo phiếu thu!';
    }
    if (this.isPaymentDone) {
      return 'Đơn hàng này đã thanh toán đủ, không thể tạo phiếu thu!';
    }
    if (this.order.status == 'N') {
      return 'Đơn hàng đã huỷ, không thể tạo phiếu thu!';
    }
    return '';
  }

  openCreateReceiptModal() {
    if (this.modal) { return; }
    if (!this.order.customer_id) {
      this.order.customer_id = this.customerService.independent_customer.id;
    }
    this.modal = this.modalController.create({
      component: CreateReceiptModalComponent,
      cssClass: 'modal-lg',
      componentProps: {
        trader_id: this.order.customer_id,
        type: 'receipt',
        amount: this.order.total_amount - this.order.received_amount,
        ref: this.order
      }
    });

    this.modal.show();
    this.modal.onDismiss().then(async () => {
      this.modal = null;
      await this.ordersController.reloadOrderList();
      await this.orderReceipts.getReceipts();
    });

    this.modal.onClosed().then(_ => {
      this.modal = null;
    });

  }

}
