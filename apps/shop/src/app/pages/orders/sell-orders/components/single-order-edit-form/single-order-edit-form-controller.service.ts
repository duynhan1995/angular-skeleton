import { Injectable } from '@angular/core';
import { Order } from 'libs/models/Order';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';

@Injectable()
export class SingleOrderEditFormControllerService {
  order = new Order({});

  constructor(private ordersController: SellOrdersController) { }

  async updateOrderData() {
    await this.ordersController.reloadOrderList();
  }

}
