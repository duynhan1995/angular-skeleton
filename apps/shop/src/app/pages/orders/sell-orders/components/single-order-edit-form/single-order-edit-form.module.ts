import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LineInfoEditModule } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/line-info-edit/line-info-edit.module';
import { OrderInfoEditComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/order-info-edit/order-info-edit.component';
import { SingleOrderEditFormComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form.component';
import { DeliveryModule } from 'apps/shop/src/app/components/delivery/delivery.module';
import { SingleOrderEditFormControllerService } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form-controller.service';
import { PaymentInfoModule } from 'apps/shop/src/app/pages/orders/sell-orders/components/payment-info/payment-info.module';
import { AuthenticateModule } from '@etop/core';
import { OrderFulfillmentsModule } from 'apps/shop/src/app/pages/orders/sell-orders/components/order-fulfillments/order-fulfillments.module';
import { InventoryInfoModule } from '../inventory-info/inventory-info.module';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import { OrderInventoryModule } from '../order-inventory/order-inventory.module';
import { FulfillmentHistoriesModule } from 'apps/shop/src/app/components/fulfillment-histories/fulfillment-histories.module';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [OrderInfoEditComponent, SingleOrderEditFormComponent],
  exports: [
    OrderInfoEditComponent,
    SingleOrderEditFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule,
    LineInfoEditModule,
    DeliveryModule,
    PaymentInfoModule,
    AuthenticateModule,
    OrderFulfillmentsModule,
    InventoryInfoModule,
    EtopMaterialModule,
    EtopPipesModule,
    OrderInventoryModule,
    FulfillmentHistoriesModule,
    NgbModule,
    MaterialModule
  ],
  providers: [
    SingleOrderEditFormControllerService
  ]
})
export class SingleOrderEditFormModule { }
