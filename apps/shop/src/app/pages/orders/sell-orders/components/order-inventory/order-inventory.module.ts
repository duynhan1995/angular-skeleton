import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderInventoryComponent } from './order-inventory.component';
import { EtopPipesModule } from '@etop/shared';



@NgModule({
  declarations: [OrderInventoryComponent],
  imports: [
    CommonModule,
    EtopPipesModule
  ],
  exports: [
    OrderInventoryComponent
  ]
})
export class OrderInventoryModule { }
