import { Injectable } from '@angular/core';
import { Order } from 'libs/models/Order';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { distinctUntilChanged, map } from 'rxjs/operators';

export enum SortShipmentServiceType {
  connectionID = 'connectionID',
  minFee = 'minFee',
  minDeliveryTime = 'minDeliveryTime'
}

export interface SortShipmentServiceOrder {
  sortType: SortShipmentServiceType;
  value: string;
}

export type SortShipmentServiceOrders = SortShipmentServiceOrder[];

export interface MoDeliveryFastData {
  selected_orders: Order[];
  sortShipmentServiceOrders: SortShipmentServiceOrders;
}

@Injectable()
export class MultiShipmentStore extends AStore<MoDeliveryFastData> {
  initState = {
    selected_orders: [],
    sortShipmentServiceOrders: []
  };

  readonly selectedOrdersUpdated$ = this.state$.pipe(
    map(({selected_orders}) => selected_orders),
    distinctUntilChanged((prev, curr) => {
      return JSON.stringify(prev) == JSON.stringify(curr);
    })
  );

  constructor() {
    super();
  }

  updateSortShipmentServiceOrders(orders: SortShipmentServiceOrders) {
    this.setState({sortShipmentServiceOrders: orders});
  }

  updateSingleOrderItem(order: Order) {
    const selected_orders = this.snapshot.selected_orders;
    const orderIndex = selected_orders.findIndex(o => o.id == order.id);
    if (orderIndex != -1) {
      selected_orders[orderIndex] = order;
    }
    this.updateSelectedOrders(selected_orders);
  }

  updateSelectedOrders(orders: Order[]) {
    this.setState({ selected_orders: orders });
  }

}
