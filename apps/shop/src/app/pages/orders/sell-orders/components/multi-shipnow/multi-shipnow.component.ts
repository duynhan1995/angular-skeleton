import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Order } from 'libs/models/Order';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { OrderService } from 'apps/shop/src/services/order.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { MultiShipnowController } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/multi-shipnow.controller';
import { GoogleMapService } from 'apps/shop/src/services/google-map.service';
import {Address, OrderAddress} from 'libs/models/Address';
import { MultiShipnowControlPanelComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/components/multi-shipnow-control-panel/multi-shipnow-control-panel.component';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { TelegramService } from '@etop/features';
import { takeUntil } from 'rxjs/operators';
import {FulfillmentShipnowService, ShipnowDeliveryPoint} from 'libs/models/Fulfillment';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import {FulfillmentAPI} from "@etop/api";

@Component({
  selector: 'shop-multi-shipnow',
  templateUrl: './multi-shipnow.component.html',
  styleUrls: ['./multi-shipnow.component.scss']
})
export class MultiShipnowComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('multiOrderShippingNow', {static: false}) multiOrderShippingNow: MultiShipnowControlPanelComponent;

  @Input() orders: Array<Order> = [];
  @Input() scroll_id: string;

  gettingServices = false;
  creatingFfm = false;

  constructor(
    private ordersController: SellOrdersController,
    private modalController: ModalController,
    private orderService: OrderService,
    private promiseQueue: PromiseQueueService,
    private auth: AuthenticateStore,
    private dNowController: MultiShipnowController,
    private googleMap: GoogleMapService,
    private ffmService: FulfillmentService,
    private telegram: TelegramService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  get totalCodAmount() {
    return this.orders.reduce((a,b) => {
      if (this.ordersController.orderToBeUpdated(b) && this.isValidAddress(b.p_data.shipping.shipping_address)) {
        return a + Number(b.p_data.shipping.cod_amount);
      }
      return a;
    }, 0) || 0;
  }

  get totalOrders() {
    return this.orders.filter(o =>
      this.ordersController.orderToBeUpdated(o) && this.isValidAddress(o.p_data.shipping.shipping_address)
    ).length;
  }

  get hasValidOrders() {
    return this.orders.some(o =>
      this.ordersController.orderToBeUpdated(o) && this.isValidAddress(o.p_data.shipping.shipping_address)
    );
  }

  ngOnInit() {
    this.ordersController.removeMapMarkers$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.toRenderMap();
      });
    this.ordersController.onUpdateShippingData$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async() => {
        for (let o of this.orders) {
          if (this.ordersController.orderToBeUpdated(o)) {
            await this.getCoorsForAddress(o.p_data.shipping.shipping_address);
          }
        }
        this.toRenderMap();
      });
  }

  ngOnDestroy() {
    this.ordersController.createShipmentFfm = false;
    this.ordersController.createShipnowFfm = false;
    this.ffmStore.discardCreatingFulfillment();
  }

  async getCoorsForAddress(address: OrderAddress) {
    if (!address) {
      return null;
    }
    const { address1, ward, district, province } = address;
    const address_fulltext = `${address1}, ${ward ? ward + ', ' : ''}${district}, ${province}`;
    const result: any = await this.googleMap.getLatLngFromAddress(address_fulltext);
    const { lat, lng } = result;
    address.coordinates = {
      latitude: lat,
      longitude: lng
    };
    return address;
  }

  noAddress(order: Order): boolean {
    return this.ordersController.noAddress(order);
  }

  selectedOrder(order: Order): boolean {
    return this.ordersController.isSelectedOrder(order);
  }

  canCreateFulfillment(order: Order) {
    return SellOrdersController.canCreateFulfillment(order);
  }

  async reCreateFFM(order: Order) {
    order.activeFulfillment = null;
    order.p_data.un_updatable = false;
    this.getServices();
  }

  getServices() {
    this.multiOrderShippingNow.getServices();
  }

  onFailedCreatingFFM() {
    const _orders = this.orders.filter(o =>
      this.ordersController.orderToBeUpdated(o) && this.isValidAddress(o.p_data.shipping.shipping_address));
    this.dNowController.onFailedCreatingFFM$.next(_orders);
  }

  async createFFM(data: {selectedService: FulfillmentShipnowService, orders: Order[]}) {
    this.creatingFfm = true;
    try {
      const {selectedService, orders} = data;

      const delivery_points: ShipnowDeliveryPoint[] = orders.map(o => {
        return {
          chargeable_weight: 1000,
          cod_amount: o.p_data.shipping.cod_amount,
          order_id: o.id,
          shipping_address: o.p_data.shipping.shipping_address
        };
      });
      const body: FulfillmentAPI.CreateShipnowFulfillmentRequest = {
        delivery_points,
        carrier: selectedService && selectedService.carrier || null,
        shipping_service_code: selectedService && selectedService.code || null,
        shipping_service_fee: selectedService && selectedService.fee || null,
        connection_id: selectedService && selectedService.connection_info.id || null,
        shipping_note: '',
        pickup_address: this.ordersController.shipping_data.pickup_address,
        coupon: this.ordersController.shipping_data.coupon
      };
      if (!body.carrier) {
        toastr.error('Vui lòng chọn gói vận chuyển!');
        return this.creatingFfm = false;
      }
      const shipnow_ffm = await this.ffmService.createShipnowFulfillment(body);
      await this.confirmShipnowFFM(shipnow_ffm.id);

      toastr.success('Tạo đơn giao tức thì thành công.');

      this.dNowController.onSuccessCreatingFFM$.next();
    } catch(e) {
      debug.error("ERROR in createShipnowFFM", e);

      toastr.error('Tạo đơn giao tức thì không thành công.', (e.message || e.msg) + (e.meta?.description && ` ${e.meta?.description}`));

      this.onFailedCreatingFFM();
    }
    this.creatingFfm = false;
  }

  async confirmShipnowFFM(shipnow_id: string) {
    try {
      const message_data = await this.ffmService.confirmShipnowFulfillment(shipnow_id);

      this.telegram.shipnowConfirm(message_data).then();
      this.ordersController.reloadOrderList().then();
    } catch(e) {
      this.ffmService.cancelShipnowFulfillment(shipnow_id, 'Xác nhận đơn không thành công.').then();
      debug.error("ERROR in confirmShipnowFFM", e);
      throw e;
    }
  }

  toRenderMap() {
    const _orders = this.orders.filter(o =>
      this.ordersController.orderToBeUpdated(o) && this.isValidAddress(o.p_data.shipping.shipping_address));
    this.dNowController.onRenderingMap$.next(_orders);
  }

  isLocatedAddress(address: Address) {
    if (!address) { return false; }

    // must have both latitude and longitude
    const coors = address.coordinates;
    return coors && coors.latitude && coors.longitude;
  }

  isSupportedAddress(address: Address) {
    if (!address) { return false; }
    // must be in SG or HN
    if (!GoogleMapService.allowedProvinces.includes(address.province_code)) {
      return false;
    }
    const pickup_address = this.ordersController.shipping_data.pickup_address;
    // must be in the same province with pickup_address
    return address.province_code == pickup_address.province_code;

  }

  isValidAddress(address: Address) {
    return this.isLocatedAddress(address) && this.isSupportedAddress(address);
  }

}
