import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultiShipnowComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/multi-shipnow.component';
import { MultiShipnowController } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/multi-shipnow.controller';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiShipnowControlPanelComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/components/multi-shipnow-control-panel/multi-shipnow-control-panel.component';
import { DeliveryModule } from 'apps/shop/src/app/components/delivery/delivery.module';
import { DragdropListModule } from 'apps/shared/src/components/dragdrop-list/dragdrop-list.module';
import { MultiShipnowRowComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/components/multi-shipnow-row/multi-shipnow-row.component';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {ShipnowVerifyWarningModule} from "apps/shop/src/app/components/shipnow-verify-warning/shipnow-verify-warning.module";

@NgModule({
  declarations: [
    MultiShipnowComponent,
    MultiShipnowControlPanelComponent,
    MultiShipnowRowComponent
  ],
  exports: [
    MultiShipnowComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        NgbModule,
        DeliveryModule,
        DragdropListModule,
        EtopMaterialModule,
        EtopPipesModule,
        MaterialModule,
        ShipnowVerifyWarningModule,
    ],
  providers: [
    MultiShipnowController
  ]
})
export class MultiShipnowModule { }
