import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderInfoComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-detail/order-info/order-info.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { LineInfoComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-detail/line-info/line-info.component';
import { ShippingInfoComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-detail/shipping-info/shipping-info.component';
import { RouterModule } from '@angular/router';
import { SingleOrderDetailComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-detail/single-order-detail.component';
import { ForceButtonModule } from 'apps/shop/src/app/components/force-button/force-button.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TicketModule } from 'apps/shop/src/app/components/ticket/ticket.module';
import { FulfillmentHistoriesModule } from 'apps/shop/src/app/components/fulfillment-histories/fulfillment-histories.module';
import { PaymentInfoModule } from 'apps/shop/src/app/pages/orders/sell-orders/components/payment-info/payment-info.module';
import { AuthenticateModule } from '@etop/core';
import { CancelObjectModule } from 'apps/shop/src/app/components/cancel-object/cancel-object.module';
import { OrderFulfillmentsModule } from 'apps/shop/src/app/pages/orders/sell-orders/components/order-fulfillments/order-fulfillments.module';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { InventoryInfoModule } from '../inventory-info/inventory-info.module';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { OrderInventoryModule } from '../order-inventory/order-inventory.module';

@NgModule({
  declarations: [
    SingleOrderDetailComponent,
    OrderInfoComponent,
    LineInfoComponent,
    ShippingInfoComponent,
  ],
  exports: [
    SingleOrderDetailComponent,
    OrderInfoComponent,
    LineInfoComponent,
    ShippingInfoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule,
    ForceButtonModule,
    TicketModule,
    NgbModule,
    FulfillmentHistoriesModule,
    PaymentInfoModule,
    AuthenticateModule,
    CancelObjectModule,
    OrderFulfillmentsModule,
    DropdownActionsModule,
    InventoryInfoModule,
    EtopMaterialModule,
    EtopPipesModule,
    OrderInventoryModule
  ]
})
export class SingleOrderDetailModule { }
