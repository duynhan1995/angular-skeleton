import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import {
  PrintType,
  SellOrdersController
} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { UserService } from 'apps/core/src/services/user.service';
import { ExportFulfillmentModalComponent } from 'apps/shop/src/app/components/modals/export-fulfillment-modal/export-fulfillment-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ExportOrderModalComponent } from 'apps/shop/src/app/components/modals/export-order-modal/export-order-modal.component';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {map} from "rxjs/operators";
import { PrintControllerService } from 'apps/shop/src/app/components/print/print.controller';

@Component({
  selector: 'shop-multiple-order-actions',
  templateUrl: './multiple-order-actions.component.html',
  styleUrls: ['./multiple-order-actions.component.scss']
})
export class MultipleOrderActionsComponent implements OnInit, OnChanges {
  @Input() orders: Array<Order> = [];

  shipmentConnectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections.filter(conn => conn.connection_subtype == 'shipment'))
  );
  shipnowConnectionsList$ = this.connectionStore.state$.pipe(
    map(s => s?.initConnections.filter(conn => conn.connection_subtype == 'shipnow'))
  );
  allNullActiveFfm = false;

  constructor(
    private printController: PrintControllerService,
    private ordersController: SellOrdersController,
    private user: UserService,
    private modalController: ModalController,
    private ffmStore: FulfillmentStore,
    private connectionStore: ConnectionStore
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    this.allNullActiveFfm = this.orders.every(o => !o.activeFulfillment || !o.activeFulfillment.shipping_code);
  }

  preCheckOrdersCondition() {
    const _orders = this.orders.filter(
      o => this.ordersController.isSelectedOrder(o) && SellOrdersController.canCreateFulfillment(o)
    );
    if (!_orders.length) {
      throw new Error('pre conditioned failed');
    }
    this.orders.forEach(o => {
      if (!this.ordersController.isSelectedOrder(o) || !SellOrdersController.canCreateFulfillment(o)) {
        o.p_data.un_updatable = true;
      }
    });
  }

  createShipmentFfm() {
    try {
      this.ffmStore.createFulfillment();
      this.preCheckOrdersCondition();
      this.ordersController.createShipmentFfm = true;
    } catch (e) {
      if (e.message == 'pre conditioned failed') {
        toastr.warning(
          'Bạn không có đơn hàng phù hợp để tạo đơn giao nhanh!'
        );
      }
      debug.error('ERROR in open delivery fase', e);
    }
  }

  async createShipnowFfm() {
    try {
      this.ffmStore.createFulfillment();
      this.preCheckOrdersCondition();
      this.ordersController.createShipnowFfm = true;
    } catch (e) {
      if (e.message == 'pre conditioned failed') {
        toastr.warning(
          'Bạn không có đơn hàng phù hợp để tạo đơn giao tức thì!'
        );
      }
      debug.error('ERROR in open delivery now', e);
    }
  }

  printOrders() {
    this.printController.print(PrintType.orders, { orders: this.orders });
  }

  printFulfillments() {
    let ffms = []
    this.orders.forEach(order => {
      if(order.activeFulfillment) {
        ffms.push(order.activeFulfillment)
      }
    });
    this.printController.print(PrintType.fulfillments, { ffms: ffms });
  }

  printOrderAndFfm() {
    this.printController.print(PrintType.o_and_f, { orders: this.orders });
  }

  exportOrder() {
    let modal = this.modalController.create({
      component: ExportOrderModalComponent,
      componentProps: {
        ids: this.orders.map(o => o.id)
      },
      showBackdrop: 'static'
    });
    modal.show();
  }

  exportFFM() {
    let modal = this.modalController.create({
      component: ExportFulfillmentModalComponent,
      componentProps: {
        ids: this.orders
          .filter(o => o.activeFulfillment)
          .map(o => o.activeFulfillment.id)
      },
      showBackdrop: 'static'
    });
    modal.show();
  }
}
