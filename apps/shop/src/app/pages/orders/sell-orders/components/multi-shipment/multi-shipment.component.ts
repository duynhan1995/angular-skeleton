import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {SellOrdersController} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {Order} from 'libs/models/Order';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {OrderService} from 'apps/shop/src/services/order.service';
import {PromiseQueueService} from 'apps/core/src/services/promise-queue.service';
import {VND} from 'libs/shared/pipes/etop.pipe';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {map, takeUntil} from 'rxjs/operators';
import {FulfillmentApi, OrderApi} from '@etop/api';
import {FulfillmentService} from 'apps/shop/src/services/fulfillment.service';
import {FulfillmentStore} from 'apps/core/src/stores/fulfillment.store';
import {
  MultiShipmentStore,
  SortShipmentServiceOrders,
  SortShipmentServiceType
} from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/multi-shipment.store';
import {FulfillmentShipmentService} from "libs/models/Fulfillment";

@Component({
  selector: 'shop-multi-shipment',
  templateUrl: './multi-shipment.component.html',
  styleUrls: ['./multi-shipment.component.scss']
})
export class MultiShipmentComponent extends BaseComponent implements OnInit, OnDestroy, OnChanges {
  @Input() orders: Array<Order> = [];
  @Input() scroll_id: string;

  getting_services = false;
  creating_ffm = false;

  getServicesIndex = 0;
  notHavingShippingServices = 0;

  display_progress_bar = false;
  interval: any;
  progress_percent = 0;

  vndTransform = new VND();

  sortShipmentServiceOrders$ = this.moFastStore.state$.pipe(map(s => s?.sortShipmentServiceOrders));
  private static selectSuitableService(sortShipmentServiceOrders: SortShipmentServiceOrders, order) {
    try {
      const services = order.p_data.services;
      if (!services || !services.length) {
        order.p_data.selectedService = {};
        order.p_data.selectedServiceCode = null;
        return;
      }

      let _services: FulfillmentShipmentService[] = [];
      let _filterByConnectionIdServices: FulfillmentShipmentService[] = [];

      switch (sortShipmentServiceOrders[0].sortType) {
        case SortShipmentServiceType.connectionID:
          _filterByConnectionIdServices = FulfillmentService.autoSelectShipmentServiceByConnectionId(
            sortShipmentServiceOrders[0].value,
            order.p_data.services
          );
          switch (sortShipmentServiceOrders[1].sortType) {
            case SortShipmentServiceType.minFee:
              _services = FulfillmentService.autoSelectShipmentServiceByMinFee(_filterByConnectionIdServices);
              break;
            case SortShipmentServiceType.minDeliveryTime:
              _services = FulfillmentService.autoSelectShipmentServiceByMinDeliveryTime(_filterByConnectionIdServices);
              break;
          }
          order.p_data.selectedService = _services.length ? _services[0] : order.p_data.services[0];
          break;

        case SortShipmentServiceType.minFee:
          _filterByConnectionIdServices = FulfillmentService.autoSelectShipmentServiceByConnectionId(
            sortShipmentServiceOrders[1].value,
            order.p_data.services
          );
          _filterByConnectionIdServices = FulfillmentService.autoSelectShipmentServiceByMinFee(
            _filterByConnectionIdServices
          );
          _services = FulfillmentService.autoSelectShipmentServiceByMinFee(order.p_data.services);

          if (_filterByConnectionIdServices.length) {
            order.p_data.selectedService = _filterByConnectionIdServices[0].fee == _services[0].fee ?
              _filterByConnectionIdServices[0] :
              _services[0];
          } else {
            order.p_data.selectedService = _services[0];
          }

          break;

        case SortShipmentServiceType.minDeliveryTime:
          _filterByConnectionIdServices = FulfillmentService.autoSelectShipmentServiceByConnectionId(
            sortShipmentServiceOrders[1].value,
            order.p_data.services
          );
          _filterByConnectionIdServices = FulfillmentService.autoSelectShipmentServiceByMinDeliveryTime(
            _filterByConnectionIdServices
          );
          _services = FulfillmentService.autoSelectShipmentServiceByMinDeliveryTime(order.p_data.services);

          if (_filterByConnectionIdServices.length) {
            const _filterTimes = new Date(_filterByConnectionIdServices[0].estimated_delivery_at).getTime();
            const _allTimes = new Date(_services[0].estimated_delivery_at).getTime();
            order.p_data.selectedService = _filterTimes == _allTimes ?
              _filterByConnectionIdServices[0] :
              _services[0];
          } else {
            order.p_data.selectedService = _services[0];
          }

          break;
      }

      order.p_data.selectedServiceCode = order.p_data.selectedService?.code;
    } catch(e) {
      debug.error('ERROR in selecting suitable service', e);
      throw e;
    }
  }


  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private ordersController: SellOrdersController,
    private modalController: ModalController,
    private orderService: OrderService,
    private fulfillmentService: FulfillmentService,
    private ffmApi: FulfillmentApi,
    private orderApi: OrderApi,
    private promiseQueue: PromiseQueueService,
    private ffmStore: FulfillmentStore,
    private moFastStore: MultiShipmentStore
  ) {
    super();
  }

  ngOnInit() {
    this.sortShipmentServiceOrders$
      .pipe(takeUntil(this.destroy$))
      .subscribe(orders => {
        this.orders.filter(o => this.ordersController.orderToBeUpdated(o))
          .forEach(o => MultiShipmentComponent.selectSuitableService(orders, o));
      });
    this.ordersController.onRegetShipServices$
      .pipe(takeUntil(this.destroy$))
      .subscribe((order_id: string) => {
        this.getServices(order_id ? order_id : null).then();
      });
    this.moFastStore.selectedOrdersUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(selected_orders => {
        this.orders = selected_orders;
      });

    this.ffmStore.initDefaultWeight();
  }

  ngOnDestroy() {
    this.ordersController.createShipmentFfm = false;
    this.ordersController.createShipnowFfm = false;
    this.ffmStore.discardCreatingFulfillment();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.moFastStore.updateSelectedOrders(this.orders);
  }

  noAddress(order: Order): boolean {
    return this.ordersController.noAddress(order);
  }

  noWeight(order: Order): boolean {
    return this.ordersController.noWeight(order);
  }

  selectedOrder(order: Order): boolean {
    return this.ordersController.isSelectedOrder(order);
  }

  canCreateFulfillment(order: Order) {
    return SellOrdersController.canCreateFulfillment(order);
  }

  reCreateFFM(order: Order) {
    order.activeFulfillment = null;
    order.p_data.un_updatable = false;
    this.getServices(order.id).then();
  }

  async getServices(order_id?: string) {
    this.getting_services = true;
    this.changeDetector.detectChanges();
    try {
      this.getServicesIndex++;
      const promises = this.orders.filter(o => this.ordersController.orderToBeUpdated(o))
        .map(o => async() => {
          if (order_id && o.id != order_id) { return; }
          o.p_data.selectedService = null;

          const pka = o.p_data.shipping.pickup_address;
          const spa = o.p_data.shipping.shipping_address;
          const weight = this.ordersController.uni.chargeable_weight ?
            this.ordersController.shipping_data.chargeable_weight :
            o.p_data.shipping.chargeable_weight;
          if (!pka || !pka.province_code || !pka.district_code) {
            return;
          }
          if (!spa || !spa.province_code || !spa.district_code) {
            return;
          }
          if (!weight) {
            return;
          }

          const index = this.getServicesIndex;
          o.p_data.gettingServices = true;
          this.changeDetector.detectChanges();

          const body = {
            from_province_code: o.p_data.shipping.pickup_address.province_code,
            from_district_code: o.p_data.shipping.pickup_address.district_code,
            from_ward_code: o.p_data.shipping.pickup_address.ward_code,

            to_province_code: o.p_data.shipping.shipping_address.province_code,
            to_district_code: o.p_data.shipping.shipping_address.district_code,
            to_ward_code: o.p_data.shipping.shipping_address.ward_code,

            chargeable_weight: weight,
            total_cod_amount: o.p_data.shipping.cod_amount,
            basket_value:  o.basket_value,
            include_insurance: o.p_data.shipping.include_insurance,
            index
          };

          try {
            const res = await this.ffmApi.getShipmentServices(body);
            if (res.index == index) {
              const carriers = this.fulfillmentService.groupShipmentServicesByConnection(res.services);
              o.p_data.services = [].concat(...carriers.map(c => c.services)).filter(s => s.is_available);

              o.p_data.services.forEach(s => {
                const from_topship = s.connection_info.name.toLowerCase().indexOf('topship') != -1;
                s.display_name = `
  <div class="d-flex align-items-center">
    <div class="carrier-image">
      <img src="${s.provider_logo}" alt="">
      ${from_topship ? `<img src="assets/images/r-topship_256.png" class="topship-logo" alt="">` : ''}
    </div>
    <div class="pl-2" style="line-height: 1.4; white-space: normal;">
      ${s.connection_info.name.toUpperCase()} - ${s.name} (${this.vndTransform.transform(s.fee)}đ)
    </div>
  </div>`;
                s.value = s.code;
              });
              MultiShipmentComponent.selectSuitableService(
                this.moFastStore.snapshot.sortShipmentServiceOrders, o
              );
              o.p_data.gettingServices = false;
              this.changeDetector.detectChanges();
            }
          } catch(e) {
            debug.error(`ERROR in Getting ExternalShippingServices #${o.code}`, e);
            o.p_data.gettingServices = false;
            this.changeDetector.detectChanges();
          }

        });
      this.start();
      await this.promiseQueue.run(promises, 10);
    } catch(e) {
      debug.error('Lấy gói dịch vụ không thành công.', e);
    }
    if (!this.orders.some(o => o.p_data.gettingServices)) {
      this.done();
      this.getting_services = false;
      this.changeDetector.detectChanges();
    }
  }

  preValidateOrderInfo() {
    const _orders = this.orders.filter(o => this.ordersController.orderToBeUpdated(o));
    _orders.forEach(o => {
      if (!o.p_data?.selectedService?.carrier) {
        this.notHavingShippingServices += 1;
      }
    });
    if (this.notHavingShippingServices > 0) {
      const invalid = `${this.notHavingShippingServices}/${_orders.length}`;
      toastr.error(`Chưa chọn gói vận chuyển cho ${invalid} đơn hàng. Vui lòng kiểm tra lại.`);
      return false;
    }
    return true;
  }

  async createFFM() {
    /* TODO:
    * - validate info: phone, COD
    */
    if (!this.preValidateOrderInfo()) {
      this.notHavingShippingServices = 0;
      return;
    }
    this.creating_ffm = true;
    try {
      let success = 0;
      const promises = this.orders.filter(o => this.ordersController.orderToBeUpdated(o))
        .map(o => async() => {
          try {
            if (o.confirm_status == 'Z') {
              await this.orderApi.confirmOrder(o.id, 'confirm');
            }

            const {
              cod_amount,
              shipping_address,
              pickup_address,
              include_insurance
            } = o.p_data.shipping;

            const chargeable_weight = this.ordersController.uni.chargeable_weight ?
              this.ordersController.shipping_data.chargeable_weight :
              o.p_data.shipping.chargeable_weight;

            const try_on = this.ordersController.uni.try_on ?
              this.ordersController.shipping_data.try_on :
              o.p_data.shipping.try_on;

            const shipping_note = this.ordersController.uni.shipping_note ?
              this.ordersController.shipping_data.shipping_note :
              o.p_data.shipping.shipping_note;

            const selectedService = o.p_data.selectedService;

            const body: any = {
              order_id: o.id,
              shipping_type: 'shipment',
              cod_amount,
              include_insurance,
              chargeable_weight,
              try_on,
              shipping_note,
              shipping_address,
              pickup_address,
              shipping_service_name: selectedService && selectedService.name || null,
              shipping_service_code: selectedService && selectedService.code || null,
              shipping_service_fee: selectedService && selectedService.fee || null,
              connection_id: (selectedService && selectedService.connection_info)
                && selectedService.connection_info.id || null,
            };

            await this.ffmApi.createShipmentFulfillment(body);
            o.p_data.un_updatable = true;
            success += 1;
          } catch(e) {
            debug.error(`ERROR in Updating Order #${o.code}`, e);
          }
        });
      await this.promiseQueue.run(promises, 10);

      const orders = this.moFastStore.snapshot.selected_orders;
      const totalOrders = orders.filter(o => this.ordersController.orderToBeUpdated(o)).length;
      if (success == totalOrders) {
        toastr.success(`Tạo thành công ${totalOrders} đơn giao nhanh.`);
      } else if (success > 0 && success < totalOrders) {
        toastr.warning(`Tạo thành công ${success}/${totalOrders} đơn giao nhanh.`);
      } else {
        toastr.error(`Tạo thất bại ${totalOrders} đơn giao nhanh.`);
      }
      await this.ordersController.reloadOrderList();
      if (success < totalOrders) {
        await this.getServices();
      }
    } catch(e) {
      toastr.error('Tạo đơn giao nhanh không thành công. Xin vui lòng thử lại!');
    }
    this.creating_ffm = false;
  }

  private start() {
    if (!this.display_progress_bar) {
      this.progress_percent = 0;
      this.display_progress_bar = true;
      this.interval = window.setInterval(() => this.increase(), 500);
    }
  }

  private increase() {
    while(this.progress_percent < 100) {
      this.progress_percent += 10;
    }
    window.clearInterval(this.interval);
  }

  private done() {
    if (this.display_progress_bar) {
      this.progress_percent = 100;
      this.display_progress_bar = false;
    }
  }

}
