import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderReceiptsModule } from 'apps/shop/src/app/pages/orders/sell-orders/components/order-receipts/order-receipts.module';
import { PaymentInfoComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/payment-info/payment-info.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticateModule } from '@etop/core';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';


@NgModule({
  declarations: [
    PaymentInfoComponent
  ],
  exports: [
    PaymentInfoComponent
  ],
  imports: [
    CommonModule,
    OrderReceiptsModule,
    SharedModule,
    AuthenticateModule,
    NgbModule,
    EtopMaterialModule,
    EtopPipesModule,
  ]
})
export class PaymentInfoModule { }
