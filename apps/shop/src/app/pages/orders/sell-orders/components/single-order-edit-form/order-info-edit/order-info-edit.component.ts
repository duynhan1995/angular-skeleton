import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { OrderService } from 'apps/shop/src/services/order.service';
import { SingleOrderEditFormControllerService } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form-controller.service';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { OrderApi } from '@etop/api';
@Component({
  selector: 'shop-order-info-edit',
  templateUrl: './order-info-edit.component.html',
  styleUrls: ['./order-info-edit.component.scss']
})
export class OrderInfoEditComponent implements OnInit {
  @Input() order = new Order({});
  loading = false;

  constructor(
    private orderService: OrderService,
    private orderEditFormController: SingleOrderEditFormControllerService,
    private traderStore: TraderStoreService,
    private orderApi: OrderApi,
  ) { }

  ngOnInit() {}

  async comfirmOrder() {
    this.loading = true;
    try {
      await this.orderApi.confirmOrder(this.order.id, "confirm");
      await this.orderEditFormController.updateOrderData();
      toastr.success('Xác nhận đơn hàng thành công.');
    } catch (e) {
      debug.error("ERROR in Updating Order", e);
      toastr.error(`Xác nhận đơn hàng thất bại: ${e.msg || e.message}`);
    }
    this.loading = false;
  }


  async updateOrder() {
    this.loading = true;
    try {
      const { basket_value, total_amount, total_items, id, order_note } = this.order;
      const body = {
        basket_value,
        total_amount,
        total_items,
        id,
        order_note
      };
      await this.orderService.updateOrder(body);
      await this.orderEditFormController.updateOrderData();
      this.orderService.switchOffCreatingFFM$.next();
      toastr.success('Cập nhật đơn hàng thành công.');
    } catch (e) {
      debug.error("ERROR in Updating Order", e);
      toastr.error(`Cập nhật đơn hàng thất bại: ${e.msg || e.message}`);
    }
    this.loading = false;
  }

  viewDetailCustomer() {
    const customer = this.order.customer;
    if (customer && customer.deleted) { return; }
    this.traderStore.navigate('customer', null, this.order.customer_id);
  }

}
