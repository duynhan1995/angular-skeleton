import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { FilterOperator, Filters } from '@etop/models';
import { Fulfillment } from '@etop/models';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { BaseComponent } from '@etop/core';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-order-fulfillments',
  templateUrl: './order-fulfillments.component.html',
  styleUrls: ['./order-fulfillments.component.scss']
})
export class OrderFulfillmentsComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() order_code: string;
  @Input() fulShippingStatus = '';
  fulfillments: Fulfillment[] = [];

  loading = false;
  show_table = false;

  constructor(
    private router: Router,
    private util: UtilService,
    private ffmService: FulfillmentService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  showCancelReason(ffm) {
    return ffm.shipping_state == 'cancelled' && ffm.cancel_reason;
  }

  ngOnInit() {
    this.ffmStore.fulfillmentCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.getFulfillments();
      });
  }

  async ngOnChanges(changes: SimpleChanges) {
    await this.getFulfillments();
  }

  async getFulfillments() {
    this.loading = true;
    try {
      const filters: Filters = [
        {
          name: "order.code",
          op: FilterOperator.eq,
          value: this.order_code
        },
        // {
        //   name: "shipping_state",
        //   op: FilterOperator.in,
        //   value: 'cancelled'
        // },
      ];
      const res: any = await this.ffmService.getFulfillments(0, 1000, filters);
      this.fulfillments = this.fulShippingStatus ? res.fulfillments.filter(ful => ful.shipping_status == this.fulShippingStatus) : res.fulfillments;
    } catch (e) {
      debug.error('ERROR in getting Fulfillments', e);
      this.fulfillments = [];
    }
    this.loading = false;
  }

  viewDetailFulfillment(code) {
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/fulfillments?code=${code}&type=shipment`
    );
  }

  toggleTable() {
    this.show_table = !this.show_table;
  }

}
