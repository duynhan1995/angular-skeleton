import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultiShipmentComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/multi-shipment.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiShipmentControlPanelComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/components/multi-shipment-control-panel/multi-shipment-control-panel.component';
import { SelectShippingServiceBoxComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/components/select-shipping-service-box/select-shipping-service-box.component';
import { MultiShipmentRowComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/components/multi-shipment-row/multi-shipment-row.component';
import { AddInfoFulfillmentModalComponent } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/components/add-info-fulfillment-modal/add-info-fulfillment-modal.component';
import { MultiShipmentStore } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/multi-shipment.store';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import { EtopFilterModule } from '@etop/shared/components/etop-filter/etop-filter.module';

@NgModule({
  declarations: [
    MultiShipmentComponent,
    MultiShipmentControlPanelComponent,
    SelectShippingServiceBoxComponent,
    MultiShipmentRowComponent,
    AddInfoFulfillmentModalComponent
  ],
  entryComponents: [
    AddInfoFulfillmentModalComponent
  ],
  exports: [
    MultiShipmentComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    EtopMaterialModule,
    EtopFilterModule,
    EtopPipesModule,
    MaterialModule,
  ],
  providers: [
    MultiShipmentStore
  ]
})
export class MultiShipmentModule { }
