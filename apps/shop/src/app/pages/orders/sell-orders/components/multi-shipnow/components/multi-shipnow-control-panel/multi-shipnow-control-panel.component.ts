import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { OrderService } from 'apps/shop/src/services/order.service';
import { AddressService } from 'apps/core/src/services/address.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { MultiShipnowController } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/multi-shipnow.controller';
import { GoogleMapService } from 'apps/shop/src/services/google-map.service';
import { Order } from 'libs/models/Order';
import { Address, OrderAddress } from 'libs/models/Address';
import { ShipnowServiceOptionsComponent } from 'apps/shop/src/app/components/delivery/shipnow/shipnow-service-options/shipnow-service-options.component';
import {map, takeUntil} from 'rxjs/operators';
import { FromAddressModalComponent } from 'libs/shared/components/from-address-modal/from-address-modal.component';
import { ShopService } from '@etop/features';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { LocationCompactPipe } from 'libs/shared/pipes/location.pipe';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {FulfillmentShipnowService} from "@etop/models";
import {AddressAPI, FulfillmentAPI} from "@etop/api";
import {ConnectionStore} from "@etop/features";

@Component({
  selector: 'shop-multi-shipnow-control-panel',
  templateUrl: './multi-shipnow-control-panel.component.html',
  styleUrls: ['./multi-shipnow-control-panel.component.scss']
})
export class MultiShipnowControlPanelComponent extends BaseComponent implements OnInit {
  @ViewChild('googleMap', { static: false }) gmapElement: any;
  @ViewChild('shipnowServiceOptions', { static: false }) shipnowServiceOptions: ShipnowServiceOptionsComponent;

  @Output() createFulfillment = new EventEmitter<{selectedService: FulfillmentShipnowService, orders: Order[]}>();

  @Input() totalOrders = 0;
  @Input() totalCodAmount = 0;
  @Input() hasValidOrders = false;
  @Input() loading = false;
  @Input() calculatingFee = false;
  totalFee = 0;

  orders: Order[] = [];

  fromAddresses = [];
  fromAddressIndex: number;

  shipping_data = {
    pickup_address: new Address({}),
    coupon: ''
  };

  addressLoading = true;
  renderingMap = true;
  gettingServices = true;
  selectedService: FulfillmentShipnowService;

  shipnowConnections$ = this.connectionStore.state$.pipe(
    map(s => s?.validConnections.filter(conn => conn?.connection_subtype == 'shipnow'))
  );

  constructor(
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private router: Router,
    private util: UtilService,
    private modalController: ModalController,
    private googleMap: GoogleMapService,
    private ordersController: SellOrdersController,
    private addressService: AddressService,
    private orderService: OrderService,
    private shipnowController: MultiShipnowController,
    private locationCompact: LocationCompactPipe,
    private connectionStore: ConnectionStore
  ) {
    super();
  }

  get validFromAddress() {
    const { province_code } = this.shipping_data.pickup_address;
    return GoogleMapService.allowedProvinces.indexOf(province_code) != -1;
  }

  get selectedConnection() {
    const connections = this.connectionStore.snapshot.validConnections;
    return connections.find(conn => conn.id == this.selectedService?.connection_info?.id);
  }

  get notVerifiedConnection() {
    const connections = this.connectionStore.snapshot.validConnections;
    const _selected = connections.find(conn => conn.id == this.selectedService?.connection_info?.id);
    return _selected?.connection_subtype == 'shipnow' && !_selected?.external_verified;
  }

  get notVerifiedConnectionUsingCod() {
    const connections = this.connectionStore.snapshot.validConnections;
    const _selected = connections.find(conn => conn.id == this.selectedService?.connection_info?.id);
    return _selected?.connection_subtype == 'shipnow' && !_selected?.external_verified
      && this.orders.some(o => Number(o.p_data.shipping.cod_amount));
  }

  async ngOnInit() {
    await this.getListAddress();
    this.googleMapRenderer().then();

    this.shipnowController.onRenderingMap$
      .pipe(takeUntil(this.destroy$))
      .subscribe((orders: Order[]) => {
        this.orders = orders;
        this.googleMapRenderer();
      });
    this.shipnowController.onSuccessCreatingFFM$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.totalFee = 0;
        this.googleMap.resetMap();
        this.setFromAddressMarker();
        this.orders = [];
        this.getServices();
      });
    this.shipnowController.onFailedCreatingFFM$
      .pipe(takeUntil(this.destroy$))
      .subscribe((orders: Order[]) => {
        this.orders = orders;
        this.googleMapRenderer();
        this.getServices();
      });
  }

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (address) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
        await this.getListAddress();
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
      }
    });
  }

  async getListAddress() {
    this.addressLoading = true;
    try {
      let { fromAddresses, fromAddressIndex } = await this.addressService.getFromAddresses();
      this.fromAddresses = fromAddresses;
      this.fromAddressIndex = fromAddressIndex;
    } catch(e) {
      debug.error('ERROR in getListAddress()', e);
    }
    this.addressLoading = false;
    this.shipping_data.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.updateShippingData();
  }

  setShopAddress() {
    this.shipping_data.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.updateShippingData();
  }

  updateShippingData(emitEvent = true) {
    this.ordersController.shipping_data = this.shipping_data;
    if (emitEvent) {
      this.ordersController.onUpdateShippingData$.next(this.shipping_data);
    }
  }

  async googleMapRenderer() {
    this.renderingMap = true;
    try {
      if (!this.validFromAddress) {
        this.googleMap.initDefaultMap(this.gmapElement);
        throw new Error('Unsupported province. Init default map!');
      }
      const { province_code } = this.shipping_data.pickup_address;
      await this.setFromAddressMarker();

      await this.googleMap.getBoundCornersOfCity(province_code);
      await this.setToAddressesMarkers();
      await this.calculateAndDisplayRoute();
      this.getServices();
    } catch(e) {
      debug.error('ERROR in Rendering Map', e);
    }
    this.renderingMap = false;
  }

  async setFromAddressMarker() {
    try {
      const { address1, ward, district, province, coordinates } = this.shipping_data.pickup_address;
      const from_address_fulltext = `${address1}, ${ward}, ${district}, ${province}`;
      let res: { lng: number; invalid: boolean; lat: number };
      if  (!(coordinates && coordinates.latitude && coordinates.longitude)) {
        const result: any = await this.googleMap.getLatLngFromAddress(from_address_fulltext);
        const { lat: latitude, lng: longitude, invalid: inv } = result;
        res = { lat: latitude, lng: longitude, invalid: inv };
        this.shipping_data.pickup_address = {
          ...this.shipping_data.pickup_address,
          coordinates: {
            latitude: latitude,
            longitude: longitude
          }
        };
        this.updateListAddresses();

        const _request: any = {...this.shipping_data.pickup_address};
        await this.addressService.updateAddress(_request);
      } else {
        res = { lat: coordinates.latitude, lng: coordinates.longitude, invalid: false };
      }
      const { lat, lng, invalid } = res;

      this.googleMap.initMap(this.gmapElement, lat, lng);

      this.googleMap.getMarkerFromLatLng(lat, lng, 'assets/images/from-item-0.png');

      if (!this.googleMap.map_points.length) {
        this.googleMap.map_points.push({ lat, lng });
      } else this.googleMap.map_points[0] = {lat, lng};
      return { lat, lng, invalid };
    } catch (e) {
      debug.error('ERROR in setFromAddressMarker', e);
    }
    return null;
  }

  async setToAddressesMarkers() {
    try {
      this.googleMap.markers.forEach(m => m.setMap(null)); // remove markers from map
      this.googleMap.markers = [];
      this.googleMap.map_points.splice(1);
      for (let i = 0; i < this.orders.length; i ++) {
        const shipping_address = this.orders[i].p_data.shipping.shipping_address;
        if (!shipping_address || !shipping_address.coordinates) {
          continue;
        }
        const { coordinates } = shipping_address;
        const { latitude, longitude } = coordinates;
        this.googleMap.map_points.push({ lat: latitude, lng: longitude });
        const marker = this.googleMap.getMarkerFromLatLng(
          latitude, longitude, `assets/images/to-item-${i + 1}.png`);
        this.googleMap.markers.push(marker);
      }
    } catch (e) {
      debug.error('ERROR in setToAddressesMarkers', e);
      throw e;
    }
  }

  async calculateAndDisplayRoute() {
    if (!this.orders.length) {
      this.googleMap.directionsDisplay.setMap(null);
      return;
    }
    const pickup = this.shipping_data.pickup_address;
    const shipping = this.orders[this.orders.length - 1].p_data.shipping.shipping_address;
    if (!pickup || !shipping || !pickup.coordinates || !shipping.coordinates) {
      return;
    }
    const coors_start = pickup.coordinates;
    const coors_end = shipping.coordinates;
    const start = new google.maps.LatLng(coors_start.latitude, coors_start.longitude);
    const end = new google.maps.LatLng(coors_end.latitude, coors_end.longitude);
    const waypoints = [];
    for (let order of this.orders) {
      const shipping_address = order.p_data.shipping.shipping_address;
      if (!shipping_address || !shipping.coordinates) {
        continue;
      }
      waypoints.push({
        location: new google.maps.LatLng(
          shipping_address.coordinates.latitude,
          shipping_address.coordinates.longitude
        ),
        stopover: true
      });
    }
    await this.googleMap.calculateAndDisplayRoute(start, end, waypoints);
  }

  drop(orders: Order[]) {
    this.orders = orders;
    this.googleMapRenderer().then();
  }

  updateListAddresses() {
    const index = this.fromAddresses.findIndex(a => a.id == this.shipping_data.pickup_address.id);
    this.fromAddresses[index] = this.shipping_data.pickup_address;
  }

  getServices() {
    this.gettingServices = true;
    try {
      const deliveryPoints = this.orders.map(o => {
        return {
          shipping_address: o.p_data.shipping.shipping_address,
          cod_amount: o.p_data.shipping.cod_amount,
        }
      });

      const data: FulfillmentAPI.GetShipnowServicesRequest = {
        pickup_address: this.shipping_data.pickup_address,
        delivery_points: deliveryPoints,
        coupon: this.shipping_data.coupon
      };

      this.shipnowServiceOptions.getServices(data).then();
    } catch(e) {
      debug.error('ERROR in getting services', e);
      throw e;
    }
    this.gettingServices = false;
  }

  onSelectedService(service) {
    this.selectedService = service;
    if (!service) { return; }
    this.totalFee = service.fee;
  }

  createFFM() {
    if (this.notVerifiedConnectionUsingCod) {
      return;
    }

    this.createFulfillment.emit({
      selectedService: this.selectedService,
      orders: this.orders
    });
  }

  addressDisplay(address: OrderAddress | Address, show_full: false) {
    if (!address) {
      return;
    }
    address = {
      ...address,
      address1: address.address1 && this.locationCompact.transform(address.address1),
      ward: address.ward && this.locationCompact.transform(address.ward),
      district: address.district && this.locationCompact.transform(address.district),
      province: address.province && this.locationCompact.transform(address.province)
    };
    const { address1, ward, district, province } = address;
    if (show_full) {
      return `${address1}, ${ward && ward + ', ' || ''}${district}, ${province}`;
    }
    return `${ward && ward + ', ' || ''}${district}, ${province}`;
  }

  toCarrierConnect() {
    const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
    window.open(`/s/${slug}/settings/shipping`, '_blank');
  }

}
