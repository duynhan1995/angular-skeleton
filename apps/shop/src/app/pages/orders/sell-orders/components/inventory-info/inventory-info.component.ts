import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { InventoryVoucher, Order } from '@etop/models';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { InventoryApi } from '@etop/api';

@Component({
  selector: 'shop-inventory-info',
  templateUrl: './inventory-info.component.html',
  styleUrls: ['./inventory-info.component.scss']
})
export class InventoryInfoComponent implements OnInit, OnChanges {
  @Input() order = new Order({});
  inInventoryVoucher : InventoryVoucher;
  outInventoryVoucher : InventoryVoucher;
  constructor(
    private inventoryStore: InventoryStoreService,
    private inventoryApi: InventoryApi) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.detectInventoryVouchers();
  }

  viewDetailVoucher(type: string, voucher_code: string) {
    this.inventoryStore.navigate(type, voucher_code);
  }

  async detectInventoryVouchers () {
    let inventoryVouchers = await this.inventoryApi.getInventoryVouchersByReference(this.order.id,'order');
    if(inventoryVouchers.length > 0){
      this.inInventoryVoucher = inventoryVouchers.find(inventoryVoucher => inventoryVoucher.type == 'in');
      this.outInventoryVoucher = inventoryVouchers.find(inventoryVoucher => inventoryVoucher.type == 'out');
    }else {
      this.inInventoryVoucher = null;
      this.outInventoryVoucher = null;
    }
  }
}
