import { Component, Input, OnInit } from '@angular/core';
import { Order } from '@etop/models';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';

@Component({
  selector: 'shop-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent implements OnInit {
  @Input() order = new Order({});

  constructor(
    private traderStore: TraderStoreService
  ) { }

  ngOnInit() {
  }

  viewDetailCustomer() {
    const customer = this.order.customer;
    if (!this.order.customer_id) { return; }
    if (customer && customer.deleted) { return; }
    this.traderStore.navigate('customer', null, this.order.customer_id);
  }

}
