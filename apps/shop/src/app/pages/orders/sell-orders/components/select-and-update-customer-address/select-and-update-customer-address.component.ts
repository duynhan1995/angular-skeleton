import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import {UtilService} from "apps/core/src/services/util.service";
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {BaseComponent} from "@etop/core";

@Component({
  selector: 'shop-select-and-update-customer-address',
  templateUrl: './select-and-update-customer-address.component.html',
  styleUrls: ['./select-and-update-customer-address.component.scss']
})
export class SelectAndUpdateCustomerAddressComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() addresses: CustomerAddress[] = [];
  @Input() currentAddress = new CustomerAddress({});
  @Input() customer = new Customer({});

  toUpdate = false;
  toCreate = false;

  loading = false;
  addressIndex;

  formInitializing = true;
  customerAddressForm = this.fb.group({
    id: '',
    full_name: '',
    phone: '',
    province_code: '',
    district_code: '',
    ward_code: '',
    address1: ''
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.customerAddressForm.controls['province_code'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.customerAddressForm.controls['district_code'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  private static validateAddress(data) {
    const {full_name, phone, province_code, district_code, ward_code, address1} = data;
    if (!full_name) {
      toastr.error('Chưa nhập tên!');
      return false;
    }
    if (!phone) {
      toastr.error('Chưa nhập số điện thoại!');
      return false;
    }
    if (!province_code) {
      toastr.error('Chưa chọn tỉnh thành!');
      return false;
    }
    if (!district_code) {
      toastr.error('Chưa chọn quận huyện!');
      return false;
    }
    if (!ward_code) {
      toastr.error('Chưa chọn phường xã!');
      return false;
    }
    if (!address1) {
      toastr.error('Chưa nhập địa chỉ cụ thể!');
      return false;
    }
    return true;
  }

  constructor(
    private fb: FormBuilder,
    private modalAction: ModalAction,
    private customerService: CustomerService,
    private util: UtilService,
    private locationQuery: LocationQuery,
  ) {
    super();
  }

  displayLocationMap = option => option && option.name || null;

  valueLocationMap = option => option && option.code || null;

  ngOnInit() {
    this.addresses.forEach(a => {
      a.p_data = {};
      a.p_data.selected = a.id == this.currentAddress.id;
    });

    this.customerAddressForm.controls['province_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.formInitializing) {
          this.customerAddressForm.patchValue({
            district_code: '',
            ward_code: ''
          });
        }
      });

    this.customerAddressForm.controls['district_code'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        if (!this.formInitializing) {
          this.customerAddressForm.patchValue({
            ward_code: ''
          });
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.addresses.forEach(a => {
      a.p_data = {};
      a.p_data.selected = a.id == this.currentAddress.id;
    });
  }

  closeModal() {
    this.modalAction.close(false);
  }

  confirm() {
    if (!this.toUpdate && !this.toCreate) {
      this.confirmSelectedAddress();
    } else if (this.toCreate) {
      this.createAddress();
    } else {
      this.updateAddress();
    }
  }

  confirmSelectedAddress() {
    const _selected = this.addresses.find(a => a.p_data && a.p_data.selected);
    if (!_selected) {
      return toastr.error('Chưa chọn địa chỉ');
    }
    this.modalAction.dismiss(_selected);
  }

  updateSelectedAddress(address, index) {
    this.addressIndex = index;
    this.toUpdate = true;

    const {full_name, phone, province_code, district_code, ward_code, address1, id} = address;

    setTimeout(_ => {
      this.formInitializing = true;

      this.customerAddressForm.patchValue({
        full_name,
        phone,
        province_code,
        district_code,
        ward_code,
        address1,
        id
      });

      this.formInitializing = false;
    }, 300);
  }

  async createAddress() {
    this.loading = true;
    try {
      let addressData = this.customerAddressForm.getRawValue();

      addressData = this.util.trimFields(addressData, ['full_name', 'phone', 'address1']);
      if (!SelectAndUpdateCustomerAddressComponent.validateAddress(addressData)) {
        return this.loading = false;
      }

      const {full_name, phone, province_code, district_code, ward_code, address1} = addressData;

      const res = await this.customerService.createCustomerAddress({
        customer_id: this.customer.id,
        full_name, phone, province_code, district_code, ward_code, address1
      });

      this.addresses.forEach(a => a.p_data.selected = false);
      this.addresses.push({
        ...res,
        p_data: {
          selected: true
        }
      });
      toastr.success('Tạo địa chỉ thành công.');
    } catch(e) {
      debug.error('ERROR in create Customer Address', e);
      toastr.error('Tạo địa chỉ không thành công.', e.message || e.msg);
    }
    this.toCreate = false;
    this.loading = false;
  }

  async updateAddress() {
    this.loading = true;
    try {
      let addressData = this.customerAddressForm.getRawValue();

      addressData = this.util.trimFields(addressData, ['full_name', 'phone', 'address1']);
      if (!SelectAndUpdateCustomerAddressComponent.validateAddress(addressData)) {
        return this.loading = false;
      }

      const {full_name, phone, province_code, district_code, ward_code, address1, id} = addressData;

      const res = await this.customerService.updateCustomerAddress(
        {full_name, phone, province_code, district_code, ward_code, address1, id}
      );

      this.addresses[this.addressIndex] = {
        ...res,
        p_data: {
          ...this.addresses[this.addressIndex].p_data
        }
      };

      toastr.success('Cập nhật địa chỉ thành công.');
    } catch(e) {
      debug.error('ERROR in update Customer Address', e);
      toastr.error('Cập nhật địa chỉ không thành công.', e.message || e.msg);
    }
    this.toUpdate = false;
    this.loading = false;
  }

  onSelectAddress(address, index) {
    this.addresses.forEach(a => {
      a.p_data.selected = false;
    });
    this.addresses[index].p_data.selected = true;
  }

  addMoreAddress() {
    this.toCreate = true;
    this.customerAddressForm.patchValue({
      full_name: this.customer.full_name,
      phone: this.customer.phone,
      province_code: '',
      district_code: '',
      ward_code: '',
      address1: '',
      id: null
    });
  }

}
