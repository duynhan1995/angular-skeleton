import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Order } from 'libs/models/Order';
import { Fulfillment } from 'libs/models/Fulfillment';
import { DeliveryComponent } from 'apps/shop/src/app/components/delivery/delivery.component';
import { SingleOrderEditFormControllerService } from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form-controller.service';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { OrderService } from 'apps/shop/src/services/order.service';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { TelegramService } from '@etop/features';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import {CustomerApi, FulfillmentAPI, FulfillmentApi, OrderApi} from '@etop/api';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { CustomerStoreService } from 'apps/core/src/stores/customer.store.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import {map, takeUntil} from 'rxjs/operators';
import { Customer } from 'libs/models/Customer';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {ConnectionStore} from "@etop/features";

@Component({
  selector: 'shop-order-edit-form',
  templateUrl: './single-order-edit-form.component.html',
  styleUrls: ['./single-order-edit-form.component.scss']
})
export class SingleOrderEditFormComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() order = new Order({});
  active_tab: 'inventory' | 'receipt' | 'detail_info' | 'history' = 'detail_info';
  fulfillment: Fulfillment;
  loading = false;

  notVerifiedConnection$ = this.ffmStore.state$.pipe(
    map(s => {
      const connections = this.connectionStore.snapshot.validConnections;
      const _selected = connections.find(conn => conn.id == s?.fulfillment?.connection_id);
      return _selected?.connection_subtype == 'shipnow' && !_selected?.external_verified
        && Number(s.fulfillment.cod_amount);
    })
  );
  selectedConnection$ = this.ffmStore.state$.pipe(
    map(s => {
      const connections = this.connectionStore.snapshot.validConnections;
      return connections.find(conn => conn.id == s?.fulfillment?.connection_id);
    })
  );

  constructor(
    private connectionStore: ConnectionStore,
    private auth: AuthenticateStore,
    private orderEditFormController: SingleOrderEditFormControllerService,
    private telegram: TelegramService,
    private ga: GoogleAnalyticsService,
    private ffmService: FulfillmentService,
    private orderService: OrderService,
    private ffmApi: FulfillmentApi,
    private orderApi: OrderApi,
    private customerApi: CustomerApi,
    private ffmStore: FulfillmentStore,
    private orderStore: OrderStoreService,
    private customerStore: CustomerStoreService,
    private ordersController: SellOrdersController
  ) {
    super();
  }

  ngOnInit() {
    this.ffmStore.fufillmentValueChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(fulfillment => {
        this.fulfillment = fulfillment;
      });
    this.ordersController.onChangeTab$
      .pipe(takeUntil(this.destroy$))
      .subscribe((active_tab: any) => {
        this.active_tab = active_tab;
      });
  }

  async ngOnChanges(changes: SimpleChanges) {
    this.ffmStore.resetState();
    const last_order = changes.order.previousValue;
    const order = changes.order.currentValue;
    if (!last_order || last_order.id != order.id) {
      this.customerStore.resetSelectedCustomer();
      this.customerStore.resetSelectedCustomerAddress();
    }

    this.orderStore.selectOrder(order);
    if (order.customer_id && !order.customer.deleted) {
      const customer: Customer = await this.customerApi.getCustomer(order.customer_id);
      customer.addresses = await this.customerApi.getCustomerAddresses(order.customer_id);

      this.customerStore.selectCustomer(customer);
    }
    if (order.total_amount && order.total_amount != 0) {
      this.orderStore.updateTotalAmount(order.total_amount);
    }
    this.ffmStore.createCustomerByShippingAddress(false);
  }

  get isCreatingFFM() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  async confirmOrder() {
    this.loading = true;
    try {
      await this.orderApi.confirmOrder(this.order.id, "confirm");
      await this.orderEditFormController.updateOrderData();
      this.orderService.createOrderSuccess$.next();
      toastr.success("Tạo hoá đơn thành công!");
    } catch(e) {
      this.orderService.createOrderFailed$.next();
      debug.error('ERROR in confirming order', e);
      toastr.error(`Tạo hoá đơn không thành công.`, e.code && (e.message || e.msg) || '');
    }
    this.loading = false;
  }

  async createFFM(needConfirm: boolean) {
    const connections = this.connectionStore.snapshot.validConnections;
    const fulfillment = this.ffmStore.snapshot.fulfillment;
    const _selected = connections.find(conn => conn.id == fulfillment?.connection_id);
    const {pickup_address, shipping_address, cod_amount, shipping_service_code} = fulfillment;

    if (!shipping_address) {
      return toastr.error("Thiếu thông tin địa chỉ giao hàng!");
    }

    if (_selected?.connection_subtype == 'shipnow') {
      if (!_selected?.external_verified && Number(fulfillment?.cod_amount)) {
        return;
      }
      if (!pickup_address.coordinates?.latitude || !pickup_address.coordinates?.longitude) {
        return toastr.error('Không xác định được địa chỉ lấy hàng trên bản đồ. Vui lòng kiểm tra lại địa chỉ lấy hàng của bạn!');
      }

      if (!shipping_address.coordinates?.latitude || !shipping_address.coordinates?.longitude) {
        return toastr.error('Không xác định được địa chỉ giao hàng trên bản đồ. Vui lòng kiểm tra lại địa chỉ giao hàng của bạn!');
      }
    }

    this.loading = true;
    try {
      if (!cod_amount && cod_amount != 0) {
        this.ffmStore.errorCOD(true);
        return this.handleErrorManually("Vui lòng nhập tiền Thu hộ (COD)!");
      }
      if (cod_amount < 5000 && cod_amount > 0) {
        return this.handleErrorManually("Vui lòng nhập tiền thu hộ bằng 0đ hoặc từ 5.000đ trở lên!");
      }
      if (!shipping_service_code) {
        return this.handleErrorManually("Vui lòng chọn gói vận chuyển!");
      }

      if (needConfirm) {
        await this.orderApi.confirmOrder(this.order.id, "confirm");
      }
      if (this.ffmService.shipping_type == 'shipment') {
        await this.createShipmentFFM(this.order.id, this.order.confirm_status);
      }
      if (this.ffmService.shipping_type == 'shipnow') {
        await this.createShipnowFFM(this.order.id, this.order.confirm_status);
      }
      await this.orderEditFormController.updateOrderData();
      this.orderService.createOrderSuccess$.next();
    } catch (e) {
      this.orderService.createOrderFailed$.next();
      debug.error('ERROR in creating FFM', e);
      toastr.error(`Tạo hoá đơn và giao hàng không thành công.`, e.code && (e.message || e.msg) || '');
    }
    this.loading = false;
  }

  async createShipmentFFM(order_id: string, confirm_status: string) {
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;

      const body: any = {
        order_id,
        ...fulfillment,
        shipping_type: 'shipment',
        chargeable_weight: this.ffmService.weight_calc_type == 1
          && fulfillment.chargeable_weight || null,
        length: this.ffmService.weight_calc_type == 2
          && fulfillment.length || null,
        width: this.ffmService.weight_calc_type == 2
          && fulfillment.width || null,
        height: this.ffmService.weight_calc_type == 2
          && fulfillment.height || null
      };

      const res = await this.ffmApi.createShipmentFulfillment(body);
      this.socialTrackingEvents(res);
      toastr.success(`${confirm_status != 'P' && 'Tạo hoá đơn và ' || 'Tạo đơn '}giao hàng thành công!`);
    } catch (e) {
      throw e;
    }
  }

  async createShipnowFFM(order_id: string, confirm_status: string) {
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;

      const {
        shipping_address,
        pickup_address,
        cod_amount,
        shipping_note,
        shipping_service_code,
        shipping_service_fee,
        carrier,
        connection_id,
        coupon
      } = fulfillment;

      const body: FulfillmentAPI.CreateShipnowFulfillmentRequest = {
        delivery_points: [{
          order_id,
          chargeable_weight: 1000,
          cod_amount,
          shipping_address
        }],
        pickup_address,
        shipping_note,
        shipping_service_code,
        shipping_service_fee,
        carrier,
        connection_id,
        coupon
      };

      const shipnow_ffm = await this.ffmService.createShipnowFulfillment(body);
      await this.confirmShipnowFFM(shipnow_ffm.id);
      toastr.success(`${confirm_status != 'P' && 'Tạo hoá đơn và ' || 'Tạo đơn '}giao tức thì thành công!`);
    } catch(e) {
      throw e;
    }
  }

  async confirmShipnowFFM(shipnow_id: string) {
    try {
      const message_data = await this.ffmService.confirmShipnowFulfillment(shipnow_id);
      this.telegram.shipnowConfirm(message_data).then();
    } catch (e) {
      this.ffmService.cancelShipnowFulfillment(shipnow_id, 'Xác nhận đơn thất bại').then();
      debug.error("ERROR in confirmShipnowFFM", e);
      throw e;
    }
  }

  private handleErrorManually(error_message: string) {
    toastr.error(error_message);
    this.loading = false;
  }

  private socialTrackingEvents(data) {
    try {
      const ffm = data[0];
      const order = data[0].order;
      this.ga.sendOrderInfo(
        order, ffm, this.auth.snapshot.shop,
        ffm.shipping_address, ffm.pickup_address
      );
      this.telegram.sendOrderTelegramMessage(order, 'confirm', null, ffm).then();
    } catch(e) {
      debug.error("ERROR in GA and Telegram", e);
    }
  }

  toggleSwitch() {
    this.ffmStore.createFulfillment();
  }

}
