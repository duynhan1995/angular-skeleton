import { Component, Input, OnInit } from '@angular/core';
import { Order } from '@etop/models';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { CustomerService } from 'apps/shop/src/services/customer.service';

@Component({
  selector: '[shop-order-row]',
  templateUrl: './sell-order-row.component.html',
  styleUrls: ['./sell-order-row.component.scss']
})
export class SellOrderRowComponent implements OnInit {
  @Input() order = new Order({});
  @Input() liteMode = false;

  constructor(
    public util: UtilService,
    private router: Router,
    private customerService: CustomerService,
    private traderStore: TraderStoreService
  ) { }

  get isNormalCustomer() {
    if (!this.customerService.independent_customer) {
      return true;
    }
    return !!this.order.customer_id
      && this.order.customer_id != this.customerService.independent_customer.id;
  }

  ngOnInit() {
  }

  viewDetailCustomer() {
    const customer = this.order.customer;
    if (!this.order.customer_id) { return; }
    if (customer && customer.deleted) { return; }
    this.traderStore.navigate('customer', null, this.order.customer_id);
  }

  viewDetailFfm() {
    const ffm_type = this.order.fulfillment_type;
    const ffm_code = this.order.activeFulfillment.shipping_code;
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/fulfillments?code=${ffm_code}&type=${ffm_type}`
    );
  }

}
