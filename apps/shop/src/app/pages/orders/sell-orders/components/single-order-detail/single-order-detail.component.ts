import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment, Order } from '@etop/models';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-order-detail',
  templateUrl: './single-order-detail.component.html',
  styleUrls: ['./single-order-detail.component.scss']
})
export class SingleOrderDetailComponent extends BaseComponent implements OnInit {
  @Input() order = new Order({});
  active_tab: 'inventory' | 'receipt' | 'detail_info' |  'history' = 'detail_info';

  constructor(
    private ordersController: SellOrdersController
  ) {
    super();
  }

  async ngOnInit() {
    this.ordersController.onChangeTab$
      .pipe(takeUntil(this.destroy$))
      .subscribe((active_tab: any) => {
        this.active_tab = active_tab;
      });
  }

  get fulfillment(): Fulfillment {
    return this.order.activeFulfillment || null;
  }

}
