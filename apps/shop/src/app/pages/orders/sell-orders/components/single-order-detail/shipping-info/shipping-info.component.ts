import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {CANCEL_ORDER_REASONS, Fulfillment, Order, ShipnowDeliveryPoint} from '@etop/models';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { FulfillmentApi, OrderApi } from '@etop/api';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { CancelObjectComponent } from 'apps/shop/src/app/components/cancel-object/cancel-object.component';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { OrderService } from 'apps/shop/src/services/order.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';

@Component({
  selector: 'shop-shipping-info',
  templateUrl: './shipping-info.component.html',
  styleUrls: ['./shipping-info.component.scss']
})
export class ShippingInfoComponent implements OnInit, OnChanges {
  @Input() fulfillment = new Fulfillment({});
  @Input() order = new Order({});

  private modal: any;
  dropdownActions = [];

  constructor(
    private router: Router,
    private ref: ChangeDetectorRef,
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private ordersController: SellOrdersController,
    private util: UtilService,
    private orderService: OrderService,
    private ffmApi: FulfillmentApi,
    private ffmStore: FulfillmentStore
  ) { }

  get cancellable() {
    return ['Z', 'S'].indexOf(this.fulfillment.status) != -1;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const total_shipnow_orders = this.fulfillment.delivery_points && this.fulfillment.delivery_points.length;
    this.dropdownActions = [
      {
        onClick: () => total_shipnow_orders > 1 ?
          this.checkShipnowOrders(total_shipnow_orders) :
          this.cancelFulfillment(total_shipnow_orders),
        title: 'Huỷ giao hàng',
        cssClass: 'text-danger',
        hidden: !this.cancellable
      }
    ];
    this.ref.detectChanges();
  }

  viewDetailFfm() {
    const ffm_type = this.order.fulfillment_type;
    const ffm_code = this.fulfillment.shipping_code;
    this.router.navigateByUrl(
      `s/${this.util.getSlug()}/fulfillments?code=${ffm_code}&type=${ffm_type}`
    );
  }

  async checkShipnowOrders(total_shipnow_orders: number) {
    if (this.modal) { return; }
    const delivery_points: ShipnowDeliveryPoint[] = [];
    for (let dp of this.fulfillment.delivery_points) {
      const order = await this.orderService.getOrder(dp.order_id);
      dp = {
        ...dp,
        order_code: order.code,
        order_status: order.status,
        order_status_display: order.status_display,
        order_total_amount: order.total_amount
      };
      delivery_points.push(dp);
    }

    this.fulfillment.delivery_points = delivery_points;
    this.modal = this.dialogController.createConfirmDialog({
      title: `Huỷ đơn giao tức thì`,
      body: `
        <div>Bạn đang có <strong>${delivery_points.length}</strong> đơn hàng đang được giao bởi đơn giao tức thì
          <strong>#${this.fulfillment.shipping_code}</strong>.
        </div>
        <div>Bạn có chắc muốn huỷ đơn giao tức thì này?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: true,
      onConfirm: () => {
        this.modal = null;
        this.cancelFulfillment(total_shipnow_orders);
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  async cancelFulfillment(total_shipnow_orders: number) {
    if (this.modal) { return; }

    this.modal = this.modalController.create({
      component: CancelObjectComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        title: `đơn giao hàng #${this.fulfillment.shipping_code}`,
        reasons: CANCEL_ORDER_REASONS,
        cancelFn: async (reason: string) => {
          try {
            if (total_shipnow_orders) {
              await this.ffmApi.cancelShipnowFulfillment(this.fulfillment.id, reason);
            } else {
              await this.ffmApi.cancelShipmentFulfillment(this.fulfillment.id, reason);
            }
            this.ordersController.reloadOrderList();
            this.ffmStore.cancelFulfillment();
            toastr.success('Huỷ đơn giao hàng thành công.');
          } catch(e) {
            toastr.error('Huỷ đơn giao hàng không thành công.', e.code && (e.message || e.msg) || '');
            throw e;
          }
        }
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(_ => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

}
