import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import {Fulfillment} from "@etop/models";
import {FulfillmentStore} from "apps/core/src/stores/fulfillment.store";
import { OrderService } from 'apps/shop/src/services/order.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { AddressService } from 'apps/core/src/services/address.service';
import List from 'identical-list';
import { FromAddressModalComponent } from 'libs/shared/components/from-address-modal/from-address-modal.component';
import { Address } from 'libs/models/Address';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ShopService } from '@etop/features';
import { SellOrdersController } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import { MultiShipmentStore } from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/multi-shipment.store';
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";

interface FilterOption {
  id: string;
  value: string | number;
}

@Component({
  selector: 'shop-multi-shipment-control-panel',
  templateUrl: './multi-shipment-control-panel.component.html',
  styleUrls: ['./multi-shipment-control-panel.component.scss']
})
export class MultiShipmentControlPanelComponent extends BaseComponent implements OnInit, OnDestroy {
  @Input() loading = false;
  @Input() calculating_fee = false;

  @Output() createFfm = new EventEmitter();

  try_on: string;

  fromAddresses = [];
  fromAddressIndex: number;
  addressLoading = true;

  shipping_data = {
    pickup_address: {},
    try_on: this.auth.snapshot.shop.try_on,
    include_insurance: false,
    chargeable_weight: 250,
    shipping_note: ''
  };

  uni = {
    chargeable_weight: false,
    try_on: false,
    shipping_note: false
  };

  defaultWeight$ = this.ffmStore.state$.pipe(
    map(s => s?.defaultWeight), distinctUntilChanged()
  );

  minAcceptWeight = 50;
  productWeights = new List<FilterOption>([
    { id: '100', value: 100 },
    { id: '200', value: 200 },
    { id: '250', value: 250 },
    { id: '300', value: 300 },
    { id: '500', value: 500 },
    { id: '1000', value: 1000 },
    { id: '1500', value: 1500 },
    { id: '2000', value: 2000 },
    { id: '2500', value: 2500 },
    { id: '3000', value: 3000 },
    { id: '4000', value: 4000 },
    { id: '5000', value: 5000 }
  ]);

  constructor(
    private shopService: ShopService,
    private modalController: ModalController,
    private orderService: OrderService,
    private addressService: AddressService,
    private auth: AuthenticateStore,
    private ordersController: SellOrdersController,
    private moFastStore: MultiShipmentStore,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  get totalCodAmount() {
    const orders = this.moFastStore.snapshot.selected_orders;
    return orders.reduce((a,b) => {
      if (this.ordersController.orderToBeUpdated(b)) {
        return a + Number(b.p_data.shipping.cod_amount);
      }
      return a;
    }, 0) || 0;
  }

  get totalOrders() {
    const orders = this.moFastStore.snapshot.selected_orders;
    return orders.filter(o => this.ordersController.orderToBeUpdated(o)).length;
  }

  get totalFee(): number {
    const orders = this.moFastStore.snapshot.selected_orders;
    return orders.reduce((a, b) => {
      if (
        this.ordersController.orderToBeUpdated(b) &&
        b.p_data.selectedService &&
        b.p_data.selectedService.code
      ) {
        return a + b.p_data.selectedService.fee;
      }
      return a;
    }, 0) || 0;
  }

  get hasValidOrders() {
    const orders = this.moFastStore.snapshot.selected_orders;
    return orders.some(o => this.ordersController.orderToBeUpdated(o));
  }

  async ngOnInit() {
    const shopDefaultWeight = this.ffmStore.snapshot.defaultWeight;

    if (shopDefaultWeight) {
      this.shipping_data.chargeable_weight = shopDefaultWeight;
      this.updateShippingData();
    } else {
      this.defaultWeight$.pipe(takeUntil(this.destroy$))
        .subscribe(value => {
          this.shipping_data.chargeable_weight = value;
          this.updateShippingData();
        });
    }

    await this.getListAddress();
  }

  validateWeightFn = (s: string) => {
    if (s) {
      const num = Number(s);
      if (Number.isNaN(num) || num < this.minAcceptWeight) {
        return false;
      }
    }
    return true;
  };

  newFromAddress() {
    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: 'shipfrom',
        title: 'Tạo địa chỉ lấy hàng'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (address) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
        await this.getListAddress();
        this.fromAddressIndex = this.fromAddresses.findIndex(addr => addr.id == address.id);
      }
    });
  }

  async getListAddress() {
    this.addressLoading = true;
    try {
      let { fromAddresses, fromAddressIndex } = await this.addressService.getFromAddresses();
      this.fromAddresses = fromAddresses;
      this.fromAddressIndex = fromAddressIndex;
    } catch(e) {
      debug.error('ERROR in getListAddress()', e);
    }
    this.addressLoading = false;
    this.shipping_data.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.updateShippingData();
  }

  setShopAddress() {
    this.shipping_data.pickup_address = this.fromAddresses[this.fromAddressIndex];
    this.updateShippingData();
  }

  updateShippingData(raise_subject = true) {
    this.ordersController.shipping_data = this.shipping_data;
    if (raise_subject) {
      this.ordersController.onUpdateShippingData$.next(this.shipping_data);
    }
  }

  updateTryOn() {
    this.updateShippingData(false);
  }

  updateShippingNote() {
    this.updateShippingData(false);
  }

  toggleUniWeight() {
    this.uni.chargeable_weight = !this.uni.chargeable_weight;
    this.updateUniData();
  }

  toggleUniTryOn() {
    this.uni.try_on = !this.uni.try_on;
    this.updateUniData();
  }

  toggleUniShippingNote() {
    this.uni.shipping_note = !this.uni.shipping_note;
    this.updateUniData();
  }

  updateUniData() {
    this.ordersController.uni = this.uni;
    this.ordersController.onChangeUniData$.next();
  }

  toggleInsurance() {
    this.shipping_data.include_insurance = !this.shipping_data.include_insurance;
    this.updateShippingData();
  }

  confirm() {
    this.createFfm.emit();
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

}
