import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MultiShipnowController {
  onRenderingMap$ = new Subject();
  onSuccessCreatingFFM$ = new Subject();
  onFailedCreatingFFM$ = new Subject();

  constructor() { }
}
