import { Injectable } from '@angular/core';
import { Order } from 'libs/models/Order';
import { SellOrderListComponent } from 'apps/shop/src/app/pages/orders/sell-orders/sell-order-list/sell-order-list.component';
import { AuthenticateStore } from '@etop/core';
import { Subject } from 'rxjs';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { FulfillmentApi } from '@etop/api';
import {TRY_ON} from "@etop/models";

export enum PrintType {
  orders = 'orders',
  fulfillments = 'fulfillments',
  o_and_f = 'o_and_f'
}

@Injectable()
export class SellOrdersController {
  private sellOrderListComponent: SellOrderListComponent;

  createShipmentFfm = false;
  createShipnowFfm = false;

  onRegetShipServices$ = new Subject();
  removeMapMarkers$ = new Subject();
  onChangeTab$ = new Subject();
  createFfmAfterImporting$ = new Subject<Order[]>();

  onUpdateShippingData$ = new Subject();
  shipping_data: {
    pickup_address: any;
    try_on?: string;
    include_insurance?: boolean;
    chargeable_weight?: number;
    shipping_note?: string;
    coupon?: string;
  } = {
    pickup_address: {},
    try_on: this.auth.snapshot.shop.try_on,
    include_insurance: false,
    chargeable_weight: 250,
    shipping_note: '',
    coupon: ''
  };

  onChangeUniData$ = new Subject();
  uni = {
    chargeable_weight: false,
    try_on: false,
    shipping_note: false
  };

  constructor(
    private auth: AuthenticateStore,
    private customerService: CustomerService
  ) { }

  static canCreateFulfillment(order: Order) {
    return (
        !order.activeFulfillment
        || order.activeFulfillment.status == 'N'
        || !order.activeFulfillment.shipping_code
      )
      && ['N', 'NS'].indexOf(order.status) == -1;
  }


  registerOrderList(instance: SellOrderListComponent) {
    this.sellOrderListComponent = instance;
  }

  async reloadOrderList() {
    await this.sellOrderListComponent.reloadOrders();
  }


  setupDataOrder(order: Order): Order {
    order.customer.id = order.customer_id;
    order.p_data = {
      ...order.p_data,
      shipping: {
        ...order.shipping,
        cod_amount: !order.shipping && (order.basket_value + order.total_fee - order.total_discount)
          || order.shipping.cod_amount
      },
      customer: order.customer,
      selected_service: {},
      selected_service_id: '-',
      fee_lines: order.fee_lines,
      total_fee: order.total_fee,
      order_discount: order.order_discount,
      total_discount: order.total_discount,
      total_amount: order.total_amount,
      order_note: order.order_note ? order.order_note : '-',
      ed_code: order.ed_code ? order.ed_code :
        (order.external_id ? order.external_id : '-')
    };
    if (this.uni.chargeable_weight) {
      order.p_data.shipping.chargeable_weight = this.shipping_data.chargeable_weight;
    }
    if (this.uni.try_on) {
      order.p_data.shipping.try_on = this.shipping_data.try_on;
    }
    if (this.uni.shipping_note) {
      order.p_data.shipping.shipping_note = this.shipping_data.shipping_note;
    }
    order.p_data.shipping.try_on_display = TRY_ON[order.p_data.shipping.try_on];
    return order;
  }

  async listOrderCustomerAddresses(orders: Order[]) {
    for (let order of orders) {
      if (!order.customer_id) {
        order.customer.addresses = [];
      } else {
        order.customer.addresses = await this.customerService.getCustomerAddresses(order.customer_id);
      }
    }
  }

  orderToBeUpdated(order): boolean {
    return this.isSelectedOrder(order) && SellOrdersController.canCreateFulfillment(order);
  }

  isCancelledOrder(order: Order): boolean {
    return order.status == 'N' || order.status == 'NS';
  }

  isSelectedOrder(order: Order): boolean {
    return order.p_data.selected && !this.isCancelledOrder(order);
  }

  noAddress(order: Order): boolean {
    const shipping_address = order.p_data.shipping.shipping_address;
    return this.noCustomerAddresses(order) && (
      !shipping_address || !shipping_address.province_code
    );
  }

  noWeight(order: Order): boolean {
    return !order.p_data.shipping.chargeable_weight && !this.uni.chargeable_weight;
  }

  noCustomerAddresses(order: Order): boolean {
    return !order.customer.addresses || !order.customer.addresses.length;
  }

}
