import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellOrdersComponent} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {ReturningFulfillmentsWarningModule} from 'apps/shop/src/app/components/returning-fulfillments-warning/returning-fulfillments-warning.module';
import {SellOrderListComponent} from 'apps/shop/src/app/pages/orders/sell-orders/sell-order-list/sell-order-list.component';
import {SellOrderRowComponent} from 'apps/shop/src/app/pages/orders/sell-orders/components/sell-order-row/sell-order-row.component';
import {MultipleOrderActionsComponent} from 'apps/shop/src/app/pages/orders/sell-orders/components/multiple-order-actions/multiple-order-actions.component';
import {ImportOrderComponent} from 'apps/shop/src/app/pages/orders/sell-orders/components/import-order/import-order.component';
import {SelectAndUpdateCustomerAddressComponent} from 'apps/shop/src/app/pages/orders/sell-orders/components/select-and-update-customer-address/select-and-update-customer-address.component';
import {SellOrdersController} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {MultiShipmentModule} from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipment/multi-shipment.module';
import {MultiShipnowModule} from 'apps/shop/src/app/pages/orders/sell-orders/components/multi-shipnow/multi-shipnow.module';
import {SingleOrderDetailModule} from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-detail/single-order-detail.module';
import {SingleOrderEditFormModule} from 'apps/shop/src/app/pages/orders/sell-orders/components/single-order-edit-form/single-order-edit-form.module';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {AuthenticateModule} from '@etop/core';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BillPrintingModule} from 'apps/shop/src/app/components/bill-printing/bill-printing.module';
import {CancelObjectModule} from 'apps/shop/src/app/components/cancel-object/cancel-object.module';
import {EtopFilterModule} from '@etop/shared/components/etop-filter/etop-filter.module';
import {SideSliderModule} from '@etop/shared/components/side-slider/side-slider.module';
import {EmptyPageModule, EtopCommonModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {PrintModule} from 'apps/shop/src/app/components/print/print.module';
@NgModule({
  declarations: [
    SellOrdersComponent,
    SellOrderListComponent,
    SellOrderRowComponent,
    MultipleOrderActionsComponent,
    ImportOrderComponent,
    SelectAndUpdateCustomerAddressComponent,
  ],
  entryComponents: [
    SelectAndUpdateCustomerAddressComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReturningFulfillmentsWarningModule,
    DropdownActionsModule,
    MultiShipmentModule,
    MultiShipnowModule,
    ReturningFulfillmentsWarningModule,
    SingleOrderDetailModule,
    SingleOrderEditFormModule,
    ModalControllerModule,
    AuthenticateModule,
    RouterModule,
    NgbModule,
    BillPrintingModule,
    CancelObjectModule,
    EtopCommonModule,
    EtopFilterModule,
    SideSliderModule,
    EtopPipesModule,
    PrintModule,
    MaterialModule,
    EmptyPageModule
  ],
  exports: [
    SellOrdersComponent
  ],
  providers: [
    SellOrdersController
  ]
})
export class SellOrdersModule {
}
