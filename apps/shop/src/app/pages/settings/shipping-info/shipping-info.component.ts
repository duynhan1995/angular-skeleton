import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, ConfigService } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { environment } from 'apps/shop/src/environments/environment';
import { ExtendedAccount } from 'libs/models/Account';
import misc from 'apps/core/src/libs/misc';

@Component({
  selector: 'shop-delivery-info',
  templateUrl: './shipping-info.component.html',
  styleUrls: ['./shipping-info.component.scss']
})
export class ShippingInfoComponent implements OnInit {
  currentShop = new ExtendedAccount({});

  constructor(
    private auth: AuthenticateStore,
    private config: ConfigService,
    private util: UtilService
  ) {}

  async ngOnInit() {
    let auth = this.auth.snapshot;
    this.currentShop = auth.shop;
  }

  
  externalLink(link) {
    let isProduction = this.config.getConfig().production || environment.production;
    let base_url = environment.topship_url;
    let access_token = this.auth.snapshot.token;
    let encodeToken = misc.encodeBase64(access_token);
    let _link = `/s/${this.util.getSlug()}/${link}`
    let _encodeLink = misc.encodeBase64(_link);
    window.open(`${base_url}/login-hub?session=${encodeToken}&&domain=eTop&&path=${_encodeLink}`)
  }

}


