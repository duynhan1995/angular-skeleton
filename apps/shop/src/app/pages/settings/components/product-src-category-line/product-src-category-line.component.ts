import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'shop-product-src-category-line',
  templateUrl: './product-src-category-line.component.html',
  styleUrls: ['./product-src-category-line.component.scss']
})
export class ProductSrcCategoryLineComponent implements OnInit {
  @Input() productSrcCategories;
  @Input() parentId;
  @Output() editCat: EventEmitter<any> = new EventEmitter();
  @Output() deleteCat: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onClickEditCat(cat) {
    this.editCat.emit(cat);
  }

  onClickDeleteCat(cat) {
    this.deleteCat.emit(cat);
  }
}
