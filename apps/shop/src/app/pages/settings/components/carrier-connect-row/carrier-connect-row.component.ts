import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Connection } from 'libs/models/Connection';
import { CarrierConnectModalComponent } from 'apps/shop/src/app/pages/settings/components/carrier-connect-modal/carrier-connect-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ConnectionApi } from '@etop/api';
import { DropdownActionOpt } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.interface';
import {ConnectCarrierUsingPhoneModalComponent} from "apps/shop/src/app/pages/settings/components/connect-carrier-using-phone-modal/connect-carrier-using-phone-modal.component";
import {ModalOptions} from "apps/core/src/components/modal-controller/modal.interface";
import { ConnectionService } from '@etop/features/connection/connection.service';

@Component({
  selector: '[shop-carrier-connect-row]',
  templateUrl: './carrier-connect-row.component.html',
  styleUrls: ['./carrier-connect-row.component.scss']
})
export class CarrierConnectRowComponent implements OnInit {
  @Output() reloadConnections = new EventEmitter();
  @Input() connection: Connection;

  dropdownActions: DropdownActionOpt[] = [
    {
      title: 'Gỡ kết nối',
      cssClass: 'text-danger',
      onClick: () => this.deleteShopConnection()
    }
  ];

  constructor(
    private modalController: ModalController,
    private dialogController: DialogControllerService,
    private connectionService: ConnectionService,
    private connectionApi: ConnectionApi
  ) { }

  get notVerified() {
    return this.connection.connect_status == 'P'
      && this.connection.connection_subtype == 'shipnow'
      && !this.connection.external_verified;
  }

  ngOnInit() {}

  carrierConnect(connection: Connection) {
    let modalOptions: ModalOptions = {
      component: CarrierConnectModalComponent,
      componentProps: {
        title: 'Kết nối nhà vận chuyển',
        connection
      }
    };
    switch (connection.connection_provider) {
      case "ghn":
        modalOptions = {
          component: ConnectCarrierUsingPhoneModalComponent,
          componentProps: {
            title: 'Kết nối nhà vận chuyển',
            connection
          }
        };
        break;
      case "ahamove":
        modalOptions = {
          component: ConnectCarrierUsingPhoneModalComponent,
          componentProps: {
            title: 'Kết nối nhà vận chuyển',
            connection,
            usingOTP: false
          }
        };
    }

    const modal = this.modalController.create(modalOptions);
    modal.show().then();
    modal.onDismiss().then(connected => {
      return connected ? this.reloadConnections.emit() : null;
    });
  }

  deleteShopConnection() {
    const dialog = this.dialogController.createConfirmDialog({
      title: `Gỡ kết nối`,
      body: `
        <div>Bạn có chắc muốn gỡ kết nối với nhà vận chuyển <strong>${this.connection.name}</strong>?</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.connectionApi.deleteShopConnection(this.connection.id);
          toastr.success(`Gỡ kết nối với nhà vận chuyển ${this.connection.name_display} thành công.`);
          dialog.close().then();
          this.reloadConnections.emit();
        } catch (e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Gỡ kết nối với nhà vận chuyển ${this.connection.name_display} không thành công.`, e.message || e.msg);
        }
      }
    });
    dialog.show().then();
  }

}
