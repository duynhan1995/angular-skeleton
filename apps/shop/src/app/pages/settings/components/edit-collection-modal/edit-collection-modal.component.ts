import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shop-edit-collection-modal',
  templateUrl: './edit-collection-modal.component.html',
  styleUrls: ['./edit-collection-modal.component.scss']
})
export class EditCollectionModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
