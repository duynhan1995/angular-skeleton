
import { Injectable } from '@angular/core';
import { Attribute, Product, Variant } from 'libs/models/Product';
import { ProductStore } from './product.store.service';
import { ProductApi } from '@etop/api';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private productStore: ProductStore,
    private productApi: ProductApi,
    private ubtService: UserBehaviourTrackingService
    ) { }

  setActiveProduct(activeProduct: Product) {
    this.productStore.setActiveProduct(activeProduct)
  }

  setVariantListForm(variants: Variant[]) {
    this.productStore.setVariantListForm(variants)
  }

  allPossibleCases(attrArr) {
    if (attrArr.length == 1) {
      return attrArr[0].values.map(a => {
        return [{
          name: attrArr[0].name,
          value: a
        }];
      });
    } else {
      let result = [];
      let allCasesOfRest = this.allPossibleCases(attrArr.slice(1));
      for (let i = 0; i < allCasesOfRest.length; i++) {
        for (let j = 0; j < attrArr[0].values.length; j++) {
          let a = JSON.parse(JSON.stringify(allCasesOfRest[i]));
          a.push({
            name: attrArr[0].name,
            value: attrArr[0].values[j]
          });
          result.push(a);
        }
      }
      return result;
    }
  }

  combineVariantFromAttrs(attrs) {
    let allAttrsArray = [];
    attrs.forEach(a => {
      allAttrsArray.push(a.values);
    });
    let variants = [];
    let allAttrsCombine = this.allPossibleCases(attrs);
    allAttrsCombine.forEach(a => {
      let v = new Variant({});
      a.reverse();
      v.code = '';
      v.attributes = a;
      v.checked = true;
      v.retail_price = 0;
      v.cost_price = 0;
      v.quantity = 0;
      variants.push(v);
    });
    return variants;
  }

  async createProduct(data: Product): Promise<any> {
    const res = await this.productApi.createProduct(data);
    if (res.id) {
      this.ubtService.sendUserBehaviour('CreateProduct');
    }
    return res;
  }

  clearCreateProductStore() {
    this.productStore.clearProduct();
  }
}
