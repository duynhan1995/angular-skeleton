import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Product, Variant } from 'libs/models/Product';
import { ShopService } from '@etop/features';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import {
  GoogleAnalyticsService,
  USER_BEHAVIOR
} from 'apps/core/src/services/google-analytics.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ImportProductModalComponent } from '../components/import-product-modal/import-product-modal.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { Filter, Filters } from '@etop/models';
import { ProductsControllerService } from '../products-controller.service';
import { ProductService } from 'apps/shop/src/services/product.service';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { ProductStore } from 'apps/core/src/stores/product.store';
import { takeUntil } from 'rxjs/operators';
import { PrintBarcodeComponent } from 'apps/shop/src/app/pages/products/components/print-barcode/print-barcode.component';
import { ActionButton, EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('productTable', { static: true }) productTable: EtopTableComponent;
  @ViewChild('productSlider', { static: true }) productSlider: SideSliderComponent;
  @ViewChild('printBarcode', { static: false }) printBarcode: PrintBarcodeComponent;

  shop: any = {};
  filters: Filters = [];
  emptyAction: ActionButton[] = [];
  tags = [];

  productList: Array<Product> = [];
  selectedProducts: Array<Product> = [];

  checkAll = false;
  collections;

  account;
  providers = [];

  productSrc: any = {};

  page: number;
  perpage: number;

  checkCreateNew = false;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }
  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }
  allowViewDetail = true;

  constructor(
    private shopService: ShopService,
    private modalController: ModalController,
    private headerController: HeaderControllerService,
    private auth: AuthenticateStore,
    private gaService: GoogleAnalyticsService,
    private productsController: ProductsControllerService,
    private productService: ProductService,
    private dropdownController: DropdownActionsControllerService,
    private productStore: ProductStore
  ) {
    super();
  }

  private _onSelectModeChanged(value) {
    this.productTable.toggleLiteMode(value);
    this.productSlider.toggleLiteMode(value);
  }

  ngOnInit() {
    this.emptyAction = [
      {
        title:'Import sản phẩm',
        cssClass:'btn-primary btn-outline',
        onClick: () => this.showImport()
      },
      {
        title:'Tạo sản phẩm',
        cssClass:'btn-primary',
        onClick: () => this.createNewProduct()
      }
    ]
    this.productsController.loadCategories();
    this.productsController.loadCollections();
    this.productsController.registerProductList(this);
    this.productSrc.id = !this.shop || this.shop.product_source_id == '0' ?
      null : this.shop.product_source_id;

    this.providers = [];
    this.productTable.loading = true;

    this.account = this.auth.snapshot.account;
    this.shop = this.auth.snapshot.shop;
    this.allowViewDetail = this.auth.snapshot.permission.permissions.includes('shop/product/basic_info:view');

    this.headerController.setActions([
      {
        title: 'Import',
        cssClass: 'btn btn-outline btn-primary',
        onClick: () => this.showImport(),
        permissions: ['shop/product:import']
      },
      {
        title: 'Tạo sản phẩm',
        cssClass: 'btn btn-primary',
        onClick: () => this.createNewProduct(),
        permissions: ['shop/product:create']
      }
    ]);

    this.productsController.onCreateProductSuccessful$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.selectMode = false;
        this.productsController.loadCategories();
        this.productsController.loadCollections();
        this.productTable.resetPagination();
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
  }

  async loadProducts(page, perpage) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.productTable.toggleNextDisabled(false);

      const res = await this.productService.getProducts(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
      if (page > 1 && res.products.length == 0) {
        this.productTable.toggleNextDisabled(true);
        this.productTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả sản phẩm.');
        return;
      }

      this.productList = res.products.map(p => this.productsController.setupData(p));

      // Remember here in-order-to reload product_list after updating products
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list products', e);
    }
  }

  async reloadProducts() {
    const { perpage, page } = this;
    const selected_product_ids = this.selectedProducts.map(p => p.id);
    const res = await this.productService.getProducts(
      (page - 1) * perpage,
      perpage,
      this.filters
    );
    const products = res.products;
    this.productList = products.map(p => {
      if (selected_product_ids.indexOf(p.id) != -1) {
        const _product = this.selectedProducts.find(
          selected => selected.id == p.id
        );
        if (_product) {
          p.p_data = _product.p_data;
        }
      }
      return this.productsController.setupData(p);
    });
    this.selectedProducts = this.productList.filter(
      p => p.p_data.selected || p.p_data.detailed
    );
  }

  createNewProduct() {
    this.checkAll = false;
    this.productList.forEach(p => {
      p.p_data.selected = false;
      p.p_data.detailed = false;
    });
    this.checkCreateNew = true;
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.productList.forEach(p => {
      p.p_data.selected = false;
      p.p_data.detailed = false;
    });
    this.checkCreateNew = false;
    this._checkSelectMode();
    this.checkAll = false;
    this.productsController.showPrintBarcode = false;
  }

  detail(event, product: Product) {
    this.checkCreateNew = false;
    if(product?.id == this.selectedProducts[0]?.id) { return; }
    if (event.target.type == 'checkbox' || this.checkedProducts) { return; }
    if (!this.allowViewDetail) { return; }
    this.productList.forEach(p => {
      p.p_data.detailed = false;
    });
    this.checkAll = false;
    product = this.productsController.setupData(product);
    product.p_data.detailed = true;
    this.selectedProducts = [product];
    this._checkSelectMode();
  }

  checkAllProduct() {
    this.checkCreateNew = false;
    this.checkAll = !this.checkAll;
    this.productList.forEach(p => {
      p.p_data.selected = this.checkAll;
    });
    this._checkSelectMode();
  }

  itemChecked(product: Product) {
    this.checkCreateNew = false;
    this.productList.forEach(o => o.p_data.detailed = false);
    product.p_data.selected = !product.p_data.selected;
    if (!product.p_data.selected) {
      this.checkAll = false;
    }
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    this.selectedProducts = this.productList.filter(product =>
      product.p_data.selected || product.p_data.detailed
    );
    this.productStore.changeSelectedProducts(this.selectedProducts);
    this.selectMode = this.selectedProducts.length > 0 || this.checkCreateNew;
    this.setupDropdownActions();
  }

  loadPage({ page, perpage }) {
    setTimeout(async () => {
      this.productTable.loading = true;
      await this.loadProducts(page, perpage);
      this.productTable.loading = false;
    }, 0);
  }

  showImport() {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_MY_SHOP,
      USER_BEHAVIOR.LABEL_IMPORT_PRODUCT
    );

    const modal = this.modalController.create({
      component: ImportProductModalComponent
    });
    modal.show().then();
    modal.onDismiss().then(() => {
      this.productTable.resetPagination();
    });
  }

  filter(filters: Array<Filter>) {
    this.filters = filters;
    this.productTable.resetPagination();
    this.loadProducts(1, this.perpage);
  }

  async deleteProducts() {
    try {
      let ids = this.selectedProducts.map(p => p.id);
      await this.productService.deleteProduct(ids);
      toastr.success('Xóa sản phẩm thành công');
      this.checkAll = false;
      this.productTable.resetPagination();
    } catch (e) {
      debug.error('ERROR in deleting products', e);
      toastr.error(e, 'Xóa sản phẩm không thành công');
    }
  }

  private setupDropdownActions() {
    if (!this.selectedProducts.length) {
      this.dropdownController.clearActions();
      return;
    }
    this.dropdownController.setActions([
      {
        onClick: () => this.deleteProducts(),
        title: 'Xoá sản phẩm',
        cssClass: 'text-danger',
        permissions: ['shop/product:delete']
      }
    ]);
  }

  undoPrintBarcode() {
    if (!this.printBarcode) { return; }
    if (this.printBarcode.showOptions) {
      this.printBarcode.showOptions = false;
      return;
    }
    this.productsController.showPrintBarcode = false;
  }

  get showPrintBarcode() {
    return this.productsController.showPrintBarcode;
  }

  get variantsBarcodeNumber() {
    return this.selectedProducts.reduce((a, b) => a + b.variants.length, 0);
  }

  get sliderTitle() {
    if (this.checkCreateNew) {
      return 'Tạo sản phẩm mới';
    }
    if (this.selectedProducts.length == 1 && this.showDetailProduct) {
      return 'Chi tiết sản phẩm';
    }
    else if (this.showPrintBarcode) {
      return `In mã vạch cho <span class="text-bold">${
        this.variantsBarcodeNumber
      }</span> mẫu mã sản phẩm`;
    }
    else {
      return `Thao tác trên <span class="text-bold">${
        this.selectedProducts.length
        }</span> sản phẩm`;
    }
  }

  get emptyResultFilter() {
    return this.page == 1 && this.productList.length == 0 && this.filters.length > 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy sản phẩm phù hợp';
    }
    return 'Cửa hàng của bạn chưa có sản phẩm';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get showDetailProduct() {
    return this.selectedProducts.length && this.selectedProducts.length == 1 && !this.checkedProducts;
  }

  get checkedProducts() {
    return this.productList.some(o => o.p_data.selected);
  }

  get showPaging() {
    return !this.productTable.liteMode && !this.productTable.loading;
  }

}
