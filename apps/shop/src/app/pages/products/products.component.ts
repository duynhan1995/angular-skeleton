import { Component, OnInit } from '@angular/core';
import { FilterOperator, FilterOptions } from '@etop/models';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';

@Component({
  selector: 'shop-products',
  template: `
<!--    <shared-filter [filters]="filters" (filterChanged)="productList.filter($event)"></shared-filter>-->
    <div class="page-content pb-0">
      <shop-product-list #productList></shop-product-list>
    </div>
  `
})
export class ProductsComponent extends PageBaseComponent implements OnInit {
  filters: FilterOptions = [
    {
      label: 'Tên sản phẩm',
      name: 'name',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
    {
      label: 'Mã sản phẩm',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.contains
    },
    {
      label: 'Giá bán lẻ',
      name: 'price',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
