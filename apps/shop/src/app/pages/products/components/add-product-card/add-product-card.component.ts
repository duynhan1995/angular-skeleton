import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CurrencyInputComponent } from '../../../../../../../../libs/shared/components/etop-forms/currency-input/currency-input.component';
import { Product } from '../../../../../../../../libs/models/Product';
import { UtilService } from '../../../../../../../core/src/services/util.service';

@Component({
  selector: 'shop-add-product-card',
  templateUrl: './add-product-card.component.html',
  styleUrls: ['./add-product-card.component.scss']
})
export class AddProductCardComponent implements OnInit {
  @ViewChild('inputName', {static: false}) inputName: ElementRef;
  @ViewChild('inputPrice', {static: false}) inputPrice: CurrencyInputComponent;

  @Input() product = new Product({});
  @Input() index: number;
  @Input() activeLine: string;
  @Input() errorName = false;
  @Input() errorPrice = false;
  @Input() haveCheckbox = false;

  @Output() nextProduct = new EventEmitter();
  @Output() removeProduct = new EventEmitter();
  @Output() focusName = new EventEmitter();
  @Output() focusPrice = new EventEmitter();

  show_attributes = false;
  to_save_product = false;
  attribute_value = '';
  attribute_name = 'Màu sắc';

  constructor(
    private util: UtilService
  ) { }

  ngOnInit() {
  }

  async onChangeLogo(event, product: Product) {
    const $parent = $(event.target).parent();
    $parent.addClass('loading');
    const { files } = event.target;
    const res = await this.util.uploadImages([files[0]], 1024);
    product.image = res[0].url;
    $parent.removeClass('loading');
  }

  onFocusInputName(code) {
    this.focusName.emit(code);
  }

  onFocusInputPrice(code) {
    this.focusPrice.emit(code);
  }

  onFocusPrice(event) {
    this.inputPrice.showEdit(event);
  }

  onKeyDown(event, index) {
    const { key } = event;
    if (key === 'Tab' || key === 'Enter') {
      event.preventDefault();
      this.nextProduct.emit(index + 1);
    }
  }

  toggleAttribute() {
    this.show_attributes = !this.show_attributes;
  }

  removeAttribute(product) {
    product.attributes = [];
    this.toggleAttribute();
  }

  setAttribute(product) {
    if (this.attribute_value) {
      product.attributes = [{ name: this.attribute_name, value: this.attribute_value }];
    } else {
      product.attributes = [];
    }
    this.toggleAttribute();
  }

  remove(index) {
    this.removeProduct.emit(index);
  }

}
