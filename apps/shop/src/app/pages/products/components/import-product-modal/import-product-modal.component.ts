import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductApi, StocktakeApi } from '@etop/api';
import { ProductsControllerService } from 'apps/shop/src/app/pages/products/products-controller.service';

@Component({
  selector: 'shop-import-product-modal',
  templateUrl: './import-product-modal.component.html',
  styleUrls: ['./import-product-modal.component.scss']
})
export class ImportProductModalComponent implements OnInit {

  productSrcImportResult = [];
  productSrcFile = null;
  importing = false;
  done = false;

  constructor(
    private modalAction: ModalAction,
    private productApi: ProductApi,
    private stocktakeApi: StocktakeApi,
    private productsController: ProductsControllerService
  ) {
  }

  ngOnInit() {
  }

  closeModal() {
    if (this.done) {
      this.modalAction.dismiss(null);
    } else {
      this.modalAction.close(false);
    }
  }

  uploadExcelFile(event) {
    try {
      if (event.target.files && event.target.files.length > 0) {
        if (this.productSrcFile?.name != event.target.files[0].name) {
          this.done = false;
        }
        this.productSrcFile = event.target.files[0];
      }
    } catch (error) {
      toastr.error(error.message);
    }
  }

  async import() {
    if (!this.productSrcFile) {
      toastr.error("Vui lòng chọn file import!");
    }
    try {
      this.importing = true;
      const formData = new FormData();

      formData.append('files', this.productSrcFile);

      const res = await this.productApi.importProductFromExcel(formData);
      const { cell_errors, import_errors, stocktake_id } = res;
      cell_errors.forEach(err => {
        if (err.code !== 'ok') {
          this.addToImportResult('error', `Lỗi ở hàng ${err.meta.row}, cột ${err.meta.col}: ${err.msg}`);
        }
      });
      import_errors.forEach(err => {
        if (err.code !== 'ok') {
          this.addToImportResult('error', err.msg);
        } else {
          this.addToImportResult('success', err.msg);
        }
      });

      if (stocktake_id && stocktake_id != "0") {
        await this.confirmStocktake(stocktake_id);
      }

      await this.productsController.reloadProductList()

      if(cell_errors.length > 0) {
        this.done = false;
      } else {
        this.done = true;
      }

    } catch (e) {
      toastr.error(e && e.message);
    }
    this.importing = false;
  }


  async confirmStocktake(stocktake_id) {
    try {
      await this.stocktakeApi.confirmStocktake(stocktake_id, "confirm");
      toastr.success('Tạo phiếu kiểm kho thành công.');
    } catch(e) {
      toastr.error('Tạo phiếu kiểm kho không thành công.');
      debug.error('ERROR in confirming Stocktake when Importing', e);
    }
  }

  addToImportResult(type, content) {
    let className = '';
    let icon = '';
    switch (type) {
      case "success":
        icon = 'fa-check-circle-o';
        className = 'alert-success';
        break;
      case "error":
        icon = 'fa-times-circle';
        className = "alert-danger";
        break;
      case "warning":
        icon = 'fa-exclamation-triangle';
        className = 'alert-warning';
        break;
    }
    this.productSrcImportResult.push({
      type,
      content: `<i class="fa ${icon}" aria-hidden="true"></i> ${content}`,
      className,
    });
  }
}
