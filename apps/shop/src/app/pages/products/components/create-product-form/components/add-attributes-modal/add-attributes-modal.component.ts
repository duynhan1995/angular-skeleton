import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { FormBuilder } from '@angular/forms';
import { ProductService } from '../../../../product-store/product.service';

@Component({
  selector: 'shop-add-attributes-modal',
  templateUrl: './add-attributes-modal.component.html',
  styleUrls: ['./add-attributes-modal.component.scss']
})
export class AddAttributesModalComponent implements OnInit {
  addAttrForm = this.fb.array([])

  addingAttr = false;
  format_money = {
    retail_price: true,
    cost_price: true,
  };
  price_error = {
    retail_price: false,
    cost_price: false,
  };

  constructor(
    private modalDismiss: ModalAction,
    private modalAction: ModalAction,
    private productService: ProductService,
    private fb: FormBuilder,
    private util: UtilService
  ) {
  }

  ngOnInit() {
  }

  addAttribute() {
    this.addAttrForm.push(
      this.fb.group({
        name: '',
        values: [],
      })
    )
  };

  get attributeLength() {
    return this.addAttrForm.controls.length;
  }

  removeAttribute(attrIndex) {
    this.addAttrForm.removeAt(attrIndex);
  }

  submit() {
    const attrs = this.addAttrForm.value;
    const attrNames = attrs.map(attr => attr.name);
    if(this.util.isDuplicateExists(attrNames)) {
      toastr.error("Tên thuộc tính không được trùng nhau. Vui lòng kiểm tra lại.");
      return;
    }
    if (!attrs.length) {
      toastr.error('Vui lòng thêm thuộc tính trước khi tiếp tục');
      this.addingAttr = false;
      return;
    } else {
      for (const [i, attr] of attrs.entries()) {
        if (!attr.name || !attr?.values?.length) {
          toastr.error(
            `Vui lòng nhập tên và giá trị cho thuộc tính thứ ${i + 1}.`
          );
          this.addingAttr = false;
          return;
        }
      }
    }
    const variants = this.productService.combineVariantFromAttrs(attrs);
    this.modalAction.dismiss(variants);
  }
  closeModal() {
    this.modalAction.close(false);
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  cancel() {
    this.modalDismiss.dismiss(null);
  }
}
