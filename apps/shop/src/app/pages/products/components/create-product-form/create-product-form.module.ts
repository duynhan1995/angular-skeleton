import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../../../../shared/src/shared.module';
import { CreateProductFormComponent } from './create-product-form.component';
import { AddAttributesModalComponent } from './components/add-attributes-modal/add-attributes-modal.component';
import { AddAttributeRowComponent } from './components/add-attribute-row/add-attribute-row.component';
import { AuthenticateModule } from '@etop/core';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { UpdateAttributeModalComponent } from './components/update-attribute-modal/update-attribute-modal.component';
import { ConfirmAttributesModalComponent } from './components/confirm-attributes-modal/confirm-attributes-modal.component';
import { VariantLinesComponent } from './components/variant-lines/variant-lines.component';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, AuthenticateModule, EtopPipesModule, EtopMaterialModule, ReactiveFormsModule],
  exports: [CreateProductFormComponent],
  entryComponents: [AddAttributesModalComponent],
  declarations: [CreateProductFormComponent, AddAttributesModalComponent, AddAttributeRowComponent, UpdateAttributeModalComponent, ConfirmAttributesModalComponent, VariantLinesComponent],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateProductFormModule {}
