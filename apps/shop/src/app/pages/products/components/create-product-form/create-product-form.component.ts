import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { InventoryVariant, Product, ShopProduct, Variant } from 'libs/models/Product';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AddAttributesModalComponent } from './components/add-attributes-modal/add-attributes-modal.component';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { ProductsControllerService } from '../../products-controller.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ProductService } from 'apps/shop/src/services/product.service';
import { AddCategoryModalComponent } from 'apps/shop/src/app/pages/products/components/add-category-modal/add-category-modal.component';
import { AddCollectionsModalComponent } from 'apps/shop/src/app/pages/products/components/add-collections-modal/add-collections-modal.component';
import { CategoryService } from 'apps/shop/src/services/category.service';
import { CollectionService } from 'apps/shop/src/services/collection.service';
import { StocktakeLine } from 'libs/models/Stocktake';
import { InventoryApi, ProductApi, ProductTagsUpdateDto, StocktakeApi } from '@etop/api';
import { FormBuilder } from '@angular/forms';
import { ProductStore } from '../../product-store/product.store.service';
import { ProductService as ProductStoreService} from '../../product-store/product.service';
import { takeUntil } from 'rxjs/operators';
import { UpdateAttributeModalComponent } from './components/update-attribute-modal/update-attribute-modal.component';
import { ConfirmAttributesModalComponent } from './components/confirm-attributes-modal/confirm-attributes-modal.component';
import { AddVariantModalComponent } from '../single-product-edit-form/components/add-variant-modal/add-variant-modal.component';
import {Collection} from "libs/models/Collection";
import {Category} from "libs/models/Category";
import { MaterialChipsAutocompleteComponent } from '@etop/shared';
@Component({
  selector: 'shop-create-product-form',
  templateUrl: './create-product-form.component.html',
  styleUrls: ['./create-product-form.component.scss']
})
export class CreateProductFormComponent extends BaseComponent implements OnInit {
  @ViewChild('retailInput', { static: false }) retail_price_input: MaterialInputComponent;
  @ViewChild('costInput', { static: false }) cost_price_input: MaterialInputComponent;
  @ViewChild('categoryInput', { static: false}) category_input: MaterialChipsAutocompleteComponent
  @ViewChild('moneyInput', { static: false }) moneyInput: MaterialInputComponent;
  creatingProduct = false;
  createProductForm = this.fb.group({
    id: [''],
    productName: [''],
    productCode: [''],
    retail_price: [''],
    cost_price: '',
    inventory_quantity: 0,
    image: '',
    tags: [],
    category_id: '',
    category: '',
    collection_ids: [],
    collections: [],
    description: [''],
    variants: [[]],
  })

  format_money = {
    retail_price: true,
    cost_price: true,
  };
  price_error = {
    retail_price: false,
    cost_price: false,
  };

  categoryDisplayMap = option => option && option.name || null;
  categoryValueMap = option => option && option.id || null;
  collectionDisplayMap = option => option && option.name || null;
  collectionValueMap = option => option && option.id || null;

  constructor(
    private util: UtilService,
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private productsController: ProductsControllerService,
    private promiseQueue: PromiseQueueService,
    private categoryService: CategoryService,
    private productService: ProductService,
    private productApi: ProductApi,
    private collectionService: CollectionService,
    private changeDetector: ChangeDetectorRef,
    private stocktakeApi: StocktakeApi,
    private inventoryApi: InventoryApi,
    private fb: FormBuilder,
    private productStoreService: ProductStoreService,
    private productStore: ProductStore,
  ) {
    super();
  }

  ngOnInit() {
    this.productStoreService.clearCreateProductStore();
    this.createProductForm.valueChanges.subscribe(value => {
      this.productStoreService.setActiveProduct(new Product({
        image_urls: [value?.image],
        name: value?.productName,
        description: value?.description,
        variants: value?.variants,
        tags: value?.tags,
        code: value?.productCode,
        retail_price: value?.retail_price,
        cost_price: value?.cost_price,
        inventory_quantity: value?.inventory_quantity,
        collection_ids: value?.collection_ids,
        collections: value?.collections,
        category_id: value?.category_id,
        category: value?.category,
      }));
    });
    this.format_money = {
      retail_price: this.product.retail_price > 0,
      cost_price: this.product.cost_price > 0,
    };
    this.productStore.forceUpdateCreateProductForm$
    .pipe(takeUntil(this.destroy$))
    .subscribe(_ => {
      this.getFormDataFromStore();
    });
  }


  get productVariantAttributes() {
    return this.product.variants[0]
      && this.product.variants[0].attributes
      && this.product.variants[0].attributes.map(a => a.name) || [];
  }
  get product() {
    return this.productStore.snapshot.product;
  }

  getFormDataFromStore() {
    const product = this.productStore.snapshot.product;
    this.createProductForm.patchValue({
      productName: product.name,
      productCode: product.code,
      retail_price: product.price,
      image: product.image,
      tags : product.tags,
      description: product.description,
      variants: product.variants,
      collection_ids: product.collection_ids,
      collections: product.collections,
      category_id: product.category_id,
      category: product.category,
    }, {emitEvent: false});
  }

  async checkAndCreateCategory(product: Product) {
    try {
      let category_id = product.category && product.category.id  || product.category_id[0];
      let category_name = product.category && product.category.name;
      if (!category_name && !category_id) {
        // this.category_input.resetSearchValue();
        return;
      }
      if (!category_id || category_id.includes('###')) {
        const res = await this.categoryService.createCategory({
          name: category_name
        });
        category_id = res.id;
      }
      await this.productService.updateProductCategory({
        category_id,
        product_id: product.id
      });
    } catch (e) {
      debug.error('ERROR in checking and creating category', e);
    }
  }

  async checkAndCreateCollections(product: Product) {
    try {
      if ((!product.collections || !product.collections.length) && (!product.collection_ids || !product.collection_ids.length)) {
        return;
      }

      const toCreateCollections = product.collections?.filter(collection => !collection.id || collection.id.includes('###')) || [];
      const newCollectionIds = product.collection_ids?.filter(coll_id => coll_id && !coll_id.includes('###'));

      const promises = toCreateCollections.map(coll => async () => {
        const res = await this.collectionService.createCollection({ name: coll.name });
        newCollectionIds.push(res.id);
      });
      await this.promiseQueue.run(promises, 10);
      if (newCollectionIds && newCollectionIds.length) {
        await this.productService.addProductCollection(product.id, newCollectionIds);
      }
    } catch (e) {
      debug.error('ERROR in checking and creating collections', e);
    }
  }

  async checkAndCreateTags(product: Product) {
    try {
      const tags = product.tags;
      let tagsData: ProductTagsUpdateDto = {
        ids: [product.id]
      };
      if (tags && tags.length) {
        tagsData.replace_all = tags;
      } else {
        tagsData.delete_all = true;
      }
      await this.productService.updateProductTags(tagsData);
    } catch (e) {
      debug.error('ERROR in checking and creating tags', e);
    }
  }

  resetData() {
    this.productStoreService.clearCreateProductStore();
    this.createProductForm.patchValue({
      id: '',
      productName: '',
      productCode: '',
      retail_price: '',
      cost_price:'',
      inventory_quantity: 0,
      image: '',
      tags: [],
      category: '',
      category_id:'',
      collection_ids: [],
      collections: [],
      description: '',
      variants: [],
    })
    this.format_money = {
      retail_price: false,
      cost_price: false,
    };
  }

  async create() {
    if (!this.product.name) {
      toastr.remove();
      toastr.error('Chưa nhập tên sản phẩm!');
      return;
    }
    if (!this.product.retail_price && (!this.product.variants || !this.product.variants.length)) {
      toastr.remove();
      toastr.error('Chưa nhập giá bán sản phẩm!');
      return;
    }

     if (!this.product.variants.every(vl => vl.code) && !this.product.variants.every(vl => !vl.code)) {
      this.product.variants.forEach((v, i) => {
        if(!v.code) {
          toastr.error(`Vui lòng nhập mã mẫu mã cho mẫu mã thứ ${i+1}.`);
        }
      })
      return;
    }

    if(!this.product.variants.every(v => v.retail_price)) {
      this.product.variants.forEach((vl, i) => {
        if(!vl.retail_price) {
          toastr.error(`Vui lòng nhập giá bán cho mẫu mã thứ ${i+1}.`);
        }
      })
      return;
    }
    this.creatingProduct = true;
    try {
      const {  image_urls, description, name, code } = this.product;

      const body : any = {
        image_urls: image_urls,
        name: name,
        code: code,
        description: description
      };
      let product: any;
      if (!this.product.id) {
        product = await this.productService.createProduct(body);
      } else {
        product = await this.productApi.updateProduct({
          ...body,
          id: this.product.id
        });
      }
      await this.createVariants(product);
      await this.checkAndCreateCategory({ ...this.product, id: product.id });
      await this.checkAndCreateCollections({ ...this.product, id: product.id });
      await this.checkAndCreateTags({ ...this.product, id: product.id });
      toastr.success('Tạo sản phẩm thành công!');
      this.productsController.onCreateProductSuccessful$.next();
      this.resetData();
    } catch (e) {
      debug.error('ERROR in creating product', e);
      toastr.error('Tạo sản phẩm thất bại', e.msg || e.message);
    }
    this.creatingProduct = false;
  }

  async createVariants(product: Product | ShopProduct) {
    try {
      let variants: Variant[] = [];
      if (!this.product.variants || !this.product.variants.length) {
        const body = new Variant({
          product_id: product.id,
          retail_price: this.product.retail_price,
          list_price: this.product.retail_price,
          image_urls: product.image_urls,
        });
        const res = await this.productService.createVariant(body);
        variants.push({
          ...res,
          inventory_variant: {
            ...new InventoryVariant(),
            quantity: this.product.inventory_quantity,
            cost_price: this.product.cost_price
          }
        });
      } else {
        let success = 0;
        const promises = this.product.variants.map(variant => async() => {
          try {
            const body = new Variant({
              product_id: product.id,
              retail_price: variant.retail_price,
              list_price: variant.list_price,
              image_urls: product.image_urls,
              attributes: variant.attributes,
              code: variant.code
            });
            const res = await this.productService.createVariant(body);
            variants.push({
              ...res,
              inventory_variant: {
                ...new InventoryVariant(),
                quantity: variant.quantity,
                cost_price: variant.cost_price
              }
            });
            success += 1;
          } catch(e) {
            debug.log('ERROR in updating variants', e);
          }
        })
        await this.promiseQueue.run(promises, 1);
        if (success == this.product.variants.length) {
          toastr.success('Tạo mẫu mã sản phẩm thành công.');
        } else if (success > 0) {
          toastr.warning(`Tạo thành công ${success}/${this.product.variants.length} mẫu mã sản phẩm.`);
        } else {
          toastr.error('Tạo mẫu mã sản phẩm thất bại.');
        }
      }
      if (variants.length) {
        await this.updateInventoryVariantCostPrice(variants);
        await this.createStocktake(variants);
      }
    } catch (e) {
      debug.error('ERROR in creating variants', e);
      throw e;
    }
  }

  async updateInventoryVariantCostPrice(variants: Variant[]) {
    try {
      const promises = variants.filter(v => v.inventory_variant.cost_price >= 0)
        .map(v => async() => {
          try {
            await this.inventoryApi.updateInventoryVariantCostPrice(v.id, v.inventory_variant.cost_price);
          } catch(e) {
            debug.error('ERROR in updating InventoryVariantCostPrice inside Promise.all', e);
          }
        });
      await this.promiseQueue.run(promises, 5);
    } catch(e) {
      debug.error('ERROR in updating InventoryVariantCostPrice', e);
    }
  }

  async createStocktake(variants: Variant[]) {
    try {
      let lines: StocktakeLine[] = variants.filter(v => v.inventory_variant.quantity >= 0)
        .map(v => {
          return {
            ...new StocktakeLine(),
            old_quantity: 0,
            new_quantity: v.inventory_variant.quantity,
            variant_id: v.id,
            image_url: v.image_urls[0]
          };
        });
      if (!lines.length) {
        return;
      }
      const body = {
        total_quantity: lines.reduce((a, b) => a + Number(b.new_quantity || 0), 0),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(body);
      await this.stocktakeApi.confirmStocktake(stocktake.id, "confirm");
      toastr.success('Tạo phiếu kiểm kho thành công.');
    } catch(e) {
      toastr.error('Tạo phiếu kiểm kho không thành công.', e);
      debug.error('ERROR in creating stocktake', e);
    }
  }

  async onChangeLogo(event) {
    try{
      const $parent = $(event.target).parent();
      $parent.addClass('loading');
      const { files } = event.target;
      const res = await this.util.uploadImages([files[0]], 1024);
      this.createProductForm.patchValue({
        image: res[0].url,
      })
      $parent.removeClass('loading');
    }
    catch(e){
      if(!e.mesage){
        toastr.error("Đã có lỗi xảy ra");
      }
      else{
        toastr.error(e.mesage);
      }
      const $parent = $(event.target).parent();
      $parent.removeClass('loading');
    }
  }

  addCategory() {
    let modal = this.modalController.create({
      component: AddCategoryModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then((category: Category) => {
      debug.log('category', category);
      this.createProductForm.patchValue({
        category_id: category.id,
        category,
      });
    });
  }

  addCollections() {
    let modal = this.modalController.create({
      component: AddCollectionsModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then((collections: Collection[]) => {
      if (!this.product.collections) {
        this.product.collections = collections;
      } else {
        this.product.collections.push(...collections);
      }
      this.createProductForm.patchValue({
        collection_ids: collections.map(col => col.id),
        collections
      });
    });
  }

  formatMoney(type) {
    if (!this.product[type]) {
      this.format_money[type] = false;
      return;
    }
    this.format_money[type] = !this.format_money[type];
    this.changeDetector.detectChanges();
    if (!this.format_money[type] && this[`${type}_input`]) {
      this[`${type}_input`].focusInput();
    }
  }

  changePrice(type) {
    if (!Number(this.product[type]) && type == 'retail_price') {
      this.price_error[type] = true;
      toastr.error('Giá bán không được bằng 0!');
    } else {
      this.price_error[type] = false;
      this.product[type] = this.product[type];
    }
  }

  get categories() {
    return this.productsController.categories;
  }

  get collections() {
    return this.productsController.collections;
  }

  get canCreateCategory() {
    return this.auth.snapshot.permission.permissions.indexOf('shop/category:create') != -1;
  }

  get canCreateCollection() {
    return this.auth.snapshot.permission.permissions.indexOf('shop/collection:create') != -1;
  }

  get category_placeholder() {
    if (this.categories && this.categories.length > 0) {
      return 'Chọn danh mục';
    }
    if (!this.canCreateCategory) {
      return 'Chọn danh mục';
    }
    return 'Bấm (+) để thêm mới';
  }

  get collection_placeholder() {
    if (this.collections && this.collections.length > 0) {
      return 'Chọn bộ sưu tập';
    }
    if (!this.canCreateCollection) {
      return 'Chọn bộ sưu tập';
    }
    return 'Bấm (+) để thêm mới';
  }

  openAddAttributesPopup(e ,isDeclareAttrsAgain = false) {
    e.preventDefault();
    if((!this.product.variants.length) || (this.product.variants.length && isDeclareAttrsAgain==true) ) {
      const modal = this.modalController.create({
        component: AddAttributesModalComponent,
        cssClass: 'modal-lg'
      });
      modal.show().then();
      modal.onDismiss().then(variants => {
        if (variants) {
          this.productStoreService.setVariantListForm(variants);
          this.openEditVariantModal(e);
        }
      });
    } else {
      const modal = this.modalController.create({
        component: AddVariantModalComponent,
        componentProps: {
          attributeNames : this.productVariantAttributes,
          product: this.product,
          fromCreateProductForm: true
        },
        cssClass: 'modal-lg'
      });
      modal.onDismiss().then((data:any) => {
        if (data?.variant) {
          this.product.variants.push({
            ...data.variant,
            cost_price: data.variant.inventory_variant.cost_price,
            quantity: data.variant.inventory_variant.quantity,
          });

          this.productStoreService.setVariantListForm(this.product.variants);
        }
      });
      modal.show().then();
    }

  }

  openEditVariantModal($e) {
    const variants = this.productStore.snapshot.variantListForm;
    const modal = this.modalController.create({
      cssClass: 'modal-lg',
      component: ConfirmAttributesModalComponent,
      componentProps: {
        variants: variants
      }
    });
    modal.show();
    modal.onDismiss().then(data => {
      if(!data) {
        return;
      }
      if (data.action == 'edit-attr') {
        return this.openEditAttributeModal($e);
      }
      if (data.action == 'submit') {
        this.createProductForm.patchValue({ variants: data.data})
      }
    })
  }

  openEditAttributeModal($e, fromConfirmAttrForm = true) {
    const attrs = this.productStore.snapshot.variantListForm[0]?.attributes.map(attr => new Object({
      name: attr.name,
      oldName: attr.name
    })) || [];
    const modal = this.modalController.create({
      cssClass: 'modal-md',
      component: UpdateAttributeModalComponent,
      componentProps: {
        attributes: attrs,
        fromConfirmAttrForm: fromConfirmAttrForm
      }
    });
    modal.show();
    modal.onDismiss().then(res => {
      if (res && res.changes) {
        const variants = [...this.productStore.snapshot.variantListForm];
        res.changes.forEach(change => {
          variants.forEach(variant => {
            switch (change.action) {
              case 'delete':
                if(change.oldName){
                  variant.attributes.splice(variant.attributes.findIndex(attr => attr.name == change.oldName), 1);
                }
                break;
              case 'update':
                variant.attributes.find(attr => attr.name == change.oldName).name = change.name;
                break;
              case 'add':
                variant.attributes.push({ name: change.name, value: '' });
                break;
            }
          })
        })
        this.productStoreService.setVariantListForm(variants)
      }
      if(res?.openAddAttributeModal) {
        this.openAddAttributesPopup($e, true);
      }
      if(fromConfirmAttrForm) {
        this.openEditVariantModal($e);
      }
    })
  }

  onDataEdited(variant) {
    variant.p_data.edited = true;
  }

}
