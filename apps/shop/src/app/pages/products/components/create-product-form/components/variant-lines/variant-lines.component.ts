import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ValueAccessorBase } from 'apps/core/src/interfaces/ValueAccessorBase';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Variant } from '@etop/models';


export class VariantLine extends Variant {
  isChecked: boolean;
  skuError: any;
  retail_price_error: boolean;
}

@Component({
  selector: 'shop-variant-lines',
  templateUrl: './variant-lines.component.html',
  styleUrls: ['./variant-lines.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: VariantLinesComponent, multi: true}
  ]
})
export class VariantLinesComponent extends ValueAccessorBase<VariantLine[]>  implements OnInit {
  @Output() checkVariantLine  :EventEmitter<any> = new EventEmitter();
  format_money = {
    retail_price: true,
    cost_price: true,

  };

  price_error = {
    retail_price: false,
    cost_price: false,
  };

  constructor() {
    super();
  }

  ngOnInit() {
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs.map(a => {
      return a.value;
    }).join(' - ');
  }

  changePrice(variant,type) {
    if (!Number(variant[type]) && type == 'retail_price') {
      this.price_error[type] = true;
      toastr.error('Giá bán không được bằng 0!');
    } else {
      this.price_error[type] = false;
    }
  }

  onCheckVariant() {
    this.checkVariantLine.emit();
  }

  checkCode(variant) {
    variant.requireCode = variant?.isChecked ? !! variant?.code : false;
  }

  invokeChange() {
    this.value = this.value.slice(0);
  }

  onRemoveVariant(index) {
    this.value.splice(index, 1);
  }

  clearSkuError(variant) {
    variant.skuError = false;
  }

  clearRetailPriceError(variant) {
    variant.retail_price_error = false;
  }
}
