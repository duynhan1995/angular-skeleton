import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { Attribute } from '@etop/models';
import { ProductStore } from '../../../../product-store/product.store.service';
import { ProductService } from '../../../../product-store/product.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-update-attribute-modal',
  templateUrl: './update-attribute-modal.component.html',
  styleUrls: ['./update-attribute-modal.component.scss']
})
export class UpdateAttributeModalComponent implements OnInit {
  @Input() attributes: Attribute[];
  @Input() fromConfirmAttrForm: boolean;
  deletedAttrArr: Attribute[] = [];
  updateAttrForm = this.fb.array([
  ])

  constructor(
      private fb: FormBuilder,
      private productStore: ProductStore,
      private productService: ProductService,
      private modalAction: ModalAction,
      private modalController: ModalController,
      private dialog: DialogControllerService,
      private util: UtilService,
    ) { }

  ngOnInit() {
    this.attributes.forEach(attr => {
      this.updateAttrForm.push(this.fb.group({
        oldName: attr.name,
        name: attr.name,
      }));
    })
  }

  addAttribute() {
    this.updateAttrForm.push(this.fb.group({
      oldName: '',
      name: '',
    }))
  }

  removeAttribute(attr, index) {
    this.updateAttrForm.removeAt(index);
    this.deletedAttrArr.push(attr);
  }

  get attributeLength() {
    return this.updateAttrForm.value.length;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  cancel() {
    this.modalAction.dismiss(null);
  }

  openConfirmAttributeDialog() {
    const modal = this.dialog.createConfirmDialog({
      title: `Kích hoạt giao lại`,
      body: `
<div class="text-danger"><i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i> Các mẫu mã sẽ được xoá và tổ hợp lại theo các giá trị mới được khai báo</div>
      `,
      cancelTitle: 'Đóng',
      confirmTitle: 'Xác nhận',
      confirmCss: 'btn btn-primary',
      closeAfterAction: false,
      onConfirm: async() => {
        this.modalAction.dismiss({openAddAttributeModal: true})
        modal.close().then();
      }
    });
    modal.show().then();
  }

  submit() {
    const changes = this.updateAttrForm.value;
    const attrNames = changes.map(attr => attr.name);
    if(this.util.isDuplicateExists(attrNames)) {
      toastr.error("Tên thuộc tính không được trùng nhau. Vui lòng kiểm tra lại.");
      return;
    }

    this.modalAction.dismiss(
      {changes: [].concat(this.deletedAttrArr.map(attr => Object.assign(attr, {action: 'delete'})))
      .concat(changes.filter(attr => attr.oldName && attr.name &&  attr.oldName != attr.name).map(attr => Object.assign(attr, {action: 'update'})))
      .concat(changes.filter(attr => !attr.oldName && attr.name).map(attr => Object.assign(attr, {action: 'add'}))) }
    )

  }
}

