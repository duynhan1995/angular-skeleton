import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Variant } from 'libs/models/Product';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { FormBuilder, FormArray, FormControl } from '@angular/forms';
import { ProductStore } from '../../../../product-store/product.store.service';
import { ProductService } from '../../../../product-store/product.service';
import { VariantLine } from '../variant-lines/variant-lines.component';
import { VariantDisplayPipe } from '@etop/shared';

@Component({
  selector: 'shop-confirm-attributes-modal',
  templateUrl: './confirm-attributes-modal.component.html',
  styleUrls: ['./confirm-attributes-modal.component.scss']
})
export class ConfirmAttributesModalComponent implements OnInit {
  @Input() variants: Variant[] = [];
  @Input() isVariantEdit = false;
  newVariantAmount = 0;
  seriesValues = {
    retail_price: '',
    cost_price: '',
    quantity: '',
  }
  confirmVariantForm = this.fb.group({
    isCheckAll: true,
    retail_price: [''],
    cost_price: [''],
    quantity: [''],
    variant_lines: [[]]
  });

  attributeNames = [];

  get variant_lines() {
    return this.confirmVariantForm.get('variant_lines') as FormControl;
  }

  isCheckAll = true;
  format_money = {
    retail_price: true,
    cost_price: true
  };

  price_error = {
    retail_price: false,
    cost_price: false
  };

  constructor(
    private modalAction: ModalAction,
    private fb: FormBuilder,
    private productService: ProductService
  ) {}

  ngOnInit() {
    this.attributeNames = this.variants[0].attributes.map(attr => attr.name);
    this.variant_lines.setValue(
      this.variants.map(
        v =>
          new Object({
            code: v.code,
            retail_price: '',
            cost_price: '',
            quantity:'',
            isChecked: true,
            attributes: v.attributes
          })
      )
    );
    this.confirmVariantForm.valueChanges.subscribe(data => {
      data.price = data.retail_price;
      this.confirmVariantForm.patchValue(data, { emitEvent: false });
    });
  }

  confirm() {
    const formData = this.confirmVariantForm.value;
    const variants = formData.variant_lines.filter(variant => variant.isChecked || formData.isCheckAll).map(variant => new VariantLine(variant));

    if (!variants.every(vl => vl.code)
      && !variants.every(vl => !vl.code)
    ) {
      this.confirmVariantForm.get('variant_lines').value.forEach((vl,i) => {
        if(!vl.code) {
          vl.skuError = true;
          toastr.error(`Vui lòng nhập mã mẫu mã cho mẫu mã thứ ${i+1}.`);
        }
      })
      return;
    }

    if(!variants.every(v => v.retail_price)) {
      this.confirmVariantForm.get('variant_lines').value.forEach((vl, i) => {
        if(!vl.retail_price) {
          vl.retail_price_error = true;
          toastr.error(`Vui lòng nhập giá bán cho mẫu mã thứ ${i+1}.`);
        }
      })
      return;
    }
    this.productService.setVariantListForm(variants);
    this.modalAction.dismiss({
      action: 'submit',
      data: variants
    });
  }

  requestOpenEditAttributeModal(e) {
    this.modalAction.dismiss({
      action: 'edit-attr',
      data: null
    })
  }

  applyPriceToAllVarriants() {
    this.confirmVariantForm.patchValue({
      retail_price: this.seriesValues.retail_price,
      cost_price: this.seriesValues.cost_price,
      quantity: this.seriesValues.quantity
    });
    const formData = this.confirmVariantForm.value;
    formData.variant_lines.forEach(line => {
      line.retail_price = formData.retail_price;
      line.cost_price = formData.cost_price;
      line.quantity = formData.quantity;
    });
    this.confirmVariantForm.patchValue(formData);
  }

  checkAllVariants() {
    this.confirmVariantForm.patchValue({
      isCheckAll: !this.confirmVariantForm.get('isCheckAll').value
    });
    let allVariants = this.confirmVariantForm.get('variant_lines') as FormArray;
    allVariants.value.forEach(
      v => (v.isChecked = this.confirmVariantForm.get('isCheckAll').value)
    );
  }

  checkVariantLine() {
    const formData = this.confirmVariantForm.value;
    const isCheckAll = formData.variant_lines.every(vl => vl.isChecked);
    formData.isCheckAll = isCheckAll;
    this.confirmVariantForm.patchValue(formData);
  }


  addNewVariantLine() {
    const lines = this.variant_lines.value;
    this.variant_lines.setValue(lines.concat({
      code: '',
      retail_price: '',
      cost_price: '',
      quantity: '',
      isChecked: true,
      attributes: this.attributeNames.map(name => new Object({name, value: ''}))
    }));
  }

 get checkedVariantCount() {
    return this.confirmVariantForm.controls.variant_lines.value.filter(vl => vl.isChecked).length;
  }

  closeModal() {
    this.modalAction.close(false);
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

}
