import { Component, Input, OnInit } from '@angular/core';
import { Variant } from 'libs/models/Product';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductService } from 'apps/shop/src/services/product.service';
import { ProductsControllerService } from '../../../../products-controller.service';

@Component({
  selector: 'shop-remove-variant-modal',
  templateUrl: './remove-variant-modal.component.html',
  styleUrls: ['./remove-variant-modal.component.scss']
})
export class RemoveVariantModalComponent implements OnInit {
  @Input() variant = new Variant({});
  loading = false;

  constructor(
    private modalDismiss: ModalAction,
    private productService: ProductService,
    private productsController: ProductsControllerService
  ) { }

  ngOnInit() {
  }

  async removeVariant() {
    this.loading = true;
    try {
      await this.productService.removeVariants([this.variant.id]);
      toastr.success('Xoá mẫu mã thành công');
      this.productsController.onRemovedVariant$.next();
      this.modalDismiss.dismiss(true);
    } catch(e) {
      debug.error('ERROR in Removing Variant', e);
      toastr.error('Xoá mẫu mã thất bại', e.msg || e.message);
    }
    this.loading = false;
  }

  closeModal() {
    this.modalDismiss.dismiss(null);
  }

}
