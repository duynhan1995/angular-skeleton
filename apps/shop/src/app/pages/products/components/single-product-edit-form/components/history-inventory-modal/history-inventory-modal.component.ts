import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { InventoryVoucher } from 'libs/models/Inventory';
import { InventoryService } from 'apps/shop/src/services/inventory.service';
import { Filter, FilterOperator } from '@etop/models';
import { Variant } from 'libs/models/Product';
import { PaginationOpt, NavigationDirection } from '@etop/shared/components/etop-common/etop-pagination/etop-pagination.component';

@Component({
  selector: 'shop-history-inventory-modal',
  templateUrl: './history-inventory-modal.component.html',
  styleUrls: ['./history-inventory-modal.component.scss']
})
export class HistoryInventoryModalComponent implements OnInit {
  @Input() variant: Variant = new Variant({});
  inventory_vouchers: Array<InventoryVoucher> = [];
  filters: Array<Filter> = [];
  paginationConfig: PaginationOpt = {
    hidePerpage: true,
    nextDisabled: false,
    previousDisabled: true
  };
  numPages = 0;
  currentPage = 1;
  perpage = 10;

  constructor(
    private modalAction: ModalAction,
    private inventoryService: InventoryService,
    private ref: ChangeDetectorRef
  ) { }

  async ngOnInit() {
     this.filters = [{
      name: "variant_ids",
      op: FilterOperator.contains,
      value: this.variant.id
    }];
    await this.setupData();
  }

  async setupData(){
    this.inventory_vouchers = await this.inventoryService.getInventoryVouchers(0, 1000, this.filters);
    this.numPages = Math.ceil(this.inventory_vouchers.length / this.perpage);
    if(this.inventory_vouchers.length < this.perpage){
      this.paginationConfig = {...this.paginationConfig,previousDisabled:true, nextDisabled:true}
    }
  }

  getIndexByVariantId(lines, id) {
    return lines && lines.findIndex(l => l.variant_id == id);
  }


  navigate(direction) {
    switch (direction) {
      case NavigationDirection.BACKWARD:
        this.currentPage = this.currentPage == 1 ? 1 : this.currentPage - 1;
        break;
      case NavigationDirection.FORWARD:
        this.currentPage = this.currentPage == this.numPages ? this.numPages : this.currentPage + 1;
        break;
    }
    if (this.currentPage == 1 && this.numPages == 1 ){
      this.paginationConfig = {...this.paginationConfig,previousDisabled:true, nextDisabled:true}
    }
    else if(this.currentPage == this.numPages) {
      this.paginationConfig = {...this.paginationConfig,nextDisabled:true,previousDisabled: false}
    }
    else if(this.currentPage == 1 ) {
      this.paginationConfig = {...this.paginationConfig,previousDisabled:true, nextDisabled:false}
    }
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  getInventoryVoucherList() {
    return this.inventory_vouchers.slice((this.currentPage * this.perpage) - this.perpage, (this.currentPage * this.perpage));
  }
}
