import { Component, OnInit, Input } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { FormBuilder } from '@angular/forms';
import { Variant, Attribute, InventoryVariant } from 'libs/models/Product';
import { ProductService } from 'apps/shop/src/services/product.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { InventoryApi, StocktakeApi } from '@etop/api';
import { ProductsControllerService } from 'apps/shop/src/app/pages/products/products-controller.service';
import { StocktakeLine } from 'libs/models/Stocktake';

@Component({
  selector: 'shop-add-variant-modal',
  templateUrl: './add-variant-modal.component.html',
  styleUrls: ['./add-variant-modal.component.scss']
})
export class AddVariantModalComponent implements OnInit {
  @Input() attributeNames: [];
  @Input() product;
  @Input() fromCreateProductForm = false;

  loading = false
  variant = new Variant({
    inventory_variant: new InventoryVariant(),
  });

  constructor(
    private modalAction: ModalAction,
    private fb: FormBuilder,
    private productService: ProductService,
    private promiseQueue: PromiseQueueService,
    private inventoryApi: InventoryApi,
    private productsController: ProductsControllerService,
    private stocktakeApi: StocktakeApi,
     ) { }

  ngOnInit() {
    this.variant.attributes =  this.attributeNames.map(attrName => {
      let attr = new Attribute();
      attr.name = attrName;
      attr.value = '';
      return attr;
    })
  }

  cancel() {
    this.modalAction.dismiss(null);
  }

  async submit() {
    this.loading = true;
    try {
      if(this.variant.attributes.every(attr => !attr.value)){
        toastr.error('Vui lòng điền ít nhất một giá trị thuộc tính');
        return this.loading = false;
      }
      if(!this.variant.retail_price){
        toastr.error('Vui lòng nhập giá bán mẫu mã');
        return this.loading = false;
      }
      this.variant.inventory_variant.quantity = Number(this.variant.inventory_variant.quantity);
      if (!this.fromCreateProductForm) {
        let variant = this.variant;
        const body = new Variant({
          product_id: this.product.id,
          retail_price: variant.retail_price,
          list_price: variant.retail_price,
          attributes: variant.attributes,
          code: variant.code,
        });
        const res = await this.productService.createVariant(body);

        await this.updateInventoryVariantCostPrice([{
          ...variant,
          id: res.id,
        }]);
        await this.createStocktake([{...res, inventory_variant: variant.inventory_variant}]);
        toastr.success('Tạo mẫu mã sản phẩm thành công.');
        this.modalAction.dismiss(null)
      } else {
        this.modalAction.dismiss({variant: this.variant})
      }

    } catch(e) {
      this.loading = false;
      return toastr.error("Thêm mẫu mã thất bại", e.message || e.msg);
    }
    this.loading = false;
  }


  async updateInventoryVariantCostPrice(variants: Variant[]) {
    try {
      const promises = variants.filter(v => v.inventory_variant.cost_price >= 0)
        .map(v => async() => {
          try {
            await this.inventoryApi.updateInventoryVariantCostPrice(v.id, v.inventory_variant.cost_price);
          } catch(e) {
            debug.error('ERROR in updating InventoryVariantCostPrice inside Promise.all', e);
          }
        });
      await this.promiseQueue.run(promises, 5);
    } catch(e) {
      debug.error('ERROR in updating InventoryVariantCostPrice', e);
    }
  }


  async createStocktake(variants: Variant[]) {
    try {
      let lines: StocktakeLine[] = variants.filter(v => v.inventory_variant.quantity >= 0)
        .map(v => {
          return {
            ...new StocktakeLine(),
            old_quantity: 0,
            new_quantity: v.inventory_variant.quantity,
            variant_id: v.id,
          };
        });
      if (!lines.length) {
        return;
      }
      const body = {
        total_quantity: lines.reduce((a, b) => a + Number(b.new_quantity || 0), 0),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(body);
      await this.stocktakeApi.confirmStocktake(stocktake.id, "confirm");
      toastr.success('Tạo phiếu kiểm kho thành công.');
    } catch(e) {
      toastr.error('Tạo phiếu kiểm kho không thành công.', e);
      debug.error('ERROR in creating stocktake', e);
    }
  }
}
