import { Component, OnInit, Input } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { Variant } from 'libs/models/Product';
import { Attribute } from '@angular/compiler';
import { StocktakeApi } from '@etop/api';

@Component({
  selector: 'shop-edit-inventory-modal',
  templateUrl: './edit-inventory-modal.component.html',
  styleUrls: ['./edit-inventory-modal.component.scss']
})
export class EditInventoryModalComponent implements OnInit {
  @Input() variant: Variant;
  quantity = 0;
  actualQuantity = 0;
  lines: Array<Attribute> = [];
  loading = false;
  product_id: string;
  product_name: string;
  data = {};

  constructor(
    private modalAction: ModalAction,
    private util: UtilService,
    private stocktakeApi: StocktakeApi
  ) { }

  ngOnInit() {
    this.quantity = this.variant.inventory_variant && this.variant.inventory_variant.quantity || 0;
    this.actualQuantity = this.quantity;
  }

  get differenceQuantity(): number {
    return this.actualQuantity - this.quantity
  }

  async confirm() {
    if (this.actualQuantity === this.quantity) {
      toastr.success('Cập nhật tồn kho thành công!');
      this.dismissModal(null);
      return;
    }
    if (!this.actualQuantity) {
      toastr.error('Vui lòng nhập thông tin số lượng thực tế trước khi xác nhận.');
      return;
    }

    this.loading = true;
    try {
      const lines = [{
        product_id: this.variant.product && this.variant.product.id,
        product_name: this.variant.product && this.variant.product.name,
        variant_name: this.variant.name,
        variant_id: this.variant.id,
        old_quantity: Number(this.quantity),
        new_quantity: Number(this.actualQuantity),
        code: this.variant.code,
        image_url: this.variant.image,
        attributes: this.variant.attributes
      }];
      const data = {
        total_quantity: Number(this.actualQuantity),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(data);

      // auto comfirm stocktake
      await this.stocktakeApi.confirmStocktake(stocktake.id, 'confirm');

      toastr.success('Cập nhật tồn kho thành công!');
      this.dismissModal(true);
    }
    catch (e) {
      toastr.error(e.message, 'Cập nhật tồn kho thất bại.');
    }

    this.loading = false;
    return;
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  dismissModal(reload) {
    this.modalAction.dismiss(reload);
  }
}
