import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Variant, ShopProduct } from 'libs/models/Product';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductApi } from '@etop/api';

@Component({
  selector: 'shop-edit-attributes-modal',
  templateUrl: './edit-attributes-modal.component.html',
  styleUrls: ['./edit-attributes-modal.component.scss']
})
export class EditAttributesModalComponent implements OnInit {

  @ViewChild('editAttributesModal', { static: true }) editAttributesModal;
  @ViewChild('updateVariantsConfirm', { static: true }) updateVariantsConfirm;

  @Input() product: ShopProduct;

  updatingVariants = false;

  attrs: Array<{
    name: string;
    values: Array<string>;
  }> = [];
  variants: Array<Variant> = [];

  list_price;

  constructor(
    private productApi: ProductApi,
    private dialog: DialogControllerService,
    private modalDismiss: ModalAction
  ) {
  }

  ngOnInit() {
    if (this.product.variants) {
      let attributes = this.product.variants && this.product.variants[0] && this.product.variants[0].attributes || [];
      this.list_price = this.product.variants[0] && this.product.variants[0].list_price;
      this.attrs = attributes.map(a => {
        return {
          name: a.name,
          values: this.getAttrValues(a.name)
        };
      });
      this.variants = this.product.p_data.variants.map(variant => {
        variant.p_data = Object.assign({}, variant.p_data, { checked: true });
        return variant;
      });
    }
  }

  onChangeAttr() {
    let allAttrsArray = [];
    this.attrs.forEach(a => {
      allAttrsArray.push(a.values);
    });
    this.variants = [];
    let allAttrsCombine = this.allPossibleCases(this.attrs);
    allAttrsCombine.forEach(a => {
      let v = new Variant({
        list_price: this.list_price,
        cost_price: this.product.cost_price,
        code: '',
        attributes: a,
        p_data: { checked: true },
      });
      this.variants.push(v);
    });
  }

  allPossibleCases(arr) {
    if (arr.length == 1) {
      return arr[0].values.map(a => {
        return [{
          name: arr[0].name,
          value: a
        }];
      });
    } else {
      let result = [];
      let allCasesOfRest = this.allPossibleCases(arr.slice(1));
      for (let i = 0; i < allCasesOfRest.length; i++) {
        for (let j = 0; j < arr[0].values.length; j++) {
          let a = JSON.parse(JSON.stringify(allCasesOfRest[i]));
          a.push({
            name: arr[0].name,
            value: arr[0].values[j]
          });
          result.push(a);
        }
      }
      return result;
    }
  }

  getAttrValues(name) {
    let values = [];
    this.product.variants.forEach(v => {
      let attribute = v.attributes.find(attr => attr.name == name);
      if (attribute && values.indexOf(attribute.value) < 0) {
        values.push(attribute.value);
      }
    });
    return values;
  }

  removeAttr(i) {
    this.attrs.splice(i, 1);
    setTimeout(() => this.onChangeAttr());
  }

  addAttr() {
    this.attrs.push({
      name: '',
      values: []
    });
  }

  onListPriceChange() {
    if (this.variants.length) {
      this.variants.forEach(v => v.list_price = this.list_price);
    }
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs.map(a => {
      return a.value;
    }).join(' - ');
  }

  validateVariants() {
    let error = '';
    this.variants.forEach(variant => {
      if (!variant.quantity_available || variant.quantity_available == 0) {
        variant.quantity_available = 100;
      }

      if (!variant.list_price || variant.list_price == 0) {
        error = 'Vui lòng nhập giá mẫu mã!';
        return;
      }
    });
    if (error) {
      throw new Error(error);
    }
  }

  async saveAttributes() {
    this.updatingVariants = true;
    try {
      this.validateVariants();
      const variants = this.variants;
      let delVariantIds = this.product.variants.map(v => v.id);

      await this.productApi.removeVariants(delVariantIds);

      let newVariants = [];

      await Promise.all(variants.map(async variant => {
        variant.product_id = this.product.id;
        variant.product_source_id = this.product.product_source_id;
        const _variant = await this.productApi.createVariant(variant);
        newVariants.push(_variant);
      }));
      toastr.success('Cập nhật mẫu mã thành công!');

      this.modalDismiss.dismiss(newVariants);
    } catch (e) {
      toastr.error('Cập nhật mẫu mã thất bại: ' + e.message);
    }
    this.updatingVariants = false;
  }

  openUpdateVariantsConfirm() {
    this.dialog.createConfirmDialog({
      body: `
        <div class="text-danger">
          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
          Thao tác thay đổi thuộc tính sẽ cập nhật lại toàn bộ mẫu mã của sản phẩm này!
        </div>
      `,
      onConfirm: () => this.saveAttributes()
    }).show().then();
  }

  cancel() {
    this.modalDismiss.dismiss(null);
  }
}
