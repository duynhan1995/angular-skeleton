import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VariantEditRowComponent } from './components/variant-edit-row/variant-edit-row.component';
import { SingleProductEditFormComponent } from './single-product-edit-form.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { EditAttributesModalComponent } from './components/edit-attributes-modal/edit-attributes-modal.component';
import { RemoveVariantModalComponent } from './components/remove-variant-modal/remove-variant-modal.component';
import { HistoryInventoryModalComponent } from './components/history-inventory-modal/history-inventory-modal.component';
import { EditInventoryModalComponent } from './components/edit-inventory-modal/edit-inventory-modal.component';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { PurchaseOrderApi } from '@etop/api';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticateModule } from '@etop/core';
import { EtopCommonModule, EtopMaterialModule, EtopPipesModule } from '@etop/shared';
import { EditAttributeModalComponent } from './components/edit-attribute-modal/edit-attribute-modal.component';
import { AddVariantModalComponent } from './components/add-variant-modal/add-variant-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    NgbModule,
    AuthenticateModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopCommonModule
  ],
  exports: [
    SingleProductEditFormComponent
  ],
  entryComponents: [
    EditAttributesModalComponent,
    RemoveVariantModalComponent,
    HistoryInventoryModalComponent,
    EditInventoryModalComponent
  ],
  declarations: [
    SingleProductEditFormComponent,
    VariantEditRowComponent,
    EditAttributesModalComponent,
    RemoveVariantModalComponent,
    HistoryInventoryModalComponent,
    EditInventoryModalComponent,
    EditAttributeModalComponent,
    AddVariantModalComponent

  ],
  providers: [
    PurchaseOrderService,
    PurchaseOrderApi
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SingleProductEditFormModule {
}
