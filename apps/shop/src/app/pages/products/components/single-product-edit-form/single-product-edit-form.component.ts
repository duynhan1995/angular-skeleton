import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { InventoryApi, StocktakeApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { Product, ShopProduct, InventoryVariant, Variant } from 'libs/models/Product';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { FilterOperator } from '@etop/models';
import { AddCategoryModalComponent } from 'apps/shop/src/app/pages/products/components/add-category-modal/add-category-modal.component';
import { AddCollectionsModalComponent } from 'apps/shop/src/app/pages/products/components/add-collections-modal/add-collections-modal.component';
import { EditAttributesModalComponent } from 'apps/shop/src/app/pages/products/components/single-product-edit-form/components/edit-attributes-modal/edit-attributes-modal.component';
import { ProductService } from 'apps/shop/src/services/product.service';
import { PurchaseOrderService } from 'apps/shop/src/services/purchase-order.service';
import { ShopService } from '@etop/features';
import { ProductsControllerService } from '../../products-controller.service';
import { EditAttributeModalComponent } from './components/edit-attribute-modal/edit-attribute-modal.component';
import { AddVariantModalComponent } from './components/add-variant-modal/add-variant-modal.component';
import { StocktakeLine } from 'libs/models/Stocktake';
import {Collection} from "libs/models/Collection";
import {Category} from "libs/models/Category";
import { MaterialChipsAutocompleteComponent } from '@etop/shared';

@Component({
  selector: 'shop-product-edit-form',
  templateUrl: './single-product-edit-form.component.html',
  styleUrls: ['./single-product-edit-form.component.scss']
})
export class SingleProductEditFormComponent implements OnInit, OnChanges {
  @Input() product: ShopProduct | Product;
  @ViewChild('categoryInput', { static: false}) category_input: MaterialChipsAutocompleteComponent
  productVariantAttributes = [];
  updating_product = false;
  updating_variant = false;

  allowBasicInfoInput = true;

  loading = false;

  constructor(
    private dialog: DialogControllerService,
    private shopService: ShopService,
    private productService: ProductService,
    private modalController: ModalController,
    private ref: ChangeDetectorRef,
    private util: UtilService,
    private productsController: ProductsControllerService,
    private purchaseOrderService: PurchaseOrderService,
    private promiseQueue: PromiseQueueService,
    private inventoryApi: InventoryApi,
    private authStore: AuthenticateStore,
    private stocktakeApi: StocktakeApi,
  ) {
  }

  get removable() {
    return this.product.variants.length > 1 &&
      this.authStore.snapshot.permission.permissions.includes('shop/product:delete');
  }

  ngOnInit() {
    this.fillEmptyAttributes();
    this.allowBasicInfoInput = this.authStore.snapshot.permission.permissions.includes('shop/product/basic_info:update');
  }

  fillEmptyAttributes() {
    this.product = JSON.parse(JSON.stringify(this.product));
    this.product.p_data.variants.forEach(v => {
      if (v.p_data.attributes.length == this.productVariantAttributes.length ) {
        let cloneAttrs = [...v.p_data.attributes];
        this.productVariantAttributes.forEach((attr, index) => {
          let targetAttr = cloneAttrs.find(a => {
            return a.name === attr;
          })
          v.p_data.attributes[index] = targetAttr;
        })
      } else {
        this.productVariantAttributes.forEach((attr, index) => {
          if(this.productVariantAttributes[index] != v.p_data.attributes[index]?.name) {
            v.p_data.attributes.splice(index,0, {name: this.productVariantAttributes[index], value:''})
          }
        })
      }
    })

  }

  ngOnChanges(changes: SimpleChanges) {
    this.category_input?.resetSearchValue();
    this._prepareEditingProduct();
    this.fillEmptyAttributes();
  }

  addCategory() {
    let modal = this.modalController.create({
      component: AddCategoryModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then((category: Category) => {
      this.product.p_data.category = category ? category : this.product.p_data.category;
      this.product.p_data.category_id = category ? category.id : this.product.p_data.category_id;
      this.product.p_data.edited = true;
      this.product.p_data.editedField['category'] = true;
    });
  }

  addCollections() {
    let modal = this.modalController.create({
      component: AddCollectionsModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    modal.show().then();
    modal.onDismiss().then((collections: Collection[]) => {
      if (!this.product.p_data.collections) {
        this.product.p_data.collections = collections;
      } else {
        this.product.p_data.collections.push(...collections);
      }
      this.product.p_data.collection_ids = this.product.p_data.collections.map(c => c.id);
      this.product.p_data.edited = true;
      this.product.p_data.editedField['collections'] = true;
    });
  }

  cateDisplayMap = option => option && option.name || null;
  cateValueMap = option => option && option.id || null;

  onDataEdited(value, field) {
    this.product.p_data = {
      ...this.product.p_data,
      category: this.categories?.find(cat => this.product.p_data.category_id[0] === cat.id),
      collections: this.collections?.filter(coll => this.product.p_data.collection_ids.includes(coll.id))
    };
    this.product.p_data.edited = true;
    this.product.p_data.editedField[field] = true;
  }


  async update() {
    this.updating_product = true;
    try {
      let product = this.product;
      // TODO: check min_price of product
      await this.productService.updateProduct(product);
      toastr.success('Cập nhật thông tin sản phẩm thành công.');
      await this.productsController.loadCategories();
      await this.productsController.loadCollections();
      await this.productsController.reloadProductList();
    } catch (e) {
      debug.error('ERROR in updating product', e);
      toastr.error('Cập nhật thông tin sản phẩm thất bại.', e.msg || e.message);
    }
    this.updating_product = false;
  }

  async updateVariants(force = false) {
    this.updating_variant = true;
    try {
      let success = 0;
      let variants = this.product.p_data.variants.filter(v => v.p_data.edited || force);
      if (variants && variants.length) {
        variants.forEach(v => {
          v.attributes = v.p_data.attributes,
          v.code = v.p_data.code,
          v.list_price = v.p_data.cost_price,
          v.retail_price = v.p_data.retail_price
        });
        const promises = variants.map(v => async() => {
          try {
            await this.productService.updateVariant(v);
            if (v.p_data.cost_price != v.cost_price) {
              await this.inventoryApi.updateInventoryVariantCostPrice(v.id, v.p_data.cost_price || 0);
            }
            success += 1;
          } catch(e) {
            debug.log('ERROR in updating inventory variant cost price', e);
          }
        });
        await this.promiseQueue.run(promises, 1);
      }
      if (success == variants.length) {
        toastr.success('Cập nhật mẫu mã sản phẩm thành công.');
      } else if (success > 0) {
        toastr.warning(`Cập nhật thành công ${success}/${variants.length} mẫu mã sản phẩm.`);
      } else {
        toastr.error('Cập nhật mẫu mã sản phẩm thất bại.');
      }
      this.productsController.reloadProductList();
    } catch(e) {
      debug.error('ERROR in updating variant', e);
      toastr.error('Cập nhật mẫu mã sản phẩm thất bại.', e.msg || e.message);
    }
    this.updating_variant = false;
  }


   private async _prepareEditingProduct() {
    this.product = this.productsController.setupData(this.product);
    this.product.p_data = {
      ...this.product.p_data,
      category: this.categories.find(cat => this.product.category_id === cat.id),
      collections: this.collections.filter(coll => this.product.collection_ids.includes(coll.id)),
    };
    this.productVariantAttributes = [];
    this.product.variants.forEach(v => { v.attributes.forEach(attr => {
      if(this.productVariantAttributes.indexOf(attr.name) == -1) {
        this.productVariantAttributes.push(attr.name); }
      })
    })
    this.fillEmptyAttributes();
  }
  openEditAttributesPopup(e) {
    e.preventDefault();
    const modal = this.modalController.create({
      component: EditAttributeModalComponent,
      componentProps: {
        product: this.product
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(changes => {
      if (!changes) {
        return;
      }
      const variants = this.product.variants;
      changes.forEach(change => {
        variants.forEach(variant => {
          switch (change.action) {
            case 'delete':
              if(change.oldName){
                let deletedIndex = variant.attributes.findIndex(attr => attr.name == change.oldName)
                if(deletedIndex != -1 ) {
                  variant.attributes.splice(deletedIndex, 1);
                }
              }
              break
            case 'add':
              variant.attributes.push({
                name: change.name,
                value: ''
              });
              break;
            case 'update':
              const targetAttr = variant.attributes.find(attr => attr.name == change.oldName);
              if(targetAttr) {
                targetAttr.name = change.name;
              }
              break;
          }
        })
      })
      this._prepareEditingProduct()
    });
  }

  async openAddVariantModal(e) {
    e.preventDefault();
    if(!this.productVariantAttributes.length) {
      toastr.error('Cần thêm thuộc tính trước khi Thêm mẫu mã');
      return;
    }
    const modal = this.modalController.create({
      component: AddVariantModalComponent,
      componentProps: {
        attributeNames : this.productVariantAttributes,
        product: this.product
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async () =>  {
      await this._prepareEditingProduct();
      this.fillEmptyAttributes();
      this.productsController.reloadProductList();
    })
  }

  async updateInventoryVariantCostPrice(variants: Variant[]) {
    try {
      const promises = variants.filter(v => v.inventory_variant.cost_price >= 0)
        .map(v => async() => {
          try {
            await this.inventoryApi.updateInventoryVariantCostPrice(v.id, v.inventory_variant.cost_price);
          } catch(e) {
            debug.error('ERROR in updating InventoryVariantCostPrice inside Promise.all', e);
          }
        });
      await this.promiseQueue.run(promises, 5);
    } catch(e) {
      debug.error('ERROR in updating InventoryVariantCostPrice', e);
    }
  }

  async createStocktake(variants: Variant[]) {
    try {
      let lines: StocktakeLine[] = variants.filter(v => v.inventory_variant.quantity >= 0)
        .map(v => {
          return {
            ...new StocktakeLine(),
            old_quantity: 0,
            new_quantity: v.inventory_variant.quantity,
            variant_id: v.id,
          };
        });
      if (!lines.length) {
        return;
      }
      const body = {
        total_quantity: lines.reduce((a, b) => a + Number(b.new_quantity || 0), 0),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(body);
      await this.stocktakeApi.confirmStocktake(stocktake.id, "confirm");
      toastr.success('Tạo phiếu kiểm kho thành công.');
    } catch(e) {
      toastr.error('Tạo phiếu kiểm kho không thành công.', e);
      debug.error('ERROR in creating stocktake', e);
    }
  }
  // get variants Purchase Orders in order to check if variant needs updateInventoryVariantCostPrice
  async getVariantsPO() {
    this.loading = true;
    try {
      const promises = this.product.variants.map(v => async() => {
        try {
          const res = await this.purchaseOrderService.getPurchaseOrders(0, 1, [{
            name: "variant_ids",
            op: FilterOperator.contains,
            value: v.id
          }, {
            name: "status",
            op: FilterOperator.eq,
            value: "P"
          }]);
          v.purchase_order = res.purchase_orders[0];
        } catch(e) {
          debug.error('ERROR in getting Variants PO in promise.all', e);
        }
      });
      await this.promiseQueue.run(promises, 5);
    } catch(e) {
      debug.error('ERROR in getting Variants PO', e);
    }
    this.loading = false;
  }

  async onChangeLogo(event, product: Product) {
    try{
      const $parent = $(event.target).parent();
      $parent.addClass('loading');
      const { files } = event.target;
      if (!files.length) {
        $parent.removeClass('loading');
        return;
      }
      const res = await this.util.uploadImages([files[0]], 1024);
      product.p_data.image = res[0].url;
      this.onDataEdited(product.image, 'images');
      $parent.removeClass('loading');
    }
    catch(e){
      if(!e.mesage){
        toastr.error("Đã có lỗi xảy ra");
      }
      else{
        toastr.error(e.mesage);
      }
      const $parent = $(event.target).parent();
      $parent.removeClass('loading');
    }
  }

  get categories() {
    return this.productsController.categories;
  }

  get collections() {
    return this.productsController.collections;
  }
}
