import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductsControllerService } from 'apps/shop/src/app/pages/products/products-controller.service';
import { Category } from 'libs/models/Category';
import {Collection} from "libs/models/Collection";

@Component({
  selector: 'shop-add-category-modal',
  templateUrl: './add-category-modal.component.html',
  styleUrls: ['./add-category-modal.component.scss']
})
export class AddCategoryModalComponent implements OnInit {
  new_category = new Category({});
  constructor(
    private modalDismiss: ModalAction,
    private productsController: ProductsControllerService
  ) { }

  ngOnInit() {}

  dismissModal() {
    this.modalDismiss.dismiss(null);
  }

  createCategory() {
    const category = {
      ...this.new_category,
      id: '###' + new Date().getTime()
    };
    if(category.name) {
      this.productsController.categories.push(category);
      this.modalDismiss.dismiss(category);
    } else {
      this.modalDismiss.dismiss(null);
    }

  }

}
