import { Component, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { ProductsControllerService } from 'apps/shop/src/app/pages/products/products-controller.service';
import { Collection } from '@etop/models';

@Component({
  selector: 'shop-add-collections-modal',
  templateUrl: './add-collections-modal.component.html',
  styleUrls: ['./add-collections-modal.component.scss']
})
export class AddCollectionsModalComponent implements OnInit {
  new_collections: Array<Collection> = [];
  constructor(
    private modalDismiss: ModalAction,
    private productsController: ProductsControllerService
  ) { }

  ngOnInit() {}

  dismissModal() {
    this.modalDismiss.dismiss(this);
  }

  createCollections() {
    const collections = this.new_collections.map(coll => {
      return new Collection({
        name: coll,
        id: '###' + new Date().getTime()
      });
    });
    this.productsController.collections.push(...collections);
    this.modalDismiss.dismiss(collections);
  }

}
