import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SupportsComponent } from './supports.component';


const components = [

];
const pages = [
];

const routes: Routes = [
  {
    path: '',
    component: SupportsComponent
  }
];

@NgModule({
  declarations: [
    SupportsComponent,
    ...components,
    ...pages,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupportsModule { }
