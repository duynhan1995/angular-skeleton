import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserService } from 'apps/core/src/services/user.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-verify-phone',
  templateUrl: './verify-phone.component.html',
  styleUrls: ['./verify-phone.component.scss']
})
export class VerifyPhoneComponent implements OnInit {
  token = '';
  loading = false;
  error = false;
  errorMsg = '';
  phoneVerified = !!this.auth.snapshot.user.phone_verified_at;
  countDown = 300;
  intervalId: any;

  constructor(
    private userService: UserService,
    private auth: AuthenticateStore,
    private util: UtilService,
  ) { }

  ngOnInit() {
    if (this.phoneVerified) {
      this.token = 'Xác thực số điện thoại thành công';
    }

    this.intervalId = setInterval(() => {
      this.countDown--;
      if (this.countDown == 0) {
        clearInterval(this.intervalId);
      }
    }, 1000);
  }

  async verifyPhone() {
    if (this.token) {
      this.error = false
      this.loading = true;
      try {
        if (this.auth) {
          await this.userService.verifyPhoneUsingToken(this.token);
          let user = this.auth.snapshot.user;
          let d = new Date();
          user.phone_verified_at = d.toISOString();
          this.auth.updateUser(user);
          this.phoneVerified = true;
          this.token = 'Xác thực số điện thoại thành công';
        }
      } catch (e) {
        this.error = true;
        this.errorMsg = e.message;
      }
      this.loading = false;
    }
  }

  async resendCode() {
    if (this.countDown) {
      toastr.error('Bạn chưa thể gửi lại mã xác nhận ngay lúc này!', 'Xin chờ giây lát');
      return;
    }
    this.countDown = 300;

    this.intervalId = setInterval(() => {
      this.countDown--;
      if (this.countDown == 0) {
        clearInterval(this.intervalId);
      }
    }, 1000);

    try {
      await this.userService.sendPhoneVerification(this.auth.snapshot.user.phone);
      toastr.success('', 'Một mã xác nhận mới vừa được gửi vào tin nhắn của bạn');
    } catch (e) {
      this.error = true;
      this.errorMsg = e.message;
    }
  }

  gotoDashboard() {
    let slug = this.util.getSlug();
    location.href = `/s/${slug}/orders`;
  }
}
