import { Injectable } from '@angular/core';
import { Filters } from '@etop/models';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Subject } from 'rxjs';

export enum PrintType {
  fulfillments = 'fulfillments',
}

@Injectable()
export class FulfillmentsController {
  fulfillment_type = 'shipment';
  filterEvent: Filters;

  onChangeTab$ = new Subject();
  onChangeFilter$ = new Subject();

  constructor() { }

}

