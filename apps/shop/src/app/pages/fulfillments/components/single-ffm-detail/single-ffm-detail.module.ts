import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleFfmDetailComponent } from 'apps/shop/src/app/pages/fulfillments/components/single-ffm-detail/single-ffm-detail.component';
import { DetailInfoComponent } from 'apps/shop/src/app/pages/fulfillments/components/single-ffm-detail/detail-info/detail-info.component';
import { TicketModule } from 'apps/shop/src/app/components/ticket/ticket.module';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FulfillmentHistoriesModule } from 'apps/shop/src/app/components/fulfillment-histories/fulfillment-histories.module';
import { FulfillmentShipnowRoutesModule } from 'apps/shop/src/app/components/fulfillment-shipnow-routes/fulfillment-shipnow-routes.module';
import { EtopMaterialModule, EtopPipesModule } from '@etop/shared';


@NgModule({
  declarations: [
    SingleFfmDetailComponent,
    DetailInfoComponent,
  ],
  exports: [
    SingleFfmDetailComponent
  ],
  imports: [
    CommonModule,
    TicketModule,
    SharedModule,
    FormsModule,
    NgbModule,
    FulfillmentHistoriesModule,
    FulfillmentShipnowRoutesModule,
    EtopMaterialModule,
    EtopPipesModule,
  ],
})
export class SingleFfmDetailModule { }
