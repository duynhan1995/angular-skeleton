import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FulfillmentDetailComponent } from 'apps/shop/src/app/pages/fulfillments/components/fulfillment-detail/fulfillment-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { ForceButtonModule } from 'apps/shop/src/app/components/force-button/force-button.module';
import { TicketModule } from 'apps/shop/src/app/components/ticket/ticket.module';
import { BillPrintingModule } from 'apps/shop/src/app/components/bill-printing/bill-printing.module';
import { PrintModule } from 'apps/shop/src/app/components/print/print.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: FulfillmentDetailComponent }
    ]
  }
];

@NgModule({
  declarations: [
    FulfillmentDetailComponent
  ],
  imports: [
CommonModule,
    RouterModule,
    EtopPipesModule,
    ForceButtonModule,
    RouterModule.forChild(routes),
    TicketModule,
    PrintModule,
    BillPrintingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FulfillmentDetailModule { }
