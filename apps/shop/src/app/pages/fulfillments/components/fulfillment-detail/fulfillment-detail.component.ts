import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Order } from 'libs/models/Order';
import { FulfillmentService } from 'apps/shop/src/services/fulfillment.service';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import * as moment from 'moment';
import { VTigerService } from 'apps/core/src/services/vtiger.service';
import { Ticket } from 'libs/models/Ticket';
import { FulfillmentFeeLine } from 'libs/models/Fee';
import { OrderService } from 'apps/shop/src/services/order.service';
import { Subscription } from 'rxjs';
import misc from '../../../../../../../core/src/libs/misc';
import { FulfillmentApi } from '@etop/api';
import { filter } from 'rxjs/operators';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { PrintControllerService } from 'apps/shop/src/app/components/print/print.controller';
import { PrintType } from '@etop/shared/typings/print';

@Component({
  selector: 'shop-fulfillment-detail',
  templateUrl: './fulfillment-detail.component.html',
  styleUrls: ['./fulfillment-detail.component.scss']
})
export class FulfillmentDetailComponent implements OnInit, OnDestroy {
  @Input() tickets: Array<Ticket>;

  fulfillment = new Fulfillment({});
  shipnow_orders: Array<Order> = [];
  order = new Order({});
  shop: any;
  loading = true;

  fulFillmentHistory: any;
  hasXShippingLogs = false;
  _token: string;
  isShipnow: any;

  fulfillmentType = 'shipment';

  private subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private fulfillmentService: FulfillmentService,
    private auth: AuthenticateStore,
    private util: UtilService,
    private vTigerService: VTigerService,
    private orderService: OrderService,
    private router: Router,
    private orderStore: OrderStoreService,
    private printController: PrintControllerService
  ) {
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.loadFullfilment();
    });
  }

  get printingOrders() {
    return this.printController.printing_orders;
  }

  get printingFulfillments() {
    return this.printController.printing_ffms;
  }

  get isPrintingOrders() {
    return this.printController.print_type == PrintType.orders;
  }

  get isPrintingFfms() {
    return this.printController.print_type == PrintType.fulfillments;
  }

  get isPrintingOrderAndFfm() {
    return this.printController.print_type == PrintType.o_and_f;
  }

  async ngOnInit() {
    await this.loadFullfilment();
  }

  async loadFullfilment(){
    const { params, queryParams } = this.activatedRoute.snapshot;
    const id = params.id;
    const _s_id = queryParams._s_id;
    this._token = _s_id ? this.auth.getTokenByShop(misc.decodeBase64(_s_id)) : null;
    this.isShipnow = this.activatedRoute.snapshot.queryParamMap.get('shipnow');
    await this.getFulfillment(id);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  async getFulfillment(id: string) {
    this.shipnow_orders = [];
    if (this.isShipnow) {
      this.fulfillmentType = 'shipnow';
      this.fulfillment = await this.fulfillmentService.getShipnowFulfillment(id, this._token);
      if (this.fulfillment.order_ids.length) {
        for (let order_id of this.fulfillment.order_ids) {
          this.shipnow_orders.push(await this.orderService.getOrder(order_id));
        }
      }
    } else {
      this.fulfillment = await this.fulfillmentService.getFulfillment(id, this._token);
    }

    setTimeout(
      () => JsBarcode('#shipping-barcode', this.fulfillment.shipping_code),
      0
    );
    this.order = this.fulfillment.order;
    this.shop = this.fulfillment.shop || this.auth.snapshot.shop;
    this.loading = false;

    this.fulfillment.shipping_fee_shop_lines_mapped = this.fulfillment
      .shipping_fee_shop_lines
      ? this._shippingFeeShopMap(this.fulfillment.shipping_fee_shop_lines)
      : [];
    if (this.fulfillment.x_shipping_logs && this.fulfillment.x_shipping_logs.length) {
      this.fulFillmentHistory = this.fulfillment.x_shipping_logs;
      this.hasXShippingLogs = true;
    } else {
      this.fulFillmentHistory = await this.fulfillmentService.getFulfillmentHistory(
        this.fulfillment
      );
      this.hasXShippingLogs = false;
    }
    this.getTickets();
  }

  _shippingFeeShopMap(lines: Array<FulfillmentFeeLine>) {
    return lines.map(line => {
      const { cost, shipping_fee_type } = line;
      debug.log('line', line);
      return { name: this.util.shippingFeeShopMap(shipping_fee_type), cost };
    });
  }

  fulfillmentShippingStateMap(status) {
    return this.util.fulfillmentShippingStateMap(status);
  }

  get logo() {
    return FulfillmentApi.getProviderLogoBig(this.fulfillment.carrier);
  }

  get trackingLink() {
    return Fulfillment.trackingLink(this.fulfillment);
  }

  get estimatedPickupTime() {
    const { estimated_pickup_at } = this.fulfillment;
    if (!estimated_pickup_at) {
      return '--';
    }
    return `trước ${moment(estimated_pickup_at).format('HH:mm DD/MM')}`;
  }

  get estimatedDeliveryTime() {
    const { estimated_delivery_at } = this.fulfillment;
    if (!estimated_delivery_at) {
      return '--';
    }
    return `${moment(estimated_delivery_at).format('DD/MM')}`;
  }

  get deliveredTime() {
    const { x_shipping_delivered_at } = this.fulfillment;
    if (!x_shipping_delivered_at) {
      return '--';
    }
    return `${moment(x_shipping_delivered_at).format('DD/MM')}`;
  }

  get pickedTIme() {
    const { x_shipping_picked_at } = this.fulfillment;
    if (!x_shipping_picked_at) {
      return '--';
    }
    return `${moment(x_shipping_picked_at).format('DD/MM')}`;
  }

  async getTickets() {
    try {
      let user = this.auth.snapshot.user;
      let tickets = await this.vTigerService.getTickets({
        etop_id: user.id,
        order_id: this.order.id
      });
      this.tickets = tickets;
      debug.log('TICKETS', tickets);
    } catch (e) {
      debug.error(e.message);
    }
    debug.log('tickets', this.tickets);
  }

  canSendSupportRequest() {
    return this.fulfillmentService.canSendSupportRequest(this.fulfillment);
  }

  viewDetailOrder(order_code) {
    this.orderStore.navigate('so', order_code);
  }

  printFulfillment() {
    this.printController.print(PrintType.fulfillments, {
      ffms: [this.fulfillment]
    });
  }
}
