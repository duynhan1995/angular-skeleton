import {BillPrintingModule} from 'apps/shop/src/app/components/bill-printing/bill-printing.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImportFfmModule} from "libs/shared/components/import-ffm/import-ffm.module";
import {FulfillmentsComponent} from 'apps/shop/src/app/pages/fulfillments/fulfillments.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FulfillmentListComponent} from './fulfillment-list/fulfillment-list.component';
import {FulfillmentRowComponent} from './components/fulfillment-row/fulfillment-row.component';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {FormsModule} from '@angular/forms';
import {ForceButtonModule} from '../../components/force-button/force-button.module';
import {FulfillmentsController} from 'apps/shop/src/app/pages/fulfillments/fulfillments.controller';
import {ReturningFulfillmentsWarningModule} from 'apps/shop/src/app/components/returning-fulfillments-warning/returning-fulfillments-warning.module';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {SingleFfmDetailModule} from 'apps/shop/src/app/pages/fulfillments/components/single-ffm-detail/single-ffm-detail.module';
import {AuthenticateModule} from '@etop/core';
import {CancelObjectModule} from '../../components/cancel-object/cancel-object.module';
import {MultipleFfmActionsComponent} from './components/multiple-ffm-actions/multiple-ffm-actions.component';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {
  EmptyPageModule,
  EtopCommonModule,
  EtopFilterModule,
  EtopPipesModule,
  MaterialModule,
  SideSliderModule
} from '@etop/shared';
import {PrintModule} from 'apps/shop/src/app/components/print/print.module';

const routes: Routes = [
  {
    path: '',
    component: FulfillmentsComponent,
    children: [
      {path: 'shipnow', component: FulfillmentListComponent},
      {path: 'shipment', component: FulfillmentListComponent},
      {path: 'import', component: FulfillmentListComponent},
      {
        path: ':id',
        loadChildren: () => import('./components/fulfillment-detail/fulfillment-detail.module').then(m => m.FulfillmentDetailModule)
      },
      {path: '**', redirectTo: 'shipment'},
    ]
  }
];

@NgModule({
  declarations: [
    FulfillmentsComponent,
    FulfillmentListComponent,
    FulfillmentRowComponent,
    MultipleFfmActionsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    ForceButtonModule,
    ModalControllerModule,
    ReturningFulfillmentsWarningModule,
    DropdownActionsModule,
    CancelObjectModule,
    RouterModule.forChild(routes),
    NgbTooltipModule,
    SingleFfmDetailModule,
    AuthenticateModule,
    BillPrintingModule,
    EtopPipesModule,
    SideSliderModule,
    EtopFilterModule,
    EtopCommonModule,
    MaterialModule,
    PrintModule,
    ImportFfmModule,
    EmptyPageModule
  ],
  providers: [
    FulfillmentsController
  ]
})
export class FulfillmentsModule {
}
