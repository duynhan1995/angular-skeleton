import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FilterOperator, FilterOptions, Filters} from '@etop/models';
import {CmsService} from "apps/core/src/services/cms.service";
import {FulfillmentService} from 'apps/shop/src/services/fulfillment.service';
import {FulfillmentsController, PrintType} from 'apps/shop/src/app/pages/fulfillments/fulfillments.controller';
import {PageBaseComponent} from '@etop/web/core/base/page.base-component';
import {ConnectionStore} from "@etop/features/connection/connection.store";
import {distinctUntilChanged, map, takeUntil} from "rxjs/operators";
import {Connection} from "libs/models/Connection";
import {LocationQuery} from "@etop/state/location";
import {FulfillmentListComponent} from "apps/shop/src/app/pages/fulfillments/fulfillment-list/fulfillment-list.component";
import {ImportFfmComponent} from "libs/shared/components/import-ffm/import-ffm.component";
import {HeaderControllerService} from "apps/core/src/components/header/header-controller.service";
import {combineLatest} from "rxjs";
import {ExportFulfillmentModalComponent} from "apps/shop/src/app/components/modals/export-fulfillment-modal/export-fulfillment-modal.component";
import {ModalController} from "apps/core/src/components/modal-controller/modal-controller.service";
import {ImportFfmQuery} from "@etop/state/shop/import-ffm";
import {PrintControllerService} from 'apps/shop/src/app/components/print/print.controller';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-fulfillments',
  template: `
    <ng-container>
      <etop-filter
        class="no-print" [topshipFilter]="true"
        [filters]="filters" (filterChanged)="onFilterChange($event)">
      </etop-filter>
      <shop-returning-fulfillments-warning
        *ngIf="returningOrdersCount > 0 && isShipment" [returningQuantity]="returningOrdersCount">
      </shop-returning-fulfillments-warning>
      <router-outlet></router-outlet>
      <shop-print>
        <shop-bill-printing
          [printOnlyFulfillments]="isPrintingFfms"
          [fulfillments]="printingFfms">
        </shop-bill-printing>
      </shop-print>
    </ng-container>`
})

export class FulfillmentsComponent extends PageBaseComponent implements OnInit, OnDestroy {
  filters: FilterOptions;

  returningOrdersCount = 0;

  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  connectionReady$ = this.connectionStore.state$.pipe(
    map(s => s?.initConnections),
    distinctUntilChanged((a, b) => a?.length == b?.length)
  );

  constructor(
    private headerController: HeaderControllerService,
    private modal: ModalController,
    private ffmService: FulfillmentService,
    private ffmsController: FulfillmentsController,
    private locationQuery: LocationQuery,
    private printController: PrintControllerService,
    private connectionStore: ConnectionStore,
    private cms: CmsService,
    private router: Router,
    private util: UtilService,
    private importFfmQuery: ImportFfmQuery,
  ) {
    super();
  }

  get isShipment() {
    return this.ffmsController.fulfillment_type == 'shipment';
  }

  get isShipnow() {
    return this.ffmsController.fulfillment_type == 'shipnow';
  }

  get printingFfms() {
    return this.printController.printing_ffms;
  }

  get isPrintingFfms() {
    return this.printController.print_type == PrintType.fulfillments;
  }

  private static specialConnectionNameDisplay(connection: Connection) {
    if (!connection.id) { 
      return connection.name;
    }
    return `
<div class="d-flex align-items-center">
  <div class="carrier-image">
    <img src="${connection.provider_logo}" alt="">
    ${connection.connection_method == 'builtin' ? '<img class="topship-logo" alt="" src="assets/images/r-topship_256.png">' : ''}
  </div>
  <div class="pl-2">${connection.name.toUpperCase()}</div>
</div>`;
  }

  ngOnInit() {
    this.init();

    this.headerController.setActions([
      {
        onClick: () => this.openExportPopup(),
        title: 'Xuất excel',
        cssClass: 'btn btn-outline btn-topship',
        permissions: ['shop/fulfillment:export']
      }
    ]);

    if (this.cms.bannersLoaded) {
      this.setupTabs();
    } else {
      this.cms.onBannersLoaded.pipe(takeUntil(this.destroy$))
        .subscribe(_ => {
          this.setupTabs();
        });
    }

    this.initFilters();
    combineLatest([this.locationReady$, this.connectionReady$])
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.initFilters();
      });

      this.ffmsController.onChangeTab$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async () => {
        const type = this.router.url.split('/')[4];
        if (type == 'import') {
          this.toImport();
        }
        else {
          this.initFilters();
        }
        this.setupTabs();
      });
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  initFilters() {
    this.filters = [
      {
        label: 'Mã đơn giao hàng',
        name: 'shipping_code',
        type: 'input',
        fixed: true,
        operator: FilterOperator.eq
      },
      {
        label: 'Mã đơn hàng',
        name: 'order.code',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Mã nội bộ',
        name: 'order.external_code, order.external_id',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: "Trạng thái giao hàng",
        name: "shipping_state",
        type: 'select',
        fixed: true,
        operator: FilterOperator.in,
        options: [
          {name: 'Tất cả', value: null},
          {name: "Đã tạo", value: 'created'},
          {name: 'Đã xác nhận', value: 'confirmed'},
          {name: 'Đang xử lý', value: 'processing'},
          {name: 'Đang lấy hàng', value: 'picking'},
          {name: 'Chờ giao', value: 'holding'},
          {name: 'Đang giao hàng', value: 'delivering'},
          {name: 'Đang trả hàng', value: 'returning'},
          {name: 'Đã giao hàng', value: 'delivered'},
          {name: 'Đã trả hàng', value: 'returned'},
          {name: 'Không giao được', value: 'undeliverable'},
          {name: 'Hủy', value: 'cancelled'},
          {name: 'Không xác định', value: 'unknown'}
        ]
      },
      {
        label: 'Trạng thái đối soát',
        name: 'etop_payment_status',
        type: 'select',
        fixed: true,
        operator: FilterOperator.in,
        options: [
          {name: 'Tất cả', value: null},
          {name: 'Chờ chuyển tiền', value: 'Z, S'},
          {name: 'Đã chuyển tiền', value: 'P, N'}
        ]
      },
      {
        label: 'Tên người nhận',
        name: 'address_to.full_name',
        type: 'input',
        operator: FilterOperator.contains
      },
      {
        label: 'Số điện thoại người nhận',
        name: 'address_to.phone',
        type: 'input',
        operator: FilterOperator.contains
      },
      {
        label: "Tỉnh thành",
        name: "address_to.province_code",
        type: "autocomplete",
        operator: FilterOperator.eq,
        options: this.locationQuery.getValue().provincesList,
        displayMap: option => option && option.filter_name || null,
        valueMap: option => option && option.code || null
      },
      {
        label: "Quận huyện",
        name: "address_to.district_code",
        type: "autocomplete",
        operator: FilterOperator.eq,
        options: this.locationQuery.getValue().districtsList,
        displayMap: option => option && option.filter_name || null,
        valueMap: option => option && option.code || null
      },
      {
        label: 'Giá trị đơn hàng',
        name: 'basket_value',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Khối lượng (g)',
        name: 'chargeable_weight',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Bảo hiểm',
        name: 'include_insurance',
        type: 'select',
        operator: FilterOperator.eq,
        options: [
          {name: 'Tất cả', value: null},
          {name: 'Có', value: 'true'},
          {name: 'Không', value: 'false'}
        ]
      },
      {
        label: 'Tiền thu hộ',
        name: 'total_cod_amount',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Phí giao hàng',
        name: 'shipping_fee_shop',
        type: 'input',
        operator: FilterOperator.eq
      },
      {
        label: 'Nhà vận chuyển',
        name: 'connection_id',
        type: 'select',
        operator: FilterOperator.eq,
        options: [{
          name: 'Tất cả',
          id: null
        }].concat(this.connectionStore.snapshot.initConnections),
        optionsHeight: '4rem',
        displayMap: option => option && FulfillmentsComponent.specialConnectionNameDisplay(option) || null,
        valueMap: option => option && option.id || null
      }
    ];
  }

  private init() {
    const type = this.router.url.split('/')[4];
    this.ffmsController.fulfillment_type = type;
    this.listReturningFfms().then();
    this.ffmService.onViewReturningFfms$.subscribe(_ => {
      const _filter = this.filters.find(f => f.name == 'shipping_state');
      if (_filter) {
        _filter.value = 'returning';
      }
    });
  }

  async listReturningFfms() {
    try {
      const filters: Filters = [{
        name: "shipping_state",
        op: FilterOperator.in,
        value: "returning"
      }];
      const res: any = await this.ffmService.getFulfillments(0, 1000, filters);
      this.returningOrdersCount = res.fulfillments.length;
    } catch (e) {
      debug.error('ERROR in listing Returning Orders', e);
    }
  }

  setupTabs() {
    const _badge = this.cms.getImportFfmBadge();

    this.headerController.setTabs([
      {
        title: 'Giao nhanh',
        cssClass: 'topship',
        active: this.ffmsController.fulfillment_type == 'shipment',
        onClick: async () => {
          this.initFilters();
          await this.router.navigateByUrl(`s/${this.util.getSlug()}/fulfillments/shipment`);
          this.headerController.setActions([
            {
              onClick: () => this.openExportPopup(),
              title: 'Xuất excel',
              cssClass: 'btn btn-outline btn-topship',
              permissions: ['shop/fulfillment:export']
            }
          ]);
        }
      },
      {
        title: 'Giao tức thì',
        cssClass: 'topship',
        active: this.ffmsController.fulfillment_type == 'shipnow',
        onClick: async () => {
          this.initFilters();
          await this.router.navigateByUrl(`s/${this.util.getSlug()}/fulfillments/shipnow`);
          this.headerController.clearActions();
        }
      },
      {
        title: `Import${_badge && `<span class="header-tab-badge bg-topship ml-2">${_badge}</span>`}`,
        cssClass: 'topship',
        active: this.ffmsController.fulfillment_type == 'import',
        onClick: async () => {
          await this.router.navigateByUrl(`s/${this.util.getSlug()}/fulfillments/import`);
          this.toImport();
        }
      }
    ]);
  }

  openExportPopup() {
    let modal = this.modal.create({
      component: ExportFulfillmentModalComponent,
      showBackdrop: 'static'
    });
    modal.show().then();
  }

  toImport() {
    this.filters = [];
    if (!this.importFfmQuery.getAll()?.length) {
      this.headerController.clearActions();
    }
  }

  onFilterChange($event: Filters) {
    this.ffmsController.filterEvent = $event;
    this.ffmsController.onChangeFilter$.next();
  }
}
