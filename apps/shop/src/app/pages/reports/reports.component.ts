import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import moment from 'moment';
import { ReportService } from 'apps/shop/src/services/report.service';
import { DashboardStoreService } from 'apps/core/src/stores/dashboard.store.service';
import { SubscriptionService } from '@etop/features/services';

@Component({
  selector: 'shop-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {
  reportsType = [
    {
      value: 'end_day',
      name: 'Báo cáo cuối ngày',
    },
    {
      value: 'income',
      name: 'Báo cáo bán hàng',
    },
  ];
  isIncome = true;
  exportType: string;
  reportType: string;
  timeFilter: string;
  contentReport: string;
  year = moment();
  date = new Date();
  loading = false;
  showTime = true;
  subscription = false;

  constructor(
    private reportService: ReportService,
    private dashboardStore: DashboardStoreService,
    private subscriptionService: SubscriptionService,
    ) {}

  async ngOnInit() {
    this.subscription = await this.subscriptionService.checkSubcriptions();
    this.dashboardStore.changeHeaderTab('report');
    this.exportType = 'excel';
    this.reportType = 'income';
    this.timeFilter = 'month';
    this.getReport();
  }

  async getReport() {
    this.loading = true;
    switch (this.reportType) {
      case 'income':
        try {
          this.showTime = !(this.timeFilter == 'year');
          const _year: number = moment(this.year).year();
          this.contentReport = await this.reportService.getIncome(
            this.timeFilter,
            _year
          );
          this.contentReport = this.contentReport.slice(
            this.contentReport.search('<div class="main">'),
            this.contentReport.search('</body>')
          );
        } catch (e) {
          debug.error('ERROR in getting report', e);
        }

        break;
      case 'end_day':
        try {
          this.showTime = true;
          const _time = moment(this.date).format('YYYY-MM-DD');
          this.contentReport = await this.reportService.getEnddayIncome(_time);
          this.contentReport = this.contentReport.slice(
            this.contentReport.search('<div class="main">'),
            this.contentReport.search('</body>')
          );
        } catch (e) {
          debug.error('ERROR in getting report', e);
        }

        break;
    }
    this.loading = false;
  }

  async confirmExport() {
    this.loading = true;
    let res: Observable<any>;
    let filename = '';
    const extention = this.exportType == 'pdf' ? 'pdf' : 'xlsx';
    switch (this.reportType) {
      case 'income':
        try {
          filename = 'Báo cáo hoạt động kinh doanh.' + extention;
          const _year: number = moment(this.year).year();
          res = await this.reportService.exportIncome(
            this.timeFilter,
            _year,
            this.exportType
          );
        } catch (e) {
          debug.error('ERROR in export report', e);
        }

        break;
      case 'end_day':
        try {
          filename = 'Báo cáo cuối ngày về bán hàng.' + extention;
          const _time = moment(this.date).format('YYYY-MM-DD');
          res = await this.reportService.exportEnddayIncome(
            _time,
            this.exportType
          );
        } catch (e) {
          debug.error('ERROR in export report', e);
        }
        break;
    }
    res.subscribe((data) => {
      const downloadURL = window.URL.createObjectURL(data);
      let link = document.createElement('a');
      link.href = downloadURL;
      link.download = filename;
      link.click();
    });
    this.loading = false;
  }

  reportsTypeChange() {
    switch (this.reportType) {
      case 'income':
        this.isIncome = true;
        this.year = moment();
        break;
      case 'end_day':
        this.isIncome = false;
        this.date = new Date();
        break;
    }
    this.getReport();
  }
}
