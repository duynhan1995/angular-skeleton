import {Component, OnInit} from '@angular/core';
import {SurveyInfo} from "@etop/models";
import {CmsService} from "apps/core/src/services/cms.service";
import {AuthenticateStore} from '@etop/core';
import {Router} from '@angular/router';
import {TimerService} from 'apps/core/src/services/timer.service';

interface SurveyQuestion {
  key: string;
  image_url: string;
  question: string;
  answersList: { text: string; value: string | number }[];
  answer: string | number;
}

@Component({
  selector: 'shop-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  industryQuestion: SurveyQuestion = null;
  ordersPerDayQuestion: SurveyQuestion = null;
  defaultWeightQuestion: SurveyQuestion = null;

  constructor(
    private auth: AuthenticateStore,
    private timerService: TimerService,
    private router: Router,
    private cms: CmsService
  ) {
  }

  get user() {
    return this.auth.snapshot.user;
  }

  ngOnInit() {
    if (this.cms.bannersLoaded) {
      const _survey = this.cms.getEtopPosSurvey();
      this.industryQuestion = _survey['industry'];
      this.ordersPerDayQuestion = _survey['orders_per_day'];
      this.defaultWeightQuestion = _survey['default_weight'];
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        const _survey = this.cms.getEtopPosSurvey();
        this.industryQuestion = _survey['industry'];
        this.ordersPerDayQuestion = _survey['orders_per_day'];
        this.defaultWeightQuestion = _survey['default_weight'];
      });
    }
  }

  saveSurvey() {
    if (!(this.industryQuestion.answer && this.ordersPerDayQuestion.answer && this.defaultWeightQuestion.answer)) {
      return toastr.error('Bạn chưa trả lời hết tất cả các câu hỏi.');
    }

    const survey: SurveyInfo[] = [
      {
        key: 'industry',
        question: this.industryQuestion.question,
        answer: this.industryQuestion.answer.toString()
      },
      {
        key: 'orders_per_day',
        question: this.ordersPerDayQuestion.question,
        answer: this.ordersPerDayQuestion.answer.toString()
      },
      {
        key: 'default_weight',
        question: this.defaultWeightQuestion.question,
        answer: this.defaultWeightQuestion.answer.toString()
      }
    ];

    localStorage.setItem('survey', JSON.stringify(survey));

    this.router.navigateByUrl('/create-shop').then();
  }

  signout() {
    this.timerService.clear();
    this.auth.clear();
    this.router.navigateByUrl('/login').then();
  }

}
