import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'shop-eorder',
  templateUrl: './eorder.component.html',
  styleUrls: ['./eorder.component.scss']
})
export class EorderComponent implements OnInit,OnDestroy {
  currentTab = 'homepage';

  constructor(
    private headerController: HeaderControllerService,
    private router: Router,
    private util: UtilService,
) {
    let url = router.url.split('/');
    if (url[url.length - 1] != 'eorder') {
      this.currentTab = url[url.length - 1];
    }
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)  
    ).subscribe((event: NavigationEnd) => {
      let params = event.url.split('/');
      if (params.indexOf('eorder') > -1) {
        if (params[params.length - 1] != 'eorder') {
          this.currentTab = params[params.length - 1];
        }
        this.setTab();
      }
    });
  }

  ngOnInit() {
    this.setTab();
  }

  setTab() {
    this.headerController.setTabs([
      {
        title: 'Giới thiệu',
        name: 'homepage',
        active: this.currentTab == 'homepage',
        onClick: () => {
          this.changeTab('homepage');
        }
      },
      {
        title: 'Đặt hàng',
        name: 'manage',
        active: this.currentTab == 'manage',
        onClick: () => {
          this.changeTab('manage');
        }
      },
      {
        title: 'Nạp tiền',
        name: 'bank-transfer',
        active: this.currentTab == 'bank-transfer',
        onClick: () => {
          this.changeTab('bank-transfer');
        }
      },
      {
        title: 'Bảng giá',
        name: 'price',
        active: this.currentTab == 'price',
        onClick: () => {
          this.changeTab('price');
        }
      },
      {
        title: 'Chính sách',
        name: 'policy',
        active: this.currentTab == 'policy',
        onClick: () => {
          this.changeTab('policy');
        }
      },
      {
        title: 'Hướng dẫn',
        name: 'tutorial',
        active: this.currentTab == 'tutorial',
        onClick: () => {
          this.changeTab('tutorial');
        }
      },
      {
        title: 'Hỗ trợ',
        name: 'support',
        active: this.currentTab == 'support',
        onClick: () => {
          this.changeTab('support');
        }
      },
    ]);
  }

  changeTab(tab) {
    this.router.navigateByUrl(`s/${this.util.getSlug()}/eorder/${tab}`);   
  }

  ngOnDestroy(): void {
    this.headerController.setTabs([]);
  }
}
