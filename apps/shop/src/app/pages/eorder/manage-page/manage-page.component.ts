import { Component, OnInit } from '@angular/core';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'shop-manage-page',
  templateUrl: './manage-page.component.html',
  styleUrls: ['./manage-page.component.scss']
})
export class ManagePageComponent implements OnInit {
  constructor(private appService: AppService) {}

  ngOnInit() {}

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }
}
