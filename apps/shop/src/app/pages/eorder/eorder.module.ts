import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EorderComponent } from './eorder.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ManagePageComponent } from './manage-page/manage-page.component';
import { ToolPageComponent } from './tool-page/tool-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';
import { BankTransferPageComponent } from './bank-transfer-page/bank-transfer-page.component';
import { PricePageComponent } from './price-page/price-page.component';
import { PolicyPageComponent } from './policy-page/policy-page.component';
import { SupportPageComponent } from './support-page/support-page.component';
import { HeaderEorderComponent } from './header-eorder/header-eorder.component';


const components = [
    EorderComponent,
    HomePageComponent,
    ManagePageComponent,
    ToolPageComponent,
    BankTransferPageComponent,
    PricePageComponent,
    PolicyPageComponent,
    TutorialPageComponent,
    SupportPageComponent,
    HeaderEorderComponent
];
const pages = [
];

const routes: Routes = [
  {
    path: '',
    component: EorderComponent,
    children: [
      {
        path: 'homepage',
        component: HomePageComponent,
      },
      {
        path: 'manage',
        component: ManagePageComponent,
      },
      {
        path: 'bank-transfer',
        component: BankTransferPageComponent,
      },
      {
        path: 'price',
        component: PricePageComponent,
      },
      {
        path: 'policy',
        component: PolicyPageComponent,
      },
      {
        path: 'tutorial',
        component: TutorialPageComponent,
      },
      {
        path: 'support',
        component: SupportPageComponent,
      },
      {
        path: '**', redirectTo: 'homepage'
      }
    ]
  }
];

@NgModule({
  declarations: [
    ...components,
    ...pages,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports:[
    HeaderEorderComponent
  ]
})
export class EorderModule { }
