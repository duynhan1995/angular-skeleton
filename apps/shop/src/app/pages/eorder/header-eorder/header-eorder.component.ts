import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { EorderComponent } from '../eorder.component';

@Component({
  selector: 'shop-header-eorder',
  templateUrl: './header-eorder.component.html',
  styleUrls: ['./header-eorder.component.scss']
})
export class HeaderEorderComponent implements OnInit {
  constructor(private router: Router, private util: UtilService) { }

  ngOnInit() {
  }

  onRegister(){
    this.router.navigateByUrl(`s/${this.util.getSlug()}/eorder/manage`);
  }
  onTool() {
    window.open('https://chrome.google.com/webstore/detail/kpnaefmcmjiekbebcibeiiogffcgckkl', '_blank');
  }
}
