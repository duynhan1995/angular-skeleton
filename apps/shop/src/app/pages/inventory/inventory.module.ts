import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryComponent } from 'apps/shop/src/app/pages/inventory/inventory.component';
import { RouterModule, Routes } from '@angular/router';
import { CreateBalanceComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/create-balance/create-balance.component';
import { UpdateBalanceComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/update-balance/update-balance.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { NotPermissionModule } from 'libs/shared/components/not-permission/not-permission.module';
import { StocktakesModule } from 'apps/shop/src/app/pages/inventory/stocktakes/stocktakes.module';
import { InventoryVouchersModule } from 'apps/shop/src/app/pages/inventory/inventory-vouchers/inventory-vouchers.module';
import { CreateDiscardComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/create-discard/create-discard.component';
import { UpdateDiscardComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/update-discard/update-discard.component';
import { NotSubscriptionModule } from '@etop/shared/components/not-subscription/not-subscription.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';
import { StocktakesComponent } from './stocktakes/stocktakes.component';
import { InventoryVouchersComponent } from './inventory-vouchers/inventory-vouchers.component';

const routes: Routes = [
  {
    path: '',
    component: InventoryComponent,
    children: [
      {
        path: 'balances/create',
        component: CreateBalanceComponent
      },
      {
        path: 'balances/update/:id',
        component: UpdateBalanceComponent
      },
      {
        path: 'discards/create',
        component: CreateDiscardComponent
      },
      {
        path: 'discards/update/:id',
        component: UpdateDiscardComponent
      },
      {
        path: 'all',
        component: InventoryVouchersComponent
      },
      {
        path: 'in',
        component: InventoryVouchersComponent
      },
      {
        path: 'out',
        component: InventoryVouchersComponent
      },
      {
        path: 'stocktake',
        component: StocktakesComponent
      },
      {
        path: '**', redirectTo: 'all'
      }
    ]
  }
];

@NgModule({
  declarations: [
    InventoryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    StocktakesModule,
    InventoryVouchersModule,
    NotPermissionModule,
    NotSubscriptionModule,
    SubscriptionWarningModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InventoryModule { }
