import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { SubscriptionService } from '@etop/features';

@Component({
  selector: 'shop-inventory',
  template: `
    <shop-subscription-warning></shop-subscription-warning>
    <ng-container *ngIf="subscription; else notSubscription">
      <ng-container *ngIf="allowView; else notPermission">
      <router-outlet></router-outlet>
        <!-- <shop-inventory-vouchers
          *ngIf="(checkTab('all') | async) || (checkTab('in') | async) || (checkTab('out') | async)">
        </shop-inventory-vouchers>
        <shop-stocktakes *ngIf="checkTab('stocktake') | async"></shop-stocktakes> -->
      </ng-container>
      <ng-template #notPermission>
        <etop-not-permission></etop-not-permission>
      </ng-template>
    </ng-container>
    <ng-template #notSubscription>
      <etop-not-subscription></etop-not-subscription>
    </ng-template>
  `
})
export class InventoryComponent extends BaseComponent implements OnInit, OnDestroy {
  allowView = true;
  subscription = false;
  constructor(
    private util: UtilService,
    private router: Router,
    private headerController: HeaderControllerService,
    private inventoryStore: InventoryStoreService,
    private subscriptionService: SubscriptionService
  ) {
    super();
  }

  checkTab(tabName) {
    return this.inventoryStore.inventoryTypeChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  async ngOnInit() {
    this.subscription = await this.subscriptionService.checkSubcriptions();
    if(this.subscription){
      this.setupTabs();
      this.setupActions();

      this.inventoryStore.inventoryTypeChanged$.subscribe(_ => {
        this.setupTabs();
        this.setupActions();
      });
    }

  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }

  setupTabs() {
    this.headerController.setTabs([
      {
        title: 'Tất cả',
        name: 'all',
        active: false,
        onClick: () => {
          this.tabClick('all');
        }
      },
      {
        title: 'Nhập kho',
        name: 'in',
        active: false,
        onClick: () => {
          this.tabClick('in');
        }
      },
      {
        title: 'Xuất kho',
        name: 'out',
        active: false,
        onClick: () => {
          this.tabClick('out');
        }
      },
      {
        title: 'Kiểm kho',
        name: 'stocktake',
        active: false,
        onClick: () => {
          this.tabClick('stocktake');
        },
        permissions: ['shop/stocktake:view']
      }
    ].map(tab => {
      this.checkTab(tab.name).subscribe(active => tab.active = active);
      return tab;
    }));
  }

  setupActions() {
    this.headerController.setActions([
      {
        title: 'Nhập kho',
        cssClass: 'btn btn-primary btn-outline',
        isDropdown: true,
        permissions: ['shop/purchase_order:create','shop/order:create'],
        dropdownItems: [
          {
            title: 'Tạo đơn nhập hàng',
            onClick: () => this.createPurchaseOrder(),
            permissions: ['shop/purchase_order:create']
          },
          {
            title: 'Tạo đơn trả hàng',
            tooltips: 'Đang phát triển',
            disabled: true,
            permissions: ['shop/refund:create']
          }
        ]
      },
      {
        title: 'Xuất kho',
        cssClass: 'btn btn-primary btn-outline',
        isDropdown: true,
        permissions: ['shop/order:create','shop/purchase_order:create','shop/stocktake:create'],
        dropdownItems: [
          {
            title: 'Tạo đơn hàng',
            onClick: () => this.createOrder(),
            permissions: ['shop/order:create']
          },
          {
            title: 'Tạo đơn trả hàng nhập',
            tooltips: 'Đang phát triển',
            disabled: true,
            permissions: ['shop/purchaserefund:create']
          },
          {
            title: 'Tạo phiếu xuất huỷ',
            onClick: () => this.createDiscard(),
            permissions: ['shop/stocktake:create']
          }
        ]
      },
      {
        title: 'Kiểm kho',
        cssClass: 'btn btn-primary btn-outline',
        permissions: ['shop/stocktake:create'],
        onClick: () => this.createStocktake()
      }
    ]);
  }

  createDiscard() {
    window.open(`/s/${this.util.getSlug()}/inventory/discards/create`, '_self');
  }

  createStocktake() {
    window.open(`/s/${this.util.getSlug()}/inventory/balances/create`, '_self');
  }

  createPurchaseOrder() {
    window.open(`/s/${this.util.getSlug()}/orders/purchase-orders/create`, '_self');
  }

  createOrder() {
    window.open(`/s/${this.util.getSlug()}/pos`, '_blank');
  }

  private async tabClick(type) {
    await this.router.navigateByUrl(`s/${this.util.getSlug()}/inventory/${type}`);
  }

}
