import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InventoryVoucherListComponent} from 'apps/shop/src/app/pages/inventory/inventory-vouchers/inventory-voucher-list/inventory-voucher-list.component';
import {InventoryVoucherRowComponent} from 'apps/shop/src/app/pages/inventory/inventory-vouchers/components/inventory-voucher-row/inventory-voucher-row.component';
import {InventoryVoucherDetailComponent} from 'apps/shop/src/app/pages/inventory/inventory-vouchers/inventory-voucher-detail/inventory-voucher-detail.component';
import {InventoryVoucherVariantsComponent} from 'apps/shop/src/app/pages/inventory/inventory-vouchers/components/inventory-voucher-variants/inventory-voucher-variants.component';
import {InventoryVouchersComponent} from 'apps/shop/src/app/pages/inventory/inventory-vouchers/inventory-vouchers.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from '@etop/core';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {InventoryStoreService} from 'apps/core/src/stores/inventory.store.service';
import {InventoryApi} from '@etop/api';
import {InventoryService} from 'apps/shop/src/services/inventory.service';
import {EtopCommonModule} from '@etop/shared/components/etop-common/etop-common.module';
import {EmptyPageModule, EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {SideSliderModule} from '@etop/shared/components/side-slider/side-slider.module';

@NgModule({
  declarations: [
    InventoryVoucherListComponent,
    InventoryVoucherRowComponent,
    InventoryVoucherDetailComponent,
    InventoryVoucherVariantsComponent,
    InventoryVouchersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    AuthenticateModule,
    DropdownActionsModule,
    EtopCommonModule,
    EtopMaterialModule,
    SideSliderModule,
    EtopPipesModule,
    MaterialModule,
    EmptyPageModule
  ],
  exports: [
    InventoryVouchersComponent
  ],
  providers: [
    InventoryStoreService,
    InventoryApi,
    InventoryService
  ]
})
export class InventoryVouchersModule {
}
