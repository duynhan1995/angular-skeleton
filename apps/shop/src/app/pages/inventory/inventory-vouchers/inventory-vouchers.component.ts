import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';

@Component({
  selector: 'shop-inventory-vouchers',
  templateUrl: './inventory-vouchers.component.html',
  styleUrls: ['./inventory-vouchers.component.scss']
})
export class InventoryVouchersComponent extends PageBaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
