import { Component, Input, OnInit } from '@angular/core';
import { InventoryVoucher, InventoryVoucherTrader } from '@etop/models';
import { Router } from '@angular/router';
import { TraderStoreService } from 'apps/core/src/stores/trader.store.service';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';

@Component({
  selector: 'shop-inventory-voucher-detail',
  templateUrl: './inventory-voucher-detail.component.html',
  styleUrls: ['./inventory-voucher-detail.component.scss']
})
export class InventoryVoucherDetailComponent implements OnInit {
  @Input() voucher = new InventoryVoucher({});

  constructor(
    private router: Router,
    private traderStore: TraderStoreService,
    private orderStore: OrderStoreService,
    private inventoryStore: InventoryStoreService
  ) { }

  ngOnInit() {
  }

  viewDetailTrader(trader: InventoryVoucherTrader) {
    if (trader.deleted) { return; }
    this.traderStore.navigate(trader.type, null, trader.id);
  }

  viewDetailRef(voucher: InventoryVoucher) {
    switch (voucher.ref_type) {
      case 'order':
        return this.orderStore.navigate('so', voucher.ref_code);
      case 'purchase_order':
        return this.orderStore.navigate('po', voucher.ref_code);
      case 'stocktake':
        this.inventoryStore.navigate('stocktake', voucher.ref_code);
        return this.inventoryStore.changeInventoryType('stocktake');
      default:
        return;
    }
  }

}
