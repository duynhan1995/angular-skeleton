import { Component, Input, OnInit } from '@angular/core';
import { InventoryVoucherLine } from '@etop/models';

@Component({
  selector: 'shop-inventory-voucher-variants',
  templateUrl: './inventory-voucher-variants.component.html',
  styleUrls: ['./inventory-voucher-variants.component.scss']
})
export class InventoryVoucherVariantsComponent implements OnInit {
  @Input() lines: InventoryVoucherLine[] = [];
  @Input() voucher_type: string;

  constructor() { }

  ngOnInit() {
  }

}
