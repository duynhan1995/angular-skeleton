import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FilterOperator, Filters } from '@etop/models';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { InventoryVoucher, InventoryVoucherType } from '@etop/models';
import { InventoryService } from 'apps/shop/src/services/inventory.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-inventory-voucher-list',
  templateUrl: './inventory-voucher-list.component.html',
  styleUrls: ['./inventory-voucher-list.component.scss']
})
export class InventoryVoucherListComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('vouchersTable', { static: true }) vouchersTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  vouchers: Array<InventoryVoucher> = [];
  selectedVouchers: Array<InventoryVoucher> = [];
  filters: Filters = [];
  checkAll = false;

  page: number;
  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  voucher_type: InventoryVoucherType;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private headerController: HeaderControllerService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private inventoryStore: InventoryStoreService,
    private inventoryService: InventoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  private _onSelectModeChanged(value) {
    this.vouchersTable.toggleLiteMode(value);
    this.slider.toggleLiteMode(value);
  }

  ngOnInit() {
    const seletedType = this.router.url.split('/')[4];
    this.inventoryStore.changeInventoryType(seletedType);
    this.inventoryStore.inventoryTypeChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async (type: InventoryVoucherType) => {
        this.voucher_type = type;

        if (type == 'in' || type == 'out' || type == 'all') {
          // NOTE: because inventory IN && OUT use same component
          this.filters = [];
          this.vouchersTable.resetPagination();
        }
      });
  }

  async filter($event: Filters) {
    this.selectedVouchers = [];
    this.filters = $event;
    this.vouchersTable.resetPagination();
  }

  async getInventoryVouchers(page?, perpage?) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.vouchersTable.toggleNextDisabled(false);

      this.filters.push({
        name: "type",
        op: FilterOperator.eq,
        value: this.voucher_type
      });

      let _vouchers;

      if (this.filters[0].value == 'all'){
        _vouchers = await this.inventoryService.getInventoryVouchers(
          ((page || this.page) - 1) * perpage,
          perpage || this.perpage,
        );
      }else{
        _vouchers = await this.inventoryService.getInventoryVouchers(
          ((page || this.page) - 1) * perpage,
          perpage || this.perpage,
          this.filters
        );
      }
      if (page > 1 && _vouchers.length == 0) {
        this.vouchersTable.toggleNextDisabled(true);
        this.vouchersTable.decreaseCurrentPage(1);
        toastr.info(`Bạn đã xem tất cả phiếu ${this.voucher_type == 'in' && 'nhập' || 'xuất'} kho.`);
        return;
      }
      this.vouchers = _vouchers.map(v => {
        v.p_data = {};
        return v;
      });
      if (this.queryParams.code) {
        this.detail(null, this.vouchers[0]);
      }

      // Remember here in-order-to reload customer_list after updating vouchers
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list vouchers', e);
    }
  }

  async loadPage({ page, perpage }) {
    this.vouchersTable.loading = true;
    this.changeDetector.detectChanges();
    this.filters = [];
    this._paramsHandling();
    await this.getInventoryVouchers(page, perpage);
    this.vouchersTable.loading = false;
    this.changeDetector.detectChanges();
  }

  private async _checkSelectMode() {
    this.selectedVouchers = this.vouchers.filter(i_v =>
      i_v.p_data.selected || i_v.p_data.detailed
    );
    this.selectMode = this.selectedVouchers.length > 0;
    if (this.queryParams.code && this.selectedVouchers.length == 0) {
      this.filters = [];
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/inventory`);
      this.vouchersTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.vouchers.forEach(v => {
      v.p_data.selected = false;
      v.p_data.detailed = false;
    });
    this.selectedVouchers = [];
    this.checkAll = false;
    this._checkSelectMode();
  }

  detail(event, voucher: InventoryVoucher) {
    if (event && event.target.type == 'checkbox') { return; }
    this.vouchers.forEach(v => {
      v.p_data.detailed = false;
    });
    this.checkAll = false;
    voucher.p_data.detailed = true;
    this.selectedVouchers = [voucher];
    this.selectMode = true;
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    if (['in', 'out'].indexOf(this.queryParams.type) == -1) {
      return;
    }
    this.filters = [{
      name: 'code',
      op: FilterOperator.eq,
      value: this.queryParams.code
    }];
  }

  get sliderTitle() {
    return `Chi tiết phiếu ${this.voucherName} kho`;
  }

  get emptyResultFilter() {
    // NOTE why > 1? Because in InventoryVoucher list, filters[0] is always
    //  { name: "type", op: "=", value: "in" || "out" }
    return this.page == 1 && this.vouchers.length == 0 && this.filters.length > 1;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return `Không tìm thấy phiếu ${this.voucherName} kho phù hợp`;
    }
    return `Cửa hàng của bạn chưa có phiếu ${this.voucherName} kho`;
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get showPaging() {
    return !this.vouchersTable.liteMode && !this.vouchersTable.loading;
  }

  get voucherName() {
    return this.voucher_type == 'in' && 'nhập' || 'xuất';
  }

}
