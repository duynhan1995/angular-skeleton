import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { Variant } from '@etop/models';
import { DiscardLinesComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/components/discard-lines/discard-lines.component';

@Component({
  selector: 'shop-create-discard',
  templateUrl: './create-discard.component.html',
  styleUrls: ['./create-discard.component.scss']
})
export class CreateDiscardComponent implements OnInit {
  @ViewChild('discardLines', {static: true}) discardLines: DiscardLinesComponent;
  search_variant_list: Variant[] = [];

  filterVariantsCallback = (variant: Variant) => {
    return variant.inventory_variant && variant.inventory_variant.quantity > 0;
  };

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private util: UtilService,
    private inventoryStore: InventoryStoreService
  ) { }

  ngOnInit() {
    this.inventoryStore.changeInventoryType(null);
    this.headerController.clearActions();
    this.headerController.clearTabs();
    this.inventoryStore.stocktakeRecentlyCreated$.subscribe(recentlyCreated => {
      if (recentlyCreated) {
        this.search_variant_list = [];
      }
    });
  }

  onVariantSearched(variant) {
    const index = this.search_variant_list.findIndex(item => item.id == variant.id);
    if (index == -1) {
      this.search_variant_list.push(variant);
      const idx = this.search_variant_list.length - 1;
      this.search_variant_list[idx].p_data = {
        ...this.search_variant_list[idx].p_data,
        old_quantity: variant.inventory_variant && variant.inventory_variant.quantity || 0,
        discard_quantity: null,
        new_quantity: null,
        cost_price: variant.cost_price
      };
      if (this.discardLines) {
        this.discardLines.lineUpdated();
      }
    }
  }

  gotoStocktakes() {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/inventory`);
    this.inventoryStore.changeInventoryType('stocktake');
  }

}
