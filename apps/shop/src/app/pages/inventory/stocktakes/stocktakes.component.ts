import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';

@Component({
  selector: 'shop-stocktakes',
  templateUrl: './stocktakes.component.html',
  styleUrls: ['./stocktakes.component.scss']
})
export class StocktakesComponent extends PageBaseComponent implements OnInit {

  constructor(
    private inventoryStore: InventoryStoreService,
  ) {
    super();
  }

  ngOnInit() {
    this.inventoryStore.changeInventoryType('stocktake');
  }

}
