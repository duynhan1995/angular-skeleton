import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { InventoryVoucher, Stocktake } from '@etop/models';
import { InventoryApi, StocktakeApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-discard-detail',
  templateUrl: './discard-detail.component.html',
  styleUrls: ['./discard-detail.component.scss']
})
export class DiscardDetailComponent implements OnInit, OnChanges {
  @Input() discard = new Stocktake({});
  vouchers: InventoryVoucher[] = [];
  voucher: InventoryVoucher;

  private modal: any;

  constructor(
    private stocktakeApi: StocktakeApi,
    private inventoryApi: InventoryApi,
    private dialog: DialogControllerService,
    private inventoryStore: InventoryStoreService,
    private util: UtilService
  ) { }

  get isNew() {
    return ['N', 'P'].indexOf(this.discard.status) == -1;
  }

  ngOnInit() {}

  async ngOnChanges(changes: SimpleChanges) {
    this.vouchers = await this.inventoryApi.getInventoryVouchersByReference(
      this.discard.id, 'stocktake'
    );
    this.voucher = this.vouchers && this.vouchers[0];
  }

  async confirm() {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu xuất huỷ #${this.discard.code}`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu xuất huỷ #${this.discard.code}?</div>
        <div>Sau khi xác nhận, hệ thống sẽ tự động sinh ra các <strong>phiếu xuất kho</strong> tương ứng.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi các phiếu xuất kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.stocktakeApi.confirmStocktake(this.discard.id, "confirm");
          toastr.success(`Xác nhận phiếu xuất huỷ #${this.discard.code} thành công.`);
          this.inventoryStore.stocktakeRecentlyUpdated();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Stocktake', e);
          toastr.error(`Xác nhận phiếu xuất huỷ #${this.discard.code} không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  updateStocktake() {
    const slug = this.util.getSlug();
    window.open(`/s/${slug}/inventory/discards/update/${this.discard.id}`, '_self');
  }

  viewDetailVoucher(voucher: InventoryVoucher) {
    this.inventoryStore.navigate(voucher.type, voucher.code);
    return this.inventoryStore.changeInventoryType(voucher.type);
  }

}
