import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Stocktake } from 'libs/models/Stocktake';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { InventoryApi, StocktakeApi } from '@etop/api';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { InventoryVoucher } from 'libs/models/Inventory';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'shop-balance-detail',
  templateUrl: './balance-detail.component.html',
  styleUrls: ['./balance-detail.component.scss']
})
export class BalanceDetailComponent implements OnInit, OnChanges {
  @Input() balance = new Stocktake({});
  vouchers: InventoryVoucher[] = [];
  voucher_in = new InventoryVoucher({});
  voucher_out = new InventoryVoucher({});

  private modal: any;

  constructor(
    private stocktakeApi: StocktakeApi,
    private inventoryApi: InventoryApi,
    private dialog: DialogControllerService,
    private inventoryStore: InventoryStoreService,
    private util: UtilService,
    private auth: AuthenticateStore
  ) { }

  get isNew() {
    return ['N', 'P'].indexOf(this.balance.status) == -1;
  }

  ngOnInit() {}

  async ngOnChanges(changes: SimpleChanges) {
    this.vouchers = await this.inventoryApi.getInventoryVouchersByReference(
      this.balance.id, 'stocktake'
    );
    this.voucher_in = this.vouchers.find(v => v.type == 'in');
    this.voucher_out = this.vouchers.find(v => v.type == 'out');
  }

  async confirm() {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu kiểm kho #${this.balance.code}`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu kiểm kho #${this.balance.code}?</div>
        <div>Sau khi xác nhận, hệ thống sẽ tự động sinh ra các <strong>phiếu nhập/xuất kho</strong> tương ứng.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi các phiếu nhập/xuất kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          await this.stocktakeApi.confirmStocktake(this.balance.id, "confirm");
          toastr.success(`Xác nhận phiếu kiểm kho #${this.balance.code} thành công.`);
          this.inventoryStore.stocktakeRecentlyUpdated();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Stocktake', e);
          toastr.error(`Xác nhận phiếu kiểm kho #${this.balance.code} không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  get updateStocktakePermission() {
    return (this.auth.snapshot.permission.permissions.indexOf('shop/stocktake:update') != -1) || (this.auth.snapshot.permission.permissions.indexOf('shop/stocktake:self_update') != -1 && this.balance?.created_by === this.auth.snapshot.user.id);
  }

  updateStocktake() {
    const slug = this.util.getSlug();
    window.open(`/s/${slug}/inventory/balances/update/${this.balance.id}`, '_self');
  }

  viewDetailVoucher(voucher: InventoryVoucher) {
    this.inventoryStore.navigate(voucher.type, voucher.code);
    return this.inventoryStore.changeInventoryType(voucher.type);
  }

}
