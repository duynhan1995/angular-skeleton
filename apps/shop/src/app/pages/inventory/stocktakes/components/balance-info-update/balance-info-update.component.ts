import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Stocktake, StocktakeLine } from 'libs/models/Stocktake';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { StocktakeApi } from '@etop/api';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-balance-info-update',
  templateUrl: './balance-info-update.component.html',
  styleUrls: ['./balance-info-update.component.scss']
})
export class BalanceInfoUpdateComponent extends BaseComponent implements OnInit {
  @Input() balance = new Stocktake({});

  loading = false;
  updateAndConfirm = false;

  private modal: any;

  constructor(
    private auth: AuthenticateStore,
    private inventoryStore: InventoryStoreService,
    private dialog: DialogControllerService,
    private stocktakeApi: StocktakeApi
  ) {
    super();
  }

  get stocktaker() {
    return this.auth.snapshot.user.full_name;
  }

  get isNew() {
    return ['N', 'P'].indexOf(this.balance.status) == -1;
  }

  ngOnInit() {
    this.inventoryStore.stocktakeLinesChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(lines => {
        this.balance.p_data.lines = lines;
      });
    this.inventoryStore.stocktakeQuantityChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(total_quantity => {
        this.balance.p_data.total_quantity = total_quantity;
      });
  }

  checkInvalidLines(lines: StocktakeLine[]) {
    let invalid_index: number;
    const hasZeroQuantity = lines.some((l, index) => {
      if (!l.new_quantity && l.new_quantity != 0) {
        invalid_index = index + 1;
        return true;
      }
    });
    return {invalid: hasZeroQuantity, invalid_index};
  }

  async update(updateAndConfirm: boolean) {
    this.loading = true;
    this.updateAndConfirm = updateAndConfirm;
    try {
      const body: Stocktake = {
        id: this.balance.id,
        ...this.balance.p_data,
      };

      if (!body.lines || !body.lines.length) {
        this.loading = false;
        this.updateAndConfirm = false;
        return toastr.error('Chưa chọn sản phẩm!');
      }
      const invalid_lines = this.checkInvalidLines(body.lines);
      if (invalid_lines.invalid) {
        this.loading = false;
        this.updateAndConfirm = false;
        return toastr.error(`Chưa nhập số lượng ở sản phẩm thứ ${invalid_lines.invalid_index}!`);
      }

      if (updateAndConfirm) {
        this.confirm(body);
      } else {
        await this.stocktakeApi.updateStocktake(body);
        toastr.success(`Cập nhật phiếu kiểm kho thành công.`);
      }
    } catch (e) {
      debug.error('ERROR in updating Stocktake', e);
      toastr.error(`Cập nhật phiếu kiểm kho không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.updateAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu kiểm kho`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu kiểm kho?</div>
        <div>Sau khi xác nhận, hệ thống sẽ tự động sinh ra các <strong>phiếu nhập/xuất kho</strong> tương ứng.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi các phiếu nhập/xuất kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const res = await this.stocktakeApi.updateStocktake(body);
          toastr.success(`Cập nhật phiếu kiểm kho thành công.`);
          await this.stocktakeApi.confirmStocktake(res.id, "confirm");
          toastr.success(`Xác nhận phiếu kiểm kho thành công.`);
          this.resetData();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Stocktake', e);
          toastr.error(`Xác nhận phiếu kiểm kho không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  resetData() {
    this.inventoryStore.stocktakeRecentlyUpdated();
  }

}
