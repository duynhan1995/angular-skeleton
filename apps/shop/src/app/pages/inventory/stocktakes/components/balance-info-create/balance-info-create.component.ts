import { Component, OnInit } from '@angular/core';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { Stocktake, StocktakeLine } from 'libs/models/Stocktake';
import { StocktakeApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-balance-info-create',
  templateUrl: './balance-info-create.component.html',
  styleUrls: ['./balance-info-create.component.scss']
})
export class BalanceInfoCreateComponent extends BaseComponent implements OnInit {
  balance = new Stocktake({});
  balance_id: string;

  loading = false;
  createAndConfirm = false;

  private modal: any;

  constructor(
    private auth: AuthenticateStore,
    private dialog: DialogControllerService,
    private stocktakeApi: StocktakeApi,
    private inventoryStore: InventoryStoreService
  ) {
    super();
  }

  get stocktaker() {
    return this.auth.snapshot.user.full_name;
  }

  get currentTime() {
    return new Date();
  }

  ngOnInit() {
    this.inventoryStore.stocktakeLinesChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(lines => {
        this.balance.p_data.lines = lines;
      });
    this.inventoryStore.stocktakeQuantityChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(total_quantity => {
        this.balance.p_data.total_quantity = total_quantity;
      });
  }

  resetData() {
    this.balance_id = null;
    this.balance = new Stocktake({
      p_data: {}
    });
    this.inventoryStore.stocktakeRecentlyCreated();
  }

  checkInvalidLines(lines: StocktakeLine[]) {
    let invalid_index: number;
    const hasZeroQuantity = lines.some((l, index) => {
      if (!l.new_quantity && l.new_quantity != 0) {
        invalid_index = index + 1;
        return true;
      }
    });
    return {invalid: hasZeroQuantity, invalid_index};
  }

  async create(createAndConfirm: boolean) {
    this.loading = true;
    this.createAndConfirm = createAndConfirm;
    try {
      const body: Stocktake = {
        ...this.balance.p_data,
        type: 'balance'
      };

      if (!body.lines || !body.lines.length) {
        this.loading = false;
        this.createAndConfirm = false;
        return toastr.error('Chưa chọn sản phẩm!');
      }
      const invalid_lines = this.checkInvalidLines(body.lines);
      if (invalid_lines.invalid) {
        this.loading = false;
        this.createAndConfirm = false;
        return toastr.error(`Chưa nhập số lượng ở sản phẩm thứ ${invalid_lines.invalid_index}!`);
      }

      if (createAndConfirm) {
        this.confirm(body);
      } else {
        await this.stocktakeApi.createStocktake(body);
        toastr.success(`Tạo phiếu kiểm kho thành công.`);
        this.resetData();
      }
    } catch (e) {
      debug.error('ERROR in creating Stocktake', e);
      toastr.error(`Tạo phiếu kiểm kho không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.createAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu kiểm kho`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu kiểm kho?</div>
        <div>Sau khi xác nhận, hệ thống sẽ tự động sinh ra các <strong>phiếu nhập/xuất kho</strong> tương ứng.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi các phiếu nhập/xuất kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          let res: any;
          if (!this.balance_id) {
            res = await this.stocktakeApi.createStocktake(body);
            this.balance_id = res.id;
          } else {
            res = await this.stocktakeApi.updateStocktake({...body, id: this.balance_id});
          }

          await this.stocktakeApi.confirmStocktake(res.id, "confirm");
          toastr.success(`Xác nhận phiếu kiểm kho thành công.`);
          this.resetData();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Xác nhận phiếu kiểm kho không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

}
