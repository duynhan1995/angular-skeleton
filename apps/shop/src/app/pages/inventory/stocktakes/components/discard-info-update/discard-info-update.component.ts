import { Component, Input, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Stocktake, StocktakeLine } from 'libs/models/Stocktake';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { StocktakeApi } from '@etop/api';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-discard-info-update',
  templateUrl: './discard-info-update.component.html',
  styleUrls: ['./discard-info-update.component.scss']
})
export class DiscardInfoUpdateComponent extends BaseComponent implements OnInit {
  @Input() discard = new Stocktake({});

  loading = false;
  updateAndConfirm = false;

  private modal: any;

  constructor(
    private auth: AuthenticateStore,
    private inventoryStore: InventoryStoreService,
    private dialog: DialogControllerService,
    private stocktakeApi: StocktakeApi
  ) {
    super();
  }

  get stocktaker() {
    return this.auth.snapshot.user.full_name;
  }

  get isNew() {
    return ['N', 'P'].indexOf(this.discard.status) == -1;
  }

  ngOnInit() {
    this.inventoryStore.stocktakeLinesChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(lines => {
        this.discard.p_data.lines = lines;
      });
    this.inventoryStore.discardQuantityChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(discard_quantity => {
        this.discard.p_data.discard_quantity = discard_quantity;
      });
    this.inventoryStore.stocktakeQuantityChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(total_quantity => {
        this.discard.p_data.total_quantity = total_quantity;
      });
  }

  checkInvalidLines(lines: StocktakeLine[]) {
    let invalid_index: number;

    for (let i = 0; i < lines.length; i++) {
      const l = lines[i];
      if (!l.discard_quantity && l.discard_quantity != 0) {
        invalid_index = i + 1;
        return {null_quantity: true, invalid_index};
      }
      if (l.new_quantity < 0) {
        invalid_index = i + 1;
        return {negative_quantity: true, invalid_index};
      }
    }
    return null;
  }

  async update(updateAndConfirm: boolean) {
    this.loading = true;
    this.updateAndConfirm = updateAndConfirm;
    try {
      const { lines, note, total_quantity } = this.discard.p_data;
      const body = {
        id: this.discard.id,
        lines, note, total_quantity
      };

      if (!body.lines || !body.lines.length) {
        this.loading = false;
        this.updateAndConfirm = false;
        return toastr.error('Chưa chọn sản phẩm!');
      }

      const invalid_lines = this.checkInvalidLines(body.lines);
      if (invalid_lines) {
        this.loading = false;
        this.updateAndConfirm = false;
        const index = invalid_lines.invalid_index;
        if (invalid_lines.null_quantity) {
          return toastr.error(`Chưa nhập số lượng huỷ ở sản phẩm thứ ${index}!`);
        }
        if (invalid_lines.negative_quantity) {
          return toastr.error(`SL huỷ không được lớn SL hiện tại ở sản phẩm thứ ${index}!`);
        }
      }

      if (updateAndConfirm) {
        this.confirm(body);
      } else {
        await this.stocktakeApi.updateStocktake(body);
        toastr.success(`Cập nhật phiếu xuất huỷ thành công.`);
      }
    } catch (e) {
      debug.error('ERROR in updating Stocktake', e);
      toastr.error(`Cập nhật phiếu xuất huỷ không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.updateAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu xuất huỷ`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu xuất huỷ?</div>
        <div>Sau khi xác nhận, hệ thống sẽ tự động sinh ra các <strong>phiếu xuất kho</strong> tương ứng.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi các phiếu xuất kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          const res = await this.stocktakeApi.updateStocktake(body);
          await this.stocktakeApi.confirmStocktake(res.id, "confirm");
          toastr.success(`Xác nhận phiếu xuất huỷ thành công.`);
          this.resetData();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Stocktake', e);
          toastr.error(`Xác nhận phiếu xuất huỷ không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

  resetData() {
    this.inventoryStore.stocktakeRecentlyUpdated();
  }

}
