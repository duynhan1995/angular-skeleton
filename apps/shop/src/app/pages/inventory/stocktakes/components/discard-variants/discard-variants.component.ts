import { Component, Input, OnInit } from '@angular/core';
import { StocktakeLine } from 'libs/models/Stocktake';

@Component({
  selector: 'shop-discard-variants',
  templateUrl: './discard-variants.component.html',
  styleUrls: ['./discard-variants.component.scss']
})
export class DiscardVariantsComponent implements OnInit {
  @Input() lines: StocktakeLine[] = [];

  constructor() { }

  ngOnInit() {
  }

}
