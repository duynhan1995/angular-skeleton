import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Variant } from 'libs/models/Product';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { StocktakeLine } from 'libs/models/Stocktake';

@Component({
  selector: 'shop-discard-lines',
  templateUrl: './discard-lines.component.html',
  styleUrls: ['./discard-lines.component.scss']
})
export class DiscardLinesComponent implements OnInit, OnChanges {
  @Input() variants: Variant[] = [];
  @Input() isNewDiscard = true;

  constructor(
    private inventoryStore: InventoryStoreService,
    private util: UtilService
  ) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    this.lineUpdated();
  }

  discardAmount(variant: Variant) {
    if (!variant.p_data.discard_quantity) {
      return 0;
    }
    return variant.p_data.discard_quantity * variant.p_data.cost_price;
  }

  errorQuantity(variant) {
    const old_quantity = variant.p_data.old_quantity || 0;
    const discard_quantity = variant.p_data.discard_quantity || 0;
    return old_quantity < discard_quantity;
  }

  numberOnly(event) {
    return this.util.numberOnly(event);
  }

  removeLine(index) {
    this.variants.splice(index, 1);
    this.lineUpdated();
  }

  lineUpdated() {
    const lines: StocktakeLine[] = [];
    for (let v of this.variants) {
      const old_quantity = v.p_data.old_quantity || 0;
      const discard_quantity = v.p_data.discard_quantity;
      lines.push({
        ...new StocktakeLine(),
        variant_id: v.id,
        old_quantity: old_quantity,
        discard_quantity: discard_quantity,
        new_quantity: old_quantity - discard_quantity
      });
    }

    const total_discard_quantity = lines.reduce((a, b) => a + Number(b.discard_quantity || 0), 0);
    const total_quantity = lines.reduce((a, b) => a + Number(b.new_quantity || 0), 0);
    this.inventoryStore.changeStocktakeLines(lines);
    this.inventoryStore.changeDiscardQuantity(total_discard_quantity);
    this.inventoryStore.changeStocktakeQuantity(total_quantity);
  }

}
