import { Component, Input, OnInit } from '@angular/core';
import { Stocktake } from 'libs/models/Stocktake';

@Component({
  selector: '[shop-stocktake-row]',
  templateUrl: './stocktake-row.component.html',
  styleUrls: ['./stocktake-row.component.scss']
})
export class StocktakeRowComponent implements OnInit {
  @Input() stocktake = new Stocktake({});
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
