import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Variant } from 'libs/models/Product';
import { StocktakeLine } from 'libs/models/Stocktake';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-balance-lines',
  templateUrl: './balance-lines.component.html',
  styleUrls: ['./balance-lines.component.scss']
})
export class BalanceLinesComponent implements OnInit, OnChanges {
  @Input() variants: Variant[] = [];
  @Input() isNewBalance = true;

  tabs: any[] = [];
  active_tab = 'all';

  constructor(
    private inventoryStore: InventoryStoreService,
    private util: UtilService
  ) { }

  ngOnInit() {
    this.tabs = [
      { name: 'Tất cả', value: 'all' },
      { name: 'Lệch', value: 'diff' },
      { name: 'Khớp', value: 'equal' }
    ];
  }

  ngOnChanges(changes: SimpleChanges) {
    this.lineUpdated();
  }

  diff(data) {
    if (!data) {
      return 0;
    }
    return (data.new_quantity - data.old_quantity) || 0;
  }

  diffPrice(data) {
    if (!data) {
      return 0;
    }
    return (data.cost_price * this.diff(data)) || 0;
  }

  numberOnly(event) {
    return this.util.numberOnly(event);
  }

  removeLine(index) {
    this.variants.splice(index, 1);
    this.lineUpdated();
  }

  changeTab(value) {
    this.active_tab = value;
  }

  lineUpdated() {
    const lines: StocktakeLine[] = this.variants.map(v => {
      return {
        ...new StocktakeLine(),
        variant_id: v.id,
        old_quantity: v.p_data.old_quantity || 0,
        new_quantity: v.p_data.new_quantity,
      }
    });
    const total_quantiy = lines.reduce((a, b) => a + Number(b.new_quantity || 0), 0);
    this.inventoryStore.changeStocktakeLines(lines);
    this.inventoryStore.changeStocktakeQuantity(total_quantiy);
  }

  lineCount(type: 'all' | 'diff' | 'equal'): number {
    switch (type) {
      case 'all':
        return this.variants.length;
      case 'diff':
        return this.variants.filter(v => v.p_data.new_quantity - v.p_data.old_quantity != 0).length;
      case 'equal':
        return this.variants.filter(v => v.p_data.new_quantity - v.p_data.old_quantity == 0).length;
      default:
        return this.variants.length;
    }
  }

  lineShown(line) {
    switch (this.active_tab) {
      case 'all':
        return true;
      case 'diff':
        return line.p_data.new_quantity - line.p_data.old_quantity != 0;
      case 'equal':
        return line.p_data.new_quantity - line.p_data.old_quantity == 0;
      default:
        return true;
    }
  }

}
