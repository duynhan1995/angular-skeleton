import { Component, OnInit, Input } from '@angular/core';
import { StocktakeLine } from 'libs/models/Stocktake';

@Component({
  selector: 'shop-balance-variants',
  templateUrl: './balance-variants.component.html',
  styleUrls: ['./balance-variants.component.scss']
})
export class BalanceVariantsComponent implements OnInit {
  @Input() lines: StocktakeLine[] = [];

  constructor() { }

  ngOnInit() {
  }

  diff(line: StocktakeLine) {
    if (!line) { return 0; }
    return (line.new_quantity || 0) - (line.old_quantity || 0);
  }

}
