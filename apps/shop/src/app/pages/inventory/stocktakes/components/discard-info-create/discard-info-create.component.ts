import { Component, OnInit } from '@angular/core';
import { Stocktake, StocktakeLine } from 'libs/models/Stocktake';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { StocktakeApi } from '@etop/api';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-discard-info-create',
  templateUrl: './discard-info-create.component.html',
  styleUrls: ['./discard-info-create.component.scss']
})
export class DiscardInfoCreateComponent extends BaseComponent implements OnInit {
  discard = new Stocktake({});
  discard_id: string;

  loading = false;
  createAndConfirm = false;

  private modal: any;

  constructor(
    private auth: AuthenticateStore,
    private dialog: DialogControllerService,
    private stocktakeApi: StocktakeApi,
    private inventoryStore: InventoryStoreService
  ) {
    super();
  }

  get stocktaker() {
    return this.auth.snapshot.user.full_name;
  }

  get currentTime() {
    return new Date();
  }

  ngOnInit() {
    this.inventoryStore.stocktakeLinesChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(lines => {
        this.discard.p_data.lines = lines;
      });
    this.inventoryStore.discardQuantityChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(discard_quantity => {
        this.discard.p_data.discard_quantity = discard_quantity;
      });
    this.inventoryStore.stocktakeQuantityChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(total_quantity => {
        this.discard.p_data.total_quantity = total_quantity;
      });
  }

  resetData() {
    this.discard_id = null;
    this.discard = new Stocktake({
      p_data: {}
    });
    this.inventoryStore.stocktakeRecentlyCreated();
  }

  checkInvalidLines(lines: StocktakeLine[]) {
    let invalid_index: number;

    for (let i = 0; i < lines.length; i++) {
      const l = lines[i];
      if (!l.discard_quantity && l.discard_quantity != 0) {
        invalid_index = i + 1;
        return {null_quantity: true, invalid_index};
      }
      if (l.new_quantity < 0) {
        invalid_index = i + 1;
        return {negative_quantity: true, invalid_index};
      }
    }
    return null;
  }

  async create(createAndConfirm: boolean) {
    this.loading = true;
    this.createAndConfirm = createAndConfirm;
    try {
      const { lines, note, total_quantity } = this.discard.p_data;
      const body = {
        lines, note, total_quantity,
        type: 'discard'
      };

      if (!body.lines || !body.lines.length) {
        this.loading = false;
        this.createAndConfirm = false;
        return toastr.error('Chưa chọn sản phẩm!');
      }

      const invalid_lines = this.checkInvalidLines(body.lines);
      if (invalid_lines) {
        this.loading = false;
        this.createAndConfirm = false;
        const index = invalid_lines.invalid_index;
        if (invalid_lines.null_quantity) {
          return toastr.error(`Chưa nhập số lượng huỷ ở sản phẩm thứ ${index}!`);
        }
        if (invalid_lines.negative_quantity) {
          return toastr.error(`SL huỷ không được lớn SL hiện tại ở sản phẩm thứ ${index}!`);
        }
      }

      if (createAndConfirm) {
        this.confirm(body);
      } else {
        await this.stocktakeApi.createStocktake(body);
        toastr.success(`Tạo phiếu xuất huỷ thành công.`);
        this.resetData();
      }
    } catch (e) {
      debug.error('ERROR in creating Stocktake', e);
      toastr.error(`Tạo phiếu xuất huỷ không thành công.`, e.message || e.msg);
    }
    this.loading = false;
    this.createAndConfirm = false;
  }

  confirm(body) {
    this.modal = this.dialog.createConfirmDialog({
      title: `Xác nhận phiếu xuất huỷ`,
      body: `
        <div>Bạn có chắc muốn xác nhận phiếu xuất huỷ?</div>
        <div>Sau khi xác nhận, hệ thống sẽ tự động sinh ra các <strong>phiếu xuất kho</strong> tương ứng.</div>
        <div>Bạn có thể vào mục <strong>Quản lý tồn kho</strong> để theo dõi các phiếu xuất kho.</div>
      `,
      cancelTitle: 'Đóng',
      closeAfterAction: false,
      onConfirm: async () => {
        try {
          let res: any;
          if (!this.discard_id) {
            res = await this.stocktakeApi.createStocktake(body);
            this.discard_id = res.id;
          } else {
            res = await this.stocktakeApi.updateStocktake({...body, id: this.discard_id});
          }

          await this.stocktakeApi.confirmStocktake(res.id, "confirm");
          toastr.success(`Xác nhận phiếu xuất huỷ thành công.`);
          this.resetData();
          this.modal.close();
          this.modal = null;
        } catch (e) {
          debug.error('ERROR in confirming Receipt', e);
          toastr.error(`Xác nhận phiếu xuất huỷ không thành công.`, e.message || e.msg);
        }
      },
      onCancel: () => {
        this.modal = null;
      }
    });
    this.modal.show();
  }

}
