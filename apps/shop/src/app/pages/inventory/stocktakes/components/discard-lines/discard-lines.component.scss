:host {
  height: calc(100% - 60px);
  overflow: hidden;
}

.table-responsive {
  overflow-y: auto;
}

thead {
  background-color: var(--light);
}
th, td {
  padding: .6rem 1.25rem;
}
th {
  font-weight: 400;
  border-bottom: 1px solid var(--border-gray-color);
  border-top: 1px solid var(--border-gray-color);
}
tr {
  border-bottom: 1px solid var(--border-gray-color);
}

.product-img {
  width: 4rem;
  height: 4rem;
  object-fit: cover;
  border-radius: 5px;
  border: 1px solid var(--border-gray-color);
}

.product-info {
  * {
    font-size: .95rem;
  }
}

.download-sample {
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
}

.blank-page {
  height: calc(100% - 35px);
  padding-top: 0;
  .info {
    border-bottom: 1px solid var(--border-gray-color);
    width: 45rem;
    padding-bottom: 1.65rem;
  }
  .empty-text {
    color: var(--default-color);
    font-size: 1.3rem;
    text-align: center;
  }
}

.remove-variant {
  border: 1px solid var(--border-gray-color);
  width: 1.8rem;
  height: 1.8rem;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 100%;
}

.inventory-input {
  color: var(--primary-color);
  font-weight: 700;
  text-align: right;
  height: 30px;
  padding: 0;
  &::placeholder {
    color: var(--default-color);
  }
  &:focus {
    border-color: var(--border-gray-color) !important;
  }
  &.error {
    color: var(--danger-color);
  }
}

etop-currency-input::ng-deep {
  .form-control {
    height: 30px;
  }
}

.tabs-container {
  display: flex;
  height: 2.5rem;
  margin-left: 1.25rem;
}

.tab {
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 1.65rem;
  border-bottom: 2px solid var(--border-gray-color);
  border-top: 3px solid transparent;
  color: var(--dark-color);
  cursor: pointer;
  &.active {
    color: var(--primary-color);
    border-bottom: 2px solid var(--primary-color);
  }
}
