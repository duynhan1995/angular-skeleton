import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FilterOperator, Filters } from '@etop/models';
import { EtopTableComponent } from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import { SideSliderComponent } from 'libs/shared/components/side-slider/side-slider.component';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { Stocktake } from '@etop/models';
import { StocktakeApi } from '@etop/api';
import { StocktakeService } from 'apps/shop/src/services/stocktake.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { DropdownActionsControllerService } from 'apps/shared/src/components/dropdown-actions/dropdown-actions-controller.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { CancelObjectComponent } from 'apps/shop/src/app/components/cancel-object/cancel-object.component';
import { ActionButton, EmptyType } from '@etop/shared';

@Component({
  selector: 'shop-stocktake-list',
  templateUrl: './stocktake-list.component.html',
  styleUrls: ['./stocktake-list.component.scss']
})
export class StocktakeListComponent extends BaseComponent implements OnInit {
  @ViewChild('stocktakeTable', { static: true }) stocktakeTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  stocktakes: Array<Stocktake> = [];
  selectedStocktakes: Array<Stocktake> = [];
  filters: Filters = [];
  emptyAction: ActionButton[] = [];
  checkAll = false;

  page: number;
  perpage: number;

  queryParams = {
    code: '',
    type: '',
  };

  private modal: any;

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  constructor(
    private stocktakeApi: StocktakeApi,
    private stocktakeService: StocktakeService,
    private headerController: HeaderControllerService,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private modalController: ModalController,
    private inventoryStore: InventoryStoreService,
    private dropdownController: DropdownActionsControllerService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  private _onSelectModeChanged(value) {
    this.stocktakeTable.toggleLiteMode(value);
    this.slider.toggleLiteMode(value);
  }

  ngOnInit() {
    this.emptyAction = [
      {
        title: 'Kiểm kho',
        cssClass: 'btn-primary',
        onClick: () => this.createStocktake()
      }
    ]
    this.inventoryStore.stocktakeRecentlyUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(stocktakeRecentlyUpdated => {
        if (stocktakeRecentlyUpdated) {
          this.reloadStocktakes();
        }
      });
  }

  async filter($event: Filters) {
    this.selectedStocktakes = [];
    this.filters = $event;
    this.stocktakeTable.resetPagination();
  }

  async getStocktakes(page?, perpage?) {
    try {
      this.selectMode = false;
      this.checkAll = false;
      this.stocktakeTable.toggleNextDisabled(false);

      const _stocktakes = await this.stocktakeService.getStocktakes(
        ((page || this.page) - 1) * perpage,
        perpage || this.perpage,
        this.filters
      );
      if (page > 1 && _stocktakes.length == 0) {
        this.stocktakeTable.toggleNextDisabled(true);
        this.stocktakeTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả phiếu kiểm kho.');
        return;
      }
      this.stocktakes = _stocktakes.map(
        s => this.inventoryStore.setupDataStocktake(s)
      );
      if (this.queryParams.code) {
        this.detail(null, this.stocktakes[0]);
      }

      // Remember here in-order-to reload customer_list after updating vouchers
      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list vouchers', e);
    }
  }

  async reloadStocktakes() {
    const { perpage, page } = this;
    const selectedStocktakeIDs = this.selectedStocktakes.map(r => r.id);

    const _stocktakes = await this.stocktakeService.getStocktakes(
      (page - 1) * perpage,
      perpage,
      this.filters
    );

    this.stocktakes = _stocktakes.map(s => {
      if (selectedStocktakeIDs.indexOf(s.id) != -1) {
        const _stocktake = this.selectedStocktakes.find(
          selected => selected.id == s.id
        );
        if (_stocktake) {
          s.p_data = _stocktake.p_data;
        }
      }
      return this.inventoryStore.setupDataStocktake(s);
    });
    this.selectedStocktakes = this.stocktakes.filter(
      s => s.p_data.selected || s.p_data.detailed
    );
  }

  async loadPage({ page, perpage }) {
    this.stocktakeTable.loading = true;
    this.changeDetector.detectChanges();
    this._paramsHandling();
    await this.getStocktakes(page, perpage);
    this.stocktakeTable.loading = false;
    this.changeDetector.detectChanges();
  }

  private async _checkSelectMode() {
    this.selectedStocktakes = this.stocktakes.filter(s =>
      s.p_data.selected || s.p_data.detailed
    );
    this.setupDropdownActions();
    this.selectMode = this.selectedStocktakes.length > 0;
    if (this.queryParams.code && this.selectedStocktakes.length == 0) {
      this.filters = [];
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/inventory`);
      this.stocktakeTable.resetPagination();
    }
  }

  onSliderClosed() {
    this.stocktakes.forEach(s => {
      s.p_data.selected = false;
      s.p_data.detailed = false;
    });
    this.selectedStocktakes = [];
    this.checkAll = false;
    this._checkSelectMode();
  }

  detail(event, stocktake: Stocktake) {
    if (event && event.target.type == 'checkbox') { return; }
    if (this.checkedStocktakes) { return; }
    this.stocktakes.forEach(s => {
      s.p_data.detailed = false;
    });
    this.checkAll = false;
    stocktake = this.inventoryStore.setupDataStocktake(stocktake);
    stocktake.p_data.detailed = true;
    this.selectedStocktakes = [stocktake];
    this._checkSelectMode();
  }

  async cancel() {
    if (this.modal) { return; }
    const stocktake = this.selectedStocktakes[0];
    const title = stocktake.type == 'discard' ? 'phiếu xuất huỷ' : 'phiếu kiểm kho';
    this.modal = this.modalController.create({
      component: CancelObjectComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        title: `${title} #${stocktake.code}`,
        cancelFn: async (reason: string) => {
          try {
            await this.stocktakeApi.cancelStocktake(stocktake.id, reason);
            toastr.success(`Huỷ ${title} thành công.`);
          } catch(e) {
            toastr.error(`Huỷ ${title} không thành công.`, e.code && (e.message || e.msg) || '');
            throw e;
          }
        }
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async() => {
      this.modal = null;
      await this.reloadStocktakes();
      this.setupDropdownActions();
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  createStocktake() {
    window.open(`/s/${this.util.getSlug()}/inventory/balances/create`, '_blank');
  }

  isNew(stocktake: Stocktake) {
    return stocktake.status != 'N' && stocktake.status != 'P';
  }

  private _paramsHandling() {
    const { queryParams } = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    if (this.queryParams.type != 'stocktake') {
      return;
    }
    this.filters = [{
      name: 'code',
      op: FilterOperator.eq,
      value: this.queryParams.code
    }];
  }

  private setupDropdownActions() {
    if (!this.selectedStocktakes.length) {
      this.dropdownController.clearActions();
      return;
    }
    const stocktake = this.selectedStocktakes[0];
    this.dropdownController.setActions([
      {
        onClick: () => this.cancel(),
        title: `Huỷ phiếu ${stocktake.type == 'discard' ? 'xuất huỷ' : 'kiểm kho'}`,
        cssClass: 'text-danger',
        permissions: ['shop/stocktake:cancel'],
        disabled: !this.isNew(stocktake)
      }
    ]);
  }

  get sliderTitle() {
    if (this.showDetailStocktake) {
      if (this.selectedStocktakes[0].type == 'balance') {
        return 'Chi tiết phiếu kiểm kho';
      }
      if (this.selectedStocktakes[0].type == 'discard') {
        return 'Chi tiết phiếu xuất huỷ';
      }
    }
  }

  get emptyResultFilter() {
    return this.page == 1 && this.stocktakes.length == 0 && this.filters.length > 0;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy phiếu kiểm kho phù hợp';
    }
    return 'Cửa hàng của bạn chưa có phiếu kiểm kho';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  get checkedStocktakes() {
    return this.stocktakes.some(c => c.p_data.selected);
  }

  get showPaging() {
    return !this.stocktakeTable.liteMode && !this.stocktakeTable.loading;
  }

  get showDetailStocktake() {
    return this.selectedStocktakes.length == 1 && !this.checkedStocktakes;
  }

}
