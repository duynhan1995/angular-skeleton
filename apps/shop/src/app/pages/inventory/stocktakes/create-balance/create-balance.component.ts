import { Component, OnInit, ViewChild } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { Variant } from 'libs/models/Product';
import { UtilService } from 'apps/core/src/services/util.service';
import { Router } from '@angular/router';
import { BalanceLinesComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/components/balance-lines/balance-lines.component';

@Component({
  selector: 'shop-create-balance',
  templateUrl: './create-balance.component.html',
  styleUrls: ['./create-balance.component.scss']
})
export class CreateBalanceComponent implements OnInit {
  @ViewChild('balanceLines', {static: true}) balanceLines: BalanceLinesComponent;
  search_variant_list: Variant[] = [];

  constructor(
    private router: Router,
    private headerController: HeaderControllerService,
    private util: UtilService,
    private inventoryStore: InventoryStoreService
  ) { }

  ngOnInit() {
    this.inventoryStore.changeInventoryType(null);
    this.headerController.clearActions();
    this.headerController.clearTabs();
    this.inventoryStore.stocktakeRecentlyCreated$.subscribe(recentlyCreated => {
      if (recentlyCreated) {
        this.search_variant_list = [];
      }
    });
  }

  onVariantSearched(variant) {
    const index = this.search_variant_list.findIndex(item => item.id == variant.id);
    if (index == -1) {
      this.search_variant_list.push(variant);
      const idx = this.search_variant_list.length - 1;
      this.search_variant_list[idx].p_data = {
        ...this.search_variant_list[idx].p_data,
        old_quantity: variant.inventory_variant && variant.inventory_variant.quantity || 0,
        new_quantity: null,
        cost_price: variant.cost_price
      };
      if (this.balanceLines) {
        this.balanceLines.lineUpdated();
      }
    }
  }

  gotoStocktakes() {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/inventory`);
    this.inventoryStore.changeInventoryType('stocktake');
  }

}
