import { Component, OnInit, ViewChild } from '@angular/core';
import { Stocktake, Variant } from '@etop/models';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StocktakeApi } from '@etop/api';
import { BalanceLinesComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/components/balance-lines/balance-lines.component';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-update-balance',
  templateUrl: './update-balance.component.html',
  styleUrls: ['./update-balance.component.scss']
})
export class UpdateBalanceComponent extends BaseComponent implements OnInit {
  @ViewChild('balanceLines', {static: true}) balanceLines: BalanceLinesComponent;
  balance = new Stocktake({});
  search_variant_list: Array<Variant> = [];

  constructor(
    private headerController: HeaderControllerService,
    private inventoryStore: InventoryStoreService,
    private stocktakeApi: StocktakeApi,
    private util: UtilService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super();
  }

  get isNew() {
    return ['N', 'P'].indexOf(this.balance.status) == -1;
  }

  ngOnInit() {
    this.inventoryStore.changeInventoryType(null);
    this.headerController.clearActions();
    this.headerController.clearTabs();

    this.inventoryStore.stocktakeRecentlyUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(recentlyUpdated => {
        if (recentlyUpdated) {
          this.search_variant_list = [];
          this.getStocktakeInfo();
        }
      });
    this.getStocktakeInfo();
  }

  async getStocktakeInfo() {
    try {
      const { id } = this.route.snapshot.params;
      this.balance = await this.stocktakeApi.getStocktake(id);
      this.balance = this.inventoryStore.setupDataStocktake(this.balance);

      this.search_variant_list = this.balance.lines.map(l => {
        return {
          ...new Variant({}),
          id: l.variant_id,
          image: l.image_url,
          code: l.code,
          name: l.product_name,
          attributes: l.attributes,
          p_data: {
            old_quantity: l.old_quantity,
            new_quantity: l.new_quantity,
            cost_price: l.cost_price
          }
        }
      })
    } catch(e) {
      debug.error('ERROR in getting Stocktake Info', e);
      toastr.error('Có lỗi xảy ra khi lấy thông tin phiếu kiểm kho');
    }
  }

  onVariantSearched(variant) {
    const index = this.search_variant_list.findIndex(item => item.id == variant.id);
    if (index == -1) {
      this.search_variant_list.push(variant);
      const idx = this.search_variant_list.length - 1;
      this.search_variant_list[idx].p_data = {
        ...this.search_variant_list[idx].p_data,
        old_quantity: variant.inventory_variant && variant.inventory_variant.quantity || 0,
        new_quantity: null,
        cost_price: variant.cost_price
      };
      if (this.balanceLines) {
        this.balanceLines.lineUpdated();
      }
    }
  }

  gotoStocktakes() {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/inventory`);
    this.inventoryStore.changeInventoryType('stocktake');
  }

}
