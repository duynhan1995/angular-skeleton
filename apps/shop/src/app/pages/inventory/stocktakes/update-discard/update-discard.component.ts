import { Component, OnInit, ViewChild } from '@angular/core';
import { Stocktake, Variant } from '@etop/models';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { InventoryStoreService } from 'apps/core/src/stores/inventory.store.service';
import { StocktakeApi } from '@etop/api';
import { UtilService } from 'apps/core/src/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { DiscardLinesComponent } from 'apps/shop/src/app/pages/inventory/stocktakes/components/discard-lines/discard-lines.component';
import { BaseComponent } from '@etop/core';

@Component({
  selector: 'shop-update-discard',
  templateUrl: './update-discard.component.html',
  styleUrls: ['./update-discard.component.scss']
})
export class UpdateDiscardComponent extends BaseComponent implements OnInit {
  @ViewChild('discardLines', {static: true}) discardLines: DiscardLinesComponent;

  discard = new Stocktake({});
  search_variant_list: Array<Variant> = [];

  constructor(
    private headerController: HeaderControllerService,
    private inventoryStore: InventoryStoreService,
    private stocktakeApi: StocktakeApi,
    private util: UtilService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super();
  }

  get isNew() {
    return ['N', 'P'].indexOf(this.discard.status) == -1;
  }

  async ngOnInit() {
    this.inventoryStore.changeInventoryType(null);
    this.headerController.clearActions();
    this.headerController.clearTabs();

    this.inventoryStore.stocktakeRecentlyUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async(recentlyUpdated) => {
        if (recentlyUpdated) {
          this.search_variant_list = [];
          this.getStocktakeInfo();
        }
      });
    this.getStocktakeInfo();
  }

  async getStocktakeInfo() {
    try {
      const { id } = this.route.snapshot.params;
      this.discard = await this.stocktakeApi.getStocktake(id);
      this.discard = this.inventoryStore.setupDataStocktake(this.discard);

      this.search_variant_list = this.discard.lines.map(l => {
        return {
          ...new Variant({}),
          id: l.variant_id,
          image: l.image_url,
          code: l.code,
          name: l.product_name,
          attributes: l.attributes,
          p_data: {
            old_quantity: l.old_quantity,
            discard_quantity: l.discard_quantity,
            cost_price: l.cost_price
          }
        }
      })
    } catch(e) {
      debug.error('ERROR in getting Stocktake Info', e);
      toastr.error('Có lỗi xảy ra khi lấy thông tin phiếu xuất huỷ');
    }
  }

  onVariantSearched(variant) {
    const index = this.search_variant_list.findIndex(item => item.id == variant.id);
    if (index == -1) {
      this.search_variant_list.push(variant);
      const idx = this.search_variant_list.length - 1;
      this.search_variant_list[idx].p_data = {
        ...this.search_variant_list[idx].p_data,
        old_quantity: variant.inventory_variant && variant.inventory_variant.quantity || 0,
        discard_quantity: null,
        new_quantity: null,
        cost_price: variant.cost_price
      };
      if (this.discardLines) {
        this.discardLines.lineUpdated();
      }
    }
  }

  gotoStocktakes() {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/inventory`);
    this.inventoryStore.changeInventoryType('stocktake');
  }

}
