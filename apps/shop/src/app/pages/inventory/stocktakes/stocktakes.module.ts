import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateBalanceComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/create-balance/create-balance.component';
import {UpdateBalanceComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/update-balance/update-balance.component';
import {StocktakeListComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/stocktake-list/stocktake-list.component';
import {StocktakeRowComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/components/stocktake-row/stocktake-row.component';
import {BalanceDetailComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/balance-detail/balance-detail.component';
import {BalanceVariantsComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/components/balance-variants/balance-variants.component';
import {BalanceLinesComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/components/balance-lines/balance-lines.component';
import {BalanceInfoCreateComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/components/balance-info-create/balance-info-create.component';
import {BalanceInfoUpdateComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/components/balance-info-update/balance-info-update.component';
import {StocktakesComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/stocktakes.component';
import {SharedModule} from 'apps/shared/src/shared.module';
import {SearchVariantsModule} from 'apps/shop/src/app/components/search-variants/search-variants.module';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateModule} from '@etop/core';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {StocktakeApi} from '@etop/api';
import {StocktakeService} from 'apps/shop/src/services/stocktake.service';
import {CreateDiscardComponent} from './create-discard/create-discard.component';
import {UpdateDiscardComponent} from './update-discard/update-discard.component';
import {DiscardInfoCreateComponent} from 'apps/shop/src/app/pages/inventory/stocktakes/components/discard-info-create/discard-info-create.component';
import {DiscardInfoUpdateComponent} from './components/discard-info-update/discard-info-update.component';
import {DiscardLinesComponent} from './components/discard-lines/discard-lines.component';
import {DiscardVariantsComponent} from './components/discard-variants/discard-variants.component';
import {DiscardDetailComponent} from './discard-detail/discard-detail.component';
import {CancelObjectModule} from 'apps/shop/src/app/components/cancel-object/cancel-object.module';
import {EmptyPageModule, EtopCommonModule, EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import {SideSliderModule} from '@etop/shared/components/side-slider/side-slider.module';

@NgModule({
  declarations: [
    CreateBalanceComponent,
    UpdateBalanceComponent,
    StocktakeListComponent,
    StocktakeRowComponent,
    BalanceDetailComponent,
    BalanceVariantsComponent,
    BalanceLinesComponent,
    BalanceInfoCreateComponent,
    BalanceInfoUpdateComponent,
    StocktakesComponent,
    CreateDiscardComponent,
    UpdateDiscardComponent,
    DiscardInfoCreateComponent,
    DiscardInfoUpdateComponent,
    DiscardLinesComponent,
    DiscardVariantsComponent,
    DiscardDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SearchVariantsModule,
    FormsModule,
    NgbModule,
    AuthenticateModule,
    DropdownActionsModule,
    CancelObjectModule,
    EtopCommonModule,
    EtopMaterialModule,
    SideSliderModule,
    EtopPipesModule,
    MaterialModule,
    EmptyPageModule
  ],
  exports: [
    StocktakesComponent
  ],
  providers: [
    StocktakeApi,
    StocktakeService
  ]
})
export class StocktakesModule {
}
