import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionService } from '@etop/features';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { DashboardStoreService } from 'apps/core/src/stores/dashboard.store.service';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'shop-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy  {
  popup_holiday_content: any;
  loading = true;
  subscription = false;
  current_menu: string;

  constructor(
    private headerController: HeaderControllerService,
    private dashboardStore: DashboardStoreService,
    private subscriptionService: SubscriptionService,
    private util: UtilService,
    private router: Router,
  ) {
  }

  async ngOnInit() {
    this.subscription = await this.subscriptionService.checkSubcriptions();
    this.setupTabs();
    const tab = this.router.url.split('/')[4];
    switch (tab) {
      case 'd-pos':
        this.dashboardStore.changeHeaderTab('pos');
        break;
      case 'report':
        this.dashboardStore.changeHeaderTab('report');
        break;
      default: 
        this.dashboardStore.changeHeaderTab('topship');
    }
    this.dashboardStore.headerTabChanged$.subscribe(_ => {
      this.setupTabs();
    });
  }

  ngOnChange() {
  }

  checkTab(tabName) {
    return this.dashboardStore.headerTabChanged$.pipe(
      map(tab => {
        return tab == tabName;
      }),
      distinctUntilChanged()
    );
  }

  setupTabs() {
    this.headerController.setTabs(
      [
        {
          title: 'Bán hàng',
          name: 'pos',
          active: false,
          onClick: () => {
            this.dashboardStore.changeHeaderTab('pos');
            this.router.navigateByUrl(`/s/${this.util.getSlug()}/dashboard/d-pos`);
          }
        },
        {
          title: 'Giao hàng',
          name: 'topship',
          active: false,
          onClick: () => {
            this.dashboardStore.changeHeaderTab('topship');
            this.router.navigateByUrl(`/s/${this.util.getSlug()}/dashboard/topship`);
          }
        },
        {
          title: 'Báo cáo',
          name: 'report',
          active: false,
          onClick: () => {
            this.dashboardStore.changeHeaderTab('report');
            this.router.navigateByUrl(`/s/${this.util.getSlug()}/dashboard/report`);
          }
        }
      ].map(tab => {
        this.checkTab(tab.name).subscribe(active => (tab.active = active));
        return tab;
      })
    );
  }

  ngOnDestroy() {
    this.headerController.clearActions();
    this.headerController.clearTabs();
  }
}
