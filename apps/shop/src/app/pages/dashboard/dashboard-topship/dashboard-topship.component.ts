import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { StatisticService } from 'apps/shop/src/services/statistic.service';
import { VND } from 'libs/shared/pipes/etop.pipe';
import { OrderService } from 'apps/shop/src/services/order.service';
import { AuthenticateStore } from '@etop/core';
import { DashboardStoreService } from 'apps/core/src/stores/dashboard.store.service';

@Component({
  selector: 'shop-dashboard-topship',
  templateUrl: './dashboard-topship.component.html',
  styleUrls: ['./dashboard-topship.component.scss']
})
export class DashboardTopshipComponent implements OnInit {
  selected: { startDate: Moment; endDate: Moment };
  alwaysShowCalendars: boolean;

  slug: string | number;
  dateArr = [];
  chartDate: any;
  chartDateData: any = [];
  chartCod: any;
  chartCodData: any = [];
  SummarizeTopshipData: any = [];
  yAxesticks = [];
  highestVal;
  chartVendor: any;
  chartRegion: any;

  orderToday: any;
  orderFilter: any;
  orderArea: any = [];
  orderProvider: any = [];
  orderRegion: any;
  loading = false;
  guideline = false;
  orderByDate: any = [];
  orderbyCod: any = [];
  transactionData: any = [];
  shippingState: any = [];
  totalCodAmount: any = [];
  shippingFeeShop: any = [];


  private transformMoney = new VND();

  constructor(
    private router: Router,
    private util: UtilService,
    private statisticService: StatisticService,
    private orderService: OrderService,
    private auth: AuthenticateStore,
    private dashboardStore: DashboardStoreService,
  ) {
    this.alwaysShowCalendars = true;
  }

  async ngOnInit() {
    this.dashboardStore.changeHeaderTab('topship');
    this.slug = this.util.getSlug();
    this.getArrdays();
    await this.loadStat();
    if (this.SummarizeTopshipData) {
      this.loading = true;
    }
    await this.drawChartVendor();
    await this.drawChartRegion();
    await this.drawChartDate();
    await this.drawChartCod();
  }

  async loadStat() {
    try {
      const res = await this.orderService.checkEmptyOrders(
        this.auth.snapshot.shop.id
      );
      this.guideline = !res;
      await this.SummarizeTopship(
        this.selected.startDate,
        this.selected.endDate
      );
    } catch (e) {
      debug.error('DASHBOARD:LOADSTAT', e);
    }
  }

  gotoTransaction() {
    this.router.navigateByUrl(`s/${this.util.getSlug()}/money-transactions`);
  }

  getArrdays() {
    let start = new Date(Number(this.selected.startDate));
    let end = new Date(Number(this.selected.endDate));
    while (start < end) {
      this.dateArr.push(moment(start).format('DD-MM'));
      let newDate = start.setDate(start.getDate() + 1);
      start = new Date(newDate);
    }
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
    }
    this.dateArr = [];
    await this.SummarizeTopship(this.selected.startDate, this.selected.endDate);
    this.getArrdays();
    await this.drawChartDate();
    await this.drawChartCod();
    await this.drawChartVendor();
    await this.drawChartRegion();
  }

  async SummarizeTopship(date_from, date_to) {
    try {
      date_to = moment(date_to).add(1, 'days');
      this.SummarizeTopshipData = await this.statisticService.summarizeTopship({
        date_from: date_from.format('YYYY-MM-DD'),
        date_to: date_to.format('YYYY-MM-DD')
      });
      this.orderToday = this.SummarizeTopshipData[0].data;
      this.orderFilter = this.SummarizeTopshipData[1].data;
      this.orderArea = this.SummarizeTopshipData[8];
      this.orderProvider = this.SummarizeTopshipData[3].data.map(p => {
        return {
          value: p[0].value,
          percent:
            this.SummarizeTopshipData[3].data[0][0].value == 0
              ? 0
              : Math.round(
                  (p[0].value / this.SummarizeTopshipData[3].data[0][0].value) *
                    100
                )
        };
      });
      this.orderRegion = this.SummarizeTopshipData[4].data.map(p => {
        return {
          value: p[0].value,
          percent:
            this.SummarizeTopshipData[3].data[0][0].value == 0
              ? 0
              : Math.round(
                  (p[0].value / this.SummarizeTopshipData[3].data[0][0].value) *
                    100
                )
        };
      });

      this.orderByDate = this.SummarizeTopshipData[2].data[0].map(p => {
        return {
          total_amount: p.value
        };
      });

      this.totalCodAmount = this.SummarizeTopshipData[5].data[0].map(p => {
        return {
          total_amount: p.value
        };
      });
      this.shippingFeeShop = this.SummarizeTopshipData[5].data[1].map(p => {
        return {
          total_amount: p.value
        };
      });
      let transactionData = this.SummarizeTopshipData[7].data;
      this.transactionData = [
        {
          label: 'Đã đối soát',
          cod: transactionData[0][0].value,
          fee: transactionData[1][0].value,
          summary: transactionData[3][0].value
        },
        {
          label: 'Đã lên phiên đối soát',
          cod: transactionData[0][1].value,
          fee: transactionData[1][1].value,
          summary: transactionData[3][1].value
        },
        {
          label: 'Chưa lên phiên đối soát',
          cod: transactionData[0][2].value,
          fee: transactionData[1][2].value,
          summary: transactionData[3][2].value
        }
      ];

      let shippingState = this.SummarizeTopshipData[6].data;
      this.shippingState = [
        {
          label: 'Thu hộ thành công',
          cod: shippingState[0][0].value,
          fee: shippingState[1][0].value,
          summary: shippingState[3][0].value
        },
        {
          label: 'Trả hàng',
          cod: shippingState[0][1].value,
          fee: shippingState[1][1].value,
          summary: shippingState[3][1].value
        },
        {
          label: 'Tổng cộng',
          cod: shippingState[0][2].value,
          fee: shippingState[1][2].value,
          summary: shippingState[3][2].value
        }
      ];
    } catch (e) {
      debug.error('ERROR', e);
    }
  }

  async drawChartVendor() {
    let config = {
      type: 'pie',
      data: {
        datasets: [
          {
            data: [
              this.orderProvider[1].value,
              this.orderProvider[2].value,
              this.orderProvider[3].value,
              this.orderProvider[4].value
            ],
            backgroundColor: [
              'rgba(51,182,212, 1)',
              'rgba(51,182,212, 0.85)',
              'rgba(51,182,212, 0.7)',
              'rgba(51,182,212, 0.55)'
            ]
          }
        ],
        labels: [
          'Giao hàng nhanh',
          'Giao hàng tiết kiệm',
          'Viettel Post',
          'Ahamove'
        ]
      },
      options: {
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
    const orderChart = document.getElementById('orders_vendor');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    // eslint-disable-next-line no-unused-expressions,@typescript-eslint/no-unused-expressions
    if (this.chartVendor) {
      this.chartVendor.destroy();
    }
    this.chartVendor = new Chart(ctx, config);
  }

  async drawChartRegion() {
    let config = {
      type: 'pie',
      data: {
        datasets: [
          {
            data: [
              this.orderRegion[1].value,
              this.orderRegion[2].value,
              this.orderRegion[3].value
            ],
            backgroundColor: [
              'rgba(51,182,212, 1)',
              'rgba(51,182,212, 0.85)',
              'rgba(51,182,212, 0.7)'
            ]
          }
        ],
        labels: ['Nội tỉnh', 'Nội miền', 'Liên miền']
      },
      options: {
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
    const orderChart = document.getElementById('orders_area');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    // eslint-disable-next-line no-unused-expressions,@typescript-eslint/no-unused-expressions
    if (this.chartRegion) {
      this.chartRegion.destroy();
    }
    this.chartRegion = new Chart(ctx, config);
  }

  async drawChartDate() {
    this.chartDateData = [];
    for (let index = 0; index < this.dateArr.length; ++index) {
      this.chartDateData.push(this.orderByDate[index].total_amount);
    }
    let max_value = Math.max(...this.chartDateData);
    let heightyAxes =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length)) *
      Math.pow(10, max_value.toString().length);

    if (max_value == 0) {
      heightyAxes = 100;
    }

    let config = {
      type: 'bar',
      data: {
        labels: this.dateArr,
        datasets: [
          {
            label: 'Số đơn',
            backgroundColor: '#26B5DA',
            data: this.chartDateData
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        title: {
          display: true,
          text: 'Số lượng đơn theo ngày'
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              return tooltipItem.yLabel + ' đơn';
            }
          }
        },
        scales: {
          xAxes: [
            {
              display: false
            }
          ],
          yAxes: [
            {
              gridLines: {
                display: true
              },
              ticks: {
                beginAtZero: true,
                stepSize: heightyAxes / 5,
                max: heightyAxes
              }
            }
          ]
        }
      }
    };

    const orderChart = document.getElementById('orders_count');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    // eslint-disable-next-line no-unused-expressions,@typescript-eslint/no-unused-expressions
    if (this.chartDate) {
      this.chartDate.destroy();
    }
    this.chartDate = new Chart(ctx, config);
  }

  async drawChartCod() {
    let chartCodData = [];
    let lineCodData = [];
    for (let index = 0; index < this.dateArr.length; ++index) {
      chartCodData.push(this.totalCodAmount[index].total_amount);
      lineCodData.push(this.shippingFeeShop[index].total_amount);
    }

    let max_value = Math.max(...chartCodData);
    let heightyAxes =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      heightyAxes = 1000000;
    }
    let config = {
      type: 'bar',
      data: {
        labels: this.dateArr,
        datasets: [
          {
            label: 'Tiền thu hộ',
            backgroundColor: '#26B5DA',
            data: chartCodData,
            order: 2
          },
          {
            backgroundColor: 'transparent',
            borderColor: 'orange',
            borderWidth: 2,
            pointHitRadius: 2,
            label: 'Phí vận chuyển',
            data: lineCodData,

            // Changes this dataset to become a line
            type: 'line',
            order: 1,
            options: {
              elements: {
                point: {
                  radius: 0
                }
              }
            }
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        title: {
          display: true,
          text: 'Tiền thu hộ'
        },
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [
            {
              display: false
            }
          ],
          yAxes: [
            {
              ticks: {
                callback: (value, index, values) => {
                  return value < 1000000
                    ? value == 0
                      ? 0
                      : this.transformMoney.transform(value / 1000) + 'K'
                    : this.transformMoney.transform(value / 1000000) + ' triệu';
                },
                beginAtZero: true,
                stepSize: heightyAxes / 5,
                max: heightyAxes
              }
            }
          ]
        }
      }
    };

    const orderChart = document.getElementById('chart_cod');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    // eslint-disable-next-line no-unused-expressions, @typescript-eslint/no-unused-expressions
    if (this.chartCod) {
      this.chartCod.destroy();
    }
    this.chartCod = new Chart(ctx, config);
  }

  randomScalingFactor() {
    return Math.floor(Math.random() * 1000000);
  }
}

