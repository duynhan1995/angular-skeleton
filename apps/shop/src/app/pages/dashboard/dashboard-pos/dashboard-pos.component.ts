import { Component, OnInit, AfterViewInit, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { StatisticService } from 'apps/shop/src/services/statistic.service';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { AuthenticateStore } from '@etop/core';
import { VND } from 'libs/shared/pipes/etop.pipe';
import { OrderService } from 'apps/shop/src/services/order.service';
import { SubscriptionService } from '@etop/features';
import { DashboardStoreService } from 'apps/core/src/stores/dashboard.store.service';

@Component({
  selector: 'shop-dashboard-pos',
  templateUrl: './dashboard-pos.component.html',
  styleUrls: ['./dashboard-pos.component.scss']
})
export class DashboardPosComponent
  implements OnInit, AfterViewInit, AfterContentChecked {
  loading = true;
  shop_name = '';
  guideline = false;
  rangeLabelOnInput = '';

  chartData: any;
  selected: { startDate: Moment; endDate: Moment };
  alwaysShowCalendars: boolean;

  bestSeller: any = [];
  subscription = false;
  private transformMoney = new VND();
  constructor(
    private statisticService: StatisticService,
    public sanitizer: DomSanitizer,
    private gaService: GoogleAnalyticsService,
    private auth: AuthenticateStore,
    private cdref: ChangeDetectorRef,
    private orderService: OrderService,
    private subscriptionService: SubscriptionService,
    private dashboardStore: DashboardStoreService,
  ) {}
  SummarizePOSData: any[] = [];
  chartDateData: any = [];
  chartCod: any;
  chartCodData: any = [];
  dateArr = [];
  chartDate: any;
  generalStatistics: any = [];
  generalSummarize: any = [];
  orderToday: any;
  returnOrderToday: any;
  revenueStaff: any = [];
  summarizeRevenue: any = [];
  compareYesterday: any = {
    increase: true,
    value: 0
  };
  compareMonth: any = {
    increase: true,
    value: 0
  };

  async ngOnInit() {
    this.dashboardStore.changeHeaderTab('pos');
    this.loading = false;
    this.subscription = await this.subscriptionService.checkSubcriptions();
    if (this.auth.snapshot.shop) {
      this.gaService.sendCurrentShop(this.auth.snapshot.shop);
    } else {
      this.auth.authenticatedData$.subscribe(info => {
        this.gaService.sendCurrentShop(info.shop);
      });
    }
    await this.loadStat();
    this.getArrdays();
    await this.drawChartDate();
    this.loading = true;
  }

  async loadStat() {
    try {
      const res = await this.orderService.checkEmptyOrders(this.auth.snapshot.shop.id);
      this.guideline = !res;
      await this.SummarizePOS(this.selected.startDate, this.selected.endDate);
    } catch (e) {
      debug.error("DASHBOARD:LOADSTAT", e);
    }
  }

  async SummarizePOS(date_from, date_to) {
    try {
      date_to = moment(date_to).add(1, 'days');
      this.SummarizePOSData = await this.statisticService.summarizePOS({
        date_from: date_from.format('YYYY-MM-DD'),
        date_to: date_to.format('YYYY-MM-DD')
      });
      let bestSeller = this.SummarizePOSData[3];
      this.bestSeller = bestSeller.data.map(p => {
        return {
          image_urls: p[2].image_urls[0],
          name: p[3].label,
          quantity: p[4].value
        };
      });

      this.revenueStaff = this.SummarizePOSData[4].data.map(p => {
        return {
          name: p[1].label,
          total_count: p[2].value,
          total_amount: p[3].value
        };
      });

      this.summarizeRevenue = this.SummarizePOSData[2].data.map(p => {
        return {
          total_amount: p[0].value
        };
      });
      this.generalStatistics = this.SummarizePOSData[0].data;
      this.orderToday = this.generalStatistics[1];
      this.returnOrderToday = this.generalStatistics[0];
      if (this.generalStatistics[2][1].value == 0) {
        if (this.orderToday[1].value > 0) {
          this.compareYesterday.value = 100;
        } else {
          this.compareYesterday.value = 0
        }
      } else {
        this.compareYesterday.value = Math.round(100 * (this.orderToday[1].value - this.generalStatistics[2][1].value) / this.generalStatistics[2][1].value);
      }
      if (this.compareYesterday.value >= 0) {
        this.compareYesterday.increase = true;
      } else {
        this.compareYesterday.increase = false;
      }


      if (this.generalStatistics[3][1].value == 0) {
        if (this.orderToday[1].value > 0) {
          this.compareMonth.value = 100;
        } else {
          this.compareMonth.value = 0;
        }
      } else {
        this.compareMonth.value = Math.round(100 * (this.orderToday[1].value - this.generalStatistics[3][1].value) / this.generalStatistics[3][1].value);
      }
      if (this.compareMonth.value >= 0) {
        this.compareMonth.increase = true;
      } else {
        this.compareMonth.increase = false;
      }

      let generalSummarize = this.SummarizePOSData[1].data;
      this.generalSummarize = {
        order: generalSummarize[0],
        return_order: generalSummarize[1]
      };
    } catch (e) {}
  }

  async onChangeFilter(event) {
    if (event.startDate) {
      this.selected = event;
    }
    await this.SummarizePOS(this.selected.startDate, this.selected.endDate);
    this.dateArr = [];
    this.getArrdays();
    await this.drawChartDate();
  }

  randomScalingFactor() {
    return Math.floor(Math.random() * 100);
  }

  getArrdays() {
    let start = new Date(Number(this.selected.startDate));
    let end = new Date(Number(this.selected.endDate));
    while (start < end) {
      this.dateArr.push(moment(start).format('DD-MM'));
      let newDate = start.setDate(start.getDate() + 1);
      start = new Date(newDate);
    }
  }

  async drawChartDate() {
    this.chartDateData = [];
    for (let index = 0; index < this.dateArr.length; ++index) {
      this.chartDateData.push(this.summarizeRevenue[index].total_amount);
    }

    let max_value = Math.max(...this.chartDateData);
    let heightyAxes = Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) * Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      heightyAxes = 1000000;
    }
    let config = {
      type: 'bar',
      data: {
        labels: this.dateArr,
        datasets: [
          {
            label: 'Doanh thu',
            backgroundColor: '#26B5DA',
            data: this.chartDateData
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        title: {
          display: true,
          text: 'Doanh thu bán hàng'
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              return (
                tooltipItem.yLabel
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, '.') + 'đ'
              );
            }
          }
        },
        scales: {
          xAxes: [
            {
              display: false
            }
          ],
          yAxes: [
            {
              ticks: {
                callback: (value, index, values) => {
                  return value < 1000000
                    ? value == 0
                      ? 0
                      : this.transformMoney.transform(value / 1000) + 'K'
                    : this.transformMoney.transform(value / 1000000) +
                        ' triệu';
                },
                beginAtZero: true,
                stepSize: heightyAxes / 5,
                max: heightyAxes
              }
            }
          ]
        }
      }
    };
    const orderChart = document.getElementById('orders_revenue');
    if (!orderChart) {
      return;
    }
    let ctx = (orderChart as any).getContext('2d');
    // eslint-disable-next-line no-unused-expressions, @typescript-eslint/no-unused-expressions
    if (this.chartDate) {
      this.chartDate.destroy();
    }
    this.chartDate = new Chart(ctx, config);
  }

  ngAfterViewInit() {}

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }
}
