import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { DashboardPosComponent } from './dashboard-pos/dashboard-pos.component';
import { DashboardTopshipComponent } from './dashboard-topship/dashboard-topship.component';
import { AuthenticateStore } from '@etop/core';

@Injectable()
export class DashboardControllerService {
  private dashboardPosComponent: DashboardPosComponent;
  private dashboardTopshipComponent: DashboardTopshipComponent;

  onChangeTab$ = new Subject();

  constructor(
    private auth: AuthenticateStore,
    private customerService: CustomerService
  ) {}

}
