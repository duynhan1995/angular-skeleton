import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from 'apps/shop/src/app/pages/dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { StatisticTableComponent } from 'apps/shop/src/app/pages/dashboard/components/statistic-table/statistic-table.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardPosComponent } from './dashboard-pos/dashboard-pos.component';
import { DashboardTopshipComponent } from './dashboard-topship/dashboard-topship.component';
import { DashboardControllerService } from './dashboard-controller.service';
import { EtopCommonModule, EtopMaterialModule, EtopPipesModule, MaterialModule } from '@etop/shared';
import { NotSubscriptionModule } from '@etop/shared/components/not-subscription/not-subscription.module';
import { SubscriptionWarningModule } from '../../components/subscription-warning/subscription-warning.module';
import { ReportsComponent } from '../reports/reports.component';
import { AuthenticateModule } from '@etop/core';
import { ReportService } from 'apps/shop/src/services/report.service';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'topship',
        component: DashboardTopshipComponent
      },
      {
        path: 'report',
        component: ReportsComponent
      },
      {
        path: 'd-pos',
        component: DashboardPosComponent
      },
      {
        path: '**', redirectTo: 'topship'
      }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    NotSubscriptionModule,
    SubscriptionWarningModule,
    RouterModule.forChild(routes),
    AuthenticateModule,
    EtopCommonModule,
    EtopMaterialModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  exports: [],
  declarations: [
    DashboardComponent,
    StatisticTableComponent,
    DashboardPosComponent,
    DashboardTopshipComponent,
    ReportsComponent
  ],
  providers: [
    DashboardControllerService,
    ReportService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class DashboardModule {}
