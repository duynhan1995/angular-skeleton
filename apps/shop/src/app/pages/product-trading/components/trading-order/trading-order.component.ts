import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ETopTradingService } from 'apps/shop/src/services/etop-trading.service';
import { OrderTrading } from 'libs/models/OrderTrading';
import { OrderAddress } from 'libs/models/Address';
import { TelegramService } from '@etop/features';
import { OrderCustomer } from '@etop/models';

@Component({
  selector: 'shop-trading-order',
  templateUrl: './trading-order.component.html',
  styleUrls: ['./trading-order.component.scss']
})
export class TradingOrderComponent implements OnInit {
  newOrder = new OrderTrading({
    customer_address: new OrderAddress({}),
    billing_address: new OrderAddress({}),
    shipping_address: new OrderAddress({}),
    customer: new OrderCustomer({}),
    lines: [],
    fee_lines: [],
    total_items: 1,
    source: 'etop_pos'
  });
  orderForm = true;
  orderSuccessForm = false;
  product: any;
  variantSelected: any;
  order_code: any;

  formOrder = false;
  formCheckout = false;
  formSuccess = false;
  totalCashback = 0;
  checkPayment = false;
  loadingOrder = false;
  meta_fields: any;
  orderSuccessful: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private eTopTradingService: ETopTradingService,
    private telegram: TelegramService
  ) {}

  async ngOnInit() {
    let params = this.activatedRoute.snapshot.params;
    let returnData = this.activatedRoute.snapshot.queryParams;
    let id = params['id'];
    this.product = await this.eTopTradingService.getTradingProduct(id);
    if (returnData && returnData.order_id) {
      await this.PaymentCheckReturnData(returnData);
    } else {
      this.formOrder = true;
    }
  }

  async orderFormChange(e) {
    this.formOrder = false;
    this.formCheckout = true;
  }

  backOrderForm() {
    this.formOrder = true;
    this.formCheckout = false;
  }

  async orderSuccess(e) {
    try {
      this.loadingOrder = true;
      this.totalCashback = e;
      if (
        !this.newOrder.customer_address.ward_code ||
        !this.newOrder.customer_address.full_name ||
        !this.newOrder.customer_address.phone ||
        !this.newOrder.customer_address.address1
      ) {
        toastr.error('Vui lòng nhập đầy đủ thông tin!');
        return;
      }
      if(this.newOrder.lines[0].meta_fields) {
        this.meta_fields = [...this.newOrder.lines[0].meta_fields];
      }
      this.newOrder.total_items = e.total_items;
      this.newOrder.order_note = e.order_note;
      this.newOrder.lines[0].quantity = Number(this.newOrder.total_items);
      this.newOrder.basket_value =
        this.newOrder.lines[0].retail_price * this.newOrder.lines[0].quantity;
      this.newOrder.total_discount = 0;
      this.newOrder.fee_lines = e.fee_lines || [];
      let total_fee = e.fee_lines.length > 0 ? e.fee_lines[0].amount : 0;
      this.newOrder.total_amount = this.newOrder.basket_value + total_fee - this.newOrder.total_discount;

      this.newOrder.order_discount = 0;
      if (this.newOrder.lines[0].meta_fields) {
        for (const meta of this.newOrder.lines[0].meta_fields) {
          if (!meta._value) {
            toastr.error('Vui lòng nhập đầy đủ thông tin mua hàng! ');
            this.loadingOrder = false;
            return;
          }
        }
        this.newOrder.lines[0].meta_fields = this.newOrder.lines[0].meta_fields.map(
          meta => {
            return {
              key: meta.key,
              value: meta._value
            };
          }
        );
      }
      let res = await this.eTopTradingService.createTradingOrder(this.newOrder);

      if (this.newOrder.lines[0].meta_fields) {
        this.newOrder.lines[0].meta_fields = this.meta_fields;
      }

      await this.telegram.newOrderTrading(res, {
        totalCashback: this.totalCashback,
        promotion: localStorage.getItem('promotion'),
        sellCashback: localStorage.getItem('sellCashback'),
        affCode: this.newOrder.referral_meta
          ? this.newOrder.referral_meta.referral_code
          : null
      });
      localStorage.removeItem('promotion');
      localStorage.removeItem('sellCashback');
      this.order_code = res.code;
      if (res.payment_method == 'vtpay') {
        localStorage.setItem('orderCode', this.order_code.toString());
        localStorage.setItem('totalCashback', this.totalCashback.toString());
        await this.eTopTradingService.TradingPaymentOrder(res);
      } else {
        this.orderSuccessful = res;
        toastr.success('Tạo đơn hàng thành công!');
        this.formOrder = false;
        this.formCheckout = false;
        this.formSuccess = true;
      }
      this.loadingOrder = false;
    } catch (e) {
      if (this.newOrder.lines[0].meta_fields) {
        this.newOrder.lines[0].meta_fields = this.meta_fields;
      }
      this.loadingOrder = false;
      toastr.error(e.message);
      debug.error('Error create TradingOrder', e);
    }
  }

  async PaymentCheckReturnData(data) {
    window.history.pushState('', '', window.location.pathname);
    try {
      let body = {
        id: data.order_id,
        code: data.error_code,
        payment_status: data.payment_status,
        amount: data.trans_amount,
        external_transaction_id: data.vt_transaction_id,
        payment_provider: 'vtpay'
      };
      let res = await this.eTopTradingService.PaymentCheckReturnData(body);
      if (res.code == 'ok') {
        toastr.success('Thanh toán đơn hàng thành công!');
        this.checkPayment = false;
      }
      this.formOrder = false;
      this.formCheckout = false;
      this.formSuccess = true;
      let order = await this.eTopTradingService.getTradingOrder(data.order_id.replace('order_', ''));
      await this.telegram.newTradingPayment(order);
      this.orderSuccessful = order;
    } catch (e) {
      toastr.error('Thanh toán đơn hàng thất bại!');
      this.formOrder = false;
      this.formCheckout = false;
      this.formSuccess = true;
      this.checkPayment = true;
      localStorage.setItem('totalCashback', '0');
    }
  }
}
