import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ETopTradingService } from 'apps/shop/src/services/etop-trading.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'shop-order-successfully',
  templateUrl: './order-successfully.component.html',
  styleUrls: ['./order-successfully.component.scss']
})
export class OrderSuccessfullyComponent implements OnInit {
  @Input() orderSuccessful;
  @Input() totalCashback;
  @Input() checkPayment;
  loadData = false;

  constructor(
    private router: Router,
    private eTopTradingService: ETopTradingService,
    private util: UtilService
  ) {}

  ngOnInit() {
    debug.log('orderSuccessful', this.orderSuccessful);
    if (!this.orderSuccessful) {
      this.totalCashback = localStorage.getItem('totalCashback');
    }
    this.loadData = true;
  }

  gotoShopping() {
    this.router.navigateByUrl(
      `/s/${this.util.getSlug()}/etop-trading`
    );
  }

  gotoSetting() {
    this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings/shop`);
  }

  async retryPayment() {
    await this.eTopTradingService.TradingPaymentOrder(this.orderSuccessful);
  }
}
