import { Component, OnInit, Input } from '@angular/core';
import { Route, Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ETopTradingService } from 'apps/shop/src/services/etop-trading.service';

@Component({
  selector: 'shop-product-trading-card',
  templateUrl: './product-trading-card.component.html',
  styleUrls: ['./product-trading-card.component.scss']
})
export class ProductTradingCardComponent implements OnInit {
  @Input() product;
  promotion: any;

  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private util: UtilService,
    private eTopTradingService: ETopTradingService
  ) {}

  async ngOnInit() {
  }

  gotoDetail() {
    let slug = this.util.getSlug();
    this.router.navigateByUrl(`/s/${slug}/etop-trading/${this.product.product.id}`);
  }
}
