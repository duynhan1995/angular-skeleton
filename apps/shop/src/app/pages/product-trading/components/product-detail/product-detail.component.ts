import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { DomSanitizer } from '@angular/platform-browser';
import { StorageService } from 'apps/core/src/services/storage.service';

@Component({
  selector: 'shop-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  @Input() description;
  @Input() product_id;
  desc: any = [];
  tabActive: any;
  loading = true;
  min_quantity;
  constructor(
    private util: UtilService,
    private sanitizer: DomSanitizer,
    private ref: ChangeDetectorRef,
    private storage: StorageService
  ) {}

  ngOnInit() {
    this.description = this.stripHTML(this.description);
    let text = this.description.split('&&&&');
    if (text && text.length > 1) {
      text[1].split('###').map(d => {
        if (d.split('##')[0] != 'min_quantity') {
          this.desc.push({
            title: d.split('##')[0],
            content: this.sanitizer.bypassSecurityTrustResourceUrl(
              d.split('##')[1]
            ),
            id: this.util.createHandle(d.split('##')[0])
          });
        } else {
          this.min_quantity = Number(d.split('##')[1]);
          this.storage.set(this.product_id, this.min_quantity);
        }
      });
      this.tabActive = this.desc[0].id;
    } else {
      let text1 = this.description.split('###');
      text1.map(d => {
        this.desc.push({
          title: d.split('##')[0],
          content: this.sanitizer.bypassSecurityTrustResourceUrl(
            d.split('##')[1]
          ),
          id: this.util.createHandle(d.split('##')[0])
        });
      });
      this.tabActive = this.desc[0].id;
    }
  }

  switchTab(handle) {
    this.tabActive = handle;
    this.loading = true;
    this.ref.detectChanges();
  }

  stripHTML(html: string) {
    const _tempDIV = document.createElement('div');
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || '';
  }

  loadEvent(e) {
    this.loading = false;
  }
}
