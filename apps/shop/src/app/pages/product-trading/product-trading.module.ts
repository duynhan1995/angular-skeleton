import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ProductTradingComponent } from './product-trading.component';
import { ProductTradingCardComponent } from './components/product-trading-card/product-trading-card.component';
import { TradingOrderComponent } from './components/trading-order/trading-order.component';
import { OrderLayoutComponent } from './components/order-layout/order-layout.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductHeaderComponent } from './components/product-header/product-header.component';
import { FormOrderComponent } from './components/form-order/form-order.component';
import { FormCheckoutComponent } from './components/form-checkout/form-checkout.component';
import { OrderSuccessfullyComponent } from './components/order-successfully/order-successfully.component';
import { FormsModule } from '@angular/forms';
import { PurchaseOrderComponent } from './components/purchase-order/purchase-order.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShopPipesModule } from 'apps/shop/src/app/pipes/shop-pipes.module';
import { EtopMaterialModule, EtopPipesModule, SideSliderModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ProductTradingComponent
      },
      {
        path: ':id',
        component: TradingOrderComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    ProductTradingComponent,
    ProductTradingCardComponent,
    TradingOrderComponent,
    OrderLayoutComponent,
    ProductDetailComponent,
    ProductHeaderComponent,
    FormOrderComponent,
    FormCheckoutComponent,
    OrderSuccessfullyComponent,
    PurchaseOrderComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgbModule,
    RouterModule.forChild(routes),
    ShopPipesModule,
    EtopPipesModule,
    EtopMaterialModule,
    SideSliderModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductTradingModule {}
