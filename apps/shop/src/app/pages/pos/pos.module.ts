import { PosComponent } from './pos.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PosFormComponent } from './components/pos-form/pos-form.component';
import { ProductCardComponent } from '../products/components/product-card/product-card.component';
import { PosCustomerComponent } from './components/pos-form/pos-customer/pos-customer.component';
import { PosOrderInfoComponent } from './components/pos-form/pos-order-info/pos-order-info.component';
import { CommonModule } from '@angular/common';
import { ModalControllerModule } from 'apps/core/src/components/modal-controller/modal-controller.module';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { PosOrderComponent } from './components/pos-order/pos-order.component';
import { HeaderListProductCardComponent } from './components/pos-order/header-list-product-card/header-list-product-card.component';
import { PopupVariantsPosOrderComponent } from './components/pos-order/popup-variants-pos-order/popup-variants-pos-order.component';
import { PosHeaderComponent } from './components/pos-header/pos-header.component';
import { OrderTabOptionsComponent } from './components/pos-header/order-tab-options/order-tab-options.component';
import { PosBodyComponent } from './components/pos-body/pos-body.component';
import { DeliveryModule } from '../../components/delivery/delivery.module';
import { PosControllerService } from './pos-controller.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrderLineModule } from './components/pos-order/order-line/order-line.module';
import { PosCreateCustomerModalComponent } from './components/pos-create-customer-modal/pos-create-customer-modal.component';
import { CreateMultiReceiptModule } from 'apps/shop/src/app/components/create-multi-receipt/create-multi-receipt.module';
import { AuthenticateModule } from '@etop/core';
import { NotPermissionModule } from '../../../../../../libs/shared/components/not-permission/not-permission.module';
import { BillPrintingModule } from 'apps/shop/src/app/components/bill-printing/bill-printing.module';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import {EtopFilterModule, EtopFormsModule, EtopPipesModule, MaterialModule} from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: PosComponent
  }
];

@NgModule({
  declarations: [
    PosComponent,
    PosFormComponent,
    ProductCardComponent,
    PosCustomerComponent,
    PosOrderInfoComponent,
    PosOrderComponent,
    HeaderListProductCardComponent,
    PopupVariantsPosOrderComponent,
    PosHeaderComponent,
    PosBodyComponent,
    OrderTabOptionsComponent,
    PosCreateCustomerModalComponent,
  ],
  entryComponents: [
    PosCreateCustomerModalComponent
  ],
    imports: [
        CommonModule,
        ModalControllerModule,
        SharedModule,
        FormsModule,
        RouterModule.forChild(routes),
        DeliveryModule,
        OrderLineModule,
        NgbModule,
        CreateMultiReceiptModule,
        AuthenticateModule,
        NotPermissionModule,
        BillPrintingModule,
        EtopPipesModule,
        EtopFilterModule,
        EtopFormsModule,
        MaterialModule
    ],
  providers: [
    PosControllerService,
    PosStoreService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PosModule { }
