import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Order } from 'libs/models/Order';
import { OrderService } from 'apps/shop/src/services/order.service';
import { Subject } from 'rxjs';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

const FEE_LINE_MAP = {
  shipping: {
    name: 'Phí ship tính với khách',
    desc: 'Phí ship tính với khách'
  },
  tax: {
    name: 'Thuế',
    desc: 'Thuế'
  },
  other: {
    name: 'Phụ thu khác',
    desc: 'Phụ thu khác'
  }
};

@Component({
  selector: 'shop-pos-order-info',
  templateUrl: './pos-order-info.component.html',
  styleUrls: ['./pos-order-info.component.scss']
})
export class PosOrderInfoComponent extends BaseComponent implements OnInit, OnChanges {
  @Output() updateOrder = new EventEmitter();
  @Output() toggle = new EventEmitter();
  @Input() order: Order;

  selectSurchargeInput: Subject<void> = new Subject<void>();
  selectDiscountInput: Subject<void> = new Subject<void>();

  orderToggle = true;
  discountPercent = false;
  discountDefault = true;
  discount_calc_type = 1;
  /*
   ** 1: default (đ)
   ** 2: percent (%)
   */

  fee_type = 'other';
  fee_amount = 0;

  private receipts: any[] = [];

  constructor(
    private orderService: OrderService,
    private receiptStore: ReceiptStoreService,
    private ffmStore: FulfillmentStore
  ) {
    super();
  }

  get feeLineDisplay() {
    return FEE_LINE_MAP;
  }

  ngOnInit() {
    this.order.p_data.discountPercent = false;
    this.receiptStore.resetData();
    this.receiptStore.receiptsUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(receipts => {
        this.receipts = receipts;
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    const _order = changes.order.currentValue;
    if (!_order.p_data.fee_lines.length) {
      this.fee_type = 'other';
      this.fee_amount = 0;
    }
  }

  orderCollapse() {
    this.orderToggle = !this.orderToggle;
    this.toggle.emit(this.orderToggle);
  }

  onChangeFee() {
    const { fee_type, fee_amount } = this;
    this.order.p_data.fee_lines = [
      {
        name: FEE_LINE_MAP[fee_type].name,
        desc: FEE_LINE_MAP[fee_type].desc,
        type: fee_type,
        amount: fee_amount
      }
    ];
    this.orderInfoUpdate();
  }

  onOrderDiscountChanged() {
    this.orderInfoUpdate();
  }

  orderInfoUpdate() {
    this.updateOrder.emit(this.order);
    this.orderService.onOrderInfoChanged$.next();
  }

  discountCalcTypeChange() {
    switch (Number(this.discount_calc_type)) {
      case 1:
        this.toggleDiscountOption('default');
        break;
      case 2:
        this.toggleDiscountOption('percent');
        break;
      default:
        this.toggleDiscountOption('default');
    }
  }

  toggleDiscountOption(type) {
    this.order.p_data.order_discount = 0;
    if (type == 'percent' && this.discountPercent) {
      return;
    }
    if (type == 'default' && this.discountDefault) {
      return;
    }
    this.discountDefault = !this.discountDefault;
    this.discountPercent = !this.discountPercent;
    this.order.p_data.discountPercent = this.discountPercent;
  }

  get customerPay() {
    return this.order.basket_value + this.fee_amount - this.order.order_discount;
  }

  get toCreateFfm() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  get totalReceipts() {
    return this.receipts.length;
  }

}
