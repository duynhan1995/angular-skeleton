import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import {map, takeUntil} from 'rxjs/operators';
import { Order } from 'libs/models/Order';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { PrintType } from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {ConnectionStore} from "@etop/features";

@Component({
  selector: 'shop-pos-form',
  templateUrl: './pos-form.component.html',
  styleUrls: ['./pos-form.component.scss']
})
export class PosFormComponent extends BaseComponent implements OnInit {
  @Output() updateOrder = new EventEmitter();
  @Output() createOrder = new EventEmitter<boolean>();

  @Input() order: Order;
  loading = false;

  print_types: {
    display: string;
    type: PrintType;
  }[] = [
    {
      display: 'Mẫu hoá đơn',
      type: PrintType.orders,
    },
    {
      display: 'Mẫu giao hàng',
      type: PrintType.fulfillments,
    },
    {
      display: 'Mẫu hoá đơn và giao hàng',
      type: PrintType.o_and_f,
    }
  ];
  print_type: {
    display: string;
    type: PrintType
  } = {
    display: 'Mẫu hoá đơn',
    type: PrintType.orders
  };

  orderToggle = true;
  createAndConfirm = false;

  notVerifiedConnection$ = this.ffmStore.state$.pipe(
    map(s => {
      const connections = this.connectionStore.snapshot.validConnections;
      const _selected = connections.find(conn => conn.id == s?.fulfillment?.connection_id);
      return _selected?.connection_subtype == 'shipnow' && !_selected?.external_verified
        && Number(s.fulfillment.cod_amount);
    })
  );
  selectedConnection$ = this.ffmStore.state$.pipe(
    map(s => {
      const connections = this.connectionStore.snapshot.validConnections;
      return connections.find(conn => conn.id == s?.fulfillment?.connection_id);
    })
  );

  constructor(
    private connectionStore: ConnectionStore,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private posStore: PosStoreService
  ) {
    super();
  }

  get to_create_ffm() {
    return this.ffmStore.snapshot.to_create_ffm;
  }

  get to_print() {
    return this.posStore.snapshot.to_print;
  }

  isDisabledPrintType(type) {
    if (type == PrintType.orders) {
      return false;
    }
    return !this.ffmStore.snapshot.to_create_ffm;
  }

  ngOnInit() {
    this.orderStore.creatingOrder$
      .pipe(takeUntil(this.destroy$))
      .subscribe(loading => this.loading = loading);

    this.ffmStore.toCreateFfmToggled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(to_create_ffm => {
        if (!to_create_ffm) {
          this.print_type = this.print_types[0];
          this.selectPrintType(this.print_type);
        } else if (localStorage.getItem('print_type')) {
          const print_type_local = localStorage.getItem('print_type');
          this.print_type = this.print_types.find(pt => pt.type == print_type_local);
          this.selectPrintType(this.print_type);
        }
      });
  }

  updateNewOrder() {
    this.updateOrder.emit();
  }

  onToggle(e) {
    this.orderToggle = e;
  }

  create(createAndConfirm: boolean) {
    this.createOrder.emit(createAndConfirm);
  }

  createOrderWithFFM() {
    const connections = this.connectionStore.snapshot.validConnections;
    const fulfillment = this.ffmStore.snapshot.fulfillment;
    const _selected = connections.find(conn => conn.id == fulfillment?.connection_id);
    if (_selected?.connection_subtype == 'shipnow') {
      if (!_selected?.external_verified && Number(fulfillment?.cod_amount)) {
        return;
      }

      const {pickup_address, shipping_address} = fulfillment;
      if (!pickup_address?.coordinates?.latitude || !pickup_address?.coordinates?.longitude) {
        return toastr.error('Không xác định được địa chỉ lấy hàng trên bản đồ. Vui lòng kiểm tra lại địa chỉ lấy hàng của bạn!');
      }

      if (!shipping_address?.coordinates?.latitude || !shipping_address?.coordinates?.longitude) {
        return toastr.error('Không xác định được địa chỉ giao hàng trên bản đồ. Vui lòng kiểm tra lại địa chỉ giao hàng của bạn!');
      }
    }

    this.createOrder.emit(true);
  }

  toggleSwitch() {
    this.ffmStore.createFulfillment();
  }

  togglePrint() {
    this.posStore.toPrint();
  }

  selectPrintType(print_type) {
    if (this.isDisabledPrintType(print_type.type)) {
      return;
    }
    this.print_type = print_type;
    this.posStore.changePrintType(print_type.type, this.to_create_ffm);
  }

}
