import { Component, OnInit } from '@angular/core';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';

@Component({
  selector: 'shop-order-tab-options',
  templateUrl: './order-tab-options.component.html',
  styleUrls: ['./order-tab-options.component.scss']
})
export class OrderTabOptionsComponent implements OnInit {
  tabs = [{ title: 'Đơn hàng 1', id: 1, active: true }];
  constructor(private dialogController: DialogControllerService) {}

  ngOnInit() {}

  addNewOrder() {
    let id = this.tabs[this.tabs.length - 1].id + 1;
    let newTab = { title: 'Đơn hàng ' + id, id: id, active: true };
    this.tabs.map(tab => (tab.active = false));
    this.tabs.push(newTab);
  }

  onActive(id) {
    this.tabs.map(tab => (tab.active = false));
    this.tabs.find(tab => tab.id == id).active = true;
  }

  removeTab(id) {
    if (this.tabs.length == 1) {
      return;
    }
    this.dialogController
      .createConfirmDialog({
        title: 'Xóa Đơn hàng',
        body: 'Bạn có chắc chắn muốn đơn hàng này',
        onConfirm: () => {
          let idx = this.tabs.map(x => x.id).indexOf(id);
          if (this.tabs[idx].active == true && idx == this.tabs.length - 1) {
            this.tabs[idx - 1].active = true;
          } else if (this.tabs[idx].active == true && idx < this.tabs.length - 1) {
            this.tabs[idx + 1].active = true;
          }
          this.tabs.splice(idx, 1);
        }
      })
      .show()
      .then();
  }
}
