import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter, HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { OrderLine } from 'libs/models/Order';
import { UtilService } from 'apps/core/src/services/util.service';
import { Variant } from 'libs/models/Product';

@Component({
  selector: 'shop-order-line-editable',
  templateUrl: './order-line-editable.component.html',
  styleUrls: ['./order-line-editable.component.scss']
})
export class OrderLineEditableComponent implements OnInit {
  @ViewChild('inputName', { static: false }) inputName: ElementRef;
  @ViewChild('inputPrice', { static: false }) inputPrice: ElementRef;

  @Output() variantSelected = new EventEmitter();
  @Output() focusOrderLine = new EventEmitter();
  @Output() updateOrderLine = new EventEmitter();
  @Output() add = new EventEmitter();
  @Output() minus = new EventEmitter();
  @Output() changeQuantity: EventEmitter<number> = new EventEmitter<number>();

  @Input() idx_order: number;
  @Input() order_line = new OrderLine();
  @Input() variant_list: Array<Variant> = [];
  photo_uploading = false;
  money_format = false;
  show_variant = false;
  display_note = false;
  search_variant_list: Array<Variant> = [];

  constructor(
    private util: UtilService,
    private changeDetector: ChangeDetectorRef
  ) { }

  @HostListener('document:click', ['$event']) clickedOutside(event) {
    const class_name = event.target.className;
    if (class_name.indexOf('variant-select-item') != -1 || class_name.indexOf('text-input') != -1) {
      this.show_variant = true;
      return;
    }
    this.search_variant_list = [];
    this.show_variant = false;
  }

  ngOnInit() {
  }

  async onChangeLogo(event) {
    this.photo_uploading = true;
    const { files } = event.target;
    const res = await this.util.uploadImages([files[0]], 1024);
    this.order_line.image_url = res[0].url;
    this.photo_uploading = false;
  }

  searchVariants() {
    setTimeout(() => {
      this.search_variant_list = this.order_line.product_name ?
        this.variant_list.filter(v => {
          const _search_name = this.util.makeSearchText(this.order_line.product_name);
          return v.p_data.search_product_name.indexOf(_search_name) != -1;
        }) :
        this.variant_list;
    }, 200);
    return this.search_variant_list;
  }

  selectVariant(variant, index) {
    this.variantSelected.emit({ variant, index });
  }

  focusPrice() {
    this.money_format = false;
    this.show_variant = false;
    this.changeDetector.detectChanges();
    if (this.inputPrice) {
      setTimeout(() => this.inputPrice.nativeElement.select());
    }
  }

  onFocusOrderLine(order_line) {
    this.search_variant_list = this.searchVariants();
    this.focusOrderLine.emit(order_line);
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  updatePrice() {
    this.money_format = !!this.order_line.retail_price || this.order_line.retail_price == 0;
    this.order_line.payment_price = this.order_line.retail_price;
    this.order_line.list_price = this.order_line.retail_price;
    this.updateOrderLine.emit();
  }

  openNote() {
    this.display_note = true;
  }

  toggleSaveProduct() {
    this.order_line.p_data.to_save = !this.order_line.p_data.to_save;
  }

}
