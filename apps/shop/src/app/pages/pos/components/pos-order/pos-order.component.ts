import {
  Component, ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { PopupVariantsPosOrderComponent } from './popup-variants-pos-order/popup-variants-pos-order.component';
import { OrderService } from 'apps/shop/src/services/order.service';
import { Product, Variant } from 'libs/models/Product';
import { PosControllerService } from '../../pos-controller.service';
import { OrderLine } from 'libs/models/Order';
import { BaseComponent } from '@etop/core';
import { ProductService } from 'apps/shop/src/services/product.service';
import { ProductStore } from 'apps/core/src/stores/product.store';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-pos-order',
  templateUrl: './pos-order.component.html',
  styleUrls: ['./pos-order.component.scss']
})
export class PosOrderComponent extends BaseComponent implements OnInit, OnChanges {
  @ViewChild('listOrderLine', {static: false}) listOrderLine: ElementRef;
  @ViewChild('addProductButton', {static: false}) addProductButton: ElementRef;
  @ViewChild('variantPopup', { static: false }) variantPopup: PopupVariantsPosOrderComponent;
  @Input() order_lines: Array<OrderLine> = [];
  @Output() orderLinesChanged = new EventEmitter();

  page_item;
  page_numb = 0;
  page_total = 0;
  perpage = 15;
  keyword = '';
  search_item = [];
  active_line: string;
  loading = false;

  productList: Array<Product> = [];
  variantList: Array<Variant> = [];
  productSelected: any = [];

  constructor(
    private util: UtilService,
    private orderService: OrderService,
    private productService: ProductService,
    private posController: PosControllerService,
    private productStore: ProductStore
  ) {
    super();
  }

  async ngOnInit() {
    this.page_numb = 1;
    this.loadProductData();
    this.productStore.productListUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.loadProductData();
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.order_lines.currentValue.length) {
      this.pushSampleProduct();
    }
  }

  async loadProductData() {
    this.loading = true;
    try {
      const products = this.productStore.snapshot.productList;
      if (!products || !products.length) {
        const res = await this.productService.getProducts();
        this.productList = res.products;
      } else {
        this.productList = products;
      }
      this.variantList = [];

      this.productList.forEach(prod => {
        this.variantList = this.variantList.concat(prod.variants);
      });
      this._paging();
    } catch (e) {
      debug.error('error -->', e);
    }
    this.loading = false;
  }

  openPopupVariant(product) {
    if (product.variants.length > 1) {
      this.productSelected = product;
      this.variantPopup.show(this.productSelected);
      $('.fade.show').removeClass('modal-backdrop');
    }

    if (product.variants.length == 1) {
      if (this.isInitProduct) {
        this.order_lines.splice(0, 1);
      }
      this.pushdata(product.variants[0], product);
    }

    if(product.variants.length == 0) {
      return toastr.error(`Sản phẩm ${product?.name} bị lỗi`)
    }
  }

  selectVariant(data) {
    const { variant, index } = data;
    const product = this.productList.find(prod => prod.id == variant?.product?.id);
    if (this.isInitProduct) {
      this.order_lines.splice(0, 1);
    }
    this.pushdata(variant, product, index);
  }

  pushdata(variant, product, index?: number) {
    const idx = this.order_lines.findIndex(
      line => line.p_data.code == variant.p_data.code);
    if (idx >= 0) {
      this.add(idx);
      this.listOrderLine.nativeElement.scrollTo({ left: 0, top: idx * 75, behavior: 'smooth' });
    } else {
      if (!variant.image_urls || !variant.image_urls.length) {
        if (product.image_urls && product.image_urls.length) {
          variant.image_urls = product.image_urls;
        } else {
          variant.image_urls = ['assets/images/placeholder_medium.png'];
        }
      }
      variant.product_name = product.name;
      variant.quantity = 1;
      variant = this.posController.orderLineMap(variant);

      if (index) {
        this.order_lines[index] = variant;
      } else {
        this.order_lines.push(variant);
      }
      if (this.listOrderLine && this.addProductButton) {
        const bottomElement = this.addProductButton.nativeElement;
        setTimeout(() => {
          this.listOrderLine.nativeElement.scrollTo(
            { left: 0, top: bottomElement.offsetTop, behavior: 'smooth' }
          );
        });
      }
    }
    this.update(variant);
  }

  updateOrderLine(e) {
    if (this.isInitProduct) {
      this.order_lines.splice(0, 1);
    }
    this.pushdata(e.variant, e.productSelected);
  }

  add(index) {
    let line = this.order_lines[index];
    line.quantity = line.quantity + 1;
    this.update(line);
  }

  del(order_line) {
    const index = this.order_lines.findIndex(
      line => line.p_data.code == order_line.p_data.code);
    this.order_lines.splice(index, 1);
    if (!this.order_lines.length) {
      this.pushSampleProduct();
    }
    this.update();
  }

  update(order_line?) {
    this.active_line = order_line ? order_line.p_data.code : null;
    this.orderLinesChanged.emit(this.order_lines);
    this.orderService.onOrderLinesChanged$.next();
  }

  search(e, opt?: { isResetCurrentPage: boolean }) {
    this.keyword = e;
    this.search_item = this.productList.filter(x =>
      this.util.searchCompare(x.name, this.keyword)
    );
    this._paging(opt);
  }

  searchSKU(sku: string) {
    const variant = this.variantList.find(v => v.code == sku);
    if (!variant) { return; }
    const product = this.productList.find(p => p.id == variant?.product?.id);
    if (this.isInitProduct) {
      this.order_lines.splice(0, 1);
    }
    this.pushdata(variant, product);
  }

  pushSampleProduct() {
    let new_product = new Product({});
    let new_variant = new Variant({});
    new_variant.p_data = { code: Date.now().toString() };
    this.pushdata(new_variant, new_product);
  }

  onFocusOrderLine(order_line) {
    this.active_line = order_line ? order_line.p_data.code : null;
  }

  _paging(opt?: { isResetCurrentPage: boolean }) {
    const defaultOpt = {
      isResetCurrentPage: false
    };
    opt = Object.assign({}, defaultOpt, opt);

    let { isResetCurrentPage } = opt;
    if (isResetCurrentPage) {
      this.page_numb = 1;
    }

    this.page_total = Math.ceil(this.productList.length / this.perpage);

    if (this.search_item.length) {
      this.page_item = this.search_item.slice(
        (this.page_numb - 1) * this.perpage,
        this.page_numb * this.perpage
      );
      this.page_total = Math.ceil(this.search_item.length / this.perpage);
    } else {
      this.page_item = this.productList.slice(
        (this.page_numb - 1) * this.perpage,
        this.page_numb * this.perpage
      );
    }
  }

  paging(e) {
    this.page_numb = e;
    this._paging();
  }

  get isInitProduct() {
    return this.order_lines[0].p_data.editable
      && !this.order_lines[0].product_name
      && !this.order_lines[0].retail_price
      && this.order_lines.length == 1;
  }

}
