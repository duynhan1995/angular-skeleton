import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OrderLine } from 'libs/models/Order';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-order-line-uneditable',
  templateUrl: './order-line-uneditable.component.html',
  styleUrls: ['./order-line-uneditable.component.scss']
})
export class OrderLineUneditableComponent implements OnInit {
  @Output() add = new EventEmitter();
  @Output() minus = new EventEmitter();
  @Output() changeQuantity: EventEmitter<number> = new EventEmitter<number>();

  @Input() order_line = new OrderLine();

  display_note = false;

  inventory_variant_quantity : Number = 0;

  constructor(private util: UtilService) { }

  ngOnInit() {
    this.inventory_variant_quantity = this.order_line.inventory_variant.quantity || 0;
  }

  numberOnly(keypress_event) {
    return this.util.numberOnly(keypress_event);
  }

  openNote() {
    this.display_note = true;
  }

}
