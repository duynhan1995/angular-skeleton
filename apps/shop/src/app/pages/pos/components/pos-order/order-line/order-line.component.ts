import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  ChangeDetectorRef, HostListener
} from '@angular/core';
import { OrderLine } from 'libs/models/Order';
import { UtilService } from '../../../../../../../../core/src/services/util.service';
import { Variant } from '../../../../../../../../../libs/models/Product';
import { PosControllerService } from '../../../pos-controller.service';

@Component({
  selector: 'shop-order-line',
  templateUrl: './order-line.component.html',
  styleUrls: ['./order-line.component.scss']
})
export class OrderLineComponent implements OnInit {

  @ViewChild('inputName', { static: false }) inputName: ElementRef;
  @ViewChild('inputPrice', { static: false }) inputPrice: ElementRef;

  @Output() update = new EventEmitter();
  @Output() delLine = new EventEmitter();
  @Output() focusOrderLine = new EventEmitter();
  @Output() variantSelected = new EventEmitter();

  @Input() order_line = new OrderLine();
  @Input() variant_list: Array<Variant> = [];
  @Input() idx_order = 0;
  @Input() active_line = '';
  @Input() removable = false;

  constructor() { }

  ngOnInit() {}

  selectVariant(data) {
    this.variantSelected.emit(data);
  }

  changeQuantity(quantity) {
    this.order_line.quantity = Number(quantity);
    this.updateOrderLine();
  }

  minus() {
    if (this.order_line.quantity > 1) {
      this.order_line.quantity--;
      this.updateOrderLine();
    }
  }

  add() {
    this.order_line.quantity++;
    this.updateOrderLine();
  }

  delete() {
    this.delLine.emit(this.order_line);
  }

  updateOrderLine() {
    this.update.emit(this.order_line);
  }

  onFocusOrderLine(order_line) {
    this.focusOrderLine.emit(order_line);
  }

}
