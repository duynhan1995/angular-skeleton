import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Product } from 'libs/models/Product';

@Component({
  selector: 'shop-popup-variants-pos-order',
  templateUrl: './popup-variants-pos-order.component.html',
  styleUrls: ['./popup-variants-pos-order.component.scss']
})
export class PopupVariantsPosOrderComponent implements OnInit {
  @Input() productSelected: Product;
  @ViewChild('variantPopup', { static: false }) variantPopup: ElementRef;
  @Output() updateOrderline = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  show(productSelected) {
    this.productSelected = productSelected;
    this.productSelected.variants.map(variant => {
      if (!variant.image_urls[0]) {
        if (productSelected.image_urls[0]) {
          variant.image_urls = productSelected.image_urls;
        } else {
          variant.image_urls = ['assets/images/placeholder_medium.png'];
        }
      }
    });
    $(this.variantPopup.nativeElement).modal('show');
  }

  pushdata(variant, productSelected) {
    this.updateOrderline.emit({
      variant: {...variant},
      productSelected: {...productSelected}
    });
    $(this.variantPopup.nativeElement).modal('hide');
  }
}
