import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import {Customer, CustomerAddress} from 'libs/models/Customer';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import {UtilService} from "apps/core/src/services/util.service";
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'shop-pos-create-customer-modal',
  templateUrl: './pos-create-customer-modal.component.html',
  styleUrls: ['./pos-create-customer-modal.component.scss']
})
export class PosCreateCustomerModalComponent implements OnInit {
  loading = false;

  customer: Customer;

  customerAddressForm = this.fb.group({
    full_name: '',
    phone: '',
    province_code: '',
    district_code: '',
    ward_code: '',
    address1: ''
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.customerAddressForm.controls['province_code'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.customerAddressForm.controls['district_code'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  private static validateCustomerAddress(customerAddress: CustomerAddress): boolean {
    const {province_code, district_code, ward_code, address1, full_name, phone} = customerAddress;

    if (!full_name) {
      toastr.error('Chưa nhập tên khách hàng!');
      return false;
    }
    if (!phone) {
      toastr.error('Chưa nhập số điện thoại khách hàng!');
      return false;
    }
    if ((province_code + district_code + ward_code + address1).length > 0) {
      if (!province_code) {
        toastr.error('Chưa chọn tỉnh thành!');
        return false;
      }
      if (!district_code) {
        toastr.error('Chưa chọn quận huyện!');
        return false;
      }
      if (!ward_code) {
        toastr.error('Chưa chọn phường xã!');
        return false;
      }
      if (!address1) {
        toastr.error('Chưa nhập địa chỉ cụ thể!');
        return false;
      }
    }

    return true;
  }

  displayLocationMap = option => option && option.name || null;
  valueLocationMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private modalAction: ModalAction,
    private util: UtilService,
    private locationQuery: LocationQuery
  ) {

  }

  ngOnInit() {
  }

  async createCustomer() {
    this.loading = true;
    try {
      let data = this.customerAddressForm.getRawValue();
      data = this.util.trimFields(data, ['full_name', 'phone', 'address1']);

      if (!PosCreateCustomerModalComponent.validateCustomerAddress(data)) {
        return this.loading = false;
      }

      const {province_code, district_code, ward_code, address1, full_name, phone} = data;

      if (!this.customer?.id) {
        this.customer = await this.customerService.createCustomer({full_name, phone});
      } else {
        this.customer = await this.customerService.updateCustomer({full_name, phone, id: this.customer.id});
      }

      if ((province_code + district_code + ward_code + address1).length > 0) {
        await this.customerService.createCustomerAddress(
          {
            customer_id: this.customer.id,
            province_code, district_code, ward_code, address1, full_name, phone
          }
        );
      }

      toastr.success('Tạo khách hàng mới thành công.');
      this.modalAction.dismiss(this.customer);
      this.customer = null;

    } catch(e) {
      toastr.error('Tạo khách hàng mới không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }

  closeModal() {
    this.modalAction.close(false);
  }

}
