import {Component, OnInit} from '@angular/core';
import {Order, OrderCustomer} from 'libs/models/Order';
import {OrderService} from 'apps/shop/src/services/order.service';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {FulfillmentService} from 'apps/shop/src/services/fulfillment.service';
import {GoogleAnalyticsService} from 'apps/core/src/services/google-analytics.service';
import {TelegramService} from '@etop/features';
import {PromiseQueueService} from 'apps/core/src/services/promise-queue.service';
import {CustomerService} from 'apps/shop/src/services/customer.service';
import {Product, Variant} from 'libs/models/Product';
import {ProductService} from 'apps/shop/src/services/product.service';
import {AddressService} from 'apps/core/src/services/address.service';
import {PosControllerService} from 'apps/shop/src/app/pages/pos/pos-controller.service';
import {Receipt} from 'libs/models/Receipt';
import {ReceiptService} from 'apps/shop/src/services/receipt.service';
import {ReceiptStoreService} from 'apps/core/src/stores/receipt.store.service';
import {
  CustomerApi, FulfillmentAPI,
  FulfillmentApi,
  InventoryApi,
  OrderApi,
  ReceiptApi
} from '@etop/api';
import {OrderStoreService} from 'apps/core/src/stores/order.store.service';
import {FulfillmentStore} from 'apps/core/src/stores/fulfillment.store';
import {CustomerStoreService} from 'apps/core/src/stores/customer.store.service';
import {takeUntil} from 'rxjs/operators';
import {PrintType} from 'apps/shop/src/app/pages/orders/sell-orders/sell-orders.controller';
import {PosStoreService} from 'apps/core/src/stores/pos.store.service';
import {UtilService} from 'apps/core/src/services/util.service';

@Component({
  selector: 'shop-pos-body',
  templateUrl: './pos-body.component.html',
  styleUrls: ['./pos-body.component.scss']
})
export class PosBodyComponent extends BaseComponent implements OnInit {
  newOrder = new Order({
    customer: new OrderCustomer({}),
    lines: [],
    source: 'etop_pos',
    payment_method: 'cod'
  });

  order_id: string;
  recentOrder: Order;
  orders_to_print: Order[] = [];

  constructor(
    private auth: AuthenticateStore,
    private promiseQueue: PromiseQueueService,
    private ga: GoogleAnalyticsService,
    private telegram: TelegramService,
    private posController: PosControllerService,
    private addressService: AddressService,
    private orderService: OrderService,
    private ffmService: FulfillmentService,
    private customerService: CustomerService,
    private productService: ProductService,
    private receiptService: ReceiptService,
    private orderApi: OrderApi,
    private ffmApi: FulfillmentApi,
    private receiptApi: ReceiptApi,
    private inventoryApi: InventoryApi,
    private customerApi: CustomerApi,
    private orderStore: OrderStoreService,
    private ffmStore: FulfillmentStore,
    private receiptStore: ReceiptStoreService,
    private customerStore: CustomerStoreService,
    private posStore: PosStoreService,
    private util: UtilService
  ) {
    super();
  }

  get isPrintingOrders() {
    return this.posStore.snapshot.print_type == PrintType.orders;
  }

  get isPrintingFfms() {
    return this.posStore.snapshot.print_type == PrintType.fulfillments;
  }

  get isPrintingOrderAndFfm() {
    return this.posStore.snapshot.print_type == PrintType.o_and_f;
  }

  ngOnInit() {
    this.customerStore.selectCustomer$
      .pipe(takeUntil(this.destroy$))
      .subscribe(customer => {
        if (customer && customer.id) {
          this.newOrder.customer_id = customer.id;
        }
      });
  }

  getNewOrder() {
    this.newOrder.total_items = this.newOrder.lines.reduce((a, b) => a + b.quantity, 0);
    // NOTE: basket_value
    this.newOrder.basket_value = this.newOrder.lines.reduce(
      (a, b) => a + (b.payment_price || 0) * b.quantity, 0);

    // NOTE: total_fee
    this.newOrder.fee_lines = this.newOrder.p_data.fee_lines;
    this.newOrder.total_fee = this.newOrder.fee_lines.reduce(
      (a, b) => a + (b.amount || 0), 0);

    // NOTE: total_discount
    this.newOrder.order_discount = this.newOrder.p_data.order_discount;
    if (this.newOrder.p_data.discountPercent) {
      this.newOrder.order_discount = (this.newOrder.p_data.order_discount / 100) * this.newOrder.basket_value;
    }
    this.newOrder.total_discount = this.newOrder.order_discount + this.newOrder.lines.reduce(
      (a, b) => a + b.quantity * (b.retail_price - b.payment_price), 0) || 0;

    // NOTE: total_amount
    const {basket_value, total_fee, total_discount} = this.newOrder;
    this.newOrder.total_amount = basket_value + total_fee - total_discount;

    this.orderStore.updateBasketValue(this.newOrder.basket_value);
    this.orderStore.updateTotalAmount(this.newOrder.total_amount);
  }

  resetData() {
    this.newOrder = new Order({
      customer: new OrderCustomer({}),
      total_fee: 0,
      source: 'etop_pos',
      payment_method: 'cod',
      order_discount: 0,
      total_discount: 0,
      lines: []
    });
    this.newOrder.p_data = {
      order_discount: 0,
      fee_lines: []
    };
    this.order_id = null;
    this.recentOrder = null;
    this.ffmService.resetData();
    this.receiptStore.resetData();
    this.ffmStore.resetState();
    this.customerStore.resetState();
  }

  async checkAndCreateCustomer(order) {
    try {
      const customer = this.customerStore.snapshot.selected_customer;
      if (!customer) {
        return;
      }
      const { full_name, phone } = customer;
      const existed_customers = await this.customerService.checkExistedCustomer({
        phone
      });
      customer.detail = this.util.stripHTML(customer.detail);
      customer.info = this.util.stripHTML(customer.info);
      const found = existed_customers.find(c => c.phone == phone);
      if (!existed_customers || !existed_customers.length || !found) {
        const res = await this.customerApi.createCustomer(customer);
        order.customer_id = res.id;
      } else {
        order.customer_id = found.id;
        if (full_name != found.full_name) {
          await this.customerApi.updateCustomer({ ...customer, id: found.id });
        }
      }
      const customer_address = this.customerStore.snapshot.selected_customer_address;
      if (!customer_address) {
        return;
      }
      const { address1, ward_code, district_code, province_code } = customer_address;
      if (!(address1 && ward_code && district_code && province_code)) {
        return;
      }
      if (customer_address.id) {
        this.customerApi.updateCustomerAddress({...customer_address});
      } else {
        this.customerApi.createCustomerAddress({
          ...customer_address,
          customer_id: order.customer_id,
          full_name: customer.full_name,
          phone: customer.phone
        });
      }
    } catch (e) {
      debug.error('ERROR in checkAndCreateCustomer', e);
    }
  }

  async checkAndCreateProducts(order) {
    try {
      const promises = order.lines
        .map((line, index) => async () => {
          try {
            if (line.p_data.to_save) {
              const new_product = new Product({
                image_urls: [line.image_url],
                name: line.product_name,
                code: Date.now().toString(),
                attributes: line.attributes
              });
              const res = await this.productService.createProduct(new_product);
              const newVariant = new Variant({
                product_id: res.id,
                name: res.name,
                retail_price: line.retail_price,
                image_urls: res.image_urls,
                is_available: true,
                attributes: line.attributes
              });
              const _variant = await this.productService.createVariant(newVariant);
              line = {
                ...line,
                variant_id: _variant.id,
                image_url: _variant.image_urls[0]
              };
              this.newOrder.lines[index] = line;
            }
          } catch (e) {
            debug.error('ERROR in creating variant with promise.all', e);
          }
        });
      await this.promiseQueue.run(promises, 10);
      await this.productService.getProducts();
    } catch (e) {
      debug.error('ERROR in creating products', e);
    }
  }

  async createOrderAndFfm(createAndConfirm: boolean) {
    this.orderStore.creatingOrder(true);
    try {
      for (let i = 0; i < this.newOrder.lines.length; i++) {
        const line = this.newOrder.lines[i];
        if (line.retail_price == null || !line.product_name) {
          return this.handleErrorManually(`Vui lòng nhập đầy đủ thông tin cho sản phẩm thứ ${i + 1}`);
        }
      }

      const {
        customer_id,
        total_items,
        basket_value,
        total_fee,
        order_note,
        order_discount,
        total_discount,
        total_amount,
        fee_lines,
        lines
      } = this.newOrder;

      const body: any = {
        source: 'etop_pos',
        payment_method: 'cod',
        customer_id,
        total_items,
        order_note,
        basket_value,
        total_fee,
        order_discount,
        total_discount,
        total_amount,
        fee_lines,
        lines: lines.map(line => ({
          ...line,
          payment_price: line.retail_price
        }))
      };

      // SECTION Fulfillment
      if (this.ffmStore.snapshot.to_create_ffm) {
        const fulfillment = this.ffmStore.snapshot.fulfillment;
        const {cod_amount, shipping_address, shipping_service_code} = fulfillment;
        if (!shipping_address) {
          return this.handleErrorManually('Thiếu thông tin địa chỉ giao hàng!');
        }
        if (!cod_amount && cod_amount != 0) {
          this.ffmStore.errorCOD(true);
          return this.handleErrorManually('Vui lòng nhập tiền Thu hộ (COD)!');
        }
        if (cod_amount < 5000 && cod_amount > 0) {
          return this.handleErrorManually('Vui lòng nhập tiền thu hộ bằng 0đ hoặc từ 5.000đ trở lên!');
        }
        if (!shipping_service_code) {
          return this.handleErrorManually('Vui lòng chọn gói vận chuyển!');
        }
      }
      if (!this.ffmStore.snapshot.to_create_ffm && (!this.customerService.independent_customer || !this.customerService.independent_customer.id)) {
        await this.checkAndCreateCustomer(body);
      }
      // END Section Fulfillment

      // SECTION Customer
      if (!body.customer_id) {
        const independent_customer = this.customerService.independent_customer;
        if (independent_customer && independent_customer.id) {
          body.customer_id = this.customerService.independent_customer.id;
        } else {
          body.customer = {
            full_name: 'Khách lẻ'
          };
        }
      }
      // END Section Customer

      if (createAndConfirm) {
        await this.confirm(body);
      } else {
        await this.create(body);
      }

      if (this.ffmStore.snapshot.to_create_ffm) {
        await this.createFFM(this.recentOrder.id);
      } else {
        this.createReceipt(this.recentOrder).then();
      }

      if (this.posStore.snapshot.to_print) {
        this.orders_to_print = [await this.orderService.getOrder(this.recentOrder.id)];
        toastr.remove();
        setTimeout(() => {
          window.print();
          this.resetData();
          this.orderService.createOrderSuccess$.next();
          this.orderStore.creatingOrder(false);
        }, 500);
      } else {
        this.resetData();
        this.orderService.createOrderSuccess$.next();
      }

    } catch (e) {
      debug.error('ERROR in createOrderAndFfm()', e);
      this.orderService.createOrderFailed$.next();
    }
    this.orderStore.creatingOrder(false);
  }

  async create(body) {
    try {
      // SECTION Products-Variants-OrderLines
      await this.checkAndCreateProducts(body);
      body.lines = this.newOrder.lines;
      // END Section Products-Variants-OrderLines

      const res = await this.orderService.createOrder(body);
      toastr.success('Đặt hàng thành công.');
      this.recentOrder = res;
    } catch (e) {
      debug.error('ERROR in creating Order', e);
      toastr.error(`Đặt hàng không thành công.`, e.code && (e.message || e.msg) || '');
      throw e;
    }
  }

  async confirm(body, draft?: boolean) {
    try {
      let res: any;
      if (!this.order_id) {
        // SECTION Products-Variants-OrderLines
        await this.checkAndCreateProducts(body);
        body.lines = this.newOrder.lines;
        // END Section Products-Variants-OrderLines

        res = await this.orderService.createOrder(body);
        this.order_id = res.id;
      } else {
        res = await this.orderApi.updateOrder({ ...body, id: this.order_id });
      }
      this.recentOrder = res;

      if (!draft) {
        await this.orderApi.confirmOrder(res.id, 'confirm');
      }
      toastr.success(`Thanh toán đơn hàng thành công.`);
    } catch (e) {
      debug.error('ERROR in confirming Order', e);
      toastr.error(`Thanh toán đơn hàng không thành công.`, e.code && (e.message || e.msg) || '');
      throw e;
    }
  }

  async createReceipt(order: Order) {
    try {
      const only_cash = this.receiptStore.snapshot.receipts.length == 1 &&
        this.receiptStore.snapshot.receipts[0].ledger_type == 'cash';
      const receipts = this.receiptStore.snapshot.receipts.filter(r => r.amount && r.amount > 0)
        .map(r => {
          return new Receipt({
            ledger_id: r.ledger_id,
            type: 'receipt',
            paid_at: new Date(),
            trader_id: order.customer_id,
            amount: (only_cash && r.amount > order.total_amount) && order.total_amount || r.amount,
            title: 'Thanh toán đơn hàng',
            ref_type: 'order',
            lines: [
              {
                title: 'Thanh toán đơn hàng',
                amount: (only_cash && r.amount > order.total_amount) && order.total_amount || r.amount,
                ref_id: order.id
              }
            ]
          });
        });
      if (!receipts.length || !order.customer_id || order.customer_id == '0') {
        return;
      }

      let success = 0;
      for (let i = 0; i < receipts.length; i++) {
        let confirmed = false;
        let receipt = new Receipt({});
        try {
          receipt = await this.receiptService.createReceipt(receipts[i]);
          success += 1;
          await this.receiptApi.confirmReceipt(receipt.id);
          confirmed = true;
        } catch (e) {
          if (!confirmed) {
            toastr.error(`Xác nhận phiếu thu ${receipt.code ? '#' + receipt.code : ''} không thành công!`);
          }
          debug.error(`ERROR in creating receipt ${i}`, e.message || e.msg);
        }
      }
      if (success) {
        toastr.success('Tạo phiếu thu thành công!');
      } else {
        toastr.error('Tạo phiếu thu không thành công!');
      }

    } catch (e) {
      toastr.error('Tạo phiếu thu không thành công!');
      debug.error('ERROR in creating receipt', e.message || e.msg);
    }
  }

  async createFFM(order_id: string) {
    try {
      if (this.ffmService.shipping_type == 'shipment') await this.createShipmentFFM(order_id);
      if (this.ffmService.shipping_type == 'shipnow') await this.createShipnowFFM(order_id);
      toastr.success('Tạo hoá đơn và giao hàng thành công!');
    } catch (e) {
      debug.error('ERROR in creating FFM', e);
      throw e;
    }
  }

  async createShipmentFFM(order_id: string) {
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;
      const body: any = {
        order_id,
        ...fulfillment,
        shipping_type: 'shipment',
        chargeable_weight: this.ffmService.weight_calc_type == 1
          && fulfillment.chargeable_weight || null,
        length: this.ffmService.weight_calc_type == 2
          && fulfillment.length || null,
        width: this.ffmService.weight_calc_type == 2
          && fulfillment.width || null,
        height: this.ffmService.weight_calc_type == 2
          && fulfillment.height || null
      };
      const res = await this.ffmApi.createShipmentFulfillment(body);
      this.socialTrackingEvents(res);
    } catch (e) {
      debug.error('ERROR in createShipmentFFM', e);
      toastr.error('Tạo đơn giao hàng không thành công!', e.code && (e.message || e.msg) || '');
      throw e;
    }
  }

  async createShipnowFFM(order_id: string) {
    try {
      const fulfillment = this.ffmStore.snapshot.fulfillment;
      const {
        shipping_address,
        pickup_address,
        cod_amount,
        shipping_note,
        shipping_service_code,
        shipping_service_fee,
        carrier,
        coupon,
        connection_id
      } = fulfillment;

      const body: FulfillmentAPI.CreateShipnowFulfillmentRequest = {
        delivery_points: [{
          order_id,
          chargeable_weight: 1000,
          cod_amount,
          shipping_address
        }],
        pickup_address,
        shipping_note,
        shipping_service_code,
        shipping_service_fee,
        carrier,
        coupon,
        connection_id
      };

      const shipnow_ffm = await this.ffmService.createShipnowFulfillment(body);
      await this.confirmShipnowFFM(shipnow_ffm.id);
    } catch (e) {
      debug.error('ERROR in createShipnowFFM', e);
      toastr.error('Tạo đơn giao tức thì không thành công!', e.code && (e.message || e.msg) || '');
      throw e;
    }
  }

  async confirmShipnowFFM(shipnow_id: string) {
    try {
      const message_data = await this.ffmService.confirmShipnowFulfillment(shipnow_id);
      this.telegram.shipnowConfirm(message_data);
    } catch (e) {
      this.ffmService.cancelShipnowFulfillment(
        shipnow_id,
        'Xác nhận giao tức thì không thành công'
      );
      debug.error('ERROR in confirmShipnowFFM', e);
      throw e;
    }
  }

  private handleErrorManually(error_message: string) {
    toastr.error(error_message);
    this.orderStore.creatingOrder(false);
  }

  private socialTrackingEvents(data) {
    try {
      const ffm = data[0];
      const order = data[0].order;
      this.ga.sendOrderInfo(
        order, ffm, this.auth.snapshot.shop,
        ffm.shipping_address, ffm.pickup_address
      );
      this.telegram.sendOrderTelegramMessage(order, 'confirm', null, ffm);
    } catch (e) {
      debug.error('ERROR in GA and Telegram', e);
    }
  }

}
