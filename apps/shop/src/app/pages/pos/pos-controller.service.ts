import { Injectable } from '@angular/core';
import { OrderLine } from 'libs/models/Order';
import { ProductService } from 'apps/shop/src/services/product.service';

@Injectable()
export class PosControllerService {

  constructor(
    private productService: ProductService
  ) { }

  orderLineMap(variant) {
    let line = new OrderLine();
    line.variant_id = variant.id;
    line.product_name = this.productService.makeProductNameForOrderLines(variant);
    line.quantity = variant.quantity || 1;
    line.list_price = variant.list_price;
    line.retail_price = variant.retail_price;
    line.payment_price = variant.retail_price;
    line.image_url = Array.isArray(variant.image_urls) ? variant.image_urls[0] : variant.image_urls;
    line.attributes = variant.attributes;
    line.p_data.editable = !variant.id;
    line.p_data.code = variant.p_data.code;
    line.inventory_variant = variant.inventory_variant || 0;
    return line;
  }

}
