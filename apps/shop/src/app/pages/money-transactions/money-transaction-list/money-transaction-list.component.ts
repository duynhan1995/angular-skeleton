import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EmptyType, EtopTableComponent, SideSliderComponent } from '@etop/shared';
import { MoneyTransactionShop } from 'libs/models/MoneyTransactionShop';
import { MoneyTransactionsService} from '../money-transactions.service';
import { MoneyTransactionsStore } from '../money-transactions.store';
import { PageBaseComponent } from "@etop/web/core/base/page.base-component";
import { AuthenticateStore } from "@etop/core";
import { distinctUntilChanged, map, takeUntil } from "rxjs/operators";
import { Filters } from '@etop/models';

@Component({
  selector: 'shop-mt-list',
  templateUrl: './money-transaction-list.component.html',
  styleUrls: ['./money-transaction-list.component.scss']
})
export class MoneyTransactionListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('transactionTable', { static: true }) transactionTable: EtopTableComponent;
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;

  shop_id: string;

  mtLoading$ = this.store.state$.pipe(
    map(s => s?.mtLoading),
    distinctUntilChanged()
  );
  mtList$ = this.store.state$.pipe(map(s => s?.mts));

  isLastPage$ = this.store.state$.pipe(
    map(s => s?.mtIsLastPage),
    distinctUntilChanged()
  );

  onCreateMtTransfer = false;

  constructor(
    private auth: AuthenticateStore,
    private changeDetector: ChangeDetectorRef,
    private transactionService: MoneyTransactionsService,
    private store: MoneyTransactionsStore
  ) {
    super();
  }

  get sliderTitle() {
    const activeMts = this.store.snapshot.activeMts;
    return `Thao tác trên <strong>${activeMts.length}</strong> phiên`;
  }

  get showPaging() {
    return !this.transactionTable.liteMode && !this.transactionTable.loading;
  }

  get emptyResultFilter() {
    const {
      mtPaging,
      mtFilters,
      mts
    } = this.store.snapshot;
    return mtPaging.offset == 0 &&
      mtFilters.length > 0 &&
      mts.length == 0 &&
      this.shop_id;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy phiên chuyển tiền phù hợp';
    }
    return 'Chưa có phiên chuyển tiền';
  }

  get emptyType() {
    if (this.emptyResultFilter) {
      return EmptyType.search;
    }
    return EmptyType.default;
  }

  isActiveMt(mt: MoneyTransactionShop) {
    const activeMts = this.store.snapshot.activeMts;
    return activeMts.some(item => item.id == mt.id);
  }

  ngOnInit() {
    this.mtLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.transactionTable.loading = loading;
        this.changeDetector.detectChanges();
        if (loading) {
          this.resetState();
        }
      });

    this.isLastPage$.pipe(takeUntil(this.destroy$))
      .subscribe(isLastPage => {
        if (isLastPage) {
          this.transactionTable.toggleNextDisabled(true);
          this.transactionTable.decreaseCurrentPage(1);
          toastr.info('Bạn đã xem tất cả phiên chuyển tiền.');
        }
      });
  }

  ngOnDestroy() {
    this.transactionService.selectFulfillments([]);
  }

  resetState() {
    this.transactionTable.toggleLiteMode(false);
    this.slider.toggleLiteMode(false);
    this.transactionTable.toggleNextDisabled(false);
    this.onCreateMtTransfer = false;
  }

  filter(filters: Filters, shop_id?: string) {
    this.transactionService.setMoneyTransactionFilters(filters);
    this.shop_id = shop_id;
    this.transactionTable.resetPagination();
  }

  loadPage({ page, perpage }) {
    this.transactionService.setMeyTransactionPaging({
      limit: perpage,
      offset: (page - 1) * perpage
    });
    this.getMts();
  }

  getMts() {
    this.transactionService.getMoneyTransactions(true, this.shop_id).then();
  }

  itemSelected(item: MoneyTransactionShop) {
    const activeMts = this.store.snapshot.activeMts;
    activeMts.push(item);
    this.transactionService.selectMoneyTransactions(activeMts);

    this._checkSelectMode();
  }

  onSliderClosed() {
    this.onCreateMtTransfer = false;
    this.transactionService.selectMoneyTransactions([]);

    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const activeMts = this.store.snapshot.activeMts;
    const selecteds = activeMts.length > 0;
    this.transactionTable.toggleLiteMode(selecteds);
    this.slider.toggleLiteMode(selecteds);
  }

}
