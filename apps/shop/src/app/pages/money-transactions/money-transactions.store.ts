import { Injectable } from '@angular/core';
import { AStore } from 'apps/core/src/interfaces/AStore';
import { Connection } from "libs/models/Connection";
import { Paging } from "@etop/shared";
import { Fulfillment } from "libs/models/Fulfillment";
import { MoneyTransactionShop } from "libs/models/MoneyTransactionShop";
import { Filters } from '@etop/models';

export interface MoneyTransactionData {
  connections: Connection[];

  mtFilters: Filters;
  mtPaging: Paging;
  mtIsLastPage: boolean;
  mtLoading: boolean;
  activeMt: MoneyTransactionShop;
  activeMts: MoneyTransactionShop[];
  mts: MoneyTransactionShop[];

  fulfillmentsLoading: boolean;
  fulfillments: Fulfillment[];
  activeFulfillments: Fulfillment[];
}

@Injectable()
export class MoneyTransactionsStore extends AStore<MoneyTransactionData> {
  initState = {
    connections: [],

    mtFilters: [],
    mtPaging: null,
    mtIsLastPage: false,
    mtLoading: true,
    activeMt: null,
    activeMts: [],
    mts: [],

    fulfillmentsLoading: true,
    fulfillments: [],
    activeFulfillments: [],

  };

  constructor() {
    super();
  }

  setConnections(connections: Connection[]) {
    this.setState({connections});
  }

  setMoneyTransactionFilters(filters: Filters) {
    this.setState({mtFilters: filters});
  }

  setMoneyTransactionPaging(paging: Paging) {
    this.setState({mtPaging: paging});
  }

  setMoneyTransactionLastPage(lastPage: boolean) {
    this.setState({ mtIsLastPage: lastPage })
  }

  setMoneyTransactionLoading(loading: boolean) {
    this.setState({mtLoading: loading});
  }

  setMoneyTransactions(transactions: MoneyTransactionShop[]) {
    this.setState({mts: transactions});
  }

  detailMoneyTransaction(transaction: MoneyTransactionShop) {
    this.setState({activeMt: transaction});
  }

  selectMoneyTransactions(transactions: MoneyTransactionShop[]) {
    this.setState({activeMts: transactions});
  }

  setFulfillmentsLoading(loading: boolean) {
    this.setState({fulfillmentsLoading: loading});
  }

  setFulfillments(fulfillments: Fulfillment[]) {
    this.setState({fulfillments});
  }

  selectFulfillments(fulfillments: Fulfillment[]) {
    this.setState({activeFulfillments: fulfillments});
  }

}
