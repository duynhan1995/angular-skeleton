import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EtopTableComponent } from '@etop/shared';
import { MoneyTransactionApi } from '@etop/api';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { distinctUntilChanged, map, takeUntil } from "rxjs/operators";
import { AuthenticateStore } from "@etop/core";
import { PageBaseComponent } from "@etop/web";
import { MoneyTransactionsStore } from '../../money-transactions.store';
import { MoneyTransactionsService } from '../../money-transactions.service';

@Component({
  selector: 'shop-mt-detail',
  templateUrl: './money-transaction-detail.component.html',
  styleUrls: ['./money-transaction-detail.component.scss']
})
export class MoneyTransactionDetailComponent extends PageBaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('ffmTable', { static: false }) ffmTable: EtopTableComponent;

  sum = {
    delivered_count: 0,
    returned_count: 0,
    cod_count: 0,
    none_cod_count: 0,
  };

  fulfillmentsLoading$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillmentsLoading),
    distinctUntilChanged()
  );
  fulfillments$ = this.transactionsStore.state$.pipe(
    map(s => s?.fulfillments),
    distinctUntilChanged((a,b) => a?.length == b?.length)
  );

  mt$ = this.transactionsStore.state$.pipe(map(s => s?.activeMt));

  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private dialog: DialogControllerService,
    private transactionApi: MoneyTransactionApi,
    private transactionsStore: MoneyTransactionsStore,
    private transactionService: MoneyTransactionsService,
    private ref: ChangeDetectorRef
  ) {
    super();
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction:view');
  }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    await this.transactionService.getMoneyTransaction(id).then();
    this.fulfillments$.pipe(takeUntil(this.destroy$))
      .subscribe(ffms => {
        ffms.forEach(ffm => {
          switch (ffm.shipping_state) {
            case 'delivered':
              this.sum.delivered_count += 1;
              break;
            case 'returning':
              this.sum.returned_count += 1;
              break;
            case 'returned':
              this.sum.returned_count += 1;
              break;
            default:
              break;
          }

          if (ffm.total_cod_amount > 0 && !['returned', 'returning'].includes(ffm.shipping_state)) {
            this.sum.cod_count += 1;
          } else {
            this.sum.none_cod_count += 1;
          }
        });
      });

  }

  ngAfterViewInit() {
    this.fulfillmentsLoading$.pipe(takeUntil(this.destroy$))
      .subscribe(loading => {
        this.ffmTable.loading = loading;
        this.ref.detectChanges();
        if (loading) {
          this.resetState();
        } else {
        }
      });
  }

  ngOnDestroy() {}

  resetState() {
    this.ffmTable.toggleLiteMode(false);
  }

}
