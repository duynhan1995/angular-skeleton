import { Component, Input, OnInit } from '@angular/core';
import { MoneyTransactionShop } from 'libs/models/MoneyTransactionShop';
import { Router } from '@angular/router';

@Component({
  selector: '[shop-mt-row]',
  templateUrl: './money-transaction-row.component.html',
  styleUrls: ['./money-transaction-row.component.scss']
})
export class MoneyTransactionRowComponent implements OnInit {
  @Input() transaction: MoneyTransactionShop;
  @Input() liteMode = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  transactionDetail() {
    this.router.navigateByUrl(`s/tran/money-transactions/${this.transaction.id}`);
  }

}
