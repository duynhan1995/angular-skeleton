import { Component, OnInit, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ForgotPasswordModalComponent } from 'apps/shared/src/pages/login/modals/forgot-password-modal/forgot-password-modal.component';
import { Invitation } from 'libs/models/Authorization';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationApi } from '@etop/api';

enum View {
  NONE = 'none',
  PHONE_SUBMIT = 'phonesubmit',
  PHONE_VERIFY = 'phoneverify',
  LOGIN = 'login',
  REGISTER = 'register',
  CREATE_SHOP = 'createshop'
}

@Component({
  selector: 'shop-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('passwordInput', {static: false}) passwordInput: ElementRef;
  login: string;
  password: string;
  phone: string;
  verify_code: string;
  email: string;
  fullname: string;

  loading = false;

  invitation_token = '';
  invitation = new Invitation({});

  loadingView = true;

  constructor(
    private commonUsecase: CommonUsecase,
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute,
    private authorizationApi: AuthorizationApi,
    private router: Router
  ) {}

  async ngOnInit() {
    this.invitation_token = this.activatedRoute.snapshot.queryParamMap.get(
      'invitation_token'
    );
    if (this.invitation_token) {
      this.invitation = await this.authorizationApi.getInvitationByToken(
        this.invitation_token
      );
      this.login = (this.invitation && this.invitation.email) || '';
    }
    this.commonUsecase.redirectIfAuthenticated().then(() => (this.loadingView = false));
  }

  registerHref() {
    if (this.invitation_token) {
      this.router.navigateByUrl(
        `/register?invitation_token=${this.invitation_token}`
      );
    } else {
      this.router.navigateByUrl(`/register`);
    }
  }

  async onEnterLoginInput() {
    if (!this.password) {
      if (this.passwordInput) {
        setTimeout(_ => {
          this.passwordInput.nativeElement.focus();
        });
      }
    } else {
      this.onLogin();
    }
  }

  showPassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'text';
      });
    }
  }

  hidePassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'password';
      });
    }
  }

  async onLogin() {
    if (!this.login || !this.password) {
      toastr.error('Vui lòng nhập đầy đủ thông tin đăng nhập để tiếp tục.');
      return;
    }

    this.loading = true;
    try {
      await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
    this.loading = false;
  }

  forgotPassword() {
    const modal = this.modalController.create({
      component: ForgotPasswordModalComponent
    });
    modal.show().then();
    modal.onDismiss().then(() => {});
  }
}
