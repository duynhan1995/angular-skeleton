import { NgModule } from '@angular/core';

// libs
import { EtopIonicCoreModule } from '@etop/ionic';

@NgModule({
  imports: [EtopIonicCoreModule]
})
export class CoreModule {}
