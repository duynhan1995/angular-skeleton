### In `apps/ionic-faboshop/android/app/src/main/AndroidManifest.xml`:

- add this attribute `xmlns:tools="http://schemas.android.com/tools"` to `<manifest>` open tag:
```
<manifest
  xmlns:android="http://schemas.android.com/apk/res/android"
  package="vn.faboshop.app"
  xmlns:tools="http://schemas.android.com/tools">
```
- add these lines of code inside `<application>` tag:

```
<meta-data
tools:replace="android:value"
android:name="com.facebook.sdk.ApplicationId"
android:value="@string/facebook_app_id"/>

<activity
android:name="com.facebook.FacebookActivity"
android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
android:label="@string/app_name"
tools:replace="android:label">
    <intent-filter>
     <action android:name="android.intent.action.VIEW" />
     <category android:name="android.intent.category.DEFAULT" />
     <categoryandroid:name="android.intent.category.BROWSABLE"/>
     <data android:scheme="@string/fb_login_protocol_scheme" />
    </intent-filter>
</activity>

<activity
  android:name="com.facebook.CustomTabActivity"
  android:exported="true">
    <intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data android:scheme="@string/fb_login_protocol_scheme" />
    </intent-filter>
</activity>
```

### In `apps/ionic-faboshop/android/build.gradle`:

- add this inside `buildscript { repositories { `:

```
mavenCentral()
```

- add this inside `allprojects { `:

```
configurations {
    compile.exclude group: 'com.google.zxing'
}
```

### In `apps/ionic-faboshop/android/app/src/main/res/values/strings.xml`:

- add this inside `<resources>` tag:

```
<string name="facebook_app_id">1581362285363031</string>
<string name="fb_login_protocol_scheme">fb1581362285363031</string>
<string name="fb_app_name">Faboshop - Test</string>
```

### In `apps/ionic-faboshop/android/app/build.gradle`:

- add this inside `dependencies { `:

```
implementation 'com.facebook.android:facebook-android-sdk:[4,5)'
```
