import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkStatusComponent } from './network-status.component';



@NgModule({
  declarations: [NetworkStatusComponent],
  entryComponents: [],
  imports: [CommonModule, FormsModule, IonicModule],
  exports: [NetworkStatusComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NetworkStatusModule {}
