import { Component, OnInit, Input } from '@angular/core';
import { FacebookUserTagQuery, FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { ModalController } from '@ionic/angular';
import { FbUserTag, FbUser } from '@etop/models';
import { ConversationsService } from '@etop/state/fabo/conversation';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'ionfabo-select-tags',
  templateUrl: './select-tags.component.html',
  styleUrls: ['./select-tags.component.scss']
})
export class SelectTagsComponent implements OnInit {
  @Input() external_user_id;
  @Input() tag_ids: FbUserTag[];
  tags: any;
  currentTags: any;
  fbUser: FbUser;
  constructor(
    private fbUserTagQuery: FacebookUserTagQuery,
    private modalCtrl: ModalController,
    private conversationService: ConversationsService,
    private toast: ToastService,
    private fbUserTagService: FacebookUserTagService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    const tags = this.fbUserTagQuery.getAll();
    this.tags = [...tags];
    this.tags = this.tags.map(tag => {
      return {
        ...tag,
        isChecked: this.tag_ids.includes(tag.id)
      };
    });
  }

  async dismiss() {
    this.modalCtrl.dismiss();
  }

  async createTag() {
    await this.modalCtrl.dismiss({ tag_ids: [], role: 'create' });
  }

  async setUserTag() {
    try {
      const checkList = this.tags
        .filter(tag => tag.isChecked == true)
        .map(tag => tag.id);
      const res = await this.fbUserTagService.updateTags(this.external_user_id, checkList);
      if (this.tag_ids.length > 0) {
        if (checkList.length == 0) {
          this.toast.success('Xóa thẻ thành công');
        } else {
          this.toast.success('Cập nhật thẻ thành công');
        }
      } else {
        this.toast.success('Gắn thẻ thành công');
      }
      this.modalCtrl.dismiss({ tag_ids: res, role: 'update' });
    } catch (e) {
      this.toast.success('Gắn thẻ thất bại' + e.message);
      debug.log('ERROR in set tag user', e);
    }
  }
}
