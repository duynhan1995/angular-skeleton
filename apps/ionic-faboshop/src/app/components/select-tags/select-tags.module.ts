import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectTagsComponent } from './select-tags.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CreateAndUpdateTagModule } from '../create-and-update-tag/create-and-update-tag.module';



@NgModule({
  declarations: [SelectTagsComponent],
  entryComponents: [SelectTagsComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateAndUpdateTagModule,
    ReactiveFormsModule
  ],
  exports: [SelectTagsComponent]
})
export class SelectTagsModule {}
