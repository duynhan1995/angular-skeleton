import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  routeLink = 'messages';
  linkSlug = `/s/${this.util.getSlug()}`;
  tabsList = [
    {
      link: `${this.linkSlug}/messages`,
      icon: 'chatbox-ellipses',
      label: 'Tin nhắn'
    },
    {
      link: `${this.linkSlug}/orders`,
      icon: 'cart',
      label: 'Đơn hàng'
    },
    {
      link: `${this.linkSlug}/customers`,
      icon: 'people',
      label: 'Khách hàng'
    },
    {
      link: `${this.linkSlug}/products`,
      icon: 'pricetag',
      label: 'Sản phẩm'
    },
    {
      link: `${this.linkSlug}/account`,
      icon: 'settings',
      label: 'Thiết lập'
    }
  ];
  constructor(
    private router: Router,
    private navCtrl: NavController,
    private platform: Platform,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef
  ) {
    this.changeRoute();
    this.platform.backButton.subscribe(() => {
      debug.log('backButton', this.router.url);
      if (this.router.url === `${this.linkSlug}/orders`) {
        // tslint:disable-next-line: no-string-literal
        navigator['app'].exitApp();
      }
    });
  }

  ngOnInit() {
    this.getColorActive('messages');
  }

  changeRoute() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeLink = event.urlAfterRedirects;
        this.activeTabs();
        this.changeDetector.detectChanges();
      }
    });
  }

  getColorActive(link) {
    return link === this.routeLink ? true : '';
  }

  get colorActive() {
    return this.routeLink;
  }

  activeTabs() {
    return !!this.tabsList.find(tab => tab.link === this.routeLink);
  }

  active(link) {
    return link === this.routeLink;
  }

  gotoLink(link) {
    this.navCtrl.navigateForward(link, { animated: false });
  }
}
