import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonInput, ModalController } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { UserService } from 'apps/core/src/services/user.service';
import { AuthenticateStore, ConfigService } from '@etop/core';

@Component({
  selector: 'etop-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  @ViewChild('codeVerify1', { static: false }) codeVerify1: IonInput;
  @ViewChild('codeVerify2', { static: false }) codeVerify2: IonInput;
  @ViewChild('codeVerify3', { static: false }) codeVerify3: IonInput;
  @ViewChild('codeVerify4', { static: false }) codeVerify4: IonInput;
  @ViewChild('codeVerify5', { static: false }) codeVerify5: IonInput;
  @ViewChild('codeVerify6', { static: false }) codeVerify6: IonInput;
  @Input() phone;
  loadReset = false;
  codeVerify = ['', '', '', '', '', ''];
  sendCode = '';
  new_password;
  confirm_password;
  ionLoading = false;
  token: any;
  countdown = 60;

  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService,
    private userService: UserService,
    private auth: AuthenticateStore,
    private config: ConfigService
  ) {}

  ngOnInit() {
    setTimeout(() => {
      this.codeVerify1.setFocus();
    }, 150);
  }

  ionViewWillEnter() {
    this.countTimeVerify();
  }

  focusInput(index) {
    const input: IonInput = this[`codeVerify${index + 1}`];
    if (index < 6 && index > 0) {
      input.setFocus();
    }
  }

  countTimeVerify() {
    this.countdown = 60;
    let interval = setInterval(() => {
      if (this.countdown < 1) {
        clearInterval(interval);
        debug.log('countdown', this.countdown);
      } else {
        this.countdown -= 1;
      }
    }, 1000);
  }

  async checkCode() {
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < this.codeVerify.length; index++) {
      this.sendCode = this.codeVerify.join('');
      debug.log('sendCode', this.sendCode);
      if (this.codeVerify[index] === '') {
        return this.toast.error('Vui lòng nhập đủ mã xác nhận!');
      }
    }
    try {
      let res = await this.userService.verifyPhoneResetPasswordUsingToken(
        this.sendCode
      );
      this.loadReset = true;
      this.token = res.reset_password_token;
    } catch (e) {
      this.codeVerify = ['', '', '', '', '', ''];
      setTimeout(() => {
        this.codeVerify1.setFocus();
      }, 150);
      this.toast.error(e.message);
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  onInputChange(event, index?) {
    if (event) {
      this.focusInput(index);
    } else {
      this.focusInput(index - 2);
    }
  }

  onChange(event, index) {
    if (this.codeVerify[index]) {
      this.focusInput(index + 1);
    }
  }

  async createPassword() {
    try {
      if (!this.new_password || !this.confirm_password) {
        return this.toast.error('Vui lòng nhập đầy đủ thông tin!');
      }
      if (this.new_password !== this.confirm_password) {
        return this.toast.error('Mật khẩu xác nhận không trùng khớp!');
      }
      this.ionLoading = true;
      let data = {
        confirm_password: this.confirm_password,
        new_password: this.new_password,
        reset_password_token: this.token
      };
      await this.userService.updatePasswordUsingToken(data);
      this.auth.clear();
      this.ionLoading = false;
      this.modalCtrl.dismiss(true);
      this.toast.success('Đặt lại mật khẩu thành công');
    } catch (e) {
      this.ionLoading = false;
      debug.log('Error in updatePasswordUsingToken', e);
      this.toast.error(e.message);
    }
  }

  async retry() {
    try {
      // let captcha = await AuthenticateService.getReCaptcha(this.config.get('recaptcha_key'));
      let res = await this.userService.requestResetPasswordByPhone({
        phone: this.phone,
        // recaptcha_token: captcha
      });
      this.auth.updateToken(res.access_token);
      this.countTimeVerify();
    } catch (e) {
      this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      debug.log('ERROR retry send requestResetPasswordByPhone', e);
    }
  }

}
