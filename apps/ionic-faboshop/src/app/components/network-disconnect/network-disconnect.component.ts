import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'etop-network-disconnect',
  templateUrl: './network-disconnect.component.html',
  styleUrls: ['./network-disconnect.component.scss']
})
export class NetworkDisconnectComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
