import {Component, Input, OnInit} from '@angular/core';
import {Customer} from 'libs/models/Customer';
import {ModalController} from '@ionic/angular';
import {ToastService} from 'apps/ionic-faboshop/src/app/services/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { ConfirmOrderService } from '@etop/features';

@Component({
  selector: 'etop-create-customer-popup',
  templateUrl: './create-customer-popup.component.html',
  styleUrls: ['./create-customer-popup.component.scss']
})
export class CreateCustomerPopupComponent implements OnInit {
  @Input() customer: Customer;
  @Input() param;
  @Input() activeFbUser: any;

  constructor(
    private toast: ToastService,
    private modalCtrl: ModalController,
    private loading: LoadingService,
    private confirmOrderService: ConfirmOrderService
  ) {}

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss({ closed: true }).then();
  }

  create() {
    if (!this.customer.full_name) {
      return this.toast.error('Chưa nhập tên.');
    }
    if (!this.customer.phone) {
      return this.toast.error('Chưa nhập số điện thoại.');
    }
    this.loading.start('Đang tạo khách hàng...')
      .then(() => this.confirmOrderService.createCustomer(this.customer))
      .then(customer => this.modalCtrl.dismiss(customer))
      .catch(e => this.toast.error(`Tạo khách hàng không thành công. ${e.code && (e.message || e.msg) || ''}`))
      .finally(() => this.loading.end());
  }
}
