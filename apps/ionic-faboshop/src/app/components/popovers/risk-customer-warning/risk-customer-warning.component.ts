import { Component, Input, OnInit } from '@angular/core';
import { FbCustomerReturnRate } from '@etop/models';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'ionfabo-risk-customer-warning',
  templateUrl: './risk-customer-warning.component.html',
  styleUrls: ['./risk-customer-warning.component.scss']
})
export class RiskCustomerWarningComponent implements OnInit {
  @Input() rating: FbCustomerReturnRate;
  @Input() phone;
  @Input() messageError: any;

  constructor(private popoverCtrl: PopoverController) { }

  ngOnInit(): void {}

  dismiss() {
    this.popoverCtrl.dismiss();
  }

}
