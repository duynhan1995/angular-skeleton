import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUpdateAddressPopupComponent } from './create-update-address-popup/create-update-address-popup.component';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListAddressesPopupComponent } from './list-addresses-popup/list-addresses-popup.component';
import {EtopPipesModule} from '@etop/shared';
import { CreateCustomerPopupComponent } from './create-customer-popup/create-customer-popup.component';
import { ImageLoaderModule } from '@etop/features';
import { SelectSuggestModule } from '../select-suggest/select-suggest.module';
import { RiskCustomerWarningComponent } from './risk-customer-warning/risk-customer-warning.component';

@NgModule({
  declarations: [
    CreateUpdateAddressPopupComponent,
    ListAddressesPopupComponent,
    CreateCustomerPopupComponent,
    RiskCustomerWarningComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    EtopPipesModule,
    ImageLoaderModule,
    SelectSuggestModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopoversModule {}
