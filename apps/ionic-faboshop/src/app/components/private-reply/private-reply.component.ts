import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { Message } from '@etop/models';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';

@Component({
  selector: 'ionfabo-private-reply',
  templateUrl: './private-reply.component.html',
  styleUrls: ['./private-reply.component.scss']
})
export class PrivateReplyComponent implements OnInit {
  @Input() message = new Message();
  consversation = this.conversationsQuery.getActive();
  text: string;
  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService,
    private conversationsService: ConversationsService,
    private conversationsQuery: ConversationsQuery,
  ) {}

  ngOnInit(): void {}

  ionViewWillEnter() {
    
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async senMessage() {
    if (!this.text) {
      this.toast.error('Vui lòng nhập nội dung tin nhắn');
      return;
    }
    try {
      await this.conversationsService.sendPrivateReply(this.message, this.text)
      this.toast.success('Gửi tin nhắn riêng thành công.')
    } catch(e) {
      this.toast.error(e.code && (e.message || e.msg) || 'Gửi tin nhắn riêng thất bại.')
    }
    this.dismiss();
  }
}
