import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivateReplyComponent } from './private-reply.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [PrivateReplyComponent],
  imports: [CommonModule, FormsModule, IonicModule]
})
export class PrivateReplyModule {}
