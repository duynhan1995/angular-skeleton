import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';

@Component({
  selector: 'ionfabo-tab-nav',
  templateUrl: './tab-nav.component.html',
  styleUrls: ['./tab-nav.component.scss']
})
export class TabNavComponent implements OnInit {
  @ViewChild(IonTabs) tabs: IonTabs;
  tabsList = [
    {
      link: `messages`,
      icon: 'chatbox-ellipses',
      label: 'Tin nhắn'
    },
    {
      link: `orders`,
      icon: 'cart',
      label: 'Đơn hàng'
    },
    {
      link: `customers`,
      icon: 'people',
      label: 'Khách hàng'
    },
    {
      link: `dashboard`,
      icon: 'bar-chart',
      label: 'Thống kê'
    },
    {
      link: `account`,
      icon: 'settings',
      label: 'Thiết lập'
    }
  ];

  currentTab = '';
  constructor() {}

  ngOnInit(): void {}

  setTab() {
    this.currentTab = this.tabs.getSelected();
  }
}
