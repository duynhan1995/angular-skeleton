import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabNavComponent } from './tab-nav.component';
import { Routes } from '@angular/router';
import { ListConversationModule } from '../../pages/messages/components/list-conversation/list-conversation.module';
import { ListConversationComponent } from '../../pages/messages/components/list-conversation/list-conversation.component';
import { OrderListModule } from 'apps/ionic-faboshop/src/app/pages/orders/order-list/order-list.module';
import { OrderListComponent } from 'apps/ionic-faboshop/src/app/pages/orders/order-list/order-list.component';
import { ListCustomerComponent } from 'apps/ionic-faboshop/src/app/pages/customers/list-customer/list-customer.component';
import { ListCustomerModule } from 'apps/ionic-faboshop/src/app/pages/customers/list-customer/list-customer.module';
import { AccountMenuComponent } from 'apps/ionic-faboshop/src/app/pages/account/account-menu/account-menu.component';
import { AccountMenuModule } from 'apps/ionic-faboshop/src/app/pages/account/account-menu/account-menu.module';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { DashboardModule } from '../../pages/dashboard/dashboard.module';

export const tabNavRoutes: Routes = [
  {
    path: 'tab-nav',
    component: TabNavComponent,
    children: [
      {
        path: '',
        redirectTo: 'messages',
        pathMatch: 'full'
      },
      {
        path: 'messages',
        component: ListConversationComponent
      },
      {
        path: 'orders',
        component: OrderListComponent
      },
      {
        path: 'customers',
        component: ListCustomerComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'account',
        component: AccountMenuComponent
      }
    ]
  }
];

@NgModule({
  declarations: [TabNavComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListConversationModule,
    OrderListModule,
    ListCustomerModule,
    AccountMenuModule,
  ],
  exports: [TabNavComponent, ListConversationModule, OrderListModule, ListCustomerModule, AccountMenuModule, DashboardModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TabNavModule {
}
