import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarriersListComponent} from "apps/ionic-faboshop/src/app/components/carrier-connect/carriers-list/carriers-list.component";
import {IonicModule} from "@ionic/angular";
import { CarrierLoginComponent } from './carrier-login/carrier-login.component';
import {FormsModule} from "@angular/forms";
import { CarrierConnectComponent } from 'apps/ionic-faboshop/src/app/components/carrier-connect/carrier-connect.component';
import { CarrierOtpComponent } from './carrier-otp/carrier-otp.component';

@NgModule({
  declarations: [
    CarriersListComponent,
    CarrierLoginComponent,
    CarrierConnectComponent,
    CarrierOtpComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class CarrierConnectModule { }
