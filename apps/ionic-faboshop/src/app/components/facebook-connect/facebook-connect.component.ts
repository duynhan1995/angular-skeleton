import { Component, NgZone, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, NavController } from '@ionic/angular';
import { PageApi } from '@etop/api';
import { ChatConnectionModalComponent } from 'apps/ionic-faboshop/src/app/components/chat-connection-modal/chat-connection-modal.component';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { FilterOperator } from '@etop/models';
import { Plugins } from '@capacitor/core';
import { FacebookLoginResponse } from 'capacitor-facebook-plugin';
import { FbPages, FbPage } from "libs/models/faboshop/FbPage";
import { ConversationsService } from '@etop/state/fabo/conversation';

@Component({
  selector: 'etop-facebook-connect',
  templateUrl: './facebook-connect.component.html',
  styleUrls: ['./facebook-connect.component.scss']
})
export class FacebookConnectComponent implements OnInit {
  fb_pages: FbPages = [];

  loading = false;

  constructor(
    private modalCtrl: ModalController,
    private popoverController: PopoverController,
    private alertController: AlertController,
    private zone: NgZone,
    private toast: ToastService,
    private api: PageApi,
    private navCtrl: NavController,
    private conversationsService: ConversationsService
  ) {}

  async ngOnInit() {
    await this.listPages();
  }

  dismiss() {
    this.navCtrl.back();
  }

  async fbConnect() {
    this.loading = true;
    try {
      const FB = Plugins.FacebookPlugin;
      await FB.logout();
      const res: FacebookLoginResponse = await FB.login({
        permissions: 'pages_show_list,pages_messaging,pages_manage_metadata,pages_read_engagement,pages_read_user_content,pages_manage_engagement,pages_manage_posts'
      });

      const token = res.accessToken && res.accessToken.token;
      if (token) {
        const connectionResult = await this.api.connectPages(token);
        await this.openModalConnectionResult(connectionResult);
      }
    } catch (e) {
      debug.error('ERROR in loginFacebook', JSON.stringify(e));
      if (e.code) {
        this.toast.error(`Có lỗi xảy ra khi kết nối với Facebook. ${e.message || e.msg}`);
      }
      await this.listPages();
    }
    this.loading = false;
  }

  async doRefresh(event) {
    this.zone.run(async() => {
      await this.listPages();
    })
    event.target.complete();
  }

  async listPages() {
    this.loading = true;
    try {
      this.fb_pages = await this.api.listPages({
        filters: [{ name: 'status', op: FilterOperator.eq, value: 'P' }]
      });
      this.conversationsService.setConversations([]);
      this.conversationsService.loadMoreConversations(true);
    } catch (e) {
      debug.error('ERROR in listPages', e);
    }
    this.loading = false;
  }

  async openModalConnectionResult(connectionResult) {
    const popover = await this.popoverController.create({
      component: ChatConnectionModalComponent,
      translucent: true,
      componentProps: {
        connectionResult
      },
      cssClass: 'select-variant chat-connection',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    popover.onDidDismiss().then(async () => {
      await this.listPages();
    });
    return await popover.present();
  }

  async removePage(index: number, page_external_id: string) {
    const alert = await this.alertController.create({
      header: 'Xóa fanpage',
      message: 'Bạn thực sự muốn xóa fanpage này?',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: _ => {}
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: () => {
            this.zone.run(async () => {
              await this.confirmRemovePage(index, page_external_id);
              alert.dismiss(true);
            });
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  async confirmRemovePage(index: number, page_external_id: string) {
    try {
      await this.api.removePages(page_external_id);
      this.fb_pages.splice(index, 1);
      this.toast.success('Xoá fanpage thành công.');
      this.conversationsService.setConversations([]);
      this.conversationsService.loadMoreConversations(true);
    } catch (e) {
      debug.error('ERROR in removePage', e);
      this.toast.error(`Xoá fanpage không thành công. ${e.code ? e.message || e.msg : ''}`);
    }
  }

  trackPage(index, page: FbPage) {
    return page.id;
  }
}
