import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAndUpdateTagComponent } from './create-and-update-tag.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [CreateAndUpdateTagComponent],
  imports: [CommonModule, FormsModule, IonicModule]
})
export class CreateAndUpdateTagModule {}
