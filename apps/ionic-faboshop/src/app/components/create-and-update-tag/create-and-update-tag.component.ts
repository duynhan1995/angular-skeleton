import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { FbUserTag } from '@etop/models';

@Component({
  selector: 'ionfabo-create-and-update-tag',
  templateUrl: './create-and-update-tag.component.html',
  styleUrls: ['./create-and-update-tag.component.scss']
})
export class CreateAndUpdateTagComponent implements OnInit {
  @Input() tag = new FbUserTag({});
  colors = [
    {
      value: '#2ecc71',
      isChecked: true
    },
    {
      value: '#3498db',
      isChecked: false
    },
    {
      value: '#9b59b6',
      isChecked: false
    },
    {
      value: '#f39c12',
      isChecked: false
    },
    {
      value: '#e74c3c',
      isChecked: false
    },
    {
      value: '#95a5a6',
      isChecked: false
    }
  ];

  currentTag: FbUserTag;

  constructor(
    private modalCtrl: ModalController,
    private toast: ToastService,
    private fbUserTagService: FacebookUserTagService
  ) {}

  ngOnInit(): void {}

  ionViewWillEnter() {
    if (this.tag.id) {
      this.colors.map(color => {
        color.isChecked = false
      });
      const index = this.colors.findIndex(color => color.value === this.tag.color);
      if (index >= 0) {
        this.colors[index].isChecked = true;
      }
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  changeColor(index) {
    this.colors.map(color => {
      color.isChecked = false;
    });
    this.colors[index].isChecked = true;
  }

  async confirm() {
    try {
      if (!this.tag.name) {
        return this.toast.error('Vui lòng nhập tên thẻ');
      }
      const _currentTag = this.colors.find(color => color.isChecked == true);
      this.tag.color = _currentTag.value;
      if (this.tag?.id) {
        this.fbUserTagService.updateTag(this.tag);
        this.toast.success('Cập nhật thẻ thành công');
      } else {
        await this.fbUserTagService.createTag(this.tag);
        this.toast.success('Thêm thẻ thành công');
      }
      this.modalCtrl.dismiss();
    } catch (e) {
      this.toast.error(e.message);
    }
  }
}
