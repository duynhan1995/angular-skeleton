import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmptyPermissionComponent } from './empty-permission.component';
import { MatIconModule } from '@angular/material/icon';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [EmptyPermissionComponent],
  imports: [
    CommonModule,
    MatIconModule,
    IonicModule
  ],
  exports: [EmptyPermissionComponent]
})
export class EmptyPermissionModule { }
