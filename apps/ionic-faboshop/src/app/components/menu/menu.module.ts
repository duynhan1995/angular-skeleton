import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from './menu.component';
import { Market } from '@ionic-native/market/ngx';
import { AuthenticateModule } from '@etop/core';



@NgModule({
  declarations: [MenuComponent],
  entryComponents: [],
  providers: [Market],
  imports: [CommonModule, FormsModule, IonicModule, AuthenticateModule],
  exports: [MenuComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class MenuModule {}
