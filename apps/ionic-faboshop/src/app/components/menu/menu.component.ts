import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { NavController, MenuController, Platform } from '@ionic/angular';
import { Market } from '@ionic-native/market/ngx';
import { Plugins, NetworkStatus } from '@capacitor/core';
import { Router } from '@angular/router';
import { ToastService } from '../../services/toast.service';
import { NetworkService } from '../../services/network.service';
const { Device, App, Network } = Plugins;

@Component({
  selector: 'etop-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  menus = [
    {
      icon: 'icon_dashboard.png',
      title: 'Tổng quan',
      link: `dashboard`,
      permissions: ['shop/dashboard:view']
    },
    {
      icon: 'icon_product.png',
      title: 'Sản phẩm',
      link: `products`,
      permissions: ['shop/product/basic_info:view']
    },
    {
      icon: 'icon_pos_banhang.png',
      title: 'Bán hàng',
      link: `pos`,
      css: 'text-primary font-bold',
      permissions: ['shop/order:create']
    },
    // {
    //   icon: 'icon_pos_dathang.png',
    //   title: 'Đặt hàng',
    //   link: `dat-hang`,
    //   css: 'text-dark'
    // },
    {
      icon: 'icon_pos_invoice.png',
      title: 'Quản lý hóa đơn',
      link: `orders`,
      permissions: ['shop/order:view', 'shop/purchase_order:view']
    },
    // {
    //   icon: 'icon_pos_QL_dathang.png',
    //   title: 'Quản lý đặt hàng',
    //   link: `dat-hang`,
    //   css: 'text-dark'
    // },
    {
      icon: 'icon_customer.png',
      title: 'Khách hàng',
      link: `customers`
    },
    {
      icon: 'icon_receipt.png',
      title: 'Thu chi',
      link: `receipts`,
      permissions: ['shop/receipt:view']
    },
    {
      icon: 'icon_inventory.png',
      title: 'Tồn kho',
      link: `inventory`,
      permissions: ['shop/inventory:view']
    }
    // {
    //   icon: 'icon_partner.png',
    //   title: 'Nhân viên',
    //   link: `staff`
    // }
  ];

  menusTwo = [
    // {
    //   icon: 'icon_support.png',
    //   title: 'Hỗ trợ',
    //   link: `supports`
    // },
    // {
    //   icon: 'icon_tut.png',
    //   title: 'Hướng dẫn sử dụng',
    //   link: `guide`
    // }
  ];

  version;

  status: NetworkStatus;
  listener: any;

  constructor(
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private menu: MenuController,
    private market: Market,
    private platform: Platform,
    private router: Router,
    private toast: ToastService,
    private network: NetworkService
  ) {
    this.platform.backButton.subscribe(() => {
      debug.log('backButton', this.router.url);
      if (this.router.url === `${this.linkSlug}/orders`) {
        // tslint:disable-next-line: no-string-literal
        navigator['app'].exitApp();
      }
    });
    this.status = this.network.status;
  }

  get permissionsOfActionsArray() {
    let permissions = [];
    this.menus.forEach(m => {
      if (m.permissions && m.permissions.length) {
        permissions = permissions.concat(m.permissions);
      }
    });
    return permissions;
  }

  get account() {
    return this.auth.snapshot.account;
  }

  get user() {
    return this.auth.snapshot.user;
  }

  get linkSlug() {
    return this.auth.snapshot.account
      ? `/s/${this.auth.snapshot.account.url_slug}`
      : '';
  }

  async ngOnInit() {
    const info = await Device.getInfo();
    this.version = info.appVersion;
  }

  gotoLink(link) {
    this.menu.close();
    this.navCtrl.navigateForward(`${this.linkSlug}/${link}`, {
      animated: false
    });
  }

  gotoProfile() {
    this.menu.close();
    this.navCtrl.navigateForward(`${this.linkSlug}/account`);
  }

  async openAppStore() {
    if (this.platform.is('ios')) {
      const res = await App.canOpenUrl({
        url: 'https://apps.apple.com/us/app/topship-topship-vn/id1453126665'
      });
      if (res.value) {
        await App.openUrl({
          url: 'https://apps.apple.com/us/app/topship-topship-vn/id1453126665'
        });
      } else {
        this.market.open('id1453126665');
      }
    } else {
      const res = await App.canOpenUrl({ url: 'vn.topship.app' });
      if (res.value) {
        await App.openUrl({ url: 'vn.topship.app' });
      } else {
        this.market.open('vn.topship.app');
      }
    }
  }
}
