import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';


@Component({
  selector: 'ionfabo-search-suggest',
  templateUrl: './search-suggest.component.html',
  styleUrls: ['./search-suggest.component.scss']
})
export class SearchSuggestComponent implements OnInit {
  @ViewChild(IonSearchbar) myInput: IonSearchbar;
  @Input() options: any;
  @Input() title: any;
  selectOptions: any;
  constructor(
    private modalCtrl: ModalController,
    private util: UtilService
  ) {}

  ngOnInit(): void {
    this.selectOptions = [...this.options];
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.myInput.setFocus();
    }, 150);
  }

  clearSearch(e) {
    this.modalCtrl.dismiss();
  }

  changeSearch(event) {
    this.selectOptions = this.options.filter(option => this.util.removeDiacritic(option.name.toLowerCase()).includes(this.util.removeDiacritic(event.target.value.toLowerCase())))
  }

  selectOption(option) {
    this.modalCtrl.dismiss(option);
  }
}
