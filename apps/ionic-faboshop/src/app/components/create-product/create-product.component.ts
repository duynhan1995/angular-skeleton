import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController, ToastController, IonInput, Platform } from '@ionic/angular';
import { Product, ShopProduct, Variant, InventoryVariant } from 'libs/models/Product';
import { ProductService } from 'apps/shop/src/services/product.service';
import { ProductApi, StocktakeApi, InventoryApi } from '@etop/api';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { StocktakeLine } from 'libs/models/Stocktake';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { Plugins, CameraResultType } from '@capacitor/core';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { ImageCompressor } from '@etop/utils/image-compressor/image-compressor.service';
import { ImageCompress } from '@etop/utils/image-compressor/image-compressor';

const { Camera } = Plugins;

declare var dataLayer: any;

@Component({
  selector: 'etop-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
  @ViewChild('discountInput', { static: false }) discountInput: IonInput;
  @ViewChild('costPriceInput', { static: false }) costPriceInput: IonInput;
  @Input() barcode: string;
  product = new Product({});
  inventory_quantity: number;
  format_money = true;
  format_cost_price = false;
  toggle = false;
  productImg = '';
  imageElement: any;
  uploading = false;
  costPriceEdited = false;

  constructor(
    private modalCtrl: ModalController,
    private productService: ProductService,
    private productApi: ProductApi,
    private stocktakeApi: StocktakeApi,
    private inventoryApi: InventoryApi,
    private promiseQueue: PromiseQueueService,
    private toastController: ToastController,
    private barcodeScanner: BarcodeScanner,
    private toastService: ToastService,
    private uploader: MobileUploader,
    private imageCompressor: ImageCompressor,
  ) {}

  ngOnInit() {
    this.product.code = this.barcode;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async takePicture() {
    try {
      const image = await Camera.getPhoto({
        quality: 100,
        allowEditing: false,
        resultType: CameraResultType.DataUrl
      });
      const limitSize = 1024 * 1024;
      const decode = await ImageCompress.decodeImage(image.dataUrl);
      const scaleRatio = (limitSize * 100) / (decode.height * decode.width);
      const compressedDataUrl = await this.imageCompressor.compressFile(
        image.dataUrl,
        decode.orientation,
        scaleRatio,
        100
      );
      this.uploading = true;
      const result = await this.uploader.uploadBase64Image(
        compressedDataUrl.split(',')[1]
      );
      this.productImg = result.result[0].url;
      this.uploading = false;
    } catch (e) {
      this.uploading = false;
      debug.log('Error in takePicture', e);
    }
  }

  formatMoney() {
    this.format_money = !this.format_money;
    setTimeout(_ => {
      if (!this.format_money) {
        this.discountInput.setFocus();
      }
    }, 200);
  }

  toggleDetail() {
    this.toggle = !this.toggle;
  }

  scanBarcode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        debug.log('Barcode data', barcodeData);
        this.product.code = barcodeData.text;
      })
      .catch(err => {
        debug.error('ERROR in scanning barcode', err);
      });
  }

  async sendToast(message, color) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color,
      mode: 'md',
      position: 'top',
      cssClass: 'font-12'
    });
    toast.present();
  }

  formatMoneyCostPrice() {
    this.format_cost_price = !this.format_cost_price;
    setTimeout(_ => {
      if (!this.format_cost_price) {
        this.costPriceInput.setFocus();
      }
    }, 200);
  }

  onCostPriceEdited() {
    this.costPriceEdited = true;
  }

  async create() {
    if (!this.product.name) {
      return this.sendToast('Chưa nhập tên sản phẩm!', 'danger');
    }
    if (
      !this.product.retail_price &&
      (!this.product.variants || !this.product.variants.length)
    ) {
      return this.sendToast('Chưa nhập giá bán sản phẩm!', 'danger');
    }
    try {
      const { image, description, name, code } = this.product;

      const body = new Product({
        image_urls: [image],
        name,
        code,
        description
      });

      let product = await this.productService.createProduct(body);
      if (this.productImg) {
        await this.updateProductImage({
          id: product.id,
          replace_all: [this.productImg]
        });
      }
      await this.createVariants(product);
      product = await this.productApi.getProduct(product.id);
      this.sendToast('Tạo sản phẩm thành công!', 'success');
      this.modalCtrl.dismiss({ product });
    } catch (e) {
      debug.error('ERROR in creating product', e);
      const msg =
        (e.code == 'failed_precondition' && e.message) ||
        'Tạo sản phẩm thất bại';
      this.toastService.error(msg);
    }
  }

  async updateProductImage(data) {
    try {
      await this.productApi.updateProductImages(data);
    } catch (e) {
      debug.error('ERROR in updateProductImages', e);
    }
  }

  async createVariants(product: Product | ShopProduct) {
    try {
      const variants: Variant[] = [];
      const body: any = {
        product_id: product.id,
        name: product.name,
        retail_price: this.product.retail_price,
        list_price: this.product.retail_price,
        image_urls: product.image_urls,
        is_available: true,
        code: product.code
      };
      const res = await this.productService.createVariant(body);
      variants.push({
        ...res,
        inventory_variant: {
          ...new InventoryVariant(),
          quantity: this.inventory_quantity,
          cost_price: this.product.cost_price
        }
      });

      if (variants.length) {
        await this.updateInventoryVariantCostPrice(variants);
        await this.createStocktake(variants);
      }
    } catch (e) {
      debug.error('ERROR in creating variants', e);
      throw e;
    }
  }

  async updateInventoryVariantCostPrice(variants: Variant[]) {
    try {
      const promises = variants
        .filter(v => v.inventory_variant.cost_price >= 0)
        .map(v => async () => {
          try {
            await this.inventoryApi.updateInventoryVariantCostPrice(
              v.id,
              v.inventory_variant.cost_price
            );
          } catch (e) {
            debug.error(
              'ERROR in updating InventoryVariantCostPrice inside Promise.all',
              e
            );
          }
        });
      await this.promiseQueue.run(promises, 5);
    } catch (e) {
      debug.error('ERROR in updating InventoryVariantCostPrice', e);
    }
  }

  async createStocktake(variants: Variant[]) {
    try {
      let lines: StocktakeLine[] = variants
        .filter(v => v.inventory_variant.quantity >= 0)
        .map(v => {
          return {
            ...new StocktakeLine(),
            old_quantity: 0,
            new_quantity: v.inventory_variant.quantity,
            variant_id: v.id,
            image_url: v.image_urls[0]
          };
        });
      if (!lines.length) {
        return;
      }
      const body = {
        total_quantity: lines.reduce(
          (a, b) => a + Number(b.new_quantity || 0),
          0
        ),
        lines,
        type: 'balance'
      };
      const stocktake = await this.stocktakeApi.createStocktake(body);
      await this.stocktakeApi.confirmStocktake(stocktake.id, 'confirm');
      this.sendToast('Tạo phiếu kiểm kho thành công', 'success');
    } catch (e) {
      this.sendToast('Tạo phiếu kiểm kho không thành công.' + e, 'success');
      debug.error('ERROR in creating stocktake', e);
    }
  }
}
