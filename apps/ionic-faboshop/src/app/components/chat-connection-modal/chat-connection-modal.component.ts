import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { FbPage } from 'libs/models/faboshop/FbPage';

@Component({
  selector: 'etop-chat-connection-modal',
  templateUrl: './chat-connection-modal.component.html',
  styleUrls: ['./chat-connection-modal.component.scss']
})
export class ChatConnectionModalComponent implements OnInit {
  @Input() connectionResult: {
    fb_user: any;
    fb_pages: FbPage[];
    fb_error_pages: FbPage[];
  };

  constructor(
    private popoverController: PopoverController
  ) {}

  ngOnInit() {}

  dismiss() {
    this.popoverController.dismiss()
  }
}
