import { Component, OnInit, Provider, NgZone, ChangeDetectorRef } from '@angular/core';
import { ModalController, ActionSheetController, AlertController, NavController } from '@ionic/angular';
import {Order, ORDER_STATUS} from 'libs/models/Order';
import { ToastService } from '../../../services/toast.service';
import { OrderApi } from '@etop/api';
import { Fulfillment } from 'libs/models/Fulfillment';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { OrderQuery, OrdersService } from '@etop/state/order';

@Component({
  selector: 'etop-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
  order = new Order({});
  showList = true;
  paymentDisplay = true;
  fulfillment: Fulfillment;

  constructor(
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private toast: ToastService,
    private orderApi: OrderApi,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private util: UtilService,
    private zone: NgZone,
    private orderQuery: OrderQuery,
    private orderService: OrdersService,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    await this.resetData();
  }

  async resetData() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.order = this.orderQuery.getEntity(id);
    if (!this.order) {
      this.order = await this.orderService.getOrder(id);
    }
    this.zone.run(async () => {
      if (this.order.fulfillments.length > 0) {
        this.fulfillment = this.order.fulfillments[0];
        this.orderService.updateInsuranceValue(this.fulfillment);
        this.fulfillment = {
          ...this.fulfillment,
          shipping_state_display: this.util.fulfillmentShippingStateMap(
            this.fulfillment.shipping_state
          ),
          shipping_provider_logo: this.orderService.getProviderLogo(
            this.fulfillment.carrier
          )
        };
        this.changeDetector.detectChanges();
      }
    })
  }

  dismiss() {
    this.navCtrl.back();
  }

  toggleList() {
    this.showList = !this.showList;
  }

  togglePayment() {
    this.paymentDisplay = !this.paymentDisplay;
  }

  get isPaymentDone() {
    // TODO: temporarily doing this!!! wait for VALID order.payment_status
    return this.order.received_amount >= this.order.total_amount;
  }

  statusMap(status) {
    return ORDER_STATUS[status] || 'Không xác định';
  }
  async editOrder(order) {
    if (!this.enableEdit(order.fulfillments[0])) {
      return this.toast.error(
        `Không thể chỉnh sửa đơn hàng. Đơn đang ở trạng thái ${order.fulfillments[0].shipping_state_display}`
      );
    }
    this.orderService.setActiveOrder(order);
    const actionSheet = await this.actionSheetController.create({
      header: 'Thay đổi thông tin đơn hàng',
      cssClass: 'action-sheet-left',
      buttons: [
        {
          text: 'Thông tin người gửi',
          icon: 'person-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/pickup-info`
              );
            });
          }
        },
        {
          text: 'Thông tin người nhận',
          icon: 'person-outline',
          handler: async () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/shipping-info`
              );
            });
          }
        },
        {
          text: 'Tiền thu hộ (COD)',
          icon: 'wallet-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/cod`
              );
            });
          }
        },
        {
          text: 'Khai báo giá trị hàng hóa',
          icon: 'wallet-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/insurance`
              );
            });
          }
        },
        {
          text: 'Khối lượng',
          icon: 'cube-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/gross-weight`
              );
            });
          }
        },
        {
          text: 'Ghi chú',
          icon: 'chatbox-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/shipping-note`
              );
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel Clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Đơn hàng',
      buttons: [
        {
          text: 'Chỉnh sửa đơn hàng',
          handler: () => {
            this.editOrder(this.order);
          }
        },
        {
          text: 'Hủy đơn hàng',
          role: 'destructive',
          handler: () => {
            this.ressonCancelSheet();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel Clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async ressonCancelSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Vui lòng chọn lý do hủy',
      cssClass: 'action-sheet-left',
      buttons: [
        {
          text: 'Sai thông tin',
          handler: () => {
            this.cancelOrder('Sai thông tin');
          }
        },
        {
          text: 'Khách yêu cầu hủy',
          handler: () => {
            this.cancelOrder('Khách yêu cầu hủy');
          }
        },
        {
          text: 'Tư vấn sai sản phẩm',
          handler: () => {
            this.cancelOrder('Tư vấn sai sản phẩm');
          }
        },
        {
          text: 'Hết hàng',
          handler: () => {
            this.cancelOrder('Hết hàng');
          }
        },
        {
          text: 'Lý do khác',
          handler: () => {
            this.otherReason();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async otherReason() {
    const alert = await this.alertController.create({
      message: `Nhập lý do bạn muốn hủy đơn hàng.`,
      inputs: [
        {
          name: 'other_reason',
          type: 'text',
          placeholder: 'Lý do hủy đơn'
        }
      ],
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {}
        },
        {
          text: 'Xác nhận',
          cssClass: 'text-primary font-12',
          handler: async alertData => {
            this.cancelOrder(alertData.other_reason);
          }
        }
      ]
    });

    await alert.present();
  }

  async cancelOrder(reason) {
    try {
      if (this.order.confirm_status != 'N') {
        const res = await this.orderApi.cancelOrder(
          this.order.id,
          reason,
          'confirm'
        );
        if (
          res.fulfillment_errors &&
          res.fulfillment_errors[0] &&
          res.fulfillment_errors[0].msg
        ) {
          throw new Error(res.fulfillment_errors[0].msg);
        }
      }
      // if (this.fulfillment?.shipping_code && this.fulfillment.shipping_state != 'cancelled') {
      //   await this.ffmApi.cancelShipmentFulfillment(this.fulfillment.id, reason);
      // }
      if (
        this.order.confirm_status == 'N' &&
        this.fulfillment.shipping_state == 'cancelled'
      ) {
        return this.toast.error(
          'Đơn đã ở trạng thái hủy. Vui lòng kiểm tra lại!'
        );
      }
      this.zone.run(async () => {
        this.order = await this.orderService.getOrder(this.order.id);
        if (this.order.fulfillments.length > 0) {
          this.fulfillment = this.order.fulfillments[0];
          this.fulfillment.shipping_state_display = this.util.fulfillmentShippingStateMap(
            this.fulfillment.shipping_state
          );
        }
      });
      this.toast.success('Hủy đơn hàng thành công!');
      await this.orderService.getOrders(0, 200, [], true);
    } catch (e) {
      debug.log('Error when cancel order', e);
      this.toast.error('Có lỗi khi hủy đơn hàng. ' + e.message);
    }
  }

  enableEdit(fulfillment: Fulfillment) {
    if (!fulfillment?.shipping_code) {
      return false;
    }
    if (fulfillment) {
      return ['Z', 'S'].includes(fulfillment.status);
    } else {
      return false;
    }
  }
}
