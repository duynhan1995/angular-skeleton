import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { OrdersComponent } from './orders.component';
import { TabsModule } from '../../components/tabs/tabs.module';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { UpdatePickupInfoComponent } from './components/update-pickup-info/update-pickup-info.component';
import { UpdateShippingInfoComponent } from './components/update-shipping-info/update-shipping-info.component';
import { UpdateCodComponent } from './components/update-cod/update-cod.component';
import { UpdateGrossWeightComponent } from './components/update-gross-weight/update-gross-weight.component';
import { UpdateShippingNoteComponent } from './components/update-shipping-note/update-shipping-note.component';
import { ConfirmShippingInfoComponent } from './components/modals/confirm-shipping-info/confirm-shipping-info.component';
import { ConfirmPickupInfoComponent } from './components/modals/confirm-pickup-info/confirm-pickup-info.component';
import { ConfirmCodComponent } from './components/modals/confirm-cod/confirm-cod.component';
import { ConfirmShippingNoteComponent } from './components/modals/confirm-shipping-note/confirm-shipping-note.component';
import { ConfirmGrossWeightComponent } from './components/modals/confirm-gross-weight/confirm-gross-weight.component';
import { IonInputFormatNumberModule } from '@etop/shared/components/etop-ionic/ion-input-format-number/ion-input-format-number.module';
import { UpdateInsuranceComponent } from './components/update-insurance/update-insurance.component';
import { ConfirmInsuranceComponent } from './components/modals/confirm-insurance/confirm-insurance.component';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'shipping-info', component: UpdateShippingInfoComponent },
      { path: 'pickup-info', component: UpdatePickupInfoComponent },
      { path: 'cod', component: UpdateCodComponent },
      { path: 'gross-weight', component: UpdateGrossWeightComponent },
      { path: 'shipping-note', component: UpdateShippingNoteComponent },
      { path: 'insurance', component: UpdateInsuranceComponent },
      { path: ':id', component: OrderDetailComponent }
    ]
  }
];

const components = [
  OrdersComponent,
  OrderDetailComponent,
  UpdatePickupInfoComponent,
  UpdateShippingInfoComponent,
  UpdateCodComponent,
  UpdateGrossWeightComponent,
  UpdateShippingNoteComponent,
  UpdateInsuranceComponent
];

@NgModule({
  declarations: [
    ...components,
    ConfirmShippingInfoComponent,
    ConfirmPickupInfoComponent,
    ConfirmCodComponent,
    ConfirmShippingNoteComponent,
    ConfirmGrossWeightComponent,
    ConfirmInsuranceComponent
  ],
  entryComponents: [],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    NetworkStatusModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    IonInputFormatNumberModule,
    SelectSuggestModule
  ],
  providers: [],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule {}
