import { Component, OnInit } from '@angular/core';
import { Order } from 'libs/models/Order';
import { NavController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfirmInsuranceComponent } from '../modals/confirm-insurance/confirm-insurance.component';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { OrderQuery } from '@etop/state/order';

@Component({
  selector: 'ionfabo-update-insurance',
  templateUrl: './update-insurance.component.html',
  styleUrls: ['./update-insurance.component.scss']
})
export class UpdateInsuranceComponent implements OnInit {
  order: Order;
  formInsurance = this.fb.group({
    insurance_value: ['', Validators.required],
    include_insurance: true
  });
  _preData;
  constructor(
    private navCtrl: NavController,
    private orderQuery: OrderQuery,
    private fb: FormBuilder,
    private routerOutlet: IonRouterOutlet,
    private modalController: ModalController,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    // this.formInsurance.valueChanges.subscribe(v => {
    //   if (!v.insurance_value) {
    //     this.formInsurance.setValue(this._preData, {emitEvent: false})
    //   } else {
    //     this._preData = v;
    //   }
    // })
  }

  async ionViewWillEnter() {
    this.order = this.orderQuery.getActive();
    // tslint:disable-next-line: max-line-length
    this.formInsurance.patchValue({
      insurance_value: this.order.activeFulfillment?.insurance_value,
      include_insurance: true
    });
  }

  back() {
    this.navCtrl.back();
  }

  async submit() {
    const insurance_value = this.formInsurance.controls.insurance_value.value;
    if (Number(insurance_value) < 0 || insurance_value == '') {
      return this.toast.error('Vui lòng nhập giá trị khai báo hàng hóa hợp lệ');
    }
    if (insurance_value == this.order.fulfillments[0].insurance_value) {
      return this.toast.error('Vui lòng thay đổi giá trị khai báo hàng hóa');
    }
    const modal = await this.modalController.create({
      component: ConfirmInsuranceComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        fulfillment: this.order.fulfillments[0],
        insurance_value: this.formInsurance.controls['insurance_value'].value
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.navCtrl.back();
      }
    });
    return await modal.present();
  }
}
