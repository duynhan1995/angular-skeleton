import { Component, OnInit } from '@angular/core';
import { NavController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { OrderStore } from '../../orders.store';
import { FormBuilder, Validators } from '@angular/forms';
import { Order } from 'libs/models/Order';
import { ConfirmGrossWeightComponent } from '../modals/confirm-gross-weight/confirm-gross-weight.component';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { OrderQuery } from '@etop/state/order';

@Component({
  selector: 'ionfabo-update-gross-weight',
  templateUrl: './update-gross-weight.component.html',
  styleUrls: ['./update-gross-weight.component.scss']
})
export class UpdateGrossWeightComponent implements OnInit {
  order: Order;
  formWeight = this.fb.group({
    gross_weight: ['', Validators.required]
  });
  _preData;
  constructor(
    private navCtrl: NavController,
    private orderQuery: OrderQuery,
    private fb: FormBuilder,
    private routerOutlet: IonRouterOutlet,
    private modalController: ModalController,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    // this.formWeight.valueChanges.subscribe(v => {
    //   if (!v.gross_weight) {
    //     this.formWeight.setValue(this._preData, {emitEvent: true})
    //   } else {
    //     this._preData = v;
    //   }
    // })
  }

  async ionViewWillEnter() {
    this.order = this.orderQuery.getActive();
    // tslint:disable-next-line: max-line-length
    this.formWeight.setValue({
      gross_weight: this.order.fulfillments[0].total_weight
    });
  }

  back() {
    this.navCtrl.back();
  }

  async submit() {
    const weight = this.formWeight.controls.gross_weight.value;
    if (Number(weight) <= 0 || weight == '') {
      return this.toast.error('Khối lượng không hợp lệ');
    }
    if (this.order.fulfillments[0].total_weight != this.formWeight.controls['gross_weight'].value) {
      const modal = await this.modalController.create({
        component: ConfirmGrossWeightComponent,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          fulfillment: this.order.fulfillments[0],
          total_weight: this.formWeight.controls['gross_weight'].value
        }
      });
      modal.onWillDismiss().then(async data => {
        if (data?.data) {
          this.navCtrl.back();
        }
      });
      return await modal.present();
    } else {
      this.toast.error('Vui lòng thay đổi giá trị');
    }
  }
}
