import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ShippingService } from 'apps/ionic-faboshop/src/app/services/shipping.service';
import { FulfillmentApi } from '@etop/api/shop';
import { LoadingService } from 'apps/ionic-faboshop/src/app/services/loading.service';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { OrdersService } from '@etop/state/order';

@Component({
  selector: 'ionfabo-confirm-gross-weight',
  templateUrl: './confirm-gross-weight.component.html',
  styleUrls: ['./confirm-gross-weight.component.scss']
})
export class ConfirmGrossWeightComponent implements OnInit {
  @Input() fulfillment;
  @Input() total_weight;
  shipping_fee;
  loading = true;

  constructor(
    private modalCtrl: ModalController,
    private shippingService: ShippingService,
    private orderService: OrdersService,
    private loadingService: LoadingService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    let data: any = this.shippingService.parseFulfillmentData(this.fulfillment);
    data = Object.assign({}, data, {
      chargeable_weight: this.total_weight
    });
    this.loading = true;
    const services = await this.shippingService.getShippingServices(data);
    const service = this.shippingService.shippingServiceMap(
      services.services,
      this.fulfillment
    );
    this.shipping_fee = service.length ? service[0].fee : null;
    this.loading = false;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async confirm() {
    try {
      await this.loadingService.start('Đang xử lý');
      await this.orderService.updateGrossWeight(this.fulfillment, this.total_weight);
      const order = await this.orderService.getOrder(this.fulfillment.order_id);
      this.orderService.updateOrderInfo(order);
      this.toast.success('Thay đổi thông tin thành công.');
      this.loadingService.end();
      this.modalCtrl.dismiss(true);
    } catch (e) {
      this.loadingService.end();
      this.toast.error(e.message || e.msg);
      debug.log('ERROR in updateFulfillmentInfo', e);
    }
  }
}
