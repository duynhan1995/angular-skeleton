import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { OrderStore } from '../../orders.store';
import { Order } from 'libs/models/Order';
import { ConfirmShippingNoteComponent } from '../modals/confirm-shipping-note/confirm-shipping-note.component';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { OrderQuery } from '@etop/state/order';

@Component({
  selector: 'ionfabo-update-shipping-note',
  templateUrl: './update-shipping-note.component.html',
  styleUrls: ['./update-shipping-note.component.scss']
})
export class UpdateShippingNoteComponent implements OnInit {
  tryOnOptions = [
    {
      code: 'try',
      value: 'try',
      name: 'Cho thử hàng'
    },
    {
      code: 'open',
      value: 'open',
      name: 'Cho xem hàng không thử'
    },
    {
      code: 'none',
      value: 'none',
      name: 'Không cho xem hàng'
    }
  ];

  noteForm = this.fb.group({
    try_on: '',
    shipping_note: ''
  });

  order: Order;

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private orderQuery: OrderQuery,
    private routerOutlet: IonRouterOutlet,
    private modalController: ModalController,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  back() {
    this.navCtrl.back();
  }

  async ionViewWillEnter() {
    this.order = this.orderQuery.getActive();
    console.log(this.order)
    // tslint:disable-next-line: max-line-length
    const { try_on, shipping_note } = this.order.fulfillments[0];
    this.noteForm.setValue({
      try_on,
      shipping_note
    });
  }

  async submit() {
    const _checkDifferent = this.checkDifferent(
      this.order.fulfillments[0],
      this.noteForm.value
    );
    if (_checkDifferent) {
      const modal = await this.modalController.create({
        component: ConfirmShippingNoteComponent,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          fulfillment: this.order.fulfillments[0],
          note: this.noteForm.value
        }
      });
      modal.onWillDismiss().then(async data => {
        if (data?.data) {
          this.navCtrl.back();
        }
      });
      return await modal.present();
    } else {
      return this.toast.error('Vui lòng thay đổi thông tin');
    }
  }

  checkDifferent(oldData, newData) {
    return (
      oldData.shipping_note != newData.shipping_note ||
      oldData.try_on != newData.try_on
    );
  }
}
