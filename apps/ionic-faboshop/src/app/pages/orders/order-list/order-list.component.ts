import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import {Order, ORDER_STATUS} from 'libs/models/Order';
import { NetworkStatus, Plugins } from '@capacitor/core';
import { ActionSheetController, IonRouterOutlet, ModalController, NavController, IonInfiniteScroll } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { OrderApi } from '@etop/api';
import { FilterOperator, FilterOptions } from '@etop/models';
import { FilterModalComponent } from '../../../components/filter-modal/filter-modal.component';
import { Fulfillment } from 'libs/models/Fulfillment';
import { ToastService } from '../../../services/toast.service';
import { OrderQuery, OrdersService } from '@etop/state/order';
import { ConversationsService } from '@etop/state/fabo/conversation';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { Customer } from '@etop/models/Customer';
import { CustomerService } from '@etop/state/fabo/customer';
import { ConfirmOrderService } from '@etop/features';

const { Network } = Plugins;

@Component({
  selector: 'etop-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  orders: Order[] = [];
  loading$ = this.orderQuery.selectLoading();
  status: NetworkStatus;
  viewDisconnect = false;
  networkStatus = true;
  orders$ = this.orderQuery.selectAll();

  offset = 0;
  perpage = 20;

  shippingState = [
    { name: 'Tất cả', value: '' },
    { name: 'Đã tạo', value: 'created' },
    { name: 'Đã xác nhận', value: 'confirmed' },
    { name: 'Đang xử lý', value: 'processing' },
    { name: 'Đang lấy hàng', value: 'picking' },
    { name: 'Chờ giao', value: 'holding' },
    { name: 'Đang giao hàng', value: 'delivering' },
    { name: 'Đang trả hàng', value: 'returning' },
    { name: 'Đã giao hàng', value: 'delivered' },
    { name: 'Đã trả hàng', value: 'returned' },
    { name: 'Không giao được', value: 'undeliverable' },
    { name: 'Hủy', value: 'cancelled' },
    { name: 'Không xác định', value: 'unknown' }
  ];

  filterOptions: FilterOptions = [
    {
      name: 'fulfillment.shipping_code',
      operator: FilterOperator.n,
      value: '',
      type: 'input',
      label: 'Mã đơn giao hàng',
      placeholder: 'Nhập mã đơn giao hàng'
    },
    {
      name: 'customer.name',
      operator: FilterOperator.contains,
      value: '',
      type: 'input',
      label: 'Tên người nhận',
      placeholder: 'Nhập tên người nhận'
    },
    {
      name: 'customer.phone',
      operator: FilterOperator.eq,
      value: '',
      type: 'input',
      label: 'Số điện thoại người nhận',
      placeholder: 'Nhập số điện thoại'
    },
    {
      name: 'fulfillment.shipping_state',
      operator: FilterOperator.n,
      value: '',
      type: 'select',
      label: 'Trạng thái đơn giao hàng',
      placeholder: 'Chọn trạng thái đơn',
      options: this.shippingState
    }
    // {
    //   name: 'chargeable_weight',
    //   operator: FilterOperator.gt,
    //   value: {
    //     lower: 0,
    //     upper: 50000
    //   },
    //   type: 'ion-range',
    //   meta: {
    //     unit: 'g',
    //     min: 0,
    //     max: 50000,
    //     step: 100
    //   },
    //   label: 'Khối lượng'
    // }
  ];

  filters = [];
  constructor(
    private conversationsService: ConversationsService,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private zone: NgZone,
    private util: UtilService,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private orderService: OrdersService,
    private orderQuery: OrderQuery,
    private actionSheetController: ActionSheetController,
    private toast: ToastService,
    private confirmOrderController: ConfirmOrderController,
    private customerService: CustomerService,
    private confirmOrderService: ConfirmOrderService
  ) {
    this.viewDisconnect = false;
    this.zone.run(async () => {
      this.status = await this.getStatus();
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.getOrders();
        });
      }
    });
  }

  ngOnInit() {}

  async getStatus() {
    return await Network.getStatus();
  }

  async ionViewWillEnter() {
    if (this.hasPermission()) {
      this.offset = 0;
      this.infiniteScroll.disabled = false;
      await this.getOrders();
    }
  }

  async doRefresh(event) {
    this.infiniteScroll.disabled = false;
    this.offset = 0;
    await this.orderService.getOrders(this.offset, this.perpage, this.filters, true);
    event.target.complete();

  }

  resetFilters() {
    this.infiniteScroll.disabled = false;
    this.offset = 0;
    this.filterOptions = this.filterOptions.map(filterOption => {
      if (filterOption.type == 'ion-range') {
        filterOption.value = {
          upper: filterOption.meta?.max,
          lower: filterOption.meta?.min
        };
      } else {
        filterOption.value = '';
      }
      return filterOption;
    });
  }

  hasPermission() {
    return this.auth.snapshot.permission.permissions.includes('shop/order:view')
  }

  async getOrders(filterOptions?) {
    if (!this.auth.snapshot.permission?.permissions.includes('shop/order:view')) {
      return;
    }
    try {
      // TODO: Map filter
      const filters =
        filterOptions
          ?.filter(fo => !!fo.value)
          .map(filterOption => {
            if (filterOption.type == 'ion-range') {
              const ranges = [];
              const value = filterOption.value;
              const meta = filterOption.meta;
              if (value.lower > meta.min) {
                ranges.push({
                  name: filterOption.name,
                  op: FilterOperator.gte,
                  value: value.lower.toString()
                });
              }
              if (value.upper < meta.max) {
                ranges.push({
                  name: filterOption.name,
                  op: FilterOperator.lte,
                  value: value.upper.toString()
                });
              }
              return ranges;
            } else {
              return [
                {
                  name: filterOption.name,
                  op: filterOption.operator,
                  value: filterOption.value
                }
              ];
            }
          })
          .reduce((a, b) => a.concat(b), []) || [];
      this.filters = filters;
      await this.orderService.getOrders(0, this.perpage, this.filters, true);
    } catch (e) {
      debug.error('ERROR in getting orders', e.message);
    }
  }

  async loadMore(event) {
    debug.log('loadMore');
    this.offset += this.perpage;
    let _orders = await this.orderService.getOrders(this.offset, this.perpage, this.filters);
    if (_orders.length == 0) {
      this.infiniteScroll.disabled = true;
    }
    event.target.complete();
  }
  async orderDetail(order) {
    if (
      !this.auth.snapshot.permission.permissions.includes('shop/order:view')
    ) {
      return;
    }
    if (!this.status.connected) {
      return;
    }
    this.orderService.setActiveOrder(order);
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/orders/${order.id}`
    );
  }

  statusMap(status) {
    return ORDER_STATUS[status] || 'Không xác định';
  }

  createOrder() {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/pos`, {
      animated: false
    });
  }

  confirmOrder() {
    this.confirmOrderController.resetForm();
    this.confirmOrderController.setCustomerForOrder(new Customer({}));
    this.conversationsService.resetActiveFbUser();
    this.customerService.resetActiveCustomer();
    this.confirmOrderService.updateSaveOrder(null);
    this.confirmOrderService.updateSaveFulfillment(null);
    this.zone.run(async() => {
      await this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/confirm-order`, {
        animated: false
      });
    })
  }

  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        filters: JSON.parse(JSON.stringify(this.filterOptions))
      }
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        if (data.data.reset) {
          this.resetFilters();
        } else {
          const filters = data.data.filters;
          this.filterOptions = filters;
        }
        this.getOrders(this.filterOptions);
      }
    });
    return await modal.present();
  }

  async editOrder(order) {
    this.orderService.setActiveOrder(order);
    const actionSheet = await this.actionSheetController.create({
      header: 'Thay đổi thông tin đơn hàng',
      cssClass: 'action-sheet-left',
      buttons: [
        {
          text: 'Thông tin người gửi',
          icon: 'person-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/pickup-info`
              );
            });
          }
        },
        {
          text: 'Thông tin người nhận',
          icon: 'person-outline',
          handler: async () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/shipping-info`
              );
            });
          }
        },
        {
          text: 'Tiền thu hộ (COD)',
          icon: 'wallet-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/cod`
              );
            });
          }
        },
        {
          text: 'Giá trị khai báo hàng hóa',
          icon: 'wallet-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/insurance`
              );
            });
          }
        },
        {
          text: 'Khối lượng',
          icon: 'cube-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/gross-weight`
              );
            });
          }
        },
        {
          text: 'Ghi chú',
          icon: 'chatbox-outline',
          handler: () => {
            this.zone.run(() => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/orders/shipping-note`
              );
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel Clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  enableEdit(fulfillment: Fulfillment) {
    if (!fulfillment?.shipping_code) {
      return false;
    }
    if (fulfillment) {
      return ['Z', 'S'].includes(fulfillment.status);
    } else {
      return false;
    }
  }

  warningEdit(fulfillment: Fulfillment) {
    return this.toast.error(
      `Không thể chỉnh sửa đơn hàng. Đơn đang ở trạng thái ${fulfillment.shipping_state_display}`
    );
  }

  trackOrder(index, order: Order) {
    return order.id;
  }
}
