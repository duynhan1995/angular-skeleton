import { Component, OnInit, Input, NgZone } from '@angular/core';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ModalController, ActionSheetController, NavController } from '@ionic/angular';
import { Customer, CustomerAddress, CustomerCreateData, CustomerEvent, CustomerUpdateData } from 'libs/models/Customer';
import * as moment from 'moment';
import { FaboCustomerService } from '@etop/features/fabo/customer/fabo-customer.service';
import { BaseComponent } from '@etop/core';
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { Router } from '@angular/router';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { EventBus } from '@etop/common';
import { UtilService } from 'apps/core/src/services/util.service';
import { LocationService, LocationQuery } from '@etop/state/location';
import { ConfirmOrderStore } from '@etop/features';

@Component({
  selector: 'etop-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent extends BaseComponent implements OnInit {
  @Input() customerData = new Customer({});
  @Input() addressData = new CustomerAddress({});
  show = true;
  initializing = true;
  address = new CustomerAddress({});
  customer = new Customer({});
  provinces = [];
  districts = [];
  wards = [];
  locationLoaded = false;

  genders = [
    {
      title: 'Nam',
      value: 'male'
    },
    {
      title: 'Nữ',
      value: 'female'
    },
    {
      title: 'Khác',
      value: 'other'
    }
  ];

  constructor(
    private customerService: CustomerService,
    private conversationQuery: ConversationsQuery,
    private actionSheetController: ActionSheetController,
    private toast: ToastService,
    private faboCustomer: FaboCustomerService,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private navCtrl: NavController,
    private eventBus: EventBus,
    private router: Router,
    private util: UtilService,
    private conversationService: ConversationsService,
    private zone: NgZone,
    private confirmOrderStore: ConfirmOrderStore,
  ) {
    super();
  }

  ngOnInit(): void {
    const navigation = this.router.getCurrentNavigation();
    const { customerData, addressData } = navigation.extras.state;
    this.customerData = customerData;
    this.addressData = addressData;
  }

  async ionViewWillEnter() {
    this.initializing = true;
    this.customer = { ...this.customerData } || new Customer({});
    this.address = { ...this.addressData } || new CustomerAddress({});
    this._prepareLocation();
    this.initializing = false;
  }

  async updateCustomer() {
    try {
      this.customer.birthday =
        (this.customer.birthday &&
          moment(this.customer.birthday).format('YYYY-MM-DD')) ||
        '';
      if (!this.customer.full_name) {
        return this.toast.error('Vui lòng nhập tên!');
      }
      if (!this.customer.phone) {
        return this.toast.error('Vui lòng nhập số điện thoại!');
      }

      const { address1, province_code, district_code } = this.address;
      const fullAddress = address1 + province_code + district_code;

      if (fullAddress) {
        if (!this.address.province_code) {
          return this.toast.error('Vui lòng chọn tỉnh thành');
        }
        if (!this.address.district_code) {
          return this.toast.error('Vui lòng chọn quận huyện');
        }
        if (!this.address.address1) {
          return this.toast.error('Vui lòng nhập địa chỉ');
        }
      }
      const res = await this.faboCustomer.createAndUpdate(
        this.customer,
        this.address
      );

      if (this.customer.id) {
        this.eventBus.message<CustomerUpdateData>(CustomerEvent.UPDATE).next({
          conversation: this.conversationQuery.getActive()
        });
      } else {
        this.eventBus.message<CustomerCreateData>(CustomerEvent.CREATE).next({
          conversation: this.conversationQuery.getActive(),
          customer: res._customer
        });
      }
      this.navCtrl.back();
      this.toast.success('Cập nhật khách hàng thành công');
    } catch (e) {
      this.toast.error(
        `Cập nhật thông tin khách hàng không thành công!\n${e.message}`
      );
    }
  }

  toggleDetail() {
    this.show = !this.show;
  }

  _prepareLocation() {
    const _provinces = this.locationQuery.getValue().provincesList;
    const _districts = this.locationQuery.getValue().districtsList;
    const _wards = this.locationQuery.getValue().wardsList;
    if (!_provinces || !_provinces.length) {
      return;
    }
    if (!_districts || !_districts.length) {
      return;
    }
    if (!_wards || !_wards.length) {
      return;
    }
    this.provinces = _provinces;
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
    this.locationLoaded = true;
  }

  onProvinceSelected() {
    this.address.district_code = '';
    this.address.ward_code = '';
    this.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(
      this.address.province_code
    );
  }

  onDistrictSelected() {
    this.address.ward_code = '';
    this.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(
      this.address.district_code
    );
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Xóa khách hàng',
          role: 'destructive',
          handler: () => {
            this.deleteCustomer();
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async deleteCustomer() {
    try {
      await this.customerService.deleteCustomer(this.customer.id);
      this.eventBus.message<CustomerUpdateData>(CustomerEvent.DELETE).next({
        conversation: this.conversationQuery.getActive()
      });
      this.conversationService.updateCustomerByFbUser(null, this.conversationQuery.getActive().external_user_id);
      this.confirmOrderStore.setActiveCustomerAddress(null);
      this.zone.run(_ => {
        this.navCtrl.navigateBack(
          `/s/${this.util.getSlug()}/messages/${
            this.conversationQuery.getActive().id
          }`
        );
      })
      this.toast.success('Xóa khách hàng thành công');
    } catch (e) {
      this.toast.error(e.message);
    }
  }
}
