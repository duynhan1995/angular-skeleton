import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { CustomerConversation, FbPage } from '@etop/models';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { FbPagesQuery } from '@etop/state/fabo/page';
import { IonSearchbar, ModalController, NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'ionfabo-search-message',
  templateUrl: './search-message.component.html',
  styleUrls: ['./search-message.component.scss']
})
export class SearchMessageComponent implements OnInit {

  @ViewChild(IonSearchbar) myInput: IonSearchbar;
  conversations: CustomerConversation[];
  searchText = '';

  constructor(
    private modalCtrl: ModalController,
    private fbPagesQuery: FbPagesQuery,
    private conversationsService: ConversationsService,
    private navCtrl: NavController,
    private util: UtilService,
    private conversationsQuery: ConversationsQuery,
    private zone: NgZone,
  ) {}

  ngOnInit(): void {}

  ionViewDidEnter() {
    let searchKey = this.conversationsQuery.getValue().searchKey;
    if (searchKey) {
      this.searchText = searchKey;
      this.myInput.value = searchKey;
      this.conversations = this.conversationsQuery.getValue().searchConversations;
    }
    setTimeout(() => {
      this.myInput.setFocus();
    }, 150);
  }

  clearSearch(e) {
    this.conversationsService.updateSearchConversations([], null);
    this.zone.run(async () => {})
    this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/messages`, { animated: false });
  }

  async changeSearch(e) {
    this.searchText = e.target.value;
     const res = await this.conversationsService.searchCustomerConversations(e.target.value);
    setTimeout(() => {
      this.conversations = res.map(conv => {
        return {
          ...conv,
          tags: this.conversationsService.mapTags(conv.external_user_tags)
        }
      })
    })
  }

  getFanpageByExternalId(external_id: string): FbPage {
    if (!external_id) {
      return null;
    }
    return this.fbPagesQuery.getAll().find(p => p.external_id == external_id);
  }

  async messageDetail(conv: CustomerConversation) {
    const _conversation = this.conversationsQuery.getAll().find(c => c.external_id == conv.external_id);
    if (!_conversation) {
      this.conversationsService.appendConversations([conv]);
      this.conversationsService.updateSearchConversation(conv);
    }
    this.conversationsService.selectConversation(conv);
    if (!['message', 'comment'].includes(conv.type)) {
      return;
    }
    this.conversationsService.updateSearchConversations(this.conversations, this.searchText);
    this.zone.run(async () => {
      await this.navCtrl.navigateForward(
        `/s/${this.util.getSlug()}/messages/${conv.id}?search=true`
      );
    })
  }

}
