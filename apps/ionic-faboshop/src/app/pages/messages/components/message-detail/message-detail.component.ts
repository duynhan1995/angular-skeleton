import { ChangeDetectorRef, Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController, IonContent, IonTextarea, ModalController, NavController, IonRouterOutlet, IonInfiniteScroll } from '@ionic/angular';
import { ImagePicker, OutputType } from '@ionic-native/image-picker/ngx';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { CustomerConversation, Message } from 'libs/models/faboshop/CustomerConversation';
import { filter, map, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { Plugins } from '@capacitor/core';
import { Customer } from 'libs/models/Customer';
import { UtilService } from 'apps/core/src/services/util.service';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { ActivatedRoute } from '@angular/router';
import { Order } from 'libs/models/Order';
import { Address } from 'libs/models/Address';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { ImageCompress } from '@etop/utils';
import { PageBaseComponent } from '@etop/ionic/core/page-base.component';
import { SystemEventService } from 'apps/ionic-faboshop/src/app/system-event.service';
import { distinctUntilArrayItemChanged } from '@datorama/akita';
import { SelectTagsComponent } from 'apps/ionic-faboshop/src/app/components/select-tags/select-tags.component';
import { CreateAndUpdateTagComponent } from 'apps/ionic-faboshop/src/app/components/create-and-update-tag/create-and-update-tag.component';
import { PrivateReplyComponent } from 'apps/ionic-faboshop/src/app/components/private-reply/private-reply.component';
import { MessageTemplateService } from '@etop/state/fabo/message-template';
import { AuthenticateStore } from '@etop/core';
import { ConfirmOrderService, ConfirmOrderStore } from '@etop/features';

const { Clipboard } = Plugins;

@Component({
  selector: 'etop-message-detail',
  templateUrl: './message-detail.component.html',
  styleUrls: ['./message-detail.component.scss']
})
export class MessageDetailComponent extends PageBaseComponent implements OnInit {
  @ViewChild('ionContent', { static: true }) ionContent: IonContent;
  @ViewChild('ionTextarea', { static: true }) ionTextarea: IonTextarea;
  @ViewChild('ionTextarea2', { static: true }) ionTextarea2: IonTextarea;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('messageList', { static: true }) messageList: ElementRef;
  @ViewChild('ionTextarea', { read: ElementRef }) footerContent: ElementRef;
  count = 1;

  conversation: CustomerConversation;
  conversation$ = this.conversationsQuery.selectActive<CustomerConversation>();
  checkWarningChatBox = false;

  messages$ = this.conversationsQuery.selectActive(c => c.messages).pipe(distinctUntilArrayItemChanged());
  activeConversationPost$ = this.conversationsQuery.selectActive(
    c => c.fb_post
  );
  needScroll$ = this.conversationsQuery.select().pipe(
    map(s => s.needMessageScroll),
    filter(need => need)
  );

  showTools = true;
  textAreaOnFocus = false;
  chatContent = '';
  imageUrl = '';

  paging = {
    after: '.',
    limit: 50,
    sort: '-external_created_time'
  };

  parentDisplay: any = [];
  displayParentById = [];
  previousMessageListScrollHeight = 0;
  displayTemplate = false;
  footerHeight = 0;
  quickTemplateMessage: any;
  currentHeightChat: any;
  private _inputBlurDelay = null;

  constructor(
    private navController: NavController,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef,
    private modalCtrl: ModalController,
    private promiseQueue: PromiseQueueService,
    private conversationsQuery: ConversationsQuery,
    private conversationsService: ConversationsService,
    private imagePicker: ImagePicker,
    private confirmOrderController: ConfirmOrderController,
    private actionSheetController: ActionSheetController,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private zone: NgZone,
    private toast: ToastService,
    public systemEvent: SystemEventService,
    public el: ElementRef,
    private ref: ChangeDetectorRef,
    private routerOutlet: IonRouterOutlet,
    private messageTemplateService: MessageTemplateService,
    private auth: AuthenticateStore,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderStore: ConfirmOrderStore,

  ) {
    super(systemEvent, el);
  }

  isEmptyContent(message: Message) {
    return !message?.message && !message?.medias?.length;
  }

  displayParentComment(message: Message) {
    return !this.isMessageOfPage(message) && message?.o_data.external_parent && message.from.id != message.o_data.external_parent.external_from.id;
  }

  mergeDisplayParentComment(message: Message) {
    const display = this.displayParentComment(message);
    if (this.displayParentById.includes(message.id)) {
      return true;
    }
    if (display && !this.parentDisplay.includes(message?.o_data.external_parent.id)) {
      this.parentDisplay.push(message.o_data.external_parent.id);
      this.displayParentById.push(message.id);
      return true;
    } else {
      return false;
    }
  }

  isMessageOfPage(message: Message) {
    return !message.from || message.external_page_id == message.from.id;
  }

  isVeryNearConversation(item: Message) {
    const messages = this.conversationsQuery.getActive().messages;
    const index = messages.findIndex(m => m.id == item.id);
    const next_item = messages[index + 1];

    if (!item || item.status == 'error' || !next_item) {
      return false;
    }

    if (this.isEmptyContent(item) || this.isEmptyContent(next_item)) {
      return false;
    }

    if (this.isMessageOfPage(item) != this.isMessageOfPage(next_item)) {
      return false;
    }

    const diffTimeInMinute =
      (new Date(next_item.external_created_time).getTime() -
        new Date(item.external_created_time).getTime()) /
      60000;
    // NOTE: 1min = 60s = 60000ms
    return diffTimeInMinute <= 1;
  }

  async ngOnInit() {
    this.messages$.pipe(
      distinctUntilChanged((pre, next) => {
        if (pre && next) {
          return pre[pre.length - 1].id === next[next.length - 1].id
        }
      }),
      takeUntil(this.destroy$)).subscribe(() => {
        setTimeout(_ => {
          this.scrollToBottom(100);
        }, 100);
    })
    this.systemEvent.keyboardDidShow.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.scrollToBottom(200);
    })
  }

  async ionViewDidEnter() {
    super.ionViewWillEnter();
    this.zone.run(_ => {
      this.footerHeight =  this.getHeightBoxChat();
      this.changeDetector.detectChanges();
    })
    this._prepareData();
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    if (!this.conversation || this.conversation.id != id) {
      this.zone.run(async (_) => {
        this.conversation = await this.conversationsService.getCustomerConversationByID(id);
        if (!this.conversation) {
          await this.dismiss();
        }
        await this.conversationsService.selectConversation(this.conversation);
        await this.conversationsService.fetchFbUser(this.conversation);
        await this.infiniteScroll.complete();
      })
    } else {
      this.conversationsService.preloadMessages(this.conversationsQuery.getActive()).then(async _ => {
        const messages = this.conversationsQuery.getActive().messages;
        if (!messages) {
          this.toast.error('Không tìm thấy tin nhắn.')
          this.dismiss();
        }
      })
    }
    await this.messageTemplateService.getMessageTemplate();
    await this.messageTemplateService.getMessageTemplateVariables();
    await this.scrollToBottom(200);
  }

  async ionViewWillLeave() {
    this.el.nativeElement.style.setProperty('background-color', 'transparent');
  }

  async scrollToBottom(duration = 0) {
    await this.ionContent.scrollToBottom(duration);
  }

  async dismiss() {
    const _searchConversation = this.conversationsQuery.getValue().searchConversation;
    if (_searchConversation) {
      this.conversationsService.updateSearchConversation(null);
    }
    const search = this.activatedRoute.snapshot.queryParamMap.get('search');
    if (search) {
      this.zone.run(async _ => {
        await this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/messages/search`, { animated: false });
      })
    } else {
      this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/messages`);
    }
  }

  async _prepareData() {
    this.checkWarningChatBox = false;
    this.conversation = this.conversationsQuery.getActive();
    const now = Date.now();
    const last = new Date(
      this.conversation?.last_customer_message_at
    ).getTime();
    if (now - last > 86400000 && this.conversation?.type == 'message') {
      this.checkWarningChatBox = true;
    }
    if (this.conversation) {
      await this.conversationsService.fetchFbUser(this.conversation);
    }
  }

  async doLoadMore(event) {
    const messages = this.conversationsQuery.getActive().messages;
    if (messages) {
      this.previousMessageListScrollHeight = this.messageList.nativeElement.scrollHeight;
      setTimeout(() => this.conversationsService.loadMoreMessages(this.conversationsQuery.getActive())
        .then(() => setTimeout(() => this.messageList.nativeElement.scrollTo(0, this.messageList.nativeElement.scrollHeight - this.previousMessageListScrollHeight, false)))
        .then(() => event?.target?.complete())
      , 2000);
    } else {
      event?.target?.complete()
    }
  }

  hasViewTemplate() {
    return this.auth.snapshot.permission.permissions.includes('facebook/message_template:view')
  }

  async replyCustomer() {
    this.switchInputFocus().then()
    const inputChats: any = document.getElementsByClassName('box-chat-item');
    for (let i = 0; i < inputChats.length; i++) {
      inputChats[i].style.alignItems = 'center';
    }

    if (!this.chatContent) {
      return;
    }
    try {
      await this.sendMessageWithText();
    } catch (e) {
      debug.error('ERROR in replyCustomer', JSON.stringify(e));
    }
  }

  switchInputFocus() {
    if ((this.count++) % 2) {
      return this.ionTextarea.setFocus();
    } else {
      return this.ionTextarea2.setFocus();
    }
  }

  async takePicture() {
    if (this.textAreaOnFocus) {
      this.ionTextarea.setFocus();
    }
    try {
      const results = await this.imagePicker.getPictures({
        allow_video: false,
        outputType: OutputType.DATA_URL,
        maximumImagesCount: 10
      });

      results.forEach(res => {
        if (res) {
          this.imageUrl = ImageCompress.addMimeIfNeeded(res);
          this.sendMessageWithImage();
        }
      });
    } catch (e) {
      debug.error('ERROR in takePicture', e);
    }
  }

  sendMessageWithText() {
    if (!this.chatContent?.trim()) {
      return;
    }
    this.conversationsService.sendMessageWithText(this.chatContent).catch(e => this.toast.error(e.message));
    this.chatContent = '';
  }

  sendMessageWithImage() {
    this.conversationsService.sendMessageWithImageNative(this.imageUrl);
  }

  retrySendMessage(message: Message) {
    if (message?.medias[0]?.preview_url) {
      return this.conversationsService.sendMessageWithImageWeb(
        this.imageUrl,
        message.id
      );
    }
    this.conversationsService
      .sendMessage(message, this.conversationsQuery.getActive())
      .then();
  }

  onBlur() {
    this._inputBlurDelay = setTimeout(() => {
      this.zone.run(_ => {
        let textarea2 = document.getElementById('message-content2');
        textarea2.style.whiteSpace = 'nowrap';
        let textarea = document.getElementById('message-content');
        textarea.style.whiteSpace = 'nowrap';
        textarea2.getElementsByTagName('textarea')[0].style.height = 'auto';
        textarea2.getElementsByTagName('div')[0].style.height = 'auto';
        textarea.getElementsByTagName('textarea')[0].style.overflow = 'hidden';
        textarea2.getElementsByTagName('textarea')[0].style.overflow = 'hidden';
        textarea.getElementsByTagName('textarea')[0].style.height = 'auto';
        textarea.getElementsByTagName('div')[0].style.height = 'auto';
        this.footerHeight = 56;
        textarea2.getElementsByTagName('textarea')[0].scrollTo({ top: 0 });
        const inputChats: any = document.getElementsByClassName('box-chat-item');
        for (let i = 0; i < inputChats.length; i++) {
            inputChats[i].style.alignItems = 'center';
        }
      })
      this.showTools = true;
      this.textAreaOnFocus = false;
      this.ref.detectChanges();
    }, 50);
  }

  onFocus() {
    clearTimeout(this._inputBlurDelay);
    if(!this.textAreaOnFocus) {
      this.zone.run(_ => {
        let textarea2 = document.getElementById('message-content2');
        textarea2.style.whiteSpace = 'unset';
        let textarea = document.getElementById('message-content');
        textarea.style.whiteSpace = 'unset';
        textarea2.getElementsByTagName('textarea')[0].style.height = this.currentHeightChat + 'px'
        textarea2.getElementsByTagName('div')[0].style.height = this.currentHeightChat + 'px';
        textarea.getElementsByTagName('textarea')[0].style.overflow = 'auto';
        textarea2.getElementsByTagName('textarea')[0].style.overflow = 'auto';
        textarea.getElementsByTagName('textarea')[0].style.height = this.currentHeightChat + 'px'
        textarea.getElementsByTagName('div')[0].style.height = this.currentHeightChat + 'px'
        this.footerHeight = this.getHeightBoxChat();
        if (this.footerHeight > 56) {
          const inputChats: any = document.getElementsByClassName('box-chat-item');
          for (let i = 0; i < inputChats.length; i++) {
              inputChats[i].style.alignItems = 'flex-end';
          }
        }
      })
      this.showTools = false;
      this.textAreaOnFocus = true;
    }
    this.ref.detectChanges();
  }

  textAreaFocus() {
    this.showTools = false;
    this.ionTextarea.setFocus();
    this.ref.detectChanges();
  }

  onShowOtherTools() {
    this.showTools = true;
    if (this.textAreaOnFocus) {
      this.ionTextarea.setFocus();
    }
  }

  onChangeChatContent() {
    const textarea = document.getElementById('message-content');
    textarea.addEventListener('keyup', (e: any) => {
      const start = e.target.selectionStart;
      if (textarea.scrollHeight > 40 && start >= this.chatContent.length) {
        textarea.scrollTo({ top: textarea.scrollHeight });
      }
      this.zone.run(_ => {
        this.footerHeight =  this.getHeightBoxChat();
        this.changeDetector.detectChanges();
      })
    });
    const inputChats: any = document.getElementsByClassName('box-chat-item');
    for (let i = 0; i < inputChats.length; i++) {
      if (textarea.scrollHeight > 40) {
        inputChats[i].style.alignItems = 'flex-end';
      } else {
        inputChats[i].style.alignItems = 'center';
      }
    }
    if (this.chatContent.charAt(0) == '/') {
      this.displayTemplate = true;
      this.mapTemplate();
    } else {
      this.displayTemplate = false;
      this.quickTemplateMessage = []
    }
    this.zone.run(_ => {
      this.footerHeight =  this.getHeightBoxChat();
      this.changeDetector.detectChanges();
    })
  }

  async mapTemplate() {
    this.quickTemplateMessage = this.messageTemplateService.getMessageTemplateWithShortCode(this.chatContent.substring(1));
    this.quickTemplateMessage = await this.messageTemplateService.replaceAllTemplate(this.quickTemplateMessage)
  }

  addToChat(template) {
    this.chatContent = this.messageTemplateService.replaceVariables(template.template)
    this.displayTemplate = false;
    this.switchInputFocus();
    this.zone.run(_ => {
      this.footerHeight =  this.getHeightBoxChat();
      this.changeDetector.detectChanges();
    })
  }

  getHeightBoxChat() {
    const textarea = document.getElementById('message-content2');
    this.currentHeightChat = textarea.scrollHeight;
    return textarea.scrollHeight + 16;
  }

  toSettings() {
    this.displayTemplate = false;
    this.navController.navigateForward(`/s/${this.util.getSlug()}/account/template-message`);
  }

  closeQuickChat() {
    this.switchInputFocus();
    this.displayTemplate = false;
    this.chatContent = '';
  }

  onModeQuickMessage() {
    this.displayTemplate = !this.displayTemplate;
    this.switchInputFocus();
    if (!this.displayTemplate) {
      this.chatContent = '';
      this.quickTemplateMessage = [];
    } else {
      if (this.chatContent) {
        this.chatContent = '/';
      }
      this.mapTemplate();
    }
  }

  async detailCustomer() {
    this.navController.navigateForward(
      `/s/${this.util.getSlug()}/messages/customer/${this.conversation.id}`
    );
  }

  async confirmOrder(state?, field?, content?) {
    this.zone.run(async () => {
      await this.conversationsService.selectConversation(this.conversation);
      const fbUser = this.conversationsQuery.getActive().fbUsers[0];
      if (fbUser?.customer) {
        this.confirmOrderController.setCustomerForOrder(fbUser?.customer);
      } else {
        this.confirmOrderController.setCustomerForOrder(
          new Customer({
            id: null,
            full_name: fbUser?.external_info?.name,
            phone: null,
            external_id: fbUser?.external_id,
            external_info: fbUser?.external_info
          })
        );
      }

      if (state == 'customer') {
        this.confirmOrderController.setCustomerForOrder(
          new Customer({ [field]: content })
        );
      }
      if (state == 'address') {
        this.confirmOrderController.setToAddress(
          new Address({
            address1: content,
            p_data: { copied: true }
          })
        );
      }

      this.navController
        .navigateForward(`/s/${this.util.getSlug()}/confirm-order?conversation=${this.conversation.id}&type=${this.conversation.type}`)
        .then();
    });
  }

  async presentActionSheet(text) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Nội dung: ' + text,
      cssClass: 'action-sheet-left',
      buttons: [
        {
          icon: 'copy-outline',
          text: `Sao chép tin nhắn`,
          handler: () => {
            this.copyToOrder(text);
          }
        },
        {
          icon: 'person-outline',
          text: `Lấy làm tên khách hàng`,
          handler: async () => {
            await this.updateCopyFfm('full_name', 'full_name', text);
          }
        },
        {
          icon: 'call-outline',
          text: `Lấy làm SĐT khách hàng`,
          handler: () => {
            this.updateCopyFfm('phone', 'phone', text);
          }
        },
        {
          icon: 'location-outline',
          text: 'Lấy làm địa chỉ khách hàng',
          handler: () => {
            this.updateCopyFfm('address1',null , text);
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  updateCopyFfm(state?, field?, content?) {
    const saveDataOrder = this.confirmOrderStore.snapshot.saveDataOrder;
    switch(state) { 
      case 'full_name': { 
         this.confirmOrderService.updateSaveOrder({
           ...saveDataOrder,
          customer: {
            ...saveDataOrder?.customer,
            full_name: content
          },
          p_data: {
            ...saveDataOrder?.p_data,
            is_save: true
          }
         })
         this.toast.success('Đã lấy tin nhắn làm tên khách hàng. Xem lại thông tin tại màn hình chốt đơn.');
         break; 
      } 
      case 'phone': { 
        this.confirmOrderService.updateSaveOrder({
          ...saveDataOrder,
         customer: {
           ...saveDataOrder?.customer,
           phone: content
         },
         p_data: {
          ...saveDataOrder?.p_data,
          is_save: true
        }
        })
        this.toast.success('Đã lấy tin nhắn làm số điện thoại khách hàng. Xem lại thông tin tại màn hình chốt đơn.');
        break; 
      }
      case 'address1': { 
        this.confirmOrderService.updateSaveOrder({
          ...saveDataOrder,
         customer: {
           ...saveDataOrder?.customer,
           address1: content
         },
         p_data: {
          ...saveDataOrder?.p_data,
          is_save: true
        }
        })
        this.toast.success('Đã lấy tin nhắn làm địa chỉ khách hàng. Xem lại thông tin tại màn hình chốt đơn.');
        break; 
     } 
      default: { 
         //statements; 
         break; 
      } 
   } 
  }

  async copyToOrder(text) {
    Clipboard.write({ string: text });
    let result = await Clipboard.read();
    if (result?.value) {
      this.toast.success('Đã sao chép tin nhắn')
    }
  }

  callPhone() {
    if (this.textAreaOnFocus) {
      this.ionTextarea.setFocus();
    }
    if (!this.conversation?.customer?.phone) {
      return this.toast.error('Khách hàng chưa có số diện thoại.');
    }
    window.location.href = 'tel:' + this.conversation?.customer?.phone;
  }

  trackMessage(index, message: Message) {
    return message.id;
  }

  async setTag() {
    const modal = await this.modalCtrl.create({
      component: SelectTagsComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        external_user_id: this.conversationsQuery.getActive().external_user_id,
        tag_ids: this.conversationsQuery.getActive().external_user_tags
      }
    });
    modal.onDidDismiss().then(async (data) => {
      if (data.data?.role == 'create') {
        this.zone.run(async () => {
          await this.createTag();
        })
      }
      if (data.data?.role == 'update') {
        this.conversationsService.setTagsConversationsByFbUser(this.conversationsQuery.getActive().external_user_id, data.data.tag_ids);
        this.changeDetector.detectChanges();
      }
    });
    return await modal.present();
  }

  async createTag() {
    const modal = await this.modalCtrl.create({
      component: CreateAndUpdateTagComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {}
    });
    modal.onDidDismiss().then(async data => {
      await this.setTag();
    });
    return await modal.present();
  }

  toggleLikeComment(message: Message) {
    const consversation = this.conversationsQuery.getActive();
    message = {...message,
      is_liked: !message.is_liked
    }
    const likeStatusText = message.is_liked? 'thích' : 'bỏ thích';
    try {
      this.conversationsService.mergeMessages([message],consversation.id);
      this.conversationsService.updateLikeCommentAction(message);
      this.toast.success(`Thực hiện ${likeStatusText} bình luận thành công.`);
    } catch(e) {
      this.toast.error(e.msg || e.message);
    }
  }

  toggleHideComment(message: Message) {
    const consversation = this.conversationsQuery.getActive();
    message = {...message,
      is_hidden: !message.is_hidden
    }
    const hiddenStatusText = !message.is_hidden? 'hiện' : 'ẩn';
    try {
      this.conversationsService.mergeMessages([message],consversation.id);
      this.conversationsService.updateHideCommentAction(message);
      this.toast.success(`Thực hiện ${hiddenStatusText} bình luận thành công.`);
    } catch(e) {
      this.toast.error(e.msg || e.message);
    }
  }

  async sendPrivateReply(message: Message) {
    const modal = await this.modalCtrl.create({
      component: PrivateReplyComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {message}
    });
    return await modal.present();
  }

  readMessage(message) {
    this.conversationsService.selectConversationWithFbUser(message.external_user_id);
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/messages/${this.conversationsQuery.getActiveId()}`
    );
  }

}
