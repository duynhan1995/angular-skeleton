import { Component, OnInit, Provider, NgZone } from '@angular/core';
import { ModalController, NavController, IonRouterOutlet, PopoverController } from '@ionic/angular';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { Order } from 'libs/models/Order';
import { ConfirmOrderService, ConnectionService, ConnectionStore, OrderService } from '@etop/features';
import { UtilService } from 'apps/core/src/services/util.service';
import { CustomerConversation, FbCustomerReturnRate, FilterOperator, Filters } from '@etop/models';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import { SelectTagsComponent } from 'apps/ionic-faboshop/src/app/components/select-tags/select-tags.component';
import { CreateAndUpdateTagComponent } from 'apps/ionic-faboshop/src/app/components/create-and-update-tag/create-and-update-tag.component';
import { RiskCustomerWarningComponent } from 'apps/ionic-faboshop/src/app/components/popovers/risk-customer-warning/risk-customer-warning.component';

@Component({
  selector: 'etop-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent extends BaseComponent implements OnInit {
  address = new CustomerAddress({});
  customer = new Customer({});
  show = true;
  order_show = true;
  customerAddresses: Array<CustomerAddress>;
  address_display = '';
  orders: Order[] = [];

  fbUser$ = this.conversationsQuery.selectActive(
    ({ fbUsers }) => fbUsers && fbUsers[0]
  );
  customer$ = this.conversationsQuery.selectActive(s => s.customer);
  conversation$ = this.conversationsQuery.selectActive<CustomerConversation>();

  customerReturnRating: FbCustomerReturnRate;
  connectionID = '';
  messageError: any = null;

  constructor(
    private customerService: CustomerService,
    private modalCtrl: ModalController,
    private conversationsQuery: ConversationsQuery,
    private conversationsService: ConversationsService,
    private orderService: OrderService,
    private util: UtilService,
    private navCtrl: NavController,
    private confirmOrderController: ConfirmOrderController,
    private activatedRoute: ActivatedRoute,
    private routerOutlet: IonRouterOutlet,
    private zone: NgZone,
    private connectionStore: ConnectionStore,
    private popoverController: PopoverController,
    private confirmOrderService: ConfirmOrderService,
    private connectionService: ConnectionService,
  ) {
    super();
  }

  async ngOnInit() {
    this.conversationsQuery
      .selectActive()
      .pipe(takeUntil(this.destroy$))
      .subscribe(conversation => this._prepareData(conversation));
  }

  async ionViewWillEnter() {
    this.messageError = null;
    await this._prepareData(this.conversationsQuery.getActive());
  }

  async _prepareData(activeConversation: CustomerConversation) {
    const { fbUsers, customer } = activeConversation;
    const activeFbUser = fbUsers[0];
    this.address = customer?.address;
    this.customer.full_name =
      this.customer.full_name ||
      activeFbUser?.external_info?.name ||
      activeConversation.external_user_name;

    if (activeFbUser?.customer) {
      this.customer = JSON.parse(JSON.stringify(activeFbUser.customer));
      if (!this.address) {
        const res = await this.customerService.getCustomerAddresses(
          this.customer.id
        );
        if (res.length) {
          this.address_display = this.mapAddress(res[0]);
        }
      } else {
        this.address_display = this.mapAddress(this.address);
      }
    }
    if (this.customer?.id) {
      const filters: Filters = [
        {
          name: 'customer.id',
          op: FilterOperator.eq,
          value: this.customer.id
        }
      ];
      this.orders = await this.orderService.getOrders(0, 100, filters);
      this.orders.map(order => {
        if (order.fulfillments.length > 0) {
          const ffm = order.fulfillments[0];
          ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
          ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
            ffm.shipping_state
          );
        }
      });
      await this.getCustomerReturnRate(this.customer.phone);
    }
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  async orderDetail(order) {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/orders/${order.id}`
    );
  }

  mapAddress(address) {
    return (
      address.address1 +
      ', ' +
      address.ward +
      ', ' +
      address.district +
      ', ' +
      address.province
    );
  }

  genderMap(gender) {
    switch (gender) {
      case 'male': {
        return 'Nam';
      }
      case 'female': {
        return 'Nữ';
      }
      case 'other': {
        return 'Khác';
      }
      default: {
        return '-';
      }
    }
  }

  dismiss() {
    this.navCtrl.back();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  toggleOrder() {
    this.order_show = !this.order_show;
  }

  confirmOrder() {
    this.confirmOrderController.setCustomerForOrder(this.customer);
    let conversation = this.conversationsQuery.getActive();
    this.navCtrl
      .navigateForward(`/s/${this.util.getSlug()}/confirm-order?conversation=${conversation.id}&type=${conversation.type}`)
      .then();
  }

  async customerEdit() {
    this.navCtrl.navigateForward(['.', 'edit'], {
      relativeTo: this.activatedRoute,
      state: {
        customerData: this.customer,
        addressData: this.address
      }
    });
  }

  async newTag() {
    const modal = await this.modalCtrl.create({
      component: SelectTagsComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        external_user_id: this.conversationsQuery.getActive().external_user_id,
        tag_ids: this.conversationsQuery.getActive().external_user_tags
      }
    });
    modal.onDidDismiss().then(data => {
      if (data.data?.role == 'create') {
        this.zone.run(async () => {
          await this.createTag();
        });
      }
      if (data.data?.role == 'update') {
        const conversation = this.conversationsQuery.getActive();
        this.conversationsService.setTagsConversationsByFbUser(conversation.external_user_id, data.data.tag_ids);
      }
    });
    return await modal.present();
  }

  async createTag() {
    const modal = await this.modalCtrl.create({
      component: CreateAndUpdateTagComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {}
    });
    modal.onDidDismiss().then(async data => {
      await this.newTag();
    });
    return await modal.present();
  }

  async getCustomerReturnRate(phone: any) {
    try {
      if (phone.length > 9) {
        const res = await this.confirmOrderService.customerReturnRate(phone);
        this.customerReturnRating = res?.customer_return_rates[0];
        const level = this.customerReturnRating.customer_return_rate.level;
        const first_letter = (level.toLowerCase()).charAt(0);
        this.customerReturnRating.customer_return_rate.level = (level.toLowerCase()).replace(first_letter, level.charAt(0));
        this.messageError = null;
      }
    } catch (e) {
      debug.log('ERROR in getCustomerReturnRate', e);
      this.messageError = e;
      this.customerReturnRating = null;
    }
  }

  async ratingPopover() {
    const popover = await this.popoverController.create({
      component: RiskCustomerWarningComponent,
      cssClass: 'select-variant chat-connection',
      translucent: true,
      componentProps: {
        rating: this.customerReturnRating,
        phone: this.customer.phone,
        messageError: this.messageError
      }
    });
    return await popover.present();
  }

  async initConnections() {
    try {
      const connections = this.connectionStore.snapshot.loggedInConnections;
      if (connections?.length == 0) {
        await this.connectionService.getConnections();
      }
    } catch (e) {
      debug.log('ERROR in initConnections', e)
    }
  }
}
