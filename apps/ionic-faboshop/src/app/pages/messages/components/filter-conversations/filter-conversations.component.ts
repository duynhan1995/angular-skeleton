import { Component, EventEmitter, OnInit, Output, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FbPage } from "libs/models/faboshop/FbPage";
import { CustomerConversationType } from "libs/models/faboshop/CustomerConversation";
import { takeUntil } from "rxjs/operators";
import { BaseComponent } from "@etop/core";
import { NavController } from '@ionic/angular';
import { FbPagesQuery, FbPagesService } from '@etop/state/fabo/page';
import { ConversationsService } from '@etop/state/fabo/conversation';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-filter-conversations',
  templateUrl: './filter-conversations.component.html',
  styleUrls: ['./filter-conversations.component.scss'],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class FilterConversationsComponent extends BaseComponent implements OnInit {
  @Output() changed = new EventEmitter<{
    externalPageId: string;
    isRead?: string;
    messageType?: string;
  }>()

  selectedExternalPageId;

  is_read_arr = [
    {
      name: 'Tất cả',
      value: 'all'
    },
    {
      name: 'Tin chưa đọc',
      value: false
    },
    {
      name: 'Tin đã đọc',
      value: true
    }
  ];
  selectedIsRead = 'all';

  type_arr = [
    {
      name: 'Tất cả',
      value: 'all'
    },
    {
      name: 'Inbox',
      value: 'message'
    },
    {
      name: 'Comment',
      value: 'comment'
    }
  ];
  selectedType: CustomerConversationType = 'all';

  pages$ = this.fbPageQuery.selectAll();
  activePage$ = this.fbPageQuery.selectActive();

  pageOptions = [];

  toggle = true;

  constructor(
    private fbPageQuery: FbPagesQuery,
    private fbPageService: FbPagesService,
    private ref: ChangeDetectorRef,
    private conversationsService: ConversationsService,
    private navCtrl: NavController,
    private util: UtilService
  ) {
    super();

  }

  async ngOnInit() {
    await this.fbPageService.fetchPages();
    this.activePage$.pipe(takeUntil(this.destroy$))
      .subscribe(activePage => {
        this.selectedExternalPageId = activePage?.external_id || 'all';
      })
    this.pages$.pipe(takeUntil(this.destroy$)).subscribe(pages => {
      if(!pages) {
        return
      }
      this.pageOptions = [{
        value: 'all',
        name: `Tất cả Fanpage (${pages.length})`
      }].concat(pages.map(page => {return ({value: page.external_id, name: page.external_name})}))
      this.selectedExternalPageId = this.selectedExternalPageId || 'all';
      this.toggle = false;
      setTimeout(() => {
        this.toggle = true
        this.ref.detectChanges();
      });
      this.ref.detectChanges();
    })
  }

  getFanpageById(id: string): FbPage {
    if (!id) {
      return null;
    }
    return this.fbPageQuery.getEntity(id);
  }

  invokeChange() {
    this.changed.next({
      externalPageId: this.selectedExternalPageId == 'all' ? null : this.selectedExternalPageId,
      isRead: this.selectedIsRead == 'all' ? null : JSON.parse(this.selectedIsRead),
      messageType: this.selectedType == 'all' ? null : this.selectedType
    });
  }

  async search() {
    this.conversationsService.updateSearchConversations([], null);
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/messages/search`, { animated: false }
    );
  }

}
