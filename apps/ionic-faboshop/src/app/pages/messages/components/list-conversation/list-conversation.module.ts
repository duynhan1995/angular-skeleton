import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ListConversationComponent } from './list-conversation.component';
import { FilterConversationsComponent } from 'apps/ionic-faboshop/src/app/pages/messages/components/filter-conversations/filter-conversations.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/ionic-faboshop/src/app/features/shared/shared.module';
import { EtopPipesModule } from '@etop/shared';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkStatusModule } from 'apps/ionic-faboshop/src/app/components/network-status/network-status.module';
import { ImageLoaderModule } from '@etop/features';
import { EmptyPermissionModule } from 'apps/ionic-faboshop/src/app/components/empty-permission/empty-permission.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    IonicModule,
    NetworkStatusModule,
    ImageLoaderModule,
    EmptyPermissionModule,
    BrowserModule
  ],
  declarations: [ListConversationComponent, FilterConversationsComponent],
  exports: [ListConversationComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ListConversationModule {

}
