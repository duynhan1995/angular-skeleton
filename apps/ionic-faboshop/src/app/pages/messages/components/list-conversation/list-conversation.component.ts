import { Component, OnInit, NgZone } from '@angular/core';
import { PageApi, NotificationApi } from '@etop/api';
import { ChatConnectionModalComponent } from 'apps/ionic-faboshop/src/app/components/chat-connection-modal/chat-connection-modal.component';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { Plugins } from '@capacitor/core';
import { FacebookLoginResponse } from 'capacitor-facebook-plugin';
import { PopoverController, NavController, ModalController, AlertController } from '@ionic/angular';
import { BaseComponent, ConfigService, AuthenticateStore } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { ConversationsQuery, ConversationsService } from '@etop/state/fabo/conversation';
import { FbPagesQuery, FbPagesService } from '@etop/state/fabo/page';
import { CustomerConversation, FbPage } from '@etop/models/faboshop';
import { Customer } from '@etop/models/Customer';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { ConfirmOrderService } from '@etop/features';
import { RelationshipService } from '@etop/state/relationship';
const { Device } = Plugins;

@Component({
  selector: 'etop-list-conversation',
  templateUrl: './list-conversation.component.html',
  styleUrls: ['./list-conversation.component.scss']
})
export class ListConversationComponent extends BaseComponent implements OnInit {
  initializing = true;

  loading$ = this.conversationsQuery.selectLoading();
  pages$ = this.fbPagesQuery.selectAll();
  activePage$ = this.fbPagesQuery.selectActive();
  conversations$ = this.conversationsQuery.selectAll();

  constructor(
    private navController: NavController,
    private util: UtilService,
    private popoverController: PopoverController,
    private toast: ToastService,
    private api: PageApi,
    private conversationsQuery: ConversationsQuery,
    private conversationsService: ConversationsService,
    private fbPagesService: FbPagesService,
    private fbPagesQuery: FbPagesQuery,
    private confirmOrderController: ConfirmOrderController,
    private navCtrl: NavController,
    private oneSignal: OneSignal,
    private config: ConfigService,
    private badge: Badge,
    private notificationApi: NotificationApi,
    private zone: NgZone,
    private fulfillmentService: FulfillmentService,
    private auth: AuthenticateStore,
    private fbUserTagService: FacebookUserTagService,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private confirmOrderService: ConfirmOrderService,
    private relationshipsService: RelationshipService
  ) {
    super();
  }

  async ngOnInit() {}
  async ionViewWillEnter() {
    await this.relationshipsService.getRelationships();
    if (this.auth.snapshot.permission.permissions.includes('facebook/message:view')) {
      await this.fbUserTagService.initState(true);
      await this.listPages();
    }
    const ionSelects = document.querySelectorAll('ion-select');
    ionSelects.forEach(sel => {
      sel.shadowRoot.querySelectorAll('.select-icon-inner').forEach(elem => {
        elem.setAttribute('style', 'display: none;');
      });
      sel.shadowRoot.querySelectorAll('.select-text').forEach(elem => {
        elem.setAttribute('style', 'padding-right: .5rem;');
      });
    });
    this.setupOneSignal();
  }

  hasPermission() {
    return this.auth.snapshot.permission.permissions.includes('facebook/message:view')
  }

  orderPermission() {
    return this.auth.snapshot.permission.permissions.includes('shop/order:create')
  }

  hasFbPermisson() {
    return this.auth.snapshot.permission.permissions.includes('facebook/comment:create')
  }

  async setupOneSignal() {
    this.oneSignal.startInit(
      this.config.getConfig().onesignal_app_id,
      this.config.getConfig().google_project_number
    );
    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.Notification
    );
    this.oneSignal.handleNotificationReceived().subscribe(data => {
      this.badge.increase(1);
    });

    this.oneSignal.handleNotificationOpened().subscribe(data => {
      const _data: any = data.notification.payload.additionalData;
      this.badge.decrease(1);
      if (_data.entity) {
        this.readNoti(_data);
      }
    });
    const oneSignal = this.auth.snapshot.oneSignal;
    if (!oneSignal) {
      this.oneSignal.getIds().then(async data => {
        if (data) {
          const device = await Device.getInfo();
          this.notificationApi
            .createDevice({
              device_id: device.uuid,
              device_name: device.model,
              external_device_id: data.userId
            })
            .then(_ => {
              this.auth.updateOneSignal({
                device_id: device.uuid,
                external_device_id: data.userId
              });
            });
        }
      });
    }
    this.oneSignal.endInit();
  }

  async readNoti(noti_data) {
    this.zone.run(_ => {
      this.alertCtrl.dismiss().then();
      this.modalCtrl.dismiss().then();
      this.popoverController.dismiss().then();
    })
    this.confirmOrderService.updateSaveOrder(null);
    this.confirmOrderService.updateSaveFulfillment(null);
    switch (noti_data.entity) {
      case 'fulfillment':
        const ffm = await this.fulfillmentService.getFulfillment(
          noti_data.entity_id
        );
        this.zone.run(() => {
          this.navCtrl.navigateForward(
            `/s/${this.util.getSlug()}/orders/${ffm.order_id}`
          );
        });
        break;
      case 'fb_external_message':
        this.zone.run(() => {
          this.navCtrl.navigateForward(
            `/s/${this.util.getSlug()}/messages/${
              noti_data.meta_data.conversation_id
            }`
          );
        });
        break;
      case 'fb_external_comment':
        this.navCtrl.navigateForward(
          `/s/${this.util.getSlug()}/messages/${
            noti_data.meta_data.conversation_id
          }`
        );
        break;
      case 'system':
        break;
      default:
        break;
    }
  }

  async listPages() {
    try {
      await this.fbPagesService.fetchPages();
      await this.conversationsService.preloadConversations();
      await this.conversationsService.preloadConversationsMessages();
    } catch (e) {
      debug.error('ERROR in listPages', JSON.stringify(e));
    }
  }

  async fbConnect() {
    try {
      const FB = Plugins.FacebookPlugin;
      await FB.logout();
      const res: FacebookLoginResponse = await FB.login({
        permissions:
          'pages_show_list,pages_messaging,pages_manage_metadata,pages_read_engagement,pages_read_user_content,pages_manage_engagement,pages_manage_posts'
      });

      const token = res.accessToken && res.accessToken.token;
      if (token) {
        const connectionResult = await this.api.connectPages(token);
        await this.openModalConnectionResult(connectionResult);
      }
    } catch (e) {
      debug.error('ERROR in loginFacebook', JSON.stringify(e));
      if (e.code) {
        this.toast.error(
          `Có lỗi xảy ra khi kết nối với Facebook. ${e.message || e.msg}`
        );
      }
      await this.listPages();
    }
  }

  async openModalConnectionResult(connectionResult) {
    const popover = await this.popoverController.create({
      component: ChatConnectionModalComponent,
      translucent: true,
      componentProps: {
        connectionResult
      },
      cssClass: 'select-variant chat-connection',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    popover.onDidDismiss().then(async () => {
      await this.listPages();
    });
    return await popover.present();
  }

  async messageDetail(conv: CustomerConversation) {
    if (!['message', 'comment'].includes(conv.type)) {
      return;
    }

    if (!conv.is_read) {
      this.conversationsService.updateReadStatus(conv);
    }
    this.confirmOrderService.updateSaveOrder(null);
    this.confirmOrderService.updateSaveFulfillment(null);
    this.conversationsService.selectConversation(conv);
    await this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/messages/${conv.id}`
    );
  }

  async doLoadMore(event) {
    await this.conversationsService.loadMoreConversations();
    event.target.complete();
  }

  async doRefresh(event) {
    await this.conversationsService.loadMoreConversations(true, true);
    event.target.complete();
  }

  async filter(event) {
    const { externalPageId, isRead, messageType } = event;
    const activePage = this.fbPagesQuery
      .getAll()
      .find(page => page.external_id == externalPageId);
    this.fbPagesService.setActivePage(activePage || null);
    this.conversationsService.setFilter({
      ...this.conversationsQuery.getValue().ui.filter,
      is_read: isRead,
      type: messageType,
      external_page_id: externalPageId
    });
    this.conversations$ = this.conversationsQuery.selectAll({
      filterBy: conversation =>
        (isRead == null ||
          isRead == undefined ||
          conversation.is_read == isRead) &&
        (!externalPageId || conversation.external_page_id == externalPageId) &&
        (!messageType ||
          messageType == 'all' ||
          conversation.type == messageType)
    });
    await this.conversationsService.loadMoreConversations();
  }

  getFanpageByExternalId(external_id: string): FbPage {
    if (!external_id) {
      return null;
    }
    return this.fbPagesQuery.getAll().find(p => p.external_id == external_id);
  }

  async confirmOrder(conversation: CustomerConversation) {
    await this.conversationsService.selectConversation(conversation);
    this.confirmOrderController.resetForm();
    const fbUser = this.conversationsQuery.getActive().fbUsers[0];
    if (fbUser?.customer) {
      this.confirmOrderController.setCustomerForOrder(fbUser?.customer);
    } else {
      this.confirmOrderController.setCustomerForOrder(
        new Customer({
          id: null,
          full_name: fbUser?.external_info?.name,
          phone: null,
          external_id: fbUser?.external_id,
          external_info: fbUser?.external_info
        })
      );
    }

    this.confirmOrderService.resetSaveDataOrder();
    this.navController
      .navigateForward(`/s/${this.util.getSlug()}/confirm-order?conversation=${conversation.id}&type=${conversation.type}`)
      .then();
  }

  callPhone(phone) {
    window.location.href = 'tel:' + phone;
  }

  trackByConversation(index: number, conversation: CustomerConversation) {
    return conversation.id;
  }
}
