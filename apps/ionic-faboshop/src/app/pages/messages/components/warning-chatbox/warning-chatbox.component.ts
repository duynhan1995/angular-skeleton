import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@etop/core';
import { ModalAction } from '../../../../../../../core/src/components/modal-controller/modal-action.service';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'warning-chatbox',
  templateUrl: './warning-chatbox.component.html',
  styleUrls: ['./warning-chatbox.component.scss']
})

export class WarningChatboxComponent extends BaseComponent implements OnInit{
  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
  ) {
    super();
  }
  async ngOnInit() {
  }
  dismiss() {
    this.modalCtrl.dismiss();
  }
}
