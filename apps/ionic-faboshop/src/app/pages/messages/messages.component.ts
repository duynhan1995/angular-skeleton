import { Component, OnInit } from '@angular/core';
import { CustomerService, CustomersQuery } from '@etop/state/fabo/customer';

@Component({
  selector: 'etop-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(
    private customerService: CustomerService,
  ) {}

  ngOnInit() {
    this.customerService.setActiveCustomer(null);
  }

}
