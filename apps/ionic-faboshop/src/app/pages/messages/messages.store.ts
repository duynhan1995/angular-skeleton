import { AStore } from 'apps/core/src/interfaces/AStore';
import { Injectable } from '@angular/core';
import List from 'identical-list';
import { Order } from 'libs/models/Order';

export interface MessageOrderData {
  order:  Order;
}

@Injectable()
export class MessageOrderStore extends AStore<MessageOrderData> {
  initState: MessageOrderData = {
    order: null
  };

  constructor() {
    super();
  }

  setNewOrder(order: Order) {
    this.setState({ order });
  }

}

