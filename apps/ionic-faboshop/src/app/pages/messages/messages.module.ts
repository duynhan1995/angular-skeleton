import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { MessagesComponent } from './messages.component';
import { MessageDetailComponent } from './components/message-detail/message-detail.component';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { ImageCompressor } from '@etop/utils/image-compressor';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { NetworkStatusModule } from 'apps/ionic-faboshop/src/app/components/network-status/network-status.module';
import { CustomerDetailComponent } from './components/customer-detail/customer-detail.component';
import { CustomerEditComponent } from './components/customer-edit/customer-edit.component';
import { FaboCustomerService } from '@etop/features/fabo/customer/fabo-customer.service';
import { WarningChatboxComponent } from './components/warning-chatbox/warning-chatbox.component';
import { CustomerService, ImageLoaderModule } from '@etop/features';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { SelectTagsModule } from '../../components/select-tags/select-tags.module';
import { SearchMessageComponent } from './components/search-message/search-message.component';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { MatIconModule } from '@angular/material/icon';
import { PrivateReplyModule } from '../../components/private-reply/private-reply.module';
import { AuthorizationApi } from '@etop/api';
// import { RelationshipsQuery } from '@etop/state/relationships';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'search', component: SearchMessageComponent },
      { path: ':id/warning', component: WarningChatboxComponent },
      { path: ':id', component: MessageDetailComponent },
      { path: 'customer/:id', component: CustomerDetailComponent },
      { path: 'customer/:id/edit', component: CustomerEditComponent },
    ]
  }
];

@NgModule({
  declarations: [
    MessagesComponent,
    MessageDetailComponent,
    CustomerDetailComponent,
    WarningChatboxComponent,
    CustomerEditComponent,
    SearchMessageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NetworkStatusModule,
    ImageLoaderModule,
    SelectTagsModule,
    SelectSuggestModule,
    MatIconModule,
    PrivateReplyModule
  ],
  entryComponents: [CustomerEditComponent],
  exports: [MessagesComponent, MessageDetailComponent],
  providers: [
    MobileUploader,
    ImageCompressor,
    ImagePicker,
    FaboCustomerService,
    CustomerService,
    OneSignal,
    // RelationshipsQuery,
    AuthorizationApi
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessagesModule {}
