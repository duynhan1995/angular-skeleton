import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { CalendarModule } from 'ion2-calendar';
import { EmptyPermissionModule } from '../../components/empty-permission/empty-permission.module';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    CalendarModule,
    EmptyPermissionModule,
    IonicModule
  ]
})
export class DashboardModule { }
