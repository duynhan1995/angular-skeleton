import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { Moment } from 'moment';
import { CalendarModal, CalendarModalOptions, DayConfig, CalendarResult } from 'ion2-calendar';
import { StatisticService } from '@etop/features/fabo/statistic';
import { RelationshipQuery, RelationshipService } from '@etop/state/relationship';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'ionfabo-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('chart', { static: false }) chartElement: ElementRef;
  @ViewChild('chartCod', { static: false }) chartCodElement: ElementRef;
  @ViewChild('chartOrder', { static: false }) chartOrderElement: ElementRef;
  @ViewChild('chartReturn', { static: false }) chartReturnElement: ElementRef;

  displayedColumns = ['user', 'new_customer', 'distinct_customer', 'message', 'comment', 'order', 'basket_value', 'total_cod_amount'];
  dataSource = [];
  chartDate: any;
  selected: { startDate: Moment; endDate: Moment, label?: string, type?: string };
  dateArr = [];
  chartDateData = [];
  chart: any;
  chartCod: any;
  chartOrder: any;
  chartReturn: any;
  dashboardData: any;
  todayData: any;
  totalData: any;
  summary: any;
  prevSummary: any;
  loading = true;

  constructor(
    private actionSheetController: ActionSheetController,
    private modalCtrl: ModalController,
    private changeDef: ChangeDetectorRef,
    private statisticService: StatisticService,
    private relationshipQuery: RelationshipQuery,
    private relationshipService: RelationshipService,
    private auth: AuthenticateStore
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.loading = true;
    this.selected = {
      startDate: moment().startOf('isoWeek'),
      endDate: moment(),
      label: 'TRONG TUẦN NÀY',
      type: 'week'
    };
    await this.relationshipService.getRelationships();
    this.dateArr = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN']
    await this.resetChart();
    this.changeDef.detectChanges();
    this.loading = false;
  }

  initChart() {
    if (this.chart) {
      this.chart.destroy();
    }
    let label = this.mapLabel(this.selected.type);
    let _data = this.summary?.charts?.basket_value?.map(data => data.value);
    let _data_2 = this.prevSummary?.charts?.basket_value?.map(data => data.value);
    const max_value = Math.max(..._data, ..._data_2);
    let yAxisHeight =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      yAxisHeight = 1000000;
    }
    let _datasets = [
      {
        label: label.toLabel,
        lineTension: 0,
        fill: false,
        data: _data,
        backgroundColor: 'rgb(40, 81, 163)',
        borderColor: 'rgb(40, 81, 163)',
        borderWidth: 1,
        pointStyle: 'circle',
      },
      {
        label: label.fromLabel,
        lineTension: 0,
        fill: false,
        data: _data_2,
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        pointStyle: 'circle',
    }]
    if (this.selected.type == 'custom') {
      _datasets.splice(1, 1)
    }
    const ctx = this.chartElement.nativeElement.getContext('2d');
    this.chart = new Chart(ctx, {
    type: 'line',
    duration: 800,
    easing: 'easeOutBounce',
    data: {
        labels: this.dateArr,
        datasets: _datasets
      },
      options: {
        elements: {
          point:{
              radius: 0
          }
      },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle:true,
            boxWidth: 5,
          },
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontSize: 10,
                    beginAtZero: true,
                    callback: (value, _, __) => {
                      return value < 1000000
                        ? value == 0
                          ? 0
                          : (value / 1000) + 'k'
                        : (value / 1000000) + 'tr';
                    },
                    stepSize: (yAxisHeight || 1000000) / 5,
                    max: yAxisHeight || 1000000
                },
            }],
            xAxes: [{
              ticks: {
                fontSize: 10
              }
            }]
        },
      }
    });
  }

  initChartCOD() {
    const ctx = this.chartCodElement.nativeElement.getContext('2d');
    if (this.chartCod) {
      this.chartCod.destroy();
    }
    let label = this.mapLabel(this.selected.type);
    let _data = this.summary?.charts?.total_cod_amount?.map(data => data.value);
    let _data_2 = this.prevSummary?.charts?.total_cod_amount?.map(data => data.value);
    const max_value = Math.max(..._data, ..._data_2);
    let yAxisHeight =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      yAxisHeight = 1000000;
    }
    let _datasets = [
      {
        label: label.toLabel,
        lineTension: 0,
        fill: false,
        data: _data,
        backgroundColor: 'rgb(40, 81, 163)',
        borderColor: 'rgb(40, 81, 163)',
        borderWidth: 1,
        pointStyle: 'circle',
      },
      {
        label: label.fromLabel,
        lineTension: 0,
        fill: false,
        data: _data_2,
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        pointStyle: 'circle',
    }]
    if (this.selected.type == 'custom') {
      _datasets.splice(1, 1)
    }
    this.chartCod = new Chart(ctx, {
    type: 'line',
    duration: 800,
    easing: 'easeOutBounce',
    data: {
        labels: this.dateArr,
        datasets: _datasets
      },
      options: {
        elements: {
          point:{
              radius: 0
          }
      },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle:true,
            boxWidth: 5,
          },
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontSize: 10,
                    beginAtZero: true,
                    callback: (value, _, __) => {
                      return value < 1000000
                        ? value == 0
                          ? 0
                          : (value / 1000) + 'k'
                        : (value / 1000000) + 'tr';
                    },
                    stepSize: (yAxisHeight || 1000000) / 5,
                    max: yAxisHeight || 1000000
                },
            }],
            xAxes: [{
              ticks: {
                fontSize: 10
              }
            }]
        },
      }
    });
  }

  initChartOrder() {
    const ctx = this.chartOrderElement.nativeElement.getContext('2d');
    if (this.chartOrder) {
      this.chartOrder.destroy();
    }
    let label = this.mapLabel(this.selected.type);
    let _data = this.summary?.charts?.delivered?.map(data => data.value);
    let _data_2 = this.prevSummary?.charts?.delivered?.map(data => data.value);
    const max_value = Math.max(..._data, ..._data_2);
    let yAxisHeight =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      yAxisHeight = 10;
    }
    let _datasets = [
      {
        label: label.toLabel,
        lineTension: 0,
        fill: false,
        data: _data,
        backgroundColor: 'rgb(40, 81, 163)',
        borderColor: 'rgb(40, 81, 163)',
        borderWidth: 1,
        pointStyle: 'circle',
      },
      {
        label: label.fromLabel,
        lineTension: 0,
        fill: false,
        data: _data_2,
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        pointStyle: 'circle',
    }]
    if (this.selected.type == 'custom') {
      _datasets.splice(1, 1)
    }
    this.chartCod = new Chart(ctx, {
    type: 'line',
    duration: 800,
    easing: 'easeOutBounce',
    data: {
        labels: this.dateArr,
        datasets: _datasets
      },
      options: {
        elements: {
          point:{
              radius: 0
          }
      },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle:true,
            boxWidth: 5,
          },
        },
        scales: {
              yAxes: [{
              fontSize: 10,
                ticks: {
                    beginAtZero: true,
                    stepSize: Math.ceil(yAxisHeight / 5),
                    max: yAxisHeight || 100
                },
            }],
            xAxes: [{
              ticks: {
                fontSize: 10
              }
            }]
        },
      }
    });
  }

  initChartReturnOrder() {
    const ctx = this.chartReturnElement.nativeElement.getContext('2d');
    if (this.chartReturn) {
      this.chartReturn.destroy();
    }
    let label = this.mapLabel(this.selected.type);
    let _data = this.summary?.charts?.returned?.map(data => data.value);
    let _data_2 = this.prevSummary?.charts?.returned?.map(data => data.value);
    const max_value = Math.max(..._data, ..._data_2);
    let yAxisHeight =
      Math.ceil(max_value / Math.pow(10, max_value.toString().length - 1)) *
      Math.pow(10, max_value.toString().length - 1);

    if (max_value == 0) {
      yAxisHeight = 10;
    }
    let _datasets = [
      {
        label: label.toLabel,
        lineTension: 0,
        fill: false,
        data: _data,
        backgroundColor: 'rgb(40, 81, 163)',
        borderColor: 'rgb(40, 81, 163)',
        borderWidth: 1,
        pointStyle: 'circle',
      },
      {
        label: label.fromLabel,
        lineTension: 0,
        fill: false,
        data: _data_2,
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        pointStyle: 'circle',
    }]
    if (this.selected.type == 'custom') {
      _datasets.splice(1, 1)
    }
    this.chartCod = new Chart(ctx, {
    type: 'line',
    duration: 800,
    easing: 'easeOutBounce',
    data: {
        labels: this.dateArr,
        datasets: _datasets
      },
      options: {
        elements: {
          point:{
              radius: 0
          }
      },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle:true,
            boxWidth: 5,
          },
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontSize: 10,
                    beginAtZero: true,
                    stepSize: Math.ceil(yAxisHeight / 5),
                    max: yAxisHeight || 100
                },
            }],
            xAxes: [{
              ticks: {
                fontSize: 10
              }
            }]
        },
      }
    });
  }

  getArrdays() {
    this.dateArr = [];
    let start = new Date(Number(this.selected.startDate));
    const end = new Date(Number(this.selected.endDate));
    if (this.selected.type == 'month') {
      const max = Math.max(moment().subtract(1, "month").daysInMonth(), Math.max(moment().daysInMonth()));
      for (let index = 1; index < max; index++) {
        this.dateArr.push(index);
      }
    } else {
      while (start < end) {
        this.dateArr.push(moment(start).format('D/M'));
        const newDate = start.setDate(start.getDate() + 1);
        start = new Date(newDate);
      }
    }

  }

  async resetChart() {
    await this.getData();
    this.initChart();
    this.initChartCOD();
    this.initChartOrder();
    this.initChartReturnOrder();
  }

  mapLabel(type) {
    const date = this.selected;
    const from = date.startDate;
    const to = date.endDate;
    switch(type) {
      case 'date': {
        return {
          fromLabel: `Tuần trước (${moment().subtract(1, 'week').startOf('week').add(1, 'days').format('D/M')}-${moment().subtract(1, 'week').endOf('week').add(1, 'days').format('D/M')})`,
          toLabel: `Tuần này (${from.format('D/M')}-${to.format('D/M')})`
        }
      }
      case 'week': {
        return {
          fromLabel: `Tuần trước (${moment().subtract(1, 'week').startOf('week').add(1, 'days').format('D/M')}-${moment().subtract(1, 'week').endOf('week').add(1, 'days').format('D/M')})`,
          toLabel: `Tuần này (${from.format('D/M')}-${to.format('D/M')})`
        }
      }
      case 'month': {
        return {
          fromLabel: `Tháng trước`,
          toLabel: `Tháng này`
        }
      }
      case 'custom': {
        return {
          fromLabel: `Tùy chọn (${from.format('D/M/yy')}-${to.format('D/M/yy')})`,
          toLabel: `Tùy chọn (${from.format('D/M/yy')}-${to.format('D/M/yy')})`
        }
      }
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Tùy chọn',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Tuần này',
        handler: () => {
          this.selected = {
            startDate: moment().startOf('isoWeek'),
            endDate: moment(),
            label: 'TRONG TUẦN NÀY',
            type: 'week'
          };
          this.dateArr = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];
          this.resetChart();
          this.changeDef.detectChanges();
        }
      }, {
        text: 'Tháng này',
        handler: () => {
          this.selected = {
            startDate: moment().startOf('month'),
            endDate: moment(),
            label: 'TRONG THÁNG NÀY',
            type: 'month'
          };
          this.getArrdays();
          this.resetChart();
          this.changeDef.detectChanges();
        }
      }, {
        text: 'Tùy chọn khác',
        handler: () => {
          this.openCalendar();
        }
      }, {
        text: 'Đóng',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async openCalendar() {
    const options: CalendarModalOptions = {
      pickMode: 'range',
      title: 'Chọn ngày',
      closeLabel: 'Đóng',
      doneLabel: 'Chọn',
      weekdays: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      // from: new Date(moment().subtract(1, 'years').format()),
      to: new Date(),
      cssClass: 'calendar-modal',
      canBackwardsSelected: true,
      step: 1,
      monthFormat: 'MM/yyyy'
    };

    const myCalendar = await this.modalCtrl.create({
      component: CalendarModal,
      componentProps: { options }
    });

    myCalendar.present();

    const event: any = await myCalendar.onDidDismiss();
    const date = event?.data;
    if (date) {
      const from: CalendarResult = date.from;
      const to: CalendarResult = date.to;
      this.selected = {
        startDate: moment(from.dateObj),
        endDate: moment(to.dateObj),
        label: 'TÙY CHỌN',
        type: 'custom'
      }
      this.getArrdays();
      this.resetChart();
      this.changeDef.detectChanges();
    }

  }

  async getData() {
    try {
      this.summary = await this.statisticService.getSummaryShop({
        date_from: this.selected.startDate.format('YYYY-MM-DD'),
        date_to: moment(this.selected.endDate).add(1, 'days').format('YYYY-MM-DD')
      });
      if (this.selected.type == 'week') {
        this.prevSummary = await this.statisticService.getSummaryShop({
          date_from: moment(this.selected.startDate).subtract(1, 'week').format('YYYY-MM-DD'),
          date_to: moment(this.selected.endDate).subtract(1, 'week').endOf('week').add(2, 'days').format('YYYY-MM-DD')
        });
      }
      if (this.selected.type == 'month') {
        this.prevSummary = await this.statisticService.getSummaryShop({
          date_from: moment(this.selected.startDate).subtract(1, 'month').format('YYYY-MM-DD'),
          date_to: moment(this.selected.endDate).subtract(1, 'month').endOf('month').add(1, 'days').format('YYYY-MM-DD')
        });
      }
      this.todayData = this.summary?.today;
      this.totalData = this.summary?.total;
      this.dataSource = this.summary?.staff?.map(column => {
        return {
          ...column,
          user: this.relationshipQuery.getRelationshipNameById(column.user)
        }
      });
      this.changeDef.detectChanges();
    } catch(e) {
      debug.log('ERROR in load Summary Shop', e);
    }

  }

  hasPermission() {
    return this.auth.snapshot.permission.permissions.includes('shop/dashboard:view')
  }
}
