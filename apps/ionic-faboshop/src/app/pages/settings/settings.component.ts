import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { ShopService } from '@etop/features';
import {AccountApi} from '@etop/api';
import { ToastController, ModalController } from '@ionic/angular';
import { ShopInfoModel } from 'apps/ionic-faboshop/src/models/ShopInfo';
import { Plugins, CameraResultType } from '@capacitor/core';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import List from 'identical-list';
import { ToastService } from '../../services/toast.service';
import {LocationQuery, LocationService} from '@etop/state/location';

const { Camera } = Plugins;

@Component({
  selector: 'etop-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent extends BaseComponent implements OnInit {
  currentShop: any = {
    address: {
      address1: '',
      province_code: '',
      district_code: '',
      ward_code: ''
    }
  };
  provinces = [];
  districts = [];
  wards = [];
  currentShopOriginal: any = new ShopInfoModel();
  currentAccount: any = {};
  uploadLoading = false;
  currentSlugHandle = '';
  user: any = {};
  loadData = false;

  accounts = new List<any>();

  constructor(
    private authStore: AuthenticateStore,
    private util: UtilService,
    private gaService: GoogleAnalyticsService,
    private shopService: ShopService,
    private accountApi: AccountApi,
    private toastController: ToastController,
    private uploader: MobileUploader,
    private modalCtrl: ModalController,
    private toastService: ToastService,
    private auth: AuthenticateStore,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {
    super();
  }

  async ngOnInit() {
    const auth = this.authStore.snapshot;
    this.loadAccount(auth);
    await this._prepareLocationData();
    await this.loadLocation();
    this.loadData = true;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  disableView(permission) {
    return this.auth.snapshot.permission.permissions.includes(permission);
  }

  loadAccount(data) {
    this.loadData = false;
    this.currentShop = data.shop || new ShopInfoModel();
    this.currentShopOriginal = this.currentShop;
    this.user = data.user;
    this.currentAccount = data.account || {};
    this.currentSlugHandle = this.currentAccount.url_slug;
    this.accounts = new List(this.authStore.snapshot.accounts);
  }

  onChangeShopName() {
    this.currentAccount.url_slug = this.util.createHandle(
      this.currentShop.name
    );
  }
  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  loadLocation() {
    this.provinces = this.locationQuery.getValue().provincesList;
    this.districts = this.locationService.filterDistrictsByProvince(this.currentShop.address.province_code);
    this.wards = this.locationService.filterWardsByDistrict(this.currentShop.address.district_code);
  }

  onProvinceSelected() {
    this.currentShop.address.district_code = '';
    this.currentShop.address.ward_code = '';
    this.currentShop.address.address1 = '';
    this.districts = this.locationService.filterDistrictsByProvince(this.currentShop.address.province_code);
  }

  onDistrictSelected() {
    this.currentShop.address.ward_code = '';
    this.currentShop.address.address1 = '';
    this.wards = this.locationService.filterWardsByDistrict(this.currentShop.address.district_code);
  }

  onWardSelected() {
    this.currentShop.address.address1 = '';
  }

  validatePhoneNumber(phone) {
    if (phone && phone.match(/^0[0-9]{9,10}$/)) {
      if (phone.length === 11 && phone[1] === '1') {
        this.toastService.error('Vui lòng nhập số điện thoại di động 10 số');
      }
      return true;
    }
    this.toastService.error('Vui lòng nhập số điện thoại hợp lệ');
  }

  async updateContactInfo() {
    try {
      if (!this.currentShop.name) {
        throw new Error('Vui lòng nhập tên cửa hàng');
      }

      const phone = this.currentShop.phone;
      // phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      // phone = (phone && phone.split('-test')[0]) || '';
      this.validatePhoneNumber(phone);
      if (
        this.currentShop.website_url &&
        !this.currentShop.website_url.match(
          /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        )
      ) {
        throw new Error(`Vui lòng điền địa chỉ website hợp lệ`);
      }

      let email = this.currentShop.email;
      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = (email && email.split('-test')[0]) || '';
      if (
        email &&
        !email.match(
          /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
      ) {
        throw new Error('Vui lòng nhập email hợp lệ!');
      }
      if (!this.currentShop.address.address1) {
        throw new Error('Vui lòng nhập địa chỉ');
      }

      const proObj = this.provinces.find(
        p => p.code == this.currentShop.address.province_code
      );
      const disObj = this.districts.find(
        d => d.code == this.currentShop.address.district_code
      );
      const warObj = this.wards.find(
        w => w.code == this.currentShop.address.ward_code
      );

      if (!this.currentShop.address.province_code || !proObj) {
        throw new Error('Vui lòng chọn tỉnh thành');
      }
      if (!this.currentShop.address.district_code || !disObj) {
        throw new Error('Vui lòng chọn quận huyện');
      }
      if (!this.currentShop.address.ward_code || !warObj) {
        throw new Error('Vui lòng chọn phường xã');
      }

      const address = {
        address1: this.currentShop.address.address1,
        province: proObj.name,
        province_code: proObj.code,
        district: disObj.name,
        district_code: disObj.code,
        ward: warObj.name,
        ward_code: warObj.code
      };

      const data = {
        name: this.currentShop.name,
        image_url: this.currentShop.image_url,
        phone: this.currentShop.phone,
        website_url: this.currentShop.website_url,
        email: this.currentShop.email,
        address
      };

      await this.shopService.updateShopField(data);
      if (this.currentAccount.url_slug) {
        let urlSlug = this.currentAccount.url_slug;
        urlSlug = this.util.createHandle(urlSlug);
        const slugExisted = this.accounts
          .filter(account => account.id !== this.currentAccount.id)
          .reduce((x, y) => x || y.url_slug === urlSlug, false);

        if (slugExisted) {
          let index: any = this.accounts
            .asArray()
            .findIndex(account => account.id === this.currentAccount.id);
          index = index.toString();

          urlSlug += index ? '-' + index : '-0';
        }

        await this.accountApi.updateURLSlug({
          account_id: this.currentAccount.id,
          url_slug: urlSlug
        });
        this.currentAccount.url_slug = urlSlug;
        this.currentSlugHandle = this.currentAccount.url_slug;
      }
      this.currentShop = Object.assign(this.currentShop, data);
      this.currentShopOriginal = this.currentShop;
      this.authStore.updateAccount(this.currentAccount);
      this.authStore.updateShop(this.currentShop);
      this.sendToast('Cập nhật thành công!', 'success');
    } catch (e) {
      this.sendToast(e.message, 'danger');
    }
  }

  async onFileSelected($e) {
    try {
      this.uploadLoading = true;
      const { files } = $e.target;
      const res = await this.util.uploadImages([files[0]], 250);
      debug.log('res', res);
      this.currentShop.image_url = res[0].url;

      const data = {
        image_url: this.currentShop.image_url
      };
      await this.shopService.updateShopField(data);
      this.authStore.updateShop(this.currentShop);

      this.sendToast('Cập nhật logo thành công!', 'success');
      this.uploadLoading = false;
    } catch (e) {
      this.uploadLoading = false;
      debug.error(e);
      this.sendToast(`Chọn logo thất bại: ${e.message}`, 'danger');
    }
  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      width: 640
    });
    const imageElement = await this.uploader.uploadBase64Image(
      image.base64String
    );
    if (imageElement) {
      this.currentShop.image_url = imageElement.result[0].url;
    }
  }

  async sendToast(message, color) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color,
      mode: 'md',
      position: 'top',
      cssClass: 'font-12'
    });
    toast.present();
  }
}
