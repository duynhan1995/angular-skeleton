import { NgModule } from '@angular/core';
import { ListCustomerComponent } from 'apps/ionic-faboshop/src/app/pages/customers/list-customer/list-customer.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'apps/ionic-faboshop/src/app/features/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabsModule } from 'apps/ionic-faboshop/src/app/components/tabs/tabs.module';
import { MenuModule } from 'apps/ionic-faboshop/src/app/components/menu/menu.module';
import { NetworkStatusModule } from 'apps/ionic-faboshop/src/app/components/network-status/network-status.module';
import { NetworkDisconnectModule } from 'apps/ionic-faboshop/src/app/components/network-disconnect/network-disconnect.module';
import { AuthenticateModule } from '@etop/core';
import { EtopPipesModule } from '@etop/shared';
import { CustomerService, FaboCustomerService, ImageLoaderModule } from '@etop/features';
import { MatIconModule } from '@angular/material/icon';
import { EmptyPermissionModule } from '../../../components/empty-permission/empty-permission.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkStatusModule,
    NetworkDisconnectModule,
    AuthenticateModule,
    EtopPipesModule,
    ReactiveFormsModule,
    ImageLoaderModule,
    MatIconModule,
    EmptyPermissionModule
  ],
  declarations: [ListCustomerComponent],
  exports: [ListCustomerComponent],
  providers: [FaboCustomerService, CustomerService]
})
export class ListCustomerModule {}
