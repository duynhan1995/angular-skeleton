import { Component, OnInit, Provider, NgZone } from '@angular/core';
import { Customer, CustomerAddress } from 'libs/models/Customer';
import { ModalController, NavController, IonRouterOutlet, PopoverController } from '@ionic/angular';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';
import { ActivatedRoute } from '@angular/router';
import { Order } from 'libs/models/Order';
import { ConfirmOrderService, ConnectionService, ConnectionStore, OrderService } from '@etop/features';
import { UtilService } from 'apps/core/src/services/util.service';
import {FbUser} from "libs/models/faboshop/FbUser";
import {FaboCustomerService} from "@etop/features/fabo/customer/fabo-customer.service";
import {ConfirmOrderController} from "@etop/features/fabo/confirm-order/confirm-order.controller";
import { CustomerService } from '@etop/state/fabo/customer/customer.service';
import { CustomersQuery } from '@etop/state/fabo/customer/customer.query';
import { distinctUntilChanged } from 'rxjs/operators';
import { FbCustomerReturnRate, FilterOperator, Filters } from '@etop/models';
import { SelectTagsComponent } from '../../../components/select-tags/select-tags.component';
import { FacebookUserTagService, FacebookUserTagQuery } from '@etop/state/fabo/facebook-user-tag';
import { CreateAndUpdateTagComponent } from '../../../components/create-and-update-tag/create-and-update-tag.component';
import { ConversationsService } from '@etop/state/fabo/conversation';
import { RiskCustomerWarningComponent } from '../../../components/popovers/risk-customer-warning/risk-customer-warning.component';


@Component({
  selector: 'etop-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent extends BaseComponent implements OnInit {
  address: any = {
    address1: '',
    province_code: '',
    district_code: '',
    ward_code: ''
  };
  show = true;
  order_show = true;
  address_display = '';
  customerEdited = false;
  fbUser = new FbUser({});
  orders: Order[] = [];
  customer$ = this.customersQuery.selectActive<Customer>().pipe(distinctUntilChanged());
  customerReturnRating: FbCustomerReturnRate;
  connectionID = '';
  phone;
  messageError: any;

  constructor(
    private customerService: CustomerService,
    private customersQuery: CustomersQuery,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private orderService: OrderService,
    private util: UtilService,
    private confirmOrderController: ConfirmOrderController,
    private routerOutlet: IonRouterOutlet,
    private zone: NgZone,
    private fbUserTagQuery: FacebookUserTagQuery,
    private conversationService: ConversationsService,
    private connectionStore: ConnectionStore,
    private popoverController: PopoverController,
    private confirmOrderService: ConfirmOrderService,
    private connectionService: ConnectionService,
    private auth: AuthenticateStore
  ) {
    super();
  }

  async ngOnInit() {}

  async ionViewWillEnter() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.customerService.setActiveCustomer(id);
    const customer = this.customersQuery.getActive();
    await this.getCustomerReturnRate(customer?.phone);
    if (customer.fb_users.length > 0) {
      this.fbUser = await this.conversationService.getFbUser(customer.fb_users[0].external_id);
      this.customerService.setTagsFbUser(id, this.fbUser.tag_ids);
    }
    await this.customerService.getCustomerAddresses(id);
    let res: CustomerAddress[];
    this.customersQuery
      .select(s => s.customer_addresses)
      .pipe()
      .subscribe(customeAddresses => {
        if (customeAddresses?.length) {
          res = customeAddresses;
          if (res.length) {
            this.address = res[0];
            this.address_display = this.mapAddress(this.address);
          }
        }
      });
    const filters: Filters = [
      {
        name: 'customer.id',
        op: FilterOperator.eq,
        value: id
      }
    ];
    this.orders = await this.orderService.getOrders(0, 100, filters);
    this.orders.map(order => {
      if (order.fulfillments.length > 0) {
        const ffm = order.fulfillments[0];
        ffm.shipping_provider_logo = this.getProviderLogo(ffm.carrier);
        ffm.shipping_state_display = this.util.fulfillmentShippingStateMap(
          ffm.shipping_state
        );
      }
    });
  }

  permissionCreateOrder() {
    return this.auth.snapshot.permission?.permissions.includes('shop/order:create')
  }

  async initConnections() {
    try {
      const connections = this.connectionStore.snapshot.loggedInConnections;
      if (connections?.length == 0) {
        await this.connectionService.getConnections();
      }
    } catch (e) {
      debug.log('ERROR in initConnections', e)
    }
  }

  mapTags(tag_ids) {
    const _tags = this.fbUserTagQuery.getAll();
    let tags = [];
    if (tag_ids) {
      _tags.forEach(tag => {
        if (tag_ids.includes(tag.id)) {
          tags.push(tag);
        }
      });
    }
    return tags;
  }

  mapAddress(address) {
    return (
      address.address1 +
      ', ' +
      address.ward +
      ', ' +
      address.district +
      ', ' +
      address.province
    );
  }

  genderMap(gender) {
    switch (gender) {
      case 'male': {
        return 'Nam';
      }
      case 'female': {
        return 'Nữ';
      }
      case 'other': {
        return 'Khác';
      }
      default: {
        return '-';
      }
    }
  }

  async dismiss() {
    this.navCtrl.back();
  }

  toggleDetail() {
    this.show = !this.show;
  }

  toggleOrder() {
    this.order_show = !this.order_show;
  }

  async customerEdit() {
    const customer = this.customersQuery.getActive();
    this.navCtrl.navigateForward(['.', 'edit'], {
      relativeTo: this.activatedRoute,
      state: {
        customerData: customer
      }
    });
  }

  getProviderLogo(provider: Provider | string, size: 'l' | 's' = 's') {
    return `assets/images/provider_logos/${provider}-${size}.png`;
  }

  async orderDetail(order) {
    this.navCtrl.navigateForward(
      `/s/${this.util.getSlug()}/orders/${order.id}`
    );
  }

  confirmOrder() {
    const customer = this.customersQuery.getActive();
    this.confirmOrderController.resetForm();
    this.confirmOrderController.setCustomerForOrder(customer);
    this.confirmOrderService.updateSaveOrder(null);
    this.confirmOrderService.updateSaveFulfillment(null);
    this.navCtrl
      .navigateForward(`/s/${this.util.getSlug()}/confirm-order`)
      .then();
  }

  async newTag() {
    const modal = await this.modalCtrl.create({
      component: SelectTagsComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        external_user_id: this.fbUser.external_id,
        tag_ids: this.customersQuery.getActive()?.fb_users[0]?.tag_ids
      }
    });
    modal.onDidDismiss().then(data => {
      if (data.data?.role == 'create') {
        this.zone.run(async () => {
          await this.createTag();
        });
      }
      if (data.data?.role == 'update') {
        const customer = this.customersQuery.getActive();
        this.customerService.setTagsFbUser(customer.id, data.data.tag_ids);
      }
    });
    return await modal.present();
  }

  async createTag() {
    const modal = await this.modalCtrl.create({
      component: CreateAndUpdateTagComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {}
    });
    modal.onDidDismiss().then(async data => {
      await this.newTag();
    });
    return await modal.present();
  }

  async getCustomerReturnRate(phone: any) {
      try {
        this.phone = phone;
        if (phone.length > 9) {
          const res = await this.confirmOrderService.customerReturnRate(phone);
          this.customerReturnRating = res?.customer_return_rates[0];
          const level = this.customerReturnRating.customer_return_rate.level;
          const first_letter = (level.toLowerCase()).charAt(0);
          this.customerReturnRating.customer_return_rate.level = (level.toLowerCase()).replace(first_letter, level.charAt(0));
          this.messageError = null;
        }
      } catch (e) {
        debug.log('ERROR in getCustomerReturnRate', e);
        this.messageError = e;
        this.customerReturnRating = null;
      }
  }

  async ratingPopover() {
    const popover = await this.popoverController.create({
      component: RiskCustomerWarningComponent,
      cssClass: 'select-variant chat-connection',
      translucent: true,
      componentProps: {
        rating: this.customerReturnRating,
        phone: this.phone,
        messageError: this.messageError
      }
    });
    return await popover.present();
  }
}
