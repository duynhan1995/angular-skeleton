import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CustomersComponent } from './customers.component';
import { MenuModule } from '../../components/menu/menu.module';
import { TabsModule } from '../../components/tabs/tabs.module';
import { SharedModule } from '../../features/shared/shared.module';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { CreateCustomerAddressComponent } from './create-customer-address/create-customer-address.component';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { AuthenticateModule } from '@etop/core';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { EtopPipesModule } from '@etop/shared';
import { FaboCustomerService } from '@etop/features/fabo/customer/fabo-customer.service';
import { CustomerService, ImageLoaderModule } from '@etop/features';
import { SelectTagsModule } from '../../components/select-tags/select-tags.module';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { MatIconModule } from '@angular/material/icon';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'detail/:id', component: CustomerDetailComponent },
      { path: 'create', component: CreateCustomerComponent },
      { path: 'detail/:id/edit', component: EditCustomerComponent }
    ]
  }
];

@NgModule({
  declarations: [
    CustomersComponent,
    CustomerDetailComponent,
    CreateCustomerComponent,
    CreateCustomerAddressComponent,
    EditCustomerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkStatusModule,
    NetworkDisconnectModule,
    AuthenticateModule,
    EtopPipesModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    ImageLoaderModule,
    SelectTagsModule,
    SelectSuggestModule,
    MatIconModule
  ],
  providers: [FaboCustomerService, CustomerService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomersModule {}
