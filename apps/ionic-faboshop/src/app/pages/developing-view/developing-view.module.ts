import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DevelopingViewComponent } from './developing-view.component';

const routes: Routes = [
  {
    path: '',
    component: DevelopingViewComponent
  }
];

@NgModule({
  declarations: [DevelopingViewComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [DevelopingViewComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DevelopingViewModule {}
