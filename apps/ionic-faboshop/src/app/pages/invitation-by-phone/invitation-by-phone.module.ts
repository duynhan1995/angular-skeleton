import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvitationByPhoneComponent } from './invitation-by-phone.component';
import { RouterModule, Routes } from '@angular/router';
import { EtopPipesModule } from '@etop/shared';
import { IonicModule } from '@ionic/angular';
import { MatIconModule } from '@angular/material/icon';

const routes: Routes = [
  {
    path: '',
    children: [{
        path: ':id',
        component: InvitationByPhoneComponent
      }
    ]
  }
];

@NgModule({
  declarations: [InvitationByPhoneComponent],
  imports: [
    CommonModule,
    EtopPipesModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule
  ]
})
export class InvitationByPhoneModule { }
