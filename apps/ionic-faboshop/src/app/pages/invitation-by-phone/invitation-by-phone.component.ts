import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationApi, NotificationApi } from '@etop/api';
import { AuthenticateStore } from '@etop/core';
import { FilterOperator, Invitation } from '@etop/models';
import { AlertController, NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { LoadingService } from '../../services/loading.service';
import { ToastService } from '../../services/toast.service';
import { resetStores } from '@datorama/akita';

@Component({
  selector: 'ionfabo-invitation-by-phone',
  templateUrl: './invitation-by-phone.component.html',
  styleUrls: ['./invitation-by-phone.component.scss']
})
export class InvitationByPhoneComponent implements OnInit {
  phone;
  userInvitation: Invitation;
  constructor(
    private navCtrl: NavController,
    private auth: AuthenticateStore,
    private activatedRoute: ActivatedRoute,
    private authorizationApi: AuthorizationApi,
    private alertController: AlertController,
    private loadingService: LoadingService,
    private toast: ToastService,
    private zone: NgZone,
    private util: UtilService,
    private commonUsecase: CommonUsecase,
    private notificationApi: NotificationApi,
  ) { }

  async ngOnInit() {
    const { params } = this.activatedRoute.snapshot;
    const id = params.id;
    this.phone = id.substring(1);
    if (this.compareAccount()) {
      await this.getUserInvitations();
    }
  }

  back() {
    this.navCtrl.navigateBack(`/s/${this.util.getSlug()}/tab-nav/messages`);
  }

  compareAccount() {
    return this.auth.snapshot.user.phone == this.phone;
  }

  displayRole(role) {
    switch (role) {
      case 'salesman':
        return 'Bán hàng';
      case 'staff_management' :
        return 'Quản lý nhân viên';
      default:
        return '';
    }
  }

  get user() {
    return this.auth.snapshot.user;
  }

  async getUserInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      const userInvitations = await this.authorizationApi.getUserInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
      let _userInvitations: any = userInvitations.filter(
        user => user.phone == this.phone
      );
      this.userInvitation = _userInvitations[0];
    } catch (e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }

  async acceptInvitation(invitation) {
    try {
      this.loadingService.start('Đang đồng bộ dữ liệu');
      await this.authorizationApi.acceptInvitation(invitation.token);
      await this.commonUsecase.updateSessionInfo(true);
      this.back();
      this.loadingService.end();
    } catch (e) {
      this.loadingService.end();
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Accepting Invitation', e);
    }
  }

  async reject(invitation) {
    const alert = await this.alertController.create({
      header: 'Từ chối tham gia quản trị',
      message: `Bạn có chắc từ chối lời mời từ cửa hàng "<strong>${invitation.shop.name}</strong>".`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Từ chối',
          cssClass: 'text-danger',
          handler: () => {
            this.rejectInvitation(invitation);
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  async rejectInvitation(invitation) {
    try {
      await this.authorizationApi.rejectInvitation(invitation.token);
      this.back();
    } catch (e) {
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Rejecting Invitation', e);
    }
  }

  async loginAnotherAccount() {
    const oneSignal = this.auth.snapshot.oneSignal;
      if (oneSignal) {
        this.notificationApi.deleteDevice(oneSignal).then();
      }
      this.auth.clear();
      this.zone.run(async () => {
        resetStores();
        await this.navCtrl.navigateForward(`/login?invitation=${this.phone}&type=phone`, {
          animated: false
        });
      });
  }
}
