import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { MessageTemplate } from '@etop/models';
import { MessageTemplateQuery, MessageTemplateService } from '@etop/state/fabo/message-template';
import { ActionSheetController, AlertController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { ToastService } from '../../../services/toast.service';
import { CreateTemplateComponent } from './components/create-template/create-template.component';

@Component({
  selector: 'ionfabo-template-message',
  templateUrl: './template-message.component.html',
  styleUrls: ['./template-message.component.scss']
})
export class TemplateMessageComponent implements OnInit {
  messageTemplates$ = this.messageTemplateQuery.selectAll();
  
  constructor(
    private modalCtrl: ModalController,
    private zone: NgZone,
    private routerOutlet: IonRouterOutlet,
    private actionSheetController: ActionSheetController,
    private changeDetector: ChangeDetectorRef,
    private messageTemplateService: MessageTemplateService,
    private messageTemplateQuery: MessageTemplateQuery,
    private toast: ToastService,
    private alertController: AlertController
  ) { }

  async ngOnInit() {
    this.messageTemplateService.getMessageTemplateVariables();
    await this.messageTemplateService.getMessageTemplate();
  }

  async createOrEditTemplate(template: MessageTemplate) {
    const modal = await this.modalCtrl.create({
      component: CreateTemplateComponent,
      componentProps: {
        template: template || new MessageTemplate({})
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    await modal.present().then();
  }

  async deleleMessageTemplate(id) {
    try {
      await this.messageTemplateService.deleteMessageTemplate(id);
      this.toast.success('Xóa mẫu trả lời nhanh thành công.')
    } catch(e) {
      debug.log('ERROR in DELETE MESAGE TEMPLATE');
      this.toast.error(e.message);
    }
  }

  async deleteConfirm(template) {
    const alert = await this.alertController.create({
      header: 'Xóa mẫu trả lời nhanh',
      message: `Bạn có muốn xóa mẫu trả lời <strong>${template.short_code}</strong>!`,
      buttons: [
        {
          text: 'Đóng',
          cssClass: 'text-medium',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Xóa',
          cssClass: 'text-danger',
          handler: async () => {
            await this.deleleMessageTemplate(template.id);
          }
        }
      ]
    });

    await alert.present();
  }

  async presentActionSheet(template: MessageTemplate) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Thao tác',
      buttons: [{
        text: 'Chỉnh sửa',
        handler:  () => {
          this.zone.run(async _ => {
            await this.createOrEditTemplate(template)
          })
        }
      }, {
        text: 'Xóa',
        cssClass: 'text-danger',
        handler: () => {
          this.zone.run(async _ => {
            await this.deleteConfirm(template)
          })
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
