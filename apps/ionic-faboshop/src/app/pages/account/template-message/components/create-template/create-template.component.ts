import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MessageTemplate, MessageTemplateVariable } from '@etop/models';
import { IonTextarea, ModalController } from '@ionic/angular';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { MessageTemplateQuery, MessageTemplateService } from '@etop/state/fabo/message-template'

@Component({
  selector: 'ionfabo-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.scss']
})
export class CreateTemplateComponent implements OnInit {
  @Input() template = new MessageTemplate({});
  _template = new MessageTemplate({});
  tempalteVariables: MessageTemplateVariable[];
  @ViewChild('templateInput', { static: false }) templateInput: IonTextarea;

  constructor(
    private toast: ToastService,
    private modalCtrl: ModalController,
    private messageTemplateQuery: MessageTemplateQuery,
    private messageTemplateService: MessageTemplateService,
  ) { }

  async ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss();
  }

  ionViewDidEnter() {
    this.tempalteVariables = this.messageTemplateQuery.getValue().messageTemplateVariables;
    this._template = {...this.template}
  }

  async appendVariable(variable: MessageTemplateVariable) {
    if (!this._template.template) {
      this._template.template = '';
    }
    this._template.template = this._template.template + ' [' + variable.label + ']';
    await this.templateInput.setFocus();
  }

  async confirm() {
    try {
      const {id, short_code, template } = this._template;
      if (!short_code) {
        return this.toast.error('Vui lòng nhập tiêu đề.')
      }
      if (!template) {
        return this.toast.error('Vui lòng nhập nội dung.')
      }
      if (id) {
        await this.messageTemplateService.updateMessageTemplate(this._template);
        this.toast.success('Cập nhật thành công.')
      } else {
        await this.messageTemplateService.createMessageTemplate(this._template);
        this.toast.success('Tạo mẫu trả lời nhanh thành công.')
      }
      this.dismiss();
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in create or update message template');
    }
  }

}
