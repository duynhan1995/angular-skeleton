import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'ionfabo-message-menu',
  templateUrl: './message-menu.component.html',
  styleUrls: ['./message-menu.component.scss']
})
export class MessageMenuComponent implements OnInit {
  listItems = [
    {
      icon: 'logo-facebook',
      title: 'Kết nối Fanpage',
      link: 'facebook-connect',
      color: 'primary',
      permissions: ['facebook/fanpage:view']
    },
    {
      icon: 'reader-outline',
      title: 'Đăng bài viết',
      link: 'post',
      permissions: ['facebook/post:create']
    },
    {
      icon: 'flash-outline',
      title: 'Template trả lời nhanh',
      link: 'template-message',
      permissions: ['facebook/message_template:view']
    },
    {
      icon: '',
      icon_img: 'assets/imgs/icon_hashtag.svg',
      title: 'Thiết lập thẻ',
      link: 'tags',
      permissions: ['facebook/shoptag:create']
    }
  ];

  constructor(
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private util: UtilService
  ) { }

  ngOnInit(): void {
  }

  page(link) {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/account/${link}`);
  }

}
