import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.scss']
})
export class AccountMenuComponent implements OnInit {
  listItems = [
    // {
    //   icon: 'cog-outline',
    //   title: 'Thiết lập cửa hàng',
    //   link: 'setting',
    //   component: SettingsComponent
    // },
    {
      icon: 'person-circle-outline',
      title: 'Thông tin tài khoản',
      link: 'profile',
      permissions: ['shop/settings/shop_info:view']
    },
    {
      icon: 'list-outline',
      title: 'Danh sách cửa hàng',
      link: 'list-store',
      permissions: ['shop/settings/shop_info:view']
    },
    {
      icon: 'people-circle-outline',
      title: 'Danh sách nhân viên',
      link: 'staff',
      permissions: ['relationship/invitation:view']
    },
    {
      icon: 'pricetag-outline',
      title: 'Sản phẩm',
      link: 'products',
      permissions: []
    },
    {
      icon: 'cart-outline',
      title: 'Giao hàng',
      link: 'shipping',
      permissions: []
    },
    {
      icon: 'chatbox-ellipses-outline',
      title: 'Tin nhắn',
      link: 'message',
      permissions: []
    },
    {
      icon: 'notifications-outline',
      title: 'Thiết lập thông báo',
      link: 'notification',
      permissions: ['facebook/message:view']
    },
  ];

  constructor(
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private util: UtilService
  ) {}

  ngOnInit() {}

  get account() {
    return this.auth.snapshot.account;
  }

  get user() {
    return this.auth.snapshot.user;
  }

  page(link) {
    this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/account/${link}`);
  }
}
