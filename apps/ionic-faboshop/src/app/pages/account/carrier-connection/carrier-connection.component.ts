import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { NavController, AlertController, PopoverController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { CarrierConnectComponent } from '../../../components/carrier-connect/carrier-connect.component';
import { map } from 'rxjs/operators';
import { ConnectionAPI } from '@etop/api';
import { LoadingService } from '../../../services/loading.service';
import { ToastService } from '../../../services/toast.service';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { ShopConnection } from 'libs/models/Connection';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'etop-carrier-connection',
  templateUrl: './carrier-connection.component.html',
  styleUrls: ['./carrier-connection.component.scss']
})
export class CarrierConnectionComponent implements OnInit {
  loggedInConnections$ = this.connectionStore.state$.pipe(
    map(s => s?.loggedInConnections)
  );
  availableConnections$ = this.connectionStore.state$.pipe(
    map(s => s?.availableConnections)
  );
  empty = {
    title: 'Chưa kết nối nhà vận chuyển',
    subtitle: 'Vui lòng kết nối Nhà vận chuyển để giao hàng',
    buttonClick: this.showCarriersList.bind(this)
  };
  shopConnections: ShopConnection[];

  constructor(
    private navCtrl: NavController,
    private popoverController: PopoverController,
    private connectionStore: ConnectionStore,
    private loading: LoadingService,
    private connectionService: ConnectionService,
    private toast: ToastService,
    private alertController: AlertController,
    private changeDetector: ChangeDetectorRef,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private zone: NgZone,
    private auth: AuthenticateStore
  ) {}

  async ngOnInit() {
    if (this.hasPermission()) {
      await this.connectionService.getConnections();  
    }
  }

  dismiss() {
    this.navCtrl.back();
  }

  hasPermission() {
    return this.auth.snapshot.permission?.permissions.includes('shop/connection:create')
  }

  async showCarriersList(lastState?: any) {
    const modal = await this.modalCtrl.create({
      component: CarrierConnectComponent,
      componentProps: {
        ...lastState
      },
      animated: true,
      showBackdrop: true,
      backdropDismiss: false,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async data => {
      if (data && data.data) {
        await this.loginConnection(data.data);
      }
    });
    await modal.present();
  }

  async loginConnection(data: any) {
    try {
      await this.loading.start('Đang xử lý...');
      const loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest = {
        ...data.loginOTP
      };
      await this.connectionService.loginShopConnectionWithOTP(loginOTP);
      this.toast.success('Kết nối thành công').then();
    } catch (e) {
      debug.error('ERROR in loginConnection', e);
      this.toast.error('Kết nối không thành công').then();
      this.showCarriersList(data).then();
    }
    this.loading.end();
  }

  async deleteShopConnection(connection) {
    const alert = await this.alertController.create({
      header: 'Gỡ kết nối',
      message: `Bạn có chắc muốn gỡ kết nối với nhà vận chuyển <strong>${connection.name}</strong>`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {}
        },
        {
          text: 'Gỡ kết nối',
          cssClass: 'text-danger font-12',
          handler: () => {
            this.zone.run(async _ => {
              await this.connectionService.deleteShopConnection(connection);
              this.changeDetector.detectChanges();
            })
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
}
