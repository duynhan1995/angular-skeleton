import { Component, OnInit } from '@angular/core';
import { TRY_ON_OPTIONS, TRY_ON_OPTIONS_SHIPPING } from 'libs/models/Fulfillment';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service'
import { ShopSettings } from '@etop/models/ShopSettings';
import { CreateUpdateAddressPopupComponent } from '../../../components/popovers/create-update-address-popup/create-update-address-popup.component';
import { Address } from '@etop/models';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { ToastService } from '../../../services/toast.service';

@Component({
  selector: 'ionfabo-shop-settings',
  templateUrl: './shop-settings.component.html',
  styleUrls: ['./shop-settings.component.scss']
})
export class ShopSettingsComponent implements OnInit {
  settings = new ShopSettings({})
  currentSetting = new ShopSettings({})

  constructor(
    private shopSettingsService: ShopSettingsService,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private fb: FormBuilder,
    private toast: ToastService
  ) {}

  async ngOnInit() {
    this.settings = await this.shopSettingsService.getSetting();
    this.currentSetting = {...this.settings};
  }

  get tryOnOptionsShipping() {
    return TRY_ON_OPTIONS_SHIPPING;
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  async createOrUpdateAddress() {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type: null,
        address: {...this.currentSetting.return_address} || new Address({})
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async data => {
      if (data.data?.address) {
        const _address = data.data?.address;
        await this.updateReturnAddress(_address);
      }
    });
    await modal.present().then();
  }


  async updateReturnAddress(address) {
    try {
      let _settings = await this.shopSettingsService.updateSetting({
        ...this.currentSetting,
        return_address: address
      })
      this.currentSetting = {..._settings};
      this.settings.return_address = _settings?.return_address
      this.toast.success('Cập nhật địa chỉ trả hàng thành công.')
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in updateReturnAddress', e);
    }
  }

  async update() {
    try {
      await this.shopSettingsService.updateSetting({
        ...this.currentSetting,
        ...this.settings,
        weight: Number(this.settings.weight)
      })
      this.toast.success('Cập nhật thông tin thành công.')
    } catch (e) {
      this.toast.error(e.message);
      debug.log('ERROR in update', e);
    }
  }

  addressDisplay(address) {
    if (!address) {
      return null
    }
    return address.full_name + ', ' + address.address1 + ', ' + address.ward + ', ' + address.district + ', ' + address.province
  }
}
