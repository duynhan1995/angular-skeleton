import { Component, OnInit, NgZone, Input } from '@angular/core';
import { ActionSheetController, AlertController, NavController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { Account, FilterOperator, Invitation } from '@etop/models';
import { AuthorizationApi } from '@etop/api';
import { ToastService } from '../../../services/toast.service';
import {resetStores} from "@datorama/akita";
import { Router } from '@angular/router';

@Component({
  selector: 'etop-list-store',
  templateUrl: './list-store.component.html',
  styleUrls: ['./list-store.component.scss']
})
export class ListStoreComponent implements OnInit {
  @Input() tab;
  accounts: any[] = [];
  currentShop: any = {};
  shopId;
  segment = 'owner';
  listInvite: any;
  userInvitations: Invitation[] = [];
  loading = true;

  constructor(
    private auth: AuthenticateStore,
    private alertController: AlertController,
    private zone: NgZone,
    private navCtrl: NavController,
    private userService: UserService,
    private authorizationApi: AuthorizationApi,
    private toast: ToastService,
    private actionSheetController: ActionSheetController,
    private router: Router,
  ) {}

  ngOnInit() {
    this.accounts = this.auth.snapshot.accounts;
    this.currentShop = this.auth.snapshot.account;
    this.shopId = this.currentShop.id;
  }

  async ionViewDidEnter() {
    this.loading = true;
    if (this.tab) {
      this.segment = this.tab;
    }
    this.currentShop = this.auth.snapshot.account;
    this.shopId = this.currentShop.id;
    await this.prepareSession();
    await this.getUserInvitations();
  }
  async prepareSession() {
    this.accounts = this.auth.snapshot.accounts;
    this.loading = false;
  }
  dismiss() {
    this.navCtrl.back();
  }

  isOwner(account) {
    if (account.permission) {
      return account.permission.roles.indexOf('owner') != -1;
    }
  }

  async getUserInvitations() {
    try {
      this.userInvitations = [];
      const date = new Date();
      const dateString = date.toISOString();
      let userInvitations = await this.authorizationApi.getUserInvitations({
        filters: [
          {
            name: 'status',
            op: FilterOperator.eq,
            value: 'Z'
          },
          {
            name: 'expires_at',
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
      this.userInvitations = userInvitations.filter(user => user.phone || user.email);
      debug.log('userInvitations', this.userInvitations);
    } catch (e) {
      debug.error('ERROR in getting Invitations of User', e);
    }
  }

  segmentChanged(event) {
    this.segment = event;
  }

  rolesDisplay(roles: string[]) {
    if (roles && roles.length) {
      return roles.map(r => AuthorizationApi.roleMap(r)).join(', ');
    }
    return 'Chủ shop';
  }
  async switchShop(event, account) {
    const alert = await this.alertController.create({
      header: 'Chuyển đổi cửa hàng.',
      message: `Bạn có chắc muốn chuyển sang cửa hàng <strong>"${account.shop.name}"</strong>`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            event.target.checked = false;
          }
        },
        {
          text: 'Truy cập',
          cssClass: 'text-primary font-12',
          handler: async () => {
            const index = this.auth.findAccountIndex(account.id)
            this.auth.selectAccount(index);
            let res =  await this.userService.switchAccount(account.id);
            this.auth.updateToken(res.access_token);
            this.auth.updatePermissions(res.account.user_account.permission);
            resetStores();
            this.zone.run(async () => {
              await this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug || index}/tab-nav/messages`)
            });
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async acceptInvitation(invitation) {
    try {
      await this.authorizationApi.acceptInvitation(invitation.token);
      this.zone.run(async () => {
        let _sessionInfo = await this.userService.checkToken(this.auth.snapshot.token);
        let _account = _sessionInfo?.available_accounts?.find(account => account.id = invitation.shop.id);
        const accRes = await this.userService.switchAccount(_account.id);
        _account = new Account({
            ..._account,
            token: accRes.access_token,
            shop: accRes.shop,
            id: accRes.shop && accRes.shop.id,
            image_url: _account.image_url,
            display_name: `${_account.code} - ${_account.name}`,
            permission: accRes.account.user_account.permission
        })
        let accounts = this.auth.snapshot.accounts;
        this.auth.updateAccounts(accounts.concat(_account))
        this.prepareSession();
        await this.getUserInvitations();
      });
    } catch (e) {
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Accepting Invitation', e);
    }
  }

  async reject(invitation) {
    const alert = await this.alertController.create({
      header: 'Từ chối tham gia quản trị',
      message: `Bạn có chắc từ chối lời mời từ cửa hàng "<strong>${invitation.shop.name}</strong>".`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Từ chối',
          cssClass: 'text-danger',
          handler: () => {
            this.rejectInvitation(invitation);
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  async rejectInvitation(invitation) {
    try {
      await this.authorizationApi.rejectInvitation(invitation.token);
      this.zone.run(async () => {
        await this.getUserInvitations();
      });
    } catch (e) {
      if (e.code == 'failed_precondition') {
        this.toast.error(e.message || e.msg);
      } else {
        this.toast.error('Có lỗi xảy ra. Vui lòng thử lại!');
      }
      debug.error('ERROR in Rejecting Invitation', e);
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Thao tác',
      cssClass: 'my-custom-class',
      buttons: [
      {
        text: 'Rời khỏi Cửa hàng',
        role: 'destructive',
        handler: async () => {
          this.toast.warning('Tính năng đang phát triển!');
        }
      },
      {
        text: 'Đóng',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
