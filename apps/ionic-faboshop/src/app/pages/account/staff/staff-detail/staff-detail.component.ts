import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthorizationApi } from '@etop/api';
import { ToastService } from '../../../../services/toast.service';

@Component({
  selector: 'etop-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.scss']
})
export class StaffDetailComponent implements OnInit {
  @Input() staff;
  full_name = '';
  roles: any = [];

  roleList = [
    { name: 'Bán hàng', value: 'salesman', checked: false },
    { name: 'Quản lý nhân viên', value: 'staff_management', checked: false }
  ];
  constructor(
    private modalCtrl: ModalController,
    private authorizationAPI: AuthorizationApi,
    private toast: ToastService,
  ) {}

  ngOnInit(): void {
    this.full_name = this.staff.full_name;
    this.roleList.map(role => {
      if (this.staff.roles.includes(role.value)) {
        role.checked = true;
      }
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async updateStaff() {
    this.roles = [];
    this.roleList.forEach(role => {
      if (role.checked) {
        this.roles.push(role.value);
      }
    });
    try {
      await this.authorizationAPI.updateRelationship(
        this.full_name,
        this.staff.user_id
      );
      await this.authorizationAPI.updatePermission(
        this.roles,
        this.staff.user_id
      );
      this.toast.success('Cập nhật nhân viên thành công.');
      this.modalCtrl.dismiss(this);
    } catch (e) {
      this.toast.error(e.message);
      debug.log('error updateStaff', e);
    }
  }
}
