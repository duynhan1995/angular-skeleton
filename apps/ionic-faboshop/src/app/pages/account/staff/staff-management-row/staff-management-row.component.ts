import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Relationship } from 'libs/models/Authorization';
import { ActionSheetController, AlertController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { StaffDetailComponent } from '../staff-detail/staff-detail.component';
import { AuthorizationApi } from '@etop/api';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import {AuthorizationService} from "@etop/state/authorization";

@Component({
  selector: 'etop-staff-management-row',
  templateUrl: './staff-management-row.component.html',
  styleUrls: ['./staff-management-row.component.scss']
})
export class StaffManagementRowComponent implements OnInit {
  @Input() staff = new Relationship();
  @Output() deleteStaff = new EventEmitter();

  constructor(
    private authorizationServcie: AuthorizationService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private routerOutlet: IonRouterOutlet,
    private alertController: AlertController,
    private authorizationAPI: AuthorizationApi,
    private toast: ToastService
  ) {}

  get isOwner() {
    return this.staff.roles.indexOf('owner') != -1;
  }

  ngOnInit() {}

  async staffDetail(staff) {
    const modal = await this.modalController.create({
      component: StaffDetailComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        staff
      },
      animated: false
    });
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.authorizationServcie.updateRelationship();
      }
    });
    return await modal.present();
  }

  async presentActionSheet(staff) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Thao tác',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Chỉnh sửa',
        handler: async () => {
          await this.staffDetail(staff);
        }
      },
      {
        text: 'Xóa khỏi shop',
        role: 'destructive',
        handler: async () => {
          await this.delStaff();
        }
      },
      {
        text: 'Đóng',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async delStaff() {
    const alert = await this.alertController.create({
      header: 'Xóa lời mời',
      message: `Bạn có chắc xóa nhân viên "<strong>${this.staff.full_name}</strong>".`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger',
          handler: () => {
            this.removeStaff();
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async removeStaff() {
    try {
      await this.authorizationAPI.removeUserFromAccount(this.staff.user_id);
      this.authorizationServcie.removeRelationship();
      this.deleteStaff.emit();
      this.toast.success('Xoá nhân viên thành công.');
    } catch (e) {
      this.toast.error(e.message);
      debug.error('ERROR in removing Staff', e);
    }
  }
}
