import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { StaffComponent } from './staff.component';
import { AddStaffModalComponent } from './add-staff-modal/add-staff-modal.component';
import { AuthenticateModule } from '@etop/core';
import { StaffManagementRowComponent } from './staff-management-row/staff-management-row.component';
import { UserInvitationRowComponent } from './user-invitation-row/user-invitation-row.component';
import { StaffDetailComponent } from './staff-detail/staff-detail.component';
import { EtopMaterialModule } from '@etop/shared';
import { EmptyPermissionModule } from '../../../components/empty-permission/empty-permission.module';

const routes: Routes = [
  {
    path: '',
    component: StaffComponent
  }
];

@NgModule({
  declarations: [
    StaffComponent,
    AddStaffModalComponent,
    StaffManagementRowComponent,
    UserInvitationRowComponent,
    StaffDetailComponent
  ],
  entryComponents: [AddStaffModalComponent, StaffDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AuthenticateModule,
    ReactiveFormsModule,
    EtopMaterialModule,
    EmptyPermissionModule
  ],
  exports: [StaffComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StaffModule {}
