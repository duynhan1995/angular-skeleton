import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, NavController, Platform } from '@ionic/angular';
import { ShopInfoModel } from 'apps/ionic-faboshop/src/models/ShopInfo';
import { BaseComponent, AuthenticateStore } from '@etop/core';
import { TelegramService } from '../../../services/telegram.service';
import { ToastService } from '../../../services/toast.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ExtendedAccount, Account } from 'libs/models/Account';
import { LoadingService } from '../../../services/loading.service';
import { UserService } from 'apps/core/src/services/user.service';
import { Plugins } from '@capacitor/core';
import {LocationQuery, LocationService} from '@etop/state/location';

const { Device } = Plugins;

@Component({
  selector: 'etop-new-shop',
  templateUrl: './new-shop.component.html',
  styleUrls: ['./new-shop.component.scss']
})
export class NewShopComponent extends BaseComponent implements OnInit {
  business = [
    {
      value: 'Thời trang',
      text: 'Thời trang'
    },
    {
      value: 'Giày dép, túi xách',
      text: 'Giày dép, túi xách'
    },
    {
      value: 'Đồng hồ, mắt kính, phụ kiện thời trang',
      text: 'Đồng hồ, mắt kính, phụ kiện thời trang'
    },
    {
      value: 'Mẹ và bé',
      text: 'Mẹ và bé'
    },
    {
      value: 'Hoa, quà tặng',
      text: 'Hoa, quà tặng'
    },
    {
      value: 'Nội thất, gia dụng',
      text: 'Nội thất, gia dụng'
    },
    {
      value: 'Công nghệ, phụ kiện công nghệ',
      text: 'Công nghệ, phụ kiện công nghệ'
    },
    {
      value: 'Mỹ phẩm',
      text: 'Mỹ phẩm'
    },
    {
      value: 'Sách, văn phòng phẩm',
      text: 'Sách, văn phòng phẩm'
    },
    {
      value: 'Nguyên vật liệu - Phụ liệu',
      text: 'Nguyên vật liệu - Phụ liệu'
    },
    {
      value: 'Nông sản, thực phẩm',
      text: 'Nông sản, thực phẩm'
    },
    {
      value: 'Thủ công mỹ nghệ',
      text: 'Thủ công mỹ nghệ'
    },
    {
      value: 'Ô tô, xe máy, linh kiện điện tử',
      text: 'Ô tô, xe máy, linh kiện điện tử'
    },
    {
      value: 'Nhà thuốc',
      text: 'Nhà thuốc'
    },
    {
      value: 'others',
      text: 'Khác'
    }
  ];

  provinces = [];
  districts = [];
  wards = [];
  shop = new ShopInfoModel();

  vtiger_obj = {
    user: {},
    shop: {},
    address: {}
  };

  business_lines;
  other_lines = '';
  source;

  constructor(
    private modalCtrl: ModalController,
    private telegramService: TelegramService,
    private toastService: ToastService,
    private util: UtilService,
    private auth: AuthenticateStore,
    private userService: UserService,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private zone: NgZone,
    private platform: Platform,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
  ) {
    super();
  }

  ngOnInit() {
    this._prepareLocationData();
    if (this.platform.is('ios')) {
      this.source = localStorage.setItem('REF', 'etop_app_ios');
    } else {
      this.source = localStorage.setItem('REF', 'etop_app_android');
    }
  }

  private _prepareLocationData() {
    this.provinces = this.locationQuery.getValue().provincesList;
  }

  onProvinceSelected() {
    this.shop.address.district_code = null;
    this.districts = this.locationService.filterDistrictsByProvince(this.shop.address.province_code);
  }

  onDistrictSelected() {
    this.shop.address.ward_code = null;
    this.wards = this.locationService.filterWardsByDistrict(this.shop.address.district_code);
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  onChangeSurvey() {
    debug.log('business_lines', this.business_lines);
  }

  async createShop() {
    try {
      if (!this.shop.name) {
        return this.toastService.error(`Vui lòng điền tên cửa hàng`);
      }
      let phone = this.shop.phone;
      phone = (phone && phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      phone = (phone && phone.split('-test')[0]) || '';
      // tslint:disable-next-line: max-line-length
      if (
        this.shop.website_url &&
        !this.shop.website_url.match(
          /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        )
      ) {
        return this.toastService.error(`Vui lòng điền địa chỉ website hợp lệ`);
      }
      this.shop.phone = this.util.cleanNumberString(this.shop.phone);
      let email = this.shop.email;
      email = (email && email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
      email = (email && email.split('-test')[0]) || '';
      if (
        email &&
        !email.match(
          // tslint:disable-next-line: max-line-length
          /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
      ) {
        return this.toastService.error('Vui lòng nhập email hợp lệ!');
      }

      if (!this.business_lines || (this.other_lines == '' && this.business_lines == 'others')) {
        return this.toastService.error(`Ngành hàng kinh doanh không được trống!`);
      }

      if (!this.shop.address.address1) {
        return this.toastService.error(`Vui lòng nhập địa chỉ cửa hàng`);
      }

      const province = this.provinces.find(
        p => p.code == this.shop.address.province_code
      );
      if (!this.shop.address.province_code || !province) {
        return this.toastService.error(`Vui lòng chọn tỉnh thành`);
      }
      this.shop.address.province = province.name;

      const district = this.districts.find(
        d => d.code == this.shop.address.district_code
      );
      if (!this.shop.address.district_code || !district) {
        return this.toastService.error(`Vui lòng chọn quận huyện`);
      }
      this.shop.address.district = district.name;

      const ward = this.wards.find(w => w.code == this.shop.address.ward_code);
      if (!this.shop.address.ward_code) {
        return this.toastService.error(`Vui lòng chọn phường xã`);
      }
      this.shop.url_slug = this.util.createHandle(this.shop.name);
      const slugExisted = this.auth.snapshot.accounts.filter(
        account => account.url_slug === this.shop.url_slug
      );
      debug.log('slugExisted', slugExisted);
      if (slugExisted.length > 0) {
        let index: any = this.auth.snapshot.accounts.findIndex(
          account => account.url_slug === this.shop.url_slug
        );
        index = index.toString();
        this.shop.url_slug += index ? '-' + index : '-0';
      }

      this.shop.address.ward = ward.name;

      const shopData: any = {
        name: this.shop.name,
        phone: this.shop.phone,
        website_url: this.shop.website_url,
        email: this.shop.email,
        address: this.shop.address,
        image_url: this.shop.image_url,
        url_slug: this.shop.url_slug,
        auto_create_ffm: true
      };
      await this.loadingService.start('Đang xử lý');
      const shopAccount = await this._createShopAccount(shopData);
      this.auth.addAccount(shopAccount);
      this.updateInfo(shopAccount);
      this.auth.updateShop(shopAccount.shop, true);
      await this.userService.checkToken(this.auth.snapshot.token);
      this.loadingService.end();
      this.zone.run(async () => {
        this.modalCtrl.dismiss(this.auth.snapshot.account);
        return await this.navCtrl.navigateForward(
          `/s/${this.auth.snapshot.account.url_slug || 0}/orders`,
          { animated: false }
        );
      });
    } catch (e) {
      await this.toastService.error(e.message);
      this.loadingService.end();
    }
  }

  updateInfo(shopAccount) {
    this.auth.updateInfo({
      token: shopAccount.token,
      account: shopAccount.account,
      session: shopAccount.session,
      isAuthenticated: true
    });
  }

  private async _createShopAccount(shopData) {
    const survey_info = [
      {
        key: 'business',
        question: 'Ngành hàng kinh doanh',
        answer:
          this.business_lines == 'others'
            ? this.other_lines
            : this.business_lines
      }
    ];
    shopData.survey_info = survey_info;
    shopData.money_transaction_rrule = 'FREQ=WEEKLY;BYDAY=TU,TH';
    const res = await this.userService.registerShop(shopData);
    const shop = new ExtendedAccount(res.shop);
    const accountRes = await this.userService.switchAccount(
      shop.id,
      'ON BOARDING COMPONENT'
    );
    const { access_token, account } = accountRes;
    this.vtiger_obj = {
      user: accountRes.user,
      shop,
      address: {}
    };
    shop.token = access_token;
    const newAccount = new Account(account);
    newAccount.token = access_token;
    newAccount.shop = shop;
    const shopAddress = this.telegramService.formatAddress(shopData.address);
    const info = await Device.getInfo();
    const survey = {
      business_lines: this.business_lines = survey_info[0].answer
    };
    localStorage.setItem('survey', JSON.stringify(survey));
    this.telegramService.newShopMessage(
      accountRes.user,
      shop,
      shopAddress,
      survey_info,
      info
    );
    return newAccount;
  }
}
