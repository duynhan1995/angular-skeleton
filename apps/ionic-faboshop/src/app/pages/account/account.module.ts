import { CUSTOM_ELEMENTS_SCHEMA, NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AccountComponent } from './account.component';
import { SettingsComponent } from '../settings/settings.component';
import { ListStoreComponent } from './list-store/list-store.component';
import { ProfileComponent } from '../profile/profile.component';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { NewShopComponent } from './new-shop/new-shop.component';
import { AuthenticateModule } from '@etop/core';
import { CarrierConnectionComponent } from './carrier-connection/carrier-connection.component';
import { FacebookConnectComponent } from '../../components/facebook-connect/facebook-connect.component';
import { CarrierConnectionItemComponent } from './carrier-connection/components/carrier-connection-item/carrier-connection-item.component';
import { CarrierConnectModule } from '../../components/carrier-connect/carrier-connect.module';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { EmptyModule } from '../../components/empty/empty.module';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { PopoversModule } from '../../components/popovers/popovers.module';
import { CreatePostComponent } from './post/post.component';
import { PostService } from '@etop/features/fabo/post/post.service';
import { NotificationSettingComponent } from './notification-setting/notification-setting.component';
import { NotifySettingService } from '@etop/features';
import { NotifySettingApi } from '@etop/api';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { TagSettingComponent } from './tag-setting/tag-setting.component';
import { CreateAndUpdateTagModule } from '../../components/create-and-update-tag/create-and-update-tag.module';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { StaffComponent } from './staff/staff.component';
import { StaffModule } from './staff/staff.module';
import { TemplateMessageComponent } from './template-message/template-message.component';
import { CreateTemplateComponent } from './template-message/components/create-template/create-template.component';
import { MessageMenuComponent } from './message-menu/message-menu.component';
import { ShippingMenuComponent } from './shipping-menu/shipping-menu.component';
import { ShopSettingsComponent } from './shop-settings/shop-settings.component';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { IonInputFormatNumberModule } from '@etop/shared';
import { ProductListComponent } from '../products/product-list/product-list.component';
import { ProductsModule } from '../products/products.module';
import { ProductListModule } from '../products/product-list/product-list.module';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'profile', component: ProfileComponent },
      { path: 'facebook-connect', component: FacebookConnectComponent },
      { path: 'carrier', component: CarrierConnectionComponent },
      { path: 'post', component: CreatePostComponent },
      { path: 'address', component: DeliveryAddressComponent },
      { path: 'notification', component: NotificationSettingComponent },
      { path: 'tags', component: TagSettingComponent },
      { path: 'list-store', component: ListStoreComponent },
      { path: 'staff', component: StaffComponent },
      { path: 'template-message', component: TemplateMessageComponent },
      { path: 'message', component: MessageMenuComponent },
      { path: 'shipping', component: ShippingMenuComponent },
      { path: 'shop-settings', component: ShopSettingsComponent },
      { path: 'products', component: ProductListComponent },
    ]
  }
];

@NgModule({
  declarations: [
    AccountComponent,
    SettingsComponent,
    ListStoreComponent,
    ProfileComponent,
    NewShopComponent,
    CarrierConnectionComponent,
    CreatePostComponent,
    CarrierConnectionItemComponent,
    DeliveryAddressComponent,
    NotificationSettingComponent,
    TagSettingComponent,
    TemplateMessageComponent,
    CreateTemplateComponent,
    MessageMenuComponent,
    ShippingMenuComponent,
    ShopSettingsComponent
  ],
  entryComponents: [
    SettingsComponent,
    ListStoreComponent,
    ProfileComponent,
    NewShopComponent,
    CreateTemplateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    CarrierConnectModule,
    EmptyModule,
    PopoversModule,
    CreateAndUpdateTagModule,
    RouterModule.forChild(routes),
    SelectSuggestModule,
    StaffModule,
    IonInputFormatNumberModule,
    ProductsModule,
    ProductListModule
  ],
  providers: [
    MobileUploader,
    PostService,
    ConnectionService,
    ConnectionStore,
    ConfirmOrderStore,
    ConfirmOrderService,
    NotifySettingService,
    NotifySettingApi,
    OpenNativeSettings,
    ShopSettingsService
  ],
  exports: [AccountComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccountModule {}
