import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'ionfabo-shipping-menu',
  templateUrl: './shipping-menu.component.html',
  styleUrls: ['./shipping-menu.component.scss']
})
export class ShippingMenuComponent implements OnInit {

  listItems = [
    {
      icon: 'cube-outline',
      title: 'Kết nối nhà vận chuyển',
      link: 'carrier',
      permissions: ['shop/connection:view']
    },
    {
      icon: 'location-outline',
      title: 'Địa chỉ lấy hàng',
      link: 'address',
      permissions: ['shop/settings/shipping_setting:view']
    },
    {
      icon: 'options-outline',
      title: 'Thông số mặc định',
      link: 'shop-settings',
      permissions: ['shop/settings/shipping_setting:update']
    }
  ];
  
  constructor(
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private util: UtilService
  ) { }

  ngOnInit(): void {
  }

  async page(link) {
    await this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/account/${link}`);
  }

}
