import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActionSheetController, AlertController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { CreateAndUpdateTagComponent } from '../../../components/create-and-update-tag/create-and-update-tag.component';
import { FacebookUserTagQuery, FacebookUserTagService } from '@etop/state/fabo/facebook-user-tag';
import { ToastService } from '../../../services/toast.service';
import { FbUserTag } from '@etop/models';

@Component({
  selector: 'ionfabo-tag-setting',
  templateUrl: './tag-setting.component.html',
  styleUrls: ['./tag-setting.component.scss']
})
export class TagSettingComponent implements OnInit {
  tags$ = this.fbUserTagQuery.selectAll();

  empty = {
    title: 'Chưa có thẻ',
    subtitle: 'Vui lòng tạo thẻ mới',
    action: 'Tạo thẻ',
    buttonClick: this.createTag.bind(this)
  };

  constructor(
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private fbUserTagQuery: FacebookUserTagQuery,
    private fbUserTagService: FacebookUserTagService,
    private toast: ToastService,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    await this.fbUserTagService.initState(true);
  }

  async presentActionSheet(tag) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Thao tác',
      buttons: [
        {
          text: 'Chỉnh sửa',
          handler: () => {
            this.createTag(tag);
          }
        },
        {
          text: 'Xóa',
          role: 'destructive',
          handler: () => {
            this.presentAlertConfirm(tag);
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    await actionSheet.present();
  }

  async presentAlertConfirm(tag) {
    const alert = await this.alertController.create({
      header: 'Xóa thẻ',
      message: `Bạn chắc chắn muốn xóa thẻ <strong>${tag.name}</strong>?`,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {}
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger',
          handler: async () => {
            await this.fbUserTagService.deleteTag(tag.id);
            this.changeDetector.detectChanges();
            this.toast.success('Xóa thẻ thành công');
          }
        }
      ]
    });

    await alert.present();
  }

  async createTag(tag?: FbUserTag) {
    const _tag = { ...tag };
    const modal = await this.modalCtrl.create({
      component: CreateAndUpdateTagComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { tag: _tag }
    });
    return await modal.present();
  }
}
