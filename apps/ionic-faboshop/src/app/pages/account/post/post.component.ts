import { BaseComponent } from '@etop/core';
import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, PopoverController } from '@ionic/angular';
import { FbPage, FilterOperator } from '@etop/models';
import { PageApi } from '@etop/api';
import { PostService } from '@etop/features/fabo/post/post.service';
import { Plugins } from '@capacitor/core';
import { FacebookLoginResponse } from 'capacitor-facebook-plugin';
import { ToastService } from '../../../services/toast.service';
import { ChatConnectionModalComponent } from '../../../components/chat-connection-modal/chat-connection-modal.component';

@Component({
  selector: 'create-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})

export class CreatePostComponent extends BaseComponent implements OnInit{
  pages: FbPage[] = [];
  slected_page_id ='';
  loading = false;
  content ='';
  constructor(
  private modalCtrl:ModalController,
  private toast:ToastService,
  private popoverController: PopoverController,
  private alertController:AlertController,
  private pageApi:PageApi,
  private postService:PostService,
  private navController: NavController,
  ) {
    super();
  }

  async ngOnInit() {
    await this._reload();
    if(this.pages?.length){
      this.slected_page_id = this.pages[0].external_id;
    }
  }
  async _reload() {
    try {
      this.pages = await this.pageApi.listPages({
        filters: [{ name: 'status', op: FilterOperator.eq, value: 'P' }]
      });
    } catch(e) {
      debug.error('ERROR in list FanPages', e);
    }
  }
  dismiss() {
    this.navController.back();
  }
  async fbConnect() {
    this.loading = true;
    try {
      const FB = Plugins.FacebookPlugin;
      await FB.logout();
      const res: FacebookLoginResponse = await FB.login({
        permissions: 'pages_show_list,pages_messaging,pages_manage_metadata,pages_read_engagement,pages_read_user_content,pages_manage_engagement,pages_manage_posts'
      });
      const token = res.accessToken && res.accessToken.token;
      if (token) {
        const connectionResult = await this.pageApi.connectPages(token);
        await this.openModalConnectionResult(connectionResult);
      }
    } catch(e) {
      debug.error('ERROR in loginFacebook', JSON.stringify(e));
      if (e.code) {
        this.toast.error(`Có lỗi xảy ra khi kết nối với Facebook. ${e.message || e.msg}`);
      }
    }
    this.loading = false;
  }
  async openModalConnectionResult(connectionResult) {
    const popover = await this.popoverController.create({
      component: ChatConnectionModalComponent,
      translucent: true,
      componentProps: {
        connectionResult
      },
      cssClass: 'select-variant chat-connection',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    popover.onDidDismiss().then(async() => {
      await this.listPages();
    });
    return await popover.present();
  }
  async listPages() {
    this.loading = true;
    try {
      this.pages = await this.pageApi.listPages({
        filters: [
          { name: 'status', op: FilterOperator.eq, value: 'P' }
        ]
      });
    } catch(e) {
      debug.error('ERROR in listPages', e);
    }
    this.loading = false;
  }
  selectPageName(){
    for (let i=0;i<this.pages.length;i++){
      if (this.pages[i].external_id == this.slected_page_id){
        return this.pages[i].external_name;
      }
    }
  }
  async createFbPost(){
    try{
      const post = await this.postService.createPost(this.slected_page_id,this.content)
      if(this.content == ''){
        this.toast.error('Vui lòng nhập nội dung bài viết')
      }
      this.presentAlert(post.external_url);
    }catch (e) {
      debug.error('ERROR in creating post', e);
      const msg =
        (e.code == 'failed_precondition' && e.message) ||
        'đăng bài viết thất bại';
      this.toast.error(msg);
    }
  }
  async presentAlert(postUrl) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Thành công',
      message: 'Tạo bài viết thành công trên Page ' + this.selectPageName(),
      backdropDismiss: false,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium',
        }, {
          text: 'Xem trên Facebook',
          handler: () => {
            window.open(postUrl,'_blank');
          }
        }
      ]
    });

    await alert.present();
  }
}
