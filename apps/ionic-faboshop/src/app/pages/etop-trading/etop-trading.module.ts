import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EtopTradingComponent } from './etop-trading.component';

const routes: Routes = [
  {
    path: '',
    component: EtopTradingComponent
  }
];

@NgModule({
  declarations: [EtopTradingComponent],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [EtopTradingComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EtopTradingModule {}
