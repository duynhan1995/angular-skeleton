import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FulfillmentAPI, FulfillmentApi, OrderAPI, OrderApi, CustomerApi } from '@etop/api';
import {Order} from 'libs/models/Order';
import {CustomerAddress, CustomerEvent} from 'libs/models/Customer';
import {ToastService} from 'apps/ionic-faboshop/src/app/services/toast.service';
import {LoadingService} from 'apps/ionic-faboshop/src/app/services/loading.service';
import {ConnectionService} from '@etop/features/connection/connection.service';
import {Connection} from 'libs/models/Connection';
import {ConfirmOrderStore} from '@etop/features/fabo/confirm-order/confirm-order.store';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { debounceTime, distinctUntilChanged, map, pairwise, takeUntil } from 'rxjs/operators';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConfirmOrderService} from '@etop/features/fabo/confirm-order/confirm-order.service';
import {Fulfillment} from 'libs/models/Fulfillment';
import {PopoverController, NavController, ModalController, IonRouterOutlet, Platform} from '@ionic/angular';
import {CreateCustomerPopupComponent} from 'apps/ionic-faboshop/src/app/components/popovers/create-customer-popup/create-customer-popup.component';
import {UtilService} from 'apps/core/src/services/util.service';
import { ListAddressesPopupComponent } from '../../components/popovers/list-addresses-popup/list-addresses-popup.component';
import { Address, FbCustomerReturnRate } from '@etop/models';
import { CreateUpdateAddressPopupComponent } from '../../components/popovers/create-update-address-popup/create-update-address-popup.component';
import { AddressDisplayPipe } from '@etop/shared';
import { District, Province, Ward } from '../../../../../../libs/models/Location';
import { ConversationsQuery, ConversationsService, ConversationsStore } from '@etop/state/fabo/conversation';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import {LocationQuery, LocationService} from '@etop/state/location';
import { OrdersService } from '@etop/state/order';
import { EventBus } from '@etop/common';
import { CustomersQuery } from '@etop/state/fabo/customer';
import { RiskCustomerWarningComponent } from '../../components/popovers/risk-customer-warning/risk-customer-warning.component';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { ShopSettings } from '@etop/models/ShopSettings';

@Component({
  selector: 'etop-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.scss']
})
export class ConfirmOrderComponent extends BaseComponent implements OnInit {
  @Input() type: 'from' | 'to' = 'from';
  toAddress$ = this.confirmOrderStore.state$.pipe(map(s => s?.activeToAddress));
  fromAddress$ = this.confirmOrderStore.state$.pipe(map(s => s?.fromAddresses));
  showProduct = true;
  showShipping = true;

  connections: Connection[];
  customerAddresses: CustomerAddress[] = [];

  provincesList: Province[] = [];
  districtsList: District[] = [
    { id: null, code: null, name: 'Chưa chọn tỉnh thành' }
  ];
  wardsList: Ward[] = [{ id: null, code: null, name: 'Chưa chọn quận huyện' }];

  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0]);
  selectedCustomer = false;
  connectionInitializing = true;
  customerInitializing = true;
  popover: HTMLIonPopoverElement;
  dropDown = false;
  order$ = this.confirmOrderStore.state$.pipe(map(s => s?.order));
  shippingAddressDisplay;
  orderForm = this.fb.group({
    customer: this.fb.group({
      full_name: ['', Validators.required],
      phone: ['', Validators.required],
      province_code: ['', Validators.required],
      address1: ['', Validators.required],
      district_code: ['', Validators.required],
      ward_code: ['', Validators.required]
    }),
    lines: [],
    order_discount: null,
    fee_lines: this.fb.array([
      this.fb.group({
        type: 'other',
        name: 'Phụ thu',
        amount: null
      })
    ]),
    basket_value: 0,
    total_items: 0,
    total_discount: 0,
    total_fee: 0,
    total_amount: 0
  });
  _checkFocus = false;
  submitted = false;
  createPopover = null;
  oldShippingAddress = null;
  customerReturnRating: FbCustomerReturnRate;
  messageError: any;
  loadingConfirm = false;
  manualMessages: any;
  displayMsgPhone = false;
  displayMsgAddress = false;
  shopSettings: ShopSettings;

  constructor(
    private auth: AuthenticateStore,
    private conversationsService: ConversationsService,
    private conversationsQuery: ConversationsQuery,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private loading: LoadingService,
    private toast: ToastService,
    private connectionService: ConnectionService,
    private customersQuery: CustomersQuery,
    private addressDisplay: AddressDisplayPipe,
    private orderApi: OrderApi,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderStore: ConfirmOrderStore,
    private fulfillmentApi: FulfillmentApi,
    private popoverController: PopoverController,
    private util: UtilService,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private shopService: ShopService,
    private customerApi: CustomerApi,
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private orderService: OrdersService,
    private eventBus: EventBus,
    private platform: Platform,
    private shopSettingService: ShopSettingsService,
  ) {
    super();
  }

  get lines() {
    return this.orderForm.controls.lines as FormArray;
  }

  get feeLines() {
    return this.orderForm?.controls.fee_lines as FormArray;
  }

  get customerForm() {
    return this.orderForm?.controls.customer as FormGroup;
  }

  get pickupAddressDisplay() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    return this.addressDisplay.transform(ffm.pickup_address);
  }

  get f() {
    return this.customerForm.controls;
  }
  private validateOrderInfo(order: Order): boolean {
    const { total_amount, lines, customer } = order;

    if (!customer.full_name || !customer.full_name.trim()) {
      this.toast.error('Chưa nhập tên khách hàng').then();
      return false;
    }

    if (!customer.phone || !customer.phone.trim()) {
      this.toast.error('Chưa nhập số điện thoại khách hàng').then();
      return false;
    }

    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];
      if (!line.product_name || !line.product_name.trim()) {
        this.toast.error(`Chưa nhập tên cho sản phẩm thứ ${i + 1}`).then();
        return false;
      }
      if (!line.payment_price && line.payment_price != 0) {
        this.toast.error(`Chưa nhập đơn giá cho sản phẩm thứ ${i + 1}`).then();
        return false;
      }
      if (!line.quantity && line.quantity != 0) {
        this.toast.error(`Chưa nhập số lượng cho sản phẩm thứ ${i + 1}`).then();
        return false;
      }
    }

    if (total_amount < 0) {
      this.toast.error('Thành tiền không được âm.').then();
      return false;
    }

    return true;
  }

  private validateFulfillmentInfo(ffm: Fulfillment): boolean {
    const {
      pickup_address,
      shipping_address,
      chargeable_weight,
      shipping_service_code
    } = ffm;

    if (!pickup_address?.district_code) {
      this.toast.error('Chưa chọn địa chỉ lấy hàng.').then();
      return false;
    }

    if (!shipping_address?.district_code) {
      this.toast.error('Chưa chọn địa chỉ giao hàng.').then();
      return false;
    }

    if (!chargeable_weight) {
      this.toast.error('Chưa nhập khối lượng.').then();
      return false;
    }

    if (!shipping_service_code) {
      this.toast.error('Chưa chọn gói vận chuyển.').then();
      return false;
    }

    return true;
  }

  async ngOnInit() {
    this.customerForm.valueChanges.pipe(distinctUntilChanged(), pairwise()).subscribe(([oldValue, newValue]) => {
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      this.displayMsgAddress = false;
      this.displayMsgPhone = false;
      const { province_code,
        address1,
        phone,
        full_name,
        district_code,
        ward_code } = newValue;
      const _shipping_address = {
        ...ffm.shipping_address,
        address1,
        phone,
        full_name,
        district_code,
        ward_code,
        province_code
      }
      if (phone?.length > 9 && phone != oldValue?.phone) {
        this.getCustomerReturnRate();
      }
      
      if (phone?.length < 10) {
        this.customerReturnRating = null;
      }
      if (oldValue.province_code != newValue.province_code) {
        this.confirmOrderService.updateFulfillment({
          ...ffm,
          shipping_address: null
        });
        return;
      }
      if (province_code && district_code && ward_code) {
        this.confirmOrderService.activeToAddress(_shipping_address);
        this.confirmOrderService.updateFulfillment({
          ...ffm,
          shipping_address: _shipping_address
        });
      } else {
        this.confirmOrderService.updateFulfillment({
          ...ffm,
          shipping_address: null
        });
      }
      if (!newValue.address1 && oldValue.address1) {
        this.displayMsgAddress = true;
      }
      if (!newValue.phone?.length && oldValue.phone?.length) {
        this.displayMsgPhone = true;
      }
    })
  };
  async ionViewWillEnter() {
    this.initConnections().then(_ => {
      this.connectionInitializing = false;
    });
    const conversation_id = this.platform.getQueryParam('conversation');
    if (conversation_id) {
      const type = this.platform.getQueryParam('type');
      this.manualMessages = await this.conversationsService.getMessageByConversation(conversation_id, type)
    }
    this.confirmOrderService.updateFulfillment(
      this.confirmOrderStore.initState.fulfillment
    );
    await this.prepareFromAddress();
    await this.prepareToAddress();
    this.getShippingAddressDisplay();
    const shipping_address = this.confirmOrderStore.snapshot.toAddresses[0];
    this.oldShippingAddress = shipping_address;
    this.orderForm.valueChanges.subscribe(order => {
      const { lines, fee_lines, order_discount } = order;
      lines?.forEach(line => {
        if (line.payment_price != null) {
          line.payment_price = Number(line.payment_price);
        }
        if (line.quantity != null) {
          line.quantity = Number(line.quantity);
        }
        line.list_price = line.payment_price;
        line.retail_price = line.payment_price;
      });
      const basketValue =
        (lines &&
          lines.reduce((a, b) => a + b.payment_price * b.quantity, 0)) ||
        0;
      const totalItems =
        (lines && lines.reduce((a, b) => a + b.quantity, 0)) || 0;
      const totalDiscount = Number(order_discount) || 0;
      const totalFee =
        (fee_lines && fee_lines.reduce((a, b) => a + Number(b.amount), 0)) || 0;
      const totalAmount = basketValue - totalDiscount + totalFee;
      order = {
        ...order,
        basket_value: basketValue,
        total_items: totalItems,
        total_discount: totalDiscount,
        total_fee: totalFee,
        total_amount: totalAmount
      };

      // NOTE: Must patch each fields, if not, input-format-number will be flicked
      this.orderForm.patchValue(
        {
          basket_value: basketValue,
          total_items: totalItems,
          total_discount: totalDiscount,
          total_fee: totalFee,
          total_amount: totalAmount
        },
        {
          emitEvent: false
        }
      );

      const _order = this.confirmOrderStore.snapshot.order;
      this.confirmOrderService.updateOrder({
        ..._order,
        ...order
      });
    });
    this.initData();

    this.activeFbUser$.subscribe(fbUser => {
      const customer = this.customersQuery.getActive()
      if (!fbUser && !customer){
        this.resetForm();
      }
    });
    this.customerInitializing = false;
    this.confirmOrderStore.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.initData();
      });
    this.checkCustomer();
    const saveDataOrder: any = this.confirmOrderStore.snapshot.saveDataOrder;
    if (saveDataOrder?.p_data?.is_save) {
      const order: any = this.confirmOrderStore.snapshot.order;
      const { full_name, phone, province_code, address1, district_code, ward_code } = saveDataOrder?.customer;
      this.provincesList = this.locationQuery.getValue().provincesList;
      this.districtsList = this.locationService.filterDistrictsByProvince(province_code || order?.customer?.province_code);
      this.wardsList = this.locationService.filterWardsByDistrict(district_code || order?.customer?.district_code);
      this.customerForm.patchValue({
        phone: phone || order?.customer.phone,
        province_code: province_code || order?.customer?.province_code,
        address1: address1 || order?.customer?.address1,
        district_code: district_code || order?.customer?.district_code,
        ward_code: ward_code || order?.customer?.ward_code,
        full_name: full_name || order?.customer?.full_name
      })
      
      await this.getCustomerReturnRate();
    }
    if (!this.customerForm.controls['phone'].value){
      this.suggestPhoneFormMessage().then();
    }
    this.shopSettings = await this.shopSettingService.getSetting();
    this.loadingConfirm = false;
  }

  async ionViewDidLeave() {
    const order  = this.confirmOrderStore.snapshot.order;
    const ffm  = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateSaveOrder({
      ...order,
      p_data: {
        is_save: true
      }
    });
    this.confirmOrderService.updateSaveFulfillment({
      ...ffm,
      p_data: {
        ...ffm.p_data,
        is_save: true
      }
    });
  }

  selectPhone(e) {
    this.customerForm.patchValue({
      phone: e
    })
    this.displayMsgPhone = false;
  }

  selectAddress(e) {
    this.customerForm.patchValue({
      address1: e
    })
    this.displayMsgAddress = false;
  }

  checkCustomerChanged(oldData, newData) {
    return (
      oldData.full_name != newData.full_name ||
      oldData.phone != newData.phone ||
      oldData.province_code != newData.province_code ||
      oldData.district_code != newData.district_code ||
      oldData.ward_code != newData.ward_code ||
      oldData.address1 != newData.address1
    );
  }

  async prepareFromAddress() {
    await this.confirmOrderService.getFromAddresses();
    const shopDefaultAddressID = this.auth.snapshot.shop.ship_from_address_id;
    const fromAddress = this.confirmOrderStore.snapshot.fromAddresses;
    let pickup_address = this.confirmOrderStore.snapshot.fromAddresses.find(
      address => address.id === shopDefaultAddressID
    );
    if (!pickup_address && fromAddress?.length > 0) {
      pickup_address = fromAddress[0];
    }
    this.confirmOrderService.updatePickupAddress(pickup_address);
    this.confirmOrderService.selectFromAddress(pickup_address);
  }

  async prepareToAddress() {
    this.confirmOrderService.activeCustomerAddress(null);
    await this.confirmOrderService.getToAddresses();
    const shipping_address = this.confirmOrderStore.snapshot.toAddresses[0];
    this.confirmOrderService.updateShippingAddress(shipping_address);
    this.provincesList = this.locationQuery.getValue().provincesList;
    this.districtsList = this.locationService.filterDistrictsByProvince(
      shipping_address?.province_code
    );
    this.wardsList = this.locationService.filterWardsByDistrict(
      shipping_address?.district_code
    );
    this.locationService.filterWardsByDistrict(shipping_address?.district_code);
  }

  customerSelected(customerAddress) {
    this.selectedCustomer = false;
    this.getCustomerAddress(customerAddress);
    const order = this.confirmOrderStore.snapshot.order;
    this.confirmOrderService.activeOrder({
      ...order,
      customer: customerAddress
    });
    this.confirmOrderService.updateShippingAddress(customerAddress);
    this.confirmOrderService.activeCustomerAddress(customerAddress);
    this.customerForm.patchValue({
      phone: customerAddress.phone,
      full_name: customerAddress.full_name
    });
    // this.confirmOrderService.setActiveCustomer({
    //   ...customerAddress,
    //   id: customerAddress.customer_id
    // });
    this.ref.detectChanges();
  }

  resetForm(){
    this.orderForm.controls['customer'].patchValue({
      full_name: '',
      phone: '',
      province_code: '',
      address1: '',
      district_code: '',
      ward_code: ''
    })
    this.orderForm.controls['basket_value'].patchValue('');
    this.lines.patchValue([]);
    this.orderForm.controls['total_amount'].patchValue('');
    this.orderForm.controls['total_fee'].patchValue('');
    this.orderForm.controls['total_items'].patchValue('');
    this.orderForm.controls['fee_lines'].patchValue([]);
    this.initData()
  }

  initData() {
    const _order = this.confirmOrderStore.snapshot.order;
    this.confirmOrderService.updateOrder(
      {
        ..._order,
        id: null
      },
      true
    );
    this.getFormDataFromStore();
  }
  getShippingAddressDisplay() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.getCustomerAddress(ffm.shipping_address);
    this.shippingAddressDisplay = this.addressDisplay.transform(
      ffm.shipping_address
    );
  }

  getFormDataFromStore() {
    const order = this.confirmOrderStore.snapshot.order;
    this.orderForm.patchValue(order);
  }

  onProvinceSelected() {
    this.customerForm.patchValue(
      {
        district_code: '',
        ward_code: ''
      },
      { emitEvent: true }
    );
    this.districtsList = this.locationService.filterDistrictsByProvince(
      this.customerForm.controls.province_code.value
    );
  }

  onDistrictSelected() {
    this.customerForm.patchValue(
      {
        ward_code: ''
      },
      { emitEvent: true }
    );
    this.wardsList = this.locationService.filterWardsByDistrict(
      this.customerForm.controls.district_code.value
    );
  }

  private async initConnections() {
    try {
      this.connections = await this.connectionService.getConnections();
    } catch (e) {
      this.connections = [];
    }
  }

  async checkCustomer() {
    const customer = this.confirmOrderStore.snapshot.customer;
    const fbUser = this.conversationsQuery.getActive()?.fbUsers[0];
    if (!fbUser?.customer && !customer?.id) {
      this.customerForm.controls.phone.valueChanges
        .pipe(
          debounceTime(500),
          map(value => value)
        )
        .subscribe(async phone => {
          if (this._checkFocus && phone) {
            this.selectedCustomer = !!phone;
            this.customerAddresses = await this.confirmOrderService.getCustomerAddress(
              { filter: { phone } }
            );
          }
        });
    } else {
      const shipping_address = this.confirmOrderStore.snapshot.activeToAddress;
      if (
        shipping_address.address1 &&
        shipping_address.province_code &&
        shipping_address.district_code &&
        shipping_address.ward_code
      ) {
        const _shipping_address: any = shipping_address;
        this.confirmOrderService.activeCustomerAddress(_shipping_address);
      }
    }
  }

  checkFocus() {
    const conversation_id = this.platform.getQueryParam('conversation');
    if (!this.customerForm.controls['phone'].value && conversation_id) {
      this.displayMsgPhone = true;
    }
    this._checkFocus = true;
  }

  outFocus() {
    this._checkFocus = false;
    setTimeout(() => {
      this.selectedCustomer = false;
      this.displayMsgPhone = false;
    });
  }

  checkFocusAddress() {
    const conversation_id = this.platform.getQueryParam('conversation');
    if (!this.customerForm.controls['address1'].value && conversation_id) {
      this.displayMsgAddress = true;
    }
  }

  outFocusAddress() {
    setTimeout(() => {
      this.displayMsgAddress = false;
    })
  }

  back() {
    this.navCtrl.back();
  }

  toggleProduct() {
    this.showProduct = !this.showProduct;
  }

  toggleShipping() {
    this.showShipping = !this.showShipping;
  }

  showDropDown() {
    this.dropDown = true;
  }
  hideDropDown() {
    this.dropDown = false;
  }
  async openCreateCustomerPopup() {
    if (this.messageError) {
      this.loadingConfirm = false;
      return this.toast.error('Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.')
    }
    const customer = this.confirmOrderStore.snapshot.customer;
    const activeFbUser = this.conversationsQuery.getActive()?.fbUsers[0];
    let param;
    if (activeFbUser){
      param = 'Tạo khách hàng liên kết với người dùng Facebook';
    } else {
      param = 'Tạo khách hàng mới';
    }
    this.loadingConfirm = false;
    const modal = await this.modalCtrl.create({
      component: CreateCustomerPopupComponent,
      componentProps: {
        customer: this.util.deepClone(customer),
        param,
        activeFbUser
      },
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    this.createPopover = modal;
    modal.onDidDismiss().then(async (data: any) => {
      if (data.data?.closed) {
        return;
      }
      if (data.data) {
        this.customerForm.patchValue({
          phone: data.data.phone,
          full_name: data.data.full_name
        });
        this.confirm().then();
        this.eventBus.message(CustomerEvent.CREATE).next({
          conversation: this.conversationsQuery.getActive(),
          customer: data.data
        })
      }
    });
    await modal.present();
  }

  searchCustomersByPhone() {
    debounceTime(500);
  }

  async confirm() {
    this.submitted = true;
    const order = this.confirmOrderStore.snapshot.order;
    const customer = this.confirmOrderStore.snapshot.customer;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    const activeCustomerAddress = this.confirmOrderStore.snapshot.activeCustomerAddress;
    if (!this.validateOrderInfo(order)) {
      return;
    }
    if (!this.validateFulfillmentInfo(ffm)) {
      return;
    }
    try {
      const {
        id,
        total_items,
        total_amount,
        total_fee,
        total_discount,
        basket_value,
        order_discount,
        fee_lines,
        lines,
        shipping
      } = order;
      if (lines?.length == 0) {
        return this.toast.error('Vui lòng nhập sản phẩm');
      }
      this.loadingConfirm = true;
      if (!customer.id) {
        if (activeCustomerAddress) {
          const fbUser = this.conversationsQuery.getActive()?.fbUsers[0];
          if (fbUser){
            const res = await this.conversationsService.createFbUserCustomer(
              activeCustomerAddress.customer_id,
              fbUser.external_id
            );
            this.conversationsService.setActiveFbUser(this.conversationsQuery.getActive().id, res);
          }
          let customerAddress = this.customerForm.value;
          if (activeCustomerAddress && this.checkCustomerChanged(activeCustomerAddress, customerAddress)) {
            customerAddress.customer_id = activeCustomerAddress.customer_id;
            await this.customerApi.createCustomerAddress(customerAddress);
          }
        } else {
          return await this.openCreateCustomerPopup();
        }
      } else {
        const customerAddress = this.customerForm.value;
        customerAddress.customer_id = customer.id;
        if (this.oldShippingAddress && this.checkCustomerChanged(this.oldShippingAddress, customerAddress)) {
          await this.customerApi.createCustomerAddress(customerAddress);
        }
        if (!activeCustomerAddress) {
          await this.customerApi.createCustomerAddress(customerAddress);
        }
      }
      this.eventBus.message(CustomerEvent.UPDATE).next({
        conversation: this.conversationsQuery.getActive(),
      })
      await this.loading.start('Đang xử lý...');
      const customerID = customer.id || activeCustomerAddress.customer_id;
      const body: OrderAPI.CreateOrderRequest = {
        source: 'etop_pos',
        payment_method: 'cod',
        customer_id: customerID,
        total_items,
        total_amount: Number(total_amount),
        total_fee: Number(total_fee),
        total_discount: Number(total_discount),
        basket_value: Number(basket_value),
        order_discount: Number(order_discount),
        fee_lines,
        lines,
      };

      let res;
      if (!id) {
        res = await this.orderApi.createOrder(body);
        order.id = res.id;

        const _order = this.confirmOrderStore.snapshot.order;
        this.confirmOrderService.updateOrder({
          ..._order,
          id: res.id
        });
      } else {
        res = await this.orderApi.updateOrder({
          ...body,
          id
        });
      }
      await this.orderApi.confirmOrder(res.id);

      const {
        pickup_address,
        shipping_address,
        chargeable_weight,
        cod_amount,
        coupon,
        insurance_value,
        shipping_service_code,
        shipping_service_fee,
        shipping_service_name,
        connection_id,
        try_on,
        shipping_note,
        include_insurance
      } = ffm;
      const ffmBody: FulfillmentAPI.CreateShipmentFulfillmentRequest = {
        pickup_address,
        shipping_address,
        coupon,
        insurance_value: Number(insurance_value),
        chargeable_weight: Number(chargeable_weight),
        cod_amount: Number(cod_amount),
        shipping_service_code,
        shipping_service_fee: Number(shipping_service_fee),
        shipping_service_name,
        connection_id,
        try_on,
        shipping_note,
        include_insurance,
        order_id: order.id,
        shipping_type: 'shipment',
        shipping_payment_type : shipping.shipping_payment_type
      };

      if (this.shopSettings?.return_address) {
        ffmBody.return_address = this.shopSettings.return_address
      }
      await this.fulfillmentApi.createShipmentFulfillment(ffmBody);
      this.loading.end();
      this.loadingConfirm = false;
      this.toast.success('Chốt đơn thành công.').then();
      this.orderService.getOrders(0, 200, [], true);
      this.back();
    } catch (e) {
      this.loadingConfirm = false;
      debug.error('ERROR in confirmOrder', e);
      this.toast
        .error(`Chốt đơn không thành công. ${e.code && (e.message || e.msg)}`)
        .then();
    }
    this.loading.end();
    this.loadingConfirm = false;
  }

  async createOrUpdateAddress(type: 'from' | 'to', address: Address) {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateAddressPopupComponent,
      componentProps: {
        type,
        address
      },
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async data => {
      if (data.data?.address) {
        const _address = data.data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.createOrUpdateShopAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.createOrUpdateCustomerAddress(_address);
        }
      }
    });
    await modal.present().then();
  }

  async createOrUpdateShopAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateFromAddress(address);
    } else {
      const _fromAddress = await this.confirmOrderService.createFromAddress(
        address
      );
      const shopDefaultAddressID = this.auth.snapshot.shop.ship_from_address_id;
      const pickup_address = this.confirmOrderStore.snapshot.fromAddresses.find(
        address => address.id === shopDefaultAddressID
      );
      if (!pickup_address) {
        await this.shopService.setDefaultAddress(_fromAddress.id, 'shipfrom');
        await this.auth.setDefaultAddress(
          _fromAddress.id,
          'shipfrom',
          shopDefaultAddressID
        );
        await this.prepareFromAddress();
      }
    }
  }

  async createOrUpdateCustomerAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateToAddress(address);
    } else {
      await this.confirmOrderService.createToAddress(address);
    }
  }

  async changeAddress(type: 'from' | 'to') {
    if (this.popover) {
      return;
    }
    const fromAddresses = this.confirmOrderStore.snapshot.fromAddresses;
    const toAddresses = this.confirmOrderStore.snapshot.toAddresses;
    this.popover = await this.popoverController.create({
      component: ListAddressesPopupComponent,
      translucent: true,
      componentProps: {
        type,
        addresses: type == 'from' ? fromAddresses : toAddresses
      },
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    this.popover.onDidDismiss().then(async data => {
      this.popover = null;
      if (data.data?.closed) {
        return;
      }
      if (data.data?.createAddress) {
        this.createOrUpdateAddress(type, new Address({}));
      }
      if (data.data?.updateAddress) {
        this.createOrUpdateAddress(type, data.data?.updateAddress);
      }
      if (data.data?.address) {
        const _address = data.data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.confirmOrderService.selectFromAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.confirmOrderService.selectToAddress(_address);
        }
      }
    });
    this.popover.present().then();
  }

  async createNewPickupAddress() {
    await this.createOrUpdateAddress('from', new Address({}));
  }

  getCustomerAddress(address) {
    this.customerForm.patchValue({
      address1: address?.address1,
      province_code: address?.province_code,
    });
    this.districtsList = this.locationService.filterDistrictsByProvince(
      address?.province_code
    );
    this.customerForm.patchValue({
      district_code: address?.district_code
    });
    this.customerForm.patchValue({
      ward_code: address?.ward_code
    });
  }

  async getCustomerReturnRate() {
    try {
      const phone = this.customerForm.controls['phone'].value;
      if (phone.length > 9) {
        const res = await this.confirmOrderService.customerReturnRate(phone);
        this.customerReturnRating = res?.customer_return_rates[0];
        const level = this.customerReturnRating.customer_return_rate.level;
        const first_letter = (level.toLowerCase()).charAt(0);
        this.customerReturnRating.customer_return_rate.level = (level.toLowerCase()).replace(first_letter, level.charAt(0));
        this.messageError = null;
      }
    } catch (e) {
      debug.log('ERROR in getCustomerReturnRate', e);
      this.messageError = e;
      this.customerReturnRating = null;
    }
  }

  async ratingPopover() {
    const popover = await this.popoverController.create({
      component: RiskCustomerWarningComponent,
      cssClass: 'select-variant chat-connection',
      translucent: true,
      componentProps: {
        rating: this.customerReturnRating,
        phone: this.customerForm.controls['phone'].value,
        messageError: this.messageError
      }
    });
    return await popover.present();
  }

  async suggestPhoneFormMessage(){
    const phone = await this.conversationsService.suggestPhoneFromMessage(100);
    this.customerForm.patchValue({
      phone: phone
    });
  }
}
