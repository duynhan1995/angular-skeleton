import { ModuleWithProviders, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmOrderComponent } from 'apps/ionic-faboshop/src/app/pages/confirm-order/confirm-order.component';
import { SharedModule } from 'apps/ionic-faboshop/src/app/features/shared/shared.module';
import { EtopPipesModule } from '@etop/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkStatusModule } from 'apps/ionic-faboshop/src/app/components/network-status/network-status.module';
import { RouterModule, Routes } from '@angular/router';
import { OrderLinesComponent } from './components/order-lines/order-lines.component';
import { OrderShippingComponent } from './components/order-shipping/order-shipping.component';
import { ConnectionService } from '@etop/features/connection/connection.service';
import { EmptyModule } from 'apps/ionic-faboshop/src/app/components/empty/empty.module';
import { ConnectionStore } from '@etop/features/connection/connection.store';
import { CarrierConnectModule } from 'apps/ionic-faboshop/src/app/components/carrier-connect/carrier-connect.module';
import { AddressService } from 'apps/core/src/services/address.service';
import { IonShipmentServicesOptionsModule } from '@etop/shared/components/etop-ionic/ion-shipment-services-options/ion-shipment-services-options.module';
import { PopoversModule } from 'apps/ionic-faboshop/src/app/components/popovers/popovers.module';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { AddressApi } from '@etop/api';
import { ConfirmOrderController } from '@etop/features/fabo/confirm-order/confirm-order.controller';
import { IonInputFormatNumberModule } from '@etop/shared/components/etop-ionic/ion-input-format-number/ion-input-format-number.module';
import { ProductSelectorComponent } from './components/product-selector/product-selector.component';
import { VariantSelectorComponent } from './components/variant-selector/variant-selector.component';
import { CreateProductLiteComponent } from './components/create-product-lite/create-product-lite.component';
import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { ImageLoaderModule } from '@etop/features';
import { SelectSuggestModule } from '../../components/select-suggest/select-suggest.module';
import { MatIconModule } from '@angular/material/icon';
import { ManualMessageComponent } from './components/manual-message/manual-message.component';
import { AuthenticateModule } from '@etop/core';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';

const routes: Routes = [
  {
    path: '',
    component: ConfirmOrderComponent
  }
]

@NgModule({
  declarations: [ConfirmOrderComponent, OrderLinesComponent, OrderShippingComponent, ProductSelectorComponent, VariantSelectorComponent, CreateProductLiteComponent, CustomerListComponent, ManualMessageComponent],
  imports: [
    CommonModule,
    SharedModule,
    EtopPipesModule,
    FormsModule,
    IonicModule,
    NetworkStatusModule,
    EmptyModule,
    CarrierConnectModule,
    PopoversModule,
    IonShipmentServicesOptionsModule,
    RouterModule.forChild(routes),
    IonInputFormatNumberModule,
    ReactiveFormsModule,
    ImageLoaderModule,
    SelectSuggestModule,
    MatIconModule,
    AuthenticateModule,
  ],
  entryComponents: [
    ConfirmOrderComponent,
    OrderShippingComponent
  ],
  providers: [
    AddressService,
    ConnectionService,
    ConnectionStore,
    AddressApi,
    ShopSettingsService
  ],
  exports: [ConfirmOrderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConfirmOrderModule {
  static provideStore(): ModuleWithProviders<ConfirmOrderModule> {
    return {
      ngModule: ConfirmOrderModule,
      providers: [
        ConfirmOrderStore,
        ConfirmOrderService,
        ConfirmOrderController,
        ConnectionService,
        ConnectionStore,
      ]
    }
  }
}
