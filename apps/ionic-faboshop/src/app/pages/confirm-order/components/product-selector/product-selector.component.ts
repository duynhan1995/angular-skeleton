import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { UtilService } from '../../../../../../../core/src/services/util.service';
import { Product, Variant } from '../../../../../../../../libs/models/Product';
import { PopoverController } from '@ionic/angular';
import { VariantSelectorComponent } from '../variant-selector/variant-selector.component';
import { CreateProductLiteComponent } from '../create-product-lite/create-product-lite.component';

@Component({
  selector: 'ionfabo-product-selector',
  templateUrl: './product-selector.component.html',
  styleUrls: ['./product-selector.component.scss']
})
export class ProductSelectorComponent implements OnInit {
  @Input() submitted;

  products$ = this.confirmOrderStore.state$.pipe(map(s => s.products));

  productQuery = new FormControl('');
  showProductList = false;
  order$ = this.confirmOrderStore.state$.pipe(map(s => s ?.order));

  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private util: UtilService,
    private popoverController: PopoverController,
    private ref: ElementRef
  ) {}

  ngOnInit(): void {
    this.productQuery.valueChanges.pipe(debounceTime(300)).subscribe(value => {
      this.showProductList = !!value;
      if (value) {
        this.confirmOrderService.searchProducts(value);
      }
    });
  }

  @HostListener('window:click', ['$event'])
  detectOutsideClick(event) {
    const checkELement = this.ref.nativeElement;
    let targetElement = event.target;

    do {
      if (targetElement == checkELement) {
        return;
      }
      targetElement = targetElement.parentNode;
    } while (targetElement);
    this.showProductList = false;
  }

  priceCompare(product) {
    return this.util.compareProductPrice(product);
  }

  async productSelected(product: Product) {
    if (product.variants.length == 1) {
      return this.variantSelected(product.variants[0]);
    }
    const popover = await this.popoverController.create({
      cssClass: 'center-popover',
      component: VariantSelectorComponent,
      componentProps: { product },
      backdropDismiss: false
    });
    await popover.present();
    popover
      .onWillDismiss()
      .then(data => data.data && this.variantSelected(data.data));
  }

  variantSelected(variant: Variant) {
    this.showProductList = false;
    this.productQuery.setValue('', { emitEvent: false });
    this.confirmOrderService.addOrderLine({
      product_name: variant.product_name,
      code: variant.code,
      variant_id: variant.id,
      payment_price: variant.retail_price,
      attributes: variant.attributes,
      quantity: 1
    });
  }

  async createProduct() {
    const popover = await this.popoverController.create({
      component: CreateProductLiteComponent,
      cssClass: 'center-popover',
      backdropDismiss: false,
      componentProps: { productName: this.productQuery.value }
    });
    await popover.present();
    popover
      .onWillDismiss()
      .then(data => data.data && this.variantSelected(data.data.variants[0]));
  }
}
