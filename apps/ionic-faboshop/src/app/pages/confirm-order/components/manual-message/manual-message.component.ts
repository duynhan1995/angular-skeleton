import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ionfabo-manual-message',
  templateUrl: './manual-message.component.html',
  styleUrls: ['./manual-message.component.scss']
})
export class ManualMessageComponent implements OnInit {
  @Input() title;
  @Input() messages: any;
  @Output() selectMessage = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  selectMsg(message) {
    this.selectMessage.emit(message.external_message)
  }
}
