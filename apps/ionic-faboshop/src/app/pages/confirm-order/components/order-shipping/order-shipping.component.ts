import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {PopoverController, ModalController, IonRouterOutlet} from '@ionic/angular';
import {CarrierConnectComponent} from 'apps/ionic-faboshop/src/app/components/carrier-connect/carrier-connect.component';
import {ToastService} from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ConnectionAPI, FulfillmentApi } from '@etop/api';
import {LoadingService} from 'apps/ionic-faboshop/src/app/services/loading.service';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {Address} from 'libs/models/Address';
import {
  ShipmentCarrier,
  FulfillmentShipmentService,
  TRY_ON_OPTIONS,
  TRY_ON_OPTIONS_SHIPPING
} from 'libs/models/Fulfillment';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {ListAddressesPopupComponent} from 'apps/ionic-faboshop/src/app/components/popovers/list-addresses-popup/list-addresses-popup.component';
import {CreateUpdateAddressPopupComponent} from 'apps/ionic-faboshop/src/app/components/popovers/create-update-address-popup/create-update-address-popup.component';
import {ConnectionService} from '@etop/features/connection/connection.service';
import {ConnectionStore} from '@etop/features/connection/connection.store';
import {ConfirmOrderService} from '@etop/features/fabo/confirm-order/confirm-order.service';
import {ConfirmOrderStore} from '@etop/features/fabo/confirm-order/confirm-order.store';
import {FormBuilder} from '@angular/forms';
import { FulfillmentService } from '@etop/features/fabo/fulfillment/fulfillment.service';
import { ConversationsQuery } from '@etop/state/fabo/conversation';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { ShopSettings } from '@etop/models/ShopSettings';

@Component({
  selector: 'etop-order-shipping',
  templateUrl: './order-shipping.component.html',
  styleUrls: ['./order-shipping.component.scss']
})
export class OrderShippingComponent extends BaseComponent implements OnInit {
  loggedInConnections$ = this.connectionStore.state$.pipe(map(s => s?.loggedInConnections));
  activeShipmentService$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.activeShipmentService),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );
  shipmentServicesRequest$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.shipmentServicesRequest),
    distinctUntilChanged((prev, next) => JSON.stringify(prev) == JSON.stringify(next))
  );
  activeFbUser$ = this.conversationsQuery.selectActive(({fbUsers}) => fbUsers && fbUsers[0]);
  activeToAddress$ = this.confirmOrderStore.state$.pipe(map(s => s?.activeToAddress));
  fromAddress$ = this.confirmOrderStore.state$.pipe(map(s => s?.fromAddresses));
  orderTotalAmountChanged$ = this.confirmOrderStore.state$.pipe(
    map(s => s?.order?.total_amount),
    distinctUntilChanged((prev, next) => prev == next)
  );

  carriers: ShipmentCarrier[] = [];
  carrierServices: any;
  getServicesIndex = 0;
  gettingServices = false;

  errorMessage = '';
  empty = {
    title: 'Chưa kết nối nhà vận chuyển',
    subtitle: 'Vui lòng kết nối Nhà vận chuyển để giao hàng',
    buttonClick: this.showCarriersList.bind(this)
  };

  popover: HTMLIonPopoverElement;

  fulfillmentForm = this.fb.group({
    chargeable_weight: null,
    shipping_service_fee:null,
    coupon: null,
    cod_amount: null,
    insurance_value:null,
    selected_service_unique_id:null,
    try_on: null,
    try_on_shipping: null,
    shipping_note: '',
    include_insurance: true,
    shipping_payment_type : null
  });

  settings = new ShopSettings({});

  constructor(
    private auth: AuthenticateStore,
    private conversationsQuery:ConversationsQuery,
    private fb: FormBuilder,
    private popoverController: PopoverController,
    private toast: ToastService,
    private loading: LoadingService,
    private ref: ChangeDetectorRef,
    private fulfillmentService: FulfillmentService,
    private fulfillmentApi: FulfillmentApi,
    private connectionService: ConnectionService,
    private connectionStore: ConnectionStore,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderStore: ConfirmOrderStore,
    private modalCtrl: ModalController,
    private routerOutlet: IonRouterOutlet,
    private shopSettingsService: ShopSettingsService
  ) {
    super();
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  async ngOnInit() {
    this.settings = await this.shopSettingsService.getSetting();
    this.fulfillmentForm.controls['shipping_payment_type'].valueChanges.subscribe(value=>{
      const order =this.confirmOrderStore.snapshot.order
      this.confirmOrderService.updateOrder({
        ...order,
        shipping : {
          ...order.shipping,
          shipping_payment_type : value
        }
      })
    })
    this.activeShipmentService$.pipe(takeUntil(this.destroy$))
      .subscribe(service => {
        const ffm = this.confirmOrderStore.snapshot.fulfillment;
        this.confirmOrderService.updateFulfillment({
          ...ffm,
          shipping_service_name: service?.name,
          shipping_service_code: service?.code,
          shipping_service_fee: service?.fee,
          connection_id: service?.connection_info?.id,
        });
      });
    this.fulfillmentForm.valueChanges.subscribe(ffm => {
      const _ffm = this.confirmOrderStore.snapshot.fulfillment;
      this.confirmOrderService.updateFulfillment({
        ..._ffm,
        ...ffm
      });
    });

    this.shipmentServicesRequest$.pipe(takeUntil(this.destroy$))
      .subscribe(async (request) => {
        await this.getServices(request);
      });

    this.fulfillmentForm.get('selected_service_unique_id').valueChanges.subscribe(value =>{
      if (value){
        let service = this.carrierServices.find(carrierService => carrierService.unique_id == value);
        this.fulfillmentForm.patchValue({
          shipping_service_fee:service.fee,
        })
        this.selectService(service);
      }
    })

    this.getFormDataFromStore();
    this.prepareTryOn();


    this.activeToAddress$.pipe(takeUntil(this.destroy$))
      .subscribe(address => {
        if (address?.p_data?.copied) {
          this.createOrUpdateAddress('to', address);
        }
      });

    this.orderTotalAmountChanged$.pipe(takeUntil(this.destroy$))
      .subscribe(totalAmount => {
        const ffm = this.confirmOrderStore.snapshot.fulfillment;
        if (!ffm?.p_data?.cod_amount_edited) {
          this.fulfillmentForm.patchValue({
            cod_amount: Number(totalAmount)
          });
        }
        if (!ffm?.p_data?.insurance_edited) {
          this.fulfillmentForm.patchValue({
            insurance_value: Number(totalAmount),
            include_insurance: true,
          });
        }
      });
    this.activeFbUser$.subscribe(fbUser => {
      if(!fbUser){
        this.resetForm()
      }
    })
    const saveDataFulfillment: any = this.confirmOrderStore.snapshot.saveDataFulfillment;
    if (saveDataFulfillment?.p_data?.is_save) {
      const { chargeable_weight, try_on, shipping_note, include_insurance, try_on_shipping, insurance_value, cod_amount } = saveDataFulfillment;
      this.fulfillmentForm.patchValue({
        chargeable_weight: Number(chargeable_weight) || 500,
        try_on,
        shipping_note,
        include_insurance,
        try_on_shipping,
        insurance_value: Number(insurance_value) || 0,
        cod_amount: Number(cod_amount) || 0
      });
      const ffm = this.confirmOrderStore.snapshot.fulfillment;
      this.confirmOrderService.updateFulfillment({
        ...ffm, 
        ...saveDataFulfillment
      })
    }
  }

  get tryOnOptionsShipping() {
    return TRY_ON_OPTIONS_SHIPPING;
  }

  onCoupon(){
    const coupon =this.fulfillmentForm.get('coupon').value;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      coupon,
    });
    this.fulfillmentForm.patchValue({
      selected_service_unique_id: null,
      shipping_service_fee: null,
    })

  }

  getFormDataFromStore() {
    this.settings.payment_type_id = this.settings?.payment_type_id == 'none'? null : this.settings?.payment_type_id;
    this.settings.try_on = this.settings?.try_on == 'unknown' ? null : this.settings?.try_on;
    this.fulfillmentForm.patchValue({
      chargeable_weight: this.settings.weight || this.confirmOrderStore.snapshot.fulfillment.chargeable_weight,
      try_on: this.settings.try_on || this.auth.snapshot.shop.try_on,
      shipping_note: this.settings.shipping_note,
      include_insurance: true,
      try_on_shipping: 'shop',
      shipping_payment_type : this.settings.payment_type_id || "seller"
    });
  }

  resetForm(){
    this.fulfillmentForm.patchValue({
      cod_amount:'',
      insurance_value:0,
    })
  }

  onCodAmountInputed() {
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      p_data: {cod_amount_edited: true}
    });
  }
  onInsuranceInputed(){
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      p_data: {insurance_edited: true}
    });
  }

  prepareTryOn() {
    const shop = this.auth.snapshot.shop;
    const ffm = this.confirmOrderStore.snapshot.fulfillment;
    this.confirmOrderService.updateFulfillment({
      ...ffm,
      try_on: shop.try_on
    });
  }

  async changeAddress(type: 'from' | 'to') {
    if (this.popover) { return; }
    const fromAddresses = this.confirmOrderStore.snapshot.fromAddresses;
    const toAddresses = this.confirmOrderStore.snapshot.toAddresses;
    this.popover = await this.popoverController.create({
      component: ListAddressesPopupComponent,
      translucent: true,
      componentProps: {
        type,
        addresses: type == 'from' ? fromAddresses : toAddresses
      },
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    this.popover.onDidDismiss().then(async(data) => {
      this.popover = null;
      if (data.data?.closed) { return; }
      if (data.data?.createAddress) {
        this.createOrUpdateAddress(type, new Address({}))
      }
      if (data.data?.updateAddress) {
        this.createOrUpdateAddress(type, data.data?.updateAddress);
      }
      if (data.data?.address) {
        const _address = data.data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.confirmOrderService.selectFromAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.confirmOrderService.selectToAddress(_address);
        }
      }

    });
    this.popover.present().then();
  }

  async createOrUpdateAddress(type: 'from' | 'to', address: Address) {
    if (this.popover) { return; }
    this.popover = await this.popoverController.create({
      component: CreateUpdateAddressPopupComponent,
      translucent: true,
      componentProps: {
        type,
        address
      },
      cssClass: 'center-popover',
      animated: true,
      showBackdrop: true,
      backdropDismiss: false
    });
    this.popover.onDidDismiss().then(async(data) => {
      this.popover = null;
      if (data.data?.closed) {
        return this.changeAddress(type);
      }
      if (data.data?.address) {
        const _address = data.data?.address;
        if (type == 'from') {
          this.confirmOrderService.updatePickupAddress(_address);
          this.createOrUpdateShopAddress(_address);
        } else {
          this.confirmOrderService.updateShippingAddress(_address);
          this.createOrUpdateCustomerAddress(_address);
        }
      }
    });
    this.popover.present().then();
  }

  async createOrUpdateShopAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateFromAddress(address);
    } else {
      await this.confirmOrderService.createFromAddress(address);
    }
  }

  async createOrUpdateCustomerAddress(address: Address) {
    if (address.id) {
      await this.confirmOrderService.updateToAddress(address);
    } else {
      await this.confirmOrderService.createToAddress(address);
    }
  }

  async showCarriersList(lastState?: any) {
    const modal = await this.modalCtrl.create({
      component: CarrierConnectComponent,
      componentProps: {
        ...lastState
      },
      animated: true,
      showBackdrop: true,
      backdropDismiss: false,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onDidDismiss().then(async(data) => {
      if (data && data.data) {
        await this.loginConnection(data.data);
      }
    });
    await modal.present();
  }

  async loginConnection(data: any) {
    try {
      await this.loading.start('Đang xử lý...');
      const loginOTP: ConnectionAPI.LoginShopConnectionWithOTPRequest = {
        ...data.loginOTP
      };
      await this.connectionService.loginShopConnectionWithOTP(loginOTP);
      this.toast.success('Kết nối thành công').then();
    } catch(e) {
      debug.error('ERROR in loginConnection', e);
      this.toast.error('Kết nối không thành công').then();
      this.showCarriersList(data).then();
    }
    this.loading.end();
  }


  selectService(service: FulfillmentShipmentService) {
    this.confirmOrderService.selectShipmentService(service);
  }

  async getServices(data) {
    if (!data || !data.to_ward_code) {
      return;
    }

    this.selectService(null);

    this.gettingServices = true;
    try {
      this.getServicesIndex++;
      const loggedInConnections: any = this.connectionStore.snapshot.loggedInConnections;
      const shopConnectionIds = loggedInConnections && loggedInConnections.map(con => con.connection_id);
      const body = {
        ...data,
        index: this.getServicesIndex,
        connection_ids: shopConnectionIds,
        insurance_value: Number(data.insurance_value)
      };
      const res = await this.fulfillmentApi.getShipmentServices(body);

      if (res.index == this.getServicesIndex) {
        this.carriers = this.fulfillmentService.groupShipmentServicesByConnection(
          res.services,
          shopConnectionIds
        );
        this.gettingServices = false;
      }
      this.gettingServices = false;
      this.getServiceGHN();
    } catch (e) {
      debug.error('ERROR in Getting Services', e);
      this.carrierServices = [];
      this.fulfillmentForm.patchValue({
        selected_service_unique_id: null,
        shipping_service_fee: null
      });
      this.errorMessage = e.message;
      this.gettingServices = false;
    }
  }

  getServiceGHN() {
    this.carrierServices = this.carriers[0]?.services;
    if (this.carrierServices?.length){
      const _services: FulfillmentShipmentService[] = JSON.parse(JSON.stringify(this.carrierServices))
      _services.sort((a,b) => a.fee - b.fee)
      this.fulfillmentForm.patchValue({
        selected_service_unique_id: _services[0].unique_id
      })
    }
    this.ref.detectChanges()
  }

}
