import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../../../../../../libs/models/Product';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'ionfabo-variant-selector',
  templateUrl: './variant-selector.component.html',
  styleUrls: ['./variant-selector.component.scss']
})
export class VariantSelectorComponent implements OnInit {
  @Input() product: Product;

  constructor(private popoverController: PopoverController) {}

  ngOnInit(): void {}

  dismiss() {
    this.popoverController.dismiss();
  }

  variantSelected(variant) {
    this.popoverController.dismiss(variant);
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs
      .map(a => {
        return a.value;
      })
      .join(' - ');
  }
}
