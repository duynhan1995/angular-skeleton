import {Component, OnInit} from '@angular/core';
import {ValueAccessorBase} from "apps/core/src/interfaces/ValueAccessorBase";
import {OrderLine} from "libs/models/Order";
import { FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ConfirmOrderStore } from '@etop/features/fabo/confirm-order/confirm-order.store';
import { BaseComponent } from '@etop/core';
import { filter, take, takeUntil } from 'rxjs/operators';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';

@Component({
  selector: 'etop-order-lines',
  templateUrl: './order-lines.component.html',
  styleUrls: ['./order-lines.component.scss']
})
export class OrderLinesComponent extends BaseComponent implements OnInit {
  orderLines = this.fb.array([]);

  private dataLoading = false;

  constructor(
    private confirmOrderStore: ConfirmOrderStore,
    private confirmOrderService: ConfirmOrderService,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.confirmOrderStore.forceUpdateForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getFormDataFromStore();
      });
    this.orderLines.valueChanges
      .pipe(filter(() => !this.dataLoading))
      .subscribe(value => {
        this.confirmOrderService.updateOrder({
          ...this.confirmOrderStore.snapshot.order,
          lines: value
        });
      });
      const saveDataOrder: any = this.confirmOrderStore.snapshot.saveDataOrder;
      if (saveDataOrder?.lines?.length) {
        this.getFormDataFromStore();
      }
  }

  forceUpdate(e) {
    this.confirmOrderStore.forceUpdateForm$.next();
  }

  getFormDataFromStore() {
    this.dataLoading = true;
    this.orderLines.clear();
    this.confirmOrderStore.snapshot.order.lines.forEach(line => {
      const group = this.fb.group(line);
      group.patchValue({
        attributes: line.attributes
      });
      this.orderLines.push(group);
    });
    this.dataLoading = false;
  }

  removeVariant(line) {
    this.confirmOrderService.removeOrderLine(line);
  }

  joinAttrs(attrs) {
    if (!attrs || !attrs.length) {
      return '';
    }
    return attrs
      .map(a => {
        return a.value;
      })
      .join(' - ');
  }
}
