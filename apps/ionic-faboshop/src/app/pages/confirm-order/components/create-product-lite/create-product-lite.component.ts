import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConfirmOrderService } from '@etop/features/fabo/confirm-order/confirm-order.service';
import { ToastService } from '../../../../services/toast.service';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'ionfabo-create-product-lite',
  templateUrl: './create-product-lite.component.html',
  styleUrls: ['./create-product-lite.component.scss']
})
export class CreateProductLiteComponent implements OnInit {
  @Input() productName: string;

  productForm = this.fb.group({
    product_name: '',
    price: null
  });

  constructor(
    private confirmOrderService: ConfirmOrderService,
    private popoverController: PopoverController,
    private toast: ToastService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.productForm.patchValue({
      product_name: this.productName
    });
  }

  dismiss() {
    this.popoverController.dismiss();
  }
  async createProduct() {
    const data = this.productForm.value;
    if (!data.product_name) {
      return this.toast.error('Tên sản phẩm không được để trống');
    }
    if (!data.price) {
      return this.toast.error('Vui lòng điền giá sản phẩm');
    }

    try {
      const product = await this.confirmOrderService.fastCreateProduct(data);
      this.popoverController.dismiss(product);
    } catch (e) {
      debug.error(e);
      this.toast.error(e.message);
    }
  }
}
