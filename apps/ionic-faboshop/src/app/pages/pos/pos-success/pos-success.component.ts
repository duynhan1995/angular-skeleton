import { Component, OnInit } from '@angular/core';
import { OrderApi } from '@etop/api';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { Order } from 'libs/models/Order';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { Router } from '@angular/router';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { LoadingService } from 'apps/ionic-faboshop/src/app/services/loading.service';
import { takeUntil } from 'rxjs/operators';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';

@Component({
  selector: 'etop-pos-success',
  templateUrl: './pos-success.component.html',
  styleUrls: ['./pos-success.component.scss']
})
export class PosSuccessComponent extends BaseComponent implements OnInit {
  order_id: string;
  order = new Order({});
  loading = true;

  constructor(
    private orderApi: OrderApi,
    private posStore: PosStoreService,
    private receiptStore: ReceiptStoreService,
    private toast: ToastService,
    private router: Router,
    private auth: AuthenticateStore,
    private loadingService: LoadingService
  ) {
    super();
  }

  get confirmStatusImg() {
    if (this.order.confirm_status == "P" || !this.posStore.snapshot.confirm) {
      return 'assets/imgs/checked.png';
    }
    return 'assets/imgs/warning.png';
  }

  get hasError() {
    return !(this.order.confirm_status == 'P' || !this.posStore.snapshot.confirm);
  }

  get paymentMethodDisplay() {
    const selected_ledger = this.receiptStore.snapshot.selected_ledger;
    if (selected_ledger.type == 'bank') {
      return `<div>Chuyển khoản qua TK thanh toán <b>${selected_ledger.name}</b></div>`;
    }
    return '<span>Tiền mặt</span>';
  }

  async ngOnInit() {
    this.posStore.onOrderCreated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(recentlyCreatedOrderID => {
        this.order_id = recentlyCreatedOrderID;
        if (this.order_id) {
          this.getOrderData();
        }
      });
  }

  async getOrderData() {
    this.loading = true;
    await this.loadingService.start('');
    try {
      this.order = await this.orderApi.getOrder(this.order_id);
    } catch(e) {
      debug.error('ERROR in getting order data', e);
    }
    this.loadingService.end();
    this.loading = false;
  }

  async retryConfirm() {
    this.loading = true;
    await this.loadingService.start('');
    try {
      await this.orderApi.confirmOrder(this.order_id, "confirm");
      this.order = await this.orderApi.getOrder(this.order_id);
    } catch(e) {
      debug.error('ERROR in getting order data', e);
      this.toast.error('Có lỗi xảy ra. Xin vui lòng thử lại!');
    }
    this.loadingService.end();
    this.loading = false;
  }

  newOrder() {
    this.posStore.recentlyCreatedOrderID(null);
    this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug}/pos`);
  }

  viewOrders() {
    this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug}/orders`);
  }

}
