import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderLine } from 'libs/models/Order';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { IonInput, ToastController, NavController } from '@ionic/angular';
import { ToastService } from '../../../services/toast.service';

@Component({
  selector: 'etop-pos-basket',
  templateUrl: './pos-basket.component.html',
  styleUrls: ['./pos-basket.component.scss']
})
export class PosBasketComponent implements OnInit {
  @ViewChild('discountInput', { static: false }) discountInput: IonInput;
  @ViewChild('feeInput', { static: false }) feeInput: IonInput;
  order_lines: OrderLine[] = [];

  discount: number;
  fee: number;
  total_amount: number;

  format_money = {
    discount: false,
    fee: false
  };

  constructor(
    private toastController: ToastController,
    private posStore: PosStoreService,
    private router: Router,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private toastService: ToastService
  ) {}

  get basketValue() {
    return this.order_lines.reduce(
      (a, b) => a + Number(b.quantity) * Number(b.retail_price),
      0
    );
  }

  ngOnInit() {
    this.order_lines = this.posStore.snapshot.order_lines;
    this.totalAmount();
  }

  lineUpdated(line: OrderLine) {
    line.quantity = Number(line.quantity);
    this.posStore.changeLines(this.order_lines);
    this.totalAmount();
  }

  discountUpdated() {
    this.discount = Number(this.discount);
    this.posStore.changeDiscount(this.discount || 0);
    this.totalAmount();
  }

  feeUpdated() {
    this.fee = Number(this.fee);
    this.posStore.changeFee(this.fee || 0);
    this.totalAmount();
  }

  totalAmount() {
    this.total_amount =
      Number(this.basketValue) -
      Number(this.discount || 0) +
      Number(this.fee || 0);
  }

  formatMoney(type) {
    if (!Number(this[type])) {
      this[type] = undefined;
      this.format_money[type] = false;
      return;
    }
    this.format_money[type] = !this.format_money[type];
    setTimeout(_ => {
      if (type == 'discount' && this.discountInput) {
        this.discountInput.setFocus();
      }
      if (type == 'fee' && this.feeInput) {
        this.feeInput.setFocus();
      }
    }, 200);
  }

  checkBasket(line: OrderLine) {
    line.quantity = Number(line.quantity);
    if (line.quantity == 0) {
      line.quantity = 1;
    }
    this.posStore.changeLines(this.order_lines);
    this.totalAmount();
  }
  async toPayment() {
    if (this.order_lines.some(l => !l.quantity)) {
      return this.toastService.warning(
        'Sản phẩm không được có số lượng bằng 0!'
      );
    }
    if (this.order_lines.length == 0) {
      return this.toastService.error('Không có sản phẩm trong giỏ hàng! Vui lòng quay lại chọn sản phẩm.');
    }

    if (this.total_amount < 0) {
      return this.toastService.error('Số tiền khách phải trả không hợp lệ!');
    }
    await this.navCtrl.navigateForward(
      `/s/${this.auth.snapshot.account.url_slug}/pos/payment`,
      { animated: false }
    );
  }

  removeLine(index: number) {
    this.order_lines.splice(index, 1);
    this.posStore.changeLines(this.order_lines);
    this.totalAmount();
  }

  lineDisplayAttributes(line: OrderLine) {
    let str = "";
    for (let i = 0; i < line.attributes.length; i++) {
      if (i == 0) {
        str += line.attributes[i].value;
      } else {
        str += ' - ' + line.attributes[i].value;
      }
    }
    return str;
  }

}
