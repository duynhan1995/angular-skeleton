import { Component, Input, OnInit } from '@angular/core';
import { Product, Variant } from 'libs/models/Product';
import { ModalController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'etop-select-variant-modal',
  templateUrl: './select-variant-modal.component.html',
  styleUrls: ['./select-variant-modal.component.scss']
})
export class SelectVariantModalComponent implements OnInit {
  @Input() product = new Product({});

  constructor(
    private modalCtrl: ModalController,
    private popoverController: PopoverController
  ) {}

  ngOnInit() {}

  dismiss() {
    this.popoverController.dismiss();
  }

  selectVariant(variant: Variant) {
    variant.p_data.selected = !variant.p_data.selected;
    this.popoverController.dismiss({ variant });
  }

  variantDisplayAttributes(variant: Variant) {
    const noVariantName = !variant.name || variant.name == variant.product_name;
    if (noVariantName && variant.attributes && variant.attributes.length) {
      return variant.attributes.map(attr => attr.value).join(' - ');
    }
    return variant.name;
  }
}
