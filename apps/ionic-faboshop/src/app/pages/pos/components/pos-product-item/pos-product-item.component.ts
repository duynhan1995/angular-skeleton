import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'libs/models/Product';

@Component({
  selector: 'etop-pos-product-item',
  templateUrl: './pos-product-item.component.html',
  styleUrls: ['./pos-product-item.component.scss']
})
export class PosProductItemComponent implements OnInit {
  @Input() product = new Product({});

  constructor() { }

  ngOnInit() {
  }

}
