import { Component, HostListener, OnInit, ViewChild, NgZone } from '@angular/core';
import { Customer } from 'libs/models/Customer';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { Order, OrderLine } from 'libs/models/Order';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { CustomerApi, InventoryApi, OrderApi, ReceiptApi } from '@etop/api';
import { Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { LoadingService } from 'apps/ionic-faboshop/src/app/services/loading.service';
import { CustomerService } from 'apps/shop/src/services/customer.service';
import { IonInput, ModalController } from '@ionic/angular';
import { ChangePaymentMethodModalComponent } from 'apps/ionic-faboshop/src/app/pages/pos/components/change-payment-method-modal/change-payment-method-modal.component';
import { Ledger } from 'libs/models/Receipt';
import { UtilService } from 'apps/core/src/services/util.service';

import { Plugins } from '@capacitor/core';
import {OrderService} from "@etop/features";

const { Keyboard } = Plugins;

@Component({
  selector: 'etop-pos-payment',
  templateUrl: './pos-payment.component.html',
  styleUrls: ['./pos-payment.component.scss']
})
export class PosPaymentComponent implements OnInit {
  @ViewChild('customerNameInput', { static: false })
  customerNameInput: IonInput;
  @ViewChild('customerPhoneInput', { static: false })
  customerPhoneInput: IonInput;
  @ViewChild('receivedAmountInput', { static: false })
  receivedAmountInput: IonInput;

  customerList: Customer[] = [];
  customerSearchResult: Customer[] = [];

  order_id: string;
  order_lines: OrderLine[] = [];
  customer = new Customer({});
  selected_ledger = new Ledger({});

  received_amount: number;
  format_money = false;

  paymentMethodDisplay = 'Khách trả tiền mặt';

  suggestDisplay = false;

  constructor(
    private receiptApi: ReceiptApi,
    private inventoryApi: InventoryApi,
    private orderApi: OrderApi,
    private customerApi: CustomerApi,
    private orderService: OrderService,
    private receiptService: ReceiptService,
    private customerService: CustomerService,
    private posStore: PosStoreService,
    private receiptStore: ReceiptStoreService,
    private router: Router,
    private auth: AuthenticateStore,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private modalCtrl: ModalController,
    private util: UtilService,
    private zone: NgZone
  ) {
    Keyboard.addListener('keyboardDidHide', () => {
      this.zone.run(async () => {
        this.hideSearchResult();
      });
    });
  }

  get basketValue() {
    return this.order_lines.reduce(
      (a, b) => a + Number(b.quantity) * Number(b.retail_price),
      0
    );
  }

  get totalAmount() {
    const discount = this.posStore.snapshot.discount;
    const fee = this.posStore.snapshot.fee;
    return this.basketValue - discount + fee;
  }

  async ngOnInit() {
    this.order_lines = this.posStore.snapshot.order_lines;
    this.received_amount = this.totalAmount;
    this.format_money = this.received_amount >= 0;
    this.prepareData();
  }

  async prepareData() {
    this.customerList = await this.customerService.getCustomers();
    let idx = this.customerList.findIndex(c => c.full_name == 'Khách lẻ');
    if (idx) {
      this.customerList.splice(idx, 1);
    }
    const _ledgers = await this.receiptService.getLedgers();
    const _selected_ledger = _ledgers.find(l => l.type == 'cash');
    this.selected_ledger = _selected_ledger;
    this.receiptStore.updateLedgers(_ledgers);
    this.receiptStore.updateSelectedLedger(_selected_ledger);
  }

  resetData() {
    this.customer = new Customer({});
    this.received_amount = null;
    this.posStore.resetState();
  }

  onChange() {
    debug.log('on change', this.received_amount);
  }

  formatMoney() {
    const amount = Number(this.received_amount);
    if (!amount && amount != 0) {
      this.received_amount = undefined;
      this.format_money = false;
      return;
    }
    this.format_money = !this.format_money;
    setTimeout(_ => {
      if (this.receivedAmountInput) {
        this.receivedAmountInput.setFocus();
      }
    }, 200);
  }

  private async checkAndCreateCustomer() {
    try {
      // NOTE: khách lẻ
      if (!this.customer.full_name && !this.customer.phone) {
        return null;
      }
      const existedCustomers: Customer[] = await this.customerService.checkExistedCustomer(
        {
          phone: this.customer.phone
        }
      );
      const found: Customer = existedCustomers.find(
        c => c.phone == this.customer.phone
      );
      if (found) {
        if (this.customer.full_name != found.full_name) {
          const body: any = {
            id: found.id,
            full_name: this.customer.full_name
          };
          await this.customerApi.updateCustomer(body);
        }
        return found.id;
      }

      const body: any = {
        full_name: this.customer.full_name,
        phone: this.customer.phone
      };
      const res = await this.customerApi.createCustomer(body);
      return res.id;
    } catch (e) {
      debug.error('ERROR in checkAndCreateCustomer', e);
      return null;
    }
  }

  private checkReceipt() {
    if (
      this.selected_ledger.type == 'bank' &&
      this.received_amount > this.totalAmount
    ) {
      this.toastService.error(
        'Với phương thức Chuyển khoản, số tiền nhận từ khách không được lớn hơn số tiền khách phải trả!'
      );
      return false;
    }
    return true;
  }

  private checkCustomer() {
    if (this.customer.full_name && !this.customer.phone) {
      this.toastService.error('Vui lòng nhập số điện thoại khách hàng!');
      return false;
    }
    if (!this.customer.full_name && this.customer.phone) {
      this.toastService.error('Vui lòng nhập tên khách hàng!');
      return false;
    }
    if (this.customer.phone && this.customer.phone.length < 8) {
      this.toastService.error('Số điện thoại không hợp lệ!');
      return false;
    }
    return true;
  }

  async create(createAndConfirm: boolean) {
    this.posStore.setConfirm(createAndConfirm);
    await this.loadingService.start('Đang xử lý');
    try {
      if (!this.checkReceipt()) {
        this.loadingService.end();
        return;
      }
      if (!this.checkCustomer()) {
        this.loadingService.end();
        return;
      }
      const body: any = {
        source: 'etop_pos',
        payment_method: 'cod',
        lines: this.order_lines,
        total_items: this.order_lines.reduce(
          (a, b) => a + Number(b.quantity),
          0
        ),
        basket_value: this.basketValue,
        order_discount: this.posStore.snapshot.discount,
        total_discount: this.posStore.snapshot.discount,
        fee_lines: [
          {
            type: 'other',
            name: 'Phụ phí',
            amount: this.posStore.snapshot.fee
          }
        ],
        total_fee: this.posStore.snapshot.fee,
        total_amount: this.totalAmount
      };
      const customer_id = await this.checkAndCreateCustomer();
      if (customer_id) {
        body.customer_id = customer_id;
      } else {
        body.customer = { full_name: 'Khách lẻ' };
      }

      const res = await this.orderService.createOrder(body);
      this.posStore.recentlyCreatedOrderID(res.id);
      if (this.received_amount) {
        await this.createReceipt(res);
      }
      if (createAndConfirm) {
        try {
          const order = await this.orderApi.confirmOrder(res.id, 'confirm');
        } catch (e) {
          debug.error('ERROR in confirming Order', e);
        }
      }
      await this.router.navigateByUrl(
        `/s/${this.auth.snapshot.account.url_slug}/pos/success`
      );
      this.resetData();
    } catch (e) {
      debug.error('ERROR in creating Order', e);
      this.toastService.error(
        (e.code && (e.msg || e.message)) ||
          'Tạo đơn hàng không thành công. Vui lòng thử lại'
      );
    }
    this.loadingService.end();
  }

  async createReceipt(order: Order) {
    try {
      const body: any = {
        ledger_id: this.selected_ledger.id,
        type: 'receipt',
        paid_at: new Date(),
        trader_id: order.customer_id,
        amount:
          (this.received_amount > this.totalAmount && this.totalAmount) ||
          this.received_amount,
        title: 'Thanh toán đơn hàng',
        ref_type: 'order',
        lines: [
          {
            title: 'Thanh toán đơn hàng',
            amount:
              (this.received_amount > this.totalAmount && this.totalAmount) ||
              this.received_amount,
            ref_id: order.id
          }
        ]
      };
      const receipt = await this.receiptService.createReceipt(body);
      await this.receiptApi.confirmReceipt(receipt.id);
      this.toastService.success('Tạo phiếu thu thành công!');
    } catch (e) {
      this.toastService.error('Tạo phiếu thu không thành công!');
      debug.error('ERROR in creating receipt', e.message || e.msg);
    }
  }

  async changePaymentMethod() {
    return this.toastService.warning('Tính năng đang phát triển');
    // const modal = await this.modalCtrl.create({
    //   component: ChangePaymentMethodModalComponent,
    //   componentProps: {},
    //   animated: false
    // });
    // modal.onDidDismiss().then(_ => {
    //   this.selected_ledger = this.receiptStore.snapshot.selected_ledger;
    //   if (this.selected_ledger.type == 'cash') {
    //     this.paymentMethodDisplay = 'Khách trả tiền mặt';
    //   } else {
    //     this.paymentMethodDisplay = 'Khách chuyển khoản';
    //   }
    // });
    // return await modal.present();
  }

  searchCustomer(search_by: string) {
    this.suggestDisplay = true;
    let query: string;
    switch (search_by) {
      case 'name':
        query = this.customer.full_name;
        break;
      case 'phone':
        query = this.customer.phone;
        break;
      default:
        query = this.customer.full_name;
        break;
    }
    debug.log('query', query);
    setTimeout(_ => {
      if (!query) {
        this.customerSearchResult = [];
        this.suggestDisplay = false;
      } else {
        query = this.util.makeSearchText(query);
        this.customerSearchResult = this.customerList.filter(
          c => c.search_text.indexOf(query.toLowerCase()) > -1
        );

        debug.log('customerSearchResult', this.customerSearchResult);
      }
    }, 250);
  }

  selectCustomer(customer: Customer) {
    this.customer.full_name = customer.full_name;
    this.customer.phone = customer.phone;
  }

  hideSearchResult() {
    setTimeout(_ => {
      this.customerSearchResult = [];
    }, 200);
    this.suggestDisplay = false;
  }

  goToNextInputField(fieldName: IonInput) {
    fieldName.setFocus();
  }
}
