import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { TabsModule } from '../../components/tabs/tabs.module';
import { MenuModule } from '../../components/menu/menu.module';
import { PosOrderLinesComponent } from 'apps/ionic-faboshop/src/app/pages/pos/pos-order-lines/pos-order-lines.component';
import { PosProductItemComponent } from 'apps/ionic-faboshop/src/app/pages/pos/components/pos-product-item/pos-product-item.component';
import { PosBasketComponent } from './pos-basket/pos-basket.component';
import { PosPaymentComponent } from './pos-payment/pos-payment.component';
import { ReceiptService } from 'apps/shop/src/services/receipt.service';
import { PosSuccessComponent } from './pos-success/pos-success.component';
import { ProductService } from 'apps/shop/src/services/product.service';
import { ChangePaymentMethodModalComponent } from './components/change-payment-method-modal/change-payment-method-modal.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { CreateProductModule } from 'apps/ionic-faboshop/src/app/components/create-product/create-product.module';
import { SelectVariantModalComponent } from './components/select-variant-modal/select-variant-modal.component';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { ImageCompressor } from '@etop/utils/image-compressor/image-compressor.service';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'lines',
    pathMatch: 'full'
  },
  {
    path: 'lines',
    component: PosOrderLinesComponent
  },
  {
    path: 'basket',
    component: PosBasketComponent
  },
  {
    path: 'payment',
    component: PosPaymentComponent
  },
  {
    path: 'success',
    component: PosSuccessComponent
  }
];

@NgModule({
  declarations: [
    PosOrderLinesComponent,
    PosProductItemComponent,
    PosBasketComponent,
    PosPaymentComponent,
    PosSuccessComponent,
    ChangePaymentMethodModalComponent,
    SelectVariantModalComponent
  ],
  entryComponents: [
    ChangePaymentMethodModalComponent,
    SelectVariantModalComponent
  ],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    CreateProductModule,
    NetworkStatusModule,
    AuthenticateModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    ReceiptService,
    ProductService,
    BarcodeScanner,
    MobileUploader,
    ImageCompressor,
    PosStoreService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PosModule {}
