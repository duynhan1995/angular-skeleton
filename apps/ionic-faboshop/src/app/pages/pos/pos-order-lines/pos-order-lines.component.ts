import { Component, OnInit, NgZone } from '@angular/core';
import { ProductService } from 'apps/shop/src/services/product.service';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Product, Variant } from 'libs/models/Product';
import { PosStoreService } from 'apps/core/src/stores/pos.store.service';
import { OrderLine } from 'libs/models/Order';
import { AlertController, ModalController, NavController, ToastController, PopoverController } from '@ionic/angular';
import { CreateProductComponent } from '../../../components/create-product/create-product.component';
import { takeUntil } from 'rxjs/operators';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { LoadingService } from 'apps/ionic-faboshop/src/app/services/loading.service';
import { SelectVariantModalComponent } from 'apps/ionic-faboshop/src/app/pages/pos/components/select-variant-modal/select-variant-modal.component';

import { Plugins, NetworkStatus } from '@capacitor/core';

const { Network } = Plugins;

const ANIMATION_DURATION = 500;
// NOTE: Remeber to change duration in SCSS file when you want to change duration time

@Component({
  selector: 'etop-pos',
  templateUrl: './pos-order-lines.component.html',
  styleUrls: ['./pos-order-lines.component.scss']
})
export class PosOrderLinesComponent extends BaseComponent implements OnInit {
  products: Product[] = [];
  variants: Variant[] = [];
  lines: OrderLine[] = [];

  showQuickBasket = false;

  animating_up = false;
  animating_down = false;
  old_quantity = 0;

  status: NetworkStatus;
  viewDisconnect = false;
  networkStatus = true;
  loading = true;

  constructor(
    private toastController: ToastController,
    private productService: ProductService,
    private posStore: PosStoreService,
    private auth: AuthenticateStore,
    private modalCtrl: ModalController,
    private barcodeScanner: BarcodeScanner,
    private alertController: AlertController,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private popoverController: PopoverController,
    private zone: NgZone
  ) {
    super();
    this.viewDisconnect = false;
    this.zone.run(async () => {
      this.status = await this.getStatus();
      if (!this.status.connected) {
        if (this.products.length == 0) {
          this.viewDisconnect = true;
        }
        this.networkStatus = false;
      }
    });
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      if (status.connected) {
        this.zone.run(async () => {
          this.viewDisconnect = false;
          await this.prepareData();
          setTimeout(() => {
            this.networkStatus = true;
          }, 1000);
        });
      } else {
        this.zone.run(async () => {
          this.networkStatus = false;
          if (!this.products) {
            this.viewDisconnect = true;
          }
        });
      }
    });
  }

  get totalItems() {
    return this.lines.reduce((a, b) => a + Number(b.quantity), 0);
  }

  get basketValue() {
    return this.lines.reduce(
      (a, b) => a + Number(b.quantity) * Number(b.retail_price),
      0
    );
  }

  async ngOnInit() {
    this.posStore.onOrderCreated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.lines = [];
        this.prepareData();
      });
    this.prepareData();
  }

  async getStatus() {
    return await Network.getStatus();
  }

  async prepareData() {
    try {
      this.loading = true;
      const res = await this.productService.getProducts(0, 1000);
      this.products = res.products.map(prod => this.productMap(prod));
      this.variants = this.products.reduce((a, b) => a.concat(b.variants), []);
      this.loading = false;
    } catch (e) {
      this.loading = false;
      debug.error('ERROR in preparing data', e);
    }
  }

  async showPopover() {
    this.showQuickBasket = !this.showQuickBasket;
  }

  private countLines() {
    const total_quantity = this.lines.reduce(
      (a, b) => a + Number(b.quantity),
      0
    );
    if (total_quantity > this.old_quantity) {
      this.animating_up = true;
    }
    if (total_quantity < this.old_quantity) {
      this.animating_down = true;
    }
    this.old_quantity = total_quantity;
    setTimeout(_ => {
      this.animating_up = false;
      this.animating_down = false;
    }, ANIMATION_DURATION);
  }

  lineUpdated(line: OrderLine) {
    line.quantity = Number(line.quantity);
    this.countLines();
    this.posStore.changeLines(this.lines);
  }

  removeLine(index: number) {
    this.onRemoveLine(this.lines[index]);
    this.lines.splice(index, 1);
    this.countLines();
    this.posStore.changeLines(this.lines);
  }

  checkBasket(line: OrderLine) {
    line.quantity = Number(line.quantity);
    if (line.quantity == 0) {
      line.quantity = 1;
    }
    this.countLines();
    this.posStore.changeLines(this.lines);
  }

  scanBarcode() {
    this.barcodeScanner
      .scan()
      .then(async barcodeData => {
        debug.log('Barcode data', barcodeData);
        if (!barcodeData.text) {
          return;
        }
        const variant = this.variants.find(v => v.code == barcodeData.text);
        if (variant) {
          this.addToOrderLines(variant);
        } else {
          await this.noSkuFound(barcodeData.text);
        }
      })
      .catch(err => {
        debug.error('ERROR in scanning barcode', err);
      });
  }

  async noSkuFound(barcode) {
    const alert = await this.alertController.create({
      header: 'Không tìm thấy!',
      message: `Mã sản phẩm không tồn tại. Bạn có muốn tạo sản phẩm mới với mã ${barcode} không?`,
      buttons: [
        {
          text: 'Không',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: () => {}
        },
        {
          text: 'Tạo',
          cssClass: 'text-primary font-12',
          handler: () => {
            this.createProduct(barcode);
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  addToOrderLines(variant: Variant) {
    const index = this.lines.findIndex(l => l.variant_id == variant.id);
    if (index >= 0) {
      this.lines[index].quantity += 1;
    } else {
      this.lines.push({
        variant_id: variant.id,
        product_id: variant.product_id,
        retail_price: variant.retail_price,
        payment_price: variant.retail_price,
        quantity: 1,
        product_name: this.productService.makeProductNameForOrderLines(variant),
        image_url: variant.image,
        attributes: variant.attributes
      });
    }
    this.countLines();
    this.posStore.changeLines(this.lines);
  }

  async toBasket() {
    if (!this.status.connected) {
      return;
    }
    if (!this.lines.length) {
      return this.toastService.error('Vui lòng chọn sản phẩm!');
    }
    await this.navCtrl.navigateForward(
      `/s/${this.auth.snapshot.account.url_slug}/pos/basket`,
      { animated: false }
    );
  }

  async createProduct(barcode?) {
    const modal = await this.modalCtrl.create({
      component: CreateProductComponent,
      componentProps: {
        barcode
      },
      animated: false
    });
    modal.onDidDismiss().then(async data => {
      if (data) {
        await this.loadingService.start('Đang xử lý');
        try {
          const product = this.productMap(data.data.product);
          this.onNewProduct(product);
        } catch (e) {
          debug.error(
            'ERROR after creating product and adding to basket value'
          );
        }
        this.loadingService.end();
      }
    });
    return await modal.present();
  }

  async selectVariant(product: Product) {
    if (product.variants && product.variants.length == 1) {
      product.p_data.selected = !product.p_data.selected;
      product.variants[0].p_data.selected = !product.variants[0].p_data
        .selected;
      this.singleVariantHandler(product.variants[0]);
    } else {
      const popover = await this.popoverController.create({
        component: SelectVariantModalComponent,
        translucent: true,
        componentProps: {
          product
        },
        cssClass: 'select-variant',
        animated: true,
        showBackdrop: true,
        backdropDismiss: false
      });
      popover.onDidDismiss().then(data => {
        if (data && data.data && data.data.variant) {
          const variant = data.data.variant;
          product.variants.forEach(v => {
            if (v.id == variant.id) {
              return (v.p_data.selected = variant.p_data.selected);
            }
          });
          product.p_data.selected = product.variants.some(
            v => v.p_data.selected
          );
          this.singleVariantHandler(variant);
        }
      });
      return await popover.present();
    }
  }

  private singleVariantHandler(variant: Variant) {
    if (variant.p_data.selected) {
      this.addToOrderLines(variant);
    } else {
      const idx = this.lines.findIndex(l => l.variant_id == variant.id);
      if (idx >= 0) {
        this.removeLine(idx);
      }
    }
  }

  private onNewProduct(product: Product) {
    product.p_data.selected = true;
    product.variants[0].p_data.selected = true;
    this.products.unshift(product);
    this.variants.unshift(product.variants[0]);
    const variant = (product.variants && product.variants[0]) || null;
    if (variant) {
      this.addToOrderLines(variant);
      this.toastService.success('Đã thêm sản phẩm vào giỏ hàng');
    }
  }

  onRemoveLine(line: OrderLine) {
    return this.products.forEach(prod => {
      if (prod.id == line.product_id) {
        if (prod.variants.length > 1) {
          prod.variants.forEach(v => {
            if (v.id == line.variant_id) {
              v.p_data.selected = false;
              return;
            }
          });
          prod.p_data.selected = prod.variants.some(v => v.p_data.selected);
        } else {
          prod.p_data.selected = false;
          prod.variants[0].p_data.selected = false;
        }
        return;
      }
    });
  }

  private productMap(product: Product): Product {
    product.p_data = {
      selected: false
    }
    product.variants.forEach(v => {
      v.p_data.selected = false;
      v.product_id = product.id;
    });
    debug.log('product id', product)
    return {
      ...product
    };
  }
}

