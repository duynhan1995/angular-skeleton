import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { NavController, ActionSheetController, AlertController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'libs/models/Product';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { UtilService } from 'apps/core/src/services/util.service';
import { ProductService } from '../../products.service';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { ProductStore } from '../../products.store';
import { AddAttributeNameComponent } from '../../components/add-attribute-name/add-attribute-name.component';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';

@Component({
  selector: 'etop-variant-list',
  templateUrl: './variant-list.component.html',
  styleUrls: ['./variant-list.component.scss']
})
export class VariantListComponent implements OnInit {
  product = new Product({});
  variants = this.fb.array([]);
  changeData = false;

  constructor(
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private actionSheetController: ActionSheetController,
    private toast: ToastService,
    private fb: FormBuilder,
    private alertController: AlertController,
    private zone: NgZone,
    private util: UtilService,
    private productStore: ProductStore,
    private changeDetector: ChangeDetectorRef,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private promiseQueue: PromiseQueueService
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.variants = this.fb.array([]);
    this.prepareData();
    this.variants.valueChanges.subscribe(val => {
      this.changeData = true;
    });
  }

  async back() {
    this.navCtrl.back();
  }

  async prepareData() {
    const id = this.activatedRoute.snapshot.params.id;
    this.product = this.productStore.snapshot.activeProduct;
    if (this.product?.id != id) {
      this.product = await this.productService.fetchProduct(id, true);
    }
    const variants: any = this.product.variants;
    this.resetData(variants);
  }

  resetData(variants) {
    variants.forEach(variant => {
      const { id, code, retail_price, attributes } = variant;
      const _variant = { id, code, retail_price, attributes };
      _variant.attributes = this.fb.array(
        variant.attributes.map(attr => this.fb.group(attr))
      );

      this.variants.push(this.fb.group(_variant));
    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Chỉnh sửa thuộc tính',
          handler: () => {
            if (this.variants.value.length == 1 && this.variants.value[0].attributes?.length == 0) {
              return this.toast.error('Sản phẩm chưa có thuộc tính! Vui lòng thêm thuộc tính trước.')
            }
            this.zone.run(async () => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/attribute`
              );
            });
          }
        },
        this.variants.value[0].attributes.length < 3 && {
          text: 'Thêm thuộc tính',
          handler: () => {
            this.zone.run(async () => {
              this.addAttribute();
            });
          }
        },
        {
          text: 'Thêm mẫu mã',
          handler: () => {
            if (this.variants.value.length == 1 && this.variants.value[0].attributes?.length == 0) {
              return this.toast.error('Vui lòng thêm thuộc tính trước khi thêm mẫu mã.')
            }
            this.zone.run(async () => {
              this.navCtrl.navigateForward(
                `/s/${this.util.getSlug()}/products/variant/create`
              );
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ].filter(_ => !!_)
    });
    await actionSheet.present();
  }

  async addAttribute() {
    const modal = await this.modalController.create({
      component: AddAttributeNameComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.mapVariants(data.data);
      }
    });
    return await modal.present();
  }

  mapVariants(attribute) {
    this.variants.value.forEach(v => {
      v.attributes.push({
        name: attribute,
        value: ''
      });
    });
    const _variants = this.variants.value;
    this.variants = this.fb.array([]);
    this.resetData(_variants);
  }
  async updateVariant() {
    try {
      const promises = this.variants.value.map(v => async () => {
        v.retail_price = Number(v.retail_price);
        await this.productService.updateVariant(v);
      });
      await this.promiseQueue.run(promises, 1);
      this.productService.fetchProduct(this.product.id, true);
      this.toast.success('Cập nhật thành công');
    } catch (e) {
      debug.log('ERROR in updateVariant', e);
      this.toast.error('Cập nhật thất bại' + e.message);
    }
  }

  async delVariant(variant, index) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Xóa mẫu mã!',
      message: `Bạn thực sự muốn xóa mẫu mã <strong>${variant.code}</strong>!!!`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            debug.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: () => {
            this.removeVariant(variant.id, index);
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  async removeVariant(id, index) {
    try {
      await this.productService.removeVariants([id]);
      this.product = await this.productService.fetchProduct(
        this.product.id,
        true
      );
      // this.prepareData();
      this.variants.removeAt(index);
      this.changeDetector.detectChanges();
      this.toast.success('Đã xóa 1 mẫu mã');
    } catch (e) {
      debug.log('ERROR in remove variant', e);
      this.toast.error(e.message);
    }
  }
}
