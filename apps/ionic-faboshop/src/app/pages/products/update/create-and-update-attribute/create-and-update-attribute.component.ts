import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ProductStore } from '../../products.store';
import { FormBuilder } from '@angular/forms';
import { LoadingService } from 'apps/ionic-faboshop/src/app/services/loading.service';
import { ProductService } from '../../products.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { ToastService } from '../../../../services/toast.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'etop-create-and-update-attribute',
  templateUrl: './create-and-update-attribute.component.html',
  styleUrls: ['./create-and-update-attribute.component.scss']
})
export class CreateAndUpdateAttributeComponent implements OnInit {
  product: any;
  attributes = this.fb.array([]);
  constructor(
    private navCtrl: NavController,
    private alertController: AlertController,
    private productStore: ProductStore,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private productService: ProductService,
    private promiseQueue: PromiseQueueService,
    private toastService: ToastService,
    private util: UtilService,
    private zone: NgZone
  ) {}

  ngOnInit(): void {}

  ionViewWillEnter() {
    this.product = this.productStore.snapshot.activeProduct;
    this.product.variants.map(variant => {
      variant.attributes.map(a => {
        let attributesName = [];
        attributesName = this.attributes.value.map(attr => attr.name);
        if (!attributesName.includes(a.name)) {
          this.attributes.push(
            this.fb.group({
              oldName: a.name,
              name: a.name,
              values: this.getAttrValues(a.name)
            })
          );
        }
      });
    });
  }

  getAttrValues(name) {
    let values = [];
    this.product.variants.forEach(v => {
      let attribute = v.attributes.find(attr => attr.name == name);
      if (attribute && values.indexOf(attribute.value) < 0) {
        values.push(attribute.value);
      }
    });
    return values.join(', ');
  }

  back() {
    this.navCtrl.back();
  }

  async delAttribute(attribute, index) {
    const alert = await this.alertController.create({
      header: 'Xóa thuộc tính!',
      message: `Bạn thực sự muốn xóa thuộc tính <strong>${attribute.name}</strong>!!!`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            debug.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: async () => {
            if (this.checkRemoveAttribute(attribute) == 0) {
              this.zone.run(async() => {
                await this.confirmDelAttribute(attribute, index);
              })
            } else {
              this.notUpdateAttribute(attribute);
            }
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  async notUpdateAttribute(attribute) {
    const alert = await this.alertController.create({
      header: 'Xóa thuộc tính!',
      message: `Bạn không thể xóa thuộc tính <strong>${attribute.name}</strong>. Nếu thuộc tính này bị xóa sẽ dẫn các phiên bản bị trùng thuộc tính`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            debug.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Đã hiểu',
          cssClass: 'text-danger font-12',
          handler: blah => {
            debug.log('Confirm Cancel: blah');
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  async confirmDelAttribute(attribute, index) {
    try {
      let variants = this.findVariantAttribute(
        this.product.variants,
        attribute
      );
      const promises = variants.map(v => async () => {
        await this.productService.updateVariant(v);
      });
      await this.promiseQueue.run(promises, 1);
      this.productService.fetchProduct(this.product.id, true);
      this.attributes.removeAt(index);
      this.toastService.success('Xóa thuộc tính thành công');
    } catch (e) {
      this.toastService.error(e.message);
    }
  }

  checkRemoveAttribute(attribute) {
    let quantity = 0;
    let _variants = this.findVariantAttribute(this.product.variants, attribute);
    // check with old list variants
    this.product.variants.some(variant => {
      _variants.some(v => {
        if (v.attributes.length == variant.attributes.length) {
          v.attributes.some((a, index) => {
            if (
              a.name == variant.attributes[index].name &&
              a.value == variant.attributes[index].value
            ) {
              ++quantity;
            }
          });
        }
      });
    });
    // check with list variants after updated
    _variants.forEach((v, i) => {
      _variants.forEach((_v, j) => {
        if (i != j && _v.attributes.length == v.attributes.length) {
          _v.attributes.some((a, index) => {
            if (
              a.name == v.attributes[index].name &&
              a.value == v.attributes[index].value
            ) {
              ++quantity;
            }
          });
        }
      });
    });
    return quantity;
  }

  findVariantAttribute(variants, attribute) {
    let _variants: any = [];
    variants.forEach(v => {
      v.attributes.forEach(attr => {
        if (attr.name == attribute.oldName) {
          _variants.push({
            id: v.id,
            attributes: this.mapAttr(v.attributes, attribute.oldName)
          });
        }
      });
    });
    return _variants;
  }

  mapAttr(attributes, name) {
    let _attrs: any = [];
    attributes.forEach(attr => {
      if (attr.name != name) {
        _attrs.push({
          name: attr.name,
          value: attr.value
        });
      }
    });
    return _attrs;
  }

  async updateAttributes() {
    await this.loadingService.start('Đang cập nhật thuộc tính');
    try {
      let _variants: any = [];
      this.product.variants.forEach(v => {
        v.attributes.map(attr => {
          this.attributes.value.forEach(a => {
            if (a.oldName != a.name && a.oldName == attr.name) {
              attr.name = a.name;
              _variants.push({
                id: v.id,
                attributes: v.attributes
              });
            }
          });
        });
      });
      const promises = _variants.map(v => async () => {
        await this.productService.updateVariant(v);
      });
      await this.promiseQueue.run(promises, 1);
      await this.productService.fetchProduct(this.product.id, true);
      this.navCtrl.back();
    } catch (e) {
      this.toastService.error(e.message);
    }

    this.loadingService.end();
  }
}
