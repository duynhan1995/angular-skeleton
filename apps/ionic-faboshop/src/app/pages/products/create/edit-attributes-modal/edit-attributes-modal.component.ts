import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { ProductStore } from '../../products.store';

@Component({
  selector: 'etop-edit-attributes-modal',
  templateUrl: './edit-attributes-modal.component.html',
  styleUrls: ['./edit-attributes-modal.component.scss']
})
export class EditAttributesModalComponent implements OnInit {
  product: any;
  attributes = this.fb.array([]);
  valueChange = false;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private productStore: ProductStore,
    private alertController: AlertController,
    private changeDetector: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {}

  back() {
    this.modalCtrl.dismiss(this.valueChange);
  }

  ionViewWillEnter() {
    this.resetData();
  }

  resetData() {
    this.attributes = this.fb.array([]);
    this.product = this.productStore.snapshot.productCreate;
    if (this.product.variants) {
      let attributes =
        (this.product.variants &&
          this.product.variants[0] &&
          this.product.variants[0].attributes) ||
        [];
      attributes.map(a => {
        this.attributes.push(
          this.fb.group({
            oldName: a.name,
            name: a.name,
            values: this.getAttrValues(a.name)
          })
        );
      });
    }
  }

  getAttrValues(name) {
    let values = [];
    this.product.variants.forEach(v => {
      let attribute = v.attributes.find(attr => attr.name == name);
      if (attribute && values.indexOf(attribute.value) < 0) {
        values.push(attribute.value);
      }
    });
    return values.join(', ');
  }

  async delAttribute(attribute) {
    const alert = await this.alertController.create({
      header: 'Xóa thuộc tính!',
      message: `Bạn thực sự muốn xóa thuộc tính <strong>${attribute.name}</strong>!!!`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {
            debug.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Xóa',
          cssClass: 'text-danger font-12',
          handler: async () => {
            this.product.variants.forEach(v => {
              let _attributes = [];
              v.attributes.map(attr => {
                if (attr.name != attribute.oldName) {
                  _attributes.push(attr);
                }
              });
              v.attributes = _attributes;
            });
            this.productStore.setProductCreate(this.product);
            this.valueChange = true;
            this.resetData();
            this.changeDetector.detectChanges();
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  async updateAttributes() {
    this.product.variants.forEach(v => {
      v.attributes.map(attr => {
        this.attributes.value.map(a => {
          if (a.oldName != a.name && a.oldName == attr.name) {
            attr.name = a.name;
          }
        });
      });
    });
    this.productStore.setProductCreate(this.product);
    this.modalCtrl.dismiss(true);
  }
}
