import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController, ActionSheetController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { ProductStore } from '../../products.store';
import { UtilService } from 'apps/core/src/services/util.service';
import { AddAttributeNameComponent } from '../../components/add-attribute-name/add-attribute-name.component';
import { AddVariantModalComponent } from '../add-variant-modal/add-variant-modal.component';
import { EditAttributesModalComponent } from '../edit-attributes-modal/edit-attributes-modal.component';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { Variant } from '@etop/models/Product';

@Component({
  selector: 'etop-variants-edit',
  templateUrl: './variants-edit.component.html',
  styleUrls: ['./variants-edit.component.scss']
})
export class VariantsEditComponent implements OnInit {
  variants = this.fb.array([]);
  product: any;
  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private productStore: ProductStore,
    private util: UtilService,
    private actionSheetController: ActionSheetController,
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    this.product = this.productStore.snapshot.productCreate;
    this.resetData(this.product.variants);
  }

  resetData(variants) {
    this.variants = this.fb.array([]);
    variants.forEach(variant => {
      const { code, retail_price, attributes } = variant;
      const _variant = { code, retail_price, attributes };
      _variant.attributes = this.fb.array(
        variant.attributes.map(attr => this.fb.group(attr))
      );

      this.variants.push(this.fb.group(_variant));
    });
  }

  async back() {
    this.navCtrl.back();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Chỉnh sửa thuộc tính',
          handler: () => {
            this.zone.run(async () => {
              this.editAttributes();
            });
          }
        },
        {
          text: 'Thêm thuộc tính',
          handler: () => {
            this.zone.run(async () => {
              this.addAttribute();
            });
          }
        },
        {
          text: 'Thêm mẫu mã',
          handler: () => {
            this.zone.run(async () => {
              this.addVariant();
            });
          }
        },
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            debug.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async addAttribute() {
    if (this.product.variants[0].attributes.length == 3) {
      return this.toast.error('Chỉ có thể tạo tối đa 3 thuộc tính');
    }
    const modal = await this.modalController.create({
      component: AddAttributeNameComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.mapVariants(data.data);
      }
    });
    return await modal.present();
  }

  async addVariant() {
    const modal = await this.modalController.create({
      component: AddVariantModalComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.product = this.productStore.snapshot.productCreate;
        this.resetData(this.product.variants);
      }
    });
    return await modal.present();
  }

  async editAttributes() {
    const modal = await this.modalController.create({
      component: EditAttributesModalComponent,
      swipeToClose: false,
      presentingElement: this.routerOutlet.nativeEl
    });
    modal.onWillDismiss().then(async data => {
      if (data?.data) {
        this.product = this.productStore.snapshot.productCreate;
        this.resetData(this.product.variants);
      }
    });
    return await modal.present();
  }

  mapVariants(attribute) {
    this.variants.value.forEach(v => {
      v.attributes.push({
        name: attribute,
        value: ''
      });
    });
    const _variants = this.variants.value;
    this.variants = this.fb.array([]);
    this.resetData(_variants);
  }

  checkVariantCode(variants: Array<Variant>) {
    let empty = 0;
    variants.forEach(v => {
      if (!v.code) {
        ++empty;
      }
    });
    return empty;
  }

  async updateVariant() {
    let _check = this.checkVariantCode(this.variants.value);
    if (_check > 0 && _check < this.variants.value.length) {
      await this.toast.error(`Vui lòng nhập mã mẫu mã đầy đủ`);
      return;
    }
    this.product.variants = this.variants.value;
    this.productStore.setActiveProduct(this.product);
    this.back();
  }

  delVariant(index) {
    this.variants.removeAt(index);
    this.changeDetector.detectChanges();
  }
}
