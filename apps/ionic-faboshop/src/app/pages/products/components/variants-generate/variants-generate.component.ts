import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormArray } from '@angular/forms';
import { ProductService } from '../../products.service';
import { ProductStore } from '../../products.store';
import { ToastService } from 'apps/ionic-faboshop/src/app/services/toast.service';
import { Variant } from '@etop/models/Product';

@Component({
  selector: 'etop-variants-generate',
  templateUrl: './variants-generate.component.html',
  styleUrls: ['./variants-generate.component.scss']
})
export class VariantsGenerateComponent implements OnInit {
  @Input() attributes;
  @Input() product;
  variants = this.fb.array([]);

  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private productService: ProductService,
    private productStore: ProductStore,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  async ionViewWillEnter() {
    let variants = this.productService.getCombineVariantsFromAttrs(
      this.attributes
    );
    variants.forEach((variant) => {
      variant.retail_price = this.product?.retail_price || null;
      variant.attributes = this.fb.array(
        variant.attributes.map(attr => this.fb.group(attr))
      );
      this.variants.push(this.fb.group(variant));
    });
  }

  removeVariant(index) {
    this.variants.removeAt(index);
  }

  back() {
    this.modalCtrl.dismiss({ submit: false });
  }

  checkVariantCode(variants: Variant[]) {
    let empty = 0;
    variants.forEach(v => {
      if (!v.code) {
        ++empty;
      }
    })
    return empty;
  }

  async submit() {
    this.variants.value.forEach((variant, index) => {
      if (!variant?.retail_price) {
        throw this.toast.error(`Vui lòng nhập giá cho mẫu mã ${index + 1}`);
      }
    });
    let _check = this.checkVariantCode(this.variants.value);
    if (_check > 0 && _check < this.variants.value.length) {
      await this.toast.error(`Vui lòng nhập mã mẫu mã đầy đủ`);
      return;
    }
    this.product.variants = this.variants.value;
    this.productStore.setProductCreate(this.product);
    this.modalCtrl.dismiss({ submit: true });
  }
}
