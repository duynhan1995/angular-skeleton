import { NgModule } from '@angular/core';
import { ProductListComponent } from './product-list.component';
import { SharedModule } from 'apps/ionic-faboshop/src/app/features/shared/shared.module';
import { EtopPipesModule, IonInputFormatNumberModule } from '@etop/shared';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabsModule } from 'apps/ionic-faboshop/src/app/components/tabs/tabs.module';
import { MenuModule } from 'apps/ionic-faboshop/src/app/components/menu/menu.module';
import { NetworkDisconnectModule } from 'apps/ionic-faboshop/src/app/components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from 'apps/ionic-faboshop/src/app/components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { EmptyPermissionModule } from '../../../components/empty-permission/empty-permission.module';

@NgModule({
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    IonInputFormatNumberModule,
    ReactiveFormsModule,
    EmptyPermissionModule
  ],
  declarations: [ProductListComponent],
  exports: [ProductListComponent]
})
export class ProductListModule{}
