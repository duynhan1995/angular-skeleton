import { Component, OnInit, NgZone } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UtilService } from 'apps/core/src/services/util.service';
import { ProductStore } from '../products.store';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { ProductService } from '../products.service';
import { Product } from '@etop/models';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'etop-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  loading = true;
  products$ = this.productStore.state$.pipe(map(s => s?.products), distinctUntilChanged());

  constructor(
    private navCtrl: NavController,
    private util: UtilService,
    private productStore: ProductStore,
    private productService: ProductService,
    private zone: NgZone,
    private auth: AuthenticateStore
  ) {}

  ngOnInit() { }

  back() {
    this.navCtrl.back();
  }

  async ionViewWillEnter() {
    if (this.auth.snapshot.permission?.permissions.includes('shop/product/basic_info:view')) {
      await this.productService.fetchProducts();
    }
    
    this.loading = false;
  }

  hasPermission() {
    return this.auth.snapshot.permission?.permissions.includes('shop/product/basic_info:view')
  }

  async doRefresh(event) {
    await this.productService.fetchProducts();
    event.target.complete();
  }

  priceCompare(product) {
    return this.util.compareProductPrice(product);
  }

  async productDetail(product) {
    this.productService.fetchProduct(product.id);
    this.zone.run(async () => {
      this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/products/detail/${product.id}`);
    })
  }

  addProduct() {
    this.productStore.setProductCreate(null);
    this.zone.run(async () => {
      this.navCtrl.navigateForward(`/s/${this.util.getSlug()}/products/create`);
    })
  }

  trackProduct(index, product: Product) {
    return product.id;
  }
}
