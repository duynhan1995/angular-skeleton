import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProductsComponent } from './products.component';
import { SharedModule } from '../../features/shared/shared.module';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { TabsModule } from '../../components/tabs/tabs.module';
import { MenuModule } from '../../components/menu/menu.module';
import { MobileUploader } from '@etop/ionic/features/uploader/MobileUploader';
import { ImageCompressor } from '@etop/utils/image-compressor/image-compressor.service';
import { NetworkDisconnectModule } from '../../components/network-disconnect/network-disconnect.module';
import { NetworkStatusModule } from '../../components/network-status/network-status.module';
import { AuthenticateModule } from '@etop/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ProductStore } from './products.store';
import { ProductService } from './products.service';
import { ProductApi } from '@etop/api';
import { IonInputFormatNumberModule } from '@etop/shared/components/etop-ionic/ion-input-format-number/ion-input-format-number.module';
import { AddAttributeComponent } from './add-attribute/add-attribute.component';
import { VariantsGenerateComponent } from './components/variants-generate/variants-generate.component';
import { ProductDetailComponent } from './update/product-detail/product-detail.component';
import { CreateAndUpdateAttributeComponent } from './update/create-and-update-attribute/create-and-update-attribute.component';
import { ProductEditComponent } from './update/product-edit/product-edit.component';
import { VariantListComponent } from './update/variant-list/variant-list.component';
import { ProductCreateComponent } from './create/product-create/product-create.component';
import { CreateAttributeComponent } from './create/create-attribute/create-attribute.component';
import { VariantCreateComponent } from './components/variant-create/variant-create.component';
import { AddAttributeNameComponent } from './components/add-attribute-name/add-attribute-name.component';
import { VariantsEditComponent } from './create/variants-edit/variants-edit.component';
import { AddVariantModalComponent } from './create/add-variant-modal/add-variant-modal.component';
import { EditAttributesModalComponent } from './create/edit-attributes-modal/edit-attributes-modal.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'create', component: ProductCreateComponent },
      { path: 'detail/:id', component: ProductDetailComponent },
      { path: 'attribute', component: CreateAndUpdateAttributeComponent },
      { path: 'attribute/create', component: CreateAttributeComponent },
      { path: 'edit/:id', component: ProductEditComponent },
      { path: 'variant/list/:id', component: VariantListComponent },
      { path: 'variant/create', component: VariantCreateComponent },
      { path: 'variants', component: VariantsEditComponent }
    ]
  }
];

@NgModule({
  declarations: [
    ProductsComponent,
    ProductDetailComponent,
    CreateAndUpdateAttributeComponent,
    ProductEditComponent,
    VariantListComponent,
    ProductCreateComponent,
    CreateAttributeComponent,
    VariantCreateComponent,
    AddAttributeComponent,
    VariantsGenerateComponent,
    AddAttributeNameComponent,
    VariantsEditComponent,
    AddVariantModalComponent,
    EditAttributesModalComponent
  ],
  entryComponents: [],
  imports: [
    SharedModule,
    EtopPipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TabsModule,
    MenuModule,
    NetworkDisconnectModule,
    NetworkStatusModule,
    AuthenticateModule,
    RouterModule.forChild(routes),
    IonInputFormatNumberModule,
    ReactiveFormsModule
  ],
  providers: [
    MobileUploader,
    ImageCompressor,
    BarcodeScanner
  ],
  exports: [ProductsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductsModule {}
