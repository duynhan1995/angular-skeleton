import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, NavController, AlertController } from '@ionic/angular';
import { AuthenticateStore } from '@etop/core';
import { ChangePasswordComponent } from '../../components/change-password/change-password.component';
import { resetStores } from '@datorama/akita';
import { NotificationApi } from '@etop/api';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Plugins } from '@capacitor/core';
import { StorageService } from 'apps/core/src/services/storage.service';
const { Device } = Plugins;

@Component({
  selector: 'etop-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  constructor(
    private modalCtrl: ModalController,
    private alertController: AlertController,
    private auth: AuthenticateStore,
    private navCtrl: NavController,
    private zone: NgZone,
    private notificationApi: NotificationApi,
    private storage: StorageService
  ) {}

  ngOnInit() {
    this.user = this.auth.snapshot.user;
  }

  dismiss() {
    this.navCtrl.back();
  }
  async logout() {
    const alert = await this.alertController.create({
      header: 'Đăng xuất!',
      message: 'Bạn thực sự muốn đăng xuất!',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          cssClass: 'text-medium font-12',
          handler: blah => {}
        },
        {
          text: 'Đăng xuất',
          cssClass: 'text-danger font-12',
          handler: async () => {
            const oneSignal = this.auth.snapshot.oneSignal;
            if (oneSignal) {
              debug.log('deleteDevice');
              this.notificationApi.deleteDevice(oneSignal).then();
            }
            this.auth.clear();
            this.zone.run(async () => {
              resetStores();
              await this.navCtrl.navigateForward('/login', {
                animated: false
              });
            });
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async changePassword() {
    const modal = await this.modalCtrl.create({
      component: ChangePasswordComponent,
      componentProps: {
        user: this.user
      }
    });
    return await modal.present();
  }
}
