import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvitationsComponent } from './invitations.component';
import { RouterModule, Routes } from '@angular/router';
import { EtopPipesModule } from '@etop/shared';
import { IonicModule } from '@ionic/angular';
import { MatIconModule } from '@angular/material/icon';

const routes: Routes = [
  {
    path: '',
    component: InvitationsComponent
  }
];

@NgModule({
  declarations: [InvitationsComponent],
  imports: [
    CommonModule,
    EtopPipesModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule
  ]
})
export class InvitationsModule { }
