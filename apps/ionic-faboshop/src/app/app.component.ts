import { Component, NgZone } from '@angular/core';
import { Platform } from '@ionic/angular';
import { KeyboardResize, Plugins, StatusBarStyle } from '@capacitor/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar as NgxStatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticateService, AuthenticateStore, ConfigService } from '@etop/core';
import { SystemEventService } from './system-event.service';
import {LocationService} from '@etop/state/location';
import { CommonUsecase } from '../../../shared/src/usecases/common.usecase.service';
import { LoadingViewService } from './components/loading-view/loading-view.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../environments/environment';

const { StatusBar, Keyboard, App } = Plugins;

@Component({
  selector: 'etop-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: NgxStatusBar,
    private authenticateService: AuthenticateService,
    private systemEvent: SystemEventService,
    private locationService: LocationService,
    private commonUsecase: CommonUsecase,
    private config: ConfigService,
    private loadingView: LoadingViewService,
    private zone: NgZone,
    private router: Router,
    private auth: AuthenticateStore,
    private activatedRoute: ActivatedRoute
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.loadingView.showLoadingView();
    this.platform.ready().then(() => {
      if (this.platform.is('capacitor')) {
        if (this.platform.is('ios')) {
          StatusBar.setStyle({
            style: StatusBarStyle.Light
          });
        } else {
          StatusBar.setStyle({
            style: StatusBarStyle.Dark
          });
        }
      } else {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }

      this.authenticateService.hookDecorator();

      Plugins.FacebookPlugin.init({
        appId: this.config.get('facebook_app_id')
      });

      // Keyboard.setResizeMode({mode: KeyboardResize.Body})
      Keyboard.addListener('keyboardWillHide', () =>
        this.systemEvent.keyboardWillHide.next()
      );
      Keyboard.addListener('keyboardDidHide', () =>
        this.systemEvent.keyboardDidHide.next()
      );
      Keyboard.addListener('keyboardWillShow', ev =>
        this.systemEvent.keyboardWillShow.next(ev)
      );
      Keyboard.addListener('keyboardDidShow', ev =>
        this.systemEvent.keyboardDidShow.next(ev)
      );
      this.locationService.initLocations().then();

      this.Applinks();

      this.commonUsecase
        .redirectIfAuthenticated()
        .then(() => setTimeout(() => this.loadingView.hideLoadingView(), 2000))
        .then(() => this.Applinks())
    });
  }

  Applinks() {
    App.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        const slug = data.url.split(environment.fabo_url).pop();
        this.auth.setRef(slug.substring(1))
        console.log('environment.fabo_url', environment.fabo_url)
        if (this.auth.snapshot?.user && this.auth.snapshot?.account) {
          this.router.navigateByUrl(`/s/${this.auth.snapshot.account.url_slug || 0}` + slug);
        } else {
          if (slug.includes('/invitation')) {
            this.router.navigateByUrl(`/login?invitation=${slug.replace('/invitation?t=', '')}&type=email`);
          }
          if (slug.includes('/i/p')) {
            this.router.navigateByUrl(`/login?invitation=${slug.replace('/i/p', '')}&type=phone`);
          }
        }
      });
    });
  }

}
