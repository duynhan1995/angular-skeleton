import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'etop-etop-app',
  templateUrl: './faboshop-app.component.html',
  styleUrls: ['./faboshop-app.component.scss']
})
export class FaboshopAppComponent implements OnInit {
  withoutFooterTabsUrls = [
    'confirm-order'
  ];

  constructor(
    private router: Router,
  ) {}

  get hideFooterTabs() {
    const path = this.router.url.split('/').pop();
    return this.withoutFooterTabsUrls.includes(path);
  }

  async ngOnInit() {}
}
