import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FaboAppRoutes } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app.route';
import { FaboshopAppComponent } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app.component';
import { FaboshopAppGuard } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app.guard';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabNavModule } from 'apps/ionic-faboshop/src/app/components/tab-nav/tab-nav.module';

const routes: Routes = [
  {
    path: 's/:shop_index',
    canActivate: [FaboshopAppGuard],
    resolve: {
      account: FaboshopAppGuard
    },
    component: FaboshopAppComponent,
    children: FaboAppRoutes
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabNavModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule]
})
export class FaboAppRoutingModule {}
