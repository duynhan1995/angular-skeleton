import { Routes } from '@angular/router';
import { tabNavRoutes } from 'apps/ionic-faboshop/src/app/components/tab-nav/tab-nav.module';

export const FaboAppRoutes: Routes = [
  ...tabNavRoutes,
  {
    path: 'products',
    loadChildren: () => import('../pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('../pages/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('../pages/customers/customers.module').then(m => m.CustomersModule)
  },
  {
    path: 'pos',
    loadChildren: () => import('../pages/pos/pos.module').then(m => m.PosModule)
  },
  {
    path: 'etop-trading',
    loadChildren: () => import('../pages/etop-trading/etop-trading.module').then(m => m.EtopTradingModule)
  },
  {
    path: 'account',
    loadChildren: () => import('../pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'developing',
    loadChildren: () => import('../pages/developing-view/developing-view.module').then(m => m.DevelopingViewModule)
  },
  {
    path: 'messages',
    loadChildren: () => import('../pages/messages/messages.module').then(m => m.MessagesModule)
  },
  {
    path: 'confirm-order',
    loadChildren: () => import('../pages/confirm-order/confirm-order.module').then(m => m.ConfirmOrderModule),
  },
  {
    path: 'invitation',
    loadChildren: () => import('../pages/invitations/invitations.module').then(m => m.InvitationsModule),
  },
  {
    path: 'i',
    loadChildren: () => import('../pages/invitation-by-phone/invitation-by-phone.module').then(m => m.InvitationByPhoneModule),
  },
  {
    path: '**',
    redirectTo: 'tab-nav'
  }
];
