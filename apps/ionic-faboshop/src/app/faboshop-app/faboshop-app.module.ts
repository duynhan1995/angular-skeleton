import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FaboAppRoutingModule } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app-routing.module';
import { FaboshopAppComponent } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app.component';
import { FaboshopAppGuard } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app.guard';
import { IonicModule } from '@ionic/angular';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { TabsModule } from '../components/tabs/tabs.module';
import {ConfirmOrderModule} from "apps/ionic-faboshop/src/app/pages/confirm-order/confirm-order.module";

const pages = [];

@NgModule({
  declarations: [FaboshopAppComponent, ...pages],
  providers: [FaboshopAppGuard, Deeplinks],
  imports: [
    CommonModule,
    IonicModule,
    EtopPipesModule,
    NgbModule,
    TabsModule,
    FaboAppRoutingModule,
    ConfirmOrderModule.provideStore()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EtopAppModule {}
