import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FacebookConnectComponent } from 'apps/ionic-faboshop/src/app/components/facebook-connect/facebook-connect.component';

import { CoreModule } from './core/core.module';
import { SharedModule } from './features/shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FaboshopAppCommonUsecase } from 'apps/ionic-faboshop/src/app/usecases/faboshop-app-commom.usecase.service';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { IonicModule } from '@ionic/angular';
import { EtopAppModule } from 'apps/ionic-faboshop/src/app/faboshop-app/faboshop-app.module';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';
import { BrowserModule } from '@angular/platform-browser';
import { MenuModule } from 'apps/ionic-faboshop/src/app/components/menu/menu.module';
import { environment } from '../environments/environment';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { NetworkStatusModule } from './components/network-status/network-status.module';
import { CONFIG_TOKEN } from '@etop/core/services/config.service';
import { AuthenticateModule } from '@etop/core';
import { TabsModule } from './components/tabs/tabs.module';
import { ChatConnectionModalComponent } from './components/chat-connection-modal/chat-connection-modal.component';
import { FilterModalComponent } from './components/filter-modal/filter-modal.component';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { LoadingViewModule } from './components/loading-view/loading-view.module';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import { SearchSuggestComponent } from './components/search-modal/search-suggest/search-suggest.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';

const apis = [];

const services = [];

const providers = [
  { provide: CONFIG_TOKEN, useValue: environment },
  { provide: CommonUsecase, useClass: FaboshopAppCommonUsecase },
  { provide: 'SERVICE_URL', useValue: environment.fabo_url },
  ...apis,
  ...services,
  OneSignal,
  Badge
];

// if (environment.production) {
//   providers.unshift({ provide: 'SERVICE_URL', useValue: 'https://etop.vn' });
// }

@NgModule({
  imports: [
    CoreModule,
    SharedModule,
    EtopPipesModule,
    AppRoutingModule,
    EtopAppModule,
    FormsModule,
    BrowserModule,
    TabsModule,
    IonicModule.forRoot({
      mode: 'ios',
      scrollAssist: true
    }),
    MenuModule,
    NetworkStatusModule,
    AuthenticateModule,
    ReactiveFormsModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    LoadingViewModule.forRoot(),
    BrowserAnimationsModule,
    MatIconModule
  ],
  declarations: [
    AppComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    FacebookConnectComponent,
    ChatConnectionModalComponent,
    FilterModalComponent,
    SearchSuggestComponent,
  ],
  entryComponents: [
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    FacebookConnectComponent,
    SearchSuggestComponent
  ],
  providers,
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
