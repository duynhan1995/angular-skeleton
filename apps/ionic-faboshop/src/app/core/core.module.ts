import { NgModule } from '@angular/core';

// libs
import { EtopIonicCoreModule } from '@etop/ionic';
import { UtilService } from 'apps/core/src/services/util.service';
import { UserApi, ShopAccountApi, CategoryApi } from '@etop/api';
import { GoogleAnalyticsService } from 'apps/core/src/services/google-analytics.service';
import { FulfillmentService } from 'apps/faboshop/src/services/fulfillment.service';
import { OrderService } from 'apps/faboshop/src/services/order.service';
import { CmsService } from 'apps/faboshop/src/services/cms.service';
import { ShopService } from 'apps/faboshop/src/services/shop.service';
import { UserService } from 'apps/core/src/services/user.service';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { CustomerService } from 'apps/faboshop/src/services/customer.service';
import { TelegramService } from '../services/telegram.service';
import { LocationApi } from '@etop/api/general/location.api';

const services = [
  UserApi,
  UtilService,
  GoogleAnalyticsService,
  TelegramService,
  FulfillmentService,
  OrderService,
  ShopAccountApi,
  CmsService,
  ShopService,
  UserService,
  UserBehaviourTrackingService,
  CategoryApi,
  PromiseQueueService,
  CustomerService,
  LocationApi
];
const api = [];

@NgModule({
  imports: [EtopIonicCoreModule],
  providers: [...services, ...api]
})
export class CoreModule {}
