import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SystemEventService {
  keyboardWillShow = new Subject();
  keyboardDidShow = new Subject();
  keyboardWillHide = new Subject();
  keyboardDidHide = new Subject();
}
