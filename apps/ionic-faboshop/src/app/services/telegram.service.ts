import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';


@Injectable()
export class TelegramService {
  groupProdIds = {
    newShop: '-298762499',
    updateShop: '-1001400752499',
    newAccount: '-270142863',
    personalMerchant: '-343016665',

    eTop_trading: '-276701871',
    paymentTrading: '-320986068'
  };
  groupId = '-285363565';
  onUpdateExpireTime = new Subject();

  constructor(
    private http: HttpClient,
  ) {}

  getSurveyInfo() {
    let survey_data = JSON.parse(localStorage.getItem('survey'));
    let orders_per_day_text = survey_data.orders_per_day_text;
    let business_lines = survey_data.business_lines;
    return {
      orders_per_day_text,
      business_lines
    };
  }

  async newMerchantMessage(account, device?) {
    const fastBuyGroupId =  this.groupProdIds.newAccount;
    let msg = `
<strong>ĐĂNG KÝ TÀI KHOẢN!</strong>
Họ tên: <strong>${account.full_name} </strong>
Số điện thoại: ${account.phone}
Email: ${account.email}`;
    if (device) {
      msg += `
—————————
Device: ${device.manufacturer} - ${device.model} - ${
        device.operatingSystem
      }  ${device.osVersion}
App version: ${device.appVersion}`;
      msg += `
—————————
REF: ${this.checkRef()}`;
    }
    try {
      await this.sendMessage(fastBuyGroupId, msg);
      debug.log('SENT MESSAGE');
    } catch (e) {
      debug.log('ERROR SEND MESSAGE');
    }
  }

  async newShopMessage(user, shop, address, surveyData?, device?) {
    const fastBuyGroupId = this.groupProdIds.newShop;
    let msg = `
<strong>ĐĂNG KÝ CỬA HÀNG!</strong>
Tên cửa hàng: <strong>${shop.name}</strong>
Số điện thoại: ${shop.phone}
Website: ${shop.website_url}
Email: ${shop.email}
Địa chỉ: ${address}
—————————
Thuộc tài khoản: <strong>${user.full_name}</strong>
Số điện thoại: ${user.phone}
Email: ${user.email}
—————————
${this.getSurveyInfo().orders_per_day_text}
Ngành hàng kinh doanh: ${this.getSurveyInfo().business_lines}`;
    if (device) {
      msg += `
—————————
Device: ${device.manufacturer} - ${device.model} - ${device.operatingSystem}  ${
        device.osVersion
        }
App version: ${device.appVersion}`;
      msg += `
—————————
REF: ${this.checkRef()}`;
    }
    try {
      await this.sendMessage(fastBuyGroupId, msg);
      debug.log('SENT MESSAGE', msg);
    } catch (e) {
      debug.log('ERROR SEND MESSAGE', e);
    }
  }

  checkRef() {
    return localStorage.getItem('REF');
  }

  formatAddress(address) {
    if (!address) {
      return '';
    }
    let res = '';

    const { district, province } = address;

    res += district ? district + ', ' : '';
    res += province || '';

    return res;
  }

  sendMessage(fastBuyGroupId, msg, enableHTML = true) {
    const fastBuyUrl = 'https://api.telegram.org/bot1746704472:AAHuIwK-0IZjRPKub_QirlCpoQmp6dAiN7M/sendMessage';
    const fastbuyData: any = {
      chat_id: fastBuyGroupId,
      text: msg
    };
    if (enableHTML) {
      fastbuyData.parse_mode = 'HTML';
    }

    return this.http.post(fastBuyUrl, fastbuyData).toPromise();
  }
}
