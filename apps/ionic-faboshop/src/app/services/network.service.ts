import { Injectable } from '@angular/core';
import { Plugins, NetworkStatus } from '@capacitor/core';
import { ToastService } from './toast.service';

const { Device, App, Network } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  status;
  constructor(private toast: ToastService) {
    Network.addListener('networkStatusChange', status => {
      this.status = status;
      debug.log('Network status changed', this.status);
      // this.checkNetWork(status);
    });
  }

  private async checkNetWork(status) {
    if (!status.connected) {
      await this.toast.netWorkDiscontect();
    }
    debug.log('checkNetWork');
  }

}
