import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loading: any;

  constructor(
    private loadingController: LoadingController,
  ) { }

  async start(message: string) {
    this.loading = await this.loadingController.create({
      message: message
    });
    this.loading.present();
  }

  end() {
    if(this.loading) {
      this.loading.dismiss();
    }
  }

}
