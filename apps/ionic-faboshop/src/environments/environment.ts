// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  recaptcha_key: '6LcVOnkUAAAAAGd1izSWEZduQuBcExcbVfQMS-7Y',
  fabo_url: 'https://faboshop.d.etop.vn',
  facebook_app_id: '1581362285363031',
  onesignal_app_id: '5186e97c-b919-4eb0-8740-ac5c12f954ae',
  google_project_number: '13267590075'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
