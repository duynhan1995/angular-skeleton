import { Injectable } from '@angular/core';
import { PromiseQueueService } from 'apps/core/src/services/promise-queue.service';
import { Product, Variant } from '@etop/models';
import { UserBehaviourTrackingService } from 'apps/core/src/services/user-behaviour-tracking.service';
import { CategoryApi, CollectionApi, ProductApi, ProductTagsUpdateDto } from '@etop/api';
import { ProductStore } from 'apps/core/src/stores/product.store';

const MIN_PRODUCT_PRICE = 1000;

@Injectable()
export class ProductService {

  constructor(
    private productApi: ProductApi,
    private collectionApi: CollectionApi,
    private categoryApi: CategoryApi,
    private promiseQueue: PromiseQueueService,
    private ubtService: UserBehaviourTrackingService,
    private productStore: ProductStore
  ) {
  }

  static get minProductPrice() {
    return MIN_PRODUCT_PRICE;
  }

  async getProduct(id) {
    return await this.productApi.getProduct(id);
  }

  async updateProduct(editedProduct) {
    let pData = editedProduct.p_data;

    // update product's collections
    await this._updateProductCollections(editedProduct);
    // update product's categories
    await this._updateProductCategory(editedProduct);
    // update product's tags
    await this._updateProductTags(editedProduct);
    // update product's images
    await this._updateProductImages(editedProduct);
    // update product's main info
    await this.productApi.updateProduct({
      id: editedProduct.id,
      name: pData.name,
      code: pData.code,
      desc_html: pData.description,
      short_desc: pData.description,
      description: pData.description
    });
  }

  private async _updateProductCollections(editedProduct) {
    let pData = editedProduct.p_data;
    const editedField = pData.editedField;
    // NOTE: to prevent an array with UNDEFINED items if we only use map(coll => coll.id)
    let newCollectionIds = pData.collections.filter(coll => coll.id && !coll.id.includes('###')).map(coll => coll.id);

    let toCreateCollections = pData.collections.filter(collection => !collection.id || collection.id.includes('###'));

    const promises = toCreateCollections.map(coll => async () => {
      const res = await this.collectionApi.createCollection({ name: coll.name });
      newCollectionIds.push(res.id);
    });
    await this.promiseQueue.run(promises, 10);

    if (editedProduct.collection_ids && editedProduct.collection_ids.length) {
      await this.productApi.removeProductCollection(editedProduct.id, editedProduct.collection_ids);
    }
    if (newCollectionIds && newCollectionIds.length) {
      await this.productApi.addProductCollection(editedProduct.id, newCollectionIds);
    }
  }

  private async _updateProductCategory(editedProduct) {
    let pData = editedProduct.p_data;
    const editedField = pData.editedField;
    if (!editedField['category']) {
      return;
    }
    if (!pData.category) {
      return await this.productApi.removeProductCategory(editedProduct.id);
    }

    let category_id = pData.category && pData.category.id;
    if (!category_id || category_id.includes('###')) {
      const res = await this.categoryApi.createCategory({
        name: pData.category.name
      });
      category_id = res.id;
    }
    await this.productApi.updateProductCategory({
      category_id,
      product_id: editedProduct.id
    });
  }

  private async _updateProductTags(editedProduct) {
    let pData = editedProduct.p_data;
    const editedField = pData.editedField;
    if (!editedField['tags']) {
      return;
    }
    const tags = pData.tags;
    let tagsData: ProductTagsUpdateDto = {
      ids: [editedProduct.id]
    };
    if (tags.length) {
      tagsData.replace_all = tags;
    } else {
      tagsData.delete_all = true;
    }
    await this.productApi.updateProductsTags(tagsData);
  }

  private async _updateProductImages(editedProduct) {
    let pData = editedProduct.p_data;
    const editedField = pData.editedField;
    if (!editedField['images']) {
      return;
    }
    return await this.productApi.updateProductImages({
      id: editedProduct.id,
      replace_all: [editedProduct.p_data.image]
    });
  }

  async updateVariant(editedVariant) {
    return await this.productApi.updateVariant(editedVariant);
  }

  async updateVariants(editedVariants) {
    await Promise.all(editedVariants.map(v => {
      const pData = v.p_data;
      return this.productApi.updateVariant({
        id: v.id,
        retail_price: pData.retail_price,
        list_price: pData.list_price,
        code: pData.code,
        attributes: pData.attributes
      });
    }));
  }

  async removeVariants(ids: Array<string>) {
    return await this.productApi.removeVariants(ids);
  }

  updateProductCategory(data) {
    return this.productApi.updateProductCategory(data);
  }

  addProductCollection(product_id: string, collection_ids: Array<string>) {
    return this.productApi.addProductCollection(product_id, collection_ids);
  }

  updateProductTags(data) {
    return this.productApi.updateProductsTags(data);
  }

  async createVariant(data): Promise<any> {
    const res = await this.productApi.createVariant(data);
    if (res.id) {
      this.ubtService.sendUserBehaviour('CreateVariant');
    }
    return res;
  }

  async createProduct(data: Product): Promise<any> {
    const res = await this.productApi.createProduct(data);
    if (res.id) {
      this.ubtService.sendUserBehaviour('CreateProduct');
    }
    return res;
  }

  deleteProduct(ids: Array<string>) {
    return this.productApi.removeProducts(ids)
  }

  getProducts(start?: number, perpage?: number, filters?: Array<any>) {
    let paging = {
      offset: start || 0,
      limit: perpage || 1000
    };

    return this.productApi.getProducts({ paging, filters })
      .then(res => {
        this.productStore.updateProductList(res.products);
        return res;
      });
  }

  async getVariantsByIDs(ids: Array<string>, token?) {
    let res = await this.productApi.getVariantsByIDs(ids, token);
    return res.variants;
  }

  makeProductNameForOrderLines(variant: Variant) {
    if (variant.name && variant.name != variant.product_name) {
      return variant.product_name + " - " + variant.name;
    }
    if (variant.attributes && variant.attributes.length) {
      return variant.product_name + " - " + variant.attributes.map(attr => attr.value).join(" ");
    }
    return variant.product_name;
  }

}
