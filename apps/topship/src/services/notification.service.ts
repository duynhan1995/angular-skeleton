import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { Noti } from 'libs/models/Noti';
import { Subject } from 'rxjs';
import { AuthenticateStore } from '@etop/core';
import { NotificationApi } from '@etop/api';
import { ConfigService } from '@etop/core/services/config.service';

@Injectable()
export class NotificationService {
  external_device_id = '';
  notifications: Array<Noti> = [];

  oneSignalInitialized = false;
  onOneSignalInitialized = new Subject();

  constructor(
    private util: UtilService,
    private router: Router,
    private notificationApi: NotificationApi,
    private auth: AuthenticateStore,
    private configService: ConfigService
  ) {
    this.init();
  }

  private async init() {
    const OneSignal = window['OneSignal'] || [];
    const config = this.configService.getConfig();
    OneSignal.push(function() {
      OneSignal.init({
        appId: config.onesignal_app_id,
        safari_web_id: config.safari_web_id,
        allowLocalhostAsSecureOrigin: true
      });
      OneSignal.on('notificationDisplay', function(event) {
        debug.log('OneSignal notification displayed:', event);
      });
    });
    this.oneSignalInitialized = true;
    this.onOneSignalInitialized.next();
  }

  async createDevice() {
    this.external_device_id = await OneSignal.getUserId();
    const data = {
      external_device_id: this.external_device_id
    };
    if (!(this.external_device_id)) {
      return;
    }
    const res = await this.notificationApi.createDevice(data);
  }

  async deleteDevice() {
    if (!this.external_device_id) {
      return;
    }
    await this.notificationApi.deleteDevice({
      device_id: this.external_device_id, external_device_id: this.external_device_id
    });
  }

  async getNotification(noti_id, token?) {
    return await this.notificationApi.getNotification(noti_id, token);
  }

  async getNotifications(start?: number, perpage?: number, token?: string) {
    const paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return await this.notificationApi.getNotifications(paging, token);
  }

  async updateNotifications(ids: Array<string>, is_read: boolean) {
    return await this.notificationApi.updateNotifications(ids, is_read);
  }

  async readNotification(noti, shop_id_encoded?) {
    const slug = this.util.getSlug();
    await this.updateNotifications([noti.id], true);
    switch (noti.entity) {
      case 'fulfillment':
        this.router.navigateByUrl(`/s/${slug}/fulfillments/${noti.entity_id}${shop_id_encoded ? `?_s_id=${shop_id_encoded}` : ''}`);
        break;
      case 'money_transaction_shipping':
        this.router.navigateByUrl(`/s/${slug}/money-transactions/${noti.entity_id}${shop_id_encoded ? `?_s_id=${shop_id_encoded}` : ''}`);
        break;
      default:
        break;
    }
  }

}
