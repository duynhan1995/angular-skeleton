import { Injectable } from '@angular/core';
import { AppService } from '@etop/web/core/app.service';

const HTML = {
  html_topship_balance_warning: `
    <span>
      Vui lòng liên hệ Topship thông qua
      <a href="https://www.facebook.com/topship.asia/" target="_blank" class="text-primary font-weight-bold">
        Facebook
      </a>
      hoặc
      <a href="https://www.topship.vn/" target="_blank" class="text-primary font-weight-bold">
        Website
      </a>
      để thanh toán.
    </span>
  `,
  html_topship_balance_note: `
    <span>
      Vui lòng liên hệ Topship thông qua
      <a href="https://www.facebook.com/topship.asia/" target="_blank" class="text-primary font-weight-bold">
        Facebook
      </a>
      hoặc
      <a href="https://www.topship.vn/" target="_blank" class="text-primary font-weight-bold">
        Website
      </a>
      để giải đáp thắc mắc.
    </span>
  `,
  html_main_slogan: `
    <div class="text-bold pb-2" style="font-size: 2.4rem">Hơn 10,000 shop đang sử dụng eTop</div>
    <h4>Miễn phí vĩnh viễn, không giới hạn tính năng!</h4>
  `
};

const TEXT = {
  text_trading_promotion: 'eTop hoàn lại'
};

@Injectable({
  providedIn: 'root'
})
export class WhiteLabelService {

  htmls: any = {};
  texts: any = {};

  etop_default = true;

  constructor(
    private appService: AppService
  ) {
    this.getAppConfig();
  }

  htmlMap(value) {
    return this.etop_default ? HTML[value] : this.htmls[value];
  }

  textMap(value) {
    return this.etop_default ? TEXT[value] : this.texts[value];
  }

  getAppConfig() {
    this.htmls = this.appService.htmls;
    this.texts = this.appService.texts;

    for (let html in this.htmls) {
      if (!this.htmls.hasOwnProperty(html)) {
        continue;
      }
      if (this.htmls[html] != 'etop_default') {
        this.etop_default = false;
        break;
      }
    }
    for (let text in this.texts) {
      if (!this.texts.hasOwnProperty(text)) {
        continue;
      }
      if (this.texts[text] != 'etop_default') {
        this.etop_default = false;
        break;
      }
    }
  }

}
