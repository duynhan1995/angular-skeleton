var cfg = {
  app: {
    appId: 'topship',
    appName: 'Topship',
    disableUser: false,
    redirectUserLink: '/welcome'
  },
  theme: {
    'primary-color': '#e67e22',
    'primary-color-rgb':'230, 126, 34',
    'primary-dark-color': '#7e5701',
    'primary-light-color': '#f8b118',
    'secondary-color': '#33B6D4',
    'default-color': '#999999',
    'light-color': '#888888',
    'dark-color': '#444444',
    'default-boder-color': '#E5E5E5',
    'light-boder-color': '#F5F5F5',
    'dark-boder-color': '#C5C5C5',
    'default-bg-color': '#F5F5F5',
    'dark-bg-color': '#F0F0F0',
    'light-bg-color': '#FEFEFE',
    'danger-color': '#E74C3C',
    'danger-dark-color': '#C92A1A',
    'danger-light-color': '#FDBDB6',
    'warning-color': '#F7941E',
    'warning-dark-color': '#E78005',
    'warning-light-color': '#FFD39D',
    'success-color': '#2ECC71',
    'success-dark-color': '#11AF54',
    'success-light-color': '#ADFFD0',
    color_init: '',
    color_init_dark: '',
    color_init_light: ''
  },
  htmls: {
    html_topship_balance_warning: "etop_default",
    html_topship_balance_note: "etop_default",
    html_main_slogan: "etop_default"
  },
  texts: {
    text_trading_promotion: "etop_default"
  },
  cms_banners: {
    cms_announcement_content: "cms_flexbox_settings",
    cms_transaction_note: "cms_flexbox_settings",
    cms_shipment_policy: "cms_flexbox_settings"
  },
  links: {
    trading_guide: '',
    invitation: ''
  }
};

window.cfg = cfg;

document.dispatchEvent(new CustomEvent('onConfigLoaded', cfg));
