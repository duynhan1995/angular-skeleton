import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trimTooLong'
})
export class TrimTooLongPipe implements PipeTransform {
  transform(value: any, length: number): string {
    let newStr = '';

    if (value.length >= length) {
      newStr = value.slice(0, length);
      newStr += '...';
      return newStr;
    }

    return value;
  }

}
