import { Pipe, PipeTransform } from '@angular/core';
import { WhiteLabelService } from 'apps/topship/src/services/white-label.service';

@Pipe({
  name: 'whiteLabel'
})
export class WhiteLabelPipe implements PipeTransform {

  constructor(
    private whiteLabel: WhiteLabelService
  ) {}

  transform(value: any, type: string): any {
    return this.whiteLabelMapping(value, type);
  }

  private whiteLabelMapping(value: any, type: string) {
    switch (type) {
      case 'html':
        return this.whiteLabel.htmlMap(value);
      case 'text':
        return this.whiteLabel.textMap(value);
      default:
        return this.whiteLabel.htmlMap(value);
    }
  }

}
