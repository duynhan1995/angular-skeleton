import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhiteLabelPipe } from 'apps/topship/src/app/pipes/white-label.pipe';

@NgModule({
  declarations: [
    WhiteLabelPipe
  ],
  exports: [
    WhiteLabelPipe
  ],
  imports: [
    CommonModule
  ]
})
export class ShopPipesModule { }
