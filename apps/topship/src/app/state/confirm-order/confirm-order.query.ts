import { Injectable } from '@angular/core';
import {Query} from '@datorama/akita';
import { ConfirmOrderStore, StateConfirmOrder } from './confirm-order.store';

@Injectable({
  providedIn: 'root'
})
export class ConfirmOrderQuery extends Query<StateConfirmOrder> {

  constructor(protected store: ConfirmOrderStore) {
    super(store);
  }
  
}
