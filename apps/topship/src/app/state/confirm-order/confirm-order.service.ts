import { Injectable } from '@angular/core';
import { Address, Fulfillment } from '@etop/models';
import { AddressService } from 'apps/core/src/services/address.service';
import { Subject } from 'rxjs';
import { ConfirmOrderQuery } from './confirm-order.query';
import { ConfirmOrderStore } from './confirm-order.store';

@Injectable({
  providedIn: 'root'
})
export class ConfirmOrderService {
  readonly forceUpdateShippingService$ = new Subject();
  readonly forceReload$ = new Subject();
  constructor(
    private store: ConfirmOrderStore,
    private query: ConfirmOrderQuery,
    private addressService: AddressService,
  ) {}

  async initData() {
    let _addresses = await this.addressService.getFromAddresses();
    this.store.update({
      fromAddresses: _addresses?.fromAddresses,
      fromAddressIndex: _addresses?.fromAddressIndex
    })
    this.updateFromAddress(_addresses?.fromAddresses[_addresses?.fromAddressIndex])
  }

  updateFulfillment(ffm) {
    const _ffm = this.query.getValue().order;
    this.store.update({
      fulfillment: {
        ..._ffm,
        ...ffm
      }
    })
  }

  updateFromAddress(address: Address) {
    const ffm = this.query.getValue().fulfillment;
    this.store.update({
      fulfillment: {
        ...ffm,
        pickup_address: address
      }
    })
  }

  updateOrder(order) {
    const _order = this.query.getValue().order;
    this.store.update({
      order: {
        ..._order,
        ...order
      }
    })
  }

}
