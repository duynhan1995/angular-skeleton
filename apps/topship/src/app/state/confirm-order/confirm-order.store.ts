import { Injectable } from '@angular/core';
import {Store, StoreConfig} from "@datorama/akita";
import { FulfillmentAPI } from '@etop/api';
import { Address, Customer, Fulfillment, Order, OrderAddress, Shipping } from '@etop/models';

export interface StateConfirmOrder {
  
  order: Order;
  fulfillment: Fulfillment;
  fromAddresses: Address[];
  fromAddressIndex;

}

const initialState = {
  order: new Order({
      lines: [],
      customer: new Customer({}),
      shipping : new Shipping(),
  }),
  resetData: false,
  fulfillment: new Fulfillment({
      pickup_address: new OrderAddress({}),
      shipping_address: new OrderAddress({}),
      chargeable_weight: null,
  }),
  shipmentServicesRequest: new FulfillmentAPI.GetShipmentServicesRequest(),
  pickupAddresses: [],
  shippingAddresses: null,
  defaultPickupAddress: null,
  products: null,
  fromAddresses: [],
  fromAddressIndex: null
};

@Injectable({
  providedIn: "root"
})
@StoreConfig({name: 'confirm-order'})
export class ConfirmOrderStore extends Store<StateConfirmOrder> {
  constructor() {
    super(initialState);
  }
}
