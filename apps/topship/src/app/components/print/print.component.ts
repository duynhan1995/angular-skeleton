import { Component, OnInit } from '@angular/core';
import { PrintControllerService } from './print.controller';

@Component({
  selector: 'topship-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.scss']
})
export class PrintComponent implements OnInit {

  constructor(
    private printController: PrintControllerService
  ) { }

  ngOnInit() {
    this.printController.registerPrint(this);
  }

  print() {
    setTimeout(() => {
      window.print();
    }, 500);
  }

}
