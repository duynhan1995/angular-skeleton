import { Injectable } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Order } from '@etop/models';
import { PrintComponent } from './print.component';

export enum PrintType {
    orders = 'orders',
    fulfillments = 'fulfillments',
    o_and_f = 'o_and_f'
  }

@Injectable()
export class PrintControllerService {
  print_type: PrintType;
  printing_ffms: Fulfillment[] = [];
  printing_orders: Order[] = [];

  private printComponent: any;

  constructor() { }

  print(type: PrintType, data: any) {
    this.print_type = type;
    if(type == PrintType.fulfillments) {
        this.printing_ffms = data.ffms;
        this.printing_orders = data.orders;
    }
    if(type == PrintType.orders) {
        this.printing_orders = data.orders;
    }
    if(type == PrintType.o_and_f) {
      this.printing_ffms = data.ffms;
      this.printing_orders = data.orders;
    }
    this.printComponent.print();
  } 

  registerPrint(instance: PrintComponent) {
    this.printComponent = instance;
  }
}
