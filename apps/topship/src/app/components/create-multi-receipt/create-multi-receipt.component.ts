import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Ledger } from '@etop/models';
import { ReceiptService } from 'apps/topship/src/services/receipt.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { ChangeLedgerModalComponent } from 'apps/topship/src/app/components/modals/change-ledger-modal/change-ledger-modal.component';
import { OrderService } from 'apps/topship/src/services/order.service';
import { PurchaseOrderStoreService } from 'apps/core/src/stores/purchase-order.store.service';
import { BaseComponent } from '@etop/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'topship-create-multi-receipt',
  templateUrl: './create-multi-receipt.component.html',
  styleUrls: ['./create-multi-receipt.component.scss']
})
export class CreateMultiReceiptComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() totalAmountText: string;
  @Input() totalPaymentText: string;
  @Input() total_amount = 0;
  receipts: any[] = [];
  cash_ledger = new Ledger({});

  private modal: any;

  constructor(
    private receiptService: ReceiptService,
    private modalController: ModalController,
    private receiptStore: ReceiptStoreService,
    private orderService: OrderService,
    private purchaseOrderStore: PurchaseOrderStoreService
  ) {
    super();
  }

  get onlyCash() {
    return this.receipts.length == 1 && this.receipts[0].ledger_type == 'cash';
  }

  async ngOnInit() {
    await this.prepareData();

    this.orderService.createOrderSuccess$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.resetReceiptData();
      });
    this.purchaseOrderStore.recentlyCreated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(recentlyCreated => {
        if (recentlyCreated) {
          this.resetReceiptData();
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.receipts[0].amount = changes.total_amount.currentValue;
  }

  resetReceiptData() {
    this.receipts = [{
      ledger_name: this.cash_ledger && this.cash_ledger.name || '',
      ledger_id: this.cash_ledger && this.cash_ledger.id || '',
      amount: null,
      ledger_type: 'cash'
    }];
    this.receiptStore.updateReceipts(this.receipts);
  }

  async prepareData() {
    const _ledgers = await this.receiptService.getLedgers();
    this.receiptStore.updateLedgers(_ledgers);
    this.cash_ledger = this.receiptStore.snapshot.ledgers.find(l => l.type == 'cash');
    this.receipts.push({
      ledger_name: this.cash_ledger && this.cash_ledger.name || '',
      ledger_id: this.cash_ledger && this.cash_ledger.id || '',
      amount: null,
      ledger_type: 'cash'
    });
    this.receiptStore.updateReceipts(this.receipts);
  }

  changeMethod() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: ChangeLedgerModalComponent,
      showBackdrop: true,
      cssClass: 'modal-lg',
      componentProps: {
        total_amount: this.total_amount,
        totalAmountText: this.totalAmountText,
        totalPaymentText: this.totalPaymentText,
        receipts: [...this.receipts]
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(receipts => {
      this.modal = null;
      if (receipts && receipts.length) {
        this.receipts = receipts;
        this.receiptStore.updateReceipts(receipts);
      }
      this.receiptStore.updateSelectedLedgerIDs([]);
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
      this.receiptStore.updateSelectedLedgerIDs([]);
    })
  }

}
