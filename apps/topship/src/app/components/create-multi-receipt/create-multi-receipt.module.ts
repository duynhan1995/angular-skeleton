import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateMultiReceiptComponent } from 'apps/topship/src/app/components/create-multi-receipt/create-multi-receipt.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { ChangeLedgerModalModule } from 'apps/topship/src/app/components/modals/change-ledger-modal/change-ledger-modal.module';
import { AuthenticateModule } from '@etop/core';
import { EtopFormsModule } from '@etop/shared/components/etop-forms/etop-forms.module';
import { EtopPipesModule } from '@etop/shared';

@NgModule({
  declarations: [
    CreateMultiReceiptComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ChangeLedgerModalModule,
    AuthenticateModule,
    EtopFormsModule,
    EtopPipesModule,
  ],
  exports: [
    CreateMultiReceiptComponent
  ],
  providers: [
  ]
})
export class CreateMultiReceiptModule { }
