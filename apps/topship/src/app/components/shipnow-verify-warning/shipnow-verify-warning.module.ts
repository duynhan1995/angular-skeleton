import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShipnowVerifyWarningComponent} from "apps/topship/src/app/components/shipnow-verify-warning/shipnow-verify-warning.component";

@NgModule({
  declarations: [
    ShipnowVerifyWarningComponent
  ],
  exports: [
    ShipnowVerifyWarningComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ShipnowVerifyWarningModule { }
