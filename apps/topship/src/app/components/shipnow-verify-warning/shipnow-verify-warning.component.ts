import {Component, Input, OnInit} from '@angular/core';
import {Connection} from "@etop/models";

@Component({
  selector: 'topship-shipnow-verify-warning',
  templateUrl: './shipnow-verify-warning.component.html',
  styleUrls: ['./shipnow-verify-warning.component.scss']
})
export class ShipnowVerifyWarningComponent implements OnInit {
  @Input() connection: Connection;

  constructor() { }

  ngOnInit() {
  }

}
