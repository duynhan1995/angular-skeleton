import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'topship-cancel-object',
  templateUrl: './cancel-object.component.html',
  styleUrls: ['./cancel-object.component.scss']
})
export class CancelObjectComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() reasons: string[];
  @Input() cancelFn: (string) => Promise<any>;
  @Input() multiple = false;

  loading = false;
  active_reason: string;
  other_reason: string;
  active_idx: number;

  constructor(
    private modalAction: ModalAction
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalAction.close(false);
  }

  chooseReason(r?, i?) {
    if (r) {
      this.active_reason = r;
      this.active_idx = i + 1;
    } else {
      this.active_idx = 0;
    }
  }

  async cancel() {
    this.loading = true;
    try {
      if (!this.active_idx) {
        this.active_reason = this.other_reason;
      }
      if (!this.active_reason) {
        this.loading = false;
        return toastr.error('Vui lòng nhập lý do huỷ!');
      }
      await this.cancelFn(this.active_reason);
      this.modalAction.dismiss(null);
    } catch(e) {
      debug.error('ERROR in cancel object', e);
    }
    this.loading = false;
  }

}
