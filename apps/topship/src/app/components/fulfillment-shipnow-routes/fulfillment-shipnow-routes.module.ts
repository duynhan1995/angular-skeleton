import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FulfillmentShipnowRoutesComponent } from 'apps/topship/src/app/components/fulfillment-shipnow-routes/fulfillment-shipnow-routes.component';
import { EtopPipesModule } from 'libs/shared/pipes/etop-pipes.module';

@NgModule({
  declarations: [
    FulfillmentShipnowRoutesComponent
  ],
  exports: [
    FulfillmentShipnowRoutesComponent
  ],
  imports: [
    CommonModule,
    EtopPipesModule
  ]
})
export class FulfillmentShipnowRoutesModule { }
