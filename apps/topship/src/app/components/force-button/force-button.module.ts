import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForceButtonComponent } from './force-button.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ForceButtonComponent],
  exports: [ForceButtonComponent],
  providers: []
})
export class ForceButtonModule {}