import { Component, OnInit, Input } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { NotiModalComponent } from 'apps/topship/src/app/components/modals/noti-modal/noti-modal.component';
import { ForceModalComponent } from 'apps/topship/src/app/components/modals/force-modal/force-modal.component';
import * as moment from 'moment';

@Component({
  selector: 'topship-force-button',
  templateUrl: './force-button.component.html',
  styleUrls: ['./force-button.component.scss']
})
export class ForceButtonComponent implements OnInit {
  @Input() classSize;
  @Input() fulfillment: Fulfillment;
  @Input() shop;

  title = '';
  loading = false;
  reason: string;
  holiday = false;
  reason_holiday = '';

  constructor(
    public util: UtilService,
    private modalController: ModalController
  ) {}

  async ngOnInit() {
    this.getHoliday();
  }

  showModalNoti(title, content) {
    let modal = this.modalController.create({
      component: NotiModalComponent,
      showBackdrop: true,
      componentProps: {
        title: title,
        content: content
      }
    });
    modal.show().then();
    modal.onDismiss().then();
  }

  showPopup() {
    const now = new Date();
    const day = now.getDay();
    const hours = now.getHours();
    if (this.holiday) {
      let content = `Để đảm bảo vận hành, TOPSHIP không nhận yêu cầu hỗ trợ trong thời gian này vì lý do ${
        this.reason_holiday
      }. Xin lỗi
        vì sự bất tiện này.`;
      this.showModalNoti('Gửi yêu cầu hỗ trợ', content);
      return;
    }
    if (day === 0 || (day === 6 && hours >= 17)) {
      let content = `Để đảm bảo vận hành, TOPSHIP không nhận yêu cầu hỗ trợ từ 5h chiều thứ 7 đến hết ngày chủ nhật. Xin lỗi vì sự
        bất tiện này.`;
      this.showModalNoti('Gửi yêu cầu hỗ trợ', content);
      return;
    }
    let modal = this.modalController.create({
      component: ForceModalComponent,
      showBackdrop: true,
      componentProps: {
        fulfillment: this.fulfillment
      }
    });
    modal.show().then();
    modal.onDismiss().then();

  }

  async getHoliday() {
    try {
      const res = await fetch('https://cms-setting.etop.vn/api/get-banner').then(r =>
        r.json()
      );
      const now = moment(new Date()).format('DD/MM/YYYY');
      if (res.data) {
        let banner = res.data[11];
        let description = this.removeNewLineCharacters(
          this.stripHTML(banner.description)
        ).split('###');
        description.forEach(item => {
          item = item.split(',');
          for (let i = 0; i < item.length; i++) {
            if (item[i] == now) {
              this.holiday = true;
              this.reason_holiday = item[item.length - 1];
            }
          }
        });
      } else {
        return '';
      }
    } catch (e) {
      debug.error(e);
    }
  }

  private stripHTML(html: string) {
    const _tempDIV = document.createElement('div');
    _tempDIV.innerHTML = html;
    return _tempDIV.textContent || _tempDIV.innerText || '';
  }

  private removeNewLineCharacters(str) {
    return str.trim().replace(/\r?\n|\r/g, '');
  }
}
