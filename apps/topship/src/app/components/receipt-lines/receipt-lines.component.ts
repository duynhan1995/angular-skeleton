import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { AddLedgerModalComponent } from 'apps/topship/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import { UtilService } from 'apps/core/src/services/util.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ReceiptStoreService } from 'apps/core/src/stores/receipt.store.service';
import { AuthenticateStore } from '@etop/core';

@Component({
  selector: 'topship-receipt-lines',
  templateUrl: './receipt-lines.component.html',
  styleUrls: ['./receipt-lines.component.scss']
})
export class ReceiptLinesComponent implements OnInit {
  @ViewChild('moneyInput', { static: false }) moneyInput: MaterialInputComponent;
  @Input() receipt: any;
  @Input() removable = false;

  @Output() lineRemoved = new EventEmitter();
  @Output() ledgerSelected = new EventEmitter();
  @Output() amountChanged = new EventEmitter();
  @Output() addLedgerOpened = new EventEmitter<boolean>();

  ledgers: any[] = [];
  ledgerIndex: number;
  showAddLedger = true;

  format_money = true;

  private modal: any;

  constructor(
    private util: UtilService,
    private modalController: ModalController,
    private changeDetector: ChangeDetectorRef,
    private receiptStore: ReceiptStoreService,
    private authStore: AuthenticateStore
  ) { }

  ngOnInit() {
    this.showAddLedger = this.authStore.snapshot.permission.permissions.includes('shop/ledger:create');
    this.format_money = !!this.receipt.amount;
    this.mappingLedgers();
    this.ledgerIndex = this.receiptStore.snapshot.ledgers.findIndex(
      l => l.id == this.receipt.ledger_id
    );
    if (this.ledgerIndex == -1) {
      this.ledgerIndex = null;
    }
    this.onSelectLedger();
  }

  mappingLedgers() {
    this.ledgers = this.receiptStore.snapshot.ledgers.map((l, index) => {
      const bank = l.bank_account;
      const fullname = l.type == 'cash' && 'Tiền mặt' ||
        `Chuyển khoản - ${l.name} - ${bank.account_number}`;
      const name = l.type == 'cash' && 'Tiền mặt' || l.name;
      const search_text = l.type == 'cash' && 'Tiền mặt' ||
        this.util.makeSearchText(`${l.name}${bank.account_number}${bank.account_name}${bank.name}`);
      return {
        index,
        id: l.id,
        name,
        fullname,
        info: `<strong>${fullname}</strong>`,
        detail: `<strong>${fullname}</strong>`,
        search_text: search_text,
        type: l.type
      };
    });
  }

  onSelectLedger() {
    const ledger = this.ledgers[this.ledgerIndex];
    if (!ledger) { return; }
    this.receipt = {
      ...this.receipt,
      ledger_name: ledger.name,
      ledger_fullname: ledger.fullname,
      ledger_id: ledger.id,
      ledger_type: ledger.type
    };
    this.receipt.duplicated_ledger = false;
    if (this.receiptStore.snapshot.selected_ledger_ids.length &&
      this.receiptStore.snapshot.selected_ledger_ids.indexOf(this.receipt.ledger_id) != -1
    ) {
      this.receipt.duplicated_ledger = true;
      toastr.error('Phương thức thanh toán đã được chọn');
    }
    this.ledgerSelected.emit(this.receipt);
  }

  focusLedger() {
    if (this.ledgers.length) {
      return;
    }
    this.addLedger();
  }


  addLedger() {
    if (this.modal) { return; }
    this.addLedgerOpened.emit(false);
    this.modal = this.modalController.create({
      component: AddLedgerModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show().then();
    this.modal.onDismiss().then(ledger => {
      this.modal = null;
      this.addLedgerOpened.emit(true);
      if (ledger) {
        this.receiptStore.snapshot.ledgers.push(ledger);
        this.mappingLedgers();
        this.ledgerIndex = this.receiptStore.snapshot.ledgers.length - 1;
        this.onSelectLedger();
      }
    });
  }

  changeReceiptAmount() {
    if (!Number(this.receipt.amount)) {
      this.receipt.error = true;
      toastr.error('Số tiền nhập vào phải lớn hơn 0đ!');
    } else {
      this.receipt.error = false;
    }
    this.amountChanged.emit(this.receipt)
  }

  removeReceiptLine() {
    this.lineRemoved.emit();
  }

  formatMoney() {
    if (!this.receipt.amount) {
      this.format_money = false;
      return;
    }
    this.format_money = !this.format_money;
    this.changeDetector.detectChanges();
    if (!this.format_money && this.moneyInput) {
      this.moneyInput.focusInput();
    }
  }

}
