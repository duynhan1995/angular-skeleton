import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {AccountApi, ShopAccountAPI, ShopAccountApi} from "@etop/api";
import {AuthenticateStore, BaseComponent} from "@etop/core";
import {Address, DEFAULT_RRULE, ExtendedAccount} from "@etop/models";
import {LocationQuery} from "@etop/state/location";
import {StringHandler} from "@etop/utils";
import {AddressService} from "apps/core/src/services/address.service";
import {CmsService} from "apps/core/src/services/cms.service";
import {UserService} from "apps/core/src/services/user.service";
import {UtilService} from "apps/core/src/services/util.service";
import {CommonUsecase} from "apps/shared/src/usecases/common.usecase.service";
import {MoneyTransactionService} from "apps/topship/src/services/money-transaction.service";
import {ShopCrmService} from "apps/topship/src/services/shop-crm.service";
import {ShopService} from "@etop/features";
import {TelegramService} from "@etop/features";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";

@Component({
  selector: 'topship-create-shop-form',
  templateUrl: './create-shop-form.component.html',
  styleUrls: ['./create-shop-form.component.scss']
})
export class CreateShopFormComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() redirect = true;
  @Input() shop: ExtendedAccount;
  @Output() shippingPolicyAccepted = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private auth: AuthenticateStore,
    private util: UtilService,
    private locationQuery: LocationQuery,
    private cms: CmsService,
    private telegramService: TelegramService,
    private shopService: ShopService,
    private userService: UserService,
    private addressesService: AddressService,
    private shopAccountApi: ShopAccountApi,
    private etopAccountApi: AccountApi,
    private shopCrm: ShopCrmService,
    private commonUsecase: CommonUsecase,
    private router: Router,
  ) {
    super();
  }

  uploading = false;

  openTimeForm = this.fb.group({
    startOpenTime: '',
    endOpenTime: '',
    morning: false,
    afternoon: false,
    evening: false,
    customOpenTime: false,
  });

  lunchBreakForm = this.fb.group({
    startLunchBreak: '12:00',
    endLunchBreak: '13:30',
    lunchBreak: false,
  });

  callOption = this.fb.control('no-call');

  policyAccept = this.fb.control(false);
  getShippingPolicyLink = '';

  shopForm = this.fb.group({
    image_url: '',
    name: '',
    website_url: ''
  });

  addressForm = this.fb.group({
    full_name: '',
    phone: '',
    email: '',
    province_code: '',
    district_code: '',
    ward_code: '',
    address1: '',
    notes: {
      other: 'no-call',
      open_time: '',
      lunch_break: ''
    }
  });

  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.addressForm.controls['province_code'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) {
        return [];
      }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.addressForm.controls['district_code'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) {
        return [];
      }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  private static validateAddress(address: Address, lunchBreak: boolean) {
    if (!address.full_name) {
      toastr.error('Vui lòng nhập tên người phụ trách!');
      return false;
    }

    let phone = (address.phone && address.phone.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    phone = (phone && phone.split('-test')[0]) || '';
    if (!StringHandler.validatePhoneNumber(phone)) {
      toastr.error(`Vui lòng nhập số điện thoại hợp lệ!`);
      return false;
    }

    let email = (address.email && address.email.split(/-[0-9a-zA-Z]+-test/)[0]) || '';
    email = (email && email.split('-test')[0]) || '';
    if (address.email && !StringHandler.validateEmail(email)) {
      toastr.error('Vui lòng nhập email hợp lệ!');
      return false;
    }

    if (!address.province_code) {
      toastr.error(`Vui lòng chọn tỉnh thành!`);
      return false;
    }
    if (!address.district_code) {
      toastr.error(`Vui lòng chọn quận huyện!`);
      return false;
    }
    if (!address.ward_code) {
      toastr.error(`Vui lòng chọn phường xã!`);
      return false;
    }
    if (!address.address1) {
      toastr.error(`Vui lòng nhập địa chỉ cụ thể!`);
      return false;
    }
    if (!address.notes.open_time) {
      toastr.error('Vui lòng nhập khung giờ lấy hàng!');
      return false;
    }
    if (lunchBreak && !address.notes.lunch_break) {
      toastr.error('Vui lòng nhập giờ nghỉ trưa!');
      return false;
    }
    return true;
  }

  private static validateShop(shop: ExtendedAccount) {
    if (!shop.name?.trim()) {
      toastr.error(`Vui lòng điền tên cửa hàng!`);
      return false;
    }

    if (shop.website_url && !StringHandler.validateWebsiteUrl(shop.website_url)) {
      toastr.error(`Vui lòng điền địa chỉ website hợp lệ!`);
      return false;
    }

    return true;
  }

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  ngOnInit() {
    this.getShippingPolicy();

    combineLatest([
      this.openTimeForm.controls.morning.valueChanges,
      this.openTimeForm.controls.afternoon.valueChanges,
      this.openTimeForm.controls.evening.valueChanges
    ]).pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        let startOpenTime = '';
        let endOpenTime = '';

        const {morning, afternoon, evening} = this.openTimeForm.getRawValue();

        if (morning) {
          startOpenTime = '08:00';
          endOpenTime = '12:00';
        }

        if (afternoon) {
          endOpenTime = '18:00';
          if (!morning) {
            startOpenTime = '12:00';
          }
        }
        if (evening) {
          endOpenTime = '21:00';
          if (!morning) {
            startOpenTime = '12:00';
          }
          if (!afternoon) {
            startOpenTime = '18:00';
          }
        }

        this.openTimeForm.patchValue({
          customOpenTime: false
        }, {emitEvent: false});

        this.openTimeForm.patchValue({
          startOpenTime,
          endOpenTime,
        });
      });

    this.openTimeForm.controls.customOpenTime.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value) {
          this.openTimeForm.patchValue({
            afternoon: false,
            evening: false,
            morning: false
          }, {emitEvent: false});
        }
      });

    this.lunchBreakForm.controls.lunchBreak.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!value) {
          this.lunchBreakForm.patchValue({
            startLunchBreak: '',
            endLunchBreak: '',
          });
        }
      });

    combineLatest([
      this.openTimeForm.controls.startOpenTime.valueChanges,
      this.openTimeForm.controls.endOpenTime.valueChanges,
      this.lunchBreakForm.controls.startLunchBreak.valueChanges,
      this.lunchBreakForm.controls.endLunchBreak.valueChanges
    ]).pipe(
      map(([startOpenTime, endOpenTime, startLunchBreak, endLunchBreak]) => {
        return {startOpenTime, endOpenTime, startLunchBreak, endLunchBreak};
      }),
      takeUntil(this.destroy$)
    ).subscribe(({startOpenTime, endOpenTime, startLunchBreak, endLunchBreak}) => {
      const notes = this.addressForm.controls.notes.value;
      this.addressForm.patchValue({
        notes: {
          ...notes,
          open_time: (startOpenTime && endOpenTime) ? (startOpenTime + ' - ' + endOpenTime) : '',
          lunch_break: (startLunchBreak && endLunchBreak) ? (startLunchBreak + ' - ' + endLunchBreak) : ''
        }
      });
    });

    this.callOption.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        const notes = this.addressForm.controls.notes.value;
        this.addressForm.patchValue({
          notes: {
            ...notes,
            other: value
          }
        });
      });

    this.policyAccept.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.shippingPolicyAccepted.emit(value);
      });

    this.openTimeForm.patchValue({
      startOpenTime: '',
      endOpenTime: '',
      morning: false,
      afternoon: false,
      evening: false,
    });

    this.lunchBreakForm.patchValue({
      startLunchBreak: '12:00',
      endLunchBreak: '13:30',
    });
  }

  ngOnChanges(changes: SimpleChanges) {}

  async onFileSelected(event) {
    this.uploading = true;
    try {
      const {files} = event.target;
      const res = await this.util.uploadImages([files[0]], 1024);
      this.shopForm.patchValue({
        image_url: res[0].url
      });
    } catch (e) {
      debug.error('ERROR in upload Shop Avatar', e);
      toastr.error('Upload ảnh không thành công.', e.code && (e.message || e.msg));
    }
    this.uploading = false;
  }

  async confirm() {
    try {
      const shopRawData = this.shopForm.getRawValue();
      const validShop = CreateShopFormComponent.validateShop(shopRawData);
      if (!validShop) {
        return false;
      }

      const addressRawData = this.addressForm.getRawValue();
      const lunchBreak = this.lunchBreakForm.getRawValue().lunchBreak;
      const validAddress = CreateShopFormComponent.validateAddress(addressRawData, lunchBreak);
      if (!validAddress) {
        return false;
      }

      const registerShopRequest: ShopAccountAPI.RegisterShopRequest = {
        ...shopRawData,
        address: addressRawData,
        email: addressRawData.email,
        phone: addressRawData.phone,
        money_transaction_rrule: DEFAULT_RRULE,
        survey_info: this.shop?.survey_info || JSON.parse(localStorage.getItem('survey'))
      };

      const res = await this.userService.registerShop(registerShopRequest);

      const account = await this.userService.switchAccount(res.shop.id);

      const url_slug = this.util.createHandle(res.shop.name) + '-' + res.shop.code.toLowerCase();
      await this.etopAccountApi.updateURLSlug({
        account_id: res.shop.id,
        url_slug: url_slug
      }, account.access_token);

      const address = await this.createAddress(addressRawData, account.access_token);

      if (this.redirect) {
        this.auth.updateToken(account.access_token);
        await this.commonUsecase.updateSessionInfo(true);
        const slug = this.auth.snapshot.account.url_slug || this.auth.currentAccountIndex();
        this.router.navigateByUrl(`/s/${slug}/dashboard`).then();
      } else {
        this.auth.addAccount({
          ...account,
          url_slug,
          display_name: `${account.shop.code} - ${account.shop.name}`,
          image_url: account.shop.image_url
        });
      }

      toastr.success('Tạo cửa hàng thành công.');

      this.vTigerUpdate({
        user: this.auth.snapshot.user,
        shop: this.auth.snapshot.shop,
        address
      }).then();

      this.telegramService.newShopMessage(
        this.auth.snapshot.user,
        this.auth.snapshot.shop,
        this.addressesService.joinAddress(this.auth.snapshot.shop.address),
        JSON.parse(localStorage.getItem('survey'))
      ).then();

      return true;

    } catch (e) {
      debug.error('ERROR in Creating Shop', e);
      toastr.error('Tạo cửa hàng không thành công.', e.code && (e.message || e.msg));

      return false;
    }
  }

  async createAddress(addressData, token?: string) {
    try {
      const address = await this.addressesService.createAddress({...addressData, type: 'shipfrom'}, token);
      if (address) {
        await this.shopService.setDefaultAddress(
          address.id,
          'shipfrom',
          token
        );
      }
      return address;
    } catch (e) {
      debug.error('ERROR in Creating Address', e);
      return null;
    }
  }

  getShippingPolicy() {
    if (this.cms.bannersLoaded) {
      this.getShippingPolicyLink = this.cms.getShippingPolicy();
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        this.getShippingPolicyLink = this.cms.getShippingPolicy();
      });
    }
  }

  private async vTigerUpdate(obj: any) {
    let {user, shop, address} = obj;
    let isTest = user.email.split(/-[0-9a-zA-Z]+-test$/).length > 1;
    let survey = JSON.parse(localStorage.getItem('survey'));

    let body = {
      company: shop.name,
      website: shop.website_url,
      secondaryemail: shop.email,
      mobile: shop.phone,
      lane: `${address.address1} - ${address.ward}`,
      city: address.district,
      state: address.province,
      country: 'Vietnam',
      email: user.email,
      firstname: isTest ? 'TEST' : '',
      lastname: user.full_name,
      phone: user.phone,
      description: survey ? `${survey.orders_per_day_text} \n` : '',
      leadsource: user.source,
      etop_id: user.id,
      assistant_name: address.full_name,
      personal_merchant: survey ? survey.reAnswer : '',
      orders_per_day: survey ? survey.orders_per_day_text : ''
    };

    this.shopCrm.rawUpdateLeadAndContact(body);
  }

}
