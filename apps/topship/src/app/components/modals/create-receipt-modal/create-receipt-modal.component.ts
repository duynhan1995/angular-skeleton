import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Receipt, Ledger } from 'libs/models/Receipt';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { ReceiptService } from 'apps/topship/src/services/receipt.service';
import { MaterialInputAutocompleteComponent } from 'libs/shared/components/etop-material/material-input-autocomplete/material-input-autocomplete.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AddLedgerModalComponent } from 'apps/topship/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ReceiptApi } from '@etop/api';

@Component({
  selector: 'topship-create-receipt-modal',
  templateUrl: './create-receipt-modal.component.html',
  styleUrls: ['./create-receipt-modal.component.scss']
})
export class CreateReceiptModalComponent implements OnInit {
  @ViewChild('moneyInput', { static: false }) moneyInput: MaterialInputComponent;
  @ViewChild('ledgerInput', { static: false }) ledgerInput: MaterialInputAutocompleteComponent;

  @Output() createReceipt = new EventEmitter();

  @Input() trader_id;
  @Input() type: 'receipt' | 'payment';
  @Input() amount; // max amount of this new receipt
  @Input() ref;

  new_receipt = new Receipt({});

  ledgerTypes = [
    { name: 'Tiền mặt', value: 'cash' },
    { name: 'Chuyển khoản', value: 'bank' },
  ];

  initLedgers: Ledger[] = [];
  ledgers: any[] = [];

  receipt_type_display = '';
  ledger_type: string;

  format_money = true;
  receipt_amount_error = false;

  loading = false;
  createAndConfirm = false;
  isSuccessfully = false;

  ledgerIndex: number;

  private modal: any;

  constructor(
    private receiptService: ReceiptService,
    private modalController: ModalController,
    private dialog: DialogControllerService,
    private receiptApi: ReceiptApi,
    private modalAction: ModalAction,
    private util: UtilService,
    private changeDetector: ChangeDetectorRef
  ) { }

  get refDisplay() {
    const ref_type = this.new_receipt.p_data.ref_type;
    if (ref_type == 'order') {
      return 'Đơn hàng';
    }
    if (ref_type == 'purchase_order') {
      return 'Đơn nhập hàng';
    }
    if (ref_type == 'fulfillment') {
      return 'Đơn giao hàng';
    }
  }

  async ngOnInit() {
    this.initLedgers = await this.receiptService.getLedgers();
    this.ledger_type = 'cash';
    this.displayLedgers();

    if (this.type == 'receipt') {
      this.new_receipt.p_data.title = 'Thanh toán đơn hàng';
      this.new_receipt.p_data.type = 'receipt';
      this.new_receipt.p_data.ref_type = 'order';
      this.receipt_type_display = 'Thu';
    }
    if (this.type == 'payment') {
      this.new_receipt.p_data.title = 'Thanh toán đơn nhập hàng';
      this.new_receipt.p_data.type = 'payment';
      this.new_receipt.p_data.ref_type = 'purchase_order';
      this.receipt_type_display = 'Chi';
    }
    this.new_receipt.p_data.paid_at = new Date();
    this.new_receipt.p_data.trader_id = this.trader_id;

    this.format_money = this.amount >= 0;
    this.new_receipt.p_data.amount = this.amount;
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  closeModal() {
    this.modalAction.close(false);
  }

  private _preProcessingReceiptLines() {
    if (!this.checkValidAmount()) { return false; }

    this.new_receipt.p_data.lines = [];
    const _line = {
      title: `Thanh toán ${this.refDisplay.toLowerCase()}`,
      amount: this.new_receipt.p_data.amount,
      ref_id: this.ref.id
    };
    this.new_receipt.p_data.lines.push(_line);

    return true;
  }

  private _validateReceiptInfo() {
    if (!this.new_receipt.p_data.type) {
      toastr.error('Vui lòng chọn loại phiếu');
      return false;
    }
    if (!this.new_receipt.p_data.paid_at) {
      toastr.error('Vui lòng nhập ngày thanh toán');
      return false;
    }
    if (!this.new_receipt.p_data.trader_id) {
      toastr.error('Vui lòng chọn đối tác');
      return false;
    }
    if (!this.new_receipt.p_data.amount) {
      toastr.error('Vui lòng nhập giá trị cho phiếu');
      return false;
    }
    if (!this.new_receipt.p_data.ledger_id) {
      toastr.error('Vui lòng chọn phương thức thanh toán');
      return false;
    }
    if (!this.new_receipt.p_data.title) {
      toastr.error('Vui lòng nhập nội dung cho phiếu');
      return false;
    }
    return true;
  }

  async create(createAndConfirm: boolean) {
    this.loading = true;
    this.createAndConfirm = createAndConfirm;
    try {
      if (!this._preProcessingReceiptLines()) {
        this.loading = false;
        this.createAndConfirm = false;
        return;
      }
      if (!this._validateReceiptInfo()) {
        this.loading = false;
        this.createAndConfirm = false;
        return;
      }
      const body: Receipt = {
        ...this.new_receipt.p_data,
      };
      const res = await this.receiptService.createReceipt(body);
      toastr.success(`Tạo phiếu ${this.receipt_type_display} thành công.`);
      this.isSuccessfully = true;

      if (createAndConfirm) {
        await this.confirm(res);
      }
      this.createReceipt.emit();
      this.dismissModal();
    } catch (e) {
      toastr.error(e.message || e.msg, `Tạo phiếu ${this.receipt_type_display} không thành công.`);
    }
    this.loading = false;
    this.createAndConfirm = false;
  }

  async confirm(receipt: Receipt) {
    try {
      await this.receiptApi.confirmReceipt(receipt.id);
      toastr.success(`Xác nhận phiếu ${this.receipt_type_display} thành công.`);
    } catch (e) {
      debug.error('ERROR in confirming Receipt', e);
      toastr.error(`Xác nhận phiếu ${this.receipt_type_display} không thành công.`, e.message || e.msg);
    }
  }

  displayLedgers() {
    if (this.ledger_type == 'cash') {
      const _ledger = this.initLedgers.find(l => l.type == 'cash');
      this.new_receipt.p_data.ledger_id = _ledger && _ledger.id || null;
    } else {
      const ledgersBank = this.initLedgers.filter(l => l.type != 'cash');
      this.ledgers = this.ledgerMapping(ledgersBank);
      this.ledgerIndex = 0;
      const ledger = this.ledgers[this.ledgerIndex];
      this.new_receipt.p_data.ledger_id = ledger && ledger.id || null;
    }
  }

  ledgerMapping(ledgers: Array<Ledger>): Array<any> {
    return ledgers.map((l, index) => {
      const bank = l.bank_account;
      return {
        index,
        id: l.id,
        info: `<strong>${l.name}</strong>
                <span>
                ${bank.account_number} - ${bank.account_name} - ${bank.name}
                </span>`,
        detail: `<strong>${l.name}</strong>
                <span>
                ${bank.account_number} - ${bank.account_name} - ${bank.name}
                </span>`,
        search_text: this.util.makeSearchText(
          `${l.name}${bank.account_number}${bank.account_name}${bank.name}`
        )
      };
    });
  }

  formatMoney() {
    if (!this.new_receipt.p_data.amount && this.new_receipt.p_data.amount != 0) {
      this.format_money = false;
      return;
    }
    this.format_money = !this.format_money;
    this.changeDetector.detectChanges();
    if (!this.format_money && this.moneyInput) {
      this.moneyInput.focusInput();
    }
  }

  onSelectLedger() {
    const ledger = this.ledgers[this.ledgerIndex];
    this.new_receipt.p_data.ledger_id = ledger && ledger.id || null;
  }

  focusLedger() {
    if (this.ledgers.length) {
      return;
    }
    this.addLedger();
  }

  checkValidAmount() {
    this.receipt_amount_error = false;
    if (this.new_receipt.p_data.amount > this.amount) {
      this.receipt_amount_error = true;
      toastr.error(`Số tiền thu ở đơn hàng #${this.ref.id} không được lớn hơn Số tiền còn phải thu ở đơn này.`);
      return false;
    }
    return true;
  }

  addLedger() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddLedgerModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show().then();
    this.modal.onDismiss().then(ledger => {
      this.modal = null;
      if (ledger) {
        this.initLedgers.push(ledger);
        this.new_receipt.p_data.ledger_id = ledger.id;

        const _ledgers = this.initLedgers.filter(l => l.type != 'cash');
        this.ledgers = this.ledgerMapping(_ledgers);
        this.ledgerIndex = this.ledgers.findIndex(l => l.id == ledger.id);
      }
    });
  }

}
