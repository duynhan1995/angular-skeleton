import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import { Ledger } from 'libs/models/Receipt';
import { BankAccount } from 'libs/models/Bank';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { ReceiptApi } from '@etop/api';
import {map} from 'rxjs/operators';
import { BaseComponent } from '@etop/core';
import {BankQuery} from "@etop/state/bank";
import {combineLatest} from "rxjs";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'topship-add-ledger-modal',
  templateUrl: './add-ledger-modal.component.html',
  styleUrls: ['./add-ledger-modal.component.scss']
})
export class AddLedgerModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() ledger = new Ledger({
    bank_account: new BankAccount()
  });

  loading = false;

  bankReady$ = this.bankQuery.select(state => !!state.bankReady);

  ledgerForm = this.fb.group({
    bankCode: '',
    bankProvinceCode: '',
    bankBranchCode: '',
    accountName: '',
    accountNumber: '',
    name: '',
    note: ''
  });

  formInitializing = true;

  banksList$ = this.bankQuery.select("banksList");
  bankProvincesList$ = combineLatest([
    this.bankQuery.select("bankProvincesList"),
    this.ledgerForm.controls['bankCode'].valueChanges]).pipe(
    map(([bankProvinces, bankCode]) => {
      if (!bankCode) { return []; }
      return bankProvinces?.filter(prov => prov.bank_code == bankCode);
    })
  );
  bankBranchesList$ = combineLatest([
    this.bankQuery.select("bankBranchesList"),
    this.ledgerForm.controls['bankCode'].valueChanges,
    this.ledgerForm.controls['bankProvinceCode'].valueChanges]).pipe(
    map(([bankBranches, bankCode, bankProvinceCode]) => {
      if (!bankProvinceCode) { return []; }
      return bankBranches?.filter(branch => branch.bank_code == bankCode && branch.province_code == bankProvinceCode);
    })
  );

  private static validateLedgerData(data) {
    const { name, bankCode, bankProvinceCode, bankBranchCode, accountName, accountNumber } = data;

    if (!name || !name.trim()) {
      toastr.error('Chưa nhập tên tài khoản thanh toán!');
      return false;
    }
    if (!bankCode) {
      toastr.error('Chưa chọn ngân hàng!');
      return false;
    }
    if (!bankProvinceCode) {
      toastr.error('Chưa chọn tỉnh!');
      return false;
    }
    if (!bankBranchCode) {
      toastr.error('Chưa chọn chi nhánh!');
      return false;
    }
    if (!accountName) {
      toastr.error('Chưa nhập tên chủ tài khoản!');
      return false;
    }
    if (!accountNumber) {
      toastr.error('Chưa nhập số tài khoản!');
      return false;
    }
    return true;
  }

  bankValueMap = bank => bank && bank.code || null;
  bankDisplayMap = bank => bank && bank.name || null;

  constructor(
    private fb: FormBuilder,
    private modalAction: ModalAction,
    private receiptApi: ReceiptApi,
    private util: UtilService,
    private bankQuery: BankQuery
  ) {
    super();
  }

  ngOnInit() {}

  ngAfterViewInit() {
    if (!this.ledger || !this.ledger.bank_account) {
      return;
    }

    this.formInitializing = true;

    const {name, province, branch, account_name, account_number} = this.ledger.bank_account;
    const bankCode = this.bankQuery.getBankByName(name)?.code;
    const bankProvinceCode = this.bankQuery.getBankProvinceByName(province)?.code;
    const bankBranchCode = this.bankQuery.getBankBranchByName(branch, bankProvinceCode)?.code;

    this.ledgerForm.patchValue({
      bankCode,
      bankProvinceCode,
      bankBranchCode,
      accountName: account_name,
      accountNumber: account_number,
      name: this.ledger.name,
      note: this.ledger.note
    });

    this.formInitializing = false;
  }

  formatName() {
    const accountName = this.ledgerForm.getRawValue().accountName;
    this.ledgerForm.patchValue({
      accountName: this.util.formatName(accountName)
    }, {emitEvent: false});
  }

  closeModal() {
    this.modalAction.close(false);
  }

  async confirm() {
    this.loading = true;
    try {

      const ledgerData = this.ledgerForm.getRawValue();

      if (!AddLedgerModalComponent.validateLedgerData(ledgerData)) {
        return this.loading = false;
      }

      const { name, bankCode, bankProvinceCode, bankBranchCode, accountName, accountNumber, note } = ledgerData;
      const body = {
        name, note,
        bank_account: {
          name: this.bankQuery.getBank(bankCode)?.name,
          province: this.bankQuery.getBankProvince(bankProvinceCode)?.name,
          branch: this.bankQuery.getBankBranch(bankBranchCode)?.name,
          account_name: accountName,
          account_number: accountNumber
        }
      };

      let ledger: any;
      if (!this.ledger.id) {
        ledger = await this.receiptApi.createLedger(body);
        toastr.success('Tạo tài khoản thanh toán thành công.');
      } else {
        ledger = await this.receiptApi.updateLedger({...body, id: this.ledger.id});
        toastr.success('Cập nhật tài khoản thanh toán thành công.');
      }
      this.modalAction.dismiss(ledger);
    } catch (e) {
      debug.error('ERROR in creating ledger', e);
      toastr.error(
        `${this.ledger.id ? 'Cập nhật' : 'Tạo'} tài khoản thanh toán không thành công.`,
        e.code && (e.message || e.msg)
      );
    }
    this.loading = false;
  }

}
