import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import {CreateShopFormComponent} from "apps/topship/src/app/components/create-shop-form/create-shop-form.component";
import { Address } from 'libs/models/Address';
import { UtilService } from 'apps/core/src/services/util.service';
import { UserService } from 'apps/core/src/services/user.service';
import { ShopService } from '@etop/features';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { AddressService } from 'apps/core/src/services/address.service';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { MoneyTransactionService } from 'apps/topship/src/services/money-transaction.service';
import { ShopInfoModel } from 'apps/topship/src/models/ShopInfo';
import { CmsService } from 'apps/core/src/services/cms.service';
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'topship-onboarding-modal',
  templateUrl: './onboarding-modal.component.html',
  styleUrls: ['./onboarding-modal.component.scss']
})
export class OnboardingModalComponent extends BaseComponent implements OnInit {
  @ViewChild('createShopForm', {static: false}) createShopForm: CreateShopFormComponent;
  loading = false;
  shippingPolicyAccepted = false;

  constructor(
    private auth: AuthenticateStore,
    private modalAction: ModalAction,
  ) {
    super();
  }

  get shop() {
    return this.auth.snapshot.shop;
  }

  ngOnInit() {}

  async createShop() {
    this.loading = true;
    const res = await this.createShopForm.confirm();
    if (res) {
      this.dismissModal();
    }
    this.loading = false;
  }

  dismissModal() {
    this.modalAction.dismiss(null);
  }

  closeModal() {
    this.modalAction.close(false);
  }

}
