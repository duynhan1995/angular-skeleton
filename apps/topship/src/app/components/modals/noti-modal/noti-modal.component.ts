import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'topship-noti-modal',
  templateUrl: './noti-modal.component.html',
  styleUrls: ['./noti-modal.component.scss']
})
export class NotiModalComponent implements OnInit {
  @Input() title;
  @Input() content;

  constructor() { }

  ngOnInit() {
  }

}
