import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'topship-announcement-modal',
  templateUrl: './announcement-modal.component.html',
  styleUrls: ['./announcement-modal.component.scss']
})
export class AnnouncementModalComponent implements OnInit {
  @Input() announcement_content: any;

  constructor(private modalDismiss: ModalAction) { }

  ngOnInit() {}

  dismissModal() {
    this.modalDismiss.dismiss(null);
  }

}
