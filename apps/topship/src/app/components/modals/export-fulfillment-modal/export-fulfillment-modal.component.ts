import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { FulfillmentApi } from '@etop/api';
import {LocationQuery} from "@etop/state/location";
import {FULFILLMENT_STATE} from "@etop/models";

@Component({
  selector: 'topship-export-fulfillment-modal',
  templateUrl: './export-fulfillment-modal.component.html',
  styleUrls: ['./export-fulfillment-modal.component.scss']
})
export class ExportFulfillmentModalComponent implements OnInit {
  @Input() ids: Array<string> = [];
  datePickerOpts = {
    editableDateField: false,
    showClearDateBtn: true,
    openSelectorOnInputClick: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '12px',
    inline: false
  };

  fromDate = null;
  toDate = null;
  shipping_state = 'all';
  carrier = 'all';
  province = 'all';
  delimiter = null;
  excel_compatible_mode = null;

  loading = false;
  display_progress = false;
  error_progress = false;
  progress_percent = 0;
  exportLink = null;

  shipping_state_options: Array<{ name: string; value: string; }> = [
    {
      name: 'Tất cả', value: 'all'
    }
  ];
  carrier_options: Array<{ name: string; value: string; }> = [
    { name: 'Tất cả', value: 'all' },
    { name: 'Giao Hàng Nhanh', value: 'ghn' },
    { name: 'Giao Hàng Tiết Kiệm', value: 'ghtk' },
    { name: 'Viettel Post', value: 'vtpost' }
  ];
  province_options: Array<{ name: string; value: string; }> = [
    {
      name: 'Tất cả', value: 'all'
    }
  ];

  constructor(
    private ffmApi: FulfillmentApi,
    public modalDismiss: ModalAction,
    private locationQuery: LocationQuery,
  ) {
  }

  async ngOnInit() {
    this.fromDate = { jsdate: new Date(moment().subtract(15, 'days').format('YYYY/MM/DD')) };
    this.toDate = { jsdate: new Date() };

    this.shipping_state_options = [{
      name: 'Tất cả', value: 'all'
    }].concat(Object.entries(FULFILLMENT_STATE)
      .filter(entry => ['default', 'unknown', 'error', 'assigning'].indexOf(entry[0]) < 0)
      .map(entry => ({
        name: entry[1],
        value: entry[0]
      })));

    const provinces = this.locationQuery.getValue().provincesList;
    this.province_options = this.province_options.concat(provinces.map(p => {
      return { value: p.id, name: p.name };
    }));
  }

  onCloseExportModal() {
    this.modalDismiss.dismiss({});
  }

  async onConfirmExportExcel() {
    this.loading = true;
    this.display_progress = true;
    this.error_progress = false;
    try {
      let to_date = new Date(this.toDate.jsdate.getTime());
      to_date.setDate(to_date.getDate() + 1);

      let queryData = {
        date_from: moment(this.fromDate.jsdate).format('YYYY-MM-DD'),
        date_to: moment(to_date).format('YYYY-MM-DD'),
        excel_compatible_mode: !this.excel_compatible_mode,
        filters: []
      };
      if (this.ids.length) {
        queryData['ids'] = this.ids;
      }
      if (this.shipping_state != 'all') {
        queryData['filters'].push({
          name: 'shipping_state',
          op: '=',
          value: this.shipping_state
        });
      }
      if (this.carrier != 'all') {
        queryData['filters'].push({
          name: 'carrier',
          op: '=',
          value: this.carrier
        })
      }
      if (this.province != 'all') {
        queryData['filters'].push({
          name: 'address_to.province_code',
          op: '=',
          value: this.province
        })
      }
      if (this.delimiter) {
        queryData['delimiter'] = ';';
      }

      let progress = this.ffmApi.requestExportFulfillmentExcel(queryData).subscribe({
        next: (step: any) => {
          if (step.error) {
            this.progress_percent = step.progress_error / step.progress_max * 100;
            progress.unsubscribe();
          } else {
            this.progress_percent = step.progress_value / step.progress_max * 100;
          }
          if (step.download_url) {
            this.loading = false;
            this.display_progress = false;
            this.exportLink = step.download_url;
            this.progress_percent = 100;
          }
        },
        complete: () => {
          this.progress_percent = 100;
          this.loading = false;
          this.display_progress = false;
          progress.unsubscribe();
        },
        error: (err: any) => {
          this.loading = false;
          this.error_progress = true;
          toastr.error(err.message, 'Export đơn giao hàng thất bại!');
          progress.unsubscribe();
        }
      });
    } catch (e) {
      debug.error('error in export', e);
    }

  }
}
