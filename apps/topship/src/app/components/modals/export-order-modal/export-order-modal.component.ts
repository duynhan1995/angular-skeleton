import { Component, Input, OnInit } from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import * as moment from 'moment';
import { OrderApi } from '@etop/api';
import {ORDER_STATUS} from "@etop/models";

@Component({
  selector: 'topship-export-order-modal',
  templateUrl: './export-order-modal.component.html',
  styleUrls: ['./export-order-modal.component.scss']
})
export class ExportOrderModalComponent implements OnInit {
  @Input() ids: Array<string> = [];
  datePickerOpts = {
    editableDateField: false,
    showClearDateBtn: true,
    openSelectorOnInputClick: true,
    dateFormat: 'dd/mm/yyyy',
    selectionTxtFontSize: '12px',
    inline: false
  };

  fromDate = null;
  toDate = null;
  order_status = 'all';
  delimiter = null;
  excel_compatible_mode = null;

  loading = false;
  display_progress = false;
  error_progress = false;
  progress_percent = 0;
  exportLink = null;

  order_status_options: Array<{ name: string; value: string; }> = [
    {
      name: 'Tất cả', value: 'all'
    }
  ];

  constructor(
    public modalDismiss: ModalAction,
    private orderApi: OrderApi
  ) { }

  ngOnInit() {
    this.fromDate = { jsdate: new Date(moment().subtract(15, 'days').format('YYYY/MM/DD')) };
    this.toDate = { jsdate: new Date() };

    this.order_status_options = [{
      name: 'Tất cả', value: 'all'
    }].concat(Object.entries(ORDER_STATUS)
      .map(entry => ({
        name: entry[1],
        value: entry[0]
      })));
  }

  onCloseExportModal() {
    this.modalDismiss.dismiss({});
  }

  async onConfirmExportExcel() {
    this.loading = true;
    this.display_progress = true;
    this.error_progress = false;
    try {
      let to_date = new Date(this.toDate.jsdate.getTime());
      to_date.setDate(to_date.getDate() + 1);

      let queryData = {
        date_from: moment(this.fromDate.jsdate).format('YYYY-MM-DD'),
        date_to: moment(to_date).format('YYYY-MM-DD'),
        excel_compatible_mode: !this.excel_compatible_mode
      };

      if (this.ids.length) {
        queryData['ids'] = this.ids;
      }

      if (this.order_status != 'all') {
        queryData['filters'] = [
          { name: 'status', op: '=', value: this.order_status }
        ];
      }
      if (this.delimiter) {
        queryData['delimiter'] = ';';
      }
      let progress = this.orderApi.requestExportOrderExcel(queryData).subscribe({
        next: (step: any) => {
          if (step.error) {
            this.progress_percent = step.progress_error / step.progress_max * 100;
            progress.unsubscribe();
          } else {
            this.progress_percent = step.progress_value / step.progress_max * 100;
          }
          if (step.download_url) {
            this.loading = false;
            this.display_progress = false;
            this.exportLink = step.download_url;
            this.progress_percent = 100;
          }
        },
        complete: () => {
          this.progress_percent = 100;
          this.loading = false;
          this.display_progress = false;
          progress.unsubscribe();
        },
        error: (err: any) => {
          this.loading = false;
          this.error_progress = true;
          progress.unsubscribe();
          toastr.error(err.message, 'Export đơn hàng thất bại!');
        }
      });
    } catch (e) {
    }
    // this.ffmApi.requestExportFulfillmentExcel is not waited for finishing its process
    // so that, codes executed here will run before it finished its process.
  }

}
