import { Component, Input, OnInit } from '@angular/core';
import { FulfillmentService } from 'apps/topship/src/services/fulfillment.service';

@Component({
  selector: 'topship-returning-fulfillments-warning',
  templateUrl: './returning-fulfillments-warning.component.html',
  styleUrls: ['./returning-fulfillments-warning.component.scss']
})
export class ReturningFulfillmentsWarningComponent implements OnInit {
  @Input() returningQuantity: number;
  showWarning = true;

  constructor(
    private ffmService: FulfillmentService
  ) { }

  ngOnInit() {
  }

  viewDetail() {
    this.ffmService.onViewReturningFfms$.next();
  }

  close() {
    this.showWarning = false;
  }

}
