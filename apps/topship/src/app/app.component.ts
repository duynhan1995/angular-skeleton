import { Component, OnInit } from '@angular/core';
import {CmsService} from "apps/core/src/services/cms.service";
import { UtilService } from 'apps/core/src/services/util.service';
import { AppService } from '@etop/web/core/app.service';
import { NavigationEnd, Router } from '@angular/router';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import {LocationService} from "@etop/state/location";

const specialRoutes = {
  login: {
    canMobile: true,
    needLogin: false
  },
  register: {
    canMobile: true,
    needLogin: false
  },
  'top-ship': {
    canMobile: true,
    needLogin: false
  },
  'survey': {
    canMobile: true,
    needLogin: false
  },
  'reset-password': {
    canMobile: true,
    needLogin: false
  },
  'create-shop': {
    canMobile: true,
    needLogin: true
  },
  'admin-login': {
    canMobile: true,
    needLogin: false
  },
  'verify-email': {
    canMobile: true,
    needLogin: true
  },
  'invitation': {
    canMobile: true,
    needLogin: true
  }
};

@Component({
  selector: 'topship-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loadConfig = true;
  delayPassed = false;
  completedView = false;

  constructor(
    private util: UtilService,
    private router: Router,
    private appService: AppService,
    private cms: CmsService,
    private commonUsecase: CommonUsecase,
    private locationService: LocationService
  ) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.updateHeader();
      }
    });
  }


  ngOnInit() {
    this.locationService.initLocations().then();
    this.cms.initBanners().then();

    setTimeout(() => this.delayPassed = true, 1000);

    this.appService.bootstrap()
      .then(() => this.commonUsecase.checkAuthorization(true))
      .then(() => this.loadConfig = false)
      .catch(err => debug.log('Bootstrap failed', err));
  }

  updateHeader() {
    const path = this.router.url;
    const terms = path.split(/\/|\?/);
    const hasSlug = terms[1] === 's';
    const url = hasSlug ? terms[3] : terms[1];
    const route = specialRoutes[url];
    if (route) {
      const { canMobile } = route;
      if (canMobile) {
        this.completedView = true;
      }
    } else {
      this.completedView = false;
    }
  }

  get isMobile() {
    return this.util.isMobile;
  }

}
