import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule} from '@angular/core';

import {CoreModule} from 'apps/core/src/core.module';
import {SharedModule} from 'apps/shared/src/shared.module';

import {AppComponent} from './app.component';
import {CommonModule, DatePipe, DecimalPipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {CommonUsecase} from 'apps/shared/src/usecases/common.usecase.service';
import {TopshipCommonUsecase} from './usecases/topship-common.usecase.service';
import {GoogleAnalyticsService} from 'apps/core/src/services/google-analytics.service';
import {RegisterComponent} from './pages/register/register.component';
import {IframeService} from 'apps/core/src/services/iframe.service';
import {UtilService} from 'apps/core/src/services/util.service';
import {SurveyComponent} from './pages/survey/survey.component';
import {TimerService} from 'apps/core/src/services/timer.service';
import {FulfillmentService} from '../services/fulfillment.service';
import {OrderService} from '../services/order.service';
import {ShopCrmService} from '../services/shop-crm.service';
import {ShopService} from '@etop/features/services/shop.service';
import {AddressService} from 'apps/core/src/services/address.service';
import {MoneyTransactionService} from '../services/money-transaction.service';
import {MaterialModule} from '@etop/shared/components/etop-material/material';
import {StatisticService} from 'apps/topship/src/services/statistic.service';
import {ProductService} from 'apps/topship/src/services/product.service';
import {OnboardingModalComponent} from './components/modals/onboarding-modal/onboarding-modal.component';
import {ForceModalComponent} from './components/modals/force-modal/force-modal.component';
import {NotiModalComponent} from './components/modals/noti-modal/noti-modal.component';
import {ForceButtonModule} from './components/force-button/force-button.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PromiseQueueService} from 'apps/core/src/services/promise-queue.service';
import {NotificationService} from '../services/notification.service';
import {ExportFulfillmentModalComponent} from './components/modals/export-fulfillment-modal/export-fulfillment-modal.component';
import {ExportOrderModalComponent} from './components/modals/export-order-modal/export-order-modal.component';
import {LoginModule} from 'apps/shared/src/pages/login/login.module';
import {AnnouncementModalComponent} from './components/modals/announcement-modal/announcement-modal.component';
import {ResetPasswordComponent} from './pages/reset-password/reset-password.component';
import {VerifyEmailComponent} from './pages/verify-email/verify-email.component';
import {VerifyPhoneComponent} from './pages/verify-phone/verify-phone.component';
import {ReceiptService} from '../services/receipt.service';
import {AddLedgerModalComponent} from 'apps/topship/src/app/components/modals/add-ledger-modal/add-ledger-modal.component';
import {CustomerService} from 'apps/topship/src/services/customer.service';
import {
  CategoryApi,
  CollectionApi,
  CustomerApi,
  FulfillmentApi,
  NotificationApi,
  OrderApi,
  ProductApi,
  ReceiptApi
} from '@etop/api';
import {LoginComponent} from './pages/login/login.component';
import {CreateShopComponent} from './pages/create-shop/create-shop.component';
import {TopshipAppGuard} from './topship-app.guard';
import {DesktopOnlyComponent} from './components/desktop-only/desktop-only.component';
import {RequireStokenModalComponent} from './components/modals/require-stoken-modal/require-stoken-modal.component';
import {IsUserActiveGuard} from './is-user-active.guard';
import {CONFIG_TOKEN} from '@etop/core/services/config.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../environments/environment';
import {VTigerService} from 'apps/core/src/services/vtiger.service';
import {TelegramService} from '@etop/features';
import {EtopPipesModule} from 'libs/shared/pipes/etop-pipes.module';
import {EtopFilterModule} from '@etop/shared/components/etop-filter/etop-filter.module';
import {EtopFormsModule, EtopMaterialModule} from '@etop/shared';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {registerLocaleData} from '@angular/common';
import localeVi from '@angular/common/locales/vi';
import { CreateShopFormComponent } from './components/create-shop-form/create-shop-form.component';

registerLocaleData(localeVi);

const apis = [
  OrderApi,
  FulfillmentApi,
  ProductApi,
  CollectionApi,
  NotificationApi,
  CategoryApi,
  ReceiptApi,
  CustomerApi
];
const services = [
  GoogleAnalyticsService,
  FulfillmentService,
  OrderService,
  StatisticService,
  UtilService,
  IframeService,
  TimerService,
  ShopCrmService,
  ShopService,
  AddressService,
  MoneyTransactionService,
  ProductService,
  PromiseQueueService,
  NotificationService,
  ReceiptService,
  CustomerService,
  VTigerService,
  TelegramService
];

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsUserActiveGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [IsUserActiveGuard]
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'create-shop',
    component: CreateShopComponent,
    canActivate: [TopshipAppGuard]
  },
  {
    path: 'survey',
    component: SurveyComponent,
    canActivate: [TopshipAppGuard, IsUserActiveGuard]
  },
  {
    path: 'verify-email',
    component: VerifyEmailComponent
  },
  {
    path: 'verify-phone',
    component: VerifyPhoneComponent
  },
  {
    path: 'invitation',
    loadChildren: () =>
      import('./pages/invitation/invitation.module').then(
        m => m.InvitationModule
      )
  },
  {
    path: 'admin-login',
    loadChildren: () =>
      import('apps/topship/src/app/pages/admin-login/admin-login.module').then(
        m => m.AdminLoginModule
      )
  },
  {
    path: 'login-hub',
    loadChildren: () =>
      import('apps/topship/src/app/pages/etop-login/etop-login.module').then(
        m => m.EtopLoginModule
      )
  },
  {
    path: 'printer',
    loadChildren: () =>
      import('apps/topship/src/app/pages/printer-page/printer-page.module').then(
        m => m.PrinterPageModule
      )
  },
  {
    path: 's',
    loadChildren: () =>
      import('apps/topship/src/app/topship/topship.module').then(m => m.TopshipModule)
  },
  {
    path: '**',
    redirectTo: 's/-1/orders'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    ResetPasswordComponent,
    SurveyComponent,
    OnboardingModalComponent,
    ForceModalComponent,
    NotiModalComponent,
    ExportFulfillmentModalComponent,
    ExportOrderModalComponent,
    AnnouncementModalComponent,
    VerifyEmailComponent,
    VerifyPhoneComponent,
    AddLedgerModalComponent,
    LoginComponent,
    CreateShopComponent,
    DesktopOnlyComponent,
    RequireStokenModalComponent,
    CreateShopFormComponent,
  ],
  entryComponents: [
    OnboardingModalComponent,
    ForceModalComponent,
    NotiModalComponent,
    ExportFulfillmentModalComponent,
    ExportOrderModalComponent,
    AnnouncementModalComponent,
    AddLedgerModalComponent,
    RequireStokenModalComponent,
  ],
  imports: [
    CoreModule.forRoot(),
    SharedModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    ForceButtonModule,
    RouterModule.forRoot(routes),
    NgbModule,
    LoginModule,
    EtopFilterModule,
    EtopPipesModule,
    EtopMaterialModule,
    EtopFormsModule,
    environment.production ? [] : AkitaNgDevtools.forRoot()
  ],
  providers: [
    {provide: CommonUsecase, useClass: TopshipCommonUsecase},
    {provide: CONFIG_TOKEN, useValue: environment},
    {provide: LOCALE_ID, useValue: 'vi'},
    ...apis,
    ...services,
    TopshipAppGuard,
    IsUserActiveGuard,
    DecimalPipe,
    DatePipe
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TopshipAppModule {
}
