import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticateStore } from '@etop/core';
import { UserService } from 'apps/core/src/services/user.service';
import { NotificationService } from 'apps/topship/src/services/notification.service';
import misc from 'apps/core/src/libs/misc';

@Component({
  selector: 'topship-etop-login',
  templateUrl: './etop-login.component.html',
  styleUrls: ['./etop-login.component.scss']
})
export class EtopLoginComponent implements OnInit {
  user: any;
  shop: any;
  redirect_url;
  domain;
  token;
  loading = true;

  constructor(
    private auth: AuthenticateStore,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private noti: NotificationService,
    private changeDetector: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    try {
      this.loading = true;
      let query = this.activatedRoute.snapshot.queryParams;
      let session = query['session'];
      if (!session || !this.isBase64(session)) {
        throw new Error("Có lỗi xảy ra. Phiên đăng nhập không tồn tại");
      }
      else {
        this.token = misc.decodeBase64(session);
        let _session = await this.userService.checkToken(this.token);
        this.user = _session?.user;
        this.shop = _session?.shop;
        let _redirect_url = query['path'];
        if (_redirect_url && this.isBase64(_redirect_url)) {
          this.redirect_url = misc.decodeBase64(_redirect_url) || '';
        }
        if (this.auth.snapshot?.user && _session?.user?.id === this.auth.snapshot?.user?.id)  {
          location.href = window.location.origin + this.redirect_url;
          this.changeDetector.detectChanges();
        } else {
          this.domain = query['domain'];
          this.loading = false;
        }
      }
    } catch (e) {
      toastr.error(e.message);
      this.router.navigateByUrl(this.redirect_url);
    }
    
  }

  isBase64(str) {
    if (str === '' || str.trim() === '') { return false; }
    try {
        return btoa(atob(str)) == str;
    } catch (err) {
        return false;
    }
  }

  async loginWithToken() {
     try {
      this.noti.deleteDevice();
      this.auth.clear();
      this.auth.updateToken(this.token);
      let res = await this.userService.checkToken();
      let accounts = res.available_accounts.filter(e => e.type == 'shop').sort((a, b) => a.id > b.id);
      for (let i = 0; i < accounts.length; i++) {
        let account = accounts[i];
        let accountRes = await this.userService.switchAccount(
          account.id,
          'LOGIN HUB'
        );
        account.token = accountRes.access_token;
        account.shop = accountRes.shop;
      }

      this.auth.updateInfo({
        token: res.access_token,
        account: res.account,
        user: res.user,
        accounts: accounts,
        shop: res.shop,
        isAuthenticated: true,
        permission: res.account.user_account.permission
      });
      location.href = window.location.origin + this.redirect_url;
    } catch (e) {
      this.router.navigateByUrl('/login');
      debug.error('LOGIN HUB', e);
    }
  }

  logout() {
    this.auth.clear();
    this.router.navigateByUrl('/login');
  }
}
