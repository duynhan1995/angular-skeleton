import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'apps/shared/src/shared.module';
import {MoneyTransactionComponent} from './money-transactions.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {
  EtopCommonModule,
  EtopFilterModule,
  EtopPipesModule,
  MaterialModule,
  SideSliderModule
} from '@etop/shared';
import {MoneyTransactionShopInfoComponent} from './shop-info/shop-info.component';
import {MoneyTransactionListComponent} from './money-transaction-list/money-transaction-list.component';
import {MoneyTransactionDetailComponent} from './components/money-transaction-detail/money-transaction-detail.component';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {MoneyTransactionFfmRowComponent} from './components/money-transaction-ffm-row/money-transaction-ffm-row.component';
import {MoneyTransactionsService} from './money-transactions.service';
import {MoneyTransactionsStore} from './money-transactions.store';
import {MoneyTransactionRowComponent} from "./components/money-transaction-row/money-transaction-row.component";
import { EmptyPageModule } from './../../../../../../libs/shared/components/empty-page/empty-page.module';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: MoneyTransactionComponent},
      {path: ':id', component: MoneyTransactionDetailComponent}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    EtopPipesModule,
    EtopCommonModule,
    EtopFilterModule,
    MaterialModule,
    SideSliderModule,
    NgbTooltipModule,
    EmptyPageModule
  ],
  exports: [],
  declarations: [
    MoneyTransactionComponent,
    MoneyTransactionRowComponent,
    MoneyTransactionShopInfoComponent,
    MoneyTransactionListComponent,
    MoneyTransactionDetailComponent,
    MoneyTransactionFfmRowComponent
  ],
  providers: [
    MoneyTransactionsService,
    MoneyTransactionsStore
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MoneyTransactionsModule {
}
