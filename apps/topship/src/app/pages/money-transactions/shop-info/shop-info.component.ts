import { Component, OnInit } from '@angular/core';
import { AuthenticateStore, BaseComponent } from "@etop/core";
import { ExtendedAccount } from '@etop/models';
import { takeUntil } from 'rxjs/operators';
import { UtilService } from 'apps/core/src/services/util.service';
import { CmsService } from 'apps/core/src/services/cms.service';

@Component({
  selector: 'topship-mt-shop-info',
  templateUrl: './shop-info.component.html',
  styleUrls: ['./shop-info.component.scss']
})
export class MoneyTransactionShopInfoComponent extends BaseComponent implements OnInit {
  shop: ExtendedAccount;
  slug: string | number;

  note = null;

  constructor(
    private auth: AuthenticateStore,
    private util: UtilService,
    private cms: CmsService
  ) {
    super();
  }

  ngOnInit() {
    this.auth.authenticatedData$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.shop = this.auth.snapshot.shop;
    });

    this.note = this.cms.getTransactionNote();
    this.cms.onBannersLoaded.pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.note = this.cms.getTransactionNote();
      });
  }
}
