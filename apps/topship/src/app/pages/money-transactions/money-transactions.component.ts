import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminShopApi } from '@etop/api';
import { AuthenticateStore } from "@etop/core";
import { FilterOperator, FilterOptions, Filters } from '@etop/models';
import { MoneyTransactionListComponent } from './money-transaction-list/money-transaction-list.component';

@Component({
  selector: 'topship-money-transactions',
  template: `
    <shop-mt-shop-info></shop-mt-shop-info>
    <shop-mt-list #transactionList></shop-mt-list>`
})
export class MoneyTransactionComponent implements OnInit {
  @ViewChild('transactionList', {static: false}) transactionList: MoneyTransactionListComponent;
  filters: FilterOptions = [
    {
      label: 'Mã phiên',
      name: 'code',
      type: 'input',
      fixed: true,
      operator: FilterOperator.eq
    },
    {
      label: 'Trạng thái',
      name: 'status',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: "Chưa xác nhận", value: 'Z' },
        { name: "Đã xác nhận", value: 'P' }
      ]
    },
    {
      label: 'Tài khoản ngân hàng',
      name: 'shop.bank_account',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        { name: "Đã có", value: 'true' },
        { name: "Chưa có", value: 'false' }
      ]
    },
    {
      label: 'Lịch chuyển khoản',
      name: 'shop.money_transaction_rrule',
      type: 'select',
      fixed: true,
      operator: FilterOperator.eq,
      options: [
        { name: 'Tất cả', value: '' },
        {
          name: 'Hàng ngày',
          value: 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR'
        },
        {
          name: 'Thứ 2-4-6 hàng tuần',
          value: 'FREQ=WEEKLY;BYDAY=MO,WE,FR'
        },
        {
          name: 'Thứ 3-5 hàng tuần',
          value: 'FREQ=WEEKLY;BYDAY=TU,TH'
        },
        {
          name: '1 tuần 1 lần vào thứ 6',
          value: 'FREQ=WEEKLY;BYDAY=FR'
        },
        {
          name: '1 tháng 2 lần vào thứ 6',
          value: 'FREQ=MONTHLY;BYDAY=+2FR,-1FR'
        },
        {
          name: '1 tháng 1 lần vào thứ 6',
          value: 'FREQ=MONTHLY;BYDAY=-1FR'
        },
        {
          name: '1 tháng 1 lần vào ngày cuối cùng của tháng',
          value: 'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1'
        }
      ]
    },
    {
      label: 'Mã shop',
      name: 'shop.code',
      type: 'input',
      operator: FilterOperator.eq,
    }
  ];

  constructor(
    private auth: AuthenticateStore,
    private shopApi: AdminShopApi
  ) {
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('admin/money_transaction:view');
  }

  ngOnInit() {}

  async specialFilterHandler($event: Filters) {
    const shop_id = await this.filterByShopCode($event);
    await this.transactionList.filter($event, shop_id);
  }

  private async filterByShopCode($event: Filters) {
    try {
      const index = $event.findIndex(f => f.name == 'shop.code');
      if (index >= 0) {
        const shops = await this.shopApi.getShops({
          paging: {offset: 0, limit: 1},
          filters: [
            { name: 'code', op: FilterOperator.eq, value: $event[index].value }
          ]
        });
        $event.splice(index, 1);
        return shops[0].id;
      } else {
        throw new Error('Don\'t have shop code filter');
      }
    } catch(e) {
      return null;
    }
  }

}
