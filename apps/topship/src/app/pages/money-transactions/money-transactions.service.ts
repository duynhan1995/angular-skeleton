import { Injectable } from '@angular/core';
import {
  ConnectionApi,
  FulfillmentApi,
  MoneyTransactionApi
} from '@etop/api';
import { Fulfillment } from "libs/models/Fulfillment";
import { MoneyTransactionsStore } from './money-transactions.store';
import { Router } from "@angular/router";
import { Paging } from "@etop/shared";
import { MoneyTransactionShop } from "libs/models/MoneyTransactionShop";
import { FilterOperator, Filters } from '@etop/models';
import { ConnectionStore } from '@etop/features';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Injectable()
export class MoneyTransactionsService {

  constructor(
    private router: Router,
    private transactionApi: MoneyTransactionApi,
    private store: MoneyTransactionsStore,
    private connectionApi: ConnectionApi,
    private fulfillmentApi: FulfillmentApi,
    private connectionStore: ConnectionStore
  ) {}

  async getEveryMoneyTransaction(filters: Filters, shop_id?: string) {
    let mts = [];
    let offset = 0;

    while (true) {
      const _mts = await this.transactionApi.getMoneyTransactions({
        paging: { offset, limit: 1000 },
        filters,
        shop_id
      });

      mts = mts.concat(_mts['money_transactions']);
      if (!_mts['money_transactions'].length || _mts['money_transactions'].length < 1000) break;
      offset += 1000;
    }
    return mts;
  }

  async getConnections() {
    try {
      const connections = await this.connectionApi.getConnections();
      this.store.setConnections(connections);
    } catch(e) {
      debug.error('ERROR in getting Connections', e);
    }
  }

  async getFulfillments(start?: number, perpage?: number, filters?: Filters): Promise<Fulfillment[]> {
    let paging = {
      offset: start || 0,
      limit: perpage || 20
    };
    return new Promise((resolve, reject) => {
      this.connectionStore.state$.pipe(
        map(s => s?.initConnections),
        distinctUntilChanged((a,b) => a?.length == b?.length)
      ).subscribe(connections => {
        if (connections?.length) {
          this.fulfillmentApi.getFulfillments({ paging, filters })
            .then(res => {
              res.fulfillments = res.fulfillments.map(f => Fulfillment.fulfillmentMap(f, connections));
              resolve(res.fulfillments);
            })
            .catch(err => reject(err));
        }
      });
    });
  }

  async getEveryFulfillment(doLoading = true, filters: Filters) {
    let ffms = [];
    let offset = 0;

    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      while (true) {
        const res = await this.getFulfillments(offset, 1000, filters);
        ffms = ffms.concat(res);
        if (!res.length || res.length < 1000) break;
        offset += 1000;
      }
    } catch(e) {
      debug.error('ERROR in getting every Fulfillments', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
    return ffms;
  }

  selectFulfillments(ffms: Fulfillment[]) {
    this.store.selectFulfillments(ffms);
  }

  setMoneyTransactionFilters(filters: Filters) {
    this.store.setMoneyTransactionFilters(filters);
  }

  setMeyTransactionPaging(paging: Paging) {
    this.store.setMoneyTransactionPaging(paging);
  }

  async getMoneyTransaction(id: string, doLoading = true) {
    if (doLoading) {
      this.store.setFulfillmentsLoading(true);
    }
    try {
      const transaction = await this.transactionApi.getMoneyTransaction(id);

      this.store.detailMoneyTransaction(transaction);

      const filters: Filters = [{
        name: 'money_transaction.id',
        op: FilterOperator.eq,
        value: transaction.id
      }];
      const _ffms = await this.getEveryFulfillment(false, filters);

      this.store.setFulfillments(_ffms);

    } catch(e) {
      debug.error('ERROR in getMtShop Detail', e);
    }
    if (doLoading) {
      this.store.setFulfillmentsLoading(false);
    }
  }

  async getMoneyTransactions(doLoading = true, shop_id?: string) {
    this.store.setMoneyTransactionLastPage(false);
    if (doLoading) {
      this.store.setMoneyTransactionLoading(true);
    }
    try {
      const {mtFilters, mtPaging} = this.store.snapshot;
      const transactions = await this.transactionApi.getMoneyTransactions({
        filters: mtFilters,
        paging: mtPaging,
        shop_id
      });
      if (mtPaging.offset > 0 && transactions['money_transactions'].length == 0) {
        this.store.setMoneyTransactionLastPage(true);
      } else {
        //const shopIds = transactions['money_transactions'].map(mt => mt.shop_id);
        //const shops = await this.shopApi.getShopsByIDs(shopIds);

        this.store.setMoneyTransactions(transactions['money_transactions'].map(mt => {
          //mt.shop = shops && shops.find(s => s.id == mt.shop_id);
          return mt;
        }));

        if (doLoading) {
          this.selectMoneyTransactions([]);
        }
      }
    } catch(e) {
      debug.error('ERROR in getting Transactions', e);
    }
    if (doLoading) {
      this.store.setMoneyTransactionLoading(false);
    }
  }

  selectMoneyTransactions(mts: MoneyTransactionShop[]) {
    this.store.selectMoneyTransactions(mts);
  }

}
