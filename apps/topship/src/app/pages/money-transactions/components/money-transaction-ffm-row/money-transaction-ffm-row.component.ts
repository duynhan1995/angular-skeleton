import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { FulfillmentStore } from 'apps/core/src/stores/fulfillment.store';

@Component({
  selector: '[topship-mt-ffm-row]',
  templateUrl: './money-transaction-ffm-row.component.html',
  styleUrls: ['./money-transaction-ffm-row.component.scss']
})
export class MoneyTransactionFfmRowComponent implements OnInit {
  @Input() ffm: Fulfillment;
  @Input() liteMode = false;

  constructor(
  private ffmStore: FulfillmentStore,
  ) { }

  get showCancelReason() {
    return this.ffm.shipping_state == 'cancelled' && this.ffm.cancel_reason;
  }

  ngOnInit() {
  }

  viewDetailOrder(order_code) {

  }

  async viewDetailFfm() {
    await this.ffmStore.navigate('shipment', this.ffm.shipping_code, null);
  }

}
