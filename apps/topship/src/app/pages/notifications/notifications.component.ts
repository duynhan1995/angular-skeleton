import { Component, OnInit } from '@angular/core';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';

@Component({
  selector: 'topship-notifications',
  template: `
    <div class="page-content pb-0">
      <topship-notification-list #notiList></topship-notification-list>
    </div>`
})
export class NotificationsComponent extends PageBaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
