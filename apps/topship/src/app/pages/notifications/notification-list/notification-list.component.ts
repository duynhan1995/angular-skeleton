import { Component, OnInit } from '@angular/core';
import { Noti } from '../../../../../../../libs/models/Noti';
import { NotificationService } from '../../../../services/notification.service';
import { AuthenticateStore } from '@etop/core';

const EMPTYTEXT = {
  important: '',
  fulfillment: 'về Giao hàng',
  money_transaction_shipping: 'về Đối soát',
  unknown: '',
};

@Component({
  selector: 'topship-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit {
  tabs = [
    {
      name: 'important',
      title: 'Quan trọng'
    },
    {
      name: 'fulfillment',
      title: 'Giao hàng'
    },
    {
      name: 'money_transaction_shipping',
      title: 'Đối soát'
    },
    {
      name: 'unknown',
      title: 'Khác'
    }
  ];

  currentTab = {
    name: 'important'
  };

  loading = true;
  loadingMore = false;
  notifications: Array<Noti> = [];
  result_notifications: Array<Noti> = [];
  emptyText = 'về Giao hàng';
  allIsRead = true;
  availableNotiShops = [];
  selectedNotiShopToken = '';
  shop_id_encoded = '';

  offset = 0;

  constructor(
    private notification: NotificationService,
    private auth: AuthenticateStore
  ) { }

  async ngOnInit() {
    await this.loadNotifications();
    this.emptyText = EMPTYTEXT[this.currentTab.name];
    this.availableNotiShops = this.auth.snapshot.accounts.map(a => {
      return { name: a.name, code: a.token, id: a.id };
    });
    const availableNotiShop = this.availableNotiShops.find(a => a.id == this.auth.snapshot.account.id);
    this.selectedNotiShopToken = availableNotiShop ? availableNotiShop.code : null;
  }

  onScroll(event) {
    const { offsetHeight, scrollTop, scrollHeight } = event.target;
    if (offsetHeight + scrollTop >= scrollHeight) {
      this.loadMoreNotifications();
    }
  }

  onTabChanged(tab_name) {
    this.currentTab.name = tab_name;
    this.offset = 0;
    this.emptyText = EMPTYTEXT[tab_name];
    this.loadNotifications();
  }

  selectNotiShop() {
    const shop_id = this.availableNotiShops.find(a => a.code == this.selectedNotiShopToken).id;
    this.shop_id_encoded = this.auth.getStringTokenByShop(shop_id);
    this.loadNotifications();
  }

  checkAllNotiIsRead() {
    return !this.notifications.some(noti => !noti.is_read);
  }

  filterNotifications(notis: Array<Noti>) {
    if (this.currentTab.name == "important") {
      return notis.filter(noti => noti.send_notification);
    }
    return notis.filter(noti => noti.entity == this.currentTab.name);
  }

  async loadNotifications(offset?: number) {
    this.loading = true;
    try {
      let res = await this.notification.getNotifications(offset, null, this.selectedNotiShopToken);
      if (this.loadingMore) {
        this.result_notifications = this.result_notifications.concat(res.notifications);
        this.notifications = this.filterNotifications(this.notifications.concat(res.notifications));
      } else {
        this.result_notifications = res.notifications;
        this.notifications = this.filterNotifications(res.notifications);
        this.offset = 0;
      }
      this.allIsRead = this.checkAllNotiIsRead();
    } catch (e) {
      toastr.error("Có lỗi trong việc load thông báo. Vui lòng thử lại.");
    }
    this.loading = false;
  }

  async loadMoreNotifications() {
    if (this.loadingMore || this.result_notifications.length < (this.offset + 1) * 20) {
      return;
    }
    this.loadingMore = true;
    try {
      this.offset++;
      await this.loadNotifications(this.offset * 20);
    } catch (e) {
      debug.log(e);
    }
    this.loadingMore = false;
  }

  async readNoti(noti: Noti, index: number) {
    try {
      this.notifications[index].is_read = true;
      await this.notification.readNotification(noti, this.shop_id_encoded);
    } catch (e) {
      debug.error(e);
    }
  }

  async readAllNoti() {
    this.loading = true;
    try {
      const ids = this.notifications.map(noti => noti.id);
      await this.notification.updateNotifications(ids, true);
      this.notifications.forEach(noti => noti.is_read = true);
      this.allIsRead = true;
    } catch (e) {
      debug.error(e);
    }
    this.loading = false;
  }

}
