import {Component, OnInit} from '@angular/core';
import {SurveyInfo} from "@etop/models";
import {CmsService} from "apps/core/src/services/cms.service";
import {AuthenticateStore} from '@etop/core';
import {Router} from '@angular/router';
import {TimerService} from 'apps/core/src/services/timer.service';

interface SurveyQuestion {
  key: string;
  image_url: string;
  question: string;
  answersList: { text: string; value: string | number }[];
  answer: string | number;
}

@Component({
  selector: 'topship-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  providerQuestion: SurveyQuestion = null;
  ordersPerDayQuestion: SurveyQuestion = null;
  defaultWeightQuestion: SurveyQuestion = null;

  constructor(
    private auth: AuthenticateStore,
    private timerService: TimerService,
    private router: Router,
    private cms: CmsService
  ) {
  }

  get user() {
    return this.auth.snapshot.user;
  }

  ngOnInit() {
    if (this.cms.bannersLoaded) {
      const _survey = this.cms.getTopshipSurvey();
      this.providerQuestion = _survey['provider'];
      this.ordersPerDayQuestion = _survey['orders_per_day'];
      this.defaultWeightQuestion = _survey['default_weight'];
    } else {
      this.cms.onBannersLoaded.subscribe(_ => {
        const _survey = this.cms.getTopshipSurvey();
        this.providerQuestion = _survey['provider'];
        this.ordersPerDayQuestion = _survey['orders_per_day'];
        this.defaultWeightQuestion = _survey['default_weight'];
      });
    }
  }

  saveSurvey() {
    if (!(this.providerQuestion.answer && this.ordersPerDayQuestion.answer && this.defaultWeightQuestion.answer)) {
      return toastr.error('Bạn chưa trả lời hết tất cả các câu hỏi.');
    }

    const survey: SurveyInfo[] = [
      {
        key: this.providerQuestion.key,
        question: this.providerQuestion.question,
        answer: this.providerQuestion.answer.toString()
      },
      {
        key: this.ordersPerDayQuestion.key,
        question: this.ordersPerDayQuestion.question,
        answer: this.ordersPerDayQuestion.answer.toString()
      },
      {
        key: this.defaultWeightQuestion.key,
        question: this.defaultWeightQuestion.question,
        answer: this.defaultWeightQuestion.answer.toString()
      }
    ];

    localStorage.setItem('survey', JSON.stringify(survey));
    const slug = this.auth.snapshot.account?.url_slug || this.auth.currentAccountIndex();
    this.router.navigateByUrl(`/s/${slug}/fulfillments`).then();
  }

  signout() {
    this.timerService.clear();
    this.auth.clear();
    this.router.navigateByUrl('/login').then();
  }

}
