import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import { OrderAddress } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { ShopService } from '@etop/features';
import { ETopTradingService } from 'apps/topship/src/services/etop-trading.service';
import { Referral } from 'libs/models/affiliate/Referral';
import { Promotion } from 'libs/models/affiliate/Promotion';
import { UtilService } from 'apps/core/src/services/util.service';
import { MaterialInputComponent } from 'libs/shared/components/etop-material/material-input/material-input.component';
import { StorageService } from 'apps/core/src/services/storage.service';
import {LocationService} from "@etop/state/location";
import {LocationQuery} from "@etop/state/location/location.query";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'topship-form-checkout',
  templateUrl: './form-checkout.component.html',
  styleUrls: ['./form-checkout.component.scss']
})
export class FormCheckoutComponent extends BaseComponent implements OnInit, OnChanges {
  @Output() orderSuccess = new EventEmitter();
  @Output() backOrderForm = new EventEmitter();
  @Input() newOrder;
  @Input() loadingOrder;
  @ViewChild('quantityInput', { static: false })
  quantityInput: MaterialInputComponent;

  orderForm = false;
  aff_code: string;
  paymentMethod;
  codWarning = false;
  address1;
  currentBalance = 0;
  currentShop: any;
  addressesFrom: any;
  defaultAddress: any = new OrderAddress({});
  loading = false;
  checkoutLoading = false;
  variantSelected: any;
  customerAddress = new OrderAddress({});
  total;
  onPromotion = false;
  productReferralCode: any;
  totalCashback = 0;

  checkReferralCode = false;
  coupon;
  checkVat = false;
  format_quantity = false;
  min_quantity;

  locationForm = this.fb.group({
    provinceCode: '',
    districtCode: '',
    wardCode: ''
  });
  formInitializing = true;

  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.locationForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );
  wardsList$ = combineLatest([
    this.locationQuery.select("wardsList"),
    this.locationForm.controls['districtCode'].valueChanges]).pipe(
    map(([wards, districtCode]) => {
      if (!districtCode) { return []; }
      return wards?.filter(ward => ward.district_code == districtCode);
    })
  );

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private auth: AuthenticateStore,
    private addressesService: AddressService,
    private shopService: ShopService,
    private eTopTradingService: ETopTradingService,
    private util: UtilService,
    private storage: StorageService,
    private locationService: LocationService,
    private locationQuery: LocationQuery
  ) {
    super();
  }

  async ngOnInit() {
    this.coupon = null;
    this.paymentMethod = 'vtpay';

    this.currentShop = this.auth.snapshot.shop;

    this.currentBalance = await this.shopService.calcShopBalance('shipping');
    this.reloadOrder();
    this.variantSelected = this.newOrder.lines[0];
    if (this.variantSelected.product_type == 'goods') {
      this.paymentMethod = 'bank';
    }
    if (this.storage.get(this.variantSelected.product_id)) {
      this.min_quantity = Number(this.storage.get(this.variantSelected.product_id));
    }
    if (this.variantSelected.meta_fields) {
      this.variantSelected.meta_fields = this.variantSelected.meta_fields.map(
        meta => {
          return {
            ...meta,
            _value: null
          };
        }
      );
    }
    this.getAddresses().then();

    this.locationForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.locationForm.patchValue({
            districtCode: '',
            wardCode: '',
          });
          this.defaultAddress.province_code = value;
        }
      });

    this.locationForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.locationForm.patchValue({
            wardCode: '',
          });
          this.defaultAddress.district_code = value;
        }
      });

    this.locationForm.controls['wardCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (!this.formInitializing) {
          this.defaultAddress.ward_code = value;
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.loadingOrder) {
      this.checkoutLoading = false;
    }
  }
  selectedPaymentMethod(type) {
    this.paymentMethod = type;
    this.codWarning = type == 'cod';
  }

  async checkout() {
    this.defaultAddress.province = this.locationQuery.getProvince(this.defaultAddress.province_code)?.name;
    this.defaultAddress.district = this.locationQuery.getDistrict(this.defaultAddress.district_code)?.name;
    this.defaultAddress.ward = this.locationQuery.getWard(this.defaultAddress.ward_code)?.name;
    this.reloadOrder();
    this.newOrder.payment_method = this.paymentMethod;

    this.newOrder.total_items = Number(this.newOrder.total_items);
    if (this.coupon) {
      this.newOrder = Object.assign(this.newOrder, {
        referral_meta: {
          referral_code: this.coupon
        }
      });
    } else {
      delete this.newOrder.referral_meta;
    }
    if (this.checkVat) {
      this.newOrder = {
        ...this.newOrder,
        fee_lines: [
          {
            amount: Number(this.VAT),
            name: 'VAT',
            type: 'tax'
          }
        ],
        order_note: 'CONG TY TNHH EB2B - 19134674513019 - Ngân hàng TMCP Kỹ Thương Việt Nam, Chi nhánh HCM'
      };
    } else {
      this.newOrder.order_note = 'CONG TY TNHH EB2B - 19134674513027 - Ngân hàng TMCP Ngoại Thương Việt Nam, Chi nhánh HCM';
    }
    if (this.min_quantity && this.newOrder.total_items < this.min_quantity) {
      return toastr.error(
        'Vui lòng đặt hàng với số lượng tối thiểu: ' + this.min_quantity
      );
    }
    this.checkoutLoading = true;
    this.orderSuccess.emit(this.newOrder);
  }

  back() {
    this.backOrderForm.emit();
  }

  reloadOrder() {
    this.newOrder.customer_address = this.defaultAddress;
    this.newOrder.billing_address = this.defaultAddress;
    this.newOrder.shipping_address = this.defaultAddress;
    this.newOrder.customer = {
      full_name: this.defaultAddress.full_name,
      phone: this.defaultAddress.phone
    };
  }

  cateDisplayMap() {
    return option => option;
  }

  async getAddresses() {
    try {
      let addresses = await this.addressesService.getAddresses();
      this.addressesFrom = addresses.filter(a => a.type == 'shipfrom');
      this.defaultAddress = this.addressesFrom.find(
        a => a.id == this.currentShop.ship_from_address_id
      );
      this.loading = true;

      this.formInitializing = true;

      this.locationForm.patchValue({
        provinceCode: this.defaultAddress.province_code,
        districtCode: this.defaultAddress.district_code,
        wardCode: this.defaultAddress.ward_code,
      });

      this.formInitializing = false;

    } catch (e) {
      debug.log(e);
    }
  }

  get totalPrice() {
    return (this.total =
      this.newOrder.total_items * this.newOrder.lines[0].retail_price);
  }

  get VAT() {
    return this.checkVat ? 0.1 * this.totalPrice : 0;
  }

  async onChangeReferralCode() {
    if (this.aff_code) {
      this.aff_code = this.aff_code.trim();
    }
    try {
      if (!this.aff_code || this.aff_code == '') {
        toastr.error('Vui lòng nhập mã giới thiệu!');
        this.checkReferralCode = false;
        delete this.newOrder.referral_meta;
        this.aff_code = null;
        this.onPromotion = false;
        this.totalCashback = 0;
        return;
      }
      let body = {
        product_id: this.newOrder.lines[0].product_id,
        referral_code: this.aff_code
      };
      let res = await this.eTopTradingService.CheckReferralCodeValid(body);

      if (!res.promotion) {
        res.promotion = new Promotion({
          ...res.promotion,
          amount: 0
        });
      }

      if (!res.referral_discount) {
        res.referral_discount = new Referral({
          ...res.referral_discount,
          amount: 0
        });
      }
      this.productReferralCode = {
        promotion:
          ((res.promotion.amount * this.newOrder.lines[0].retail_price) / 100) *
          this.newOrder.total_items,
        promotion_amount: res.promotion.amount,
        referral_discount:
          res.referral_discount.amount * this.newOrder.total_items
      };
      this.onPromotion = true;
      this.totalCashback =
        this.productReferralCode.promotion +
        this.productReferralCode.referral_discount;
      this.coupon = this.aff_code;
      this.aff_code = null;
      localStorage.setItem('promotion', this.productReferralCode.promotion);
      localStorage.setItem(
        'sellCashback',
        this.productReferralCode.referral_discount
      );
    } catch (e) {
      this.onPromotion = false;
      this.totalCashback = 0;
      this.aff_code = null;
      localStorage.removeItem('promotion');
      localStorage.removeItem('sellCashback');
      toastr.remove();
      toastr.error(e.message);
    }
  }

  removeCoupon() {
    this.coupon = null;
    this.onPromotion = false;
    this.totalCashback = 0;
    localStorage.removeItem('promotion');
    localStorage.removeItem('sellCashback');
  }

  onChangeVat() {
    this.checkVat = !this.checkVat;
  }

  checkQuantity() {
    if (
      !this.util.isNumber(this.newOrder.total_items) ||
      Number(this.newOrder.total_items) < 1
    ) {
      toastr.error('Vui lòng nhập số lượng sản phẩm hợp lệ!');
      return;
    }
  }

  formatMoney() {
    if (!this.newOrder.total_items) {
      this.format_quantity = false;
      return;
    }
    this.format_quantity = !this.format_quantity;
    if (!this.format_quantity && this.quantityInput) {
      this.quantityInput.focusInput();
    }
  }
}
