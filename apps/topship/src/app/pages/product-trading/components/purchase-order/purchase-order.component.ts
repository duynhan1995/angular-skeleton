import { Component, Input, OnInit } from '@angular/core';
import { MappingService } from '../../../../../../../core/src/services/mapping.service';
import { ETopTradingService } from 'apps/topship/src/services/etop-trading.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'topship-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {
  @Input() orders;

  constructor(
    private mappingService: MappingService,
    private eTopTradingService: ETopTradingService,
    private util: UtilService
  ) {}

  async ngOnInit() {}

  statusMap(status) {
    return this.mappingService.mapOrderStatus(status);
  }

  paymentStatusMap(order, status) {
    if (status != 'P') {
      return 'Chưa thanh toán';
    }
    return this.mappingService.mapPaymentStatus(status);
  }

  paymentMethodMap(method) {
    const methodMap = {
      cod: 'COD',
      bank: 'Chuyển khoản',
      vtpay: 'Viettel Pay'
    };
    return methodMap[method] || '';
  }


  async TradingPaymentOrder(order) {
    await this.eTopTradingService.TradingPaymentOrder(order);
  }
}
