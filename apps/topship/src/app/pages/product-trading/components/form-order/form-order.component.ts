import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { ETopTradingService } from 'apps/topship/src/services/etop-trading.service';
import { StorageService } from 'apps/core/src/services/storage.service';

@Component({
  selector: 'topship-form-order',
  templateUrl: './form-order.component.html',
  styleUrls: ['./form-order.component.scss']
})
export class FormOrderComponent implements OnInit {
  @Input() product;
  @Input() newOrder;
  @Output() orderFormChange = new EventEmitter();
  variant_id;
  attribute;
  attributes: any = [];
  priceVariant: any;
  variants = [];
  variantSelected: any;
  promotion: any;
  min_quantity;

  constructor(
    private util: UtilService,
    private eTopTradingService: ETopTradingService,
    private storage: StorageService
  ) {}

  async ngOnInit() {
    this.variants = this.product.variants;
    if (this.storage.get(this.product.id)) {
      this.min_quantity = Number(this.storage.get(this.product.id));
      debug.log('min_quantity', this.min_quantity);
      this.newOrder.total_items = this.min_quantity;
    }
    // this.product.product_type = 'services';
    this.priceVariant = this.variants[0].retail_price;
    if (this.product.variants[0].attributes.length > 0) {
      this.getAttributes();
      this.variant_id = this.attributes[0].variant_id;
      if (this.newOrder.lines.length > 0) {
        this.variant_id = this.newOrder.lines[0].variant_id;
      }
      this.variantPrice();
    }
    this.variantSelected = this.product.variants[0];
    this.promotion = await this.eTopTradingService.GetProductPromotion({
      product_id: this.product.id
    });
  }

  onSelectVariant(e) {
    this.variant_id = e.value;
    this.variantPrice();
  }

  getAttributes() {
    this.variants.map(v => {
      this.attributes.push({
        variant_id: v.id,
        value: v.id,
        name: v.attributes[0].value
      });
    });
    if (this.product.variants[0].attributes) {
      this.attribute = this.product.variants[0].attributes[0].name;
    }
  }
  variantPrice() {
    this.variantSelected = this.product.variants.find(
      v => v.id === this.variant_id
    );
    this.priceVariant = this.variantSelected.retail_price;
  }

  orderLineMap(variant) {
    let res: any;
    return (res = {
      variant_id: variant.id,
      product_name: this.product.name,
      product_id: this.product.id,
      product_type: this.product.product_type,
      quantity: 1,
      list_price: variant.list_price,
      retail_price: variant.retail_price,
      payment_price: variant.retail_price,
      image_url: variant.image_urls[0] || this.product.image_urls[0],
      attributes: variant.attributes,
      meta_fields: this.product.meta_fields
    });
  }

  gotoCheckout() {
    if (this.variantSelected.quantity < 1) {
      return toastr.error('Sản phẩm tạm thời hết hàng!');
    }
    if (
      !this.util.isNumber(this.newOrder.total_items) ||
      Number(this.newOrder.total_items) < 1
    ) {
      toastr.error('Vui lòng nhập số lượng sản phẩm hợp lệ!');
      return;
    }
    if (this.storage.get(this.product.id)) {
      debug.log('min_quantity', this.min_quantity);
      if (this.newOrder.total_items < this.min_quantity) {
        return toastr.error('Vui lòng đặt hàng với số lượng tối thiểu: ' + this.min_quantity);
      }
    }
    this.newOrder.lines[0] = this.orderLineMap(this.variantSelected);
    this.orderFormChange.emit(this.newOrder);
  }
}
