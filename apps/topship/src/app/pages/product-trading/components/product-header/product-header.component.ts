import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: 'topship-product-header',
  templateUrl: './product-header.component.html',
  styleUrls: ['./product-header.component.scss']
})
export class ProductHeaderComponent implements OnInit {
  @Input() name;
  constructor(
    private router: Router,
    private util: UtilService,
  ) { }

  ngOnInit() { }

  gotoShopping() {
    let slug = this.util.getSlug();
    this.router.navigateByUrl(`/s/${slug}/etop-trading`);
  }
}
