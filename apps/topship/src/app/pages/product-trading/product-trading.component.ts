import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {StringHandler} from "@etop/utils";
import { ETopTradingService } from 'apps/topship/src/services/etop-trading.service';
import { HeaderControllerService } from '../../../../../core/src/components/header/header-controller.service';
import { SideSliderComponent } from '../../../../../../libs/shared/components/side-slider/side-slider.component';
import { CmsService } from 'apps/core/src/services/cms.service';
import { PageBaseComponent } from '@etop/web/core/base/page.base-component';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'topship-product-trading',
  templateUrl: './product-trading.component.html',
  styleUrls: ['./product-trading.component.scss']
})
export class ProductTradingComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('slider', { static: true }) slider: SideSliderComponent;
  products;
  promotions;
  orders: any = [];
  banners: any;
  bannerLeft: any;
  bannerRight: any;
  sortList: any = [];
  orProducts: any = [];
  currentTab = 'goods';

  goodsProduct: any;
  servicesProduct: any;

  constructor(
    private eTopTradingService: ETopTradingService,
    private headerController: HeaderControllerService,
    private cms: CmsService,
    private appService: AppService
  ) {
    super();
  }

  private _onSelectModeChanged(value) {
    this.slider.toggleLiteMode(value);
  }

  async ngOnInit() {
    await this.getBanners();
    this.bannerLeft = this.banners[30];
    this.bannerRight = this.banners[31];
    this.getProducts(1, 1000);
    this.headerController.setActions([
      {
        title: 'Đơn hàng đã mua',
        cssClass: 'btn btn-primary btn-outline',
        onClick: () => this.getMyOrder()
      }
    ]);
    if (this.appService.appID != 'etop.vn' && this.appService.links.trading_guide != '') {
      this.headerController.setHeaderButtons([
        {
          onClick: () => {
            window.open(this.appService.links.trading_guide, '_blank');
          },
          title: 'Hướng dẫn mua hàng',
          cssClass: 'btn-gray btn-outline',
          titleClass: 'text-primary',
          icon: 'fas fa-info-circle text-primary mr-2'
        }
      ]);
    } else if (this.banners[37].is_active == '1' && this.appService.appID == 'etop.vn') {
      this.headerController.setHeaderButtons([
        {
          onClick: () => {
            if (this.banners[37].url) {
              window.open(this.banners[37].url, '_blank');
            }
          },
          title: 'Hướng dẫn mua hàng',
          cssClass: 'btn-gray btn-outline',
          titleClass: 'text-primary',
          icon: 'fas fa-info-circle text-primary mr-2'
        }
      ]);
    }
    if (this.appService.appID == 'etop.vn') {
      this.headerController.setTabs([
        {
          title: 'Hàng hóa',
          name: 'goods',
          active: this.currentTab == 'goods',
          onClick: () => {
            this.changeTab('goods');
          }
        },
        {
          title: 'Dịch vụ',
          name: 'services',
          active: this.currentTab == 'services',
          onClick: () => {
            this.changeTab('services');
          }
        }
      ]);
    }
  }

  ngOnDestroy(): void {
    this.headerController.clearActions();
    this.headerController.clearHeaderButtons();
    this.headerController.setTabs([]);
  }

  getMyOrder() {
    this._onSelectModeChanged(true);
    this.loadOrder(1, 100).then();
  }
  async loadOrder(page, perpage) {
    try {
      this.orders = await this.eTopTradingService.loadTradingOrders(
        (page - 1) * perpage,
        perpage
      );
    } catch (e) {}
  }

  async getProducts(page, perpage) {
    this.products = await this.eTopTradingService.loadTradingProducts(
      (page - 1) * perpage,
      perpage
    );
    this.sortProducts();
  }

  async getBanners() {
    try {
      const res = await fetch('https://cms-setting.etop.vn/api/get-banner').then(r =>
        r.json()
      );
      this.banners = res.data;
      return this.banners;
    } catch (e) {
      debug.log(e);
    }
  }

  directLink(url) {
    window.open(url);
  }

  sortProducts() {
    const desc = StringHandler.trimHtml(this.banners[36].description);
    const json_desc = JSON.parse(desc);
    const _products = [...this.products];
    for (const p of json_desc) {
      let _product = _products.find(
        item => item.product.id == p.product_id
      );
      if (_product) {
        this.sortList.push(_product);
      }
    }
    if (this.sortList && this.sortList.length > 0) {
      const idx = this.sortList.map(x => x.product.id);
      for (const p of this.products) {
        if (!idx.includes(p.product.id)) {
          this.orProducts.push(p)
        }
      }
      this.products = this.sortList.concat(this.orProducts);
      this.goodsProduct = this.products.filter(
        p => p.product.product_type == 'goods' && p.product.variants[0]?.quantity > 0
      );
      this.servicesProduct = this.products.filter(
        p => p.product.product_type != 'goods' && p.product.variants[0]?.quantity > 0
      );
    }
  }

  changeTab(tab) {
    this.currentTab = tab;
  }
}
