import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateOrderComponent } from './create-order.component';
import { EtopMaterialModule, EtopPipesModule, MaterialModule } from '@etop/shared';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { CreateProductLiteComponent } from './components/create-product-lite/create-product-lite.component';
import { MatDialogModule } from '@angular/material/dialog';
import { VariantSelectComponent } from './components/variant-select/variant-select.component';
import { ShipmentServiceOptionsComponent } from './components/shipment-service-options/shipment-service-options.component';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';



@NgModule({
  declarations: [CreateOrderComponent, CreateProductLiteComponent, VariantSelectComponent, ShipmentServiceOptionsComponent],
  exports: [CreateOrderComponent],
  imports: [
    CommonModule,
    MaterialModule,
    EtopMaterialModule,
    SharedModule,
    FormsModule,
    MatDialogModule,
    EtopPipesModule,
  ],
  providers: [ShopSettingsService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreateOrderModule { }
