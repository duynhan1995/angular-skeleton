import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogTagData {
  product_name: string;
  product_price: number;
}

@Component({
  selector: 'topship-create-product-lite',
  templateUrl: './create-product-lite.component.html',
  styleUrls: ['./create-product-lite.component.scss']
})
export class CreateProductLiteComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogTagData
  ) {}

  ngOnInit(): void {
    debug.log('data', this.data)
  }

}
