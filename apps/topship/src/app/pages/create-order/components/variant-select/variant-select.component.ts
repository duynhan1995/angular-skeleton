import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '@etop/models';

export interface DialogTagData {
  product: Product
}

@Component({
  selector: 'topship-variant-select',
  templateUrl: './variant-select.component.html',
  styleUrls: ['./variant-select.component.scss']
})
export class VariantSelectComponent implements OnInit {
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogTagData,
    private dialogRef: MatDialogRef<VariantSelectComponent>,
  ) { }

  ngOnInit(): void {
  }

  variantSelected(variant) {
    this.dialogRef.close(variant);
  }

}
