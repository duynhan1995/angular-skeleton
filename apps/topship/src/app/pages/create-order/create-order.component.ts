import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {AddressApi, FulfillmentApi, FulfillmentAPI, OrderApi, OrderAPI} from '@etop/api';
import {AuthenticateStore, BaseComponent} from '@etop/core';
import {ShopSettingsService} from '@etop/features/services/shop-settings.service';
import {
  CustomerAddress,
  District,
  FilterOperator,
  Product,
  Province,
  TRY_ON_OPTIONS,
  Variant,
  Ward
} from '@etop/models';
import {ShopSettings} from '@etop/models/ShopSettings';
import {LocationQuery, LocationService} from '@etop/state/location';
import {CustomerService} from 'apps/topship/src/services/customer.service';
import {ProductService} from 'apps/topship/src/services/product.service';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {ConfirmOrderQuery, ConfirmOrderService} from '../../state/confirm-order';
import {CreateProductLiteComponent} from './components/create-product-lite/create-product-lite.component';
import {ShipmentServiceOptionsComponent} from './components/shipment-service-options/shipment-service-options.component';
import {VariantSelectComponent} from './components/variant-select/variant-select.component';

@Component({
  selector: 'topship-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('shipmentServiceOptions', {static: false}) shipmentServiceOptions: ShipmentServiceOptionsComponent;
  orderForm = this.fb.group({
    customer: this.fb.group({
      full_name: ['', Validators.required],
      phone: ['', Validators.required],
      province_code: ['', Validators.required],
      address1: ['', Validators.required],
      district_code: ['', Validators.required],
      ward_code: ['', Validators.required]
    }),
    lines: this.fb.array([]),
    order_discount: null,
    fee_lines: this.fb.array([
      this.fb.group({
        type: 'other',
        name: 'Phụ thu',
        amount: null
      })
    ]),
    basket_value: 0,
    total_items: 0,
    total_discount: 0,
    total_fee: 0,
    total_amount: 0,
    product_name: '',
    product_price: ''
  });

  fulfillmentForm = this.fb.group({
    chargeable_weight: [250, Validators.required],
    shipping_service_fee: '',
    cod_amount: ['', Validators.required],
    try_on: 'open',
    try_on_shipping: null,
    shipping_note: '',
    include_insurance: false,
    shipping_payment_type: null
  });
  shipping_note = '';

  selected_service_unique_id;

  provincesList: Province[] = [
    {id: null, code: null, name: 'Chưa chọn tỉnh thành'}
  ];
  districtsList: District[] = [
    {id: null, code: null, name: 'Chưa chọn tỉnh thành'}
  ];
  wardsList: Ward[] = [{id: null, code: null, name: 'Chưa chọn quận huyện'}];

  loadData = false;
  productName;

  TRY_ON = [
    {
      value: true,
      name: 'Có'
    },
    {
      value: false,
      name: 'Không'
    }
  ];

  products: Product[];
  services = [];
  serviceSelect;
  customer_id;
  customerSelect;
  productSelected;
  showProductSearch = false;
  loading = false;

  customers: CustomerAddress[];
  settings: ShopSettings;

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.id || null;

  constructor(
    private locationQuery: LocationQuery,
    private locationService: LocationService,
    private auth: AuthenticateStore,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private productService: ProductService,
    private cdRef: ChangeDetectorRef,
    private customerService: CustomerService,
    private addressApi: AddressApi,
    private orderApi: OrderApi,
    private fulfillmentApi: FulfillmentApi,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderQuery: ConfirmOrderQuery,
    private shopSettingsService: ShopSettingsService,
  ) {
    super()
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS;
  }

  get tryOn() {
    return this.TRY_ON;
  }

  get customerForm() {
    return this.orderForm?.controls.customer as FormGroup;
  }

  get feeLines() {
    return this.orderForm?.controls['fee_lines'] as FormArray;
  }

  get lines() {
    return this.orderForm?.controls['lines'] as FormArray;
  }

  async ngOnInit() {
    this.loadData = false;
    this.provincesList = this.locationQuery.getValue().provincesList;
    this.loadData = true;
    this.orderForm.controls['product_name'].valueChanges.pipe(debounceTime(500)).subscribe(product_name => {
      this.searchProduct(product_name);
      if (this.productSelected && this.productSelected?.product_name != product_name) {
        this.lines.at(0).patchValue({
          product_name,
          variant_id: null
        })
      }
    });
    this.orderForm.controls['product_price'].valueChanges.pipe(debounceTime(500)).subscribe(async product_price => {
      if (this.productSelected && this.productSelected?.product_price != product_price) {
        this.lines.at(0).patchValue({
          payment_price: product_price,
          list_price: product_price,
          retail_price: product_price,
          total_price: product_price,
          variant_id: null
        })
        this.resetLinesValue();
        await this.getShippingServices();
      }
    });
    this.customerForm.controls.province_code.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      this.onProvinceSelect(value)
    })
    this.customerForm.controls.district_code.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
      this.onDistrictSelect(value)
    })
    this.customerForm.controls.phone.valueChanges.pipe(distinctUntilChanged(), debounceTime(500)).subscribe(phone => {
      this.searchCustomer(phone)
    })
    this.customerForm.controls.ward_code.valueChanges.pipe(distinctUntilChanged(), debounceTime(200)).subscribe(async value => {
      if (value) {
        await this.getShippingServices();
      } else {
        this.services = [];
        this.selected_service_unique_id = '';
        this.serviceSelect = null;
      }
    })
    this.fulfillmentForm.valueChanges.pipe(distinctUntilChanged(), debounceTime(500)).subscribe(async value => {
      await this.getShippingServices();
    })

    this.lines.valueChanges.pipe(distinctUntilChanged(), debounceTime(200)).subscribe(_ => {
      this.resetLinesValue()
    })

    this.orderForm.controls.order_discount.valueChanges.pipe(distinctUntilChanged(), debounceTime(200)).subscribe(_ => {
      this.resetLinesValue()
    })

    this.orderForm.controls.fee_lines.valueChanges.pipe(distinctUntilChanged(), debounceTime(200)).subscribe(_ => {
      this.resetLinesValue()
    })

    this.confirmOrderService.forceUpdateShippingService$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getShippingServices();
      });

  }

  async setInitialValue() {
    this.settings = await this.shopSettingsService.getSetting();
    this.fulfillmentForm.get('include_insurance').setValue(false);
    this.fulfillmentForm.get('try_on').setValue(this.settings?.try_on || 'open')
    this.fulfillmentForm.get('chargeable_weight').setValue(this.settings?.weight || 250);
    this.shipping_note = this.settings?.shipping_note;
  }

  ngAfterViewInit() {
    this.setInitialValue();
    this.cdRef.detectChanges();
  }

  addressDisplay(address: CustomerAddress) {
    const {address1, ward, district, province} = address;
    return address1 + ', ' + ward + ', ' + district + ', ' + province;
  }

  async searchProduct(key) {
    this.showProductSearch = false;
    if (!key) {
      this.products = [];
      return;
    }
    let res = await this.productService.getProducts(0, 20, [{
      name: 'name',
      op: FilterOperator.contains,
      value: key
    }]);
    this.products = res?.products;
    if (this.products?.length) {
      this.showProductSearch = true;
    }
  }

  async searchCustomer(phone) {
    if (!phone) {
      this.customers = [];
      return;
    }
    this.customers = await this.addressApi.getCustomerAddress({
      filter: {
        phone,
      }
    });
  }

  onProvinceSelect(province_code) {
    this.districtsList = this.locationService.filterDistrictsByProvince(province_code);
    this.wardsList = [{id: null, code: null, name: 'Chưa chọn quận huyện'}]
  }

  onDistrictSelect(district_code) {
    this.wardsList = this.locationService.filterWardsByDistrict(district_code)
  }

  selectProduct(product: Product) {
    if (product?.variants?.length > 1) {
      this.openSelectProductVariants(product);
    } else {
      this.addVariantLines(product?.variants[0]);
    }
    this.orderForm.patchValue({
      product_name: product.name,
      product_price: product.variants[0].retail_price
    }, {emitEvent: false})
    this.showProductSearch = false;
  }

  addVariantLines(variant: Variant) {
    if (variant) {
      this.lines.removeAt(0);
      this.lines.push(this.fb.group({
        variant_id: variant.id,
        product_name: variant.product_name,
        payment_price: variant.retail_price,
        list_price: variant.retail_price,
        retail_price: variant.retail_price,
        total_price: variant.retail_price,
        quantity: 1,
        code: variant.code
      }))
      this.productSelected = variant;
      this.cdRef.detectChanges();
    } else {
      return;
    }
  }

  removeVariantLines(index) {
    this.lines.removeAt(index);
  }

  openSelectProductVariants(product: Product) {
    const dialogRef = this.dialog.open(VariantSelectComponent, {
      width: '500px',
      backdropClass: '',
      data: {product}
    });

    dialogRef.afterClosed().subscribe(variant => {
      this.addVariantLines(variant);
    });
  }

  searchBlur(e) {
    setTimeout(_ => {
      this.showProductSearch = false;
      this.customers = [];
    }, 200)
  }

  async customerSelected(customer) {
    const {full_name, phone, address1, province_code, district_code, ward_code} = customer;
    this.customerForm.patchValue({
      full_name, phone, address1,
      province_code
    }, {emitEvent: false});
    this.onProvinceSelect(province_code)
    this.customerForm.patchValue({
      district_code
    }, {emitEvent: false})
    this.onDistrictSelect(district_code)
    this.customerForm.patchValue({
      ward_code
    }, {emitEvent: true})
    this.customerSelect = customer;
    this.getShippingServices();
    this.cdRef.detectChanges();
  }

  async createProduct() {
    this.dialog.open(CreateProductLiteComponent, {
      width: '500px',
      data: {product_name: this.orderForm.controls.product_name.value}
    });
  }

  validatorShippingService(force = false) {
    if (this.orderForm.get('customer').invalid) {
      if (force) toastr.error(`Vui lòng điền đủ thông tin người nhận`);
      return false;
    }
    return true;
  }

  validatorFfm() {
    const {
      chargeable_weight,
      cod_amount,
    } = this.fulfillmentForm.value;
    if (cod_amount?.length == 0) {
      toastr.error(`Vui lòng nhập tiền thu hộ`);
      return false
    }
    if (cod_amount < 0 || (0 < cod_amount && cod_amount < 5000)) {
      toastr.error(`Tiền thu hộ (COD) phải bằng 0 hoặc từ 5.000đ trở lên`);
      this.services = [];
      this.selected_service_unique_id = '';
      this.serviceSelect = null;
      return false;
    }

    if (chargeable_weight <= 0) {
      toastr.error(`Khối lượng sản phẩm phải lớn hơn 0`);
      this.services = [];
      this.selected_service_unique_id = '';
      this.serviceSelect = null;
      return false;
    }
    if (!this.selected_service_unique_id) {
      toastr.error(`Vui lòng chọn gói dịch vụ`);
      return false;
    }
    return true;
  }

  async getShippingServices() {
    if (!this.validatorShippingService()) {
      return;
    }
    const from_address = this.confirmOrderQuery.getValue().fulfillment.pickup_address;
    this.selected_service_unique_id = '';
    this.serviceSelect = null;
    const {customer, basket_value, product_price} = this.orderForm.getRawValue();
    const {chargeable_weight, include_insurance, cod_amount} = this.fulfillmentForm.getRawValue();

    const data: any = {
      chargeable_weight,
      include_insurance: Boolean(include_insurance),
      basket_value: Number(basket_value) || Number(product_price) || Number(cod_amount),
      total_cod_amount: Number(cod_amount),
      from_province_code: from_address?.province_code,
      from_district_code: from_address?.district_code,
      from_ward_code: from_address?.ward_code,
      to_province_code: customer?.province_code,
      to_district_code: customer?.district_code,
      to_ward_code: customer?.ward_code
    }

    await this.shipmentServiceOptions.getServices(data)
  }

  selectShippingService(e) {
    if (e) {
      this.serviceSelect = e;
      this.selected_service_unique_id = e?.unique_id;
      this.cdRef.detectChanges();
    }
  }

  resetLinesValue() {
    const {lines, fee_lines, order_discount} = this.orderForm.value;
    const basket_value = lines && lines.reduce((a, b) => a + b.payment_price * b.quantity, 0) || 0;
    const total_items = lines && lines.reduce((a, b) => a + b.quantity, 0) || 0;
    const total_discount = Number(order_discount) || 0;
    const total_fee = fee_lines && fee_lines.reduce((a, b) => a + Number(b.amount), 0) || 0;
    const total_amount = basket_value - total_discount + total_fee;
    this.orderForm.patchValue({
      basket_value,
      total_items,
      total_discount,
      total_fee,
      total_amount,
    }, {emitEvent: false})
  }

  async confirm() {
    const from_address = this.confirmOrderQuery.getValue().fulfillment.pickup_address;
    if (!from_address) {
      toastr.error(`Chưa có địa chỉ lấy hàng`);
      return false;
    }
    if (!this.validatorShippingService(true)) {
      return;
    }
    if (!this.validatorFfm()) {
      return;
    }
    this.loading = true;
    try {
      const {
        customer,
        fee_lines,
        lines,
        basket_value,
        total_items,
        total_fee,
        total_discount,
        order_discount
      } = this.orderForm.getRawValue();
      const {
        chargeable_weight,
        include_insurance,
        cod_amount,
        try_on,
      } = this.fulfillmentForm.getRawValue();
      let _customer, customerAdrress;
      if (!this.customerSelect?.customer_id) {
        _customer = await this.customerService.createCustomer(customer);
        customerAdrress = await this.customerService.createCustomerAddress({
          ...customer,
          customer_id: _customer.id
        })
      } else {
        let {full_name, phone, address1, ward_code} = this.customerSelect;
        if (
          full_name != customer.full_name
          || phone != customer.phone
          || address1 != customer.address1
          || ward_code != customer.ward_code
        ) {
          customerAdrress = await this.customerService.createCustomerAddress({
            ...customer,
            customer_id: this.customerSelect.customer_id
          })
        }
      }

      let body: OrderAPI.CreateOrderRequest = {
        source: 'etop_pos',
        payment_method: 'cod',
        customer_id: _customer?.id || this.customerSelect?.customer_id,
        total_items,
        total_amount: Number(basket_value) || Number(cod_amount),
        total_fee,
        total_discount,
        basket_value: Number(basket_value) || Number(cod_amount),
        order_discount,
        fee_lines,
        lines,
      };

      const productName = this.orderForm.get('product_name').value;
      const productPrice = this.orderForm.get('product_price').value;
      if (!body?.lines?.length) {
        this.resetLinesValue();
        body.lines = [{
          product_name: productName || `Sản phẩm từ shop ${this.auth.snapshot.shop?.name}`,
          retail_price: Number(productPrice) || Number(cod_amount),
          payment_price: Number(productPrice) || Number(cod_amount),
          total_discount: 0,
          quantity: 1
        }];
        body.basket_value = Number(productPrice) || Number(cod_amount);
        body.total_amount = Number(productPrice) || Number(cod_amount);
        body.total_items = 1;
      }
      if (productName && productPrice) {
        await this.checkCreateProduct(body.lines[0]);
      }

      let _order = await this.orderApi.createOrder(body);
      await this.orderApi.confirmOrder(_order.id);
      const ffm = this.confirmOrderQuery.getValue().fulfillment;
      const ffmBody: FulfillmentAPI.CreateShipmentFulfillmentRequest = {
        pickup_address: ffm.pickup_address,
        shipping_address: customerAdrress || this.customerSelect,
        chargeable_weight: Number(chargeable_weight),
        cod_amount: Number(cod_amount),
        shipping_service_code: this.serviceSelect?.code,
        shipping_service_fee: Number(this.serviceSelect?.fee),
        shipping_service_name: this.serviceSelect?.name,
        connection_id: this.serviceSelect?.connection_info?.id,
        try_on,
        shipping_note: this.shipping_note,
        include_insurance: Boolean(include_insurance),
        order_id: _order.id,
        shipping_type: 'shipment',
      };
      if (this.settings?.return_address) {
        ffmBody.return_address = this.settings.return_address
      }
      await this.fulfillmentApi.createShipmentFulfillment(ffmBody);
      toastr.success('Tạo đơn hàng thành công');
      this.loading = false;
      this.confirmOrderService.forceReload$.next();
    } catch (e) {
      this.loading = false;
      debug.log('ERROR in confirm', e)
      toastr.error(e?.message);
    }
  }

  async checkCreateProduct(line) {
    if (!line?.variant_id) {
      const new_product = new Product({
        name: line.product_name,
        code: Date.now().toString(),
      });
      const res = await this.productService.createProduct(new_product);
      const newVariant = new Variant({
        product_id: res.id,
        name: res.name,
        retail_price: line.retail_price,
        is_available: true,
      });
      await this.productService.createVariant(newVariant);
    }
  }

}
