import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'apps/shared/src/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from 'apps/topship/src/app/pages/settings/settings.component';
import { ChangePasswordModalComponent } from './components/change-password-modal/change-password-modal.component';
import { ModalControllerModule } from 'apps/core/src/components/modal-controller/modal-controller.module';
import { EditCategoryModalComponent } from './components/edit-category-modal/edit-category-modal.component';
import { EditCollectionModalComponent } from './components/edit-collection-modal/edit-collection-modal.component';
import { ProductSrcCategoryLineComponent } from './components/product-src-category-line/product-src-category-line.component';
import { StaffManagementComponent } from './staff-management/staff-management.component';
import { AuthorizationApi } from '@etop/api';
import { StaffManagementRowComponent } from './components/staff-management-row/staff-management-row.component';
import { AddStaffModalComponent } from 'apps/topship/src/app/pages/settings/components/add-staff-modal/add-staff-modal.component';
import { NotMyShopsManagamentComponent } from './components/not-my-shops-managament/not-my-shops-managament.component';
import { AccountInvitationRowComponent } from 'apps/topship/src/app/pages/settings/components/account-invitation-row/account-invitation-row.component';
import { MyShopsManagementComponent } from './components/my-shops-management/my-shops-management.component';
import { UserInvitationRowComponent } from './components/user-invitation-row/user-invitation-row.component';
import { UpdateStaffModalComponent } from './components/update-staff-modal/update-staff-modal.component';
import { AuthenticateModule } from '@etop/core';
import { ShippingInfoComponent } from 'apps/topship/src/app/pages/settings/shipping-info/shipping-info.component';
import { TransactionInfoComponent } from 'apps/topship/src/app/pages/settings/transaction-info/transaction-info.component';
import { CarrierConnectModalComponent } from './components/carrier-connect-modal/carrier-connect-modal.component';
import { IntegratedInfoComponent } from './integrated-info/integrated-info.component';
import { UserInfoComponent } from 'apps/topship/src/app/pages/settings/user-info/user-info.component';
import { DropdownActionsModule } from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import { FromAddressComponent } from './components/from-address/from-address.component';
import { CarrierConnectComponent } from './components/carrier-connect/carrier-connect.component';
import { ShopsManagementRowComponent } from 'apps/topship/src/app/pages/settings/components/not-my-shops-row/shops-management-row.component';
import { CarrierConnectRowComponent } from './components/carrier-connect-row/carrier-connect-row.component';
import { ShopInfoComponent } from 'apps/topship/src/app/pages/settings/shop-info/shop-info.component';
import { AccountsInfoComponent } from './accounts-info/accounts-info.component';
import { ShopPipesModule } from 'apps/topship/src/app/pipes/shop-pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateUserInfoModalComponent } from 'apps/topship/src/app/pages/settings/components/update-user-info-modal/update-user-info-modal.component';
import { TopshipCommonUsecase } from 'apps/topship/src/app/usecases/topship-common.usecase.service';
import {EtopMaterialModule, EtopPipesModule, MaterialModule} from '@etop/shared';
import { ConnectCarrierUsingPhoneModalComponent } from 'apps/topship/src/app/pages/settings/components/connect-carrier-using-phone-modal/connect-carrier-using-phone-modal.component';
import {ShipnowVerifyWarningModule} from "apps/topship/src/app/components/shipnow-verify-warning/shipnow-verify-warning.module";
import { CarrierInfoComponent } from './carrier-info/carrier-info.component';
import { ShopSettingsComponent } from './shop-settings/shop-settings.component';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { ReturnAddressModalComponent } from './components/return-address-modal/return-address-modal.component';
import { CreateShopModalComponent } from './components/create-shop-modal/create-shop-modal.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'shop',
        component: ShopInfoComponent
      },
      {
        path: 'shipping_info',
        component: ShippingInfoComponent
      },
      {
        path: 'carrier',
        component: CarrierInfoComponent
      },
      {
        path: 'transaction',
        component: TransactionInfoComponent
      },
      {
        path: 'staff',
        component: StaffManagementComponent
      },
      {
        path: 'integrated',
        component: IntegratedInfoComponent
      },
      {
        path: 'user',
        component: UserInfoComponent
      },
      {
        path: 'accounts',
        component: AccountsInfoComponent
      },
      {
        path: 'shop-setting',
        component: ShopSettingsComponent
      },
      {
        path: '**', redirectTo: 'shop'
      }
    ]
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        ModalControllerModule,
        AuthenticateModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        DropdownActionsModule,
        ShopPipesModule,
        NgbModule,
        EtopPipesModule,
        EtopMaterialModule,
        MaterialModule,
        ShipnowVerifyWarningModule,
    ],
  exports: [],
  entryComponents: [
    ChangePasswordModalComponent,
    EditCategoryModalComponent,
    EditCollectionModalComponent,
    AddStaffModalComponent,
    UpdateStaffModalComponent,
    CarrierConnectModalComponent,
    UpdateUserInfoModalComponent,
    ConnectCarrierUsingPhoneModalComponent,
  ],
  declarations: [
    SettingsComponent,
    ChangePasswordModalComponent,
    EditCategoryModalComponent,
    EditCollectionModalComponent,
    ProductSrcCategoryLineComponent,
    StaffManagementComponent,
    StaffManagementRowComponent,
    AddStaffModalComponent,
    NotMyShopsManagamentComponent,
    AccountInvitationRowComponent,
    MyShopsManagementComponent,
    UserInvitationRowComponent,
    UpdateStaffModalComponent,
    ShopInfoComponent,
    ShippingInfoComponent,
    TransactionInfoComponent,
    CarrierConnectModalComponent,
    IntegratedInfoComponent,
    UserInfoComponent,
    FromAddressComponent,
    CarrierConnectComponent,
    ShopsManagementRowComponent,
    CarrierConnectRowComponent,
    AccountsInfoComponent,
    UpdateUserInfoModalComponent,
    ConnectCarrierUsingPhoneModalComponent,
    CarrierInfoComponent,
    ShopSettingsComponent,
    ReturnAddressModalComponent,
    CreateShopModalComponent
  ],
  providers: [
    AuthorizationApi,
    TopshipCommonUsecase,
    ShopSettingsService,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {}
