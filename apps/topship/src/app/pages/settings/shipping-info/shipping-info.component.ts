import { Component, OnInit } from '@angular/core';
import { Address } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { AuthenticateStore } from '@etop/core';
import { ShopService } from '@etop/features';
import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import { FromAddressModalComponent } from 'libs/shared/components/from-address-modal/from-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { Router } from '@angular/router';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { OrderService } from 'apps/topship/src/services/order.service';
import { ExtendedAccount } from 'libs/models/Account';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'topship-delivery-info',
  templateUrl: './shipping-info.component.html',
  styleUrls: ['./shipping-info.component.scss']
})
export class ShippingInfoComponent implements OnInit {
  addressesFrom: Array<Address> = [];
  addressesTo: Array<Address> = [];
  addressesLoading = true;
  currentShop = new ExtendedAccount({});

  constructor(
    private addressesService: AddressService,
    private auth: AuthenticateStore,
    private shopService: ShopService,
    private gaService: GoogleAnalyticsService,
    private modalController: ModalController,
    private orderService: OrderService
  ) {}

  async ngOnInit() {
    let auth = this.auth.snapshot;
    this.currentShop = auth.shop;
    await this.getAddresses();
  }

  async getAddresses() {
    try {
      let addresses = await this.addressesService.getAddresses();
      this.addressesTo = addresses.filter(a => a.type == 'shipto');
      this.addressesFrom = addresses.filter(a => a.type == 'shipfrom');
      this.addressesLoading = false;
    } catch (e) {
      debug.log(e);
    }
  }

  get tryOnOptions() {
    return this.orderService.tryOnOptions;
  }

  async updateTryOn() {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_DELIVER_SETTING,
      USER_BEHAVIOR.LABEL_UPDATE_GHN_NOTE_CODE
    );

    try {
      await this.shopService.updateShopField({ try_on: this.currentShop.try_on });
      this.auth.updateTryOn(this.currentShop.try_on);
      toastr.success('Cập nhật thành công!');
    } catch (e) {
      toastr.error('Cập nhật thất bại: ' + e.message);
    }
  }

  openAddAddressForm(type) {
    this.gaService.sendUserBehavior(
      USER_BEHAVIOR.ACTION_DELIVER_SETTING,
      USER_BEHAVIOR.LABEL_ADD_FROM_ADDRESS
    );

    const modal = this.modalController.create({
      component: FromAddressModalComponent,
      componentProps: {
        address: new Address({}),
        type: type,
        title: 'Thêm địa chỉ'
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then(async ({ address }) => {
      if (!address) {
        return;
      }
      if (this.addressesFrom.length == 0) {
        await this.shopService.setDefaultAddress(address.id, 'shipfrom');
        this.auth.setDefaultAddress(address.id, 'shipfrom');
      }
      this.addressesFrom.push(address);
    });
  }

  onRemoveAddress(address_id) {
    this.addressesFrom = this.addressesFrom.filter(a => a.id != address_id);
  }
}


