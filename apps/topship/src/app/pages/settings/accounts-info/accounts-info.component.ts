import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { Account, ExtendedAccount } from 'libs/models/Account';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'topship-accounts-info',
  templateUrl: './accounts-info.component.html',
  styleUrls: ['./accounts-info.component.scss']
})
export class AccountsInfoComponent implements OnInit {
  accounts: Account[] = [];
  shop: ExtendedAccount;

  constructor(
    private auth: AuthenticateStore,
    private appService: AppService
  ) { }

  ngOnInit() {
    this.auth.authenticatedData$.subscribe(info => {
      this.accounts = info.accounts;
      this.shop = info.shop || new ExtendedAccount({});
    });
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn'
  }

}
