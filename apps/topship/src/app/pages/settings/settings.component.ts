import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { UtilService } from 'apps/core/src/services/util.service';
import { Account, CompanyInfo, ExtendedAccount } from 'libs/models/Account';
import { User } from 'libs/models';
import { BankAccount } from 'libs/models/Bank';
import { SettingMenu, SettingStore } from 'apps/core/src/stores/setting.store';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppService } from '@etop/web/core/app.service';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { ModalController } from "apps/core/src/components/modal-controller/modal-controller.service";
import { CreateShopModalComponent } from './components/create-shop-modal/create-shop-modal.component';

@Component({
  selector: 'topship-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SettingsComponent extends BaseComponent implements OnInit {
  accounts: Account[] = [];
  user: User;
  currentShop = new ExtendedAccount({
    bank_account: {
      account_name: '',
      account_number: ''
    },
    address: {
      address1: '',
      province_code: '',
      district_code: '',
      ward_code: ''
    },
    company_info: {
      name: '',
      tax_code: '',
      address: '',
      website: '',
      legal_representative: {
        name: '',
        position: '',
        phone: '',
        email: ''
      }
    }
  });

  current_menu: SettingMenu;
  menus = [
    {
      title: 'Thông tin cửa hàng',
      slug: 'shop',
      permissions: ['shop/settings/shop_info:view','shop/settings/company_info:view']
    },
    {
      title: 'Địa chỉ lấy hàng',
      slug: 'shipping_info',
      permissions: ['shop/settings/shipping_setting:view']
    },
    {
      title: 'Kết nối nhà vận chuyển',
      slug: 'carrier',
      permissions: ['shop/settings/shipping_setting:view']
    },
    {
      title: 'Đối soát TOPSHIP',
      slug: 'transaction',
      permissions: ['shop/settings/wallet_balance:view','shop/settings/bank_info:view','shop/settings/payment_schedule:view']
    },
    {
      title: 'Nhân viên',
      slug: 'staff',
      permissions: ['relationship/relationship:view','relationship/invitation:view'],
    },
    {
      title: 'Thông số mặc định',
      slug: 'shop-setting',
      permissions: [],
    },
  ];

  accountMenus = [
    {
      title: 'Thông tin tài khoản',
      slug: 'user'
    },
    {
      title: 'Danh sách cửa hàng',
      slug: 'accounts'
    }
  ];
  private modal: any;
  constructor(
    private router: Router,
    private auth: AuthenticateStore,
    private settingStore: SettingStore,
    private util: UtilService,
    private appService: AppService,
    private headerController: HeaderControllerService,
    private modalController: ModalController,
  ) {
    super();
  }

  async ngOnInit() {
    this.auth.authenticatedData$.subscribe(info => {
      this.loadAccount(info);
    });

    this.settingStore.menuChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe((menu: SettingMenu) => {
        if (menu == "accounts") {
          this.headerController.setActions([
            {
              title: 'Tạo cửa hàng',
              cssClass: 'btn btn-primary',
              onClick: () => this.openCreateShopModal(),
            }
          ]);
        } else {
          this.headerController.setActions([]);
        }
        this.current_menu = menu;
        this.changeMenu(menu);
      });
  }

  async changeMenu(menu) {
    this.settingStore.changeMenu(menu);
    const slug = this.util.getSlug();
    await this.router.navigateByUrl(`/s/${slug}/settings/${menu}`);
  }

  get appName() {
    return this.appService.appID != 'etop.vn' ? 'IMGroup ID' : '';
  }
  loadAccount(data) {
    this.currentShop = data.shop || new ExtendedAccount({});
    this.user = data.user;
    this.accounts = this.auth.snapshot.accounts;

    this.checkNullShop();
  }

  checkNullShop() {
    if (!this.currentShop.bank_account) {
      this.currentShop.bank_account = new BankAccount();
    }
    if (!this.currentShop.company_info) {
      this.currentShop.company_info = new CompanyInfo({
        legal_representative: {
          name: '',
          position: '',
          phone: '',
          email: ''
        }
      });
    }
  }

  async openCreateShopModal() {
    this.modal = this.modalController.create({
      component: CreateShopModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async () => {
      this.modal = null;
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }
}
