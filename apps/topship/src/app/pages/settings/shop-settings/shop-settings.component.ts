import { Component, OnInit } from '@angular/core';
import { ShopSettings } from '@etop/models/ShopSettings';
import { ShopSettingsService } from '@etop/features/services/shop-settings.service';
import { Address, TRY_ON_OPTIONS, TRY_ON_OPTIONS_SHIPPING } from '@etop/models';
import { AddressService } from 'apps/core/src/services/address.service';
import { ReturnAddressModalComponent } from '../components/return-address-modal/return-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';

@Component({
  selector: 'topship-shop-settings',
  templateUrl: './shop-settings.component.html',
  styleUrls: ['./shop-settings.component.scss']
})
export class ShopSettingsComponent implements OnInit {

  updating = false;
  settings = new ShopSettings({});
  
  constructor(
    private shopSettingsService: ShopSettingsService,
    private addressService: AddressService,
    private modalController: ModalController,
  ) { }

  async ngOnInit() {
    this.settings = await this.shopSettingsService.getSetting();
  }

  get tryOnOptions() {
    return TRY_ON_OPTIONS
  }

  get tryOnOptionsShipping() {
    return TRY_ON_OPTIONS_SHIPPING;
  }

  formatAddress(address) {
    return this.addressService.joinAddress(address);
  }

  onEditAddress() {
    this.editReturnAddress(this.settings?.return_address)
  }

  editReturnAddress(address: Address) {
    let modal = this.modalController.create({
      component: ReturnAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: address,
        title: 'Cập nhật địa chỉ trả hàng',
        setting: this.settings
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({settings: settings}) => {
      this.settings = settings
    });
  }

  openAddReturnAddressForm() {
    let modal = this.modalController.create({
      component: ReturnAddressModalComponent,
      showBackdrop: 'static',
      componentProps: {
        address: new Address({}),
        title: 'Tạo địa chỉ trả hàng',
        setting: this.settings
      },
      cssClass: 'modal-lg'
    });

    modal.show().then();
    modal.onDismiss().then(({settings: settings}) => {
      this.settings = settings
    });
  }

  async updateSetting() {
    try{
      this.updating = true;
      const res = await this.shopSettingsService.updateSetting(this.settings);
      this.settings = res;
      this.updating = false;
      toastr.success('Cập nhật thông số mặc định thành công.');
    } catch(e) {
      toastr.error(e.msg || e.message || 'Cập nhật thất bại.');
    }
  }

}
