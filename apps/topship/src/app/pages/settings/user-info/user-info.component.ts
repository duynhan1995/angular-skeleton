import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { ChangePasswordModalComponent } from 'apps/topship/src/app/pages/settings/components/change-password-modal/change-password-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { UpdateUserInfoModalComponent } from 'apps/topship/src/app/pages/settings/components/update-user-info-modal/update-user-info-modal.component';
import { AppService } from '@etop/web/core/app.service';
import { User } from 'libs/models';
import { UserService } from 'apps/core/src/services/user.service';
import { VerifyAccountModalComponent } from '../../../../../../core/src/components/header/components/verify-account-modal/verify-account-modal.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'topship-account-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  sendingCode = {
    email: false,
    phone: false,
  };
  ref_aff = '';
  show_ref_aff= false;
  loading = false;
  constructor(
    private auth: AuthenticateStore,
    private route: ActivatedRoute,
    private appService: AppService,
    private modalController: ModalController,
    private userService: UserService,
    private ref: ChangeDetectorRef
  ) {}

  get user(): User {
    return this.auth.snapshot.user;
  }

  get isAtEtop() {
    return this.appService.appID == 'topship.vn';
  }

  get appName() {
    return this.appService.appName;
  }

  ngOnInit() {
    const user = this.auth.snapshot.user
    if (user.ref_aff){
      this.show_ref_aff = true;
    }
  }

  onChangePassword() {
      this.openChangePasswordModal();
  }

  async submitUser(){
    try{
      this.loading = true
      if(!this.ref_aff){
        return toastr.error('Vui lòng nhập mã giới thiệu')
      }
      await this.userService.changeRefAff(this.ref_aff)
      toastr.success('Cập nhật mã giới thiệu thành công')
      this.user.ref_aff = this.ref_aff
      this.show_ref_aff = true;
    }catch (e) {
      toastr.error(e.msg || e.message);
    }
    this.loading = false;
  }

  openChangePasswordModal() {
    this.modalController
      .create({
        component: ChangePasswordModalComponent
      })
      .show().then();
  }

  updateUserEmail() {
    const modal = this.modalController.create({
      component: UpdateUserInfoModalComponent,
      componentProps: {
        info_to_update: 'email'
      }
    });
    modal.show().then();
  }

  updateUserPhone() {
    const modal = this.modalController.create({
      component: UpdateUserInfoModalComponent,
      componentProps: {
        info_to_update: 'phone'
      }
    });
    modal.show().then();
  }

  async sendVerifyEmail() {
    if(this.sendingCode.email || this.sendingCode.phone) {
      return;
    }
    this.sendingCode.email = true;
    this.ref.detectChanges();
    try {
      let res = await this.userService.sendEmailVerificationUsingOTP(this.auth.snapshot.user.email);
      let modal = this.modalController.create({
        component: VerifyAccountModalComponent,
        componentProps: {
          verifyEmail: true,
          email: this.auth.snapshot.user.email
        },
        showBackdrop: 'static'
      });
      modal.show().then();
    } catch (e) {
      toastr.error(e.message || e.msg);
    }
    this.sendingCode.email = false;
    this.ref.detectChanges();
  }

  async sendVerifyPhone() {
    if(this.sendingCode.email || this.sendingCode.phone) {
      return;
    }
    this.sendingCode.phone = true;
    this.ref.detectChanges();
    try {
      let res = await this.userService.sendPhoneVerification(this.auth.snapshot.user.phone);

      let modal = this.modalController.create({
        component: VerifyAccountModalComponent,
        componentProps: {
          verifyEmail: false,
          phone: this.auth.snapshot.user.phone
        },
        showBackdrop: 'static'
      });
      modal.show().then();
    } catch (e) {
      toastr.error(e.msg || e.message);
    }

    this.sendingCode.phone = false;
    this.ref.detectChanges();
  }
}
