import { Component, Input, OnInit } from '@angular/core';
import { OnboardingModalComponent } from 'apps/topship/src/app/components/modals/onboarding-modal/onboarding-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { AuthenticateStore } from '@etop/core';
import { Account } from 'libs/models/Account';
import { AppService } from '@etop/web/core/app.service';
import { CreateShopModalComponent } from '../create-shop-modal/create-shop-modal.component';

@Component({
  selector: 'topship-my-shops-management',
  templateUrl: './my-shops-management.component.html',
  styleUrls: ['./my-shops-management.component.scss']
})
export class MyShopsManagementComponent implements OnInit {
  @Input() accounts: Account[] = [];
  @Input() currentShop: any = {};

  constructor(
    private modalController: ModalController,
    private auth: AuthenticateStore,
    private appService: AppService
  ) {}

  ngOnInit() {}

  isMyShop(roles: string[]) {
    if (roles && roles.length) {
      return roles.indexOf('owner') != -1;
    }
    return true;
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn';
  }

  openCreateShopModal() {
    const modal = this.modalController.create({
      component: CreateShopModalComponent,
      componentProps: {
        shopCurrent: this.auth.snapshot.shop
      },
      cssClass: 'modal-lg'
    });
    modal.show().then();
    modal.onDismiss().then();
  }
}
