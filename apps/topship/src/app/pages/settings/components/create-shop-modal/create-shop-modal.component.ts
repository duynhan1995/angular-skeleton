import { Component, OnInit } from '@angular/core';
import { AuthenticateStore } from '@etop/core';
import { AccountApi, ShopAccountAPI } from '@etop/api';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { StringHandler } from '@etop/utils';
import { UserService } from 'apps/core/src/services/user.service';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';

@Component({
  selector: 'topship-create-shop-modal',
  templateUrl: './create-shop-modal.component.html',
  styleUrls: ['./create-shop-modal.component.scss']
})
export class CreateShopModalComponent implements OnInit {
  loading = false;
  shopName = '';
  constructor(
    private userService: UserService,
    private auth: AuthenticateStore,
    private modalAction: ModalAction,
    private commonUsecase: CommonUsecase,
    private etopAccountApi: AccountApi,
  ) { }

  ngOnInit(): void {
  }

  close() {
    this.modalAction.close(false);
  }

  async createShop() {
    this.loading = true;
    if(!this.shopName) {
      this.loading = false;
      return toastr.error("Vui lòng nhập tên cửa hàng");
    }
    try {
      const registerShopRequest: ShopAccountAPI.RegisterShopRequest = {
        name: this.shopName,
        phone: this.auth.snapshot.user.phone
      }
      const res = await this.userService.registerShop(registerShopRequest);

      const account = await this.userService.switchAccount(res.shop.id);

      await this.etopAccountApi.updateURLSlug({
        account_id: res.shop.id,
        url_slug: StringHandler.createHandle(res.shop.name + "-" + res.shop.code)
      }, account.access_token);
      await this.commonUsecase.updateSessionInfo(true);
      toastr.success("Tạo cửa hàng thành công");
      this.modalAction.dismiss(null);
    } catch (e) {
      debug.error('ERROR in Creating Shop', e);
      toastr.error('Tạo cửa hàng không thành công.', e.code && (e.message || e.msg));
    }
    this.loading = false;
  }
}
