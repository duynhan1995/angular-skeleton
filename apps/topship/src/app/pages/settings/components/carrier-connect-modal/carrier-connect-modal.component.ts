import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import { ModalAction } from 'apps/core/src/components/modal-controller/modal-action.service';
import { Connection } from 'libs/models/Connection';
import { ConnectionAPI, ConnectionApi } from '@etop/api';
import { BaseComponent } from '@etop/core';
import {LocationQuery} from "@etop/state/location";
import {combineLatest} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'topship-carrier-connect-modal',
  templateUrl: './carrier-connect-modal.component.html',
  styleUrls: ['./carrier-connect-modal.component.scss']
})
export class CarrierConnectModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() connection: Connection;

  tab: 'login' | 'register' = 'login';

  login_info = new ConnectionAPI.LoginShopConnectionRequest();
  login_by_token = new ConnectionAPI.LoginShopConnectionByTokenRequest();
  register_info = new ConnectionAPI.RegisterShopConnectionRequest();

  loading = false;

  locationForm = this.fb.group({
    provinceCode: '',
    districtCode: ''
  });

  locationReady$ = this.locationQuery.select(state => !!state.locationReady);
  provincesList$ = this.locationQuery.select("provincesList");
  districtsList$ = combineLatest([
    this.locationQuery.select("districtsList"),
    this.locationForm.controls['provinceCode'].valueChanges]).pipe(
    map(([districts, provinceCode]) => {
      if (!provinceCode) { return []; }
      return districts?.filter(dist => dist.province_code == provinceCode);
    })
  );

  private static validateLoginInfo(data) {
    if (!data.email) {
      toastr.error('Vui lòng nhập email!');
      return false;
    }
    if (!data.password) {
      toastr.error('Vui lòng nhập password!');
      return false;
    }
    return true;
  }

  private static validateRegisterInfo(data, provider: string) {
    if (!data.name) {
      toastr.error('Vui lòng nhập họ tên!');
      return false;
    }
    if (!data.phone) {
      toastr.error('Vui lòng nhập số điện thoại!');
      return false;
    }
    if (!data.email) {
      toastr.error('Vui lòng nhập số email!');
      return false;
    }
    if (!data.password && provider == 'ghn') {
      toastr.error('Vui lòng nhập mật khẩu!');
      return false;
    }
    if (!data.province) {
      toastr.error('Vui lòng nhập tỉnh thành!');
      return false;
    }
    if (!data.district) {
      toastr.error('Vui lòng nhập quận huyện!');
      return false;
    }
    if (!data.address) {
      toastr.error('Vui lòng nhập địa chỉ!');
      return false;
    }
    return true;
  }

  private static validateLoginByTokenInfo(data: ConnectionAPI.LoginShopConnectionByTokenRequest) {
    if (!data.user_id) {
      toastr.error('Vui lòng nhập User ID!');
      return false;
    }
    if (!data.token) {
      toastr.error('Vui lòng nhập Token!');
      return false;
    }
    return true;
  }

  displayMap = option => option && option.name || null;
  valueMap = option => option && option.code || null;

  constructor(
    private fb: FormBuilder,
    private modalAction: ModalAction,
    private connectionApi: ConnectionApi,
    private locationQuery: LocationQuery
  ) {
    super();
  }

  closeModal() {
    this.modalAction.close(false);
  }

  ngOnInit() {
    this.locationForm.controls['provinceCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.locationForm.patchValue({
          districtCode: ''
        });
        this.register_info.province = this.locationQuery.getProvince(value)?.name;
      });

    this.locationForm.controls['districtCode'].valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.register_info.district = this.locationQuery.getDistrict(value)?.name;
      });
  }

  ngAfterViewInit() {

  }

  async doConnect() {
    this.loading = true;
    if (this.connection.connection_provider == 'partner') {
      await this.loginUsingToken();
    } else {
      switch (this.tab) {
        case 'login':
          await this.loginShopConnection();
          break;
        case 'register':
          await this.registerShopConnection();
          break;
        default:
          break;
      }
    }
    this.loading = false;
  }

  async loginUsingToken() {
    try {
      const valid = CarrierConnectModalComponent.validateLoginByTokenInfo(this.login_by_token);
      if (!valid) {
        return this.loading = false;
      }

      const { user_id, token } = this.login_by_token;

      const body: ConnectionAPI.LoginShopConnectionByTokenRequest = {
        external_data: { user_id },
        token,
        connection_id: this.connection.id
      };
      await this.connectionApi.updateShopConnection(body);
      toastr.success(`Kết nối với ${this.connection.name} thành công.`);
      this.modalAction.dismiss(true);
    } catch(e) {
      debug.error('ERROR in loginUsingToken', e);
      toastr.error(
        `Kết nối với ${this.connection.name} không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
  }

  async loginShopConnection() {
    try {
      const valid = CarrierConnectModalComponent.validateLoginInfo(this.login_info);
      if (!valid) {
        return this.loading = false;
      }
      const body: ConnectionAPI.LoginShopConnectionRequest = {
        ...this.login_info,
        connection_id: this.connection.id
      };
      await this.connectionApi.loginShopConnection(body);
      toastr.success(`Kết nối với ${this.connection.name} thành công.`);
      this.modalAction.dismiss(true);
    } catch(e) {
      debug.error('ERROR in loginShopConnection', e);
      toastr.error(
        `Kết nối với ${this.connection.name} không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
  }

  async registerShopConnection() {
    try {
      const valid = CarrierConnectModalComponent.validateRegisterInfo(
        this.register_info,
        this.connection.connection_provider
      );
      if (!valid) {
        return this.loading = false;
      }
      const body: ConnectionAPI.RegisterShopConnectionRequest = {
        ...this.register_info,
        connection_id: this.connection.id
      };
      await this.connectionApi.registerShopConnection(body);
      toastr.success(`Kết nối với ${this.connection.name_display} thành công.`);
      this.modalAction.dismiss(true);
    } catch(e) {
      debug.error('ERROR in registerShopConnection', e);
      toastr.error(
        `Kết nối với ${this.connection.name_display} không thành công.`,
        e.code ? (e.message || e.msg) : ''
      );
    }
  }

  switchTab(tab) {
    this.tab = tab;
  }

}
