import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSrcCategoryLineComponent } from './product-src-category-line.component';

describe('ProductSrcCategoryLineComponent', () => {
  let component: ProductSrcCategoryLineComponent;
  let fixture: ComponentFixture<ProductSrcCategoryLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSrcCategoryLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSrcCategoryLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
