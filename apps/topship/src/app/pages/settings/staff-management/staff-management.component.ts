import { Component, OnInit } from '@angular/core';
import { AuthorizationApi } from '@etop/api';
import { BaseComponent } from '@etop/core';
import { AppService } from '@etop/web/core/app.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { Invitation, Relationship } from 'libs/models/Authorization';
import { FilterOperator } from '@etop/models';
import { AddStaffModalComponent } from 'apps/topship/src/app/pages/settings/components/add-staff-modal/add-staff-modal.component';
import { takeUntil } from 'rxjs/operators';
import {AuthorizationQuery} from "@etop/state/authorization";

@Component({
  selector: 'topship-staff-management',
  templateUrl: './staff-management.component.html',
  styleUrls: ['./staff-management.component.scss']
})
export class StaffManagementComponent extends BaseComponent implements OnInit {
  shop_invitations: Invitation[] = [];
  relationships: Relationship[] = [];
  invitationCancelled$ = this.authorizationQuery.select('invitationCancelled');
  relationshipDeleted$ = this.authorizationQuery.select('relationshipDeleted');
  relationshipUpdated$ = this.authorizationQuery.select('relationshipUpdated');

  private modal: any;

  constructor(
    private authorizationApi: AuthorizationApi,
    private authorizationQuery: AuthorizationQuery,
    private modalController: ModalController,
    private appService: AppService
  ) {
    super();
  }

  async ngOnInit() {
    await this.getRelationships();
    await this.getShopInvitations();

    this.invitationCancelled$
      .pipe(takeUntil(this.destroy$))
      .subscribe(cancelled => {
        if (cancelled) { this.getShopInvitations(); }
      });

    this.relationshipDeleted$
      .pipe(takeUntil(this.destroy$))
      .subscribe(deleted => {
        if (deleted) { this.getRelationships(); }
      });
    this.relationshipUpdated$
      .pipe(takeUntil(this.destroy$))
      .subscribe(updated => {
        if (updated) { this.getRelationships(); }
      });
  }

  async getShopInvitations() {
    try {
      const date = new Date();
      const dateString = date.toISOString();
      this.shop_invitations = await this.authorizationApi.getShopInvitations({
        filters: [
          {
            name: "status",
            op: FilterOperator.eq,
            value: "Z"
          },
          {
            name: "expires_at",
            op: FilterOperator.gt,
            value: dateString
          }
        ]
      });
    } catch(e) {
      debug.error('ERROR in getting Invitations of Shop', e);
    }
  }

  async getRelationships() {
    try {
      this.relationships = await this.authorizationApi.getRelationships({});
      this.relationships = this.relationships.filter(r => !r.deleted);
    } catch(e) {
      debug.error('ERROR in getting Relationships of Shop', e);
    }
  }

  addStaff() {
    if (this.modal) { return; }
    this.modal = this.modalController.create({
      component: AddStaffModalComponent,
      showBackdrop: true,
      cssClass: 'modal-md'
    });
    this.modal.show();
    this.modal.onDismiss().then(invitation => {
      this.modal = null;
      if (invitation) {
        invitation = this.authorizationApi.invitationMap(invitation);
        this.shop_invitations.push(invitation);
      }
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  get isAtEtop() {
    return this.appService.appID == 'etop.vn'
  }

}
