import { Component, OnInit } from '@angular/core';
import { Address } from 'libs/models/Address';
import { AddressService } from 'apps/core/src/services/address.service';
import { AuthenticateStore } from '@etop/core';
import { ShopService } from '@etop/features';
import { GoogleAnalyticsService, USER_BEHAVIOR } from 'apps/core/src/services/google-analytics.service';
import { FromAddressModalComponent } from 'libs/shared/components/from-address-modal/from-address-modal.component';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { Router } from '@angular/router';
import { DialogControllerService } from 'apps/core/src/components/modal-controller/dialog-controller.service';
import { OrderService } from 'apps/topship/src/services/order.service';
import { ExtendedAccount } from 'libs/models/Account';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'topship-carrier-info',
  templateUrl: './carrier-info.component.html',
  styleUrls: ['./carrier-info.component.scss']
})
export class CarrierInfoComponent implements OnInit {
  constructor(
  ) {}

  async ngOnInit() {
  }
}


