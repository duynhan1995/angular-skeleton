import { Component, OnInit, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ForgotPasswordModalComponent } from 'apps/shared/src/pages/login/modals/forgot-password-modal/forgot-password-modal.component';
import { Invitation } from 'libs/models/Authorization';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountApi, AuthorizationApi, ShopAccountAPI } from '@etop/api';
import { AuthenticateStore } from '@etop/core/modules/authenticate';
import { StringHandler } from '@etop/utils';
import { UserService } from 'apps/core/src/services/user.service';

enum View {
  NONE = 'none',
  PHONE_SUBMIT = 'phonesubmit',
  PHONE_VERIFY = 'phoneverify',
  LOGIN = 'login',
  REGISTER = 'register',
  CREATE_SHOP = 'createshop'
}

@Component({
  selector: 'topship-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('passwordInput', {static: false}) passwordInput: ElementRef;
  login: string;
  password: string;
  phone: string;
  verify_code: string;
  email: string;
  fullname: string;
  _currentView: View = View.NONE;
  userName: string;
  shop_name: string;
  loading = false;
  view = View;
  invitation_token = '';
  invitation = new Invitation({});

  loadingView = true;

  constructor(
    private commonUsecase: CommonUsecase,
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute,
    private authorizationApi: AuthorizationApi,
    private router: Router,
    private auth: AuthenticateStore,
    private userService: UserService,
    private etopAccountApi: AccountApi,
  ) {}

  async ngOnInit() {
    this._currentView = View.LOGIN;
    this.invitation_token = this.activatedRoute.snapshot.queryParamMap.get(
      'invitation_token'
    );
    if (this.invitation_token) {
      this.invitation = await this.authorizationApi.getInvitationByToken(
        this.invitation_token
      );
      this.login = (this.invitation && this.invitation.email) || '';
    }
    this.commonUsecase.redirectIfAuthenticated().then(() => (this.loadingView = false));
  }

  registerHref() {
    if (this.invitation_token) {
      this.router.navigateByUrl(
        `/register?invitation_token=${this.invitation_token}`
      );
    } else {
      this.router.navigateByUrl(`/register`);
    }
  }

  async onEnterLoginInput() {
    if (!this.password) {
      if (this.passwordInput) {
        setTimeout(_ => {
          this.passwordInput.nativeElement.focus();
        });
      }
    } else {
      this.onLogin();
    }
  }

  showPassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'text';
      });
    }
  }

  hidePassword() {
    if (this.passwordInput) {
      setTimeout(_ => {
        this.passwordInput.nativeElement.type = 'password';
      });
    }
  }

  async onLogin() {
    if (!this.login || !this.password) {
      toastr.error('Vui lòng nhập đầy đủ thông tin đăng nhập để tiếp tục.');
      return;
    }

    this.loading = true;
    try {
      const res = await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
      if ( res == "create_shop" ) {
        this.userName = this.auth.snapshot.user.full_name;
        this._currentView = View.CREATE_SHOP;
      }
    } catch (e) {
      toastr.error(e.message, 'Đăng nhập thất bại!');
    }
    this.loading = false;
  }

  forgotPassword() {
    const modal = this.modalController.create({
      component: ForgotPasswordModalComponent
    });
    modal.show().then();
    modal.onDismiss().then(() => {});
  }

  async createShop(){
    if(!this.shop_name) {
      toastr.error('Vui lòng nhập Tên cửa hàng đầy đủ.');
      return;
    }
    this.loading = true;
    try{
      const registerShopRequest: ShopAccountAPI.RegisterShopRequest = {
        name: this.shop_name,
        phone: this.auth.snapshot.user.phone
      }
      const res = await this.userService.registerShop(registerShopRequest);
      const account = await this.userService.switchAccount(res.shop.id);
      await this.etopAccountApi.updateURLSlug({
        account_id: res.shop.id,
        url_slug: StringHandler.createHandle(res.shop.name + "-" + res.shop.code)
      }, account.access_token);
      await this.commonUsecase.updateSessionInfo(true);
      await this.commonUsecase.login({
        login: this.login,
        password: this.password
      });
      this.router.navigateByUrl('/survey').then();
    } catch (e) {
      toastr.error(e.message, 'Tạo cửa hàng thất bại!');
    }
    this.loading = false;
  }

  currentView(view: View) {
    return this._currentView == view;
  }
}
