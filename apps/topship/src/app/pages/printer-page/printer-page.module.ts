import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrinterPageComponent } from './printer-page.component';
import { RouterModule, Routes } from '@angular/router';
import { BarcodePageComponent } from './page-barcode/barcode-page.component';
// Import ngx-barcode module
import { TrimtoolongPipe } from './pipes/trimtoolong.pipe';
import { SharedModule } from 'apps/shared/src/shared.module';
import { TopshipAppModule } from '../../topship-app.module';
import { EtopPipesModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: PrinterPageComponent,
    children: [
      {
        path: 'barcode',
        component: BarcodePageComponent
      }
    ]
  }
];

@NgModule({
  declarations: [PrinterPageComponent, BarcodePageComponent, TrimtoolongPipe],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    EtopPipesModule,
    // ShopAppModule
  ]
})
export class PrinterPageModule { }
