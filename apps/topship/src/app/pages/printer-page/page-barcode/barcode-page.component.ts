import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from 'apps/core/src/services/util.service';
import { DomSanitizer } from '@angular/platform-browser';

declare const document: any;

@Component({
  selector: 'topship-demo-page',
  templateUrl: './barcode-page.component.html',
  styleUrls: ['./barcode-page.component.scss']
})
export class BarcodePageComponent implements OnInit {
  paramData: any;
  parsedData: any = [];
  shopName = '';
  showShopname = false;
  showPrice = false;
  paperSize = '';

  loading = false;
  fontSize = 10;
  height = 25;
  width = 1;

  constructor(
    private route: ActivatedRoute,
    private util: UtilService,
    private santinizer: DomSanitizer
  ) {}

  print(){
    window.print();
  }

  async ngOnInit() {
    const queryParams = this.route.snapshot.queryParams;
    this.paramData = JSON.parse(queryParams.data);
    this.shopName = this.paramData.shopName;
    this.showShopname = this.paramData.showShopname;
    this.showPrice = this.paramData.showPrice;
    this.paperSize = this.paramData.size;

    this.paramData.barcodeInfo = this.paramData.barcodeInfo.map(b => {
      return {
        name: b[0],
        code: b[1],
        price: b[2],
        print_quantity: b[3]
      }
    });

    let offset = 1;
    switch (this.paramData.size) {
      case 'three104x22':
        offset = 3;
        this.fontSize = 10;
        this.height = 25;
        this.updatePageLayout('104mm 22mm');
        break;
      case 'two72x22':
        offset = 2;
        this.fontSize = 10;
        this.height = 25;
        this.updatePageLayout('72mm 22mm');
        break;
      case 'two74x22':
        offset = 2;
        this.fontSize = 10;
        this.height = 25;
        this.updatePageLayout('74mm 22mm');
        break;
      case 'twelve202x162':
        offset = 12;
        this.fontSize = 10;
        this.height = 28;
        this.updatePageLayout('202mm 162mm');
        break;
      case 'sixtyfivea4':
        offset = 65;
        this.fontSize = 10;
        this.height = 25;
        this.updatePageLayout('a4');
        break;
      case 'one75x10':
        offset = 1;
        this.fontSize = 12;
        this.height = 32;
        this.width = 1.3;
        this.updatePageLayout('75mm 10mm');
        break;
      case 'one40x30':
        offset = 1;
        this.fontSize = 10;
        this.height = 40;
        this.updatePageLayout('40mm 30mm');
        break;
      default:
        offset = 3;
        this.updatePageLayout('104mm 22mm');
        break;
    }

    const _preData = [];

    this.paramData.barcodeInfo.forEach(variant => {
      for (let i = 0; i < variant.print_quantity; i++){
        _preData.push({
          name: variant.name,
          code: variant.code,
          price: variant.price,
          svg: this.santinizer.bypassSecurityTrustUrl(`data:image/svg+xml;charset=UTF-8,${this.generateBarcodeSVG(variant.code, this.paramData.standard)}`)
        });
      }
    });

    while(_preData.length) {
      this.parsedData.push(_preData.splice(0, offset));
    }
  }

  updatePageLayout(size) {
    let style = document.createElement('style');
    document.head.appendChild(style);
    style.innerHTML = `
      @media print {
        * {
          margin: 0 !important;
        }
        @page {
          size: ${size}};
        }
      }`;
  }

  generateBarcodeSVG(code, standard) {
    const barcodeContainer = document.getElementById("barcode-container");
    const { DOMImplementation, XMLSerializer } = require('xmldom');
    const xmlSerializer = new XMLSerializer();
    const svgNode = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    barcodeContainer.appendChild(svgNode);
    // font-size,height 104x22: 10 25 | 75x10: 8 10
    JsBarcode(svgNode, code, {
      format: standard,
      fontSize: this.fontSize,
      height: this.height,
      width: this.width,
      margin: 5,
      xmlDocument: document,
    });

    const svgText = xmlSerializer.serializeToString(svgNode);
    return this.util.encodeSVG(svgText)
  }



}
