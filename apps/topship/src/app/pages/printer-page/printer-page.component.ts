import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'topship-printer-page',
  templateUrl: './printer-page.component.html',
  styleUrls: ['./printer-page.component.scss']
})
export class PrinterPageComponent implements OnInit {
  constructor(private router: Router) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(_ => {
        setTimeout(() => {
          // window.print();
        });
      });
  }

  ngOnInit() {}

}
