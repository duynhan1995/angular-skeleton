import {Component, OnInit, ViewChild} from '@angular/core';
import { AuthenticateStore, BaseComponent } from '@etop/core';
import { Router } from '@angular/router';
import {CreateShopFormComponent} from "apps/topship/src/app/components/create-shop-form/create-shop-form.component";

@Component({
  selector: 'topship-create-shop',
  templateUrl: './create-shop.component.html',
  styleUrls: ['./create-shop.component.scss']
})
export class CreateShopComponent extends BaseComponent implements OnInit {
  @ViewChild('createShopForm', {static: false}) createShopForm: CreateShopFormComponent;
  loading = false;
  shippingPolicyAccepted = false;

  constructor(
    private auth: AuthenticateStore,
    private router: Router,
  ) {
    super();
  }

  ngOnInit() {}

  async confirm() {
    this.loading = true;
    await this.createShopForm.confirm();
    this.loading = false;
  }

  signout() {
    this.auth.clear();
    this.router.navigateByUrl('/login').then();
  }

}
