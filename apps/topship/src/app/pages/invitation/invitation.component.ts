import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'apps/core/src/services/user.service';
import { UtilService } from 'apps/core/src/services/util.service';
import { AuthenticateStore } from '@etop/core';
import { Invitation } from '@etop/models';
import { AuthorizationApi, UserApi } from '@etop/api';
import { CommonUsecase } from 'apps/shared/src/usecases/common.usecase.service';
import { AppService } from '@etop/web/core/app.service';

@Component({
  selector: 'topship-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {
  token = '';
  invitation = new Invitation({});

  unauthenticated = false;
  not_found = false;
  invalid_email = false;

  loading = false;
  refusing = false;
  accepting = false;

  emailVerified = '';
  isSentVerificationEmail = '';
  sendEmailMsg = '';
  sendEmailSuccess: boolean;
  sendingEmail = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private auth: AuthenticateStore,
    private authorizationApi: AuthorizationApi,
    private userApi: UserApi,
    private util: UtilService,
    private router: Router,
    private ref: ChangeDetectorRef,
    private commonUseCase: CommonUsecase,
    private appService: AppService
  ) { }

  get user() {
    return this.auth.snapshot.user;
  }

  get actionAvailable() {
    return !this.invitation.declined_at && !this.invitation.accepted_at && !this.not_found;
  }

  roleMap(role) {
    return AuthorizationApi.roleMap(role);
  }

  ngOnInit() {
    this.checkAndSetupInvitation();
  }

  async checkAndSetupInvitation() {
    this.loading = true;
    try {
      this.token = this.activatedRoute.snapshot.queryParamMap.get('t');
      await this.checkToken();
      this.invitation = await this.authorizationApi.getInvitationByToken(this.token);

      const _snapshot = this.auth.snapshot;
      this.emailVerified = _snapshot.user && _snapshot.user.email_verified_at;
      this.isSentVerificationEmail = _snapshot.user && _snapshot.user.email_verification_sent_at;

      if (!_snapshot.user) {
        this.unauthenticated = true;
      } else if (_snapshot.user.email != this.invitation.email) {
        this.unauthenticated = true;
        this.invalid_email = true;
      }

    } catch(e) {
      this.not_found = e.code == 'not_found';
      debug.error('ERROR in checkAndSetupInvitation', e);
    }
    this.loading = false;
  }

  async checkToken() {
    try {
      await this.userApi.sessionInfo();
    } catch(e) {
      debug.error('ERROR in checking Token', e);
      if (e.code == 'unauthenticated') {
        this.unauthenticated = true;
      }
    }
  }

  async refuse() {
    this.refusing = true;
    try {
      await this.authorizationApi.rejectInvitation(this.token);
      this.invitation = await this.authorizationApi.getInvitationByToken(this.token);
    } catch(e) {
      toastr.error(e.message || e.msg);
      debug.error('ERROR in refusing Invitation', e);
    }
    this.refusing = false;
  }


  async accept() {
    this.accepting = true;
    try {
      await this.authorizationApi.acceptInvitation(this.token);
      await this.commonUseCase.updateSessionInfo(true);
      this.auth.selectAccount(this.invitation.shop_id);
      await this.router.navigateByUrl(`/s/${this.util.getSlug()}/settings`);
    } catch(e) {
      if (e.code == 'failed_precondition') {
        toastr.error(e.message || e.msg);
      } else {
        toastr.error(e.message || e.msg);
      }
      debug.error('ERROR in accepting Invitation', e);
    }
    this.accepting = false;
  }

  login() {
    this.auth.clear();
    this.router.navigate(['/register'], { queryParams: { invitation_token: this.token } });
  }

  async sendVerifyEmail() {
    this.sendingEmail = true;
    this.ref.detectChanges();
    try {
      let res = await this.userService.sendEmailVerification(this.auth.snapshot.user.email);
      this.sendEmailMsg = res.msg;
      this.sendEmailSuccess = true;
      this.ref.detectChanges();
    } catch (e) {
      this.sendEmailMsg = e.message;
      this.ref.detectChanges();
    }

    this.sendingEmail = false;
    this.ref.detectChanges();
  }

  get isAtEtop() {
    return this.appService.appID == 'topship.vn';
  }

}

