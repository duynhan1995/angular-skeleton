import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InvitationComponent } from 'apps/topship/src/app/pages/invitation/invitation.component';
import { SharedModule } from 'apps/shared/src/shared.module';
import { ShopPipesModule } from 'apps/topship/src/app/pipes/shop-pipes.module';
import { EtopPipesModule } from '@etop/shared';

const routes: Routes = [
  {
    path: '',
    component: InvitationComponent
  }
];

@NgModule({
  declarations: [
    InvitationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ShopPipesModule,
    EtopPipesModule,
  ],
  providers: [
  ]
})
export class InvitationModule { }
