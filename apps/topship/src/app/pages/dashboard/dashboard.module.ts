import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from 'apps/topship/src/app/pages/dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'apps/shared/src/shared.module';
import { FormsModule } from '@angular/forms';
import { DashboardTopshipComponent } from './dashboard-topship/dashboard-topship.component';
import { DashboardControllerService } from './dashboard-controller.service';
import { EtopPipesModule } from '@etop/shared';
import { NotSubscriptionModule } from '@etop/shared/components/not-subscription/not-subscription.module';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    EtopPipesModule,
    NotSubscriptionModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  declarations: [
    DashboardComponent,
    DashboardTopshipComponent,
  ],
  providers: [DashboardControllerService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class DashboardModule {}
