import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeaderControllerService } from 'apps/core/src/components/header/header-controller.service';
import { DashboardStoreService } from 'apps/core/src/stores/dashboard.store.service';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'topship-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  popup_holiday_content: any;

  constructor() {}

  async ngOnInit() {}

  ngOnDestroy() {}
}
