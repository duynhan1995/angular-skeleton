import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CustomerService } from 'apps/topship/src/services/customer.service';
import { DashboardTopshipComponent } from './dashboard-topship/dashboard-topship.component';
import { AuthenticateStore } from '@etop/core';

@Injectable()
export class DashboardControllerService {

  onChangeTab$ = new Subject();

  constructor() {}

}
