import {Component, Input, OnInit} from '@angular/core';
import {Ticket} from "@etop/models";

@Component({
  selector: '[topship-tickets-system-row]',
  templateUrl: './tickets-system-row.component.html',
  styleUrls: ['./tickets-system-row.component.scss']
})
export class TicketsSystemRowComponent implements OnInit {
  @Input() ticket: Ticket;
  @Input() liteMode = false;

  constructor() { }

  ngOnInit() {
  }

}
