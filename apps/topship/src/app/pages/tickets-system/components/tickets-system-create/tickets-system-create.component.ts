import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BaseComponent} from '@etop/core';
import {Ticket} from '@etop/models/Ticket';
import {TicketService} from '@etop/state/shop/ticket';
import {TicketLabelQuery, TicketLabelService} from '@etop/state/shop/ticket-labels';

@Component({
  selector: 'topship-tickets-system-create',
  templateUrl: './tickets-system-create.component.html',
  styleUrls: ['./tickets-system-create.component.scss']
})
export class TicketsSystemCreateComponent extends BaseComponent implements OnInit {
  @Output() createTicket = new EventEmitter();

  ticket = new Ticket();
  labels = [];
  selectedLabel: any;
  loading = false;

  constructor(
    private ticketService: TicketService,
    private ticketLabelService: TicketLabelService,
    private ticketLabelQuery: TicketLabelQuery
  ) {
    super();
  }

  async ngOnInit() {
    this.loading = true;
    await this.ticketLabelService.getTicketLabels();
    const etelecomLabel = this.ticketLabelQuery.getValue()
      .ticketProductLabels.find(t => t.code == 'etelecom');
    this.labels = this.ticketLabelQuery.getValue().ticketSubjectLabels
      .filter(t => t.parent_id == etelecomLabel?.id).map(t => {
        return {
          value: t.id,
          name: t.name
        }
      })
    this.loading = false;
  }

  validateColor(color: string) {
    return TicketLabelService.validateColor(color);
  }

  async create() {
    this.loading = true;
    try {
      if (!this.selectedLabel) {
        this.loading = false;
        return toastr.error('Vui lòng chọn phân loại!');
      }
      if (!this.ticket.description) {
        this.loading = false;
        return toastr.error('Vui lòng nhập nội dung!');
      }
      if (!this.ticket.title) {
        this.loading = false;
        return toastr.error('Vui lòng nhập tiêu đề!');
      }
      const data = {
        source: null,
        label_ids: [this.selectedLabel],
        title: this.ticket.title,
        description: this.ticket.description
      }
      await this.ticketService.createEtelecomAdminTicket(data);
      toastr.success('Gửi yêu cầu mới thành công');
      this.createTicket.emit();
      this.ticket = new Ticket();
    } catch (e) {
      debug.log(e)
      toastr.error('Gửi yêu cầu thất bại!', e.message);
    }
    this.loading = false;
  }

}
