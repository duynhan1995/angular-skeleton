import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticateStore} from "@etop/core";
import {TicketsSystemListComponent} from './tickets-system-list/tickets-system-list.component';
import {TicketService} from "@etop/state/shop/ticket";

@Component({
  selector: 'topship-tickets-system',
  template: `
    <etop-not-permission *ngIf="noPermission"></etop-not-permission>
    <topship-tickets-system-list
      *ePermissions="['shop/shop_ticket_comment:view']" #ticketList>
    </topship-tickets-system-list>`
})
export class TicketsSystemComponent implements OnInit {
  @ViewChild('ticketList', {static: true}) ticketList: TicketsSystemListComponent;

  constructor(
    private auth: AuthenticateStore,
    private ticketService: TicketService
  ) {
  }

  get noPermission() {
    return !this.auth.snapshot.permission.permissions.includes('shop/shop_ticket_comment:view');
  }

  async ngOnInit() {
    this.ticketService.prepareData().then();
  }

}
