import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {HeaderControllerService} from 'apps/core/src/components/header/header-controller.service';
import {PageBaseComponent} from '@etop/web/core/base/page.base-component';
import {EtopTableComponent, Paging, SideSliderComponent} from '@etop/shared';
import {Ticket, TicketRefType} from '@etop/models';
import {takeUntil} from 'rxjs/operators';
import {TicketService, TicketQuery} from '@etop/state/shop/ticket';
import {ActivatedRoute} from '@angular/router';
import {ShopTicketAPI} from '@etop/api';

@Component({
  selector: 'topship-tickets-system-list',
  templateUrl: './tickets-system-list.component.html',
  styleUrls: ['./tickets-system-list.component.scss'],
})
export class TicketsSystemListComponent extends PageBaseComponent implements OnInit, OnDestroy {
  @ViewChild('ticketTable', {static: true}) ticketTable: EtopTableComponent;
  @ViewChild('ticketSlider', {static: true})
  ticketSlider: SideSliderComponent;
  queryParams = {id: ''};

  private _selectMode = false;
  get selectMode() {
    return this._selectMode;
  }

  set selectMode(value) {
    this._selectMode = value;
    this._onSelectModeChanged(value);
  }

  createTicketMode: boolean;

  finishPrepareData$ = this.ticketQuery.select(
    (state) => state.ui.finishPrepareData
  );

  ticketsLoading$ = this.ticketQuery.selectLoading();
  ticketsList$ = this.ticketQuery.selectAll();
  activeTicket$ = this.ticketQuery.selectActive();

  isLastPage$ = this.ticketQuery.select((state) => state.ui.isLastPage);

  emptyTitle = 'Chưa có yêu cầu hỗ trợ nào.';

  constructor(
    private changeDetector: ChangeDetectorRef,
    private headerController: HeaderControllerService,
    private ticketQuery: TicketQuery,
    private ticketService: TicketService,
    private route: ActivatedRoute,
  ) {
    super();
  }

  get showPaging() {
    return !this.ticketTable.liteMode && !this.ticketTable.loading;
  }

  get sliderTitle() {
    return this.createTicketMode ? 'Gửi yêu cầu hỗ trợ' : 'Chi tiết yêu cầu';
  }

  private _onSelectModeChanged(value) {
    this.ticketTable.toggleLiteMode(value);
    this.ticketSlider.toggleLiteMode(value);
  }

  async ngOnInit() {
    await this._prepareData()
    this.ticketsLoading$.pipe(takeUntil(this.destroy$)).subscribe((loading) => {
      this.ticketTable.loading = loading;
      this.changeDetector.detectChanges();
      if (loading) {
        this.resetState();
      }
    });

    this.isLastPage$.pipe(takeUntil(this.destroy$)).subscribe((isLastPage) => {
      if (isLastPage) {
        this.ticketTable.toggleNextDisabled(true);
        this.ticketTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả các yêu cầu hỗ trợ.');
      }
    });
  }

  ngOnDestroy() {
    this.headerController.clearTabs();
    this.ticketService.setFilter({});
  }

  resetState() {
    this.selectMode = false;
    this.ticketTable.toggleNextDisabled(false);
  }

  async getTickets() {
    const finishPrepareData = this.ticketQuery.getValue().ui.finishPrepareData;
    if (finishPrepareData) {
      await this.ticketService.getTickets(false).then();
    } else {
      this.finishPrepareData$
        .pipe(takeUntil(this.destroy$))
        .subscribe(async (loading) => {
          if (loading) {
            await this.ticketService.getTickets(false).then();
          }
        });
    }
  }

  async loadPage({page, perpage}) {
    this.ticketService.setPaging({
      limit: perpage,
      offset: (page - 1) * perpage,
    });

    await this.getTickets();
    this._paramsHandling();
  }

  detail(ticket: Ticket) {
    this.createTicketMode = false;
    this.ticketService.selectTicket(ticket);
    this._checkSelectMode();
  }

  onSliderClosed() {
    this.createTicketMode = false;
    this.ticketService.selectTicket(null);
    this._checkSelectMode();
  }

  private _checkSelectMode() {
    const selected = !!this.ticketQuery.getActive();
    this.ticketTable.toggleLiteMode(selected);
    this.ticketSlider.toggleLiteMode(selected);
  }

  createTicket() {
    this.createTicketMode = true;
    this.ticketService.selectTicket(null);
    this.selectMode = true;
  }

  private _paramsHandling() {
    const {queryParams} = this.route.snapshot;
    this.queryParams.id = queryParams.id;
    if (!this.queryParams.id) {
      return;
    }
    const ticket = this.ticketQuery.getAll().find(t => t.id == this.queryParams.id);
    this.detail(ticket);
    this._checkSelectMode();
  }

  private async _prepareData() {
    const paging: Paging = {
      offset: 0,
      limit: 1000,
    }
    this.ticketService.setPaging(paging)

    const filter: ShopTicketAPI.GetTicketsFilter = {
      ref_id: null,
      ref_type: TicketRefType.ffm
    }
    this.ticketService.setFilter(filter)
    await this.ticketService.prepareData().then();
    this._paramsHandling();
  }
}
