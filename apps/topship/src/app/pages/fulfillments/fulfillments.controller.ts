import { Injectable } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Subject } from 'rxjs';

export enum PrintType {
  fulfillments = 'fulfillments',
}

@Injectable()
export class FulfillmentsController {
  fulfillment_type = 'shipment';

  onChangeTab$ = new Subject();
  onCreateOrder$ = new Subject();

  constructor() { }

}

