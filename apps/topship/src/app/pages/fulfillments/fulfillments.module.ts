import { EmptyPageModule } from './../../../../../../libs/shared/components/empty-page/empty-page.module';
import {BillPrintingModule} from 'apps/topship/src/app/components/bill-printing/bill-printing.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImportFfmModule} from "libs/shared/components/import-ffm/import-ffm.module";
import {FulfillmentsComponent} from 'apps/topship/src/app/pages/fulfillments/fulfillments.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from 'apps/shared/src/shared.module';
import {FulfillmentListComponent} from './fulfillment-list/fulfillment-list.component';
import {FulfillmentRowComponent} from './components/fulfillment-row/fulfillment-row.component';
import {ModalControllerModule} from 'apps/core/src/components/modal-controller/modal-controller.module';
import {FormsModule} from '@angular/forms';
import {ForceButtonModule} from '../../components/force-button/force-button.module';
import {FulfillmentsController} from 'apps/topship/src/app/pages/fulfillments/fulfillments.controller';
import {ReturningFulfillmentsWarningModule} from 'apps/topship/src/app/components/returning-fulfillments-warning/returning-fulfillments-warning.module';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {SingleFfmDetailModule} from 'apps/topship/src/app/pages/fulfillments/components/single-ffm-detail/single-ffm-detail.module';
import {AuthenticateModule} from '@etop/core';
import {CancelObjectModule} from '../../components/cancel-object/cancel-object.module';
import {MultipleFfmActionsComponent} from './components/multiple-ffm-actions/multiple-ffm-actions.component';
import {DropdownActionsModule} from 'apps/shared/src/components/dropdown-actions/dropdown-actions.module';
import {EtopCommonModule, EtopFilterModule, EtopPipesModule, MaterialModule, SideSliderModule} from '@etop/shared';
import {PrintModule} from 'apps/topship/src/app/components/print/print.module';
import { CreateOrderModule } from '../create-order/create-order.module';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: FulfillmentsComponent},
      {
        path: ':id',
        loadChildren: () => import('./components/fulfillment-detail/fulfillment-detail.module').then(m => m.FulfillmentDetailModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    FulfillmentsComponent,
    FulfillmentListComponent,
    FulfillmentRowComponent,
    MultipleFfmActionsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    ForceButtonModule,
    ModalControllerModule,
    ReturningFulfillmentsWarningModule,
    DropdownActionsModule,
    CancelObjectModule,
    RouterModule.forChild(routes),
    NgbTooltipModule,
    SingleFfmDetailModule,
    AuthenticateModule,
    BillPrintingModule,
    EtopPipesModule,
    SideSliderModule,
    EtopFilterModule,
    EtopCommonModule,
    MaterialModule,
    PrintModule,
    ImportFfmModule,
    CreateOrderModule,
    EmptyPageModule
  ],
  providers: [
    FulfillmentsController
  ]
})
export class FulfillmentsModule {
}
