import {
  ChangeDetectorRef,
  Component, EventEmitter,
  OnInit, Output,
  ViewChild
} from '@angular/core';
import {FulfillmentService} from '../../../../services/fulfillment.service';
import {Fulfillment} from 'libs/models/Fulfillment';
import {ModalController} from 'apps/core/src/components/modal-controller/modal-controller.service';
import {
  Address,
  CANCEL_ORDER_REASONS,
  FilterOperator,
  Filters
} from '@etop/models';
import {EtopTableComponent} from 'libs/shared/components/etop-common/etop-table/etop-table.component';
import {FulfillmentsController} from 'apps/topship/src/app/pages/fulfillments/fulfillments.controller';
import {UtilService} from 'apps/core/src/services/util.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SideSliderComponent} from 'libs/shared/components/side-slider/side-slider.component';
import {AuthenticateStore} from '@etop/core';
import {takeUntil} from 'rxjs/operators';
import {CancelObjectComponent} from 'apps/topship/src/app/components/cancel-object/cancel-object.component';
import {FulfillmentApi} from '@etop/api';
import {PromiseQueueService} from 'apps/core/src/services/promise-queue.service';
import {TelegramService} from '@etop/features';
import {OrderApi} from '@etop/api';
import {DropdownActionOpt} from "apps/shared/src/components/dropdown-actions/dropdown-actions.interface";
import {PageBaseComponent} from "@etop/web";
import { ConfirmOrderQuery, ConfirmOrderService } from '../../../state/confirm-order';
import { ActionButton, EmptyType } from '@etop/shared/components/empty-page/empty-page.component';

@Component({
  selector: 'topship-fulfillment-list',
  templateUrl: './fulfillment-list.component.html',
  styleUrls: ['./fulfillment-list.component.scss']
})
export class FulfillmentListComponent extends PageBaseComponent implements OnInit {
  @ViewChild('fulfillmentTable', {static: true}) fulfillmentTable: EtopTableComponent;
  @ViewChild('slider', {static: true}) slider: SideSliderComponent;

  @Output() toImport = new EventEmitter();

  filters: Filters = [];

  tabs = [
    {name: 'Thông tin', value: 'detail_info'},
    {name: 'Yêu cầu hỗ trợ', value: 'support_request'},
    {name: 'Lịch sử GH', value: 'history'},
    {name: 'Lộ trình', value: 'shipnow_route'}
  ];
  activeTab: 'detail_info' | 'support_request' | 'history' | 'shipnow_route' = 'detail_info';

  fulfillmentList: Fulfillment[] = [];
  selectedFfms: Fulfillment[] = [];
  checkAll = false;
  emptyAction: ActionButton[] = [];

  page: number;
  perpage: number;

  forcedHidePaging = false;

  queryParams = {
    code: '',
    type: ''
  };

  dropdownActions: DropdownActionOpt[] = [];

  private modal: any;

  createNewOrder = false;
  fromAddresses: Address[];
  fromAddress: Address;

  constructor(
    private fulfillmentService: FulfillmentService,
    private modalController: ModalController,
    private changeDetector: ChangeDetectorRef,
    private util: UtilService,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthenticateStore,
    private ffmsController: FulfillmentsController,
    private ffmApi: FulfillmentApi,
    private promiseQueue: PromiseQueueService,
    private telegram: TelegramService,
    private confirmOrderService: ConfirmOrderService,
    private confirmOrderQuery: ConfirmOrderQuery,
  ) {
    super();
  }

  async ngOnInit() {
    this.emptyAction = [
      {
        title: 'Tạo đơn hàng',
        cssClass: 'btn-topship btn-outline',
        onClick: () => this.gotoPOS()
      },
      {
        title: 'Import',
        cssClass: 'btn-topship',
        onClick: () => this.gotoImport()
      },
    ]
    this.fulfillmentService.onViewReturningFfms$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.loadAllReturningFfms();
      });
    this.ffmsController.onCreateOrder$
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.showCreateOrder();
      });
      await this.confirmOrderService.initData();

      let addresses: any = this.confirmOrderQuery.getValue().fromAddresses;
      this.fromAddresses = addresses.map(address => {
        return {
          ...address,
          info: this.addressDisplay(address),
          detail: this.addressDisplay(address)
        }
      });
      this.fromAddress = this.confirmOrderQuery.getValue().fulfillment.pickup_address;
      this.confirmOrderService.forceReload$
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          this.createNewOrder = false;
          this.reloadFfms();
          this.resetState();
        });
  }

  addressDisplay(address: Address) {
    const { full_name, address1, ward, district, province } = address;
    return full_name + ' - ' + address1 + ', ' + ward + ', ' + district + ', ' + province;
  }

  changeFromAddress(address) {
    this.confirmOrderService.updateFromAddress(address)
    this.fromAddress = this.confirmOrderQuery.getValue().fulfillment.pickup_address;
    this.confirmOrderService.forceUpdateShippingService$.next();
  }

  showCreateOrder() {
    this.onSliderClosed();
    this.createNewOrder = true;
    this.slider.toggleLiteMode(this.createNewOrder);
    this.fulfillmentTable.toggleLiteMode(this.createNewOrder);
  }

  private setupDropdownActions(single: boolean, ffms: Fulfillment[]) {
    if (single) {
      const fulfillment = ffms[0];
      if (!fulfillment) {
        return;
      }
      return this.dropdownActions = [
        {
          onClick: () => this.cancel(true),
          title: `Huỷ đơn giao hàng`,
          cssClass: 'text-danger',
          permissions: ['shop/fulfillment:cancel'],
          hidden: !this.cancellable
        }
      ];
    }
    const availableFfms = ffms.filter(ffm => {
      return ffm.status == 'Z' || ffm.status == 'S';
    });
    const allCanceled = ffms.every(ffm => {
      return ffm.status == 'N' || ffm.status == 'NS' || ffm.status == 'P';
    });
    this.dropdownActions = [
      {
        onClick: () => this.cancel(false),
        title: `Huỷ ${availableFfms.length} đơn giao hàng`,
        cssClass: 'text-danger',
        permissions: ['shop/fulfillment:cancel'],
        hidden: allCanceled
      }
    ];
  }

  async cancel(single: boolean) {
    if (this.modal) {
      return;
    }
    const availableFfms = this.selectedFfms.filter(o =>
      ['Z', 'S'].includes(o.status)
    );
    const title = single
      ? `đơn giao hàng #${this.selectedFfms[0].shipping_code}`
      : `${availableFfms.length} đơn giao hàng`;
    this.modal = this.modalController.create({
      component: CancelObjectComponent,
      showBackdrop: true,
      cssClass: 'modal-md',
      componentProps: {
        title: title,
        subtitle: single ? null : availableFfms.map(f => `#${f.shipping_code}`).join(', '),
        reasons: CANCEL_ORDER_REASONS,
        multiple: !single,
        cancelFn: reason => this.cancelFn(reason, single, availableFfms)
      }
    });
    this.modal.show().then();
    this.modal.onDismiss().then(async () => {
      this.modal = null;
      await this.reloadFfms();
      this.setupDropdownActions(single, this.selectedFfms);
    });
    this.modal.onClosed().then(_ => {
      this.modal = null;
    });
  }

  async cancelFn(reason: string, single?: boolean, availableFfms?: Fulfillment[]) {
    try {
      let success = 0;
      let failed = 0;
      let errorMessage: string;
      const promises = availableFfms.map(f => async () => {
        try {
          await this.cancelSingleFfm(f, reason);
          success += 1;
        } catch (e) {
          errorMessage = e.message;
          failed += 1;
        }
      });
      await this.promiseQueue.run(promises, 10);
      this.sendTelegramMessage(reason, single);
      if (success == this.selectedFfms.length) {
        toastr.success(`Hủy thành công ${success} đơn giao hàng`);
      }
      if (success && success < this.selectedFfms.length) {
        toastr.warning(
          `Huỷ thành công ${success}/${this.selectedFfms.length} đơn giao hàng`
        );
      }
      if (failed == this.selectedFfms.length) {
        toastr.error(
          `Hủy thất bại ${this.selectedFfms.length} đơn giao hàng. ${errorMessage}`
        );
      }
    } catch (e) {
      toastr.error(
        'Huỷ đơn giao hàng không thành công.',
        (e.code && (e.message || e.msg)) || ''
      );
      throw e;
    }
  }

  async cancelSingleFfm(ffm: Fulfillment, reason) {
    if (this.ffmsController.fulfillment_type === 'shipnow') {
      await this.ffmApi.cancelShipnowFulfillment(ffm.id, reason);
    } else {
      await this.ffmApi.cancelShipmentFulfillment(ffm.id, reason);
    }
  }

  sendTelegramMessage(reason, single) {
    if (single) {
      this.telegram.sendCancelSingleOrderMessage(
        this.selectedFfms[0].order,
        reason
      );
      return;
    }
    this.telegram.sendCancelMultiFulfillmentMessage(
      this.selectedFfms.length,
      this.auth.snapshot.shop.name
    );
  }

  resetState() {
    this.slider.toggleLiteMode(false);
    this.fulfillmentTable.toggleLiteMode(false);
    this.checkAll = false;
  }

  resetFilter() {
    this.filters = [];
  }

  async filter($event: Filters) {
    this.selectedFfms = [];
    if (this.queryParams.code) {
      await this.router.navigateByUrl(`s/${this.util.getSlug()}/fulfillments`);
    }
    this.filters = $event;
    this.fulfillmentTable.resetPagination();
  }

  async loadAllReturningFfms() {
    this.filters = [
      {
        name: 'shipping_state',
        op: FilterOperator.in,
        value: 'returning'
      }
    ];
    await this.loadFulfillments(1, 1000);
    this.forcedHidePaging = true;
  }

  async getShipmentFfms(page, perpage) {
    try {
      this.resetState();
      this.fulfillmentTable.toggleNextDisabled(false);

      const res: any = await this.fulfillmentService.getFulfillments(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
      if (page > 1 && res.fulfillments.length == 0) {
        this.fulfillmentTable.toggleNextDisabled(true);
        this.fulfillmentTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả đơn giao hàng.');
        return;
      }

      this.fulfillmentList = res.fulfillments.map(ffm => ({...ffm, p_data: {}}));
      if (this.queryParams.code) {
        this.detail(null, this.fulfillmentList[0]);
      }

      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list ffms', e);
    }
  }

  async getShipnowFfms(page, perpage) {
    try {
      this.resetState();
      this.fulfillmentTable.toggleNextDisabled(false);

      let res = await this.fulfillmentService.getShipnowFulfillments(
        (page - 1) * perpage,
        perpage,
        this.filters
      );
      if (page > 1 && res.shipnow_fulfillments.length == 0) {
        this.fulfillmentTable.toggleNextDisabled(true);
        this.fulfillmentTable.decreaseCurrentPage(1);
        toastr.info('Bạn đã xem tất cả đơn giao hàng.');
        return;
      }

      this.fulfillmentList = res.shipnow_fulfillments.map(ffm => ({...ffm, p_data: {}}));
      if (this.queryParams.code) {
        this.detail(null, this.fulfillmentList[0]);
      }

      this.page = page;
      this.perpage = perpage;
    } catch (e) {
      debug.error('ERROR in getting list shipnow ffms', e);
    }
  }

  async loadFulfillments(page, perpage) {
    if (this.ffmsController.fulfillment_type == 'shipment') {
      await this.getShipmentFfms(page, perpage);
    }
    if (this.ffmsController.fulfillment_type == 'shipnow') {
      await this.getShipnowFfms(page, perpage);
    }
  }

  async reloadFfms() {
    const {perpage, page} = this;
    const selected_ffm_ids = this.selectedFfms.map(f => f.id);

    let _ffms: Fulfillment[];

    let res: any;
    if (this.ffmsController.fulfillment_type == 'shipment') {
      res = await this.fulfillmentService.getFulfillments((page - 1) * perpage, perpage, this.filters);
      _ffms = res.fulfillments.map(ffm => ({...ffm, p_data: {}}));
    }
    if (this.ffmsController.fulfillment_type == 'shipnow') {
      res = await this.fulfillmentService.getShipnowFulfillments((page - 1) * perpage, perpage, this.filters);
      _ffms = res.shipnow_fulfillments.map(ffm => ({...ffm, p_data: {}}));
    }


    this.fulfillmentList = _ffms.map(f => {
      if (selected_ffm_ids.indexOf(f.id) != -1) {
        const _ffm = this.selectedFfms.find(selected => selected.id == f.id);
        if (_ffm) {
          f.p_data = _ffm.p_data;
        }
      }
      return f;
    });
    this.selectedFfms = this.fulfillmentList.filter(
      o => o.p_data.selected || o.p_data.detailed
    );
  }

  async loadPage({page, perpage}) {
    this.forcedHidePaging = false;
    this.fulfillmentTable.loading = true;
    this.changeDetector.detectChanges();
    this._paramsHandling();
    await this.loadFulfillments(page, perpage);
    this.fulfillmentTable.loading = false;
    this.changeDetector.detectChanges();
  }

  private _checkSelectMode() {
    this.selectedFfms = this.fulfillmentList.filter(ffm => ffm.p_data.selected);
    this.setupDropdownActions(false, this.selectedFfms);

    this.checkAll = this.selectedFfms.length == this.fulfillmentList.length;
    const selected = this.selectedFfms.length;

    this.slider.toggleLiteMode(selected > 0);
    this.fulfillmentTable.toggleLiteMode(selected > 0);
  }

  itemSelected(item: Fulfillment) {
    this.fulfillmentList.forEach(f => {
      f.p_data.detailed = false;
    });
    item.p_data.selected = !item.p_data.selected;
    this.selectedFfms = this.fulfillmentList.filter(ffm => ffm.p_data.selected);
    if (!item.p_data.selected) {
      this.checkAll = false;
    }
    this._checkSelectMode();
  }

  allItemSelected() {
    this.checkAll = !this.checkAll;
    this.createNewOrder = false;
    this.fulfillmentList.forEach(ffm => {
      ffm.p_data.selected = this.checkAll;
      if (!this.checkAll) {
      ffm.p_data.detailed = false;
      }
    });
    this._checkSelectMode();
  }

  private _paramsHandling() {
    const {queryParams} = this.route.snapshot;
    this.queryParams.code = queryParams.code;
    this.queryParams.type = queryParams.type;
    if (!this.queryParams.code || !this.queryParams.type) {
      return;
    }
    /* NOTE: required jobs when have queryParams.code:
     * 1/ Turn on state selected of an ffm row
     * 2/ Open Slide to view ffm detail
     * 3/ Reload ffm list when close slider OR when fill-in new values to FILTER INPUTS
     * */
    if (['shipment', 'none'].indexOf(this.queryParams.type) != -1) {
      this.ffmsController.fulfillment_type = 'shipment';
      this.changeDetector.detectChanges();
    }
    if (this.queryParams.type == 'shipnow') {
      this.ffmsController.fulfillment_type = 'shipnow';
      this.changeDetector.detectChanges();
    }
    this.filters = [
      {
        name: 'shipping_code',
        op: FilterOperator.eq,
        value: this.queryParams.code
      }
    ];
  }

  detail(event, ffm: Fulfillment) {
    this.createNewOrder = false;
    this.changeTab('detail_info');
    if (event && event.target.type == 'checkbox') {
      return;
    }
    if (this.checkedFulfillments) {
      return;
    }
    this.fulfillmentList.forEach(f => {
      f.p_data.detailed = (f.id == ffm.id);
    });

    this.selectedFfms = [ffm];
    this.slider.toggleLiteMode(true);
    this.fulfillmentTable.toggleLiteMode(true);
    this.setupDropdownActions(true, this.selectedFfms);
  }

  onSliderClosed() {
    this.checkAll = false;
    this.createNewOrder = false;
    this.fulfillmentList.forEach(f => {
      f.p_data.detailed = false;
      f.p_data.selected = false;
    });
    this.selectedFfms = this.fulfillmentList.filter(f => f.p_data.detailed);
    this.slider.toggleLiteMode(false);
    this.fulfillmentTable.toggleLiteMode(false);
    if (this.queryParams.code && this.selectedFfms.length == 0) {
      this.filters = [];
      this.router.navigateByUrl(`s/${this.util.getSlug()}/fulfillments`).then(_ => {
        this.fulfillmentTable.resetPagination();
      });
    }
  }

  gotoImport() {
    this.toImport.emit();
  }

  gotoPOS() {
    this.showCreateOrder();
  }

  changeTab(tab_value) {
    this.activeTab = tab_value;
    this.ffmsController.onChangeTab$.next(tab_value);
  }

  showTab(tab) {
    if (this.fulfillmentType == 'shipnow') {
      return !['history', 'support_request'].includes(tab.value);
    }
    if (this.fulfillmentType == 'shipment') {
      return !['shipnow_route'].includes(tab.value);
    }
  }

  get checkedFulfillments() {
    return this.fulfillmentList.some(f => f.p_data.selected);
  }

  get showDetailFFM() {
    return this.selectedFfms.length == 1 && !this.checkedFulfillments;
  }

  get sliderTitle() {
    if (this.createNewOrder) {
      return 'Tạo đơn'
    }
    return (
      (this.showDetailFFM && 'Chi tiết đơn giao hàng') ||
      `Thao tác trên <strong>${this.selectedFfms.length}</strong> đơn giao hàng`
    );
  }

  get showPaging() {
    return !this.fulfillmentTable.liteMode && !this.fulfillmentTable.loading;
  }

  get fulfillmentType() {
    return this.ffmsController.fulfillment_type;
  }

  get emptyResultFilter() {
    return (
      this.page == 1 &&
      this.fulfillmentList.length == 0 &&
      this.filters.length > 0
    );
  }

  get emptyType(){
    if(this.page == 1 &&this.fulfillmentList.length == 0 && this.filters.length > 0)
      return EmptyType.search;
    return EmptyType.default;
  }

  get emptyTitle() {
    if (this.emptyResultFilter) {
      return 'Không tìm thấy đơn giao hàng phù hợp';
    }
    return 'Cửa hàng của bạn chưa có đơn giao hàng';
  }

  get cancellable() {
    return ['Z', 'S'].indexOf(this.selectedFfms[0]?.status) != -1;
  }

  toSetting() {
    window.open(`/s/${this.util.getSlug()}/settings/shipping_info`)
  }
}
