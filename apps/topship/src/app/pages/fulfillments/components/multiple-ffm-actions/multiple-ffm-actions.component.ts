import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { ModalController } from 'apps/core/src/components/modal-controller/modal-controller.service';
import { ExportFulfillmentModalComponent } from 'apps/topship/src/app/components/modals/export-fulfillment-modal/export-fulfillment-modal.component';
import { FulfillmentsController, PrintType } from 'apps/topship/src/app/pages/fulfillments/fulfillments.controller';
import { PrintControllerService } from 'apps/topship/src/app/components/print/print.controller';

@Component({
  selector: 'topship-multiple-ffm-actions',
  templateUrl: './multiple-ffm-actions.component.html',
  styleUrls: ['./multiple-ffm-actions.component.scss']
})
export class MultipleFfmActionsComponent implements OnInit, OnChanges  {
  @Input() ffms: Array<Fulfillment> = [];
  allNullActiveFfm = false;

  constructor(
    private fulfillmentsControler: FulfillmentsController,
    private modalController: ModalController,
    private printController: PrintControllerService,
  ) {
   }

  ngOnInit() {
  }

  printFulfillments() {
    this.printController.print(PrintType.fulfillments,{ffms: this.ffms});
  }


  ngOnChanges() {
    this.allNullActiveFfm = this.ffms.every(f => !f || !f.shipping_code);
  }

  exportFFM() {
    let modal = this.modalController.create({
      component: ExportFulfillmentModalComponent,
      componentProps: {
        ids: this.ffms
          .map(f => f.id)
      },
      showBackdrop: 'static'
    });
    modal.show();
  }
}
