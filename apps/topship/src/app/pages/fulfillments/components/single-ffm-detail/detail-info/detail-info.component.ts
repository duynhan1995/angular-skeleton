import { Component, Input, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import {OrderStoreService} from "apps/core/src/stores/order.store.service";
import { Fulfillment } from 'libs/models/Fulfillment';
import * as moment from 'moment';
import { FulfillmentService } from 'apps/topship/src/services/fulfillment.service';
import { FulfillmentsController } from 'apps/topship/src/app/pages/fulfillments/fulfillments.controller';

@Component({
  selector: 'topship-detail-info',
  templateUrl: './detail-info.component.html',
  styleUrls: ['./detail-info.component.scss'],
})
export class DetailInfoComponent implements OnInit, OnChanges {
  @Input() ffm = new Fulfillment({});
  constructor(
    private ffmService: FulfillmentService,
    private ffmsController: FulfillmentsController,
    private orderStore: OrderStoreService,
  ) { }

  ngOnInit() {
    this.initBarcode();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initBarcode();
  }

  initBarcode() {
    setTimeout(
      () => JsBarcode('#shipping-barcode', this.ffm.shipping_code),
      0
    );
  }

  trackingLink() {
    const link = Fulfillment.trackingLink(this.ffm);
    if (!link) { return; }
    window.open(link);
  }

  viewDetailOrder(order_code) {
    this.orderStore.navigate('so', order_code).then();
  }

  get isShipment() {
    return this.ffmsController.fulfillment_type == 'shipment';
  }

  get isShipnow() {
    return this.ffmsController.fulfillment_type == 'shipnow';
  }

}
