import { Component, Input, OnInit } from '@angular/core';
import { Fulfillment } from 'libs/models/Fulfillment';
import { Router } from '@angular/router';
import { FulfillmentService } from 'apps/topship/src/services/fulfillment.service';
import { FulfillmentsController } from 'apps/topship/src/app/pages/fulfillments/fulfillments.controller';
import { OrderStoreService } from 'apps/core/src/stores/order.store.service';
import { UtilService } from 'apps/core/src/services/util.service';

@Component({
  selector: '[topship-fulfillment-row]',
  templateUrl: './fulfillment-row.component.html',
  styleUrls: ['./fulfillment-row.component.scss']
})
export class FulfillmentRowComponent implements OnInit {
  @Input() fulfillment = new Fulfillment({});
  @Input() liteMode = false;

  constructor(
    private fulfillmentService: FulfillmentService,
    private ffmsController: FulfillmentsController,
    private router: Router,
    private orderStore: OrderStoreService,
    private util: UtilService
  ) {}

  get showCancelReason() {
    return this.fulfillment.shipping_state == 'cancelled' && this.fulfillment.cancel_reason;
  }

  get showErrorMessage(){
    return this.fulfillment.shipping_state !== 'cancelled' && this.fulfillment.status == 'N';
  }

  ngOnInit() {
  }

  get fulfillmentType() {
    return this.ffmsController.fulfillment_type;
  }

  get hasMoneyTransaction() {
    const id = this.fulfillment.money_transaction_shipping_id;
    return id && id != "0";
  }

  gotoMoneyTransactionDetail() {
    const id = this.fulfillment.money_transaction_shipping_id;
    if (!id || id == "0") {
      return;
    }
    const slug = this.util.getSlug();
    this.router.navigateByUrl(`/s/${slug}/money-transactions/${id}`);
  }

}
