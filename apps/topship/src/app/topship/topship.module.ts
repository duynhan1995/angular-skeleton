import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FromAddressModalModule} from "@etop/shared/components/from-address-modal/from-address-modal.module";

import { ShopRoutingModule } from './topship-routing.module';
import { TopshipComponent } from './topship.component';
import { CoreModule } from 'apps/core/src/core.module';
import { TopshipGuard } from './topship.guard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const pages = [];

@NgModule({
  declarations: [TopshipComponent, ...pages],
  providers: [TopshipGuard],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    ShopRoutingModule,
    FromAddressModalModule
  ]
})
export class TopshipModule { }
