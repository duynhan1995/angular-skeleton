import { Routes } from '@angular/router';

export const topshipRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'fulfillments',
    loadChildren: () => import('../pages/fulfillments/fulfillments.module').then(m => m.FulfillmentsModule)
  },
  {
    path: 'money-transactions',
    loadChildren: () => import('../pages/money-transactions/money-transactions.module').then(m => m.MoneyTransactionsModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'settings/:type',
    loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'supports',
    loadChildren: () => import('../pages/tickets-system/tickets-system.module').then(m => m.TicketsSystemModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('../pages/notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: 'etop-trading',
    loadChildren: () => import('../pages/product-trading/product-trading.module').then(m => m.ProductTradingModule)
  },
  {
    path: '**',
    redirectTo: 'fulfillments'
  }
];
