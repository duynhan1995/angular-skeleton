import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopshipComponent } from './topship.component';
import { TopshipGuard } from './topship.guard';
import { topshipRoutes } from './topship.route';

const routes: Routes = [
  {
    path: ':shop_index',
    canActivate: [TopshipGuard],
    resolve: {
      account: TopshipGuard
    },
    component: TopshipComponent,
    children: topshipRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
