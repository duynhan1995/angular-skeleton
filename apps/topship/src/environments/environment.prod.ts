export const environment = {
  production: true,
  hmr: false,

  base_url: "https://shop.topship.vn",
  app: "dashboard",
  ticket_environment: "production",
  admin_url: "https://admin.etop.vn",
  crm_url: "https://crm-service.etop.vn",
  prev_url: "https://prev.etop.vn",
  recaptcha_key: '6Letv3gUAAAAABhYCo3kUEzh-qHZWHN2qpVCKsco',
  onesignal_app_id: "d0a53f52-09a8-4674-b2e0-d08b454302d9",
  safari_web_id: "665e4130-cb92-443f-ad4d-8156961c0995",
};
