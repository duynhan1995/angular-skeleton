module.exports = {
  name: 'topship',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/topship/',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
